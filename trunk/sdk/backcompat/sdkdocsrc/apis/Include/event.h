// Event.h
//
// Copyright (C) 2009 McAfee, Inc.  All rights reserved. 
//
// Definitions for the NAI Event Interface
#ifndef _PLATFORM_H_
	#define _DODEFINITIONS_

    #if defined(EXPORT)
        #undef EXPORT
    #endif

    #define EXPORT


    #if defined(EXTERN_C)
        #undef EXTERN_C
    #endif

    #if defined(__cplusplus)
        #define EXTERN_C extern "C"
    #else
        #define EXTERN_C        
    #endif


#endif

#ifdef _NOT_MCPAL_PLATFORM_H_
	#define _DODEFINITIONS_

    #if defined(EXPORT)
        #undef EXPORT
    #endif

    #define EXPORT


    #if defined(EXTERN_C)
        #undef EXTERN_C
    #endif

    #if defined(__cplusplus)
        #define EXTERN_C extern "C"
    #else
        #define EXTERN_C        
    #endif


#endif



#ifndef _MCPAL_H_
#ifndef _DEFINE_tagMCPALERR_
#define _DEFINE_tagMCPALERR_
// ---------------------------------------------------------------------------
// Possible error codes from initializing the use of the MCPAL subsystem.
// ---------------------------------------------------------------------------

typedef enum tagMCPALERR
{
    MCPALERR_OK = 0,                    // No error, we are happy.
    MCPALERR_MEMALLOC,                  // Memory allocation error
    MCPALERR_NOINSTALLDIRREGVALUE,      // Can't find szInstallDir registry value.
    MCPALERR_NOSHAREDREGISTRY,          // Unable to open shared registry.
    MCPALERR_MCPALDIRNOTFOUND,          // The shared directory does not exist.
    MCPALERR_DATDIRNOTFOUND,            // Directory for the DAT files not found.
    MCPALERR_MCSCANDIRNOTFOUND,         // Dir for scan engine not found.
    MCPALERR_CANTLOADDLL,               // Unable to load specified dll
    MCPALERR_UNKNOWNDLL,                // Not recognized as valid dll
    MCPALERR_LOADINGMCKRNL,             // Unable to load NAKrnl32.dll
    MCPALERR_LOADINGMCGUI,              // Unable to load NAGui32.dll
    MCPALERR_LOADINGMCUTIL,             // Unable to load NAUtil32.dll
    MCPALERR_LOADINGMCSCAN,             // Unable to load MCScan32.dll
    MCPALERR_LOADINGMCARCHIV,           // Unable to load NAArchiv.dll
    MCPALERR_LOADINGNAEVENT,            // Unable to load NAEvent.dll
	MCPALERR_USINGCURRENTVERSION		// can't use specified version using Current instead

} MCPALERR;
#endif
#endif

#ifdef WANT_ANSI
#undef DEFINE_EVENT
#endif

#ifndef DEFINE_EVENT
#define DEFINE_EVENT

#ifndef WANT_ANSI
#ifndef NO_CENTRAL_ALERTING
//#include "centalrt.h"       // for historical apps
#endif

// Common Event Definitions
// (Note: Always use the name provided not the value)

// uSeverity values (naiTrapSeverity values)

typedef enum tagEVENTSEVERITY {
	SEVERITY_INFORMATIONAL  =0,
	SEVERITY_WARNING        =1,
	SEVERITY_MINOR          =2,
	SEVERITY_MAJOR          =3,
	SEVERITY_CRITICAL       =4    
	} EVENTSEVERITY;


// Meanings for the different levels:
//
// Informational	- 	events requiring no operator intervention - example scan complete
// Warning -		events which indicate a process failure  - example set to scan drive 		
//          		N: but no map was currently set up for drive N:
// Minor - 		events which require attention but nothing which could lead
// 			to a data loss -- example missing log file 
// Major - 		events which will need human attention - example Virus Found
// Critical - 		events  needing immediate attention -- example Virus Found
// 				unable to repair - any event which could lead to data loss if not 				
//              corrected

#endif


#ifndef WANT_ANSI
// Application IDs for uApplicationID
// ADD THREE ENTRIES (1 of 3)
typedef enum tagNAIAPPID {
 ID_UNDEFINED                   =   0,
 ID_VIRUSSCAN            		=	1,
 ID_NETSHIELDNT          		=	2,
 ID_NETSHIELDNW          		=	3,
 ID_GROUPSHIELD	      			=   4,
 ID_POLICYORCHESTRATOR   		=   5,
 ID_SCANNERENGINE        		=   6,
 ID_MANAGEMENTEDITION    		=   7,
 ID_WEBSHIELD            		=	8,
 ID_ALERTMANAGER         		=   9,
 ID_AUTOIMMUNE           		=	10,
 ID_INSTALLER                   =   11,
 ID_OUTBREAK_MANAGER            =   12,
 ID_EPOLICYORCHESTRATOR_AGENT   =   13,
 ID_VIRUSSCANTC					=	14,
 ID_MYCIO						=   15,
 ID_GROUPSHIELD_EXCHANGE		=   16,
 ID_GROUPSHIELD_DOMINO			=   17,
 ID_WEBSHIELD_EPPLIANCE			=   18,
 ID_COMMON_MANAGEMENT_AGENT		=	19,
 ID_COMMON_LICENSING_AGENT		=	20,
 ID_VIRUSSCANENT				=   21,
 ID_TALKBACK					=	22,
 ID_VIRUSSCANENTSRV				=	23,
 ID_PORTALSHIELD			    =	24,
 ID_SPAMKILLER					=	25,
 ID_VSPDA2						=   26,
 ID_VIRUSSCANNETAPP				=   27,
 ID_LINUXSHIELD					=	28,
 ID_ENTERCEPT				    =   29,
 ID_INTRUVERT					=   30,
 ID_NATIVESCAN					=	31,
 ID_SCE							=	32,
 ID_ANTISPYWAREENTERPRISE		=	33,
 ID_HOST_INTRUSION_PROTECTION	=   34,
 ID_INVALID_ID					=	35


} NAIAPPID;



// Application Names (16 chars or less)
// (2 of 3)
// (GLOBALIZATION DO NOT TRANSLATE THESE!!!)
#define NAME_VIRUSSCAN          		TEXT("VirusScan")
#define NAME_NETSHIELDNT        		TEXT("NetShield")
#define NAME_NETSHIELDNW        		TEXT("NetShield NW")
#define NAME_GROUPSHIELD       		    TEXT("GroupShield")
#define NAME_POLICYORCHESTRATOR 	    TEXT("PolOrchestrator")
#define NAME_SCANNERENGINE      		TEXT("Scan Engine")
#define NAME_MANAGEMENTEDITION  	    TEXT("Mgmt Edition")
#define NAME_WEBSHIELD          		TEXT("WebShield")
#define NAME_ALERTMANAGER       		TEXT("Alert Manager")
#define NAME_AUTOIMMUNE         		TEXT("AutoImmune")
#define NAME_INSTALLER                  TEXT("Installer")
#define NAME_OUTBREAKMGR                TEXT("OutbreakManager")
#define NAME_EPOLICYORCHESTRATORAGENT   TEXT("ePO Agent")
#define NAME_VIRUSSCANTC          		TEXT("VirusScan TC")
#define NAME_MYCIO						TEXT("MyCIO.COM")
#define NAME_GROUPSHIELD_EXCHANGE		TEXT("GS Exchange")
#define NAME_GROUPSHIELD_DOMINO			TEXT("GS Domino")
#define NAME_WEBSHIELD_EP				TEXT("Webshield Eplnc")
#define NAME_COMMON_MANAGEMENT_AGENT	TEXT("Common Managemt")
#define NAME_COMMON_LICENSING_AGENT		TEXT("Licensing")
#define NAME_VIRUSSCANENT				TEXT("VirusScan Ent.")
#define NAME_TALKBACK					TEXT("Talkback")
#define NAME_VIRUSSCANENTSRV			TEXT("VirusScan EntSv")
#define NAME_PORTALSHIELD				TEXT("PortalShield")
#define NAME_SPAMKILLER					TEXT("SpamKiller")
#define NAME_VSPDA2						TEXT("VirusScan PDA 2")
#define NAME_VIRUSSCANNETAPP			TEXT("VirusScanNetApp")
#define NAME_LINUXSHIELD				TEXT("SecurityShield")
#define NAME_ENTERCEPT					TEXT("Entercept")
#define NAME_INTRUVERT					TEXT("Intruvert")
#define NAME_NATIVESCAN					TEXT("NativeScan")
#define NAME_SCE						TEXT("SCE")
#define NAME_ANTISPYWAREENTERPRISE		TEXT("AntiSpyware")
#define NAME_HOST_INTRUSION_PROTECTION  TEXT("Host IPS")

#define KEYNAME_VIRUSSCAN				TEXT("VSC")
#define KEYNAME_NETSHIELDNT        		TEXT("NetShield")
#define KEYNAME_NETSHIELDNW        		TEXT("NetShield NW")
#define KEYNAME_GROUPSHIELD       		   TEXT("GroupShield")
#define KEYNAME_POLICYORCHESTRATOR 	    TEXT("PolOrchestrator")
#define KEYNAME_SCANNERENGINE      		TEXT("Scan Engine")
#define KEYNAME_MANAGEMENTEDITION  	    TEXT("Mgmt Edition")
#define KEYNAME_WEBSHIELD          		TEXT("WebShield")
#define KEYNAME_ALERTMANAGER       		TEXT("AMG")
#define KEYNAME_AUTOIMMUNE         		TEXT("AutoImmune")
#define KEYNAME_INSTALLER                  TEXT("Installer")
#define KEYNAME_OUTBREAKMGR                TEXT("OutbreakManager")
#define KEYNAME_EPOLICYORCHESTRATORAGENT   TEXT("ePO Agent")
#define KEYNAME_VIRUSSCANTC          		TEXT("VirusScan TC")
#define KEYNAME_MYCIO						TEXT("MyCIO.COM")
#define KEYNAME_GROUPSHIELD_EXCHANGE		TEXT("GS Exchange")
#define KEYNAME_GROUPSHIELD_DOMINO			TEXT("GS Domino")
#define KEYNAME_WEBSHIELD_EP				TEXT("Webshield Eplnc")
#define KEYNAME_COMMON_MANAGEMENT_AGENT	TEXT("Common Managemt")
#define KEYNAME_COMMON_LICENSING_AGENT		TEXT("Licensing")
#define KEYNAME_VIRUSSCANENT				TEXT("VSE")
#define KEYNAME_TALKBACK					TEXT("Talkback")
#define KEYNAME_VIRUSSCANENTSRV				TEXT("VSE")				// note this is a dup intentionally
#define KEYNAME_PORTALSHIELD				TEXT("PortalShield")
#define KEYNAME_SPAMKILLER					TEXT("SpamKiller")
#define KEYNAME_VSPDA2						TEXT("VSPDA2")
#define KEYNAME_VIRUSSCANNETAPP				TEXT("VSE")
#define KEYNAME_LINUXSHIELD				TEXT("LinuxShield")
#define KEYNAME_ENTERCEPT					TEXT("Entercept")
#define KEYNAME_INTRUVERT					TEXT("Intruvert")
#define KEYNAME_NATIVESCAN					TEXT("Native")
#define KEYNAME_SCE						TEXT("SCE")
#define KEYNAME_ANTISPYWAREENTERPRISE		TEXT("AntiSpyware")
#define KEYNAME_HOST_INTRUSION_PROTECTION   TEXT("Host IPS")

#ifdef DEFINE_GLOBALS
// (3 of 3)
TCHAR *AppNames[100]=
{ 
	TEXT("-"),				// will get loaded from resource DLL
	NAME_VIRUSSCAN,          
	NAME_NETSHIELDNT,        
	NAME_NETSHIELDNW,        
	NAME_GROUPSHIELD,       	
	NAME_POLICYORCHESTRATOR, 
	NAME_SCANNERENGINE,      
	NAME_MANAGEMENTEDITION,  
	NAME_WEBSHIELD,          
	NAME_ALERTMANAGER,       
	NAME_AUTOIMMUNE,         
	NAME_INSTALLER,          
	NAME_OUTBREAKMGR,
	NAME_EPOLICYORCHESTRATORAGENT,
	NAME_VIRUSSCANTC,
	NAME_MYCIO,
	NAME_GROUPSHIELD_EXCHANGE,
	NAME_GROUPSHIELD_DOMINO,
	NAME_WEBSHIELD_EP,
	NAME_COMMON_MANAGEMENT_AGENT,
	NAME_COMMON_LICENSING_AGENT,
	NAME_VIRUSSCANENT,
	NAME_TALKBACK,
	NAME_VIRUSSCANENTSRV,
	NAME_PORTALSHIELD,
	NAME_SPAMKILLER,
	NAME_VSPDA2,
	NAME_VIRUSSCANNETAPP,
	NAME_LINUXSHIELD,
	NAME_ENTERCEPT,
	NAME_INTRUVERT,
	NAME_NATIVESCAN,
	NAME_SCE,
	NAME_ANTISPYWAREENTERPRISE,
	NAME_HOST_INTRUSION_PROTECTION

	};

TCHAR * Appkeynames[100]=
{
	TEXT("-"),				// will get loaded from resource DLL
	KEYNAME_VIRUSSCAN,          
	KEYNAME_NETSHIELDNT,        
	KEYNAME_NETSHIELDNW,        
	KEYNAME_GROUPSHIELD,       	
	KEYNAME_POLICYORCHESTRATOR, 
	KEYNAME_SCANNERENGINE,      
	KEYNAME_MANAGEMENTEDITION,  
	KEYNAME_WEBSHIELD,          
	KEYNAME_ALERTMANAGER,       
	KEYNAME_AUTOIMMUNE,         
	KEYNAME_INSTALLER,          
	KEYNAME_OUTBREAKMGR,
	KEYNAME_EPOLICYORCHESTRATORAGENT,
	KEYNAME_VIRUSSCANTC,
	KEYNAME_MYCIO,
	KEYNAME_GROUPSHIELD_EXCHANGE,
	KEYNAME_GROUPSHIELD_DOMINO,
	KEYNAME_WEBSHIELD_EP,
	KEYNAME_COMMON_MANAGEMENT_AGENT,
	KEYNAME_COMMON_LICENSING_AGENT,
	KEYNAME_VIRUSSCANENT,
	KEYNAME_TALKBACK,
	KEYNAME_VIRUSSCANENTSRV,
	KEYNAME_PORTALSHIELD,
	KEYNAME_SPAMKILLER,
	KEYNAME_VSPDA2,
	KEYNAME_VIRUSSCANNETAPP,
	KEYNAME_LINUXSHIELD,
	KEYNAME_ENTERCEPT,
	KEYNAME_INTRUVERT,
	KEYNAME_NATIVESCAN,
	KEYNAME_SCE,
	KEYNAME_ANTISPYWAREENTERPRISE,
	KEYNAME_HOST_INTRUSION_PROTECTION

	};


int gAppNameCount = sizeof( AppNames )/sizeof(TCHAR *);
#else
extern TCHAR *AppNames[];
extern TCHAR *Appkeynames[];
extern int gAppNameCount;
#endif




typedef enum tagEVENTSTATUS {
    ES_SUCCESS=0,
    ES_FAIL,                         // @emem unknown failure
    ES_BADCONFIG,                    // @emem Configuration not properly set up
    ES_INVALIDAPPID,                 // @emem Invalid application Id passed in
    ES_NO_WRITE_ACCESS,              // @emem We do not have write access to our own registry      
    ES_INVALID_PARAMETERS,           // @emem The input parameters are not valid. Either the centralalert or the message is NULL
    ES_SEND_ALERT,                   // @emem Some error occurred while sending the alert. Check the dwErr for specific error
    ES_LOAD_AMINTERFACE,             // @emem The client interface dll, AlertMgrIf.dll couldn't be loaded
    ES_ADSEARCH,                     // @emem The Active Directory Lookup for Alert Manager Failed.
    ES_EVENT_NOT_IN_REGISTRY,        // @emem that event is not in the registry
    ES_EVENT_MEMALLOC,
    ES_AD_NOT_AVAILABLE,             // @emem Active Directory Support Not Available
    ES_NODEFAULT,                    // @emem No Default Alert Manager is published
	ES_CANCELED,					 // user canceled
} EVENTSTATUS;



	#ifndef MCPAL_LITE
		#define USE_LOCAL_COMPUTER_PARAM TEXT("Local Computer")  // TRANSLATOR DONT TRANSLATE
		#define USE_LOCAL_COMPUTER TEXT("Local Computer")  // TRANSLATOR DONT TRANSLATE
	#endif



#endif



#ifdef WANT_ANSI
	#undef XCHAR
	#define XCHAR char
#else
	#define XCHAR TCHAR
#endif

typedef int EVENTID;
#define MAX_LONG_DESCRIPTION    1024
#define VERSION_NAISTANDARDMIB 1

typedef struct 

#ifdef WANT_ANSI
	tagNAISTANDARDMIB_ANSI
#else
	tagNAISTANDARDMIB
#endif
{
    UINT        uVersion;               // Structure version, mandatory
                                        // from VERSION_NAISTANDARDMIB

// BEGIN OF VERSION 1 FIELDS

 // in the NAI Standard MIB
    XCHAR       szSoftware[16];         // Originator use Application NAME_
                                        // mandatory  
                                        // %SOFTWARENAME%
                                        // naiTrapAgent
                                        // {enterprises NAI 1 1 }

    XCHAR       szSoftwareVersion[16];  // Application Version (String)
                                        // mandatory
                                        // %SOFTWAREVERSION%
                                        // naiTrapAgentVersion
                                        // {enterprises NAI 1 17 }

    XCHAR       szTaskID[16];           // task name app specific component diagnostic id
                                        // optional
                                        // %TASKID%
                                        // naiTrapAgentVersion
                                        // {enterprises NAI TVD 11 }
                                    
    XCHAR       szGMTTime[32];          // GMT time of the Event
                                        // mandatory
                                        // %GMTTIME%
                                        // naiTrapGMTTime
                                        // {enterprises NAI 1 7 }

    XCHAR       szLocalTime[32];        // local time of the event
                                        // optional
                                        // %LOCALTIME%
                                        // naiTrapLocalTime
                                        // {enterprises NAI 1 8 }

    
    XCHAR       sznaiTrapLongDescription[MAX_LONG_DESCRIPTION];
                                        // long description
                                        // mandatory
                                        // %LONGDESCRIPT%
                                        // naiTrapLongDescription
                                        // {enterprises NAI 1 11 }

    XCHAR       sznaiTrapProblemResolution[256];
                                        // suggested resolution
                                        // optional
                                        // %RESOLUTION%
                                        // naiTrapProblemResolution
                                        // {enterprises NAI 1 12 }

   
    UINT  uSeverity ;                   // The severity of the alert SEVERITY_xxx
                                        // mandatory
                                        // %SEVERITY%
                                        // naiTrapSeverity
                                        // {enterprises NAI 1 2 }

    XCHAR      sznaiTrapShortDescription[32];
                                        // short description 32 chars or less
                                        // mandatory
                                        // %SHORTDESCRIPT%
                                        // naiTrapShortDescription
                                        // {enterprises NAI 1 3 }

    XCHAR      szComputerName[64];      // Computer name (DNS name)
                                        // optional
                                        // %COMPUTERNAME%
                                        // naiTrapSourceDNSName
                                        // {enterprises NAI 1 5 }
                                        
	XCHAR      szSourceIP[16];         // IP Address of the machine
                                        // mandatory
                                        // %SOURCEIP%
                                        // naiTrapSourceIPAddress
                                        // {enterprises NAI 1 4 }

	XCHAR      sznaiTrapSourceMACAddress[64];
                                        // MAC Address of the machine
                                        // optional
                                        // %SOURCEMAC%
                                        // naiTrapSourceMACAddress
                                        // {enterprises NAI 1 13 }

	XCHAR      sznaiTrapSourceSegmentName[64];
                                         // identifies source that detected ???? tbd
                                        // optional
                                        // %SOURCESEG%
                                        // naiTrapSourceSegmentName
                                        // {enterprises NAI 1 6 }


    XCHAR      sznaiTrapTargetName[64]; // Name of infected machine (if different from
                                        //   the name of the machine in pszComputerName)
                                        // optional
                                        // %TARGETCOMPUTERNAME%
                                        // naiTrapTargetDNSName
                                        // {enterprises NAI 1 15 }
                                        
	XCHAR       sznaiTrapTargetIPAddress[16];
                                        // IP Address of the infected machine(if different from
                                        //    the name of the machine in pszSourceIP)
                                        // mandatory
                                        // %TARGETIP%
                                        // naiTrapTargetIPAddress
                                        // {enterprises NAI 1 14 }

	XCHAR       sznaiTrapTargetMACAddress[64];
                                        // MAC Address of the infected machine(if different from
                                        //    the name of the machine in psznaiTrapTargetMACAddress)
                                        // optional
                                        // %TARGETMAC%
                                        // naiTrapTargetMACAddress
                                        // {enterprises NAI 1 16 }


    XCHAR       sznaiTrapURL[256];      // fully qualified URL to more information HTTP or FTP
                                        // mandatory
                                        // %URL%
                                        // naiTrapURL;
                                        // {enterprises NAI 1 10 }

                                        
    // end of version 1 fields

	UINT	uGMT_Month;
	UINT	uGMT_Day;
	UINT	uGMT_Year;
	UINT	uGMT_Hr;
	UINT	uGMT_Min;
	UINT	uGMT_Sec;
	UINT	uMonth;
	UINT	uDay;
	UINT	uYear;
	UINT	uHr;
	UINT	uMin;
	UINT	uSec;
    XCHAR       szGMTTimeF[64];          // GMT time of the Event
                                        // mandatory
                                        // %GMTTIME%
                                        // naiTrapGMTTime
                                        // {enterprises NAI 1 7 }

    XCHAR       szLocalTimeF[64];        // local time of the event
                                        // optional
                                        // %LOCALTIME%
                                        // naiTrapLocalTime
                                        // {enterprises NAI 1 8 }



  // end of the NAI standard MIB portion

}
#ifdef WANT_ANSI
	NAISTANDARDMIB_ANSI, * PNAISTANDARDMIB_ANSI;
#else
	NAISTANDARDMIB, * PNAISTANDARDMIB;
#endif


#define VERSION_TVDSTANDARDMIB 1

// TVDStandardMIB Structure
// these fields are required for all TVD applications

// For SNMP purposes these are all fields used in 
//	{ enterprises NAI TVD TVDCOMMON } 	{1.3.6.1.4.1.3401.12.1}

typedef struct 
#ifdef WANT_ANSI
	tagTVDSTANDARDMIB_ANSI
#else
	tagTVDSTANDARDMIB
#endif
{
    UINT        uVersion;               // Structure version, mandatory
                                        // from VERSION_TVDSTANDARDMIB

// BEGIN OF VERSION 1 FIELDS

 // in the TVD Standard MIB (optional fields are to be filled in if they apply to the message)

	UINT        uMessageID ;            // The psuedo-trap ID for that message
                                        // these come from the TVD Common MSG header
                                        // mandatory
                                        // %TRAPID%
                                        // { enterprises NAI TVD 1 1 }


	XCHAR       szEngineVersion[16];    // Scan Engine Version
                                        // optional
                                        // %ENGINEVERSION%
                                        // { enterprises NAI TVD 1 2 }

	XCHAR       szDatVersion[16] ;      // DAT file version
                                        // optional
                                        // %DATVERSION%
                                        // { enterprises NAI TVD 1 3 }


    UINT         uStatus;               // From VSERROR.H file
                                        // optional
                                        // %ENGINESTATUS%
                                        // { enterprises NAI TVD 1 4 }


	XCHAR       szVirusName[64];        // Virus name
                                        // optional
                                        // %VIRUSNAME%
                                        // { enterprises NAI TVD 1 5 }

    XCHAR       szVirusType[32];        // Virus type (trojan,varient,etc)
                                        // optional
                                        // %VIRUSTYPE%
                                        // { enterprises NAI TVD 1 6 }

   	XCHAR       szFileName[1024];       // Item name must include a full path (size based on Virnotify.h max)
                                        // mandatory
                                        // %FILENAME% 
                                        // { enterprises NAI TVD 1 7 }


    XCHAR       szUserName[64];         // User name
                                        // optional
                                        // %USERNAME%
                                        // { enterprises NAI TVD 1 8 }

    	
	XCHAR       szOS[32];               // OS platform and version
                                        // optional
                                        // %OS%
                                        // { enterprises NAI TVD 1 9 }

    XCHAR       szSerialNumber[32];     // Processor Serial number
                                        // optional (mandatory for PIII)
                                        // %PROCESSORSERIAL%
                                        // { enterprises NAI TVD 1 10 }

    XCHAR       szTaskName[16];         // task name app specific component diagnostic id
                                        // optional
                                        // %TASKNAME%
                                        // naiTrapAgentVersion
                                        // {enterprises NAI TVD 1 11 }

    // messages associated with an end of scan

	UINT		uNumberVirusesFound;    // total number of infections found
				                        // optional (required if EVENT_ indicating end of scan
                                        // %NUMVIRS%
				                        // {enterprises NAI TVD 1 15 }

	UINT		uNumberVirusesCleaned;  // total number of infections cleaned
				                        // optional (required if EVENT_ indicating end of scan
                                        // %NUMCLEANED%
				                        // {enterprises NAI TVD 1 16 }

	UINT		uNumberVirusesDeleted;  // total number of viruses deleted
	                        			// optional (required if EVENT_ indicating end of scan
                                        // %NUMDELETED%
				                        // {enterprises NAI TVD 1 17 }

	UINT		uNumberVirusesQuarantined; // total number quaranteened
				                        // optional (required if EVENT_ indicating end of scan
                                        // %NUMQUARANTINED%                    
				                        // {enterprises NAI TVD 1 18 }

	UINT		uODSReturnCode;         // end of on demand scan return code
				                        // optional (required if  indicating end of on-demand scan
                                        // %SCANRETURNCODE%
				                        // {enterprises NAI TVD 1 19 }


// for expansion
    XCHAR      szx1[64];      //
				                        //
                                        //
				                        //
    
    XCHAR      szx2[64];        //
				                        //
                                        //
				                        //

    XCHAR      szx3[64];        //
				                        //
                                        //
				                        //

    XCHAR      szx4[64];   //
				                        //
                                        //
				                        //

	XCHAR		szOBRuleName [256];		//%OBRULENAME%

				                        //
                                        //
                                        //

    XCHAR       szLanguage[16];         // optional
                                        // %LANGUAGECODE% (from originating machine)
                                        // {enterprises NAI TVD 1 40 }

    XCHAR       szClientComputerName[64];// optional
                                        // %CLIENTCOMPUTER%
                                        // {enterprises NAI TVD 1 41 }

    UINT        uClientID;              // optional (if terminal server this is the CLient ID for TS)
                                        // %TSCLIENTID%
                                        // {enterprises NAI TVD 1 42 }

    XCHAR       szAccessProcessName[64];// optional
                                        // %ACCESSPROCESSNAME%
                                            // {enterprises NAI TVD 1 43 }



    XCHAR       szEventIDString[64];    // the "EVENT_xxx" name used by the user
                                        // %EVENTNAME%

    XCHAR       szInfo[64];             // additional string up to 64 char
                                        // %INFO%

    UINT        uAlwaysAlert;           // normally zero

    // Mail related fields

    XCHAR      szMailFromName[256];     //
				                        //optional (required for Mail Viruses?)
                                        // %MAILFROMNAME%
				                        // {enterprises NAI TVD 1 30 }
    
    XCHAR      szMailToName[1024];      //
				                        //optional (required for Mail Viruses?)
                                        // %MAILTONAME%
				                        // {enterprises NAI TVD 1 31 }

    XCHAR      szMailCCName[1024];      //
				                        //optional (required for Mail Viruses?)
                                        // %MAILCCNAME%
				                        // {enterprises NAI TVD 1 32 }

    XCHAR      szMailSubjectLine[256];  //
				                        //optional (required for Mail Viruses?)
                                        // %MAILSUBJECTLINE%
				                        // {enterprises NAI TVD 1 33 }

    XCHAR      szMailIdentifierInfo[256];    //
				                        //optional (required for Mail Viruses?)
                                        // %MAILIDENTIFIERINFO%
                                        // {enterprises NAI TVD 1 34 }

    XCHAR      szNoteID[16];            //%NOTEID%				STRING 11 chars

    XCHAR      szNotesServerName[258];  //%NOTESSERVERNAME%		STRING 257 chars

    XCHAR      szNotesDBName[258];      //%NOTESDBNAME%			STRING 257 chars

	XCHAR	   szDomain[256];		//%DOMAIN%		

	XCHAR		* lpFilename;					// long pointer to an long filename
	XCHAR		* recipientlist;

    // end of version 1 fields

	

  // end of the TVD standard MIB portion

}
#ifdef WANT_ANSI
	TVDSTANDARDMIB_ANSI, * PTVDSTANDARDMIB_ANSI;
#else
	TVDSTANDARDMIB, * PTVDSTANDARDMIB;
#endif


#define VERSION_APPSTANDARDMIB 1

//APPStandardMIB structure
//each application uses this structure differently 

// all TVD apps will fill in this structure for app specific messages

typedef struct 
#ifdef WANT_ANSI
	tagAPPSTANDARDMIB_ANSI
#else
	tagAPPSTANDARDMIB
#endif
{
    UINT        uVersion;               // Structure version, mandatory
                                        // from VERSION_APPSTANDARDMIB

// BEGIN OF VERSION 1 FIELDS

 // in the APP Standard MIB

	//
    // Extra fields added to send more info thru snmp trap to snmp manager
	//

    UINT        uApplicationID;         // from header file
                                        // sets base { enterprises NAI TVD appid }
    
	UINT        uMessageID ;            // The psuedo-trap ID for that message
                                        // these come from the Application specific MSG header	
                                        // { enterprises NAI TVD appid 1 }

    XCHAR       szMessageBody[256];     // app specific message body
                                        // may have fields such as %name1% %name2% %name3"
                                        // { enterprises NAI TVD appid 2 }

    XCHAR       pszParameters[256];          // string parameters in the form
                                        // name1="param1",name2="param2",...
                                        // breaks into { enterprises NAI TVD appid n }

	XCHAR		* lpParam;				// long pointer to an extended param

                                        
  // end of the TVD standard MIB portion

} 
#ifdef WANT_ANSI
	APPSTANDARDMIB_ANSI, * PAPPSTANDARDMIB_ANSI;
#else
	APPSTANDARDMIB, * PAPPSTANDARDMIB;
#endif

#ifndef WANT_ANSI


// function to load only NaEvent from a specific path

EXTERN_C int EXPORT WINAPI Event_Init( NAIAPPID appid, TCHAR * path);

// function to unload only NaEvent

EXTERN_C VOID EXPORT Event_DeInit( void );

EXTERN_C VOID EXPORT WINAPI SetAppID( NAIAPPID appid );
typedef VOID (WINAPI * LPFNSETAPPID)( int appid );
                                           
EXTERN_C VOID EXPORT WINAPI SetAppIDEx( NAIAPPID appid, TCHAR * appname, TCHAR * keyname );
typedef VOID (WINAPI * LPFNSETAPPIDEX)( int appid, TCHAR * appname, TCHAR * keyname );

EXTERN_C VOID EXPORT WINAPI SetAppIDExx( NAIAPPID appid, TCHAR * appname, TCHAR * keyname,TCHAR * productver );
typedef VOID (WINAPI * LPFNSETAPPIDEXX)( int appid, TCHAR * appname, TCHAR * keyname ,TCHAR * productver);


EXTERN_C BOOL EXPORT WINAPI SetEPOForwarding( BOOL bXMLForwarding );
typedef BOOL (WINAPI * LPFNSETEPOFORWARDING)( BOOL bXMLForwarding );

// function for sending alerts and or events
	
EXTERN_C BOOL  EXPORT   WINAPI    IsEventClientConfigurable( void );
typedef BOOL        ( WINAPI * LPFNISEVENTCLIENTCONFIGURABLE) ( void );
EXTERN_C BOOL  EXPORT   WINAPI    IsRemoteEventClientConfigurable( TCHAR*server );
typedef BOOL        ( WINAPI * LPFNISREMOTEEVENTCLIENTCONFIGURABLE) ( TCHAR*server );

//For VirusScan ONLY FOR NOW
EXTERN_C BOOL  EXPORT   WINAPI    HasEventClientBeenConfigured( void );
typedef BOOL        ( WINAPI * LPFNHASEVENTCLIENTBEENCONFIGURED) ( void );


EXTERN_C EVENTSTATUS  EXPORT WINAPI ConfigureEventMgrClient( HWND hInst );
typedef EVENTSTATUS    ( WINAPI * LPFNCONFIGUREEVENTMGRCLIENT )( HWND hInst );

EXTERN_C EVENTSTATUS  EXPORT WINAPI ConfigureEventMgrClientRemote( TCHAR*Server,HWND hInst );
typedef EVENTSTATUS    ( WINAPI * LPFNCONFIGUREEVENTMGRCLIENTREMOTE )( TCHAR*Server,HWND hInst );

EXTERN_C BOOL WINAPI GetEventMgrConfig(

	TCHAR*remoteserver,					// [i] machine to query config for
	NAIAPPID id,						// [i] application id to query
	TCHAR * serverpath,					// [i/o] server name of destination
	TCHAR * logicalname,				// [i/o] logical name for active dir
	DWORD size,							// [i] size for server
	DWORD* bActivedir,					// [i/o] is active dir the config
	DWORD* bNamepipe,					// [i/o] is named pipe the config (if both bActivedir and bNamepipe fals then centalert
	DWORD* bActiveDefault);				// [i/o] is the active dir server the default


						
typedef BOOL (WINAPI *LPFNGETEVENTMGRCONFIG)(
	TCHAR*remoteserver,					// [i] machine to query config for
	NAIAPPID id,						// [i] application id to query
	TCHAR * serverpath,					// [i/o] server name of destination
	TCHAR * logicalname,				// [i/o] logical name for active dir
	DWORD size,							// [i] size for server
	DWORD* bActivedir,					// [i/o] is active dir the config
	DWORD* bNamepipe,					// [i/o] is named pipe the config (if both bActivedir and bNamepipe fals then centalert
	DWORD* bActiveDefault) ;				// [i/o] is the active dir server the default



EXTERN_C BOOL WINAPI GetEventMgrConfigExtended(

	TCHAR*remoteserver,					// [i] machine to query config for
	NAIAPPID id,						// [i] application id to query
	TCHAR * serverpath,					// [i/o] server name of destination
	TCHAR * logicalname,				// [i/o] logical name for active dir
	DWORD size,							// [i] size for server
	DWORD* bActivedir,					// [i/o] is active dir the config
	DWORD* bNamepipe,					// [i/o] is named pipe the config (if both bActivedir and bNamepipe fals then centalert
	DWORD* bActiveDefault,				// [i/o] is the active dir server the default
	DWORD* bLogLocal,
	DWORD* bSNMPLocal,
	DWORD* dwSeveritySuppressBelow);


						
typedef BOOL (WINAPI *LPFNGETEVENTMGRCONFIGEXTENDED)(
	TCHAR*remoteserver,					// [i] machine to query config for
	NAIAPPID id,						// [i] application id to query
	TCHAR * serverpath,					// [i/o] server name of destination
	TCHAR * logicalname,				// [i/o] logical name for active dir
	DWORD size,							// [i] size for server
	DWORD* bActivedir,					// [i/o] is active dir the config
	DWORD* bNamepipe,					// [i/o] is named pipe the config (if both bActivedir and bNamepipe fals then centalert
	DWORD* bActiveDefault,				// [i/o] is the active dir server the default
	DWORD* bLogLocal,
	DWORD* bSNMPLocal,
	DWORD* dwSeveritySuppressBelow);

EXTERN_C BOOL WINAPI SetEventMgrConfigExtended (
	TCHAR*remoteserver,					// [i] machine to query config for
	DWORD bLogLocal,
	DWORD bSNMPLocal,
	DWORD dwSeveritySuppressBelow);

typedef BOOL (WINAPI *LPFNSETEVENTMGRCONFIGEXTENDED)(
	TCHAR*remoteserver,					// [i] machine to query config for
	DWORD bLogLocal,
	DWORD bSNMPLocal,
	DWORD dwSeveritySuppressBelow);


EXTERN_C EVENTSTATUS  EXPORT WINAPI ConfigureEventMgrClientEx( TCHAR *szAMServer, HWND hInst, BOOL bUseAdvancedSearch); // direct to advanced configuration screen
typedef EVENTSTATUS    ( WINAPI * LPFNCONFIGUREEVENTMGRCLIENTEX )( TCHAR *szAMServer, HWND hInst, BOOL bUseAdvancedSearch ); // this is mainly for NetShield NT

EXTERN_C EVENTSTATUS EXPORT WINAPI SelectAlertManagerDestination( 
         TCHAR * NameOutBuffer,     // [out] buffer for the result (server name or logical name)
         DWORD dSizeNameBuffer,     // [in]  size of buffer passed in
         DWORD * bActiveDirUsed);   // [out] 1 if Active Dir was used, 2 if not

typedef EVENTSTATUS (  WINAPI * LPFNSELECTALERTMANAGERDESTINATION)( 
         TCHAR * NameOutBuffer,     // [out] buffer for the result (server name or logical name)
         DWORD dSizeNameBuffer,     // [in]  size of buffer passed in
         DWORD * bActiveDirUsed);   // [out] 1 if Active Dir was used, 2 if not

EXTERN_C EVENTSTATUS EXPORT WINAPI GetDefaultPublishedAM(
         TCHAR * LogicalNameOutBuffer,     // [out] buffer for the result (logical name)
         TCHAR * ServerNameOutBuffer,      // [out] buffer for the result (servername)
         DWORD dSizeNameBuffer);    // [in]  size of buffer passed in

typedef EVENTSTATUS ( WINAPI * LPFNGETDEFAULTPUBLISHEDAM)(
         TCHAR * LogicalNameOutBuffer,     // [out] buffer for the result (logical name)
         TCHAR * ServerNameOutBuffer,      // [out] buffer for the result (servername)
         DWORD dSizeNameBuffer);    // [in]  size of buffer passed in

         


EXTERN_C EVENTSTATUS  EXPORT WINAPI SendNAIMessage(                  
                NAIAPPID    uAppID,
                EVENTID     uEventID,
                PNAISTANDARDMIB pNAIStandardMib,
                PTVDSTANDARDMIB pTVDStandardMib,
                PAPPSTANDARDMIB pAppStandardMib
                );
typedef EVENTSTATUS    ( WINAPI * LPFNSENDNAIMESSAGE) (                  
                NAIAPPID    uAppID,
                EVENTID     uEventID,
                PNAISTANDARDMIB pNAIStandardMib,
                PTVDSTANDARDMIB pTVDStandardMib,
                PAPPSTANDARDMIB pAppStandardMib
                );

EXTERN_C EVENTSTATUS EXPORT WINAPI SendNAIEvent(
                NAIAPPID    uAppID,
                EVENTID     uEventID,
                TCHAR *     szSubProcessName        // let app specify a subprocess
                );
typedef EVENTSTATUS ( WINAPI * LPFNSENDNAIEVENT) (
                NAIAPPID    uAppID,
                EVENTID     uEventID,
                TCHAR *     szSubProcessName        // let app specify a subprocess
                );

EXTERN_C EVENTSTATUS  EXPORT WINAPI SendNAIMessageEx(                  
                TCHAR *szAMServer, 
                NAIAPPID    uAppID,
                EVENTID     uEventID,
                PNAISTANDARDMIB pNAIStandardMib,
                PTVDSTANDARDMIB pTVDStandardMib,
                PAPPSTANDARDMIB pAppStandardMib,
                BOOL bDont_Alert,
                BOOL bUseAdvancedSearch
                );
typedef EVENTSTATUS    ( WINAPI * LPFNSENDNAIMESSAGEEX) (                  
                TCHAR *szAMServer, 
                NAIAPPID    uAppID,
                EVENTID     uEventID,
                PNAISTANDARDMIB pNAIStandardMib,
                PTVDSTANDARDMIB pTVDStandardMib,
                PAPPSTANDARDMIB pAppStandardMib,
                BOOL bDont_Alert,
                BOOL bUseAdvancedSearch
                );

EXTERN_C EVENTSTATUS EXPORT WINAPI SendNAIEventEx(
                TCHAR *szAMServer, 
                NAIAPPID    uAppID,
                EVENTID     uEventID,
                TCHAR *     szSubProcessName ,       // let app specify a subprocess
                BOOL bDont_Alert,
                BOOL bUseAdvancedSearch
                );
typedef EVENTSTATUS ( WINAPI * LPFNSENDNAIEVENTEX) (
                TCHAR *szAMServer, 
                NAIAPPID    uAppID,
                EVENTID     uEventID,
                TCHAR *     szSubProcessName,       // let app specify a subprocess
                BOOL bDont_Alert,
                BOOL bUseAdvancedSearch
                );


EXTERN_C EVENTSTATUS EXPORT WINAPI PopulateEventStructures(
                NAIAPPID    uAppID,
                TCHAR *     szSubProcessName,        // let app specify a subprocess
                PNAISTANDARDMIB pNAIStandardMib,
                PTVDSTANDARDMIB pTVDStandardMib,
                PAPPSTANDARDMIB pAppStandardMib
                );
typedef EVENTSTATUS ( WINAPI *LPFNPOPULATEEVENTSTRUCTURES )(
                NAIAPPID    uAppID,
                TCHAR *     szSubProcessName,        // let app specify a subprocess
                PNAISTANDARDMIB pNAIStandardMib,
                PTVDSTANDARDMIB pTVDStandardMib,
                PAPPSTANDARDMIB pAppStandardMib
                );

EXTERN_C EVENTSTATUS   WINAPI   EXPORT  ConfigureEventMessages( HWND hInst );
typedef EVENTSTATUS        ( WINAPI * LPFNCONFIGUREEVENTMESSAGES) ( HWND hInst );

EXTERN_C EVENTSTATUS   WINAPI   EXPORT  ConfigureEventMessagesEx( TCHAR *szAMServer, HWND hInst ,BOOL bUseAdvancedSearch);
typedef EVENTSTATUS        ( WINAPI * LPFNCONFIGUREEVENTMESSAGESEX) ( TCHAR *szAMServer, HWND hInst,BOOL bUseAdvancedSearch );
   



EXTERN_C BOOL WINAPI EVT_EnableAlternateRegistry( TCHAR * regkeyname,  BOOL bReadFromMIDKEY, BOOL bWriteToMIDKEY  );
typedef BOOL (WINAPI * LPFNEVT_ENABLEALTERNATEREGISTRY)( TCHAR * regkeyname,BOOL bReadFromMIDKEY, BOOL bWriteToMIDKEY);


EXTERN_C EVENTSTATUS  EXPORT WINAPI QueryMessageText ( 
               NAIAPPID    uAppID,
               EVENTID uEventid, 
               PNAISTANDARDMIB pNAIStandardMib,
               PTVDSTANDARDMIB pTVDStandardMib,
               PAPPSTANDARDMIB pAppStandardMib,
               LPTSTR pMessageTextBuffer,
               UINT BufferSize                  // size in BYTES
               );
typedef EVENTSTATUS    ( WINAPI * LPFNQUERYMESSAGETEXT ) ( 
               NAIAPPID    uAppID,
               EVENTID uEventid, 
               PNAISTANDARDMIB pNAIStandardMib,
               PTVDSTANDARDMIB pTVDStandardMib,
               PAPPSTANDARDMIB pAppStandardMib,
               LPTSTR pMessageTextBuffer,
               UINT BufferSize                  // size in BYTES
               );

EXTERN_C EVENTSTATUS  EXPORT WINAPI QueryMessageTextShort ( 
               NAIAPPID    uAppID,
               EVENTID uEventid, 
               PNAISTANDARDMIB pNAIStandardMib,
               PTVDSTANDARDMIB pTVDStandardMib,
               PAPPSTANDARDMIB pAppStandardMib,
               LPTSTR pMessageTextBuffer,
               UINT BufferSize                  // size in BYTES
               );
typedef EVENTSTATUS    ( WINAPI * LPFNQUERYMESSAGETEXTSHORT ) ( 
               NAIAPPID    uAppID,
               EVENTID uEventid, 
               PNAISTANDARDMIB pNAIStandardMib,
               PTVDSTANDARDMIB pTVDStandardMib,
               PAPPSTANDARDMIB pAppStandardMib,
               LPTSTR pMessageTextBuffer,
               UINT BufferSize                  // size in BYTES
               );

// only for event forwarding (apps should not call this direct)

BOOL EXPORT WINAPI UnLoadForwardingCheck( void );

typedef BOOL ( WINAPI * lpfnUnloadForwardingCheck) ( void );

EVENTSTATUS  EXPORT WINAPI ForwardNAIMessage(                  
                NAIAPPID    uAppID,                 // which application sent this
                EVENTID     uEventID,               // which event is this
                PNAISTANDARDMIB pNAIStandardMib,    // contains NAI SNMP required info
                PTVDSTANDARDMIB pTVDStandardMib,    // provides TVD specific info
                PAPPSTANDARDMIB pAppStandardMib     // for future use (can ignore)
                );
       

#endif
typedef EVENTSTATUS    ( WINAPI * LPFNFORWARDNAIMESSAGE) (                  
                NAIAPPID    uAppID,
                EVENTID     uEventID,
                PNAISTANDARDMIB pNAIStandardMib,
                PTVDSTANDARDMIB pTVDStandardMib,
                PAPPSTANDARDMIB pAppStandardMib
                );

EVENTSTATUS  EXPORT WINAPI ForwardNAIMessage_ANSI(   
				LPFNFORWARDNAIMESSAGE pfn,												  
                NAIAPPID    uAppID,                 // which application sent this
                EVENTID     uEventID,               // which event is this
                PNAISTANDARDMIB pNAIStandardMib,    // contains NAI SNMP required info
                PTVDSTANDARDMIB pTVDStandardMib,    // provides TVD specific info
                PAPPSTANDARDMIB pAppStandardMib     // for future use (can ignore)
                );

#ifdef WANT_ANSI

EVENTSTATUS  EXPORT WINAPI ForwardNAIMessage(                  
                NAIAPPID    uAppID,                 // which application sent this
                EVENTID     uEventID,               // which event is this
                PNAISTANDARDMIB pNAIStandardMib,    // contains NAI SNMP required info
                PTVDSTANDARDMIB pTVDStandardMib,    // provides TVD specific info
                PAPPSTANDARDMIB pAppStandardMib     // for future use (can ignore)
                );
       

typedef EVENTSTATUS    ( WINAPI * LPFNFORWARDNAIMESSAGEA) (                  
                NAIAPPID    uAppID,
                EVENTID     uEventID,
                PNAISTANDARDMIB_ANSI pNAIStandardMib,
                PTVDSTANDARDMIB_ANSI pTVDStandardMib,
                PAPPSTANDARDMIB_ANSI pAppStandardMib
                );



#endif

#ifndef WANT_ANSI
typedef enum tagEVENTCONFIGTYPE {
	ECT_AlertManager		=0,
	ECT_CentralAlerting     =1,
	} EVENTCONFIGTYPE;


typedef struct tagEVENTCONFIG
{
	DWORD	version;				// must be 1
	EVENTCONFIGTYPE		alerttype;	// must be ECT_AlertManager or ECT_CentralAlerting			
	TCHAR	servername[MAX_PATH];	// server name for alerting
	TCHAR	logicalname[MAX_PATH];	// logical name for ad alerting
	BOOL	bUseActiveDirectory;	// use active directory if present
	BOOL	bEnableDMI;				// DMI alerting flag
	BOOL	bDisable;				// disable alerting
    BOOL   bDontAllowApplicationChanges;    // allow locking out all UI changes
    TCHAR   DmiID[6];                  // a string value of range "0"-"1024"
} EVENTCONFIG;

EVENTSTATUS EXPORT WINAPI ConfigureEventClient(	EVENTCONFIG * ec);
typedef EVENTSTATUS ( WINAPI *LPFNCONFIGUREEVENTCLIENT)(
	EVENTCONFIG * ec);

EVENTSTATUS EXPORT WINAPI GetEventClientConfig(
	EVENTCONFIG * ec);
typedef EVENTSTATUS ( WINAPI *LPFNGETEVENTCLIENTCONFIG)(
	EVENTCONFIG * ec);
#endif


#endif
