/*************************************************************************************************************************/
#ifndef _EVENT_WRITER_H_INCLUDED_
#define _EVENT_WRITER_H_INCLUDED_

#include <time.h>
#include <wchar.h>

#ifdef __cplusplus
extern "C"  {
#endif

typedef enum ICmaEventErr 
{
	ICMA_OK = 0,
	ICMA_FALSE,
	ICMA_LOWMEMORY,	
	ICMA_EVENT_FILTERED,	
	ICMA_IPC,
	ICMA_INVALID_ARG,
	ICMA_UNKNOWN
}ICmaEventErr;




/* MachineInfo structure used to create MachineInfo element in event xml file , as per schema by the point product 
<MachineInfo>
  <MachineName>scmgateway</MachineName> 
  <AgentGUID>{E4C76CC7-98AC-DD11-87C6-0A089C1B2040}</AgentGUID> 
  <IPAddress>172.16.220.222</IPAddress> 
  <OSName>"YYYYYYYYYYYYYY</OSName> 
  <UserName>root</UserName> 
  <TimeZoneBias>-330</TimeZoneBias> 
  <RawMACAddress>001422212131</RawMACAddress> 
</MachineInfo>
*/

//All defines for the size of MachineInfo member variables , all inclusive the extra '\0'
#define MAX_COMPUTER_NAME 65 
#define MAX_OS_LENGTH   17  
#define MAX_USER_NAME   65 
#define MAX_MACADDR_LEN     17
#define MAX_IPADDR_LEN      40 
#define MAX_AGENT_GUID_SIZE 65 

typedef struct MachineInfo MachineInfo ;
struct MachineInfo
{
	wchar_t 	m_wstrMachineName[MAX_COMPUTER_NAME] ;
	wchar_t 	m_wstrOSName[MAX_OS_LENGTH];
	wchar_t 	m_wstrUserName[MAX_USER_NAME] ;
	char 		m_strRawMACAddress[MAX_MACADDR_LEN];
	char 		m_strIPAddress[MAX_IPADDR_LEN] ;
	char		m_strAgentGUID[MAX_AGENT_GUID_SIZE];
	long 		m_nTimeZoneBias ;
};



typedef ICmaEventErr (*Itf_CheckDiskSpace)(void);
typedef ICmaEventErr (*Itf_IsEventFiltered)(const long lEventId);
typedef ICmaEventErr (*Itf_GetEventFileName)(const wchar_t *, wchar_t ** , unsigned int );
typedef ICmaEventErr (*Itf_GetMachineInfo)(struct MachineInfo *);
typedef ICmaEventErr (*Itf_ProcessSavedEvent)(const wchar_t * , unsigned long , unsigned int  , unsigned short int );
typedef ICmaEventErr (*Itf_ProcessEvent)(const wchar_t * , const wchar_t * ,  unsigned long , unsigned int  , unsigned short int );


/**
  * Check's the disk space to for generating the event's. 
  * We need to check the file system resources.  If the used resources are 75% or greater, then generate an event so that the
  * administrator can do something to free up resources and signal MA to generate the file system full event.  
  * If the used resources are 95% or greater, then stop generating events and return ICMA_LOW_MEMORY in both cases. 
  * If there is disk space less then 75% , the return ICMA_OK 
*/
ICmaEventErr CheckDiskSpace(void) ;

/**
  * Return's ICMA_EVENT_FILTERED if the passed event ID is filtered according to current policies
  * else return's ICMA_FALSE 
*/
ICmaEventErr IsEventFiltered(const long lEventId);

/**
  * GetEventFileName return's the  full path of the uniquely generated event file name , which PP's will save.
  * Params , first product ID , so that it can be used to generate more unique name 
             second pFileName , for which memory will be allocated and unique path will be copied. 
				It will be null terminated string. Caller has the reposinsility of freeing this memory
	     third severity , on which basis we create txml or xml, to distnguis immediate or normal events
  * Return ICMA_OK if all good.
*/
ICmaEventErr GetEventFileName(const wchar_t *pProductId , wchar_t **pFileName , unsigned int iSeverity);


/**
  *Fills up the structure MachineInfo with the required attribute, used by the point product to create MachineInfo element 
  *in event xml as required by schema.
  *Returned value will be structure filled with required values. 
*/
ICmaEventErr GetMachineInfo(struct MachineInfo *pMachineInfo);

/**
  *ProcessSavedEvent , it process the eventFileName which is already on the disk.
  * It will delete the file , if it is filtered event and still user is asking to process this event and will return ICMA_EVENT_FILTERED
  *Return's ICMA_OK on success.
*/
ICmaEventErr ProcessSavedEvent(const wchar_t *pEventPath , unsigned long lEventId , unsigned int iSeverity );

/**
  *ProcessEvent , it creates the event file on the disk and then process it using rawXML passed as per schema
  * It will not take care of checking disk space , event filter , and creating the unique file name , conditions are same event should not be 
  * filtered
  * Return's ICMA_OK on success
*/
ICmaEventErr ProcessEvent(const wchar_t *pRawXMLBuffer , const wchar_t *pProductId , unsigned long lEventId , unsigned int iSeverity);

#ifdef __cplusplus
}
#endif

#endif //ifndef _EVENT_WRITER_H_INCLUDED_
