/*  Copyright (C) 2009 McAfee, Inc.  All rights reserved. */
/***********************************************************************

naistd.h

Copyright (C) 2009 McAfee, Inc.
All Rights Reserved
Confidential

***********************************************************************/

/**********************************************************************/
#ifndef NAISTD_H
#define NAISTD_H

/**********************************************************************/

#if defined(_WIN32)
    #define NAI_EXPORT __declspec(dllexport)
    #define NAI_IMPORT __declspec(dllimport)
#else
    #define NAI_EXPORT
    #define NAI_IMPORT
#endif

#if defined(_MSDOS) && defined(_WINDOWS)
    #define WIN16_EXPORT __export
#else
    #define WIN16_EXPORT
#endif

#if defined(_WIN32)
    #define NAI_API __stdcall
#elif defined(_WINDOWS)
    #define NAI_API __pascal
#else
    #define NAI_API
#endif

#if !defined(IN)
    #define IN
#endif

#if !defined(OUT)
    #define OUT
#endif

#if !defined(OPTIONAL)
    #define OPTIONAL
#endif

#ifndef TRUE
    #define TRUE 1
#endif

#ifndef FALSE
    #define FALSE 0
#endif

typedef signed char SCHAR;


#if !defined(_WINNT_)
    #define VOID void
    typedef char CHAR;
#endif

#if !defined(_WINDEF_)
    typedef int INT;
#endif

#if !defined(_WINNT_)
    #if !defined(_COMPOBJ_H_)
        typedef short SHORT;
    #endif
    #if !defined(_INC_WINDOWS)
        typedef long LONG;
    #endif
#endif

#if !defined(_WINDEF_)
    typedef unsigned char UCHAR;
    #if !defined(_INC_WINDOWS)
        typedef unsigned int UINT;
    #endif
    #if !defined(_COMPOBJ_H_)
        typedef unsigned short USHORT;
        typedef unsigned long ULONG;
    #endif
#endif

typedef signed char INT8;
typedef short INT16;
#if !defined(_BASETSD_H_) 
    typedef long INT32;
    #if _MSC_VER >= 900
        typedef __int64 INT64;
    #endif
#endif

typedef unsigned char UINT8;
typedef unsigned short UINT16;
#if !defined(_BASETSD_H_) 
    typedef unsigned long UINT32;
    #if _MSC_VER >= 900
        typedef unsigned __int64 UINT64;
    #endif
#endif

#if defined(_WIN64)
    #if !defined(_BASETSD_H_)
        typedef INT64 INT_PTR;
        typedef UINT64 UINT_PTR;
    #endif
    typedef INT64 INT_FPTR;
    typedef UINT64 UINT_FPTR;
    typedef INT64 INT_NPTR;
    typedef UINT64 UINT_NPTR;
#elif defined(_WIN32)
    #if !defined(_BASETSD_H_)
        typedef INT32 INT_PTR;
        typedef UINT32 UINT_PTR;
    #endif
    typedef INT32 INT_NPTR;
    typedef UINT32 UINT_NPTR;
    typedef INT32 INT_FPTR;
    typedef UINT32 UINT_FPTR;
#elif defined(_MSDOS)
    #if defined(M_I86TM) || defined(M_I86SM) || defined(M_I86MM)
        typedef INT16 INT_PTR;
        typedef UINT16 UINT_PTR;
    #else
        typedef INT32 INT_PTR;
        typedef UINT32 UINT_PTR;
    #endif
    typedef INT16 INT_NPTR;
    typedef UINT16 UINT_NPTR;
    typedef INT32 INT_FPTR;
    typedef UINT32 UINT_FPTR;
#else
    typedef INT32 INT_PTR;
    typedef UINT32 UINT_PTR;
    typedef INT32 INT_NPTR;
    typedef UINT32 UINT_NPTR;
    typedef INT32 INT_FPTR;
    typedef UINT32 UINT_FPTR;
#endif

#if defined(_MSDOS) && (defined(M_I86TM) || defined(M_I86SM) || defined(M_I86MM))
    #define FP_SEG(fp) (*((unsigned __far *)&(fp)+1))
    #define FP_OFF(fp) (*((unsigned FAR *)&(fp)))    
    #define FP_CLIP(fp) ((void NEAR *)FP_OFF(fp))
#else
    #define FP_CLIP(fp) (fp)
#endif


#if !defined(NEAR)
    #if defined(_MSDOS)
        #define NEAR __near
    #else
        #define NEAR
    #endif
#endif

#if !defined(FAR)
    #if defined(_MSDOS)
        #define FAR __far
    #else
        #define FAR
    #endif
#endif

#if !defined(HUGE)
    #if defined(_MSDOS)
        #define HUGE __huge
    #else
        #define HUGE
    #endif
#endif


/* naires_t is the same as an SCODE in Win16 or a HRESULT in Win32 */
typedef long naires_t;
typedef naires_t NAIRES;

#define NAIRES_CODE(nr) ((nr) & 0xFFFF)
#define NAIRES_FACILITY(nr) (((nr) >> 16) & 0x1fff)
#define NAIRES_SEVERITY(nr) (((nr) >> 31) & 0x1)
#define MAKE_NAIRES(sev,fac,code) \
    ((naires_t) (((unsigned long)(sev)<<31) | ((unsigned long)(fac)<<16) | ((unsigned long)(code))) )

#if !defined(SUCCEEDED)

#define SEVERITY_SUCCESS    0
#define SEVERITY_ERROR      1

#define FACILITY_NULL           0
#define FACILITY_RPC            1
#define FACILITY_DISPATCH       2
#define FACILITY_STORAGE        3
#define FACILITY_ITF            4
#define FACILITY_WIN32          7
#define FACILITY_WINDOWS        8
#define FACILITY_SSPI           9
#define FACILITY_CONTROL        10
#define FACILITY_CERT           11
#define FACILITY_INTERNET       12
#define FACILITY_MEDIASERVER    13
#define FACILITY_MSMQ           14
#define FACILITY_SETUPAPI       15

#define SUCCEEDED(Status) ((naires_t)(Status) >= 0)
#define FAILED(Status) ((naires_t)(Status)<0)

#define S_OK            0L
#define S_FALSE         MAKE_NAIRES(SEVERITY_SUCCESS,   FACILITY_NULL,  1)

#define E_UNEXPECTED    MAKE_NAIRES(SEVERITY_ERROR,     FACILITY_NULL,  0xffff) 
#define E_NOTIMPL       MAKE_NAIRES(SEVERITY_ERROR,     FACILITY_NULL,  1)
#define E_OUTOFMEMORY   MAKE_NAIRES(SEVERITY_ERROR,     FACILITY_NULL,  2)
#define E_INVALIDARG    MAKE_NAIRES(SEVERITY_ERROR,     FACILITY_NULL,  3)
#define E_NOINTERFACE   MAKE_NAIRES(SEVERITY_ERROR,     FACILITY_NULL,  4)
#define E_POINTER       MAKE_NAIRES(SEVERITY_ERROR,     FACILITY_NULL,  5)
#define E_HANDLE        MAKE_NAIRES(SEVERITY_ERROR,     FACILITY_NULL,  6)
#define E_ABORT         MAKE_NAIRES(SEVERITY_ERROR,     FACILITY_NULL,  7)
#define E_FAIL          MAKE_NAIRES(SEVERITY_ERROR,     FACILITY_NULL,  8)
#define E_ACCESSDENIED  MAKE_NAIRES(SEVERITY_ERROR,     FACILITY_NULL,  9)

#endif

#define E_NULL          MAKE_NAIRES(SEVERITY_ERROR,     FACILITY_NULL,  10)


#define FACILITY_NAI   69

#ifndef _COUNTOF_
#define _COUNTOF_
#define countof(x) ((sizeof(x))/(sizeof(x[0])))
#endif

#define field_offset(type, field)    ((size_t)(INT_PTR)&(((type *)0)->field))
#define containing_record(address, type, field) ((type *)( \
                                                  (PCHAR)(address) - \
                                                  (UINT_PTR)(&((type *)0)->field)))
                                                  
#if defined(_MSDOS) && !defined(_WINDOWS)
#define IF_MSDOS(x) x
#else
#define IF_MSDOS(x)
#endif

#if defined(_MSDOS) && defined(_WINDOWS)
#define IF_WIN16(x) x
#else
#define IF_WIN16(x)
#endif

#if defined(_WIN32)
#define IF_WIN32(x) x
#else
#define IF_WIN32(x)
#endif
                                                  
#if defined (_WIN32)
#define INITIALIZE_CRITICAL_SECTION(cs) InitializeCriticalSection(cs)
#define DELETE_CRITICAL_SECTION(cs) DeleteCriticalSection(cs)
#define ENTER_CRITICAL_SECTION(cs) EnterCriticalSection(cs)
#define LEAVE_CRITICAL_SECTION(cs) LeaveCriticalSection(cs)
#else
#define INITIALIZE_CRITICAL_SECTION(cs)
#define DELETE_CRITICAL_SECTION(cs)
#define ENTER_CRITICAL_SECTION(cs) 
#define LEAVE_CRITICAL_SECTION(cs)
#endif

#if defined(_MSDOS) && defined(_WINDOWS)
#define MAKEWORD(a, b) ((unsigned int) (((unsigned char) (a)) | ((unsigned int) ((unsigned char) (b))) << 8))
#define MAXDWORD 0xffff
#define LPCTSTR LPCSTR
#define TCHAR CHAR
#define APIENTRY PASCAL
#define PULONG unsigned long*


#ifdef _WINDLL
size_t __cdecl strftime(char *, size_t, const char *, const struct tm *);
#endif
#endif


/**********************************************************************/
/***** naStd stuff */

/**********************************************************************/
typedef char  naCHAR, *naPCHAR, **naPPCHAR;
typedef unsigned short naWCHAR, *naPWCHAR, **naPPWCHAR;
typedef short naSHORT, *naPSHORT, **naPPSHORT;
typedef int   naINT, *naPINT, **naPPINT;
typedef long  naLONG, *naPLONG, **naPPLONG;
typedef unsigned char  naUCHAR, *naPUCHAR, **naPPUCHAR;
typedef unsigned short naUSHORT, *naPUSHORT, **naPPUSHORT;
typedef unsigned int   naUINT, *naPUINT, **naPPUINT;
typedef unsigned long  naULONG, *naPULONG, **naPPULONG;
typedef signed char naSCHAR, *naPSCHAR, **naPPSCHAR;

typedef signed char naINT8, *naPINT8, **naPPINT8;
typedef short naINT16, *naPINT16, **naPPINT16;
typedef long  naINT32, *naPINT32, **naPPINT32;
typedef unsigned char  naUINT8, *naPUINT8, **naPPUINT8;
typedef unsigned short naUINT16, *naPUINT16, **naPPUINT16;
typedef unsigned long  naUINT32, *naPUINT32, **naPPUINT32;

typedef void * naHANDLE, *naPHANDLE,**naPPHANDLE;

typedef long naRES, *naPRES, **naPPRES;

typedef GUID naGUID, *naPGUID, **naPPGUID;

#ifdef __cplusplus
#define naNULL    0
#else
#define naNULL    ((void *)0)
#endif

#define naINVALID_HANDLE_VALUE (naPVOID)-1


#define naRES_CODE(nr) ((nr) & 0xFFFF)
#define naRES_FACILITY(nr) (((nr) >> 16) & 0x1fff)
#define naRES_SEVERITY(nr) (((nr) >> 31) & 0x1)
#define naMAKE_RES(sev,fac,code) \
    ((naRES) (((unsigned long)(sev)<<31) | ((unsigned long)(fac)<<16) | ((unsigned long)(code))) )
#define naSUCCEEDED(Status) ((naRES)(Status) >= 0)
#define naFAILED(Status) ((naRES)(Status)<0)
#define naPENDING(res) \
    (res == naE_PENDING) 

#define naMAKE_MSG(fac,code) \
    ((naRES)(((naUINT)(fac)<<8) | ((naUINT)(code))))


#define naE_PENDING MAKE_NAIRES(SEVERITY_ERROR,     FACILITY_NULL,  999)


#ifndef NDEBUG
    #if _MSC_VER >= 1400      // assumes win32 also
    #define naASSERT(exp) assert(exp)
    #define naVERIFY(exp) assert(exp)
    #else
    #define naASSERT(exp) ((void)( (exp) || (_assert(#exp, __FILE__, __LINE__), 0) ))
    #define naVERIFY(exp) ((void)( (exp) || (_assert(#exp, __FILE__, __LINE__), 0) ))
    #endif
#else
	#define naASSERT(x) {}
	#define naVERIFY(x) {(x)}
#endif

#define naABORT() abort()


enum
{
    naFACILITY_NULL           = 0,
    naFACILITY_RPC            = 1,
    naFACILITY_DISPATCH       = 2,
    naFACILITY_STORAGE        = 3,
    naFACILITY_ITF            = 4,
    naFACILITY_WIN32          = 7,
    naFACILITY_WINDOWS        = 8,
    naFACILITY_SSPI           = 9,
    naFACILITY_CONTROL        = 10,
    naFACILITY_CERT           = 11,
    naFACILITY_INTERNET       = 12,
    naFACILITY_MEDIASERVER    = 13,
    naFACILITY_MSMQ           = 14,
    naFACILITY_SETUPAPI       = 15,
    naFACILITY_CONSOLE        = 16,
    naFACILITY_HTTP           = 17,
    naFACILITY_SPIPE          = 18,
    naFACILITY_NAI            = 20
};

#define    naS_OK            0L
#define    naS_FALSE         naMAKE_RES(SEVERITY_SUCCESS,   naFACILITY_NULL,  1)
#define    naS_IDLE          naMAKE_RES(SEVERITY_SUCCESS,   naFACILITY_NAI,   1)
#define    naE_UNEXPECTED    naMAKE_RES(SEVERITY_ERROR,     naFACILITY_NULL,  0xffff)
#define    naE_NOTIMPL       naMAKE_RES(SEVERITY_ERROR,     naFACILITY_NULL,  1)
#define    naE_OUTOFMEMORY   naMAKE_RES(SEVERITY_ERROR,     naFACILITY_NULL,  2)
#define    naE_INVALIDARG    naMAKE_RES(SEVERITY_ERROR,     naFACILITY_NULL,  3)
#define    naE_NOINTERFACE   naMAKE_RES(SEVERITY_ERROR,     naFACILITY_NULL,  4)
#define    naE_POINTER       naMAKE_RES(SEVERITY_ERROR,     naFACILITY_NULL,  5)
#define    naE_HANDLE        naMAKE_RES(SEVERITY_ERROR,     naFACILITY_NULL,  6)
#define    naE_ABORT         naMAKE_RES(SEVERITY_ERROR,     naFACILITY_NULL,  7)
#define    naE_FAIL          naMAKE_RES(SEVERITY_ERROR,     naFACILITY_NULL,  8)
#define    naE_ACCESSDENIED  naMAKE_RES(SEVERITY_ERROR,     naFACILITY_NULL,  9)
#define    naE_NOTFOUND      naMAKE_RES(SEVERITY_ERROR,     naFACILITY_NULL,  10)
#define    naE_MOREDATA      naMAKE_RES(SEVERITY_ERROR,     naFACILITY_NULL,  11)
#define    naE_OPEN          naMAKE_RES(SEVERITY_ERROR,     naFACILITY_NULL,  12)
#define    naE_DELETE        naMAKE_RES(SEVERITY_ERROR,     naFACILITY_NULL,  13)
#define    naE_READ          naMAKE_RES(SEVERITY_ERROR,     naFACILITY_NULL,  14)
#define    naE_WRITE         naMAKE_RES(SEVERITY_ERROR,     naFACILITY_NULL,  15)
#define    naE_NOMOREITEMS   naMAKE_RES(SEVERITY_ERROR,     naFACILITY_NULL,  16)
#define    naE_PGP_ERR		 naMAKE_RES(SEVERITY_ERROR,     naFACILITY_NULL,  17)

#define naCOUNTOF(x) ((sizeof(x))/(sizeof(x[0])))
#define naFIELD_OFFSET(type, field)    ((size_t)(INT_PTR)&(((type *)0)->field))
#define naCONTAINING_RECORD(address, type, field) ((type *)( \
                                                  (PCHAR)(address) - \
                                                  (UINT_PTR)(&((type *)0)->field)))


typedef int naBOOL, *naPBOOL, **naPPBOOL;

typedef char *naPSTR, **naPPSTR;
typedef unsigned short *naPWSTR, **naPPWSTR;
typedef const char *naPCSTR, **naPPCSTR;
typedef const unsigned short *naPCWSTR, **naPPCWSTR;

#if defined(_UNICODE)
    typedef unsigned short naTCHAR, *naPTCHAR, **naPPTCHAR;
    typedef unsigned short *naPTSTR, **naPPTSTR;
    typedef const unsigned short *naPCTSTR, **naPPCTSTR;
    #define naT(x) L x

#elif defined(_MBCS)
    typedef char naTCHAR, *naPTCHAR, **naPPTCHAR;
    typedef char *naPTSTR, **naPPTSTR;
    typedef const char *naPCTSTR, **naPPCTSTR;
    #define naT(x) x
#else
    typedef char naTCHAR, *naPTCHAR, **naPPTCHAR;
    typedef char *naPTSTR, **naPPTSTR;
    typedef const char *naPCTSTR, **naPPCTSTR;
    #define naT(x) x
#endif

typedef enum naBOOL_STRICT
{
    naFALSE = 0,
    naTRUE  = 1
} naBOOL_STRICT;

#define naVOID void
typedef void *naPVOID, **naPPVOID;

#ifdef __cplusplus
    #define naEXTERN_C extern "C"
#else
    #define naEXTERN_C
#endif

#if defined(_WIN32)
    #define naPRE    
    #define naPOST   __stdcall
    #define naPREV   
    #define naPOSTV  __cdecl
    #define naEXPPRE    __declspec(dllexport)
    #define naEXPPOST   __stdcall
    #define naEXPPREV   __declspec(dllexport)
    #define naEXPPOSTV  __cdecl
    #define naIMPPRE    __declspec(dllimport)
    #define naIMPPOST   __stdcall
    #define naIMPPREV   __declspec(dllimport)
    #define naIMPPOSTV  __cdecl
#elif defined(_WINDOWS)
    #define naPRE    
    #define naPOST   __pascal
    #define naPREV   
    #define naPOSTV  __cdecl
    #define naPRE    
    #define naEXPPRE    
    #define naEXPPOST   __pascal __export
    #define naEXPPREV   
    #define naEXPPOSTV  __cdecl __export
    #define naIMPPRE    
    #define naIMPPOST   __pascal
    #define naIMPPREV   
    #define naIMPPOSTV  __cdecl
#else
    #define naPRE    
    #define naPOST   
    #define naPREV   
    #define naPOSTV  
    #define naPRE    
    #define naEXPPRE
    #define naEXPPOST
    #define naEXPPREV
    #define naEXPPOSTV
    #define naIMPPRE
    #define naIMPPOST
    #define naIMPPREV
    #define naIMPPOSTV
#endif

#define naAPIPTR(func) naPRE naRES (naPOST* func)
#define naAPIPTR_(type, func) naPRE type (naPOST* func)
#define naAPIVPTR(func) naPREV naRES (naPOSTV *func)
#define naAPIVPTR_(type, func) naPREV type (naPOSTV  *func)
#define naCALLBACK_API   naRES naPOST 

#ifndef NDEBUG
    #define naDEBUG_ONLY(x) x
#else
    #define naDEBUG_ONLY(x)
#endif

#define MAX_STRING_GUID_SIZE        64

#if defined (_WIN32)
#define INITIALIZE_CRITICAL_SECTION(cs) InitializeCriticalSection(cs)
#define DELETE_CRITICAL_SECTION(cs) DeleteCriticalSection(cs)
#define ENTER_CRITICAL_SECTION(cs) EnterCriticalSection(cs)
#define LEAVE_CRITICAL_SECTION(cs) LeaveCriticalSection(cs)
#else
#define INITIALIZE_CRITICAL_SECTION(cs)
#define DELETE_CRITICAL_SECTION(cs)
#define ENTER_CRITICAL_SECTION(cs) 
#define LEAVE_CRITICAL_SECTION(cs)
#endif

#define sizeoftstr(str) sizeof(str)/sizeof(naTCHAR)

/**********************************************************************/  
/*  used for memory leak testing..
/**********************************************************************/

#undef MEMORY_LEAK_TEST           


#ifdef MEMORY_LEAK_TEST    
#ifdef _DEBUG
    #define malloc(x) \
        naimcomn_malloc_dbg(x,__FILE__,__LINE__)
    #define calloc(x,y) \
        naimcomn_calloc_dbg(x,y,__FILE__,__LINE__)
    #define free(x) \
        naimcomn_free_dbg(x)
#else
    #define malloc(x) \
        naimcomn_malloc(x)
    #define calloc(x,y) \
        naimcomn_calloc(x,y)
    #define free(x) \
        naimcomn_free(x)
#endif    
#endif

/**********************************************************************/  
/*  for tracing output messages
/**********************************************************************/

#ifndef TraceMessage
    #if defined(_DEBUG) || defined(_PRERELEASE)
        #define TraceMessage(x) OutputDebugString(x)
    #else
        #define TraceMessage(x) ((VOID)0)
    #endif
#endif

/**********************************************************************/  
/*  string manipulation
/**********************************************************************/

#define TSTR_SIZEOF(s)	(sizeof(s) / sizeof(*(s)) )
#define STR_SIZEOF(s)	(sizeof(s) / sizeof(*(s)) )
#define WSTR_SIZEOF(s)	(sizeof(s) / sizeof(*(s)) )
#ifdef _MBCS
#define STR_GET_LAST_CHARACTER(s)	(s == NULL || _tcslen(s) == 0 ? 0 : *(_tcsdec(s, s + strlen(s))))
#else
#define STR_GET_LAST_CHARACTER(s)	(s == NULL || _tcslen(s) == 0 ? 0 : *(s + (_tcslen(s)-1)))
#endif
#define MBSTR_GET_LAST_CHARACTER(s)	(s == NULL || strlen(s) == 0 ? 0 : *(_mbsdec((unsigned char *)s, (unsigned char *)(s + strlen(s)))))

/**********************************************************************/  
/*  Compiler macro for displaying clickable CODE REVIEW and TODO messages
/*  example:
/*  #pragma CODEREVIEW("Code does not properly check for failure of realloc")
/*  #pragma TODO("add code here to cleanup this object")
/**********************************************************************/

#define EXPANDEDLINENUM(x)  #x
#define EXPANDEDLINENUM2(x) EXPANDEDLINENUM(x)
#define CODEREVIEW(x) message (__FILE__ "(" EXPANDEDLINENUM2(__LINE__) ") : -CODE REVIEW- " #x)    

//A TODO macro already is defined in CMA.
//#define TODO(x) message (__FILE__ "(" EXPANDEDLINENUM2(__LINE__) ") : TODO: " #x)    

//#define MAKE_PRIORITY_LEVEL_CONFIGURABLE

/**********************************************************************/
#endif /* NAISTD_H */
/**********************************************************************/
