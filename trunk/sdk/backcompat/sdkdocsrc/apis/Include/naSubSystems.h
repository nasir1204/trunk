/*******************************************************************************************************
 * $Workfile: naSubSystems.h $ : File
 * (C)  Copyright (C) 2009 McAfee, Inc.  All rights reserved. 
 * All rights reserved.
 *
 * 
 * Description: defines anything common to subsystems 
 *
 * Author: Eric Hamilton
 * E-Mail: <Eric_Hamilton@nai.com>
 *
 * $Log: naSubSystems.h,v $
 * Revision 1.1  2004/10/26 23:23:19  GWeber
 * Initial checkin of CML, CMA, MSE, and NWA code
 *
 * Revision 1.1.1.1.4.1.2.2  2004/03/10 01:29:34  hsingh
 * Merged with CMA TC branch (second merge)
 *
 * Revision 1.1.1.1.4.1.4.3  2003/12/12 00:34:00  mthompso
 * Changed TC subsystem name to match manifest file.
 *
 * Revision 1.1.1.1.4.1.4.2  2003/12/10 02:53:20  hsingh
 * changed TC subsystem description
 *
 * Revision 1.1.1.1.4.1.4.1  2003/12/08 22:45:00  mthompso
 * Added TC subsystem.
 *
 * Revision 1.1.1.1.4.1  2003/09/12 02:08:22  gweber
 * Merged in EPO 3.0 SP1 Release.
 * MSE 	2.1.1.150
 * CML 	3.1.1.159
 * CMA 	3.1.1.184
 * SIM 	1.1.1.147
 * EPO 	3.0.1.150
 *
 * 
 * 10    6/02/02 2:02p Ehamilton
 * 
 * 9     5/26/02 2:09p Ehamilton
 * updated for installer
 * 
 * 8     5/24/02 6:58p Jhuang
 * 
 * 7     5/17/02 10:33a Ehamilton
 * 
 * 6     5/17/02 9:47a Ehamilton
 * updated logging component
 * 
 * 5     5/06/02 6:05p Skadam
 * Added Subsystem defination for User Space Controler
 * 
 * 4     5/06/02 11:04a Ehamilton
 * updated or management component
 * 
 * 3     4/24/02 5:16p Jhuang
 * 
 * 2     4/23/02 3:36p Ehamilton
 * 
 * 1     4/17/02 5:06p Ehamilton
 * first prototype of common framework
 * 
 ******************************************************************************************************/

#if !defined(AFX_NASUBSYSTEMS_H__95CC287F_1E5F_4165_9F30_05DB9961243F__INCLUDED_)
#define AFX_NASUBSYSTEMS_H__95CC287F_1E5F_4165_9F30_05DB9961243F__INCLUDED_

#define NASUBSYS_LOGGING				_T("Logging")				
#define NASUBSYS_MANAGEMENT				_T("Management")			
#define NASUBSYS_UPDATE					_T("Updater")				
#define NASUBSYS_SCRIPT					_T("Script")
#define NASUBSYS_INTERNET_MGR			_T("Internet Manager")
#define NASUBSYS_TEST_HARNESS			_T("Test Harness")
#define NASUBSYS_SCHEDULE				_T("Scheduler")
#define NASUBSYS_POLICY_HARNESS			_T("PolicyHarness")
#define NASUBSYS_USER_SPACE_CONTROLER	_T("User Space Controller")
#define NASUBSYS_AGENT					_T("Agent")
#define NASUBSYS_LISTEN_SERVER			_T("Listen Server")
#define NASUBSYS_TC						_T("Trusted Connection")


// for use in logger
#define NALIB_SECURE_FRAMEWORK_FACTORY	_T("Framework Factory")
#define NALIB_SECURE_FRAMEWORK_SERVICE	_T("Framework Service")
#define NALIB_PRODUCT_MANAGER			_T("Product Manager")
#define NALIB_POLICY_MANAGER			_T("Policy Manager")
#define NALIB_SCHEDULER					_T("Scheduler Library")
#define NALIB_FRAMEWORK_PLUGIN			_T("Framework Plugin")


typedef enum tag_FRAMEWORK_CHECK
{
	eFRAMEWORK_CHECK_UPTODATE = 0, //check all the subsystems and managed products are upto date
	eFRAMEWORK_CHECK_HEALTH, //Health check for all the subsystem. Are you running fine

} FRAMEWORK_CHECK;

typedef enum tag_FRAMEWORK_STATE
{
	eFRAMEWORK_STATE_OK = 0, //Everything is fine, be happy
	eFRAMEWORK_STATE_UPTODATE = 0, //Everything is fine, be happy
	eFRAMEWORK_STATE_HEALTHY = 0, //Everything is fine, be happy
	eFRAMEWORK_STATE_FAIL,    //Something is wrong I know it
	eFRAMEWORK_STATE_UNKNOWN, //Unknown or Unable to find required information
	eFRAMEWORK_STATE_INTERMEDIATE, //In between of task excution/Update/or policy collection and enforcement
	eFRAMEWORK_STATE_WAITING_REBOOT //Update/deployment is waiting for reboot

} FRAMEWORK_STATE;


#endif // #if !defined(AFX_NASUBSYSTEMS_H__95CC287F_1E5F_4165_9F30_05DB9961243F__INCLUDED_)
