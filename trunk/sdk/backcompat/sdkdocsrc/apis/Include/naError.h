/*******************************************************************************************************
 * $Workfile: naError.h $ : File
 *  Copyright (C) 2009 McAfee, Inc.  All rights reserved. 
 * All rights reserved.
 *
 * 
 * Description: Defines all common return values
 *
 * Author: Eric Hamilton
 * E-Mail: <Eric_Hamilton@nai.com>
 *
 * $Log: naError.h,v $
 * Revision 1.4  2007/02/13 21:44:28  WCoburn
 * Merge of CMA355_BRANCH into trunk.
 *
 * Revision 1.1.2.2  2005/07/19 23:09:15  syoshiha
 * CMA 350 Patch 4 merge
 *
 * Revision 1.1  2004/10/26 23:23:28  GWeber
 * Initial checkin of CML, CMA, MSE, and NWA code
 * Revision 1.1.1.1.4.1.2.2.4.2  2005/01/25 20:08:55  jhuang
 * PEERED BY: Merritt
 * DEFECT:BZ214251
 * CHANGE:
 *
 * Revision 1.1.1.1.4.1.2.2.4.1  2005/01/12 17:41:36  jhuang
 * PEERED BY:
 * DEFECT: BZ214566
 * CHANGE:
 *
 * Revision 1.1.1.1.4.1.2.2  2004/06/15 18:21:53  jhuang
 * Fixed bug 203080
 *
 * Revision 1.1.1.1.4.1.2.1  2004/02/20 15:49:52  efarrenk
 * Initial checkin for the Selective replication feature
 *
 * Revision 1.1.1.1.4.1  2003/09/12 02:08:25  gweber
 * Merged in EPO 3.0 SP1 Release.
 * MSE 	2.1.1.150
 * CML 	3.1.1.159
 * CMA 	3.1.1.184
 * SIM 	1.1.1.147
 * EPO 	3.0.1.150
 *
 * 
 * 26    7/01/02 1:59p Ehamilton
 * 
 * 25    6/25/02 12:11p Sdeshmuk
 * 
 * 24    6/20/02 2:41p Hsingh
 * 
 * 23    6/20/02 2:29p Hsingh
 * error code for update not starting
 * 
 * 22    6/20/02 1:57p Ehamilton
 * enabled logging in frameworkharness
 * 
 * 21    6/17/02 3:41p Hsingh
 * added update reboot error code
 * 
 * 19    6/08/02 3:51p Ehamilton
 * added cleanup.exe to build, package, and install
 * 
 * 18    6/08/02 10:36a Ehamilton
 * added move/delete files on reboot
 * 
 * 17    6/06/02 3:44p Ehamilton
 * -removed naimcomn32
 * -updated installer
 * -added Update sub system to package
 * 
 * 13    5/29/02 11:48a Ehamilton
 * first draft of installer with sfx
 * 
 * 10    5/26/02 2:09p Ehamilton
 * updated for installer
 * 
 * 9     5/20/02 11:13a Hsingh
 * added error code for update subsys
 * 
 * 8     5/20/02 3:32a Ehamilton
 * support for loading and unloading subsystems based on dependencies
 * 
 * 4     5/14/02 3:27p Ehamilton
 * updated to handle load and unload of framework
 * 
 * 3     5/06/02 11:04a Ehamilton
 * updated or management component
 * 
 * 1     4/17/02 5:06p Ehamilton
 * first prototype of common framework
 * 
 ******************************************************************************************************/

#if !defined(AFX_ERRORCODES_H__95CC287F_1E5F_4165_9F30_05DB9961243F__INCLUDED_)
#define AFX_ERRORCODES_H__95CC287F_1E5F_4165_9F30_05DB9961243F__INCLUDED_


#define FACILITY_SPIPE						105


// General (-1)
#define ERROR_FAILURE						-1		// unknown failure occurred
#define ERROR_UNKNOWN_EXCEPTION_THROWN		-2		// exception thrown, unknown reason
#define ERROR_INSUFFICIENT_MEMORY			-3		// failed to allocate required memory
#define ERROR_INTIALIZATION_REQUIRED		-4		// the object requires initialization before use
#define ERROR_NAI_ACCESS_DENIED				-5		// access to requested resource is denied

// Product Library
#define ERROR_NAI_METHOD_NOT_IMPLEMENTED	-10		// the method has not been implemented, or is not supported
#define ERROR_NAI_OBJECT_NOT_INITIALIZED	-11		// the object has not been properly initialized
#define ERROR_NAI_INITIALIZE_REQ_LIB		-12		// failed to initialize a required library
#define ERROR_NAI_PARAMETER_INVALID			-13		// the passed parameter appears to be invalid
#define ERROR_NAI_INSUFFICIENT_BUFFER		-14		// the allocated buffer was insufficient
#define ERROR_NAI_LIBRARY_NOT_FOUND			-15		// the requested library could not be found
#define ERROR_NAI_LIBRARY_NOT_VALID			-16		// the requested library was not valid (failed DllMain)
#define ERROR_NAI_LIBRARY_WIN32_INVALID		-17		// a required system library does not contain required API
#define ERROR_NAI_CREATE_EVENT				-18		// could not open/create a required event
#define ERROR_NAI_CREATE_MUTEX				-19		// could not open/create a required mutex
#define ERROR_NAI_THREAD_TERMINATE			-20		// thread terminated unexpectedly
#define ERROR_NAI_SERVICE_START				-21		// the service could not be started
#define ERROR_NAI_SERVICE_STOP				-22		// the service could not be stopped
#define ERROR_NAI_CREATE_PROCESS			-23		// failed to create a required process
#define ERROR_NAI_OBJECT_INVALID			-24		// the object is not valid, or is not being used properly
#define ERROR_NAI_IMPERSONATE_FAILED		-25		// failed to impersonate the requested user

// COM
#define ERROR_NAI_COM_FAILED_INIT_OBJECT	-30		// failed to init a required COM object
#define ERROR_NAI_COM_FAILED_INIT			-31		// failed to init COM 
#define ERROR_NAI_COM_EXCEPTION_THROWN		-32		// COM threw error
#define ERROR_NAI_COM_IFACE_UNSUPPORTED		-33		// the object does not support the required interface

// File IO
#define ERROR_NAI_FILE_DELETE_FAIL			-40		// the file specified could not be removed
#define ERROR_NAI_FILE_CREATE_FAIL			-41		// the file specified could not be created
#define ERROR_NAI_FILE_INVALID_ENCODING		-42		// The UTF encoding is not supported
#define ERROR_NAI_FILE_WRITE_FAIL			-43		// Unable to write the new file
#define ERROR_NAI_FILE_INVALID				-44		// the file is missing or corrupt
#define ERROR_NAI_FILE_COPY_FAIL			-45		// unable to copy a file
#define ERROR_NAI_FILE_CREATE_DIR			-46		// unable to create directory
#define ERROR_NAI_FILE_DELETE_DIR			-47		// unable to Delete directory
#define ERROR_NAI_FILE_OPEN					-48		// unable to open a file
#define ERROR_NAI_FILE_EXISTS				-49		// the file already exists
#define ERROR_NAI_FILE_SEEK					-50		// the location being "seek"ed is invalid
#define ERROR_NAI_FILE_READ					-51		// the file read failed
#define ERROR_NAI_FILE_SIZE_VERIFICATION	-52		// file size verification failed
#define ERROR_NAI_FILE_HASH_VERIFICATION	-53		// file hash verification failed
#define ERROR_NAI_FILE_CREATE_TEMP_DIR		-54		// unable to create temp directory
#define ERROR_NAI_FILE_TOO_BIG				-55		// The file to be written to disk is too big to be processed further (This is not same as out of disk space)

// Registry
#define ERROR_NAI_REG_CREATE_KEY			-60		// failed to create the required registry key
#define ERROR_NAI_REG_DELETE_KEY			-61		// failed to remove the required registry key
#define ERROR_NAI_REG_OPEN_KEY				-62		// failed to open the required registry key
#define ERROR_NAI_REG_OPEN_KEY_ALL			-63		// failed to open the required registry key with all access
#define ERROR_NAI_REG_VALUE_INVALID			-64		// failed to read a required value from the registry
#define ERROR_NAI_REG_SET_VALUE				-65		// failed to set a required value

// Localization
#define ERROR_NAI_RESOURCE_NOT_FOUND		-70		// the requested resource could not be found
#define ERROR_NAI_FAILED_SET_LOCALE			-71		// could not set the requested locale

// Common Library
#define ERROR_NAI_CMNLIB_INFO_UNAVAILABLE	-71		// the requested info could not be obtained

// Profile file
#define ERROR_INI_SETTING_NOT_FOUND			-80	// the requested setting does not exist

// Plugin Specific (-1000)
#define ERROR_PLUGIN_FAILED					-1000	// general failure in plugin
#define ERROR_PLUGIN_EXCEPTION				-1001	// the plugin threw an exception
#define ERROR_PLUGIN_NOT_EXIST				-1002	// the plugin library specified does not exist
#define ERROR_PLUGIN_EXISTS					-1003	
#define ERROR_PLUGIN_INVALID				-1004	// exists but is invalid
#define ERROR_PLUGIN_VALID					-1005
#define ERROR_PLUGIN_METHOD_UNSUPPORTED		-1006	// the plugin doesn't supported the requested interface
#define ERROR_PLUGIN_DISABLED				-1007	// the plugin has been disabled
#define ERROR_PLUGIN_ENABLED				-1008
#define ERROR_PLUGIN_SOFTWARE_NOT_FOUND		-1009	// the software that the plugin belongs to is not available
#define ERROR_PLUGIN_SOFTWARE_FOUND			-1010
#define ERROR_PLUGIN_NOT_REGISTERED			-1011	// the specified plugin is not properly registered
#define ERROR_PLUGIN_TASK_NOT_FOUND			-1012	// the requested task is not valid
#define ERROR_PLUGIN_TASK_INVALID			-1013	// the task exists but is invalid
#define ERROR_PLUGIN_INVALID_DATA_TYPE		-1014	// the data type requested/sent is not valid
#define ERROR_PLUGIN_SOFTWARE_ID_INVALID	-1015	// the software ID is not supported or is invalid
#define ERROR_PLUGIN_UNABLE_TO_LOAD			-1016	
#define ERROR_PLUGIN_DENY_TO_RUN_TASK		naMAKE_RES(SEVERITY_SUCCESS,naFACILITY_NULL,1016) //Plugin doesn't support the run task feature
	
// Product\Policy Manager (-1100)
#define ERROR_MANAGER_XML_INVALID_SCHEMA	-1100	// the document does not match the required schema
#define ERROR_MANAGER_ABSTRACTION_INVALID	-1101	// CPluginAbstraction not properly initialized for given task
#define ERROR_MANAGER_TASK_INVALID			-1102	// the task is no longer valid
#define ERROR_MANAGER_PLUGINMGR_INVALID		-1103	// CPluginManager not globaly initialized
#define ERROR_MANAGER_PLUGINLIB_INVALID		-1104	// CPluginLibrary not properly returned 
#define ERROR_MANAGER_INIT_PLUGINLIST_FAIL  -1105	// An error occurred while initializing the plugin list
#define ERROR_MANAGER_XML_PARSING_EXCEPTION	-1106	// an exception was thrown by the XML parser
#define ERROR_MANAGER_REG_CREATE_KEY		-1107	// failed to create the required registry key
#define ERROR_MANAGER_REG_DELETE_KEY		-1108	// failed to remove the required registry key
#define ERROR_MANAGER_REG_OPEN_KEY			-1109	// failed to open the required registry key
#define ERROR_MANAGER_REG_OPEN_KEY_ALL		-1110	// failed to open the required registry key with all access
#define ERROR_MANAGER_TASK_LIST_EMPTY		-1111	// their are currently no managed tasks
#define ERROR_MANAGER_TASK_LIST_END			-1112	// iterator at end of list (no more elements)
#define ERROR_MANAGER_INTERNAL_ERROR		-1113	// an internal error occurred
#define ERROR_MANAGER_MACRO_INVALID			-1114	// the macro could not be properly expanded
#define ERROR_MANAGER_THREAD_STARTED		-1115	// the epo thread is already running
#define ERROR_MANAGER_CREATE_THREAD			-1116	// the epo thread could not be created
#define ERROR_MANAGER_NOT_INITIALIZED		-1117	// the object is not properly initialized

// XML DataObject (-1200)
#define ERROR_XML_SECTION_NOT_FOUND			-1200	// the requested section does not appear valid
#define ERROR_XML_SETTING_NOT_FOUND			-1201	// the requested setting does not appear valid
#define ERROR_XML_ATTRIBUTE_NOT_FOUND		-1202	// the requested attribute does not appear valid
#define ERROR_XML_INVALID_OBJECT			-1203	// internal error, object not valid
#define ERROR_XML_FAILED_SET_VALUE			-1204
#define ERROR_XML_FAILED_CREATE_ELEMENT		-1205
#define ERROR_XML_FAILED_CREATE_ATTRIB		-1206
#define ERROR_XML_POLICY_NOT_FOUND			-1207	// the requested policy is not valid
#define ERROR_XML_TASK_NOT_FOUND			-1208	// the requested task is not valid
#define ERROR_XML_COMP_PROPS_NOT_FOUND		-1209	// the computer properties section is missing
#define ERROR_XML_PROPERTIES_NOT_FOUND		-1210	// the software properties section is missing
#define ERROR_XML_SOURCE_ELEMENT_INVALID	-1211	// the source element is invalid
#define ERROR_XML_DEST_ELEMENT_INVALID		-1212	// the destination element is invalid
#define ERROR_XML_ELEMENTS_NOT_EQUAL		-1213	// the two elements are not equivalent
#define ERROR_XML_ATTRIB_NOT_EQUAL			-1214	// the two attribute are not equivalent
#define ERROR_XML_TASK_EXISTS				-1215	// the requested task is already running

// Management (-1300)

// Policy Compilation (-1400)
#define ERROR_POLICY_PATH_NOT_FOUND			-1400	// the path object was not found

// myDialup (-1500)
#define ERROR_MYDIALUP_THREAD_STARTED		-1500	// the dialup thread is already running
#define ERROR_MYDIALUP_CREATE_THREAD		-1501	// the dialup thread could not be created

// Framework (-1600)
#define ERROR_SUBSYS_DEINITIALIZED			-1600	// the requested subsystem in not intialized
#define ERROR_SUBSYS_NOT_FOUND				-1601	// the requested subsystem was not found
#define ERROR_SUBSYS_STOPPED				-1602	// the subsystem is not running
#define ERROR_SUBSYS_SUSPENDED				-1603	// the subsystem is suspended
#define ERROR_SUBSYS_DISABLED				-1604	// the subsystem is not registered for use (see framework manifest)
#define ERROR_SUBSYS_STOPPING				-1605	// the subsystem is stopping, can't process any more

// Framework Factory (-1700)
#define ERROR_CALLBACK_NOT_REGISTERED		-1700	// caller must register callback before using factory
#define ERROR_FRAMEWORK_UNAVAILABLE			-1701	// the framework service is not available
#define ERROR_FRAMEWORK_AVAILABLE			-1702	// the framework is currently available

// Internet Manager (-1800)
#define ERROR_INETMGR_FILE_NOT_FOUND		 -1800   // File to be uploaded or downloaded not found
#define ERROR_INETMGR_SITE_NOT_FOUND		 -1801	// Site not found
#define ERROR_INETMGR_SITELIST_NOT_FOUND	 -1802	// Subsystem cannot be initialized, sitelist is missing
#define ERROR_INETMGR_COPY_FILE				 -1803	// ImportSiteList failed to copy sitelist file
#define ERROR_INETMGR_NO_NEXT_SITE_AVAILABLE -1804	// No next nearest site is available
#define ERROR_INETMGR_PARAMETER_INVALID		 -1805	// the passed parameter appears to be invalid


// Update SubSystem (-1900)
#define ERROR_UPDATE_RUNNING        		-1900   // If update is already running then return this error for other updates
#define ERROR_UPDATE_WAITING_REBOOT     	-1901   // If waiting for reboot then fail all other updates till rebooted
#define ERROR_UPDATE_SETTINGS     			-1902   // If wrong settings
#define ERROR_UPDATE_SITE_INVALID     		-1903   // If download site is disabled or deleted
#define ERROR_UPDATE_REBOOT_PENDING			-1904	// Update/deployment failing because reboot is pending
#define ERROR_UPDATE_NO_REPOSITORY			-1905	// No valid repository available for Update/deployment
#define ERROR_UPDATE_PARAMETER_INVALID		-1906	// the passed parameter appears to be invalid

//Pull Replication
#define ERROR_PULL_REP_FAILED_TO_START		-1925   // If Pull task not started by Updater
#define ERROR_PULL_REP_RUNNING				-1926   // If Pull task allraedy running

// Script SubSystem (-1950)
#define ERROR_SCRIPT_START	     			-1950   // If failed to start script


// Installer (-2000)
#define ERROR_INST_INVALID_CMD				-2000	// invalid command line parameters passed
#define ERROR_INST_MANIFEST_INVALID			-2001	// the install manifest is not valid
#define ERROR_COMPONENT_NOT_FOUND			-2002	// the requested component was not found in manifest
#define ERROR_COMPONENT_INVALID				-2003	// the requested component was invalid (internal error)
#define ERROR_ZIP_PACKAGE_CREATION			-2004	// zip package creation failed
#define ERROR_SFX_PACKAGE_CREATION			-2006	// SFX package creation failed
#define ERROR_BIN_PACKAGE_CREATION			-2007	// bin replace failed
#define ERROR_INST_ALREADY_RUNNING			-2008	// install is already started
#define ERROR_INST_NOTHING_TO_UPGRADE		-2009	// Previous install is not found for upgrade
#define ERROR_NONEMBEDDED_WITHCREDENTIAL	-2010	// Non-embedded package does not support embedded credentials
#define ERROR_EMBEDDED_INVALIDCREDENTIAL	-2011	// Please verify the embedded credentials are correct

//Installer Stub (2051)
#define ERROR_STUB_ALREADY_RUNNING			-2051	// Framework installer stub already running
#define ERROR_STUB_CREATE_MUTEX				-2052	// Framework installer stub failed to create mutex
#define ERROR_STUB_OPEN_ITSELF				-2053	// Framework installer stub can not open itself
#define ERROR_STUB_ZIP_PACKAGE_EXTRACTION	-2054	// zip package extraction failed
#define ERROR_STUB_CREATE_PROCESS			-2055	// Unable to launch framework installer instance
#define ERROR_STUB_INVALID_FORMAT			-2056	// Unable to understand package format
#define ERROR_STUB_FILE_CREATE_TEMP_DIR		-2057	// unable to create temp directory
#define ERROR_STUB_TEMP_ZIP_CREATE			-2058	// unable to create temp zip file

// Logging (-2100)
#define ERROR_LOGGER_LOG_NOT_FOUND			-2100	// the requested log file was not found
#define ERROR_LOGGER_NO_MESSAGES			-2101	// there are currently no messages

// SPIPE (-2200)
#define ERROR_SPIPE_SETTING_NOT_FOUND		-2200	// the requested INFO was not found
#define ERROR_SPIPE_PACKAGE_SIGN			-2201	// failed to sign a package
#define ERROR_SPIPE_KEY_NOT_FOUND			-2202	// a required encryption key is not available
#define ERROR_SPIPE_PACKAGE_INVALID			-2203	// this does not appear to be a valid package
#define ERROR_SPIPE_PACKAGE_UNSUPPORTED		-2204	// this is a package we don't support
#define ERROR_SPIPE_PACKAGE_PROCESSED		-2205	// the package has already been processed
#define ERROR_SPIPE_NO_MORE_FILES			-2206	// there are no more files in map
#define ERROR_SPIPE_NO_MORE_INFO			-2207	// there are no more info in map


// Event Filter DLL (-2300)
#define ERROR_EVTINF_EVT_FILTERED			-2300	// The event has been filtered
#define ERROR_EVTINF_CMA_IN_UNMANAGED_MODE	-2301	// CMA is in un-managed mode 

// Agent communication(-2400)
#define ERROR_AGENT_CONNECT_FAILED			-2400		// failed to connect to ePO Server
#define ERROR_NO_PACKAGE_RECEIVE			naMAKE_RES(SEVERITY_SUCCESS,naFACILITY_NULL,2402)		// NAINET_SPIPE_NO_PACKAGE_RECEIVE
#define ERROR_SERVER_MAX_DOWNLOAD_REACHED	-2403		// NAINET_SPIPE_SERVER_MAX_DOWNLOAD_REACHED
#define ERROR_FAILED_TO_UPLOAD_PACKAGE		-2404		// NAINET_SPIPE_FAILED_TO_UPLOAD_PACKAGE
#define ERROR_FAILED_TO_DOWNLOAD_PACKAGE	-2405		// NAINET_SPIPE_FAILED_TO_DOWNLOAD_PACKAGE
// Added for GUID Regeneration  
#define ERROR_AGENT_GUID_EXISTING			-2406		// Agent GUID is already existing in Epo server. Regenerate it
#define ERROR_SERVER_ACCESS_DENIED			-2407		//HTTP 403 from server, trigger key update


#endif //#if !defined(AFX_ERRORCODES_H__95CC287F_1E5F_4165_9F30_05DB9961243F__INCLUDED_)

