#pragma once
//Copyright (C) 2009 McAfee, Inc.  All rights reserved.

/* This file provides the necessary definitions used by the ILogger interface. */

enum ESUBSYSTEM
{
	eLogging             = 0x1,
	eManagement          = 0x2,
	eUpdater             = 0x4,
	eScript              = 0x8,
	eInetMgr             = 0x10,
	eTestHarness         = 0x20,
	eScheduler           = 0x40,
	eAgentMonitor        = 0x80,
	eUserSpaceController = 0x100,
	eFrameworkFactory    = 0x200,
	eFrameworkService    = 0x400,
	ePolicyManager       = 0x800,
	eProductManager      = 0x1000,
	eAgent               = 0x2000,
	eListenServer        = 0x4000,
	eFrameworkPlugin     = 0x8000,
	eSiteManager         = 0x10000,
    eTrustedConnection   = 0x20000,
    ESUBSYSTEM_ALL       = 0x3ffff
};

typedef enum _naimcomn_LogLevel
{
    NAIMCOMN_FIRST_LOGLEVEL,
    NAIMCOMN_ACTIVITY_ERROR = 1,
    NAIMCOMN_ACTIVITY_WARN = 2,
    NAIMCOMN_ACTIVITY_INFO = 3,
    NAIMCOMN_ACTIVITY_DETAIL = 4,
    NAIMCOMN_MAX_ACTIVITY_LEVEL = NAIMCOMN_ACTIVITY_DETAIL,
    NAIMCOMN_DEBUG_ERROR = 5,
    NAIMCOMN_DEBUG_WARN = 6,
    NAIMCOMN_DEBUG_INFO = 7,
    NAIMCOMN_DEBUG_DETAIL = 8,
    NAIMCOMN_LAST_LOGLEVEL
}naimcomn_LogLevel;

#define CMNLIB_MAX_MAILSLOT_NAME (64 + 1)
#define NAIMCOMN_MAX_LOG_MESSAGE (4096 + 1)

#pragma pack(push, 8)
struct cmnlib_LogMailslotPacket
{
    struct
    {
        SYSTEMTIME timeStamp;
        long logLevel;
        long subsystem;
    } header;
    WCHAR message[NAIMCOMN_MAX_LOG_MESSAGE];
};
#pragma pack(pop)

/* Map legacy log types to naimcomn log levels. */
#define LOGTYPE naimcomn_LogLevel

#define eFailure      NAIMCOMN_ACTIVITY_ERROR
#define eWarning      NAIMCOMN_ACTIVITY_WARN
#define eStandard1    NAIMCOMN_ACTIVITY_INFO
#define eStandard2    NAIMCOMN_ACTIVITY_DETAIL
#define eDebugFailure NAIMCOMN_DEBUG_ERROR
#define eDebugWarning NAIMCOMN_DEBUG_WARN
#define eDebug1	      NAIMCOMN_DEBUG_INFO
#define eDebug2	      NAIMCOMN_DEBUG_DETAIL
