

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 6.00.0361 */
/* at Fri Dec 01 20:33:30 2006
 */
/* Compiler settings for \Cma1\Build\Cma\win\Framework\UpdateSubSys\UpdateSubSys.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
//@@MIDL_FILE_HEADING(  )

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __UpdateSubSys_h__
#define __UpdateSubSys_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IMcAfeeUpdate_FWD_DEFINED__
#define __IMcAfeeUpdate_FWD_DEFINED__
typedef interface IMcAfeeUpdate IMcAfeeUpdate;
#endif 	/* __IMcAfeeUpdate_FWD_DEFINED__ */


#ifndef ___IMcAfeeUpdateEvents_FWD_DEFINED__
#define ___IMcAfeeUpdateEvents_FWD_DEFINED__
typedef interface _IMcAfeeUpdateEvents _IMcAfeeUpdateEvents;
#endif 	/* ___IMcAfeeUpdateEvents_FWD_DEFINED__ */


#ifndef __IMcAfeeUpdate_FWD_DEFINED__
#define __IMcAfeeUpdate_FWD_DEFINED__
typedef interface IMcAfeeUpdate IMcAfeeUpdate;
#endif 	/* __IMcAfeeUpdate_FWD_DEFINED__ */


#ifndef __IMcAfeeUpdate2_FWD_DEFINED__
#define __IMcAfeeUpdate2_FWD_DEFINED__
typedef interface IMcAfeeUpdate2 IMcAfeeUpdate2;
#endif 	/* __IMcAfeeUpdate2_FWD_DEFINED__ */


#ifndef __McAfeeUpdate_FWD_DEFINED__
#define __McAfeeUpdate_FWD_DEFINED__

#ifdef __cplusplus
typedef class McAfeeUpdate McAfeeUpdate;
#else
typedef struct McAfeeUpdate McAfeeUpdate;
#endif /* __cplusplus */

#endif 	/* __McAfeeUpdate_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 

void * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void * ); 

/* interface __MIDL_itf_UpdateSubSys_0000 */
/* [local] */ 

typedef 
enum tag_UPDATE_STATE
    {	eUPDATE_UNKNOWN	= -1,
	eUPDATE_READY	= 0,
	eUPDATE_FINISHED_OK	= eUPDATE_READY + 1,
	eUPDATE_FINISHED_FAIL	= eUPDATE_FINISHED_OK + 1,
	eUPDATE_FINISHED_NOTFOUND	= eUPDATE_FINISHED_FAIL + 1,
	eUPDATE_FINISHED_CANCEL	= eUPDATE_FINISHED_NOTFOUND + 1,
	eUPDATE_FINISHED_WAITING_REBOOT	= eUPDATE_FINISHED_CANCEL + 1,
	eUPDATE_FINISHED_WAITING_REBOOT_RESTART	= eUPDATE_FINISHED_WAITING_REBOOT + 1,
	eUPDATE_FINISHED_PARTIAL	= eUPDATE_FINISHED_WAITING_REBOOT_RESTART + 1,
	eUPDATE_SUCCEEDED	= eUPDATE_FINISHED_PARTIAL + 1,
	eUPDATE_FAILED	= eUPDATE_SUCCEEDED + 1,
	eUPDATE_INIT	= eUPDATE_FAILED + 1,
	eUPDATE_LOADCONFIG	= eUPDATE_INIT + 1,
	eUPDATE_PRENOTIFY	= eUPDATE_LOADCONFIG + 1,
	eUPDATE_EOL_CHECK	= eUPDATE_PRENOTIFY + 1,
	eUPDATE_ROLLBACK_CHECK	= eUPDATE_EOL_CHECK + 1,
	eUPDATE_RUNNING	= eUPDATE_ROLLBACK_CHECK + 1,
	eUPDATE_CRITICAL	= eUPDATE_RUNNING + 1,
	eUPDATE_NONCRITICAL	= eUPDATE_CRITICAL + 1,
	eUPDATE_SUSPEND	= eUPDATE_NONCRITICAL + 1,
	eUPDATE_RESUME	= eUPDATE_SUSPEND + 1,
	eUPDATE_DOWNLOAD	= eUPDATE_RESUME + 1,
	eUPDATE_STOPSERVICE	= eUPDATE_DOWNLOAD + 1,
	eUPDATE_STARTSERVICE	= eUPDATE_STOPSERVICE + 1,
	eUPDATE_BACKUP	= eUPDATE_STARTSERVICE + 1,
	eUPDATE_COPY	= eUPDATE_BACKUP + 1,
	eUPDATE_SETCONFIG	= eUPDATE_COPY + 1,
	eUPDATE_POSTNOTIFY	= eUPDATE_SETCONFIG + 1,
	eUPDATE_CHECK_SITE_STATUS	= eUPDATE_POSTNOTIFY + 1,
	eUPDATE_COMPARE_SITES	= eUPDATE_CHECK_SITE_STATUS + 1,
	eUPDATE_PRENOTIFYFORCE	= eUPDATE_COMPARE_SITES + 1
    } 	UPDATE_STATE;

typedef 
enum tag_REBOOT_TYPE
    {	eREBOOT_TYPE_NORMAL	= 0,
	eREBOOT_TYPE_FORCE	= eREBOOT_TYPE_NORMAL + 1
    } 	REBOOT_TYPE;

typedef 
enum tag_UPDATE_ERROR
    {	eUPDATE_ERROR_UNKNOWN	= -1,
	eUPDATE_ERROR_OK	= 0,
	eUPDATE_ERROR_ACCEPT	= 0,
	eUPDATE_ERROR_LATEST	= eUPDATE_ERROR_ACCEPT + 1,
	eUPDATE_ERROR_FAILED_TO_START	= eUPDATE_ERROR_LATEST + 1,
	eUPDATE_ERROR_CALLBACKINIT	= eUPDATE_ERROR_FAILED_TO_START + 1,
	eUPDATE_ERROR_REJECT	= eUPDATE_ERROR_CALLBACKINIT + 1,
	eUPDATE_ERROR_CANCEL	= eUPDATE_ERROR_REJECT + 1,
	eUPDATE_ERROR_EOL	= eUPDATE_ERROR_CANCEL + 1,
	eUPDATE_ERROR_LOADCONFIG	= eUPDATE_ERROR_EOL + 1,
	eUPDATE_ERROR_DOWNLOAD	= eUPDATE_ERROR_LOADCONFIG + 1,
	eUPDATE_ERROR_POSTPONE	= eUPDATE_ERROR_DOWNLOAD + 1,
	eUPDATE_ERROR_REPLICATE	= eUPDATE_ERROR_POSTPONE + 1,
	eUPDATE_ERROR_SERVICE_STOP	= eUPDATE_ERROR_REPLICATE + 1,
	eUPDATE_ERROR_SERVICE_START	= eUPDATE_ERROR_SERVICE_STOP + 1,
	eUPDATE_ERROR_INVALID	= eUPDATE_ERROR_SERVICE_START + 1,
	eUPDATE_ERROR_INVALID_DATS	= eUPDATE_ERROR_INVALID + 1,
	eUPDATE_ERROR_BACKUP	= eUPDATE_ERROR_INVALID_DATS + 1,
	eUPDATE_ERROR_ROLLBACK	= eUPDATE_ERROR_BACKUP + 1,
	eUPDATE_ERROR_COPY	= eUPDATE_ERROR_ROLLBACK + 1,
	eUPDATE_ERROR_DISKSPACE	= eUPDATE_ERROR_COPY + 1,
	eUPDATE_ERROR_SETCONFIG	= eUPDATE_ERROR_DISKSPACE + 1,
	eUPDATE_ERROR_LICENSE_EXPIRED	= eUPDATE_ERROR_SETCONFIG + 1,
	eUPDATE_ERROR_REBOOT_PENDING	= eUPDATE_ERROR_LICENSE_EXPIRED + 1,
	eUPDATE_ERROR_UPDATEOPTIONS	= eUPDATE_ERROR_REBOOT_PENDING + 1,
	eUPDATE_ERROR_SCRIP_ENGINE_TERMINATED_UNEXPECTEDLY	= eUPDATE_ERROR_UPDATEOPTIONS + 1
    } 	UPDATE_ERROR;

typedef 
enum tag_PRODUCT_RETURN_VALUE
    {	ePRODUCT_RETURN_OK	= 0,
	ePRODUCT_RETURN_ACCEPT	= 0,
	ePRODUCT_RETURN_FAIL	= ePRODUCT_RETURN_ACCEPT + 1,
	ePRODUCT_RETURN_REJECT	= ePRODUCT_RETURN_FAIL + 1,
	ePRODUCT_RETURN_ROLLBACK	= ePRODUCT_RETURN_REJECT + 1,
	ePRODUCT_RETURN_LICENSE_EXPIRED	= ePRODUCT_RETURN_ROLLBACK + 1
    } 	PRODUCT_RETURN_VALUE;



extern RPC_IF_HANDLE __MIDL_itf_UpdateSubSys_0000_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_UpdateSubSys_0000_v0_0_s_ifspec;

#ifndef __IMcAfeeUpdate_INTERFACE_DEFINED__
#define __IMcAfeeUpdate_INTERFACE_DEFINED__

/* interface IMcAfeeUpdate */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IMcAfeeUpdate;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("D831533D-0324-4EA4-B3FD-073AFEE85181")
    IMcAfeeUpdate : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE RunUpdateTask( 
            /* [in] */ const BSTR StartBy,
            /* [in] */ const BSTR strTaskID) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE RunUpdateNow( 
            /* [in] */ const BSTR StartBy,
            /* [in] */ LONG lCookie,
            /* [in] */ BOOL bQuiet) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE StopUpdate( void) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_UserLocale( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_UpdateState( 
            /* [retval][out] */ UPDATE_STATE *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SoftwareID( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_UserCookie( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetUpdateOption( 
            /* [in] */ const BSTR strSectionName,
            /* [in] */ const BSTR strValueName,
            /* [in] */ const BSTR strValue) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE DownloadSitelist( 
            /* [in] */ BSTR URL,
            /* [in] */ BSTR DomainName,
            /* [in] */ BSTR UserName,
            /* [in] */ BSTR Password,
            /* [in] */ BOOL UseLoggedOnUser,
            /* [in] */ BOOL UseProxy) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetProxySettings( 
            /* [in] */ BOOL UseIESettings,
            /* [in] */ BSTR ProxyServer,
            /* [in] */ short ProxyPort,
            /* [in] */ BSTR ProxyUser,
            /* [in] */ BSTR ProxyPassword,
            /* [in] */ BOOL UseProxyAuthentication) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE RunDeploymentTask( 
            /* [in] */ const BSTR StartBy,
            /* [in] */ const BSTR TaskID) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ImportSitelist( 
            /* [in] */ const BSTR Sitelist) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE RunPullTask( 
            /* [in] */ const BSTR StartBy,
            /* [in] */ const BSTR TaskID,
            /* [in] */ const BSTR SourceSiteName,
            /* [in] */ const BSTR DestDir) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE StopPullTask( void) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_PullTaskState( 
            /* [retval][out] */ UPDATE_STATE *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE RunRollbackUpdate( 
            /* [in] */ const BSTR ProductID,
            /* [in] */ LONG lUserCookie) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IMcAfeeUpdateVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IMcAfeeUpdate * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IMcAfeeUpdate * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IMcAfeeUpdate * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IMcAfeeUpdate * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IMcAfeeUpdate * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IMcAfeeUpdate * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IMcAfeeUpdate * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RunUpdateTask )( 
            IMcAfeeUpdate * This,
            /* [in] */ const BSTR StartBy,
            /* [in] */ const BSTR strTaskID);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RunUpdateNow )( 
            IMcAfeeUpdate * This,
            /* [in] */ const BSTR StartBy,
            /* [in] */ LONG lCookie,
            /* [in] */ BOOL bQuiet);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *StopUpdate )( 
            IMcAfeeUpdate * This);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_UserLocale )( 
            IMcAfeeUpdate * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_UpdateState )( 
            IMcAfeeUpdate * This,
            /* [retval][out] */ UPDATE_STATE *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SoftwareID )( 
            IMcAfeeUpdate * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_UserCookie )( 
            IMcAfeeUpdate * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetUpdateOption )( 
            IMcAfeeUpdate * This,
            /* [in] */ const BSTR strSectionName,
            /* [in] */ const BSTR strValueName,
            /* [in] */ const BSTR strValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *DownloadSitelist )( 
            IMcAfeeUpdate * This,
            /* [in] */ BSTR URL,
            /* [in] */ BSTR DomainName,
            /* [in] */ BSTR UserName,
            /* [in] */ BSTR Password,
            /* [in] */ BOOL UseLoggedOnUser,
            /* [in] */ BOOL UseProxy);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetProxySettings )( 
            IMcAfeeUpdate * This,
            /* [in] */ BOOL UseIESettings,
            /* [in] */ BSTR ProxyServer,
            /* [in] */ short ProxyPort,
            /* [in] */ BSTR ProxyUser,
            /* [in] */ BSTR ProxyPassword,
            /* [in] */ BOOL UseProxyAuthentication);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RunDeploymentTask )( 
            IMcAfeeUpdate * This,
            /* [in] */ const BSTR StartBy,
            /* [in] */ const BSTR TaskID);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ImportSitelist )( 
            IMcAfeeUpdate * This,
            /* [in] */ const BSTR Sitelist);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RunPullTask )( 
            IMcAfeeUpdate * This,
            /* [in] */ const BSTR StartBy,
            /* [in] */ const BSTR TaskID,
            /* [in] */ const BSTR SourceSiteName,
            /* [in] */ const BSTR DestDir);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *StopPullTask )( 
            IMcAfeeUpdate * This);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_PullTaskState )( 
            IMcAfeeUpdate * This,
            /* [retval][out] */ UPDATE_STATE *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RunRollbackUpdate )( 
            IMcAfeeUpdate * This,
            /* [in] */ const BSTR ProductID,
            /* [in] */ LONG lUserCookie);
        
        END_INTERFACE
    } IMcAfeeUpdateVtbl;

    interface IMcAfeeUpdate
    {
        CONST_VTBL struct IMcAfeeUpdateVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IMcAfeeUpdate_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IMcAfeeUpdate_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IMcAfeeUpdate_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IMcAfeeUpdate_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IMcAfeeUpdate_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IMcAfeeUpdate_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IMcAfeeUpdate_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IMcAfeeUpdate_RunUpdateTask(This,StartBy,strTaskID)	\
    (This)->lpVtbl -> RunUpdateTask(This,StartBy,strTaskID)

#define IMcAfeeUpdate_RunUpdateNow(This,StartBy,lCookie,bQuiet)	\
    (This)->lpVtbl -> RunUpdateNow(This,StartBy,lCookie,bQuiet)

#define IMcAfeeUpdate_StopUpdate(This)	\
    (This)->lpVtbl -> StopUpdate(This)

#define IMcAfeeUpdate_put_UserLocale(This,newVal)	\
    (This)->lpVtbl -> put_UserLocale(This,newVal)

#define IMcAfeeUpdate_get_UpdateState(This,pVal)	\
    (This)->lpVtbl -> get_UpdateState(This,pVal)

#define IMcAfeeUpdate_get_SoftwareID(This,pVal)	\
    (This)->lpVtbl -> get_SoftwareID(This,pVal)

#define IMcAfeeUpdate_put_UserCookie(This,newVal)	\
    (This)->lpVtbl -> put_UserCookie(This,newVal)

#define IMcAfeeUpdate_SetUpdateOption(This,strSectionName,strValueName,strValue)	\
    (This)->lpVtbl -> SetUpdateOption(This,strSectionName,strValueName,strValue)

#define IMcAfeeUpdate_DownloadSitelist(This,URL,DomainName,UserName,Password,UseLoggedOnUser,UseProxy)	\
    (This)->lpVtbl -> DownloadSitelist(This,URL,DomainName,UserName,Password,UseLoggedOnUser,UseProxy)

#define IMcAfeeUpdate_SetProxySettings(This,UseIESettings,ProxyServer,ProxyPort,ProxyUser,ProxyPassword,UseProxyAuthentication)	\
    (This)->lpVtbl -> SetProxySettings(This,UseIESettings,ProxyServer,ProxyPort,ProxyUser,ProxyPassword,UseProxyAuthentication)

#define IMcAfeeUpdate_RunDeploymentTask(This,StartBy,TaskID)	\
    (This)->lpVtbl -> RunDeploymentTask(This,StartBy,TaskID)

#define IMcAfeeUpdate_ImportSitelist(This,Sitelist)	\
    (This)->lpVtbl -> ImportSitelist(This,Sitelist)

#define IMcAfeeUpdate_RunPullTask(This,StartBy,TaskID,SourceSiteName,DestDir)	\
    (This)->lpVtbl -> RunPullTask(This,StartBy,TaskID,SourceSiteName,DestDir)

#define IMcAfeeUpdate_StopPullTask(This)	\
    (This)->lpVtbl -> StopPullTask(This)

#define IMcAfeeUpdate_get_PullTaskState(This,pVal)	\
    (This)->lpVtbl -> get_PullTaskState(This,pVal)

#define IMcAfeeUpdate_RunRollbackUpdate(This,ProductID,lUserCookie)	\
    (This)->lpVtbl -> RunRollbackUpdate(This,ProductID,lUserCookie)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IMcAfeeUpdate_RunUpdateTask_Proxy( 
    IMcAfeeUpdate * This,
    /* [in] */ const BSTR StartBy,
    /* [in] */ const BSTR strTaskID);


void __RPC_STUB IMcAfeeUpdate_RunUpdateTask_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IMcAfeeUpdate_RunUpdateNow_Proxy( 
    IMcAfeeUpdate * This,
    /* [in] */ const BSTR StartBy,
    /* [in] */ LONG lCookie,
    /* [in] */ BOOL bQuiet);


void __RPC_STUB IMcAfeeUpdate_RunUpdateNow_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IMcAfeeUpdate_StopUpdate_Proxy( 
    IMcAfeeUpdate * This);


void __RPC_STUB IMcAfeeUpdate_StopUpdate_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IMcAfeeUpdate_put_UserLocale_Proxy( 
    IMcAfeeUpdate * This,
    /* [in] */ long newVal);


void __RPC_STUB IMcAfeeUpdate_put_UserLocale_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IMcAfeeUpdate_get_UpdateState_Proxy( 
    IMcAfeeUpdate * This,
    /* [retval][out] */ UPDATE_STATE *pVal);


void __RPC_STUB IMcAfeeUpdate_get_UpdateState_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IMcAfeeUpdate_get_SoftwareID_Proxy( 
    IMcAfeeUpdate * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IMcAfeeUpdate_get_SoftwareID_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IMcAfeeUpdate_put_UserCookie_Proxy( 
    IMcAfeeUpdate * This,
    /* [in] */ long newVal);


void __RPC_STUB IMcAfeeUpdate_put_UserCookie_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IMcAfeeUpdate_SetUpdateOption_Proxy( 
    IMcAfeeUpdate * This,
    /* [in] */ const BSTR strSectionName,
    /* [in] */ const BSTR strValueName,
    /* [in] */ const BSTR strValue);


void __RPC_STUB IMcAfeeUpdate_SetUpdateOption_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IMcAfeeUpdate_DownloadSitelist_Proxy( 
    IMcAfeeUpdate * This,
    /* [in] */ BSTR URL,
    /* [in] */ BSTR DomainName,
    /* [in] */ BSTR UserName,
    /* [in] */ BSTR Password,
    /* [in] */ BOOL UseLoggedOnUser,
    /* [in] */ BOOL UseProxy);


void __RPC_STUB IMcAfeeUpdate_DownloadSitelist_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IMcAfeeUpdate_SetProxySettings_Proxy( 
    IMcAfeeUpdate * This,
    /* [in] */ BOOL UseIESettings,
    /* [in] */ BSTR ProxyServer,
    /* [in] */ short ProxyPort,
    /* [in] */ BSTR ProxyUser,
    /* [in] */ BSTR ProxyPassword,
    /* [in] */ BOOL UseProxyAuthentication);


void __RPC_STUB IMcAfeeUpdate_SetProxySettings_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IMcAfeeUpdate_RunDeploymentTask_Proxy( 
    IMcAfeeUpdate * This,
    /* [in] */ const BSTR StartBy,
    /* [in] */ const BSTR TaskID);


void __RPC_STUB IMcAfeeUpdate_RunDeploymentTask_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IMcAfeeUpdate_ImportSitelist_Proxy( 
    IMcAfeeUpdate * This,
    /* [in] */ const BSTR Sitelist);


void __RPC_STUB IMcAfeeUpdate_ImportSitelist_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IMcAfeeUpdate_RunPullTask_Proxy( 
    IMcAfeeUpdate * This,
    /* [in] */ const BSTR StartBy,
    /* [in] */ const BSTR TaskID,
    /* [in] */ const BSTR SourceSiteName,
    /* [in] */ const BSTR DestDir);


void __RPC_STUB IMcAfeeUpdate_RunPullTask_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IMcAfeeUpdate_StopPullTask_Proxy( 
    IMcAfeeUpdate * This);


void __RPC_STUB IMcAfeeUpdate_StopPullTask_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IMcAfeeUpdate_get_PullTaskState_Proxy( 
    IMcAfeeUpdate * This,
    /* [retval][out] */ UPDATE_STATE *pVal);


void __RPC_STUB IMcAfeeUpdate_get_PullTaskState_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IMcAfeeUpdate_RunRollbackUpdate_Proxy( 
    IMcAfeeUpdate * This,
    /* [in] */ const BSTR ProductID,
    /* [in] */ LONG lUserCookie);


void __RPC_STUB IMcAfeeUpdate_RunRollbackUpdate_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IMcAfeeUpdate_INTERFACE_DEFINED__ */



#ifndef __UPDATESUBSYSLib_LIBRARY_DEFINED__
#define __UPDATESUBSYSLib_LIBRARY_DEFINED__

/* library UPDATESUBSYSLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_UPDATESUBSYSLib;

#ifndef ___IMcAfeeUpdateEvents_DISPINTERFACE_DEFINED__
#define ___IMcAfeeUpdateEvents_DISPINTERFACE_DEFINED__

/* dispinterface _IMcAfeeUpdateEvents */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__IMcAfeeUpdateEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("963B7D71-C519-4a5d-B8E7-742B08628F5D")
    _IMcAfeeUpdateEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _IMcAfeeUpdateEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _IMcAfeeUpdateEvents * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _IMcAfeeUpdateEvents * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _IMcAfeeUpdateEvents * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _IMcAfeeUpdateEvents * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _IMcAfeeUpdateEvents * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _IMcAfeeUpdateEvents * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _IMcAfeeUpdateEvents * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _IMcAfeeUpdateEventsVtbl;

    interface _IMcAfeeUpdateEvents
    {
        CONST_VTBL struct _IMcAfeeUpdateEventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _IMcAfeeUpdateEvents_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _IMcAfeeUpdateEvents_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _IMcAfeeUpdateEvents_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _IMcAfeeUpdateEvents_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define _IMcAfeeUpdateEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define _IMcAfeeUpdateEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define _IMcAfeeUpdateEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___IMcAfeeUpdateEvents_DISPINTERFACE_DEFINED__ */


#ifndef __IMcAfeeUpdate2_INTERFACE_DEFINED__
#define __IMcAfeeUpdate2_INTERFACE_DEFINED__

/* interface IMcAfeeUpdate2 */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IMcAfeeUpdate2;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("A33F52E5-9F67-459C-95D3-8420B50E7183")
    IMcAfeeUpdate2 : public IMcAfeeUpdate
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE RunRemediationUpdate( 
            /* [in] */ const BSTR StartBy,
            /* [in] */ LONG lCookie,
            /* [in] */ BOOL bQuiet,
            /* [in] */ const BSTR RemediationURL) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE WaitForRemediationUpdateToComplete( 
            /* [in] */ LONG lWaitTimeOut,
            /* [out] */ LONG *pStatus) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetUpdateTaskOption( 
            /* [in] */ const BSTR StartBy,
            /* [in] */ const BSTR strTaskID,
            /* [in] */ const BSTR strSectionName,
            /* [in] */ const BSTR strValueName,
            /* [in] */ const BSTR strValue) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetUpdateNowOption( 
            /* [in] */ const BSTR StartBy,
            /* [in] */ const BSTR strSectionName,
            /* [in] */ const BSTR strValueName,
            /* [in] */ const BSTR strValue) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IMcAfeeUpdate2Vtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IMcAfeeUpdate2 * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IMcAfeeUpdate2 * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IMcAfeeUpdate2 * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IMcAfeeUpdate2 * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IMcAfeeUpdate2 * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IMcAfeeUpdate2 * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IMcAfeeUpdate2 * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RunUpdateTask )( 
            IMcAfeeUpdate2 * This,
            /* [in] */ const BSTR StartBy,
            /* [in] */ const BSTR strTaskID);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RunUpdateNow )( 
            IMcAfeeUpdate2 * This,
            /* [in] */ const BSTR StartBy,
            /* [in] */ LONG lCookie,
            /* [in] */ BOOL bQuiet);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *StopUpdate )( 
            IMcAfeeUpdate2 * This);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_UserLocale )( 
            IMcAfeeUpdate2 * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_UpdateState )( 
            IMcAfeeUpdate2 * This,
            /* [retval][out] */ UPDATE_STATE *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SoftwareID )( 
            IMcAfeeUpdate2 * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_UserCookie )( 
            IMcAfeeUpdate2 * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetUpdateOption )( 
            IMcAfeeUpdate2 * This,
            /* [in] */ const BSTR strSectionName,
            /* [in] */ const BSTR strValueName,
            /* [in] */ const BSTR strValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *DownloadSitelist )( 
            IMcAfeeUpdate2 * This,
            /* [in] */ BSTR URL,
            /* [in] */ BSTR DomainName,
            /* [in] */ BSTR UserName,
            /* [in] */ BSTR Password,
            /* [in] */ BOOL UseLoggedOnUser,
            /* [in] */ BOOL UseProxy);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetProxySettings )( 
            IMcAfeeUpdate2 * This,
            /* [in] */ BOOL UseIESettings,
            /* [in] */ BSTR ProxyServer,
            /* [in] */ short ProxyPort,
            /* [in] */ BSTR ProxyUser,
            /* [in] */ BSTR ProxyPassword,
            /* [in] */ BOOL UseProxyAuthentication);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RunDeploymentTask )( 
            IMcAfeeUpdate2 * This,
            /* [in] */ const BSTR StartBy,
            /* [in] */ const BSTR TaskID);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ImportSitelist )( 
            IMcAfeeUpdate2 * This,
            /* [in] */ const BSTR Sitelist);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RunPullTask )( 
            IMcAfeeUpdate2 * This,
            /* [in] */ const BSTR StartBy,
            /* [in] */ const BSTR TaskID,
            /* [in] */ const BSTR SourceSiteName,
            /* [in] */ const BSTR DestDir);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *StopPullTask )( 
            IMcAfeeUpdate2 * This);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_PullTaskState )( 
            IMcAfeeUpdate2 * This,
            /* [retval][out] */ UPDATE_STATE *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RunRollbackUpdate )( 
            IMcAfeeUpdate2 * This,
            /* [in] */ const BSTR ProductID,
            /* [in] */ LONG lUserCookie);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RunRemediationUpdate )( 
            IMcAfeeUpdate2 * This,
            /* [in] */ const BSTR StartBy,
            /* [in] */ LONG lCookie,
            /* [in] */ BOOL bQuiet,
            /* [in] */ const BSTR RemediationURL);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *WaitForRemediationUpdateToComplete )( 
            IMcAfeeUpdate2 * This,
            /* [in] */ LONG lWaitTimeOut,
            /* [out] */ LONG *pStatus);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetUpdateTaskOption )( 
            IMcAfeeUpdate2 * This,
            /* [in] */ const BSTR StartBy,
            /* [in] */ const BSTR strTaskID,
            /* [in] */ const BSTR strSectionName,
            /* [in] */ const BSTR strValueName,
            /* [in] */ const BSTR strValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetUpdateNowOption )( 
            IMcAfeeUpdate2 * This,
            /* [in] */ const BSTR StartBy,
            /* [in] */ const BSTR strSectionName,
            /* [in] */ const BSTR strValueName,
            /* [in] */ const BSTR strValue);
        
        END_INTERFACE
    } IMcAfeeUpdate2Vtbl;

    interface IMcAfeeUpdate2
    {
        CONST_VTBL struct IMcAfeeUpdate2Vtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IMcAfeeUpdate2_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IMcAfeeUpdate2_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IMcAfeeUpdate2_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IMcAfeeUpdate2_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IMcAfeeUpdate2_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IMcAfeeUpdate2_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IMcAfeeUpdate2_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IMcAfeeUpdate2_RunUpdateTask(This,StartBy,strTaskID)	\
    (This)->lpVtbl -> RunUpdateTask(This,StartBy,strTaskID)

#define IMcAfeeUpdate2_RunUpdateNow(This,StartBy,lCookie,bQuiet)	\
    (This)->lpVtbl -> RunUpdateNow(This,StartBy,lCookie,bQuiet)

#define IMcAfeeUpdate2_StopUpdate(This)	\
    (This)->lpVtbl -> StopUpdate(This)

#define IMcAfeeUpdate2_put_UserLocale(This,newVal)	\
    (This)->lpVtbl -> put_UserLocale(This,newVal)

#define IMcAfeeUpdate2_get_UpdateState(This,pVal)	\
    (This)->lpVtbl -> get_UpdateState(This,pVal)

#define IMcAfeeUpdate2_get_SoftwareID(This,pVal)	\
    (This)->lpVtbl -> get_SoftwareID(This,pVal)

#define IMcAfeeUpdate2_put_UserCookie(This,newVal)	\
    (This)->lpVtbl -> put_UserCookie(This,newVal)

#define IMcAfeeUpdate2_SetUpdateOption(This,strSectionName,strValueName,strValue)	\
    (This)->lpVtbl -> SetUpdateOption(This,strSectionName,strValueName,strValue)

#define IMcAfeeUpdate2_DownloadSitelist(This,URL,DomainName,UserName,Password,UseLoggedOnUser,UseProxy)	\
    (This)->lpVtbl -> DownloadSitelist(This,URL,DomainName,UserName,Password,UseLoggedOnUser,UseProxy)

#define IMcAfeeUpdate2_SetProxySettings(This,UseIESettings,ProxyServer,ProxyPort,ProxyUser,ProxyPassword,UseProxyAuthentication)	\
    (This)->lpVtbl -> SetProxySettings(This,UseIESettings,ProxyServer,ProxyPort,ProxyUser,ProxyPassword,UseProxyAuthentication)

#define IMcAfeeUpdate2_RunDeploymentTask(This,StartBy,TaskID)	\
    (This)->lpVtbl -> RunDeploymentTask(This,StartBy,TaskID)

#define IMcAfeeUpdate2_ImportSitelist(This,Sitelist)	\
    (This)->lpVtbl -> ImportSitelist(This,Sitelist)

#define IMcAfeeUpdate2_RunPullTask(This,StartBy,TaskID,SourceSiteName,DestDir)	\
    (This)->lpVtbl -> RunPullTask(This,StartBy,TaskID,SourceSiteName,DestDir)

#define IMcAfeeUpdate2_StopPullTask(This)	\
    (This)->lpVtbl -> StopPullTask(This)

#define IMcAfeeUpdate2_get_PullTaskState(This,pVal)	\
    (This)->lpVtbl -> get_PullTaskState(This,pVal)

#define IMcAfeeUpdate2_RunRollbackUpdate(This,ProductID,lUserCookie)	\
    (This)->lpVtbl -> RunRollbackUpdate(This,ProductID,lUserCookie)


#define IMcAfeeUpdate2_RunRemediationUpdate(This,StartBy,lCookie,bQuiet,RemediationURL)	\
    (This)->lpVtbl -> RunRemediationUpdate(This,StartBy,lCookie,bQuiet,RemediationURL)

#define IMcAfeeUpdate2_WaitForRemediationUpdateToComplete(This,lWaitTimeOut,pStatus)	\
    (This)->lpVtbl -> WaitForRemediationUpdateToComplete(This,lWaitTimeOut,pStatus)

#define IMcAfeeUpdate2_SetUpdateTaskOption(This,StartBy,strTaskID,strSectionName,strValueName,strValue)	\
    (This)->lpVtbl -> SetUpdateTaskOption(This,StartBy,strTaskID,strSectionName,strValueName,strValue)

#define IMcAfeeUpdate2_SetUpdateNowOption(This,StartBy,strSectionName,strValueName,strValue)	\
    (This)->lpVtbl -> SetUpdateNowOption(This,StartBy,strSectionName,strValueName,strValue)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IMcAfeeUpdate2_RunRemediationUpdate_Proxy( 
    IMcAfeeUpdate2 * This,
    /* [in] */ const BSTR StartBy,
    /* [in] */ LONG lCookie,
    /* [in] */ BOOL bQuiet,
    /* [in] */ const BSTR RemediationURL);


void __RPC_STUB IMcAfeeUpdate2_RunRemediationUpdate_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IMcAfeeUpdate2_WaitForRemediationUpdateToComplete_Proxy( 
    IMcAfeeUpdate2 * This,
    /* [in] */ LONG lWaitTimeOut,
    /* [out] */ LONG *pStatus);


void __RPC_STUB IMcAfeeUpdate2_WaitForRemediationUpdateToComplete_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IMcAfeeUpdate2_SetUpdateTaskOption_Proxy( 
    IMcAfeeUpdate2 * This,
    /* [in] */ const BSTR StartBy,
    /* [in] */ const BSTR strTaskID,
    /* [in] */ const BSTR strSectionName,
    /* [in] */ const BSTR strValueName,
    /* [in] */ const BSTR strValue);


void __RPC_STUB IMcAfeeUpdate2_SetUpdateTaskOption_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IMcAfeeUpdate2_SetUpdateNowOption_Proxy( 
    IMcAfeeUpdate2 * This,
    /* [in] */ const BSTR StartBy,
    /* [in] */ const BSTR strSectionName,
    /* [in] */ const BSTR strValueName,
    /* [in] */ const BSTR strValue);


void __RPC_STUB IMcAfeeUpdate2_SetUpdateNowOption_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IMcAfeeUpdate2_INTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_McAfeeUpdate;

#ifdef __cplusplus

class DECLSPEC_UUID("6E389062-C540-4145-96B9-B8745CF7D856")
McAfeeUpdate;
#endif
#endif /* __UPDATESUBSYSLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long *, BSTR * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


