

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 6.00.0361 */
/* at Fri Dec 01 20:32:26 2006
 */
/* Compiler settings for .\SecureFrameworkFactory.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
//@@MIDL_FILE_HEADING(  )

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __SecureFrameworkFactory_h__
#define __SecureFrameworkFactory_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IFrameworkFactory_FWD_DEFINED__
#define __IFrameworkFactory_FWD_DEFINED__
typedef interface IFrameworkFactory IFrameworkFactory;
#endif 	/* __IFrameworkFactory_FWD_DEFINED__ */


#ifndef __IFrameworkFactory2_FWD_DEFINED__
#define __IFrameworkFactory2_FWD_DEFINED__
typedef interface IFrameworkFactory2 IFrameworkFactory2;
#endif 	/* __IFrameworkFactory2_FWD_DEFINED__ */


#ifndef __FrameworkFactory_FWD_DEFINED__
#define __FrameworkFactory_FWD_DEFINED__

#ifdef __cplusplus
typedef class FrameworkFactory FrameworkFactory;
#else
typedef struct FrameworkFactory FrameworkFactory;
#endif /* __cplusplus */

#endif 	/* __FrameworkFactory_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 

void * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void * ); 

#ifndef __IFrameworkFactory_INTERFACE_DEFINED__
#define __IFrameworkFactory_INTERFACE_DEFINED__

/* interface IFrameworkFactory */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IFrameworkFactory;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("026F9603-6243-427D-B740-08D11BC5A392")
    IFrameworkFactory : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetSubSystemInterface( 
            /* [in] */ const BSTR SubSystemID,
            /* [in] */ const BSTR InterfaceID,
            /* [out] */ IUnknown **ppSubSysInterface) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetCookie( 
            /* [out] */ long *pdwCookie) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE RegisterFrameworkCallback( 
            /* [in] */ IUnknown *pFrameworkCallback) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE NotifyFrameworkAvailable( 
            /* [in] */ IUnknown *pFrameworkCallback) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IFrameworkFactoryVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IFrameworkFactory * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IFrameworkFactory * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IFrameworkFactory * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IFrameworkFactory * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IFrameworkFactory * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IFrameworkFactory * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IFrameworkFactory * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetSubSystemInterface )( 
            IFrameworkFactory * This,
            /* [in] */ const BSTR SubSystemID,
            /* [in] */ const BSTR InterfaceID,
            /* [out] */ IUnknown **ppSubSysInterface);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetCookie )( 
            IFrameworkFactory * This,
            /* [out] */ long *pdwCookie);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RegisterFrameworkCallback )( 
            IFrameworkFactory * This,
            /* [in] */ IUnknown *pFrameworkCallback);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *NotifyFrameworkAvailable )( 
            IFrameworkFactory * This,
            /* [in] */ IUnknown *pFrameworkCallback);
        
        END_INTERFACE
    } IFrameworkFactoryVtbl;

    interface IFrameworkFactory
    {
        CONST_VTBL struct IFrameworkFactoryVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IFrameworkFactory_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IFrameworkFactory_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IFrameworkFactory_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IFrameworkFactory_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IFrameworkFactory_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IFrameworkFactory_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IFrameworkFactory_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IFrameworkFactory_GetSubSystemInterface(This,SubSystemID,InterfaceID,ppSubSysInterface)	\
    (This)->lpVtbl -> GetSubSystemInterface(This,SubSystemID,InterfaceID,ppSubSysInterface)

#define IFrameworkFactory_GetCookie(This,pdwCookie)	\
    (This)->lpVtbl -> GetCookie(This,pdwCookie)

#define IFrameworkFactory_RegisterFrameworkCallback(This,pFrameworkCallback)	\
    (This)->lpVtbl -> RegisterFrameworkCallback(This,pFrameworkCallback)

#define IFrameworkFactory_NotifyFrameworkAvailable(This,pFrameworkCallback)	\
    (This)->lpVtbl -> NotifyFrameworkAvailable(This,pFrameworkCallback)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IFrameworkFactory_GetSubSystemInterface_Proxy( 
    IFrameworkFactory * This,
    /* [in] */ const BSTR SubSystemID,
    /* [in] */ const BSTR InterfaceID,
    /* [out] */ IUnknown **ppSubSysInterface);


void __RPC_STUB IFrameworkFactory_GetSubSystemInterface_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IFrameworkFactory_GetCookie_Proxy( 
    IFrameworkFactory * This,
    /* [out] */ long *pdwCookie);


void __RPC_STUB IFrameworkFactory_GetCookie_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IFrameworkFactory_RegisterFrameworkCallback_Proxy( 
    IFrameworkFactory * This,
    /* [in] */ IUnknown *pFrameworkCallback);


void __RPC_STUB IFrameworkFactory_RegisterFrameworkCallback_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IFrameworkFactory_NotifyFrameworkAvailable_Proxy( 
    IFrameworkFactory * This,
    /* [in] */ IUnknown *pFrameworkCallback);


void __RPC_STUB IFrameworkFactory_NotifyFrameworkAvailable_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IFrameworkFactory_INTERFACE_DEFINED__ */


#ifndef __IFrameworkFactory2_INTERFACE_DEFINED__
#define __IFrameworkFactory2_INTERFACE_DEFINED__

/* interface IFrameworkFactory2 */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IFrameworkFactory2;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("DFF0AB5E-D44F-4709-A977-8B77D31DA452")
    IFrameworkFactory2 : public IFrameworkFactory
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE DestroyCookiePermanently( 
            /* [in] */ long dwCookie) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IFrameworkFactory2Vtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IFrameworkFactory2 * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IFrameworkFactory2 * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IFrameworkFactory2 * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IFrameworkFactory2 * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IFrameworkFactory2 * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IFrameworkFactory2 * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IFrameworkFactory2 * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetSubSystemInterface )( 
            IFrameworkFactory2 * This,
            /* [in] */ const BSTR SubSystemID,
            /* [in] */ const BSTR InterfaceID,
            /* [out] */ IUnknown **ppSubSysInterface);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetCookie )( 
            IFrameworkFactory2 * This,
            /* [out] */ long *pdwCookie);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RegisterFrameworkCallback )( 
            IFrameworkFactory2 * This,
            /* [in] */ IUnknown *pFrameworkCallback);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *NotifyFrameworkAvailable )( 
            IFrameworkFactory2 * This,
            /* [in] */ IUnknown *pFrameworkCallback);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *DestroyCookiePermanently )( 
            IFrameworkFactory2 * This,
            /* [in] */ long dwCookie);
        
        END_INTERFACE
    } IFrameworkFactory2Vtbl;

    interface IFrameworkFactory2
    {
        CONST_VTBL struct IFrameworkFactory2Vtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IFrameworkFactory2_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IFrameworkFactory2_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IFrameworkFactory2_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IFrameworkFactory2_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IFrameworkFactory2_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IFrameworkFactory2_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IFrameworkFactory2_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IFrameworkFactory2_GetSubSystemInterface(This,SubSystemID,InterfaceID,ppSubSysInterface)	\
    (This)->lpVtbl -> GetSubSystemInterface(This,SubSystemID,InterfaceID,ppSubSysInterface)

#define IFrameworkFactory2_GetCookie(This,pdwCookie)	\
    (This)->lpVtbl -> GetCookie(This,pdwCookie)

#define IFrameworkFactory2_RegisterFrameworkCallback(This,pFrameworkCallback)	\
    (This)->lpVtbl -> RegisterFrameworkCallback(This,pFrameworkCallback)

#define IFrameworkFactory2_NotifyFrameworkAvailable(This,pFrameworkCallback)	\
    (This)->lpVtbl -> NotifyFrameworkAvailable(This,pFrameworkCallback)


#define IFrameworkFactory2_DestroyCookiePermanently(This,dwCookie)	\
    (This)->lpVtbl -> DestroyCookiePermanently(This,dwCookie)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IFrameworkFactory2_DestroyCookiePermanently_Proxy( 
    IFrameworkFactory2 * This,
    /* [in] */ long dwCookie);


void __RPC_STUB IFrameworkFactory2_DestroyCookiePermanently_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IFrameworkFactory2_INTERFACE_DEFINED__ */



#ifndef __SECUREFRAMEWORKFACTORYLib_LIBRARY_DEFINED__
#define __SECUREFRAMEWORKFACTORYLib_LIBRARY_DEFINED__

/* library SECUREFRAMEWORKFACTORYLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_SECUREFRAMEWORKFACTORYLib;

EXTERN_C const CLSID CLSID_FrameworkFactory;

#ifdef __cplusplus

class DECLSPEC_UUID("50CCECAC-7286-4CA0-9AD0-E309A2318482")
FrameworkFactory;
#endif
#endif /* __SECUREFRAMEWORKFACTORYLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long *, BSTR * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


