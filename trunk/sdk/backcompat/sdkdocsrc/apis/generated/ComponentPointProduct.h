

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 6.00.0361 */
/* at Fri Dec 01 20:35:09 2006
 */
/* Compiler settings for .\ComponentPointProduct.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
//@@MIDL_FILE_HEADING(  )

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __ComponentPointProduct_h__
#define __ComponentPointProduct_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IMcAfeePointProduct_FWD_DEFINED__
#define __IMcAfeePointProduct_FWD_DEFINED__
typedef interface IMcAfeePointProduct IMcAfeePointProduct;
#endif 	/* __IMcAfeePointProduct_FWD_DEFINED__ */


#ifndef __McAfeePointProduct_FWD_DEFINED__
#define __McAfeePointProduct_FWD_DEFINED__

#ifdef __cplusplus
typedef class McAfeePointProduct McAfeePointProduct;
#else
typedef struct McAfeePointProduct McAfeePointProduct;
#endif /* __cplusplus */

#endif 	/* __McAfeePointProduct_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 

void * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void * ); 

#ifndef __IMcAfeePointProduct_INTERFACE_DEFINED__
#define __IMcAfeePointProduct_INTERFACE_DEFINED__

/* interface IMcAfeePointProduct */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IMcAfeePointProduct;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("520BF1B9-481F-4011-A31C-493A64BF250D")
    IMcAfeePointProduct : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE UpdateNotify( 
            /* [in] */ const BSTR UpdateType,
            /* [in] */ const BSTR ExtraInfo,
            /* [in] */ const BSTR Message,
            int *pState) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetProductInfo( 
            /* [in] */ const BSTR UpdateType,
            /* [in] */ const BSTR InfoType,
            /* [out] */ VARIANT *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetProductInfo( 
            /* [in] */ const BSTR UpdateType,
            /* [in] */ const BSTR InfoType,
            /* [in] */ VARIANT Value) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IMcAfeePointProductVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IMcAfeePointProduct * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IMcAfeePointProduct * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IMcAfeePointProduct * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IMcAfeePointProduct * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IMcAfeePointProduct * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IMcAfeePointProduct * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IMcAfeePointProduct * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *UpdateNotify )( 
            IMcAfeePointProduct * This,
            /* [in] */ const BSTR UpdateType,
            /* [in] */ const BSTR ExtraInfo,
            /* [in] */ const BSTR Message,
            int *pState);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetProductInfo )( 
            IMcAfeePointProduct * This,
            /* [in] */ const BSTR UpdateType,
            /* [in] */ const BSTR InfoType,
            /* [out] */ VARIANT *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetProductInfo )( 
            IMcAfeePointProduct * This,
            /* [in] */ const BSTR UpdateType,
            /* [in] */ const BSTR InfoType,
            /* [in] */ VARIANT Value);
        
        END_INTERFACE
    } IMcAfeePointProductVtbl;

    interface IMcAfeePointProduct
    {
        CONST_VTBL struct IMcAfeePointProductVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IMcAfeePointProduct_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IMcAfeePointProduct_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IMcAfeePointProduct_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IMcAfeePointProduct_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IMcAfeePointProduct_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IMcAfeePointProduct_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IMcAfeePointProduct_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IMcAfeePointProduct_UpdateNotify(This,UpdateType,ExtraInfo,Message,pState)	\
    (This)->lpVtbl -> UpdateNotify(This,UpdateType,ExtraInfo,Message,pState)

#define IMcAfeePointProduct_GetProductInfo(This,UpdateType,InfoType,pVal)	\
    (This)->lpVtbl -> GetProductInfo(This,UpdateType,InfoType,pVal)

#define IMcAfeePointProduct_SetProductInfo(This,UpdateType,InfoType,Value)	\
    (This)->lpVtbl -> SetProductInfo(This,UpdateType,InfoType,Value)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IMcAfeePointProduct_UpdateNotify_Proxy( 
    IMcAfeePointProduct * This,
    /* [in] */ const BSTR UpdateType,
    /* [in] */ const BSTR ExtraInfo,
    /* [in] */ const BSTR Message,
    int *pState);


void __RPC_STUB IMcAfeePointProduct_UpdateNotify_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IMcAfeePointProduct_GetProductInfo_Proxy( 
    IMcAfeePointProduct * This,
    /* [in] */ const BSTR UpdateType,
    /* [in] */ const BSTR InfoType,
    /* [out] */ VARIANT *pVal);


void __RPC_STUB IMcAfeePointProduct_GetProductInfo_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IMcAfeePointProduct_SetProductInfo_Proxy( 
    IMcAfeePointProduct * This,
    /* [in] */ const BSTR UpdateType,
    /* [in] */ const BSTR InfoType,
    /* [in] */ VARIANT Value);


void __RPC_STUB IMcAfeePointProduct_SetProductInfo_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IMcAfeePointProduct_INTERFACE_DEFINED__ */



#ifndef __COMPONENTPOINTPRODUCTLib_LIBRARY_DEFINED__
#define __COMPONENTPOINTPRODUCTLib_LIBRARY_DEFINED__

/* library COMPONENTPOINTPRODUCTLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_COMPONENTPOINTPRODUCTLib;

EXTERN_C const CLSID CLSID_McAfeePointProduct;

#ifdef __cplusplus

class DECLSPEC_UUID("A979026A-78D0-45D4-85E5-FE7E386C2CAF")
McAfeePointProduct;
#endif
#endif /* __COMPONENTPOINTPRODUCTLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long *, BSTR * ); 

unsigned long             __RPC_USER  VARIANT_UserSize(     unsigned long *, unsigned long            , VARIANT * ); 
unsigned char * __RPC_USER  VARIANT_UserMarshal(  unsigned long *, unsigned char *, VARIANT * ); 
unsigned char * __RPC_USER  VARIANT_UserUnmarshal(unsigned long *, unsigned char *, VARIANT * ); 
void                      __RPC_USER  VARIANT_UserFree(     unsigned long *, VARIANT * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


