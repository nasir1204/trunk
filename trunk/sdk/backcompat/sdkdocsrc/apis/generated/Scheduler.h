

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 6.00.0361 */
/* at Fri Dec 01 20:33:08 2006
 */
/* Compiler settings for .\Scheduler.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
//@@MIDL_FILE_HEADING(  )

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __Scheduler_h__
#define __Scheduler_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __ISchedule_FWD_DEFINED__
#define __ISchedule_FWD_DEFINED__
typedef interface ISchedule ISchedule;
#endif 	/* __ISchedule_FWD_DEFINED__ */


#ifndef ___IScheduleEvents_FWD_DEFINED__
#define ___IScheduleEvents_FWD_DEFINED__
typedef interface _IScheduleEvents _IScheduleEvents;
#endif 	/* ___IScheduleEvents_FWD_DEFINED__ */


#ifndef __ITask_FWD_DEFINED__
#define __ITask_FWD_DEFINED__
typedef interface ITask ITask;
#endif 	/* __ITask_FWD_DEFINED__ */


#ifndef __IEnumTask_FWD_DEFINED__
#define __IEnumTask_FWD_DEFINED__
typedef interface IEnumTask IEnumTask;
#endif 	/* __IEnumTask_FWD_DEFINED__ */


#ifndef __ITrigger_FWD_DEFINED__
#define __ITrigger_FWD_DEFINED__
typedef interface ITrigger ITrigger;
#endif 	/* __ITrigger_FWD_DEFINED__ */


#ifndef __Schedule_FWD_DEFINED__
#define __Schedule_FWD_DEFINED__

#ifdef __cplusplus
typedef class Schedule Schedule;
#else
typedef struct Schedule Schedule;
#endif /* __cplusplus */

#endif 	/* __Schedule_FWD_DEFINED__ */


#ifndef __Task_FWD_DEFINED__
#define __Task_FWD_DEFINED__

#ifdef __cplusplus
typedef class Task Task;
#else
typedef struct Task Task;
#endif /* __cplusplus */

#endif 	/* __Task_FWD_DEFINED__ */


#ifndef __EnumTask_FWD_DEFINED__
#define __EnumTask_FWD_DEFINED__

#ifdef __cplusplus
typedef class EnumTask EnumTask;
#else
typedef struct EnumTask EnumTask;
#endif /* __cplusplus */

#endif 	/* __EnumTask_FWD_DEFINED__ */


#ifndef __Trigger_FWD_DEFINED__
#define __Trigger_FWD_DEFINED__

#ifdef __cplusplus
typedef class Trigger Trigger;
#else
typedef struct Trigger Trigger;
#endif /* __cplusplus */

#endif 	/* __Trigger_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 

void * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void * ); 

/* interface __MIDL_itf_Scheduler_0000 */
/* [local] */ 

typedef /* [public][public][public] */ 
enum __MIDL___MIDL_itf_Scheduler_0000_0001
    {	stypeDaily	= 0,
	stypeWeekly	= 0x1,
	stypeMonthly	= 0x2,
	stypeOnce	= 0x3,
	stypeAtStartup	= 0x4,
	stypeAtLogon	= 0x5,
	stypeWhenIdle	= 0x6,
	stypeImmediate	= 0x7,
	stypeDialup	= 0x8,
	stypeAll	= 0xff
    } 	SCHEDULETYPE;

typedef enum __MIDL___MIDL_itf_Scheduler_0000_0001 *PSCHEDULETYPE;

typedef /* [public][public][public] */ 
enum __MIDL___MIDL_itf_Scheduler_0000_0002
    {	moptionDayOfMonth	= 0,
	moptionDayOfWeek	= 0x1
    } 	MONTHOPTION;

typedef /* [public] */ 
enum __MIDL___MIDL_itf_Scheduler_0000_0003
    {	roptionRepeatInHours	= 0,
	roptionRepeatInMinutes	= 0x1
    } 	REPEATOPTION;

typedef /* [public] */ 
enum __MIDL___MIDL_itf_Scheduler_0000_0004
    {	uoptionUntilTime	= 0,
	uoptionUntilDuration	= 0x1
    } 	UNTILOPTION;

typedef struct _DAILY
    {
    long nRepeatDays;
    } 	DAILY;

typedef struct _WEEKLY
    {
    long nRepeatWeeks;
    long maskDaysOfWeek;
    } 	WEEKLY;

typedef struct _MONTHLYDATE
    {
    MONTHOPTION eMonthlyOption;
    long nDayNumOfMonth;
    long nWeekNumOfMonth;
    long nDayOfWeek;
    long maskMonthsOfYear;
    } 	MONTHLYDATE;

typedef struct _IDLE
    {
    long nIdleMinutes;
    } 	IDLE;

typedef struct _SCHEDULETYPE_STRCT
    {
    DAILY Daily;
    WEEKLY Weekly;
    MONTHLYDATE MonthlyDate;
    IDLE Idle;
    } 	SCHEDULETYPE_STRCT;

typedef 
enum tagDaysOfWeek
    {	daySun	= 0x1,
	dayMon	= 0x2,
	dayTue	= 0x4,
	dayWed	= 0x8,
	dayThu	= 0x10,
	dayFri	= 0x20,
	daySat	= 0x40,
	dayAll	= 0xff
    } 	DAYOFWEEK;

typedef /* [public] */ 
enum __MIDL___MIDL_itf_Scheduler_0000_0005
    {	monthJan	= 0x1,
	monthFeb	= 0x2,
	monthMar	= 0x4,
	monthApr	= 0x8,
	monthMay	= 0x10,
	monthJun	= 0x20,
	monthJul	= 0x40,
	monthAug	= 0x80,
	monthSep	= 0x100,
	monthOct	= 0x200,
	monthNov	= 0x400,
	monthDec	= 0x800,
	monthAll	= 0xfff
    } 	MONTHOFYEAR;

typedef /* [public] */ 
enum __MIDL___MIDL_itf_Scheduler_0000_0006
    {	tfDisabled	= 0x1,
	tfDeleteWhenFinished	= 0x2
    } 	SCHEDSETTINGS;

typedef /* [public] */ 
enum __MIDL___MIDL_itf_Scheduler_0000_0007
    {	eTaskHasFailed	= -1,
	eTaskIsStillRunning	= eTaskHasFailed + 1,
	eTaskIsSuccessful	= eTaskIsStillRunning + 1,
	eTaskStopedByScheduler	= eTaskIsSuccessful + 1,
	eTaskToBeInvokedByScheduler	= eTaskStopedByScheduler + 1,
	eTaskUnknown	= eTaskToBeInvokedByScheduler + 1,
	eFailedToGetStatus	= eTaskUnknown + 1,
	eProductManagerUnavailable	= eFailedToGetStatus + 1
    } 	TASKSTATUS;






extern RPC_IF_HANDLE __MIDL_itf_Scheduler_0000_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_Scheduler_0000_v0_0_s_ifspec;

#ifndef __ISchedule_INTERFACE_DEFINED__
#define __ISchedule_INTERFACE_DEFINED__

/* interface ISchedule */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_ISchedule;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("846BEF02-7F37-43B4-80DA-C763F782EE93")
    ISchedule : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE AddTask( 
            /* [in] */ BSTR bstrTaskGUID) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE CreateTask( 
            /* [out][in] */ BSTR *pbstrTaskGUID,
            /* [retval][out] */ ITask **ppITask) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetTask( 
            /* [in] */ BSTR bstrTaskGUID,
            /* [retval][out] */ ITask **ppITask) = 0;
        
        virtual /* [hidden][helpstring][id] */ HRESULT STDMETHODCALLTYPE FreeTask( 
            /* [in] */ BSTR bstrTaskGUID) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE DeleteTask( 
            /* [in] */ BSTR bstrTaskGUID,
            /* [in] */ BSTR bstrCreatorSoftwareID) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE EnumAllTask( 
            /* [retval][out] */ IEnumTask **ppIEnumTask) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE CreatorEnumTask( 
            /* [in] */ BSTR bstrTaskCreatorSoftwareID,
            /* [retval][out] */ IEnumTask **ppIEnumTask) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ModifyTask( 
            /* [in] */ BSTR bstrTaskGUID,
            /* [in] */ BSTR bstrCreatorSoftwareID,
            /* [retval][out] */ ITask **ppITask) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE AddCmdLineTask( 
            /* [in] */ BSTR bstrTaskGUID,
            /* [in] */ BSTR bstrApplicationName,
            /* [in] */ BSTR bstrCommandLine) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetTaskStatus( 
            /* [in] */ BSTR bstrTaskGUID,
            /* [out] */ int *pStatus) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE RunTaskNow( 
            /* [in] */ BSTR bstrTaskGUID,
            /* [in] */ BSTR bstrCreatorSoftwareID) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE StopTaskNow( 
            /* [in] */ BSTR bstrTaskGUID,
            /* [in] */ BSTR bstrCreatorSoftwareID) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE RunTaskAfterReboot( 
            /* [in] */ BSTR bstrTaskGUID) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetNextRunTime( 
            /* [in] */ BSTR bstrTaskGUID,
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetLastRunTime( 
            /* [in] */ BSTR bstrTaskGUID,
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE OwnerEnumTask( 
            /* [in] */ BSTR bstrTaskOwnerSoftwareID,
            /* [retval][out] */ IEnumTask **ppIEnumTask) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE RepeatTask( 
            /* [in] */ BSTR bstrTaskGUID,
            /* [in] */ BSTR bstrStartLocalTime,
            /* [in] */ long RepeatNumber,
            /* [in] */ long IntervalMinute) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetNextRunTimeAsStr( 
            /* [in] */ BSTR bstrTaskGUID,
            /* [out] */ BSTR *pbstrNextRunTime) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IScheduleVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ISchedule * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ISchedule * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ISchedule * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ISchedule * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ISchedule * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ISchedule * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ISchedule * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *AddTask )( 
            ISchedule * This,
            /* [in] */ BSTR bstrTaskGUID);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CreateTask )( 
            ISchedule * This,
            /* [out][in] */ BSTR *pbstrTaskGUID,
            /* [retval][out] */ ITask **ppITask);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetTask )( 
            ISchedule * This,
            /* [in] */ BSTR bstrTaskGUID,
            /* [retval][out] */ ITask **ppITask);
        
        /* [hidden][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *FreeTask )( 
            ISchedule * This,
            /* [in] */ BSTR bstrTaskGUID);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *DeleteTask )( 
            ISchedule * This,
            /* [in] */ BSTR bstrTaskGUID,
            /* [in] */ BSTR bstrCreatorSoftwareID);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *EnumAllTask )( 
            ISchedule * This,
            /* [retval][out] */ IEnumTask **ppIEnumTask);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CreatorEnumTask )( 
            ISchedule * This,
            /* [in] */ BSTR bstrTaskCreatorSoftwareID,
            /* [retval][out] */ IEnumTask **ppIEnumTask);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ModifyTask )( 
            ISchedule * This,
            /* [in] */ BSTR bstrTaskGUID,
            /* [in] */ BSTR bstrCreatorSoftwareID,
            /* [retval][out] */ ITask **ppITask);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *AddCmdLineTask )( 
            ISchedule * This,
            /* [in] */ BSTR bstrTaskGUID,
            /* [in] */ BSTR bstrApplicationName,
            /* [in] */ BSTR bstrCommandLine);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetTaskStatus )( 
            ISchedule * This,
            /* [in] */ BSTR bstrTaskGUID,
            /* [out] */ int *pStatus);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RunTaskNow )( 
            ISchedule * This,
            /* [in] */ BSTR bstrTaskGUID,
            /* [in] */ BSTR bstrCreatorSoftwareID);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *StopTaskNow )( 
            ISchedule * This,
            /* [in] */ BSTR bstrTaskGUID,
            /* [in] */ BSTR bstrCreatorSoftwareID);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RunTaskAfterReboot )( 
            ISchedule * This,
            /* [in] */ BSTR bstrTaskGUID);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetNextRunTime )( 
            ISchedule * This,
            /* [in] */ BSTR bstrTaskGUID,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetLastRunTime )( 
            ISchedule * This,
            /* [in] */ BSTR bstrTaskGUID,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *OwnerEnumTask )( 
            ISchedule * This,
            /* [in] */ BSTR bstrTaskOwnerSoftwareID,
            /* [retval][out] */ IEnumTask **ppIEnumTask);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RepeatTask )( 
            ISchedule * This,
            /* [in] */ BSTR bstrTaskGUID,
            /* [in] */ BSTR bstrStartLocalTime,
            /* [in] */ long RepeatNumber,
            /* [in] */ long IntervalMinute);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetNextRunTimeAsStr )( 
            ISchedule * This,
            /* [in] */ BSTR bstrTaskGUID,
            /* [out] */ BSTR *pbstrNextRunTime);
        
        END_INTERFACE
    } IScheduleVtbl;

    interface ISchedule
    {
        CONST_VTBL struct IScheduleVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ISchedule_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define ISchedule_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define ISchedule_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define ISchedule_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define ISchedule_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define ISchedule_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define ISchedule_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define ISchedule_AddTask(This,bstrTaskGUID)	\
    (This)->lpVtbl -> AddTask(This,bstrTaskGUID)

#define ISchedule_CreateTask(This,pbstrTaskGUID,ppITask)	\
    (This)->lpVtbl -> CreateTask(This,pbstrTaskGUID,ppITask)

#define ISchedule_GetTask(This,bstrTaskGUID,ppITask)	\
    (This)->lpVtbl -> GetTask(This,bstrTaskGUID,ppITask)

#define ISchedule_FreeTask(This,bstrTaskGUID)	\
    (This)->lpVtbl -> FreeTask(This,bstrTaskGUID)

#define ISchedule_DeleteTask(This,bstrTaskGUID,bstrCreatorSoftwareID)	\
    (This)->lpVtbl -> DeleteTask(This,bstrTaskGUID,bstrCreatorSoftwareID)

#define ISchedule_EnumAllTask(This,ppIEnumTask)	\
    (This)->lpVtbl -> EnumAllTask(This,ppIEnumTask)

#define ISchedule_CreatorEnumTask(This,bstrTaskCreatorSoftwareID,ppIEnumTask)	\
    (This)->lpVtbl -> CreatorEnumTask(This,bstrTaskCreatorSoftwareID,ppIEnumTask)

#define ISchedule_ModifyTask(This,bstrTaskGUID,bstrCreatorSoftwareID,ppITask)	\
    (This)->lpVtbl -> ModifyTask(This,bstrTaskGUID,bstrCreatorSoftwareID,ppITask)

#define ISchedule_AddCmdLineTask(This,bstrTaskGUID,bstrApplicationName,bstrCommandLine)	\
    (This)->lpVtbl -> AddCmdLineTask(This,bstrTaskGUID,bstrApplicationName,bstrCommandLine)

#define ISchedule_GetTaskStatus(This,bstrTaskGUID,pStatus)	\
    (This)->lpVtbl -> GetTaskStatus(This,bstrTaskGUID,pStatus)

#define ISchedule_RunTaskNow(This,bstrTaskGUID,bstrCreatorSoftwareID)	\
    (This)->lpVtbl -> RunTaskNow(This,bstrTaskGUID,bstrCreatorSoftwareID)

#define ISchedule_StopTaskNow(This,bstrTaskGUID,bstrCreatorSoftwareID)	\
    (This)->lpVtbl -> StopTaskNow(This,bstrTaskGUID,bstrCreatorSoftwareID)

#define ISchedule_RunTaskAfterReboot(This,bstrTaskGUID)	\
    (This)->lpVtbl -> RunTaskAfterReboot(This,bstrTaskGUID)

#define ISchedule_GetNextRunTime(This,bstrTaskGUID,pVal)	\
    (This)->lpVtbl -> GetNextRunTime(This,bstrTaskGUID,pVal)

#define ISchedule_GetLastRunTime(This,bstrTaskGUID,pVal)	\
    (This)->lpVtbl -> GetLastRunTime(This,bstrTaskGUID,pVal)

#define ISchedule_OwnerEnumTask(This,bstrTaskOwnerSoftwareID,ppIEnumTask)	\
    (This)->lpVtbl -> OwnerEnumTask(This,bstrTaskOwnerSoftwareID,ppIEnumTask)

#define ISchedule_RepeatTask(This,bstrTaskGUID,bstrStartLocalTime,RepeatNumber,IntervalMinute)	\
    (This)->lpVtbl -> RepeatTask(This,bstrTaskGUID,bstrStartLocalTime,RepeatNumber,IntervalMinute)

#define ISchedule_GetNextRunTimeAsStr(This,bstrTaskGUID,pbstrNextRunTime)	\
    (This)->lpVtbl -> GetNextRunTimeAsStr(This,bstrTaskGUID,pbstrNextRunTime)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ISchedule_AddTask_Proxy( 
    ISchedule * This,
    /* [in] */ BSTR bstrTaskGUID);


void __RPC_STUB ISchedule_AddTask_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ISchedule_CreateTask_Proxy( 
    ISchedule * This,
    /* [out][in] */ BSTR *pbstrTaskGUID,
    /* [retval][out] */ ITask **ppITask);


void __RPC_STUB ISchedule_CreateTask_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ISchedule_GetTask_Proxy( 
    ISchedule * This,
    /* [in] */ BSTR bstrTaskGUID,
    /* [retval][out] */ ITask **ppITask);


void __RPC_STUB ISchedule_GetTask_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [hidden][helpstring][id] */ HRESULT STDMETHODCALLTYPE ISchedule_FreeTask_Proxy( 
    ISchedule * This,
    /* [in] */ BSTR bstrTaskGUID);


void __RPC_STUB ISchedule_FreeTask_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ISchedule_DeleteTask_Proxy( 
    ISchedule * This,
    /* [in] */ BSTR bstrTaskGUID,
    /* [in] */ BSTR bstrCreatorSoftwareID);


void __RPC_STUB ISchedule_DeleteTask_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ISchedule_EnumAllTask_Proxy( 
    ISchedule * This,
    /* [retval][out] */ IEnumTask **ppIEnumTask);


void __RPC_STUB ISchedule_EnumAllTask_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ISchedule_CreatorEnumTask_Proxy( 
    ISchedule * This,
    /* [in] */ BSTR bstrTaskCreatorSoftwareID,
    /* [retval][out] */ IEnumTask **ppIEnumTask);


void __RPC_STUB ISchedule_CreatorEnumTask_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ISchedule_ModifyTask_Proxy( 
    ISchedule * This,
    /* [in] */ BSTR bstrTaskGUID,
    /* [in] */ BSTR bstrCreatorSoftwareID,
    /* [retval][out] */ ITask **ppITask);


void __RPC_STUB ISchedule_ModifyTask_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ISchedule_AddCmdLineTask_Proxy( 
    ISchedule * This,
    /* [in] */ BSTR bstrTaskGUID,
    /* [in] */ BSTR bstrApplicationName,
    /* [in] */ BSTR bstrCommandLine);


void __RPC_STUB ISchedule_AddCmdLineTask_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ISchedule_GetTaskStatus_Proxy( 
    ISchedule * This,
    /* [in] */ BSTR bstrTaskGUID,
    /* [out] */ int *pStatus);


void __RPC_STUB ISchedule_GetTaskStatus_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ISchedule_RunTaskNow_Proxy( 
    ISchedule * This,
    /* [in] */ BSTR bstrTaskGUID,
    /* [in] */ BSTR bstrCreatorSoftwareID);


void __RPC_STUB ISchedule_RunTaskNow_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ISchedule_StopTaskNow_Proxy( 
    ISchedule * This,
    /* [in] */ BSTR bstrTaskGUID,
    /* [in] */ BSTR bstrCreatorSoftwareID);


void __RPC_STUB ISchedule_StopTaskNow_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ISchedule_RunTaskAfterReboot_Proxy( 
    ISchedule * This,
    /* [in] */ BSTR bstrTaskGUID);


void __RPC_STUB ISchedule_RunTaskAfterReboot_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ISchedule_GetNextRunTime_Proxy( 
    ISchedule * This,
    /* [in] */ BSTR bstrTaskGUID,
    /* [retval][out] */ long *pVal);


void __RPC_STUB ISchedule_GetNextRunTime_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ISchedule_GetLastRunTime_Proxy( 
    ISchedule * This,
    /* [in] */ BSTR bstrTaskGUID,
    /* [retval][out] */ long *pVal);


void __RPC_STUB ISchedule_GetLastRunTime_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ISchedule_OwnerEnumTask_Proxy( 
    ISchedule * This,
    /* [in] */ BSTR bstrTaskOwnerSoftwareID,
    /* [retval][out] */ IEnumTask **ppIEnumTask);


void __RPC_STUB ISchedule_OwnerEnumTask_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ISchedule_RepeatTask_Proxy( 
    ISchedule * This,
    /* [in] */ BSTR bstrTaskGUID,
    /* [in] */ BSTR bstrStartLocalTime,
    /* [in] */ long RepeatNumber,
    /* [in] */ long IntervalMinute);


void __RPC_STUB ISchedule_RepeatTask_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ISchedule_GetNextRunTimeAsStr_Proxy( 
    ISchedule * This,
    /* [in] */ BSTR bstrTaskGUID,
    /* [out] */ BSTR *pbstrNextRunTime);


void __RPC_STUB ISchedule_GetNextRunTimeAsStr_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __ISchedule_INTERFACE_DEFINED__ */



#ifndef __SCHEDULERLib_LIBRARY_DEFINED__
#define __SCHEDULERLib_LIBRARY_DEFINED__

/* library SCHEDULERLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_SCHEDULERLib;

#ifndef ___IScheduleEvents_DISPINTERFACE_DEFINED__
#define ___IScheduleEvents_DISPINTERFACE_DEFINED__

/* dispinterface _IScheduleEvents */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__IScheduleEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("20F0E677-D037-4A07-BCFF-186A825B3E02")
    _IScheduleEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _IScheduleEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _IScheduleEvents * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _IScheduleEvents * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _IScheduleEvents * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _IScheduleEvents * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _IScheduleEvents * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _IScheduleEvents * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _IScheduleEvents * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _IScheduleEventsVtbl;

    interface _IScheduleEvents
    {
        CONST_VTBL struct _IScheduleEventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _IScheduleEvents_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _IScheduleEvents_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _IScheduleEvents_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _IScheduleEvents_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define _IScheduleEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define _IScheduleEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define _IScheduleEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___IScheduleEvents_DISPINTERFACE_DEFINED__ */


#ifndef __ITask_INTERFACE_DEFINED__
#define __ITask_INTERFACE_DEFINED__

/* interface ITask */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_ITask;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("5AA193A7-1A75-4B41-93B9-02503C32A075")
    ITask : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetGUID( 
            /* [retval][out] */ BSTR *pbstrTaskGUID) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetTaskEnabled( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetTaskEnabled( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [hidden][helpstring][id] */ HRESULT STDMETHODCALLTYPE ActivateTask( 
            /* [in] */ unsigned __int64 lCookie,
            /* [in] */ long uType) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetRunIfMissedDelayMins( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetRunIfMissedDelayMins( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetRunIfMissed( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetRunIfMissed( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetTaskPriority( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetTaskPriority( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetStopAfterMinutes( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetStopAfterMinutes( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetPlatforms( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetPlatforms( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetTaskName( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetTaskName( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetTaskVersion( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetCreatorSoftwareID( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetCreatorSoftwareID( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetOwnerSoftwareID( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetOwnerSoftwareID( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetAppConfig( 
            /* [in] */ BSTR bstrSectionName,
            /* [in] */ BSTR bstrSettingName,
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetAppConfig( 
            /* [in] */ BSTR bstrSectionName,
            /* [in] */ BSTR bstrSettingName,
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE RemoveAppConfig( 
            /* [in] */ BSTR bstrSectionName,
            /* [in] */ BSTR bstrSettingName) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetAccountInformation( 
            /* [in] */ BSTR bstrUserName,
            /* [in] */ BSTR bstrDomainName,
            /* [in] */ BSTR bstrPassword) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Save( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetConflicts( 
            /* [out] */ BSTR *pbstrConflicts) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetDependencies( 
            /* [out] */ BSTR *pbstrDependencies) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetTaskType( 
            /* [out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetTaskType( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [hidden][helpstring][id] */ HRESULT STDMETHODCALLTYPE UpdateTask( 
            /* [in] */ IUnknown *pPolicyTask,
            /* [out] */ BOOL *pbUpdated,
            /* [out] */ int *pnUpdatedFiledCount) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetTaskTrigger( 
            /* [retval][out] */ ITrigger **ppTrigger) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetConflicts( 
            /* [in] */ BSTR bstrConflicts) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetDependencies( 
            /* [in] */ BSTR bstrDependencies) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetDialupTriggerEnabled( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetDialupTriggerEnabled( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetIniData( 
            /* [out] */ BSTR *pbstrData) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ITaskVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ITask * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ITask * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ITask * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ITask * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ITask * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ITask * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ITask * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetGUID )( 
            ITask * This,
            /* [retval][out] */ BSTR *pbstrTaskGUID);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetTaskEnabled )( 
            ITask * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetTaskEnabled )( 
            ITask * This,
            /* [in] */ BOOL newVal);
        
        /* [hidden][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ActivateTask )( 
            ITask * This,
            /* [in] */ unsigned __int64 lCookie,
            /* [in] */ long uType);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetRunIfMissedDelayMins )( 
            ITask * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetRunIfMissedDelayMins )( 
            ITask * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetRunIfMissed )( 
            ITask * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetRunIfMissed )( 
            ITask * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetTaskPriority )( 
            ITask * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetTaskPriority )( 
            ITask * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetStopAfterMinutes )( 
            ITask * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetStopAfterMinutes )( 
            ITask * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetPlatforms )( 
            ITask * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetPlatforms )( 
            ITask * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetTaskName )( 
            ITask * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetTaskName )( 
            ITask * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetTaskVersion )( 
            ITask * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetCreatorSoftwareID )( 
            ITask * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetCreatorSoftwareID )( 
            ITask * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetOwnerSoftwareID )( 
            ITask * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetOwnerSoftwareID )( 
            ITask * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetAppConfig )( 
            ITask * This,
            /* [in] */ BSTR bstrSectionName,
            /* [in] */ BSTR bstrSettingName,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetAppConfig )( 
            ITask * This,
            /* [in] */ BSTR bstrSectionName,
            /* [in] */ BSTR bstrSettingName,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RemoveAppConfig )( 
            ITask * This,
            /* [in] */ BSTR bstrSectionName,
            /* [in] */ BSTR bstrSettingName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetAccountInformation )( 
            ITask * This,
            /* [in] */ BSTR bstrUserName,
            /* [in] */ BSTR bstrDomainName,
            /* [in] */ BSTR bstrPassword);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Save )( 
            ITask * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetConflicts )( 
            ITask * This,
            /* [out] */ BSTR *pbstrConflicts);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetDependencies )( 
            ITask * This,
            /* [out] */ BSTR *pbstrDependencies);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetTaskType )( 
            ITask * This,
            /* [out] */ BSTR *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetTaskType )( 
            ITask * This,
            /* [in] */ BSTR newVal);
        
        /* [hidden][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *UpdateTask )( 
            ITask * This,
            /* [in] */ IUnknown *pPolicyTask,
            /* [out] */ BOOL *pbUpdated,
            /* [out] */ int *pnUpdatedFiledCount);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetTaskTrigger )( 
            ITask * This,
            /* [retval][out] */ ITrigger **ppTrigger);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetConflicts )( 
            ITask * This,
            /* [in] */ BSTR bstrConflicts);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetDependencies )( 
            ITask * This,
            /* [in] */ BSTR bstrDependencies);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetDialupTriggerEnabled )( 
            ITask * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetDialupTriggerEnabled )( 
            ITask * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetIniData )( 
            ITask * This,
            /* [out] */ BSTR *pbstrData);
        
        END_INTERFACE
    } ITaskVtbl;

    interface ITask
    {
        CONST_VTBL struct ITaskVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ITask_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define ITask_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define ITask_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define ITask_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define ITask_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define ITask_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define ITask_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define ITask_GetGUID(This,pbstrTaskGUID)	\
    (This)->lpVtbl -> GetGUID(This,pbstrTaskGUID)

#define ITask_GetTaskEnabled(This,pVal)	\
    (This)->lpVtbl -> GetTaskEnabled(This,pVal)

#define ITask_SetTaskEnabled(This,newVal)	\
    (This)->lpVtbl -> SetTaskEnabled(This,newVal)

#define ITask_ActivateTask(This,lCookie,uType)	\
    (This)->lpVtbl -> ActivateTask(This,lCookie,uType)

#define ITask_GetRunIfMissedDelayMins(This,pVal)	\
    (This)->lpVtbl -> GetRunIfMissedDelayMins(This,pVal)

#define ITask_SetRunIfMissedDelayMins(This,newVal)	\
    (This)->lpVtbl -> SetRunIfMissedDelayMins(This,newVal)

#define ITask_GetRunIfMissed(This,pVal)	\
    (This)->lpVtbl -> GetRunIfMissed(This,pVal)

#define ITask_SetRunIfMissed(This,newVal)	\
    (This)->lpVtbl -> SetRunIfMissed(This,newVal)

#define ITask_GetTaskPriority(This,pVal)	\
    (This)->lpVtbl -> GetTaskPriority(This,pVal)

#define ITask_SetTaskPriority(This,newVal)	\
    (This)->lpVtbl -> SetTaskPriority(This,newVal)

#define ITask_GetStopAfterMinutes(This,pVal)	\
    (This)->lpVtbl -> GetStopAfterMinutes(This,pVal)

#define ITask_SetStopAfterMinutes(This,newVal)	\
    (This)->lpVtbl -> SetStopAfterMinutes(This,newVal)

#define ITask_GetPlatforms(This,pVal)	\
    (This)->lpVtbl -> GetPlatforms(This,pVal)

#define ITask_SetPlatforms(This,newVal)	\
    (This)->lpVtbl -> SetPlatforms(This,newVal)

#define ITask_GetTaskName(This,pVal)	\
    (This)->lpVtbl -> GetTaskName(This,pVal)

#define ITask_SetTaskName(This,newVal)	\
    (This)->lpVtbl -> SetTaskName(This,newVal)

#define ITask_GetTaskVersion(This,pVal)	\
    (This)->lpVtbl -> GetTaskVersion(This,pVal)

#define ITask_GetCreatorSoftwareID(This,pVal)	\
    (This)->lpVtbl -> GetCreatorSoftwareID(This,pVal)

#define ITask_SetCreatorSoftwareID(This,newVal)	\
    (This)->lpVtbl -> SetCreatorSoftwareID(This,newVal)

#define ITask_GetOwnerSoftwareID(This,pVal)	\
    (This)->lpVtbl -> GetOwnerSoftwareID(This,pVal)

#define ITask_SetOwnerSoftwareID(This,newVal)	\
    (This)->lpVtbl -> SetOwnerSoftwareID(This,newVal)

#define ITask_GetAppConfig(This,bstrSectionName,bstrSettingName,pVal)	\
    (This)->lpVtbl -> GetAppConfig(This,bstrSectionName,bstrSettingName,pVal)

#define ITask_SetAppConfig(This,bstrSectionName,bstrSettingName,newVal)	\
    (This)->lpVtbl -> SetAppConfig(This,bstrSectionName,bstrSettingName,newVal)

#define ITask_RemoveAppConfig(This,bstrSectionName,bstrSettingName)	\
    (This)->lpVtbl -> RemoveAppConfig(This,bstrSectionName,bstrSettingName)

#define ITask_SetAccountInformation(This,bstrUserName,bstrDomainName,bstrPassword)	\
    (This)->lpVtbl -> SetAccountInformation(This,bstrUserName,bstrDomainName,bstrPassword)

#define ITask_Save(This)	\
    (This)->lpVtbl -> Save(This)

#define ITask_GetConflicts(This,pbstrConflicts)	\
    (This)->lpVtbl -> GetConflicts(This,pbstrConflicts)

#define ITask_GetDependencies(This,pbstrDependencies)	\
    (This)->lpVtbl -> GetDependencies(This,pbstrDependencies)

#define ITask_GetTaskType(This,pVal)	\
    (This)->lpVtbl -> GetTaskType(This,pVal)

#define ITask_SetTaskType(This,newVal)	\
    (This)->lpVtbl -> SetTaskType(This,newVal)

#define ITask_UpdateTask(This,pPolicyTask,pbUpdated,pnUpdatedFiledCount)	\
    (This)->lpVtbl -> UpdateTask(This,pPolicyTask,pbUpdated,pnUpdatedFiledCount)

#define ITask_GetTaskTrigger(This,ppTrigger)	\
    (This)->lpVtbl -> GetTaskTrigger(This,ppTrigger)

#define ITask_SetConflicts(This,bstrConflicts)	\
    (This)->lpVtbl -> SetConflicts(This,bstrConflicts)

#define ITask_SetDependencies(This,bstrDependencies)	\
    (This)->lpVtbl -> SetDependencies(This,bstrDependencies)

#define ITask_GetDialupTriggerEnabled(This,pVal)	\
    (This)->lpVtbl -> GetDialupTriggerEnabled(This,pVal)

#define ITask_SetDialupTriggerEnabled(This,newVal)	\
    (This)->lpVtbl -> SetDialupTriggerEnabled(This,newVal)

#define ITask_GetIniData(This,pbstrData)	\
    (This)->lpVtbl -> GetIniData(This,pbstrData)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ITask_GetGUID_Proxy( 
    ITask * This,
    /* [retval][out] */ BSTR *pbstrTaskGUID);


void __RPC_STUB ITask_GetGUID_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ITask_GetTaskEnabled_Proxy( 
    ITask * This,
    /* [retval][out] */ BOOL *pVal);


void __RPC_STUB ITask_GetTaskEnabled_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ITask_SetTaskEnabled_Proxy( 
    ITask * This,
    /* [in] */ BOOL newVal);


void __RPC_STUB ITask_SetTaskEnabled_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [hidden][helpstring][id] */ HRESULT STDMETHODCALLTYPE ITask_ActivateTask_Proxy( 
    ITask * This,
    /* [in] */ unsigned __int64 lCookie,
    /* [in] */ long uType);


void __RPC_STUB ITask_ActivateTask_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ITask_GetRunIfMissedDelayMins_Proxy( 
    ITask * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB ITask_GetRunIfMissedDelayMins_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ITask_SetRunIfMissedDelayMins_Proxy( 
    ITask * This,
    /* [in] */ long newVal);


void __RPC_STUB ITask_SetRunIfMissedDelayMins_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ITask_GetRunIfMissed_Proxy( 
    ITask * This,
    /* [retval][out] */ BOOL *pVal);


void __RPC_STUB ITask_GetRunIfMissed_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ITask_SetRunIfMissed_Proxy( 
    ITask * This,
    /* [in] */ BOOL newVal);


void __RPC_STUB ITask_SetRunIfMissed_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ITask_GetTaskPriority_Proxy( 
    ITask * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB ITask_GetTaskPriority_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ITask_SetTaskPriority_Proxy( 
    ITask * This,
    /* [in] */ long newVal);


void __RPC_STUB ITask_SetTaskPriority_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ITask_GetStopAfterMinutes_Proxy( 
    ITask * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB ITask_GetStopAfterMinutes_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ITask_SetStopAfterMinutes_Proxy( 
    ITask * This,
    /* [in] */ long newVal);


void __RPC_STUB ITask_SetStopAfterMinutes_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ITask_GetPlatforms_Proxy( 
    ITask * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB ITask_GetPlatforms_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ITask_SetPlatforms_Proxy( 
    ITask * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB ITask_SetPlatforms_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ITask_GetTaskName_Proxy( 
    ITask * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB ITask_GetTaskName_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ITask_SetTaskName_Proxy( 
    ITask * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB ITask_SetTaskName_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ITask_GetTaskVersion_Proxy( 
    ITask * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB ITask_GetTaskVersion_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ITask_GetCreatorSoftwareID_Proxy( 
    ITask * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB ITask_GetCreatorSoftwareID_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ITask_SetCreatorSoftwareID_Proxy( 
    ITask * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB ITask_SetCreatorSoftwareID_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ITask_GetOwnerSoftwareID_Proxy( 
    ITask * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB ITask_GetOwnerSoftwareID_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ITask_SetOwnerSoftwareID_Proxy( 
    ITask * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB ITask_SetOwnerSoftwareID_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ITask_GetAppConfig_Proxy( 
    ITask * This,
    /* [in] */ BSTR bstrSectionName,
    /* [in] */ BSTR bstrSettingName,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB ITask_GetAppConfig_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ITask_SetAppConfig_Proxy( 
    ITask * This,
    /* [in] */ BSTR bstrSectionName,
    /* [in] */ BSTR bstrSettingName,
    /* [in] */ BSTR newVal);


void __RPC_STUB ITask_SetAppConfig_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ITask_RemoveAppConfig_Proxy( 
    ITask * This,
    /* [in] */ BSTR bstrSectionName,
    /* [in] */ BSTR bstrSettingName);


void __RPC_STUB ITask_RemoveAppConfig_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ITask_SetAccountInformation_Proxy( 
    ITask * This,
    /* [in] */ BSTR bstrUserName,
    /* [in] */ BSTR bstrDomainName,
    /* [in] */ BSTR bstrPassword);


void __RPC_STUB ITask_SetAccountInformation_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ITask_Save_Proxy( 
    ITask * This);


void __RPC_STUB ITask_Save_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ITask_GetConflicts_Proxy( 
    ITask * This,
    /* [out] */ BSTR *pbstrConflicts);


void __RPC_STUB ITask_GetConflicts_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ITask_GetDependencies_Proxy( 
    ITask * This,
    /* [out] */ BSTR *pbstrDependencies);


void __RPC_STUB ITask_GetDependencies_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ITask_GetTaskType_Proxy( 
    ITask * This,
    /* [out] */ BSTR *pVal);


void __RPC_STUB ITask_GetTaskType_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ITask_SetTaskType_Proxy( 
    ITask * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB ITask_SetTaskType_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [hidden][helpstring][id] */ HRESULT STDMETHODCALLTYPE ITask_UpdateTask_Proxy( 
    ITask * This,
    /* [in] */ IUnknown *pPolicyTask,
    /* [out] */ BOOL *pbUpdated,
    /* [out] */ int *pnUpdatedFiledCount);


void __RPC_STUB ITask_UpdateTask_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ITask_GetTaskTrigger_Proxy( 
    ITask * This,
    /* [retval][out] */ ITrigger **ppTrigger);


void __RPC_STUB ITask_GetTaskTrigger_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ITask_SetConflicts_Proxy( 
    ITask * This,
    /* [in] */ BSTR bstrConflicts);


void __RPC_STUB ITask_SetConflicts_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ITask_SetDependencies_Proxy( 
    ITask * This,
    /* [in] */ BSTR bstrDependencies);


void __RPC_STUB ITask_SetDependencies_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ITask_GetDialupTriggerEnabled_Proxy( 
    ITask * This,
    /* [retval][out] */ BOOL *pVal);


void __RPC_STUB ITask_GetDialupTriggerEnabled_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ITask_SetDialupTriggerEnabled_Proxy( 
    ITask * This,
    /* [in] */ BOOL newVal);


void __RPC_STUB ITask_SetDialupTriggerEnabled_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ITask_GetIniData_Proxy( 
    ITask * This,
    /* [out] */ BSTR *pbstrData);


void __RPC_STUB ITask_GetIniData_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __ITask_INTERFACE_DEFINED__ */


#ifndef __IEnumTask_INTERFACE_DEFINED__
#define __IEnumTask_INTERFACE_DEFINED__

/* interface IEnumTask */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IEnumTask;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("941A666E-022B-45C1-80F6-32DC82178762")
    IEnumTask : public IDispatch
    {
    public:
        virtual /* [hidden][helpstring][id] */ HRESULT STDMETHODCALLTYPE CreateEnumTaskForCreator( 
            /* [in] */ BSTR bstrTaskCreatorSoftwareID) = 0;
        
        virtual /* [hidden][helpstring][id] */ HRESULT STDMETHODCALLTYPE CreateEnumAllTask( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE FreeEnumObj( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetFirst( 
            /* [retval][out] */ BSTR *pbstrTaskGUID) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetNext( 
            /* [retval][out] */ BSTR *pbstrTaskGUID) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE CreateEnumTaskForOwner( 
            /* [in] */ BSTR bstrTaskOwnerSoftwareID) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEnumTaskVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IEnumTask * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IEnumTask * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IEnumTask * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IEnumTask * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IEnumTask * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IEnumTask * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IEnumTask * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [hidden][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CreateEnumTaskForCreator )( 
            IEnumTask * This,
            /* [in] */ BSTR bstrTaskCreatorSoftwareID);
        
        /* [hidden][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CreateEnumAllTask )( 
            IEnumTask * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *FreeEnumObj )( 
            IEnumTask * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetFirst )( 
            IEnumTask * This,
            /* [retval][out] */ BSTR *pbstrTaskGUID);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetNext )( 
            IEnumTask * This,
            /* [retval][out] */ BSTR *pbstrTaskGUID);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CreateEnumTaskForOwner )( 
            IEnumTask * This,
            /* [in] */ BSTR bstrTaskOwnerSoftwareID);
        
        END_INTERFACE
    } IEnumTaskVtbl;

    interface IEnumTask
    {
        CONST_VTBL struct IEnumTaskVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEnumTask_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEnumTask_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEnumTask_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEnumTask_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IEnumTask_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IEnumTask_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IEnumTask_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IEnumTask_CreateEnumTaskForCreator(This,bstrTaskCreatorSoftwareID)	\
    (This)->lpVtbl -> CreateEnumTaskForCreator(This,bstrTaskCreatorSoftwareID)

#define IEnumTask_CreateEnumAllTask(This)	\
    (This)->lpVtbl -> CreateEnumAllTask(This)

#define IEnumTask_FreeEnumObj(This)	\
    (This)->lpVtbl -> FreeEnumObj(This)

#define IEnumTask_GetFirst(This,pbstrTaskGUID)	\
    (This)->lpVtbl -> GetFirst(This,pbstrTaskGUID)

#define IEnumTask_GetNext(This,pbstrTaskGUID)	\
    (This)->lpVtbl -> GetNext(This,pbstrTaskGUID)

#define IEnumTask_CreateEnumTaskForOwner(This,bstrTaskOwnerSoftwareID)	\
    (This)->lpVtbl -> CreateEnumTaskForOwner(This,bstrTaskOwnerSoftwareID)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [hidden][helpstring][id] */ HRESULT STDMETHODCALLTYPE IEnumTask_CreateEnumTaskForCreator_Proxy( 
    IEnumTask * This,
    /* [in] */ BSTR bstrTaskCreatorSoftwareID);


void __RPC_STUB IEnumTask_CreateEnumTaskForCreator_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [hidden][helpstring][id] */ HRESULT STDMETHODCALLTYPE IEnumTask_CreateEnumAllTask_Proxy( 
    IEnumTask * This);


void __RPC_STUB IEnumTask_CreateEnumAllTask_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IEnumTask_FreeEnumObj_Proxy( 
    IEnumTask * This);


void __RPC_STUB IEnumTask_FreeEnumObj_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IEnumTask_GetFirst_Proxy( 
    IEnumTask * This,
    /* [retval][out] */ BSTR *pbstrTaskGUID);


void __RPC_STUB IEnumTask_GetFirst_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IEnumTask_GetNext_Proxy( 
    IEnumTask * This,
    /* [retval][out] */ BSTR *pbstrTaskGUID);


void __RPC_STUB IEnumTask_GetNext_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IEnumTask_CreateEnumTaskForOwner_Proxy( 
    IEnumTask * This,
    /* [in] */ BSTR bstrTaskOwnerSoftwareID);


void __RPC_STUB IEnumTask_CreateEnumTaskForOwner_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEnumTask_INTERFACE_DEFINED__ */


#ifndef __ITrigger_INTERFACE_DEFINED__
#define __ITrigger_INTERFACE_DEFINED__

/* interface ITrigger */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_ITrigger;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("016B6659-F7CE-4490-A058-D8FBE286D92E")
    ITrigger : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_GMTTime( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_GMTTime( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [hidden][helpstring][id] */ HRESULT STDMETHODCALLTYPE SetTask( 
            /* [in] */ unsigned __int64 lCookie,
            /* [in] */ BOOL bReadOnly) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_StartHour( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_StartHour( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_StartMinute( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_StartMinute( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_StartDay( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_StartDay( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_StartMonth( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_StartMonth( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_StartYear( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_StartYear( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_StopDateValid( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_StopDateValid( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_StopDay( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_StopDay( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_StopMonth( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_StopMonth( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_StopYear( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_StopYear( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Repeatable( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Repeatable( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_RepeatOption( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_RepeatOption( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_RepeatInterval( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_RepeatInterval( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_UntilOption( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_UntilOption( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_UntilHour( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_UntilHour( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_UntilMinute( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_UntilMinute( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_UntilDuration( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_UntilDuration( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_RandomizationEnabled( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_RandomizationEnabled( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_RandomizationWndMins( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_RandomizationWndMins( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_OnceADayEnabled( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_OnceADayEnabled( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_RepeatDays( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_RepeatDays( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_RepeatWeeks( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_RepeatWeeks( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_maskDaysOfWeek( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_maskDaysOfWeek( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_MonthlyOption( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_MonthlyOption( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DayNumOfMonth( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_DayNumOfMonth( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_WeekNumOfMonth( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_WeekNumOfMonth( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DayOfWeek( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_DayOfWeek( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_maskMonthsOfYear( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_maskMonthsOfYear( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_IdleMinutes( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_IdleMinutes( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_eScheduleType( 
            /* [retval][out] */ SCHEDULETYPE *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_eScheduleType( 
            /* [in] */ SCHEDULETYPE newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_AtStartupDelayMinutes( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_AtStartupDelayMinutes( 
            /* [in] */ long newVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ITriggerVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ITrigger * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ITrigger * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ITrigger * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ITrigger * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ITrigger * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ITrigger * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ITrigger * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_GMTTime )( 
            ITrigger * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_GMTTime )( 
            ITrigger * This,
            /* [in] */ BOOL newVal);
        
        /* [hidden][helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetTask )( 
            ITrigger * This,
            /* [in] */ unsigned __int64 lCookie,
            /* [in] */ BOOL bReadOnly);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_StartHour )( 
            ITrigger * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_StartHour )( 
            ITrigger * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_StartMinute )( 
            ITrigger * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_StartMinute )( 
            ITrigger * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_StartDay )( 
            ITrigger * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_StartDay )( 
            ITrigger * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_StartMonth )( 
            ITrigger * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_StartMonth )( 
            ITrigger * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_StartYear )( 
            ITrigger * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_StartYear )( 
            ITrigger * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_StopDateValid )( 
            ITrigger * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_StopDateValid )( 
            ITrigger * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_StopDay )( 
            ITrigger * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_StopDay )( 
            ITrigger * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_StopMonth )( 
            ITrigger * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_StopMonth )( 
            ITrigger * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_StopYear )( 
            ITrigger * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_StopYear )( 
            ITrigger * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Repeatable )( 
            ITrigger * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Repeatable )( 
            ITrigger * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_RepeatOption )( 
            ITrigger * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_RepeatOption )( 
            ITrigger * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_RepeatInterval )( 
            ITrigger * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_RepeatInterval )( 
            ITrigger * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_UntilOption )( 
            ITrigger * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_UntilOption )( 
            ITrigger * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_UntilHour )( 
            ITrigger * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_UntilHour )( 
            ITrigger * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_UntilMinute )( 
            ITrigger * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_UntilMinute )( 
            ITrigger * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_UntilDuration )( 
            ITrigger * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_UntilDuration )( 
            ITrigger * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_RandomizationEnabled )( 
            ITrigger * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_RandomizationEnabled )( 
            ITrigger * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_RandomizationWndMins )( 
            ITrigger * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_RandomizationWndMins )( 
            ITrigger * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_OnceADayEnabled )( 
            ITrigger * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_OnceADayEnabled )( 
            ITrigger * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_RepeatDays )( 
            ITrigger * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_RepeatDays )( 
            ITrigger * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_RepeatWeeks )( 
            ITrigger * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_RepeatWeeks )( 
            ITrigger * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_maskDaysOfWeek )( 
            ITrigger * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_maskDaysOfWeek )( 
            ITrigger * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MonthlyOption )( 
            ITrigger * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_MonthlyOption )( 
            ITrigger * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DayNumOfMonth )( 
            ITrigger * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DayNumOfMonth )( 
            ITrigger * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_WeekNumOfMonth )( 
            ITrigger * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_WeekNumOfMonth )( 
            ITrigger * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DayOfWeek )( 
            ITrigger * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DayOfWeek )( 
            ITrigger * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_maskMonthsOfYear )( 
            ITrigger * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_maskMonthsOfYear )( 
            ITrigger * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_IdleMinutes )( 
            ITrigger * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_IdleMinutes )( 
            ITrigger * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_eScheduleType )( 
            ITrigger * This,
            /* [retval][out] */ SCHEDULETYPE *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_eScheduleType )( 
            ITrigger * This,
            /* [in] */ SCHEDULETYPE newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_AtStartupDelayMinutes )( 
            ITrigger * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_AtStartupDelayMinutes )( 
            ITrigger * This,
            /* [in] */ long newVal);
        
        END_INTERFACE
    } ITriggerVtbl;

    interface ITrigger
    {
        CONST_VTBL struct ITriggerVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ITrigger_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define ITrigger_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define ITrigger_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define ITrigger_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define ITrigger_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define ITrigger_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define ITrigger_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define ITrigger_get_GMTTime(This,pVal)	\
    (This)->lpVtbl -> get_GMTTime(This,pVal)

#define ITrigger_put_GMTTime(This,newVal)	\
    (This)->lpVtbl -> put_GMTTime(This,newVal)

#define ITrigger_SetTask(This,lCookie,bReadOnly)	\
    (This)->lpVtbl -> SetTask(This,lCookie,bReadOnly)

#define ITrigger_get_StartHour(This,pVal)	\
    (This)->lpVtbl -> get_StartHour(This,pVal)

#define ITrigger_put_StartHour(This,newVal)	\
    (This)->lpVtbl -> put_StartHour(This,newVal)

#define ITrigger_get_StartMinute(This,pVal)	\
    (This)->lpVtbl -> get_StartMinute(This,pVal)

#define ITrigger_put_StartMinute(This,newVal)	\
    (This)->lpVtbl -> put_StartMinute(This,newVal)

#define ITrigger_get_StartDay(This,pVal)	\
    (This)->lpVtbl -> get_StartDay(This,pVal)

#define ITrigger_put_StartDay(This,newVal)	\
    (This)->lpVtbl -> put_StartDay(This,newVal)

#define ITrigger_get_StartMonth(This,pVal)	\
    (This)->lpVtbl -> get_StartMonth(This,pVal)

#define ITrigger_put_StartMonth(This,newVal)	\
    (This)->lpVtbl -> put_StartMonth(This,newVal)

#define ITrigger_get_StartYear(This,pVal)	\
    (This)->lpVtbl -> get_StartYear(This,pVal)

#define ITrigger_put_StartYear(This,newVal)	\
    (This)->lpVtbl -> put_StartYear(This,newVal)

#define ITrigger_get_StopDateValid(This,pVal)	\
    (This)->lpVtbl -> get_StopDateValid(This,pVal)

#define ITrigger_put_StopDateValid(This,newVal)	\
    (This)->lpVtbl -> put_StopDateValid(This,newVal)

#define ITrigger_get_StopDay(This,pVal)	\
    (This)->lpVtbl -> get_StopDay(This,pVal)

#define ITrigger_put_StopDay(This,newVal)	\
    (This)->lpVtbl -> put_StopDay(This,newVal)

#define ITrigger_get_StopMonth(This,pVal)	\
    (This)->lpVtbl -> get_StopMonth(This,pVal)

#define ITrigger_put_StopMonth(This,newVal)	\
    (This)->lpVtbl -> put_StopMonth(This,newVal)

#define ITrigger_get_StopYear(This,pVal)	\
    (This)->lpVtbl -> get_StopYear(This,pVal)

#define ITrigger_put_StopYear(This,newVal)	\
    (This)->lpVtbl -> put_StopYear(This,newVal)

#define ITrigger_get_Repeatable(This,pVal)	\
    (This)->lpVtbl -> get_Repeatable(This,pVal)

#define ITrigger_put_Repeatable(This,newVal)	\
    (This)->lpVtbl -> put_Repeatable(This,newVal)

#define ITrigger_get_RepeatOption(This,pVal)	\
    (This)->lpVtbl -> get_RepeatOption(This,pVal)

#define ITrigger_put_RepeatOption(This,newVal)	\
    (This)->lpVtbl -> put_RepeatOption(This,newVal)

#define ITrigger_get_RepeatInterval(This,pVal)	\
    (This)->lpVtbl -> get_RepeatInterval(This,pVal)

#define ITrigger_put_RepeatInterval(This,newVal)	\
    (This)->lpVtbl -> put_RepeatInterval(This,newVal)

#define ITrigger_get_UntilOption(This,pVal)	\
    (This)->lpVtbl -> get_UntilOption(This,pVal)

#define ITrigger_put_UntilOption(This,newVal)	\
    (This)->lpVtbl -> put_UntilOption(This,newVal)

#define ITrigger_get_UntilHour(This,pVal)	\
    (This)->lpVtbl -> get_UntilHour(This,pVal)

#define ITrigger_put_UntilHour(This,newVal)	\
    (This)->lpVtbl -> put_UntilHour(This,newVal)

#define ITrigger_get_UntilMinute(This,pVal)	\
    (This)->lpVtbl -> get_UntilMinute(This,pVal)

#define ITrigger_put_UntilMinute(This,newVal)	\
    (This)->lpVtbl -> put_UntilMinute(This,newVal)

#define ITrigger_get_UntilDuration(This,pVal)	\
    (This)->lpVtbl -> get_UntilDuration(This,pVal)

#define ITrigger_put_UntilDuration(This,newVal)	\
    (This)->lpVtbl -> put_UntilDuration(This,newVal)

#define ITrigger_get_RandomizationEnabled(This,pVal)	\
    (This)->lpVtbl -> get_RandomizationEnabled(This,pVal)

#define ITrigger_put_RandomizationEnabled(This,newVal)	\
    (This)->lpVtbl -> put_RandomizationEnabled(This,newVal)

#define ITrigger_get_RandomizationWndMins(This,pVal)	\
    (This)->lpVtbl -> get_RandomizationWndMins(This,pVal)

#define ITrigger_put_RandomizationWndMins(This,newVal)	\
    (This)->lpVtbl -> put_RandomizationWndMins(This,newVal)

#define ITrigger_get_OnceADayEnabled(This,pVal)	\
    (This)->lpVtbl -> get_OnceADayEnabled(This,pVal)

#define ITrigger_put_OnceADayEnabled(This,newVal)	\
    (This)->lpVtbl -> put_OnceADayEnabled(This,newVal)

#define ITrigger_get_RepeatDays(This,pVal)	\
    (This)->lpVtbl -> get_RepeatDays(This,pVal)

#define ITrigger_put_RepeatDays(This,newVal)	\
    (This)->lpVtbl -> put_RepeatDays(This,newVal)

#define ITrigger_get_RepeatWeeks(This,pVal)	\
    (This)->lpVtbl -> get_RepeatWeeks(This,pVal)

#define ITrigger_put_RepeatWeeks(This,newVal)	\
    (This)->lpVtbl -> put_RepeatWeeks(This,newVal)

#define ITrigger_get_maskDaysOfWeek(This,pVal)	\
    (This)->lpVtbl -> get_maskDaysOfWeek(This,pVal)

#define ITrigger_put_maskDaysOfWeek(This,newVal)	\
    (This)->lpVtbl -> put_maskDaysOfWeek(This,newVal)

#define ITrigger_get_MonthlyOption(This,pVal)	\
    (This)->lpVtbl -> get_MonthlyOption(This,pVal)

#define ITrigger_put_MonthlyOption(This,newVal)	\
    (This)->lpVtbl -> put_MonthlyOption(This,newVal)

#define ITrigger_get_DayNumOfMonth(This,pVal)	\
    (This)->lpVtbl -> get_DayNumOfMonth(This,pVal)

#define ITrigger_put_DayNumOfMonth(This,newVal)	\
    (This)->lpVtbl -> put_DayNumOfMonth(This,newVal)

#define ITrigger_get_WeekNumOfMonth(This,pVal)	\
    (This)->lpVtbl -> get_WeekNumOfMonth(This,pVal)

#define ITrigger_put_WeekNumOfMonth(This,newVal)	\
    (This)->lpVtbl -> put_WeekNumOfMonth(This,newVal)

#define ITrigger_get_DayOfWeek(This,pVal)	\
    (This)->lpVtbl -> get_DayOfWeek(This,pVal)

#define ITrigger_put_DayOfWeek(This,newVal)	\
    (This)->lpVtbl -> put_DayOfWeek(This,newVal)

#define ITrigger_get_maskMonthsOfYear(This,pVal)	\
    (This)->lpVtbl -> get_maskMonthsOfYear(This,pVal)

#define ITrigger_put_maskMonthsOfYear(This,newVal)	\
    (This)->lpVtbl -> put_maskMonthsOfYear(This,newVal)

#define ITrigger_get_IdleMinutes(This,pVal)	\
    (This)->lpVtbl -> get_IdleMinutes(This,pVal)

#define ITrigger_put_IdleMinutes(This,newVal)	\
    (This)->lpVtbl -> put_IdleMinutes(This,newVal)

#define ITrigger_get_eScheduleType(This,pVal)	\
    (This)->lpVtbl -> get_eScheduleType(This,pVal)

#define ITrigger_put_eScheduleType(This,newVal)	\
    (This)->lpVtbl -> put_eScheduleType(This,newVal)

#define ITrigger_get_AtStartupDelayMinutes(This,pVal)	\
    (This)->lpVtbl -> get_AtStartupDelayMinutes(This,pVal)

#define ITrigger_put_AtStartupDelayMinutes(This,newVal)	\
    (This)->lpVtbl -> put_AtStartupDelayMinutes(This,newVal)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ITrigger_get_GMTTime_Proxy( 
    ITrigger * This,
    /* [retval][out] */ BOOL *pVal);


void __RPC_STUB ITrigger_get_GMTTime_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ITrigger_put_GMTTime_Proxy( 
    ITrigger * This,
    /* [in] */ BOOL newVal);


void __RPC_STUB ITrigger_put_GMTTime_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [hidden][helpstring][id] */ HRESULT STDMETHODCALLTYPE ITrigger_SetTask_Proxy( 
    ITrigger * This,
    /* [in] */ unsigned __int64 lCookie,
    /* [in] */ BOOL bReadOnly);


void __RPC_STUB ITrigger_SetTask_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ITrigger_get_StartHour_Proxy( 
    ITrigger * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB ITrigger_get_StartHour_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ITrigger_put_StartHour_Proxy( 
    ITrigger * This,
    /* [in] */ long newVal);


void __RPC_STUB ITrigger_put_StartHour_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ITrigger_get_StartMinute_Proxy( 
    ITrigger * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB ITrigger_get_StartMinute_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ITrigger_put_StartMinute_Proxy( 
    ITrigger * This,
    /* [in] */ long newVal);


void __RPC_STUB ITrigger_put_StartMinute_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ITrigger_get_StartDay_Proxy( 
    ITrigger * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB ITrigger_get_StartDay_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ITrigger_put_StartDay_Proxy( 
    ITrigger * This,
    /* [in] */ long newVal);


void __RPC_STUB ITrigger_put_StartDay_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ITrigger_get_StartMonth_Proxy( 
    ITrigger * This,
    /* [retval][out] */ short *pVal);


void __RPC_STUB ITrigger_get_StartMonth_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ITrigger_put_StartMonth_Proxy( 
    ITrigger * This,
    /* [in] */ short newVal);


void __RPC_STUB ITrigger_put_StartMonth_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ITrigger_get_StartYear_Proxy( 
    ITrigger * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB ITrigger_get_StartYear_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ITrigger_put_StartYear_Proxy( 
    ITrigger * This,
    /* [in] */ long newVal);


void __RPC_STUB ITrigger_put_StartYear_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ITrigger_get_StopDateValid_Proxy( 
    ITrigger * This,
    /* [retval][out] */ BOOL *pVal);


void __RPC_STUB ITrigger_get_StopDateValid_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ITrigger_put_StopDateValid_Proxy( 
    ITrigger * This,
    /* [in] */ BOOL newVal);


void __RPC_STUB ITrigger_put_StopDateValid_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ITrigger_get_StopDay_Proxy( 
    ITrigger * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB ITrigger_get_StopDay_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ITrigger_put_StopDay_Proxy( 
    ITrigger * This,
    /* [in] */ long newVal);


void __RPC_STUB ITrigger_put_StopDay_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ITrigger_get_StopMonth_Proxy( 
    ITrigger * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB ITrigger_get_StopMonth_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ITrigger_put_StopMonth_Proxy( 
    ITrigger * This,
    /* [in] */ long newVal);


void __RPC_STUB ITrigger_put_StopMonth_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ITrigger_get_StopYear_Proxy( 
    ITrigger * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB ITrigger_get_StopYear_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ITrigger_put_StopYear_Proxy( 
    ITrigger * This,
    /* [in] */ long newVal);


void __RPC_STUB ITrigger_put_StopYear_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ITrigger_get_Repeatable_Proxy( 
    ITrigger * This,
    /* [retval][out] */ BOOL *pVal);


void __RPC_STUB ITrigger_get_Repeatable_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ITrigger_put_Repeatable_Proxy( 
    ITrigger * This,
    /* [in] */ BOOL newVal);


void __RPC_STUB ITrigger_put_Repeatable_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ITrigger_get_RepeatOption_Proxy( 
    ITrigger * This,
    /* [retval][out] */ short *pVal);


void __RPC_STUB ITrigger_get_RepeatOption_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ITrigger_put_RepeatOption_Proxy( 
    ITrigger * This,
    /* [in] */ short newVal);


void __RPC_STUB ITrigger_put_RepeatOption_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ITrigger_get_RepeatInterval_Proxy( 
    ITrigger * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB ITrigger_get_RepeatInterval_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ITrigger_put_RepeatInterval_Proxy( 
    ITrigger * This,
    /* [in] */ long newVal);


void __RPC_STUB ITrigger_put_RepeatInterval_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ITrigger_get_UntilOption_Proxy( 
    ITrigger * This,
    /* [retval][out] */ short *pVal);


void __RPC_STUB ITrigger_get_UntilOption_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ITrigger_put_UntilOption_Proxy( 
    ITrigger * This,
    /* [in] */ short newVal);


void __RPC_STUB ITrigger_put_UntilOption_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ITrigger_get_UntilHour_Proxy( 
    ITrigger * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB ITrigger_get_UntilHour_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ITrigger_put_UntilHour_Proxy( 
    ITrigger * This,
    /* [in] */ long newVal);


void __RPC_STUB ITrigger_put_UntilHour_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ITrigger_get_UntilMinute_Proxy( 
    ITrigger * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB ITrigger_get_UntilMinute_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ITrigger_put_UntilMinute_Proxy( 
    ITrigger * This,
    /* [in] */ long newVal);


void __RPC_STUB ITrigger_put_UntilMinute_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ITrigger_get_UntilDuration_Proxy( 
    ITrigger * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB ITrigger_get_UntilDuration_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ITrigger_put_UntilDuration_Proxy( 
    ITrigger * This,
    /* [in] */ long newVal);


void __RPC_STUB ITrigger_put_UntilDuration_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ITrigger_get_RandomizationEnabled_Proxy( 
    ITrigger * This,
    /* [retval][out] */ BOOL *pVal);


void __RPC_STUB ITrigger_get_RandomizationEnabled_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ITrigger_put_RandomizationEnabled_Proxy( 
    ITrigger * This,
    /* [in] */ BOOL newVal);


void __RPC_STUB ITrigger_put_RandomizationEnabled_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ITrigger_get_RandomizationWndMins_Proxy( 
    ITrigger * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB ITrigger_get_RandomizationWndMins_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ITrigger_put_RandomizationWndMins_Proxy( 
    ITrigger * This,
    /* [in] */ long newVal);


void __RPC_STUB ITrigger_put_RandomizationWndMins_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ITrigger_get_OnceADayEnabled_Proxy( 
    ITrigger * This,
    /* [retval][out] */ BOOL *pVal);


void __RPC_STUB ITrigger_get_OnceADayEnabled_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ITrigger_put_OnceADayEnabled_Proxy( 
    ITrigger * This,
    /* [in] */ BOOL newVal);


void __RPC_STUB ITrigger_put_OnceADayEnabled_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ITrigger_get_RepeatDays_Proxy( 
    ITrigger * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB ITrigger_get_RepeatDays_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ITrigger_put_RepeatDays_Proxy( 
    ITrigger * This,
    /* [in] */ long newVal);


void __RPC_STUB ITrigger_put_RepeatDays_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ITrigger_get_RepeatWeeks_Proxy( 
    ITrigger * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB ITrigger_get_RepeatWeeks_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ITrigger_put_RepeatWeeks_Proxy( 
    ITrigger * This,
    /* [in] */ long newVal);


void __RPC_STUB ITrigger_put_RepeatWeeks_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ITrigger_get_maskDaysOfWeek_Proxy( 
    ITrigger * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB ITrigger_get_maskDaysOfWeek_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ITrigger_put_maskDaysOfWeek_Proxy( 
    ITrigger * This,
    /* [in] */ long newVal);


void __RPC_STUB ITrigger_put_maskDaysOfWeek_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ITrigger_get_MonthlyOption_Proxy( 
    ITrigger * This,
    /* [retval][out] */ short *pVal);


void __RPC_STUB ITrigger_get_MonthlyOption_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ITrigger_put_MonthlyOption_Proxy( 
    ITrigger * This,
    /* [in] */ short newVal);


void __RPC_STUB ITrigger_put_MonthlyOption_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ITrigger_get_DayNumOfMonth_Proxy( 
    ITrigger * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB ITrigger_get_DayNumOfMonth_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ITrigger_put_DayNumOfMonth_Proxy( 
    ITrigger * This,
    /* [in] */ long newVal);


void __RPC_STUB ITrigger_put_DayNumOfMonth_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ITrigger_get_WeekNumOfMonth_Proxy( 
    ITrigger * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB ITrigger_get_WeekNumOfMonth_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ITrigger_put_WeekNumOfMonth_Proxy( 
    ITrigger * This,
    /* [in] */ long newVal);


void __RPC_STUB ITrigger_put_WeekNumOfMonth_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ITrigger_get_DayOfWeek_Proxy( 
    ITrigger * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB ITrigger_get_DayOfWeek_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ITrigger_put_DayOfWeek_Proxy( 
    ITrigger * This,
    /* [in] */ long newVal);


void __RPC_STUB ITrigger_put_DayOfWeek_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ITrigger_get_maskMonthsOfYear_Proxy( 
    ITrigger * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB ITrigger_get_maskMonthsOfYear_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ITrigger_put_maskMonthsOfYear_Proxy( 
    ITrigger * This,
    /* [in] */ long newVal);


void __RPC_STUB ITrigger_put_maskMonthsOfYear_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ITrigger_get_IdleMinutes_Proxy( 
    ITrigger * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB ITrigger_get_IdleMinutes_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ITrigger_put_IdleMinutes_Proxy( 
    ITrigger * This,
    /* [in] */ long newVal);


void __RPC_STUB ITrigger_put_IdleMinutes_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ITrigger_get_eScheduleType_Proxy( 
    ITrigger * This,
    /* [retval][out] */ SCHEDULETYPE *pVal);


void __RPC_STUB ITrigger_get_eScheduleType_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ITrigger_put_eScheduleType_Proxy( 
    ITrigger * This,
    /* [in] */ SCHEDULETYPE newVal);


void __RPC_STUB ITrigger_put_eScheduleType_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ITrigger_get_AtStartupDelayMinutes_Proxy( 
    ITrigger * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB ITrigger_get_AtStartupDelayMinutes_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ITrigger_put_AtStartupDelayMinutes_Proxy( 
    ITrigger * This,
    /* [in] */ long newVal);


void __RPC_STUB ITrigger_put_AtStartupDelayMinutes_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __ITrigger_INTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_Schedule;

#ifdef __cplusplus

class DECLSPEC_UUID("4EF17F94-3975-4ACF-B228-29485BDE5860")
Schedule;
#endif

EXTERN_C const CLSID CLSID_Task;

#ifdef __cplusplus

class DECLSPEC_UUID("3AEC7772-2766-4C67-8487-4189C55DDE4E")
Task;
#endif

EXTERN_C const CLSID CLSID_EnumTask;

#ifdef __cplusplus

class DECLSPEC_UUID("056ADD67-DDB0-47BE-9F7D-DC652206F766")
EnumTask;
#endif

EXTERN_C const CLSID CLSID_Trigger;

#ifdef __cplusplus

class DECLSPEC_UUID("D8D9EEBC-0640-47AC-84FF-97C3A6B2FC79")
Trigger;
#endif
#endif /* __SCHEDULERLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long *, BSTR * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


