

/* this ALWAYS GENERATED file contains the IIDs and CLSIDs */

/* link this file in with the server and any clients */


 /* File created by MIDL compiler version 6.00.0361 */
/* at Fri Dec 01 20:33:45 2006
 */
/* Compiler settings for .\genevtinf.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
//@@MIDL_FILE_HEADING(  )

#if !defined(_M_IA64) && !defined(_M_AMD64)


#pragma warning( disable: 4049 )  /* more than 64k source lines */


#ifdef __cplusplus
extern "C"{
#endif 


#include <rpc.h>
#include <rpcndr.h>

#ifdef _MIDL_USE_GUIDDEF_

#ifndef INITGUID
#define INITGUID
#include <guiddef.h>
#undef INITGUID
#else
#include <guiddef.h>
#endif

#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
        DEFINE_GUID(name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8)

#else // !_MIDL_USE_GUIDDEF_

#ifndef __IID_DEFINED__
#define __IID_DEFINED__

typedef struct _IID
{
    unsigned long x;
    unsigned short s1;
    unsigned short s2;
    unsigned char  c[8];
} IID;

#endif // __IID_DEFINED__

#ifndef CLSID_DEFINED
#define CLSID_DEFINED
typedef IID CLSID;
#endif // CLSID_DEFINED

#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
        const type name = {l,w1,w2,{b1,b2,b3,b4,b5,b6,b7,b8}}

#endif !_MIDL_USE_GUIDDEF_

MIDL_DEFINE_GUID(IID, IID_IEpoEventInf,0xFCD58329,0xBBAC,0x4c5a,0x9F,0x84,0xE4,0x1B,0xEE,0x7B,0xD4,0x53);


MIDL_DEFINE_GUID(IID, LIBID_GENEVTINFLib,0xB5FA7874,0xACBF,0x46B3,0xBE,0x2E,0x98,0x38,0x1F,0xDA,0x9D,0x44);


MIDL_DEFINE_GUID(IID, DIID__IEpoEventInfEvents,0x51A3A47B,0x373D,0x4066,0x9D,0x21,0x99,0xAC,0x85,0x5C,0xE5,0xBE);


MIDL_DEFINE_GUID(IID, IID_IEventElement,0x146A29BB,0x9EBF,0x4ED3,0xAB,0x80,0xD6,0xCC,0x89,0x2D,0x47,0x4C);


MIDL_DEFINE_GUID(IID, IID_IEpoEventInf2,0x2D870730,0x7363,0x4CFD,0xBB,0x42,0x88,0x41,0xD8,0x51,0x50,0x32);


MIDL_DEFINE_GUID(IID, IID_IEpoEventInf3,0xBD1C616F,0x0298,0x49af,0xB7,0xEE,0x81,0x1D,0xF7,0x25,0xD1,0x87);


MIDL_DEFINE_GUID(IID, IID_IEpoEventInfCallback,0xA2153C51,0x2E2E,0x442E,0xBD,0x1A,0x50,0x58,0xB4,0x19,0x65,0xC5);


MIDL_DEFINE_GUID(CLSID, CLSID_EpoEventInf,0x0D7C69CB,0xCF5F,0x4594,0x8F,0xEE,0x0B,0x6D,0xDA,0x9F,0x75,0xD6);


MIDL_DEFINE_GUID(CLSID, CLSID_EventElement,0x528F66A7,0x7891,0x4F6B,0x8F,0x1E,0x51,0x32,0xCC,0x80,0x65,0x0C);


MIDL_DEFINE_GUID(CLSID, CLSID_EpoEventInfCallback,0x5284E46B,0x63F2,0x4D12,0x94,0x34,0x88,0xB7,0x89,0x9E,0xA7,0xDD);

#undef MIDL_DEFINE_GUID

#ifdef __cplusplus
}
#endif



#endif /* !defined(_M_IA64) && !defined(_M_AMD64)*/

