

/* this ALWAYS GENERATED file contains the IIDs and CLSIDs */

/* link this file in with the server and any clients */


 /* File created by MIDL compiler version 6.00.0361 */
/* at Fri Dec 01 20:33:08 2006
 */
/* Compiler settings for .\Scheduler.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
//@@MIDL_FILE_HEADING(  )

#if !defined(_M_IA64) && !defined(_M_AMD64)


#pragma warning( disable: 4049 )  /* more than 64k source lines */


#ifdef __cplusplus
extern "C"{
#endif 


#include <rpc.h>
#include <rpcndr.h>

#ifdef _MIDL_USE_GUIDDEF_

#ifndef INITGUID
#define INITGUID
#include <guiddef.h>
#undef INITGUID
#else
#include <guiddef.h>
#endif

#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
        DEFINE_GUID(name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8)

#else // !_MIDL_USE_GUIDDEF_

#ifndef __IID_DEFINED__
#define __IID_DEFINED__

typedef struct _IID
{
    unsigned long x;
    unsigned short s1;
    unsigned short s2;
    unsigned char  c[8];
} IID;

#endif // __IID_DEFINED__

#ifndef CLSID_DEFINED
#define CLSID_DEFINED
typedef IID CLSID;
#endif // CLSID_DEFINED

#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
        const type name = {l,w1,w2,{b1,b2,b3,b4,b5,b6,b7,b8}}

#endif !_MIDL_USE_GUIDDEF_

MIDL_DEFINE_GUID(IID, IID_ISchedule,0x846BEF02,0x7F37,0x43B4,0x80,0xDA,0xC7,0x63,0xF7,0x82,0xEE,0x93);


MIDL_DEFINE_GUID(IID, LIBID_SCHEDULERLib,0xC83F84A8,0x241A,0x4837,0xA6,0xBA,0x1C,0x51,0x31,0x14,0x17,0x43);


MIDL_DEFINE_GUID(IID, DIID__IScheduleEvents,0x20F0E677,0xD037,0x4A07,0xBC,0xFF,0x18,0x6A,0x82,0x5B,0x3E,0x02);


MIDL_DEFINE_GUID(IID, IID_ITask,0x5AA193A7,0x1A75,0x4B41,0x93,0xB9,0x02,0x50,0x3C,0x32,0xA0,0x75);


MIDL_DEFINE_GUID(IID, IID_IEnumTask,0x941A666E,0x022B,0x45C1,0x80,0xF6,0x32,0xDC,0x82,0x17,0x87,0x62);


MIDL_DEFINE_GUID(IID, IID_ITrigger,0x016B6659,0xF7CE,0x4490,0xA0,0x58,0xD8,0xFB,0xE2,0x86,0xD9,0x2E);


MIDL_DEFINE_GUID(CLSID, CLSID_Schedule,0x4EF17F94,0x3975,0x4ACF,0xB2,0x28,0x29,0x48,0x5B,0xDE,0x58,0x60);


MIDL_DEFINE_GUID(CLSID, CLSID_Task,0x3AEC7772,0x2766,0x4C67,0x84,0x87,0x41,0x89,0xC5,0x5D,0xDE,0x4E);


MIDL_DEFINE_GUID(CLSID, CLSID_EnumTask,0x056ADD67,0xDDB0,0x47BE,0x9F,0x7D,0xDC,0x65,0x22,0x06,0xF7,0x66);


MIDL_DEFINE_GUID(CLSID, CLSID_Trigger,0xD8D9EEBC,0x0640,0x47AC,0x84,0xFF,0x97,0xC3,0xA6,0xB2,0xFC,0x79);

#undef MIDL_DEFINE_GUID

#ifdef __cplusplus
}
#endif



#endif /* !defined(_M_IA64) && !defined(_M_AMD64)*/

