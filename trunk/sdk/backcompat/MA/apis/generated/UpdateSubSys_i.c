

/* this ALWAYS GENERATED file contains the IIDs and CLSIDs */

/* link this file in with the server and any clients */


 /* File created by MIDL compiler version 6.00.0361 */
/* at Fri Dec 01 20:33:30 2006
 */
/* Compiler settings for \Cma1\Build\Cma\win\Framework\UpdateSubSys\UpdateSubSys.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
//@@MIDL_FILE_HEADING(  )

#if !defined(_M_IA64) && !defined(_M_AMD64)


#pragma warning( disable: 4049 )  /* more than 64k source lines */


#ifdef __cplusplus
extern "C"{
#endif 


#include <rpc.h>
#include <rpcndr.h>

#ifdef _MIDL_USE_GUIDDEF_

#ifndef INITGUID
#define INITGUID
#include <guiddef.h>
#undef INITGUID
#else
#include <guiddef.h>
#endif

#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
        DEFINE_GUID(name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8)

#else // !_MIDL_USE_GUIDDEF_

#ifndef __IID_DEFINED__
#define __IID_DEFINED__

typedef struct _IID
{
    unsigned long x;
    unsigned short s1;
    unsigned short s2;
    unsigned char  c[8];
} IID;

#endif // __IID_DEFINED__

#ifndef CLSID_DEFINED
#define CLSID_DEFINED
typedef IID CLSID;
#endif // CLSID_DEFINED

#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
        const type name = {l,w1,w2,{b1,b2,b3,b4,b5,b6,b7,b8}}

#endif !_MIDL_USE_GUIDDEF_

MIDL_DEFINE_GUID(IID, IID_IMcAfeeUpdate,0xD831533D,0x0324,0x4EA4,0xB3,0xFD,0x07,0x3A,0xFE,0xE8,0x51,0x81);


MIDL_DEFINE_GUID(IID, LIBID_UPDATESUBSYSLib,0x7576F677,0x3945,0x4DA2,0xB9,0xF0,0x37,0x85,0x00,0x28,0xA7,0xE7);


MIDL_DEFINE_GUID(IID, DIID__IMcAfeeUpdateEvents,0x963B7D71,0xC519,0x4a5d,0xB8,0xE7,0x74,0x2B,0x08,0x62,0x8F,0x5D);


MIDL_DEFINE_GUID(IID, IID_IMcAfeeUpdate2,0xA33F52E5,0x9F67,0x459C,0x95,0xD3,0x84,0x20,0xB5,0x0E,0x71,0x83);


MIDL_DEFINE_GUID(CLSID, CLSID_McAfeeUpdate,0x6E389062,0xC540,0x4145,0x96,0xB9,0xB8,0x74,0x5C,0xF7,0xD8,0x56);

#undef MIDL_DEFINE_GUID

#ifdef __cplusplus
}
#endif



#endif /* !defined(_M_IA64) && !defined(_M_AMD64)*/

