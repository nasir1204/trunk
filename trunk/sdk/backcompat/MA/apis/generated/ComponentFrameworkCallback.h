

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 6.00.0361 */
/* at Fri Dec 01 20:31:30 2006
 */
/* Compiler settings for .\ComponentFrameworkCallback.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
//@@MIDL_FILE_HEADING(  )

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __ComponentFrameworkCallback_h__
#define __ComponentFrameworkCallback_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IFrameworkCallback_FWD_DEFINED__
#define __IFrameworkCallback_FWD_DEFINED__
typedef interface IFrameworkCallback IFrameworkCallback;
#endif 	/* __IFrameworkCallback_FWD_DEFINED__ */


#ifndef __FrameworkCallback_FWD_DEFINED__
#define __FrameworkCallback_FWD_DEFINED__

#ifdef __cplusplus
typedef class FrameworkCallback FrameworkCallback;
#else
typedef struct FrameworkCallback FrameworkCallback;
#endif /* __cplusplus */

#endif 	/* __FrameworkCallback_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 

void * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void * ); 

#ifndef __IFrameworkCallback_INTERFACE_DEFINED__
#define __IFrameworkCallback_INTERFACE_DEFINED__

/* interface IFrameworkCallback */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IFrameworkCallback;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("69B9BCAF-BF14-4BFF-997D-DC8D4AB0228F")
    IFrameworkCallback : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE OnFrameworkTerminate( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE OnFrameworkAvailable( void) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IFrameworkCallbackVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IFrameworkCallback * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IFrameworkCallback * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IFrameworkCallback * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IFrameworkCallback * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IFrameworkCallback * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IFrameworkCallback * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IFrameworkCallback * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *OnFrameworkTerminate )( 
            IFrameworkCallback * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *OnFrameworkAvailable )( 
            IFrameworkCallback * This);
        
        END_INTERFACE
    } IFrameworkCallbackVtbl;

    interface IFrameworkCallback
    {
        CONST_VTBL struct IFrameworkCallbackVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IFrameworkCallback_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IFrameworkCallback_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IFrameworkCallback_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IFrameworkCallback_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IFrameworkCallback_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IFrameworkCallback_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IFrameworkCallback_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IFrameworkCallback_OnFrameworkTerminate(This)	\
    (This)->lpVtbl -> OnFrameworkTerminate(This)

#define IFrameworkCallback_OnFrameworkAvailable(This)	\
    (This)->lpVtbl -> OnFrameworkAvailable(This)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IFrameworkCallback_OnFrameworkTerminate_Proxy( 
    IFrameworkCallback * This);


void __RPC_STUB IFrameworkCallback_OnFrameworkTerminate_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IFrameworkCallback_OnFrameworkAvailable_Proxy( 
    IFrameworkCallback * This);


void __RPC_STUB IFrameworkCallback_OnFrameworkAvailable_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IFrameworkCallback_INTERFACE_DEFINED__ */



#ifndef __COMPONENTFRAMEWORKCALLBACKLib_LIBRARY_DEFINED__
#define __COMPONENTFRAMEWORKCALLBACKLib_LIBRARY_DEFINED__

/* library COMPONENTFRAMEWORKCALLBACKLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_COMPONENTFRAMEWORKCALLBACKLib;

EXTERN_C const CLSID CLSID_FrameworkCallback;

#ifdef __cplusplus

class DECLSPEC_UUID("BE3AF818-DA43-407C-A5E6-3C0049CD2445")
FrameworkCallback;
#endif
#endif /* __COMPONENTFRAMEWORKCALLBACKLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


