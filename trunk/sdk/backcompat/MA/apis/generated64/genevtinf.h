

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 6.00.0366 */
/* at Fri Dec 01 20:43:42 2006
 */
/* Compiler settings for .\genevtinf.idl:
    Oicf, W1, Zp8, env=Win64 (32b run)
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
//@@MIDL_FILE_HEADING(  )

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __genevtinf_h__
#define __genevtinf_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IEpoEventInf_FWD_DEFINED__
#define __IEpoEventInf_FWD_DEFINED__
typedef interface IEpoEventInf IEpoEventInf;
#endif 	/* __IEpoEventInf_FWD_DEFINED__ */


#ifndef ___IEpoEventInfEvents_FWD_DEFINED__
#define ___IEpoEventInfEvents_FWD_DEFINED__
typedef interface _IEpoEventInfEvents _IEpoEventInfEvents;
#endif 	/* ___IEpoEventInfEvents_FWD_DEFINED__ */


#ifndef __IEventElement_FWD_DEFINED__
#define __IEventElement_FWD_DEFINED__
typedef interface IEventElement IEventElement;
#endif 	/* __IEventElement_FWD_DEFINED__ */


#ifndef __IEpoEventInf_FWD_DEFINED__
#define __IEpoEventInf_FWD_DEFINED__
typedef interface IEpoEventInf IEpoEventInf;
#endif 	/* __IEpoEventInf_FWD_DEFINED__ */


#ifndef __IEpoEventInf2_FWD_DEFINED__
#define __IEpoEventInf2_FWD_DEFINED__
typedef interface IEpoEventInf2 IEpoEventInf2;
#endif 	/* __IEpoEventInf2_FWD_DEFINED__ */


#ifndef __IEpoEventInf3_FWD_DEFINED__
#define __IEpoEventInf3_FWD_DEFINED__
typedef interface IEpoEventInf3 IEpoEventInf3;
#endif 	/* __IEpoEventInf3_FWD_DEFINED__ */


#ifndef __IEpoEventInfCallback_FWD_DEFINED__
#define __IEpoEventInfCallback_FWD_DEFINED__
typedef interface IEpoEventInfCallback IEpoEventInfCallback;
#endif 	/* __IEpoEventInfCallback_FWD_DEFINED__ */


#ifndef __EpoEventInf_FWD_DEFINED__
#define __EpoEventInf_FWD_DEFINED__

#ifdef __cplusplus
typedef class EpoEventInf EpoEventInf;
#else
typedef struct EpoEventInf EpoEventInf;
#endif /* __cplusplus */

#endif 	/* __EpoEventInf_FWD_DEFINED__ */


#ifndef __EventElement_FWD_DEFINED__
#define __EventElement_FWD_DEFINED__

#ifdef __cplusplus
typedef class EventElement EventElement;
#else
typedef struct EventElement EventElement;
#endif /* __cplusplus */

#endif 	/* __EventElement_FWD_DEFINED__ */


#ifndef __EpoEventInfCallback_FWD_DEFINED__
#define __EpoEventInfCallback_FWD_DEFINED__

#ifdef __cplusplus
typedef class EpoEventInfCallback EpoEventInfCallback;
#else
typedef struct EpoEventInfCallback EpoEventInfCallback;
#endif /* __cplusplus */

#endif 	/* __EpoEventInfCallback_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 

void * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void * ); 

#ifndef __IEpoEventInf_INTERFACE_DEFINED__
#define __IEpoEventInf_INTERFACE_DEFINED__

/* interface IEpoEventInf */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IEpoEventInf;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("FCD58329-BBAC-4c5a-9F84-E41BEE7BD453")
    IEpoEventInf : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Initialize( 
            BSTR strSoftwareID,
            BSTR strRootName,
            BSTR strSoftwareNodeName,
            BSTR strSoftwareName,
            BSTR strVersion,
            BSTR strProductFamily) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE DeInitialize( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetMachineElement( 
            /* [out] */ IUnknown **pIMachineNode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetSoftwareElement( 
            /* [out] */ IUnknown **pISoftwareNode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE CreateEventElement( 
            BSTR strEventNodeName,
            long lEventID,
            long lSeverity,
            DATE GMTTime,
            /* [out] */ IUnknown **pIEventElemNode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Save( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE CreateMachineElement( 
            BSTR szIPAddress,
            BSTR szOS,
            BSTR szUserName) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpoEventInfVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IEpoEventInf * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IEpoEventInf * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IEpoEventInf * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IEpoEventInf * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IEpoEventInf * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IEpoEventInf * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IEpoEventInf * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Initialize )( 
            IEpoEventInf * This,
            BSTR strSoftwareID,
            BSTR strRootName,
            BSTR strSoftwareNodeName,
            BSTR strSoftwareName,
            BSTR strVersion,
            BSTR strProductFamily);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *DeInitialize )( 
            IEpoEventInf * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetMachineElement )( 
            IEpoEventInf * This,
            /* [out] */ IUnknown **pIMachineNode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetSoftwareElement )( 
            IEpoEventInf * This,
            /* [out] */ IUnknown **pISoftwareNode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CreateEventElement )( 
            IEpoEventInf * This,
            BSTR strEventNodeName,
            long lEventID,
            long lSeverity,
            DATE GMTTime,
            /* [out] */ IUnknown **pIEventElemNode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Save )( 
            IEpoEventInf * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CreateMachineElement )( 
            IEpoEventInf * This,
            BSTR szIPAddress,
            BSTR szOS,
            BSTR szUserName);
        
        END_INTERFACE
    } IEpoEventInfVtbl;

    interface IEpoEventInf
    {
        CONST_VTBL struct IEpoEventInfVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpoEventInf_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpoEventInf_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpoEventInf_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpoEventInf_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IEpoEventInf_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IEpoEventInf_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IEpoEventInf_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IEpoEventInf_Initialize(This,strSoftwareID,strRootName,strSoftwareNodeName,strSoftwareName,strVersion,strProductFamily)	\
    (This)->lpVtbl -> Initialize(This,strSoftwareID,strRootName,strSoftwareNodeName,strSoftwareName,strVersion,strProductFamily)

#define IEpoEventInf_DeInitialize(This)	\
    (This)->lpVtbl -> DeInitialize(This)

#define IEpoEventInf_GetMachineElement(This,pIMachineNode)	\
    (This)->lpVtbl -> GetMachineElement(This,pIMachineNode)

#define IEpoEventInf_GetSoftwareElement(This,pISoftwareNode)	\
    (This)->lpVtbl -> GetSoftwareElement(This,pISoftwareNode)

#define IEpoEventInf_CreateEventElement(This,strEventNodeName,lEventID,lSeverity,GMTTime,pIEventElemNode)	\
    (This)->lpVtbl -> CreateEventElement(This,strEventNodeName,lEventID,lSeverity,GMTTime,pIEventElemNode)

#define IEpoEventInf_Save(This)	\
    (This)->lpVtbl -> Save(This)

#define IEpoEventInf_CreateMachineElement(This,szIPAddress,szOS,szUserName)	\
    (This)->lpVtbl -> CreateMachineElement(This,szIPAddress,szOS,szUserName)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IEpoEventInf_Initialize_Proxy( 
    IEpoEventInf * This,
    BSTR strSoftwareID,
    BSTR strRootName,
    BSTR strSoftwareNodeName,
    BSTR strSoftwareName,
    BSTR strVersion,
    BSTR strProductFamily);


void __RPC_STUB IEpoEventInf_Initialize_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IEpoEventInf_DeInitialize_Proxy( 
    IEpoEventInf * This);


void __RPC_STUB IEpoEventInf_DeInitialize_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IEpoEventInf_GetMachineElement_Proxy( 
    IEpoEventInf * This,
    /* [out] */ IUnknown **pIMachineNode);


void __RPC_STUB IEpoEventInf_GetMachineElement_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IEpoEventInf_GetSoftwareElement_Proxy( 
    IEpoEventInf * This,
    /* [out] */ IUnknown **pISoftwareNode);


void __RPC_STUB IEpoEventInf_GetSoftwareElement_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IEpoEventInf_CreateEventElement_Proxy( 
    IEpoEventInf * This,
    BSTR strEventNodeName,
    long lEventID,
    long lSeverity,
    DATE GMTTime,
    /* [out] */ IUnknown **pIEventElemNode);


void __RPC_STUB IEpoEventInf_CreateEventElement_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IEpoEventInf_Save_Proxy( 
    IEpoEventInf * This);


void __RPC_STUB IEpoEventInf_Save_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IEpoEventInf_CreateMachineElement_Proxy( 
    IEpoEventInf * This,
    BSTR szIPAddress,
    BSTR szOS,
    BSTR szUserName);


void __RPC_STUB IEpoEventInf_CreateMachineElement_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpoEventInf_INTERFACE_DEFINED__ */



#ifndef __GENEVTINFLib_LIBRARY_DEFINED__
#define __GENEVTINFLib_LIBRARY_DEFINED__

/* library GENEVTINFLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_GENEVTINFLib;

#ifndef ___IEpoEventInfEvents_DISPINTERFACE_DEFINED__
#define ___IEpoEventInfEvents_DISPINTERFACE_DEFINED__

/* dispinterface _IEpoEventInfEvents */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__IEpoEventInfEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("51A3A47B-373D-4066-9D21-99AC855CE5BE")
    _IEpoEventInfEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _IEpoEventInfEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _IEpoEventInfEvents * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _IEpoEventInfEvents * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _IEpoEventInfEvents * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _IEpoEventInfEvents * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _IEpoEventInfEvents * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _IEpoEventInfEvents * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _IEpoEventInfEvents * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _IEpoEventInfEventsVtbl;

    interface _IEpoEventInfEvents
    {
        CONST_VTBL struct _IEpoEventInfEventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _IEpoEventInfEvents_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _IEpoEventInfEvents_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _IEpoEventInfEvents_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _IEpoEventInfEvents_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define _IEpoEventInfEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define _IEpoEventInfEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define _IEpoEventInfEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___IEpoEventInfEvents_DISPINTERFACE_DEFINED__ */


#ifndef __IEventElement_INTERFACE_DEFINED__
#define __IEventElement_INTERFACE_DEFINED__

/* interface IEventElement */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IEventElement;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("146A29BB-9EBF-4ED3-AB80-D6CC892D474C")
    IEventElement : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetAttributeString( 
            BSTR strName,
            BSTR ValueName) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetAttributeInt( 
            BSTR strName,
            long ValueName) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE AppendElement( 
            BSTR strName,
            BSTR strValue,
            /* [out] */ IEventElement **pElement) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE AddElementInto( 
            BSTR strName,
            BSTR strValue,
            /* [out] */ IEventElement **pElement) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEventElementVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IEventElement * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IEventElement * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IEventElement * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IEventElement * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IEventElement * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IEventElement * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IEventElement * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetAttributeString )( 
            IEventElement * This,
            BSTR strName,
            BSTR ValueName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetAttributeInt )( 
            IEventElement * This,
            BSTR strName,
            long ValueName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *AppendElement )( 
            IEventElement * This,
            BSTR strName,
            BSTR strValue,
            /* [out] */ IEventElement **pElement);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *AddElementInto )( 
            IEventElement * This,
            BSTR strName,
            BSTR strValue,
            /* [out] */ IEventElement **pElement);
        
        END_INTERFACE
    } IEventElementVtbl;

    interface IEventElement
    {
        CONST_VTBL struct IEventElementVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEventElement_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEventElement_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEventElement_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEventElement_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IEventElement_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IEventElement_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IEventElement_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IEventElement_SetAttributeString(This,strName,ValueName)	\
    (This)->lpVtbl -> SetAttributeString(This,strName,ValueName)

#define IEventElement_SetAttributeInt(This,strName,ValueName)	\
    (This)->lpVtbl -> SetAttributeInt(This,strName,ValueName)

#define IEventElement_AppendElement(This,strName,strValue,pElement)	\
    (This)->lpVtbl -> AppendElement(This,strName,strValue,pElement)

#define IEventElement_AddElementInto(This,strName,strValue,pElement)	\
    (This)->lpVtbl -> AddElementInto(This,strName,strValue,pElement)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IEventElement_SetAttributeString_Proxy( 
    IEventElement * This,
    BSTR strName,
    BSTR ValueName);


void __RPC_STUB IEventElement_SetAttributeString_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IEventElement_SetAttributeInt_Proxy( 
    IEventElement * This,
    BSTR strName,
    long ValueName);


void __RPC_STUB IEventElement_SetAttributeInt_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IEventElement_AppendElement_Proxy( 
    IEventElement * This,
    BSTR strName,
    BSTR strValue,
    /* [out] */ IEventElement **pElement);


void __RPC_STUB IEventElement_AppendElement_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IEventElement_AddElementInto_Proxy( 
    IEventElement * This,
    BSTR strName,
    BSTR strValue,
    /* [out] */ IEventElement **pElement);


void __RPC_STUB IEventElement_AddElementInto_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEventElement_INTERFACE_DEFINED__ */


#ifndef __IEpoEventInf2_INTERFACE_DEFINED__
#define __IEpoEventInf2_INTERFACE_DEFINED__

/* interface IEpoEventInf2 */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IEpoEventInf2;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("2D870730-7363-4CFD-BB42-8841D8515032")
    IEpoEventInf2 : public IEpoEventInf
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetMachineElement( 
            /* [out] */ IEventElement **pIMachineNode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetSoftwareElement( 
            /* [out] */ IEventElement **pISoftwareNode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE CreateEventElement( 
            BSTR strEventNodeName,
            long lEventID,
            long lSeverity,
            DATE GMTTime,
            /* [out] */ IEventElement **pIEventElemNode) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SaveAgentEventOnly( 
            /* [in] */ BSTR strSoftwareID,
            /* [in] */ BSTR strFileExtension) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpoEventInf2Vtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IEpoEventInf2 * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IEpoEventInf2 * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IEpoEventInf2 * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IEpoEventInf2 * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IEpoEventInf2 * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IEpoEventInf2 * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IEpoEventInf2 * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Initialize )( 
            IEpoEventInf2 * This,
            BSTR strSoftwareID,
            BSTR strRootName,
            BSTR strSoftwareNodeName,
            BSTR strSoftwareName,
            BSTR strVersion,
            BSTR strProductFamily);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *DeInitialize )( 
            IEpoEventInf2 * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetMachineElement )( 
            IEpoEventInf2 * This,
            /* [out] */ IUnknown **pIMachineNode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetSoftwareElement )( 
            IEpoEventInf2 * This,
            /* [out] */ IUnknown **pISoftwareNode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CreateEventElement )( 
            IEpoEventInf2 * This,
            BSTR strEventNodeName,
            long lEventID,
            long lSeverity,
            DATE GMTTime,
            /* [out] */ IUnknown **pIEventElemNode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Save )( 
            IEpoEventInf2 * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CreateMachineElement )( 
            IEpoEventInf2 * This,
            BSTR szIPAddress,
            BSTR szOS,
            BSTR szUserName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetMachineElement )( 
            IEpoEventInf2 * This,
            /* [out] */ IEventElement **pIMachineNode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetSoftwareElement )( 
            IEpoEventInf2 * This,
            /* [out] */ IEventElement **pISoftwareNode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CreateEventElement )( 
            IEpoEventInf2 * This,
            BSTR strEventNodeName,
            long lEventID,
            long lSeverity,
            DATE GMTTime,
            /* [out] */ IEventElement **pIEventElemNode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SaveAgentEventOnly )( 
            IEpoEventInf2 * This,
            /* [in] */ BSTR strSoftwareID,
            /* [in] */ BSTR strFileExtension);
        
        END_INTERFACE
    } IEpoEventInf2Vtbl;

    interface IEpoEventInf2
    {
        CONST_VTBL struct IEpoEventInf2Vtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpoEventInf2_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpoEventInf2_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpoEventInf2_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpoEventInf2_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IEpoEventInf2_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IEpoEventInf2_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IEpoEventInf2_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IEpoEventInf2_Initialize(This,strSoftwareID,strRootName,strSoftwareNodeName,strSoftwareName,strVersion,strProductFamily)	\
    (This)->lpVtbl -> Initialize(This,strSoftwareID,strRootName,strSoftwareNodeName,strSoftwareName,strVersion,strProductFamily)

#define IEpoEventInf2_DeInitialize(This)	\
    (This)->lpVtbl -> DeInitialize(This)

#define IEpoEventInf2_GetMachineElement(This,pIMachineNode)	\
    (This)->lpVtbl -> GetMachineElement(This,pIMachineNode)

#define IEpoEventInf2_GetSoftwareElement(This,pISoftwareNode)	\
    (This)->lpVtbl -> GetSoftwareElement(This,pISoftwareNode)

#define IEpoEventInf2_CreateEventElement(This,strEventNodeName,lEventID,lSeverity,GMTTime,pIEventElemNode)	\
    (This)->lpVtbl -> CreateEventElement(This,strEventNodeName,lEventID,lSeverity,GMTTime,pIEventElemNode)

#define IEpoEventInf2_Save(This)	\
    (This)->lpVtbl -> Save(This)

#define IEpoEventInf2_CreateMachineElement(This,szIPAddress,szOS,szUserName)	\
    (This)->lpVtbl -> CreateMachineElement(This,szIPAddress,szOS,szUserName)


#define IEpoEventInf2_GetMachineElement(This,pIMachineNode)	\
    (This)->lpVtbl -> GetMachineElement(This,pIMachineNode)

#define IEpoEventInf2_GetSoftwareElement(This,pISoftwareNode)	\
    (This)->lpVtbl -> GetSoftwareElement(This,pISoftwareNode)

#define IEpoEventInf2_CreateEventElement(This,strEventNodeName,lEventID,lSeverity,GMTTime,pIEventElemNode)	\
    (This)->lpVtbl -> CreateEventElement(This,strEventNodeName,lEventID,lSeverity,GMTTime,pIEventElemNode)

#define IEpoEventInf2_SaveAgentEventOnly(This,strSoftwareID,strFileExtension)	\
    (This)->lpVtbl -> SaveAgentEventOnly(This,strSoftwareID,strFileExtension)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IEpoEventInf2_GetMachineElement_Proxy( 
    IEpoEventInf2 * This,
    /* [out] */ IEventElement **pIMachineNode);


void __RPC_STUB IEpoEventInf2_GetMachineElement_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IEpoEventInf2_GetSoftwareElement_Proxy( 
    IEpoEventInf2 * This,
    /* [out] */ IEventElement **pISoftwareNode);


void __RPC_STUB IEpoEventInf2_GetSoftwareElement_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IEpoEventInf2_CreateEventElement_Proxy( 
    IEpoEventInf2 * This,
    BSTR strEventNodeName,
    long lEventID,
    long lSeverity,
    DATE GMTTime,
    /* [out] */ IEventElement **pIEventElemNode);


void __RPC_STUB IEpoEventInf2_CreateEventElement_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IEpoEventInf2_SaveAgentEventOnly_Proxy( 
    IEpoEventInf2 * This,
    /* [in] */ BSTR strSoftwareID,
    /* [in] */ BSTR strFileExtension);


void __RPC_STUB IEpoEventInf2_SaveAgentEventOnly_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpoEventInf2_INTERFACE_DEFINED__ */


#ifndef __IEpoEventInf3_INTERFACE_DEFINED__
#define __IEpoEventInf3_INTERFACE_DEFINED__

/* interface IEpoEventInf3 */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IEpoEventInf3;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("BD1C616F-0298-49af-B7EE-811DF725D187")
    IEpoEventInf3 : public IEpoEventInf2
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Initialize3( 
            BSTR strSoftwareID,
            BSTR strRootName,
            BSTR strSoftwareNodeName,
            BSTR strSoftwareName,
            BSTR strVersion,
            BSTR strProductFamily) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpoEventInf3Vtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IEpoEventInf3 * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IEpoEventInf3 * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IEpoEventInf3 * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IEpoEventInf3 * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IEpoEventInf3 * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IEpoEventInf3 * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IEpoEventInf3 * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Initialize )( 
            IEpoEventInf3 * This,
            BSTR strSoftwareID,
            BSTR strRootName,
            BSTR strSoftwareNodeName,
            BSTR strSoftwareName,
            BSTR strVersion,
            BSTR strProductFamily);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *DeInitialize )( 
            IEpoEventInf3 * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetMachineElement )( 
            IEpoEventInf3 * This,
            /* [out] */ IUnknown **pIMachineNode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetSoftwareElement )( 
            IEpoEventInf3 * This,
            /* [out] */ IUnknown **pISoftwareNode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CreateEventElement )( 
            IEpoEventInf3 * This,
            BSTR strEventNodeName,
            long lEventID,
            long lSeverity,
            DATE GMTTime,
            /* [out] */ IUnknown **pIEventElemNode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Save )( 
            IEpoEventInf3 * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CreateMachineElement )( 
            IEpoEventInf3 * This,
            BSTR szIPAddress,
            BSTR szOS,
            BSTR szUserName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetMachineElement )( 
            IEpoEventInf3 * This,
            /* [out] */ IEventElement **pIMachineNode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetSoftwareElement )( 
            IEpoEventInf3 * This,
            /* [out] */ IEventElement **pISoftwareNode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CreateEventElement )( 
            IEpoEventInf3 * This,
            BSTR strEventNodeName,
            long lEventID,
            long lSeverity,
            DATE GMTTime,
            /* [out] */ IEventElement **pIEventElemNode);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SaveAgentEventOnly )( 
            IEpoEventInf3 * This,
            /* [in] */ BSTR strSoftwareID,
            /* [in] */ BSTR strFileExtension);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Initialize3 )( 
            IEpoEventInf3 * This,
            BSTR strSoftwareID,
            BSTR strRootName,
            BSTR strSoftwareNodeName,
            BSTR strSoftwareName,
            BSTR strVersion,
            BSTR strProductFamily);
        
        END_INTERFACE
    } IEpoEventInf3Vtbl;

    interface IEpoEventInf3
    {
        CONST_VTBL struct IEpoEventInf3Vtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpoEventInf3_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpoEventInf3_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpoEventInf3_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpoEventInf3_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IEpoEventInf3_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IEpoEventInf3_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IEpoEventInf3_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IEpoEventInf3_Initialize(This,strSoftwareID,strRootName,strSoftwareNodeName,strSoftwareName,strVersion,strProductFamily)	\
    (This)->lpVtbl -> Initialize(This,strSoftwareID,strRootName,strSoftwareNodeName,strSoftwareName,strVersion,strProductFamily)

#define IEpoEventInf3_DeInitialize(This)	\
    (This)->lpVtbl -> DeInitialize(This)

#define IEpoEventInf3_GetMachineElement(This,pIMachineNode)	\
    (This)->lpVtbl -> GetMachineElement(This,pIMachineNode)

#define IEpoEventInf3_GetSoftwareElement(This,pISoftwareNode)	\
    (This)->lpVtbl -> GetSoftwareElement(This,pISoftwareNode)

#define IEpoEventInf3_CreateEventElement(This,strEventNodeName,lEventID,lSeverity,GMTTime,pIEventElemNode)	\
    (This)->lpVtbl -> CreateEventElement(This,strEventNodeName,lEventID,lSeverity,GMTTime,pIEventElemNode)

#define IEpoEventInf3_Save(This)	\
    (This)->lpVtbl -> Save(This)

#define IEpoEventInf3_CreateMachineElement(This,szIPAddress,szOS,szUserName)	\
    (This)->lpVtbl -> CreateMachineElement(This,szIPAddress,szOS,szUserName)


#define IEpoEventInf3_GetMachineElement(This,pIMachineNode)	\
    (This)->lpVtbl -> GetMachineElement(This,pIMachineNode)

#define IEpoEventInf3_GetSoftwareElement(This,pISoftwareNode)	\
    (This)->lpVtbl -> GetSoftwareElement(This,pISoftwareNode)

#define IEpoEventInf3_CreateEventElement(This,strEventNodeName,lEventID,lSeverity,GMTTime,pIEventElemNode)	\
    (This)->lpVtbl -> CreateEventElement(This,strEventNodeName,lEventID,lSeverity,GMTTime,pIEventElemNode)

#define IEpoEventInf3_SaveAgentEventOnly(This,strSoftwareID,strFileExtension)	\
    (This)->lpVtbl -> SaveAgentEventOnly(This,strSoftwareID,strFileExtension)


#define IEpoEventInf3_Initialize3(This,strSoftwareID,strRootName,strSoftwareNodeName,strSoftwareName,strVersion,strProductFamily)	\
    (This)->lpVtbl -> Initialize3(This,strSoftwareID,strRootName,strSoftwareNodeName,strSoftwareName,strVersion,strProductFamily)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IEpoEventInf3_Initialize3_Proxy( 
    IEpoEventInf3 * This,
    BSTR strSoftwareID,
    BSTR strRootName,
    BSTR strSoftwareNodeName,
    BSTR strSoftwareName,
    BSTR strVersion,
    BSTR strProductFamily);


void __RPC_STUB IEpoEventInf3_Initialize3_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpoEventInf3_INTERFACE_DEFINED__ */


#ifndef __IEpoEventInfCallback_INTERFACE_DEFINED__
#define __IEpoEventInfCallback_INTERFACE_DEFINED__

/* interface IEpoEventInfCallback */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IEpoEventInfCallback;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("A2153C51-2E2E-442E-BD1A-5058B41965C5")
    IEpoEventInfCallback : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE RegisterEpoEventCallBack( 
            /* [in] */ void *lEpoEventPtr) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpoEventInfCallbackVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IEpoEventInfCallback * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IEpoEventInfCallback * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IEpoEventInfCallback * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IEpoEventInfCallback * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IEpoEventInfCallback * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IEpoEventInfCallback * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IEpoEventInfCallback * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RegisterEpoEventCallBack )( 
            IEpoEventInfCallback * This,
            /* [in] */ void *lEpoEventPtr);
        
        END_INTERFACE
    } IEpoEventInfCallbackVtbl;

    interface IEpoEventInfCallback
    {
        CONST_VTBL struct IEpoEventInfCallbackVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpoEventInfCallback_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpoEventInfCallback_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpoEventInfCallback_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpoEventInfCallback_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IEpoEventInfCallback_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IEpoEventInfCallback_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IEpoEventInfCallback_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IEpoEventInfCallback_RegisterEpoEventCallBack(This,lEpoEventPtr)	\
    (This)->lpVtbl -> RegisterEpoEventCallBack(This,lEpoEventPtr)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IEpoEventInfCallback_RegisterEpoEventCallBack_Proxy( 
    IEpoEventInfCallback * This,
    /* [in] */ void *lEpoEventPtr);


void __RPC_STUB IEpoEventInfCallback_RegisterEpoEventCallBack_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpoEventInfCallback_INTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_EpoEventInf;

#ifdef __cplusplus

class DECLSPEC_UUID("0D7C69CB-CF5F-4594-8FEE-0B6DDA9F75D6")
EpoEventInf;
#endif

EXTERN_C const CLSID CLSID_EventElement;

#ifdef __cplusplus

class DECLSPEC_UUID("528F66A7-7891-4F6B-8F1E-5132CC80650C")
EventElement;
#endif

EXTERN_C const CLSID CLSID_EpoEventInfCallback;

#ifdef __cplusplus

class DECLSPEC_UUID("5284E46B-63F2-4D12-9434-88B7899EA7DD")
EpoEventInfCallback;
#endif
#endif /* __GENEVTINFLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long *, BSTR * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


