/*******************************************************************************************************
 * $Workfile: poplugin.h $ : File
 *  Copyright (C) 2009 McAfee, Inc.  All rights reserved.
 * All rights reserved.
 *
 * 
 * Description: contains the supported methods for use in creating a ePO plugin
 *
 * Author: 
 * E-Mail: 
 *
 * $Log: poplugin.h,v $
 * Revision 1.2  2005/02/17 21:09:39  mthompso
 * Various changes to support passing policy objects to plugins.
 *
 * Revision 1.1  2004/10/26 23:23:29  GWeber
 * Initial checkin of CML, CMA, MSE, and NWA code
 *
 * Revision 1.1.1.1.4.1  2003/09/12 02:08:26  gweber
 * Merged in EPO 3.0 SP1 Release.
 * MSE 	2.1.1.150
 * CML 	3.1.1.159
 * CMA 	3.1.1.184
 * SIM 	1.1.1.147
 * EPO 	3.0.1.150
 *
 * 
 * 3     7/03/02 7:26a Ehamilton
 * multiple bug fixes to framework installer
 * 
 * 2     7/01/02 1:59p Ehamilton
 * 
 * 1     4/18/02 10:15a Ehamilton
 * 
 * 7     1/25/02 5:30p Ehamilton
 * added strings for plugin names
 * 
 * 6     1/24/02 4:18p Ehamilton
 * added definitions for plugin names
 * 
 * 5     1/18/02 11:00a Ehamilton
 * tweaked getproductdata
 * 
 * 4     1/03/02 10:12a Ehamilton
 * updated document, now contains EPO 3.0 API
 * 
 ******************************************************************************************************/

#ifndef POPLUGIN_H
#define POPLUGIN_H

#include <tchar.h>

#define MAX_APP_DATA		3072

//=====================================
// For ePO Agent Version 1.0 - 3.0
//=====================================

#define POPLUGIN_EPO_AGENT_2X				_T("ePOAgent2000")
#define POPLUGIN_EPO_AGENT_3X				_T("EPOAGENT3000")

//=====================================
// For ePO System Properties 1.0 (McAfee Agent 3.0+ )
//=====================================
#define POPLUGIN_MCAFEE_SYSPROPS_1X			_T("SYSPROPS1000")		

//=====================================
// For Common Updater
//=====================================
#define POPLUGIN_MCAFEE_COMMON_UPDATE_3X	_T("CMNUPD__3000")		

//=====================================
// C++ Policy Object Interface
//=====================================

template <class TYPE>
class CMAPtr
{
    TYPE *ptr_;
public:
    inline CMAPtr ()
    {
        ptr_ = NULL;
    }

    inline void deletePtr (
        void)
    {
        if (ptr_ != NULL)
        {
         /* Don't use the calling program's 'delete' function -- it may be from
            a different run-time library and have a different heap from the one
            CMA allocated ptr_ from. */
            ptr_->deleteThis ();
        }
    }

    inline ~CMAPtr ()
    {
        deletePtr ();
    }

    inline void operator = (
        TYPE *ptr)
    {
        deletePtr ();
        ptr_ = ptr;
    }

    inline operator TYPE *&() {return ptr_;}

    inline TYPE *operator -> () const {return ptr_;}
};

typedef CMAPtr<class ICMAString> CMAStringPtr;
typedef CMAPtr<class ICMAPolicyObjectList> CMAPolicyObjectListPtr;
typedef CMAPtr<class ICMAPolicySettingObjectList> CMAPolicySettingObjectListPtr;
typedef CMAPtr<class ICMAPolicySettingList> CMAPolicySettingListPtr;

class ICMAString
{
public:
    virtual wchar_t *get (
        void) = 0;

    virtual ~ICMAString (
        void) = 0
    {
    }

    virtual void deleteThis (
        void) = 0;
};

class ICMAPolicySettingList
{
public:
    virtual bool get (
        wchar_t *sectionName,
        wchar_t *settingName,
        CMAStringPtr &value) = 0;

    virtual bool getNext (
        CMAStringPtr &sectionName,
        CMAStringPtr &settingName,
        CMAStringPtr &value) = 0;

    virtual bool reset (
        void) = 0;

    virtual void deleteThis (
        void) = 0;
};

class ICMAPolicySettingObjectList
{
public:
    virtual bool getNext (
        long &paramInt,
        CMAStringPtr &paramStr,
        CMAStringPtr &name,
        CMAPolicySettingListPtr &policySettingList) = 0;

    virtual bool reset (
        void) = 0;

    virtual void deleteThis (
        void) = 0;
};

class ICMAPolicyObjectList
{
public:
    virtual bool getNext (
        CMAStringPtr &feature,
        CMAStringPtr &category,
        CMAStringPtr &type,
        CMAStringPtr &name,
        CMAPolicySettingObjectListPtr &policySettingObjectList) = 0;

    virtual void getSoftwareId (
        CMAStringPtr &softwareId) = 0;

    virtual bool reset (
        void) = 0;

    virtual void deleteThis (
        void) = 0;
};


#ifdef __cplusplus
extern "C" {
#endif // #ifdef __cplusplus

enum 
{
    VAL_STRING     = 0x00,
    VAL_DWORD      = 0x01,
    VAL_INT64      = 0x02,
    VAL_BINARY     = 0x03
};

typedef enum tagPluginFlag
{
	doAll				= 0x0000,
    dontCollectProps	= 0x0001,
    dontEnforcePolicy	= 0x0002,
    dontRunTask			= 0x0004,
	dontLoadPlugin		= 0x0008
} PLUGINFLAG;

//////////////////////////////////////////////////////////////////////////////
// 
// 


//////////////////////////////////////////////////////////////////////////////
// UNICODE METHODS
//
#define POPLUGIN_GETPROPERTIESW         "POPLUGIN_GetPropertiesW"
#define POPLUGIN_ENFORCEPOLICYW         "POPLUGIN_EnforcePolicyW" // Don't use this interface as it deprecated. Instead use POPLUGIN_EnforcePolicyObjects
#define POPLUGIN_ENFORCETASKW           "POPLUGIN_EnforceTaskW"
#define POPLUGIN_INITIALIZEW			"POPLUGIN_InitializeW"			// EPO 3.0
#define POPLUGIN_DEINITIALIZEW			"POPLUGIN_DeinitializeW"		// EPO 3.0


//////////////////////////////////////////////////////////////////////////////
// ANSI METHODS
//
#define POPLUGIN_GETPROPERTIES          "POPLUGIN_GetProperties"		
#define POPLUGIN_ENFORCEPOLICY          "POPLUGIN_EnforcePolicy"
#define POPLUGIN_ENFORCETASK            "POPLUGIN_EnforceTask"
#define POPLUGIN_INITIALIZEA			"POPLUGIN_InitializeA"			// EPO 3.0
#define POPLUGIN_DEINITIALIZEA			"POPLUGIN_DeinitializeA"		// EPO 3.0


//////////////////////////////////////////////////////////////////////////////
// GENERIC METHODS
//
#define POPLUGIN_GETTASKSTATUS          "POPLUGIN_GetTaskStatus"
#define POPLUGIN_STOPTASK               "POPLUGIN_StopTask"
#define POPLUGIN_FREECOOKIE             "POPLUGIN_FreeCookie"
#define POPLUGIN_ISSOFTWAREINSTALLED	"POPLUGIN_IsSoftwareInstalled"
#define POPLUGIN_ISINTERFACEUNICODE     "POPLUGIN_IsInterfaceUnicode"
#define POPLUGIN_GETTASKSTATUSEX        "POPLUGIN_GetTaskStatusEx"		// EPO 2.5
#define POPLUGIN_ENFORCEPOLICYOBJECT	"POPLUGIN_EnforcePolicyObject"  // EPO 3.6

//////////////////////////////////////////////////////////////////////////////
// 
// 


//////////////////////////////////////////////////////////////////////////////
// UNICODE INTERFACE
//
typedef BOOL (WINAPI *PO_SETPROP_PROCW)			( LPCTSTR lpszSoftwareID, LPCTSTR lpszSectionName, LPCTSTR lpszKeyName, CONST BYTE * lpValue, int iValType, DWORD dwSize );
typedef BOOL (WINAPI *PO_GETPOLICY_PROCW)		( LPCTSTR lpszSoftwareID, LPCTSTR lpszSectionName, LPCTSTR lpszKeyName, LPBYTE lpValue, int iValType, LPDWORD lpdwSize );
typedef BOOL (WINAPI *PO_GETTASKOPTION_PROCW)	( LPCTSTR lpszSoftwareID, LPCTSTR lpszTaskID, LPCTSTR lpszSectionName, LPCTSTR lpszKeyName, LPBYTE lpValue, int iValType, LPDWORD lpdwSize );

typedef BOOL (*POPLUGIN_GETPROPERTIES_PROCW)( LPCTSTR lpszSoftwareID, PO_SETPROP_PROCW fnSetPropProcW );
typedef BOOL (*POPLUGIN_ENFORCEPOLICY_PROCW)( LPCTSTR lpszSoftwareID, PO_GETPOLICY_PROCW fnGetPolicyProcW );
typedef BOOL (*POPLUGIN_ENFORCETASK_PROCW)	( LPCTSTR lpszSoftwareID, LPCTSTR lpszTaskID, PO_GETTASKOPTION_PROCW fnGetTaskOptionProcW, LPVOID *lplpCookie );

typedef HRESULT (*POPLUGIN_DEINITIALIZE_PROCW)	( LPCWSTR lpszSoftwareID );											// EPO 3.0
typedef HRESULT (*POPLUGIN_INITIALIZE_PROCW)	( LPCWSTR lpszSoftwareID );											// EPO 3.0


//////////////////////////////////////////////////////////////////////////////
// ANSI INTERFACE
//
typedef BOOL (WINAPI *PO_SETPROP_PROC)		( LPCSTR lpszSoftwareID, LPCSTR lpszSectionName, LPCSTR lpszKeyName, CONST BYTE * lpValue, int iValType, DWORD dwSize );
typedef BOOL (WINAPI *PO_GETPOLICY_PROC)	( LPCSTR lpszSoftwareID, LPCSTR lpszSectionName, LPCSTR lpszKeyName, LPBYTE lpValue, int iValType, LPDWORD lpdwSize );
typedef BOOL (WINAPI *PO_GETTASKOPTION_PROC)( LPCSTR lpszSoftwareID, LPCSTR lpszTaskID, LPCSTR lpszSectionName, LPCSTR lpszKeyName, LPBYTE lpValue, int iValType, LPDWORD lpdwSize );

typedef BOOL (*POPLUGIN_GETPROPERTIES_PROC)	( LPCSTR lpszSoftwareID, PO_SETPROP_PROC fnSetPropProc );
typedef BOOL (*POPLUGIN_ENFORCEPOLICY_PROC)	( LPCSTR lpszSoftwareID, PO_GETPOLICY_PROC fnGetPolicyProc );
typedef BOOL (*POPLUGIN_ENFORCETASK_PROC)	( LPCSTR lpszSoftwareID, LPCSTR lpszTaskID, PO_GETTASKOPTION_PROC fnGetTaskOptionProc, LPVOID *lplpCookie );

typedef HRESULT (*POPLUGIN_DEINITIALIZE_PROCA)	( LPCSTR lpszSoftwareID );										// EPO 3.0
typedef HRESULT (*POPLUGIN_INITIALIZE_PROCA)	( LPCSTR lpszSoftwareID );										// EPO 3.0


//////////////////////////////////////////////////////////////////////////////
// GENERIC INTERFACE
//
typedef int  (*POPLUGIN_GETTASKSTATUS_PROC)			( LPVOID lpCookie );
typedef BOOL (*POPLUGIN_STOPTASK_PROC)				( LPVOID lpCookie );
typedef BOOL (*POPLUGIN_FREECOOKIE_PROC)			( LPVOID lpCookie ); 
typedef BOOL (*POPLUGIN_ISSOFTWAREINSTALLED_PROC)	( void );
typedef BOOL (*POPLUGIN_ISINTERFACEUNICODE_PROC)	( void );
typedef int  (*POPLUGIN_GETTASKSTATUSEX_PROC)		( LPVOID lpCookie );		// EPO 2.5
typedef bool (*POPLUGIN_ENFORCEPOLICYOBJECT_PROC)   ( ICMAPolicyObjectList *policyObjectList ); // EPO 3.6



//////////////////////////////////////////////////////////////////////////////
// 
// 


//////////////////////////////////////////////////////////////////////////////
// Description: This lets the agent know that this plugin now supports the
//              UNICODE interface
//
BOOL POPLUGIN_IsInterfaceUnicode(void);


//////////////////////////////////////////////////////////////////////////////
// Description: The PO agent periodically gathers properties from 
//              installed point products and put it in an ini file 
//              called PROPS.INI and send it to the server. This 
//              POPLUGIN_GetProperties function is the interface 
//              that gets called by the PO Agent to collect the 
//              properties of the specific point product. Properties 
//              can be the product version, engine version and product 
//              configurations and so forth. The point product should 
//              implement this function by calling the passed in fnSetPropProc 
//              function to relay the properties to Agent.
//
BOOL POPLUGIN_GetProperties(LPCTSTR lpszSoftwareID, PO_SETPROP_PROCW fnSetPropProcW);


//////////////////////////////////////////////////////////////////////////////
// Description: The PO administrator can use the PO Console to set policies 
//              to groups, users and computers. Whenever there're new policies 
//              available for the specific computer, it will be sent over to 
//              the Agent. Then, the agent will call the function POPLUGIN_EnforcePolicy. 
//              POPLUGIN_EnforcePolicy reads the policy settings by calling fnGetPolicyProc 
//              and enforce them to the agnet local machine. 
//
//
BOOL POPLUGIN_EnforcePolicy(LPCTSTR lpszSoftwareID, PO_GETPOLICY_PROCW fnGetPolicyProcW);

 
//////////////////////////////////////////////////////////////////////////////
// Description: The PO Console can schedule tasks for groups, users and 
//              computers. Whenever there're new or changed tasks for the computer. 
//              It'll get sent to the Agent. Agent then calls the POPLUGIN_EnforceTask 
//              function,  then POPLUGIN_EnforceTask reads the task settings by calling
//              lpGetTaskOption for the specific point product and execute the task 
//              with the settings. The task settings can be the point product configurations 
//              to run the task. The product should return a cookie to agent to identify
//              current task. 
//
BOOL POPLUGIN_EnforceTask			(LPCTSTR lpszSoftwareID, LPCTSTR lpszTaskID, PO_GETTASKOPTION_PROCW fnGetTaskOptionProcW, LPVOID *lplpCookie);
int  POPLUGIN_GetTaskStatus			(LPVOID lpCookie);
BOOL POPLUGIN_StopTask				(LPVOID lpCookie);
BOOL POPLUGIN_FreeCookie			(LPVOID lpCookie);
BOOL POPLUGIN_IsSoftwareInstalled	(void);
int  POPLUGIN_GetTaskStatusEx		(LPVOID lpCookie);			// EPO 2.5


//////////////////////////////////////////////////////////////////////////////
// Description: The initialize/deinitalize methods are provided so that plugins
//				need not load libraries in DllMain. The plugin can count on these
//				methods being called before any other methods are used. The plugin
//				need not worry about reference counting these methods as the plugin
//				manager will take care of this. If the deinitialize method is called
//				the plugin should release any memory and libraries it has loaded 
//				regardless of how many times initialize has been called.
//
HRESULT POPLUGIN_InitializeA	(LPCSTR lpszSoftwareID);		// EPO 3.0
HRESULT POPLUGIN_DeinitializeA	(LPCSTR lpszSoftwareID);		// EPO 3.0
HRESULT POPLUGIN_InitializeW	(LPCWSTR lpszSoftwareID);		// EPO 3.0
HRESULT POPLUGIN_DeinitializeW	(LPCWSTR lpszSoftwareID);		// EPO 3.0


//////////////////////////////////////////////////////////////////////////////
// Description: Policy object enforcement.
//
bool POPLUGIN_EnforcePolicyObject (ICMAPolicyObjectList *policyObjectList); // EPO 3.6


//////////////////////////////////////////////////////////////////////////////
// Description: Ehamilton 01-03-2002 - These strings should not be used, there is no 
//				guarantee that these files will exist in future releases of the ePO agent.
//				Also: String decoding is now handled by the product manager, the plugin
//				no longer needs to worry about encryption unless it is product specific.
//
#define NAI_AGENT_SHARE_DLL             "nagshr32.dll"
#define NAGSHR_DECODESTRING             "DecodeString"
typedef int (WINAPI *NAGSHR_DECODESTRING_PROC)(CHAR * pbInData, DWORD dwInDataSize, CHAR * pbOutData, DWORD dwOutDataSize, DWORD * pnDecodedDataSize);



//
// Following definations are used all the plug-ins to return Licensing info 
//

#define EPO_PROPS_STR_LICENSED				_T("Licensed")
#define EPO_PROPS_STR_LICENSEEXPIRED		_T("LicenseExpired")
#define EPO_PROPS_STR_EVALUATION			_T("Evaluation")
#define EPO_PROPS_STR_EVALUATIONEXPIRED		_T("EvaluationExpired")
#define EPO_PROPS_STR_UNREGISTERED			_T("Unregistered")
#define EPO_PROPS_STR_UNREGISTEREDEXPIRED	_T("UnregisteredExpired")
#define EPO_PROPS_STR_NONACTIVATED			_T("NonActivated")
#define EPO_PROPS_STR_UNKNOWN				_T("Unknown")



#ifdef __cplusplus
}
#endif // #ifdef __cplusplus
#endif // #ifndef POPLUGIN_H
