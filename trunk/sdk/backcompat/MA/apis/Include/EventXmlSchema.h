/* This file defines the identifiers used in event XML files. On the sending 
   side, these are created automatically by the IEpoEventInf2 functions.

   On the receiving side, event parser plugins need to use these to extract
   data from event files.

        <?xml version="1.0" encoding="UTF-8"?>
        <YOUR_EVENT_ROOT>
          <MachineInfo>
            <MachineName>YOUR_COMPUTER_NAME</MachineName>
            <AgentGUID>YOUR_GUID</AgentGUID>
            <IPAddress>YOUR_IP</IPAddress>
            <OSName>YOUR_OS</OSName>
            <UserName>YOUR_USER_NAME</UserName>
            <TimeZoneBias>YOUR_BIAS</TimeZoneBias>
          </MachineInfo>
          <YOUR_SOFTWARE_ROOT ProductName="YOUR_PRODUCT_NAME" ProductVersion="YOUR_PRODUCT_VERSION" ProductFamily="YOUR_PRODUCT_FAMILY">
            <YOUR_EVENT_NAME>
              <EventID>YOUR_EVENT_ID</EventID>
              <Severity>YOUR_SEVERITY</Severity>
              <GMTTime>YOUR_TIME</GMTTime>
            </YOUR_EVENT_NAME>
          </YOUR_SOFTWARE_ROOT>
        </YOUR_EVENT_ROOT>
*/
// Machine info
#define NA_XML_EVENT_MACHINE_INFO					    TEXT("MachineInfo")
#define NA_XML_EVENT_MACHINE_NAME						TEXT("MachineName")
#define NA_XML_EVENT_AGENT_GUID							TEXT("AgentGUID")
#define NA_XML_EVENT_IP_ADDRESS							TEXT("IPAddress")
#define NA_XML_EVENT_OS_NAME							TEXT("OSName")
#define NA_XML_EVENT_USER_NAME							TEXT("UserName")
#define NA_XML_EVENT_TIMEZONE_BIAS						TEXT("TimeZoneBias")

// Software info
#define NA_XML_EVENT_PRODUCT_NAME						TEXT("ProductName")
#define NA_XML_EVENT_PRODUCT_VERSION					TEXT("ProductVersion")
#define NA_XML_EVENT_PRODUCT_FAMILY					    TEXT("ProductFamily")

// Event info
#define NA_XML_EVENT_EVENTID							TEXT("EventID")
#define NA_XML_EVENT_SEVERITY							TEXT("Severity")
#define NA_XML_EVENT_GMT_TIME							TEXT("GMTTime")
