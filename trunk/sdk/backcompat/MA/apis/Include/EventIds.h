// EventIDs.h
//
// Copyright (C) 2009 McAfee, Inc.  All rights reserved. 
//      
//

// NEVER RENUMBER SHARED PRODUCTS COUNT ON THESE VALUES

// Event Interface Event IDS -- comments are short message followed by long message

#define EVENT_FILLSTRUCTURES               0    // NONEVENT (just fill in the structures)

#define EVENT_VIRFOUND                  1024	// Infected file found.
   	// %SOFTWARENAME% %SOFTWAREVERSION%: The file %FILENAME% on %COMPUTERNAME% (IP Address: 
    // %SOURCEIP%) accessed by %USERNAME%,  is infected with the virus %VIRUSNAME%. Virus 
    // detected with Scan Engine %ENGINEVERSION% DAT version %DATVERSION%

#define EVENT_FILECLEANED	            1025	// Infected file successfully Cleaned.
    // %SOFTWARENAME% %SOFTWAREVERSION%: The file %FILENAME% on %COMPUTERNAME% accessed by 
    // %USERNAME%, was infected with the virus %VIRUSNAME%. The file was successfully cleaned 
    // with Scan engine version %ENGINEVERSION% DAT version %DATVERSION%. 

#define EVENT_FILECLEANERROR	        1026	// Unable to clean infected file.	
    // %SOFTWARENAME% %SOFTWAREVERSION%: The file %FILENAME% on %COMPUTERNAME% accessed by 
    // %USERNAME%,  is infected with the virus %VIRUSNAME%.  Unable to clean the file using 
    // the current Scan engine version %ENGINEVERSION% DAT version %DATVERSION%.   

#define EVENT_FILEDELETED	            1027	// Infected file deleted.
    // %SOFTWARENAME% %SOFTWAREVERSION%: The file %FILENAME% on %COMPUTERNAME% accessed by 
    // %USERNAME%, is infected with the virus %VIRUSNAME%.  The file was successfully deleted.  

#define EVENT_FILEDELETEERROR	        1028	// Unable to delete infected file.	
    // %SOFTWARENAME% %SOFTWAREVERSION%: The file %FILENAME% on %COMPUTERNAME% accessed by 
    // %USERNAME%, is infected with the virus %VIRUSNAME%.  Unable to delete the infected file.  

#define EVENT_FILEEXCLUDING	            1029	// File to be excluded from scans.	
    // %SOFTWARENAME% %SOFTWAREVERSION%: The file %FILENAME% on %COMPUTERNAME% will be excluded 
    // from further scans.

#define EVENT_FILEEXCLUDEERROR	        1030	// Unable to exclude item from scans.	
    // %SOFTWARENAME% %SOFTWAREVERSION%: Unable to exclude %FILENAME% on %COMPUTERNAME% from 
    // further scans.

#define EVENT_FILE_ACCESSERROR	        1031	// Infected file access denied.	
    // %SOFTWARENAME% %SOFTWAREVERSION%: The file %FILENAME% on %COMPUTERNAME% accessed by 
    // %USERNAME%, is infected with the virus %VIRUSNAME%. Access to the file was denied. 
    // Virus detected using Scan engine version %ENGINEVERSION% DAT version %DATVERSION%

#define EVENT_STATUS_QUARANTINED	    1032	// Infected file was moved to quarantine area.	
    // %SOFTWARENAME% %SOFTWAREVERSION%: The file %FILENAME% on %COMPUTERNAME% accessed 
    // by %USERNAME%, is infected with the virus %VIRUSNAME%.  The infected file was moved to 
    // quarantine area. Virus detected using Scan engine version %SCANENGINE% DAT version 
    // %DATVERSION%

#define EVENT_STATUS_QUARANTINEERROR	1033	// Unable to move infected file to quarantine.	
    // %SOFTWARENAME% %SOFTWAREVERSION%: The file %FILENAME% on %COMPUTERNAME% accessed by
    // %USERNAME%, is infected with the virus %VIRUSNAME%.  Unable to move the file to the 
    // quarantine area. Virus is detected by Scan engine version %ENGINEVERSION% DAT version 
    // %DATVERSION%

#define EVENT_NOVIRFOUND	            1034	// Scan completed. No viruses found.	
    // %SOFTWARENAME% %SOFTWAREVERSION%: The scan on %COMPUTERNAME% completed.  No infected files 
    // were found. Scan engine version used is %ENGINEVERSION% DAT version %DATVERSION%.

// engine related events

#define EVENT_VSERROR_SCANCANCELED	    1035	// Scan was cancelled.	
    // %SOFTWARENAME% %SOFTWAREVERSION%: The scan on %COMPUTERNAME% was cancelled by %USERNAME%
    // at time %GMTTIME%.

#define EVENT_VSERROR_MEMORYINFECTED	1036	// Virus found in Memory.	
    // %SOFTWARENAME% %SOFTWAREVERSION%: The scan on %COMPUTERNAME% found the system's memory
    // infected with a virus using Scan engine version %ENGINEVERSION% DAT version %DATVERSION%.

#define EVENT_VSERROR_BOOT_INFECTED	    1037	// Infected boot record found.	
    // %SOFTWARENAME% %SOFTWAREVERSION%: The scan on %COMPUTERNAME% found an infected boot 
    // record using Scan Engine version %ENGINEVERSION% DAT version %DATVERSION%.

#define EVENT_VSERROR_FILE_INFECTED	    1038	// Scan found infected files.	
    // %SOFTWARENAME% %SOFTWAREVERSION%: The scan on %COMPUTERNAME% found infected files. 
    // Scan engine version %ENGINEVERSION% DAT version %DATVERSION%"

#define EVENT_VSERROR_ALLCLEANED	    1039	// Scan found and cleaned infected files.	
    // %SOFTWARENAME% %SOFTWAREVERSION%: The scan on %COMPUTERNAME% found and cleaned infected 
    // files using Scan engine version %ENGINEVERSION% DAT version %DATVERSION%.  

#define EVENT_VSERROR_LOGFILE	        1040	// Error while accessing activity log file.	
    // %SOFTWARENAME% %SOFTWAREVERSION%: The scan on %COMPUTERNAME% reported an error accessing 
    // the activity log file  while scanning file %FILENAME%. 

#define EVENT_VSERROR_MEMORYALLOCATION	1041	// Scan reports memory allocation error.	
    // %SOFTWARENAME% %SOFTWAREVERSION%: The scan on %COMPUTERNAME% reported a memory allocation 
    // error while scanning file %FILENAME%.

#define EVENT_VSERROR_PATHTOLONG	    1042	// Directory length access error.	
    // %SOFTWARENAME% %SOFTWAREVERSION%: The directory path name is too long.  The scan on
    // %COMPUTERNAME% could not scan some items in the specified location.  Error occurred 
    // while scanning file %FILENAME%.

#define EVENT_VSERROR_WRITEPROTECT	    1043	// Media is write protected.	
    // %SOFTWARENAME% %SOFTWAREVERSION%: The scan on %COMPUTERNAME% could not access the media
    // due to write protection while scanning file %FILENAME%.

#define EVENT_VSERROR_NOMEDIA	        1044	// Specified media not found.	
    // %SOFTWARENAME% %SOFTWAREVERSION%: The scan on %COMPUTERNAME% could not find the specified
    // media while scanning file %FILENAME%.

#define EVENT_VSERROR_SCANITEM_INVALID	1045	// Specified scan item is invalid.	
    // %SOFTWARENAME% %SOFTWAREVERSION%: The scan on %COMPUTERNAME% found an invalid scan item 
    // while scanning file %FILENAME%.

#define EVENT_VSERROR_FILE	            1046	// File I/O errors.	
    // %SOFTWARENAME% %SOFTWAREVERSION%: The scan on %COMPUTERNAME% reported a file I/O error 
    // while scanning file %FILENAME%.

#define EVENT_VSERROR_DISK	            1047	// Disk I/O errors.	
    // %SOFTWARENAME% %SOFTWAREVERSION%: The scan on %COMPUTERNAME% reported a disk I/O error
    // while scanning file %FILENAME%.

#define EVENT_VSERROR_SYSTEM	        1048	// Scan reports general system error.	
    // %SOFTWARENAME% %SOFTWAREVERSION%: The scan on %COMPUTERNAME% reported a general system
    // error while scanning file %FILENAME%.

#define EVENT_VSERROR_INTERNAL	        1049	// Scan reported an internal application error.	
    // %SOFTWARENAME% %SOFTWAREVERSION%: The scan on %COMPUTERNAME% reported an internal 
    // application error while scanning file %FILENAME%.

#define EVENT_INFECTED_PASSORD_PROTECTED        1050

#define EVENT_NOT_SCANNED_PASSWORD_PROTECTED    1051

#define EVENT_INFECTED_BINDER_OBJECT            1052

#define EVENT_HEUR_INFECTED                     1053

#define EVENT_HEUR_DELETED                      1054

#define EVENT_HEUR_DELETEERROR                  1055

#define EVENT_HEUR_MOVED                        1056

#define EVENT_HEUR_MOVEERROR                    1057

#define EVENT_SCAN_TIMED_OUT                    1059

#define EVENT_BOOTCLEANED                       1060

#define EVENT_BOOTCLEANERROR                    1061

#define EVENT_ALERT_ERROR                       1062

#define EVENT_OPTION_ERROR                      1063

#define EVENT_SERVICE_STARTED                   1064

#define EVENT_SERVICE_ENDED                     1065

#define EVENT_SCHED_TASKSTARTOK                 1066

#define EVENT_SCHED_TASKSTARTERR                1067

#define EVENT_SCHED_TASKSTOPOK                  1068    

#define EVENT_SCHED_TASKSTOPERR                 1069

#define EVENT_TASK_SUCCESS                      1070

#define EVENT_TASK_CANCELLED                    1071

#define EVENT_TASK_ERRLOGFILE                   1076

#define EVENT_TASK_ERRMEMALLOC                  1077

#define EVENT_SCAN_PROCERROR                    1086

#define EVENT_SCAN_START                        1087    // on access

#define EVENT_SCAN_STOP                         1088    // on access

#define EVENT_SCAN_SETTINGS                     1089

#define EVENT_SCAN_STOP_UNEXPECTED				1090	// oas stopped unexpectedly

//1091 1093 VSE 7.5 Events maybe ???

// macro messages

#define EVENT_MACRO_DETECTED                    1100
#define EVENT_MACRO_DELETED                     1101


#define EVENT_VSERROR_UPGRADE_OK		1117	// we don't really know unless the setup exe returns a result

#define EVENT_VSERROR_UPDATE_OK         1118	// The update was successful	

#define EVENT_VSERROR_UPDATE_FAILED     1119	// The update failed; see event log	

#define EVENT_VSERROR_UPDATE_RUNNING    1120	// The update is running	

#define EVENT_VSERROR_UPDATE_CANCELLED  1121	// The update was cancelled	

#define EVENT_VSERROR_UPGRADE_RUNNING   1122	// The upgrade is running	

#define EVENT_VSERROR_UPGRADE_FAILED    1123	// The upgrade failed; see event log	

#define EVENT_VSERROR_UPGRADE_CANCELLED 1124	// The upgrade was cancelled	

#define EVENT_VSERROR_UPDATE_VERS_OLDER 1125	// The DAT version was not new enough	

#define EVENT_VSERROR_SCANCANCELLED_BY_UPDATE	1126	// Scan was cancelled by autoupdate of DAT files	

#define EVENT_SCANNER_DISABLED			1127

#define EVENT_VSERROR_INCSCANSTOPPED	1128	// incremental scan completed
#define EVENT_VSERROR_SCANCANCELLEDBYWINDOWS	1129	//scan was canceled as system shut down


// process related events 1200 - 1250

#define EVENT_PROCESS_STARTED	        1200	// Process started	

#define EVENT_PROCESS_ENDED	            1201	// Process Ended	

#define EVENT_SCAN_STARTED	            1202	// On-demand scan started	

#define EVENT_SCAN_ENDED	            1203	// On Demand scan ended	
#define EVENT_SCAN_SUMMARY           EVENT_SCAN_ENDED  // ON DEMAND 1090 was replaced with this duplicate message

#define EVENT_REPORT_OS_AND_SERIAL      1204    // report the OS and serial # if P3

// new messages for EPO 3  

#define EVENT_CLEANERR_N_Q 1270 // Clean error (N=no cleaner available), Q=quarantined successfully
#define EVENT_CLEANERR_H_Q 1271 // Clean error (H=heuristic detection), quarantined successfully
#define EVENT_CLEANERR_U_Q 1272 // Clean error (U=undetermined error), quarantined successfully
#define EVENT_CLEANERR_E_Q 1273 // Clean error (E=Encrypted file),         quarantined successfully

#define EVENT_CLEANERR_N_NQ 1274 // Clean error (no cleaner available), NQ=quarantine failed
#define EVENT_CLEANERR_H_NQ 1275 // Clean error (heuristic detection), quarantine failed
#define EVENT_CLEANERR_U_NQ 1276 // Clean error (undetermined error), quarantine failed
#define EVENT_CLEANERR_E_NQ 1277 // Clean error (E=Encrypted file),          quarantine failed


#define EVENT_CLEANERR_N_D 1278 // Clean error (no cleaner available), D=deleted successfully
#define EVENT_CLEANERR_H_D 1279 // Clean error (heuristic detection), deleted successfully
#define EVENT_CLEANERR_U_D 1280 // Clean error (undetermined error), deleted successfully
#define EVENT_CLEANERR_E_D 1281 // Clean error (E=Encrypted file),         deleted successfully

#define EVENT_CLEANERR_N_ND 1282 // Clean error (no cleaner available), ND=delete failed
#define EVENT_CLEANERR_H_ND 1283 // Clean error (heuristic detection), delete failed
#define EVENT_CLEANERR_U_ND 1284 // Clean error (undetermined error), delete failed
#define EVENT_CLEANERR_E_ND 1285 // Clean error (E=Encrypted file),          delete failed

#define EVENT_CLEANERR_N_C 1286 // Clean error (no cleaner available), C=continued scanning (ODS)
#define EVENT_CLEANERR_H_C 1287 // Clean error (heuristic detection), continued scanning (ODS)
#define EVENT_CLEANERR_U_C 1288 // Clean error (undetermined error), continued scanning (ODS)
#define EVENT_CLEANERR_E_C 1289 // Clean error (E=Encrypted file),         continued scanning 

// these are stupid defects as all messages have the subcomponent (either OAS or ODS)

#define EVENT_CLEANERR_N_C_OAS 1290 // Clean error (no cleaner available), C_OAS=denied access and continued (OAS)
#define EVENT_CLEANERR_H_C_OAS 1291 // Clean error (heuristic detection), denied access and continued (OAS)
#define EVENT_CLEANERR_U_C_OAS 1292 // Clean error (undetermined error), denied access and continued (OAS)

#define EVENT_MOVEERR_D  1293 // Move failed, deleted successfully
#define EVENT_MOVEERR_ND 1294 // Move failed, delete failed
#define EVENT_MOVEERR_C  1295 // Move failed, continued scanning (ODS)
#define EVENT_MOVEERR_C_OAS 1296 // Move failed, denied access and continued (OAS)

#define EVENT_DELETEERR_Q  1297 // Delete failed, quarantined
#define EVENT_DELETEERR_NQ 1298 // Delete failed, quarantine filed
#define EVENT_DELETEERR_C  1299 // Delete failed, continued scanning (ODS)
#define EVENT_DELETEERR_C_OAS 1300 // Delete failed, denied access and continued (OAS)





// virusscan events 1400-1499



// webshield SMTP events 1500-1699

#define EVENT_MAIL_VIRUSCLEANED	1500
//%SOFTWARENAME% %SOFTWAREVERSION%: An email for %MAILTONAME% (CC to %MAILCCNAME%) from %MAILFROMNAME% with the subject line %MAILSUBJECTLINE% was infected with the virus %VIRUSNAME%. The virus has been removed, and the email delivered.

#define EVENT_MAIL_VIRUSQUARANTINED	1501
//%SOFTWARENAME% %SOFTWAREVERSION%: An email for %MAILTONAME% (CC to %MAILCCNAME%) from %MAILFROMNAME% with the subject line %MAILSUBJECTLINE% is infected with the virus %VIRUSNAME%. The email has been quarantined.

#define EVENT_MAIL_VIRUSNOTCLEANED	1502
//%SOFTWARENAME% %SOFTWAREVERSION%: An email for %MAILTONAME% (CC to %MAILCCNAME%) from %MAILFROMNAME% with the subject line %MAILSUBJECTLINE% is infected with the virus %VIRUSNAME%. The virus could not be removed with Scan engine version %ENGINEVERSION% DAT version %DATVERSION%.

#define EVENT_MAIL_VIRUSFOUND		1503
//%SOFTWARENAME% %SOFTWAREVERSION%: An email for %MAILTONAME% (CC to %MAILCCNAME%) from %MAILFROMNAME% with the subject line %MAILSUBJECTLINE% is infected with the virus %VIRUSNAME%.

#define EVENT_MAIL_VIRUSDELETED	1504
//%SOFTWARENAME% %SOFTWAREVERSION%: An email for %MAILTONAME% (CC to %MAILCCNAME%) from %MAILFROMNAME% with the subject line %MAILSUBJECTLINE% was infected with the virus %VIRUSNAME%. The email has been deleted.

#define EVENT_MAIL_CONTENTFILTEREDFILTERED	1505
//%SOFTWARENAME% %SOFTWAREVERSION%: An email for %MAILTONAME% (CC to %MAILCCNAME%) from %MAILFROMNAME% with the subject line %MAILSUBJECTLINE% has broken a Content Filter rule. The email has been filtered.

#define EVENT_MAIL_CONTENTFILTERBLOCKED	1506
//%SOFTWARENAME% %SOFTWAREVERSION%: An email for %MAILTONAME% (CC to %MAILCCNAME%) from %MAILFROMNAME% with the subject line %MAILSUBJECTLINE% has broken a Content Filter rule. The email has been blocked.

#define EVENT_MAIL_LOWERDISKLIMIT	1507
//%SOFTWARENAME% %SOFTWAREVERSION%: Inbound email has been suspended due to low disk space.

#define EVENT_MAIL_UPPERDISKLIMIT	1508
//%SOFTWARENAME% %SOFTWAREVERSION%: Inbound email has been resumed now that sufficent disk space is available.

#define EVENT_MAIL_SERVICESTARTUP	1509
//%SOFTWARENAME% %SOFTWAREVERSION%: Startup request successfully processed.

#define EVENT_MAIL_SERVICEHUTDOWN	1510
//%SOFTWARENAME% %SOFTWAREVERSION%: Shutdown request successfully processed.

#define EVENT_MAIL_SERVICEABEND	1511
//%SOFTWARENAME% %SOFTWAREVERSION%: Warning - abnormal termination!

#define EVENT_MAIL_MAXIMUMLOAD	1512
//%SOFTWARENAME% %SOFTWAREVERSION%: A maximum load condition is occuring!

#define EVENT_MAIL_VIRUSQUARANTINEDANDCLEANED 1513
#define EVENT_MAIL_VIRUSQUARANTINEDANDNOTCLEANED 1514

// 

#define EVENT_WEBSHIELD_VIRUSCLEANED	        EVENT_MAIL_VIRUSCLEANED
#define EVENT_WEBSHIELD_VIRUSQUARANTINED	    EVENT_MAIL_VIRUSQUARANTINED
#define EVENT_WEBSHIELD_VIRUSNOTCLEANED	        EVENT_MAIL_VIRUSNOTCLEANED
#define EVENT_WEBSHIELD_VIRUSFOUND		        EVENT_MAIL_VIRUSFOUND
#define EVENT_WEBSHIELD_VIRUSDELETED	        EVENT_MAIL_VIRUSDELETED
#define EVENT_WEBSHIELD_CONTENTFILTEREDFILTERED	EVENT_MAIL_CONTENTFILTEREDFILTERED
#define EVENT_WEBSHIELD_CONTENTFILTERBLOCKED	EVENT_MAIL_CONTENTFILTERBLOCKED
#define EVENT_WEBSHIELD_LOWERDISKLIMIT	        EVENT_MAIL_LOWERDISKLIMIT
#define EVENT_WEBSHIELD_UPPERDISKLIMIT	        EVENT_MAIL_UPPERDISKLIMIT
#define EVENT_WEBSHIELD_SERVICESTARTUP	        EVENT_MAIL_SERVICESTARTUP
#define EVENT_WEBSHIELD_SERVICESHUTDOWN	        EVENT_MAIL_SERVICESHUTDOWN
#define EVENT_WEBSHIELD_SERVICEABEND	        EVENT_MAIL_SERVICEABEND
#define EVENT_WEBSHIELD_MAXIMUMLOAD	            EVENT_MAIL_MAXIMUMLOAD


// groupshield events 1700-1899

#define EVENT_GROUPSHIELD_EXCHANGE      1700
#define EVENT_GROUPSHIELD_DOMINO	    1800

// alertmanager events 1900-1999

#define EVENT_NEW_MIB_INSTALL           1900

// alert manager config 2000-2099
#define EVENT_AMG_TEST_ALERT	2000
#define EVENT_AMG_TEST_ALERT2		2001


// outbreak manager events  2100-2199

// EPO Agent		2200-2499
#define EVENT_EPO_AGENT_BASE                    2200

#define EVENT_INSTALL_SOFTWARE_FAIL             EVENT_EPO_AGENT_BASE+1	// ePO agent failed to install a software
#define EVENT_REACH_RETRY_INSTALL_LIMIT         EVENT_EPO_AGENT_BASE+2	// ePO agent has reached install retry limit for a software
#define EVENT_NOT_ENOUGH_DISK_SPACE             EVENT_EPO_AGENT_BASE+4	// ePO agent cannot install a software because of not enough disk space
#define EVENT_SPIPE_NOT_ENOUGH_DISK_SPACE       EVENT_EPO_AGENT_BASE+8	// SPIPE cannot extract the package because of not enough disk space
#define EVENT_SOFTWARE_PLATFORM_NOT_MATCH       EVENT_EPO_AGENT_BASE+16	// the software doesn't match ePO agent's platform
#define EVENT_POLICY_ENFORCEMENT_FAILED			EVENT_EPO_AGENT_BASE+32  // Policy enforcement failed
#define EVENT_PROPERTY_COLLECTION_FAILED		EVENT_EPO_AGENT_BASE+64  // Props collection failed
#define EVENT_TASK_ENFORCEMENT_FAILED			EVENT_EPO_AGENT_BASE+128 // Task enforcement failed



// lightweight installer events range 4500-4599 (name them EVENT_LW_INST_xxxx)

//--- webshield e-pliance 4600-4699-----------------------------------

//Spam detected: %FILENAME%, %ENGINEVERSION%, %VIRUSNAME%, %VIRUSTYPE%"
#define EVENT_SPAM_DETECTED		4650

//Spam statistics: %FILENAME%, %VIRUSNAME%
#define EVENT_SPAM_STATISTICS	4651

//--- Common Management Agent	4700-4999-----------------------------

#define EVENT_VSERR_CMAUPDATECONNECT	4700              // Failed to connect to CMA updater 
#define EVENT_VSERR_CMASCHEDULECONNECT  4701              // Failed to connect to CMA scheduler
#define EVENT_VSERR_CMASCHEDULEWRITE    4702              // Failed to save schedule data into CMA

 



// Common Licensing Agent 5000-5199
#define EVENT_NAIL_LICENSE_BASE						5000


// The software license for %SOFTWARENAME% will expire in %INFO% days
#define EVENT_NAIL_LICENSE_EXPIRING					EVENT_NAIL_LICENSE_BASE+0

// The software license for %SOFTWARENAME% has expired
#define EVENT_NAIL_LICENSE_EXPIRED					EVENT_NAIL_LICENSE_BASE+1

// The evaluation software license for %SOFTWARENAME% will expire in %INFO% days
#define EVENT_NAIL_EVALUATION						EVENT_NAIL_LICENSE_BASE+2

// The evaluation software license for %SOFTWARENAME% has expired
#define EVENT_NAIL_EVALUATION_EXPIRED				EVENT_NAIL_LICENSE_BASE+3

// Permitted period of unregistered use of %SOFTWARENAME% will expire in %INFO% days
#define EVENT_NAIL_UNREGISTERED						EVENT_NAIL_LICENSE_BASE+4

// Permitted period of unregistered use of %SOFTWARENAME% has expired
#define EVENT_NAIL_UNREGISTERED_EXPIRED				EVENT_NAIL_LICENSE_BASE+5

//The product code given was not correct
#define EVENT_NAIL_ERR_INVALID_PRODUCT_CODE			EVENT_NAIL_LICENSE_BASE+6

// The structSize member of the NAIL_LICENSE_DATA struct was not correct
#define EVENT_NAIL_ERR_INVALID_STRUCT_SIZE			EVENT_NAIL_LICENSE_BASE+7

// The license number given was invalid
#define EVENT_NAIL_ERR_INVALID_LICENSE_NUMBER		EVENT_NAIL_LICENSE_BASE+8

// The computer has been 'back-clocked'
#define EVENT_NAIL_ERR_BACK_CLOCKING_DETECTED		EVENT_NAIL_LICENSE_BASE+9

// The license information is corrupt
#define EVENT_NAIL_ERR_LICENSE_CORRUPT				EVENT_NAIL_LICENSE_BASE+10

// The license information could not be written to persistent storage
#define EVENT_NAIL_ERR_WRITE_DATA_FAILURE			EVENT_NAIL_LICENSE_BASE+11

// The license information could not be read from persistent storage
#define EVENT_NAIL_ERR_READ_DATA_FAILURE			EVENT_NAIL_LICENSE_BASE+12

// No licenses are installed for the specified product
#define EVENT_NAIL_ERR_NO_LICENSE_INSTALLED			EVENT_NAIL_LICENSE_BASE+13

// A license is already installed for the specified product
#define EVENT_NAIL_ERR_LICENSE_ALREADY_INSTALLED	EVENT_NAIL_LICENSE_BASE+14

// The buffer supplied is not big enough to hold the data
#define EVENT_NAIL_ERR_BUFFER_TOO_SMALL				EVENT_NAIL_LICENSE_BASE+15

// The license library is corrupted
#define EVENT_NAIL_ERR_LIBRARY_CORRUPT				EVENT_NAIL_LICENSE_BASE+16

// InitializeLicenseLibrary has not been called
#define EVENT_NAIL_ERR_LIBRARY_NOT_INITIALIZED		EVENT_NAIL_LICENSE_BASE+17

// InitializeLicenseLibrary has already been called
#define EVENT_NAIL_ERR_LIBRARY_ALREADY_INITIALIZED	EVENT_NAIL_LICENSE_BASE+18

// A bad license number was entered
#define EVENT_NAIL_ERR_BAD_LICENSE_NUMBER			EVENT_NAIL_LICENSE_BASE+19

// An error occurred encrypting data
#define EVENT_NAIL_ERR_ENCRYPTION_FAILURE			EVENT_NAIL_LICENSE_BASE+20

// Out of memory!
#define EVENT_NAIL_ERR_OUT_OF_MEMORY				EVENT_NAIL_LICENSE_BASE+21

// An unknown exception was thrown
#define EVENT_NAIL_ERR_EXCEPTION					EVENT_NAIL_LICENSE_BASE+22

// A bad alphanumeric string was given
#define EVENT_NAIL_ERR_BAD_ALPHA_STRING				EVENT_NAIL_LICENSE_BASE+23

// A bad product code was given
#define EVENT_NAIL_ERR_BAD_PRODUCT_CODE				EVENT_NAIL_LICENSE_BASE+24

// A bad expiry date was given
#define EVENT_NAIL_ERR_BAD_EXPIRY_DATE				EVENT_NAIL_LICENSE_BASE+25

// A bad generation date was given
#define EVENT_NAIL_ERR_BAD_GENERATION_DATE			EVENT_NAIL_LICENSE_BASE+26

// Unable to get the current time
#define EVENT_NAIL_ERR_BAD_LOCAL_TIME				EVENT_NAIL_LICENSE_BASE+27

// No license was installed and evaluation mode was chosen
#define EVENT_NAIL_EVALUATION_REQUEST				EVENT_NAIL_LICENSE_BASE+28

// Unknown error
#define EVENT_NAIL_UNKNOWN_ERROR					EVENT_NAIL_LICENSE_BASE+29

// Unsupported language was requested
#define EVENT_NAIL_ERR_UNSUPPORTED_LANGUAGE			EVENT_NAIL_LICENSE_BASE+30

// Invalid parameter was passed to the API
#define EVENT_NAIL_ERR_BAD_PARAM					EVENT_NAIL_LICENSE_BASE+31

// Access was denied
#define EVENT_NAIL_ERR_ACCESS_DENIED				EVENT_NAIL_LICENSE_BASE+32

// No license was installed and "Cancel" was chosen
#define EVENT_NAIL_CANCEL							EVENT_NAIL_LICENSE_BASE+33

//--- PortalShield  6000-6999 -----------------------------------------
//--- Talkback 6000 (CONFLICT) ------------------------------------------

// The Network Associates Error Reporting service detected a problem with %FILENAME%, %ACCESSPROCESSNAME% caused an exception and was terminated."
#define EVENT_TALKBACK 6000


// SPAMKILLER 7000-7199

//--- Talkback 8000-8999-------------------------------------------

//The item %FILENAME% is infected with %VIRUSNAME%. Detected with Scan Engine %ENGINEVERSION% DAT version %DATVERSION%."
#define EVENT_PS_VIRFOUND 8000

// The item %FILENAME% contains banned content.
#define EVENT_PS_BANNED 8500

// The item %FILENAME% is encrypted or corrupted.
#define EVENT_PS_CORRUPTED 8501

//The item %FILENAME% has triggered a file filter rule.
#define EVENT_PS_FILEFILTER 8502

// The item %FILENAME% has triggered an anti-spam rule.
#define EVENT_PS_ANTISPAM 8503


//---- MyCIO 9000-9999-------------------------------------------------
// Additional GroupShield Events 10000-10999
// McAfee for ISA Server 11000-11999
// Rogue Machine Detection (SnowCap) 12000-12999
// LinuxShield	13000-13999

// Entercept 14000-14999
// Intruvert 15000-15999
// epo 16000-16999
// native scan 17000-17499
// SCE (Swati) 17500-17999
// Host Intrusion Prevention 18000-18099

// sample product -- DO NOT RELEASE PRODUCTS USING THIS RANGE
// 99900-99999

// Unwanted Program events 1000000-1999999 (add 1000000 to our events)
