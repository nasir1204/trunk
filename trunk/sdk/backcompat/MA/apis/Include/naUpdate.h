#ifndef _NA_UPDATE_H_BB1F3C0A_E6DB_444c_BF43_42A572E731B5_
#define _NA_UPDATE_H_BB1F3C0A_E6DB_444c_BF43_42A572E731B5_


typedef enum tag_UPDATE_STATE
{
	eUPDATE_UNKNOWN = -1,
	eUPDATE_READY	= 0,
	eUPDATE_FINISHED_OK,  //finished updating all products
	eUPDATE_FINISHED_FAIL,   //failed to update any product
	eUPDATE_FINISHED_NOTFOUND,   //no qualifying product found
	eUPDATE_FINISHED_CANCEL,   //failed to update any product
	eUPDATE_FINISHED_WAITING_REBOOT,  //finished updating all products but requires reboot
	eUPDATE_FINISHED_WAITING_REBOOT_RESTART,  //finished updating some products but requires reboot and restart update
	eUPDATE_FINISHED_PARTIAL, //some product updated successfully some are not
	eUPDATE_SUCCEEDED, 		//component update succeeded
	eUPDATE_FAILED,			//component update failed
	eUPDATE_INIT, //10
	eUPDATE_LOADCONFIG,
	eUPDATE_PRENOTIFY, //prenotify, if update required
	eUPDATE_EOL_CHECK,
	eUPDATE_ROLLBACK_CHECK,
	eUPDATE_RUNNING,
	eUPDATE_CRITICAL,
	eUPDATE_NONCRITICAL,
	eUPDATE_SUSPEND,
	eUPDATE_RESUME,
	eUPDATE_DOWNLOAD, //20
	eUPDATE_STOPSERVICE, 
	eUPDATE_STARTSERVICE,
	eUPDATE_BACKUP,
	eUPDATE_COPY,	
	eUPDATE_SETCONFIG, //25
	eUPDATE_POSTNOTIFY,   //postnotify, if updated
	eUPDATE_CHECK_SITE_STATUS, 
	eUPDATE_COMPARE_SITES, 
	eUPDATE_PRENOTIFYFORCE

} UPDATE_STATE;

typedef enum tag_REBOOT_TYPE
{
	eREBOOT_TYPE_NORMAL = 0, //Co-operative reboot
	eREBOOT_TYPE_FORCE

} REBOOT_TYPE;


typedef enum tag_UPDATE_ERROR
{
	eUPDATE_ERROR_UNKNOWN = -1,
	eUPDATE_ERROR_OK =	0,	// update OK, Happy
	eUPDATE_ERROR_ACCEPT =	0,	// update OK, Happy
	eUPDATE_ERROR_LATEST,		//Update OK, but just to tell running latest version
	eUPDATE_ERROR_FAILED_TO_START, // update not started
	eUPDATE_ERROR_CALLBACKINIT,	//Point product callback component can not be loaded
	eUPDATE_ERROR_REJECT,	// point product rejected the update
	eUPDATE_ERROR_CANCEL,	// user cancelled the update
	eUPDATE_ERROR_EOL,		//End of life for product/scanengine
	eUPDATE_ERROR_LOADCONFIG,		//Load config error
	eUPDATE_ERROR_DOWNLOAD,
	eUPDATE_ERROR_POSTPONE,
	eUPDATE_ERROR_REPLICATE,
	eUPDATE_ERROR_SERVICE_STOP,
	eUPDATE_ERROR_SERVICE_START,
	eUPDATE_ERROR_INVALID, //invalid files/invalid site
	eUPDATE_ERROR_INVALID_DATS, // create different code for invalid dats
	eUPDATE_ERROR_BACKUP,
	eUPDATE_ERROR_ROLLBACK,
	eUPDATE_ERROR_COPY,
	eUPDATE_ERROR_DISKSPACE,
	eUPDATE_ERROR_SETCONFIG,
	eUPDATE_ERROR_LICENSE_EXPIRED,
	eUPDATE_ERROR_REBOOT_PENDING, //update/deployment failed because reboot pending
	eUPDATE_ERROR_UPDATEOPTIONS,   // Something wrong with the format of UpdateOptions.ini
	eUPDATE_ERROR_SCRIP_ENGINE_TERMINATED_UNEXPECTEDLY

} UPDATE_ERROR;

typedef enum tag_PRODUCT_RETURN_VALUE
{
	ePRODUCT_RETURN_OK = 0, //if point product returns this then update proceeds
	ePRODUCT_RETURN_ACCEPT = 0, //if point product returns this then update is applied
	ePRODUCT_RETURN_FAIL,    // Generic error. update fails for this product, unless other alternate in script provided
	ePRODUCT_RETURN_REJECT, // update is not applied for this product, if rejected
	ePRODUCT_RETURN_ROLLBACK, //if Point product returns this then only rollback done
	ePRODUCT_RETURN_LICENSE_EXPIRED //if Point product returns this when license gets expired

} PRODUCT_RETURN_VALUE;


#endif //_NA_UPDATE_H_BB1F3C0A_E6DB_444c_BF43_42A572E731B5_