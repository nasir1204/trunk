/***********************************************************************
naupdstr.h                 
this header file contains common string definitions, which should be used
point product component for script

***********************************************************************/

#ifndef _NAUPDATESTR_H
#define _NAUPDATESTR_H

//Event IDs from agent block, which will be used by updater
#define EVENT_COMMON_UPDATE_SUCCESS		2401
#define EVENT_COMMON_UPDATE_FAIL		2402

#define EVENT_DEPLOYMENT_SUCCESS		2411
#define EVENT_DEPLOYMENT_FAIL			2412


//UpdateTypes from Update Catalog schema
#define STR_UPDATE_TYPE_DAT								TEXT("DAT")
#define STR_UPDATE_TYPE_ENGINE							TEXT("Engine")
#define STR_UPDATE_TYPE_EXTRA							TEXT("ExtraDAT")
#define STR_UPDATE_TYPE_SP								TEXT("ServicePack")
#define STR_UPDATE_TYPE_HF								TEXT("HotFix")
#define STR_UPDATE_TYPE_LIC								TEXT("License")
#define STR_UPDATE_TYPE_LEGACY							TEXT("Legacy")
#define STR_UPDATE_TYPE_PLUGIN							TEXT("Plugin")
#define STR_UPDATE_TYPE_LANGPACK						TEXT("LangPack")
#define STR_UPDATE_TYPE_PRODUCT							TEXT("Product") // To retrieve/set product information 
#define STR_UPDATE_TYPE_PRODUCT_INSTALL					TEXT("Install") // product Install
#define STR_UPDATE_TYPE_PRODUCT_UNINSTALL				TEXT("Uninstall") // product uninstall

//Update info
#define STR_UPDATE_INFO_VERSION							TEXT("Version")		//Version number of the component
#define STR_UPDATE_INFO_BUILDNUM						TEXT("BuildNumber")	//Release build number of the component
#define STR_UPDATE_INFO_DATE_TIME						TEXT("DateTime")	//Release date of the component
#define STR_UPDATE_INFO_LOCALE							TEXT("Locale")		// Locale of the component
#define STR_UPDATE_INFO_ERROR							TEXT("UpdateError")	// Used to set update error for the component
#define STR_UPDATE_INFO_INSTALL_DATE					TEXT("InstallDate")	// Used to set last update time for the component
#define STR_UPDATE_INFO_LOCATION						TEXT("Location")	// Used to get the location for the component

#endif //_NAUPDATESTR_H
