/***********************************************************************

schdstd.h

 Copyright (C) 2009 McAfee, Inc.  All rights reserved. 
All Rights Reserved
Confidential

***********************************************************************/

/**********************************************************************/
#ifndef SCHDSTD_H
#define SCHDSTD_H

/**********************************************************************/


////////////////////////////////////////////////
#define schdBeginErrorNumber 17

#define schdE_NO_SECURITY_SERVICES naMAKE_RES(SEVERITY_ERROR,naFACILITY_NULL,schdBeginErrorNumber)		//0x80000011
#define schdE_PRODUCT_NOT_INSTALLED naMAKE_RES(SEVERITY_ERROR,naFACILITY_NULL,schdBeginErrorNumber+1)	//0x80000012
#define schdE_TASK_SETTING_INCORRECT naMAKE_RES(SEVERITY_ERROR,naFACILITY_NULL,schdBeginErrorNumber+2)	//0x80000013
#define schdE_DISABLED_TASK_DENIED naMAKE_RES(SEVERITY_ERROR,naFACILITY_NULL,schdBeginErrorNumber+3)	//0x80000014
#define schdE_PENDING_TASK_DENIED naMAKE_RES(SEVERITY_ERROR,naFACILITY_NULL,schdBeginErrorNumber+4)		//0x80000015
#define schdE_PLATFORM_NOT_MATCHED naMAKE_RES(SEVERITY_ERROR,naFACILITY_NULL,schdBeginErrorNumber+5)	//0x80000016
#define schdE_BEING_MODIFIED naMAKE_RES(SEVERITY_ERROR,naFACILITY_NULL,schdBeginErrorNumber+6)			//0x80000017
#define schdE_TASK_GUID_CONFLICT naMAKE_RES(SEVERITY_ERROR,naFACILITY_NULL,schdBeginErrorNumber+7)		//0x80000018
#define schdE_STRING_LENGTH_TOO_BIG naMAKE_RES(SEVERITY_ERROR,naFACILITY_NULL,schdBeginErrorNumber+8)	//0x80000019
#define schdE_TASK_ALREADY_RUNNING	naMAKE_RES(SEVERITY_ERROR,naFACILITY_NULL,schdBeginErrorNumber+9)	//0x8000001A
#define schdE_TASK_NOTFOUND	naMAKE_RES(SEVERITY_ERROR,naFACILITY_NULL,schdBeginErrorNumber+10)			//0x8000001B
#define schdE_ACCESS_DENIED	naMAKE_RES(SEVERITY_ERROR,naFACILITY_NULL,schdBeginErrorNumber+11)			//0x8000001C
#define schdE_OUTOFMEMORY	naMAKE_RES(SEVERITY_ERROR,naFACILITY_NULL,schdBeginErrorNumber+12)			//0x8000001D
#define schdE_NOTIMPL		naMAKE_RES(SEVERITY_ERROR,naFACILITY_NULL,schdBeginErrorNumber+13)			//0x8000001E
#define schdE_UNKNOWN_SCHEDULE_TYPE	naMAKE_RES(SEVERITY_ERROR,naFACILITY_NULL,schdBeginErrorNumber+14)			//0x8000001F
#define schdE_UNSUPPORT_REPEAT_OPTION	naMAKE_RES(SEVERITY_ERROR,naFACILITY_NULL,schdBeginErrorNumber+15)		//0x80000020
#define schdE_UNSUPPRT_UNTIL_OPTION	naMAKE_RES(SEVERITY_ERROR,naFACILITY_NULL,schdBeginErrorNumber+16)			//0x80000021
#define schdE_MISSED_TASK_STORAGE	naMAKE_RES(SEVERITY_ERROR,naFACILITY_NULL,schdBeginErrorNumber+17)			//0x80000022
#define schdE_WRITE_TASK_TO_FILE	naMAKE_RES(SEVERITY_ERROR,naFACILITY_NULL,schdBeginErrorNumber+18)			//0x80000023
#define schdE_DAILY_SETTING_INCORRECT	naMAKE_RES(SEVERITY_ERROR,naFACILITY_NULL,schdBeginErrorNumber+19)		//0x80000024
#define schdE_WEEKLY_SETTING_INCORRECT	naMAKE_RES(SEVERITY_ERROR,naFACILITY_NULL,schdBeginErrorNumber+20)		//0x80000025
#define schdE_MONTHLY_SETTING_INCORRECT	naMAKE_RES(SEVERITY_ERROR,naFACILITY_NULL,schdBeginErrorNumber+21)		//0x80000026
#define schdE_ONCE_SETTING_INCORRECT	naMAKE_RES(SEVERITY_ERROR,naFACILITY_NULL,schdBeginErrorNumber+22)		//0x80000027
#define schdE_IDLE_SETTING_INCORRECT	naMAKE_RES(SEVERITY_ERROR,naFACILITY_NULL,schdBeginErrorNumber+23)		//0x80000028
#define schdE_GENERIC_SETTING_INCORRECT	naMAKE_RES(SEVERITY_ERROR,naFACILITY_NULL,schdBeginErrorNumber+24)		//0x80000029
#define schdE_NO_MORE_DATA	naMAKE_RES(SEVERITY_ERROR,naFACILITY_NULL,schdBeginErrorNumber+25)					//0x8000002A
#define schdE_UNSUPPORTED_SCHEDULE_TYPE	naMAKE_RES(SEVERITY_ERROR,naFACILITY_NULL,schdBeginErrorNumber+26)		//0x8000002B
#define schdE_ARGUMENT_INCORRECT	naMAKE_RES(SEVERITY_ERROR,naFACILITY_NULL,schdBeginErrorNumber+27)			//0x8000002C
#define schdE_SCHEDULER_NOT_START_CORRECTLY	naMAKE_RES(SEVERITY_ERROR,naFACILITY_NULL,schdBeginErrorNumber+28)	//0x8000002D
#define schdE_SCHEDULER_NOT_GET_MUTEX_HANDLE	naMAKE_RES(SEVERITY_ERROR,naFACILITY_NULL,schdBeginErrorNumber+29)	//0x8000002E
#define schdE_TASK_ID_TOO_LONG naMAKE_RES(SEVERITY_ERROR,naFACILITY_NULL,schdBeginErrorNumber+30)				//0x8000002F
#define schdE_INVOKE_TASK_TIME_OUT naMAKE_RES(SEVERITY_ERROR,naFACILITY_NULL,schdBeginErrorNumber+31)				//0x80000030

/**********************************************************************/
#endif /* SCHDSTD_H */
/**********************************************************************/
