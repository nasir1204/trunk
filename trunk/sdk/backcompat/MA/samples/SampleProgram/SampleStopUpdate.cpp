/* Copyright (C) 2005 McAfee, Inc.  All rights reserved. */
#include "stdafx.h"
#include "SampleProgram.h"
#include "..\Common\SamplePointProduct.h"
#include "CmaWrap\CmaWrap.h"
#include "stdio.h"

void SampleStopUpdate (
    void)
{
/** StopUpdate

    void CmaWrap::StopUpdate (
        void)

    Uses

        Call this function to stop any currently running file update. See
        CmaWrap::RunUpdateNow for how to start an update.

 */

    SampleLog (TRUE, MODULE, L"Stop update");

    CmaWrap *cma = CmaWrapCreate (SAMPLE_SOFTWARE_CLASS, 0);
    if (!cma)
    {
        SampleLog (FALSE, MODULE, L"CmaWrapCreate failed");
    }
    else
    {
        cma->StopUpdate ();

        cma->Release();
    }
}
