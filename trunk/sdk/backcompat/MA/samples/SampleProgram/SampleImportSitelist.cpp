/* Copyright (C) 2005 McAfee, Inc.  All rights reserved. */
#include "stdafx.h"
#include "SampleProgram.h"
#include "..\Common\SamplePointProduct.h"
#include "CmaWrap\CmaWrap.h"
#include "stdio.h"

void SampleImportSitelist (
    void)
{
/** ImportSitelist

    int ImportSitelist (
        const wchar_t *siteListFile)

    Uses

        An "update" is when CMA downloads new DAT files, engine files, product
        files, or other files, if the existing versions on the user's computer
        are not up to date.

        The SiteList.xml file specifies where CMA should look for new files to
        download. The SiteList.xml file contains a list of "software
        repositories". At a minimum the list usually contains the McAfee FTP
        and HTTP repositories. Users may also create their own repositories on
        their local network using the MAA program.
        
        Your product may provide a way for users to change their SiteList.xml
        file, by importing a new SiteList.xml file into CMA.

        The CmaWrap::ImportSitelist function allows you to do this. It checks
        that the file is valid, and makes CMA start using it as its repository.

    Parameters

        const wchar_t *siteListFile

            This specifies the full path to the SiteList.xml file to import.

    Return value

        Returns a negative error code, or zero for success. The MgmtSDK
        Include\naError.h file contains a list of the error codes.

    Troubleshooting

        - To verify whether ImportSitelist is importing your file correctly, check
          the SiteList.xml file in the CMA data directory after your run it. It
          should match the file you imported.

        - Site list files contain both software repositories and SPIPE sites,
          which are ePO servers (which can also be used as repositories). If CMA
          is running in managed mode (that is, as an McAfee agent), and you try to
          import a site list that does not contain an SPIPE site for the agent to
          report to, ImportSiteList will fail with ERROR_SPIPE_SETTING_NOT_FOUND
          (-2200).
 */
    SampleLog (TRUE, MODULE, L"Import site list");

    CmaWrap *cma = CmaWrapCreate (SAMPLE_SOFTWARE_CLASS, 0);
    if (!cma)
    {
        SampleLog (FALSE, MODULE, L"CmaWrapCreate failed");
    }
    else
    {
     /* For the purpose of illustrating this function, the SiteList.xml file
        always comes from the directory SampleProgram.exe is in . If you want to
        test importing different files, you can change the file in that
        location, or modify this code as you wish. */
        wchar_t path[MAX_PATH];
        GetModuleFileName (NULL, path, SPACE (path));

        wchar_t *end = wcsrchr (path, L'\\');
        if (end != NULL)
        {
            ++end;
            *end = L'\0';
            wcsncat (path, L"SampleSiteList.xml", SPACE (path));

            int result = cma->ImportSitelist (path);
            if (result < 0)
            {
                SampleLog (FALSE, MODULE, L"ImportSiteList failed %d", result);
            }
        }
        cma->Release();
    }
}
