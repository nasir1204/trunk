/* Copyright (C) 2005 McAfee, Inc.  All rights reserved. */
// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__B22D2F6C_D6F9_4B3D_AC87_3F8FE97DFDD1__INCLUDED_)
#define AFX_STDAFX_H__B22D2F6C_D6F9_4B3D_AC87_3F8FE97DFDD1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _WINDOWS
#define _WINDOWS
#endif
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#ifndef WINVER
#define WINVER 0x400
#endif
#ifndef _WIN32_WINNT
#define _WIN32_WINNT 0x400
#endif
#ifndef _WIN32_WINDOWS
#define _WIN32_WINDOWS 0x400
#endif
#ifndef _WIN32_IE
#define _WIN32_IE 0x0400
#endif
#define STRICT
#define UNICODE
#define _UNICODE
#include <windows.h>

// include ADO type information (implementation in .cpp file)
#import <c:\\program files\\common files\\system\\ADO\\msado15.dll> \
    named_guids no_implementation rename("EOF", "adoEOF")

// TODO: reference additional headers your program requires here

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__B22D2F6C_D6F9_4B3D_AC87_3F8FE97DFDD1__INCLUDED_)
