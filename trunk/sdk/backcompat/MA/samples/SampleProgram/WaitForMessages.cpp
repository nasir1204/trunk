/* Copyright (C) 2005 McAfee, Inc.  All rights reserved. */
#include "stdafx.h"
#include "SampleProgram.h"
#include "..\Common\SamplePointProduct.h"

static BOOL GetPolicyValue (
    HKEY key,
    WCHAR *valueName,
    BYTE **value)
{
    BOOL result = FALSE;
    
 /* First, we are going to get the size of the named value. */
    DWORD size = 0;
    LONG error = RegQueryValueEx (key, valueName, NULL, NULL, NULL, &size);
    if (error != ERROR_SUCCESS)
    {
        SampleLog (FALSE, MODULE, L"RegQueryValueEx [%s] to get size, error %d", valueName, error);
    }
    else
    {
     /* Next, allocate a buffer of the required size. */
        *value = new BYTE[size];
        if (value == NULL)
        {
            SampleLog (FALSE, MODULE, L"Out of memory allocating registry buffer");
        }
        else
        {
         /* Now get the actual value into the buffer. */
            error = RegQueryValueEx (key, valueName, NULL, NULL, *value, &size);
            if (error != ERROR_SUCCESS)
            {
                SampleLog (FALSE, MODULE, L"RegQueryValueEx [%s] to get value, error %d", valueName, error);
            }
            else
            {
                result = TRUE;
            }
        }
    }
    return result;
}

void WaitForMessages (
    void)
{
 /* This sample function illustrates how to have a service or service-like
    program that runs continuously and waits for policy enforcement requests
    from your CMA plugin DLL.

    You could use this same technique to execute tasks in a service when
    requested by your plugin.
    
    When our sample program is run with the parameter 'policies', it enters
    this code and goes into a loop, waiting for the sample registry key values
    to change.
    
    If there is already an instance of the sample program running, this instance
    exits. We don't need two copies of the same 'service'. */

//DebugBreak ();

    HANDLE mutex = CreateMutex (NULL, FALSE, SAMPLE_MUTEX_NAME);
    if (mutex == NULL)
    {
        SampleLog (FALSE, MODULE, L"CreateMutex %s error %d", SAMPLE_MUTEX_NAME, GetLastError ());
    }
    else
    {
        if (GetLastError () == ERROR_ALREADY_EXISTS)
        {
            SampleLog (TRUE, MODULE, L"Policy enforcement 'service' already running, exiting");
        }
        else
        {
            HKEY key;
            LONG error = RegOpenKey (HKEY_LOCAL_MACHINE, SAMPLE_REG_KEY, &key);
            if (error != ERROR_SUCCESS)
            {
                SampleLog (FALSE, MODULE, L"RegOpenKey error %d", error);
            }
            else
            {
             /* Whenever CMA calls the sample plugin DLL's POPLUGIN_EnforcePolicyW
                function, the function will rewrite our sample registry values.
                Monitor the key for rewrites. When we get one, we re-enforce the
                policy values, even if they haven't changed.
                
                Just keep running indefinitely, like a service. For convenience,
                you can set the KeepRunning registry value to 0 to stop it. The
                Sample Point Product uninstaller uses this registry value to
                stop the program. */
                for (;;)
                {
                 /* Enforce the current policy values the plugin's
                    POPLUGIN_EnforcePolicyW function has set in the registry.
                    First get the values. */
                    BYTE *dwordPolicyValue = NULL;
                    BYTE *stringPolicyValue = NULL;
                    BYTE *keepRunningValue = NULL;
                    BOOL failed = !GetPolicyValue (key, SAMPLE_DWORD_POLICY, &dwordPolicyValue)
                        || !GetPolicyValue (key, SAMPLE_STRING_POLICY, &stringPolicyValue)
                        || !GetPolicyValue (key, SAMPLE_KEEP_RUNNING, &keepRunningValue);
                    if (!failed)
                    {
                     /* For our sample, "enforcing policies" means logging a
                        message. A real program would probably have additional
                        actions to do.
                        
                        For example, for a virus-scanning program, if there were
                        a policy setting like bEnableOnAccessScan=1, we could check
                        that the on-access scanner was running, and start it if
                        it wasn't. */
                        SampleLog (TRUE, MODULE, L"Enforcing policies with values:");
                        SampleLog (TRUE, MODULE, L" DWORD:  %d", *(DWORD *)dwordPolicyValue);
                        SampleLog (TRUE, MODULE, L" STRING: %s", (WCHAR *)stringPolicyValue);
                    }
                    
                    delete dwordPolicyValue;
                    delete stringPolicyValue;

                    if (failed)
                        break;

                    if (!*(BOOL *)keepRunningValue)
                        break;

                 /* RegNotifyChangeKeyValue will put this thread to sleep until
                    a POPLUGIN_EnforcePolicyW changes a value, then it will
                    return. */
                    LONG error = RegNotifyChangeKeyValue (key, FALSE, REG_NOTIFY_CHANGE_LAST_SET, NULL, FALSE);
                    if (error != ERROR_SUCCESS)
                    {
                        SampleLog (FALSE, MODULE, L"RegNotifyChangeKeyValue error %d", error);
                        break;
                    }
                }
                RegCloseKey (key);
            }
        }
        CloseHandle (mutex);
    }
}
