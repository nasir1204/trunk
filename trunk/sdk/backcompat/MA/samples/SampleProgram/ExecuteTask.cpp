/* Copyright (C) 2005 McAfee, Inc.  All rights reserved. */
#include "stdafx.h"
#include "SampleProgram.h"
#include "..\Common\SamplePointProduct.h"
#include <stdlib.h>

void ExecuteTask (
    WCHAR *parameters)
{
 /* This example illustrates how to execute a task from the CMA plugin function
    POPLUGIN_EnforceTaskW by starting a program and passing it task parameters.

    At this point our sample plugin's POPLUGIN_EnforceTaskW function has used
    CreateProcess to start this sample program, and has pass us a parameter
    string like this:
 
        "task <number> <string>"
        
    If you choose to use this method of executing tasks, you can pass any
    parameters you need to.
    
    For example, a virus-scanning program might have an on-demand scan task.
    The parameters would be information about which drives to scan, whether to
    scan in compressed files, etc. */
    SampleLog (TRUE, MODULE, L"Executing task");

    DWORD dwordOption;
    dwordOption = _wtoi (parameters);

    WCHAR *stringOption;
    stringOption = wcsstr (parameters, L" ");
    if (stringOption == NULL)
    {
        SampleLog (FALSE, MODULE, L"Task parameter string has bad format: [%s]", parameters);
    }
    else
    {
        ++stringOption;

     /* To "execute" the task, we are going to:
     
        1. Print a message that we have started the task, showing the parameters */
        SampleLog (TRUE, MODULE, L"Starting task with parameters %d and [%s]", dwordOption, stringOption);

     /* 2. Sleep for 2 minutes. This gives us the opportunity to illustrate how
           to stop a task that has run too long using the POPLUGIN_StopTask
           function. */
        Sleep (2 * 60 * 1000);

     /* 3. Print a message that the task is now competed */
        SampleLog (TRUE, MODULE, L"Completed task");
    }
}
