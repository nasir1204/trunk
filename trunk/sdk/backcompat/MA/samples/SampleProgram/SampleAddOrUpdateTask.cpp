/* Copyright (C) 2005 McAfee, Inc.  All rights reserved. */
#include "stdafx.h"
#include "SampleProgram.h"
#include "..\Common\SamplePointProduct.h"
#include "CmaWrap\CmaWrap.h"

void SampleAddOrUpdateTask (
    void)
{
/** AddOrUpdateTask

    bool CmaWrap::AddOrUpdateTask (
        const wchar_t *taskId,
        const wchar_t *taskName,
        const wchar_t *taskType,
        const CmaTaskScheduleInfo *taskSchedInfo,
        CmaAppConfig *appConfig,
        int appConfigCount)

    Uses

        Use AddOrUpdateTask to create or modify tasks for your product to
        execute.
    
        For example, a virus-scanning program might have a UI that allows users
        to specify
    
            - When scanning should take place
            - What scanning options should be in force for the scan
        
        The product would call the AddOrUpdateTask function to add the task to
        the CMA Scheduler database of tasks.

        When the time comes for the task to execute, CMA calls the product's
        plugin DLL's POPLUGIN_EnforceTaskW function. CMA passes in the task
        options, and the plugin does whatever is required to run the task.
 
    Parameters

        const wchar_t *taskId

            When adding a new task, create a new GUID string for this value. To
            modify the task options later, pass in the same GUID string.

        const wchar_t *taskName

            A user-readable name for the task.

            Do not pass printf specifiers such as %s in task names, ex.

                "Task that runs on a 12%schedule" -- BAD

        const wchar_t *taskType

            You can make up types for your tasks. For example, a virus-scanning
            program might have a type "SCAN". Your plugin POPLUGIN_EnforceTaskW
            function can access this value, so that it knows what kind of task
            to run (if you have more than one type).

        CmaTaskScheduleInfo *scheduleInfo

            Use this structure to specify when the task should execute. There
            are numerous scheduling options. You can create tasks that execute:

                daily 
                weekly 
                monthly 
                once 
                at system start up 
                when a user logs onto the machine 
                when the machine is idle 
                immediately 
                when the machine dials into the network

            See the MgmtSDK documentation section "How do I schedule tasks?"
            for full information.

        CmaAppConfig *appConfig

            Create an array of these structures to specify product-specific
            task options. For example, for a virus-scanning product, the
            options might be what disks to scan, whether to scan the boot sector,
            etc.

            The structure has these members:

                struct CmaAppConfig
                {
                    wchar_t *szSection;
                    wchar_t *szSetting;
                    wchar_t *szValue;
                };

            See the Sample Program for an example of its use.

        int appConfigCount

            Pass the number of CmaAppConfig objects in your appConfig array.

    Return value

        Returns false if the task could not be added or updated.

    Troubleshooting

        After you call AddOrUpdateTask, you should see messages in the AVIView
        log showing whether your task was scheduled successfully, such as:
        
            Next time(local) of task {EF96D4DF-F5AF-4b40-B8E7-AD49F17674C6}: Thursday, August 12, 2004 02:48:00 PM

        If it was not successful, you may see messages like this:

            Failed to add the task: error code= 0x80000027

        The error codes are in the MgmtSDK Include\schdstd.h file.

        After calling AddOrUpdateTask, a task file for your task should appear
        in "<CMA data directory>\Task\<taskId>.ini". For example:

            C:\Documents and Settings\All Users\Application Data\Network Associates\Common Framework\Task\{EF96D4DF-F5AF-4b40-B8E7-AD49F17674C6}.ini

        Try putting some logging in your plugin's POPLUGIN_EnforceTaskW function.
        The logs should appear when it is time to execute your task. The Sample
        Plugin project does this.
 */
    SampleLog (TRUE, MODULE, L"Scheduling a task");

    CmaWrap *cma = CmaWrapCreate (SAMPLE_SOFTWARE_CLASS, 0);
    if (!cma)
    {
        SampleLog (FALSE, MODULE, L"CmaWrapCreate failed");
    }
    else
    {
     /* 1. Prepare the task schedule information */
        CmaTaskScheduleInfo scheduleInfo = {0};

     /* Enable the task. It won't run unless this is set to true. */
        scheduleInfo.bEnabled = true;

     /* This will be a run-once task */
        scheduleInfo.eScheduleType = stypeOnce;

     /* We will specify the start time using local time, not GMT */
        scheduleInfo.bGMTTime = false;

     /* It will start today */
        SYSTEMTIME now;
        GetLocalTime (&now);

        scheduleInfo.nStartYear = now.wYear;
        scheduleInfo.nStartMonth = now.wMonth;
        scheduleInfo.nStartDay = now.wDay;

     /* It will start one minute from now (OK, there's a bug here, so don't run
        this sample at HH:59). */
        scheduleInfo.nStartMinute = now.wMinute + 1;
        scheduleInfo.nStartHour = now.wHour;

     /* You don't have to put a time limit on a task, but to demonstrate all
        options we will not let the task run for more than 1 minute. Our task
        execution code normally runs for 2 minutes (see ExecuteTask.cpp). We'll
        cut it short after 1 minute just to trigger the sample plugin's
        POPLUGIN_StopTask function. */
        scheduleInfo.dwStopAfterMinutes = 1;

     /* 2. Prepare the product-specific options. For our sample we have 2
        options. */
        CmaAppConfig appConfig[2];

        appConfig[0].szSection = SAMPLE_TASK_SECTION_NAME;
        appConfig[0].szSetting = SAMPLE_DWORD_TASK_OPTION;
        appConfig[0].szValue = L"22";

        appConfig[1].szSection = SAMPLE_TASK_SECTION_NAME;
        appConfig[1].szSetting = SAMPLE_STRING_TASK_OPTION;
        appConfig[1].szValue = L"Sample string option value";

     /* 3. The first time you run this example, it will create the task with the
        SAMPLE_TASK_ID ({EF96D4DF-F5AF-4b40-B8E7-AD49F17674C6}). Further runs will
        keep modifying that task. The SampleDeleteTask.cpp example function removes
        the task.

        For a real program, you would probably not have a fixed GUID as shown
        here. Instead you would generate a GUID every time a user needs to
        create a task with different options or which needs to run at a
        different time, ex.

            GUID taskGuid;
            CoCreateGuid (&taskGuid);

            WCHAR taskId[64];
            StringFromGUID2 (&taskGuid, taskId, SPACE (taskId));
     */
        if (!cma->AddOrUpdateTask (SAMPLE_TASK_ID, L"Sample Name", L"SAMPLETYPE1", &scheduleInfo,
            appConfig, SPACE (appConfig)))
        {
            SampleLog (FALSE, MODULE, L"AddOrUpdateTask failed");
        }

     /* If successful, there should now be a file Task\{EF96D4DF-F5AF-4b40-B8E7-AD49F17674C6}.ini
        in the CMA data directory. */

        cma->Release();
    }
}
