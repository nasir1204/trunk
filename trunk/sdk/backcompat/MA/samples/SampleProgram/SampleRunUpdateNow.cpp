/* Copyright (C) 2005 McAfee, Inc.  All rights reserved. */
#include "stdafx.h"
#include "SampleProgram.h"
#include "..\Common\SamplePointProduct.h"
#include "CmaWrap\CmaWrap.h"

void SampleRunUpdateNow (
    void)
{
/** RunUpdateNow

    int CmaWrap::RunUpdateNow (
        bool quiet)

    Uses

        An "update" is when CMA downloads new DAT files, engine files, product
        files, or other files, if the existing versions on the user's computer
        are not up to date.

        CmaWrap::RunUpdateNow allows you to easily kick off an immediate update
        from your product, should it require this functionality.

    Parameters

        bool quiet

            Pass true to have no progress UI. Pass false to present a progress UI.

    Return value

        Returns a negative error code, or zero for success. The MgmtSDK
        Include\naError.h file contains a list of the error codes.
  */
    SampleLog (TRUE, MODULE, L"Run update now");

    CmaWrap *cma = CmaWrapCreate (SAMPLE_SOFTWARE_CLASS, 0);
    if (!cma)
    {
        SampleLog (FALSE, MODULE, L"CmaWrapCreate failed");
    }
    else
    {
     /* While we're at it, let's demonstrate SetUpdateNowOption too. This call,
        before RunUpdateNow, will restrict the downloads to just the listed
        software. */
        int result = cma->SetUpdateNowOption (L"SelectiveUpdateOptions", L"SelectiveClientUpdateList", L"VSCANDAT100;VSCANENG1000;EXTRADAT1000;SPAMSAFE1000");
        if (result < 0)
        {
            SampleLog (FALSE, MODULE, L"SetUpdateNowOption failed %d", result);
        }
        
        result = cma->RunUpdateNow (false);
        if (result < 0)
        {
            SampleLog (FALSE, MODULE, L"RunUpdateNow failed %d", result);
        }
        cma->Release();
    }
}
