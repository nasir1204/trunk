/* Copyright (C) 2005 McAfee, Inc.  All rights reserved. */
#include "stdafx.h"
#include "SampleProgram.h"
#include "..\Common\SamplePointProduct.h"
#include "CmaWrap\CmaWrap.h"
#include "stdio.h"
#include "time.h"

void SampleGetTaskInfo (
    void)
{
/** GetTaskInfo

    bool CmaWrap::GetTaskInfo (
        const wchar_t *taskId,
        const wchar_t *taskName,
        int *maxTaskNameChars,
        wchar_t *taskType,
        size_t maxTaskTypeChars,
        int *taskStatus,
        long *taskNextTime,
        long *taskLastTime,
        CmaTaskScheduleInfo *taskSchedInfo,
        CmaAppConfig* appConfig,
        int appConfigCount)

    Uses

        Use this function to retrieve information about a task.

    Parameters

        const wchar_t *taskId

            This specifies the task to retrieve information about. Pass in the
            GUID value you used when you created the task using
            CmaWrap::AddOrUpdateTask.

        const wchar_t *taskName

            Pass in a buffer to receive the task's name. Pass NULL if you don't
            want this information.

        wchar_t *taskType

            Pass in a buffer to receive the task's type. Pass NULL if you don't
            want this information.

        size_t maxTaskTypeChars

            The size of the taskType buffer.

        int *taskStatus

            Pass in a buffer to receive the current task status, one of:

                - eTaskHasFailed
                - eTaskIsStillRunning
                - eTaskIsSuccessful
                - eTaskStopedByScheduler (Task reached the CmaTaskScheduleInfo::dwStopAfterMinutes value)
                - eTaskUnknown (Task has never run)
                - eFailedToGetStatus

            Pass NULL if you don't want this information.

            This value is derived from your CMA plugin's POPLUGIN_GetTaskStatusEx
            function.

        long *taskNextTime
        
            Pass in a pointer to a time_t value that will receive the next time
            the task will run. Pass NULL if you don't want this information.

        long *taskLastTime

            Pass in a pointer to a time_t value that will receive the last time
            the task ran, or 0 if is has never run. Pass NULL if you don't want 
            this information.

        CmaTaskScheduleInfo *taskSchedInfo

            Pass in a pointer to a CmaTaskScheduleInfo object that will receive
            the scheduling information for the task. Pass NULL if you don't want
            this information.

            See the Management SDK documentation section "How to schedule tasks"
            for full information on the scheduling options.

        CmaAppConfig* appConfig
        
            Pass in a pointer to an array of CmaAppConfig objects. Pass NULL if
            you don't want this information.
            
            Set the szSection and szSetting members to the sections and settings
            you wish to retrieve. Set the szValue members to NULL.

            After the call to GetTaskInfo, the szValue members will point to
            the values of each szSection and szSetting.

            Call the CmaWrap::FreeAppConfig function to free the szValue members:

                void FreeAppConfig (
                    CmaAppConfig *appConfig,
                    int appConfigCount)

        int appConfigCount

            Pass in the number of CmaAppConfig objects pointed to by the appConfig
            parameter.

    Return value

        Returns false if there was an error getting the task information.

 */

    SampleLog (TRUE, MODULE, L"Get task info");

    CmaWrap *cma = CmaWrapCreate (SAMPLE_SOFTWARE_CLASS, 0);
    if (!cma)
    {
        SampleLog (FALSE, MODULE, L"CmaWrapCreate failed");
    }
    else
    {
        wchar_t taskName[128];
        wchar_t taskType[32];
        int taskStatus;
        time_t nextTime;
        time_t lastTime;
        CmaTaskScheduleInfo scheduleInfo;
        CmaAppConfig appConfig[2] =
        {
            {SAMPLE_TASK_SECTION_NAME, SAMPLE_DWORD_TASK_OPTION, NULL},
            {SAMPLE_TASK_SECTION_NAME, SAMPLE_STRING_TASK_OPTION, NULL}
        };

        if (!cma->GetTaskInfo (
            SAMPLE_TASK_ID,
            taskName,
            SPACE (taskName),
            taskType,
            SPACE (taskType),
            &taskStatus,
            &nextTime,
            &lastTime,
            &scheduleInfo,
            appConfig,
            SPACE (appConfig)))
        {
            SampleLog (FALSE, MODULE, L"GetTaskInfo failed");
        }
        else
        {
            SampleLog (TRUE, MODULE, L" ID       %s",   SAMPLE_TASK_ID);
            SampleLog (TRUE, MODULE, L" Name     %s",   taskName);

            SampleLog (TRUE, MODULE, L" Type     %s",   taskType);

            wchar_t *taskStatusString;
            switch (taskStatus)
            {
                case eTaskHasFailed:
                    taskStatusString = L"failed";
                    break;
                case eTaskIsStillRunning:
                    taskStatusString = L"running";
                    break;
                case eTaskIsSuccessful:
                    taskStatusString = L"successful";
                    break;
                case eTaskStopedByScheduler:
                    taskStatusString = L"stopped";
                    break;
                case eTaskUnknown:
                    taskStatusString = L"never run";
                    break;
                case eFailedToGetStatus:
                    taskStatusString = L"failed to get status";
                    break;
                default:
                    taskStatusString = L"unknown value";
                    break;
            }
            SampleLog (TRUE, MODULE, L" Status   %s (%d)", taskStatusString, taskStatus);

            WCHAR *timeString = _wctime (&nextTime);
            timeString[24] = L'\0';
            SampleLog (TRUE, MODULE, L" Next     %s (0x%x)", timeString, nextTime);
            
            timeString = _wctime (&lastTime);
            timeString[24] = L'\0';
            SampleLog (TRUE, MODULE, L" Last     %s (0x%x)", timeString, lastTime);

         /* For this sample, just print the schedule type, not all of the
            information. */
            wchar_t *scheduleTypeString;
            switch (scheduleInfo.eScheduleType)
            {
                case stypeDaily:
                    scheduleTypeString = L"daily";
                    break;
                case stypeWeekly:
                    scheduleTypeString = L"weekly";
                    break;
                case stypeMonthly:
                    scheduleTypeString = L"monthly";
                    break;
                case stypeOnce:
                    scheduleTypeString = L"once";
                    break;
                case stypeAtStartup:
                    scheduleTypeString = L"at startup";
                    break;
                case stypeAtLogon:
                    scheduleTypeString = L"at logon";
                    break;
                case stypeWhenIdle:
                    scheduleTypeString = L"when idle";
                    break;
                case stypeImmediate:
                    scheduleTypeString = L"immediate";
                    break;
                case stypeDialup:
                    scheduleTypeString = L"dailup";
                    break;
                default:
                    scheduleTypeString = L"unknown value";
                    break;
            }

            SampleLog (TRUE, MODULE, L" Schedule %s (%d)",   scheduleTypeString, scheduleInfo.eScheduleType);

            for (int i = 0; i < SPACE (appConfig); ++i)
            {
                SampleLog (TRUE, MODULE, L" Setting  %s,%s,%s",   appConfig[i].szSection, appConfig[i].szSetting, appConfig[i].szValue);
            }

            cma->FreeAppConfig (appConfig, SPACE (appConfig));
        }
        cma->Release();
    }
}
