/* Copyright (C) 2005 McAfee, Inc.  All rights reserved. */
#include "stdafx.h"
#include "SampleProgram.h"
#include "..\Common\SamplePointProduct.h"
#include "CmaWrap\CmaWrap.h"
#include "stdio.h"

void SampleDeleteTask (
    void)
{
/** DeleteTask

    bool CmaWrap::DeleteTask (
        const wchar_t *taskId)

    Uses

        Use this function to delete tasks you have created.

    Parameters

        const wchar_t* taskId

            This specifies the task to delete. Pass in the GUID value you used
            when you created the task using CmaWrap::AddOrUpdateTask.

    Return value

        Returns false if there was an error deleting the task.

 */

    SampleLog (TRUE, MODULE, L"Delete task");

    CmaWrap *cma = CmaWrapCreate (SAMPLE_SOFTWARE_CLASS, 0);
    if (!cma)
    {
        SampleLog (FALSE, MODULE, L"CmaWrapCreate failed");
    }
    else
    {
     /* This sample program only creates or deletes a single task. The
        SampleAddOrUpdateTask function creates this task. */
        if (!cma->DeleteTask (SAMPLE_TASK_ID))
        {
            SampleLog (FALSE, MODULE, L"DeleteTask failed");
        }

        cma->Release();
    }
}
