/* Copyright (C) 2005 McAfee, Inc.  All rights reserved. */
#include "stdafx.h"
#include "SampleProgram.h"
#include "..\Common\SamplePointProduct.h"
#include "CmaWrap\CmaWrap.h"
#include "stdio.h"

void SampleIterateTasks (
    void)
{
/** IterateTasks

    CmaTaskIterator *CmaWrap::IterateTasks (
        const wchar_t *softwareId)

    Uses

        Your program may allow users to schedule tasks for your program to execute
        at specific times or for specific repeating periods. For example, a
        virus-scanning program may allow users to schedule different kinds of
        scanning tasks that run periodically.
        
        You may need to have a UI that displays what task the user has scheduled.
        The IterateTasks function allows you to get a list of all of your current
        tasks.

    Parameters

        const wchar_t* softwareId

            IterateTasks will only return tasks owned by the given software ID,
            which should probably be your product's software ID.

    Return value

        IterateTasks returns an object that allows you to iterate through your
        tasks. Use its Next function to get each task's ID, name, and type:

            bool CmaTaskIterator::Next (
                wchar_t *taskId,
                size_t maxTaskIdChars,
                wchar_t *taskName,
                size_t maxTaskNameChars,
                wchar_t * taskType,
                size_t maxTaskTypeChars)

        Call the CmaTaskIterator::Release function when you are done with the
        CmaTaskIterator object.

        Use the CmaWrap::GetTaskInfo function to get more detailed information
        about each task.

 */

    SampleLog (TRUE, MODULE, L"Iterating tasks");

    CmaWrap *cma = CmaWrapCreate (SAMPLE_SOFTWARE_CLASS, 0);
    if (!cma)
    {
        SampleLog (FALSE, MODULE, L"CmaWrapCreate failed");
    }
    else
    {
        CmaTaskIterator *iterator = cma->IterateTasks (SAMPLE_SOFTWARE_ID);
        if (iterator == NULL)
        {
            SampleLog (FALSE, MODULE, L"IterateTasks failed");
        }
        else
        {
            WCHAR taskId[64];
            WCHAR taskName[256];
            WCHAR taskType[32];
            while (iterator->Next (taskId, SPACE (taskId), taskName, SPACE (taskName), taskType, SPACE (taskType)))
            {
                SampleLog (TRUE, MODULE, L" %s,%s,%s", taskId, taskName, taskType);
            }

            iterator->Release ();
        }

        cma->Release();
    }
}
