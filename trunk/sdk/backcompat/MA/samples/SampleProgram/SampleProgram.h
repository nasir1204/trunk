/* Copyright (C) 2005 McAfee, Inc.  All rights reserved. */

extern void ExecuteTask (WCHAR *parameters);
extern void WaitForMessages (void);
extern void SampleRunUpdateNow (void);
extern void SampleAddOrUpdateTask (void);
extern void SampleIterateTasks (void);
extern void SampleDeleteTask (void);
extern void SampleGetTaskInfo (void);
extern void SampleImportSitelist (void);
extern void SampleStopUpdate (void);
extern void SampleRunRollbackUpdate (void);
extern void SampleGetUpdateState (void);
extern void SampleSendEvent (WCHAR *parameters);

#define SAMPLE_MUTEX_NAME L"McAfeeSampleProductMutex"
#define SAMPLE_TASK_ID    L"{EF96D4DF-F5AF-4b40-B8E7-AD49F17674C6}"

#define MODULE L"Program"
