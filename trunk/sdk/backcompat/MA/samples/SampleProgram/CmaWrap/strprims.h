//----------------------------------------------------------------------------
// Copyright 2004 Network Associates Inc. All Rights Reserved.
//----------------------------------------------------------------------------
// filename:    strprims.h
// created:     Nov 8, 2001
//----------------------------------------------------------------------------
#ifndef STRPRIMS_H
#define STRPRIMS_H


void Stradd(char* dest,size_t maxDestChars,const char* source);
void Stradd(wchar_t* dest,size_t maxDestChars,const wchar_t* source);
void Straddn(char* dest,size_t maxDestChars,const char* source,size_t length);
void Straddn(wchar_t* dest,size_t maxDestChars,const wchar_t* source,size_t);
void Straddlwr(char* dest,size_t maxDestChars,const char* source);
void Straddlwr(wchar_t* dest,size_t maxDestChars,const wchar_t* source);
void Strins(char* dest,size_t maxDestChars,const char* string);
void Strins(wchar_t* dest,size_t maxDestChars,const wchar_t* string);
int/*0,1,-1*/ Strcmp(const char* string1,const char* string2);
int/*0,1,-1*/ Strcmp(const wchar_t* string1,const wchar_t* string2);
int/*0,1,-1*/ Strncmp(const char* string1,const char* string2,size_t count);
int/*0,1,-1*/ Strncmp(const wchar_t* string1,const wchar_t* string2,size_t);
int/*0,1,-1*/ Strscmp(const char* string1,const char* string2);
int/*0,1,-1*/ Strscmp(const wchar_t* string1,const wchar_t* string2);
int/*0,1,-1*/ Strcmpi(const char* string1,const char* string2);
int/*0,1,-1*/ Strcmpi(const wchar_t* string1,const wchar_t* string2);
int/*0,1,-1*/ Strncmpi(const char* string1,const char* string2,size_t count);
int/*0,1,-1*/ Strncmpi(const wchar_t* string1,const wchar_t* string2,size_t);
int/*0,1,-1*/ Strscmpi(const char* string1,const char* string2);
int/*0,1,-1*/ Strscmpi(const wchar_t* string1,const wchar_t* string2);
char* Strstri(const char* dest,const char* subString);
wchar_t* Strstri(const wchar_t* dest,const wchar_t* subString);
void Strset(char* dest,char value,size_t count);
void Strset(wchar_t* dest,wchar_t value,size_t count);
enum {
    STR_INTHEX       = 0x01,
    STR_INTSIGNED    = 0x02,
    STR_INTYESNO     = 0x04,
    STR_INTTRUEFALSE = 0x08,
    STR_INTONOFF     = 0x10 };
INT64 Strtoint(const char* string,size_t length,int* flags);
INT64 Strtoint(const wchar_t* string,size_t length,int* flags);
void Straddint(char* dest,size_t maxDestChars,INT64 value,int flags,int pad);
void Straddint(wchar_t* dest,size_t maxDestChars,INT64 value,int flags,int
    pad);
void Strmove(char* dest,const char* source,size_t count);
void Strmove(wchar_t* dest,const wchar_t* source,size_t count);
char* Strmchr(const char* data,char value,size_t length);
wchar_t* Strmchr(const wchar_t* data,wchar_t value,size_t length);
void Straddfv(wchar_t* dest,size_t maxDestChars,const wchar_t* format,va_list);
void Straddf(wchar_t* dest,size_t maxDestChars,const wchar_t* format,...);
bool Strisempty(const char* string);
bool Strisempty(const wchar_t* string);
void StrToEscape(char* dest,size_t maxDestChars,const char* source);
void StrFromEscape(char* dest,size_t maxDestChars,const char* source);
void StrToEscape(wchar_t* dest,size_t maxDestChars,const wchar_t* source);
void StrFromEscape(wchar_t* dest,size_t maxDestChars,const wchar_t* source);

size_t/*destLength*/ Utf8ToWcs(wchar_t* dest,size_t maxDestLength,
    const char* source,size_t sourceLength);
size_t/*destLength*/ WcsToUtf8(char* dest,size_t maxDestLength,
    const wchar_t* source,size_t sourceLength);

const wchar_t* /*value*/ ArgsGetProperty(int argc,const wchar_t* argv[],
    const wchar_t* propName);

#ifdef DEBUGHEAP

char* DebugHeapStrmalloc(size_t chars,const char* file,int line);
wchar_t* DebugHeapWcsmalloc(size_t chars,const char* file,int line);
char* DebugHeapStrdup(const char* string,const char* file,int line);
wchar_t* DebugHeapStrdup(const wchar_t* string,const char* file,int line);
void DebugHeapStrfree(char* string);
void DebugHeapStrfree(wchar_t* string);
char* DebugHeapStrdup(const char* string);
wchar_t* DebugHeapWcsdup(const wchar_t* string);
#define Strmalloc(chars) DebugHeapStrmalloc(chars,__FILE__,__LINE__)
#define Wcsmalloc(chars) DebugHeapWcsmalloc(chars,__FILE__,__LINE__)
#define Strdup(string) DebugHeapStrdup(string,__FILE__,__LINE__)
#define Strfree(mem) DebugHeapStrfree(mem)

#else

char* Strmalloc(size_t chars);
wchar_t* Wcsmalloc(size_t chars);
char* Strdup(const char* string);
wchar_t* Strdup(const wchar_t* string);
void Strfree(char* string);
void Strfree(wchar_t* string);

#endif

#ifdef _UNICODE
#define Tcsmalloc Wcsmalloc
#else
#define Tcsmalloc Strmalloc
#endif


#endif
//----------------------------------------------------------------------------
// Copyright 2004 Network Associates Inc. All Rights Reserved.
//----------------------------------------------------------------------------
