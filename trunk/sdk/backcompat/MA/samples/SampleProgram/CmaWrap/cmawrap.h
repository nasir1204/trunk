//----------------------------------------------------------------------------
/* Copyright (C) 2005 McAfee, Inc.  All rights reserved. */
//----------------------------------------------------------------------------
// file name:   cmawrap.cpp
// created:     2003.09.10
//----------------------------------------------------------------------------

// Adapted from the VirusScan cmawrap.h file for general use in the MgmtSDK

#ifndef CMAWRAP_H
#define CMAWRAP_H

//for CmaTaskScheduleInfo eScheduleType values, etc.
#include "..\..\..\Generated\Scheduler.h"

struct CmaTaskScheduleInfo
{
    bool bGMTTime;
    int nStartHour;
    int nStartMinute;
    int nStartDay;
    int nStartMonth;
    int nStartYear;
    bool bStopDateValid;
    int nStopDay;
    int nStopMonth;
    int nStopYear;
    bool bRepeatable;
    int eRepeatOption;
    int nRepeatInterval;
    int eUntilOption;
    int nUntilHour;
    int nUntilMinute;
    int nUntilDuration;
    bool bRandomizationEnabled;
    int nRandomizationWndMins;
    bool bOnceADayEnabled;
    int eScheduleType;
    struct
    {
        int Daily_nRepeatDays;
        int Weekly_nRepeatWeeks;
        int Weekly_maskDaysOfWeek;
        int Monthly_eMonthlyOption;
        int Monthly_nDayNumOfMonth;
        int Monthly_nWeekNumOfMonth;
        int Monthly_nDayOfWeek;
        int Monthly_maskMonthsOfYear;
        int Idle_nIdleMinutes;
    } Type;
    bool bStopScanPeriod;
    int dwStopAfterMinutes;
    bool bRunIfMissed;
    int uMissedTaskDelay;
    int uStartupDelay;
    enum { maxRunAsUserChars = 100 };
    wchar_t szRunAsUser[maxRunAsUserChars];
    enum { maxRunAsDomainChars = 100 };
    wchar_t szRunAsDomain[maxRunAsDomainChars];
    enum { maxRunAsPasswordChars = 100 };
    wchar_t szRunAsPassword[maxRunAsPasswordChars];
    bool bPasswordValid;
    bool bEnabled;
    bool bDirty;
};
typedef CmaTaskScheduleInfo TASKSCHEDULE,*LPTASKSCHEDULE;

struct CmaAppConfig
{
    wchar_t *szSection;
    wchar_t *szSetting;
    wchar_t *szValue;
};
typedef CmaAppConfig APPCONFIG,*LPAPPCONFIG;

struct CmaTaskIterator
{
    virtual void AddRef(void) = 0;
    virtual void Release(void) = 0;
    virtual void Reset(void) = 0;
    virtual bool/*success*/ Next(wchar_t* taskGuid,size_t maxTaskGuidChars,
        wchar_t* taskName,size_t maxTaskNameChars,wchar_t* taskType,
        size_t maxTaskTypeChars) = 0;
};

struct CmaWrap
{
    virtual void AddRef(void) = 0;
    virtual void Release(void) = 0;

 /* Scheduling */
    virtual CmaTaskIterator* IterateTasks(const wchar_t* softwareId) = 0;
    virtual bool/*success*/ GetTaskInfo(const wchar_t* taskId,wchar_t*
        taskName,size_t maxTaskNameChars,wchar_t* taskType,size_t
        maxTaskTypeChars,int* taskStatus,long* taskNextTime,long*
        taskLastTime,CmaTaskScheduleInfo* taskSchedInfo,CmaAppConfig*
        appConfig,int appConfigCount) = 0;
    virtual void FreeAppConfig(CmaAppConfig* appConfig,int appConfigCount) = 0;
    virtual bool/*success*/ AddOrUpdateTask(const wchar_t* taskId,
        const wchar_t* taskName,const wchar_t* taskType,
        const CmaTaskScheduleInfo* taskSchedInfo,CmaAppConfig* appConfig,
        int appConfigCount) = 0;
    virtual bool/*success*/ DeleteTask(const wchar_t* taskId) = 0;

 /* Updating */
    virtual int/*result*/ RunUpdateNow(bool quiet) = 0;
    virtual void StopUpdate(void) = 0;
    virtual int/*result*/ GetUpdateState(int* updateStats) = 0;
    virtual int/*result*/ ImportSitelist(const wchar_t* siteListFile) = 0;
    virtual int/*result*/ RunUpdateTask(const wchar_t* taskId) = 0;
    virtual int/*result*/ SetUpdateNowOption(const wchar_t* section,const
        wchar_t* setting,const wchar_t* value) = 0;
    virtual int/*result*/ SetUpdateTaskOption(const wchar_t* taskId,const
        wchar_t* section,const wchar_t* setting,const wchar_t* value) = 0;
    virtual int/*result*/ RunPullTask(const wchar_t* taskId,const wchar_t*
        dest) = 0;
    virtual int/*result*/ RunRollbackUpdate(void) = 0;
    virtual int/*result*/ GetPullTaskState(int* updateStats) = 0;
    virtual void StopPullTask(void) = 0;
};

struct CmaWrapNotify
{
    virtual void AddRef(void) = 0;
    virtual void Release(void) = 0;
    virtual void CmaWrapNotifyTerminate(void) = 0;
};

typedef CmaWrap* CmaWrapPtr;
extern "C" CmaWrapPtr CDECL CmaWrapCreate(const wchar_t* clientId,
    CmaWrapNotify* notify);


#endif
//----------------------------------------------------------------------------
// (C) Copyright 2004 Network Associates Inc. All Rights Reserved
//----------------------------------------------------------------------------
