/* Copyright (C) 2005 McAfee, Inc.  All rights reserved. */
// EPOBllExt.h : Declaration of the CEPOBllExt

#ifndef __EPOBLLEXT_H_
#define __EPOBLLEXT_H_

#include "resource.h"       // main symbols

#define STR_EPO_BLL_EXT     TEXT("SAMPLEEVENTPARSERPLUGIN")//

/////////////////////////////////////////////////////////////////////////////
// CEPOBllExt
class ATL_NO_VTABLE CEPOBllExt : 
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<CEPOBllExt, &CLSID_EPOBllExt>,
    public ISupportErrorInfo,
	public IDispatchImpl<IEPOBllExt, &IID_IEPOBllExt, &LIBID_SAMPLEEVENTPARSERPLUGINLib>
{
public:
	CEPOBllExt() {} 	

	DECLARE_REGISTRY_RESOURCEID(IDR_SAMPLEEVENTPARSERPLUGIN)
	DECLARE_PROTECT_FINAL_CONSTRUCT()

	BEGIN_COM_MAP(CEPOBllExt)
		COM_INTERFACE_ENTRY(IEPOBllExt)
		COM_INTERFACE_ENTRY(ISupportErrorInfo)
		COM_INTERFACE_ENTRY(IDispatch)
	END_COM_MAP()

// ISupportsErrorInfo
public:
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);

protected:
	static bstr_t m_bstrConnectionString;

// IEPOBllExt
public:
	STDMETHOD(ePOBllExt_ProcessProps)(/*[in]*/BSTR bstrPropsXMLFile);
    STDMETHOD(ePOBllExt_ProcessEvent)(/* in */BSTR bstrEventXMLFile);
	STDMETHOD(ePOBllExt_SetConnectionString)(/* in */BSTR bstrConnectionString);
};

#endif //__EPOBLLEXT_H_
