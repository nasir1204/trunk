<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Local Procedure Call: an Agent Integration Framework</title>
    <link rel="stylesheet" href="../../css/style.css" type="text/css" />
    <link rel="stylesheet" href="../../css/jqtree.css" type="text/css" />
    <link rel="stylesheet" href="../../css/orionDocs.css" type="text/css" />
    <link rel="stylesheet" href="../../css/prettify.css" type="text/css" />
    <link rel="stylesheet" href="../../../css/jquery-ui.css" type="text/css" />
    <link rel="stylesheet" href="../../../css/style.css" type="text/css" />
</head>

<body>
     <!-- JavaScript -->
     <script src="../../js/orionDocs.js" type="text/javascript">
</script><script src="../../js/jquery-1.8.2.min.js" type="text/javascript">
</script><script src="../../js/tree.jquery.js" type="text/javascript">
</script><script src="../../js/prettify.js" type="text/javascript">
</script><!-- HTML5 shim, for IE6-8 support of HTML5 elements --><!--[if lt IE 9]>
<script src="../../js/html5shiv.js"></script>
<![endif]-->

    <div id="page-wrap">
        <div id="left-sidebar2"></div>

        <div id="inside">
            <!-- End left sidebar -->
            <!-- Begin content -->

            <div class="even-height" id="main-content">
                <!-- A good title helps users find what they need -->

                <h1>Local Procedure Call: an Agent Integration Framework</h1><!-- Use a good, effective short description -->
                <!-- Article content goes here -->

                <p>This document describes how the Local Procedure Call (LPC) facility is used by McAfee Agent (MA).</p>

                <h2 id="Background">Background</h2>

                <p>ePO manageability related operations can be broadly categorized into the following two types, with their associated tasks and operations:</p>

                <ul>
                    <li>MA initiated operation

                        <ul>
                            <li>Property collection</li>

                            <li>Policy enforcement</li>

                            <li>Task enforcement</li>
                        </ul>
                    </li>

                    <li>Product initiated operations

                        <ul>
                            <li>Event generation</li>

                            <li><code>updateNow</code>, for example</li>
                        </ul>
                    </li>
                </ul>

                <p>To achieve ePO manageability, MA 4.0 and previous versions of MA use a plug-in based mechanisms. In a plug-in based mechanism, Point Product teams write plug-in libraries to support MA initiated operations and use MA developed libraries to invoke Point Product initiated actions. The plug-in based architecture has some inherent issues, for example:</p>

                <ul>
                    <li>Binary compatibility issues</li>

                    <li>Point products and MA can not independently select compilers of their choice</li>

                    <li>Bug in a plug-in can bring down MA process and endanger ePO manageability of an end node</li>

                    <li>Inter-process communication related code duplication and development overhead for point product teams</li>

                    <li>Does not suit service oriented architecture</li>

                    <li>Does not assist multiplatform code development</li>

                    <li>Does not scale well for new MA features like Data Channel and UBP</li>
                </ul>

                <p>This caused the MA team to search for alternatives to plug-ins. Different options, for example COM, CORBA, and third party libraries were evaluated. Some of them do not pass the multiplatform availability tests and, if they do, they are overkill for ePO manageability requirements. All of these constraints resulted in the development of a home-grown mechanism called LPC. LPC stands for Local Procedure Call.</p>

                <h2 id="Overview">Overview</h2>

                <p>LPC is a generic multi-platform and inter-process procedure invocation framework. MA 4.5 uses it to communicate between MA and point products. For example, property collection, task enforcement, policy enforcement, and more. LPC framework can also be used for point product to point product communication, but this functionality is not officially supported with MA 4.5 release. LPC calls communication with end points as client and servant. Similar to other client-server communication technologies, the process that initiates the communication is called the "client" and the process that serves the request is called the "servant." For a given business logic that needs to be accessed across a process, user creates a pure abstract C++ interface. The user provides client-stub and servant implementations for that interface. Server process creates an instance of a servant object and registers it with the LPC framework. Once registered, a servant object can be accesses from a client process using the client stub.</p>

                <h2 id="Identifiers">LPC Process Identifiers</h2>

                <p>Process identifiers are used to uniquely identify LPC processes on a given machine. They also associate security related configuration information from <code>registry/config.xml</code> with processes. The raw point product software ID could have been the best candidate for a process identifier, but it lacks the ability to uniquely identify multiple processes belonging to the same point product. The current process identifier format is:</p>

                <p><code>&lt;Software ID&gt;:&lt;User Tag&gt;:&lt;Number&gt;</code></p>

                <p>The variables are:</p>

                <ul>
                    <li>Software ID: - Point product software ID</li>

                    <li>User Tag: - User specified unique tag, for example, A, B, C, etc.</li>

                    <li>Number: - LPC framework generated number</li>
                </ul>

                <p>Normally point products can be ignorant of process identifier format and just specify the software ID as a process identifier during LPC framework initialization. In this case, LPC framework converts the software ID to a proper process identifier format and everthing works as expected. But if a point product has multiple processes that interact with MA, or if they are using Data Channel functionality, then they must use the <em>generateLPCUID()</em> API function to generate a process identifier.</p>

                <p>Consider the following cases:</p>

                <ul>
                    <li>Case A - Point product <strong>XYZ</strong> does not use data channel and it does not have multiple processes interacting with MA. While calling the framework initialization API, it can directly provide the following software ID:<br />
                    <em>initializeLPC(<strong>"XYZ"</strong>, ...)</em></li>

                    <li>Case B - Point product <strong>LMN</strong> uses the data channel or it has multiple processes that interact with MA. LMN must generate the following process identifier and use that identifier while initializing the framework.:<br />
                    <em>generateLPCUID(<strong>"LMN", "A",</strong> ...)</em></li>
                </ul>

                <h2 id="Marshaling">Data Marshaling</h2>

                <p>The LPC library uses the boost serialization library to perform data marshaling/demarshaling (serialization/deserialization). See the following <a target="_blank" href="http://boost.org/">boost.org</a> link for details. MA 4.5 supports only text based serialization even though the boost serialization library can support binary and XML based serialization. Currently the boost serialization library, version 1.32, is used as a default serialization library. The MA-supplied SDK controls marshaling/demarshling of ePO manageability related data across processes. This means point products are not responsible for marshaling/demarshling.</p>

                <p>If point products are using some higher version of boost serialization library for their product specific needs, then those point products must supply that library to the LPC framework to perform marshaling/demarshaling related tasks. To supply a custom serializer, implement the abstract interface <strong>"ISerializationFactory"</strong> and supply the instance of this implementation to the LPC framework during initialization. For details on the ISerializationFactory interface refer to the API documentation.</p>

                <h2 id="Error">Error Handling</h2>

                <p>The LPC categorizes errors as the following two types:</p>

                <ul>
                    <li><strong>Business logic related API level errors</strong> - Reported as a return value of an API. These errors can have simple C++ primitive data types. For example, int, char, etc. or some user defined data type. Data type selection depends on the developer of the business logic.</li>

                    <li><strong>Errors related to LPC framework</strong> - Reported as C++ exceptions. Common examples include, MA not installed, MA not running, MA/Point product is not supporting some service, framework could not get some system resource. For example, system error, etc. For details on different types of exceptions thrown by framework, refer to lpc_excetpions_extern.hpp from the SDK. <strong>CAUTION:</strong> Since unhandled exceptions result in program termination, point products must guard all API invocations with try-catch blocks..</li>
                </ul>

                <h2 id="Security">LPC Security</h2>

                <p>Framework uses a pass phrase based authentication mechanism to validate point products. Each point product must select some strong pass phrase and store the SHA-1 hash pass phrase in the <code>registry/config.xml</code>. This must occur once per point product. Point products can choose to do it in their installer or it can be done through their executables. LPC SDK provides a mfelpchelper library to assist in SHA-1 hash generation, but point products are free to use another cryptographic library. During LPC communication, the point product process needs to supply the pass phrase to the LPC framework. The LPC framework callback mechanism is used to accept the pass phrase during runtime.</p>

                <h2 id="Callbacks">LPC Callbacks</h2>

                <p>Framework uses a callback mechanism to take input from user. Currently user needs to register the following three callbacks:</p>

                <ol>
                    <li><strong>Pass phrase callback</strong> - Used to accept user selected pass phrase. The pass phrase passed through this callback should correspond to the SHA-1 hash stored in <code>registry/config.xml</code>. For details on LPC security please refer to the <a href="#Security">LPC security section</a>.</li>

                    <li><strong>LPC runtime installation notification</strong> - Called when a new LPC runtime is installed on a machine. This is invoked only at the time of an LPC runtime installation/upgrade. The user should perform their initialization related tasks in this callback. A typical initialization task is used to publish/run user supplied services like property collection, task enforcement, etc.</li>

                    <li><strong>LPC runtime uninstallation notification</strong> - Called when the LPC runtime is uninstalled from a machine. This is invoked only at the time of an LPC runtime uninstallation/upgrade. The user should perform their resource cleanup tasks in this callback. Typical cleanup tasks include revoke/stop user supplied services like property collection, task enforcement, etc.</li>
                </ol>

                <h2 id="Setup">LPC Setup Information</h2>

                <p>Point products need to do some one time setup activity before they can use the LPC framework. Point products can use either the LPC based mechanism or plug-in based mechanism for ePO manageability. Point product cannot use both mechanisms simultaneously. For example, a point product cannot use plug-in for property collection and LPC for Data Channel. <strong>Plug-in is a deprecated mechanism and should not be used for MA 4.5 integration.</strong> LPC uses registry on windows and config.xml on UNIX platforms to store setup information. The following table lists required setup information.</p>

                <table class="zebra">
                    <caption>
                        Registry Information (Windows)
                    </caption>

                    <thead>
                        <tr>
                            <th>Sr.No.</th>

                            <th>Registry Key</th>

                            <th>Description</th>
                        </tr>
                    </thead>

                    <tr>
                        <td>1</td>

                        <td>HKLM\SOFTWARE\Network Associates\ePolicy Orchestrator\Application Plugins\software_ID\Use Ipc</td>

                        <td>The point product should create this key and set its value to 1 if it wants to use LPC. If this key value is 0 or if this key is not present then the point product cannot use the LPC framework.</td>
                    </tr>

                    <tr>
                        <td>2</td>

                        <td>HKLM\SOFTWARE\Network Associates\ePolicy Orchestrator\Application Plugins\software_ID\AuthorizationHash</td>

                        <td>The LPC using point product must select some pass phrase to secure communication over the LPC.SHA1 hash or the selected pass phrase needs to be stored in this key.</td>
                    </tr>
                </table><br />

                <table class="zebra">
                    <caption>
                        Point Product Config file Information (UNIX)
                    </caption>

                    <thead>
                        <tr>
                            <th>Sr.No.</th>

                            <th>Element Name</th>

                            <th>Description</th>
                        </tr>
                    </thead>

                    <tr>
                        <td>1</td>

                        <td>UseIpc</td>

                        <td>The point product should add this element to its config.xml file and set its value to 1 to use LPC. If this element value is 0 or not present the point product will not be able to use LPC framework.</td>
                    </tr>

                    <tr>
                        <td>2</td>

                        <td>AuthorizationHash</td>

                        <td>LPC using point product need to select some pass phrase to secure communication over LPC. SHA1 hash of the selected pass phrase needs to be stored in this element.</td>
                    </tr>
                </table>

                <h2 id="Runtime">LPC Runtime Distribution</h2>

                <p>LPC has the following two libraries:</p>

                <ul>
                    <li>Dynamic library, "mfelpc" - Contains low level LPC implementation details and is called at LPC runtime.</li>

                    <li>Static library "mfelpcservices" - Contains ePO manageability related business logic and does not need not be installed on the machine.</li>
                </ul>

                <p>Normally only one copy of LPC runtime is installed on a client machine and this shared copy is used by all point products. But there are some exceptions to this rule. The following two sections describe the two cases associated with runtime distribution:</p>

                <h3>(A) Compatible build environment</h3>

                <p>Point products using the same compiler (and compatible versions) as the MA are in the compatible build environment category. For details on MA build environment refer to the <a href="../../articles/devsetup/devsetup.html">build environment section</a>. In this case MA installs and upgrades the LPC at runtime. Point products do not need LPC availability on client machines. Point products can use LPC framework even if MA (and hence runtime) is not installed on the machine. LPC framework automatically detects runtime installation and upgrade activity and initiates these processes to point products using a <a href="#Callbacks">callback mechanism</a>. Point products need to supply two callbacks during LPC initialization. One is called during runtime installation and the other during uninstallation. Point products can perform other activities in these callbacks. For example, starting and stopping services and other business logic related initialization and release related activities.</p>

                <h3>(B) Non-compatible Build Environment</h3>

                <p>Point products using different compilers (or non-compatible version) as the MA are in the non-compatible build environment category. For details on MA build environment, refer to the <a href="../../articles/devsetup/devsetup.html">build environment section</a>. In this case point products build LPC runtime and other related libraries using their compiler. Similarly, they need to ship custom runtime distribution and manage updating the LPC.</p>

                <h2 id="Initialization">LPC Framework Initialization and Cleanup</h2>

                <p>A point product must perform LPC framework initialization before using any of the LPC functionalities. Initialization consists of two steps and it must be performed only once per process.</p>

                <ul>
                    <li>First step - Register all the interfaces theuser wants to use with LPC framework using <strong>registerInterface()</strong> API.
                        <pre class="prettyprint">
//To run as a service
...
if ( false == registerInterface&lt; IEcho_Servant_Impl &gt;("IEcho") )
{
  //Log error and do not proceed with other LPC stuff
}
...
(or/and)
...
//To access an exposed service
if ( false == registerInterface&lt; IEcho_Client_Impl &gt;("IEcho") )
{
  //Log error and do not proceed with other LPC stuff
}
...
</pre>
                    </li>

                    <li>Second step - Start LPC framework using <strong>initializeLPC()</strong> API.<br />
                        These steps can be performed irrespective of whether the LPC runtime (i.e. MA) is installed on a machine or not. Any attempt to access LPC functionality without these initialization steps can result into unpredictable results.
                        <pre class="prettyprint">
...
#include "lpc/lpc_access/lpc.hpp"
...
using namespace mileaccess_com::MA::LPC;
...
//define callbacks
void startPointProductServicesCallBack(void)
{
  // Add code to take care when this is called at the time of install/upgrade of MA
  // ...
}
void stopPointProductServicesCallBack(void)
{
  // Add code to take care when this is called at the time of uninstall/upgrade of MA
  // ...
}
/*
*We also need a callback to pass the passphrase to lpc
*/
bool sHA1_Validation_callback(unsigned char *arg1, unsigned long arg1_size, unsigned char *arg2, unsigned long *arg2_size)
{
  // SHA1 validation
  //...
}
//Initialize the callbacks
MfeLpcCallbacks *pcallback = new(std::nothrow) MfeLpcCallbacks;
if(NULL != pcallback)
{
  /* callback while MA Install/upgrade */ 
  pcallback-&gt;mfelpcInstalledCallBack_ = startPointProductServicesCallBack;
  
  /* callback while MA Uninstall */ 
  pcallback-&gt;mfelpcUnInstalledCallBack_ = stopPointProductServicesCallBack;
  
  /* passphrase callback for validation */ 
  pcallback-&gt;validationCallback_ = sHA1_Validation_callback;
}

//Get the serialization factory
ISerializationFactory *pfactory = new SerializationFactory;

//Initialize LPC framework
int retVal = initializeLPC(<strong>"Example"</strong>, dirPath.c_str(), pcallback, pfactory);
if(retVal &lt; 0)
{
  //Log error and do not proceed with other LPC stuff
  return;
}
if ( 1 == retVal )
{
  //LPC runtime is not installed so differ further initialization till installation callback
  return;
}
...
</pre>
                    </li>
                </ul>

                <ul>
                    <li>User must perform LPC framework cleanup using <strong>deinitialize()</strong> API.<br />
                        This activity must be performed only once per process. User should not access any LPC functionality after framework cleanup, any attempt to do so may result into unpredictable results. Refer <a href="#Example">Examples</a> section for sample code.
                        <pre class="prettyprint">
// Release LPC framework
deinitializeLPC();
</pre>
                    </li>
                </ul>

                <h2 id="LPC_Integration_Tips">LPC Integration Tips</h2>

                <p>Point product teams can benefit from the following when using MA 4.5 integration.</p>

                <ul>
                    <li><strong>Create Multiplatform Agent Integration Code</strong><br />
                    Prior to release of MA 4.0 the Point Product teams were forced to write lot of inter-process communication (IPC) related code to transfer data between the Agent process and Point Product process. This made it difficult to write a code that worked on Windows and other UNIX platforms. The new LPC based integration framework eliminates the need to write IPC related code for agent integration. Now the Point product teams can use pure C++ code for ePO managed business logic and also create multiplatform agent integration code.</li>

                    <li><strong>Enable ePO Manageability Support on the Fly</strong><br />
                    If a point product uses LPC based framework for MA integration then these point products are not closely tied with MA installation on the client machine. The LPC framework allows the MA to detect install and uninstall events and notify the point product of these events using callbacks. Point products can use this event and callback information to enable ePO management support quickly.</li>

                    <li><strong>Build Environment Selection Freedom</strong><br />
                    Starting with MA 4.5, the build environment (for example, compilers and compiler versions) for a point product can be different from the environment of the agent. <strong>NOTE:</strong> It is still easier to integrate with the agent if the point product uses the same build environment as the agent. If the envoronments are different LPC runtime must be installed or upgraded on the client machine.</li>

                    <li><strong>Improved API to Access Agent Information</strong><br />
                    Point products do not need to refer to MA internal files to get agent information. For example, agent.ini and sitelist.xml. MA 4.5 uses an IAgentInformation interface over LPC to get agent information. For example, agent GUID, epo server IP, and more.</li>
                </ul>

                <h2 id="Integration_Testing_Tips">Integration Testing Tips</h2>

                <p>Point products should always perform the following tests in addition to their normal agnet integration testing.</p>

                <ul>
                    <li><strong>Agent Start/Stop Test</strong><br />
                    Start and Stop agent service multiple times and check if the ePO managment functiony is working for your product.</li>

                    <li><strong>Agent Install/Uninstall Test</strong><br />
                    Try to uninstall an agent and confirm the point product receives agent uninstall notification. Install agent again and confirm the point product receives the agent installation notification and confirm the ePO management function works as expected.</li>

                    <li><strong>Accessing agent functionality when agent is not installed</strong><br />
                    Uninstall the agent from a machine and install only a Point Product. Then try to access Data Channel, SiteList information (UNIX only), or Updater (UNIX only). Confirm the point product logs the appropriate message in the product log files and does not show any abnormal behavior, for example, a crash.</li>

                    <li><strong>Accessing agent functionality when agent is not running</strong><br />
                    Stop the agent service then try to access Data Channel, SiteList information (UNIX only), or Updater (UNIX only). Confirm the point product logs the appropriate message in product log files and does not show any abnormal behavior, for example, a crash.</li>

                    <li><strong>Machine restart test</strong><br />
                    Install agent and the point product and confirm ePO management is working as expected. Restart the test machine and confirm processes are working as expected after the restart.</li>
                </ul>

                <h2 id="Example">Examples</h2>

                <p>The following examples show LPC usage basic code flow:</p>

                <ul>
                    <li><a href="#Example_Setup">Example Setup</a>: <strong>IEcho</strong> Interface Declaration</li>

                    <li><a href="#Example1">Example 1</a>: Publishing <strong>IEcho</strong> Service</li>

                    <li><a href="#Example2">Example 2</a>: Accessing <strong>IEcho</strong> Service</li>
                </ul>

                <h3 id="Example_Setup">Example Setup: <strong>IEcho</strong> Interface Declaration</h3>

                <p>"Echo Message" service example that user wants to run or access. Following is the echo service interface:</p>
                <pre class="prettyprint">
<strong>class IEcho {
public:
  <em>virtual std::string echoString(const std::string &amp;input) = 0;</em>
};</strong>

//Corresponding Stub and Servant implementations for this interface are,

class <strong>IEcho_Client_Imp</strong> : public <strong>IEcho</strong> {
public:
  std::string echoString(const std::string &amp;input)
  {
    // Data marshaling related LPC framework specific generated code.
  }
};

class <strong>IEcho_Servant_Imp</strong> : public <strong>IEcho</strong> {
public:
  std::string echoString(const std::string &amp;input)
  {
    //Some user specific implementation details.
    return input;
  }
};
</pre>

                <h3 id="Example1">Example 1: Publishing <strong>IEcho</strong> service</h3>

                <p>Following is a server side code example to publish "Echo Message" service:</p>
                <pre class="prettyprint">
<strong>// Step 1: Register interface and its implementation with LPC framework </strong>
if ( false == registerInterface&lt; IEcho_Servant_Impl &gt;("IEcho") )
{
  //Log error and do not proceed with other LPC stuff
}

<strong>// Step 2: initialize framework</strong>
int retVal = initializeLPC(<strong>"Example1"</strong>...)
if(retVal &lt; 0)
{
  //Log error and do not proceed with other LPC stuff
  return;
}
if ( 1 == retVal )
{
  //LPC runtime is not installed so differ further initialization till installation callback
  return;
}

<strong>//Step 3: Now create "Echo Message" service and publish it through framework </strong>
IEcho_Servant_Imp *pechoService = new <strong>IEcho_Servant_Imp</strong>;
LPCServiceController<strong>&lt;IEcho_Servant_Imp&gt;</strong> controller;
controller.runService(pechoService); //Now other processes can access echo service
...
...
...
//Perform other product specific tasks ...
...
...
...
<strong>// Step 4 : Time to stop the process so perform framework cleanup tasks</strong>
//First stop all running services
IEcho_Servant_Imp *pechoService
LPCServiceController&lt;IEcho_Servant_Imp&gt; controller;
pechoService  = controller.stopService("IEcho");
delete pechoService;

//Now release LPC framework
deinitializeLPC();
</pre>

                <h3 id="Example2">Example 2: Accessing <strong>IEcho</strong> service</h3>

                <p>Following is a client side code example used to access the "Echo Message" service shown in <a href="lpc.html#Example1">example 1</a>:</p>
                <pre class="prettyprint">
<strong>// Step 1 : Register interface and its implementation with LPC framework </strong>
if ( false == registerInterface&lt; IEcho_Client_Impl &gt;("IEcho") )
{
  //Log error and do not proceed with other LPC stuff
}

<strong>// Step 2 : initialize framework</strong>
int retVal = initializeLPC(<strong>"Example2"</strong>...)
if(retVal &lt; 0)
{
  //Log error and do not proceed with other LPC stuff
  return;
}
if ( 1 == retVal )
{
  //LPC runtime is not installed so differ further initialization till installation callback
  return;
}

<strong>// Step 3 : Now you can access "Echo Message" service exposed by example 2 above</strong>
<strong>IEcho</strong> *pecho = NULL;
if(!createInstance(<strong>"IEcho", "Example1"</strong>, pecho))
{
  //Some error ... do not proceed log error and return
}
<strong>try</strong>
{
  std::string returnStr = pecho-&gt;echoString("Test");
  //Display returnStr
}
<strong>catch(const LPCException &amp;err)</strong>
{
  //Some LPC framework error occurred during echoString() call.
  //Log error
}

//We are done with accessing service so delete a pointer
delete pecho;

...
...
...
//Perform other product specific tasks ...
...
...
...

<strong>// Step 4 : Time to stop the process so perform framework cleanup tasks</strong>
// Release LPC framework
deinitializeLPC();
</pre><!-- Author and last date modified. -->
            </div><!-- End Content -->

            <div style="clear: both;"></div>
        </div>
    </div><script type='text/javascript' src='../../../js/jquery.min.js'>
</script><script type='text/javascript' src='../../../js/jquery-ui.min.js'>
</script><script type='text/javascript' src='../../../js/navigation.min.js'>
</script>
</body>
</html>
