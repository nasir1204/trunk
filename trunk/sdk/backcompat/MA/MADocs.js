// creates a table of contents that links to each
// H2 in the document.
function buildTOC()
{
    var TOC = document.getElementById("TOC");

    if(TOC == null)
        return;

    var h2s = document.getElementsByTagName("h2");
    var h3s = document.getElementsByTagName("h3");

    for(var i = 0; i < h2s.length; i++)
    {
        // add the LI element.
        var item = document.createElement("li");
        item.innerHTML = "<a href=\"#TOC_ITEM_" + i + "\">" + h2s[i].innerHTML + "</a>";
        TOC.appendChild(item);
        
        // put in the sub sections
        var list = document.createElement("ul");
        for(var j=0; j<h3s.length; j++){
        	if( i<h2s.length-1 && h3s[j].compareDocumentPosition(h2s[i])==2 && h3s[j].compareDocumentPosition(h2s[i+1])==4 
        		|| i==h2s.length-1 && h3s[j].compareDocumentPosition(h2s[i])==2){
        		var h3item = document.createElement("li");
        		h3item.innerHTML = "<a href=\"#TOC_ITEM_" + i + "_"+j+"\">" + h3s[j].innerHTML + "</a>";
        		list.appendChild(h3item);
        		
        		// add the anchor to the h3
        		var h3anchor = document.createElement("a");
        		h3anchor.id = "TOC_ITEM_" + i+"_"+j;
        		h3anchor.name = "TOC_ITEM_" + i+"_"+j;
        		h3s[j].appendChild(h3anchor);
        	}
        }
        item.appendChild(list);

        // add the anchor to the H2
        var anchor = document.createElement("a");
        anchor.id = "TOC_ITEM_" + i;
        anchor.name = "TOC_ITEM_" + i;

        h2s[i].appendChild(anchor);
    }
}


function buildTOCById()
{
    var TOC = document.getElementById("TOC");

    if(TOC == null)
        return;

    var titles = document.getElementsByTagName("h2");
    for(var i = 0; i < titles.length; i++)
    {
        // add the LI element.
        var title = titles[i];
        if( title.id )
        {
            var item = document.createElement("li");
            item.innerHTML = "<a href=\"#" + titles[i].id + "\">" + titles[i].innerHTML + "</a>";
            TOC.appendChild(item);
        }
    }
}