
/* This file shows the minimum required for a CMA plugin.
   Copyright (C) 2005 McAfee, Inc.  All rights reserved. */

#include "stdafx.h"
#include "..\..\Include\PoPlugin.h"
#include "..\..\Generated\Scheduler.h"

BOOL APIENTRY DllMain (
    HINSTANCE module,
    DWORD reason,
    LPVOID reserved)
{
    return TRUE;
}

BOOL POPLUGIN_IsInterfaceUnicode (
    void)
{
    return TRUE; //Change to FALSE for ANSI
}

HRESULT POPLUGIN_InitializeW (
    LPCWSTR softwareId)
{
    return S_OK; //Or a negative number for failure
}

HRESULT POPLUGIN_DeinitializeW (
    LPCWSTR softwareId)
{
    return S_OK;
}

BOOL POPLUGIN_EnforcePolicyW (
    LPCWSTR softwareId,
    PO_GETPOLICY_PROCW GetPolicy)
{
    return TRUE; //FALSE for failure
}

BOOL POPLUGIN_GetPropertiesW (
    LPCWSTR softwareId,
    PO_SETPROP_PROCW SetProperty)
{
    return TRUE; //FALSE for failure
}

BOOL POPLUGIN_EnforceTaskW (
    LPCWSTR softwareId,
    LPCWSTR taskId,
    PO_GETTASKOPTION_PROCW GetTaskOption, 
    LPVOID *cookie)
{
    return TRUE; //FALSE for failure
}

int POPLUGIN_GetTaskStatusEx (
    LPVOID cookie)
{
    return eTaskIsSuccessful; //or eTaskIsStillRunning or eTaskHasFailed
}

BOOL POPLUGIN_StopTask (
    LPVOID cookie)
{
    return TRUE; //FALSE for failure
}

BOOL POPLUGIN_FreeCookie (
    LPVOID cookie)
{
    return TRUE;
}

bool POPLUGIN_EnforcePolicyObject (
    ICMAPolicyObjectList *policyObjectList)
{
    return true; //false for failure
}
