var footer_text = "Copyright. &copy; 2014 McAfee, Inc. | ePO SDK: 5.3.0.125 | ePO: 5.3.0.175 | MFS: 5.3.0.161 | MA: 5.0.0.2411";
// IE FIX: if the console is not open, console.log will cause ie to stop executing javascript!
// The log will be disabled if debug is true
var DEBUG = false;

var meta = [
    {"url":"MFS/api/MajicJSLibrary.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/api/ui/CSS.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/api/ui/GeneralUIConcepts.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/api/yui.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/Auditing/AuditLog.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/AutomaticResponse/AdvancedIntegration.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":true, "epo51":true, "epo52":false, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/AutomaticResponse/BasicIntegration.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":true, "epo51":true, "epo52":false, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/AutomaticResponse/UsingTheResponseUI.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":true, "epo51":true, "epo52":false, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/Branding/Branding.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/BuildingUI/Building UI with Orion.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/Charts/Charts.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/Clustering/Clustering.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":false, "epo50":false, "epo51":false, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/Compatibility/Compatibility.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/Dashboards/dashboards.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/Database/UsingTheDatabase.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/DetailPages/DetailPages.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/DisasterRecovery/DisasterRecovery.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":false, "epo46":false, "epo50":true, "epo51":true, "epo52":false, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/ExtensionCompatibility/ExtensionCompatibility.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":false, "epo46":false, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/Extensions/CannedContent.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/Extensions/OrionExtensions.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/Extensions/UsingMFSTestExtension.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/FIPS/FIPS140Compliance.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":true, "epo51":true, "epo52":false, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/IssueManagement/IssueManagement.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":true, "epo51":true, "epo52":false, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/JNI/JNI.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/KeyConcepts/KeyConcepts.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/LDAP/LDAP.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":true, "epo51":true, "epo52":false, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/Localization/Localization.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/Logging/Logging.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/Multitenant/MigratingToTheCloud.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":false, "epo50":false, "epo51":false, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/MVC/OrionMVC.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/Navigation/UI_Navigation.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/OrionEventDispatchService/OrionEventDispatchService.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/ORM/OrionORM.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/Overview/Overview_Orion.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/Ownership/OwnershipFramework.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/Plugins/OrionPlugins.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/PublicAPI/PublicURL.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/PythonClient/en/getting_started.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":true, "epo51":true, "epo52":false, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/Queries/AdvancedQuerySystemIntegration.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/Queries/BasicQuerySystemIntegration.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/Queries/RemoteDatabaseSupport.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":true, "epo51":true, "epo52":false, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/Queries/UsingQueryCommands.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/Queries/UsingTheQuerySystemUI.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/RegisteredServers/RegisteredServers.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":true, "epo51":true, "epo52":false, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/RegisteredTypes/FilterableUIAction.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/RegisteredTypes/RegisteredTypes.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/Report/AdvancedReporting.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/Scheduler/Scheduler.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/Security/CSRFAttackPrevention.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/Security/SecureExtensions.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/Server/CertBasedAuth.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/Server/CertificatesAndKeys.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/Server/ExternalAuth.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/Server/Server.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/Server/ServerSetting.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/Server/ThirdPartyLibraries.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/Sexp/SExpressions.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/TaskLog/TaskLogIntegration.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/Testing/OrionTestTools.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":false, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/Transporter/Transporter.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/UIAction/UIActions.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/UnexpectedConditions/UnexpectedConditions.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MFS/articles/Users/RolesAndPermissions.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"ePO/articles/BasicClientTask.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":true},
    {"url":"ePO/articles/ClientTaskPermission.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"ePO/articles/CreatingSystemCommands.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"ePO/articles/ePO Update Doc/ePOEvent.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":true},
    {"url":"ePO/articles/ePO Update Doc/ePOPolicyIntegrationOverview.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":true},
    {"url":"ePO/articles/EPOCommonEventCategories.htm", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":true},
    {"url":"ePO/articles/EPOCommonEventFormat.htm", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":true},
    {"url":"ePO/articles/EPOCommonEventSeverity.htm", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":true},
    {"url":"ePO/articles/ePOCommonEventXMLFormat.htm", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":true},
    {"url":"ePO/articles/ePOCommonProductProperties.htm", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":true},
    {"url":"ePO/articles/ePOCrossVersionPlatformPolicyMgmt.htm", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":true},
    {"url":"ePO/articles/ePOCustomProductProperties.htm", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":true},
    {"url":"ePO/articles/ePODataChannelBestPractices.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":true},
    {"url":"ePO/articles/EPOFormValidation.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"ePO/articles/ePOGuidedTour.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"ePO/articles/ePONotificationServices.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"ePO/articles/ePOPolicyConfigFiles.htm", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":true},
    {"url":"ePO/articles/ePOPolicyManagementIntegration.htm", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"ePO/articles/epoPolicyOverView.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"ePO/articles/ePORepositoryManagementIntegration.htm", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"ePO/articles/ePORollupReporting/index.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"ePO/articles/ePOUserBasedPolicyOverview.htm", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"ePO/articles/EventFiltersAndDescriptions.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"ePO/articles/ExtensionDependencies.htm", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"ePO/articles/keyConcepts/KeyConcepts.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"ePO/articles/MVCActionBasedPolicyUIExtension.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"ePO/articles/MVCActionBasedTaskUIPrdExtension.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"ePO/articles/policyUITipandTricks.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"ePO/articles/ProductComparison.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"ePO/articles/ProductFeaturePolicyPermIntegration.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"ePO/articles/PublicURLs.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"ePO/articles/RuleBasedPolicyAssignment.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"ePO/articles/ScriptableCommands.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"ePO/articles/SoftwareManager.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"ePO/articles/UIBestPractices.htm", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"ePO/Overview/EPOExtensionPointOverview.htm", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"ePO/Overview/ePOIntroduction.htm", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"ePO/tutorials/PointProductSetup.htm", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"MSA/apis/CmaWrap/CmaWrap_Reference.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":false, "epo51":false, "epo52":false, "epo53":false, "server_side":false, "client_side":true},
    {"url":"MSA/apis/EventGeneration/EventGeneration_Reference.htm", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":false, "epo51":false, "epo52":false, "epo53":false, "server_side":false, "client_side":true},
    {"url":"MSA/apis/Script Engine/script-command-reference.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":false, "epo52":false, "epo53":false, "server_side":false, "client_side":true},
    {"url":"MSA/apis/template/template.html", "windows":false, "non_windows":false, "on_premise":false, "multi_tenant":false, "epo46":false, "epo50":false, "epo51":false, "epo52":false, "epo53":false, "server_side":false, "client_side":true},
    {"url":"MSA/articles/devsetup/devsetup.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":false, "epo51":false, "epo52":false, "epo53":false, "server_side":false, "client_side":true},
    {"url":"MSA/articles/Glossary/Glossary.html", "windows":false, "non_windows":false, "on_premise":false, "multi_tenant":false, "epo46":false, "epo50":false, "epo51":false, "epo52":false, "epo53":false, "server_side":false, "client_side":true},
    {"url":"MSA/articles/mngmt_sdk/cma_wrap-api-reference.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":false, "epo51":false, "epo52":false, "epo53":false, "server_side":false, "client_side":true},
    {"url":"MSA/articles/mngmt_sdk/event-database-reference.html", "windows":false, "non_windows":false, "on_premise":false, "multi_tenant":false, "epo46":false, "epo50":false, "epo51":false, "epo52":false, "epo53":false, "server_side":true, "client_side":false},
    {"url":"MSA/articles/mngmt_sdk/event-generation-api-reference.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":false, "epo51":false, "epo52":false, "epo53":false, "server_side":false, "client_side":true},
    {"url":"MSA/articles/mngmt_sdk/implement-management-features.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":false, "epo51":false, "epo52":false, "epo53":false, "server_side":false, "client_side":true},
    {"url":"MSA/articles/mngmt_sdk/integrate-with-epo-management-features.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":false, "epo51":false, "epo52":false, "epo53":false, "server_side":true, "client_side":true},
    {"url":"MSA/articles/mngmt_sdk/plugin-api-reference.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":false, "epo51":false, "epo52":false, "epo53":false, "server_side":false, "client_side":true},
    {"url":"MSA/articles/mngmt_sdk/point-product-certification-test.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":false, "epo51":false, "epo52":false, "epo53":false, "server_side":false, "client_side":true},
    {"url":"MSA/articles/mngmt_sdk/sample-point-product-functional-specification.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":false, "epo51":false, "epo52":false, "epo53":false, "server_side":false, "client_side":true},
    {"url":"MSA/articles/mngmt_sdk/script-command-reference.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":false, "epo52":false, "epo53":false, "server_side":false, "client_side":true},
    {"url":"MSA/articles/mngmt_sdk/scripting-overview.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":false, "epo51":false, "epo52":false, "epo53":false, "server_side":false, "client_side":true},
    {"url":"MSA/articles/mngmt_sdk/test-program-reference.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":false, "epo51":false, "epo52":false, "epo53":false, "server_side":false, "client_side":true},
    {"url":"MSA/articles/mngmt_sdk/update-plugin-api-reference.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":false, "epo51":false, "epo52":false, "epo53":false, "server_side":false, "client_side":true},
    {"url":"MSA/articles/Overview/DataChannel.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":false, "epo51":false, "epo52":false, "epo53":false, "server_side":false, "client_side":true},
    {"url":"MSA/articles/Overview/DataChannel_external.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":false, "epo51":false, "epo52":false, "epo53":false, "server_side":false, "client_side":true},
    {"url":"MSA/articles/Overview/lpc.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":false, "epo51":false, "epo52":false, "epo53":false, "server_side":false, "client_side":true},
    {"url":"MSA/articles/Overview/Overview_MA.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":false, "epo51":false, "epo52":false, "epo53":false, "server_side":false, "client_side":true},
    {"url":"MSA/articles/5.x/MA5_XOverview.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":false, "epo51":false, "epo52":false, "epo53":false, "server_side":false, "client_side":true},
    {"url":"MSA/articles/Overview/Scripting_Overview.htm", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":false, "epo51":false, "epo52":false, "epo53":false, "server_side":false, "client_side":true},
    {"url":"MSA/samples/beacon/beacon.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":false, "epo51":false, "epo52":false, "epo53":false, "server_side":false, "client_side":true},
    {"url":"MSA/tutorials/ManagementFeaturesImplementation/CreateADetectionScript.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":true, "epo51":false, "epo52":false, "epo53":false, "server_side":false, "client_side":true},
    {"url":"MSA/tutorials/ManagementFeaturesImplementation/CreateAPluginPatchScript.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":false, "epo51":false, "epo52":false, "epo53":false, "server_side":false, "client_side":true},
    {"url":"MSA/tutorials/ManagementFeaturesImplementation/CreateReportsOfProductEvents.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":false, "epo51":false, "epo52":false, "epo53":false, "server_side":true, "client_side":false},
    {"url":"MSA/tutorials/ManagementFeaturesImplementation/HowTo.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":false, "epo51":false, "epo52":false, "epo53":false, "server_side":false, "client_side":true},
    {"url":"MSA/tutorials/ManagementFeaturesImplementation/PackagingEventParserPluginFiles.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":false, "epo51":false, "epo52":false, "epo53":false, "server_side":true, "client_side":false},
    {"url":"MSA/tutorials/ManagementFeaturesImplementation/PackagingForRemoteDeployment.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":false, "epo51":false, "epo52":false, "epo53":false, "server_side":true, "client_side":true},
    {"url":"MSA/tutorials/ManagementFeaturesImplementation/WritingAnEventParserPlugin.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":false, "epo51":false, "epo52":false, "epo53":false, "server_side":false, "client_side":true},
    {"url":"MSA/tutorials/ManagementFeaturesIntegration/AccessSitelist.html", "windows":false, "non_windows":true, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":false, "epo51":false, "epo52":false, "epo53":false, "server_side":true, "client_side":false},
    {"url":"MSA/tutorials/ManagementFeaturesIntegration/AutomaticallyDeployOrUpdate.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":false, "epo51":false, "epo52":false, "epo53":false, "server_side":false, "client_side":true},
    {"url":"MSA/tutorials/ManagementFeaturesIntegration/CryptoService.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":false, "epo51":false, "epo52":false, "epo53":false, "server_side":false, "client_side":true},
    {"url":"MSA/tutorials/ManagementFeaturesIntegration/CryptoService_external.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":false, "epo51":false, "epo52":false, "epo53":false, "server_side":false, "client_side":true},
    {"url":"MSA/tutorials/ManagementFeaturesIntegration/EnforcingSettings.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":false, "epo51":false, "epo52":false, "epo53":false, "server_side":false, "client_side":true},
    {"url":"MSA/tutorials/ManagementFeaturesIntegration/EnforcingUserSettings.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":false, "epo51":false, "epo52":false, "epo53":false, "server_side":false, "client_side":true},
    {"url":"MSA/tutorials/ManagementFeaturesIntegration/HowTo.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":false, "epo51":false, "epo52":false, "epo53":false, "server_side":false, "client_side":true},
    {"url":"MSA/tutorials/ManagementFeaturesIntegration/InspectSettingsRemotely.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":false, "epo51":false, "epo52":false, "epo53":false, "server_side":false, "client_side":true},
    {"url":"MSA/tutorials/ManagementFeaturesIntegration/LicenseInformation.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":false, "epo51":false, "epo52":false, "epo53":false, "server_side":false, "client_side":true},
    {"url":"MSA/tutorials/ManagementFeaturesIntegration/MAInfo.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":false, "epo51":false, "epo52":false, "epo53":false, "server_side":false, "client_side":true},
    {"url":"MSA/tutorials/ManagementFeaturesIntegration/Miscellaneous.htm", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":false, "epo51":false, "epo52":false, "epo53":false, "server_side":false, "client_side":true},
    {"url":"MSA/tutorials/ManagementFeaturesIntegration/ScheduleTasks.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":false, "epo51":false, "epo52":false, "epo53":false, "server_side":false, "client_side":true},
    {"url":"MSA/tutorials/ManagementFeaturesIntegration/ScheduleTasksRemotely.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":false, "epo51":false, "epo52":false, "epo53":false, "server_side":false, "client_side":true},
    {"url":"MSA/tutorials/ManagementFeaturesIntegration/SignalingEvents.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":false, "epo51":false, "epo52":false, "epo53":false, "server_side":false, "client_side":true},
    {"url":"MSA/tutorials/ManagementFeaturesIntegration/UpdateFiles.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":false, "epo51":false, "epo52":false, "epo53":false, "server_side":false, "client_side":true},
    {"url":"content/advanced_automation_response.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":false, "epo53":false, "server_side":true, "client_side":false},
    {"url":"content/api.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":false, "server_side":true, "client_side":false},
    {"url":"content/api_scanner.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":false, "server_side":true, "client_side":false},
    {"url":"content/articles.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":false, "epo53":false, "server_side":true, "client_side":true},
    {"url":"content/client_tasks.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":true},
    {"url":"content/contact_information.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":true},
    {"url":"content/create_extension.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":true},
    {"url":"content/data_channel.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":false, "epo53":false, "server_side":true, "client_side":true},
    {"url":"content/events.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":true},
    {"url":"content/examples.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":true, "epo51":true, "epo52":false, "epo53":false, "server_side":true, "client_side":true},
    {"url":"content/example_common_properties.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":false, "server_side":true, "client_side":true},
    {"url":"content/example_custom_properties.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":false, "server_side":true, "client_side":true},
    {"url":"content/example_policy_management.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":false, "server_side":true, "client_side":true},
    {"url":"content/example_product_deployment_multitenant.html", "windows":true, "non_windows":false, "on_premise":false, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":false, "server_side":true, "client_side":true},
    {"url":"content/example_product_deployment_onpremise.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":true, "epo51":true, "epo52":false, "epo53":false, "server_side":true, "client_side":true},
    {"url":"content/example_query_reporting.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":false, "server_side":true, "client_side":true},
    {"url":"content/example_query_reporting_custom_properties.html", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":false, "server_side":true, "client_side":true},
    {"url":"content/get_started.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":true, "epo51":true, "epo52":false, "epo53":false, "server_side":true, "client_side":true},
    {"url":"content/home.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":true},
    {"url":"content/install_plugin_dll.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":false, "epo51":false, "epo52":false, "epo53":false, "server_side":true, "client_side":true},
    {"url":"content/integration.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":true},
    {"url":"content/integration_advanced.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":true, "epo51":true, "epo52":false, "epo53":false, "server_side":true, "client_side":true},
    {"url":"content/integration_basic.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":false, "server_side":true, "client_side":true},
    {"url":"content/interop_services.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":true},
    {"url":"content/known_issues.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":true},
    {"url":"content/master_check_lists.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":true},
    {"url":"content/multitenant_design_considerations.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":false, "server_side":true, "client_side":true},
    {"url":"content/policy_task_comparison.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":true, "epo46":false, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":false},
    {"url":"content/prerequisites.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":true},
    {"url":"content/product_deployment.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":true},
    {"url":"content/product_policies.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":true},
    {"url":"content/product_properties.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":true},
    {"url":"content/reporting.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":true},
    {"url":"content/server_tasks.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":true},
    {"url":"content/setup.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":true, "server_side":true, "client_side":true},
    {"url":"content/tasks.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":false, "epo53":false, "server_side":true, "client_side":true},
    {"url":"content/tutorials.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":false, "epo53":false, "server_side":true, "client_side":true},
    {"url":"content/unified_examples.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":true, "epo53":false, "server_side":true, "client_side":true},
    {"url":"content/whats_new.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":true, "epo46":true, "epo50":true, "epo51":true, "epo52":false, "epo53":false, "server_side":false, "client_side":false},
    {"url":"VirtualAgent/articles/comon_ui_action.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":true, "epo51":true, "epo52":false, "epo53":true, "server_side":true, "client_side":false},
    {"url":"VirtualAgent/articles/connection_errors.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":true, "epo51":true, "epo52":false, "epo53":true, "server_side":true, "client_side":false},
    {"url":"VirtualAgent/articles/mobile.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":true, "epo51":true, "epo52":false, "epo53":true, "server_side":true, "client_side":false},
    {"url":"VirtualAgent/articles/mobileProperties.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":true, "epo51":true, "epo52":false, "epo53":true, "server_side":true, "client_side":false},
    {"url":"VirtualAgent/articles/overview.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":true, "epo51":true, "epo52":false, "epo53":true, "server_side":true, "client_side":false},
    {"url":"VirtualAgent/articles/spotlight_intro.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":true, "epo51":true, "epo52":false, "epo53":true, "server_side":true, "client_side":false},
    {"url":"VirtualAgent/articles/supportability.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":true, "epo51":true, "epo52":false, "epo53":true, "server_side":true, "client_side":false},
    {"url":"VirtualAgent/articles/template.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":true, "epo51":true, "epo52":false, "epo53":true, "server_side":true, "client_side":false},
    {"url":"VirtualAgent/articles/urlpluginprops.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":true, "epo51":true, "epo52":false, "epo53":true, "server_side":true, "client_side":false},
    {"url":"VirtualAgent/articles/URL_Plugin_Design.htm", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":true, "epo51":true, "epo52":false, "epo53":true, "server_side":true, "client_side":false},
    {"url":"VirtualAgent/index.html", "windows":true, "non_windows":true, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":true, "epo51":true, "epo52":false, "epo53":true, "server_side":true, "client_side":false},
    {"url":"SIA/GettingStarted/articles/Code review Checklist.htm", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":true, "epo51":true, "epo52":false, "epo53":false, "server_side":true, "client_side":true},
    {"url":"SIA/GettingStarted/articles/Product_upgrade.htm", "windows":true, "non_windows":false, "on_premise":true, "multi_tenant":false, "epo46":true, "epo50":true, "epo51":true, "epo52":false, "epo53":false, "server_side":true, "client_side":true}
];

function isIE() {
    if (navigator.appName === "Microsoft Internet Explorer") {
        return true;
    } else {
        return false;
    }
}

function isFirefox() {
    if (navigator.userAgent.search("Firefox") > -1) {
        return true;
    }
    else {
        return false;
    }
}

function isChrome() {
    if (/chrom(e|ium)/.test(navigator.userAgent.toLowerCase())) {
        return true;
    } else {
        return false;
    }
}

function br() {
    return $(document.createElement("br"));
}

function hr() {
    return $(document.createElement("hr"));
}

function div() {
    return $(document.createElement("div"));
}

function form() {
    return $(document.createElement("form"));
}

function table() {
    return $(document.createElement("table"));
}

function td() {
    return $(document.createElement("tr"));
}

function tr() {
    return $(document.createElement("tr"));
}

// Configure TOC's for different browsers
var tocItem = $(".TOCItem");
//tocItem.css("position", "relative");
if (isChrome()) {
    // Remove the IE/Firefox hack css
    tocItem.css("top", "15px");
} else if (isFirefox()) {
    tocItem.css("top", "-46px");
} else if (isIE()) {
    tocItem.css("top", "-1px");
}

//Hide broken injected copyrights
$(".copyright").hide();

if (typeof console === "undefined" || (!DEBUG && !isFirefox())) {
    this.console = {
        log: function(msg) { /* do nothing */
        }
    };
}

if (DEBUG) {
    var start_time = new Date().getTime();
}

// Get the directory root of the SDK
var scripts = document.getElementsByTagName('script');
var script_location = scripts[scripts.length - 1].src;
var script_dir = script_location.substring(0, script_location.lastIndexOf('/'));
var SDKDOCROOT = script_dir.substring(0, script_dir.lastIndexOf('/')) + "/";
var local_url_path = window.location.href.substring(SDKDOCROOT.length, window.location.href.length);
var TAB_NAMES = ["Getting Started",
    "Basic Client Integration",
    "Advanced Client Integration",
    "Basic Server Integration",
    "Advanced Server Integration",
    "SDK Samples",
    "McAfee Internal",
    "SIA",
    "API"];
var SIDEBAR_WIDTH = 310;
var ls = null;
var using_old_sidebar = false;
var using_new_sidebar = false;
var using_injected_sidebar = false;
var contents = $("#main-content");
var page_wrap = $("#page-wrap");
var inside = $("#inside");
var search_url = "https://eposdksearch.corp.nai.org:8984/solr/select?json.wrf=?";
var top_filter = 0; //Get Started = 1, Get Integrated = 2, Reference = 3, Samples = 4
var prevSearchTermsList = [];

/* Helper functions */

function browserHasLocalStorage() {
    if (isIE()) { // IE will sometimes break on next condition so this must come first
        return false;
    }

    if ("localStorage" in window && window["localStorage"] && !isFirefox()) {
        return true;
    } else {
        return false;
    }
}

// Load a .js file. You need to wait about 40 ms before it can be used
function loadScript(src) {
    var myjs = document.createElement('script');
    myjs.setAttribute('type', 'text/javascript');
    myjs.setAttribute('src', SDKDOCROOT + src);
    document.getElementsByTagName('head')[0].appendChild(myjs);
}

function getSidebar() {
    var ls = null;
    if (!document.getElementById('left-sidebar2')) { // div with id=left-sidebar2 is not present
        if (document.getElementById('left-sidebar')) {
            ls = $("#left-sidebar");
            using_old_sidebar = true;
            console.log("Found old sidebar, this should be updated to be an empty div with id left-sidebar2.");

        }
        else {
            // No left-sidebar* found
            ls = $(document.createElement('left-sidebar-injected'));
            //$("body").prepend(ls);
            using_injected_sidebar = true;
            console.log("No sidebar found, injecting it in the beginning.");
        }
    }
    else {
        ls = $("#left-sidebar2");
        using_new_sidebar = true;
    }
    return ls;
}

function getIndexOfTabNames(title) {
    for (var i = 0; i < TAB_NAMES.length; i++) {
        if (TAB_NAMES[i] === title) {
            return i;
        }
    }
    // Return error code if it isn't returned
    return -1;
}

/* Local storage objects that support cookies 
 ************************************************************/

// Cookies are used for browsers that do not support local storage
function getCookie(c_name) {
    var c_value = document.cookie;
    var c_start = c_value.indexOf(" " + c_name + "=");
    if (c_start === -1) {
        c_start = c_value.indexOf(c_name + "=");
    }
    if (c_start === -1) {
        c_value = null;
    }
    else {
        c_start = c_value.indexOf("=", c_start) + 1;
        var c_end = c_value.indexOf(";", c_start);
        if (c_end === -1) {
            c_end = c_value.length;
        }
        c_value = unescape(c_value.substring(c_start, c_end));
    }
    return c_value;
}

function setCookie(c_name, value, exdays) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value = escape(value) + ((exdays === null) ? "" : "; expires=" + exdate.toUTCString());
    document.cookie = c_name + "=" + c_value + "; path=/"; //The path only gets applied correctly on some browsers but localStorage should be used anyways
}

/* Local storage object */
function LocalStorageObject(_name, value) {
    var name = _name;

    if (value === undefined) {
        value = 0;
    }

    if (browserHasLocalStorage()) {
        if (localStorage.getItem(name) === null) {
            localStorage.setItem(name, value);
        }
    } else if (getCookie(name) === null) {
        console.log("setting cookie " + name + " to " + value);
        setCookie(name, value);
    }

    this.get = function() {
        if (browserHasLocalStorage()) {
            return localStorage.getItem(name);
        } else {
            return getCookie(name);
        }
    };

    this.set = function(value) {
        if (browserHasLocalStorage()) {
            localStorage.setItem(name, value);
        } else {
            setCookie(name, value);
        }
    };

    this.parseInt = function() {
        if (browserHasLocalStorage()) {
            return parseInt(localStorage.getItem(name), 10);
        }
        else {
            return parseInt(getCookie(name), 10);
        }
    };
}

/*****************************************************************/
/* Local storage variables */

var navIndex = new LocalStorageObject("NAV_INDEX", 0);
var navScrollPos = new LocalStorageObject("NAV_SCROLL_POS", 0);
var newSession = new LocalStorageObject("NEW_SESSION", 1);

var showFilterContent = new LocalStorageObject("SHOW_FILTER_CONTENT", 1);
//var showMetaFilterBox = new LocalStorageObject("SHOW_META_FILTER_BOX", 0);

var topFilter = new LocalStorageObject("TOP_FILTER", 1);

var selectedMFS = new LocalStorageObject("DISPLAY_MFS", 1);
var selectedEPO = new LocalStorageObject("DISPLAY_EPO", 1);
var selectedMSA = new LocalStorageObject("DISPLAY_MSA", 1);

// Store all search keywords
var searchedKeywords = new LocalStorageObject("SEARCHED_TERMS", "");

// Meta filtering checkbox values
var selectedWindows = new LocalStorageObject("DISPLAY_WINDOWS", 1);
var selectedNonWindows = new LocalStorageObject("DISPLAY_NONWINDOWS", 0);
var selectedOnPremise = new LocalStorageObject("DISPLAY_ONPREMISE", 1);
var selectedMultiTenant = new LocalStorageObject("DISPLAY_MULTITENANT", 0);
var selectedEPO46 = new LocalStorageObject("DISPLAY_EPO46", 1);
var selectedEPO50 = new LocalStorageObject("DISPLAY_EPO50", 0);
var selectedEPO51 = new LocalStorageObject("DISPLAY_EPO51", 0);
var selectedEPO52 = new LocalStorageObject("DISPLAY_EPO52", 0);
var selectedClientSide = new LocalStorageObject("DISPLAY_CLIENTSIDE", 0);
var selectedServerSide = new LocalStorageObject("DISPLAY_SERVERSIDE", 0);

var maxNumResults = new LocalStorageObject("MAX_NUM_RESULTS", 20);
var showSearchWindow = new LocalStorageObject("SHOW_SEARCH_WINDOW", 0);
var lastSearchTerm = new LocalStorageObject("LAST_SEARCH_TERM", "Search the SDK");

var numExternalScriptLoads = 0;
var maxNumExternalScriptLoadAttempts = 4; // This is to prevent it from looping infinitely if script is not found

function createNavigation() {
    ls.empty();
    ls.remove();
    var body = $("body");
    if (using_old_sidebar || using_new_sidebar) {
        //ls.css("left", "-10px");
    } else if (using_injected_sidebar) {
        body.prepend($(document.createElement("br"))); // Add a newline (This is for pages that are not set up correctly).
    }
    ls.width(SIDEBAR_WIDTH);
    body.prepend(ls);
    var navHeader = createHeader();
    body.prepend(navHeader);

    var accordion = createAccordion();
    ls.append(accordion);
    ls.prepend(createFilterBox());
    $(window).trigger('resize');
    ls.scrollTop(window.navScrollPos.parseInt());

    if (window.showSearchWindow.parseInt() === 1) {
        showSearchDialog();
    }
}

function showSearchDialog()
{
    if ($("#search-input-submit").length > 0) {
        $("#search-input-submit").trigger("click");
    } else {
        window.setTimeout(showSearchDialog, 100);
    }
}

function setup() {
    if (typeof jQuery === 'undefined' && numExternalScriptLoads < maxNumExternalScriptLoadAttempts) {
        console.log("jQuery not loaded, loading...");
        numExternalScriptLoads++;
        setTimeout(setup, 1); // If we didn't wait long enough for it to load, try again until it works
        return;
    }

    if (typeof jQuery.ui === 'undefined' && numExternalScriptLoads < maxNumExternalScriptLoadAttempts) {
        console.log("jQueryUI not loaded, loading...");
        numExternalScriptLoads++;
        loadScript("js/jquery-ui.min.js");
        setTimeout(setup, 40); // Make sure jqueryui is loaded
        return;
    }

    ls = getSidebar();

    if (window.newSession.parseInt() !== 1 && window.newSession.parseInt() !== 2) {
        console.log("Resetting new session information");
        window.newSession.set(2);
    }

    if (window.newSession.parseInt() === 2) {
        console.log("Resetting new session to start at Get Started");
        window.topFilter.set(1);
        window.selectedMSA.set(1);
        window.selectedEPO.set(1);
        window.selectedMFS.set(1);
        window.newSession.set(1);
    }

    contents.append($(document.createElement("br"))); // Make sure there is sufficient space at the bottom to account for the footer
    contents.append($(document.createElement("br")));

    $(window).resize(function() {
        var winheight = $(window).height();
        var winwidth = $(window).width();
        var ls = getSidebar();
        ls.height(winheight - 85);
        var cwidth = winwidth - ls.width() - 75;
        var cheight = winheight - 80;
        page_wrap.width(cwidth);
        page_wrap.height(cheight);
        contents.width(cwidth);
        contents.height(cheight);
        var search_results = $("#search-results");
        search_results.css("left", ($(window).width() - search_results.width() - 50) + "px"); // connected to right side
        search_results.height($(window).height() - 120);
        $("#feedback-mailto-link").css("left", ($(window).width() - 160) + "px");
        $("#settings-menu-icon").css("left", ($(window).width() - 40) + "px");
        if (winwidth < 950) {
            $("#search-container").hide();
        } else {
            $("#search-container").show();
            $("#search-form").show();
        }
    });

    $("body").append(createFooter());

    createNavigation();
    if (DEBUG) {
        console.log("Total time to load script = " + (new Date().getTime() - start_time) + "ms");
    }
}

/* Create navigation */
setup();


function getMetaForCurrentPage() {
    return getMetaForPageByUrl(local_url_path);
}

var current_page_meta = getMetaForCurrentPage();

function getMetaForPageByUrl(url) {
    for (var i = 0; i < window.meta.length; i++) {
        if (url.indexOf(window.meta[i].url) > -1) {
            return window.meta[i];
        }
    }
    //console.log("Warning, no meta specified for " + url);
    return -1; // no meta specified for document
}




function createHeader() {
    var header = $(document.createElement("header"));
    header.height(45);
    header.attr("id", "nav-header");

    var sdk_link = $(document.createElement("a"));
    var sdk_lb = $(document.createElement("div"));
    sdk_link.attr("href", SDKDOCROOT + "content/home.html");
    sdk_link.attr("id", "top-left-home-link");
    sdk_link.append("ePO SDK");
    sdk_lb.attr("id", "top-left-home-link-lb");
    sdk_link.click(function() {
        window.topFilter.set(1);
    });
    sdk_lb.append(sdk_link);
    header.append(sdk_lb);

    var get_started_link = $(document.createElement("a"));
    var get_started_link_cn = $(document.createElement("div"));
    get_started_link.attr("href", SDKDOCROOT + "content/home.html");
    get_started_link_cn.attr("id", "get-started");
    get_started_link.append("Get Started");
    get_started_link.click(function() {
        window.topFilter.set(1);
    });
    if (window.topFilter.parseInt() === 1) {
        get_started_link.css("font-weight", "bold");
        get_started_link_cn.css("border-top-left-radius", "5px");
        get_started_link_cn.css("border-top-right-radius", "5px");
        get_started_link_cn.css("background-color", "rgb(255, 255, 255)");
    }
    get_started_link_cn.append(get_started_link);
    header.append(get_started_link_cn);

    var get_integrated_link = $(document.createElement("a"));
    var get_integrated_link_cn = $(document.createElement("div"));
    get_integrated_link.attr("href", SDKDOCROOT + "content/integration.html");
    get_integrated_link_cn.attr("id", "get-integrated");
    get_integrated_link.append("Get Integrated");
    get_integrated_link.click(function() {
        window.topFilter.set(2);
    });
    if (window.topFilter.parseInt() === 2) {
        get_integrated_link.css("font-weight", "bold");
        get_integrated_link_cn.css("border-top-left-radius", "5px");
        get_integrated_link_cn.css("border-top-right-radius", "5px");
        get_integrated_link_cn.css("background-color", "rgb(255, 255, 255)");
    }
    get_integrated_link_cn.append(get_integrated_link);
    header.append(get_integrated_link_cn);

    var reference_link = $(document.createElement("a"));
    var reference_link_cn = $(document.createElement("div"));
    reference_link.attr("href", SDKDOCROOT + "content/articles.html");
    reference_link_cn.attr("id", "reference");
    reference_link.append("Reference");
    reference_link.click(function() {
        window.topFilter.set(3);
    });
    if (window.topFilter.parseInt() === 3) {
        reference_link.css("font-weight", "bold");
        reference_link.css("color", "rgb(0, 0, 0)");
        reference_link_cn.css("border-top-left-radius", "5px");
        reference_link_cn.css("border-top-right-radius", "5px");
        reference_link_cn.css("background-color", "rgb(255, 255, 255)");
    }
    reference_link_cn.append(reference_link);
    header.append(reference_link_cn);
    var samples_link = $(document.createElement("a"));
    var samples_link_cn = $(document.createElement("div"));
    samples_link.attr("href", SDKDOCROOT + "content/tutorials.html");
    samples_link_cn.attr("id", "samples");
    samples_link.append("Samples");
    samples_link.click(function() {
        window.topFilter.set(4);
        window.navIndex.set(5);
    });
    if (window.topFilter.parseInt() === 4) {
        samples_link.css("font-weight", "bold");
        samples_link.css("font-weight", "bold");
        samples_link_cn.css("border-top-left-radius", "5px");
        samples_link_cn.css("border-top-right-radius", "5px");
        samples_link_cn.css("background-color", "rgb(255, 255, 255)");
    }
    samples_link_cn.append(samples_link);
    header.append(samples_link_cn);

    //Make sure the search server is reachable before adding the search bar
    $.getJSON(search_url, {'wt': 'json'},
    function() {
        header.append(createSearchBox());
    });
    var current_path = "<ePO-SDK>/" + local_url_path;
    var message_body = "I'd Like to provide the following feedback on the ePO SDK.";
    var message_subject = "Feedback on the SDK Documentation in " + current_path;
    //Note: multiple recipients is not supported by all email clients. Outlook seems to work with all three and thunderbird gets the first two
    // but a standard mailto link is only supposed to have one.
    //var message_recipients = "_1adb0@McAfee.com";
    var message_recipients = "sdk-feedback@mileaccess.com";
    var mailto_link = $(document.createElement("a"));
    mailto_link.attr("href", "mailto:" + message_recipients + "&subject=" + message_subject + "&body=" + message_body + " ");
    var feedback_icon = $(document.createElement("img"));
    feedback_icon.attr("src", SDKDOCROOT + "images/feedback-icon.png");
    feedback_icon.attr("alt", "Provide Feedback About this Page");
    mailto_link.attr("id", "feedback-mailto-link");
    mailto_link.attr("title", "Provide feedback for this page");
    mailto_link.tooltip();
    mailto_link.append(feedback_icon);
    header.append(mailto_link);
    var vline = $(document.createElement("hr"));
    vline.height(39);
    vline.width(0);
    header.append(vline);

    // McAfee logo
    var logo_container = $(document.createElement("div"));
    logo_container.attr("id", "logo-top-right");
    logo_container.width(SIDEBAR_WIDTH + 5);
    var logo_img = $(document.createElement("img"));
    logo_img.attr("alt", "Home");
    logo_img.attr("src", SDKDOCROOT + "images/mfe_ePO_logo.png");
    var logo_link = $(document.createElement("a"));
    logo_link.attr("href", SDKDOCROOT + "content/home.html");
    logo_link.append(logo_img);
    logo_link.width(245);
    logo_link.click(function() {
        window.topFilter.set(1);
    });
    logo_container.append(logo_link);
    logo_container.height(45);
    header.append(logo_container);
    return header;
}

function createCurrentMetaInfoString() {
    // Meta info
    var meta_info = $(document.createElement("div"));
    meta_info.attr("id", "meta-info");


    var pageInfo = getMetaForCurrentPage();
    var meta_str = "";
    if (pageInfo === -1) {
        meta_str = "No meta information availible for this page";
    } else {
        //meta_str = "Meta info: ";
        if (pageInfo.windows === true) {
            meta_str += "| windows ";
        }
        if (pageInfo.non_windows === true) {
            meta_str += "| non windows ";
        }
        if (pageInfo.on_premise === true) {
            meta_str += "| on-premise ";
        }
        if (pageInfo.on_premise === true) {
            meta_str += "| multi-tenant ";
        }
        if (pageInfo.epo46 === true) {
            meta_str += "| ePO 4.6 ";
        }
        if (pageInfo.epo50 === true) {
            meta_str += "| ePO 5.0 ";
        }
        if (pageInfo.epo51 === true) {
            meta_str += "| ePO 5.1 ";
        }
        if (pageInfo.epo52 === true) {
            meta_str += "| ePO 5.2 ";
        }
        if (pageInfo.client_side === true) {
            meta_str += "| client-side ";
        }
        if (pageInfo.server_side === true) {
            meta_str += "| server-side ";
        }
        meta_str += "|";
    }

    meta_info.append(meta_str);
    return meta_info;
}
$('body').prepend(createCurrentMetaInfoString());

function createCheckBox(id, label, isChecked, setCheck) {
    var cb_input = $(document.createElement("input"));
    cb_input.attr("type", "checkbox");
    cb_input.attr("name", "content_filter");
    cb_input.attr("id", id);
    cb_input.attr("value", id);
    if (isChecked()) {
        cb_input.prop("checked", true);
    }
    cb_input.click(function() {
        if (cb_input.is(":checked")) {
            setCheck(1);
        } else {
            setCheck(0);
        }
        createNavigation();
    });

    var cb_lb = $(document.createElement("label"));
    cb_lb.append(label);
    var cb_cn = $(document.createElement("div"));
    cb_cn.attr("class", "filter-checkbox");
    cb_cn.append(cb_input);
    cb_cn.append(cb_lb);
    return cb_cn;
}


function createFilterBox() {
    // Checkboxes
    var cb_form = form();
    cb_form.append(createCheckBox("cb-mfs", "MFS", window.selectedMFS.parseInt, window.selectedMFS.set));
    cb_form.append(createCheckBox("cb-epo", "ePO", window.selectedEPO.parseInt, window.selectedEPO.set));
    cb_form.append(createCheckBox("cb-msa", "MSA", window.selectedMSA.parseInt, window.selectedMSA.set));

    var settings_pane = div();
    settings_pane.css("margin", "0px");
    settings_pane.css("padding", "0px");

    settings_pane.append(document.createElement("br"));
    settings_pane.append(createCheckBox("cb-platform-windows", "Windows", window.selectedWindows.parseInt, window.selectedWindows.set));
    settings_pane.append(createCheckBox("cb-platform-nonwindows", "Non-Windows", window.selectedNonWindows.parseInt, window.selectedNonWindows.set)
            .addClass("checkbox-column2"));
    settings_pane.append(document.createElement("br"));
    settings_pane.append(createCheckBox("cb-deployment-onpremise", "On-Premise", window.selectedOnPremise.parseInt, window.selectedOnPremise.set));
    settings_pane.append(createCheckBox("cb-deployment-multitenant", "Multi-Tenant", window.selectedMultiTenant.parseInt, window.selectedMultiTenant.set)
            .addClass("checkbox-column2"));
    settings_pane.append(document.createElement("br"));
    settings_pane.append(createCheckBox("cb-version-epo46", "ePO 4.6", window.selectedEPO46.parseInt, window.selectedEPO46.set));
    settings_pane.append(createCheckBox("cb-version-epo50", "ePO 5.0", window.selectedEPO50.parseInt, window.selectedEPO50.set)
            .addClass("checkbox-column2"));
    settings_pane.append(br());
    settings_pane.append(createCheckBox("cb-version-epo51", "ePO 5.1", window.selectedEPO51.parseInt, window.selectedEPO51.set));
    settings_pane.append(createCheckBox("cb-version-epo52", "ePO 5.2", window.selectedEPO52.parseInt, window.selectedEPO52.set)
            .addClass("checkbox-column2"));
    settings_pane.append(document.createElement("br"));
    settings_pane.append(createCheckBox("cb-technology-client", "Client-Side", window.selectedClientSide.parseInt, window.selectedClientSide.set));
    settings_pane.append(createCheckBox("cb-technology-server", "Server-Side", window.selectedServerSide.parseInt, window.selectedServerSide.set)
            .addClass("checkbox-column2"));
    settings_pane.append(br()).append(br());
    settings_pane.append(hr());
    cb_form.append(settings_pane);


    // Checkboxes in filter accordion
    var filter_accordion = $(document.createElement("div"));
    var filter_accordion_h3 = $(document.createElement("h3"));
    filter_accordion_h3.attr("id", "filter-accordion-label");
    filter_accordion_h3.append("Filter Content");
    filter_accordion_h3.click(function() {
        if (window.showFilterContent.parseInt() === 1) {
            window.showFilterContent.set(0);
        } else {
            window.showFilterContent.set(1);
        }
    });
    //filter_accordion_h3.css("font-size", "14px");
    var filter_accordion_content = $(document.createElement("div"));
    filter_accordion_content.attr("id", "filter-accordion");
    filter_accordion_content.append(cb_form);
    filter_accordion.append(filter_accordion_h3);
    filter_accordion.append(filter_accordion_content);
    if (window.showFilterContent.parseInt() === 0) {
        filter_accordion.accordion({
            clearStyle: true,
            autoHeight: true,
            collapsible: true,
            heightStyle: "content",
            active: false,
            event: "click",
            animate: false
        });
    } else {
        filter_accordion.accordion({
            clearStyle: true,
            autoHeight: true,
            collapsible: true,
            heightStyle: "content",
            active: 0,
            event: "click",
            animate: false
        });
    }
    filter_accordion.append(br());
    return filter_accordion;

}

var prevSearchTermsList = [];

function createSearchBox() {
    // Search Bar
    var search_container = $(document.createElement("div"));
    search_container.addClass("search");
    search_container.attr("id", "search-container");
    var search_form = $(document.createElement("form"));
    search_form.attr("id", "search-form");
    search_form.attr("method", "post");
    var search_input_text = $(document.createElement("input"));
    search_input_text.attr("id", "search-input-text");
    search_input_text.attr("name", "query");
    search_input_text.attr("onblur", "if(this.value==''){this.value='';}");
    search_input_text.attr("onfocus", "if(this.value=='Search the SDK'){this.value='';}");
    search_input_text.attr("type", "text");
    search_input_text.attr("value", window.lastSearchTerm.get());
    search_input_text.css("height", "100%");

    var search_input_submit = $(document.createElement("input"));
    search_input_submit.attr("id", "search-input-submit");
    search_input_submit.attr("type", "submit");
    search_input_submit.attr("value", "");
    search_form.append(search_input_text);
    search_form.append(search_input_submit);
    search_container.append(search_form);
    search_input_submit.css("background", "url(" + SDKDOCROOT + "images/search-zoom-icon.png) no-repeat 10px 4px");
    search_input_submit.click(performSearch(search_url));
    if ($(window).width() < 900) {
        search_container.hide();
        search_form.hide();
    }

    return search_container;
}

function removeDuplicatsFromArray(arr) {
    var uniqueNames = [];
    $.each(arr, function(i, el) {
        if ($.inArray(el, uniqueNames) === -1)
            uniqueNames.push(el);
    });
    return uniqueNames;
}

function updateSearchAutocomplete()
{
    var previousKeywordsStr = window.searchedKeywords.get();
    previousKeywordsStr += $("#search-input-text").val().replace(";", "") + ";";
    window.lastSearchTerm.set($("#search-input-text").val());
    window.searchedKeywords.set(previousKeywordsStr);
    window.showSearchWindow.set("1");

    if (isIE()) {
        if (navigator.appVersion.indexOf("MSIE 1") === -1) {
            // IE9 or lower, does not support split() :/
        }
    } else {
        //Make search an autocomplete
        arrNoDuplicates = removeDuplicatsFromArray(window.searchedKeywords.get().split(";"));

        //console.log(arrNoDuplicates);
        //window.prevSearchTermList.set("");
        window.prevSearchTermsList = [];
        arrNoDuplicates.forEach(function(a) {
            if (a !== "" && a !== " ") {
                window.prevSearchTermsList.push(a);
            }
        });
    }

    //ensure no duplicates are saved in local storage
    var newSearchTermsStr = "";
    window.prevSearchTermsList.forEach(function(a) {
        newSearchTermsStr += a + ";";
    });

    console.log("newSearchTermStr " + newSearchTermsStr);
    window.searchedKeywords.set(newSearchTermsStr);

    var search_input_text = $("#search-input-text");
    search_input_text.attr("autocomplete", "off");

    search_input_text.autocomplete({
        source: function(request, response) {
            var results = $.ui.autocomplete.filter(window.prevSearchTermsList, request.term);
            response(results.slice(results.length - 5, results.length).reverse());
        }
        //source: window.prevSearchTermsList
    });
}

function performSearch(searchUrl) {

    var crawlBase = "http://localhost:8002/";
    $.getJSON(searchUrl, {'wt': 'json'},
    function() {
        var search_results = $("#search-results");

        // If the request succeeds, assume that Search is online and show the search box.
        $(".search")
                .show()
                .submit(function(e) {
                    updateSearchAutocomplete();
                    e.preventDefault();
                    search_results.empty().show();
                    search_results.css("left", ($(window).width() - 275) + "px");


                    $(document.createElement("div"))
                            .addClass("loading")
                            .text("Loading")
                            .css("left", ($(window).width() - 375) + "px")
                            .width(150).height(100)
                            .appendTo("#search-results");
                    $.getJSON(
                            searchUrl,
                            {
                                wt: 'json',
                                q: $("#search-input-text").val(),
                                rows: window.maxNumResults.parseInt()
                            },
                    function(res) {
                        // Remember search terms


                        var sr = $("#search-results");
                        sr.css("left", ($(window).width() - sr.width() - 50) + "px"); // connected to right side

                        //sr.height($(window).height() - 120);
                        sr.empty().show();

                        var maxNumResultsConfig = $(document.createElement("div")).css("float", "right");
                        maxNumResultsConfig.append("limit to ");
                        var maxNumResultsInput = $(document.createElement("input"));
                        maxNumResultsInput.val(window.maxNumResults.get());
                        maxNumResultsInput.width(50);
                        //maxNumResultsInput.spinner(); // not working properly
                        maxNumResultsInput.attr("type", "number")
                                .attr("min", "1").attr("max", "10000")
                                //.attr("step", "5")
                                .attr("id", "maxNumResultsInput");


                        maxNumResultsConfig.append(maxNumResultsInput);
                        maxNumResultsConfig.append(" results ");



                        var applyButton = $(document.createElement("button")).append("apply")
                                .on("click", function(a) {
                                    window.showSearchWindow.set("1");
                                    window.maxNumResults.set($("#maxNumResultsInput").val());
                                    location.reload();
                                });
                        maxNumResultsConfig.append(applyButton);

                        sr.append(maxNumResultsConfig);

                        var closeBtn = $(document.createElement("a"));
                        closeBtn.append("X");
                        closeBtn.css("cursor", "pointer");
                        closeBtn.click(function() {
                            sr.hide();
                            window.showSearchWindow.set("0");
                        });
                        sr.append(closeBtn);
                        sr.append($(document.createElement("h3")).append("Search Results").css("text-align", "center"));

                        if (!res.response.docs.length) {
                            $("<p>")
                                    .text("No Results Found")
                                    .appendTo("#search-results");
                            return;
                        }
                        // If we've made it this far, show the result list.
                        var list = $("<ul>").appendTo("#search-results");
                        $.each(res.response.docs, function(i, doc) {
                            if (!doc.title) {
                                return;
                            }
                            var li_a = $(document.createElement("li")).append(createLink(doc.title, doc.url.replace(crawlBase, "")));
                            list.append(li_a);
                            search_results.width(500);
                            search_results.css("left", ($(window).width() - search_results.width() - 50) + "px"); // connected to right side

                        });
                    }
                    );
                });
        // Create the search results box, position it, then hide it.
        $("<ul/>")
                .attr("id", "search-results")
                .appendTo("body")
                .css({
                    top: "35px",
                    position: "fixed",
                    height: $(window).height() - 120
                }).hide();

        $(document).click(function(e) {
            var sr = $("#search-results");
            var srpos = sr.position();
            // Hide if clicked to the left
            if (e.pageX < srpos.left) {
                sr.hide();
                window.showSearchWindow.set("0");
            }
        });
    });
}

function createFooter() {
    var footer = $(document.createElement("footer"));
    var text_div = $(document.createElement("div"));
    text_div.attr("id", "footer-text");
    footer.attr("id", "footer-bar");
    // If the build numbers have not been inserted, don't display the rest of the footer.
    // This is broken up so that it doesn't get replaced by the build process
    var search_str = "_SDK" + "_" + "ver" + "_";
    if (footer_text.search(search_str) !== -1) {
        footer_text = "Copyright &copy; McAfee, Inc.";
    }
    text_div.append(footer_text);
    footer.append(text_div);
    return footer;
}

function createAccordionTab(title, data, accordion) {

    //process data, and make sure only checked(mfs/epo/ma) items are used
    var filtered_data1 = [];

    for (var i = 0; i < data.length; i++) {
        var showItemMetaFilter = false;
        var showItemURLFilter = false;
        if (data[i][1].substring(0, 1) === "^" || data[i][1].substring(1, 2) === "^") {
            // If a URL has the prefix ^MFS&EPO or similar, it means that it requires one of them to be present in order be displayed
            var has_mfs = false;
            var has_epo = false;
            var has_msa = false;
            // Check to see which prefixes are needed
            for (var j = 0; j < 13; j++) {
                if (data[i][1].indexOf("MFS") > -1) {
                    has_mfs = true;
                }
                if (data[i][1].indexOf("ePO") > -1) {
                    has_epo = true;
                }
                if (data[i][1].indexOf("MSA") > -1) {
                    has_msa = true;
                }
            }
            // handle epo and mfs
            if (has_mfs && has_epo && (window.selectedMFS.parseInt() || window.selectedEPO.parseInt())) {
                showItemURLFilter = true;
            }
            // handle mfs and msa
            else if (has_mfs && has_msa && (window.selectedMFS.parseInt() || window.selectedMSA.parseInt())) {
                showItemURLFilter = true;
            }
            // handle epo and msa
            else if (has_epo && has_msa && (window.selectedEPO.parseInt() || window.selectedMSA.parseInt())) {
                showItemURLFilter = true;
            }
            // has all three
            else if (has_mfs && has_epo && has_msa && (window.selectedMFS.parseInt() || window.selectedEPO.parseInt() || window.selectedMSA.parseInt())) {
                showItemURLFilter = true;
            }
        }
        else if (data[i][1].indexOf("MFS") > -1) {
            if (window.selectedMFS.parseInt()) {
                showItemURLFilter = true;
            }
        }
        else if (data[i][1].indexOf("ePO") > -1) {
            if (window.selectedEPO.parseInt()) {
                showItemURLFilter = true;
            }
        }
        else if (data[i][1].indexOf("MSA") > -1) {
            if (window.selectedMSA.parseInt()) {
                showItemURLFilter = true;
            }
        } else if (data[i][1].indexOf("content/") > -1) {
            showItemURLFilter = true;
        }

        if (data[i][1][0] !== "#") {
            var linkUrl = data[i][1];
            linkmeta = getMetaForPageByUrl(data[i][1]);
            if (linkmeta === -1) {
                showItemMetaFilter = false;
            } else {
                // Meta checkbox filter logic
                if (
                        ((window.selectedWindows.parseInt() === 1) && linkmeta.windows) ||
                        ((window.selectedNonWindows.parseInt() === 1) && linkmeta.non_windows) ||
                        ((window.selectedOnPremise.parseInt() === 1) && linkmeta.on_premise) ||
                        ((window.selectedMultiTenant.parseInt() === 1) && linkmeta.multi_tenant) ||
                        ((window.selectedEPO46.parseInt() === 1) && linkmeta.epo46) ||
                        ((window.selectedEPO50.parseInt() === 1) && linkmeta.epo50) ||
                        ((window.selectedEPO51.parseInt() === 1) && linkmeta.epo51) ||
                        ((window.selectedEPO52.parseInt() === 1) && linkmeta.epo52) ||
                        ((window.selectedClientSide.parseInt() === 1) && linkmeta.client_side) ||
                        ((window.selectedServerSide.parseInt() === 1) && linkmeta.server_side)

                        ) {
                    showItemMetaFilter = true;
                } else if (
                        /* all checked is the same as none checked */
                                (window.selectedWindows.parseInt() === 0) &&
                                (window.selectedNonWindows.parseInt() === 0) &&
                                (window.selectedOnPremise.parseInt() === 0) &&
                                (window.selectedMultiTenant.parseInt() === 0) &&
                                (window.selectedEPO46.parseInt() === 0) &&
                                (window.selectedEPO50.parseInt() === 0) &&
                                (window.selectedEPO51.parseInt() === 0) &&
                                (window.selectedEPO52.parseInt() === 0) &&
                                (window.selectedClientSide.parseInt() === 0) &&
                                (window.selectedServerSide.parseInt() === 0)
                                ) {
                    showItemMetaFilter = true;
                }
            }
        }

        if (data[i][1].indexOf("http://") > -1 || data[i][1].indexOf("https://") > -1) {
            showItemMetaFilter = true;
            showItemURLFilter = true;
        } else if (data[i][1].indexOf("SIA") > -1) {
            //TODO: obtain SIA meta info
            showItemMetaFilter = true;
            showItemURLFilter = true;
        } else if (data[i][1].indexOf("$") > -1) {
            showItemMetaFilter = true;
            showItemURLFilter = true;
        }

        if (showItemMetaFilter && showItemURLFilter) {
            filtered_data1.push(data[i]);
        }
    } // for

    //remove headings that don't link to anyware
    // who's children were filtered out.
    var filtered_data = [];
    for (var i = 0; i < filtered_data1.length; i++) {      
        if(filtered_data1[i][1].indexOf("$") > -1
                && filtered_data1[i][1].indexOf("*") === -1) {
            
            // heading without link
            // if the next item is not bold, display it, otherwise dont
            
            if(i+1 < filtered_data1.length) {
                if(filtered_data1[i+1][1].indexOf("$") === -1) {
                    filtered_data.push(filtered_data1[i]);
                }
            }
        } else {
            filtered_data.push(filtered_data1[i]);
        }
    }
    
    // Filter the top menu/links selection
    if (window.topFilter.parseInt() === 1 // Get Started section hides the following tabs
            && (title === "Basic Client Integration"
                    || title === "Advanced Client Integration"
                    || title === "Basic Server Integration"
                    || title === "Advanced Server Integration"
                    || title === "SDK Contents"
                    || title === "SDK Samples"
                    || title === "Articles"
                    || title === "API"
                    )) {
        return;
    } else if (window.topFilter.parseInt() === 2 // Get Integrated section hides the following tabs
            && (title === "Getting Started"
                    || title === "SDK Contents"
                    || title === "McAfee Internal"
                    || title === "SIA"
                    || title === "Articles"
                    || title === "API"
                    || title === "SDK Samples"
                    )) {
        return;
    } else if (window.topFilter.parseInt() === 3 // Reference section hides the following tabs
            && (title === "Getting Started"
                    || title === "SDK Contents"
                    || title === "McAfee Internal"
                    || title === "SIA"
                    || title === "SDK Samples"
                    )) {
        return;
    } else if (window.topFilter.parseInt() === 4 // Samples section hides the following tabs
            && (title === "Getting Started"
                    || title === "Basic Client Integration"
                    || title === "Advanced Client Integration"
                    || title === "Basic Server Integration"
                    || title === "Advanced Server Integration"
                    || title === "McAfee Internal"
                    || title === "SIA"
                    || title === "Articles"
                    || title === "API"
                    )) {
        return;
    }

    var header = $(document.createElement("h3"));
    header.append(document.createTextNode(title));
    header.bind("click", {nav: getIndexOfTabNames(title)}, function(e) {
        var data = e.data;
        if (data.nav === window.navIndex.parseInt()) {
            window.navIndex.set(-1);
        } else {
            window.navIndex.set(data.nav);
        }
    });

    accordion.append(header);
    var tab = $(document.createElement("div"));
    var menu = createMenu();
    for (var i = 0; i < filtered_data.length; i++) {
        var menu_item = createMenuItem(filtered_data[i][0], filtered_data[i][1]);
        if (menu_item) {
            menu.append(menu_item);
        }
    }

    // Internet explorer can't handle jqueryui menus...
    if (isIE()) {
        menu.css("font-size", "11px");
        menu.css("text-decoration", "none");
        menu.css("list-style-type", "none");
        menu.css("padding", "5px");
        menu.css("padding-left", "2px");
        menu.css("line-height", "12px");
        menu.width(SIDEBAR_WIDTH - 12);
    }
    else {
        menu.menu();
    }

    tab.append(menu);
    accordion.append(tab);
}

function createAccordion() {
    var accordion = $(document.createElement("div"));
    accordion.attr("id", "accordion");
    accordion.width(SIDEBAR_WIDTH);
    // Getting Started
    var acc1data = [
        ["Overview", "$^MFS&ePO&MSA"], //$ means submenu title
        //["", "~^MFS&ePO&MSA"], //~ means line seperator
        //["Server Side", "%^MFS&ePO"], //% means submenu 2 title
        ["Introduction to ePO Integration", "ePO/Overview/ePOIntroduction.htm"],
        ["MFS Overview", "MFS/articles/Overview/Overview_Orion.html"],
        ["Extension Points for Integration", "ePO/Overview/EPOExtensionPointOverview.htm"],
        ["General UI Concepts", "MFS/api/ui/GeneralUIConcepts.html"],
        ["UI Best Practices", "ePO/articles/UIBestPractices.htm"],
        ["McAfee Agent Overview", "MSA/articles/Overview/Overview_MA.html"],
        ["Script Engine Overview", "MSA/articles/Overview/Scripting_Overview.htm"],
        ["Virtual Agent (MePO) Overview", "VirtualAgent/articles/overview.html"],
        ["Set up Dev Environment", "$^ePO&MSA"],
        ["Set up the Server Dev Environment", "ePO/tutorials/PointProductSetup.htm"],
        ["Set up the Client Dev Environment (MA)", "MSA/articles/devsetup/devsetup.html"],
        ["Requirements and Check Lists", "$*content/master_check_lists.html"]
    ];
    createAccordionTab(TAB_NAMES[0], acc1data, accordion);
    // Basic Client Integration
    var bci_data = [
        ["Product Properties", "$*content/product_properties.html"],
        ["Implement Property Collection", "MSA/tutorials/ManagementFeaturesIntegration/InspectSettingsRemotely.html"],
        ["Install Plug-in DLL", "content/install_plugin_dll.html"],
        ["Events", "$^MFS&ePO*content/events.html"],
        ["Event Notification and Reporting", "MSA/tutorials/ManagementFeaturesIntegration/SignalingEvents.html#typical-steps-for-ma-event-integration"],
        ["Event Generation API", "MSA/apis/EventGeneration/EventGeneration_Reference.htm"],
        ["Deployment", "$MSA*content/product_deployment.html"],
        ["Checklist for Product Deployment", "SIA/GettingStarted/articles/Code%20review%20Checklist.htm"],
        ["Create a Detection Script", "MSA/tutorials/ManagementFeaturesIntegration/AutomaticallyDeployOrUpdate.html#create_detection_script"],
        ["Implement Update Callback with LPC", "MSA/tutorials/ManagementFeaturesIntegration/AutomaticallyDeployOrUpdate.html#implement_updatecallback_api"],
        ["Create a Package Catalog File", "MSA/tutorials/ManagementFeaturesIntegration/AutomaticallyDeployOrUpdate.html#create_pkg_catalog_file"],
        ["Package your Product", "MSA/tutorials/ManagementFeaturesIntegration/AutomaticallyDeployOrUpdate.html#package_product"],
        ["Product Upgrade", "$*SIA/GettingStarted/articles/Product_upgrade.htm"]
    ];

    // Basic Client Info that is diplayed in the Reference Tab
    var bci_ref_data = [
        ["Overview", "$^MSA&ePO"],
        ["Agent Overview", "MSA/articles/Overview/Overview_MA.html"],
        ["Script Engine Overview", "MSA/articles/Overview/Scripting_Overview.htm"],
        ["LPC: an Integration Framework", "MSA/articles/Overview/lpc.html"],
        ["Form Validation, Dirty-Bit Checking and Required Fields", "ePO/articles/EPOFormValidation.html"],
        ["Plugin and Extension Dependencies", "ePO/articles/ExtensionDependencies.htm"],
        ["Product Properties", "$*content/product_properties.html"],
        ["Implementing Common Product Properties", "ePO/articles/ePOCommonProductProperties.htm"],
        ["Implementing Custom Product Properties", "ePO/articles/ePOCustomProductProperties.htm"],
        ["Implement Property Collections", "MSA/tutorials/ManagementFeaturesIntegration/InspectSettingsRemotely.html"],
        ["Install Plug-in DLL", "content/install_plugin_dll.html"],
        ["Events", "$^MFS&ePO*content/events.html"],
        ["Common Event Format", "ePO/articles/EPOCommonEventFormat.htm"],
        ["Common Event Categories", "ePO/articles/EPOCommonEventCategories.htm"],
        ["Common Event Severity", "ePO/articles/EPOCommonEventSeverity.htm"],
        ["Common Event XML Format", "ePO/articles/ePOCommonEventXMLFormat.htm"],
        ["Event Notification and Reporting", "MSA/tutorials/ManagementFeaturesIntegration/SignalingEvents.html"],
        ["Event Generation API Windows", "MSA/apis/EventGeneration/EventGeneration_Reference.htm#creating-and-using-an-event-interface-object"],
        ["Event Generation API Non-Windows", "MSA/apis/EventGeneration/EventGeneration_Reference.htm#Event-Generation-API-Reference-for-Non-Windows"],
        ["ePO Event Filter and Repository Keys Notification", "ePO/articles/ePONotificationServices.html"]
    ];

    if (window.topFilter.parseInt() === 3) {
        createAccordionTab(TAB_NAMES[1], bci_ref_data, accordion);
    } else {
        createAccordionTab(TAB_NAMES[1], bci_data, accordion);
    }
    // Advanced Client Integration
    var aci_data = [
        ["Policies", "$MSA"],
        ["Enforce Settings as a Policy", "MSA/tutorials/ManagementFeaturesIntegration/EnforcingSettings.html#how-do-i-implement-policy-enforcement-for-my-product"],
        ["Data Channel", "$^ePO&MSA*content/data_channel.html"],
        ["Best Practices for Using the Data Channel", "ePO/articles/ePODataChannelBestPractices.html"],
        ["Overview of the Data Channel", "MSA/articles/Overview/DataChannel.html"],
        ["Client Tasks", "$^ePO&MSA*content/client_tasks.html"],
        ["Basic Client Task Integration", "ePO/articles/BasicClientTask.html"],
        ["MVC Based Task Settings UI Extensions", "ePO/articles/MVCActionBasedTaskUIPrdExtension.html"],
        ["Remote Task Scheduling", "MSA/tutorials/ManagementFeaturesIntegration/ScheduleTasksRemotely.html"],
        ["Install the Plugin DLL", "content/install_plugin_dll.html"],
        ["Client Task Permission Integration", "ePO/articles/ClientTaskPermission.html"]
    ];

    // Advanced Client Info that is diplayed in the Reference Tab
    var aci_ref_data = [
        ["Policies", "$ePO*content/product_policies.html"],
        ["Getting started with Policies", "ePO/articles/epoPolicyOverView.html"],
        ["How to add Point Product policy to ePO", "ePO/articles/ePO%20Update%20Doc/ePOPolicyIntegrationOverview.html"],
        ["ePO Compatible Policy Management", "ePO/articles/ePOCrossVersionPlatformPolicyMgmt.htm"],
        ["Rule-based Policy Assignment", "ePO/articles/RuleBasedPolicyAssignment.html"],
        ["Product Feature Policy Permissions", "ePO/articles/ProductFeaturePolicyPermIntegration.html"],
        ["MVC Action-based Policy UI Extension", "ePO/articles/MVCActionBasedPolicyUIExtension.html"],
        ["Policy UI Tips, Tricks and Guidelines", "ePO/articles/policyUITipandTricks.html"],
        ["ePO Policy Management Integration Details", "ePO/articles/ePOPolicyManagementIntegration.htm"],
        ["ePO Policy/Task Configuration Guide", "ePO/articles/ePOPolicyConfigFiles.htm"],
        ["ePO User Based Policy Management", "ePO/articles/ePOUserBasedPolicyOverview.htm"],
        ["ePO Policy and Client Tasks Comparison", "ePO/articles/ProductComparison.html"],
        ["Data Channel", "$^ePO&MSA*content/data_channel.html"],
        ["Best Practices for Using the Data Channel", "ePO/articles/ePODataChannelBestPractices.html"]
    ];

    if (window.topFilter.parseInt() === 3) {
        createAccordionTab(TAB_NAMES[2], aci_ref_data, accordion);
    } else {
        createAccordionTab(TAB_NAMES[2], aci_data, accordion);
    }

    // Basic Server in the Get Integrated tab
    var bs_data = [
        ["Overview", "$^ePO&MFS"],
        ["Migrating to the Cloud", "MFS/articles/Multitenant/MigratingToTheCloud.html"],
        ["Compatability Matrix", "MFS/articles/CompatabilityMatrix/CompatabilityMatrix.html"],
        ["ePO Key Concepts", "ePO/articles/keyConcepts/KeyConcepts.html"],
        ["MFS Key Concepts", "MFS/articles/KeyConcepts/KeyConcepts.html"],
        ["The Python Remote Client", "MFS/articles/PythonClient/en/getting_started.html"],
        ["Server Settings", "MFS/articles/Server/ServerSetting.html"],
        ["UI Navigation", "MFS/articles/Navigation/UI_Navigation.html"],
        ["Using the Database", "MFS/articles/Database/UsingTheDatabase.html"],
        ["Reporting", "$MFS*content/reporting.html"],
        ["The Query System UI", "MFS/articles/Queries/UsingTheQuerySystemUI.html"],
        ["Basic Query System Integration", "MFS/articles/Queries/BasicQuerySystemIntegration.html"],
        ["Using Query Commands", "MFS/articles/Queries/UsingQueryCommands.html"],
        ["Registered Server", "$MFS"],
        ["LDAP APIs", "MFS/articles/LDAP/LDAP.html"],
        ["Registered Servers", "MFS/articles/RegisteredServers/RegisteredServers.html"],
        ["Registered Types", "MFS/articles/RegisteredTypes/RegisteredTypes.html"],
        ["Registered Type Detail Pages", "MFS/articles/DetailPages/DetailPages.html"],
        ["Product Properties", "$*content/product_properties.html"],
        ["Implementing Common Product Properties", "ePO/articles/ePOCommonProductProperties.htm"],
        ["Implementing Custom Product Properties", "ePO/articles/ePOCustomProductProperties.htm"],
        ["Events", "$^MFS&ePO*content/events.html"],
        ["Common Event Categories", "ePO/articles/EPOCommonEventCategories.htm"],
        ["Common Event Format", "ePO/articles/EPOCommonEventFormat.htm"],
        ["Common Event Severity", "ePO/articles/EPOCommonEventSeverity.htm"],
        ["Common Event XML Format", "ePO/articles/ePOCommonEventXMLFormat.htm"],
        ["Event Dispatch Service", "MFS/articles/OrionEventDispatchService/OrionEventDispatchService.html"],
        ["Event Filters and Descriptions", "ePO/articles/EventFiltersAndDescriptions.html"],
        ["Event Processing", "ePO/articles/ePO%20Update%20Doc/ePOEvent.html"]
    ];

    // Basic Server Info that is diplayed in the Reference Tab
    var bs_ref_data = [
        ["Overview", "$^ePO&MFS"],
        ["Migrating to the Cloud", "MFS/articles/Multitenant/MigratingToTheCloud.html"],
        ["Compatability Matrix", "MFS/articles/CompatabilityMatrix/CompatabilityMatrix.html"],
        ["Introduction to ePO", "ePO/Overview/ePOIntroduction.htm"],
        ["MFS Overview", "MFS/articles/Overview/Overview_Orion.html"],
        ["MFS Model View Controller Development Overview", "MFS/articles/MVC/OrionMVC.html"],
        ["Extension Points for Integration", "ePO/Overview/EPOExtensionPointOverview.htm"],
        ["General UI Concepts", "MFS/api/ui/GeneralUIConcepts.html"],
        ["ePO Key Concepts", "ePO/articles/keyConcepts/KeyConcepts.html"],
        ["MFS Key Concepts", "MFS/articles/KeyConcepts/KeyConcepts.html"],
        ["Server Settings", "MFS/articles/Server/ServerSetting.html"],
        ["UI Best Practices", "ePO/articles/UIBestPractices.htm"],
        ["Using the Database", "MFS/articles/Database/UsingTheDatabase.html"],
        ["Building Blocks", "$^ePO&MFS"],
        ["MFS Extensions", "MFS/articles/Extensions/OrionExtensions.html"],
        ["Plugins", "MFS/articles/Plugins/OrionPlugins.html"],
        ["Canned Content", "MFS/articles/Extensions/CannedContent.html"],
        ["Building the UI", "MFS/articles/BuildingUI/Building%20UI%20with%20Orion.html"],
        ["Import / Export Framework", "MFS/articles/Transporter/Transporter.html"],
        ["LDAP APIs", "MFS/articles/LDAP/LDAP.html"],
        ["Localization", "MFS/articles/Localization/Localization.html"],
        ["Logging", "MFS/articles/Logging/Logging.html"],
        ["Ownership Framework", "MFS/articles/Ownership/OwnershipFramework.html"],
        ["Getting Started with the Python Remote Client", "MFS/articles/PythonClient/en/getting_started.html"],
        ["MFS S-Expressions", "MFS/articles/Sexp/SExpressions.html"],
        ["Using the MFS Test Extension", "MFS/articles/Extensions/UsingMFSTestExtension.html"],
        ["Orion Test Tools", "MFS/articles/Testing/OrionTestTools.html"],
        ["Unexpected Conditions", "MFS/articles/UnexpectedConditions/UnexpectedConditions.html"],
        ["ePO Public URLs", "ePO/articles/PublicURLs.html"],
        ["MFS Public URLs", "MFS/articles/PublicAPI/PublicURL.html"],
        ["Reporting", "$MFS"],
        ["The Query System UI", "MFS/articles/Queries/UsingTheQuerySystemUI.html"],
        ["Basic Query System Integration", "MFS/articles/Queries/BasicQuerySystemIntegration.html"],
        ["Using Query Commands", "MFS/articles/Queries/UsingQueryCommands.html"]
    ];

    if (window.topFilter.parseInt() === 3) {
        createAccordionTab(TAB_NAMES[3], bs_ref_data, accordion);
    } else {
        createAccordionTab(TAB_NAMES[3], bs_data, accordion);
    }

    // Advanced Server in Get Integrated
    var as_data = [
        ["Charts", "$MFS*MFS/articles/Charts/Charts.html"],
        ["Dashboards", "$MFS*MFS/articles/Dashboards/dashboards.html"],
        ["Disaster Recovery", "$MFS*MFS/articles/DisasterRecovery/DisasterRecovery.html"],
        ["ePOEvents", "$*ePO/articles/ePONotificationServices.html"],
        ["Import/Export Framework", "$MFS*MFS/articles/Transporter/Transporter.html"],
        ["MFS Clustering Support", "$MFS*MFS/articles/Clustering/Clustering.html"],
        ["Repository Management Integration", "$ePO*ePO/articles/ePORepositoryManagementIntegration.htm"],
        ["Software Manager (Internal Use Only)", "$ePO*ePO/articles/SoftwareManager.html"],
        ["Reporting", "$*content/reporting.html"],
        ["Advanced Reporting", "MFS/articles/Report/AdvancedReporting.html"],
        ["Advanced Query System Integration", "MFS/articles/Queries/AdvancedQuerySystemIntegration.html"],
        ["Remote Database Integration", "MFS/articles/Queries/RemoteDatabaseSupport.html"],
        ["Using Query Commands", "MFS/articles/Queries/UsingQueryCommands.html"],
        ["ePO Rollup Reporting", "ePO/articles/ePORollupReporting/index.html"],
        ["Policies", "$ePO*content/product_policies.html"],
        ["Getting Started with Policies", "ePO/articles/epoPolicyOverView.html"],
        ["Adding Point Products Policies to ePO", "ePO/articles/ePO%20Update%20Doc/ePOPolicyIntegrationOverview.html"],
        ["ePO Cross Version/Platform Policy Management", "ePO/articles/ePOCrossVersionPlatformPolicyMgmt.htm"],
        ["Rule-based Policy Assignment", "ePO/articles/RuleBasedPolicyAssignment.html"],
        ["Product Feature Policy Permissions", "ePO/articles/ProductFeaturePolicyPermIntegration.html"],
        ["Policy UI Tips, Tricks and Guidelines", "ePO/articles/policyUITipandTricks.html"],
        ["ePO Policy Management Integration", "ePO/articles/ePOPolicyManagementIntegration.htm"],
        ["ePO Policy/Task Configuration Guide", "ePO/articles/ePOPolicyConfigFiles.htm"],
        ["ePO Product Policy & Task Comparison", "ePO/articles/ProductComparison.html"],
        ["User Based Policies", "ePO/articles/ePOUserBasedPolicyOverview.htm"],
        ["Commands", "$^ePO&MFS"],
        ["Creating System Commands", "ePO/articles/CreatingSystemCommands.html"],
        ["Issue Management", "MFS/articles/IssueManagement/IssueManagement.html"],
        ["Automatic Responses", "$MFS"],
        ["Basic Automatic Response Integration", "MFS/articles/AutomaticResponse/BasicIntegration.html"],
        ["Advanced Automatic Response Integration", "MFS/articles/AutomaticResponse/AdvancedIntegration.html"],
        ["Using Automatic Response", "MFS/articles/AutomaticResponse/UsingTheResponseUI.html"],
        ["UI Actions", "$MFS"],
        ["UI Actions", "MFS/articles/UIAction/UIActions.html"],
        ["Enable and Disable UI Actions from Selection", "MFS/articles/RegisteredTypes/FilterableUIAction.html"],
        ["Server Tasks", "$MFS*content/server_tasks.html"],
        ["The MFS Server Task Scheduler", "MFS/articles/Scheduler/Scheduler.html"],
        ["Task Log Integration", "MFS/articles/TaskLog/TaskLogIntegration.html"],
        ["Virtual Agent (MePO)", "$*VirtualAgent/index.html"],
        ["Agent Wakeup Integration", "VirtualAgent/articles/agentWakeup.html"],
        ["Wipe/Lock Integration", "VirtualAgent/articles/comon_ui_action.html"],
        ["Mobile Properties Extension Point", "VirtualAgent/articles/mobileProperties.html"]
    ];

    // Adv Server Info that is diplayed in the Reference Tab
    var as_ref_data = [
        ["Charts", "$MFS*MFS/articles/Charts/Charts.html"],
        ["Dashboards", "$MFS*MFS/articles/Dashboards/dashboards.html"],
        ["Disaster Recovery", "$MFS*MFS/articles/DisasterRecovery/DisasterRecovery.html"],
        ["Import/Export Framework", "$MFS*MFS/articles/Transporter/Transporter.html"],
        ["MFS Clustering Support", "$MFS*MFS/articles/Clustering/Clustering.html"],
        ["Repository Management Integration", "$ePO*ePO/articles/ePORepositoryManagementIntegration.htm"],
        ["Software Manager (Internal Use Only)", "$ePO*ePO/articles/SoftwareManager.html"],
        ["Reporting", "$MFS*content/reporting.html"],
        ["Basic Query System Integration", "MFS/articles/Queries/BasicQuerySystemIntegration.html"],
        ["Advanced Query System Integration", "MFS/articles/Queries/AdvancedQuerySystemIntegration.html"],
        ["Advanced Reporting", "MFS/articles/Report/AdvancedReporting.html"],
        ["Remote Database Integration", "MFS/articles/Queries/RemoteDatabaseSupport.html"],
        ["Branding, Licensing and Login (Internal Use Only)", "MFS/articles/Branding/Branding.html"],
        ["Key Concepts", "$^MFS&ePO"],
        ["Extension Compatibility", "MFS/articles/ExtensionCompatibility/ExtensionCompatibility.html"],
        ["Validation, Dirty-Bits and Required Fields", "ePO/articles/EPOFormValidation.html"],
        ["Plugin and Extension Dependencies", "ePO/articles/ExtensionDependencies.htm"],
        ["Client Communication with the MFS Server", "MFS/articles/Server/Server.html"], //(Internal Use Only)
        ["MFS Extensions", "MFS/articles/Extensions/OrionExtensions.html"],
        ["MFS Plugins", "MFS/articles/Plugins/OrionPlugins.html"],
        ["Canned Content", "MFS/articles/Extensions/CannedContent.html"],
        ["Building the UI", "MFS/articles/BuildingUI/Building%20UI%20with%20Orion.html"],
        ["Localization", "MFS/articles/Localization/Localization.html"],
        ["Logging", "MFS/articles/Logging/Logging.html"],
        ["MFS S-Expressions", "MFS/articles/Sexp/SExpressions.html"],
        ["Using the MFS Test Extension", "MFS/articles/Extensions/UsingMFSTestExtension.html"],
        ["Orion Test Tools", "MFS/articles/Testing/OrionTestTools.html"],
        ["Unexpected Conditions", "MFS/articles/UnexpectedConditions/UnexpectedConditions.html"],
        ["ePO Guided Tour Framework", "ePO/articles/ePOGuidedTour.html"],
        ["Environment Considerations", "$MFS"],
        ["Multi-version Compatibility", "MFS/articles/Compatibility/Compatibility.html"],
        ["FIPS 140-2 Compliance", "MFS/articles/FIPS/FIPS140Compliance.html"],
        ["Using JNI", "MFS/articles/JNI/JNI.html"],
        ["Third Party Libraries", "MFS/articles/Server/ThirdPartyLibraries.html"],
        ["MFS Object/Relational Mapping Framework", "MFS/articles/ORM/OrionORM.html"],
        ["Automatic Responses", "$MFS*MFS/articles/AutomaticResponse/UsingTheResponseUI.html"],
        ["Basic Automatic Response Integration", "MFS/articles/AutomaticResponse/BasicIntegration.html"],
        ["Advanced Automatic Response Integration", "MFS/articles/AutomaticResponse/AdvancedIntegration.html"],
        ["Registered Server", "$MFS"],
        ["LDAP APIs", "MFS/articles/LDAP/LDAP.html"],
        ["Registered Servers", "MFS/articles/RegisteredServers/RegisteredServers.html"],
        ["Registered Types", "MFS/articles/RegisteredTypes/RegisteredTypes.html"],
        ["Registered Type Detail Pages", "MFS/articles/DetailPages/DetailPages.html"],
        ["Security", "$MFS"],
        ["Auditing", "MFS/articles/Auditing/AuditLog.html"],
        ["Certificate Based Authentication", "MFS/articles/Server/CertBasedAuth.html"],
        ["MFS External Authentication", "MFS/articles/Server/ExternalAuth.html"],
        ["Certificates and Keys in MFS", "MFS/articles/Server/CertificatesAndKeys.html"],
        ["Preventing CSRF Attacks", "MFS/articles/Security/CSRFAttackPrevention.html"],
        ["Users, Roles and Permissions", "MFS/articles/Users/RolesAndPermissions.html"],
        ["Secure Programming Techniques", "MFS/articles/Security/SecureExtensions.html"],
        ["UI Actions", "$MFS"],
        ["UI Actions", "MFS/articles/UIAction/UIActions.html"],
        ["Enable and Disable UI Actions from Selection", "MFS/articles/RegisteredTypes/FilterableUIAction.html"],
        ["Commands", "$ePO"],
        ["Creating System Commands", "ePO/articles/CreatingSystemCommands.html"],
        ["Scriptable Commands", "ePO/articles/ScriptableCommands.html"],
        ["Virtual Agent (MePO)", "$*VirtualAgent/index.html"],
        ["Virtual Agent (MePO) Technical Overview", "VirtualAgent/articles/supportability.html"],
        ["Client Connection Error Suggestions", "VirtualAgent/articles/connection_errors.html"]
    ];

    if (window.topFilter.parseInt() === 3) {
        createAccordionTab(TAB_NAMES[4], as_ref_data, accordion);
    } else {
        createAccordionTab(TAB_NAMES[4], as_data, accordion);
    }

    // SDK Samples
    var acc6data = [
        ["Tutorials", "content/tutorials.html"],
        ["Examples", "content/examples.html"],
        ["Unified Examples", "content/unified_examples.html"]
    ];
    createAccordionTab(TAB_NAMES[5], acc6data, accordion);

    // McAfee Internal
    var acc7data = [
        ["Interoperability Services", "content/interop_services.html"],
        ["ePO Data Sheet", "@http://www.mileaccess.com/us/resources/data-sheets/ds-epolicy-orchestrator.pdf"],
        ["Product and Customer Service", "@http://www.mileaccess.com/us/support.aspx"],
        ["Technical Support ServicePortal", "@https://mysupport.mileaccess.com/Eservice/Default.aspx"],
        ["Product Downloads, Trials, and Tools", "@http://www.mileaccess.com/us/downloads/downloads.aspx"]
    ];
    createAccordionTab(TAB_NAMES[6], acc7data, accordion);

    // SIA
    var acc8data = [
        ["SIA Engineering Best Practices", "SIA/GettingStarted/articles/SIA%20Engineering%20Best%20Practices.htm"],
        ["SIA Engineering FAQs", "SIA/GettingStarted/articles/SIA%20Engineering%20FAQs.htm"],
        ["Master Check List", "SIA/StarterKit/Master%20Checklist.xls"],
        ["SIA Testing Process", "SIA/GettingStarted/articles/SIA_Testing_Process.htm"],
        ["SIA Support Process", "SIA/GettingStarted/articles/SIA_Support_Process.htm"],
        ["Starter Kit", "$"],
        ["Use Case Template", "&SIA/StarterKit/Use%20Cases%20template.doc"],
        ["Functional Spec Template", "&SIA/StarterKit/Functional%20Specification%20Document%20Template.doc"],
        ["ePO Integration Guide Template", "&SIA/StarterKit/ePO%20Integration%20Guide%20Template.doc"],
        ["Test Plan", "&SIA/StarterKit/SIA%20External%20Test%20Plan.doc"],
        ["Test Case Sheet", "&SIA/StarterKit/ePO%205.0-%20SIA%20Certification%20Test%20Cases.xls"],
        ["Partner Delivery Package (PDP)", "&SIA/StarterKit/Partner%20Delivery%20package.zip"]
    ];
    createAccordionTab(TAB_NAMES[7], acc8data, accordion);

    var apidata = [
        ["ePO", "$ePO"],
        ["Java API", "ePO/javadoc/index.html"],
        ["JavaScript API", "ePO/jsdoc/index.html"],
        ["MFS", "$MFS"],
        ["Java API", "MFS/javadoc/index.html"],
        ["JavaScript API", "MFS/jsdoc/index.html"],
        ["Tags Library", "content/api.html"],
        ["CSS Selectors", "MFS/api/ui/CSS.html"],
        ["majic.js Library", "MFS/api/MajicJSLibrary.html"],
        ["Yahoo! UI Library", "MFS/api/yui.html"],
        ["MA", "$MSA"],
        ["McAfee Agent C++ (doxygen)", "MSA/doxygen/html/index.html"],
        ["McAfee Script Engine Commands", "MSA/apis/Script%20Engine/script-command-reference.html"],
        ["CmaWrap API (Windows Only)", "MSA/apis/CmaWrap/CmaWrap_Reference.html"],
        ["Event Generation API", "MSA/apis/EventGeneration/EventGeneration_Reference.htm"],
        ["Virtual Agent (MePO)", "$*VirtualAgent/index.html"],
        ["Java API", "VirtualAgent/javadoc/index.html"],
        ["Public API", "VirtualAgent/articles/mobile.html"],
        ["URL Plugin API", "VirtualAgent/articles/URL_Plugin_Design.htm"],
        ["URL Plugin API - Device Property Descriptions", "VirtualAgent/articles/urlpluginprops.html"]
    ];
    createAccordionTab(TAB_NAMES[8], apidata, accordion);

    var nav_index = parseInt(window.navIndex.parseInt(), 10);

    // Fix navigation indexes to account for hidden tabs 
    var top_filter = window.topFilter.parseInt();
    if (top_filter === 1) { // Get Started
        if (nav_index > 0 && nav_index < 5) {
            nav_index -= 5;
        } else if (nav_index === 6) {
            nav_index = 1;
        } else if (nav_index === 7) {
            nav_index = 2;
        }
    } else {
        if (top_filter === 2) { // Get Integrated
            if (nav_index === 0) {
                nav_index = -1;
            } else if (nav_index === 1 || nav_index === 3 || nav_index === 4) {
                nav_index -= 1;
            }
            else {
                if (nav_index === 2) {
                    nav_index = 1;
                } else {
                    if (nav_index > 4) {
                        nav_index -= 3;
                    }
                }
            }
        } else {
            if (top_filter === 3) { // Reference
                if (nav_index === 8) {
                    nav_index = 4;
                } else {
                    nav_index -= 1;
                }
                /*if (nav_index < 5) {
                 nav_index = -1;
                 } else if (nav_index > 7) {
                 nav_index -= 8;
                 }*/
            }
        }
        if (top_filter === 4) { // Samples
            nav_index = 0;
        }
    }

    if (nav_index >= 0) {
        accordion.accordion({clearStyle: true, autoHeight: true, collapsible: true, heightStyle: "content", active: nav_index, event: "click", animate: false});
    } else {
        accordion.accordion({clearStyle: true, autoHeight: true, collapsible: true, heightStyle: "content", active: false, event: "click", animate: false});
    }
    return accordion;
}

function createLink(label, href) {
    var is_bold_heading = false;
    var is_external = false;
    var a = $(document.createElement("a"));
    a.css("font-size", "12px");
    a.css("padding", "0px");
    a.css("padding-left", "20px");
    a.css("text-decoration", "none");
    a.css("color", "black");
    a.css("font-family", "arial");

    if (href.length < 1) {
        href = "#";
    } else if (href[0] === "@") { // If the first character is @, it is an external link
        href = href.substring(1);
        is_external = true;
    } else if (href[0] === "%") {
        var st = $(document.createElement("div"));
        st.append(document.createTextNode(label));
        st.css("margin-top", "2px");
        st.css("margin-bottom", "4px");
        st.css("font-size", "12px");
        st.css("text-decoration", "underline");
        st.css("font-wieght", "bold");
        st.css("margin-left", "22px");
        if (isIE()) {
            st.css("padding", "4px");
        } else {
            st.css("padding", "0px");
        }
        return st;
    } else if (href[0] === "~") {
        return hr();
    } else if (href[0] === "$") {
        is_bold_heading = true;
        a.css("font-weight", "bold");
        a.css("position", "relative");
        a.css("left", "-10px");
        var sr = href.search('\\*');
        if (sr >= 0) {
            href = SDKDOCROOT + href.substr(sr + 1);
        } else {
            href = "#";
        }
    }
    else {
        if (href[0] === "&") {
            href = href.substring(1);
            is_external = true;
        }
        href = SDKDOCROOT + href;
    }

    a.attr("href", href);
    a.attr("title", label);

    if (isIE()) {
        a.css("width", "90%");
        a.hover(
                function() {
                    $(this).css("background-color", "#DEDEDE");
                    $(this).css("border", "1px solid grey");
                    //$(this).css("border-radius", "5px");
                    $(this).width(SIDEBAR_WIDTH - 12);
                    $(this).css("height", "4px");
                    //$(this).css("width", "220px");
                    //$(this).css("padding", "5px");
                },
                function() {
                    if (href == window.location.href) {
                        $(this).css("background-color", "#DDD");
                    } else {
                        $(this).css("background-color", "white");
                    }
                    $(this).css("border", "none");
                    $(this).css("height", "6px");
                });
    }
    a.append(document.createTextNode(label));
    a.click(function() {
        if (ls.scrollTop() > 0) {
            window.navScrollPos.set(ls.scrollTop());
        } else {
            window.navScrollPos.set(0);
        }
    });

    if (is_external) {
        a.attr("target", "_blank");
    }
    a.css("margin-left", "22px");

    // Identify links that contain anchors, and extract the anchors
    var anchorInURL = href.substring(window.location.href.lastIndexOf("#"));
    var urlWithAnchorsStripped = href.substring(0, window.location.href.lastIndexOf("#"));
    if (anchorInURL.length > 0 && urlWithAnchorsStripped.length > 0) {
        // The page has an anchor in the URL
        //console.log("problem links: " +urlWithAnchorsStripped + " and " + anchorInURL + " len = " +anchorInURL.length);
        var anchorFromWindowHref = window.location.href.substring(window.location.href.lastIndexOf("#"));
        if (anchorFromWindowHref == anchorInURL) {
            // Page is the same, and anchor from the window.location.href is the same as the current page
            // All remaining links of current document with different anchors will get a page refresh even scheduled,
            a.css("background-color", "#DDD");
        } else {
            a.css("background-color", "white");
            a.click(function() {
                // This is necessary for pages with same document but different anchor to apply highlight update.
                setTimeout("location.reload(true);", 30);
            });
        }
    } else if (href == window.location.href) { // Perform highlighting on remaining link types
        a.css("background-color", "#DDD");
    } else {
        a.css("background-color", "white");
    }
    return a;
}


function createMenu() {
    var ul = $(document.createElement("ul"));
    ul.css("border", "none");
    if (isIE()) {
        ul.css("margin-left", "-34px");
    } else {
        ul.css("margin-left", "-29px");
    }

    ul.css("margin-right", "-30px");
    ul.css("margin-top", "-12px");
    ul.css("margin-bottom", "-11px");
    ul.css("padding", "0px");
    return ul;
}

function createMenuItem(label, href) {
    var li = $(document.createElement("li"));
    if (isIE()) {
        li.css("line-height", "15px");
    } else {
        li.css("line-height", "15px");
    }
    var link = createLink(label, href);
    if (link) {
        li.append(link);
        return li;
    } else {
        return 0;
    }
}

// Toggle button for showing/hiding the example code. (not part of navigation)
function setToggleSectionButton(code_section_id, toggle_button_id, button_word) {
    var is_hidden;
    if (is_hidden !== 'undefined') {
        // example code is hidden by default
        is_hidden = true;
        $("#" + code_section_id).hide(1);
        $("#" + toggle_button_id).html("Show " + button_word);
    }

    $("#" + toggle_button_id).click(function(e) {
        if (is_hidden) {
            is_hidden = false;
            $("#" + toggle_button_id).html("Hide " + button_word);
            $("#" + code_section_id).show(30);
        } else {
            is_hidden = true;
            $("#" + toggle_button_id).html("Show " + button_word);
            $("#" + code_section_id).hide(30);
        }
    });
}
