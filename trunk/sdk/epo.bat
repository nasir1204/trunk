@ECHO OFF
SETLOCAL
set MA_KEY_NAME="HKEY_LOCAL_MACHINE\SOFTWARE\McAfee\Agent"
set MA_VALUE_NAME=InstallPath

FOR /F "usebackq skip=2 tokens=1,2*" %%A IN (
 `REG QUERY %MA_KEY_NAME% /v %MA_VALUE_NAME% 2^>nul`) DO (
    set Home="%%C"
)

IF DEFINED home SET home=%home:"=%
if defined Home echo "McAfee Agent 5.0 is already installed"
if NOT defined Home "\\MyServer\Agent$\Update\FramePkg.exe /install=agent"
exit /b 0
