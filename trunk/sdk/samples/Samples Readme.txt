The following projects are samples on different usage of the Message Bus and MA Logger 

For Message bus samples

The client is a whitelisting service which sends a set of files to be scanned to a scanning service

	- The sample msgbus_client_async_send demonstrates the use of asynchronous callbacks
	- The sample msgbus_client_sync_send demonstrates the use of synchronous callbacks
	- The sample msgbus_client_send_and_forget demonstrates the use of simply sending a request but does not expect a response.This will just send a list of whitelisted files received from last scan to the scanning service.

The service is a scanning service which scans a set of files received from a whitelisting files and sends back the result.

The publisher is a sample which says how to publish messages to subscribers listening on a topic

The subscriber subscribes to the policy publisher and receives some data load.


All the above samples take command line params to work. use /? with the built exe to see how to use. start ma_broker.exe install and then ma_broker.exe start for outproc communication
