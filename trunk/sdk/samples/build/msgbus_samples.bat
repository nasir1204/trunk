@echo off
cd ..\
.\build\pwd.exe > .\build\cur_path.txt
cd .\build\
copy /Y root_cmd.txt + cur_path.txt set_samples_base.bat > nul
call set_samples_base.bat
if "%SAMPLES_BASE%"=="" goto error

set VC_VER=vc100
set MA_BASE=%SAMPLES_BASE%\..\..
set MA_TOOLS=%SAMPLES_BASE%\..\..\..
set MA_SDK_INCLUDE=%MA_BASE%\include
set MA_SDK_LIB=%MA_BASE%\build\msvc
set PATH=%PATH%


if "%SAMPLES_BASE%"=="" goto error1
if "%MA_BASE%"=="" goto error2
if "%MA_TOOLS%"=="" goto error3
if "%MA_SDK_INCLUDE%"=="" goto error4
if "%MA_SDK_LIB%"=="" goto error5

echo SAMPLES_BASE = %SAMPLES_BASE%
echo MA_BASE = %MA_BASE%
echo MA_TOOLS = %MA_TOOLS%
echo MA_SDK_INCLUDE = %MA_SDK_INCLUDE%
echo MA_SDK_LIB = %MA_SDK_LIB%


start msgbus_samples.sln

del cur_path.txt
del set_samples_base.bat

echo *************************************************************************
echo Verify the above paths are set properly for compiling the sample product
echo *************************************************************************

pause

goto end

:error
echo *********************************
echo Error: Did not find base path set
echo .
goto end

:error1
echo Error: SAMPLES_BASE path not defined
goto end

:error2
echo Error: MA_BASE path not defined
goto end

:error3
echo Error: MA_TOOLS path not defined
goto end

:error4
echo Error: MA_SDK_INCLUDE path not defined
goto end

:error5
echo Error: MA_SDK_LIB path not defined


:end
