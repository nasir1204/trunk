/******************************************************************************
1) A script to start the broker and multiple endpoints at one go for execution.

2) Command to execute -
   msgbus_benchmarking_test_main <File>

3) Sample Layout of the file (Do not provide new line at the end of the file) -

   PUBLISHER 1111 OUTPROC 1 1000 3 Topic_1 Topic_2 Topic_3
   SUBSCRIBER 2222 OUTPROC 1 Topic_3
   SUBSCRIBER 3333 OUTPROC 1 Topic_1
   SUBSCRIBER 4444 OUTPROC 1 Topic_2
   SERVER 12 OUTPROC server3
   SERVER 13 OUTPROC server2
   SERVER 14 OUTPROC server4
   SERVER 15 OUTPROC server1
   SYNC_CLIENT 106 OUTPROC 1 300 server1 NULL
   SYNC_CLIENT 107 OUTPROC 1 400 server1 NULL
   SYNC_CLIENT 108 OUTPROC 1 500 server4 NULL
   ASYNC_CLIENT 101 OUTPROC 1 100 server2 NULL
   ASYNC_CLIENT 102 OUTPROC 1 200 server3 NULL
   ASYNC_CLIENT 103 OUTPROC 1 300 server1 NULL
   ASYNC_CLIENT 104 OUTPROC 1 400 server2 NULL
   ASYNC_CLIENT 105 OUTPROC 1 500 server4 NULL
   SEND_FORGET_CLIENT 109 OUTPROC 1 400 server2 NULL
   SEND_FORGET_CLIENT 110 OUTPROC 1 500 server3 NULL
******************************************************************************/
#include <stdio.h>

#ifndef WIN32
    #include <signal.h>
#else
    #include <Windows.h>
#endif

/******************************************************************************
Interrupt Handling. The control reaches this function when the CTRL+C/CTRL+Break
is hit, it calls the SIGINT to helps in graceful termination of the process after
logging the overall statistics.

Generates the interrupt for all the child processes which are spawned from the
same console.
******************************************************************************/
#ifndef WIN32
    void interrupt_handler(int s)
    {
        kill(0, SIGINT);
    }
#else
    BOOL interrupt_handler(DWORD control_type)
    {
        if(control_type == CTRL_BREAK_EVENT)
        {
            GenerateConsoleCtrlEvent(1, 0);
            return TRUE;
        }
        return FALSE;
    }
#endif

// Arguments - <No. of Publishers> <No. of subscribers> <No. of Topics>
int main(int argc, char* argv[])
{
    char *file_name = NULL;
    FILE *file_handle = NULL;
    char line_buffer[200];
    int line_number = 0;

    /**************************************************************************
    Defining signal handler.
    **************************************************************************/
    #ifndef WIN32
        signal(SIGINT, interrupt_handler);
    #else
        SetConsoleCtrlHandler((PHANDLER_ROUTINE)interrupt_handler, TRUE);
    #endif

    // Reading file name as the first argument passed from the command line
    file_name = argv[1];
    file_handle = fopen(file_name, "r");
    if(!file_handle)
    {
        printf("Couldn't open %s file for endpoint configuration\n", file_name);
        return -1;
    }
    else
    {
        printf("Opened %s file for endpoint configuration\n", file_name);
    }

    printf("Initiating Broker\n");
    system("start \"BROKER\" /B ma_broker.exe");

    Sleep(2000);

    printf("Initiating message bus communication\n");

    while(fgets(line_buffer, sizeof(line_buffer), file_handle))
    {
        char mystring[256] = "";
        sprintf(mystring, "start /B message_bus_sample.exe %s", line_buffer);
        system(mystring);
        Sleep(500); // Some delay before each new process startup
        line_number++;
    }
    printf(" %d client/server/publisher/subscriber processes started....\n", line_number);

    // Do not allow process to complete, till interrupted.
    while(1);
}

