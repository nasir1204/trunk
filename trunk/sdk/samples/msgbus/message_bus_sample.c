/******************************************************************************
1) A generic implementation to test the message bus functionality. Please refer
   to documentation throughout this file to understand the functionality.

   For execution commands, see the 'help_string' below or execute following command -
   message_bus_sample -help

2) Symbols to be defined -
   ENABLE_MSGBUS_LOGGING  - Enables Message Bus Logging
   LOG_STATS_IN_FILE      - Enables statistics logging in file, otherwise,
                            the stats are only displayed on console.
   DEBUG                  - Displays extra information on console which is
                            useful while debugging
3)  TO DO :
    a) Updates for Out of Box communcation.
    b) Logic for better timing resolution. Use gettimeofday().
    c) How to handle and measure time taken for out-of-box.
    d) Logic to get the summary statistics. May be done in this file or over
       the output result file.
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include "ma/ma_msgbus.h"
#include "ma/ma_message.h"
#include "ma/logger/ma_file_logger.h"
#include "ma/logger/ma_console_logger.h"
#include <time.h>

#define LOG_STATS_IN_FIL

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "MESSAGEBUS_SAMPLES"

#ifndef WIN32
    #include <signal.h>

    void Sleep(unsigned long millisecs)
    {
        unsigned long secs = millisecs/1000;
        sleep(secs);
    }
#else
    #include <Windows.h>
#endif

#define PRODUCT_ID  "MSGBUS_SAMPLES"

// Help Message
char *help_string = "\n" \
"argv[0]            argv[1]            argv[2]              argv[3]                               argv[4]                          argv[5]                   argv[6] \n" \
"message_bus_sample PUBLISHER          <Publisher Number>  <Reachability (INPROC/OUTPROC/OUTBOX)> <Frequency (in Seconds)>         <Message Size (in Bytes)> <No. Of Topics to be Published> <Topics List....> \n" \
"message_bus_sample SUBSCRIBER         <Subscriber Number> <Reachability (INPROC/OUTPROC/OUTBOX)> <No. Of Topics to be Subscribed> <Topics List....> \n" \
"message_bus_sample ASYNC_CLIENT       <Client Number>     <Reachability (INPROC/OUTPROC/OUTBOX)> <Frequency (in Seconds)>         <Message Size (in Bytes)> <Server Name> <Host Name> \n" \
"message_bus_sample SYNC_CLIENT        <Client Number>     <Reachability (INPROC/OUTPROC/OUTBOX)> <Frequency (in Seconds)>         <Message Size (in Bytes)> <Server Name> <Host Name> \n" \
"message_bus_sample SEND_FORGET_CLIENT <Client Number>     <Reachability (INPROC/OUTPROC/OUTBOX)> <Frequency (in Seconds)>         <Message Size (in Bytes)> <Server Name> <Host Name> \n" \
"message_bus_sample SERVER             <Server Number>     <Reachability (INPROC/OUTPROC/OUTBOX)> <Server Name> \n";

/******************************************************************************
Defines the enumeration consisting of various endpoint types. The correct
endpoint will be chosen based on the command line arguments. Functionality of
this sample is driven based on this endpoint type.
******************************************************************************/
enum {
         SUBSCRIBER = 0,
         PUBLISHER = 1,
         SYNC_CLIENT = 2,
         ASYNC_CLIENT = 3,
         SEND_FORGET_CLIENT = 4,
         SERVER = 5
     } endpoint_type;

/******************************************************************************
File Logger pointer. File Logger is being used for logging by message bus. The
message bus logging will be enabled if ENABLE_MSGBUS_LOGGING is defined.
******************************************************************************/
#ifdef ENABLE_MSGBUS_LOGGING
    ma_file_logger_t *file_logger = NULL;
#endif

/******************************************************************************
Console Logger pointer. Console logger is being used for logging within this
script. Please Note that this sample code exits only in case of ctrl+C,
otherwise, on receiving error, it logs the error message on console and keeps
on executing further.
******************************************************************************/
ma_console_logger_t *console_logger = NULL;

/******************************************************************************
Message bus Subscriber object pointer
******************************************************************************/
ma_msgbus_subscriber_t *p_subscriber = NULL;

/******************************************************************************
Message Bus Endpoint (Client) object pointer
******************************************************************************/
ma_msgbus_endpoint_t *p_endpoint = NULL;

/******************************************************************************
 Message bus Server object pointer
******************************************************************************/
ma_msgbus_server_t *p_server = NULL;

/******************************************************************************
1) Define file handle and file name for logging statistics in file. Statistics
   will only be displayed on console if LOG_STATS_IN_FILE symbol is not defined.
2) Define LOG_STATS_IN_FILE symbol to enable file logging.
3) Modify the file name here, if you wish to use a different file name.
******************************************************************************/
#ifdef LOG_STATS_IN_FILE
    FILE * benchmarking_info_file_handle = NULL;
    char * benchmarking_info_file_name = "benchmarking_info.txt";
#endif

/******************************************************************************
total_requests_sent_by_client      - Total number of requests (sync/async/send
                                     and forget) sent by client.
total_requests_sent_by_client      - Total number of messages published by Publisher
total_requests_handled_by_server   - Total number of requests received and processed
                                     by server. Note that if the same server is
                                     handling requests from multiple clients, this
                                     number may be considerably high.
total_requests_handled_by_server   - Total number of messages received by Subscriber.
total_responses_received_by_client - Number of responses received by client for
                                     the requests sent. Note - No responses are
                                     expected for 'send and forget' type client
                                     and publisher.
******************************************************************************/
unsigned int total_requests_sent_by_client = 0;
unsigned int total_requests_handled_by_server = 0;
unsigned int total_responses_received_by_client = 0;

/******************************************************************************
Interrupt Handling. The control reaches this function when the CTRL+C/CTRL+Break
is hit, it helps in graceful termination of the process after logging the overall
statistics.

Sets the 'ctrl_c_hit' global variable to 1 indicating that the infinite loop
which is created for sending receiving messages can be exited now.
******************************************************************************/
int ctrl_c_hit = 0;

#ifndef WIN32
    void interrupt_handler(int s)
    {
        ctrl_c_hit = 1;
    }
#else
    BOOL interrupt_handler(DWORD control_type)
    {
        if(control_type == CTRL_BREAK_EVENT)
        {
            ctrl_c_hit = 1;
            return TRUE;
        }
        return FALSE;
    }
#endif

/******************************************************************************
Utility function for generating random numbers.

Returns a random value between 0 and passed input value (excluding the input
value) on every function call.
******************************************************************************/
int generate_random_number(int max_value_exclusive)
{
    static int count = 0;

    if(count == 0)
    {
        srand(time(NULL));
        count++;
    }
    return(rand() % max_value_exclusive);
}

/******************************************************************************
Utility function for generating random messages. Fills the passed string of given
length with any one character from the below character set.

NOTE : The user can replace this function with its own utility function.
******************************************************************************/
const char charset[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*()+_:<>?";

void generate_message(char *str, unsigned int length)
{
    unsigned int counter = 0;

    char mychar = 'a';

    mychar = charset[generate_random_number(strlen(charset))];

    for(counter = 0; counter < length; counter++)
    {
        str[counter] = mychar;
    }
}

/******************************************************************************
Utility function for generating response to the received message. Converts the
upper case letters in given string to lower case letters and vice-versa. Numbers
and alphanumeric characters remain unchanged.

NOTE : This function simulates as if the server is doing some processing before
sending the response message. The user can replace this function with its own
utility function.
******************************************************************************/
void generate_response(char *str)
{
    int count = 0;

    while(str[count] != '\0')
    {
        int int_value = (int)str[count];

        if((int_value >= 65) && (int_value <= 90))
        {
            str[count] = (char)(int_value + 32);
        }

        if((int_value >= 97) && (int_value <= 122))
        {
            str[count] = (char)(int_value - 32);
        }
        count++;
    }
}

/******************************************************************************
Publisher function which publishes a single message of given size and topic on
the message bus.
******************************************************************************/
void publish(ma_msgbus_t *msgbus, unsigned int publisher_number, ma_msgbus_consumer_reachability_t reach, unsigned int message_size_in_bytes, char *topic)
{
    // Uniquely incrementing message number within this process
    static unsigned int message_number = 1;

    // Variable to hold current time
    time_t current_time;

    // Pointer pointing to the 'message_size_in_bytes' message.
    char *my_string = NULL;

    // Pointer pointing to the combined message with header information (publisher
    // number and message number appended).
    char *my_combined_string = NULL;

    // Variant Pointer for holding the variant created from message string.
    ma_variant_t *variant_ptr = NULL;

    // Payload created from Variant
    ma_message_t* publisher_payload = NULL;

    // Proceed with allocating memory for storing the message (1 extra byte for NULL).
    my_string = (char *)calloc(message_size_in_bytes + 1, 1);
    my_combined_string = (char *)calloc(message_size_in_bytes + 20, 1);

    /* Fill the string with random message */
    generate_message(my_string, message_size_in_bytes);

    #ifdef DEBUG
        printf("Publisher No. - %d \t Message No. - %d \t Topic - %s \t Message Size - %d \t Message - %s\n", publisher_number, message_number, topic, message_size_in_bytes, my_string);
    #endif

    // Prepending Publisher Number and Message Number in front of the Message
    sprintf(my_combined_string, "%d-%d-%s", publisher_number, message_number, my_string);

    // Creating fresh message
    if(ma_message_create(&publisher_payload) != MA_OK)
    {
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Message Number %d could not be created for Publisher Number %d", message_number, publisher_number);
    }

    // Creating variant from string
    ma_variant_create_from_string(my_combined_string, strlen(my_combined_string), &variant_ptr);

    // Setting string variant as message payload
    ma_message_set_payload(publisher_payload, variant_ptr);

    // Publishing the Message on message Bus
    if(ma_msgbus_publish(msgbus, topic, reach, publisher_payload) != MA_OK)
    {
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Publisher Number %d Message number %d - Message Publishing failed", publisher_number, message_number);
    }

    // Getting the current time
    time(&current_time);

    // Log timestamps in file if the following macro is defined
    #ifdef LOG_STATS_IN_FILE
        benchmarking_info_file_handle = fopen(benchmarking_info_file_name, "a+");
        fprintf(benchmarking_info_file_handle, "Publisher No. - %d \t Msg. No. - %d \t Msg. Topic - %s \t Publish Time - %s\n", publisher_number, message_number, topic, ctime(&current_time));
        fclose(benchmarking_info_file_handle);
    #endif
    printf("Publisher No. - %d \t Msg. No. - %d \t Msg. Topic - %s \t Publish Time - %s\n", publisher_number, message_number, topic, ctime(&current_time));

    // Increment the message number (message number is unique within the process)
    message_number++;

    // Increment the count of the messages published
    total_requests_sent_by_client++;

    // Cleanup code
    ma_message_release(publisher_payload);
    ma_variant_release(variant_ptr);
    free(my_string);
    free(my_combined_string);
}


/******************************************************************************
Subscriber callback function. It will be executed when the subscriber recieves
a message corresponding to the topic it had subscribed for.
******************************************************************************/
ma_error_t subscriber_callback_func(const char *topic, ma_message_t *subscriber_payload, void *cb_data)
{
    // Variant to hold the variant extracted from message
    ma_variant_t *variant_ptr = NULL;

    // Buffer to hold string extracted from variant
    ma_buffer_t *buffer = NULL;

    // String Message retrieved from the buffer
    char *my_combined_string = NULL;
    size_t size;

    // Fields to be seperated from the message received by the Subscriber
    char *seperator = "-";
    unsigned int publisher_number = 0;
    unsigned int message_number = 0;
    char *my_string = NULL;
    char *last = NULL;

    // Variable to hold current time
    time_t current_time;

    // Getting current time. Obtain the time as soon as possible for better accurancy.
    // Here, the time is obtained as the first executable statement in the call-back function.
    time(&current_time);

    // Handling the message payload
    if(subscriber_payload != NULL)
    {
        ma_message_get_payload(subscriber_payload, &variant_ptr);
        ma_variant_get_string_buffer(variant_ptr, &buffer);
        ma_buffer_get_string(buffer, &my_combined_string, &size);

        /* Retrieving fields out of the string */
        publisher_number = atoi(strtok(my_combined_string, seperator));
        message_number = atoi(strtok(NULL, seperator));
        my_string = strtok(NULL, seperator);

        #ifdef DEBUG
            printf("Message No. %d received from Publisher No. %d (Message - %s)\n", message_number, publisher_number, my_string);
        #endif

        // Log timestamps in file if the following macro is defined
        #ifdef LOG_STATS_IN_FILE
            benchmarking_info_file_handle = fopen(benchmarking_info_file_name, "a+");
            fprintf(benchmarking_info_file_handle, "Subscriber No. - %d received message (%d bytes) from \t Publisher No. - %d \t Msg. No. - %d \t Msg. Topic - %s \t Recieve Time - %s\n", *(int*)cb_data, size, publisher_number, message_number, topic, ctime(&current_time));
            fclose(benchmarking_info_file_handle);
        #endif
        printf("Subscriber No. - %d received message (%d bytes) from \t Publisher No. - %d \t Msg. No. - %d \t Msg. Topic - %s \t Recieve Time - %s\n", *(int*)cb_data, size, publisher_number, message_number, topic, ctime(&current_time));

        // Incrementing count of number of messages received by subscriber
        total_requests_handled_by_server++;

        // Cleanup code
        ma_variant_release(variant_ptr);
    }
    else
    {
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "No payload in the message received");
        return MA_ERROR_UNEXPECTED;
    }

    return MA_OK;
}

/******************************************************************************
Client call back function. This function gets executed -
1) as a call back function, whenever an asynchronous client receives response
   from server.
2) called by send_request_message() function in this sample code to process the
   response packet received by synchronous client.
******************************************************************************/
ma_error_t process_server_response(ma_error_t status, ma_message_t *response_payload, void *cb_data, ma_msgbus_request_t *request)
{
    // Variant to hold the variant extracted from message
    ma_variant_t *variant_ptr = NULL;

    // Buffer to hold string extracted from variant
    ma_buffer_t *buffer = NULL;

    // String Message retrieved from the buffer
    char *my_combined_string = NULL;
    size_t size;

    // Fields to be seperated from the response message received from server
    char *seperator = "-";
    unsigned int client_number = 0;
    unsigned int request_number = 0;
    char *my_string = NULL;
    char *last = NULL;

    // Variable to hold current time
    time_t current_time;

    // Getting current time. Obtain the time as soon as possible for better accurancy.
    time(&current_time);

    // Handling the message payload
    if(response_payload != NULL)
    {
        ma_message_get_payload(response_payload, &variant_ptr);
        ma_variant_get_string_buffer(variant_ptr, &buffer);
        ma_buffer_get_string(buffer, &my_combined_string, &size);

        /* Retrieving fields out of the string */
        client_number = atoi(strtok(my_combined_string, seperator));
        request_number = atoi(strtok(NULL, seperator));
        my_string = strtok(NULL, seperator);

        #ifdef DEBUG
            printf("Response received for Request No. %d from Client No. %d (Reponse Message - %s)\n", request_number, client_number, my_string);
        #endif

        // Log timestamps in file if the following macro is defined
        #ifdef LOG_STATS_IN_FILE
            benchmarking_info_file_handle = fopen(benchmarking_info_file_name, "a+");
            fprintf(benchmarking_info_file_handle, "Client No. - %d received response (%d bytes) for \t Request No. - %d \t Recieve Time - %s\n", client_number, size, request_number, ctime(&current_time));
            fclose(benchmarking_info_file_handle);
        #endif
        printf("Client No. - %d received response (%d bytes) for \t Request No. - %d \t Recieve Time - %s\n", client_number, size, request_number, ctime(&current_time));

        // Incrementing count of number of responses recieved by client
        total_responses_received_by_client++;

        // Cleanup code
        ma_variant_release(variant_ptr);
    }
    else
    {
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "No payload in the message received");
        return MA_ERROR_UNEXPECTED;
    }

    return MA_OK;
}


/******************************************************************************
Client function for sending the Request to server. This function sends a single
request message (sync/async or send &forget type) of given size on the message
bus.
******************************************************************************/
void send_request_message(unsigned int client_number, unsigned int message_size_in_bytes)
{
    // Uniquely incrementing request number within this process
    static unsigned int request_number = 1;

    // Variable to hold current time
    time_t current_time;

    // Pointer pointing to the 'message_size_in_bytes' message.
    char *my_string = NULL;

    // Pointer pointing to the combined message with header information (publisher
    // number and message number appended).
    char *my_combined_string = NULL;

    // Variant Pointer for holding the variant created from message string.
    ma_variant_t *variant_ptr = NULL;

    // Payload created from Variant
    ma_message_t* request_payload = NULL;

    // Will hold the response message received from server for a synchronous request
    ma_message_t* response_payload = NULL;

    // Request Packet
    ma_msgbus_request_t *request_packet = NULL;

    // Proceed with allocating memory for storing the message (1 extra byte for NULL).
    my_string = (char *)calloc(message_size_in_bytes + 1, 1);
    my_combined_string = (char *)calloc(message_size_in_bytes + 20, 1);

    generate_message(my_string, message_size_in_bytes);

    #ifdef DEBUG
        printf("Client No. - %d \t Request No. - %d \t Request Message Size - %d \t Request Message - %s\n", client_number, request_number, message_size_in_bytes, my_string);
    #endif

    // Prepending Client Number and Request Number in front of the Message
    sprintf(my_combined_string, "%d-%d-%s", client_number, request_number, my_string);

    // Creating fresh message
    if(ma_message_create(&request_payload) != MA_OK)
    {
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Request Number %d could not be created for Client Number %d", request_number, client_number);
    }

    // Creating variant from string
    ma_variant_create_from_string(my_combined_string, strlen(my_combined_string), &variant_ptr);

    // Setting string variant as request message payload
    ma_message_set_payload(request_payload, variant_ptr);

    // Sending correct request based on the Client type
    if(endpoint_type == SEND_FORGET_CLIENT)
    {
        if(ma_msgbus_send_and_forget(p_endpoint, request_payload) != MA_OK)
        {
            MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Client Number %d Request Number %d - Request could not be sent on Message Bus", client_number, request_number);
        }
    }

    if(endpoint_type == SYNC_CLIENT)
    {
        if(ma_msgbus_send(p_endpoint, request_payload, &response_payload) != MA_OK)
        {
            MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Client Number %d Request Number %d - Request could not be sent on Message Bus", client_number, request_number);
        }
    }

    if(endpoint_type == ASYNC_CLIENT)
    {
        if(ma_msgbus_async_send(p_endpoint, request_payload, process_server_response, NULL, &request_packet) != MA_OK)
        {
            MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Client Number %d Request Number %d - Request could not be sent on Message Bus", client_number, request_number);
        }
    }

    // Getting the current time
    time(&current_time);

    // Log timestamps in file if the following macro is defined
    #ifdef LOG_STATS_IN_FILE
        benchmarking_info_file_handle = fopen(benchmarking_info_file_name, "a+");
        fprintf(benchmarking_info_file_handle, "Client No. - %d \t Request. No. - %d \t Request Time - %s\n", client_number, request_number, ctime(&current_time));
        fclose(benchmarking_info_file_handle);
    #endif
    printf("Client No. - %d \t Request. No. - %d \t Request Time - %s\n", client_number, request_number, ctime(&current_time));

    // Increment the request number (message number is unique within the process)
    request_number++;

    // Increment the count of the requests sent by this client
    total_requests_sent_by_client++;

    if(endpoint_type == SYNC_CLIENT)
    {
        process_server_response(MA_OK, response_payload, NULL, request_packet);
    }

    // Cleanup code
    ma_message_release(request_payload);
    ma_variant_release(variant_ptr);
    free(my_string);
    free(my_combined_string);
}


/******************************************************************************
Call back function to be executed when the server receives a request from the
client. This server will handle both synchronous as well as asynchronous requests.
******************************************************************************/
ma_error_t respond_to_client_requests(ma_msgbus_server_t *server, ma_message_t *request_payload, void *cb_data, ma_msgbus_client_request_t *c_request)
{
    // Variant to hold the variant extracted from message
    ma_variant_t *variant_ptr = NULL;

    // Buffer to hold string extracted from variant
    ma_buffer_t *buffer = NULL;

    // String Message retrieved from the buffer
    char *my_combined_string = NULL;
    size_t size;

    // The string recovered from client's request message will be segregated
    // into multiple parts to retrieve the client number and request number,
    // therefore, make naother copy of it.
    char *response_string = NULL;

    // Will hold the response message created by this server for incoming request
    ma_message_t* response_payload = NULL;

    // Fields to be seperated from the request message received from client
    char *seperator = "-";
    unsigned int client_number = 0;
    unsigned int request_number = 0;
    char *my_string = NULL;
    char *last = NULL;

    // Variable to hold current time
    time_t current_time;

    // Getting current time. Obtain the time as soon as possible for better accurancy.
    time(&current_time);

    // Handling the message payload
    if(request_payload != NULL)
    {
        ma_message_get_payload(request_payload, &variant_ptr);
        ma_variant_get_string_buffer(variant_ptr, &buffer);
        ma_buffer_get_string(buffer, &my_combined_string, &size);

        // Make a copy of the string retrieved from incoming payload
        response_string = strdup(my_combined_string);

        /* Retrieving fields out of the string */
        client_number = atoi(strtok(my_combined_string, seperator));
        request_number = atoi(strtok(NULL, seperator));
        my_string = strtok(NULL, seperator);

        #ifdef DEBUG
            printf("Server No. %d received Req. No. %d from Client No. - %d \t Request Message Size - %d \t Request Message - %s\n", *(int*)cb_data, request_number, client_number, size, my_string);
        #endif

        // Log timestamps in file if the following macro is defined
        #ifdef LOG_STATS_IN_FILE
            // Opening file for logging timestamp at which message is received by the subscriber
            benchmarking_info_file_handle = fopen(benchmarking_info_file_name, "a+");
            fprintf(benchmarking_info_file_handle, "Server No. - %d received Req. No. - %d from Client No. - %d \t Recieve Time - %s\n", *(int*)cb_data, request_number, client_number, ctime(&current_time));
            fclose(benchmarking_info_file_handle);
        #endif
        printf("Server No. - %d received Req. No. - %d from Client No. - %d \t Recieve Time - %s\n", *(int*)cb_data, request_number, client_number, ctime(&current_time));

        ma_variant_release(variant_ptr);

        // Incrementing the count of requests handled by the server
        total_requests_handled_by_server++;

        // Response string
        generate_response(response_string);

        // Creating fresh response message
        if(ma_message_create(&response_payload) != MA_OK)
        {
            MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Response Message could not be created for Client Number %d Request Number %d", client_number, request_number);
        }

        // Create variant from message and set it as payload of the Message
        ma_variant_create_from_string(response_string, strlen(response_string), &variant_ptr);

        ma_message_set_payload(response_payload, variant_ptr);

        #ifdef DEBUG
            printf("Server No. %d responds to Req. No. %d from Client No. - %d \t Response Message - %s\n", *(int*)cb_data, request_number, client_number, response_string);
        #endif

        // Sending response for client request
        if(ma_msgbus_server_client_request_post_result(c_request, MA_OK, response_payload) != MA_OK)
        {
            MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Server Number %d could not respond to Request number %d from Client Number", *(int*)cb_data, request_number, client_number);
        }

        ma_variant_release(variant_ptr);
        free(response_string);
    }
    else
    {
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "No payload in the message received");
        return MA_ERROR_UNEXPECTED;
    }

    return MA_OK;
}


/******************************************************************************
argv[1]            argv[2]              argv[3]                               argv[4]                          argv[5]                   argv[6]
PUBLISHER          <Publisher Number>  <Reachability (INPROC/OUTPROC/OUTBOX)> <Frequency (in Seconds)>         <Message Size (in Bytes)> <No. Of Topics to be Published> <Topics List....>
SUBSCRIBER         <Subscriber Number> <Reachability (INPROC/OUTPROC/OUTBOX)> <No. Of Topics to be Subscribed> <Topics List....>
ASYNC_CLIENT       <Client Number>     <Reachability (INPROC/OUTPROC/OUTBOX)> <Frequency (in Seconds)>         <Message Size (in Bytes)> <Server Name> <Host Name>
SYNC_CLIENT        <Client Number>     <Reachability (INPROC/OUTPROC/OUTBOX)> <Frequency (in Seconds)>         <Message Size (in Bytes)> <Server Name> <Host Name>
SEND_FORGET_CLIENT <Client Number>     <Reachability (INPROC/OUTPROC/OUTBOX)> <Frequency (in Seconds)>         <Message Size (in Bytes)> <Server Name> <Host Name>
SERVER             <Server Number>     <Reachability (INPROC/OUTPROC/OUTBOX)> <Server Name>
******************************************************************************/
int main(int argc, char* argv[])
{
    unsigned int endpoint_number = 0;
    ma_msgbus_consumer_reachability_t reach = MSGBUS_CONSUMER_REACH_OUTPROC;
    unsigned int message_frequency_in_seconds = 0;
    unsigned int message_size_in_bytes = 0;
    char *server_name = NULL;
    char *host_name = NULL;
    unsigned int num_topics = 0; // Refers to number of topics published
                                 // (for a publisher) or number of topics
                                 // subscribed (for a subscriber)
    ma_msgbus_t *msgbus = NULL;

    // Internal counter for looping
    unsigned int counter = 0;

    /**************************************************************************
    Creating Console Logger for logging Info generated by this Sample. Creating
    logger as soon as possible so that error/info logging on console can be
    initiated.
    **************************************************************************/
    if(ma_console_logger_create(&console_logger) != MA_OK)
    {
        printf("Console Logger could not be created.\n");
    }

    /**************************************************************************
    Processing Command Line Arguments (argument 1) to see if the help is
    requested. Argument 0 is the program name.
    **************************************************************************/
    if((strcmp(argv[1], "/?") == 0) || (strcmp(argv[1], "-?") == 0) || (strcmp(argv[1], "/help") == 0) || (strcmp(argv[1], "-help") == 0))
    {
        printf("!!! Welcome to Message Bus Test Sample Code !!!\n");
        printf("Usage Instructions : \n %s", help_string);
        return -1;
    }

    /**************************************************************************
    Processing Command Line Arguments (argument 1) to identify the endpoint type.
    **************************************************************************/
    if(strcmp(argv[1], "PUBLISHER") == 0)
    {
        endpoint_type = PUBLISHER;
    }
    else if(strcmp(argv[1], "SUBSCRIBER") == 0)
    {
        endpoint_type = SUBSCRIBER;
    }
    else if(strcmp(argv[1], "ASYNC_CLIENT") == 0)
    {
        endpoint_type = ASYNC_CLIENT;
    }
    else if(strcmp(argv[1], "SYNC_CLIENT") == 0)
    {
        endpoint_type = SYNC_CLIENT;
    }
    else if(strcmp(argv[1], "SEND_FORGET_CLIENT") == 0)
    {
        endpoint_type = SEND_FORGET_CLIENT;
    }
    else if(strcmp(argv[1], "SERVER") == 0)
    {
        endpoint_type = SERVER;
    }
    else
    {
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Usage Instructions : \n %s", help_string);
        return -1;
    }

    /**************************************************************************
    Processing Command Line Arguments (argument 2) to identify the endpoint number.
    It is a unique number given by the user to identify the running endpoint in
    the logs.
    **************************************************************************/
    endpoint_number = atoi(argv[2]);

    if(endpoint_number < 0)
    {
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Endpoint Number cannot be negative");
        return -1;
    }

    /**************************************************************************
    Processing Command Line Arguments (argument 3) to identify the message/request
    reachability.
    **************************************************************************/
    if(strcmp(argv[3], "INPROC") == 0)
    {
        reach = MSGBUS_CONSUMER_REACH_INPROC;
    }
    else if(strcmp(argv[3], "OUTPROC") == 0)
    {
        reach = MSGBUS_CONSUMER_REACH_OUTPROC;
    }
    else if(strcmp(argv[3], "OUTBOX") == 0)
    {
        reach = MSGBUS_CONSUMER_REACH_OUTBOX;
    }
    else
    {
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Incorrect reachability argument. Defaulting to OUTPROC");
        reach = MSGBUS_CONSUMER_REACH_OUTPROC;
    }

    /**************************************************************************
    Processing further command line arguments to identify server name, host name,
    number of topics to be published (or subscribed) etc. based on the type of
    endpoint).
    **************************************************************************/
    if(endpoint_type == SERVER)
    {
        server_name = argv[4];
    }
    else if(endpoint_type == SUBSCRIBER)
    {
        num_topics = atoi(argv[4]);
    }
    else
    {
        message_frequency_in_seconds = atoi(argv[4]);
    }

    if((endpoint_type == PUBLISHER) || (endpoint_type == SYNC_CLIENT) || (endpoint_type == ASYNC_CLIENT) || (endpoint_type == SEND_FORGET_CLIENT))
    {
        message_size_in_bytes = atoi(argv[5]);
    }

    if(endpoint_type == PUBLISHER)
    {
        num_topics = atoi(argv[6]);
    }

    if((endpoint_type == SYNC_CLIENT) || (endpoint_type == ASYNC_CLIENT) || (endpoint_type == SEND_FORGET_CLIENT))
    {
        server_name = argv[6];
        host_name = argv[7];

        // For client requests, the host name can be NULL if the requests are INPROC or OUTPROC only
        if(strcmp(host_name, "NULL") == 0)
        {
            host_name = NULL;
        }
    }

    #ifdef DEBUG
        printf("Endpoint Type - %d\n", endpoint_type);
        printf("Endpoint Number - %d\n", endpoint_number);
        printf("Endpoint Reach - %d\n", reach);
        printf("Message Frequency (in seconds) - %d\n", message_frequency_in_seconds);
        printf("Message Size (in bytes) - %d\n", message_size_in_bytes);
        printf("Server Name - %s\n", server_name);
        printf("Host Name - %s\n", host_name);
        printf("No. of Topics - %d\n", num_topics);
    #endif

    /**************************************************************************
    Defining signal handler.
    **************************************************************************/
    #ifndef WIN32
        signal(SIGINT, interrupt_handler);
    #else
        SetConsoleCtrlHandler((PHANDLER_ROUTINE)interrupt_handler, TRUE);
    #endif

    /**************************************************************************
    Creating File Logger for Message Bus Internal Logging
    **************************************************************************/
    #ifdef ENABLE_MSGBUS_LOGGING
        if(ma_file_logger_create(NULL, "MessageBus", ".log", &file_logger) != MA_OK)
        {
            MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "File Logger could not be created for Message Bus");
        }

        if(ma_msgbus_set_logger((ma_logger_t *)file_logger) != MA_OK)
        {
            MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "File Logging could not be enabled for Message Bus");
        }
    #endif

    /**************************************************************************
    Initializing message bus
    **************************************************************************/
    if(ma_msgbus_create(PRODUCT_ID, &msgbus) != MA_OK)
    {
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Message Bus could not be Created");
    }

	ma_msgbus_start(msgbus);

    /**************************************************************************
    If endpoint is a SUBSCRIBER, create a subscriber and set the options.
    **************************************************************************/
    if(endpoint_type == SUBSCRIBER)
    {
        if(ma_msgbus_subscriber_create(msgbus, &p_subscriber) != MA_OK)
        {
            MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Subscriber No. %d could not subscribe on Message Bus", endpoint_number);
        }

        if(ma_msgbus_subscriber_setopt(p_subscriber, MSGBUSOPT_REACH, reach) != MA_OK)
        {
            MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Subscriber No. %d reachability could not be set", endpoint_number);
        }
    }

    /**************************************************************************
    If endpoint is a SERVER, create a server and set the options.
    **************************************************************************/
    if(endpoint_type == SERVER)
    {
        if(ma_msgbus_server_create(msgbus, server_name, &p_server) != MA_OK)
        {
            MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Message Bus Server No. %d could not be created", endpoint_number);
        }

        if(ma_msgbus_server_setopt(p_server, MSGBUSOPT_REACH, reach) != MA_OK)
        {
            MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Server No. %d reachability could not be set", endpoint_number);
        }
    }

    /**************************************************************************
    If endpoint is a CLIENT, create a client and set the options.
    **************************************************************************/
    if((endpoint_type == SYNC_CLIENT) || (endpoint_type == ASYNC_CLIENT) || (endpoint_type == SEND_FORGET_CLIENT))
    {
        if(ma_msgbus_endpoint_create(msgbus, server_name, host_name, &p_endpoint) != MA_OK)
        {
            MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Message Bus Client No. %dcould not be created", endpoint_number);
        }

        if(ma_msgbus_endpoint_setopt(p_endpoint, MSGBUSOPT_REACH, reach) != MA_OK)
        {
            MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Client No. %d reachability could not be set", endpoint_number);
        }
    }

    /**************************************************************************
    If endpoint is a SUBSCRIBER, subscriber for topics to be be monitored on the
    message bus.
    **************************************************************************/
    if(endpoint_type == SUBSCRIBER)
    {
        for(counter = 0; counter < num_topics; counter++)
        {
            if(ma_msgbus_subscriber_register(p_subscriber, argv[5 + counter], subscriber_callback_func, (void *)&endpoint_number) != MA_OK)
            {
                MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Message Bus could not be subscribed for Subscriber Number - %d Topic - %s", endpoint_number, argv[5 + counter]);
            }
        }
    }

    /**************************************************************************
    If endpoint is a SERVER, start the server by registering the  call-back
    function.
    **************************************************************************/
    if(endpoint_type == SERVER)
    {
        if(ma_msgbus_server_start(p_server, respond_to_client_requests, (void *)&endpoint_number) != MA_OK)
        {
            MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Message Bus Server No. %d cound not be started", endpoint_number);
        }
    }

    /**************************************************************************
    Loop infinitely till CTRL+C is pressed.
    a) If it is a publisher endpoint, publish messages contineously after the
       finite user given delay.
    b) If it is a client endpoint, send requests on message bus contineously
       after the finite user given delay.
    c) If it is a server or subscriber, wait infinitely for messages belonging
       to your endpoint.
    **************************************************************************/
    if(endpoint_type == PUBLISHER)
    {
        while(!ctrl_c_hit)
        {
            for(counter = 0; counter < num_topics; counter++)
            {
                publish(msgbus, endpoint_number, reach, message_size_in_bytes, argv[7 + counter]);
                Sleep(message_frequency_in_seconds * 1000);
            }
        }
    }
    else if((endpoint_type == SYNC_CLIENT) || (endpoint_type == ASYNC_CLIENT) || (endpoint_type == SEND_FORGET_CLIENT))
    {
        while(!ctrl_c_hit)
        {
            send_request_message(endpoint_number, message_size_in_bytes);
            Sleep(message_frequency_in_seconds * 1000);
        }
    }
    else
    {
        while(!ctrl_c_hit)
        {
        }
    }

    /**************************************************************************
    Log the overall statistics after the CTRL+C is hit. This information can be
    used to identify the number of messages dropped during communication.
    **************************************************************************/
    switch(endpoint_type)
    {
        case PUBLISHER          : printf("Publisher No. - %d \t Msg. Published - %d\n", endpoint_number, total_requests_sent_by_client);
                                  break;
        case SUBSCRIBER         : printf("Subscriber No. - %d \t No. of Msg. Rcvd. - %d\n", endpoint_number, total_requests_handled_by_server);
                                  break;
        case SYNC_CLIENT        : printf("Client No. - %d \t Requests Sent - %d\n", endpoint_number, total_requests_sent_by_client);
                                  printf("Client No. - %d \t Response Rcvd. - %d\n", endpoint_number, total_responses_received_by_client);
                                  break;
        case ASYNC_CLIENT       : printf("Client No. - %d \t Requests Sent - %d\n", endpoint_number, total_requests_sent_by_client);
                                  printf("Client No. - %d \t Response Rcvd. - %d\n", endpoint_number, total_responses_received_by_client);
                                  break;
        case SEND_FORGET_CLIENT : printf("Client No. - %d \t Requests Sent - %d\n", endpoint_number, total_requests_sent_by_client);
                                  break;
        case SERVER             : printf("Server No. - %d \t No. of Req. handled - %d\n", endpoint_number, total_requests_handled_by_server);
                                  break;
        default                 : MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Unknown Endpoint Type.");
    }

    #ifdef LOG_STATS_IN_FILE
        benchmarking_info_file_handle = fopen(benchmarking_info_file_name, "a+");
        switch(endpoint_type)
        {
            case PUBLISHER          : fprintf(benchmarking_info_file_handle, "Publisher No. - %d \t Msg. Published - %d\n", endpoint_number, total_requests_sent_by_client);
                                      break;
            case SUBSCRIBER         : fprintf(benchmarking_info_file_handle, "Subscriber No. - %d \t No. of Msg. Rcvd. - %d\n", endpoint_number, total_requests_handled_by_server);
                                      break;
            case SYNC_CLIENT        : fprintf(benchmarking_info_file_handle, "Client No. - %d \t Requests Sent - %d\n", endpoint_number, total_requests_sent_by_client);
                                      fprintf(benchmarking_info_file_handle, "Client No. - %d \t Response Rcvd. - %d\n", endpoint_number, total_responses_received_by_client);
                                      break;
            case ASYNC_CLIENT       : fprintf(benchmarking_info_file_handle, "Client No. - %d \t Requests Sent - %d\n", endpoint_number, total_requests_sent_by_client);
                                      fprintf(benchmarking_info_file_handle, "Client No. - %d \t Response Rcvd. - %d\n", endpoint_number, total_responses_received_by_client);
                                      break;
            case SEND_FORGET_CLIENT : fprintf(benchmarking_info_file_handle, "Client No. - %d \t Requests Sent - %d\n", endpoint_number, total_requests_sent_by_client);
                                      break;
            case SERVER             : fprintf(benchmarking_info_file_handle, "Server No. - %d \t No. of Req. handled - %d\n", endpoint_number, total_requests_handled_by_server);
                                      break;
            default                 : MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Unknown Endpoint Type.");
        }
        fclose(benchmarking_info_file_handle);
    #endif

    /**************************************************************************
    Unregister and release the subscriber.
    **************************************************************************/
    if(endpoint_type == SUBSCRIBER)
    {
        if(ma_msgbus_subscriber_unregister(p_subscriber) != MA_OK)
        {
            MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Subscriber No. %d could not be unregistered", endpoint_number);
        }

        if(ma_msgbus_subscriber_release(p_subscriber) != MA_OK)
        {
            MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Subscriber No. %d could not be released", endpoint_number);
        }
    }

    /**************************************************************************
    Stop and release server.
    **************************************************************************/
    if(endpoint_type == SERVER)
    {
        if(ma_msgbus_server_stop(p_server) != MA_OK)
        {
            MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Server No. %d could not be stopped", endpoint_number);
        }

        if(ma_msgbus_server_release(p_server) != MA_OK)
        {
            MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Server No. %d could not be released", endpoint_number);
        }
    }

    /**************************************************************************
    Release the endpoint client.
    **************************************************************************/
    if((endpoint_type == SYNC_CLIENT) || (endpoint_type == ASYNC_CLIENT) || (endpoint_type == SEND_FORGET_CLIENT))
    {
        if(ma_msgbus_endpoint_release(p_endpoint) != MA_OK)
        {
            MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Message Bus Client No. %d could not be released", endpoint_number);
        }
    }

	ma_msgbus_stop(msgbus, MA_TRUE);
    /**************************************************************************
    Deinitialize the message-bus
    **************************************************************************/
    if(ma_msgbus_release(msgbus) != MA_OK)
    {
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Message Bus could not be Released");
        return -1;
    }

    /**************************************************************************
    Cleanup loggers.
    **************************************************************************/
    #ifdef ENABLE_MSGBUS_LOGGING
        ma_logger_dec_ref((ma_logger_t *)file_logger);
    #endif

    ma_logger_dec_ref((ma_logger_t *)console_logger);
}





