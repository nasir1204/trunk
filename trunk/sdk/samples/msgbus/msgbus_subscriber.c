#include <stdio.h>
#include <ma/ma_msgbus.h>
#include <ma/ma_message.h>
#include <string.h>
#include "utils.h"

int bStop = 0;
char topic_name[BUFSIZ] = { 0 };
ma_msgbus_consumer_reachability_t reachability;


void control_handler(void)
{
	bStop = 1;
}
static void timer_cb() 
{
	//Does nothing, this is just to keep the uv loop running until the user wants to exit
}
ma_error_t sub_callback(const char *topic, ma_message_t *payload, void *cb_data)
{
	ma_variant_h variant = NULL;
	ma_buffer_h buffer = NULL;
	char *str_message = NULL;
	size_t size;

	if(payload)
	{
		ma_message_get_payload(payload,&variant);
		ma_variant_get_string_buffer(variant,&buffer);
		ma_buffer_get_string(buffer,&str_message,&size);

	}

	printf("received publisher message : %s\n",str_message == NULL?"[NULL]":str_message);
	return MA_OK;
}
int main(int argc, char* argv[])
{
	int ret = OK;
	int interval = 1000;
	set_ctrl_c_handler(control_handler);
	ret = process_subscriber_arguments(argc,argv,topic_name,&reachability);

	if(ret == OK)
	{
		ma_msgbus_subscriber_h p_subscriber;
	    ma_msgbus_t *msgbus = NULL;

		ret = ma_msgbus_create(PRODUCT_ID, &msgbus);
		if(ret != MA_OK)
		{
			printf("failed to create message bus. error = %d\n",ret);
			return ret;
		}
		ma_msgbus_start(msgbus);

		ma_msgbus_subscriber_create(msgbus, &p_subscriber);

		ma_msgbus_subscriber_setopt(p_subscriber,MSGBUSOPT_REACH,reachability);

		ma_msgbus_subscriber_register(p_subscriber,topic_name,sub_callback,NULL);

		do
		{
			timer_cb();
			if(interval > 0)
				Sleep(interval );
		}while(!bStop);
		
		printf("Exiting\n");
		ma_msgbus_subscriber_unregister(p_subscriber);

		ma_msgbus_subscriber_release(p_subscriber);

		ma_msgbus_stop(msgbus, MA_TRUE);
		ma_msgbus_release(msgbus);
	}
	return 0;
}