
#include <ma/ma_msgbus.h>
#include "utils.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <time.h>

#ifndef WIN32
#define strcpy_s(dst, size, src) strncpy(dst, src, size)
#include <unistd.h>
void Sleep(unsigned long millisecs)
{
	unsigned long secs = millisecs/1000;
	sleep(secs);
}
#endif

void print_service_usage(char *argv)
{
	printf("Usage:\t%s /name service_name /reach 1(in_proc)/2(out_proc)/3(out_box) [/replyto string] [/reply string]\nUse /? for help\n",argv);
}
void print_client_usage(char *argv)
{
	printf("Usage:\t%s /name service_name /reach 1(in_proc)/2(out_proc)/3(out_box) [/request string] [/interval millisecs] [/timeout millisecs]\nUse /? for help\n",argv);
}

void print_publisher_usage(char *argv)
{
	printf("Usage:\t%s /topic topic_name /reach 1(in_proc)/2(out_proc)/3(out_box) [/message string] [/interval millisecs] \nUse /? for help\n",argv);
}

void print_subscriber_usage(char *argv)
{
	printf("Usage:\t%s /topic topic_name /reach 1(in_proc)/2(out_proc)/3(out_box)\nUse /? for help\n",argv);
}

void print_service_manager_usage(char *argv)
{
	printf("Usage:\t%s /topic topic_name /option 1(notification)/2(subscribers_list) \nUse /? for help\n",argv);
}

int process_service_arguments(int argc, char* argv[],
	char* service_name,
	ma_msgbus_consumer_reachability_t* reachablility,
	char* reply_to,
	char* reply_message	)
{
	int i = 0;
	int required_args = 0;
	if(argc == 1 || service_name == NULL || reachablility == NULL || reply_to == NULL || reply_message == NULL)
	{
		print_service_usage(argv[0]);
		return INVALID_ARGUMENTS;
	}

	for( i = 1 ; i < argc ; i++)
	{
		if( strcmp(argv[i],"/?") == 0 || strcmp(argv[i],"-?") == 0 || strcmp(argv[i],"/help") == 0 || strcmp(argv[i],"-help") == 0 )
		{
			print_service_usage(argv[0]);
			return INVALID_ARGUMENTS;
		}
		else if( ( strcmp(argv[i],"/name") == 0 || strcmp(argv[i],"-name") == 0) && (i+1 < argc) )
		{
			i++;
			required_args++;
			strcpy_s(service_name,BUFSIZ,argv[i]);
		}
		else if( ( strcmp(argv[i],"/reach") == 0 || strcmp(argv[i],"-reach") == 0) && (i+1 < argc) )
		{
			int reach = 0;
			i++;
			required_args++;
			reach = atoi(argv[i]);
			switch(reach)
			{
			case 1:
				*reachablility = MSGBUS_CONSUMER_REACH_INPROC;
				break;
			case 2:
				*reachablility = MSGBUS_CONSUMER_REACH_OUTPROC;
				break;
			case 3:
				*reachablility = MSGBUS_CONSUMER_REACH_OUTBOX;
				break;
			default:
				printf("Invalid reachability option provided\n");
				print_service_usage(argv[0]);
				return INVALID_ARGUMENTS;
				break;
			}
		}
		else if( ( strcmp(argv[i],"/replyto") == 0 || strcmp(argv[i],"-replyto") == 0) && (i+1 < argc) )
		{
			i++;
			strcpy_s(reply_to,BUFSIZ,argv[i]);
		}
		else if( ( strcmp(argv[i],"/reply") == 0 || strcmp(argv[i],"-reply") == 0) && (i+1 < argc) )
		{
			i++;
			strcpy_s(reply_message,BUFSIZ,argv[i]);
		}
		else
		{
			print_service_usage(argv[0]);
			return INVALID_ARGUMENTS;
		}
	}
	if(required_args != 2)
		return INVALID_ARGUMENTS;
	return MA_OK;
}


int process_client_arguments(int argc, char* argv[],
	char* service_name,
	ma_msgbus_consumer_reachability_t* reachablility,
	char *request_message,
	int *request_interval,
	int *request_timeout)

{
	int i = 0;
	int required_args = 0;
	if(argc == 1 || service_name == NULL || reachablility == NULL || request_message == NULL || request_interval == NULL || request_timeout == NULL)
	{
		print_client_usage(argv[0]);
		return INVALID_ARGUMENTS;
	}

	for( i = 1 ; i < argc ; i++)
	{
		if( strcmp(argv[i],"/?") == 0 || strcmp(argv[i],"-?") == 0 || strcmp(argv[i],"/help") == 0 || strcmp(argv[i],"-help") == 0 )
		{
			print_client_usage(argv[0]);
			return INVALID_ARGUMENTS;
		}
		else if( ( strcmp(argv[i],"/name") == 0 || strcmp(argv[i],"-name") == 0) && (i+1 < argc) )
		{
			i++;
			required_args++;
			strcpy_s(service_name,BUFSIZ,argv[i]);
		}
		else if( ( strcmp(argv[i],"/reach") == 0 || strcmp(argv[i],"-reach") == 0) && (i+1 < argc) )
		{
			int reach = 0;
			i++;
			required_args++;
			reach = atoi(argv[i]);
			switch(reach)
			{
			case 1:
				*reachablility = MSGBUS_CONSUMER_REACH_INPROC;
				break;
			case 2:
				*reachablility = MSGBUS_CONSUMER_REACH_OUTPROC;
				break;
			case 3:
				*reachablility = MSGBUS_CONSUMER_REACH_OUTBOX;
				break;
			default:
				printf("Invalid reachability option provided\n");
				print_client_usage(argv[0]);
				return INVALID_ARGUMENTS;
				break;
			}
		}
		else if( ( strcmp(argv[i],"/request") == 0 || strcmp(argv[i],"-request") == 0) && (i+1 < argc) )
		{
			i++;
			strcpy_s(request_message,BUFSIZ,argv[i]);
		}
		else if( ( strcmp(argv[i],"/interval") == 0 || strcmp(argv[i],"-interval") == 0) && (i+1 < argc) )
		{
			i++;
			*request_interval = atoi(argv[i]);
		}
		else if( ( strcmp(argv[i],"/timeout") == 0 || strcmp(argv[i],"-timeout") == 0) && (i+1 < argc) )
		{
			i++;
			*request_timeout = atoi(argv[i]);
		}
		else
		{
			print_client_usage(argv[0]);
			return INVALID_ARGUMENTS;
		}
	}
	if(required_args != 2)
		return INVALID_ARGUMENTS;
	return MA_OK;
}

int process_service_manager_arguments(	int argc, char* argv[],
										char* topic,
										int *option
									 ) {
	int i = 0;
	int required_args = 0;
	if(argc == 1 || topic== NULL || option == NULL)
	{
		print_service_manager_usage(argv[0]);
		return INVALID_ARGUMENTS;
	}

	for( i = 1 ; i < argc ; i++)
	{
		if( strcmp(argv[i],"/?") == 0 || strcmp(argv[i],"-?") == 0 || strcmp(argv[i],"/help") == 0 || strcmp(argv[i],"-help") == 0 )
		{
			print_service_manager_usage(argv[0]);
			return INVALID_ARGUMENTS;
		}
		else if( ( strcmp(argv[i],"/topic") == 0 || strcmp(argv[i],"-topic") == 0) && (i+1 < argc) )
		{
			i++;
			required_args++;
			strcpy_s(topic,BUFSIZ,argv[i]);
		}
		else if( ( strcmp(argv[i],"/option") == 0 || strcmp(argv[i],"-option") == 0) && (i+1 < argc) )
		{	
			i++;
			required_args++;
			*option = atoi(argv[i]);
			if(!(*option == 1 || *option == 2)) {
				printf("Invalid option provided option %d\n", *option);
				print_service_manager_usage(argv[0]);
				return INVALID_ARGUMENTS;
				break;
			}
		}
		else
		{
			print_service_manager_usage(argv[0]);
			return INVALID_ARGUMENTS;
		}
	}
	if(required_args != 2)
	{
		print_service_manager_usage(argv[0]);
		return INVALID_ARGUMENTS;
	}
	return MA_OK;
}

int process_subscriber_arguments(int argc, char* argv[],
									char* topic,
									ma_msgbus_consumer_reachability_t* reachablility)
{
	int i = 0;
	int required_args = 0;
	if(argc == 1 || topic== NULL || reachablility == NULL)
	{
		print_subscriber_usage(argv[0]);
		return INVALID_ARGUMENTS;
	}

	for( i = 1 ; i < argc ; i++)
	{
		if( strcmp(argv[i],"/?") == 0 || strcmp(argv[i],"-?") == 0 || strcmp(argv[i],"/help") == 0 || strcmp(argv[i],"-help") == 0 )
		{
			print_subscriber_usage(argv[0]);
			return INVALID_ARGUMENTS;
		}
		else if( ( strcmp(argv[i],"/topic") == 0 || strcmp(argv[i],"-topic") == 0) && (i+1 < argc) )
		{
			i++;
			required_args++;
			strcpy_s(topic,BUFSIZ,argv[i]);
		}
		else if( ( strcmp(argv[i],"/reach") == 0 || strcmp(argv[i],"-reach") == 0) && (i+1 < argc) )
		{
			int reach = 0;
			i++;
			required_args++;
			reach = atoi(argv[i]);
			switch(reach)
			{
			case 1:
				*reachablility = MSGBUS_CONSUMER_REACH_INPROC;
				break;
			case 2:
				*reachablility = MSGBUS_CONSUMER_REACH_OUTPROC;
				break;
			case 3:
				*reachablility = MSGBUS_CONSUMER_REACH_OUTBOX;
				break;
			default:
				printf("Invalid reachability option provided\n");
				print_subscriber_usage(argv[0]);
				return INVALID_ARGUMENTS;
				break;
			}
		}
		else
		{
			print_subscriber_usage(argv[0]);
			return INVALID_ARGUMENTS;
		}
	}
	if(required_args != 2)
	{
		print_subscriber_usage(argv[0]);
		return INVALID_ARGUMENTS;
	}
	return MA_OK;

}

int process_publisher_arguments	(	int argc, char* argv[],
									ma_msgbus_consumer_reachability_t* reachablility,
									char *topic,
									char *publish_message,
									int *publish_interval)
{

	int i = 0;
	int required_args = 0;
	if(argc == 1 || topic == NULL || reachablility == NULL || publish_message == NULL || publish_interval == NULL)
	{
		print_publisher_usage(argv[0]);
		return INVALID_ARGUMENTS;
	}

	for( i = 1 ; i < argc ; i++)
	{
		if( strcmp(argv[i],"/?") == 0 || strcmp(argv[i],"-?") == 0 || strcmp(argv[i],"/help") == 0 || strcmp(argv[i],"-help") == 0 )
		{
			print_publisher_usage(argv[0]);
			return INVALID_ARGUMENTS;
		}
		else if( ( strcmp(argv[i],"/topic") == 0 || strcmp(argv[i],"-topic") == 0) && (i+1 < argc) )
		{
			i++;
			required_args++;
			strcpy_s(topic,BUFSIZ,argv[i]);
		}
		else if( ( strcmp(argv[i],"/reach") == 0 || strcmp(argv[i],"-reach") == 0) && (i+1 < argc) )
		{
			int reach = 0;
			i++;
			required_args++;
			reach = atoi(argv[i]);
			switch(reach)
			{
			case 1:
				*reachablility = MSGBUS_CONSUMER_REACH_INPROC;
				break;
			case 2:
				*reachablility = MSGBUS_CONSUMER_REACH_OUTPROC;
				break;
			case 3:
				*reachablility = MSGBUS_CONSUMER_REACH_OUTBOX;
				break;
			default:
				printf("Invalid reachability option provided\n");
				print_publisher_usage(argv[0]);
				return INVALID_ARGUMENTS;
				break;
			}
		}
		else if( ( strcmp(argv[i],"/message") == 0 || strcmp(argv[i],"-message") == 0) && (i+1 < argc) )
		{
			i++;required_args++;
			strcpy_s(publish_message,BUFSIZ,argv[i]);
		}
		else if( ( strcmp(argv[i],"/interval") == 0 || strcmp(argv[i],"-interval") == 0) && (i+1 < argc) )
		{
			i++;
			*publish_interval = atoi(argv[i]);
		}
		else
		{
			print_publisher_usage(argv[0]);
			return INVALID_ARGUMENTS;
		}
	}
	if(required_args != 3) {
                print_publisher_usage(argv[0]);
		return INVALID_ARGUMENTS;
        }
	return OK;
}

InterruptSignalled callback = NULL;

#ifdef WIN32
#include <Windows.h>

BOOL WINAPI HandleConsoleInput(DWORD param)
{
	switch(param)
	{
	case CTRL_C_EVENT:
	case CTRL_BREAK_EVENT:
	case CTRL_CLOSE_EVENT:
	case CTRL_LOGOFF_EVENT:
	case CTRL_SHUTDOWN_EVENT:
		{
			if(callback)
				(*callback)();
			return TRUE;
		}
		break;
	default:
		break;
	}
	return FALSE;
}
void initialize_samples(InterruptSignalled func, HANDLE *hEvent)
{
	if(hEvent == NULL)
		return;
	if( SetConsoleCtrlHandler( HandleConsoleInput, TRUE ) == FALSE )
	{
		return;
	}
	callback = func;
	*hEvent = CreateEvent(NULL,FALSE,FALSE,NULL);
	if(*hEvent == NULL)
		printf("Event creation failed\n");
}
#endif
void set_ctrl_c_handler(InterruptSignalled func)
{
#ifdef WIN32
	if( SetConsoleCtrlHandler( HandleConsoleInput, TRUE ) == FALSE )
	{
		return;
	}
#else
	//For non windows getting the ctrl + c signal
	if(0)
	{
		return;
	}
#endif
	callback = func;
}

