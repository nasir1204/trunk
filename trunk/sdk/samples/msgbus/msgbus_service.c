#include <stdio.h>
#include <string.h>
#include <ma/ma_msgbus.h>
#include <ma/ma_message.h>

#ifdef WIN32
#include <Windows.h>
#endif

#include "utils.h"

ma_error_t handle_service_requests(ma_msgbus_server_h service,ma_message_t* request_payload, void* cb_data, ma_msgbus_client_request_h c_request);
ma_msgbus_server_handler a;

int bStop = 0;

void get_ctrl_c_notification(void)
{
	printf("Received interrupt\n");
	bStop  = 1;
}

char str_reply_to_message[BUFSIZ];
char str_reply[BUFSIZ];


int main(int argc, char* argv[])
{
	int ret = OK;
	char service_name[BUFSIZ] = {0};
	ma_msgbus_consumer_reachability_t reachability;

	//Register for console handlers (windows)
	set_ctrl_c_handler(get_ctrl_c_notification);

	//Process command line arguments
	ret = process_service_arguments(argc,argv,service_name,&reachability,str_reply_to_message,str_reply);
	
	//If valid arguments were passed (use /? for help to get the arguments that must be passed) proceed with starting the service
	if(ret == OK)
	{
		ma_msgbus_server_h p_service = NULL; //Holds the handle to the service
		ma_error_t ret_code = MA_OK; //to handle 
        ma_msgbus_t *msgbus = NULL;
		//initialize message bus
		ret_code = ma_msgbus_create(PRODUCT_ID, &msgbus);
		if(ret_code != MA_OK)
		{
			printf("Failed to Create Message Bus. Error :%d\n",ret_code);
		}
		ma_msgbus_start(msgbus);
		//Create a handler to a service by passing the service name
		ret_code = ma_msgbus_server_create(msgbus, service_name, &p_service);
		if(ret_code != MA_OK)
		{
			printf("Unable to create service. Error :%d\n",ret_code);
			return ret_code;
		}
		//Set the reachability 
		ret_code = ma_msgbus_server_setopt(p_service,MSGBUSOPT_REACH,reachability);
		if(ret_code != MA_OK)
		{
			printf("Unable to set Reachability option. Error :%d\n",ret_code);
			ma_msgbus_server_release(p_service);
			p_service = NULL;
			return ret_code;
		}
		//Start the server
		ret_code = ma_msgbus_server_start(p_service,handle_service_requests,NULL);
		if(ret_code != MA_OK)
		{
			printf("Unable to start the server. Error :%d\n",ret_code);
			ma_msgbus_server_release(p_service);
			p_service = NULL;
			ma_msgbus_release(msgbus);
			return ret_code;
		}
		//This is an asynchronous call and would return immediately
		//Your business logic should probably go here, if there is no business logic, 
		//and you don't want to exit, wait for a signal to quit.
		//Remove this code if you have some logic which prevents your program from exiting
		
		do
		{
			Sleep(1000);
		}while(!bStop);

		//The call will be blocked unless Ctrl + C or Ctrl + Break is pressed
		//Stop the service
		printf("Stopping server and deinitializing message bus\n");
		ret_code = ma_msgbus_server_stop(p_service);
		if(ret_code != MA_OK)
			printf("Unable to stop the server. Error: %d\n",ret_code);

		ret_code = ma_msgbus_server_release(p_service);
		if(ret_code != MA_OK)
			printf("Unable to release the server instance. Error: %d\n",ret_code);

		ma_msgbus_stop(msgbus, MA_TRUE);
		ret_code = ma_msgbus_release(msgbus);
	}
	return ret;
}

//The service handles 2 types of requests
//1 type of request is for clients which except a response
//2 type is for clients that don't except a response
ma_error_t handle_service_requests(	ma_msgbus_server_h service, 
									ma_message_t* request_payload,
									void* cb_data, 
									ma_msgbus_client_request_h c_request)
{
	ma_message_t* response_message = NULL;
	ma_error_t err_code = MA_OK;
	ma_variant_h payload = NULL;
	ma_buffer_h buffer = NULL;
	size_t size;
	char *request = NULL;
	char *prop_value = NULL;

	//The below code is just to illustrate an example, feel free to remove the code and replace your business logic 
	//The below logic, checks for any request that sent a string pattern matching to a command line argument if passed
	//and replies with the string (if supplied in command line arguments), otherwise an empty response is sent
	printf("received a request\n");

	err_code = ma_message_create(&response_message);
	if( strlen(str_reply_to_message) > 0 )
	{
		int invalid = 0;
		int saf = 0;
		//Check if there is a property SAF present, if so, just return since client is not expecting a response
		ma_message_get_property(request_payload,"SAF",&prop_value);
		if(prop_value)
		{
			if(strcmp(prop_value,"100")==0)
				saf = 1;
		}
		//Extract the variant from request
		ma_message_get_payload(request_payload,&payload);
		if(payload)
		{
			//Extract the buffer from variant
			ma_variant_get_string_buffer(payload,&buffer);
			//Extract the string from variant
			ma_buffer_get_string(buffer,&request,&size);
			//Cleanup unwanted variables
			ma_buffer_release(buffer);
			buffer = NULL;
			ma_variant_release(payload);
			payload = NULL;
		}
		//Check if the 
		if(( request ) )
		{	
			if(saf)
			{
				printf("Received a message from client %s\n",request);	
				ma_message_release(response_message);
				return MA_OK;
			}
			if(strcmp(request,str_reply_to_message) != 0 )
				invalid = 1;
		}
		else
		{
			if(saf)
			{
				printf("Obtained an empty message from client\n");	
				ma_message_release(response_message);
				return MA_OK;
			}
			invalid = 1;
		}
		if(invalid)
		{
			printf("Did not find the request pattern in attached in the request\n");
			ma_msgbus_server_client_request_post_result(c_request,MA_ERROR_ACCESS_DENIED,response_message);
			ma_message_release(response_message);
			return MA_OK;
		}
	}
	if(strlen(str_reply) > 0)
	{
		ma_variant_create_from_string(str_reply,strlen(str_reply),&payload);
		ma_message_set_payload(response_message,payload);
		printf("Attaching payload \n");
	}
	printf("posting response to client\n");
	ma_msgbus_server_client_request_post_result(c_request,MA_OK,response_message);
	ma_message_release(response_message);
	return MA_OK;
}