#include <stdio.h>
#include <string.h>
#include <ma/ma_msgbus.h>
#include <ma/ma_message.h>
#include "utils.h"

#include "ma/logger/ma_file_logger.h"

int bStop = 0;
char service_name[BUFSIZ] = { 0 };
char req_message[BUFSIZ] = { 0 };
int request_interval = 0;
int timeout = 0;
ma_msgbus_consumer_reachability_t reachability;
ma_msgbus_endpoint_h p_endpoint;

void control_handler(void)
{
	bStop = 1;
}

void send_client_message()
{
	ma_error_t ret_code;
	ma_message_t* response = NULL;
	ma_message_t* payload = NULL;
	ret_code = ma_msgbus_endpoint_setopt(p_endpoint,MSGBUSOPT_REACH,MSGBUS_CONSUMER_REACH_OUTPROC);
	ret_code = ma_msgbus_endpoint_setopt(p_endpoint,MSGBUSOPT_TIMEOUT,timeout > 0 ? timeout : 10000);
	ret_code = ma_message_create(&payload);
	if(strlen(req_message) > 0)
	{
		ma_variant_h variant;
		ret_code = ma_variant_create_from_string(req_message,strlen(req_message),&variant);
		ret_code = ma_message_set_payload(payload,variant);
	}
	ret_code = ma_msgbus_send(p_endpoint,payload,&response);
	if(ret_code == MA_OK)
	{
		ma_variant_h variant;
		ma_buffer_h buffer;
		char* value = NULL;
		size_t size;
		ma_message_get_payload(response,&variant);
		if(variant)
		{
			ma_variant_get_string_buffer(variant,&buffer);
			ma_buffer_get_string(buffer,&value,&size);
			ma_buffer_release(buffer);
			ma_variant_release(variant);
			printf("Received response: %s\n",value);
		}
		else
		{
			printf("Obtained a response\n");
		}
	}
	else
		printf("error:%d\n",ret_code);
}

static const char log_filename[] = "msgbus_client_test";
ma_file_logger_t *file_logger = NULL;

int main(int argc, char* argv[])
{
	int ret = OK;
	set_ctrl_c_handler(control_handler);

  	ret = process_client_arguments(argc, argv, service_name, &reachability, req_message, &request_interval, &timeout);
	if(ret == OK)
	{
		ma_error_t ret_code = MA_OK;
		ma_msgbus_t *msgbus = NULL;
        ret_code = ma_msgbus_create(PRODUCT_ID, &msgbus);
		if(ret_code != MA_OK)
		{
			printf("failed to create message bus. error = %d\n",ret_code);
			return ret_code;
		}
		ma_msgbus_start(msgbus);
        if(MA_OK == ma_file_logger_create(NULL, log_filename, ".log", &file_logger))
        ma_msgbus_set_logger(msgbus, (ma_logger_t*)file_logger);  


		ret_code = ma_msgbus_endpoint_create(msgbus, service_name,"localhost",&p_endpoint);		
		send_client_message();

		while(!bStop)
		{
			if(request_interval > 0)
			{
				Sleep(request_interval);
				if(!bStop) send_client_message();
			}
		}


		ret_code = ma_msgbus_endpoint_release(p_endpoint);
		ma_msgbus_stop(msgbus, MA_TRUE);
		ret_code = ma_msgbus_release(msgbus);

        if(file_logger) {
        ma_file_logger_release(file_logger);
        file_logger = NULL;
        }

	}

	return 0;
}