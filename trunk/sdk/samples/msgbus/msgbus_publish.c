#include <stdio.h>
#include <string.h>
#include <ma/ma_msgbus.h>
#include <ma/ma_message.h>
#include "utils.h"

int bStop = 0;
char topic_name[BUFSIZ] = { 0 };
char publish_message[BUFSIZ] = { 0 };
ma_msgbus_consumer_reachability_t reachability;
int b_uv_loop_use = 0;
void control_handler(void)
{
	bStop = 1;
}

static void publish(ma_msgbus_t *msgbus) 
{
	ma_error_t err;
	ma_message_t* payload;
	ma_variant_h data;
	ma_message_create(&payload);
	ma_variant_create_from_string(publish_message,strlen(publish_message),&data);
	ma_message_set_payload(payload,data);
	printf("Publish message ... %s \n", topic_name);
	err = ma_msgbus_publish(msgbus, topic_name, reachability, payload);
	if(err != MA_OK)
		printf("Publish API failed : %d\n",err);
}

int main(int argc, char* argv[])
{
	int interval = 0;
	int ret = 0;
	set_ctrl_c_handler(control_handler);
	ret = process_publisher_arguments(argc,argv,&reachability,topic_name,publish_message,&interval);
	if(ret == OK)
	{
		ma_msgbus_t *msgbus = NULL;

		ma_error_t ret_code = ma_msgbus_create(PRODUCT_ID, &msgbus);
		if(ret_code != MA_OK)
		{
			printf("failed to create message bus. error = %d\n",ret_code);
			return ret_code;
		}
		ma_msgbus_start(msgbus);
		publish(msgbus);

		while(!bStop)
		{
			if(interval> 0)
			{
				Sleep(interval);
				publish(msgbus);
			}
		}

		ma_msgbus_stop(msgbus, MA_TRUE);
		ma_msgbus_release(msgbus);
	}
	return 0;
}
