#ifndef UTILS_H_
#define UTILS_H_

#define PRODUCT_ID  "MSGBUS_SAMPLES"

#define INVALID_ARGUMENTS -1
#define OK 0

#ifndef WIN32
void Sleep(unsigned long millisecs);
#else
#include <Windows.h>
#endif

typedef void (*InterruptSignalled)(void);

int process_client_arguments	(	int argc, char* argv[],
									char* service_name,
									ma_msgbus_consumer_reachability_t* reachablility,
									char *request_message,
									int *request_interval,
									int *request_timeout);

int process_service_arguments	(	int argc, char* argv[],
									char* service_name,
									ma_msgbus_consumer_reachability_t* reachablility,
									char* reply_to,
									char* reply_message);

int process_publisher_arguments	(	int argc, char* argv[],
									ma_msgbus_consumer_reachability_t* reachablility,
									char *topic,
									char *publish_message,
									int *publish_interval);

int process_subscriber_arguments(	int argc, char* argv[],
									char* topic,
									ma_msgbus_consumer_reachability_t* reachability);

int process_service_manager_arguments(	int argc, char* argv[],
										char* topic,
										int *option);

void set_ctrl_c_handler(InterruptSignalled func);

#endif //UTILS_H_