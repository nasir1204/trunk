#include <stdio.h>
#include <string.h>
#include <ma/ma_msgbus.h>
#include <ma/ma_message.h>
#include "utils.h"

int bStop = 0;

char service_name[BUFSIZ] = { 0 };
char req_message[BUFSIZ] = { 0 };
int request_interval = 0;
int timeout = 0;
ma_msgbus_consumer_reachability_t reachability;
ma_msgbus_endpoint_h p_endpoint;

void control_handler(void)
{
	bStop = 1;
}


void send_client_message()
{
	ma_error_t ret_code = MA_OK;
	ma_message_t* response = NULL;
	ma_message_t* payload = NULL;

	printf("Send client message\n");
	ret_code = ma_message_create(&payload);
	if(strlen(req_message) > 0)
	{
		ma_variant_h variant;
		ret_code = ma_variant_create_from_string(req_message, strlen( req_message ), &variant);
		ret_code = ma_message_set_payload( payload, variant);
	}
	printf("Setting property\n");
	ma_message_set_property(payload,"SAF","100");
	ret_code = ma_msgbus_send_and_forget(p_endpoint,payload);
	if(ret_code == MA_OK)
	{
		printf("Sent a message\n");
	}
	else
		printf("error:%d\n",ret_code);
}


int main(int argc, char* argv[])
{
	int ret = OK;
	set_ctrl_c_handler(control_handler);

	ret = process_client_arguments(argc, argv, service_name, &reachability, req_message, &request_interval, &timeout);
	if(ret == OK)
	{
        ma_error_t ret_code = MA_OK;
		ma_msgbus_t *msgbus = NULL;

		ret_code = ma_msgbus_create(PRODUCT_ID, &msgbus);
        if(ret_code != MA_OK)
		{
			printf("failed to create message bus. error = %d\n",ret_code);
			return ret_code;
		}
		ma_msgbus_start(msgbus);

		ret_code = ma_msgbus_endpoint_create( msgbus, service_name, "localhost", &p_endpoint);
		ret_code = ma_msgbus_endpoint_setopt( p_endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
		ret_code = ma_msgbus_endpoint_setopt( p_endpoint, MSGBUSOPT_TIMEOUT, timeout > 0 ? timeout : 10000);
		printf("Timer initialization");
		
		
		send_client_message();

		while(!bStop)
		{
			if(request_interval > 0)
			{
				Sleep(request_interval);
				if(!bStop) send_client_message();
			}
		}


		ret_code = ma_msgbus_endpoint_release(p_endpoint);
		ma_msgbus_stop(msgbus, MA_TRUE);
		ret_code = ma_msgbus_release(msgbus);
	}
	printf("Exiting ");
		
	return 0;
}