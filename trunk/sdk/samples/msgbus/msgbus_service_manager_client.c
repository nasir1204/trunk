#include <stdio.h>
#include <string.h>
#include <ma/ma_msgbus.h>
#include <ma/ma_message.h>
#include "ma/ma_service_manager.h"
#include "ma/logger/ma_file_logger.h"

#include "utils.h"
int bStop = 0;
char topic_name[BUFSIZ] = { 0 };
int option = 0; 

ma_file_logger_t *file_logger = NULL;

ma_service_manager_t *srvc_mgr = NULL;

void control_handler(void) {	
	bStop  = 1;
}

static void timer_cb() 
{
	//Does nothing, this is just to keep the uv loop running until the user wants to exit
}

static ma_error_t on_subscribe_cb(ma_service_manager_t *service_manager, ma_bool_t status, const char *topic, const char *product_id, void *cb_data) {
	printf("Receivied Subscriber Notification topic filter <%s> status <%s> product id <%s>\n", topic, (status) ? "Registered" : "UnRegistered", product_id);
	return MA_OK;
}

int main(int argc, char* argv[])
{
	int ret = OK;
	int interval = 1000;

	set_ctrl_c_handler(control_handler);
	
	ret = process_service_manager_arguments(argc,argv,topic_name, &option);
    
	ma_file_logger_create(NULL, "service_manager_client", ".log", &file_logger);	   
    
	if(ret == OK) {
		ma_error_t ret_code = MA_OK;
		ma_msgbus_t *msgbus = NULL;

		ret_code = ma_msgbus_create(PRODUCT_ID, &msgbus);
		if(ret_code != MA_OK) {
			printf("failed to create message bus. error = %d\n",ret_code);
			return ret_code;
		}
		
		ma_msgbus_set_logger(msgbus, (ma_logger_t *)file_logger);

		ret_code = ma_msgbus_start(msgbus);
		if(ret_code != MA_OK) {
			printf("failed to start message bus. error = %d\n",ret_code);
			ma_msgbus_release(msgbus);
			return ret_code;
		}
		
		ret_code = ma_service_manager_create(msgbus, &srvc_mgr);
		if(ret_code != MA_OK)
		{
			printf("failed to create service manager handle. error = %d\n",ret_code);
			ma_msgbus_stop(msgbus, MA_TRUE);
			ma_msgbus_release(msgbus);
			return ret_code;
		}

		if(1 == option) {
			ma_service_manager_set_logger(srvc_mgr, (ma_logger_t *)file_logger);

			ret_code = ma_service_manager_register_subscriber_notification(srvc_mgr, topic_name, on_subscribe_cb, NULL);
			if(ret_code != MA_OK)
			{
				printf("failed to register subscriber notifications. error = %d\n",ret_code);
				ma_service_manager_release(srvc_mgr);
				ma_msgbus_stop(msgbus, MA_TRUE);
				ma_msgbus_release(msgbus);
				return ret_code;
			}

			do
			{
				timer_cb();
				if(interval > 0)
					Sleep(interval );
			}while(!bStop);
			
			ma_service_manager_unregister_subscriber_notification(srvc_mgr, topic_name);
		}
		else {
			ma_service_manager_subscriber_list_t *list = NULL;
			ma_uint32_t count = 0;
			ret_code = ma_service_manager_get_subscribers(srvc_mgr, topic_name, &list, &count);
			if(ret_code == MA_OK)
			{
				int i = 0;
				printf("Number of Subscribers registered = %d for topic %s\n", count, topic_name);
				for(i = 0; i < count; i++) {
					char *topic = NULL, *product_id = NULL;
					ma_int64_t pid = 0;
					if(MA_OK == (ret_code = ma_service_manager_subscriber_list_get_topic_name(list, i, &topic))) 
						if(MA_OK == (ret_code = ma_service_manager_subscriber_list_get_product_id(list, i, &product_id))) 
								printf("index = %d - topic filter = %s, product id = %s\n", i, topic, product_id);
				}
				if(list) ma_service_manager_subscriber_list_release(list);
			}
		}
		printf("Exiting\n");	

		ret_code = ma_service_manager_release(srvc_mgr);
		ma_msgbus_stop(msgbus, MA_TRUE);
		ma_msgbus_release(msgbus);
		ma_logger_dec_ref((ma_logger_t *)file_logger);
	}

	return ret;
}