#include <stdio.h>
#include <string.h>
#include <ma/ma_msgbus.h>
#include <ma/ma_message.h>
#include "utils.h"
int bStop = 0;

char service_name[BUFSIZ] = { 0 };
char req_message[BUFSIZ] = { 0 };
int request_interval = 0;
int timeout = 0;
ma_msgbus_consumer_reachability_t reachability;
ma_msgbus_endpoint_h p_endpoint;

void control_handler(void)
{
	bStop  = 1;
}

ma_error_t request_callback(ma_error_t status, ma_message_t *response, void *cb_data, ma_msgbus_request_t *request)
{
	if(status == MA_OK)
	{
		ma_variant_h variant;
		ma_buffer_h buffer;
		char* value = NULL;
		size_t size;
		ma_message_get_payload(response,&variant);
		if(variant)
		{
			ma_variant_get_string_buffer(variant,&buffer);
			ma_buffer_get_string(buffer,&value,&size);
			ma_buffer_release(buffer);
			ma_variant_release(variant);
			printf("Received response: %s\n",value);
		}
		else
			printf("Obtained a empty response\n");       
	}
	else
		printf("The service responded status : %d\n",status);

     ma_msgbus_request_release(request);

	return MA_OK;
}

void send_client_message()
{
	ma_error_t ret_code = MA_OK;
	ma_msgbus_request_h request;
	ma_message_t* payload;

	ret_code = ma_message_create(&payload);
	if(strlen( req_message ) > 0)
	{
		ma_variant_h variant;
		ret_code = ma_variant_create_from_string( req_message, strlen( req_message ), &variant);
		if(ret_code != MA_OK)
			printf("variant creation failed. error = %d\n",ret_code);
		ret_code = ma_message_set_payload( payload, variant);
		if(ret_code != MA_OK)
			printf("setting the payload to message failed. error = %d\n",ret_code);

        ma_variant_release(variant);
	}

	ret_code = ma_msgbus_async_send( p_endpoint, payload, request_callback, NULL, &request);

	if( ret_code != MA_OK )
		printf("Async send failed error: %d\n",ret_code);
	
	ret_code = ma_message_release(payload);
}


int main(int argc, char* argv[])
{
	int ret = OK,i=0;
	set_ctrl_c_handler(control_handler);
	ret = process_client_arguments(argc,argv,service_name,&reachability,req_message,&request_interval,&timeout);
	
	if(ret == OK)
	{
		ma_error_t ret_code = MA_OK;
		ma_msgbus_t *msgbus = NULL;        
		ret_code = ma_msgbus_create(PRODUCT_ID, &msgbus);
		if(ret_code != MA_OK)
		{
			printf("failed to create message bus. error = %d\n",ret_code);
			return ret_code;
		}
		ma_msgbus_start(msgbus);
		ret_code = ma_msgbus_endpoint_create( msgbus, service_name, "localhost", &p_endpoint);
		if(ret_code != MA_OK)
		{
			printf("failed to create end point. error = %d\n",ret_code);
			ma_msgbus_release(msgbus);
			return ret_code;
		}
		ret_code = ma_msgbus_endpoint_setopt( p_endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
		if(ret_code != MA_OK)
		{
			printf("failed to set endpoint option. error = %d\n",ret_code);
			ma_msgbus_endpoint_release(p_endpoint);
			p_endpoint = NULL;
			ma_msgbus_release(msgbus);
            return ret_code;
		}

		ret_code = ma_msgbus_endpoint_setopt( p_endpoint, MSGBUSOPT_TIMEOUT, timeout > 0 ? timeout : 10000);
		if(ret_code != MA_OK)
		{
			printf("failed to set endpoint option. error = %d\n",ret_code);
			ma_msgbus_endpoint_release(p_endpoint);
			p_endpoint = NULL;
			ma_msgbus_release(msgbus);
			return ret_code;
		}

		send_client_message();

		while(!bStop)
		{
			if(request_interval > 0)
			{
				Sleep(request_interval);
				if(!bStop) send_client_message();
			}
		}

		ret_code = ma_msgbus_endpoint_release(p_endpoint);
		ma_msgbus_stop(msgbus, MA_TRUE);
		ret_code = ma_msgbus_release(msgbus);
	}

	return ret;
}