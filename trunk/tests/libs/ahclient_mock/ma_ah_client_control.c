#include "ma/internal/utils/platform/ma_sys_apis.h"
#include "ma/internal/services/ahclient/ma_ah_client_service.h"

#include "src/services/ahclient/ma_spipe_mux.h"
#include "src/services/ahclient/ma_spipe_demux.h"
#include "src/services/ahclient/ma_spipe_url_request.h"
#include "src/services/ahclient/ma_spipe_alert_list.h"

typedef enum ma_ah_client_service_state_e{
	/*AH client idle*/
	MA_AH_CS_STATE_IDLE=0,

	/*AH client schduled, with normal priority*/
	MA_AH_CS_STATE_SPIPE_SCHEDULED,

	/*AH client schduled, with immediate priority*/
	MA_AH_CS_STATE_SPIPE_IMMEDIATE_SCHEDULED,		

	/*AH client in process of spipe decoration*/
	MA_AH_CS_STATE_SPIPE_MUX,	

	/*AH client in process of spipe upload*/
	MA_AH_CS_STATE_SPIPE_SEND,	

	/*AH client in process of spipe response handling*/
	MA_AH_CS_STATE_SPIPE_DEMUX,		
}ma_ah_client_service_state_t;

#define CHECK_ACTIVE_SPIPE(state) (state >= MA_AH_CS_STATE_SPIPE_MUX) 

struct ma_ah_client_service_s{	
	uv_loop_t *uvloop;
	ma_crypto_t *crypto;	
	ma_bool_t cancel_spipe_connection;
	uv_timer_t spipe_mux_trigger;	
	ma_ah_site_url_provider_t spipe_url_provider;
	ma_spipe_mux_t spipe_multiplexer;
	ma_spipe_demux_t spipe_demultiplexer;	
	ma_spipe_alert_list_t spipe_alerts;
	ma_ah_client_service_state_t ah_service_state;
	ma_spipe_priority_t spipe_priority;
	/*ma_bool_t spipe_alert;	*/
	ma_spipe_url_request_t *spipe_url_request;
};

void ma_ah_client_control_set_spipe_response(ma_ah_client_service_t *self, ma_error_t error, int response_code, ma_spipe_package_t *package){
	self->spipe_url_request->error = error;
	self->spipe_url_request->response_code = response_code;
	self->spipe_demultiplexer.pkg = package;
}