#ifndef MA_AH_CLIENT_CONTROL_H_INCLUDED
#define MA_AH_CLIENT_CONTROL_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/services/ahclient/ma_ah_client_service.h"

MA_CPP(extern "C" {)

MA_AH_CLIENT_API void ma_ah_client_control_set_spipe_response(ma_ah_client_service_t *self, ma_error_t error, int response_code, ma_spipe_package_t *package);

MA_CPP(})

#endif