/*
Updater Client - Updater UTs
*/

#define UNITY_FIXTURE_MALLOC_OVERRIDES_H_ 1

#include "unity.h"
#include "unity_fixture.h"
#include "unity_internals.h"
#include "ma/internal/services/updater/ma_updater_utils.h"
#include "ma/updater/ma_updater.h"
#include "ma_ut_setup.h"
#include <string.h>
#include <stdlib.h>

static ma_client_t *g_ma_client = NULL;
static ma_msgbus_t *g_msgbus = NULL;
static ma_ut_setup_t *g_ut_setup = NULL;

static const char product_id[] = "ma.updater.client.test";

// Updater and Scheduler service objects (message bus server)
static ma_msgbus_server_t *updater_server = NULL;
static ma_msgbus_server_t *scheduler_server = NULL;

// Indicates the test scenario currently executing
static ma_uint8_t test_scenario = 0;

// Stores the task ID sent to the updater service for verification with the session ID returned by the client.
char *global_task_id = NULL;

// Declaration for callback functions
static ma_error_t updater_server_cb(ma_msgbus_server_t *server, ma_message_t *request_message, void *cb_data, ma_msgbus_client_request_t *c_request);
static ma_error_t scheduler_server_cb(ma_msgbus_server_t *server, ma_message_t *request_message, void *cb_data, ma_msgbus_client_request_t *c_request);

// Extern definition
extern ma_error_t convert_variant_to_task(ma_variant_t *task_details, ma_task_t *task);

TEST_GROUP_RUNNER(ma_updater_test_group)
{
    //do {RUN_TEST_CASE(ma_updater_tests, updater_test)} while (0);
}


/**************************************************************************
1) Creating the unit test context.
2) Retrieving underlying message bus instance.
3) Creating and starting message bus services (Updater and Scheduler Services)
   using the message bus instance.
**************************************************************************/
TEST_SETUP(ma_updater_tests)
{
    ma_ut_setup_create("updater.update.tests", product_id, &g_ut_setup);
    g_ma_client = ma_ut_setup_get_client(g_ut_setup);
    g_msgbus = ma_ut_setup_get_msgbus(g_ut_setup);
    ma_msgbus_server_create(g_msgbus, "ma.service.updater", &updater_server);
    ma_msgbus_server_create(g_msgbus, "ma.service.scheduler", &scheduler_server);

    ma_msgbus_server_start(updater_server, updater_server_cb, NULL);
    ma_msgbus_server_start(scheduler_server, scheduler_server_cb, NULL);
}

/**************************************************************************
1) Stopping and releasing the message bus service (Updater and Scheduler
   services).
2) Releasing the context.
**************************************************************************/
TEST_TEAR_DOWN(ma_updater_tests)
{
    ma_msgbus_server_stop(updater_server);
    ma_msgbus_server_stop(scheduler_server);
    ma_msgbus_server_release(updater_server);
    ma_msgbus_server_release(scheduler_server);
    ma_ut_setup_release(g_ut_setup);
}

// Dummy callbacks used during registration
ma_error_t notify_callback(ma_client_t *ma_client, const char *product_id, void *cb_data, const char *update_type, ma_update_state_t state, const char *message, const char *extra_info, ma_updater_product_return_code_t *return_code)
{
    return MA_OK;
}

ma_error_t set_callback(ma_client_t *ma_client, const char *product_id, void *cb_data, const char *update_type, const char *key, ma_variant_t *value, ma_updater_product_return_code_t *return_code)
{
    return MA_OK;
}

ma_error_t get_callback(ma_client_t *ma_client, const char *product_id, void *cb_data, const char *update_type, const char *key, ma_variant_t **value, ma_updater_product_return_code_t *return_code)
{
    return MA_OK;
}

ma_error_t update_event_callback(ma_client_t *ma_client, const char *product_id, void *cb_data, const ma_updater_event_t *event_info)
{
    return MA_OK;
}

static struct ma_updater_callbacks_s const callbacks_bundle = { notify_callback, set_callback, get_callback, update_event_callback };


/******************************************************************************
Call back function to be executed when the Updater Service receives requests
from the updater client.

This function verifies the update parameters (Task ID, Update Request Type
and Update Initiator Type) sent to the updater service in each scenario.

TEST 1 : "Start Update" test (Scenario - Update task could not be added to
         scheduler service successfully)
TEST 2 : "Start Update" test (Scenario - Updater started successfully)
TEST 3 : "Stop Update" test
TEST 4 : "Check Update Status" test. Respond with  MA_UPDATE_STATE_BACKUP state.
TEST 5 : "Check Update Status" test. Respond with  MA_UPDATE_STATE_PRENOTIFY_FORCE
         state.
TEST 6 : "Check Update Status" test. Respond with  MA_UPDATE_STATE_SUSPEND state.
TEST 7 : "Check Update Status" test. Respond with  MA_UPDATE_STATE_DOWNLOAD state.
******************************************************************************/
static ma_error_t updater_server_cb(ma_msgbus_server_t *server, ma_message_t *payload, void *cb_data, ma_msgbus_client_request_t *c_request)
{
    ma_variant_t *variant_ptr = NULL;
    ma_updater_request_param_t *params = NULL;
    ma_message_t *response_payload = NULL;

    // Get the update type and other parameter information from the payload for verification
    TEST_ASSERT(MA_OK == ma_message_get_payload(payload, &variant_ptr));
    TEST_ASSERT(MA_OK == ma_updater_request_param_from_variant(variant_ptr, &params));
    TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

    // "Start Update" test (Scenario - Update task could not be added to scheduler service successfully)
    if(test_scenario == 1)
    {
        TEST_ASSERT(MA_UPDATER_REQUEST_TYPE_START == params->request_type);
        TEST_ASSERT(MA_UPDATER_INITIATOR_TYPE_TASK_MIRROR == params->initiator_type);
        TEST_ASSERT(0 == strcmp(global_task_id, params->task_id));

        // Respond with MA_OK message
        TEST_ASSERT(MA_OK == ma_message_create(&response_payload));
        TEST_ASSERT(MA_OK == ma_msgbus_server_client_request_post_result(c_request, MA_OK, response_payload));
        TEST_ASSERT(MA_OK == ma_message_release(response_payload));
    }

    // "Start Update" test (Scenario - Updater started successfully)
    if(test_scenario == 2)
    {
        TEST_ASSERT(MA_UPDATER_REQUEST_TYPE_START == params->request_type);
        TEST_ASSERT(MA_UPDATER_INITIATOR_TYPE_TASK_MIRROR == params->initiator_type);
        TEST_ASSERT(0 == strcmp(global_task_id, params->task_id));

        // Respond with MA_OK message
        TEST_ASSERT(MA_OK == ma_message_create(&response_payload));
        TEST_ASSERT(MA_OK == ma_msgbus_server_client_request_post_result(c_request, MA_OK, response_payload));
        TEST_ASSERT(MA_OK == ma_message_release(response_payload));
    }

    // "Stop Update" test
    if(test_scenario == 3)
    {
        TEST_ASSERT(MA_UPDATER_REQUEST_TYPE_STOP == params->request_type);
        TEST_ASSERT(MA_UPDATER_INITIATOR_TYPE_ONDEMAND_UPDATE == params->initiator_type);
        TEST_ASSERT(0 == strcmp(global_task_id, params->task_id));

        // Respond with MA_OK message
        TEST_ASSERT(MA_OK == ma_message_create(&response_payload));
        TEST_ASSERT(MA_OK == ma_msgbus_server_client_request_post_result(c_request, MA_OK, response_payload));
        TEST_ASSERT(MA_OK == ma_message_release(response_payload));

    }

    // "Check Update Status" test
    if(test_scenario == 4)
    {
        TEST_ASSERT(MA_UPDATER_REQUEST_TYPE_STATUS_CHECK == params->request_type);
        TEST_ASSERT(MA_UPDATER_INITIATOR_TYPE_ONDEMAND_UPDATE == params->initiator_type);
        TEST_ASSERT(0 == strcmp(global_task_id, params->task_id));

        // Respond with MA_OK message and update state (payload) as MA_UPDATE_STATE_BACKUP
        TEST_ASSERT(MA_OK == ma_message_create(&response_payload));
        TEST_ASSERT(MA_OK == ma_variant_create_from_int16(MA_UPDATE_STATE_BACKUP, &variant_ptr));
        TEST_ASSERT(MA_OK == ma_message_set_payload(response_payload, variant_ptr));
        TEST_ASSERT(MA_OK == ma_msgbus_server_client_request_post_result(c_request, MA_OK, response_payload));
        TEST_ASSERT(MA_OK == ma_message_release(response_payload));
        TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));
    }

    // "Check Update Status" test
    if(test_scenario == 5)
    {
        TEST_ASSERT(MA_UPDATER_REQUEST_TYPE_STATUS_CHECK == params->request_type);
        TEST_ASSERT(MA_UPDATER_INITIATOR_TYPE_ONDEMAND_UPDATE == params->initiator_type);
        TEST_ASSERT(0 == strcmp(global_task_id, params->task_id));

        // Respond with MA_OK message and update state (payload) as MA_UPDATE_STATE_PRENOTIFY_FORCE
        TEST_ASSERT(MA_OK == ma_message_create(&response_payload));
        TEST_ASSERT(MA_OK == ma_variant_create_from_int16(MA_UPDATE_STATE_PRENOTIFY_FORCE, &variant_ptr));
        TEST_ASSERT(MA_OK == ma_message_set_payload(response_payload, variant_ptr));
        TEST_ASSERT(MA_OK == ma_msgbus_server_client_request_post_result(c_request, MA_OK, response_payload));
        TEST_ASSERT(MA_OK == ma_message_release(response_payload));
        TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));
    }

    // "Check Update Status" test
    if(test_scenario == 6)
    {
        TEST_ASSERT(MA_UPDATER_REQUEST_TYPE_STATUS_CHECK == params->request_type);
        TEST_ASSERT(MA_UPDATER_INITIATOR_TYPE_ONDEMAND_UPDATE == params->initiator_type);
        TEST_ASSERT(0 == strcmp(global_task_id, params->task_id));

        // Respond with MA_OK message and update state (payload) as MA_UPDATE_STATE_SUSPEND
        TEST_ASSERT(MA_OK == ma_message_create(&response_payload));
        TEST_ASSERT(MA_OK == ma_variant_create_from_int16(MA_UPDATE_STATE_SUSPEND, &variant_ptr));
        TEST_ASSERT(MA_OK == ma_message_set_payload(response_payload, variant_ptr));
        TEST_ASSERT(MA_OK == ma_msgbus_server_client_request_post_result(c_request, MA_OK, response_payload));
        TEST_ASSERT(MA_OK == ma_message_release(response_payload));
        TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));
    }

    // "Check Update Status" test
    if(test_scenario == 7)
    {
        TEST_ASSERT(MA_UPDATER_REQUEST_TYPE_STATUS_CHECK == params->request_type);
        TEST_ASSERT(MA_UPDATER_INITIATOR_TYPE_ONDEMAND_UPDATE == params->initiator_type);
        TEST_ASSERT(0 == strcmp(global_task_id, params->task_id));

        // Respond with MA_OK message and update state (payload) as MA_UPDATE_STATE_DOWNLOAD
        TEST_ASSERT(MA_OK == ma_message_create(&response_payload));
        TEST_ASSERT(MA_OK == ma_variant_create_from_int16(MA_UPDATE_STATE_DOWNLOAD, &variant_ptr));
        TEST_ASSERT(MA_OK == ma_message_set_payload(response_payload, variant_ptr));
        TEST_ASSERT(MA_OK == ma_msgbus_server_client_request_post_result(c_request, MA_OK, response_payload));
        TEST_ASSERT(MA_OK == ma_message_release(response_payload));
        TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));
    }

    ma_updater_request_param_release(params);
	params = NULL;

    return MA_OK;
}

/******************************************************************************
Call back function to be executed when the Scheduler Service receives requests
from the scheduler client. Updater Client uses the Scheduler client to schedule
the Update task.

This function verifies the update parameters (Task ID, Task Type, Locale and
Product List) passed to the Scheduler service while adding the update task.

In Test Scenario 1, this function returns "TASK_REPLY_STATUS_FAILED" in response,
simulating that the update task could not be added to scheduler.

In Test Scenario 2, this function returns "TASK_REPLY_STATUS_SUCCESS" in response,
simulating that the update task successfully added to scheduler.
******************************************************************************/
static ma_error_t scheduler_server_cb(ma_msgbus_server_t *server, ma_message_t *payload, void *cb_data, ma_msgbus_client_request_t *c_request)
{
    // Retrieve the Task ID from the message properties for verification
    {
        const char *string_val = NULL;

        TEST_ASSERT(MA_OK == ma_message_get_property(payload, "TASK_ID", &string_val));
        global_task_id = strdup(string_val); // Store Task ID in a global variable for later use (verification)
    }

    // Retrieve the Task Type from the message properties for verification
    {
        const char *string_val = NULL;

        TEST_ASSERT(MA_OK == ma_message_get_property(payload, "TASK_TYPE", &string_val));
        TEST_ASSERT(0 == strcmp("ma.updater.task.type.update", string_val));
    }

    // Retrieve the Locale and Product List from the message payload for verification
    {
        ma_variant_t *variant_ptr = NULL;
        ma_task_t *my_task = NULL;
        ma_int32_t my_locale = -100;
        ma_buffer_t *my_buffer = NULL;
        const char *my_string = NULL;
        size_t my_string_size = 0;

        // Creating task object and retrieving task from message payload
        TEST_ASSERT(MA_OK == ma_task_create(global_task_id, &my_task));
        TEST_ASSERT(MA_OK == ma_message_get_payload(payload, &variant_ptr));
        TEST_ASSERT(MA_OK == convert_variant_to_task(variant_ptr, my_task));
        TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

        // Verifying Locale
        TEST_ASSERT(MA_OK == ma_task_get_setting(my_task, "General", "Locale", &variant_ptr));
        TEST_ASSERT(MA_OK == ma_variant_get_int32(variant_ptr, &my_locale));
        TEST_ASSERT(-23630236 == my_locale);
        TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

        // Verifying Product List
        TEST_ASSERT(MA_OK == ma_task_get_setting(my_task, "SelectedUpdates", "NumOfUpdates", &variant_ptr));
        TEST_ASSERT(MA_OK == ma_variant_get_string_buffer(variant_ptr, &my_buffer));
        TEST_ASSERT(MA_OK == ma_buffer_get_string(my_buffer, &my_string, &my_string_size));
        TEST_ASSERT(0 == strcmp("4", my_string));
        TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
        TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

        TEST_ASSERT(MA_OK == ma_task_get_setting(my_task, "SelectedUpdates", "Update_1", &variant_ptr));
        TEST_ASSERT(MA_OK == ma_variant_get_string_buffer(variant_ptr, &my_buffer));
        TEST_ASSERT(MA_OK == ma_buffer_get_string(my_buffer, &my_string, &my_string_size));
        TEST_ASSERT(0 == strcmp("dfklhdklfhnsdklfndklsjfh", my_string));
        TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
        TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

        TEST_ASSERT(MA_OK == ma_task_get_setting(my_task, "SelectedUpdates", "Update_2", &variant_ptr));
        TEST_ASSERT(MA_OK == ma_variant_get_string_buffer(variant_ptr, &my_buffer));
        TEST_ASSERT(MA_OK == ma_buffer_get_string(my_buffer, &my_string, &my_string_size));
        TEST_ASSERT(0 == strcmp("lkh9078u34or8738nr rkl7r", my_string));
        TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
        TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

        TEST_ASSERT(MA_OK == ma_task_get_setting(my_task, "SelectedUpdates", "Update_3", &variant_ptr));
        TEST_ASSERT(MA_OK == ma_variant_get_string_buffer(variant_ptr, &my_buffer));
        TEST_ASSERT(MA_OK == ma_buffer_get_string(my_buffer, &my_string, &my_string_size));
        TEST_ASSERT(0 == strcmp("39246809234yh34i bio3y4oi3bvo32i4y8", my_string));
        TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
        TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

        TEST_ASSERT(MA_OK == ma_task_get_setting(my_task, "SelectedUpdates", "Update_4", &variant_ptr));
        TEST_ASSERT(MA_OK == ma_variant_get_string_buffer(variant_ptr, &my_buffer));
        TEST_ASSERT(MA_OK == ma_buffer_get_string(my_buffer, &my_string, &my_string_size));
        TEST_ASSERT(0 == strcmp("Product 4", my_string));
        TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
        TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

        // Releasing task
        TEST_ASSERT(MA_OK == ma_task_release(my_task));
    }

    // Respond with MA_OK message and task status (Success/Failed)
    {
        ma_message_t *response_payload = NULL;

        TEST_ASSERT(MA_OK == ma_message_create(&response_payload));

        if(test_scenario == 1)
        {
            TEST_ASSERT(MA_OK == ma_message_set_property(response_payload, "TASK_STATUS", "TASK_REPLY_STATUS_FAILED"));
        }

        if(test_scenario == 2)
        {
            TEST_ASSERT(MA_OK == ma_message_set_property(response_payload, "TASK_STATUS", "TASK_REPLY_STATUS_SUCCESS"));
        }

        TEST_ASSERT(MA_OK == ma_msgbus_server_client_request_post_result(c_request, MA_OK, response_payload));
        TEST_ASSERT(MA_OK == ma_message_release(response_payload));
    }

    return MA_OK;
}


/**************************************************************************
Updater Test for "Start Update", "Stop Update" and "Check Update Status"
**************************************************************************/
TEST(ma_updater_tests, updater_test)
{
    ma_updater_update_request_t *my_update_request = NULL;
    ma_buffer_t *session_id = NULL;
    ma_update_state_t state = MA_UPDATE_STATE_COMPARE_SITES;

    /**************************************************************************
    Registering callbacks
    **************************************************************************/
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_register_callbacks(NULL, product_id, &callbacks_bundle, NULL));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_register_callbacks(g_ma_client, product_id, NULL, NULL));
    TEST_ASSERT(MA_OK == ma_updater_register_callbacks(g_ma_client, product_id, &callbacks_bundle, NULL));

    // Trying to register again
    TEST_ASSERT(MA_ERROR_UPDATER_CLIENT_ALREADY_REGISTERED == ma_updater_register_callbacks(g_ma_client, product_id, &callbacks_bundle, NULL));

    /**************************************************************************
    TEST 1 - "Start Update" Test (Scenario - Update task could not be added to
             scheduler service successfully)
    **************************************************************************/
    test_scenario = 1;

    // Creating an update request, setting Product List, Locale and Initiator Type
    TEST_ASSERT(MA_OK == ma_updater_update_request_create(MA_UPDATER_UPDATE_TYPE_NORMAL_UPDATE, &my_update_request));
    TEST_ASSERT(MA_OK == ma_updater_update_request_set_locale(my_update_request, -23630236));
    TEST_ASSERT(MA_OK == ma_updater_update_request_set_initiator_type(my_update_request, MA_UPDATER_INITIATOR_TYPE_TASK_MIRROR));

    {
        ma_array_t *product_list = NULL;
        ma_variant_t *variant_ptr = NULL;

        // Creating product list
        TEST_ASSERT(MA_OK == ma_array_create(&product_list));
        TEST_ASSERT(MA_OK == ma_variant_create_from_string("dfklhdklfhnsdklfndklsjfh", strlen("dfklhdklfhnsdklfndklsjfh"), &variant_ptr)); // Product 1
        TEST_ASSERT(MA_OK == ma_array_push(product_list, variant_ptr));
        TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

        TEST_ASSERT(MA_OK == ma_variant_create_from_string("lkh9078u34or8738nr rkl7r", strlen("lkh9078u34or8738nr rkl7r"), &variant_ptr)); // Product 2
        TEST_ASSERT(MA_OK == ma_array_push(product_list, variant_ptr));
        TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

        TEST_ASSERT(MA_OK == ma_variant_create_from_string("39246809234yh34i bio3y4oi3bvo32i4y8", strlen("39246809234yh34i bio3y4oi3bvo32i4y8"), &variant_ptr)); // Product 3
        TEST_ASSERT(MA_OK == ma_array_push(product_list, variant_ptr));
        TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

        TEST_ASSERT(MA_OK == ma_variant_create_from_string("Product 4", strlen("Product 4"), &variant_ptr)); // Product 4
        TEST_ASSERT(MA_OK == ma_array_push(product_list, variant_ptr));
        TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

        // Setting product list into the updater update request object.
        TEST_ASSERT(MA_OK == ma_updater_update_request_set_product_list(my_update_request, product_list));
        TEST_ASSERT(MA_OK == ma_array_release(product_list));
        product_list = NULL;
    }

    // Starting Update
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_start_update(NULL, my_update_request, &session_id));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_start_update(g_ma_client, NULL, &session_id));
    TEST_ASSERT(MA_ERROR_APIFAILED == ma_updater_start_update(g_ma_client, my_update_request, &session_id));

    // Not releasing update request. It is being used in Test 2.

    /**************************************************************************
    TEST 2 - "Start Update" Test (Updater started successfully)
    **************************************************************************/
    test_scenario = 2;

    // Using the "Updater Request" object from Test 1

    // Starting Update
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_start_update(NULL, my_update_request, &session_id));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_start_update(g_ma_client, NULL, &session_id));
    TEST_ASSERT(MA_OK == ma_updater_start_update(g_ma_client, my_update_request, &session_id));

    // Verify that correct Session ID is returned
    {
        const char *my_string = NULL;
        size_t my_string_size = 0;
        TEST_ASSERT(MA_OK == ma_buffer_get_string(session_id, &my_string, &my_string_size));
        TEST_ASSERT(0 == strcmp(global_task_id, my_string));
    }

    // Releasing update request
    TEST_ASSERT(MA_OK == ma_updater_update_request_release(my_update_request));

    /**************************************************************************
    TEST 3 - "Stop Update" Test
    **************************************************************************/
    test_scenario = 3;

    // Stop Update (using Session ID buffer from previous Test 1)
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_stop_update(NULL, session_id));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_stop_update(g_ma_client, NULL));
    TEST_ASSERT(MA_OK == ma_updater_stop_update(g_ma_client, session_id));

    /**************************************************************************
    TEST 4 - "Check Status" Test
    **************************************************************************/
    test_scenario = 4;

    // Update Status Check (using Session ID buffer from previous Test 1)
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_get_update_state(NULL, session_id, &state));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_get_update_state(g_ma_client, NULL, &state));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_get_update_state(g_ma_client, session_id, NULL));
    TEST_ASSERT(MA_OK == ma_updater_get_update_state(g_ma_client, session_id, &state));
    TEST_ASSERT(MA_UPDATE_STATE_BACKUP == state);

    /**************************************************************************
    TEST 5 - "Check Status" Test
    **************************************************************************/
    test_scenario = 5;

    // Update Status Check (using Session ID buffer from previous Test 1)
    TEST_ASSERT(MA_OK == ma_updater_get_update_state(g_ma_client, session_id, &state));
    TEST_ASSERT(MA_UPDATE_STATE_PRENOTIFY_FORCE == state);

    /**************************************************************************
    TEST 6 - "Check Status" Test
    **************************************************************************/
    test_scenario = 6;

    // Update Status Check (using Session ID buffer from previous Test 1)
    TEST_ASSERT(MA_OK == ma_updater_get_update_state(g_ma_client, session_id, &state));
    TEST_ASSERT(MA_UPDATE_STATE_SUSPEND == state);

    /**************************************************************************
    TEST 7 - "Check Status" Test
    ***************************************************************************/
    test_scenario = 7;

    // Update Status Check (using Session ID buffer from previous Test 1)
    TEST_ASSERT(MA_OK == ma_updater_get_update_state(g_ma_client, session_id, &state));
    TEST_ASSERT(MA_UPDATE_STATE_DOWNLOAD == state);

    /**************************************************************************
    Un-registering callbacks
    ***************************************************************************/
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_unregister_callbacks(NULL, product_id));
    TEST_ASSERT(MA_OK == ma_updater_unregister_callbacks(g_ma_client, product_id));

    // Trying to un-register once the it is already un-registered.
    TEST_ASSERT(MA_ERROR_UPDATER_CLIENT_NOT_REGISTERED == ma_updater_unregister_callbacks(g_ma_client, product_id));

    // Release Session ID buffer and Global task ID string
    TEST_ASSERT(MA_OK == ma_buffer_release(session_id));
    free(global_task_id);
}


