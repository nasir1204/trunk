/*
Updater Client - Updater update request UTs
*/

#include "unity.h"
#include "unity_fixture.h"
#include "unity_internals.h"
#include "ma/updater/ma_updater_update_request.h"
#include <string.h>

TEST_GROUP_RUNNER(ma_updater_update_request_test_group)
{
    do {RUN_TEST_CASE(ma_updater_update_request_tests, updater_update_request_test)} while (0);
}

TEST_SETUP(ma_updater_update_request_tests)
{

}

TEST_TEAR_DOWN(ma_updater_update_request_tests)
{

}


/**************************************************************************
creating the updater sync request to and from variant
**************************************************************************/
TEST(ma_updater_update_request_tests, updater_update_request_test)
{
    ma_updater_update_request_t *request = NULL;
    ma_updater_update_type_t update_type = MA_UPDATER_UPDATE_TYPE_ROLLBACK_UPDATE;
    ma_uint32_t locale = 0;
    ma_updater_initiator_type_t type = MA_UPDATER_INITIATOR_TYPE_REMEDIATION_UPDATE;

    {
        // ma_updater_update_request_create API
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_update_request_create(MA_UPDATER_UPDATE_TYPE_NORMAL_UPDATE, NULL));
        TEST_ASSERT(MA_OK == ma_updater_update_request_create(MA_UPDATER_UPDATE_TYPE_NORMAL_UPDATE, &request));

        // ma_updater_update_request_get_type API
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_update_request_get_type(NULL, &update_type));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_update_request_get_type(request, NULL));
        TEST_ASSERT(MA_OK == ma_updater_update_request_get_type(request, &update_type));
        TEST_ASSERT(update_type == MA_UPDATER_UPDATE_TYPE_NORMAL_UPDATE);

        // ma_updater_update_request_release API
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_update_request_release(NULL));
        TEST_ASSERT(MA_OK == ma_updater_update_request_release(request));
        request = NULL;
    }

    {
        // ma_updater_update_request_create API
        TEST_ASSERT(MA_OK == ma_updater_update_request_create(MA_UPDATER_UPDATE_TYPE_ROLLBACK_UPDATE, &request));

        // ma_updater_update_request_get_type API
        TEST_ASSERT(MA_OK == ma_updater_update_request_get_type(request, &update_type));
        TEST_ASSERT(update_type == MA_UPDATER_UPDATE_TYPE_ROLLBACK_UPDATE);

        // ma_updater_update_request_set_locale and ma_updater_update_request_get_locale APIs
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_update_request_set_locale(NULL, 2003));
        TEST_ASSERT(MA_OK == ma_updater_update_request_set_locale(request, 2003));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_update_request_get_locale(NULL, &locale));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_update_request_get_locale(request, NULL));
        TEST_ASSERT(MA_OK == ma_updater_update_request_get_locale(request, &locale));
        TEST_ASSERT(locale == 2003);

        TEST_ASSERT(MA_OK == ma_updater_update_request_set_locale(request, 12345678));
        TEST_ASSERT(MA_OK == ma_updater_update_request_get_locale(request, &locale));
        TEST_ASSERT(locale == 12345678);

        // ma_updater_update_request_set_initiator_type and ma_updater_update_request_get_initiator_type APIs
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_update_request_set_initiator_type(NULL, MA_UPDATER_INITIATOR_TYPE_TASK_MIRROR));
        TEST_ASSERT(MA_OK == ma_updater_update_request_set_initiator_type(request, MA_UPDATER_INITIATOR_TYPE_TASK_MIRROR));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_update_request_get_initiator_type(NULL, &type));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_update_request_get_initiator_type(request, NULL));
        TEST_ASSERT(MA_OK == ma_updater_update_request_get_initiator_type(request, &type));
        TEST_ASSERT(MA_UPDATER_INITIATOR_TYPE_TASK_MIRROR == type);

        TEST_ASSERT(MA_OK == ma_updater_update_request_set_initiator_type(request, MA_UPDATER_INITIATOR_TYPE_TASK_DEPLOYMENT));
        TEST_ASSERT(MA_OK == ma_updater_update_request_get_initiator_type(request, &type));
        TEST_ASSERT(MA_UPDATER_INITIATOR_TYPE_TASK_DEPLOYMENT == type);


        // ma_updater_update_request_set_product_list and ma_updater_update_request_get_product_list APIs
        {
            ma_array_t *product_list = NULL;
            ma_variant_t *variant_ptr = NULL;
            ma_buffer_t *my_buffer = NULL;
            const char *my_string = NULL;
            size_t my_string_size = 0;

            // Creating product list
            TEST_ASSERT(MA_OK == ma_array_create(&product_list));
            TEST_ASSERT(MA_OK == ma_variant_create_from_string("dfklhdklfhnsdklfndklsjfh", strlen("dfklhdklfhnsdklfndklsjfh"), &variant_ptr)); // Product 1
            TEST_ASSERT(MA_OK == ma_array_push(product_list, variant_ptr));
            TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

            TEST_ASSERT(MA_OK == ma_variant_create_from_string("lkh9078u34or8738nr rkl7r", strlen("lkh9078u34or8738nr rkl7r"), &variant_ptr)); // Product 2
            TEST_ASSERT(MA_OK == ma_array_push(product_list, variant_ptr));
            TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

            TEST_ASSERT(MA_OK == ma_variant_create_from_string("39246809234yh34i bio3y4oi3bvo32i4y8", strlen("39246809234yh34i bio3y4oi3bvo32i4y8"), &variant_ptr)); // Product 3
            TEST_ASSERT(MA_OK == ma_array_push(product_list, variant_ptr));
            TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

            TEST_ASSERT(MA_OK == ma_variant_create_from_string("Product 4", strlen("Product 4"), &variant_ptr)); // Product 4
            TEST_ASSERT(MA_OK == ma_array_push(product_list, variant_ptr));
            TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

            // Setting product list into the updater update request object.
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_update_request_set_product_list(NULL, product_list));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_update_request_set_product_list(request, NULL));
            TEST_ASSERT(MA_OK == ma_updater_update_request_set_product_list(request, product_list));
            TEST_ASSERT(MA_OK == ma_array_release(product_list));
            product_list = NULL;

            // Retrieving product list from the update request object.
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_update_request_get_product_list(NULL, &product_list));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_update_request_get_product_list(request, NULL));
            TEST_ASSERT(MA_OK == ma_updater_update_request_get_product_list(request, &product_list));

            // Verifying the products in the product list
            TEST_ASSERT(MA_OK == ma_array_get_element_at(product_list, 1, &variant_ptr));
            TEST_ASSERT(MA_OK == ma_variant_get_string_buffer(variant_ptr, &my_buffer));
            TEST_ASSERT(MA_OK == ma_buffer_get_string(my_buffer, &my_string, &my_string_size));
            TEST_ASSERT(0 == strcmp(my_string, "dfklhdklfhnsdklfndklsjfh"));
            TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
            TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

            TEST_ASSERT(MA_OK == ma_array_get_element_at(product_list, 2, &variant_ptr));
            TEST_ASSERT(MA_OK == ma_variant_get_string_buffer(variant_ptr, &my_buffer));
            TEST_ASSERT(MA_OK == ma_buffer_get_string(my_buffer, &my_string, &my_string_size));
            TEST_ASSERT(0 == strcmp(my_string, "lkh9078u34or8738nr rkl7r"));
            TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
            TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

            TEST_ASSERT(MA_OK == ma_array_get_element_at(product_list, 3, &variant_ptr));
            TEST_ASSERT(MA_OK == ma_variant_get_string_buffer(variant_ptr, &my_buffer));
            TEST_ASSERT(MA_OK == ma_buffer_get_string(my_buffer, &my_string, &my_string_size));
            TEST_ASSERT(0 == strcmp(my_string, "39246809234yh34i bio3y4oi3bvo32i4y8"));
            TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
            TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

            TEST_ASSERT(MA_OK == ma_array_get_element_at(product_list, 4, &variant_ptr));
            TEST_ASSERT(MA_OK == ma_variant_get_string_buffer(variant_ptr, &my_buffer));
            TEST_ASSERT(MA_OK == ma_buffer_get_string(my_buffer, &my_string, &my_string_size));
            TEST_ASSERT(0 == strcmp(my_string, "Product 4"));
            TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
            TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

            TEST_ASSERT(MA_OK == ma_array_release(product_list));
            product_list = NULL;
        }

        // ma_updater_update_request_release API
        TEST_ASSERT(MA_OK == ma_updater_update_request_release(request));
    }

}


