#include "unity.h"
#include "unity_fixture.h"
#include "unity_internals.h"
#include "ma/internal/clients/updater/ma_update_info_sync.h"
#include <string.h>
#if 0
TEST_GROUP_RUNNER(ma_updater_sync_request_test_group)
{
    do {RUN_TEST_CASE(ma_updater_sync_request_tests, updater_sync_request_variant_test)} while (0);
}

TEST_SETUP(ma_updater_sync_request_tests)
{
    
}

TEST_TEAR_DOWN(ma_updater_sync_request_tests)
{
    
}


/**************************************************************************
creating the updater sync request to and from variant
**************************************************************************/
TEST(ma_updater_sync_request_tests, updater_sync_request_variant_test)
{
    ma_update_info_sync_t *request = NULL;
    ma_updater_product_return_code_t set_return_code = MA_UPDATER_PRODUCT_RETURN_CODE_ACCEPT, get_return_code = MA_UPDATER_PRODUCT_RETURN_CODE_FAIL;
	ma_update_info_sync_type_t set_request_type = MA_UPDATE_INFO_SYNC_TYPE_SET, get_request_type = MA_UPDATE_INFO_SYNC_TYPE_GET ;
    ma_variant_t *variant = NULL;
    const char *set_software_id = "EPOAGENT3000", *get_software_id = NULL;
    const char *set_key = "DatVersion", *get_key = NULL;
    ma_uint32_t set_value = 1356, get_value = 0;


    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_sync_request_create(NULL));
    TEST_ASSERT(MA_OK == ma_updater_sync_request_create(&request));


    /* set some parameters */
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_sync_request_set_type(NULL, set_request_type));
	TEST_ASSERT(MA_OK == ma_updater_sync_request_set_type(request, set_request_type));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_sync_request_set_return_code(NULL, set_return_code));
    TEST_ASSERT(MA_OK == ma_updater_sync_request_set_return_code(request, set_return_code));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_sync_request_set_software_id(NULL, NULL));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_sync_request_set_software_id(request, NULL));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_sync_request_set_software_id(NULL, set_software_id));
    TEST_ASSERT(MA_OK == ma_updater_sync_request_set_software_id(request, set_software_id));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_sync_request_set_key(NULL, NULL));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_sync_request_set_key(NULL, set_key));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_sync_request_set_key(request, NULL));
    TEST_ASSERT(MA_OK == ma_updater_sync_request_set_key(request, set_key));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_variant_create_from_uint32(NULL, NULL));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_variant_create_from_uint32(set_value, NULL));
    TEST_ASSERT(MA_OK == ma_variant_create_from_uint32(set_value, &variant));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_sync_request_set_value(NULL, NULL));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_sync_request_set_value(request, NULL));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_sync_request_set_value(NULL, variant));
    TEST_ASSERT(MA_OK == ma_updater_sync_request_set_value(request, variant));
    ma_variant_release(variant);

    /* convert into variant */
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_sync_request_to_variant(NULL, NULL));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_sync_request_to_variant(NULL, &variant));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_sync_request_to_variant(request, NULL));
    TEST_ASSERT(MA_OK == ma_updater_sync_request_to_variant(request, &variant));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_sync_request_release(NULL));
    TEST_ASSERT(MA_OK == ma_updater_sync_request_release(request));

    /* create from variant */
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_sync_request_from_variant(NULL, NULL));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_sync_request_from_variant(NULL, &request));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_sync_request_from_variant(variant, NULL));
    TEST_ASSERT(MA_OK == ma_updater_sync_request_from_variant(variant, &request));

    ma_variant_release(variant);
	printf("nhayyal6:\n");
    /* get the params */
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_sync_request_get_type(NULL, NULL));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_sync_request_get_type(NULL, &get_request_type));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_sync_request_get_type(request, NULL));
    TEST_ASSERT(MA_OK == ma_updater_sync_request_get_type(request, &get_request_type));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_sync_request_get_return_code(NULL, NULL));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_sync_request_get_return_code(request, NULL));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_sync_request_get_return_code(NULL, &get_return_code));
    TEST_ASSERT(MA_OK == ma_updater_sync_request_get_return_code(request, &get_return_code));
    
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_sync_request_get_software_id(NULL, NULL));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_sync_request_get_software_id(request, NULL));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_sync_request_get_software_id(NULL, &get_software_id));
    TEST_ASSERT(MA_OK == ma_updater_sync_request_get_software_id(request, &get_software_id));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_sync_request_get_key(NULL, NULL));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_sync_request_get_key(request, NULL));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_sync_request_get_key(NULL, &get_key));
    TEST_ASSERT(MA_OK == ma_updater_sync_request_get_key(request, &get_key));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_sync_request_get_value(NULL, NULL));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_sync_request_get_value(NULL, &variant));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_sync_request_get_value(request, NULL));
    TEST_ASSERT(MA_OK == ma_updater_sync_request_get_value(request, &variant));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_variant_get_uint32(NULL, NULL));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_variant_get_uint32(NULL, &get_value));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_variant_get_uint32(variant, NULL));
    TEST_ASSERT(MA_OK == ma_variant_get_uint32(variant, &get_value));
    ma_variant_release(variant);

    /* compare set and get */
    TEST_ASSERT_EQUAL_INT(set_request_type,get_request_type);
    TEST_ASSERT_EQUAL_INT(set_return_code,get_return_code);
    TEST_ASSERT_EQUAL_INT(set_value,get_value);

    TEST_ASSERT_EQUAL_STRING(set_software_id,get_software_id);
    TEST_ASSERT_EQUAL_STRING(set_key, get_key);
    

    ma_update_info_sync_release(request);

}
#endif