/*
Updater Client - Updater UI Provider UTs
*/

#include "unity.h"
#include "unity_fixture.h"
#include "unity_internals.h"
#include "ma/updater/ma_updater_ui_provider.h"
#include "ma_ut_setup.h"
#include <string.h>
#include <stdlib.h>

#ifdef MA_WINDOWS
#include <Windows.h>
#endif

static ma_client_t *g_ma_client = NULL;
static ma_msgbus_t *g_msgbus = NULL;
static ma_ut_setup_t *g_ut_setup = NULL;

static const char product_id[] = "ma.ui.client.test";

// Updater service object (message bus server)
static ma_msgbus_server_t *updater_server = NULL;

// Indicates the test scenario currently executing
static ma_uint8_t test_scenario = 0;

// To store the expected name of Show UI server
char expected_show_ui_server_name[100];

// Declaration for callback functions
static ma_error_t updater_server_cb(ma_msgbus_server_t *server, ma_message_t *request_message, void *cb_data, ma_msgbus_client_request_t *c_request);

/**************************************************************************
Function to retrieve the Session ID using Windows APIs
**************************************************************************/
static unsigned long get_session_id()
{
    #ifdef MA_WINDOWS
        DWORD session_id = 0 ;
        ProcessIdToSessionId(GetCurrentProcessId(), &session_id) ;
        return (unsigned long)session_id ;
    #else
        return 0 ;
    #endif
}

TEST_GROUP_RUNNER(ma_updater_ui_provider_test_group)
{
    do {RUN_TEST_CASE(ma_updater_ui_provider_tests, ui_provider_register_unregister_test)} while (0);
    do {RUN_TEST_CASE(ma_updater_ui_provider_tests, show_UI_postpone_request_response_test)} while (0);
    do {RUN_TEST_CASE(ma_updater_ui_provider_tests, show_UI_reboot_request_response_test)} while (0);
}


/**************************************************************************
1) Creating the unit test context.
2) Retrieving underlying message bus instance.
3) Creating and starting message bus services (Updater Service) using the
   message bus instance.
**************************************************************************/
TEST_SETUP(ma_updater_ui_provider_tests)
{
    ma_ut_setup_create("updater.ui_provider.tests", product_id, &g_ut_setup);
    g_ma_client = ma_ut_setup_get_client(g_ut_setup);
    g_msgbus = ma_ut_setup_get_msgbus(g_ut_setup);
    ma_msgbus_server_create(g_msgbus, "ma.service.updater", &updater_server);
    ma_msgbus_server_start(updater_server, updater_server_cb, NULL);
}

/**************************************************************************
1) Stopping and releasing the message bus service (Updater service).
2) Releasing the context.
**************************************************************************/
TEST_TEAR_DOWN(ma_updater_ui_provider_tests)
{
    ma_msgbus_server_stop(updater_server);
    ma_msgbus_server_release(updater_server);
    ma_ut_setup_release(g_ut_setup);
}

/**************************************************************************
Show UI Callback Function. This function performs following tasks -

1) Verifies the parameters UI Type, Initiator Type, Title, Message etc.
   which are part of the Show UI request.
2) Sends response message with known values of return code, postpone timeout,
   or reboot timeout parameters.

TEST Scenario 4 : UI Type is MA_UPDATER_UI_TYPE_POSTPONE
TEST Scenario 5 : UI Type is MA_UPDATER_UI_TYPE_REBOOT
**************************************************************************/
ma_error_t on_show_ui_callback(ma_client_t *ma_client, void *cb_data,  const char *session_id, ma_updater_ui_type_t type, const ma_updater_show_ui_request_t *request, ma_updater_show_ui_response_t *response)
{
    if(test_scenario == 4)
    {
        // Verifying various fields in Show UI request
        {
            ma_uint32_t int_val = 0;
            const char *string_val = NULL;

            // "UI Type" field
            TEST_ASSERT(MA_UPDATER_UI_TYPE_POSTPONE == type);

            // "Initiator Type" field
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_show_ui_request_get_initiator_type(NULL, &int_val));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_show_ui_request_get_initiator_type(request, NULL));
            TEST_ASSERT(MA_OK == ma_updater_show_ui_request_get_initiator_type(request, &int_val));
            TEST_ASSERT(12 == int_val);

            // "Title" field
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_show_ui_request_get_title(NULL, &string_val));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_show_ui_request_get_title(request, NULL));
            TEST_ASSERT(MA_OK == ma_updater_show_ui_request_get_title(request, &string_val));
            TEST_ASSERT(0 == strcmp("Postpone Menu", string_val));

            // "Message" field
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_show_ui_request_get_message(NULL, &string_val));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_show_ui_request_get_message(request, NULL));
            TEST_ASSERT(MA_OK == ma_updater_show_ui_request_get_message(request, &string_val));
            TEST_ASSERT(0 == strcmp("DAT download in-progress", string_val));

            // "Countdown Message" field
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_show_ui_request_get_count_down_message(NULL, &string_val));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_show_ui_request_get_count_down_message(request, NULL));
            TEST_ASSERT(MA_OK == ma_updater_show_ui_request_get_count_down_message(request, &string_val));
            TEST_ASSERT(0 == strcmp("DAT download 90% complete", string_val));

            // "Countdown Value" field
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_show_ui_request_get_count_down_value(NULL, &int_val));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_show_ui_request_get_count_down_value(request, NULL));
            TEST_ASSERT(MA_OK == ma_updater_show_ui_request_get_count_down_value(request, &int_val));
            TEST_ASSERT(90 == int_val);
        }

        // Fill the passed Response object with the known response values and Post Response
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_show_ui_response_set_return_code(NULL, MA_UPDATER_UI_RC_POSTPONE_NOW));
        TEST_ASSERT(MA_OK == ma_updater_show_ui_response_set_return_code(response, MA_UPDATER_UI_RC_POSTPONE_NOW));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_show_ui_response_set_postpone_timeout(NULL, 123));
        TEST_ASSERT(MA_OK == ma_updater_show_ui_response_set_postpone_timeout(response, 123));

        // This value should be ignored in response
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_show_ui_response_set_reboot_timeout(NULL, 5500));
        TEST_ASSERT(MA_OK == ma_updater_show_ui_response_set_reboot_timeout(response, 5500));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_show_ui_response_post(NULL, response));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_show_ui_response_post(g_ma_client, NULL));
        TEST_ASSERT(MA_OK == ma_updater_show_ui_response_post(g_ma_client, response));
    }

    if(test_scenario == 5)
    {
        // Verifying various fields in Show UI request
        {
            ma_uint32_t int_val = 0;
            const char *string_val = NULL;

            // "UI Type" field
            TEST_ASSERT(MA_UPDATER_UI_TYPE_REBOOT == type);

            // "Initiator Type" field
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_show_ui_request_get_initiator_type(NULL, &int_val));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_show_ui_request_get_initiator_type(request, NULL));
            TEST_ASSERT(MA_OK == ma_updater_show_ui_request_get_initiator_type(request, &int_val));
            TEST_ASSERT(34 == int_val);

            // "Title" field
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_show_ui_request_get_title(NULL, &string_val));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_show_ui_request_get_title(request, NULL));
            TEST_ASSERT(MA_OK == ma_updater_show_ui_request_get_title(request, &string_val));
            TEST_ASSERT(0 == strcmp("Reboot Menu", string_val));

            // "Message" field
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_show_ui_request_get_message(NULL, &string_val));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_show_ui_request_get_message(request, NULL));
            TEST_ASSERT(MA_OK == ma_updater_show_ui_request_get_message(request, &string_val));
            TEST_ASSERT(0 == strcmp("Rebooting in 5 minutes", string_val));

            // "Countdown Message" field
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_show_ui_request_get_count_down_message(NULL, &string_val));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_show_ui_request_get_count_down_message(request, NULL));
            TEST_ASSERT(MA_OK == ma_updater_show_ui_request_get_count_down_message(request, &string_val));
            TEST_ASSERT(0 == strcmp("Rebooting after updates", string_val));

            // "Countdown Value" field
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_show_ui_request_get_count_down_value(NULL, &int_val));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_show_ui_request_get_count_down_value(request, NULL));
            TEST_ASSERT(MA_OK == ma_updater_show_ui_request_get_count_down_value(request, &int_val));
            TEST_ASSERT(300 == int_val);
        }

        // Fill the passed Response object with the known response values and Post Response
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_show_ui_response_set_return_code(NULL, MA_UPDATER_UI_RC_REBOOT_TIMEOUT));
        TEST_ASSERT(MA_OK == ma_updater_show_ui_response_set_return_code(response, MA_UPDATER_UI_RC_REBOOT_TIMEOUT));

        // This value should be ignored in response
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_show_ui_response_set_postpone_timeout(NULL, 789));
        TEST_ASSERT(MA_OK == ma_updater_show_ui_response_set_postpone_timeout(response, 789));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_show_ui_response_set_reboot_timeout(NULL, 32345));
        TEST_ASSERT(MA_OK == ma_updater_show_ui_response_set_reboot_timeout(response, 32345));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_show_ui_response_post(NULL, response));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_show_ui_response_post(g_ma_client, NULL));
        TEST_ASSERT(MA_OK == ma_updater_show_ui_response_post(g_ma_client, response));
    }

    return MA_OK;
}

ma_error_t on_progress_callback(ma_client_t *ma_client, void *cb_data, const char *session_id, ma_int32_t event_type, ma_int32_t progress_step , ma_int32_t max_progress_step, const char *progress_message)
{
    printf("On progress UI callback\n");
    return MA_OK;
}

ma_error_t on_end_ui_callback(ma_client_t *ma_client, void *cb_data, const char *session_id, const char *title, const char *end_message, const char *count_down_message, ma_int64_t count_down_value)
{
    printf("On end UI callback\n");
    return MA_OK;
}

ma_error_t on_update_event_callback(ma_client_t *ma_client, void *cb_data, const char *session_id, const ma_updater_event_t *event_info)
{
    printf("On update event callback\n");
    return MA_OK;
}

static struct ma_updater_ui_provider_callbacks_s const callbacks_bundle = { on_show_ui_callback, on_progress_callback, on_end_ui_callback, on_update_event_callback };


/******************************************************************************
Call back function to be executed when the Updater Service receives requests
from the updater client.

This function verifies the update parameters (Update Request Type, Software ID
and Show UI Provider Server Name) sent to the updater service in each scenario.
In response, it either returns MA_OK (successful) or failure response to simulate
various scenarios.

TEST Scenario 1 : Callbacks registration unsuccessful
TEST Scenario 2 : Callbacks registration successful
TEST Scenario 3 : Callbacks unregistration successful
******************************************************************************/
static ma_error_t updater_server_cb(ma_msgbus_server_t *server, ma_message_t *payload, void *cb_data, ma_msgbus_client_request_t *c_request)
{
    const char *string_val = NULL;
    ma_message_t *response_payload = NULL;

    // Callbacks registration unsuccessful
    if(test_scenario == 1)
    {
        TEST_ASSERT(MA_OK == ma_message_get_property(payload, "prop.key.request_type", &string_val));
        TEST_ASSERT(0 == strcmp("prop.val.rqst.register_uiprovider", string_val));

        TEST_ASSERT(MA_OK == ma_message_get_property(payload, "prop.key.software_id", &string_val));
        TEST_ASSERT(0 == strcmp(product_id, string_val));

        TEST_ASSERT(MA_OK == ma_message_get_property(payload, "prop.key.server_name", &string_val));
        TEST_ASSERT(0 == strcmp(expected_show_ui_server_name, string_val));

        // Respond with error message (response code = MA_ERROR_UPDATER_UI_PROVIDER_NOT_REGISTERED)
        TEST_ASSERT(MA_OK == ma_message_create(&response_payload));
        TEST_ASSERT(MA_OK == ma_msgbus_server_client_request_post_result(c_request, MA_ERROR_UPDATER_UI_PROVIDER_NOT_REGISTERED, response_payload));
        TEST_ASSERT(MA_OK == ma_message_release(response_payload));
    }

    if(test_scenario == 2)
    {
        TEST_ASSERT(MA_OK == ma_message_get_property(payload, "prop.key.request_type", &string_val));
        TEST_ASSERT(0 == strcmp("prop.val.rqst.register_uiprovider", string_val));

        TEST_ASSERT(MA_OK == ma_message_get_property(payload, "prop.key.software_id", &string_val));
        TEST_ASSERT(0 == strcmp(product_id, string_val));

        TEST_ASSERT(MA_OK == ma_message_get_property(payload, "prop.key.server_name", &string_val));
        TEST_ASSERT(0 == strcmp(expected_show_ui_server_name, string_val));

        // Respond with MA_OK message (response code = MA_OK)
        TEST_ASSERT(MA_OK == ma_message_create(&response_payload));
        TEST_ASSERT(MA_OK == ma_msgbus_server_client_request_post_result(c_request, MA_OK, response_payload));
        TEST_ASSERT(MA_OK == ma_message_release(response_payload));
    }

    if(test_scenario == 3)
    {
        TEST_ASSERT(MA_OK == ma_message_get_property(payload, "prop.key.request_type", &string_val));
        TEST_ASSERT(0 == strcmp("prop.val.rqst.unregister_uiprovider", string_val));

        TEST_ASSERT(MA_OK == ma_message_get_property(payload, "prop.key.software_id", &string_val));
        TEST_ASSERT(0 == strcmp(product_id, string_val));

        TEST_ASSERT(MA_OK == ma_message_get_property(payload, "prop.key.server_name", &string_val));
        TEST_ASSERT(0 == strcmp(expected_show_ui_server_name, string_val));

        // Respond with MA_OK message (response code = MA_OK)
        TEST_ASSERT(MA_OK == ma_message_create(&response_payload));
        TEST_ASSERT(MA_OK == ma_msgbus_server_client_request_post_result(c_request, MA_OK, response_payload));
        TEST_ASSERT(MA_OK == ma_message_release(response_payload));
    }

    return MA_OK;
}

/**************************************************************************
UI Provider Callbacks Registration/Unregistration tests
**************************************************************************/
TEST(ma_updater_ui_provider_tests, ui_provider_register_unregister_test)
{
    //// Expected Server Name for internal Show UI service is "ma.service.updater.uiprovider" + Product ID + Session ID
    //sprintf(expected_show_ui_server_name, "%s.%s.%lu", "ma.service.updater.uiprovider", product_id, get_session_id());

    ///**************************************************************************
    //Verifying UI provider Show UI Service name is correctly returned by API.
    //The UI provider Client and show UI service is not created, therefore, this
    //API should return MA_ERROR_UPDATER_UI_PROVIDER_NOT_REGISTERED
    //**************************************************************************/
    //{
    //    const char *ui_provider_name = NULL;

    //    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_get_ui_provider_name(NULL, &ui_provider_name));
    //    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_get_ui_provider_name(g_ma_client, NULL));
    //    TEST_ASSERT(MA_ERROR_UPDATER_UI_PROVIDER_NOT_REGISTERED == ma_updater_get_ui_provider_name(g_ma_client, &ui_provider_name));
    //    TEST_ASSERT_NULL(ui_provider_name);
    //}

    ///**************************************************************************
    //Registering callbacks (UI Provider Client could not be registered)
    //**************************************************************************/
    //test_scenario = 1; // Simulate unsuccessful registration

    //TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_register_ui_provider_callbacks(NULL, MA_MSGBUS_CALLBACK_THREAD_POOL, &callbacks_bundle, NULL));
    //TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_register_ui_provider_callbacks(g_ma_client, MA_MSGBUS_CALLBACK_THREAD_POOL, NULL, NULL));
    //TEST_ASSERT(MA_ERROR_UPDATER_UI_PROVIDER_NOT_REGISTERED == ma_updater_register_ui_provider_callbacks(g_ma_client, MA_MSGBUS_CALLBACK_THREAD_POOL, &callbacks_bundle, NULL));

    ///**************************************************************************
    //Registering callbacks (UI Provider Client successfully registered)
    //**************************************************************************/
    //test_scenario = 2; // Simulate successful registration

    //TEST_ASSERT(MA_OK == ma_updater_register_ui_provider_callbacks(g_ma_client, MA_MSGBUS_CALLBACK_THREAD_POOL, &callbacks_bundle, NULL));

    ///**************************************************************************
    //Registering callbacks (Callbacks already registered once)
    //**************************************************************************/
    //test_scenario = 2; // Simulate successful registration

    //// Trying to register again
    //TEST_ASSERT(MA_ERROR_UPDATER_UI_PROVIDER_ALREADY_REGISTERED == ma_updater_register_ui_provider_callbacks(g_ma_client, MA_MSGBUS_CALLBACK_THREAD_POOL, &callbacks_bundle, NULL));

    ///**************************************************************************
    //Verifying UI provider Show UI Service name is correctly returned by API
    //**************************************************************************/
    //{
    //    const char *ui_provider_name = NULL;

    //    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_get_ui_provider_name(NULL, &ui_provider_name));
    //    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_get_ui_provider_name(g_ma_client, NULL));
    //    TEST_ASSERT(MA_OK == ma_updater_get_ui_provider_name(g_ma_client, &ui_provider_name));
    //    TEST_ASSERT(0 == strcmp(expected_show_ui_server_name, ui_provider_name));
    //}

    ///**************************************************************************
    //Un-registering callbacks (UI Provider Client unregistration failed)
    //***************************************************************************/
    //// This scenario is not verified. The ma_updater_ui_provider_client_release
    //// API returns MA_OK even if unregistration fails.

    ///**************************************************************************
    //Un-registering callbacks (UI Provider Client unregistration successful)
    //***************************************************************************/
    //test_scenario = 3; // Simulate successful unregistration

    //TEST_ASSERT(MA_OK == ma_updater_unregister_ui_provider_callbacks(g_ma_client));

    ///**************************************************************************
    //Un-registering callbacks (Callbacks already unregistered)
    //***************************************************************************/
    //test_scenario = 3; // Simulate successful unregistration

    //TEST_ASSERT(MA_ERROR_UPDATER_UI_PROVIDER_NOT_REGISTERED == ma_updater_unregister_ui_provider_callbacks(g_ma_client));
}


/**************************************************************************
UI Provider - Show Postpone UI request/response tests
**************************************************************************/
TEST(ma_updater_ui_provider_tests, show_UI_postpone_request_response_test)
{
    //ma_array_t *request_array = NULL;
    //ma_variant_t *variant_ptr = NULL;
    //ma_message_t *request_message = NULL;
    //ma_message_t *response_message = NULL;
    //ma_msgbus_endpoint_t *show_ui_client = NULL;
    //ma_msgbus_request_t *request_packet = NULL;

    //// Expected Server Name for internal Show UI service is "ma.service.updater.uiprovider" + Product ID + Session ID
    //sprintf(expected_show_ui_server_name, "%s.%s.%lu", "ma.service.updater.uiprovider", product_id, get_session_id());

    ///**************************************************************************
    //Registering callbacks
    //**************************************************************************/
    //test_scenario = 2; // Simulate successful registration

    //TEST_ASSERT(MA_OK == ma_updater_register_ui_provider_callbacks(g_ma_client, MA_MSGBUS_CALLBACK_THREAD_POOL, &callbacks_bundle, NULL));

    ///**************************************************************************
    //Creating Show UI request message
    //***************************************************************************/
    //test_scenario = 4; // Simulate request for Postpone UI

    //TEST_ASSERT(MA_OK == ma_array_create(&request_array));

    //// Initiator Type
    //TEST_ASSERT(MA_OK == ma_variant_create_from_int64(12, &variant_ptr));
    //TEST_ASSERT(MA_OK == ma_array_push(request_array, variant_ptr));
    //TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

    //// UI Type
    //TEST_ASSERT(MA_OK == ma_variant_create_from_int64(MA_UPDATER_UI_TYPE_POSTPONE, &variant_ptr));
    //TEST_ASSERT(MA_OK == ma_array_push(request_array, variant_ptr));
    //TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

    //// Title
    //TEST_ASSERT(MA_OK == ma_variant_create_from_string("Postpone Menu", strlen("Postpone Menu"), &variant_ptr));
    //TEST_ASSERT(MA_OK == ma_array_push(request_array, variant_ptr));
    //TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

    //// Message
    //TEST_ASSERT(MA_OK == ma_variant_create_from_string("DAT download in-progress", strlen("DAT download in-progress"), &variant_ptr));
    //TEST_ASSERT(MA_OK == ma_array_push(request_array, variant_ptr));
    //TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

    //// Countdown Message
    //TEST_ASSERT(MA_OK == ma_variant_create_from_string("DAT download 90% complete", strlen("DAT download 90% complete"), &variant_ptr));
    //TEST_ASSERT(MA_OK == ma_array_push(request_array, variant_ptr));
    //TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

    //// Countdown Value
    //TEST_ASSERT(MA_OK == ma_variant_create_from_int64(90, &variant_ptr));
    //TEST_ASSERT(MA_OK == ma_array_push(request_array, variant_ptr));
    //TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

    //// Converting array to variant and releasing array
    //TEST_ASSERT(MA_OK == ma_variant_create_from_array(request_array, &variant_ptr));
    //TEST_ASSERT(MA_OK == ma_array_release(request_array));

    //// Creating message
    //TEST_ASSERT(MA_OK == ma_message_create(&request_message));
    //TEST_ASSERT(MA_OK == ma_message_set_property(request_message, "prop.key.request_type", "prop.val.rqst.showui"));
    //TEST_ASSERT(MA_OK == ma_message_set_payload(request_message, variant_ptr));
    //TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

    //// Creating an endpoint to send show UI request to UI provider
    //TEST_ASSERT(MA_OK == ma_msgbus_endpoint_create(g_msgbus, expected_show_ui_server_name, NULL, &show_ui_client));
    //TEST_ASSERT(MA_OK == ma_msgbus_endpoint_setopt(show_ui_client, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_POOL));
    //TEST_ASSERT(MA_OK == ma_msgbus_endpoint_setopt(show_ui_client, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_INPROC));

    //// Sending the show UI request
    //TEST_ASSERT(MA_OK == ma_msgbus_send(show_ui_client, request_message, &response_message));

    //// Parsing the response message
    //{
    //    ma_variant_t *variant_ptr;
    //    ma_array_t *response_array = NULL;
    //    ma_int64_t val = 0;

    //    TEST_ASSERT(MA_OK == ma_message_get_payload(response_message, &variant_ptr));
    //    TEST_ASSERT(MA_OK == ma_variant_get_array(variant_ptr, &response_array));
    //    TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

    //    TEST_ASSERT(MA_OK == ma_array_get_element_at(response_array, 1, &variant_ptr));
    //    TEST_ASSERT(MA_OK == ma_variant_get_int64(variant_ptr, &val));
    //    TEST_ASSERT(MA_UPDATER_UI_RC_POSTPONE_NOW == val);
    //    TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

    //    TEST_ASSERT(MA_OK == ma_array_get_element_at(response_array, 2, &variant_ptr));
    //    TEST_ASSERT(MA_OK == ma_variant_get_int64(variant_ptr, &val));
    //    TEST_ASSERT(123 == val);
    //    TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

    //    TEST_ASSERT(MA_OK == ma_array_release(response_array));
    //}

    //// Releasing request message
    //TEST_ASSERT(MA_OK == ma_message_release(request_message));
    //TEST_ASSERT(MA_OK == ma_message_release(response_message));

    ///**************************************************************************
    //Un-registering callbacks
    //***************************************************************************/
    //test_scenario = 3; // Simulate successful unregistration

    //TEST_ASSERT(MA_OK == ma_updater_unregister_ui_provider_callbacks(g_ma_client));
}

/**************************************************************************
UI Provider - Show Reboot UI request/response tests
**************************************************************************/
TEST(ma_updater_ui_provider_tests, show_UI_reboot_request_response_test)
{
    //ma_array_t *request_array = NULL;
    //ma_variant_t *variant_ptr = NULL;
    //ma_message_t *request_message = NULL;
    //ma_message_t *response_message = NULL;
    //ma_msgbus_endpoint_t *show_ui_client = NULL;
    //ma_msgbus_request_t *request_packet = NULL;

    //// Expected Server Name for internal Show UI service is "ma.service.updater.uiprovider" + Product ID + Session ID
    //sprintf(expected_show_ui_server_name, "%s.%s.%lu", "ma.service.updater.uiprovider", product_id, get_session_id());

    ///**************************************************************************
    //Registering callbacks
    //**************************************************************************/
    //test_scenario = 2; // Simulate successful registration

    //TEST_ASSERT(MA_OK == ma_updater_register_ui_provider_callbacks(g_ma_client, MA_MSGBUS_CALLBACK_THREAD_POOL, &callbacks_bundle, NULL));

    ///**************************************************************************
    //Creating Show UI request message
    //***************************************************************************/
    //test_scenario = 5; // Simulate request for Reboot UI

    //TEST_ASSERT(MA_OK == ma_array_create(&request_array));

    //// Initiator Type
    //TEST_ASSERT(MA_OK == ma_variant_create_from_int64(34, &variant_ptr));
    //TEST_ASSERT(MA_OK == ma_array_push(request_array, variant_ptr));
    //TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

    //// UI Type
    //TEST_ASSERT(MA_OK == ma_variant_create_from_int64(MA_UPDATER_UI_TYPE_REBOOT, &variant_ptr));
    //TEST_ASSERT(MA_OK == ma_array_push(request_array, variant_ptr));
    //TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

    //// Title
    //TEST_ASSERT(MA_OK == ma_variant_create_from_string("Reboot Menu", strlen("Reboot Menu"), &variant_ptr));
    //TEST_ASSERT(MA_OK == ma_array_push(request_array, variant_ptr));
    //TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

    //// Message
    //TEST_ASSERT(MA_OK == ma_variant_create_from_string("Rebooting in 5 minutes", strlen("Rebooting in 5 minutes"), &variant_ptr));
    //TEST_ASSERT(MA_OK == ma_array_push(request_array, variant_ptr));
    //TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

    //// Countdown Message
    //TEST_ASSERT(MA_OK == ma_variant_create_from_string("Rebooting after updates", strlen("Rebooting after updates"), &variant_ptr));
    //TEST_ASSERT(MA_OK == ma_array_push(request_array, variant_ptr));
    //TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

    //// Countdown Value
    //TEST_ASSERT(MA_OK == ma_variant_create_from_int64(300, &variant_ptr));
    //TEST_ASSERT(MA_OK == ma_array_push(request_array, variant_ptr));
    //TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

    //// Converting array to variant and releasing array
    //TEST_ASSERT(MA_OK == ma_variant_create_from_array(request_array, &variant_ptr));
    //TEST_ASSERT(MA_OK == ma_array_release(request_array));

    //// Creating message
    //TEST_ASSERT(MA_OK == ma_message_create(&request_message));
    //TEST_ASSERT(MA_OK == ma_message_set_property(request_message, "prop.key.request_type", "prop.val.rqst.showui"));
    //TEST_ASSERT(MA_OK == ma_message_set_payload(request_message, variant_ptr));
    //TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

    //// Creating an endpoint to send show UI request to UI provider
    //TEST_ASSERT(MA_OK == ma_msgbus_endpoint_create(g_msgbus, expected_show_ui_server_name, NULL, &show_ui_client));
    //TEST_ASSERT(MA_OK == ma_msgbus_endpoint_setopt(show_ui_client, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_POOL));
    //TEST_ASSERT(MA_OK == ma_msgbus_endpoint_setopt(show_ui_client, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_INPROC));

    //// Sending the show UI request
    //TEST_ASSERT(MA_OK == ma_msgbus_send(show_ui_client, request_message, &response_message));

    //// Parsing the response message
    //{
    //    ma_variant_t *variant_ptr;
    //    ma_array_t *response_array = NULL;
    //    ma_int64_t val = 0;

    //    TEST_ASSERT(MA_OK == ma_message_get_payload(response_message, &variant_ptr));
    //    TEST_ASSERT(MA_OK == ma_variant_get_array(variant_ptr, &response_array));
    //    TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

    //    TEST_ASSERT(MA_OK == ma_array_get_element_at(response_array, 1, &variant_ptr));
    //    TEST_ASSERT(MA_OK == ma_variant_get_int64(variant_ptr, &val));
    //    TEST_ASSERT(MA_UPDATER_UI_RC_REBOOT_TIMEOUT == val);
    //    TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

    //    TEST_ASSERT(MA_OK == ma_array_get_element_at(response_array, 2, &variant_ptr));
    //    TEST_ASSERT(MA_OK == ma_variant_get_int64(variant_ptr, &val));
    //    TEST_ASSERT(32345 == val);
    //    TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

    //    TEST_ASSERT(MA_OK == ma_array_release(response_array));
    //}

    //// Releasing request message
    //TEST_ASSERT(MA_OK == ma_message_release(request_message));
    //TEST_ASSERT(MA_OK == ma_message_release(response_message));

    ///**************************************************************************
    //Un-registering callbacks
    //***************************************************************************/
    //test_scenario = 3; // Simulate successful unregistration

    //TEST_ASSERT(MA_OK == ma_updater_unregister_ui_provider_callbacks(g_ma_client));
}


