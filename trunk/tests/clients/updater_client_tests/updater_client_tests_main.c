/*****************************************************************************
Main Test runner for Property client unit tests.
******************************************************************************/
#include "unity.h"
#include "unity_fixture.h"
#include <stdlib.h>
#include <stdio.h>

#ifdef MA_WINDOWS
    #include <windows.h>
#endif

#ifdef _MSC_VER
    #include <crtdbg.h> /* */
    #define MA_TRACKLEAKS _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF)
#else
    #define MA_TRACKLEAKS ((void *)0)
#endif

static void run_all_tests(void)
{
   do {RUN_TEST_GROUP(ma_updater_info_sync_test_group); } while (0);
   do {RUN_TEST_GROUP(ma_updater_update_request_test_group); } while (0);
   do {RUN_TEST_GROUP(ma_updater_event_test_group); } while (0);
   do {RUN_TEST_GROUP(ma_updater_test_group); } while (0);
   do {RUN_TEST_GROUP(ma_updater_ui_provider_test_group); } while (0);   
}

int main(int argc, char *argv[]) {
    MA_TRACKLEAKS;
    return UnityMain(argc, argv, run_all_tests);
}


