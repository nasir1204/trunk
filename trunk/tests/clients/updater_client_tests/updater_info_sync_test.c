/*
Updater Client - Updater Sync Information UTs
*/

#include "unity.h"
#include "unity_fixture.h"
#include "unity_internals.h"
#include "ma/internal/clients/updater/ma_update_info_sync.h"
#include <string.h>

TEST_GROUP_RUNNER(ma_updater_info_sync_test_group)
{
    do {RUN_TEST_CASE(ma_updater_info_sync_tests, updater_info_sync_variant_test)} while (0);
}

TEST_SETUP(ma_updater_info_sync_tests)
{

}

TEST_TEAR_DOWN(ma_updater_info_sync_tests)
{

}

struct LookupTable
{
    ma_update_info_sync_type_t sync_type;
    ma_update_state_t state;
    const char *software_id;
    const char *info_key;
    const char *info_value;                         // Will be converted to variant
    ma_updater_product_return_code_t return_code;
    const char *update_type;
    const char *message;
    const char *extra_info;
};

static struct LookupTable const test_vector[] =
{
    { MA_UPDATE_INFO_SYNC_TYPE_GET,             MA_UPDATE_STATE_RUNNING,                 "Software 1",  "Key 1", "d;fkddfknldlk;kf",      MA_UPDATER_PRODUCT_RETURN_CODE_REJECT,          "New",              "3483hy3h8d38hd837h3d873y3h",  "dfg/dfm,g;'dfmlg"          },
    { MA_UPDATE_INFO_SYNC_TYPE_SET,             MA_UPDATE_STATE_STOP_SERVICE,            "Software 3",  "Key 3", "sdflkns;lsc d0dv",      MA_UPDATER_PRODUCT_RETURN_CODE_FAIL,            "Hotfix",           "dsg3249736403897463087836",   "sdfdsl;fjoiyhno;"          },
    { MA_UPDATE_INFO_SYNC_TYPE_RESPONSE,        MA_UPDATE_STATE_PRE_UPDATE_CHECKS,       "Software 2",  "Key 2", "dslnhdvl;dsfhjdnd",     MA_UPDATER_PRODUCT_RETURN_CODE_UNKNOWN,         "Service Pack",     "dsfsd,ljfhsdnknksdlfge80076", "dsklfh ksjdfhsd sdklfh"    },
    { MA_UPDATE_INFO_SYNC_TYPE_NOTIFY,          MA_UPDATE_STATE_FINISHED_WAITING_REBOOT, "Software 4",  "Key 4", "iasdyhndskjdhs,ksdn",   MA_UPDATER_PRODUCT_RETURN_CODE_LICENSE_EXPIRED, "Fresh Install",    "sdfsdkfjhsdklfhs",            "sdfiqwt6gili7yo878yhi876y" },
    { MA_UPDATE_INFO_SYNC_TYPE_RESPONSE,        MA_UPDATE_STATE_FINISHED_OK,             "Software 6",  "Key 6", "dfldshdklcndskjdfhdlk", MA_UPDATER_PRODUCT_RETURN_CODE_ACCEPT,          "Recovery Mode",    "sdfksdhflsdkfgh",             "aw9w8786789697856959"      },
    { MA_UPDATE_INFO_SYNC_TYPE_CHECK_EXISTANCE, MA_UPDATE_STATE_EOL_CHECK,               "Software 5",  "Key 5", "kfgnf;dgnfgjnfgnf",     MA_UPDATER_PRODUCT_RETURN_CODE_OK,              "DAT Info update",  "dsfklhsdfikfkldsfnhdslk",     "iwo76wd78dhsx8osyxhgs"     },
};


/**************************************************************************
creating the updater sync request to and from variant
**************************************************************************/
TEST(ma_updater_info_sync_tests, updater_info_sync_variant_test)
{
    ma_uint8_t counter = 0;

    for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        ma_update_info_sync_t *request = NULL;
        ma_update_info_sync_t *new_request = NULL; // Retrieved updater info from variant
        ma_variant_t *my_update_info_variant = NULL;

        // ma_update_info_sync_create API
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_update_info_sync_create(NULL));
        TEST_ASSERT(MA_OK == ma_update_info_sync_create(&request));

        // ma_update_info_sync_set_type API
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_update_info_sync_set_type(NULL, test_vector[counter].sync_type));
        TEST_ASSERT(MA_OK == ma_update_info_sync_set_type(request, test_vector[counter].sync_type));

        // ma_update_info_sync_set_update_state API
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_update_info_sync_set_update_state(NULL, test_vector[counter].state));
        TEST_ASSERT(MA_OK == ma_update_info_sync_set_update_state(request, test_vector[counter].state));

        // ma_update_info_sync_set_software_id API
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_update_info_sync_set_software_id(NULL, test_vector[counter].software_id));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_update_info_sync_set_software_id(request, NULL));
        TEST_ASSERT(MA_OK == ma_update_info_sync_set_software_id(request, test_vector[counter].software_id));

        // ma_update_info_sync_set_info_key API
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_update_info_sync_set_info_key(NULL, test_vector[counter].info_key));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_update_info_sync_set_info_key(request, NULL));
        TEST_ASSERT(MA_OK == ma_update_info_sync_set_info_key(request, test_vector[counter].info_key));

        // ma_update_info_sync_set_info_value API
        {
            ma_variant_t *variant_ptr = NULL;
            TEST_ASSERT(MA_OK == ma_variant_create_from_string(test_vector[counter].info_value, strlen(test_vector[counter].info_value), &variant_ptr));

            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_update_info_sync_set_info_value(NULL, variant_ptr));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_update_info_sync_set_info_value(request, NULL));
            TEST_ASSERT(MA_OK == ma_update_info_sync_set_info_value(request, variant_ptr));

             TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));
        }

        // ma_update_info_sync_set_product_response_code API
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_update_info_sync_set_product_response_code(NULL, test_vector[counter].return_code));
        TEST_ASSERT(MA_OK == ma_update_info_sync_set_product_response_code(request, test_vector[counter].return_code));

        // ma_update_info_sync_set_update_type API
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_update_info_sync_set_update_type(NULL, test_vector[counter].update_type));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_update_info_sync_set_update_type(request, NULL));
        TEST_ASSERT(MA_OK == ma_update_info_sync_set_update_type(request, test_vector[counter].update_type));

        // ma_update_info_sync_set_message API
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_update_info_sync_set_message(NULL, test_vector[counter].message));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_update_info_sync_set_message(request, NULL));
        TEST_ASSERT(MA_OK == ma_update_info_sync_set_message(request, test_vector[counter].message));

        // ma_update_info_sync_set_extra_info API
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_update_info_sync_set_extra_info(NULL, test_vector[counter].extra_info));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_update_info_sync_set_extra_info(request, NULL));
        TEST_ASSERT(MA_OK == ma_update_info_sync_set_extra_info(request, test_vector[counter].extra_info));

        // Converting Updater Info to variant and releasing updater info
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_update_info_sync_to_variant(NULL, &my_update_info_variant));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_update_info_sync_to_variant(request, NULL));
        TEST_ASSERT(MA_OK == ma_update_info_sync_to_variant(request, &my_update_info_variant));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_update_info_sync_release(NULL));
        TEST_ASSERT(MA_OK == ma_update_info_sync_release(request));
        request = NULL;

        // Retrieving updater info from variant and then releasing variant
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_update_info_sync_from_variant(NULL, &new_request));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_update_info_sync_from_variant(my_update_info_variant, NULL));
        TEST_ASSERT(MA_OK == ma_update_info_sync_from_variant(my_update_info_variant, &new_request));
        TEST_ASSERT(MA_OK == ma_variant_release(my_update_info_variant));

        // ma_update_info_sync_get_type API
        {
            ma_update_info_sync_type_t value = MA_UPDATE_INFO_SYNC_TYPE_NOTIFY;

            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_update_info_sync_get_type(NULL, &value));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_update_info_sync_get_type(new_request, NULL));
            TEST_ASSERT(MA_OK == ma_update_info_sync_get_type(new_request, &value));
            TEST_ASSERT(value == test_vector[counter].sync_type);
        }

        // ma_update_info_sync_get_update_state API
        {
            ma_update_state_t value = MA_UPDATE_STATE_FINISHED_PARTIAL;

            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_update_info_sync_get_update_state(NULL, &value));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_update_info_sync_get_update_state(new_request, NULL));
            TEST_ASSERT(MA_OK == ma_update_info_sync_get_update_state(new_request, &value));
            TEST_ASSERT(value == test_vector[counter].state);
        }

        // ma_update_info_sync_get_software_id API
        {
            const char *value = NULL;

            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_update_info_sync_get_software_id(NULL, &value));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_update_info_sync_get_software_id(new_request, NULL));
            TEST_ASSERT(MA_OK == ma_update_info_sync_get_software_id(new_request, &value));
            TEST_ASSERT(0 == strcmp(value, test_vector[counter].software_id));
        }

        // ma_update_info_sync_get_info_key API
        {
            const char *value = NULL;

            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_update_info_sync_get_info_key(NULL, &value));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_update_info_sync_get_info_key(new_request, NULL));
            TEST_ASSERT(MA_OK == ma_update_info_sync_get_info_key(new_request, &value));
            TEST_ASSERT(0 == strcmp(value, test_vector[counter].info_key));
        }

        // ma_update_info_sync_get_update_type API
        {
            const char *value = NULL;

            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_update_info_sync_get_update_type(NULL, &value));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_update_info_sync_get_update_type(new_request, NULL));
            TEST_ASSERT(MA_OK == ma_update_info_sync_get_update_type(new_request, &value));
            TEST_ASSERT(0 == strcmp(value, test_vector[counter].update_type));
        }

        // ma_update_info_sync_get_info_value API
        {
            ma_variant_t *value = NULL;
            ma_buffer_t *my_buffer = NULL;
            const char *my_string = NULL;
            size_t my_string_size = 0;

            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_update_info_sync_get_info_value(NULL, &value));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_update_info_sync_get_info_value(new_request, NULL));
            TEST_ASSERT(MA_OK == ma_update_info_sync_get_info_value(new_request, &value));

            TEST_ASSERT(MA_OK == ma_variant_get_string_buffer(value, &my_buffer));
            TEST_ASSERT(MA_OK == ma_buffer_get_string(my_buffer, &my_string, &my_string_size));
            TEST_ASSERT(0 == strcmp(my_string, test_vector[counter].info_value));

            TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
            TEST_ASSERT(MA_OK == ma_variant_release(value));
        }

        // ma_update_info_sync_get_product_response_code API
        {
            ma_updater_product_return_code_t value = MA_UPDATER_PRODUCT_RETURN_CODE_ROLLBACK;

            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_update_info_sync_get_product_response_code(NULL, &value));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_update_info_sync_get_product_response_code(new_request, NULL));
            TEST_ASSERT(MA_OK == ma_update_info_sync_get_product_response_code(new_request, &value));
            TEST_ASSERT(value == test_vector[counter].return_code);
        }

        // ma_update_info_sync_get_update_type API
        {
            const char *value = NULL;

            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_update_info_sync_get_update_type(NULL, &value));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_update_info_sync_get_update_type(new_request, NULL));
            TEST_ASSERT(MA_OK == ma_update_info_sync_get_update_type(new_request, &value));
            TEST_ASSERT(0 == strcmp(value, test_vector[counter].update_type));
        }

        // ma_update_info_sync_get_message API
        {
            const char *value = NULL;

            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_update_info_sync_get_message(NULL, &value));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_update_info_sync_get_message(new_request, NULL));
            TEST_ASSERT(MA_OK == ma_update_info_sync_get_message(new_request, &value));
            TEST_ASSERT(0 == strcmp(value, test_vector[counter].message));
        }

        // ma_update_info_sync_get_extra_info API
        {
            const char *value = NULL;

            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_update_info_sync_get_extra_info(NULL, &value));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_update_info_sync_get_extra_info(new_request, NULL));
            TEST_ASSERT(MA_OK == ma_update_info_sync_get_extra_info(new_request, &value));
            TEST_ASSERT(0 == strcmp(value, test_vector[counter].extra_info));
        }

        // Releasing new request object
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_update_info_sync_release(NULL));
        TEST_ASSERT(MA_OK == ma_update_info_sync_release(new_request));
        new_request = NULL;
    }
}



