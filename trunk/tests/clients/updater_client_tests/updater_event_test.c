/*
Updater Client - Updater Event UTs
*/

#include "unity.h"
#include "unity_fixture.h"
#include "unity_internals.h"
#include "ma/updater/ma_updater_event.h"
#include <string.h>

TEST_GROUP_RUNNER(ma_updater_event_test_group)
{
    do {RUN_TEST_CASE(ma_updater_event_tests, updater_event_test)} while (0);
}


TEST_SETUP(ma_updater_event_tests)
{
}

TEST_TEAR_DOWN(ma_updater_event_tests)
{
}

struct LookupTable
{
    ma_int64_t event_id;
    ma_int64_t severity;
    const char *product_id;
    const char *locale;
    const char *update_type;
    ma_update_state_t update_state;
    ma_int64_t update_error;
    const char *new_version;
    const char *date_time;
};

static struct LookupTable const test_vector[] =
{
    { 78970,          0,           "Product 1", "cs_CZ.UTF-8",   "Deployment",    MA_UPDATE_STATE_SUCCEEDED,               -503, "5.0.23.12",  "11-01-2000 12:45:10.03" },
    { -3784,          -39486,      "Product 2", "cs_IN.UTF-16",  "Fresh Install", MA_UPDATE_STATE_FINISHED_WAITING_REBOOT, 0,    "5.80.67.99", "21-10-1989 16:59:35.99" },
    { 39872,          390429,      "Product 7", "cs_US.Unicode", "Uninstall",     MA_UPDATE_STATE_FINISHED_PARTIAL,        110,  "5.8.9.123",  "30-11-1999 24:14:29.72" },
    { 0,              3793,        "Product 3", "cs_CH.UTF-8",   "DAT update",    MA_UPDATE_STATE_FAILED,                  200,  "5.0.23.1",   "12-08-2005 19:02:50.47" },
    { -3983,          0x11111111L, "Product",   "cs_CZ.UTF-16",  "New Version",   MA_UPDATE_STATE_PRENOTIFY,               34,   "5.0.32.0",   "09-04-2099 09:10:55.23" },
};


/*
The updater event information is sent to the updater client in a variant format.
Variant is an array variant containing following data at each array location.

Index     Info
---------------------
1         Event ID
2         Severity
3         Product ID
4         Locale
5         Update Type
6         Update State
7         Update Error
8         New Version
9         Date/Time
10        Script ID
*/

TEST(ma_updater_event_tests, updater_event_test)
{
    ma_uint8_t counter = 0;

    for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        // Updater event object
        ma_updater_event_t *updater_event = NULL;

        // Creating a event variant
        ma_variant_t *event_variant = NULL;
        ma_array_t *array_ptr = NULL;

        // Variant for internal use
        ma_variant_t *variant_ptr = NULL;

        TEST_ASSERT(MA_OK == ma_array_create(&array_ptr));

        TEST_ASSERT(MA_OK == ma_variant_create_from_int64(test_vector[counter].event_id, &variant_ptr));
        TEST_ASSERT(MA_OK == ma_array_push(array_ptr, variant_ptr));
        TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

        TEST_ASSERT(MA_OK == ma_variant_create_from_int64(test_vector[counter].severity, &variant_ptr));
        TEST_ASSERT(MA_OK == ma_array_push(array_ptr, variant_ptr));
        TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

        TEST_ASSERT(MA_OK == ma_variant_create_from_string(test_vector[counter].product_id,
                                                            strlen(test_vector[counter].product_id),
                                                            &variant_ptr));
        TEST_ASSERT(MA_OK == ma_array_push(array_ptr, variant_ptr));
        TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

        TEST_ASSERT(MA_OK == ma_variant_create_from_string(test_vector[counter].locale,
                                                            strlen(test_vector[counter].locale),
                                                            &variant_ptr));
        TEST_ASSERT(MA_OK == ma_array_push(array_ptr, variant_ptr));
        TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

        TEST_ASSERT(MA_OK == ma_variant_create_from_string(test_vector[counter].update_type,
                                                            strlen(test_vector[counter].update_type),
                                                            &variant_ptr));
        TEST_ASSERT(MA_OK == ma_array_push(array_ptr, variant_ptr));
        TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

        TEST_ASSERT(MA_OK == ma_variant_create_from_int64(test_vector[counter].update_state, &variant_ptr));
        TEST_ASSERT(MA_OK == ma_array_push(array_ptr, variant_ptr));
        TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

        TEST_ASSERT(MA_OK == ma_variant_create_from_int64(test_vector[counter].update_error, &variant_ptr));
        TEST_ASSERT(MA_OK == ma_array_push(array_ptr, variant_ptr));
        TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

        TEST_ASSERT(MA_OK == ma_variant_create_from_string(test_vector[counter].new_version,
                                                            strlen(test_vector[counter].new_version),
                                                            &variant_ptr));
        TEST_ASSERT(MA_OK == ma_array_push(array_ptr, variant_ptr));
        TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

        TEST_ASSERT(MA_OK == ma_variant_create_from_string(test_vector[counter].date_time,
                                                            strlen(test_vector[counter].date_time),
                                                            &variant_ptr));
        TEST_ASSERT(MA_OK == ma_array_push(array_ptr, variant_ptr));
        TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

        // Converting event information to variant
        TEST_ASSERT(MA_OK == ma_variant_create_from_array(array_ptr, &event_variant));
        TEST_ASSERT(MA_OK == ma_array_release(array_ptr));

        // Retrieving event for variant and then, release variant
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_event_from_variant(NULL, "previous session closed", &updater_event));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_event_from_variant(event_variant, NULL, &updater_event));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_event_from_variant(event_variant, "previous session closed", NULL));
        TEST_ASSERT(MA_OK == ma_updater_event_from_variant(event_variant, "previous session closed", &updater_event));
        TEST_ASSERT(MA_OK == ma_variant_release(event_variant));

        {
            long event_id = 0;
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_event_get_event_id(NULL, &event_id));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_event_get_event_id(updater_event, NULL));
            TEST_ASSERT(MA_OK == ma_updater_event_get_event_id(updater_event, &event_id));
            TEST_ASSERT(event_id == test_vector[counter].event_id);
        }

        {
            ma_int32_t severity = 0;
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_event_get_severity(NULL, &severity));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_event_get_severity(updater_event, NULL));
            TEST_ASSERT(MA_OK == ma_updater_event_get_severity(updater_event, &severity));
            TEST_ASSERT(severity == test_vector[counter].severity);
        }

        {
            const char *string_val = NULL;
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_event_get_session_id(NULL, &string_val));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_event_get_session_id(updater_event, NULL));
            TEST_ASSERT(MA_OK == ma_updater_event_get_session_id(updater_event, &string_val));
            TEST_ASSERT(0 == strcmp(string_val, "previous session closed"));
        }

        {
            const char *string_val = NULL;
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_event_get_product_id(NULL, &string_val));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_event_get_product_id(updater_event, NULL));
            TEST_ASSERT(MA_OK == ma_updater_event_get_product_id(updater_event, &string_val));
            TEST_ASSERT(0 == strcmp(string_val, test_vector[counter].product_id));
        }

        {
            const char *string_val = NULL;
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_event_get_locale(NULL, &string_val));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_event_get_locale(updater_event, NULL));
            TEST_ASSERT(MA_OK == ma_updater_event_get_locale(updater_event, &string_val));
            TEST_ASSERT(0 == strcmp(string_val, test_vector[counter].locale));
        }

        {
            const char *string_val = NULL;
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_event_get_update_type(NULL, &string_val));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_event_get_update_type(updater_event, NULL));
            TEST_ASSERT(MA_OK == ma_updater_event_get_update_type(updater_event, &string_val));
            TEST_ASSERT(0 == strcmp(string_val, test_vector[counter].update_type));
        }

        {
            ma_update_state_t value = MA_UPDATE_STATE_PRE_UPDATE_CHECKS;
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_event_get_update_state(NULL, &value));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_event_get_update_state(updater_event, NULL));
            TEST_ASSERT(MA_OK == ma_updater_event_get_update_state(updater_event, &value));
            TEST_ASSERT(value == test_vector[counter].update_state);
        }

        {
            ma_int32_t update_error = 0;
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_event_get_update_error(NULL, &update_error));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_event_get_update_error(updater_event, NULL));
            TEST_ASSERT(MA_OK == ma_updater_event_get_update_error(updater_event, &update_error));
            TEST_ASSERT(update_error == test_vector[counter].update_error);
        }

        {
            const char *string_val = NULL;
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_event_get_new_version(NULL, &string_val));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_event_get_new_version(updater_event, NULL));
            TEST_ASSERT(MA_OK == ma_updater_event_get_new_version(updater_event, &string_val));
            TEST_ASSERT(0 == strcmp(string_val, test_vector[counter].new_version));
        }

        {
            const char *string_val = NULL;
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_event_get_date_time(NULL, &string_val));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_event_get_date_time(updater_event, NULL));
            TEST_ASSERT(MA_OK == ma_updater_event_get_date_time(updater_event, &string_val));
            TEST_ASSERT(0 == strcmp(string_val, test_vector[counter].date_time));
        }

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_event_release(NULL));
        TEST_ASSERT(MA_OK == ma_updater_event_release(updater_event));
    }
}

