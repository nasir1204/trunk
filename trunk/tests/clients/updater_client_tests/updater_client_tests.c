#include "unity.h"
#include "unity_fixture.h"
#include "unity_internals.h"
#include "ma/updater/ma_updater.h"
#include "ma_ut_client_setup.h"
#include "ma/internal/ma_strdef.h"

#include <string.h>
#include <uv.h>



static ma_ut_setup_t *g_ut_setup = NULL;
static ma_client_t *g_ma_client = NULL;
static ma_msgbus_t *g_msgbus = NULL;
static ma_msgbus_server_t *g_updater_server = NULL;
static ma_msgbus_server_t *g_scheduler_server = NULL;

static ma_bool_t done = 0;
static uv_cond_t cond;
static uv_mutex_t mux;

static void wait_until_done() {

    uv_mutex_lock(&mux);

    while (!done) {uv_cond_wait(&cond,&mux);}

    uv_mutex_unlock(&mux);
}


TEST_GROUP_RUNNER(ma_updater_client_test_group)
{
    do {RUN_TEST_CASE(ma_updater_client_tests, updater_client_on_demand_update_test)} while (0);
}

static ma_error_t updater_server_cb(ma_msgbus_server_t *server, ma_message_t *request_message, void *cb_data, ma_msgbus_client_request_t *c_request);
static ma_error_t scheduler_server_cb(ma_msgbus_server_t *server, ma_message_t *request_message, void *cb_data, ma_msgbus_client_request_t *c_request);
TEST_SETUP(ma_updater_client_tests)
{
    uv_mutex_init(&mux);
    uv_cond_init(&cond);

    ma_ut_setup_create("updater_client.update.tests", "ma.updater.client.test", &g_ut_setup);
    g_ma_client = ma_ut_setup_get_client(g_ut_setup);
    g_msgbus = ma_ut_setup_get_msgbus(g_ut_setup);
    ma_msgbus_server_create(g_msgbus, MA_UPDATER_SERVICE_NAME_STR, &g_updater_server);
    ma_msgbus_server_setopt(g_updater_server, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_INPROC);
    ma_msgbus_server_start(g_updater_server, updater_server_cb, NULL);

    ma_msgbus_server_create(g_msgbus, MA_SCHEDULER_SERVICE_NAME_STR, &g_scheduler_server);
    ma_msgbus_server_setopt(g_scheduler_server, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_INPROC);
    ma_msgbus_server_start(g_scheduler_server, scheduler_server_cb, NULL);
}

TEST_TEAR_DOWN(ma_updater_client_tests)
{
    ma_msgbus_server_stop(g_updater_server);
	ma_msgbus_server_release(g_updater_server);

    ma_msgbus_server_stop(g_scheduler_server);
	ma_msgbus_server_release(g_scheduler_server);

    ma_ut_setup_release(g_ut_setup);
    uv_cond_destroy(&cond);
    uv_mutex_destroy(&mux);
    
}


TEST(ma_updater_client_tests, updater_client_on_demand_update_test)
{
    ma_updater_update_request_t *request = NULL;
    ma_buffer_t *session_id = NULL;

    TEST_ASSERT(MA_OK == ma_updater_update_request_create(MA_UPDATER_UPDATE_TYPE_NORMAL_UPDATE, &request));

    TEST_ASSERT(MA_OK == ma_updater_start_update(g_ma_client, request, &session_id));

    TEST_ASSERT(MA_OK == ma_updater_update_request_release(request));

    TEST_ASSERT(MA_OK == ma_updater_stop_update(g_ma_client, session_id));

    ma_buffer_release(session_id);

}


static ma_error_t updater_server_cb(ma_msgbus_server_t *server, ma_message_t *request_message, void *cb_data, ma_msgbus_client_request_t *c_request) {
    /* do the validation and send the response */
    ma_message_t *response = NULL;
    TEST_ASSERT(MA_OK == ma_message_create(&response))
    TEST_ASSERT(MA_OK == ma_msgbus_server_client_request_post_result(c_request, MA_OK, response));
    (void)ma_message_release(response);
    return MA_OK;
}


static ma_error_t scheduler_server_cb(ma_msgbus_server_t *server, ma_message_t *request_message, void *cb_data, ma_msgbus_client_request_t *c_request) {
    /* Just say OKAY */
    ma_message_t *response = NULL;
    TEST_ASSERT(MA_OK == ma_message_create(&response))
    TEST_ASSERT(MA_OK == ma_message_set_property(response, MA_TASK_STATUS, MA_TASK_REPLY_STATUS_SUCCESS));
    TEST_ASSERT(MA_OK == ma_msgbus_server_client_request_post_result(c_request, MA_OK, response));
    (void)ma_message_release(response);
    return MA_OK;
}
