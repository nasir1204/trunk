#include "unity.h"
#include "unity_fixture.h"
#include "unity_internals.h"
#include "ma/updater/ma_updater_update_task.h"
#include <string.h>

TEST_GROUP_RUNNER(ma_updater_update_task_test_group)
{
    do {RUN_TEST_CASE(ma_updater_update_task_tests, updater_update_task_test)} while (0);
}

TEST_SETUP(ma_updater_update_task_tests)
{
    
}

TEST_TEAR_DOWN(ma_updater_update_task_tests)
{
    
}


/**************************************************************************
creating the updater sync request to and from variant
**************************************************************************/
TEST(ma_updater_update_task_tests, updater_update_task_test)
{
	ma_task_t *task = NULL;
	const char *taskid  = "Unit Test Task";
	ma_array_t *product_list = NULL;
	ma_variant_t *variant_ptr = NULL;
		printf("updater Tasks on the run...\n");
	
		ma_array_create(&product_list);
        ma_variant_create_from_string("MY PRODUCT ID 1", strlen("MY PRODUCT ID 1"), &variant_ptr);
        ma_array_push(product_list, variant_ptr);
        ma_variant_release(variant_ptr);

        ma_variant_create_from_string("MY PRODUCT ID 2", strlen("MY PRODUCT ID 2"), &variant_ptr);
        ma_array_push(product_list, variant_ptr);
        ma_variant_release(variant_ptr);

	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_update_task_create(NULL,NULL));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_update_task_create(taskid,NULL));
//	TEST_ASSERT(MA_OK == ma_updater_update_task_create(NULL,&task));
//	TEST_ASSERT(MA_OK == client_task_release(task));
    TEST_ASSERT(MA_OK == ma_updater_update_task_create(taskid,&task));
	
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_update_task_set_locale(NULL,NULL));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_update_task_set_locale(NULL,100));
	TEST_ASSERT(MA_OK == ma_updater_update_task_set_locale(task,NULL));
	TEST_ASSERT(MA_OK == ma_updater_update_task_set_locale(task,100));

	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_update_task_set_product_list(NULL,NULL));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_update_task_set_product_list(task,NULL));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_updater_update_task_set_product_list(NULL,product_list));
	TEST_ASSERT(MA_OK == ma_updater_update_task_set_product_list(task,product_list));

	
	
}