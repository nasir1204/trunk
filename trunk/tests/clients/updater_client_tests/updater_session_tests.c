#include "unity.h"
#include "unity_fixture.h"
#include "unity_internals.h"
//#include "ma/internal/clients/updater/ma_updater_session_internal.h"
#include <string.h>

TEST_GROUP_RUNNER(ma_updater_session_test_group)
{
    do {RUN_TEST_CASE(ma_updater_session_tests, updater_session_variant_test)} while (0);
}

TEST_SETUP(ma_updater_session_tests)
{
    
}

TEST_TEAR_DOWN(ma_updater_session_tests)
{
    
}


/**************************************************************************
creating the updater session to and from variant
**************************************************************************/
TEST(ma_updater_session_tests, updater_session_variant_test)
{
    ma_updater_session_t *session = NULL;
    ma_uint32_t set_locale = 0x0409, get_locale = 0;
    ma_updater_initiator_type_t set_updater_initiator_type = MA_UPDATER_INITIATOR_TYPE_ONDEMAND_UPDATE , get_updater_initiator_type = 0;
    ma_variant_t *variant = NULL;
    const char *set_location = "Bangalore", *get_location = NULL;
    const char *set_build_number = "5.0.0.100", *get_build_number = NULL;

    TEST_ASSERT(MA_OK == ma_updater_session_create(&session));

    /* set some parameters */
    TEST_ASSERT(MA_OK == ma_updater_session_set_info(session, MA_UPDATER_INFO_LOCATION_STR, set_location))
    TEST_ASSERT(MA_OK == ma_updater_session_set_info(session, MA_UPDATER_INFO_BUILDNUM_STR, set_build_number));

    TEST_ASSERT(MA_OK == ma_updater_session_set_locale(session, set_locale));

    TEST_ASSERT(MA_OK == ma_updater_session_set_updater_initiator_type(session, set_updater_initiator_type));

    /* convert into variant */
    TEST_ASSERT(MA_OK == ma_updater_session_to_variant(session, &variant));

    TEST_ASSERT(MA_OK == ma_updater_session_release(session));

    /* create from variant */
    TEST_ASSERT(MA_OK == ma_updater_session_from_variant(variant, &session));

    ma_variant_release(variant);

    /* get the params */
    TEST_ASSERT(MA_OK == ma_updater_session_get_info(session, MA_UPDATER_INFO_LOCATION_STR, &get_location));
    TEST_ASSERT(MA_OK == ma_updater_session_get_info(session, MA_UPDATER_INFO_BUILDNUM_STR, &get_build_number));
    
    TEST_ASSERT(MA_OK == ma_updater_session_get_locale(session, &get_locale));

    TEST_ASSERT(MA_OK == ma_updater_session_get_updater_initiator_type(session, &get_updater_initiator_type));

    /* compare set and get */
    TEST_ASSERT_EQUAL_STRING(set_location,get_location);
    TEST_ASSERT_EQUAL_STRING(set_build_number, get_build_number);
    TEST_ASSERT_EQUAL_INT(set_locale,get_locale);
    TEST_ASSERT_EQUAL_INT(set_updater_initiator_type,get_updater_initiator_type);

    ma_updater_session_release(session);

}
