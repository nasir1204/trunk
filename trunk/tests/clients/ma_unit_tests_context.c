#include "ma/ma_context.h"
#include "ma/internal/ma_context_internal.h"
#include "ma/internal/clients/ma/controller/ma_client_controller.h"
#include "ma/ma_dispatcher.h"

#include <stdlib.h>
#include <string.h>

typedef struct ma_client_context_s {
    ma_context_t                base_context;
    ma_msgbus_t                 *msgbus;
    ma_logger_t                 *logger;
    ma_client_controller_t      *client_controller;
    char                        *product_id;
}ma_client_context_t;


static void *ma_client_context_get_object(ma_context_t *context, const char *object_name);
static ma_error_t ma_client_context_release(ma_context_t *context);
static const ma_context_methods_t context_methods = {
    &ma_client_context_get_object,
    &ma_client_context_release
};

ma_error_t ma_context_create(const char *product_id,  ma_context_t **context) {
    if(product_id && context) {
        ma_error_t rc = MA_OK;
        ma_client_context_t *self = (ma_client_context_t *)calloc(1,sizeof(ma_client_context_t));
        if(!self)       return MA_ERROR_OUTOFMEMORY;
       
        
        if(MA_OK != (rc = ma_msgbus_create(product_id, &self->msgbus))) {   
            free(self);
            return rc;
        }

		if(MA_OK != (rc = ma_msgbus_start(self->msgbus))) {
			ma_msgbus_release(self->msgbus);
            free(self);
            return rc;
        }

        if(MA_OK != (rc = ma_client_controller_create(self->msgbus, MA_MSGBUS_CALLBACK_THREAD_IO, MSGBUS_CONSUMER_REACH_INPROC, &self->client_controller))) {			
			ma_msgbus_stop(self->msgbus, MA_TRUE);
            ma_msgbus_release(self->msgbus);
   
            free(self);
            return rc;
        }
		ma_client_controller_start(self->client_controller);
        ma_context_init((ma_context_t *)self, &context_methods, self);
        self->product_id = strdup(product_id);
        *context = (ma_context_t*)self;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_context_release(ma_context_t *context) {
    if(context && context->methods && context->methods->release) {
        return context->methods->release(context);
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_context_get_msgbus(ma_context_t *context , ma_msgbus_t **msgbus) {
    if(context && msgbus) {
        *msgbus = ((ma_client_context_t *)context)->msgbus;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_context_get_logger(ma_context_t *context, ma_logger_t **logger) {
    if(context && logger) {
        *logger = ((ma_client_context_t *)context)->logger;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_context_set_logger(ma_context_t *context, ma_logger_t *logger) {
    if(context && logger) {
       ((ma_client_context_t *)context)->logger = logger;
       (void)ma_client_controller_set_logger(((ma_client_context_t*)context)->client_controller, logger);
       return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_client_context_release(ma_context_t *context) {
    if(context) {
        ma_client_context_t *self = (ma_client_context_t *)context;
        
        if(self->client_controller){
			ma_client_controller_stop(self->client_controller);
			ma_client_controller_release(self->client_controller);
		}
        
        if(self->msgbus) {
			ma_msgbus_stop(self->msgbus, MA_TRUE);
			ma_msgbus_release(self->msgbus);
		}

   
        if(self->product_id) free(self->product_id);

        free(self);

        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


static void *ma_client_context_get_object(ma_context_t *context, const char *object_name) {
    if(0 == strcmp(MA_OBJECT_CLIENT_CONTROLLER_NAME_STR ,object_name))
        return ((ma_client_context_t*)context)->client_controller;
    else if(0 == strcmp(MA_OBJECT_LOGGER_NAME_STR ,object_name))
        return ((ma_client_context_t*)context)->logger;
    else if(0 == strcmp(MA_OBJECT_MSGBUS_NAME_STR ,object_name))
        return ((ma_client_context_t*)context)->msgbus;
	else if(0 == strcmp(MA_OBJECT_PRODUCT_ID_STR ,object_name))
        return ((ma_client_context_t*)context)->product_id;
    else
        return NULL;
}


