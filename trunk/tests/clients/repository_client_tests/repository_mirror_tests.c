/*****************************************************************************
Repository Mirror Unit Tests.

Test Approach - This test creates a dummy Repository Service which captures
the information sent from Repository client and verifies it against the
expected properties sent from Repository client.
******************************************************************************/

#define UNITY_FIXTURE_MALLOC_OVERRIDES_H_ 1

#include "unity.h"
#include "unity_fixture.h"
#include "unity_internals.h"
#include "ma/repository/ma_repository_mirrror.h"
#include "ma/internal/services/repository/ma_mirror_request_param.h"
#include "ma/ma_message.h"
#include "ma_ut_setup.h"
#include <string.h>
#include <uv.h>

TEST_GROUP_RUNNER(ma_repository_mirror_test_group)
{
    do {RUN_TEST_CASE(ma_repository_mirror_tests, repository_mirror_start_stop_test)} while (0);
    do {RUN_TEST_CASE(ma_repository_mirror_tests, repository_mirror_task_create_test)} while (0);
}

static ma_client_t *g_ma_client = NULL;
static ma_msgbus_t *g_msgbus = NULL;
static ma_ut_setup_t *g_ut_setup  = NULL;

static const char product_id[] = "ma.repository.client.test";

// Repository and Scheduler service objects (message bus server)
static ma_msgbus_server_t *repository_server = NULL;
static ma_msgbus_server_t *scheduler_server = NULL;

static ma_bool_t done = 0;
static uv_cond_t cond;
static uv_mutex_t mux;

// Stores the task ID sent to the Repository Service for verification with the session ID returned by the client.
char *global_task_id = NULL;

static void wait_until_done()
{
    uv_mutex_lock(&mux);
    while (!done)
    {
        uv_cond_wait(&cond,&mux);
    }

    uv_mutex_unlock(&mux);
}

static void set_done()
{
    uv_mutex_lock(&mux);
    done = MA_TRUE;
    uv_cond_signal(&cond);
    uv_mutex_unlock(&mux);
}

// Declaration for callback functions
static ma_error_t repository_server_cb(ma_msgbus_server_t *server, ma_message_t *request_message, void *cb_data, ma_msgbus_client_request_t *c_request);
static ma_error_t scheduler_server_cb(ma_msgbus_server_t *server, ma_message_t *request_message, void *cb_data, ma_msgbus_client_request_t *c_request);

// Extern definition
extern ma_error_t convert_variant_to_task(ma_variant_t *task_details, ma_task_t *task);

// Indicates the test scenario currently executing
static ma_uint8_t test_scenario = 0;


/**************************************************************************
1) Creating the unit test context.
2) Retrieving underlying message bus instance.
3) Creating and starting message bus service (Repository Service) using the
   message bus instance.
**************************************************************************/
TEST_SETUP(ma_repository_mirror_tests)
{
    uv_mutex_init(&mux);
    uv_cond_init(&cond);

    ma_ut_setup_create("repository_client.repository_mirror.tests", product_id, &g_ut_setup);
    g_ma_client = ma_ut_setup_get_client(g_ut_setup);
    g_msgbus = ma_ut_setup_get_msgbus(g_ut_setup);
    ma_msgbus_server_create(g_msgbus, "ma.service.repository", &repository_server);
    ma_msgbus_server_create(g_msgbus, "ma.service.scheduler", &scheduler_server);

    ma_msgbus_server_start(repository_server, repository_server_cb, NULL);
    ma_msgbus_server_start(scheduler_server, scheduler_server_cb, NULL);
}

/**************************************************************************
1) Stopping and releasing the message bus service (Repository service).
2) Releasing the context.
**************************************************************************/
TEST_TEAR_DOWN(ma_repository_mirror_tests)
{
    ma_msgbus_server_stop(repository_server);
    ma_msgbus_server_stop(scheduler_server);
    ma_msgbus_server_release(repository_server);
    ma_msgbus_server_release(scheduler_server);

    ma_ut_setup_release(g_ut_setup);

    uv_cond_destroy(&cond);
    uv_mutex_destroy(&mux);
}

/******************************************************************************
Call back function to be executed when the Repository Service receives requests
from the Repository client.

This function verifies the mirror request parameters (Task ID, Mirror Request
Type and Mirror Request Initiator Type) sent to the repository service in each
scenario.

TEST 1 : "Start Repository Mirror" test (Scenario - Mirror task could not be
          added to Scheduler service successfully)
TEST 2 : "Start Repository Mirror" test (Scenario - Mirror process started
          successfully)
TEST 3 : "Stop Repository Mirror" test
TEST 4 : "Check Repository Mirror" test. Respond with MA_REPOSITORY_MIRROR_STATE_FINISHED_FAILED state.
TEST 5 : "Check Repository Mirror" test. Respond with MA_REPOSITORY_MIRROR_STATE_TIMEOUT state.
TEST 6 : "Check Repository Mirror" test. Respond with MA_REPOSITORY_MIRROR_STATE_FINISHED_SUCCEEDED state.
******************************************************************************/
static ma_error_t repository_server_cb(ma_msgbus_server_t *server, ma_message_t *payload, void *cb_data, ma_msgbus_client_request_t *c_request)
{
    ma_variant_t *variant_ptr = NULL;
    ma_mirror_request_param_t params;
    ma_message_t *response_payload = NULL;
    ma_message_t *publisher_payload = NULL;

    // Verifying request type
    {
        const char *string_val = NULL;

        TEST_ASSERT(MA_OK == ma_message_get_property(payload, "prop.key.msg_type", &string_val));
        TEST_ASSERT(0 == strcmp("prop.val.rqst.mirror.task", string_val));
    }

    // Get the Mirror Task related information from payload for verification
    TEST_ASSERT(MA_OK == ma_message_get_payload(payload, &variant_ptr));
    TEST_ASSERT(MA_OK == ma_mirror_request_param_from_variant(variant_ptr, &params));
    TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

    // "Start Repository Mirror" test (Scenario - Mirror task could not
    //  be added to scheduler service successfully)
    if(test_scenario == 1)
    {
        TEST_ASSERT(MA_MIRROR_REQUEST_TYPE_START == params.request_type);
        TEST_ASSERT(MA_REPOSITORY_MIRROR_INITIATOR_TYPE_ONDEMAND_MIRROR == params.initiator_type);
        TEST_ASSERT(0 == strcmp(global_task_id, params.task_id));

        // Respond with MA_OK message
        TEST_ASSERT(MA_OK == ma_message_create(&response_payload));
        TEST_ASSERT(MA_OK == ma_msgbus_server_client_request_post_result(c_request, MA_OK, response_payload));
        TEST_ASSERT(MA_OK == ma_message_release(response_payload));
    }

    // "Start Repository Mirror" test (Scenario - Mirror task scheduled
    //  successfully)
    if(test_scenario == 2)
    {
        TEST_ASSERT(MA_MIRROR_REQUEST_TYPE_START == params.request_type);
        TEST_ASSERT(MA_REPOSITORY_MIRROR_INITIATOR_TYPE_ONDEMAND_MIRROR == params.initiator_type);
        TEST_ASSERT(0 == strcmp(global_task_id, params.task_id));

        // Respond with MA_OK message
        TEST_ASSERT(MA_OK == ma_message_create(&response_payload));
        TEST_ASSERT(MA_OK == ma_msgbus_server_client_request_post_result(c_request, MA_OK, response_payload));
        TEST_ASSERT(MA_OK == ma_message_release(response_payload));
    }

    // "Stop Repository Mirror" test
    if(test_scenario == 3)
    {
        TEST_ASSERT(MA_MIRROR_REQUEST_TYPE_STOP == params.request_type);
        TEST_ASSERT(MA_REPOSITORY_MIRROR_INITIATOR_TYPE_ONDEMAND_MIRROR == params.initiator_type);
        TEST_ASSERT(0 == strcmp(global_task_id, params.task_id));

        // Respond with MA_OK message
        TEST_ASSERT(MA_OK == ma_message_create(&response_payload));
        TEST_ASSERT(MA_OK == ma_msgbus_server_client_request_post_result(c_request, MA_OK, response_payload));
        TEST_ASSERT(MA_OK == ma_message_release(response_payload));
    }

    // "Mirror State" test
    if(test_scenario == 4)
    {
        TEST_ASSERT(MA_MIRROR_REQUEST_TYPE_STATUS_CHECK == params.request_type);
        TEST_ASSERT(MA_REPOSITORY_MIRROR_INITIATOR_TYPE_ONDEMAND_MIRROR == params.initiator_type);
        TEST_ASSERT(0 == strcmp(global_task_id, params.task_id));

        // Respond with MA_OK message and update state (payload) as MA_REPOSITORY_MIRROR_STATE_FINISHED_FAILED
        TEST_ASSERT(MA_OK == ma_message_create(&response_payload));
        TEST_ASSERT(MA_OK == ma_variant_create_from_int16(MA_REPOSITORY_MIRROR_STATE_FINISHED_FAILED, &variant_ptr));
        TEST_ASSERT(MA_OK == ma_message_set_payload(response_payload, variant_ptr));
        TEST_ASSERT(MA_OK == ma_msgbus_server_client_request_post_result(c_request, MA_OK, response_payload));
        TEST_ASSERT(MA_OK == ma_message_release(response_payload));

        // Also publishing the state information
        TEST_ASSERT(MA_OK == ma_message_create(&publisher_payload));
        TEST_ASSERT(MA_OK == ma_message_set_property(publisher_payload, "prop.key.mirror.session_id", "my_known_session_id"));
        TEST_ASSERT(MA_OK == ma_message_set_payload(publisher_payload, variant_ptr));
        TEST_ASSERT(MA_OK == ma_msgbus_publish(g_msgbus, "ma.repository.mirror.state", MSGBUS_CONSUMER_REACH_INPROC, publisher_payload));
        TEST_ASSERT(MA_OK == ma_message_release(publisher_payload));

        TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));
    }

    // "Mirror State" test
    if(test_scenario == 5)
    {
        TEST_ASSERT(MA_MIRROR_REQUEST_TYPE_STATUS_CHECK == params.request_type);
        TEST_ASSERT(MA_REPOSITORY_MIRROR_INITIATOR_TYPE_ONDEMAND_MIRROR == params.initiator_type);
        TEST_ASSERT(0 == strcmp(global_task_id, params.task_id));

        // Respond with MA_OK message and update state (payload) as MA_REPOSITORY_MIRROR_STATE_TIMEOUT
        TEST_ASSERT(MA_OK == ma_message_create(&response_payload));
        TEST_ASSERT(MA_OK == ma_variant_create_from_int16(MA_REPOSITORY_MIRROR_STATE_TIMEOUT, &variant_ptr));
        TEST_ASSERT(MA_OK == ma_message_set_payload(response_payload, variant_ptr));
        TEST_ASSERT(MA_OK == ma_msgbus_server_client_request_post_result(c_request, MA_OK, response_payload));
        TEST_ASSERT(MA_OK == ma_message_release(response_payload));

        // Also publishing the state information
        TEST_ASSERT(MA_OK == ma_message_create(&publisher_payload));
        TEST_ASSERT(MA_OK == ma_message_set_property(publisher_payload, "prop.key.mirror.session_id", "my_known_session_id"));
        TEST_ASSERT(MA_OK == ma_message_set_payload(publisher_payload, variant_ptr));
        TEST_ASSERT(MA_OK == ma_msgbus_publish(g_msgbus, "ma.repository.mirror.state", MSGBUS_CONSUMER_REACH_INPROC, publisher_payload));
        TEST_ASSERT(MA_OK == ma_message_release(publisher_payload));

        TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));
    }

    // "Mirror State" test
    if(test_scenario == 6)
    {
        TEST_ASSERT(MA_MIRROR_REQUEST_TYPE_STATUS_CHECK == params.request_type);
        TEST_ASSERT(MA_REPOSITORY_MIRROR_INITIATOR_TYPE_ONDEMAND_MIRROR == params.initiator_type);
        TEST_ASSERT(0 == strcmp(global_task_id, params.task_id));

        // Respond with MA_OK message and update state (payload) as MA_REPOSITORY_MIRROR_STATE_FINISHED_SUCCEEDED
        TEST_ASSERT(MA_OK == ma_message_create(&response_payload));
        TEST_ASSERT(MA_OK == ma_variant_create_from_int16(MA_REPOSITORY_MIRROR_STATE_FINISHED_SUCCEEDED, &variant_ptr));
        TEST_ASSERT(MA_OK == ma_message_set_payload(response_payload, variant_ptr));
        TEST_ASSERT(MA_OK == ma_msgbus_server_client_request_post_result(c_request, MA_OK, response_payload));
        TEST_ASSERT(MA_OK == ma_message_release(response_payload));

        // Also publishing the state information
        TEST_ASSERT(MA_OK == ma_message_create(&publisher_payload));
        TEST_ASSERT(MA_OK == ma_message_set_property(publisher_payload, "prop.key.mirror.session_id", "my_known_session_id"));
        TEST_ASSERT(MA_OK == ma_message_set_payload(publisher_payload, variant_ptr));
        TEST_ASSERT(MA_OK == ma_msgbus_publish(g_msgbus, "ma.repository.mirror.state", MSGBUS_CONSUMER_REACH_INPROC, publisher_payload));
        TEST_ASSERT(MA_OK == ma_message_release(publisher_payload));

        TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));
    }

    return MA_OK;
}

/******************************************************************************
Call back function to be executed when the Scheduler Service receives requests
from the scheduler client. Repository Client uses the Scheduler client to
schedule the Mirror tasks.

This function verifies the Mirror Request parameters (Task ID, Task Type and
Destination Repository) passed to the Scheduler service while adding the
Mirror task.

In Test Scenario 1, this function returns "TASK_REPLY_STATUS_FAILED" in response,
simulating that the mirror task could not be added to scheduler.

In Test Scenario 2, this function returns "TASK_REPLY_STATUS_SUCCESS" in response,
simulating that the mirror task successfully added to scheduler.
******************************************************************************/
static ma_error_t scheduler_server_cb(ma_msgbus_server_t *server, ma_message_t *payload, void *cb_data, ma_msgbus_client_request_t *c_request)
{
    // Retrieve the Scheduler Task Type from the message properties for verification
    {
        const char *string_val = NULL;

        TEST_ASSERT(MA_OK == ma_message_get_property(payload, "TASK_OP_TYPE", &string_val));
        TEST_ASSERT(0 == strcmp("TASK_ADD", string_val));
    }

    // Retrieve the Task ID from the message properties for verification
    {
        const char *string_val = NULL;

        TEST_ASSERT(MA_OK == ma_message_get_property(payload, "TASK_ID", &string_val));
        global_task_id = strdup(string_val); // Store Task ID in a global variable for later use (verification)
    }

    // Retrieve the Task Type from the message properties for verification
    {
        const char *string_val = NULL;

        TEST_ASSERT(MA_OK == ma_message_get_property(payload, "TASK_TYPE", &string_val));
        TEST_ASSERT(0 == strcmp("ma.mirror.task.type.mirror", string_val));
    }

    // Retrieve the Destination Path from the message payload for verification
    {
        ma_variant_t *variant_ptr = NULL;
        ma_task_t *my_task = NULL;
        ma_buffer_t *my_buffer = NULL;
        const char *my_string = NULL;
        size_t my_string_size = 0;

        // Creating task object and retrieving task from message payload
        TEST_ASSERT(MA_OK == ma_task_create(global_task_id, &my_task));
        TEST_ASSERT(MA_OK == ma_message_get_payload(payload, &variant_ptr));
        TEST_ASSERT(MA_OK == convert_variant_to_task(variant_ptr, my_task));
        TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

        // Verifying Destination Path
        TEST_ASSERT(MA_OK == ma_task_get_setting(my_task, "MirrorOptions", "szPath", &variant_ptr));
        TEST_ASSERT(MA_OK == ma_variant_get_string_buffer(variant_ptr, &my_buffer));
        TEST_ASSERT(MA_OK == ma_buffer_get_string(my_buffer, &my_string, &my_string_size));
        TEST_ASSERT(0 == strcmp("c:\\Repository\\Mirror\\Destination", my_string));
        TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
        TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

        // Releasing task
        TEST_ASSERT(MA_OK == ma_task_release(my_task));
    }

    // Respond with MA_OK message and task status (Success/Failed)
    {
        ma_message_t *response_payload = NULL;

        TEST_ASSERT(MA_OK == ma_message_create(&response_payload));

        if(test_scenario == 1)
        {
            TEST_ASSERT(MA_OK == ma_message_set_property(response_payload, "TASK_STATUS", "TASK_REPLY_STATUS_FAILED"));
        }

        if(test_scenario == 2)
        {
            TEST_ASSERT(MA_OK == ma_message_set_property(response_payload, "TASK_STATUS", "TASK_REPLY_STATUS_SUCCESS"));
        }

        TEST_ASSERT(MA_OK == ma_msgbus_server_client_request_post_result(c_request, MA_OK, response_payload));
        TEST_ASSERT(MA_OK == ma_message_release(response_payload));
    }

    return MA_OK;
}


/******************************************************************************
Call back function to be executed when the application has registered for the
State callback.

Verifies the Session ID (Task ID) and Mirror State is correctly passed to this
callback function.
******************************************************************************/
ma_error_t state_callback(ma_client_t *ma_client, const char *session_id, ma_repository_mirror_state_t state, void *cb_data)
{
    // Verifying Session ID
    TEST_ASSERT(0 == strcmp("my_known_session_id", session_id));

    // Verifying that State is correctly returned
    if(test_scenario == 4) { TEST_ASSERT(MA_REPOSITORY_MIRROR_STATE_FINISHED_FAILED == state); }
    if(test_scenario == 5) { TEST_ASSERT(MA_REPOSITORY_MIRROR_STATE_TIMEOUT == state); }
    if(test_scenario == 6) { TEST_ASSERT(MA_REPOSITORY_MIRROR_STATE_FINISHED_SUCCEEDED == state); }

    set_done();
    return MA_OK;
}


/**************************************************************************
Repository Mirror - Start and Stop test
**************************************************************************/
TEST(ma_repository_mirror_tests, repository_mirror_start_stop_test)
{
    ma_buffer_t *session_id = NULL;

    /**************************************************************************
    TEST 1 - "Start Repository Mirror" Test (Scenario - Mirror task could not
              be added to Scheduler service successfully)
    **************************************************************************/
    test_scenario = 1;

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_mirror_start(NULL, "c:\\Repository\\Mirror\\Destination", &session_id));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_mirror_start(g_ma_client, NULL, &session_id));
    TEST_ASSERT(MA_ERROR_APIFAILED == ma_repository_mirror_start(g_ma_client, "c:\\Repository\\Mirror\\Destination", &session_id));

    /**************************************************************************
    TEST 2 - "Start Repository Mirror" Test (Scenario - Mirror task successfully
              added to Scheduler)
    **************************************************************************/
    test_scenario = 2;

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_mirror_start(NULL, "c:\\Repository\\Mirror\\Destination", &session_id));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_mirror_start(g_ma_client, NULL, &session_id));
    TEST_ASSERT(MA_OK == ma_repository_mirror_start(g_ma_client, "c:\\Repository\\Mirror\\Destination", &session_id));

    // Verify that correct Session ID is returned
    {
        const char *my_string = NULL;
        size_t my_string_size = 0;
        TEST_ASSERT(MA_OK == ma_buffer_get_string(session_id, &my_string, &my_string_size));
        TEST_ASSERT(0 == strcmp(global_task_id, my_string));
    }

    /**************************************************************************
    TEST 3 - "Stop Repository Mirror" Test (Scenario - Stopping Mirror Task)
    **************************************************************************/
    test_scenario = 3;

    // Stop Update (using Session ID buffer from previous Test 1)
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_mirror_stop(NULL, session_id));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_mirror_stop(g_ma_client, NULL));
    TEST_ASSERT(MA_OK == ma_repository_mirror_stop(g_ma_client, session_id));


    /**************************************************************************
    Registering Repository Mirror State Callbacks
    **************************************************************************/
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_mirror_register_state_callback(NULL, state_callback, NULL));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_mirror_register_state_callback(g_ma_client, NULL, NULL));
    TEST_ASSERT(MA_OK == ma_repository_mirror_register_state_callback(g_ma_client, state_callback, NULL));

    // Trying to register callback, when it is already registered once
    TEST_ASSERT(MA_ERROR_ALREADY_REGISTERED == ma_repository_mirror_register_state_callback(g_ma_client, state_callback, NULL));

    /**************************************************************************
    TEST 4 - "Check Repository Mirror Progress State" Test (MA_REPOSITORY_MIRROR_STATE_FINISHED_FAILED)
    **************************************************************************/
    test_scenario = 4;
    done = 0;

    {
        ma_repository_mirror_state_t my_state = MA_REPOSITORY_MIRROR_STATE_IN_PROGRESS;

        // Check State (using Session ID buffer from previous Test 1)
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_mirror_get_state(NULL, session_id, &my_state));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_mirror_get_state(g_ma_client, NULL, &my_state));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_mirror_get_state(g_ma_client, session_id, NULL));
        TEST_ASSERT(MA_OK == ma_repository_mirror_get_state(g_ma_client, session_id, &my_state));
        TEST_ASSERT(MA_REPOSITORY_MIRROR_STATE_FINISHED_FAILED == my_state);
    }

    // Waiting for published state information to be verified.
    wait_until_done();

    /**************************************************************************
    TEST 5 - "Check Repository Mirror Progress State" Test (MA_REPOSITORY_MIRROR_STATE_TIMEOUT)
    **************************************************************************/
    test_scenario = 5;
    done = 0;

    {
        ma_repository_mirror_state_t my_state = MA_REPOSITORY_MIRROR_STATE_IN_PROGRESS;

        // Check State (using Session ID buffer from previous Test 1)
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_mirror_get_state(NULL, session_id, &my_state));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_mirror_get_state(g_ma_client, NULL, &my_state));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_mirror_get_state(g_ma_client, session_id, NULL));
        TEST_ASSERT(MA_OK == ma_repository_mirror_get_state(g_ma_client, session_id, &my_state));
        TEST_ASSERT(MA_REPOSITORY_MIRROR_STATE_TIMEOUT == my_state);
    }

    // Waiting for published state information to be verified.
    wait_until_done();

    /**************************************************************************
    TEST 6 - "Check Repository Mirror Progress State" Test (MA_REPOSITORY_MIRROR_STATE_FINISHED_SUCCEEDED)
    **************************************************************************/
    test_scenario = 6;
    done = 0;

    {
        ma_repository_mirror_state_t my_state = MA_REPOSITORY_MIRROR_STATE_IN_PROGRESS;

        // Check State (using Session ID buffer from previous Test 1)
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_mirror_get_state(NULL, session_id, &my_state));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_mirror_get_state(g_ma_client, NULL, &my_state));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_mirror_get_state(g_ma_client, session_id, NULL));
        TEST_ASSERT(MA_OK == ma_repository_mirror_get_state(g_ma_client, session_id, &my_state));
        TEST_ASSERT(MA_REPOSITORY_MIRROR_STATE_FINISHED_SUCCEEDED == my_state);
    }

    // Waiting for published state information to be verified.
    wait_until_done();

    // Release Session ID buffer and Global task ID string
    TEST_ASSERT(MA_OK == ma_buffer_release(session_id));
    free(global_task_id);
}


/**************************************************************************
Repository Mirror Create Task test
**************************************************************************/
TEST(ma_repository_mirror_tests, repository_mirror_task_create_test)
{
    ma_task_t *my_task = NULL;

    /**************************************************************************
    TEST 1 - When "task_id" argument is not NULL
    **************************************************************************/
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_mirror_task_create(NULL, "{33294-cdklh-348937-cdnlkd-329476}", "c:\\Repository\\Mirror\\Destination\\New Folder", &my_task));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_mirror_task_create(g_ma_client, "{33294-cdklh-348937-cdnlkd-329476}", NULL, &my_task));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_mirror_task_create(g_ma_client, "{33294-cdklh-348937-cdnlkd-329476}", "c:\\Repository\\Mirror\\Destination\\New Folder", NULL));
    TEST_ASSERT(MA_OK == ma_repository_mirror_task_create(g_ma_client, "{33294-cdklh-348937-cdnlkd-329476}", "c:\\Repository\\Mirror\\Destination\\New Folder", &my_task));

    // Verify Task Type
    {
        const char *my_string = NULL;

        TEST_ASSERT(MA_OK == ma_task_get_type(my_task, &my_string));
        TEST_ASSERT(0 == strcmp("ma.mirror.task.type.mirror", my_string));
    }

    // Verify that correct Session ID is returned
    {
        ma_buffer_t *session_id = NULL;
        const char *my_string = NULL;
        size_t my_string_size = 0;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_mirror_task_get_session_id(NULL, &session_id));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_mirror_task_get_session_id(my_task, NULL));
        TEST_ASSERT(MA_OK == ma_repository_mirror_task_get_session_id(my_task, &session_id));

        TEST_ASSERT(MA_OK == ma_buffer_get_string(session_id, &my_string, &my_string_size));
        TEST_ASSERT(0 == strcmp("{33294-cdklh-348937-cdnlkd-329476}", my_string));

        TEST_ASSERT(MA_OK == ma_buffer_release(session_id));
    }

    // Verify the Destination Path
    {
        ma_variant_t *variant_ptr = NULL;
        ma_buffer_t *my_buffer = NULL;
        const char *my_string = NULL;
        size_t my_string_size = 0;

        // Verifying Destination Path
        TEST_ASSERT(MA_OK == ma_task_get_setting(my_task, "MirrorOptions", "szPath", &variant_ptr));
        TEST_ASSERT(MA_OK == ma_variant_get_string_buffer(variant_ptr, &my_buffer));
        TEST_ASSERT(MA_OK == ma_buffer_get_string(my_buffer, &my_string, &my_string_size));
        TEST_ASSERT(0 == strcmp("c:\\Repository\\Mirror\\Destination\\New Folder", my_string));
        TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
        TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));
    }

    TEST_ASSERT(MA_OK == ma_task_release(my_task));

    /**************************************************************************
    TEST 2 - When "task_id" argument is NULL (Scheduler generates a Task ID)
    **************************************************************************/
    TEST_ASSERT(MA_OK == ma_repository_mirror_task_create(g_ma_client, NULL, "c:\\Repository\\Mirror\\Destination1\\New Folder (1)", &my_task));

    // Verify Task Type
    {
        const char *my_string = NULL;

        TEST_ASSERT(MA_OK == ma_task_get_type(my_task, &my_string));
        TEST_ASSERT(0 == strcmp("ma.mirror.task.type.mirror", my_string));
    }

    // Verify that a non-NULL session ID is returned.
    {
        ma_buffer_t *session_id = NULL;
        const char *my_string = NULL;
        size_t my_string_size = 0;

        TEST_ASSERT(MA_OK == ma_repository_mirror_task_get_session_id(my_task, &session_id));

        TEST_ASSERT(MA_OK == ma_buffer_get_string(session_id, &my_string, &my_string_size));
        TEST_ASSERT_NOT_NULL(my_string);

        TEST_ASSERT(MA_OK == ma_buffer_release(session_id));
    }

    // Verify the Destination Path
    {
        ma_variant_t *variant_ptr = NULL;
        ma_buffer_t *my_buffer = NULL;
        const char *my_string = NULL;
        size_t my_string_size = 0;

        // Verifying Destination Path
        TEST_ASSERT(MA_OK == ma_task_get_setting(my_task, "MirrorOptions", "szPath", &variant_ptr));
        TEST_ASSERT(MA_OK == ma_variant_get_string_buffer(variant_ptr, &my_buffer));
        TEST_ASSERT(MA_OK == ma_buffer_get_string(my_buffer, &my_string, &my_string_size));
        TEST_ASSERT(0 == strcmp("c:\\Repository\\Mirror\\Destination1\\New Folder (1)", my_string));
        TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
        TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));
    }

    TEST_ASSERT(MA_OK == ma_task_release(my_task));
}



















