/*****************************************************************************
Repository Client Unit Tests.

Test Approach - This test creates a dummy Repository Service which captures the
information sent from Repository client and verifies it against the expected
properties sent from Repository client.
******************************************************************************/
#include "unity.h"
#include "unity_fixture.h"
#include "unity_internals.h"
#include "ma/repository/ma_repository_client.h"
#include "ma/ma_message.h"
#include "ma_ut_setup.h"
#include <string.h>
#include <uv.h>

TEST_GROUP_RUNNER(ma_repository_client_test_group)
{
    do {RUN_TEST_CASE(ma_repository_client_tests, repository_client_set_repositories_test)} while (0);
    do {RUN_TEST_CASE(ma_repository_client_tests, repository_client_get_repositories_test)} while (0);
    do {RUN_TEST_CASE(ma_repository_client_tests, repository_client_set_proxy_config_test)} while (0);
    do {RUN_TEST_CASE(ma_repository_client_tests, repository_client_get_proxy_config_test)} while (0);
    do {RUN_TEST_CASE(ma_repository_client_tests, repository_client_import_repositories_test)} while (0);
}

/* Sample SiteList

<ns:SiteLists xmlns:ns="naSiteList" GlobalVersion="wQiVG7/WmF6siKTe0UDTVDtTReLhP4ol7ataPZ7w3XA=" LocalVersion="20131019150804" Type="Client" FipsMode="0" EncryptedContent="1" Key=""><SiteList Default="1" Name="Default"><HttpSite Name="McAfeeHttp" ID="McAfeeHttp" Server="update.nai.com:80" Enabled="1" Type="fallback"><RelativePath>Products/CommonUpdater</RelativePath><UseAuth>0</UseAuth><UserName></UserName><Password Encrypted="0"></Password></HttpSite><HttpSite Name="TESTFIPS" ID="TESTFIPS" Server="1.1.1.1:80" Enabled="1" Type="repository"><RelativePath></RelativePath><UseAuth>0</UseAuth><UserName></UserName><Password Encrypted="0"></Password><ExclusionList/></HttpSite><SpipeSite ID="handler_1" Enabled="1" Type="master" Name="ePO_W2K8-EPO50-FIPS" Server="W2K8-ePO50-FIPS:88" ServerIP="10.213.254.69:88" ServerName="W2K8-EPO50-FIPS:88" Version="5.1.0" SecurePort="443" Order="1"><RelativePath>Software</RelativePath></SpipeSite></SiteList><CaBundle><CaCertificate>
MIIDXTCCAkWgAwIBAgIIU/FK/WkmvMIwDQYJKoZIhvcNAQEFBQAwRDEPMA0GA1UE
CgwGTWNBZmVlMQ4wDAYDVQQLDAVPcmlvbjEhMB8GA1UEAwwYT3Jpb25fQ0FfVzJL
OC1lUE81MC1GSVBTMB4XDTcwMDEwMTAwMDAwMFoXDTQzMDkyNjEzNTY1OFowRDEP
MA0GA1UECgwGTWNBZmVlMQ4wDAYDVQQLDAVPcmlvbjEhMB8GA1UEAwwYT3Jpb25f
Q0FfVzJLOC1lUE81MC1GSVBTMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKC
AQEA38J+8ATIhuUrLqKiF0aMA3SNamG0t8FEh+mMdF44ZTkBYMtZ4U7L1A3yXopV
+W3GrY521vNs7kON43MmYsAhuw7YQ0oKq5cX5mj217BEyF9ORNaYDYI0IXEg58rH
Sc3eGP0tTCL6ZUZU08NHfBGbX+xLgN1vLTMd+Fxsxug7Trtiq9ynMswpjSm+wSF6
BcufydLfFl08taAkqZL8wsyyL3C6lFtFRDUpmYyOryn/b5oSL5FOknHFSh7+z2V3
HCkFiuu5gxSaTJXRQ6jLx+8R740FyicTyPvTXqEImwRXDiifNBxiubk0+oqBMABQ
RnlLauPIn8LMAvrYappx1nazsQIDAQABo1MwUTAPBgNVHRMBAf8EBTADAQH/MB0G
A1UdDgQWBBRlpAOb2pkKk+1Yf6r/cIbkBNEAbDAfBgNVHSMEGDAWgBRlpAOb2pkK
k+1Yf6r/cIbkBNEAbDANBgkqhkiG9w0BAQUFAAOCAQEAN6K06Hdtxy0W6YcgIZ//
C/wzTJT0Bb6Udl9If90ahExOdx0BLNGBgxUyeW4xm+nquIvk8cZ75ZsrB69h0LkL
c08/CT2TPSfA31YCTMhyYTaPZ+nKVYjyYQDtsbAmQudHrqr+kCTvC5if0jWJ/Dwl
mB6WUW9diH5N3uCThVxUmV6fT0cKZXV7ycn5DrcXm1Ryw81nGAWlWW9PGqEoN5zU
7WQcJJwryV2mW7CNHDxCaiqtqBV4NqrXBXKpdmCT/VN2jfJkoivK7YhXwjIPcD4R
c6vvjdgQkjYdvtPdmYgtA/NE9VPUA/yCMOGlarThqgba4ERTMdtB8H5wpkkvKLof
tQ==

</CaCertificate><CaCertificate>
MIIDVzCCAj+gAwIBAgIIGuYF085rVQMwDQYJKoZIhvcNAQEFBQAwRDEPMA0GA1UE
CgwGTWNBZmVlMQ4wDAYDVQQLDAVPcmlvbjEhMB8GA1UEAwwYT3Jpb25fQ0FfVzJL
OC1lUE81MC1GSVBTMB4XDTcwMDEwMTAwMDAwMFoXDTQzMDkyNTEzNTkzOVowPjEP
MA0GA1UECgwGTWNBZmVlMQswCQYDVQQLDAJBSDEeMBwGA1UEAwwVQUhfQ0FfVzJL
OC1lUE81MC1GSVBTMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlYHg
i5SqKsc7BWRTP7CJvBisOg0ol/xthn+pU5tMqzdT9rcgqurCghnPoaRl5Rre3uLP
BLUwoPnlqoQWRT91R9wOO4JXnrI3T7Mdy2UL0Nb+sYiS8Ga2apbgmurD4QVelZL3
Hme0YLLuavhBroXS+EQRX4Ix8fKm4m3A5297t8qQT18xVJ0+3eF5ruLTsbFQs8C/
n7DitjwEiTc9r7OwhgEUGfD95IDTsqswMgoy9V6I1LO7rtNExJ84BOW4xgLjc0tl
oypW0dkhzhvgy+LNMSmPf5kFTJF3U7knsOqjanCOQZp9FaAUmuaowgCgfwrO4K2q
M1hzEllPQbKpJGyqkQIDAQABo1MwUTAPBgNVHRMBAf8EBTADAQH/MB0GA1UdDgQW
BBQHQjSjW5Kh+uYxZDhiqNhyB0+/uzAfBgNVHSMEGDAWgBRlpAOb2pkKk+1Yf6r/
cIbkBNEAbDANBgkqhkiG9w0BAQUFAAOCAQEAO8j6LyNotGWT6902bVhRqjn1znrc
KaATsbBydOGu27cE1gRrLu2/EGg52WJuuS4Gm5R2vpLdnqO6EvJ0kqn5LDEIsps1
Mx06GCG1fkzfs/PT5hAPyNMSMPbYe9ZoS8ij0y2zT+W3E18iUXTupTQLEGBK3+cz
bdYspX72+OzojElImWF1px+nRk74fgnnxZha95BEeEMJyDDxtNlokWgUY1gpUenp
m7SO0yyuJnhZoV37jKOPXo+9RHLkXr5nvazAzOi1e9R3+8gQc9ND+lh8Eoss0Oht
HTVDtr1kJy4GJ7Elg009u3Lxwcw2qoSECnLttcPSWdMEAuw6YSy6u6XX0g==

</CaCertificate></CaBundle></ns:SiteLists>
*/

static char sitelist_xml[] = "\x3c\x6e\x73\x3a\x53\x69\x74\x65\x4c\x69\x73\x74\x73\x20\x78\x6d\x6c\x6e\x73\x3a\x6e\x73\x3d\x22\x6e\x61\x53\x69\x74\x65\x4c\x69\x73\x74\x22\x20\x47\x6c\x6f\x62\x61\x6c\x56\x65\x72\x73\x69\x6f\x6e\x3d\x22\x77\x51\x69\x56\x47\x37\x2f\x57\x6d\x46\x36\x73\x69\x4b\x54\x65\x30\x55\x44\x54\x56\x44\x74\x54\x52\x65\x4c\x68\x50\x34\x6f\x6c\x37\x61\x74\x61\x50\x5a\x37\x77\x33\x58\x41\x3d\x22\x20\x4c\x6f\x63\x61\x6c\x56\x65\x72\x73\x69\x6f\x6e\x3d\x22\x32\x30\x31\x33\x31\x30\x31\x39\x31\x35\x30\x38\x30\x34\x22\x20\x54\x79\x70\x65\x3d\x22\x43\x6c\x69\x65\x6e\x74\x22\x20\x46\x69\x70\x73\x4d\x6f\x64\x65\x3d\x22\x30\x22\x20\x45\x6e\x63\x72\x79\x70\x74\x65\x64\x43\x6f\x6e\x74\x65\x6e\x74\x3d\x22\x31\x22\x20\x4b\x65\x79\x3d\x22\x22\x3e\x3c\x53\x69\x74\x65\x4c\x69\x73\x74\x20\x44\x65\x66\x61\x75\x6c\x74\x3d\x22\x31\x22\x20\x4e\x61\x6d\x65\x3d\x22\x44\x65\x66\x61\x75\x6c\x74\x22\x3e\x3c\x48\x74\x74\x70\x53\x69\x74\x65\x20\x4e\x61\x6d\x65\x3d\x22\x4d\x63\x41\x66\x65\x65\x48\x74\x74\x70\x22\x20\x49\x44\x3d\x22\x4d\x63\x41\x66\x65\x65\x48\x74\x74\x70\x22\x20\x53\x65\x72\x76\x65\x72\x3d\x22\x75\x70\x64\x61\x74\x65\x2e\x6e\x61\x69\x2e\x63\x6f\x6d\x3a\x38\x30\x22\x20\x45\x6e\x61\x62\x6c\x65\x64\x3d\x22\x31\x22\x20\x54\x79\x70\x65\x3d\x22\x66\x61\x6c\x6c\x62\x61\x63\x6b\x22\x3e\x3c\x52\x65\x6c\x61\x74\x69\x76\x65\x50\x61\x74\x68\x3e\x50\x72\x6f\x64\x75\x63\x74\x73\x2f\x43\x6f\x6d\x6d\x6f\x6e\x55\x70\x64\x61\x74\x65\x72\x3c\x2f\x52\x65\x6c\x61\x74\x69\x76\x65\x50\x61\x74\x68\x3e\x3c\x55\x73\x65\x41\x75\x74\x68\x3e\x30\x3c\x2f\x55\x73\x65\x41\x75\x74\x68\x3e\x3c\x55\x73\x65\x72\x4e\x61\x6d\x65\x3e\x3c\x2f\x55\x73\x65\x72\x4e\x61\x6d\x65\x3e\x3c\x50\x61\x73\x73\x77\x6f\x72\x64\x20\x45\x6e\x63\x72\x79\x70\x74\x65\x64\x3d\x22\x30\x22\x3e\x3c\x2f\x50\x61\x73\x73\x77\x6f\x72\x64\x3e\x3c\x2f\x48\x74\x74\x70\x53\x69\x74\x65\x3e\x3c\x48\x74\x74\x70\x53\x69\x74\x65\x20\x4e\x61\x6d\x65\x3d\x22\x54\x45\x53\x54\x46\x49\x50\x53\x22\x20\x49\x44\x3d\x22\x54\x45\x53\x54\x46\x49\x50\x53\x22\x20\x53\x65\x72\x76\x65\x72\x3d\x22\x31\x2e\x31\x2e\x31\x2e\x31\x3a\x38\x30\x22\x20\x45\x6e\x61\x62\x6c\x65\x64\x3d\x22\x31\x22\x20\x54\x79\x70\x65\x3d\x22\x72\x65\x70\x6f\x73\x69\x74\x6f\x72\x79\x22\x3e\x3c\x52\x65\x6c\x61\x74\x69\x76\x65\x50\x61\x74\x68\x3e\x3c\x2f\x52\x65\x6c\x61\x74\x69\x76\x65\x50\x61\x74\x68\x3e\x3c\x55\x73\x65\x41\x75\x74\x68\x3e\x30\x3c\x2f\x55\x73\x65\x41\x75\x74\x68\x3e\x3c\x55\x73\x65\x72\x4e\x61\x6d\x65\x3e\x3c\x2f\x55\x73\x65\x72\x4e\x61\x6d\x65\x3e\x3c\x50\x61\x73\x73\x77\x6f\x72\x64\x20\x45\x6e\x63\x72\x79\x70\x74\x65\x64\x3d\x22\x30\x22\x3e\x3c\x2f\x50\x61\x73\x73\x77\x6f\x72\x64\x3e\x3c\x45\x78\x63\x6c\x75\x73\x69\x6f\x6e\x4c\x69\x73\x74\x2f\x3e\x3c\x2f\x48\x74\x74\x70\x53\x69\x74\x65\x3e\x3c\x53\x70\x69\x70\x65\x53\x69\x74\x65\x20\x49\x44\x3d\x22\x68\x61\x6e\x64\x6c\x65\x72\x5f\x31\x22\x20\x45\x6e\x61\x62\x6c\x65\x64\x3d\x22\x31\x22\x20\x54\x79\x70\x65\x3d\x22\x6d\x61\x73\x74\x65\x72\x22\x20\x4e\x61\x6d\x65\x3d\x22\x65\x50\x4f\x5f\x57\x32\x4b\x38\x2d\x45\x50\x4f\x35\x30\x2d\x46\x49\x50\x53\x22\x20\x53\x65\x72\x76\x65\x72\x3d\x22\x57\x32\x4b\x38\x2d\x65\x50\x4f\x35\x30\x2d\x46\x49\x50\x53\x3a\x38\x38\x22\x20\x53\x65\x72\x76\x65\x72\x49\x50\x3d\x22\x31\x30\x2e\x32\x31\x33\x2e\x32\x35\x34\x2e\x36\x39\x3a\x38\x38\x22\x20\x53\x65\x72\x76\x65\x72\x4e\x61\x6d\x65\x3d\x22\x57\x32\x4b\x38\x2d\x45\x50\x4f\x35\x30\x2d\x46\x49\x50\x53\x3a\x38\x38\x22\x20\x56\x65\x72\x73\x69\x6f\x6e\x3d\x22\x35\x2e\x31\x2e\x30\x22\x20\x53\x65\x63\x75\x72\x65\x50\x6f\x72\x74\x3d\x22\x34\x34\x33\x22\x20\x4f\x72\x64\x65\x72\x3d\x22\x31\x22\x3e\x3c\x52\x65\x6c\x61\x74\x69\x76\x65\x50\x61\x74\x68\x3e\x53\x6f\x66\x74\x77\x61\x72\x65\x3c\x2f\x52\x65\x6c\x61\x74\x69\x76\x65\x50\x61\x74\x68\x3e\x3c\x2f\x53\x70\x69\x70\x65\x53\x69\x74\x65\x3e\x3c\x2f\x53\x69\x74\x65\x4c\x69\x73\x74\x3e\x3c\x43\x61\x42\x75\x6e\x64\x6c\x65\x3e\x3c\x43\x61\x43\x65\x72\x74\x69\x66\x69\x63\x61\x74\x65\x3e\x0d\x0a\x4d\x49\x49\x44\x58\x54\x43\x43\x41\x6b\x57\x67\x41\x77\x49\x42\x41\x67\x49\x49\x55\x2f\x46\x4b\x2f\x57\x6b\x6d\x76\x4d\x49\x77\x44\x51\x59\x4a\x4b\x6f\x5a\x49\x68\x76\x63\x4e\x41\x51\x45\x46\x42\x51\x41\x77\x52\x44\x45\x50\x4d\x41\x30\x47\x41\x31\x55\x45\x0d\x0a\x43\x67\x77\x47\x54\x57\x4e\x42\x5a\x6d\x56\x6c\x4d\x51\x34\x77\x44\x41\x59\x44\x56\x51\x51\x4c\x44\x41\x56\x50\x63\x6d\x6c\x76\x62\x6a\x45\x68\x4d\x42\x38\x47\x41\x31\x55\x45\x41\x77\x77\x59\x54\x33\x4a\x70\x62\x32\x35\x66\x51\x30\x46\x66\x56\x7a\x4a\x4c\x0d\x0a\x4f\x43\x31\x6c\x55\x45\x38\x31\x4d\x43\x31\x47\x53\x56\x42\x54\x4d\x42\x34\x58\x44\x54\x63\x77\x4d\x44\x45\x77\x4d\x54\x41\x77\x4d\x44\x41\x77\x4d\x46\x6f\x58\x44\x54\x51\x7a\x4d\x44\x6b\x79\x4e\x6a\x45\x7a\x4e\x54\x59\x31\x4f\x46\x6f\x77\x52\x44\x45\x50\x0d\x0a\x4d\x41\x30\x47\x41\x31\x55\x45\x43\x67\x77\x47\x54\x57\x4e\x42\x5a\x6d\x56\x6c\x4d\x51\x34\x77\x44\x41\x59\x44\x56\x51\x51\x4c\x44\x41\x56\x50\x63\x6d\x6c\x76\x62\x6a\x45\x68\x4d\x42\x38\x47\x41\x31\x55\x45\x41\x77\x77\x59\x54\x33\x4a\x70\x62\x32\x35\x66\x0d\x0a\x51\x30\x46\x66\x56\x7a\x4a\x4c\x4f\x43\x31\x6c\x55\x45\x38\x31\x4d\x43\x31\x47\x53\x56\x42\x54\x4d\x49\x49\x42\x49\x6a\x41\x4e\x42\x67\x6b\x71\x68\x6b\x69\x47\x39\x77\x30\x42\x41\x51\x45\x46\x41\x41\x4f\x43\x41\x51\x38\x41\x4d\x49\x49\x42\x43\x67\x4b\x43\x0d\x0a\x41\x51\x45\x41\x33\x38\x4a\x2b\x38\x41\x54\x49\x68\x75\x55\x72\x4c\x71\x4b\x69\x46\x30\x61\x4d\x41\x33\x53\x4e\x61\x6d\x47\x30\x74\x38\x46\x45\x68\x2b\x6d\x4d\x64\x46\x34\x34\x5a\x54\x6b\x42\x59\x4d\x74\x5a\x34\x55\x37\x4c\x31\x41\x33\x79\x58\x6f\x70\x56\x0d\x0a\x2b\x57\x33\x47\x72\x59\x35\x32\x31\x76\x4e\x73\x37\x6b\x4f\x4e\x34\x33\x4d\x6d\x59\x73\x41\x68\x75\x77\x37\x59\x51\x30\x6f\x4b\x71\x35\x63\x58\x35\x6d\x6a\x32\x31\x37\x42\x45\x79\x46\x39\x4f\x52\x4e\x61\x59\x44\x59\x49\x30\x49\x58\x45\x67\x35\x38\x72\x48\x0d\x0a\x53\x63\x33\x65\x47\x50\x30\x74\x54\x43\x4c\x36\x5a\x55\x5a\x55\x30\x38\x4e\x48\x66\x42\x47\x62\x58\x2b\x78\x4c\x67\x4e\x31\x76\x4c\x54\x4d\x64\x2b\x46\x78\x73\x78\x75\x67\x37\x54\x72\x74\x69\x71\x39\x79\x6e\x4d\x73\x77\x70\x6a\x53\x6d\x2b\x77\x53\x46\x36\x0d\x0a\x42\x63\x75\x66\x79\x64\x4c\x66\x46\x6c\x30\x38\x74\x61\x41\x6b\x71\x5a\x4c\x38\x77\x73\x79\x79\x4c\x33\x43\x36\x6c\x46\x74\x46\x52\x44\x55\x70\x6d\x59\x79\x4f\x72\x79\x6e\x2f\x62\x35\x6f\x53\x4c\x35\x46\x4f\x6b\x6e\x48\x46\x53\x68\x37\x2b\x7a\x32\x56\x33\x0d\x0a\x48\x43\x6b\x46\x69\x75\x75\x35\x67\x78\x53\x61\x54\x4a\x58\x52\x51\x36\x6a\x4c\x78\x2b\x38\x52\x37\x34\x30\x46\x79\x69\x63\x54\x79\x50\x76\x54\x58\x71\x45\x49\x6d\x77\x52\x58\x44\x69\x69\x66\x4e\x42\x78\x69\x75\x62\x6b\x30\x2b\x6f\x71\x42\x4d\x41\x42\x51\x0d\x0a\x52\x6e\x6c\x4c\x61\x75\x50\x49\x6e\x38\x4c\x4d\x41\x76\x72\x59\x61\x70\x70\x78\x31\x6e\x61\x7a\x73\x51\x49\x44\x41\x51\x41\x42\x6f\x31\x4d\x77\x55\x54\x41\x50\x42\x67\x4e\x56\x48\x52\x4d\x42\x41\x66\x38\x45\x42\x54\x41\x44\x41\x51\x48\x2f\x4d\x42\x30\x47\x0d\x0a\x41\x31\x55\x64\x44\x67\x51\x57\x42\x42\x52\x6c\x70\x41\x4f\x62\x32\x70\x6b\x4b\x6b\x2b\x31\x59\x66\x36\x72\x2f\x63\x49\x62\x6b\x42\x4e\x45\x41\x62\x44\x41\x66\x42\x67\x4e\x56\x48\x53\x4d\x45\x47\x44\x41\x57\x67\x42\x52\x6c\x70\x41\x4f\x62\x32\x70\x6b\x4b\x0d\x0a\x6b\x2b\x31\x59\x66\x36\x72\x2f\x63\x49\x62\x6b\x42\x4e\x45\x41\x62\x44\x41\x4e\x42\x67\x6b\x71\x68\x6b\x69\x47\x39\x77\x30\x42\x41\x51\x55\x46\x41\x41\x4f\x43\x41\x51\x45\x41\x4e\x36\x4b\x30\x36\x48\x64\x74\x78\x79\x30\x57\x36\x59\x63\x67\x49\x5a\x2f\x2f\x0d\x0a\x43\x2f\x77\x7a\x54\x4a\x54\x30\x42\x62\x36\x55\x64\x6c\x39\x49\x66\x39\x30\x61\x68\x45\x78\x4f\x64\x78\x30\x42\x4c\x4e\x47\x42\x67\x78\x55\x79\x65\x57\x34\x78\x6d\x2b\x6e\x71\x75\x49\x76\x6b\x38\x63\x5a\x37\x35\x5a\x73\x72\x42\x36\x39\x68\x30\x4c\x6b\x4c\x0d\x0a\x63\x30\x38\x2f\x43\x54\x32\x54\x50\x53\x66\x41\x33\x31\x59\x43\x54\x4d\x68\x79\x59\x54\x61\x50\x5a\x2b\x6e\x4b\x56\x59\x6a\x79\x59\x51\x44\x74\x73\x62\x41\x6d\x51\x75\x64\x48\x72\x71\x72\x2b\x6b\x43\x54\x76\x43\x35\x69\x66\x30\x6a\x57\x4a\x2f\x44\x77\x6c\x0d\x0a\x6d\x42\x36\x57\x55\x57\x39\x64\x69\x48\x35\x4e\x33\x75\x43\x54\x68\x56\x78\x55\x6d\x56\x36\x66\x54\x30\x63\x4b\x5a\x58\x56\x37\x79\x63\x6e\x35\x44\x72\x63\x58\x6d\x31\x52\x79\x77\x38\x31\x6e\x47\x41\x57\x6c\x57\x57\x39\x50\x47\x71\x45\x6f\x4e\x35\x7a\x55\x0d\x0a\x37\x57\x51\x63\x4a\x4a\x77\x72\x79\x56\x32\x6d\x57\x37\x43\x4e\x48\x44\x78\x43\x61\x69\x71\x74\x71\x42\x56\x34\x4e\x71\x72\x58\x42\x58\x4b\x70\x64\x6d\x43\x54\x2f\x56\x4e\x32\x6a\x66\x4a\x6b\x6f\x69\x76\x4b\x37\x59\x68\x58\x77\x6a\x49\x50\x63\x44\x34\x52\x0d\x0a\x63\x36\x76\x76\x6a\x64\x67\x51\x6b\x6a\x59\x64\x76\x74\x50\x64\x6d\x59\x67\x74\x41\x2f\x4e\x45\x39\x56\x50\x55\x41\x2f\x79\x43\x4d\x4f\x47\x6c\x61\x72\x54\x68\x71\x67\x62\x61\x34\x45\x52\x54\x4d\x64\x74\x42\x38\x48\x35\x77\x70\x6b\x6b\x76\x4b\x4c\x6f\x66\x0d\x0a\x74\x51\x3d\x3d\x0d\x0a\x0d\x0a\x3c\x2f\x43\x61\x43\x65\x72\x74\x69\x66\x69\x63\x61\x74\x65\x3e\x3c\x43\x61\x43\x65\x72\x74\x69\x66\x69\x63\x61\x74\x65\x3e\x0d\x0a\x4d\x49\x49\x44\x56\x7a\x43\x43\x41\x6a\x2b\x67\x41\x77\x49\x42\x41\x67\x49\x49\x47\x75\x59\x46\x30\x38\x35\x72\x56\x51\x4d\x77\x44\x51\x59\x4a\x4b\x6f\x5a\x49\x68\x76\x63\x4e\x41\x51\x45\x46\x42\x51\x41\x77\x52\x44\x45\x50\x4d\x41\x30\x47\x41\x31\x55\x45\x0d\x0a\x43\x67\x77\x47\x54\x57\x4e\x42\x5a\x6d\x56\x6c\x4d\x51\x34\x77\x44\x41\x59\x44\x56\x51\x51\x4c\x44\x41\x56\x50\x63\x6d\x6c\x76\x62\x6a\x45\x68\x4d\x42\x38\x47\x41\x31\x55\x45\x41\x77\x77\x59\x54\x33\x4a\x70\x62\x32\x35\x66\x51\x30\x46\x66\x56\x7a\x4a\x4c\x0d\x0a\x4f\x43\x31\x6c\x55\x45\x38\x31\x4d\x43\x31\x47\x53\x56\x42\x54\x4d\x42\x34\x58\x44\x54\x63\x77\x4d\x44\x45\x77\x4d\x54\x41\x77\x4d\x44\x41\x77\x4d\x46\x6f\x58\x44\x54\x51\x7a\x4d\x44\x6b\x79\x4e\x54\x45\x7a\x4e\x54\x6b\x7a\x4f\x56\x6f\x77\x50\x6a\x45\x50\x0d\x0a\x4d\x41\x30\x47\x41\x31\x55\x45\x43\x67\x77\x47\x54\x57\x4e\x42\x5a\x6d\x56\x6c\x4d\x51\x73\x77\x43\x51\x59\x44\x56\x51\x51\x4c\x44\x41\x4a\x42\x53\x44\x45\x65\x4d\x42\x77\x47\x41\x31\x55\x45\x41\x77\x77\x56\x51\x55\x68\x66\x51\x30\x46\x66\x56\x7a\x4a\x4c\x0d\x0a\x4f\x43\x31\x6c\x55\x45\x38\x31\x4d\x43\x31\x47\x53\x56\x42\x54\x4d\x49\x49\x42\x49\x6a\x41\x4e\x42\x67\x6b\x71\x68\x6b\x69\x47\x39\x77\x30\x42\x41\x51\x45\x46\x41\x41\x4f\x43\x41\x51\x38\x41\x4d\x49\x49\x42\x43\x67\x4b\x43\x41\x51\x45\x41\x6c\x59\x48\x67\x0d\x0a\x69\x35\x53\x71\x4b\x73\x63\x37\x42\x57\x52\x54\x50\x37\x43\x4a\x76\x42\x69\x73\x4f\x67\x30\x6f\x6c\x2f\x78\x74\x68\x6e\x2b\x70\x55\x35\x74\x4d\x71\x7a\x64\x54\x39\x72\x63\x67\x71\x75\x72\x43\x67\x68\x6e\x50\x6f\x61\x52\x6c\x35\x52\x72\x65\x33\x75\x4c\x50\x0d\x0a\x42\x4c\x55\x77\x6f\x50\x6e\x6c\x71\x6f\x51\x57\x52\x54\x39\x31\x52\x39\x77\x4f\x4f\x34\x4a\x58\x6e\x72\x49\x33\x54\x37\x4d\x64\x79\x32\x55\x4c\x30\x4e\x62\x2b\x73\x59\x69\x53\x38\x47\x61\x32\x61\x70\x62\x67\x6d\x75\x72\x44\x34\x51\x56\x65\x6c\x5a\x4c\x33\x0d\x0a\x48\x6d\x65\x30\x59\x4c\x4c\x75\x61\x76\x68\x42\x72\x6f\x58\x53\x2b\x45\x51\x52\x58\x34\x49\x78\x38\x66\x4b\x6d\x34\x6d\x33\x41\x35\x32\x39\x37\x74\x38\x71\x51\x54\x31\x38\x78\x56\x4a\x30\x2b\x33\x65\x46\x35\x72\x75\x4c\x54\x73\x62\x46\x51\x73\x38\x43\x2f\x0d\x0a\x6e\x37\x44\x69\x74\x6a\x77\x45\x69\x54\x63\x39\x72\x37\x4f\x77\x68\x67\x45\x55\x47\x66\x44\x39\x35\x49\x44\x54\x73\x71\x73\x77\x4d\x67\x6f\x79\x39\x56\x36\x49\x31\x4c\x4f\x37\x72\x74\x4e\x45\x78\x4a\x38\x34\x42\x4f\x57\x34\x78\x67\x4c\x6a\x63\x30\x74\x6c\x0d\x0a\x6f\x79\x70\x57\x30\x64\x6b\x68\x7a\x68\x76\x67\x79\x2b\x4c\x4e\x4d\x53\x6d\x50\x66\x35\x6b\x46\x54\x4a\x46\x33\x55\x37\x6b\x6e\x73\x4f\x71\x6a\x61\x6e\x43\x4f\x51\x5a\x70\x39\x46\x61\x41\x55\x6d\x75\x61\x6f\x77\x67\x43\x67\x66\x77\x72\x4f\x34\x4b\x32\x71\x0d\x0a\x4d\x31\x68\x7a\x45\x6c\x6c\x50\x51\x62\x4b\x70\x4a\x47\x79\x71\x6b\x51\x49\x44\x41\x51\x41\x42\x6f\x31\x4d\x77\x55\x54\x41\x50\x42\x67\x4e\x56\x48\x52\x4d\x42\x41\x66\x38\x45\x42\x54\x41\x44\x41\x51\x48\x2f\x4d\x42\x30\x47\x41\x31\x55\x64\x44\x67\x51\x57\x0d\x0a\x42\x42\x51\x48\x51\x6a\x53\x6a\x57\x35\x4b\x68\x2b\x75\x59\x78\x5a\x44\x68\x69\x71\x4e\x68\x79\x42\x30\x2b\x2f\x75\x7a\x41\x66\x42\x67\x4e\x56\x48\x53\x4d\x45\x47\x44\x41\x57\x67\x42\x52\x6c\x70\x41\x4f\x62\x32\x70\x6b\x4b\x6b\x2b\x31\x59\x66\x36\x72\x2f\x0d\x0a\x63\x49\x62\x6b\x42\x4e\x45\x41\x62\x44\x41\x4e\x42\x67\x6b\x71\x68\x6b\x69\x47\x39\x77\x30\x42\x41\x51\x55\x46\x41\x41\x4f\x43\x41\x51\x45\x41\x4f\x38\x6a\x36\x4c\x79\x4e\x6f\x74\x47\x57\x54\x36\x39\x30\x32\x62\x56\x68\x52\x71\x6a\x6e\x31\x7a\x6e\x72\x63\x0d\x0a\x4b\x61\x41\x54\x73\x62\x42\x79\x64\x4f\x47\x75\x32\x37\x63\x45\x31\x67\x52\x72\x4c\x75\x32\x2f\x45\x47\x67\x35\x32\x57\x4a\x75\x75\x53\x34\x47\x6d\x35\x52\x32\x76\x70\x4c\x64\x6e\x71\x4f\x36\x45\x76\x4a\x30\x6b\x71\x6e\x35\x4c\x44\x45\x49\x73\x70\x73\x31\x0d\x0a\x4d\x78\x30\x36\x47\x43\x47\x31\x66\x6b\x7a\x66\x73\x2f\x50\x54\x35\x68\x41\x50\x79\x4e\x4d\x53\x4d\x50\x62\x59\x65\x39\x5a\x6f\x53\x38\x69\x6a\x30\x79\x32\x7a\x54\x2b\x57\x33\x45\x31\x38\x69\x55\x58\x54\x75\x70\x54\x51\x4c\x45\x47\x42\x4b\x33\x2b\x63\x7a\x0d\x0a\x62\x64\x59\x73\x70\x58\x37\x32\x2b\x4f\x7a\x6f\x6a\x45\x6c\x49\x6d\x57\x46\x31\x70\x78\x2b\x6e\x52\x6b\x37\x34\x66\x67\x6e\x6e\x78\x5a\x68\x61\x39\x35\x42\x45\x65\x45\x4d\x4a\x79\x44\x44\x78\x74\x4e\x6c\x6f\x6b\x57\x67\x55\x59\x31\x67\x70\x55\x65\x6e\x70\x0d\x0a\x6d\x37\x53\x4f\x30\x79\x79\x75\x4a\x6e\x68\x5a\x6f\x56\x33\x37\x6a\x4b\x4f\x50\x58\x6f\x2b\x39\x52\x48\x4c\x6b\x58\x72\x35\x6e\x76\x61\x7a\x41\x7a\x4f\x69\x31\x65\x39\x52\x33\x2b\x38\x67\x51\x63\x39\x4e\x44\x2b\x6c\x68\x38\x45\x6f\x73\x73\x30\x4f\x68\x74\x0d\x0a\x48\x54\x56\x44\x74\x72\x31\x6b\x4a\x79\x34\x47\x4a\x37\x45\x6c\x67\x30\x30\x39\x75\x33\x4c\x78\x77\x63\x77\x32\x71\x6f\x53\x45\x43\x6e\x4c\x74\x74\x63\x50\x53\x57\x64\x4d\x45\x41\x75\x77\x36\x59\x53\x79\x36\x75\x36\x58\x58\x30\x67\x3d\x3d\x0d\x0a\x0d\x0a\x3c\x2f\x43\x61\x43\x65\x72\x74\x69\x66\x69\x63\x61\x74\x65\x3e\x3c\x2f\x43\x61\x42\x75\x6e\x64\x6c\x65\x3e\x3c\x2f\x6e\x73\x3a\x53\x69\x74\x65\x4c\x69\x73\x74\x73\x3e\x0d\x0a";

static ma_client_t *g_ma_client = NULL;
static ma_msgbus_t *g_msgbus = NULL;
static ma_ut_setup_t *g_ut_setup  = NULL;
static const char service_name[] = "ma.service.repository";
static const char product_id[] = "ma.repository.client.test";
static ma_msgbus_server_t *server = NULL;

static ma_bool_t done = 0;
static uv_cond_t cond;
static uv_mutex_t mux;

static void wait_until_done()
{
    uv_mutex_lock(&mux);
    while (!done)
    {
        uv_cond_wait(&cond,&mux);
    }

    uv_mutex_unlock(&mux);
}

static void set_done()
{
    uv_mutex_lock(&mux);
    done = MA_TRUE;
    uv_cond_signal(&cond);
    uv_mutex_unlock(&mux);
}

static ma_error_t server_cb(ma_msgbus_server_t *server, ma_message_t *request_message, void *cb_data, ma_msgbus_client_request_t *c_request);

static ma_uint8_t scenario = 0;

/**************************************************************************
1) Creating the unit test context.
2) Retrieving underlying message bus instance.
3) Creating and starting message bus service (Repository Service) using the
   message bus instance.
**************************************************************************/
TEST_SETUP(ma_repository_client_tests)
{
    uv_mutex_init(&mux);
    uv_cond_init(&cond);

    ma_ut_setup_create("repository_client.repository_client.tests", product_id, &g_ut_setup);
    g_ma_client = ma_ut_setup_get_client(g_ut_setup);
    g_msgbus = ma_ut_setup_get_msgbus(g_ut_setup);
    ma_msgbus_server_create(g_msgbus, service_name, &server);
    ma_msgbus_server_start(server, &server_cb, NULL);
}

/**************************************************************************
1) Stopping and releasing the message bus service (Repository service).
2) Releasing the context.
**************************************************************************/
TEST_TEAR_DOWN(ma_repository_client_tests)
{
    ma_msgbus_server_stop(server);
    ma_msgbus_server_release(server);

    ma_ut_setup_release(g_ut_setup);

    uv_cond_destroy(&cond);
    uv_mutex_destroy(&mux);
}

/**************************************************************************
List of repositories
**************************************************************************/
static const char *rep_name_list[] = { "Repository_1", "Repository_2", "Repository_3", "Repository_4", "Repository_5",
                                       "Repository_6", "Repository_7", "Repository_8", "Repository_9", "Repository_10" };

// For capturing the repository list variant (in the between the test scenarios).
static ma_variant_t *repository_list_variant_obj = NULL;

// For capturing the proxy config variant (in the between the test scenarios).
static ma_variant_t *proxy_config_variant_obj = NULL;

/******************************************************************************
Dummy implementation of a Repository Service - Call back function to be executed
when the Repository Service receives a request from the repository client. Various
scenarios and corresponding behavior of this service, is listed below -

1) Scenario 1 - When repository service receives a new repository list. In this
   case, this test verifies the request type and then stores the received
   repository list variant.
2) Scenario 2 - When repository service receives a request to fetch the
   repository list. In this case, this test returns the repository list variant
   (captured in Scenario 1) as the payload in the response.
3) Scenario 3 - When repository service receives a new proxy configuration. In
   this case, this test verifies the request type and then stores the received
   proxy config variant.
4) Scenario 4 - When repository service receives a request to fetch the
   proxy configuration. In this case, this test returns the proxy configuration
   variant (captured in Scenario 3) as the payload in the response.
5) Scenario 5 - When repository service receives a sitelist. In this case, this
   test verifies the request type and then verifies the sitelist received in the
   payload against the original one.
******************************************************************************/
static ma_error_t server_cb(ma_msgbus_server_t *server, ma_message_t *repository_payload, void *cb_data, ma_msgbus_client_request_t *c_request)
{
    if(scenario == 1)
    {
        // Verifying the request type
        {
            const char *request_type = NULL;
            TEST_ASSERT(MA_OK == ma_message_get_property(repository_payload, "prop.key.msg_type", &request_type));
            TEST_ASSERT(strcmp("prop.val.rqst.repositories.update", request_type) == 0);
        }

        // Verifying the Product ID
        {
            const char *repository_value = NULL;
            TEST_ASSERT(MA_OK == ma_message_get_property(repository_payload, "prop.key.product_id", &repository_value));
            TEST_ASSERT(strcmp(product_id, repository_value) == 0);
        }

        // Capturing the repository list (in a global object) sent to this repository service
        TEST_ASSERT(MA_OK == ma_message_get_payload(repository_payload, &repository_list_variant_obj));

        // Sending the MA_OK response with dummy variant payload
        {
            ma_message_t* response_payload = NULL;
            ma_variant_t *repository_list_variant_obj_local = NULL;

            ma_message_create(&response_payload);
            ma_variant_create_from_string("Repository List Received", strlen("Repository List Received"), &repository_list_variant_obj_local);
            ma_message_set_payload(response_payload, repository_list_variant_obj_local);
            ma_msgbus_server_client_request_post_result(c_request, MA_OK, response_payload);
            ma_variant_release(repository_list_variant_obj_local);
            ma_message_release(response_payload);
            repository_list_variant_obj_local = NULL;
            response_payload = NULL;
        }

    }

    if(scenario == 2)
    {
        // Verifying the request type
        {
            const char *request_type = NULL;
            TEST_ASSERT(MA_OK == ma_message_get_property(repository_payload, "prop.key.msg_type", &request_type));
            TEST_ASSERT(strcmp("prop.val.rqst.repositories.get", request_type) == 0);
        }

        // Verifying the Product ID
        {
            const char *repository_value = NULL;
            TEST_ASSERT(MA_OK == ma_message_get_property(repository_payload, "prop.key.product_id", &repository_value));
            TEST_ASSERT(strcmp(product_id, repository_value) == 0);
        }

        // Sending the MA_OK response with the repository variant sent to server in previous scenario
        {
            ma_message_t* response_payload = NULL;
            ma_message_create(&response_payload);
            ma_message_set_payload(response_payload, repository_list_variant_obj);
            ma_msgbus_server_client_request_post_result(c_request, MA_OK, response_payload);
            ma_message_release(response_payload);
            response_payload = NULL;
            ma_variant_release(repository_list_variant_obj);
        }
    }

    if(scenario == 3)
    {
        // Verifying the request type
        {
            const char *request_type = NULL;
            TEST_ASSERT(MA_OK == ma_message_get_property(repository_payload, "prop.key.msg_type", &request_type));
            TEST_ASSERT(strcmp("prop.val.rqst.proxies.update", request_type) == 0);
        }

        // Verifying the Product ID
        {
            const char *repository_value = NULL;
            TEST_ASSERT(MA_OK == ma_message_get_property(repository_payload, "prop.key.product_id", &repository_value));
            TEST_ASSERT(strcmp(product_id, repository_value) == 0);
        }

        // Capturing the proxy configuration (in a global object) sent to this repository service
        TEST_ASSERT(MA_OK == ma_message_get_payload(repository_payload, &proxy_config_variant_obj));

        // Sending the MA_OK response with dummy variant payload
        {
            ma_message_t* response_payload = NULL;
            ma_variant_t *proxy_config_variant_obj_local = NULL;

            ma_message_create(&response_payload);
            ma_variant_create_from_string("Proxy Configuration Information received", strlen("Proxy Configuration Information received"), &proxy_config_variant_obj_local);
            ma_message_set_payload(response_payload, proxy_config_variant_obj_local);
            ma_msgbus_server_client_request_post_result(c_request, MA_OK, response_payload);
            ma_variant_release(proxy_config_variant_obj_local);
            ma_message_release(response_payload);
            proxy_config_variant_obj_local = NULL;
            response_payload = NULL;
        }

    }

    if(scenario == 4)
    {
        // Verifying the request type
        {
            const char *request_type = NULL;
            TEST_ASSERT(MA_OK == ma_message_get_property(repository_payload, "prop.key.msg_type", &request_type));
            TEST_ASSERT(strcmp("prop.val.rqst.proxies.get", request_type) == 0);
        }

        // Verifying the Product ID
        {
            const char *repository_value = NULL;
            TEST_ASSERT(MA_OK == ma_message_get_property(repository_payload, "prop.key.product_id", &repository_value));
            TEST_ASSERT(strcmp(product_id, repository_value) == 0);
        }

        // Sending the MA_OK response with the repository variant sent to server in previous scenario
        {
            ma_message_t* response_payload = NULL;
            ma_message_create(&response_payload);
            ma_message_set_payload(response_payload, proxy_config_variant_obj);
            ma_msgbus_server_client_request_post_result(c_request, MA_OK, response_payload);
            ma_message_release(response_payload);
            response_payload = NULL;
            ma_variant_release(proxy_config_variant_obj);
        }
    }

    if(scenario == 5)
    {
        // Verifying the request type
        {
            const char *request_type = NULL;
            TEST_ASSERT(MA_OK == ma_message_get_property(repository_payload, "prop.key.msg_type", &request_type));
            TEST_ASSERT(strcmp("prop.val.rqst.repositories.import", request_type) == 0);
        }

        // Verifying the Product ID
        {
            const char *repository_value = NULL;
            TEST_ASSERT(MA_OK == ma_message_get_property(repository_payload, "prop.key.product_id", &repository_value));
            TEST_ASSERT(strcmp(product_id, repository_value) == 0);
        }

        // Retrieving the payload and verifying it against original sitelist
        {
            ma_variant_t *variant_obj = NULL;
            ma_buffer_t *buffer_ptr = NULL;
            const char *string_val = NULL;
            size_t string_size = 0;
            TEST_ASSERT(MA_OK == ma_message_get_payload(repository_payload, &variant_obj));
            TEST_ASSERT(MA_OK == ma_variant_get_string_buffer(variant_obj, &buffer_ptr));
            TEST_ASSERT(MA_OK == ma_buffer_get_string(buffer_ptr, &string_val, &string_size));
            TEST_ASSERT(0 == strcmp(string_val, sitelist_xml));
            TEST_ASSERT(MA_OK == ma_buffer_release(buffer_ptr));
            TEST_ASSERT(MA_OK == ma_variant_release(variant_obj));
        }

        // Sending the MA_OK response with dummy variant payload
        {
            ma_message_t* response_payload = NULL;
            ma_variant_t *variant_obj_local = NULL;

            ma_message_create(&response_payload);
            ma_variant_create_from_string("Sitelist Received", strlen("Sitelist Received"), &variant_obj_local);
            ma_message_set_payload(response_payload, variant_obj_local);
            ma_msgbus_server_client_request_post_result(c_request, MA_OK, response_payload);
            ma_variant_release(variant_obj_local);
            ma_message_release(response_payload);
            variant_obj_local = NULL;
            response_payload = NULL;
        }
    }


    // Control flow is complete, therefore, unlock the mutex now, so that control can exit.
    set_done();

    return MA_OK;
}

/**************************************************************************
Sending the Repository List to the Server
**************************************************************************/
TEST(ma_repository_client_tests, repository_client_set_repositories_test)
{
    ma_uint8_t counter = 0;

    ma_repository_t *repositories[sizeof(rep_name_list)/sizeof(rep_name_list[0])];

    // Repository List
    ma_int32_t repository_count = -1;
    ma_repository_list_t *my_repository_list = NULL;

    scenario = 1;

    // Creating repositories and setting the repository names
    for(counter = 0; counter < sizeof(rep_name_list)/sizeof(rep_name_list[0]); counter++)
    {
        TEST_ASSERT(MA_OK == ma_repository_create(&repositories[counter]));
        TEST_ASSERT(MA_OK == ma_repository_set_name(repositories[counter], rep_name_list[counter]));
    }

    // Creating repository list
    TEST_ASSERT(MA_OK == ma_repository_list_create(&my_repository_list));

    // Adding repositories to repository list, and then releasing the repository
    for(counter = 0; counter < sizeof(rep_name_list)/sizeof(rep_name_list[0]); counter++)
    {
        TEST_ASSERT(MA_OK == ma_repository_list_add_repository(my_repository_list, repositories[counter]));
        TEST_ASSERT(MA_OK == ma_repository_release(repositories[counter]));
    }

    // Sending repository list to Repository service
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_client_set_repositories(g_ma_client, NULL));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_client_set_repositories(NULL, my_repository_list));
    TEST_ASSERT(MA_OK == ma_repository_client_set_repositories(g_ma_client, my_repository_list));

    wait_until_done();

    // Releasing Repository List
    TEST_ASSERT(MA_OK == ma_repository_list_release(my_repository_list));

}

/**************************************************************************
Receiving the Repository List from the server
**************************************************************************/
TEST(ma_repository_client_tests, repository_client_get_repositories_test)
{
    ma_uint8_t counter = 0;

    // Repository List
    ma_repository_list_t *my_repository_list = NULL;
    ma_repository_t *my_repository = NULL;
    const char *my_repository_name = NULL;

    scenario = 2;

    // Fetching repository list from Repository Service
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_client_get_repositories(g_ma_client, NULL));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_client_get_repositories(NULL, &my_repository_list));
    TEST_ASSERT(MA_OK == ma_repository_client_get_repositories(g_ma_client, &my_repository_list));

    wait_until_done();

    // Verifying that the repository list is fetched correctly to the server
    // Indexes are from (1 <= x <= N) not (0 <= x <= N-1)
    for(counter = 0; counter < sizeof(rep_name_list)/sizeof(rep_name_list[0]); counter++)
    {
        TEST_ASSERT(MA_OK == ma_repository_list_get_repository_by_index(my_repository_list, counter, &my_repository));
        TEST_ASSERT(MA_OK == ma_repository_get_name(my_repository, &my_repository_name));
        TEST_ASSERT(strcmp(rep_name_list[counter], my_repository_name) == 0);
        TEST_ASSERT(MA_OK == ma_repository_release(my_repository));
        my_repository = NULL;
    }

    // Releasing Repository List
    TEST_ASSERT(MA_OK == ma_repository_list_release(my_repository_list));
}



/**************************************************************************
Sending the Proxy Configuration to the Server
**************************************************************************/
TEST(ma_repository_client_tests, repository_client_set_proxy_config_test)
{
    ma_proxy_config_t *my_proxy_config = NULL;

    scenario = 3;

    // Creating Proxy Configuration and setting up few parameters
    TEST_ASSERT(MA_OK == ma_proxy_config_create(&my_proxy_config));
    TEST_ASSERT(MA_OK == ma_proxy_config_set_proxy_usage_type(my_proxy_config, MA_PROXY_USAGE_TYPE_SERVER_CONFIGURED));
    TEST_ASSERT(MA_OK == ma_proxy_config_set_allow_local_proxy_configuration(my_proxy_config, MA_TRUE));
    TEST_ASSERT(MA_OK == ma_proxy_config_set_bypass_local(my_proxy_config, MA_FALSE));
    TEST_ASSERT(MA_OK == ma_proxy_config_set_exclusion_urls(my_proxy_config, "192.16.220.45; 34.56.78.67"));

    // Sending Proxy Configuration to Repository service
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_client_set_proxy_config(g_ma_client, NULL));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_client_set_proxy_config(NULL, my_proxy_config));
    TEST_ASSERT(MA_OK == ma_repository_client_set_proxy_config(g_ma_client, my_proxy_config));

    wait_until_done();

    // Releasing Proxy Configuration
    TEST_ASSERT(MA_OK == ma_proxy_config_release(my_proxy_config));

}

/**************************************************************************
Receiving the Proxy Configuration from the server
**************************************************************************/
TEST(ma_repository_client_tests, repository_client_get_proxy_config_test)
{
    ma_proxy_config_t *my_proxy_config = NULL;

    scenario = 4;

    // Fetching Proxy Configuration from Repository Service
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_client_get_proxy_config(g_ma_client, NULL));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_client_get_proxy_config(NULL, &my_proxy_config));
    TEST_ASSERT(MA_OK == ma_repository_client_get_proxy_config(g_ma_client, &my_proxy_config));

    wait_until_done();

    // Verifying that the Proxy Configuration is fetched correctly from the server
    {
        ma_proxy_usage_type_t usage_type = MA_PROXY_USAGE_TYPE_NONE;
        TEST_ASSERT(MA_OK == ma_proxy_config_get_proxy_usage_type(my_proxy_config, &usage_type));
        TEST_ASSERT(usage_type == MA_PROXY_USAGE_TYPE_SERVER_CONFIGURED);
    }

    {
        ma_bool_t status = MA_FALSE;
        TEST_ASSERT(MA_OK == ma_proxy_config_get_allow_local_proxy_configuration(my_proxy_config, &status));
        TEST_ASSERT(MA_TRUE == status);
    }

    {
        ma_bool_t status = MA_TRUE;
        TEST_ASSERT(MA_OK == ma_proxy_config_get_bypass_local(my_proxy_config, &status));
        TEST_ASSERT(MA_FALSE == status);
    }

    {
        const char *urls = NULL;
        TEST_ASSERT(MA_OK == ma_proxy_config_get_exclusion_urls(my_proxy_config, &urls));
        TEST_ASSERT(strcmp(urls, "192.16.220.45; 34.56.78.67") == 0);
    }

    // Releasing Proxy Configuration
    TEST_ASSERT(MA_OK == ma_proxy_config_release(my_proxy_config));
}


/**************************************************************************
Sending the repository information to the server
**************************************************************************/
TEST(ma_repository_client_tests, repository_client_import_repositories_test)
{
    ma_buffer_t *my_buffer = NULL;

    scenario = 5;

    // Creating a buffer from sitelist
    TEST_ASSERT(MA_OK == ma_buffer_create(&my_buffer, strlen(sitelist_xml)));
    TEST_ASSERT(MA_OK == ma_buffer_set(my_buffer, sitelist_xml, strlen(sitelist_xml)));

    // Sending Repository Information to Repository Service
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_client_import_repositories(NULL, my_buffer));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_client_import_repositories(g_ma_client, NULL));
    TEST_ASSERT(MA_OK == ma_repository_client_import_repositories(g_ma_client, my_buffer));

    TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
}
















