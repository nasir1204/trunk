#include "unity.h"
#include "unity_fixture.h"

#include "ma/ma_common.h"
#include "ma/internal/ma_macros.h"
#include "ma/scheduler/ma_scheduler.h"
#include "ma/scheduler/ma_triggers.h"
#include "ma/scheduler/ma_task.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"
#include "ma/internal/defs/ma_scheduler_defs.h"
#include "ma/internal/services/scheduler/ma_scheduler_service.h"
#include "ma/internal/services/ma_service.h"
#include "ma/ma_msgbus.h"
#include "ma/internal/utils/context/ma_context.h"

#include "ma_ut_setup.h"
#include <uv.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


#ifdef MA_WINDOWS
# include <crtdbg.h>
#include <Windows.h>
# define MA_TRACKLEAKS _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF)
# define MA_INPUT_FOLDER_PATH ".\\infile.xml"
# define MA_OUTPUT_FOLDER_PATH ".\\outfile.xml"
#else
# define MA_TRACKLEAKS ((void *)0)
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
# define MA_INPUT_FOLDER_PATH "./infile.xml"
# define MA_OUTPUT_FOLDER_PATH "./outfile.xml"
#endif

static unsigned short done = 0;
static ma_service_t *g_scheduler_service = NULL;
static ma_client_t *g_ma_client;
static ma_ut_setup_t    *g_ut_setup;



#define PRINT_TIME(X) \
    { \
        time_t t = time(NULL); \
        printf("\n ");\
        printf(X);\
        printf("\n%s - %s", __FUNCTION__, ctime(&t)); \
    }

#define PRINT_TMSTAMP() \
    {\
        ma_task_time_t ttime;\
        get_localtime(&ttime);\
        printf("\n(%d-%d-%d:%d:%d:%d)", ttime.day, ttime.month,ttime.year, ttime.hour, ttime.minute, ttime.secs);\
    }


TEST_GROUP_RUNNER(scheduler_client_run_once_test_group) {
    do {RUN_TEST_CASE(scheduler_client_run_once_test_group, start_service); }while(0); // it should always executed first to ensure scheduler service is running before client requests
   // do {RUN_TEST_CASE(scheduler_client_run_once_test_group, add_run_once_task_test); }while(0);
	//do {RUN_TEST_CASE(scheduler_client_run_once_test_group, add_multiple_run_once_task_test); }while(0);
	// do {RUN_TEST_CASE(scheduler_client_run_once_test_group, add_stop_task_run_once_task_test); }while(0);//remove task not working.
	//do {RUN_TEST_CASE(scheduler_client_run_once_test_group, add_multiple_run_once_task_test1); }while(0);
	//do {RUN_TEST_CASE(scheduler_client_run_once_test_group, add_multiple_run_once_task_test2); }while(0);
	//do {RUN_TEST_CASE(scheduler_client_run_once_test_group, add_lastruntime_run_once_task_test); }while(0);
	//do {RUN_TEST_CASE(scheduler_client_run_once_test_group, add_start_task_run_once_task_test); }while(0);
	//do {RUN_TEST_CASE(scheduler_client_run_once_test_group, trigger_state_run_once_task_test); }while(0);
    //do {RUN_TEST_CASE(scheduler_client_run_once_test_group, diffrent_trigger_in_task_test); }while(0); 
	//do {RUN_TEST_CASE(scheduler_client_run_once_test_group, trigger_state_AP_idel_task_test); }while(0); 
	 // do {RUN_TEST_CASE(scheduler_client_run_once_test_group, get_task_test); }while(0);
	
    do {RUN_TEST_CASE(scheduler_client_run_once_test_group, stop_service); }while(0); // don't forget to stop and release the service
}

TEST_SETUP(scheduler_client_run_once_test_group) {
    
}

TEST_TEAR_DOWN(scheduler_client_run_once_test_group) {
    
}

void run_once_timer_event(uv_timer_t *handle, int status) {
}

TEST(scheduler_client_run_once_test_group, start_service) {
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ut_setup_create("scheduler_client_test", "scheduler.client.test", &g_ut_setup));
    g_ma_client = ma_ut_setup_get_client(g_ut_setup);
    // start the scheduler service
    TEST_ASSERT_MESSAGE(MA_OK == ma_scheduler_service_create(MA_SCHEDULER_SERVICE_NAME_STR, &g_scheduler_service), "scheduler service creation failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_service_configure(g_scheduler_service, ma_ut_setup_get_context(g_ut_setup), MA_SERVICE_CONFIG_NEW), "scheduler service configuration failed");
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_service_start(g_scheduler_service));
	//MA_MSC_SELECT(Sleep(2 * 1000), sleep(2));
}

ma_error_t run_once_task_execute(ma_client_t *ma_client,  const char *product_id, const char *task_type, const ma_task_t *task, void *cb_data) {
    const char *task_id = NULL;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id((ma_task_t *)task, &task_id));
	PRINT_TMSTAMP();
    printf("\nExecuting Task id %s...\n", task_id);
   // ma_scheduler_update_task_status(context, task_id, MA_TASK_STATUS_SUCCESS);
    done = 1;
    return MA_OK;
}

ma_error_t run_once_task_stop(ma_client_t *ma_client,  const char *product_id, const char *task_type, const ma_task_t *task, void *cb_data) {
    const char *task_id = NULL;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id((ma_task_t *)task, &task_id));
    printf("Stopping Task id %s...\n", task_id);
    done = 2;
    return MA_OK;
}

ma_error_t run_once_task_remove(ma_client_t *ma_client, const char *product_id, const char *task_type, const ma_task_t *task, void *cb_data) {
    const char *task_id = NULL;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id((ma_task_t *)task, &task_id));
    printf("Removing Task id %s...\n", task_id);
    done = 1;
    return MA_OK;
}

TEST(scheduler_client_run_once_test_group, add_run_once_task_test) {
    ma_task_t *task = NULL;
    ma_trigger_run_once_policy_t policy = {0};
    ma_trigger_t *trigger = NULL;
    ma_task_time_t start_date = {0};
    ma_variant_t *app_payload = NULL;
    ma_scheduler_task_callbacks_t cb = {&run_once_task_execute, &run_once_task_stop, &run_once_task_remove};
    
   
	printf("%s begin \n",__FUNCTION__);
	PRINT_TMSTAMP();
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger));
	advance_current_date_by_seconds(20, &start_date);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger, &start_date));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_create("ma.pp1.1001", &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_name(task, "Task 1"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_type(task, "Add"));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_set_max_run_time_limit(task,1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_string("add task", strlen("add task"), &app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_app_payload(task, app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_creator_id(task, "scheduler consumer"));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_register_task_callbacks(g_ma_client, "ZZZ", "Add", &cb, NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_add_task(g_ma_client, task));
    done = 0;
    // wait for the published messages
    while(done != 2) {
        
    }
	printf("While done\n");
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(app_payload));
//   TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_release(task));
}

ma_error_t run_once_task_execute1(ma_client_t *ma_client,  const char *product_id, const char *task_type, const ma_task_t *task, void *cb_data) {
    const char *task_id = NULL;
	ma_task_state_t task_state = MA_TASK_STATE_NOT_SCHEDULED;
	ma_task_exec_status_t task_ex_state = MA_TASK_EXEC_NOT_STARTED ;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id((ma_task_t *)task, &task_id));
	PRINT_TMSTAMP();
    printf("\nExecuting Task id %s...\n", task_id);
	ma_task_get_state((ma_task_t *)task,&task_state);
	ma_task_get_execution_status((ma_task_t *)task,&task_ex_state);
	printf("Task execution state=%d\n",task_ex_state);
	printf("Task state=%d\n",task_state);
	TEST_ASSERT_EQUAL_INT(task_state,MA_TASK_STATE_RUNNING);
	TEST_ASSERT_EQUAL_INT(task_ex_state,MA_TASK_EXEC_RUNNING);

	if(done == 0)
		ma_scheduler_update_task_status(g_ma_client, task_id, MA_TASK_EXEC_SUCCESS);
	done++;
    return MA_OK;
}

ma_error_t run_once_task_stop1(ma_client_t *ma_client,  const char *product_id, const char *task_type, const ma_task_t *task, void *cb_data) {
    const char *task_id = NULL;
	ma_task_state_t task_state;
	ma_task_exec_status_t task_ex_state;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id((ma_task_t *)task, &task_id));
	printf("Stopping Task id %s...\n", task_id);
	ma_task_get_state((ma_task_t *)task,&task_state);
	ma_task_get_execution_status((ma_task_t *)task,&task_ex_state);
	printf("Task execution state=%d\n",task_ex_state);
	printf("Task state=%d\n",task_state);
	TEST_ASSERT_EQUAL_INT(task_state,MA_TASK_STATE_NO_TRIGGER_TIME);
	TEST_ASSERT_EQUAL_INT(task_ex_state,MA_TASK_EXEC_FAILED);
	done++;
    return MA_OK;
}


TEST(scheduler_client_run_once_test_group, add_multiple_run_once_task_test) {
    ma_task_t *task = NULL;
    ma_trigger_run_once_policy_t policy = {0};
    ma_trigger_t *trigger = NULL;
	ma_trigger_t *trigger1 = NULL;
    ma_task_time_t start_date = {0};
    ma_variant_t *app_payload = NULL;
    ma_scheduler_task_callbacks_t cb = {&run_once_task_execute1, &run_once_task_stop1, &run_once_task_remove};
       
	printf("%s begin \n",__FUNCTION__);
	PRINT_TMSTAMP();
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger));
	advance_current_date_by_seconds(20, &start_date);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger, &start_date));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger1));
	advance_current_date_by_seconds(100, &start_date);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger1, &start_date));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_create("ma.pp1.1001", &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_name(task, "Task 1"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_type(task, "Add"));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_set_max_run_time_limit(task,1));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger1));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_string("add task", strlen("add task"), &app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_app_payload(task, app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_creator_id(task, "scheduler consumer"));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_register_task_callbacks(g_ma_client, "ZZZ", "Add", &cb, NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_add_task(g_ma_client, task));
    done = 0;
    // wait for the published messages
    do{

	}while(done != 3); 
	printf("While done\n");
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_release(task));
}

ma_error_t run_once_task_execute2(ma_client_t *ma_client, const char *product_id, const char *task_type, const ma_task_t *task, void *cb_data) {
    const char *task_id = NULL;
	ma_task_state_t task_state = MA_TASK_STATE_NOT_SCHEDULED;
	ma_task_exec_status_t task_ex_state = MA_TASK_EXEC_NOT_STARTED ;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id((ma_task_t *)task, &task_id));
	PRINT_TMSTAMP();
    printf("\nExecuting Task id %s...\n", task_id);
	ma_task_get_state((ma_task_t *)task,&task_state);
	ma_task_get_execution_status((ma_task_t *)task,&task_ex_state);
	printf("Task execution state=%d\n",task_ex_state);
	printf("Task state=%d\n",task_state);
	TEST_ASSERT_EQUAL_INT(task_state,MA_TASK_STATE_RUNNING);
	TEST_ASSERT_EQUAL_INT(task_ex_state,MA_TASK_EXEC_RUNNING);

	//if(done == 0)
	//	ma_scheduler_update_task_status(context, task_id, "Task 1", "Add",MA_TASK_STATUS_SUCCESS);
//	TEST_ASSERT_EQUAL_INT(MA_OK,ma_scheduler_remove_task(context,task_id));
	printf("Error retun=%d\n",ma_scheduler_remove_task(g_ma_client,task_id));
	done++;
    return MA_OK;
}

ma_error_t run_once_task_stop2(ma_client_t *ma_client, const char *product_id, const char *task_type, const ma_task_t *task, void *cb_data) {
    const char *task_id = NULL;
	ma_task_state_t task_state;
	ma_task_exec_status_t task_ex_state;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id((ma_task_t *)task, &task_id));
	PRINT_TMSTAMP();
	printf("\nStopping Task id %s...\n", task_id);
	ma_task_get_state((ma_task_t *)task,&task_state);
	ma_task_get_execution_status((ma_task_t *)task,&task_ex_state);
	printf("Task execution state=%d\n",task_ex_state);
	printf("Task state=%d\n",task_state);
	TEST_ASSERT_EQUAL_INT(task_state,MA_TASK_STATE_NO_TRIGGER_TIME);
	TEST_ASSERT_EQUAL_INT(task_ex_state,MA_TASK_EXEC_FAILED);
	done++;
    return MA_OK;
}

ma_error_t run_once_task_remove2(ma_client_t *ma_client,  const char *product_id, const char *task_type, const ma_task_t *task, void *cb_data) {
    const char *task_id = NULL;
	ma_task_state_t task_state = MA_TASK_STATE_NOT_SCHEDULED;
	ma_task_exec_status_t task_ex_state = MA_TASK_EXEC_NOT_STARTED ;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id((ma_task_t *)task, &task_id));
	printf("\n\n##########Removing Task id %s...###########\n", task_id);
	ma_task_get_state((ma_task_t *)task,&task_state);
	ma_task_get_execution_status((ma_task_t *)task,&task_ex_state);
	printf("Task execution state=%d\n",task_ex_state);
	printf("Task state=%d\n",task_state);
	printf("Error return=%d\n",ma_scheduler_remove_task(g_ma_client,task_id));
   // done = 1;
    return MA_OK;
}

/*

check for calling stop task after one trigger elapsed, and next trigger should not execute.
and on stop call back should be called,also check the status of task and task execution.

*/
TEST(scheduler_client_run_once_test_group, add_stop_task_run_once_task_test) {
    ma_task_t *task = NULL;
	ma_task_t *get_task_1 = NULL;
    ma_trigger_run_once_policy_t policy = {0};
    ma_trigger_t *trigger = NULL;
	 ma_trigger_t *trigger1 = NULL;
    ma_task_time_t start_date = {0};
    ma_variant_t *app_payload = NULL;
    ma_scheduler_task_callbacks_t cb = {&run_once_task_execute2, &run_once_task_stop2, &run_once_task_remove2};
    ma_task_state_t task_state = MA_TASK_STATE_NOT_SCHEDULED;
	int check = 1;
   
	printf("%s begin \n",__FUNCTION__);
	PRINT_TMSTAMP();
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger));
	advance_current_date_by_seconds(20, &start_date);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger, &start_date));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger1));
	advance_current_date_by_seconds(100, &start_date);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger1, &start_date));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_create("ma.pp1.1001", &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_name(task, "Task 1"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_type(task, "Add"));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_set_max_run_time_limit(task,1));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger1));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_string("add task", strlen("add task"), &app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_app_payload(task, app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_creator_id(task, "scheduler consumer"));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_register_task_callbacks(g_ma_client, "ZZZ", "Add", &cb, NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_add_task(g_ma_client, task));
    done = 0;
    // wait for the published messages
    do{
		if(check && done == 1){
		printf("Error return from get=%d\n", ma_scheduler_get_task(g_ma_client, "ma.pp1.1001", &get_task_1));
        TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_name(get_task_1, "Task 1"));
        TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_type(get_task_1, "Add"));
		printf("Error return from ma_task_get_state=%d\n", ma_task_get_state(get_task_1,&task_state));

		 check = 0;
		}
	}while(done != 3); 
	printf("While done\n");
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_release(task));
}




ma_error_t run_once_task_execute3(ma_client_t *ma_client, const char *product_id, const char *task_type, const ma_task_t *task, void *cb_data) {
    const char *task_id = NULL;
	ma_task_state_t task_state = MA_TASK_STATE_NOT_SCHEDULED;
	ma_task_exec_status_t task_ex_state = MA_TASK_EXEC_NOT_STARTED ;
    
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id((ma_task_t *)task, &task_id));
	PRINT_TMSTAMP();
	printf("\n#####Executing Task id %s...#######\n", task_id);
	ma_task_get_state((ma_task_t *)task,&task_state);
	ma_task_get_execution_status((ma_task_t *)task,&task_ex_state);
	printf("Task execution state=%d\n",task_ex_state);
	printf("Task state=%d\n",task_state);
	TEST_ASSERT_EQUAL_INT(task_state,MA_TASK_STATE_RUNNING);
	TEST_ASSERT_EQUAL_INT(task_ex_state,MA_TASK_EXEC_RUNNING);

	done++;
    return MA_OK;
}

ma_error_t run_once_task_stop3(ma_client_t *ma_client, const char *product_id, const char *task_type, const ma_task_t *task, void *cb_data) {
    const char *task_id = NULL;
	ma_task_state_t task_state;
	ma_task_exec_status_t task_ex_state;

	PRINT_TMSTAMP();
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id((ma_task_t *)task, &task_id));
	printf("\n\n#######Stopping Task id %s...#######\n", task_id);
	ma_task_get_state((ma_task_t *)task,&task_state);
	ma_task_get_execution_status((ma_task_t *)task,&task_ex_state);
	printf("Task execution state=%d\n",task_ex_state);
	printf("Task state=%d\n",task_state);
	TEST_ASSERT_EQUAL_INT(task_state,MA_TASK_STATE_NO_TRIGGER_TIME);
	TEST_ASSERT_EQUAL_INT(task_ex_state,MA_TASK_EXEC_FAILED);
	done++;
    return MA_OK;
}

/*

check the behaviour when one trigger is running and 2nd trigger time elapsed.
2nd trigger is missed since its run once it will be deleting trigger.
*/

TEST(scheduler_client_run_once_test_group, add_multiple_run_once_task_test1) {
    ma_task_t *task = NULL;
    ma_trigger_run_once_policy_t policy = {0};
    ma_trigger_t *trigger = NULL;
	 ma_trigger_t *trigger1 = NULL;
    ma_task_time_t start_date = {0};
    ma_variant_t *app_payload = NULL;
    ma_scheduler_task_callbacks_t cb = {&run_once_task_execute3, &run_once_task_stop3, &run_once_task_remove};
    
   
	printf("%s begin \n",__FUNCTION__);
	PRINT_TMSTAMP();
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger));
	advance_current_date_by_seconds(20, &start_date);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger, &start_date));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger1));
	advance_current_date_by_seconds(100, &start_date);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger1, &start_date));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_create("ma.pp1.1001", &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_name(task, "Task 1"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_type(task, "Add"));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_set_max_run_time_limit(task,3));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger1));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_string("add task", strlen("add task"), &app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_app_payload(task, app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_creator_id(task, "scheduler consumer"));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_register_task_callbacks(g_ma_client, "ZZZ", "Add", &cb, NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_add_task(g_ma_client, task));
    done = 0;
    // wait for the published messages
    do{

	}while(done != 2); 
	printf("While done\n");
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_release(task));
}


ma_error_t run_once_task_execute4(ma_client_t *ma_client, const char *product_id, const char *task_type, const ma_task_t *task, void *cb_data) {
    const char *task_id = NULL;
	ma_task_state_t task_state = MA_TASK_STATE_NOT_SCHEDULED;
	ma_task_exec_status_t task_ex_state = MA_TASK_EXEC_NOT_STARTED ;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id((ma_task_t *)task, &task_id));
	PRINT_TMSTAMP();
    printf("\nExecuting Task id %s...\n", task_id);
	ma_task_get_state((ma_task_t *)task,&task_state);
	ma_task_get_execution_status((ma_task_t *)task,&task_ex_state);
	printf("Task execution state=%d\n",task_ex_state);
	printf("Task state=%d\n",task_state);
	TEST_ASSERT_EQUAL_INT(task_state,MA_TASK_STATE_RUNNING);
	TEST_ASSERT_EQUAL_INT(task_ex_state,MA_TASK_EXEC_RUNNING);
	done++;
    return MA_OK;
}

ma_error_t run_once_task_stop4(ma_client_t *ma_client, const char *product_id, const char *task_type, const ma_task_t *task, void *cb_data) {
    const char *task_id = NULL;
	ma_task_state_t task_state;
	ma_task_exec_status_t task_ex_state;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id((ma_task_t *)task, &task_id));
	printf("Stopping Task id %s...\n", task_id);
	ma_task_get_state((ma_task_t *)task,&task_state);
	ma_task_get_execution_status((ma_task_t *)task,&task_ex_state);
	printf("Task execution state=%d\n",task_ex_state);
	printf("Task state=%d\n",task_state);
	TEST_ASSERT_EQUAL_INT(task_state,MA_TASK_STATE_NO_TRIGGER_TIME);
	TEST_ASSERT_EQUAL_INT(task_ex_state,MA_TASK_EXEC_FAILED);
	done++;
    return MA_OK;
}

/*
two triggers are set one after another and 1st trigger cross maxrun time limit,hence the task execution will fail.
task will be deleted once both trigger elapsed.
*/
TEST(scheduler_client_run_once_test_group, add_multiple_run_once_task_test2) {
    ma_task_t *task = NULL;
    ma_trigger_run_once_policy_t policy = {0};
    ma_trigger_t *trigger = NULL;
	 ma_trigger_t *trigger1 = NULL;
    ma_task_time_t start_date = {0};
    ma_variant_t *app_payload = NULL;
    ma_scheduler_task_callbacks_t cb = {&run_once_task_execute4, &run_once_task_stop4, &run_once_task_remove};
    
   
	printf("%s begin \n",__FUNCTION__);
	PRINT_TMSTAMP();
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger));
	advance_current_date_by_seconds(20, &start_date);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger, &start_date));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger1));
	advance_current_date_by_seconds(100, &start_date);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger1, &start_date));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_create("ma.pp1.1001", &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_name(task, "Task 1"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_type(task, "Add"));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_set_max_run_time_limit(task,1));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger1));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_string("add task", strlen("add task"), &app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_app_payload(task, app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_creator_id(task, "scheduler consumer"));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_register_task_callbacks(g_ma_client, "ZZZ", "Add", &cb, NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_add_task(g_ma_client, task));
    done = 0;
    // wait for the published messages
    do{

	}while(done != 4); 
	printf("While done\n");
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_release(task));
}




ma_error_t run_once_task_execute5(ma_client_t *ma_client, const char *product_id, const char *task_type, const ma_task_t *task, void *cb_data) {
    const char *task_id = NULL;
	ma_task_state_t task_state = MA_TASK_STATE_NOT_SCHEDULED;
	ma_task_exec_status_t task_ex_state = MA_TASK_EXEC_NOT_STARTED ;
	ma_task_time_t last_time ={0};
	ma_task_time_t next_time ={0};

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id((ma_task_t *)task, &task_id));
	PRINT_TMSTAMP();
    printf("\n\n##########Executing Task id %s...##########\n", task_id);
	ma_task_get_state((ma_task_t *)task,&task_state);
	ma_task_get_execution_status((ma_task_t *)task,&task_ex_state);
	printf("Task execution state=%d\n",task_ex_state);
	printf("Task state=%d\n",task_state);
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_last_run_time((ma_task_t *)task,&last_time));
	printf("Last run time <%d:%d:%d:%d:%d:%d>\n",last_time.day,last_time.month,last_time.year,last_time.hour,last_time.minute,last_time.secs);

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_next_run_time((ma_task_t *)task,&next_time));
	printf("Next run time <%d:%d:%d:%d:%d:%d>\n",next_time.day,next_time.month,next_time.year,next_time.hour,next_time.minute,next_time.secs);

	TEST_ASSERT_EQUAL_INT(task_state,MA_TASK_STATE_RUNNING);
	TEST_ASSERT_EQUAL_INT(task_ex_state,MA_TASK_EXEC_RUNNING);
	done++;
    return MA_OK;
}

ma_error_t run_once_task_stop5(ma_client_t *ma_client, const char *product_id, const char *task_type, const ma_task_t *task, void *cb_data) {
    const char *task_id = NULL;
	ma_task_state_t task_state;
	ma_task_exec_status_t task_ex_state;
	ma_task_time_t last_time ={0};
	ma_task_time_t next_time ={0};

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id((ma_task_t *)task, &task_id));
	printf("\n\n##########Stopping Task id %s...###########\n", task_id);
	ma_task_get_state((ma_task_t *)task,&task_state);
	ma_task_get_execution_status((ma_task_t *)task,&task_ex_state);
	printf("Task execution state=%d\n",task_ex_state);
	printf("Task state=%d\n",task_state);
	TEST_ASSERT_EQUAL_INT(task_state,MA_TASK_STATE_NO_TRIGGER_TIME);
	TEST_ASSERT_EQUAL_INT(task_ex_state,MA_TASK_EXEC_FAILED);
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_last_run_time((ma_task_t *)task,&last_time));
	printf("Last run time <%d:%d:%d:%d:%d:%d>\n",last_time.day,last_time.month,last_time.year,last_time.hour,last_time.minute,last_time.secs);

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_next_run_time((ma_task_t *)task,&next_time));
	printf("Next run time <%d:%d:%d:%d:%d:%d>\n",next_time.day,next_time.month,next_time.year,next_time.hour,next_time.minute,next_time.secs);
	done++;
    return MA_OK;
}

/*
Check for Next run time and Last runn time
*/
TEST(scheduler_client_run_once_test_group, add_lastruntime_run_once_task_test) {
    ma_task_t *task = NULL;
    ma_trigger_run_once_policy_t policy = {0};
    ma_trigger_t *trigger = NULL;
	 ma_trigger_t *trigger1 = NULL;
    ma_task_time_t start_date = {0};
    ma_variant_t *app_payload = NULL;
    ma_scheduler_task_callbacks_t cb = {&run_once_task_execute5, &run_once_task_stop5, &run_once_task_remove};
    
   
	printf("%s begin \n",__FUNCTION__);
	PRINT_TMSTAMP();
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger));
	advance_current_date_by_seconds(20, &start_date);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger, &start_date));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger1));
	advance_current_date_by_seconds(100, &start_date);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger1, &start_date));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_create("ma.pp1.1001", &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_name(task, "Task 1"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_type(task, "Add"));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_set_max_run_time_limit(task,1));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger1));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_string("add task", strlen("add task"), &app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_app_payload(task, app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_creator_id(task, "scheduler consumer"));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_register_task_callbacks(g_ma_client, "ZZZ", "Add", &cb, NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_add_task(g_ma_client, task));
    done = 0;
    // wait for the published messages
    do{

	}while(done != 4); 
	printf("While done\n");
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_release(task));
}



ma_error_t run_once_task_execute6(ma_client_t *ma_client, const char *product_id, const char *task_type, const ma_task_t *task, void *cb_data) {
    const char *task_id = NULL;
	ma_task_state_t task_state = MA_TASK_STATE_NOT_SCHEDULED;
	ma_task_exec_status_t task_ex_state = MA_TASK_EXEC_NOT_STARTED;
    
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id((ma_task_t *)task, &task_id));
	PRINT_TMSTAMP();
	printf("\n\n###########Executing Task id %s...##############\n", task_id);
	ma_task_get_state((ma_task_t *)task,&task_state);
	ma_task_get_execution_status((ma_task_t *)task,&task_ex_state);
	
	printf("Task execution state=%d\n",task_ex_state);
	printf("Task state=%d\n",task_state);
	TEST_ASSERT_EQUAL_INT(task_state,MA_TASK_STATE_RUNNING);
	TEST_ASSERT_EQUAL_INT(task_ex_state,MA_TASK_EXEC_RUNNING);
	done++;
    return MA_OK;
}

ma_error_t run_once_task_stop6(ma_client_t *ma_client, const char *product_id, const char *task_type, const ma_task_t *task, void *cb_data) {
    const char *task_id = NULL;
	ma_task_state_t task_state;
	ma_task_exec_status_t task_ex_state;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id((ma_task_t *)task, &task_id));
	PRINT_TMSTAMP();
	printf("\n\n###################Stopping Task id %s...##########\n", task_id);
	ma_task_get_state((ma_task_t *)task,&task_state);
	ma_task_get_execution_status((ma_task_t *)task,&task_ex_state);
	printf("Task execution state=%d\n",task_ex_state);
	printf("Task state=%d\n",task_state);
	TEST_ASSERT_EQUAL_INT(task_state,MA_TASK_STATE_NO_TRIGGER_TIME);
	TEST_ASSERT_EQUAL_INT(task_ex_state,MA_TASK_EXEC_FAILED);
	done++;
    return MA_OK;
}

/*

Task is started after 2 trigger begin date elapsed and task should run for remaining 2 trigger.

*/
TEST(scheduler_client_run_once_test_group, add_start_task_run_once_task_test) {
    ma_task_t *task = NULL;
    ma_trigger_run_once_policy_t policy = {0};
    ma_trigger_t *trigger = NULL;
	 ma_trigger_t *trigger1 = NULL;
	 ma_trigger_t *trigger2 = NULL;
	 ma_trigger_t *trigger3 = NULL;
    ma_task_time_t start_date = {0};
    ma_variant_t *app_payload = NULL;
    ma_scheduler_task_callbacks_t cb = {&run_once_task_execute6, &run_once_task_stop6, &run_once_task_remove};
    
   
	printf("%s begin \n",__FUNCTION__);
	PRINT_TMSTAMP();
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger));
	advance_current_date_by_seconds(20, &start_date);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger, &start_date));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger1));
	advance_current_date_by_seconds(100, &start_date);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger1, &start_date));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger2));
	advance_current_date_by_seconds(180, &start_date);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger2, &start_date));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger3));
	advance_current_date_by_seconds(260, &start_date);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger3, &start_date));


    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_create("ma.pp1.1001", &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_name(task, "Task 1"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_type(task, "Add"));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_set_max_run_time_limit(task,1));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger2));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger3));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_string("add task", strlen("add task"), &app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_app_payload(task, app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_creator_id(task, "scheduler consumer"));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_register_task_callbacks(g_ma_client, "ZZZ", "Add", &cb, NULL));
#ifdef MA_WINDOWS
	Sleep(110*1000);
#endif
	PRINT_TMSTAMP();   
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_add_task(g_ma_client, task));
	printf("\n Added task\n");
    done = 0;
    // wait for the published messages
    do{

	}while(done != 4); 
	printf("While done\n");
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_release(task));
}


ma_error_t run_once_task_execute7(ma_client_t *ma_client, const char *product_id, const char *task_type, const ma_task_t *task, void *cb_data) {
    const char *task_id = NULL;
	ma_task_state_t task_state = MA_TASK_STATE_NOT_SCHEDULED;
	ma_task_exec_status_t task_ex_state = MA_TASK_EXEC_NOT_STARTED ;
	ma_trigger_state_t trigger_state = MA_TRIGGER_STATE_NOT_STARTED;
	char id[100] = {0};
	ma_trigger_t *trigger=NULL;
	ma_trigger_type_t trigger_type = MA_TRIGGER_DAILY;
	ma_task_time_t begine_date={0};
	ma_task_time_t end_date={0};

        sprintf(id, "%d",done);
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id((ma_task_t *)task, &task_id));
	printf("\n\n###########Executing Task id %s...##############\n", task_id);
	PRINT_TMSTAMP();
	printf("\nTrigger ID=%s\n",id);
	ma_task_get_state((ma_task_t *)task,&task_state);
	ma_task_get_execution_status((ma_task_t *)task,&task_ex_state);
	printf("Task execution state=%d\n",task_ex_state);
	printf("Task state=%d\n",task_state);

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_find_trigger((ma_task_t *)task,"1",&trigger));
	ma_trigger_get_state(trigger,&trigger_state);
	printf("Trigger status=%d\n",trigger_state);
	
	ma_trigger_get_type(trigger,&trigger_type);
	printf("Trigger type=%d\n",trigger_type);
	
	ma_trigger_get_begin_date(trigger,&begine_date);
	printf("Begine time <%d:%d:%d:%d:%d:%d>\n",begine_date.day,begine_date.month,begine_date.year,begine_date.hour,begine_date.minute,begine_date.secs);
	
	ma_trigger_get_end_date(trigger,&begine_date);
	printf("end time <%d:%d:%d:%d:%d:%d>\n",end_date.day,end_date.month,end_date.year,end_date.hour,begine_date.minute,end_date.secs);

	TEST_ASSERT_EQUAL_INT(task_state,MA_TASK_STATE_RUNNING);
	TEST_ASSERT_EQUAL_INT(task_ex_state,MA_TASK_EXEC_RUNNING);
	done++;
    return MA_OK;
}

ma_error_t run_once_task_stop7(ma_client_t *ma_client, const char *product_id, const char *task_type, const ma_task_t *task, void *cb_data) {
    char *task_id = NULL;
	ma_task_state_t task_state;
	ma_task_exec_status_t task_ex_state;
	ma_trigger_state_t trigger_state = MA_TRIGGER_STATE_NOT_STARTED;
	char *id = "2";
	ma_trigger_t *trigger=NULL;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id((ma_task_t *)task, &task_id));
	PRINT_TMSTAMP();
	printf("\n\n###################Stopping Task id %s...##########\n", task_id);
	ma_task_get_state((ma_task_t *)task,&task_state);
	ma_task_get_execution_status((ma_task_t *)task,&task_ex_state);
	printf("Task execution state=%d\n",task_ex_state);
	printf("Task state=%d\n",task_state);
	TEST_ASSERT_EQUAL_INT(task_state,MA_TASK_STATE_NO_TRIGGER_TIME);
	TEST_ASSERT_EQUAL_INT(task_ex_state,MA_TASK_EXEC_FAILED);
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_find_trigger((ma_task_t *)task,id,&trigger));
	ma_trigger_get_state(trigger,&trigger_state);
	printf("Trigger status=%d\n",trigger_state);
	//done++;
    return MA_OK;
}

/*

Have multiple trigger in a task and check for tigeer atributes.

*/
TEST(scheduler_client_run_once_test_group, trigger_state_run_once_task_test) {
    ma_task_t *task = NULL;
    ma_trigger_run_once_policy_t policy = {0};
    ma_trigger_t *trigger = NULL;
	ma_trigger_t *trigger1 = NULL;
	ma_trigger_t *trigger2 = NULL;
	ma_trigger_t *trigger3 = NULL;
    ma_task_time_t start_date = {0};
    ma_variant_t *app_payload = NULL;
    ma_scheduler_task_callbacks_t cb = {&run_once_task_execute7, &run_once_task_stop7, &run_once_task_remove};
    
	printf("%s begin \n",__FUNCTION__);
	PRINT_TMSTAMP();
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger));
	advance_current_date_by_seconds(20, &start_date);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger, &start_date));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger1));
	advance_current_date_by_seconds(100, &start_date);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger1, &start_date));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger2));
	advance_current_date_by_seconds(180, &start_date);
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger2, &start_date));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger3));
	advance_current_date_by_seconds(260, &start_date);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger3, &start_date));


    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_create("ma.pp1.1001", &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_name(task, "Task 1"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_type(task, "Add"));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_set_max_run_time_limit(task,1));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger2));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger3));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_string("add task", strlen("add task"), &app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_app_payload(task, app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_creator_id(task, "scheduler consumer"));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_register_task_callbacks(g_ma_client, "ZZZ", "Add", &cb, NULL));
	//Sleep(110*1000);
	PRINT_TMSTAMP();   
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_add_task(g_ma_client, task));
	printf("\n Added task\n");
    done = 0;
    // wait for the published messages
    do{
		 //TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_get_task(context, "ma.pp1.1001","Task 1", "Add", &get_task_1));
		 //ma_task_get_state((ma_task_t *)task,&task_state);
		 //if(task_state == MA_TASK_STATE_RUNNING){
		 //}
	}while(done != 4); 
	printf("While done\n");
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_release(task));
}




ma_error_t run_once_task_execute8(ma_client_t *ma_client, const char *product_id, const char *task_type, const ma_task_t *task, void *cb_data) {
    char *task_id = NULL;
	ma_task_state_t task_state = MA_TASK_STATE_NOT_SCHEDULED;
	ma_task_exec_status_t task_ex_state = MA_TASK_EXEC_NOT_STARTED ;
	ma_trigger_state_t trigger_state = MA_TRIGGER_STATE_NOT_STARTED;
	char id[100] = {0};
	ma_trigger_t *trigger=NULL;
	ma_trigger_type_t trigger_type = MA_TRIGGER_DAILY;
	ma_task_time_t begine_date={0};
	ma_task_time_t end_date={0};
	ma_task_time_t last_time ={0};
	ma_task_time_t next_time ={0};

        sprintf(id, "%d",done);	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id((ma_task_t *)task, &task_id));
	printf("\n\n###########Executing Task id %s...##############\n", task_id);
	PRINT_TMSTAMP();
	printf("\nTrigger ID=%s\n",id);
	ma_task_get_state((ma_task_t *)task,&task_state);
	ma_task_get_execution_status((ma_task_t *)task,&task_ex_state);
	printf("Task execution state=%d\n",task_ex_state);
	printf("Task state=%d\n",task_state);

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_find_trigger((ma_task_t *)task,id,&trigger));
	ma_trigger_get_state(trigger,&trigger_state);
	printf("Trigger status=%d\n",trigger_state);
	
	ma_trigger_get_type(trigger,&trigger_type);
	switch(trigger_type){
		case MA_TRIGGER_WEEKLY: printf("MA_TRIGGER_WEEKLY is runing\n");
								break;	
		case MA_TRIGGER_MONTHLY_DATE: printf("MA_TRIGGER_MONTHLY_DATE is runing\n");
								break;	
		case MA_TRIGGER_MONTHLY_DOW: printf("MA_TRIGGER_MONTHLY_DOW is runing\n");
								break;
	}
	
	ma_trigger_get_begin_date(trigger,&begine_date);
	printf("Begine time <%d:%d:%d:%d:%d:%d>\n",begine_date.day,begine_date.month,begine_date.year,begine_date.hour,begine_date.minute,begine_date.secs);
	
	ma_trigger_get_end_date(trigger,&begine_date);
	printf("end time <%d:%d:%d:%d:%d:%d>\n",end_date.day,end_date.month,end_date.year,end_date.hour,begine_date.minute,end_date.secs);

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_last_run_time((ma_task_t *)task,&last_time));
	printf("Last run time <%d:%d:%d:%d:%d:%d>\n",last_time.day,last_time.month,last_time.year,last_time.hour,last_time.minute,last_time.secs);

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_next_run_time((ma_task_t *)task,&next_time));
	printf("Next run time <%d:%d:%d:%d:%d:%d>\n",next_time.day,next_time.month,next_time.year,next_time.hour,next_time.minute,next_time.secs);

	TEST_ASSERT_EQUAL_INT(task_state,MA_TASK_STATE_RUNNING);
	TEST_ASSERT_EQUAL_INT(task_ex_state,MA_TASK_EXEC_RUNNING);
	done++;
    return MA_OK;
}

ma_error_t run_once_task_stop8(ma_client_t *ma_client, const char *product_id, const char *task_type, const ma_task_t *task, void *cb_data) {
    char *task_id = NULL;
	ma_task_state_t task_state;
	ma_task_exec_status_t task_ex_state;
	ma_trigger_state_t trigger_state = MA_TRIGGER_STATE_NOT_STARTED;
	char id[100] = {0};
	ma_trigger_t *trigger=NULL;
	ma_task_time_t last_time ={0};
	ma_task_time_t next_time ={0};

        sprintf(id, "%d",done);	
        TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id((ma_task_t *)task, &task_id));
	PRINT_TMSTAMP();
	printf("\n\n###################Stopping Task id %s...##########\n", task_id);
	ma_task_get_state((ma_task_t *)task,&task_state);
	ma_task_get_execution_status((ma_task_t *)task,&task_ex_state);
	printf("Task execution state=%d\n",task_ex_state);
	printf("Task state=%d\n",task_state);
	TEST_ASSERT_EQUAL_INT(task_state,MA_TASK_STATE_NO_TRIGGER_TIME);
	TEST_ASSERT_EQUAL_INT(task_ex_state,MA_TASK_EXEC_FAILED);
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_find_trigger((ma_task_t *)task,id,&trigger));
	ma_trigger_get_state(trigger,&trigger_state);
	printf("Trigger status=%d\n",trigger_state);
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_last_run_time((ma_task_t *)task,&last_time));
	printf("Last run time <%d:%d:%d:%d:%d:%d>\n",last_time.day,last_time.month,last_time.year,last_time.hour,last_time.minute,last_time.secs);

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_next_run_time((ma_task_t *)task,&next_time));
	printf("Next run time <%d:%d:%d:%d:%d:%d>\n",next_time.day,next_time.month,next_time.year,next_time.hour,next_time.minute,next_time.secs);
	if(done == 3)
		done++;
    return MA_OK;
}

/*

Have multiple trigger in a task and check for tigeer atributes.

*/
TEST(scheduler_client_run_once_test_group, diffrent_trigger_in_task_test) {
    ma_task_t *task = NULL;
    ma_trigger_t *trigger = NULL;
	ma_trigger_t *trigger1 = NULL;
	ma_trigger_t *trigger2 = NULL;
    ma_task_time_t start_date = {0};
    ma_variant_t *app_payload = NULL;
    ma_scheduler_task_callbacks_t cb = {&run_once_task_execute8, &run_once_task_stop8, &run_once_task_remove};
    
	ma_trigger_weekly_policy_t policy_weekly;
	ma_trigger_monthly_date_policy_t policy_monthly_date;
	ma_trigger_monthly_dow_policy_t policy_monthly_dow;
	
	policy_weekly.days_of_the_week = THURSDAY;
	policy_weekly.weeks_interval = 1;

	policy_monthly_date.date = 27;
	policy_monthly_date.months=JUNE|FEBRUARY;

    policy_monthly_dow.day_of_the_week = THURSDAY;
	policy_monthly_dow.months = JUNE|JANUARY;
	policy_monthly_dow.which_week = FOURTH_WEEK;


	printf("%s begin \n",__FUNCTION__);
	PRINT_TMSTAMP();
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_weekly(&policy_weekly, &trigger));
	advance_current_date_by_seconds(20, &start_date);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger, &start_date));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_monthly_date(&policy_monthly_date, &trigger1));
	advance_current_date_by_seconds(100, &start_date);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger1, &start_date));
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_monthly_dow(&policy_monthly_dow, &trigger2));
	advance_current_date_by_seconds(180, &start_date);
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger2, &start_date));
	

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_create("ma.pp1.1001", &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_name(task, "Task 1"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_type(task, "Add"));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_set_max_run_time_limit(task,1));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger2));
	

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_string("add task", strlen("add task"), &app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_app_payload(task, app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_creator_id(task, "scheduler consumer"));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_register_task_callbacks(g_ma_client, "ZZZ", "Add", &cb, NULL));
	//Sleep(110*1000);
	PRINT_TMSTAMP();   
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_add_task(g_ma_client, task));
	printf("\n Added task\n");
    done = 0;
    // wait for the published messages
    do{
		 
	}while(done != 4); 
	printf("While done\n");
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_release(task));
}


ma_error_t run_once_task_execute9(ma_client_t *ma_client, const char *product_id, const char *task_type, const ma_task_t *task, void *cb_data) {
    char *task_id = NULL;
	ma_task_state_t task_state = MA_TASK_STATE_NOT_SCHEDULED;
	ma_task_exec_status_t task_ex_state = MA_TASK_EXEC_NOT_STARTED ;
	ma_trigger_state_t trigger_state = MA_TRIGGER_STATE_NOT_STARTED;
	char id[100] = {0};
	ma_trigger_t *trigger=NULL;
	ma_trigger_type_t trigger_type = MA_TRIGGER_DAILY;
	ma_task_time_t begine_date={0};
	ma_task_time_t end_date={0};
	ma_task_time_t last_time ={0};
	ma_task_time_t next_time ={0};

        sprintf(id, "%d",done);	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id((ma_task_t *)task, &task_id));
	printf("\n\n###########Executing Task id %s...##############\n", task_id);
	PRINT_TMSTAMP();
	printf("\nTrigger ID=%s\n",id);
	ma_task_get_state((ma_task_t *)task,&task_state);
	ma_task_get_execution_status((ma_task_t *)task,&task_ex_state);
	printf("Task execution state=%d\n",task_ex_state);
	printf("Task state=%d\n",task_state);

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_find_trigger((ma_task_t *)task,id,&trigger));
	ma_trigger_get_state(trigger,&trigger_state);
	printf("Trigger status=%d\n",trigger_state);
	
	ma_trigger_get_type(trigger,&trigger_type);
	switch(trigger_type){
		case MA_TRIGGER_WEEKLY: printf("MA_TRIGGER_WEEKLY is runing\n");
								break;	
		case MA_TRIGGER_MONTHLY_DATE: printf("MA_TRIGGER_MONTHLY_DATE is runing\n");
								break;	
		case MA_TRIGGER_MONTHLY_DOW: printf("MA_TRIGGER_MONTHLY_DOW is runing\n");
								break;	
		case MA_TRIGGER_ONCE: printf("MA_TRIGGER_ONCE is runing\n");
								break;
	}
	
	ma_trigger_get_begin_date(trigger,&begine_date);
	printf("Begine time <%d:%d:%d:%d:%d:%d>\n",begine_date.day,begine_date.month,begine_date.year,begine_date.hour,begine_date.minute,begine_date.secs);
	
	ma_trigger_get_end_date(trigger,&begine_date);
	printf("end time <%d:%d:%d:%d:%d:%d>\n",end_date.day,end_date.month,end_date.year,end_date.hour,begine_date.minute,end_date.secs);

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_last_run_time((ma_task_t *)task,&last_time));
	printf("Last run time <%d:%d:%d:%d:%d:%d>\n",last_time.day,last_time.month,last_time.year,last_time.hour,last_time.minute,last_time.secs);

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_next_run_time((ma_task_t *)task,&next_time));
	printf("Next run time <%d:%d:%d:%d:%d:%d>\n",next_time.day,next_time.month,next_time.year,next_time.hour,next_time.minute,next_time.secs);

	TEST_ASSERT_EQUAL_INT(task_state,MA_TASK_STATE_RUNNING);
	TEST_ASSERT_EQUAL_INT(task_ex_state,MA_TASK_EXEC_RUNNING);
	done++;
    return MA_OK;
}

ma_error_t run_once_task_stop9(ma_client_t *ma_client, const char *product_id, const char *task_type, const ma_task_t *task, void *cb_data) {
    const char *task_id = NULL;
	ma_task_state_t task_state;
	ma_task_exec_status_t task_ex_state;
	ma_trigger_state_t trigger_state = MA_TRIGGER_STATE_NOT_STARTED;
	char id[100] = {0};
	ma_trigger_t *trigger=NULL;
	ma_task_time_t last_time ={0};
	ma_task_time_t next_time ={0};

    sprintf(id, "%d",done);	
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id((ma_task_t *)task, &task_id));
	PRINT_TMSTAMP();
	printf("\n\n###################Stopping Task id %s...##########\n", task_id);
	ma_task_get_state((ma_task_t *)task,&task_state);
	ma_task_get_execution_status((ma_task_t *)task,&task_ex_state);
	printf("Task execution state=%d\n",task_ex_state);
	printf("Task state=%d\n",task_state);
	TEST_ASSERT_EQUAL_INT(task_state,MA_TASK_STATE_NO_TRIGGER_TIME);
	TEST_ASSERT_EQUAL_INT(task_ex_state,MA_TASK_EXEC_FAILED);
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_find_trigger((ma_task_t *)task,id,&trigger));
	ma_trigger_get_state(trigger,&trigger_state);
	printf("Trigger status=%d\n",trigger_state);
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_last_run_time((ma_task_t *)task,&last_time));
	printf("Last run time <%d:%d:%d:%d:%d:%d>\n",last_time.day,last_time.month,last_time.year,last_time.hour,last_time.minute,last_time.secs);

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_next_run_time((ma_task_t *)task,&next_time));
	printf("Next run time <%d:%d:%d:%d:%d:%d>\n",next_time.day,next_time.month,next_time.year,next_time.hour,next_time.minute,next_time.secs);
	if(done == 3)
		done++;
    return MA_OK;
}

/*

AP idel tigger test

*/
TEST(scheduler_client_run_once_test_group, trigger_state_AP_idel_task_test) {
    ma_task_t *task = NULL;
    ma_trigger_run_once_policy_t policy = {0};
    ma_trigger_t *trigger = NULL;
	ma_trigger_t *trigger1 = NULL;
	ma_trigger_t *trigger2 = NULL;
	ma_trigger_t *trigger3 = NULL;
    ma_task_time_t start_date = {0};
    ma_variant_t *app_payload = NULL;
    ma_scheduler_task_callbacks_t cb = {&run_once_task_execute9, &run_once_task_stop9, &run_once_task_remove};
    
   
	printf("%s begin \n",__FUNCTION__);
	PRINT_TMSTAMP();
    

	advance_current_date_by_seconds(100, &start_date);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger1, &start_date));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger));
	advance_current_date_by_seconds(20, &start_date);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger, &start_date));

	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger2));
	advance_current_date_by_seconds(180, &start_date);
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger2, &start_date));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger3));
	advance_current_date_by_seconds(260, &start_date);
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger3, &start_date));


    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_create("ma.pp1.1001", &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_name(task, "Task 1"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_type(task, "Add"));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_set_max_run_time_limit(task,1));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger2));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger3));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_string("add task", strlen("add task"), &app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_app_payload(task, app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_creator_id(task, "scheduler consumer"));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_register_task_callbacks(g_ma_client, "ZZZ", "Add", &cb, NULL));
	//Sleep(110*1000);
	PRINT_TMSTAMP();   
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_add_task(g_ma_client, task));
	printf("\n Added task\n");
    done = 0;
    // wait for the published messages
    do{
		 //TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_get_task(context, "ma.pp1.1001","Task 1", "Add", &get_task_1));
		 //ma_task_get_state((ma_task_t *)task,&task_state);
		 //if(task_state == MA_TASK_STATE_RUNNING){
		 //}
	}while(done != 5); 
	printf("While done\n");
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_release(task));
}

TEST(scheduler_client_run_once_test_group, stop_service) {
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_service_stop(g_scheduler_service));
    ma_service_release(g_scheduler_service);
    ma_ut_setup_release(g_ut_setup);
}

