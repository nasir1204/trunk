#include "ma/ma_context.h"
#include "ma/internal/ma_context_internal.h"
#include "ma/datastore/ma_ds_database.h"
#include "ma/internal/core/msgbus/ma_msgbus_impl.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/services/ahclient/ma_ah_client_service.h"
#include "ma/internal/utils/network/ma_net_client_service.h"
#include "ma/internal/clients/ma/controller/ma_client_controller.h"
#include "ma/ma_dispatcher.h"
#include "ma/internal/services/scheduler/ma_scheduler_core.h"
#include "ma/ma_common.h"
#include "ma/internal/ma_macros.h"

#include <stdlib.h>
#include <string.h>
#ifdef MA_WINDOWS
#include <Windows.h>
#endif
#include <stdio.h>

typedef struct ma_client_context_s {
    ma_context_t                base_context;
    ma_msgbus_t                 *msgbus;
	ma_event_loop_t             *msgbus_loop;
	ma_db_t                     *ma_scheduler_db;
	ma_ds_t                     *scheduler_ds;
	ma_scheduler_t				*scheduler;
    
	ma_client_controller_t      *client_controller;
    char                        *product_id;
	unsigned long				timeout_ms;
	char						db_name[MA_MAX_LEN];
}ma_client_context_t;


static void *ma_ut_context_get_object(ma_context_t *context, const char *object_name);
ma_error_t ma_ut_context_release(ma_context_t *context);
static const ma_context_methods_t context_methods = {
    &ma_ut_context_get_object,
    &ma_ut_context_release
};

ma_error_t ma_ut_context_create(const char *product_id, ma_bool_t thread_pool_enabled, const char *db_name, ma_context_t **context) {
    if(product_id && context) {
        ma_error_t rc = MA_OK;
        ma_client_context_t *self = (ma_client_context_t *)calloc(1,sizeof(ma_client_context_t));
        if(!self)       return MA_ERROR_OUTOFMEMORY;
       
        if(MA_OK != (rc = ma_msgbus_create(product_id, &self->msgbus))) {   
            free(self);
            return rc;
        }

		if(MA_OK != (rc = ma_msgbus_start(self->msgbus))) {
			ma_msgbus_release(self->msgbus);
            free(self);
            return rc;
        }

		if(MA_OK != (rc = ma_client_controller_create(self->msgbus, thread_pool_enabled ? MA_MSGBUS_CALLBACK_THREAD_POOL : MA_MSGBUS_CALLBACK_THREAD_IO, MSGBUS_CONSUMER_REACH_INPROC, &self->client_controller))) {			
			ma_msgbus_stop(self->msgbus, MA_TRUE);
            ma_msgbus_release(self->msgbus);   
            free(self);
            return rc;
        }
		ma_client_controller_start(self->client_controller);
		self->timeout_ms = 60 * 1000 * 1000; // bigger timeout, to ensure no MA_ERROR_TIMEOUT during debugging

		ma_db_initialize();		

		
		if(db_name) {
			strncpy(self->db_name, db_name, MA_MAX_LEN - 1);
		}
		else {
			unlink("SCHEDULER.db");
			strncpy(self->db_name, "SCHEDULER.db", strlen("SCHEDULER.db"));
		}
		ma_db_open(self->db_name, &self->ma_scheduler_db);
		ma_ds_database_create(self->ma_scheduler_db, "SCHEDULER", (ma_ds_database_t**)&self->scheduler_ds);	
		
		if(MA_OK != (rc = ma_scheduler_create(self->msgbus, self->scheduler_ds, "scheduler_test", &self->scheduler))) {
			ma_msgbus_stop(self->msgbus, MA_TRUE);
			ma_msgbus_release(self->msgbus);   
            free(self);
            return rc;
		}
		
		if(MA_OK != (rc = ma_scheduler_start(self->scheduler))) {
			ma_scheduler_release(self->scheduler);
			ma_msgbus_stop(self->msgbus, MA_TRUE);
			ma_msgbus_release(self->msgbus);   
            free(self);
            return rc;
		}

        ma_context_init((ma_context_t *)self, &context_methods, self);
        self->product_id = strdup(product_id);
        *context = (ma_context_t*)self;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_ut_context_release(ma_context_t *context) {
    if(context) {
		ma_client_context_t *self = (ma_client_context_t *)context; 

		ma_scheduler_stop(self->scheduler);
		ma_scheduler_release(self->scheduler);

		ma_ds_release(self->scheduler_ds);
		ma_db_close(self->ma_scheduler_db);
		if(! self->db_name) unlink("SCHEDULER.db");

        if(self->client_controller){
			ma_client_controller_stop(self->client_controller);
			ma_client_controller_release(self->client_controller);        
		}
        
		if(self->msgbus) {
			ma_msgbus_stop(self->msgbus, MA_TRUE);
			ma_msgbus_release(self->msgbus);   
		}

		if(self->product_id) free(self->product_id);
        free(self);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_context_release(ma_context_t *context) {
    if(context && context->methods && context->methods->release) {
        return context->methods->release(context);
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_context_get_msgbus(ma_context_t *context , ma_msgbus_t **msgbus) {
    if(context && msgbus) {
        *msgbus = ((ma_client_context_t *)context)->msgbus;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static void *ma_ut_context_get_object(ma_context_t *context, const char *object_name) {
    if(0 == strcmp(MA_OBJECT_CLIENT_CONTROLLER_NAME_STR ,object_name))
        return ((ma_client_context_t*)context)->client_controller;
    else if(0 == strcmp(MA_OBJECT_MSGBUS_NAME_STR ,object_name))
        return ((ma_client_context_t*)context)->msgbus;
    else if(0 == strcmp(MA_OBJECT_MSGBUS_LOOP_NAME_STR ,object_name))
		return ((ma_client_context_t*)context)->msgbus_loop;
	else if(0 == strcmp(MA_OBJECT_SCHEDULER_DATASTORE_NAME_STR ,object_name))
		return ((ma_client_context_t*)context)->scheduler_ds;
	else if(0 == strcmp(MA_OBJECT_SCHEDULER_NAME_STR ,object_name))
		return ((ma_client_context_t*)context)->scheduler;
	else if(0 == strcmp(MA_ENDPOINT_TIMEOUT_MILLI_SEC_STR ,object_name))
		return &((ma_client_context_t*)context)->timeout_ms;
	else if(0 == strcmp(MA_OBJECT_PRODUCT_ID_STR ,object_name))
		return ((ma_client_context_t*)context)->product_id;
    else
        return NULL;
}


