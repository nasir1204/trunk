#include "unity.h"
#include "unity_fixture.h"

#include "ma/ma_common.h"
#include "ma/internal/ma_macros.h"
#include "ma/scheduler/ma_scheduler.h"
#include "ma/scheduler/ma_triggers.h"
#include "ma/scheduler/ma_task.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"
#include "ma/internal/defs/ma_scheduler_defs.h"
#include "ma/internal/services/scheduler/ma_scheduler_service.h"
#include "ma/internal/services/ma_service.h"
#include "ma/ma_msgbus.h"
#include "ma_ut_setup.h"
#include <uv.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


#ifdef MA_WINDOWS
# include <crtdbg.h>
#include <Windows.h>
# define MA_TRACKLEAKS _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF)
#else
# define MA_TRACKLEAKS ((void *)0)
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#endif

#define DB_UT    "scheduler_client_test.db"


static ma_service_t *g_scheduler_service = NULL;
static ma_bool_t done = 0;
static ma_bool_t done_1 = 0;
static ma_bool_t is_task_executed = MA_FALSE;
static ma_bool_t is_task_stopped = MA_FALSE;
static ma_bool_t is_task_removed = MA_FALSE;

static ma_client_t *g_ma_client;
static ma_ut_setup_t *g_ut_setup;

static void reset_task_flags() {
    is_task_executed = MA_FALSE;
    is_task_stopped = MA_FALSE;
    is_task_removed = MA_FALSE;
}


TEST_GROUP_RUNNER(scheduler_client_test_group) {
	do {RUN_TEST_CASE(scheduler_client_test, start_service); }while(0); // it should always be executed first to ensure scheduler service is running before client requests
	//do {RUN_TEST_CASE(scheduler_client_test, test_multiple_task_call_backs); }while(0);
	do {RUN_TEST_CASE(scheduler_client_test, task_persistence_test_before); }while(0);
	do {RUN_TEST_CASE(scheduler_client_test, stop_service); }while(0); // don't forget to stop and release the service


	do {RUN_TEST_CASE(scheduler_client_test, start_service); }while(0); // it should always be executed first to ensure scheduler service is running before client requests
	do {RUN_TEST_CASE(scheduler_client_test, task_persistence_test_after); }while(0);
	//do {RUN_TEST_CASE(scheduler_client_test, add_task_test); }while(0);
	//do {RUN_TEST_CASE(scheduler_client_test, get_task_test); }while(0);
	do {RUN_TEST_CASE(scheduler_client_test, stop_service); }while(0); // don't forget to stop and release the service
}

TEST_SETUP(scheduler_client_test) {
    
}

TEST_TEAR_DOWN(scheduler_client_test) {
    
}

void timer_event(uv_timer_t *handle, int status) {
}

TEST(scheduler_client_test, start_service) {
	// let's enable the thread pool for testing. Else it will be tough to test the APIs that are internally using "send and forget" 
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_ut_setup_create("scheduler_client_test", "scheduler.client.test", &g_ut_setup));
    g_ma_client = ma_ut_setup_get_client(g_ut_setup);
    // start the scheduler service
    TEST_ASSERT_MESSAGE(MA_OK == ma_scheduler_service_create(MA_SCHEDULER_SERVICE_NAME_STR, &g_scheduler_service), "scheduler service creation failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_service_configure(g_scheduler_service, ma_ut_setup_get_context(g_ut_setup), MA_SERVICE_CONFIG_NEW), "scheduler service configuration failed");
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_service_start(g_scheduler_service));
	
}

ma_error_t on_task_execute_A(ma_client_t *ma_client, const char *product_id, const char *task_type, ma_task_t *task, void *cb_data) {
    const char *task_id = NULL;
	const char *task_name = NULL;
	const char *task_type_ = NULL;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id((ma_task_t *)task, &task_id));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_name((ma_task_t *)task, &task_name));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_type((ma_task_t *)task, &task_type_));
    printf("Executing Task id %s...\n", task_id);
    //ma_scheduler_update_task_status(g_ma_client, task_id, task_name, task_type_, MA_TASK_STATUS_SUCCESS);
    is_task_executed = MA_TRUE;
    return MA_OK;
}

ma_error_t on_task_stop_A(ma_client_t *ma_client, const char *task_type, const char *product_id, ma_task_t *task, void *cb_data) {
    const char *task_id = NULL;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id((ma_task_t *)task, &task_id));
    printf("Stopping Task id %s...\n", task_id);
    is_task_stopped = MA_TRUE;
    return MA_OK;
}

ma_error_t on_task_remove_A(ma_client_t *ma_client, const char *task_type, const char *product_id, ma_task_t *task, void *cb_data) {
    const char *task_id = NULL;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id((ma_task_t *)task, &task_id));
    printf("Removing Task id %s...\n", task_id);
    is_task_removed = MA_TRUE;
    return MA_OK;
}

ma_error_t on_task_execute_B(ma_client_t *ma_client, const char *product_id, const char *task_type, ma_task_t *task, void *cb_data) {
    const char *task_id = NULL;
	const char *task_name = NULL;
	const char *task_type_ = NULL;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id((ma_task_t *)task, &task_id));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_name((ma_task_t *)task, &task_name));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_type((ma_task_t *)task, &task_type_));
    printf("Executing Task id %s...\n", task_id);
    //ma_scheduler_update_task_status(g_ma_client, task_id, task_name, task_type_, MA_TASK_STATUS_SUCCESS);
    is_task_executed = MA_TRUE;
    return MA_OK;
}

ma_error_t on_task_stop_B(ma_client_t *ma_client, const char *task_type, const char *product_id, ma_task_t *task, void *cb_data) {
    const char *task_id = NULL;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id((ma_task_t *)task, &task_id));
    printf("Stopping Task id %s...\n", task_id);
    is_task_stopped = MA_TRUE;
    return MA_OK;
}

ma_error_t on_task_remove_B(ma_client_t *ma_client, const char *task_type, const char *product_id, ma_task_t *task, void *cb_data) {
    const char *task_id = NULL;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id((ma_task_t *)task, &task_id));
    printf("Removing Task id %s...\n", task_id);
    is_task_removed = MA_TRUE;
    return MA_OK;
}

ma_error_t on_task_execute(ma_client_t *ma_client, const char *product_id, const char *task_type, ma_task_t *task, void *cb_data) {
    const char *task_id = NULL;
	const char *task_name = NULL;
	const char *task_type_ = NULL;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id((ma_task_t *)task, &task_id));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_name((ma_task_t *)task, &task_name));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_type((ma_task_t *)task, &task_type_));
    printf("Executing Task id %s...\n", task_id);
    //ma_scheduler_update_task_status(g_ma_client, task_id, task_name, task_type_, MA_TASK_STATUS_SUCCESS);
    done = 1;
    is_task_executed = MA_TRUE;
    return MA_OK;
}

ma_error_t on_task_stop(ma_client_t *ma_client, const char *task_type, const char *product_id, ma_task_t *task, void *cb_data) {
    const char *task_id = NULL;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id((ma_task_t *)task, &task_id));
    printf("Stopping Task id %s...\n", task_id);
    done = 1;
    is_task_stopped = MA_TRUE;
    return MA_OK;
}

ma_error_t on_task_remove(ma_client_t *ma_client, const char *task_type, const char *product_id, ma_task_t *task, void *cb_data) {
    const char *task_id = NULL;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id((ma_task_t *)task, &task_id));
    printf("Removing Task id %s...\n", task_id);
    done = 1;
    is_task_removed = MA_TRUE;
    return MA_OK;
}


ma_error_t on_task_execute_1(ma_client_t *ma_client, const char *product_id, const char *task_type, ma_task_t *task, void *cb_data) {
    const char *task_id = NULL;
	const char *task_name = NULL;
	const char *task_type_ = NULL;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id((ma_task_t *)task, &task_id));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_name((ma_task_t *)task, &task_name));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_type((ma_task_t *)task, &task_type_));
    printf("Executing Task id %s...\n", task_id);
    //ma_scheduler_update_task_status(g_ma_client, task_id, task_name, task_type_, MA_TASK_STATUS_SUCCESS);
    done_1 = 1;
    return MA_OK;
}

ma_error_t on_task_stop_1(ma_client_t *ma_client, const char *task_type, const char *product_id, ma_task_t *task, void *cb_data) {
    const char *task_id = NULL;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id((ma_task_t *)task, &task_id));
    printf("Stopping Task id %s...\n", task_id);
    done_1 = 1;
    return MA_OK;
}

ma_error_t on_task_remove_1(ma_client_t *ma_client, const char *task_type, const char *product_id, ma_task_t *task, void *cb_data) {
    const char *task_id = NULL;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id((ma_task_t *)task, &task_id));
    printf("Removing Task id %s...\n", task_id);
    done_1 = 1;
    return MA_OK;
}

TEST(scheduler_client_test, task_persistence_test_before) {
    ma_task_t *task = NULL;
    ma_trigger_daily_policy_t policy = {0};
    ma_trigger_t *trigger = NULL;
    ma_task_time_t start_date = {0};

    ma_variant_t *app_payload = NULL;
    ma_scheduler_task_callbacks_t cb = {&on_task_execute, &on_task_stop, &on_task_remove};
	ma_task_repeat_policy_t repeat_policy = { 0, 0 };
    
	policy.days_interval = 5;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&policy, &trigger));
	advance_current_date_by_seconds(30, &start_date); /* bump the start date if you are debugging. timeout might be elapsed before the actual fire time!!*/
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger, &start_date));
	//advance_current_date_by_seconds(120, &end_date); 
	//TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_end_date(trigger, &end_date));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_create("REBORN", &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_name(task, "Task 1"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_type(task, "Get"));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_software_id(task, "XXX"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_string("add task", strlen("add task"), &app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_app_payload(task, app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_creator_id(task, "scheduler consumer"));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time(task, 1));
	repeat_policy.repeat_interval.hour = 0;
	repeat_policy.repeat_interval.minute = 0;
	repeat_policy.repeat_until.hour = 0;
	repeat_policy.repeat_until.minute = 0;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_repeat_policy(task, repeat_policy));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_register_task_callbacks(g_ma_client, "XXX", "Add", &cb, NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_add_task(g_ma_client, task));
    
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(app_payload));
}

TEST(scheduler_client_test, test_multiple_task_call_backs) {
    ma_task_t *task_1 = NULL;
    ma_task_t *task_2 = NULL;
    ma_trigger_daily_policy_t policy = {0};
    ma_trigger_t *trigger_1 = NULL;
    ma_trigger_t *trigger_2 = NULL;
    ma_task_time_t start_date = {0};
    ma_variant_t *app_payload = NULL;
    ma_scheduler_task_callbacks_t cb_1 = {&on_task_execute_A, &on_task_stop_A, &on_task_remove_A};
    ma_scheduler_task_callbacks_t cb_2 = {&on_task_execute_B, &on_task_stop_B, &on_task_remove_B};
	ma_task_repeat_policy_t repeat_policy = { 0, 0 };
    ma_int32_t ctr = 0;
    
	policy.days_interval = 2;
	repeat_policy.repeat_interval.hour = 0;
	repeat_policy.repeat_interval.minute = 1;
	repeat_policy.repeat_until.hour = 0;
	repeat_policy.repeat_until.minute = 10;

    reset_task_flags();

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_create("ma.task.101", &task_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_name(task_1, "Task 1"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_type(task_1, "Get"));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_software_id(task_1, "ma.cb.id.101"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_string("TASK_1", strlen("TASK_1"), &app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_app_payload(task_1, app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_creator_id(task_1, "ma.task.creator.101"));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time_limit(task_1, 1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_repeat_policy(task_1, repeat_policy));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&policy, &trigger_1));
	advance_current_date_by_seconds(30, &start_date);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_1, &start_date));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_1, trigger_1));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_create("ma.task.201", &task_2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_name(task_2, "Task 1"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_type(task_2, "Get"));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_software_id(task_2, "ma.cb.id.201"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_string("TASK_1", strlen("TASK_2"), &app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_app_payload(task_2, app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_creator_id(task_2, "ma.task.creator.201"));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time_limit(task_2, 1));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&policy, &trigger_2));
	advance_current_date_by_seconds(100, &start_date);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_2, &start_date));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_1, trigger_2));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_register_task_callbacks(g_ma_client, "ma.cb.id.101", "TYPE_1", &cb_1, NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_register_task_callbacks(g_ma_client, "ma.cb.id.201", "TYPE_2", &cb_2, NULL));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_add_task(g_ma_client, task_1));
    while(ctr < 2) {
        if(is_task_stopped) {
            ctr++;
            is_task_stopped = MA_FALSE;
        }
    }
    
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(app_payload));
	/* do not unregister task callbacks for this test case. it is all about testing whether the task is triggered after scheduler restarts */
}

TEST(scheduler_client_test, task_persistence_test_after) {
    ma_task_t *task = NULL;
	ma_task_time_t next_run_time = {0};

	ma_scheduler_task_callbacks_t cb = {&on_task_execute, &on_task_stop, &on_task_remove};
    
	/* register again */
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_register_task_callbacks(g_ma_client, "XXX", "Add", &cb, NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_get_task(g_ma_client, "REBORN", &task));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_next_run_time(task, &next_run_time));
	while(!done) {
         #ifdef WIN32
            Sleep(2 * 1000);
        #else
            sleep(2);
        #endif
    }
}
TEST(scheduler_client_test, add_task_test) {
    ma_task_t *task = NULL;
    ma_trigger_daily_policy_t policy = {0};
    ma_trigger_t *trigger = NULL;
    ma_task_time_t start_date = {0};
    ma_variant_t *app_payload = NULL;
    ma_scheduler_task_callbacks_t cb = {&on_task_execute, &on_task_stop, &on_task_remove};
	ma_scheduler_task_callbacks_t cb_1 = {&on_task_execute_1, &on_task_stop_1, &on_task_remove_1};
	ma_task_repeat_policy_t repeat_policy = {0};
	ma_task_t *task_1 = NULL;
	ma_trigger_t *trigger_1 = NULL;
    
    policy.days_interval = 5;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&policy, &trigger));
	advance_current_date_by_seconds(5, &start_date);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger, &start_date));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&policy, &trigger_1));
	advance_current_date_by_seconds(10, &start_date);
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_1, &start_date));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_create("ma.pp1.1001", &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_name(task, "Task 1"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_type(task, "Get"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_string("add task", strlen("add task"), &app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_app_payload(task, app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_creator_id(task, "scheduler consumer"));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_software_id(task, "ZZZ"));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time(task, 1));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_create("ma.pp1.5005", &task_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_name(task_1, "Task 1"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_type(task_1, "Get"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_1, trigger_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_string("add task", strlen("add task"), &app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_app_payload(task_1, app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_creator_id(task_1, "scheduler consumer"));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_software_id(task_1, "ZZZ"));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time(task_1, 1));

	repeat_policy.repeat_interval.hour = 0;
	repeat_policy.repeat_interval.minute = 0;
	repeat_policy.repeat_until.hour = 0;
	repeat_policy.repeat_until.minute = 0;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_repeat_policy(task, repeat_policy));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_repeat_policy(task_1, repeat_policy));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_register_task_callbacks(g_ma_client, "ZZZ", "Add", &cb, NULL));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_register_task_callbacks(g_ma_client, "ZZZ", "Add_1", &cb_1, NULL));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_add_task(g_ma_client, task));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_add_task(g_ma_client, task_1));
    
    // wait for the published messages
    while(0 == done || 0 == done_1) {
         #ifdef WIN32
            Sleep(2 * 1000);
        #else
            sleep(2);
        #endif
    }

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(app_payload));

	TEST_ASSERT_EQUAL_INT(MA_ERROR_SCHEDULER_CALLBACKS_ALREADY_REGISTERED, ma_scheduler_register_task_callbacks(g_ma_client, "ZZZ", "Add", &cb, NULL));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_SCHEDULER_CALLBACKS_ALREADY_REGISTERED, ma_scheduler_register_task_callbacks(g_ma_client, "ZZZ", "Add_1", &cb_1, NULL));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_unregister_task_callbacks(g_ma_client, "ZZZ", "Add"));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_unregister_task_callbacks(g_ma_client, "ZZZ", "Add_1"));
}

TEST(scheduler_client_test, get_task_test) {
    ma_task_t *task_1 = NULL;
    ma_task_t *task_2 = NULL;
    ma_task_t *get_task_1 = NULL;
    ma_trigger_daily_policy_t policy = {0};
    ma_trigger_t *trigger_1 = NULL;
    ma_trigger_t *trigger_2 = NULL;
    ma_trigger_t *get_trigger_1 = NULL;
    ma_task_time_t start_date = {0};
    ma_variant_t *app_payload = NULL;
    ma_scheduler_task_callbacks_t cb = {&on_task_execute, &on_task_stop, &on_task_remove};
    
    policy.days_interval = 3;
    advance_current_date_by_seconds(20, &start_date);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&policy, &trigger_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_1, &start_date));
    advance_current_date_by_seconds(40, &start_date);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&policy, &trigger_2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_2, &start_date));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_create("ma.pp1.1001", &task_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_name(task_1, "Task 1"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_type(task_1, "Get"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_create("ma.pp1.2001", &task_2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_name(task_2, "Task 1"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_type(task_2, "Get"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_1, trigger_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_2, trigger_2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_string("Get Task", strlen("Get Task"), &app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_app_payload(task_1, app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_app_payload(task_2, app_payload));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_creator_id(task_1, __FUNCTION__));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_creator_id(task_2, __FUNCTION__));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_register_task_callbacks(g_ma_client, "ZZZ", "Get Task", &cb, NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_add_task(g_ma_client, task_1));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_APIFAILED, ma_scheduler_get_task(g_ma_client, "ma.pp1.xyz", &get_task_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_get_task(g_ma_client, "ma.pp1.1001", &get_task_1));
    //TEST_ASSERT_EQUAL_INT(task_1, get_task_1); /* this is not the way to check if two tasks are same. */
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_find_trigger(get_task_1, "0", &get_trigger_1));
    //TEST_ASSERT_EQUAL_INT(trigger_1, get_trigger_1); /* this is not the way to check if two triggers are same. */

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_remove_task(g_ma_client, "ma.pp1.1001"));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_APIFAILED, ma_scheduler_get_task(g_ma_client, "ma.pp1.1001", &get_task_1));
    
    // wait for the published messages
    while(!done) {
    }

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(app_payload));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_unregister_task_callbacks(g_ma_client, "ZZZ", "Get"));
}

TEST(scheduler_client_test, stop_service) {
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_service_stop(g_scheduler_service));
    ma_service_release(g_scheduler_service);
    ma_ut_setup_release(g_ut_setup);
}


static void run_all_unit_tests(void) {
    do {RUN_TEST_GROUP(scheduler_client_api_test_group); } while (0);
}

static void run_all_function_tests(void) {
    do {RUN_TEST_GROUP(scheduler_client_daily_test_group);} while (0);
	do {RUN_TEST_GROUP(scheduler_client_test_group);} while (0);
	do {RUN_TEST_GROUP(scheduler_client_run_once_test_group);} while (0);
}


int main(int argc, char *argv[]) {
    int i =	0;
    int group = 0;
#ifdef MA_WINDOWS
    MA_TRACKLEAKS;
#endif

    if(argc == 1)
        return UnityMain(argc, argv, run_all_unit_tests);
    else {
        for(i =0;i<argc;i++)
            if(!strcmp("-g",argv[i])){
                group = 1;
                break;
            }
            if(group)
                return UnityMain(argc, argv, run_all_function_tests);
            else
                return UnityMain(argc, argv, run_all_unit_tests);
    }
}
