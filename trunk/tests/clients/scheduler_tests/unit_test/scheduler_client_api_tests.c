#include "unity.h"
#include "unity_fixture.h"

#include "ma/ma_common.h"
#include "ma/internal/ma_macros.h"
#include "ma/scheduler/ma_triggers.h"
#include "ma/scheduler/ma_triggerlist.h"
#include "ma/scheduler/ma_task.h"
#include "ma/internal/defs/ma_scheduler_defs.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"

#include <uv.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


#ifdef MA_WINDOWS
# include <crtdbg.h>
#include <Windows.h>
# define MA_TRACKLEAKS _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF)
#else
# define MA_TRACKLEAKS ((void *)0)
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#endif
ma_task_t *task = NULL;

extern ma_error_t ma_trigger_list_create(ma_trigger_list_t **);
extern ma_error_t ma_trigger_list_release(ma_trigger_list_t *);

TEST_GROUP_RUNNER(scheduler_client_api_test_group) {
    //do {RUN_TEST_CASE(scheduler_client_api_test, client_trigger_set_get_date); }while(0);
    do {RUN_TEST_CASE(scheduler_client_api_test, client_trigger_create_run_once); }while(0);
    do {RUN_TEST_CASE(scheduler_client_api_test, client_create_daily); }while(0);
    do {RUN_TEST_CASE(scheduler_client_api_test, client_create_weekly); }while(0);
    do {RUN_TEST_CASE(scheduler_client_api_test, client_create_monthly_date); }while(0);
    do {RUN_TEST_CASE(scheduler_client_api_test, client_create_monthly_dow); }while(0);
    do {RUN_TEST_CASE(scheduler_client_api_test, client_triggerlist_test); }while(0);
	do {RUN_TEST_CASE(scheduler_client_api_test, ma_task_create); }while(0);
	do {RUN_TEST_CASE(scheduler_client_api_test, ma_task_set_missed_policy); }while(0);
	do {RUN_TEST_CASE(scheduler_client_api_test, ma_task_set_app_payload); }while(0);
	do {RUN_TEST_CASE(scheduler_client_api_test, ma_task_set_retry_policy); }while(0);
	do {RUN_TEST_CASE(scheduler_client_api_test, ma_task_set_repeat_policy); }while(0);
	do {RUN_TEST_CASE(scheduler_client_api_test, ma_task_set_max_run_time); }while(0);
	do {RUN_TEST_CASE(scheduler_client_api_test, ma_task_set_max_run_time_limit); }while(0);
	do {RUN_TEST_CASE(scheduler_client_api_test, ma_task_set_randomize_policy); }while(0);


	do {RUN_TEST_CASE(scheduler_client_api_test, ma_task_set_priority); }while(0);
	do {RUN_TEST_CASE(scheduler_client_api_test, ma_task_set_conditions); }while(0);
	do {RUN_TEST_CASE(scheduler_client_api_test, ma_task_set_execution_status); }while(0);
	do {RUN_TEST_CASE(scheduler_client_api_test, ma_task_set_delay_policy); }while(0);
	do {RUN_TEST_CASE(scheduler_client_api_test, ma_task_set_creator_id); }while(0);
	do {RUN_TEST_CASE(scheduler_client_api_test, ma_task_get_state); }while(0);
	do {RUN_TEST_CASE(scheduler_client_api_test, ma_task_get_last_run_time); }while(0);
	do {RUN_TEST_CASE(scheduler_client_api_test, ma_task_get_next_run_time); }while(0);
	do {RUN_TEST_CASE(scheduler_client_api_test, ma_task_get_app_payload); }while(0);
	do {RUN_TEST_CASE(scheduler_client_api_test, ma_task_get_missed_policy); }while(0);
	do {RUN_TEST_CASE(scheduler_client_api_test, ma_task_get_repeat_policy); }while(0);
	do {RUN_TEST_CASE(scheduler_client_api_test, ma_task_get_priority); }while(0);
	do {RUN_TEST_CASE(scheduler_client_api_test, ma_task_get_conditions); }while(0);
	do {RUN_TEST_CASE(scheduler_client_api_test, ma_task_get_id); }while(0);
	do {RUN_TEST_CASE(scheduler_client_api_test, ma_task_get_name); }while(0);
	do {RUN_TEST_CASE(scheduler_client_api_test, ma_task_get_type); }while(0);
	do {RUN_TEST_CASE(scheduler_client_api_test, ma_task_get_creator_id); }while(0);
	do {RUN_TEST_CASE(scheduler_client_api_test, ma_task_get_retry_policy); }while(0);
	do {RUN_TEST_CASE(scheduler_client_api_test, ma_task_get_max_run_time); }while(0);
	do {RUN_TEST_CASE(scheduler_client_api_test, ma_task_get_max_run_time_limit); }while(0);
	do {RUN_TEST_CASE(scheduler_client_api_test, ma_task_get_execution_status); }while(0);
	//do {RUN_TEST_CASE(scheduler_client_api_test, ma_task_add_trigger); }while(0);
	do {RUN_TEST_CASE(scheduler_client_api_test, ma_task_remove_trigger); }while(0);
	do {RUN_TEST_CASE(scheduler_client_api_test, ma_task_find_trigger); }while(0);
	do {RUN_TEST_CASE(scheduler_client_api_test, ma_task_release); }while(0);
}

TEST_SETUP(scheduler_client_api_test) {
}

TEST_TEAR_DOWN(scheduler_client_api_test) {
}

TEST(scheduler_client_api_test, client_trigger_set_get_date) {
    ma_trigger_run_once_policy_t policy = {0};
    ma_trigger_t *trigger = NULL;
    ma_task_time_t start_date = {0};
    ma_task_time_t get_start_date = {0};
    ma_task_time_t end_date = {0};
    ma_task_time_t get_end_date = {0};
    policy.once = MA_TRUE;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger));
    ma_trigger_release(trigger);

    get_localtime(&start_date);
    start_date.minute += 1;
    end_date = start_date;
    end_date.hour += 2;

    //valid Tests
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger, NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger, &start_date));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_get_begin_date(trigger, &get_start_date));
    TEST_ASSERT_EQUAL_INT(get_start_date.day,start_date.day);

    TEST_ASSERT_EQUAL_INT(MA_OK,ma_trigger_set_end_date(trigger,&end_date));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_get_end_date(trigger, &get_end_date));
    TEST_ASSERT_EQUAL_INT(get_end_date.day,end_date.day);

    //Invalid Tests
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_set_begin_date(NULL, &start_date));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_set_begin_date(NULL, NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_get_begin_date(trigger, NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_get_begin_date(NULL, &get_start_date));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_get_begin_date(NULL, NULL));

    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_set_end_date(trigger, NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_set_end_date(NULL, &end_date));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger, NULL));
    end_date.day -= 4;
    TEST_ASSERT_EQUAL_INT(MA_ERROR_SCHEDULER_INVALID_DATE, ma_trigger_set_end_date(trigger, &end_date));

    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_get_end_date(trigger, NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_get_end_date(NULL, &get_start_date));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_get_end_date(NULL, NULL));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));
}

TEST(scheduler_client_api_test, client_trigger_create_run_once) {
    ma_trigger_run_once_policy_t policy = {0};
    ma_trigger_t *trigger = NULL;
    ma_trigger_type_t type = MA_TRIGGER_ONCE;
    policy.once = MA_TRUE;
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_create_run_once(NULL, NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_create_run_once(NULL, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));
    policy.once = MA_FALSE;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_get_type(trigger, &type));
    TEST_ASSERT_EQUAL_INT(MA_TRIGGER_ONCE, type);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));
}

TEST(scheduler_client_api_test, client_create_daily) {
    ma_trigger_daily_policy_t policy = {0};
    ma_trigger_daily_policy_t get_policy = {0};
    ma_trigger_t *trigger = NULL;
    ma_trigger_type_t type = MA_TRIGGER_DAILY;
    policy.days_interval = -2;
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_create_daily(NULL, NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_create_daily(NULL, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&policy, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_get_type(trigger, &type));
    TEST_ASSERT_EQUAL_INT(MA_TRIGGER_DAILY, type);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_get_policy(trigger, &get_policy));
    TEST_ASSERT_EQUAL_INT(policy.days_interval, get_policy.days_interval);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));
}

TEST(scheduler_client_api_test, client_create_weekly) {
    ma_trigger_weekly_policy_t policy = {0};
    ma_trigger_t *trigger = NULL;
    ma_trigger_type_t type = MA_TRIGGER_WEEKLY;
    policy.days_of_the_week = WEDNESDAY;
    policy.weeks_interval = 3;
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_create_weekly(NULL, NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_create_weekly(NULL, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_weekly(&policy, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_get_type(trigger, &type));
    TEST_ASSERT_EQUAL_INT(MA_TRIGGER_WEEKLY, type);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));
}

TEST(scheduler_client_api_test, client_create_monthly_date) {
    ma_trigger_monthly_date_policy_t policy = {0};
    ma_trigger_t *trigger = NULL;
    ma_trigger_type_t type = MA_TRIGGER_MONTHLY_DATE;
    policy.date = 29;
    policy.months = FEBRUARY;
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_create_monthly_date(NULL, NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_create_monthly_date(NULL, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_monthly_date(&policy, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_get_type(trigger, &type));
    TEST_ASSERT_EQUAL_INT(MA_TRIGGER_MONTHLY_DATE, type);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));
}

TEST(scheduler_client_api_test, client_create_monthly_dow) {
    ma_trigger_monthly_dow_policy_t policy = {0};
    ma_trigger_t *trigger = NULL;
    ma_trigger_type_t type = MA_TRIGGER_MONTHLY_DOW;
    policy.day_of_the_week = FRIDAY;
    policy.months = DECEMBER;
    policy.which_week = SECOND_WEEK;
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_create_monthly_dow(NULL, NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_create_monthly_dow(NULL, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_monthly_dow(&policy, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_get_type(trigger, &type));
    TEST_ASSERT_EQUAL_INT(MA_TRIGGER_MONTHLY_DOW, type);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));
}


TEST(scheduler_client_api_test, client_triggerlist_test) {
    ma_trigger_list_t *tl = NULL;
    ma_trigger_t *trigger1 = NULL;
    ma_trigger_t *trigger2 = NULL;
    ma_trigger_t *trigger3 = NULL;
    ma_trigger_t *ret_trigger = NULL;
    ma_trigger_t *ret_trigger_at = NULL;
    ma_trigger_run_once_policy_t runonce = {0};
    const char *trigger3_id = NULL;
    size_t size = 0;

    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_list_create(NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_list_create(&tl));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&runonce, &trigger1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&runonce, &trigger2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&runonce, &trigger3));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_list_add(NULL, trigger1));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_list_add(tl, NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_list_add(tl, trigger1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_list_add(tl, trigger2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_list_add(tl, trigger3));
    ma_trigger_release(trigger1);
    ma_trigger_release(trigger2);
    ma_trigger_release(trigger3);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_list_size(tl, &size));
    TEST_ASSERT_EQUAL_INT(3, size);

    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_list_at(NULL, 0, &ret_trigger_at));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_list_at(tl, 0, NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_list_at(tl, 0, &ret_trigger_at));
    TEST_ASSERT_EQUAL_PTR(trigger1, ret_trigger_at);
    ma_trigger_release(ret_trigger_at);
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_list_find(NULL, NULL, &ret_trigger));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_list_find(NULL, trigger3_id, &ret_trigger));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_get_id(trigger3, &trigger3_id));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_list_find(tl, trigger3_id, &ret_trigger));
    TEST_ASSERT_EQUAL_PTR(trigger3,ret_trigger);
    ma_trigger_release(ret_trigger);
    //Curently this is kept as expected because remove is not remove the trigger it just
    //stops it hence still find will return the removed triger.
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_list_remove(tl, trigger3_id));
    //TEST_ASSERT_EQUAL_INT(2, size);

    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_list_remove(tl, NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_list_remove(NULL,trigger3_id));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_list_release(NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_list_release(tl));

}


TEST(scheduler_client_api_test, ma_task_create) {
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_create("ma.pp1.1001", &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_name(task, "Task 1"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_type(task, "Add"));
}

TEST(scheduler_client_api_test, ma_task_set_missed_policy) {
	ma_task_missed_policy_t missed_policy;
	missed_policy.missed_duration.hour = 1;
	missed_policy.missed_duration.minute = 30;
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_missed_policy(task, missed_policy));
}

TEST(scheduler_client_api_test, ma_task_set_app_payload) {
	ma_variant_t *app_variant = NULL;
	ma_variant_create(&app_variant);
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_set_app_payload(task,app_variant));
}
	
TEST(scheduler_client_api_test, ma_task_set_retry_policy) {	
	ma_task_retry_policy_t retry_policy;
	retry_policy.retry_count = 1;
	retry_policy.retry_interval.hour=1;
	retry_policy.retry_interval.minute=1;
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_set_retry_policy(task, retry_policy));
}
	
TEST(scheduler_client_api_test, ma_task_set_repeat_policy) {
	ma_task_repeat_policy_t repeat_policy;
	repeat_policy.repeat_interval.hour=11;
	repeat_policy.repeat_until.hour=11;
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_set_repeat_policy(task,repeat_policy));
}
	
TEST(scheduler_client_api_test, ma_task_set_max_run_time) {
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_set_max_run_time(task,10));
}

TEST(scheduler_client_api_test, ma_task_set_max_run_time_limit) {
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_set_max_run_time_limit(task,10));
}

TEST(scheduler_client_api_test, ma_task_set_randomize_policy) {
	ma_task_randomize_policy_t randomize_policy;
	randomize_policy.random_interval.hour=11;
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_set_randomize_policy(task,randomize_policy));
}
	
TEST(scheduler_client_api_test, ma_task_set_priority) {
	ma_task_priority_t priority = MA_TASK_PRIORITY_HIGH;
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_set_priority(task,priority));
}
	
TEST(scheduler_client_api_test, ma_task_set_conditions) {
	ma_task_cond_t conds = MA_TASK_COND_INTERACTIVE;
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_set_conditions(task,conds));
}
	
TEST(scheduler_client_api_test, ma_task_set_execution_status) {
	 ma_task_exec_status_t status=MA_TASK_EXEC_RUNNING;
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_set_execution_status(task,status));
}
	
TEST(scheduler_client_api_test, ma_task_set_delay_policy) {
	ma_task_delay_policy_t delay_policy;
	delay_policy.delay_time.hour=11;
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_set_delay_policy(task,delay_policy));
}
	
TEST(scheduler_client_api_test, ma_task_set_creator_id) {
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_set_creator_id(task,"001"));
}
	
TEST(scheduler_client_api_test, ma_task_get_state) {
	ma_task_state_t state = MA_TASK_STATE_NOT_SCHEDULED;
	//task->state = MA_TASK_STATE_NOT_SCHEDULED;
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_state(task,&state));
}
	
TEST(scheduler_client_api_test, ma_task_get_last_run_time) {
	ma_task_time_t last_run_time;
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_last_run_time(task,&last_run_time));
}
	
TEST(scheduler_client_api_test, ma_task_get_next_run_time) {
	ma_task_time_t next_run_time;
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_next_run_time(task,&next_run_time));
}
	
TEST(scheduler_client_api_test, ma_task_get_app_payload) {
	ma_variant_t *app_variant = NULL;
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_app_payload(task,&app_variant));
    ma_variant_release(app_variant);
}
	
TEST(scheduler_client_api_test, ma_task_get_missed_policy) {
	ma_task_missed_policy_t missed_policy;
	missed_policy.missed_duration.hour=10;
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_missed_policy(task,&missed_policy));
}
	
TEST(scheduler_client_api_test, ma_task_get_repeat_policy) {
	ma_task_repeat_policy_t  repeat_policy;
	repeat_policy.repeat_interval.hour=11;
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_repeat_policy(task,&repeat_policy));
}
	
TEST(scheduler_client_api_test, ma_task_get_priority) {
	ma_task_priority_t priority = MA_TASK_PRIORITY_HIGH;
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_priority(task,&priority));
}
	
TEST(scheduler_client_api_test, ma_task_get_conditions) {
	ma_uint32_t i_conds;
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_conditions(task,&i_conds));
}
	
TEST(scheduler_client_api_test, ma_task_get_id) {
	char *task_id = "task_id_001";
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_id(task,&task_id));
}
	
TEST(scheduler_client_api_test, ma_task_get_name) {
	char *task_name = "task_name_001";
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_name(task,&task_name));
}
	
TEST(scheduler_client_api_test, ma_task_get_type) {
	 char *task_type = "task_type_001";
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_type(task,&task_type));
}
	
TEST(scheduler_client_api_test, ma_task_get_creator_id) {
	 char *creator_id = "creator_id_001";
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_creator_id(task,&creator_id));
}

TEST(scheduler_client_api_test, ma_task_get_retry_policy) {
	ma_task_retry_policy_t retry_policy;
	retry_policy.retry_count = 1;
	retry_policy.retry_interval.hour=1;
	retry_policy.retry_interval.minute=1;
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_retry_policy(task,&retry_policy));
}
	
TEST(scheduler_client_api_test, ma_task_get_max_run_time) {
	ma_uint16_t max_run_time;
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_max_run_time(task,&max_run_time));
}
	
TEST(scheduler_client_api_test, ma_task_get_max_run_time_limit) {
	ma_uint16_t max_run_time_limit;
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_max_run_time_limit(task,&max_run_time_limit));
}
	
TEST(scheduler_client_api_test, ma_task_get_execution_status) {
	ma_task_exec_status_t status;
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_execution_status(task,&status));
}
	
	
TEST(scheduler_client_api_test, ma_task_add_trigger) {
	ma_trigger_t *trigger=NULL;
	ma_trigger_daily_policy_t run_daily_policy = {0};
	run_daily_policy.days_interval = 1;
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&run_daily_policy, &trigger));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_add_trigger(task,trigger));
}
	
TEST(scheduler_client_api_test, ma_task_remove_trigger) {
	char *trigger_id = "trigger_1" ;
	//TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_remove_trigger(task,trigger_id));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_SCHEDULER_INVALID_TRIGGER_ID,ma_task_remove_trigger(task,trigger_id));
}
	
TEST(scheduler_client_api_test, ma_task_find_trigger) {
	ma_trigger_t *trigger=NULL;
	char *trigger_id = "trigger_1" ;
	ma_trigger_daily_policy_t run_daily_policy = {0};
	run_daily_policy.days_interval = 1;
	//TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_find_trigger(task,trigger_id,&trigger ));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_SCHEDULER_INVALID_TRIGGER_ID,ma_task_find_trigger(task,trigger_id,&trigger ));
}
	
TEST(scheduler_client_api_test, ma_task_release) {
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_release(task));
}