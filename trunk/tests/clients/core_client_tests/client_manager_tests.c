/*****************************************************************************
Core Client - Client Manager Unit Tests.
*****************************************************************************/

#define UNITY_FIXTURE_MALLOC_OVERRIDES_H_ 1

#include "unity.h"
#include "unity_fixture.h"
#include "unity_internals.h"
#include "ma/internal/clients/ma/client/ma_client_manager.h"
#include "ma/logger/ma_console_logger.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_message.h"
#include <stdlib.h>
#include <string.h>
#include <uv.h>

#ifdef MA_WINDOWS
    #include <Windows.h>
#else
    #include <unistd.h>
#endif

// Global variable declaration
static ma_msgbus_t *g_msgbus = NULL;

static ma_bool_t done = 0;
static uv_cond_t cond;
static uv_mutex_t mux;

static char *global_topic = NULL;
static ma_bool_t release_hit = MA_FALSE;

// Following objects are used for threading tests.
static ma_client_manager_t *global_client_manager = NULL; // Global client manager object
static ma_bool_t added = MA_FALSE; // Global variable for synchronization b/w threads
static const char *clients[] = { "client_111", "client_222", "client_333", "client_444", "client_555", "client_666", "client_777", "client_888", "client_999", "client_000" }; // List of client names


static void wait_until_done()
{
    uv_mutex_lock(&mux);
    while (!done)
    {
        uv_cond_wait(&cond,&mux);
    }
    uv_mutex_unlock(&mux);
}

static void set_done()
{
    uv_mutex_lock(&mux);
    done = MA_TRUE;
    uv_cond_signal(&cond);
    uv_mutex_unlock(&mux);
}

TEST_GROUP_RUNNER(ma_core_client_manager_test_group)
{
    do {RUN_TEST_CASE(ma_core_client_manager_tests, client_manager_create_release_test)} while (0);
    do {RUN_TEST_CASE(ma_core_client_manager_tests, client_manager_create_release_test_threading_multiple_client_managers)} while (0);
    do {RUN_TEST_CASE(ma_core_client_manager_tests, client_manager_create_release_test_threading_single_client_manager)} while (0);
    do {RUN_TEST_CASE(ma_core_client_manager_tests, client_manager_create_release_test_threading_verification_in_main_thread)} while (0);
}

/**************************************************************************
1) Creating the unit test context.
2) Retrieving underlying message bus instance.
3) Creating and starting message bus service (IO Service) using the
   message bus instance.
**************************************************************************/
TEST_SETUP(ma_core_client_manager_tests)
{
    uv_mutex_init(&mux);
    uv_cond_init(&cond);

    TEST_ASSERT(MA_OK == ma_msgbus_create("CORE_CLIENT", &g_msgbus));
    TEST_ASSERT(MA_OK == ma_msgbus_start(g_msgbus));
}

/**************************************************************************
1) Stopping and releasing the message bus service (IO service).
2) Releasing the context.
**************************************************************************/
TEST_TEAR_DOWN(ma_core_client_manager_tests)
{
    TEST_ASSERT(MA_OK == ma_msgbus_stop(g_msgbus, MA_TRUE));
    TEST_ASSERT(MA_OK == ma_msgbus_release(g_msgbus));

    uv_cond_destroy(&cond);
    uv_mutex_destroy(&mux);
}

ma_error_t my_client_on_message(ma_client_base_t *client_base, const char *topic, ma_message_t *message)
{
    global_topic = strdup(topic);
    set_done();
    return MA_OK;
}

 ma_error_t my_client_release(ma_client_base_t *client_base)
{
    release_hit = MA_TRUE;
    set_done();
    return MA_OK;
}

TEST(ma_core_client_manager_tests, client_manager_create_release_test)
{
    ma_client_manager_t *my_client_manager = NULL;
    ma_console_logger_t *console_logger = NULL;

    // Creating Client Manager
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_client_manager_create(NULL, &my_client_manager));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_client_manager_create(g_msgbus, NULL));
    TEST_ASSERT(MA_OK == ma_client_manager_create(g_msgbus, &my_client_manager));

    // Creating and adding logger to the Client Manager
    TEST_ASSERT(MA_OK == ma_console_logger_create(&console_logger));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_client_manager_set_logger(NULL, (ma_logger_t *)console_logger));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_client_manager_set_logger(my_client_manager, NULL));
    TEST_ASSERT(MA_OK == ma_client_manager_set_logger(my_client_manager, (ma_logger_t *)console_logger));

    // Setting Thread Option and retrieving Thread Option
    {
        ma_msgbus_callback_thread_options_t t_option = MA_MSGBUS_CALLBACK_THREAD_APC;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_client_manager_set_thread_option(NULL, MA_MSGBUS_CALLBACK_THREAD_IO));
        TEST_ASSERT(MA_OK == ma_client_manager_set_thread_option(my_client_manager, MA_MSGBUS_CALLBACK_THREAD_IO));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_client_manager_get_thread_option(NULL, &t_option));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_client_manager_get_thread_option(my_client_manager, NULL));
        TEST_ASSERT(MA_OK == ma_client_manager_get_thread_option(my_client_manager, &t_option));
        TEST_ASSERT(t_option == MA_MSGBUS_CALLBACK_THREAD_IO);

        TEST_ASSERT(MA_OK == ma_client_manager_set_thread_option(my_client_manager, MA_MSGBUS_CALLBACK_THREAD_POOL));
        TEST_ASSERT(MA_OK == ma_client_manager_get_thread_option(my_client_manager, &t_option));
        TEST_ASSERT(t_option == MA_MSGBUS_CALLBACK_THREAD_POOL);
    }

    // Setting Consumer Reachability and retrieving Consumer Reachability
    {
        ma_msgbus_consumer_reachability_t con_reach = MSGBUS_CONSUMER_REACH_INPROC;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_client_manager_set_consumer_reachability(NULL, MSGBUS_CONSUMER_REACH_OUTBOX));
        TEST_ASSERT(MA_OK == ma_client_manager_set_consumer_reachability(my_client_manager, MSGBUS_CONSUMER_REACH_OUTBOX));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_client_manager_get_consumer_reachability(NULL, &con_reach));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_client_manager_get_consumer_reachability(my_client_manager, NULL));
        TEST_ASSERT(MA_OK == ma_client_manager_get_consumer_reachability(my_client_manager, &con_reach));
        TEST_ASSERT(con_reach == MSGBUS_CONSUMER_REACH_OUTBOX);

        TEST_ASSERT(MA_OK == ma_client_manager_set_consumer_reachability(my_client_manager, MSGBUS_CONSUMER_REACH_OUTPROC));
        TEST_ASSERT(MA_OK == ma_client_manager_get_consumer_reachability(my_client_manager, &con_reach));
        TEST_ASSERT(con_reach == MSGBUS_CONSUMER_REACH_OUTPROC);
    }

    // Starting Client Manager
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_client_manager_start(NULL));
    TEST_ASSERT(MA_OK == ma_client_manager_start(my_client_manager));

    {
        // Creating a client
        ma_client_base_t *my_client = NULL;
        ma_client_base_t *new_client = NULL;  // for retrieving the reference

        struct ma_client_base_methods_s my_client_methods = { my_client_on_message, my_client_release };
        my_client = (ma_client_base_t *)calloc(sizeof(ma_client_base_t), 1);
        ma_client_base_init(my_client, &my_client_methods, NULL);

        // Checking if the client manager already holds a client with same name (It should
        // return false)
        TEST_ASSERT(MA_FALSE == ma_client_manager_has_client(my_client_manager, NULL));
        TEST_ASSERT(MA_FALSE == ma_client_manager_has_client(NULL, "Client_1"));
        TEST_ASSERT(MA_FALSE == ma_client_manager_has_client(my_client_manager, "Client_1"));

        // Adding it to Client Manager
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_client_manager_add_client(NULL, "Client_1", my_client));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_client_manager_add_client(my_client_manager, NULL, my_client));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_client_manager_add_client(my_client_manager, "Client_1", NULL));
        TEST_ASSERT(MA_OK == ma_client_manager_add_client(my_client_manager, "Client_1", my_client));

        // Adding client with same client name
        TEST_ASSERT(MA_ERROR_CLIENT_ALREADY_REGISTERED == ma_client_manager_add_client(my_client_manager, "Client_1", my_client));

        // Verifying that client manager holds a client with name "Client_1"
        TEST_ASSERT(MA_FALSE == ma_client_manager_has_client(my_client_manager, NULL));
        TEST_ASSERT(MA_FALSE == ma_client_manager_has_client(NULL, "Client_1"));
        TEST_ASSERT(MA_TRUE == ma_client_manager_has_client(my_client_manager, "Client_1"));

        /**********************************************************************
        TEST 1 - Publishing a message with "ma.*" topic. The on_message API
        should be hit.
        **********************************************************************/
        // Resetting global variables
        global_topic = NULL;
        release_hit = MA_FALSE;
        done = 0;

        // Publishing Message
        {
            ma_message_t *payload = NULL;
            TEST_ASSERT(MA_OK == ma_message_create(&payload));
            TEST_ASSERT(MA_OK == ma_msgbus_publish(g_msgbus, "ma.client", MSGBUS_CONSUMER_REACH_INPROC, payload));
            TEST_ASSERT(MA_OK == ma_message_release(payload));
        }

        wait_until_done();

        // on_message API should be hit. Release API should not be hit
        TEST_ASSERT(0 == strcmp(global_topic, "ma.client"));
        TEST_ASSERT(MA_FALSE == release_hit);
        free(global_topic);

        /**********************************************************************
        TEST 2 - Publishing a message with a topic other than "ma.*". This
        message should not be handled by client manager.
        **********************************************************************/

        // Resetting global variables
        global_topic = NULL;
        release_hit = MA_FALSE;

        // Publishing Message
        {
            ma_message_t *payload = NULL;
            TEST_ASSERT(MA_OK == ma_message_create(&payload));
            TEST_ASSERT(MA_OK == ma_msgbus_publish(g_msgbus, "client", MSGBUS_CONSUMER_REACH_INPROC, payload));
            TEST_ASSERT(MA_OK == ma_message_release(payload));
        }

        #ifdef WIN32
            Sleep(1000);
        #else
            sleep(1);
        #endif

        // on_message or release API should not be hit
        TEST_ASSERT_NULL(global_topic);
        TEST_ASSERT(MA_FALSE == release_hit);

        /**********************************************************************
        TEST 3 - Removing the client from Client Manager and then publishing
        a message with a topic "ma.*". This message should not reach callback
        API.
        **********************************************************************/

        // Retrieving client reference from Client Manager and using it to remove client
        TEST_ASSERT(MA_ERROR_INVALIDARG ==  ma_client_manager_get_client(NULL, "Client_1", &new_client));
        TEST_ASSERT(MA_ERROR_INVALIDARG ==  ma_client_manager_get_client(my_client_manager, NULL, &new_client));
        TEST_ASSERT(MA_ERROR_INVALIDARG ==  ma_client_manager_get_client(my_client_manager, "Client_1", NULL));
        TEST_ASSERT(MA_OK ==  ma_client_manager_get_client(my_client_manager, "Client_1", &new_client));

        // Removing client from Client Manager
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_client_manager_remove_client(NULL, new_client));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_client_manager_remove_client(my_client_manager, NULL));
        TEST_ASSERT(MA_OK == ma_client_manager_remove_client(my_client_manager, new_client));

        // Verifying that client manager doesn't hold a client with name "Client_1"
        TEST_ASSERT(MA_FALSE == ma_client_manager_has_client(my_client_manager, "Client_1"));

        // Resetting global variables
        global_topic = NULL;
        release_hit = MA_FALSE;

        // Publishing Message
        {
            ma_message_t *payload = NULL;
            TEST_ASSERT(MA_OK == ma_message_create(&payload));
            TEST_ASSERT(MA_OK == ma_msgbus_publish(g_msgbus, "ma.client", MSGBUS_CONSUMER_REACH_INPROC, payload));
            TEST_ASSERT(MA_OK == ma_message_release(payload));
        }

        #ifdef WIN32
            Sleep(1000);
        #else
            sleep(1);
        #endif

        // on_message or release API should not be hit
        TEST_ASSERT_NULL(global_topic);
        TEST_ASSERT(MA_FALSE == release_hit);

        // Resetting global variables
        global_topic = NULL;
        release_hit = MA_FALSE;

        // Free the Client
        ma_client_base_release(new_client);
        free(new_client);

        // Release API should be hit
        TEST_ASSERT_NULL(global_topic);
        TEST_ASSERT(MA_TRUE == release_hit);
    }

    // Stopping Client Manager
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_client_manager_stop(NULL));
    TEST_ASSERT(MA_OK == ma_client_manager_stop(my_client_manager));

    // Releasing Client Manager
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_client_manager_release(NULL));
    TEST_ASSERT(MA_OK == ma_client_manager_release(my_client_manager));

    // Release console logger
    TEST_ASSERT(MA_OK == ma_logger_release((ma_logger_t *)console_logger));
}

static void client_manager_thread_1(void *param)
{
    ma_client_manager_t *my_client_manager = NULL;
    ma_uint8_t counter = 0;

    for(counter = 0; counter < 10; counter++)
    {
        // Creating Client Manager
        TEST_ASSERT(MA_OK == ma_client_manager_create(g_msgbus, &my_client_manager));

        // Setting Thread Option and Consumer Reachability
        TEST_ASSERT(MA_OK == ma_client_manager_set_thread_option(my_client_manager, MA_MSGBUS_CALLBACK_THREAD_POOL));
        TEST_ASSERT(MA_OK == ma_client_manager_set_consumer_reachability(my_client_manager, MSGBUS_CONSUMER_REACH_OUTPROC));

        // Starting Client Manager
        TEST_ASSERT(MA_OK == ma_client_manager_start(my_client_manager));

        {
            // Creating a client
            ma_client_base_t *my_client = NULL;
            ma_client_base_t *new_client = NULL;  // for retrieving the reference

            struct ma_client_base_methods_s my_client_methods = { my_client_on_message, my_client_release };
            my_client = (ma_client_base_t *)calloc(sizeof(ma_client_base_t), 1);
            ma_client_base_init(my_client, &my_client_methods, NULL);

            // Checking if the client manager already holds a client with same name (It should
            // return false)
            TEST_ASSERT(MA_FALSE == ma_client_manager_has_client(my_client_manager, "Client_1_1"));

            // Adding it to Client Manager
            TEST_ASSERT(MA_OK == ma_client_manager_add_client(my_client_manager, "Client_1_1", my_client));

            // Verifying that client manager holds a client with name "Client_1_1"
            TEST_ASSERT(MA_TRUE == ma_client_manager_has_client(my_client_manager, "Client_1_1"));

            // Retrieving client reference from Client Manager and using it to remove client
            TEST_ASSERT(MA_OK ==  ma_client_manager_get_client(my_client_manager, "Client_1_1", &new_client));

            // Removing client from Client Manager
            TEST_ASSERT(MA_OK == ma_client_manager_remove_client(my_client_manager, new_client));

            // Verifying that client manager doesn't hold a client with name "Client_1_1"
            TEST_ASSERT(MA_FALSE == ma_client_manager_has_client(my_client_manager, "Client_1_1"));

            // Free the Client
            ma_client_base_release(new_client);
            free(new_client);
        }

        // Stopping and Releasing Client Manager
        TEST_ASSERT(MA_OK == ma_client_manager_stop(my_client_manager));
        TEST_ASSERT(MA_OK == ma_client_manager_release(my_client_manager));
    }
}

static void client_manager_thread_2(void *param)
{
    ma_client_manager_t *my_client_manager = NULL;
    ma_uint8_t counter = 0;

    for(counter = 0; counter < 10; counter++)
    {
        // Creating Client Manager
        TEST_ASSERT(MA_OK == ma_client_manager_create(g_msgbus, &my_client_manager));

        // Setting Thread Option and Consumer Reachability
        TEST_ASSERT(MA_OK == ma_client_manager_set_thread_option(my_client_manager, MA_MSGBUS_CALLBACK_THREAD_POOL));
        TEST_ASSERT(MA_OK == ma_client_manager_set_consumer_reachability(my_client_manager, MSGBUS_CONSUMER_REACH_OUTPROC));

        // Starting Client Manager
        TEST_ASSERT(MA_OK == ma_client_manager_start(my_client_manager));

        {
            // Creating a client
            ma_client_base_t *my_client = NULL;
            ma_client_base_t *new_client = NULL;  // for retrieving the reference

            struct ma_client_base_methods_s my_client_methods = { my_client_on_message, my_client_release };
            my_client = (ma_client_base_t *)calloc(sizeof(ma_client_base_t), 1);
            ma_client_base_init(my_client, &my_client_methods, NULL);

            // Checking if the client manager already holds a client with same name (It should
            // return false)
            TEST_ASSERT(MA_FALSE == ma_client_manager_has_client(my_client_manager, "Client_2_1"));

            // Adding it to Client Manager
            TEST_ASSERT(MA_OK == ma_client_manager_add_client(my_client_manager, "Client_2_1", my_client));

            // Verifying that client manager holds a client with name "Client_2_1"
            TEST_ASSERT(MA_TRUE == ma_client_manager_has_client(my_client_manager, "Client_2_1"));

            // Retrieving client reference from Client Manager and using it to remove client
            TEST_ASSERT(MA_OK ==  ma_client_manager_get_client(my_client_manager, "Client_2_1", &new_client));

            // Removing client from Client Manager
            TEST_ASSERT(MA_OK == ma_client_manager_remove_client(my_client_manager, new_client));

            // Verifying that client manager doesn't hold a client with name "Client_2_1"
            TEST_ASSERT(MA_FALSE == ma_client_manager_has_client(my_client_manager, "Client_2_1"));

            // Free the Client
            ma_client_base_release(new_client);
            free(new_client);
        }

        // Stopping and Releasing Client Manager
        TEST_ASSERT(MA_OK == ma_client_manager_stop(my_client_manager));
        TEST_ASSERT(MA_OK == ma_client_manager_release(my_client_manager));
    }
}

static void client_manager_thread_3(void *param)
{
    ma_client_manager_t *my_client_manager = NULL;
    ma_uint8_t counter = 0;

    for(counter = 0; counter < 10; counter++)
    {
        // Creating Client Manager
        TEST_ASSERT(MA_OK == ma_client_manager_create(g_msgbus, &my_client_manager));

        // Setting Thread Option and Consumer Reachability
        TEST_ASSERT(MA_OK == ma_client_manager_set_thread_option(my_client_manager, MA_MSGBUS_CALLBACK_THREAD_POOL));
        TEST_ASSERT(MA_OK == ma_client_manager_set_consumer_reachability(my_client_manager, MSGBUS_CONSUMER_REACH_OUTPROC));

        // Starting Client Manager
        TEST_ASSERT(MA_OK == ma_client_manager_start(my_client_manager));

        {
            // Creating a client
            ma_client_base_t *my_client = NULL;
            ma_client_base_t *new_client = NULL;  // for retrieving the reference

            struct ma_client_base_methods_s my_client_methods = { my_client_on_message, my_client_release };
            my_client = (ma_client_base_t *)calloc(sizeof(ma_client_base_t), 1);
            ma_client_base_init(my_client, &my_client_methods, NULL);

            // Checking if the client manager already holds a client with same name (It should
            // return false)
            TEST_ASSERT(MA_FALSE == ma_client_manager_has_client(my_client_manager, "Client_3_1"));

            // Adding it to Client Manager
            TEST_ASSERT(MA_OK == ma_client_manager_add_client(my_client_manager, "Client_3_1", my_client));

            // Verifying that client manager holds a client with name "Client_3_1"
            TEST_ASSERT(MA_TRUE == ma_client_manager_has_client(my_client_manager, "Client_3_1"));

            // Retrieving client reference from Client Manager and using it to remove client
            TEST_ASSERT(MA_OK ==  ma_client_manager_get_client(my_client_manager, "Client_3_1", &new_client));

            // Removing client from Client Manager
            TEST_ASSERT(MA_OK == ma_client_manager_remove_client(my_client_manager, new_client));

            // Verifying that client manager doesn't hold a client with name "Client_3_1"
            TEST_ASSERT(MA_FALSE == ma_client_manager_has_client(my_client_manager, "Client_3_1"));

            // Free the Client
            ma_client_base_release(new_client);
            free(new_client);
        }

        // Stopping and Releasing Client Manager
        TEST_ASSERT(MA_OK == ma_client_manager_stop(my_client_manager));
        TEST_ASSERT(MA_OK == ma_client_manager_release(my_client_manager));
    }
}


/*
Client Manager tests for scenario where the clients are created and removed by
different threads
*/
TEST(ma_core_client_manager_tests, client_manager_create_release_test_threading_multiple_client_managers)
{
    /* Thread handles */
    uv_thread_t tid_1, tid_2, tid_3;

    /* Creating thread 1 */
    TEST_ASSERT(0 == uv_thread_create(&tid_1, client_manager_thread_1, NULL));

    /* Creating thread 2 */
    TEST_ASSERT(0 == uv_thread_create(&tid_2, client_manager_thread_2, NULL));

    /* Creating thread 3 */
    TEST_ASSERT(0 == uv_thread_create(&tid_3, client_manager_thread_3, NULL));

    // Wait until all threads have terminated.
    uv_thread_join(&tid_1);
    uv_thread_join(&tid_2);
    uv_thread_join(&tid_3);
}


static void client_manager_thread_4(void *param)
{
    ma_uint8_t counter = 0;

    for(counter = 0; counter < 10; counter++)
    {
        // Creating a client
        ma_client_base_t *my_client = NULL;
        ma_client_base_t *new_client = NULL;  // for retrieving the reference

        struct ma_client_base_methods_s my_client_methods = { my_client_on_message, my_client_release };
        my_client = (ma_client_base_t *)calloc(sizeof(ma_client_base_t), 1);
        ma_client_base_init(my_client, &my_client_methods, NULL);

        // Checking if the client manager already holds a client with same name (It should
        // return false)
        TEST_ASSERT(MA_FALSE == ma_client_manager_has_client(global_client_manager, "Client_4_1"));

        // Adding it to Client Manager
        TEST_ASSERT(MA_OK == ma_client_manager_add_client(global_client_manager, "Client_4_1", my_client));

        // Verifying that client manager holds a client with name "Client_4_1"
        TEST_ASSERT(MA_TRUE == ma_client_manager_has_client(global_client_manager, "Client_4_1"));

        // Retrieving client reference from Client Manager and using it to remove client
        TEST_ASSERT(MA_OK ==  ma_client_manager_get_client(global_client_manager, "Client_4_1", &new_client));

        // Removing client from Client Manager
        TEST_ASSERT(MA_OK == ma_client_manager_remove_client(global_client_manager, new_client));

        // Verifying that client manager doesn't hold a client with name "Client_4_1"
        TEST_ASSERT(MA_FALSE == ma_client_manager_has_client(global_client_manager, "Client_4_1"));

        // Free the Client
        ma_client_base_release(new_client);
        free(new_client);
    }
}

static void client_manager_thread_5(void *param)
{
    ma_uint8_t counter = 0;

    for(counter = 0; counter < 10; counter++)
    {
        // Creating a client
        ma_client_base_t *my_client = NULL;
        ma_client_base_t *new_client = NULL;  // for retrieving the reference

        struct ma_client_base_methods_s my_client_methods = { my_client_on_message, my_client_release };
        my_client = (ma_client_base_t *)calloc(sizeof(ma_client_base_t), 1);
        ma_client_base_init(my_client, &my_client_methods, NULL);

        // Checking if the client manager already holds a client with same name (It should
        // return false)
        TEST_ASSERT(MA_FALSE == ma_client_manager_has_client(global_client_manager, "Client_5_1"));

        // Adding it to Client Manager
        TEST_ASSERT(MA_OK == ma_client_manager_add_client(global_client_manager, "Client_5_1", my_client));

        // Verifying that client manager holds a client with name "Client_5_1"
        TEST_ASSERT(MA_TRUE == ma_client_manager_has_client(global_client_manager, "Client_5_1"));

        // Retrieving client reference from Client Manager and using it to remove client
        TEST_ASSERT(MA_OK ==  ma_client_manager_get_client(global_client_manager, "Client_5_1", &new_client));

        // Removing client from Client Manager
        TEST_ASSERT(MA_OK == ma_client_manager_remove_client(global_client_manager, new_client));

        // Verifying that client manager doesn't hold a client with name "Client_5_1"
        TEST_ASSERT(MA_FALSE == ma_client_manager_has_client(global_client_manager, "Client_5_1"));

        // Free the Client
        ma_client_base_release(new_client);
        free(new_client);
    }
}

static void client_manager_thread_6(void *param)
{
    ma_uint8_t counter = 0;

    for(counter = 0; counter < 10; counter++)
    {
        // Creating a client
        ma_client_base_t *my_client = NULL;
        ma_client_base_t *new_client = NULL;  // for retrieving the reference

        struct ma_client_base_methods_s my_client_methods = { my_client_on_message, my_client_release };
        my_client = (ma_client_base_t *)calloc(sizeof(ma_client_base_t), 1);
        ma_client_base_init(my_client, &my_client_methods, NULL);

        // Checking if the client manager already holds a client with same name (It should
        // return false)
        TEST_ASSERT(MA_FALSE == ma_client_manager_has_client(global_client_manager, "Client_6_1"));

        // Adding it to Client Manager
        TEST_ASSERT(MA_OK == ma_client_manager_add_client(global_client_manager, "Client_6_1", my_client));

        // Verifying that client manager holds a client with name "Client_6_1"
        TEST_ASSERT(MA_TRUE == ma_client_manager_has_client(global_client_manager, "Client_6_1"));

        // Retrieving client reference from Client Manager and using it to remove client
        TEST_ASSERT(MA_OK ==  ma_client_manager_get_client(global_client_manager, "Client_6_1", &new_client));

        // Removing client from Client Manager
        TEST_ASSERT(MA_OK == ma_client_manager_remove_client(global_client_manager, new_client));

        // Verifying that client manager doesn't hold a client with name "Client_6_1"
        TEST_ASSERT(MA_FALSE == ma_client_manager_has_client(global_client_manager, "Client_6_1"));

        // Free the Client
        ma_client_base_release(new_client);
        free(new_client);
    }
}


/*
Client Manager tests for scenario where the clients are created and removed by
different threads. The client manager is global object which is shared by all
threads.
*/
TEST(ma_core_client_manager_tests, client_manager_create_release_test_threading_single_client_manager)
{
    /* Thread handles */
    uv_thread_t tid_1, tid_2, tid_3;

    // Creating Client Manager (Global)
    TEST_ASSERT(MA_OK == ma_client_manager_create(g_msgbus, &global_client_manager));

    // Setting Thread Option and Consumer Reachability
    TEST_ASSERT(MA_OK == ma_client_manager_set_thread_option(global_client_manager, MA_MSGBUS_CALLBACK_THREAD_POOL));
    TEST_ASSERT(MA_OK == ma_client_manager_set_consumer_reachability(global_client_manager, MSGBUS_CONSUMER_REACH_OUTPROC));

    // Starting Client Manager
    TEST_ASSERT(MA_OK == ma_client_manager_start(global_client_manager));

    /* Creating thread 1 */
    TEST_ASSERT(0 == uv_thread_create(&tid_1, client_manager_thread_4, NULL));

    /* Creating thread 2 */
    TEST_ASSERT(0 == uv_thread_create(&tid_2, client_manager_thread_5, NULL));

    /* Creating thread 3 */
    TEST_ASSERT(0 == uv_thread_create(&tid_3, client_manager_thread_6, NULL));

    // Wait until all threads have terminated.
    uv_thread_join(&tid_1);
    uv_thread_join(&tid_2);
    uv_thread_join(&tid_3);

    // Stopping and Releasing Client Manager
    TEST_ASSERT(MA_OK == ma_client_manager_stop(global_client_manager));
    TEST_ASSERT(MA_OK == ma_client_manager_release(global_client_manager));
}


static void client_manager_thread_7(void *param)
{
    ma_uint8_t counter = 0;

    for(counter = 0; counter < sizeof(clients)/sizeof(clients[0]); counter++)
    {
        // Creating a client
        ma_client_base_t *my_client = NULL;

        struct ma_client_base_methods_s my_client_methods = { my_client_on_message, my_client_release };
        my_client = (ma_client_base_t *)calloc(sizeof(ma_client_base_t), 1);
        ma_client_base_init(my_client, &my_client_methods, NULL);

        // Adding it to Client Manager and setting "added" sync variable to true
        TEST_ASSERT(MA_OK == ma_client_manager_add_client(global_client_manager, clients[counter], my_client));
        added = MA_TRUE;

        // Verifying that client manager holds a client now
        TEST_ASSERT(MA_TRUE == ma_client_manager_has_client(global_client_manager, clients[counter]));

        // Waiting for client to be removed
        while(added == MA_TRUE)
        {
            #ifdef MA_WINDOWS
                Sleep(1000);
            #else
                sleep(1);
            #endif
        }

        // Client must have been removed by other thread.
        TEST_ASSERT(MA_FALSE == ma_client_manager_has_client(global_client_manager, clients[counter]));
    }
}

static void client_manager_thread_8(void *param)
{
    ma_uint8_t counter = 0;

    for(counter = 0; counter < sizeof(clients)/sizeof(clients[0]); counter++)
    {
        ma_client_base_t *new_client = NULL;  // for retrieving the reference

        // Waiting for client to be added
        while(added == MA_FALSE)
        {
            #ifdef MA_WINDOWS
                Sleep(1000);
            #else
                sleep(1);
            #endif
        }

        // Retrieving client reference from Client Manager and using it to remove client
        TEST_ASSERT(MA_OK == ma_client_manager_get_client(global_client_manager, clients[counter], &new_client))

        // Removing client from Client Manager and setting "added" sync flag to false
        TEST_ASSERT(MA_OK == ma_client_manager_remove_client(global_client_manager, new_client));
        added = MA_FALSE;

        // Verifying that client manager doesn't hold a client with name "Client_6_1"
        TEST_ASSERT(MA_FALSE == ma_client_manager_has_client(global_client_manager, clients[counter]));

        // Free the Client
        ma_client_base_release(new_client);
        free(new_client);
    }
}

/*
Client Manager tests for scenario where the clients are created in one thread and removed in other thread.

1) Add clients in one thread.
2) Remove clients in other thread.
3) Once, the thread joins, verify that the client manager doesn't own any threads.
*/
TEST(ma_core_client_manager_tests, client_manager_create_release_test_threading_verification_in_main_thread)
{
    ma_uint8_t counter = 0;

    /* Thread handles */
    uv_thread_t tid_1, tid_2;

    // Creating Client Manager (Global)
    TEST_ASSERT(MA_OK == ma_client_manager_create(g_msgbus, &global_client_manager));

    // Setting Thread Option and Consumer Reachability
    TEST_ASSERT(MA_OK == ma_client_manager_set_thread_option(global_client_manager, MA_MSGBUS_CALLBACK_THREAD_POOL));
    TEST_ASSERT(MA_OK == ma_client_manager_set_consumer_reachability(global_client_manager, MSGBUS_CONSUMER_REACH_OUTPROC));

    // Starting Client Manager
    TEST_ASSERT(MA_OK == ma_client_manager_start(global_client_manager));

    /* Creating thread 1 */
    TEST_ASSERT(0 == uv_thread_create(&tid_1, client_manager_thread_7, NULL));

    /* Creating thread 2 */
    TEST_ASSERT(0 == uv_thread_create(&tid_2, client_manager_thread_8, NULL));

    // Wait until all threads have terminated.
    uv_thread_join(&tid_1);
    uv_thread_join(&tid_2);

    // Verifying that all clients have been removed by the time..main thread is reached.
    for(counter = 0; counter < sizeof(clients)/sizeof(clients[0]); counter++)
    {
       TEST_ASSERT(MA_FALSE == ma_client_manager_has_client(global_client_manager, clients[counter]));
    }

    // Stopping and Releasing Client Manager
    TEST_ASSERT(MA_OK == ma_client_manager_stop(global_client_manager));
    TEST_ASSERT(MA_OK == ma_client_manager_release(global_client_manager));
}

