/*****************************************************************************
Core Client - Client Handler Unit Tests.
*****************************************************************************/

#include "unity.h"
#include "unity_fixture.h"
#include "unity_internals.h"
#include "ma/internal/clients/ma/client/ma_client_handler.h"
#include "ma/logger/ma_console_logger.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_message.h"


#define LOG_FACILITY_NAME "EVENT_CLIENT"

TEST_GROUP_RUNNER(ma_core_client_handler_test_group)
{
    do {RUN_TEST_CASE(ma_core_client_handler_tests, client_handler_default_properties_test)} while (0);
    do {RUN_TEST_CASE(ma_core_client_handler_tests, client_handler_custom_properties_test)} while (0);
}

TEST_SETUP(ma_core_client_handler_tests)
{
}

TEST_TEAR_DOWN(ma_core_client_handler_tests)
{
}

void client_logger_callback(ma_log_severity_t severity, const char *module_name, const char *log_message, void *user_data)
{
    printf("Module Name - %s", module_name);
    printf("Log Message - %s", log_message);
}

ma_error_t passphrase_provider_callback(ma_msgbus_t *msgbus, void *cb_data, unsigned char *passphrase, size_t *passphrase_size)
{
    printf("Pass Phrase - %s", passphrase);
    printf("Pass Phrase Size - %d", *passphrase_size);

    return MA_OK;
}


/******************************************************************************
Client Handler test using the default message bus, thread option and other
properties (created by the Client Handler itself).
******************************************************************************/
TEST(ma_core_client_handler_tests, client_handler_default_properties_test)
{
    ma_client_handler_t *my_handler = NULL;

    // Creating Client Handler
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_client_handler_create(NULL, &my_handler));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_client_handler_create("MY_PRODUCT", NULL));
    TEST_ASSERT(MA_OK == ma_client_handler_create("MY_PRODUCT", &my_handler));

    // Starting Client Handler
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_client_handler_start(NULL));
    TEST_ASSERT(MA_OK == ma_client_handler_start(my_handler));

    // Retrieving the Consumer Reach from the Client Handler
    {
        ma_msgbus_consumer_reachability_t reach = MSGBUS_CONSUMER_REACH_INPROC;
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_client_handler_get_consumer_reachability(NULL, &reach));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_client_handler_get_consumer_reachability(my_handler, NULL));
        TEST_ASSERT(MA_OK == ma_client_handler_get_consumer_reachability(my_handler, &reach));
        TEST_ASSERT(MSGBUS_CONSUMER_REACH_OUTPROC == reach);
    }

    // Retrieving the Thread Option from the Client Handler
    {
        ma_msgbus_callback_thread_options_t t_option = MA_MSGBUS_CALLBACK_THREAD_IO;
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_client_handler_get_thread_option(NULL, &t_option));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_client_handler_get_thread_option(my_handler, NULL));
        TEST_ASSERT(MA_OK == ma_client_handler_get_thread_option(my_handler, &t_option));
        TEST_ASSERT(MA_MSGBUS_CALLBACK_THREAD_POOL == t_option);
    }

    // Stopping Client Handler
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_client_handler_stop(NULL));
    TEST_ASSERT(MA_OK == ma_client_handler_stop(my_handler));

    // Releasing Client Handler
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_client_handler_release(NULL));
    TEST_ASSERT(MA_OK == ma_client_handler_release(my_handler));
}

/******************************************************************************
Client Handler test using the message bus and logger created by the user (This
Test).
******************************************************************************/
TEST(ma_core_client_handler_tests, client_handler_custom_properties_test)
{
    ma_msgbus_t *g_msgbus = NULL;
    ma_msgbus_t *g_msgbus_1 = NULL;

    ma_client_handler_t *my_handler = NULL;
    ma_console_logger_t *console_logger = NULL;

    // Creating Console Logger
    TEST_ASSERT(MA_OK == ma_console_logger_create(&console_logger));

    // Creating and Starting the Message Bus
    TEST_ASSERT(MA_OK == ma_msgbus_create("CORE_CLIENT", &g_msgbus));
    TEST_ASSERT(MA_OK == ma_msgbus_start(g_msgbus));

    // Temporary for message-bus reinitialization verification
    TEST_ASSERT(MA_OK == ma_msgbus_create("CORE_CLIENT_1", &g_msgbus_1));
    TEST_ASSERT(MA_OK == ma_msgbus_start(g_msgbus_1));

    // Creating Client Handler
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_client_handler_create(NULL, &my_handler));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_client_handler_create("CORE_CLIENT", NULL));
    TEST_ASSERT(MA_OK == ma_client_handler_create("CORE_CLIENT", &my_handler));

    // Setting Logger for the Client Handler
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_client_handler_set_logger(NULL, (ma_logger_t *)console_logger));
    TEST_ASSERT(MA_OK == ma_client_handler_set_logger(my_handler, (ma_logger_t *)console_logger));

    // Setting Logger Callback for the Client Handler
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_client_handler_set_logger_callback(NULL, &client_logger_callback, NULL));
    TEST_ASSERT(MA_OK == ma_client_handler_set_logger_callback(my_handler, &client_logger_callback, NULL));

    // Setting Message Bus for the Client Handler
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_client_handler_set_msgbus(NULL, g_msgbus));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_client_handler_set_msgbus(my_handler, NULL));
    TEST_ASSERT(MA_OK == ma_client_handler_set_msgbus(my_handler, g_msgbus));

    // Trying to set another message bus when it is already set once
    TEST_ASSERT(MA_ERROR_INVALID_OPERATION == ma_client_handler_set_msgbus(my_handler, g_msgbus_1));

    // Setting Message Bus PassPhrase Callback for the Client Handler
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_client_handler_set_msgbus_passphrase_callback(NULL, &passphrase_provider_callback, NULL));
    TEST_ASSERT(MA_OK == ma_client_handler_set_msgbus_passphrase_callback(my_handler, &passphrase_provider_callback, NULL));

    // Setting Consumer Reach for the Client Handler
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_client_handler_set_consumer_reachability(NULL, MSGBUS_CONSUMER_REACH_INPROC));
    TEST_ASSERT(MA_OK == ma_client_handler_set_consumer_reachability(my_handler, MSGBUS_CONSUMER_REACH_INPROC));

    // Setting Thread Option for the Client Handler
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_client_handler_set_thread_option(NULL, MA_MSGBUS_CALLBACK_THREAD_IO));
    TEST_ASSERT(MA_OK == ma_client_handler_set_thread_option(my_handler, MA_MSGBUS_CALLBACK_THREAD_IO));

    // Starting Client Handler
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_client_handler_start(NULL));
    TEST_ASSERT(MA_OK == ma_client_handler_start(my_handler));

    // Retrieving the Client Context and verifying that it is not NULL
    {
        ma_context_t *my_context = NULL;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_client_handler_get_context(NULL, &my_context));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_client_handler_get_context(my_handler, NULL));
        TEST_ASSERT(MA_OK == ma_client_handler_get_context(my_handler, &my_context));
        TEST_ASSERT_NOT_NULL(my_context);
    }

    // Retrieving the Message Bus from the Client Handler. Verifying that the msgbus instance
    // retrieved from the Client Handler is same as message bus assigned to Client Handler
    {
        ma_msgbus_t *fetched_msgbus = NULL;
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_client_handler_get_msgbus(NULL, &fetched_msgbus));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_client_handler_get_msgbus(my_handler, NULL));
        TEST_ASSERT(MA_OK == ma_client_handler_get_msgbus(my_handler, &fetched_msgbus));
        TEST_ASSERT(fetched_msgbus == g_msgbus);
    }

    // Retrieving the Consumer Reach from the Client Handler
    {
        ma_msgbus_consumer_reachability_t reach = MSGBUS_CONSUMER_REACH_OUTPROC;
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_client_handler_get_consumer_reachability(NULL, &reach));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_client_handler_get_consumer_reachability(my_handler, NULL));
        TEST_ASSERT(MA_OK == ma_client_handler_get_consumer_reachability(my_handler, &reach));
        TEST_ASSERT(MSGBUS_CONSUMER_REACH_INPROC == reach);
    }

    // Retrieving the Thread Option from the Client Handler
    {
        ma_msgbus_callback_thread_options_t t_option = MA_MSGBUS_CALLBACK_THREAD_POOL;
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_client_handler_get_thread_option(NULL, &t_option));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_client_handler_get_thread_option(my_handler, NULL));
        TEST_ASSERT(MA_OK == ma_client_handler_get_thread_option(my_handler, &t_option));
        TEST_ASSERT(MA_MSGBUS_CALLBACK_THREAD_IO == t_option);
    }

    // Stopping Client Handler
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_client_handler_stop(NULL));
    TEST_ASSERT(MA_OK == ma_client_handler_stop(my_handler));
    //
    // Releasing Client Handler
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_client_handler_release(NULL));
    TEST_ASSERT(MA_OK == ma_client_handler_release(my_handler));

    // Stopping and Releasing Message Bus
    TEST_ASSERT(MA_OK == ma_msgbus_stop(g_msgbus, MA_TRUE));
    TEST_ASSERT(MA_OK == ma_msgbus_release(g_msgbus));

    TEST_ASSERT(MA_OK == ma_msgbus_stop(g_msgbus_1, MA_TRUE));
    TEST_ASSERT(MA_OK == ma_msgbus_release(g_msgbus_1));

    // Releasing the Logger
    TEST_ASSERT(MA_OK == ma_logger_release((ma_logger_t *)console_logger));
}



