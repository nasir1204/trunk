/*****************************************************************************
Property Bag Unit Tests.
******************************************************************************/
#include "unity.h"
#include "unity_fixture.h"
#include "ma/property/ma_property_bag.h"
#include <string.h>

TEST_GROUP_RUNNER(property_bag_tests_group)
{
    do {RUN_TEST_CASE(property_bag_tests, property_bag_test_create_release_test)} while (0);
    do {RUN_TEST_CASE(property_bag_tests, property_bag_test_data)} while (0);
}
TEST_SETUP(property_bag_tests)
{
}

TEST_TEAR_DOWN(property_bag_tests)
{
}

TEST(property_bag_tests, property_bag_test_create_release_test)
{
    ma_property_bag_t *bag = NULL;

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_property_bag_create(NULL));
    TEST_ASSERT(MA_OK == ma_property_bag_create(&bag));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_property_bag_release(NULL));
    TEST_ASSERT(MA_OK == ma_property_bag_release(bag));
}

TEST(property_bag_tests, property_bag_test_data)
{
    ma_property_bag_t *bag = NULL ;
    ma_variant_t *variant_ptr = NULL;
    ma_buffer_t *buffer = NULL;
    const char *string_val = NULL;
    size_t string_size = 0 ;

    TEST_ASSERT(MA_OK == ma_property_bag_create(&bag));

    // Adding properties
    TEST_ASSERT(MA_OK == ma_variant_create_from_string("Windows 7 Desktop", strlen("Windows 7 Desktop"), &variant_ptr));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_property_bag_add_property(NULL, "ComputerProperties1", "PlatformID", variant_ptr));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_property_bag_add_property(bag, NULL, "PlatformID", variant_ptr));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_property_bag_add_property(bag, "ComputerProperties1", NULL, variant_ptr));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_property_bag_add_property(bag, "ComputerProperties1", "PlatformID", NULL));
    TEST_ASSERT(MA_OK == ma_property_bag_add_property(bag, "ComputerProperties1", "PlatformID", variant_ptr));
    TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));
    variant_ptr = NULL;

    TEST_ASSERT(MA_OK == ma_variant_create_from_string("user.beaeng.mfeeng.org", strlen("user.beaeng.mfeeng.org"), &variant_ptr));
    TEST_ASSERT(MA_OK == ma_property_bag_add_property(bag, "ComputerProperties2" , "IPHostName", variant_ptr));
    TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));
    variant_ptr = NULL;

    TEST_ASSERT(MA_OK == ma_variant_create_from_string("X's Laptop", strlen("X's Laptop"), &variant_ptr));
    TEST_ASSERT(MA_OK == ma_property_bag_add_property(bag, "ComputerProperties1", "ComputerName", variant_ptr));
    TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));
    variant_ptr = NULL;

    // Retrieving properties in reverse order
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_property_bag_get_property(NULL, "ComputerProperties1", "ComputerName", &variant_ptr));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_property_bag_get_property(bag, NULL, "ComputerName", &variant_ptr));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_property_bag_get_property(bag, "ComputerProperties1", NULL, &variant_ptr));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_property_bag_get_property(bag, "ComputerProperties1", "ComputerName", NULL));
    TEST_ASSERT(MA_OK == ma_property_bag_get_property(bag, "ComputerProperties1", "ComputerName", &variant_ptr));
    TEST_ASSERT(MA_OK == ma_variant_get_string_buffer(variant_ptr, &buffer));
    TEST_ASSERT(MA_OK == ma_buffer_get_string(buffer, &string_val, &string_size));
    TEST_ASSERT(strlen("X's Laptop") == string_size);
    TEST_ASSERT(0 == strcmp("X's Laptop", string_val));
    TEST_ASSERT(MA_OK == ma_buffer_release(buffer));
    TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));
    variant_ptr = NULL;

    TEST_ASSERT(MA_OK == ma_property_bag_get_property(bag, "ComputerProperties2", "IPHostName", &variant_ptr));
    TEST_ASSERT(MA_OK == ma_variant_get_string_buffer(variant_ptr, &buffer));
    TEST_ASSERT(MA_OK == ma_buffer_get_string(buffer, &string_val, &string_size));
    TEST_ASSERT(strlen("user.beaeng.mfeeng.org") == string_size);
    TEST_ASSERT(0 ==strcmp("user.beaeng.mfeeng.org", string_val));
    TEST_ASSERT(MA_OK == ma_buffer_release(buffer));
    TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));
    variant_ptr = NULL;

    TEST_ASSERT(MA_OK == ma_property_bag_get_property(bag, "ComputerProperties1", "PlatformID", &variant_ptr));
    TEST_ASSERT(MA_OK == ma_variant_get_string_buffer(variant_ptr, &buffer));
    TEST_ASSERT(MA_OK == ma_buffer_get_string(buffer, &string_val, &string_size));
    TEST_ASSERT(strlen("Windows 7 Desktop") == string_size);
    TEST_ASSERT(0 == strcmp("Windows 7 Desktop", string_val));
    TEST_ASSERT(MA_OK == ma_buffer_release(buffer));
    TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));
    variant_ptr = NULL;

    // Retrieving from a non-existent path
    TEST_ASSERT(MA_ERROR_PROPERTY_BAG_PATH_NOT_FOUND == ma_property_bag_get_property(bag, "ComputerProperties3", "PlatformID", &variant_ptr));
    TEST_ASSERT_NULL(variant_ptr);

    // Retrieving a non-existent property from a valid path
    TEST_ASSERT(MA_ERROR_OBJECTNOTFOUND == ma_property_bag_get_property(bag, "ComputerProperties1", "IP Address", &variant_ptr));
    TEST_ASSERT_NULL(variant_ptr);

    TEST_ASSERT(MA_OK == ma_property_bag_release(bag));
}

