#include "ma/internal/clients/udp/ma_udp_client.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/utils/datastructures/ma_queue.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/network/ma_net_interface.h"
#include "ma/internal/utils/platform/ma_net_interface_list_internal.h"
#include "ma/internal/utils/event_loop/ma_uv_helpers.h"
#include "ma/internal/clients/udp/ma_udp_msg.h"
#include <assert.h>
#include "uv.h"

#include "unity.h"
#include "unity_fixture.h"


static ma_event_loop_t *ma_ev_loop = 0;
static ma_logger_t *logger = 0;
static ma_udp_client_t *client;

const int PORT					= 9009;
const int TIMEOUT				= 2 * 1000;
const char MA_MULTICAST_ADDR[]	= "ff08::2";

TEST_GROUP_RUNNER(default_test_group) {
    do {RUN_TEST_CASE(all_tests, end_to_end)} while (0);
}

TEST_SETUP(all_tests) {
    ma_event_loop_create(&ma_ev_loop);
}

TEST_TEAR_DOWN(all_tests) {
    ma_event_loop_release(ma_ev_loop);
}

ma_error_t cb(ma_udp_request_status_t status, ma_udp_msg_t *response, void *user_data, ma_udp_request_t *udp_request) {
	ma_udp_request_stop(udp_request);
	return MA_OK;
}

TEST(all_tests, end_to_end) {
	char msg[] = "test";
	ma_udp_request_t *request = NULL;

	ma_udp_client_create(&client);
	ma_udp_client_set_event_loop(client, ma_ev_loop);
	ma_udp_client_set_port(client, PORT);
	ma_udp_client_set_multicast_address(client, MA_MULTICAST_ADDR);
	ma_udp_client_start(client);

	ma_udp_client_async_send(client, (ma_udp_msg_t *)msg, TIMEOUT, cb, NULL, &request);

	ma_event_loop_run(ma_ev_loop);
	
	ma_udp_request_release(request);
	ma_udp_client_stop(client);
	ma_udp_client_release(client);
}