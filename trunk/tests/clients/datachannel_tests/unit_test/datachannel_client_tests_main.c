#include "unity.h"
#include "unity_fixture.h"

#include "ma/ma_common.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/services/ma_service.h"
#include "ma/ma_msgbus.h"
#include "ma/datachannel/ma_datachannel.h"
#include "ma/datachannel/ma_datachannel_item.h"
#include "ma/internal/services/datachannel/ma_datachannel_service.h"
#include "ma_ut_setup.h"
#include <uv.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


#ifdef MA_WINDOWS
# include <crtdbg.h>
#include <Windows.h>
# define MA_TRACKLEAKS _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF)
#else
# define MA_TRACKLEAKS ((void *)0)
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#endif

#define DB_UT    "datachannel_client_test.db"


static ma_service_t *g_datachannel_service = NULL;
static ma_client_t *g_ma_client;
static ma_ut_setup_t *g_ut_setup;


TEST_GROUP_RUNNER(datachannel_client_api_test_group) {

	do {RUN_TEST_CASE(datachannel_client_api_test, ma_datachannel_item_create_valid); }while(0);
	do {RUN_TEST_CASE(datachannel_client_api_test, ma_datachannel_item_create_valid1); }while(0);
	do {RUN_TEST_CASE(datachannel_client_api_test, ma_datachannel_item_create_valid2); }while(0);
	do {RUN_TEST_CASE(datachannel_client_api_test, ma_datachannel_item_create_invalid); }while(0);
	
	//do {RUN_TEST_CASE(datachannel_client_api_test, ma_datachannel_item_get_message_type_valid); }while(0);//Not added in .c file
	
	 do {RUN_TEST_CASE(datachannel_client_api_test, ma_datachannel_item_set_get_origin_valid); }while(0);
	 do {RUN_TEST_CASE(datachannel_client_api_test, ma_datachannel_item_set_get_origin_invalid); }while(0);
	
	do {RUN_TEST_CASE(datachannel_client_api_test, ma_datachannel_item_set_get_payload_valid); }while(0);
	do {RUN_TEST_CASE(datachannel_client_api_test, ma_datachannel_item_set_get_payload_invalid); }while(0);

	do {RUN_TEST_CASE(datachannel_client_api_test, ma_datachannel_item_set_get_ttl_valid); }while(0);
	do {RUN_TEST_CASE(datachannel_client_api_test, ma_datachannel_item_set_get_ttl_invalid); }while(0);

	do {RUN_TEST_CASE(datachannel_client_api_test, ma_datachannel_item_set_get_correlation_id_valid); }while(0);
	do {RUN_TEST_CASE(datachannel_client_api_test, ma_datachannel_item_set_get_correlation_id_invalid); }while(0);

	do {RUN_TEST_CASE(datachannel_client_api_test, ma_datachannel_item_set_get_option_valid); }while(0);
	do {RUN_TEST_CASE(datachannel_client_api_test, ma_datachannel_item_set_get_option_valid1); }while(0);
	do {RUN_TEST_CASE(datachannel_client_api_test, ma_datachannel_item_set_get_option_invalid); }while(0);

	
	do {RUN_TEST_CASE(datachannel_client_api_test, start_client); }while(0); 
	do {RUN_TEST_CASE(datachannel_client_api_test, ma_datachannel_register_notification_callback_valid); }while(0);
	do {RUN_TEST_CASE(datachannel_client_api_test, ma_datachannel_register_notification_callback_invalid); }while(0);

	do {RUN_TEST_CASE(datachannel_client_api_test, ma_datachannel_register_message_handler_callback_valid); }while(0);
	do {RUN_TEST_CASE(datachannel_client_api_test, ma_datachannel_register_message_handler_callback_invalid); }while(0);
	do {RUN_TEST_CASE(datachannel_client_api_test, stop_client); }while(0); 

}

TEST_SETUP(datachannel_client_api_test) {
    
}

TEST_TEAR_DOWN(datachannel_client_api_test) {
    
}

TEST(datachannel_client_api_test,ma_datachannel_item_create_valid){
	char *message_type = "DC-MSG";
	ma_variant_t *payload = NULL;
	ma_datachannel_item_t *dc_message = NULL;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_string("DC-Test-MSG", strlen("DC-Test-MSG"), &payload));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_create(message_type,payload,&dc_message));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_release(dc_message));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(payload));

}

TEST(datachannel_client_api_test,ma_datachannel_item_create_valid1){
	char *message_type = "DC-MSG";
	wchar_t *msg = L"Data-Channel-Test-MSG";
	ma_variant_t *payload = NULL;
	ma_datachannel_item_t *dc_message = NULL;
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_wstring(msg, wcslen(msg), &payload));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_create(message_type,payload,&dc_message));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_release(dc_message));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(payload));

}

TEST(datachannel_client_api_test,ma_datachannel_item_create_valid2){
	char *message_type = "DC-MSG";
	//unsigned char *msg = NULL;
	void *msg1 = NULL;
	ma_variant_t *payload = NULL;
	ma_datachannel_item_t *dc_message = NULL;
	int i =0;

	//msg = (unsigned char *)calloc(1,100);
	msg1 = calloc(1,100);
	/*for(;i<100;i++){
		*(msg+i) = 'a';
		if(i == 5 || i == 10)
			*(msg+i) = '\0';
	}*/

	for(i =0;i<100;i++){
		*((char*)msg1+i) = 'a';
		if(i == 5 || i == 10)
			*((char*)msg1+i) = '\0';
	}
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_raw(msg1,100,&payload));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_create(message_type,payload,&dc_message));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_release(dc_message));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(payload));
	free(msg1);

}

TEST(datachannel_client_api_test,ma_datachannel_item_create_invalid){
	char *message_type = "DC-MSG";
	ma_variant_t *payload = NULL;
	ma_datachannel_item_t *dc_message = NULL;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_string("DC-Test-MSG", strlen("DC-Test-MSG"), &payload));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_datachannel_item_create(NULL,payload,&dc_message));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_datachannel_item_create(message_type,NULL,&dc_message));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_datachannel_item_create(message_type,payload,NULL));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_datachannel_item_create(NULL,NULL,NULL));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_datachannel_item_release(dc_message));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(payload));

}

//TEST(datachannel_client_api_test,ma_datachannel_item_get_message_type_valid){
//	char *message_type = "DC-MSG";
//	ma_variant_t *payload = NULL;
//	ma_buffer_t *buff = NULL;
//	ma_datachannel_item_t *dc_message = NULL;
//
//	TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_string("DC-Test-MSG", strlen("DC-Test-MSG"), &payload));
//	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_create(message_type,payload,&dc_message));
//	ma_datachannel_item_get_message_type(dc_message,&buff);
//	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_release(dc_message));
//	TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(payload));
//
//}

TEST(datachannel_client_api_test,ma_datachannel_item_set_get_origin_valid){
	char *message_type = "DC-MSG";
	char *return_msg = NULL;
	size_t len =0;
	ma_variant_t *payload = NULL;
	ma_buffer_t *buff = NULL;
	ma_datachannel_item_t *dc_message = NULL;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_string("DC-Test-MSG", strlen("DC-Test-MSG"), &payload));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_create(message_type,payload,&dc_message));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_set_origin(dc_message,"DC-Testing"));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_get_origin(dc_message,&buff));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_buffer_get_string(buff,&return_msg,&len));
	TEST_ASSERT_EQUAL_INT(MA_OK,strcmp("DC-Testing",return_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_release(dc_message));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_buffer_release(buff));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(payload));

}

TEST(datachannel_client_api_test,ma_datachannel_item_set_get_origin_invalid){
	char *message_type = "DC-MSG";
	char *return_msg = NULL;
	size_t len =0;
	ma_variant_t *payload = NULL;
	ma_buffer_t *buff = NULL;
	ma_datachannel_item_t *dc_message = NULL;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_string("DC-Test-MSG", strlen("DC-Test-MSG"), &payload));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_create(message_type,payload,&dc_message));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_datachannel_item_set_origin(NULL,"DC-Testing"));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_datachannel_item_set_origin(dc_message,NULL));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_get_origin(dc_message,&buff));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_datachannel_item_get_origin(NULL,&buff));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_datachannel_item_get_origin(dc_message,NULL));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_buffer_get_string(buff,&return_msg,&len));
	TEST_ASSERT_EQUAL_INT(MA_OK,strcmp("",return_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_release(dc_message));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_buffer_release(buff));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(payload));

}
TEST(datachannel_client_api_test,ma_datachannel_item_set_get_payload_valid){
	char *message_type = "DC-MSG";
	char *return_msg = NULL;
	size_t len =0;
	ma_variant_t *payload = NULL;
	ma_variant_t *payload_r = NULL;
	ma_buffer_t *buff = NULL;
	ma_datachannel_item_t *dc_message = NULL;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_string("DC-Test-MSG", strlen("DC-Test-MSG"), &payload));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_create(message_type,payload,&dc_message));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_get_payload(dc_message,&payload_r));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_variant_get_string_buffer(payload_r,&buff));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_buffer_get_string(buff,&return_msg,&len));
	TEST_ASSERT_EQUAL_INT(MA_OK,strcmp("DC-Test-MSG",return_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_buffer_release(buff));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(payload_r));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(payload));

	payload_r = NULL;
	payload = NULL;
	buff = NULL;
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_string("Next Pay Load", strlen("Next Pay Load"), &payload));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_set_payload(dc_message,payload));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_get_payload(dc_message,&payload_r));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_variant_get_string_buffer(payload_r,&buff));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_buffer_get_string(buff,&return_msg,&len));
	TEST_ASSERT_EQUAL_INT(MA_OK,strcmp("Next Pay Load",return_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_buffer_release(buff));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(payload_r));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(payload));
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_release(dc_message));
	
}

TEST(datachannel_client_api_test,ma_datachannel_item_set_get_payload_invalid){
	char *message_type = "DC-MSG";
	ma_variant_t *payload = NULL;
	ma_variant_t *payload_r = NULL;
	ma_datachannel_item_t *dc_message = NULL;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_string("DC-Test-MSG", strlen("DC-Test-MSG"), &payload));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_create(message_type,payload,&dc_message));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_datachannel_item_get_payload(dc_message,NULL));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_datachannel_item_get_payload(NULL,&payload_r));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(payload));

	
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_datachannel_item_set_payload(dc_message,NULL));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_datachannel_item_set_payload(NULL,payload));
	
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_release(dc_message));

}

TEST(datachannel_client_api_test,ma_datachannel_item_set_get_ttl_valid){
	char *message_type = "DC-MSG";
	ma_int64_t test_ttl = 100;
	ma_int64_t test_ttl_r = 0;
	ma_variant_t *payload = NULL;
	ma_datachannel_item_t *dc_message = NULL;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_string("DC-Test-MSG", strlen("DC-Test-MSG"), &payload));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_create(message_type,payload,&dc_message));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_set_ttl(dc_message,test_ttl));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_get_ttl(dc_message,&test_ttl_r));
	TEST_ASSERT_EQUAL_INT(test_ttl,test_ttl_r);
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_release(dc_message));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(payload));

}

TEST(datachannel_client_api_test,ma_datachannel_item_set_get_ttl_invalid){
	char *message_type = "DC-MSG";
	ma_int64_t test_ttl = 100;
	ma_int64_t test_ttl_r = 0;
	ma_variant_t *payload = NULL;
	ma_datachannel_item_t *dc_message = NULL;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_string("DC-Test-MSG", strlen("DC-Test-MSG"), &payload));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_create(message_type,payload,&dc_message));

	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_datachannel_item_set_ttl(NULL,test_ttl));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_datachannel_item_get_ttl(dc_message,NULL));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_datachannel_item_get_ttl(NULL,&test_ttl_r));
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_release(dc_message));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(payload));

}


TEST(datachannel_client_api_test,ma_datachannel_item_set_get_correlation_id_valid){
	char *message_type = "DC-MSG";
	ma_int64_t correlation_id = 100;
	ma_int64_t correlation_id_r = 0;
	ma_variant_t *payload = NULL;
	ma_datachannel_item_t *dc_message = NULL;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_string("DC-Test-MSG", strlen("DC-Test-MSG"), &payload));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_create(message_type,payload,&dc_message));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_set_correlation_id(dc_message,correlation_id));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_get_correlation_id(dc_message,&correlation_id_r));
	TEST_ASSERT_EQUAL_INT(correlation_id,correlation_id_r);
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_release(dc_message));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(payload));

}

TEST(datachannel_client_api_test,ma_datachannel_item_set_get_correlation_id_invalid){
	char *message_type = "DC-MSG";
	ma_int64_t correlation_id = 100;
	ma_int64_t correlation_id_r = 0;
	ma_variant_t *payload = NULL;
	ma_datachannel_item_t *dc_message = NULL;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_string("DC-Test-MSG", strlen("DC-Test-MSG"), &payload));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_create(message_type,payload,&dc_message));

	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_datachannel_item_set_correlation_id(NULL,correlation_id));

	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_datachannel_item_get_correlation_id(dc_message,NULL));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_datachannel_item_get_correlation_id(NULL,&correlation_id_r));
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_release(dc_message));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(payload));

}

TEST(datachannel_client_api_test,ma_datachannel_item_set_get_option_valid){
	char *message_type = "DC-MSG";
	ma_variant_t *payload = NULL;
	ma_datachannel_item_t *dc_message = NULL;
	ma_uint32_t option = 0;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_string("DC-Test-MSG", strlen("DC-Test-MSG"), &payload));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_create(message_type,payload,&dc_message));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_set_option(dc_message,MA_DATACHANNEL_PERSIST,MA_TRUE));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_set_option(dc_message,MA_DATACHANNEL_ENCRYPT,MA_TRUE));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_set_option(dc_message,MA_DATACHANNEL_DELIVERY_NOTIFICATION,MA_TRUE));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_set_option(dc_message,MA_DATACHANNEL_PURGED_NOTIFICATION,MA_TRUE));

	option = 0;
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_get_option(dc_message,&option));
	TEST_ASSERT_EQUAL_INT(MA_DATACHANNEL_PERSIST | 
		MA_DATACHANNEL_ENCRYPT | 
		MA_DATACHANNEL_DELIVERY_NOTIFICATION | 
		MA_DATACHANNEL_PURGED_NOTIFICATION,option);

	option = 0;
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_set_option(dc_message,MA_DATACHANNEL_PERSIST,MA_FALSE));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_get_option(dc_message,&option));
	TEST_ASSERT_EQUAL_INT( MA_DATACHANNEL_ENCRYPT | 
		MA_DATACHANNEL_DELIVERY_NOTIFICATION | 
		MA_DATACHANNEL_PURGED_NOTIFICATION,option);

	option = 0;
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_set_option(dc_message,MA_DATACHANNEL_ENCRYPT,MA_FALSE));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_get_option(dc_message,&option));
	TEST_ASSERT_EQUAL_INT(MA_DATACHANNEL_DELIVERY_NOTIFICATION | MA_DATACHANNEL_PURGED_NOTIFICATION,option);
	
	option = 0;
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_set_option(dc_message,MA_DATACHANNEL_DELIVERY_NOTIFICATION,MA_FALSE));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_get_option(dc_message,&option));
	TEST_ASSERT_EQUAL_INT(MA_DATACHANNEL_PURGED_NOTIFICATION,option);
	
	option = 0;
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_set_option(dc_message,MA_DATACHANNEL_PURGED_NOTIFICATION,MA_FALSE));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_get_option(dc_message,&option));
	TEST_ASSERT_EQUAL_INT(MA_DATACHANNEL_DEFAULT ,option);


	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_release(dc_message));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(payload));

}

TEST(datachannel_client_api_test,ma_datachannel_item_set_get_option_valid1){
	char *message_type = "DC-MSG";
	ma_variant_t *payload = NULL;
	ma_uint32_t option = 0;
	ma_datachannel_item_t *dc_message = NULL;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_string("DC-Test-MSG", strlen("DC-Test-MSG"), &payload));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_create(message_type,payload,&dc_message));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_set_option(dc_message,MA_DATACHANNEL_PERSIST|MA_DATACHANNEL_PURGED_NOTIFICATION,MA_TRUE));
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_get_option(dc_message,&option));
	TEST_ASSERT_EQUAL_INT(MA_DATACHANNEL_PURGED_NOTIFICATION | MA_DATACHANNEL_PERSIST,option);

	option = 0;
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_set_option(dc_message,MA_DATACHANNEL_PERSIST,MA_TRUE));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_get_option(dc_message,&option));
	TEST_ASSERT_EQUAL_INT(MA_DATACHANNEL_PURGED_NOTIFICATION | MA_DATACHANNEL_PERSIST,option);


	option = 0;
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_set_option(dc_message,MA_DATACHANNEL_DEFAULT,MA_TRUE));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_get_option(dc_message,&option));
	TEST_ASSERT_EQUAL_INT(MA_DATACHANNEL_DEFAULT,option);

	option = 0;
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_set_option(dc_message,MA_DATACHANNEL_PURGED_NOTIFICATION,MA_FALSE));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_get_option(dc_message,&option));
	TEST_ASSERT_EQUAL_INT(MA_DATACHANNEL_DEFAULT,option);

	option = 0;
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_set_option(dc_message,MA_DATACHANNEL_ENCRYPT|MA_DATACHANNEL_PURGED_NOTIFICATION
		|MA_DATACHANNEL_PERSIST|MA_DATACHANNEL_DELIVERY_NOTIFICATION,MA_TRUE));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_get_option(dc_message,&option));
	TEST_ASSERT_EQUAL_INT(MA_DATACHANNEL_ENCRYPT|MA_DATACHANNEL_PURGED_NOTIFICATION
		|MA_DATACHANNEL_PERSIST|MA_DATACHANNEL_DELIVERY_NOTIFICATION,option);

	option = 0;
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_set_option(dc_message,MA_DATACHANNEL_ENCRYPT|MA_DATACHANNEL_PURGED_NOTIFICATION
		|MA_DATACHANNEL_PERSIST|MA_DATACHANNEL_DELIVERY_NOTIFICATION,MA_FALSE));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_get_option(dc_message,&option));
	TEST_ASSERT_EQUAL_INT(MA_DATACHANNEL_DEFAULT,option);

	option = 0;
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_set_option(dc_message,MA_DATACHANNEL_ENCRYPT|MA_DATACHANNEL_PURGED_NOTIFICATION
		|MA_DATACHANNEL_PERSIST|MA_DATACHANNEL_DELIVERY_NOTIFICATION,MA_TRUE));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_set_option(dc_message,MA_DATACHANNEL_DEFAULT,MA_TRUE));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_get_option(dc_message,&option));
	TEST_ASSERT_EQUAL_INT(MA_DATACHANNEL_DEFAULT,option);

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_release(dc_message));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(payload));

}

TEST(datachannel_client_api_test,ma_datachannel_item_set_get_option_invalid){
	char *message_type = "DC-MSG";
	ma_uint32_t check_r = MA_FALSE;
	ma_variant_t *payload = NULL;
	ma_datachannel_item_t *dc_message = NULL;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_string("DC-Test-MSG", strlen("DC-Test-MSG"), &payload));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_create(message_type,payload,&dc_message));

	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_datachannel_item_set_option(NULL,MA_DATACHANNEL_PERSIST,MA_TRUE));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALID_OPTION, ma_datachannel_item_set_option(dc_message,MA_DATACHANNEL_DEFAULT,MA_FALSE));
	
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_datachannel_item_get_option(dc_message,NULL));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_datachannel_item_get_option(NULL,&check_r));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_release(dc_message));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(payload));
	
}

TEST(datachannel_client_api_test, start_client) {
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_client_create("DC-Client",&g_ma_client));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_client_start(g_ma_client));
	   
}


void dc_notification_cb(ma_client_t *client, const char *product_id, const char *message_id, ma_int64_t correlation_id, ma_datachannel_item_notification_t status,ma_error_t reason, void *cb_data) {
	    
    printf("datachannel on notification callback invoked product id = %s, message id=%s, correlation id=%d, notification=%d\n",product_id, message_id, correlation_id, status);
}

void dc_onmessage_cb(ma_client_t *client, const char *product_id, const char *message_id, ma_int64_t correlation_id,  ma_datachannel_item_t *dc_message, void *cb_data) {
	    
    printf("datachannel on notification callback invoked product id = %s, message id=%s, correlation id=%d, \n",product_id, message_id, correlation_id);
}


TEST(datachannel_client_api_test,ma_datachannel_register_notification_callback_valid){
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_datachannel_register_notification_callback(g_ma_client,"DC-TEST",dc_notification_cb,NULL));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_datachannel_unregister_notification_callback(g_ma_client,"DC-TEST"));
}


TEST(datachannel_client_api_test,ma_datachannel_register_notification_callback_invalid){
	ma_client_t *ma_client;

	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_datachannel_register_notification_callback(NULL,"DC-TEST",dc_notification_cb,NULL));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_datachannel_register_notification_callback(g_ma_client,NULL,dc_notification_cb,NULL));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_datachannel_register_notification_callback(g_ma_client,"DC-TEST",NULL,NULL));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_datachannel_unregister_notification_callback(NULL,"DC-TEST"));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_datachannel_unregister_notification_callback(g_ma_client,NULL));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_CLIENT_NOT_FOUND,ma_datachannel_unregister_notification_callback(g_ma_client,"DC-TEST"));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_datachannel_register_notification_callback(g_ma_client,"DC-TEST",dc_notification_cb,NULL));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_ALREADY_REGISTERED,ma_datachannel_register_notification_callback(g_ma_client,"DC-TEST",dc_notification_cb,NULL));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_datachannel_unregister_notification_callback(g_ma_client,"DC-TEST"));

	ma_client_create("DC-Client",&ma_client);
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALID_CLIENT_OBJECT,ma_datachannel_register_notification_callback(ma_client,"DC-TEST",dc_notification_cb,NULL));
	ma_client_release(ma_client);
}


TEST(datachannel_client_api_test,ma_datachannel_register_message_handler_callback_valid){
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_datachannel_register_message_handler_callback(g_ma_client,"DC-TEST",dc_onmessage_cb,NULL));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_datachannel_unregister_message_handler_callback(g_ma_client,"DC-TEST"));
}


TEST(datachannel_client_api_test,ma_datachannel_register_message_handler_callback_invalid){
	ma_client_t *ma_client;

	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_datachannel_register_message_handler_callback(NULL,"DC-TEST",dc_onmessage_cb,NULL));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_datachannel_register_message_handler_callback(g_ma_client,NULL,dc_onmessage_cb,NULL));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_datachannel_register_message_handler_callback(g_ma_client,"DC-TEST",NULL,NULL));
	
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_datachannel_unregister_message_handler_callback(NULL,"DC-TEST"));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_datachannel_unregister_message_handler_callback(g_ma_client,NULL));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_CLIENT_NOT_FOUND,ma_datachannel_unregister_message_handler_callback(g_ma_client,"DC-TEST"));

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_datachannel_register_message_handler_callback(g_ma_client,"DC-TEST",dc_onmessage_cb,NULL));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_ALREADY_REGISTERED,ma_datachannel_register_message_handler_callback(g_ma_client,"DC-TEST",dc_onmessage_cb,NULL));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_datachannel_unregister_message_handler_callback(g_ma_client,"DC-TEST"));

	ma_client_create("DC-Client",&ma_client);
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALID_CLIENT_OBJECT,ma_datachannel_register_message_handler_callback(ma_client,"DC-TEST",dc_onmessage_cb,NULL));
	//ma_client_release(ma_client);
}

TEST(datachannel_client_api_test, stop_client) {
   	TEST_ASSERT_EQUAL_INT(MA_OK,ma_client_stop(g_ma_client));
	Sleep(1*1000);
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_client_release(g_ma_client));
}



static void run_all_unit_tests(void) {
	do {RUN_TEST_GROUP(datachannel_client_api_test_group); } while (0);
	//do {RUN_TEST_GROUP(datachannel_client_functional_test_group); } while (0);
	
}

static void run_all_function_tests(void) {
    //do {RUN_TEST_GROUP(datachannel_client_daily_test_group);} while (0);
	//do {RUN_TEST_GROUP(datachannel_client_test_group);} while (0);
	//do {RUN_TEST_GROUP(datachannel_client_run_once_test_group);} while (0);
}


int main(int argc, char *argv[]) {
    int i =	0;
    int group = 0;
#ifdef MA_WINDOWS
    MA_TRACKLEAKS;
#endif

    if(argc == 1)
        return UnityMain(argc, argv, run_all_unit_tests);
    else {
        for(i =0;i<argc;i++)
            if(!strcmp("-g",argv[i])){
                group = 1;
                break;
            }
            if(group)
                return UnityMain(argc, argv, run_all_function_tests);
            else
                return UnityMain(argc, argv, run_all_unit_tests);
    }
}
