#include "unity.h"
#include "unity_fixture.h"

#include "ma/ma_common.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/services/ma_service.h"
#include "ma/ma_msgbus.h"
#include "ma/datachannel/ma_datachannel.h"
#include "ma/datachannel/ma_datachannel_item.h"
#include "ma/internal/services/datachannel/ma_datachannel_service.h"
#include "ma/internal/defs/ma_datachannel_defs.h"
#include "ma_ut_setup.h"
#include <uv.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


#ifdef MA_WINDOWS
# include <crtdbg.h>
#include <Windows.h>
# define MA_TRACKLEAKS _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF)
#else
# define MA_TRACKLEAKS ((void *)0)
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#endif

#define DB_UT    "datachannel_client_test.db"
// Facility Name.
// To be used for Logging and as Product ID.
#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "MESSAGEBUS_SERVER"
#define BC_DC_ITEM1										"DcTestEcho"

static int endpoint_number = 101;
static const char *server_name = "epo.service.datachannel";
static ma_msgbus_t *msgbus_obj = NULL; // Message Bus
static ma_msgbus_server_t *p_server = NULL; // Message Bus Server

static ma_service_t *g_datachannel_service = NULL;
static ma_client_t *g_ma_client;
static ma_ut_setup_t *g_ut_setup;
static const char *client_product_id = "datachannel.client.test";
static int check;
TEST_GROUP_RUNNER(datachannel_client_functional_test_group) {
	
	do {RUN_TEST_CASE(datachannel_client_functional_test, start_client); }while(0);
	do {RUN_TEST_CASE(datachannel_client_functional_test, start_service); }while(0); // it should always be executed first to ensure datachannel service is running before client requests
		
	do {RUN_TEST_CASE(datachannel_client_functional_test, dc_message_test_valid1); }while(0);
	do {RUN_TEST_CASE(datachannel_client_functional_test, dc_message_test_valid2); }while(0);
	do {RUN_TEST_CASE(datachannel_client_functional_test, dc_message_test_valid3); }while(0);
	do {RUN_TEST_CASE(datachannel_client_functional_test, dc_message_test_valid4); }while(0);

	do {RUN_TEST_CASE(datachannel_client_functional_test, stop_service); }while(0); // don't forget to stop and release the service
	do {RUN_TEST_CASE(datachannel_client_functional_test, stop_client); }while(0);
}

TEST_SETUP(datachannel_client_functional_test) {
    
}

TEST_TEAR_DOWN(datachannel_client_functional_test) {
    
}

ma_error_t respond_to_client_requests(ma_msgbus_server_t *server, ma_message_t *request_payload, void *cb_data, ma_msgbus_client_request_t *c_request)
{
    ma_message_t* response_payload = NULL;
	ma_variant_t *variant_ptr = NULL;
	ma_buffer_t *buffer = NULL;
	char *my_string = NULL;
	char *my_string_r = "Reply";
    size_t size;
	const char *client_number = NULL;

    ma_message_get_property(request_payload, EPO_DATACHANNEL_ITEM_PROP_ORIGIN_STR, &client_number);
    ma_message_get_payload(request_payload, &variant_ptr);
    ma_variant_get_string_buffer(variant_ptr, &buffer);
    ma_buffer_get_string(buffer, &my_string, &size);
    ma_buffer_release(buffer);
	ma_variant_release(variant_ptr);
    printf("DC Message Recived - %s\n\n", my_string);
	
    // Sending the response to client for incoming message
	if(my_string){
		ma_message_create(&response_payload);
		ma_variant_create_from_string(my_string_r, strlen(my_string_r), &variant_ptr);
		ma_message_set_property(response_payload,EPO_DATACHANNEL_ITEM_PROP_MESSAGE_TYPE_STR,"DC-Item1");
		ma_message_set_property(response_payload,EPO_DATACHANNEL_ITEM_PROP_CORRELATION_ID_STR,"100");
		ma_message_set_property(response_payload,EPO_DATACHANNEL_ITEM_PROP_ORIGIN_STR,client_product_id);
		ma_message_set_property(response_payload,EPO_DATACHANNEL_ITEM_PROP_STATUS_STR,"0");
		ma_message_set_payload(response_payload, variant_ptr);
		//ma_msgbus_server_client_request_post_result(c_request, MA_OK, response_payload);
		ma_variant_release(variant_ptr);
		printf("Reply is sent\n");
		ma_msgbus_publish(msgbus_obj, EPO_DATACHANNEL_NOTIFICATION_PUBLISHER_TOPIC_STR, MSGBUS_CONSUMER_REACH_OUTPROC, response_payload);
	}
    return MA_OK;
}

TEST(datachannel_client_functional_test, start_client) {

	ma_client_create("dc-client",&g_ma_client);
	ma_client_start(g_ma_client);
}

/*start Service*/
TEST(datachannel_client_functional_test, start_service) {
ma_client_get_msgbus(g_ma_client,&msgbus_obj);
TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_server_create(msgbus_obj, "epo.service.datachannel", &p_server));
TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_server_setopt(p_server, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC));
TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_server_setopt(p_server, MSGBUSOPT_THREADMODEL, MA_MSGBUS_CALLBACK_THREAD_IO));
TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_server_setopt(p_server, MSGBUSOPT_TIMEOUT, 100000));
TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_server_start(p_server, respond_to_client_requests, (void *)&endpoint_number));
	
}

//////////////////////////Test1///////////////////////////////////

static void dc_notification_cb(ma_client_t *client, const char *product_id, const char *message_id, ma_int64_t correlation_id, ma_datachannel_item_notification_t status, void *cb_data) {
	    
    printf("###datachannel on notification callback invoked for product id = %s###\n",product_id);
	printf("message id=%s, correlation id=%d, notification=%d\n", message_id, correlation_id, status);
	TEST_ASSERT_EQUAL_INT(100,correlation_id);
	if(correlation_id == 100)
		check = 0;
	
}


TEST(datachannel_client_functional_test,dc_message_test_valid1){
	char *message_type = BC_DC_ITEM1;
	ma_bool_t check_r = MA_FALSE;
	ma_variant_t *payload = NULL;
	ma_datachannel_item_t *dc_message = NULL;
	ma_int64_t test_ttl = 10;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_string("DC-Test-MSG", strlen("DC-Test-MSG"), &payload));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_create(message_type,payload,&dc_message));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_set_option(dc_message,MA_DATACHANNEL_DELIVERY_NOTIFICATION,MA_TRUE));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_set_option(dc_message,MA_DATACHANNEL_PERSIST,MA_TRUE));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_set_ttl(dc_message,test_ttl));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_datachannel_item_set_origin(dc_message,client_product_id));

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_datachannel_register_notification_callback(g_ma_client,client_product_id,dc_notification_cb,NULL));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_datachannel_async_send(g_ma_client,client_product_id,dc_message));
	check = 1;
	while(check){
	};
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_datachannel_unregister_notification_callback(g_ma_client,client_product_id));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_datachannel_item_release(dc_message));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_variant_release(payload));
}

//////////////////////////////////////////////////////////////////////////////

///////////////////////Test2//////////////////////////////////////////////////

static void dc_onmessage_cb2(ma_client_t *client, const char *product_id, const char *message_id, ma_int64_t correlation_id,  ma_datachannel_item_t *dc_message, void *cb_data) {
	
	ma_variant_t *variant_ptr = NULL;
	ma_buffer_t *buffer = NULL;
	char *my_string = NULL;
	size_t size;

	printf("###datachannel on message callback invoked for product id = %s###\n",product_id);
	printf("message id=%s, correlation id=%d, message_id=%d\n", message_id, correlation_id, message_id);

	//ma_message_get_payload(dc_message, &variant_ptr);
	ma_datachannel_item_get_payload(dc_message,&variant_ptr);
    ma_variant_get_string_buffer(variant_ptr, &buffer);
    ma_buffer_get_string(buffer, &my_string, &size);
    ma_buffer_release(buffer);
	ma_variant_release(variant_ptr);
    printf("Datachanel Message Recived - %s\n\n", my_string);
	if(correlation_id == 200)
		check = 0;
}

void publish_message(ma_message_t* msg){
	
	ma_variant_t *variant_ptr = NULL;
	ma_message_create(&msg);
	ma_variant_create_from_string("DC-Reply-Message", strlen("DC-Reply-Message"), &variant_ptr);
	ma_message_set_property(msg,EPO_DATACHANNEL_ITEM_PROP_MESSAGE_TYPE_STR,"DC-Item1");
	ma_message_set_property(msg,EPO_DATACHANNEL_ITEM_PROP_CORRELATION_ID_STR,"200");
	ma_message_set_property(msg,EPO_DATACHANNEL_ITEM_PROP_ORIGIN_STR,client_product_id);
	ma_message_set_property(msg,EPO_DATACHANNEL_ITEM_PROP_STATUS_STR,"0");
	ma_message_set_property(msg,EPO_DATACHANNEL_ITEM_PROP_FLAGS_STR,"11111");
	ma_message_set_payload(msg, variant_ptr);
	ma_variant_release(variant_ptr);
	printf("Reply is sent\n");
	ma_msgbus_publish(msgbus_obj, "Echo-message", MSGBUS_CONSUMER_REACH_OUTPROC, msg);
}


TEST(datachannel_client_functional_test,dc_message_test_valid2){
	
	static const char *product_id = "DC_PP1";
	ma_message_t* response_payload = NULL;

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_datachannel_register_message_handler_callback(g_ma_client,product_id,dc_onmessage_cb2,NULL));
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_datachannel_subscribe(g_ma_client,product_id,"Echo-message",MA_DC_SUBSCRIBER_LISTENER));
	publish_message(response_payload);
	check = 1;
	while(check){
	};
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_datachannel_unsubscribe(g_ma_client,product_id,"Echo-message"));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_datachannel_unregister_message_handler_callback(g_ma_client,product_id));
		
}

/////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////Test3//////////////////////////////////////////
static void dc_onmessage_cb3(ma_client_t *client, const char *product_id, const char *message_id, ma_int64_t correlation_id,  ma_datachannel_item_t *dc_message, void *cb_data) {
	
	ma_variant_t *variant_ptr = NULL;
	ma_buffer_t *buffer = NULL;
	char *my_string = NULL;
	size_t size;

	printf("###datachannel on message callback invoked for product id = %s###\n",product_id);
	printf("message id=%s, correlation id=%d, message_id=%d\n", message_id, correlation_id, message_id);

	ma_datachannel_item_get_payload(dc_message,&variant_ptr);
    ma_variant_get_string_buffer(variant_ptr, &buffer);
    ma_buffer_get_string(buffer, &my_string, &size);
    ma_buffer_release(buffer);
	ma_variant_release(variant_ptr);
    printf("Datachanel Message Recived - %s\n\n", my_string);
	//TEST_ASSERT_EQUAL_INT(100,correlation_id);
	//if(correlation_id == 100)
		check++;
}
static void dc_onmessage_cb3_1(ma_client_t *client, const char *product_id, const char *message_id, ma_int64_t correlation_id,  ma_datachannel_item_t *dc_message, void *cb_data) {
	
	ma_variant_t *variant_ptr = NULL;
	ma_buffer_t *buffer = NULL;
	char *my_string = NULL;
	size_t size;

	printf("###datachannel on message callback invoked for product id = %s###\n",product_id);
	printf("message id=%s, correlation id=%d, message_id=%d\n", message_id, correlation_id, message_id);

	//ma_message_get_payload(dc_message, &variant_ptr);
	ma_datachannel_item_get_payload(dc_message,&variant_ptr);
    ma_variant_get_string_buffer(variant_ptr, &buffer);
    ma_buffer_get_string(buffer, &my_string, &size);
    ma_buffer_release(buffer);
	ma_variant_release(variant_ptr);
    printf("Datachanel Message Recived - %s\n\n", my_string);
	//TEST_ASSERT_EQUAL_INT(200,correlation_id);
	//if(correlation_id == 200)
		check++;;
}

void publish_dcmessage(ma_message_t* msg,const char *pr_id,char *cr_id){
	
	ma_variant_t *variant_ptr = NULL;

	//msgbus_obj = ma_ut_setup_get_msgbus(g_ut_setup);
	ma_message_create(&msg);
	ma_variant_create_from_string("DC-Reply-Message", strlen("DC-Reply-Message"), &variant_ptr);
	ma_message_set_property(msg,EPO_DATACHANNEL_ITEM_PROP_MESSAGE_TYPE_STR,"DC-Item1");
	ma_message_set_property(msg,EPO_DATACHANNEL_ITEM_PROP_CORRELATION_ID_STR,cr_id);
	ma_message_set_property(msg,EPO_DATACHANNEL_ITEM_PROP_ORIGIN_STR,pr_id);
	ma_message_set_property(msg,EPO_DATACHANNEL_ITEM_PROP_STATUS_STR,"0");
	ma_message_set_property(msg,EPO_DATACHANNEL_ITEM_PROP_FLAGS_STR,"11111");
	ma_message_set_payload(msg, variant_ptr);
	ma_variant_release(variant_ptr);
	printf("DC message published for product=%s\n",pr_id);
	ma_msgbus_publish(msgbus_obj, "Echo-message", MSGBUS_CONSUMER_REACH_OUTPROC, msg);
	ma_message_release(msg);
}

TEST(datachannel_client_functional_test,dc_message_test_valid3){
	
	ma_int64_t test_ttl = 100;
	static const char *product_id1 = "DC_PP1";
	static const char *product_id2 = "DC_PP2";
	ma_message_t* response_payload = NULL;

	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_datachannel_register_message_handler_callback(g_ma_client,product_id1,dc_onmessage_cb3,NULL));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_datachannel_register_message_handler_callback(g_ma_client,product_id2,dc_onmessage_cb3_1,NULL));

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_datachannel_subscribe(g_ma_client,product_id1,"Echo-message",MA_DC_SUBSCRIBER_LISTENER));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_datachannel_subscribe(g_ma_client,product_id2,"Echo-message",MA_DC_SUBSCRIBER_LISTENER));
	
	check = 0;
	publish_dcmessage(response_payload,product_id1,"100");
	#ifdef MA_WINDOWS
		Sleep(1000*10);
	#else
		sleep(10);
	#endif	

	TEST_ASSERT_EQUAL_INT(2,check);
	
	check = 0;
	publish_dcmessage(response_payload,product_id2,"200");
	#ifdef MA_WINDOWS
		Sleep(1000*10);
	#else
		sleep(10);
	#endif	
	TEST_ASSERT_EQUAL_INT(2,check);
	
		
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_datachannel_unsubscribe(g_ma_client,product_id1,"Echo-message"));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_datachannel_unsubscribe(g_ma_client,product_id2,"Echo-message"));

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_datachannel_unregister_message_handler_callback(g_ma_client,product_id1));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_datachannel_unregister_message_handler_callback(g_ma_client,product_id2));
}
////////////////////////////////////////////////////////////////////////////

////////////////////////////Test4//////////////////////////////////////////
static void dc_notification_cb3(ma_client_t *client, const char *product_id, const char *message_id, ma_int64_t correlation_id, ma_datachannel_item_notification_t status, void *cb_data) {
	printf("###datachannel on notification callback invoked for product id = %s###\n",product_id);
	printf("message id=%s, correlation id=%d, notification=%d\n", message_id, correlation_id, status);
	if(correlation_id == 100)
		check++;
}

static void dc_notification_cb3_1(ma_client_t *client, const char *product_id, const char *message_id, ma_int64_t correlation_id, ma_datachannel_item_notification_t status, void *cb_data) {
	printf("###datachannel on notification callback invoked for product id = %s###\n",product_id);
	printf("message id=%s, correlation id=%d, notification=%d\n", message_id, correlation_id, status);
	if(correlation_id == 200)
		check++;
}

void publish_notification(ma_message_t* msg,const char *pr_id,char *cr_id){
	
	ma_variant_t *variant_ptr = NULL;

	//msgbus_obj = ma_ut_setup_get_msgbus(g_ut_setup);
	ma_message_create(&msg);
	ma_variant_create_from_string("DC-Reply-Message", strlen("DC-Reply-Message"), &variant_ptr);
	ma_message_set_property(msg,EPO_DATACHANNEL_ITEM_PROP_MESSAGE_TYPE_STR,"DC-Item1");
	ma_message_set_property(msg,EPO_DATACHANNEL_ITEM_PROP_CORRELATION_ID_STR,cr_id);
	ma_message_set_property(msg,EPO_DATACHANNEL_ITEM_PROP_ORIGIN_STR,pr_id);
	ma_message_set_property(msg,EPO_DATACHANNEL_ITEM_PROP_STATUS_STR,"0");
	ma_message_set_property(msg,EPO_DATACHANNEL_ITEM_PROP_FLAGS_STR,"11111");
	ma_message_set_payload(msg, variant_ptr);
	ma_variant_release(variant_ptr);
	printf("DC message Notification for product=%s\n",pr_id);
	ma_msgbus_publish(msgbus_obj, EPO_DATACHANNEL_NOTIFICATION_PUBLISHER_TOPIC_STR, MSGBUS_CONSUMER_REACH_OUTPROC, msg);
	ma_message_release(msg);
	
}

TEST(datachannel_client_functional_test,dc_message_test_valid4){
	
	ma_int64_t test_ttl = 100;
	static const char *product_id1 = "DC_PP1";
	static const char *product_id2 = "DC_PP2";
	ma_message_t* response_payload = NULL;

	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_datachannel_register_notification_callback(g_ma_client,product_id1,dc_notification_cb3,NULL));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_datachannel_register_notification_callback(g_ma_client,product_id2,dc_notification_cb3_1,NULL));
	check = 0;
	publish_notification(response_payload,product_id1,"100");
	#ifdef MA_WINDOWS
		Sleep(1000*10);
	#else
		sleep(10);
	#endif	
	TEST_ASSERT_EQUAL_INT(1,check);
	
	
	publish_notification(response_payload,product_id2,"200");
	#ifdef MA_WINDOWS
		Sleep(1000*10);
	#else
		sleep(10);
	#endif	
	TEST_ASSERT_EQUAL_INT(2,check);
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_datachannel_unregister_notification_callback(g_ma_client,product_id1));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_datachannel_unregister_notification_callback(g_ma_client,product_id2));

}
////////////////////////////////////////////////////////////////////////////

TEST(datachannel_client_functional_test, stop_service) {
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_server_stop(p_server));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_server_release(p_server));

 }

TEST(datachannel_client_functional_test, stop_client) {

	ma_client_stop(g_ma_client);
	ma_client_release(g_ma_client);
	
}



