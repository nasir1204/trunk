/*****************************************************************************
Policy Client - Policy URI Unit Tests
******************************************************************************/

#include "unity.h"
#include "unity_fixture.h"
#include "ma/policy/ma_policy_uri.h"
#include <string.h>

TEST_GROUP_RUNNER(policy_uri_tests_group)
{
    do {RUN_TEST_CASE(policy_uri_tests, set_get_policy_information_tests)} while (0);
    do {RUN_TEST_CASE(policy_uri_tests, uri_set_get_interface_tests)} while (0);
}

TEST_SETUP(policy_uri_tests)
{
}

TEST_TEAR_DOWN(policy_uri_tests)
{
}

TEST(policy_uri_tests, set_get_policy_information_tests)
{
    char *value = NULL;
    ma_policy_uri_t *policy_uri = NULL;
    ma_policy_uri_t *policy_uri_new = NULL;
    ma_uint8_t counter = 0;

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_create(NULL));
    TEST_ASSERT(MA_OK == ma_policy_uri_create(&policy_uri));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_set_feature(NULL, "test_feature"));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_set_feature(policy_uri, NULL));
    TEST_ASSERT(MA_OK == ma_policy_uri_set_feature(policy_uri, "test_feature"));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_set_category(NULL, "test_category"));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_set_category(policy_uri, NULL));
    TEST_ASSERT(MA_OK == ma_policy_uri_set_category(policy_uri, "test_category"));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_set_type(NULL, "test_type"));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_set_type(policy_uri, NULL));
    TEST_ASSERT(MA_OK == ma_policy_uri_set_type(policy_uri, "test_type"));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_set_name(NULL, "test_name"));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_set_name(policy_uri, NULL));
    TEST_ASSERT(MA_OK == ma_policy_uri_set_name(policy_uri, "test_name"));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_set_pso_name(NULL, "test_policy_name"));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_set_pso_name(policy_uri, NULL));
    TEST_ASSERT(MA_OK == ma_policy_uri_set_pso_name(policy_uri, "test_policy_name"));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_set_pso_param_int(NULL, "1234"));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_set_pso_param_int(policy_uri, NULL));
    TEST_ASSERT(MA_OK == ma_policy_uri_set_pso_param_int(policy_uri, "1234"));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_set_pso_param_str(NULL, "test_policy_parameter"));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_set_pso_param_str(policy_uri, NULL));
    TEST_ASSERT(MA_OK == ma_policy_uri_set_pso_param_str(policy_uri, "test_policy_parameter"));

    /* Policy information copying */
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_copy(NULL, &policy_uri_new));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_copy(policy_uri, NULL));
    TEST_ASSERT(MA_OK == ma_policy_uri_copy(policy_uri, &policy_uri_new));

    /* Releasing the first Policy URI */
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_release(NULL));
    TEST_ASSERT(MA_OK == ma_policy_uri_release(policy_uri));

    /* Verifying the policy information using get APIs (through copied URI Information object) */
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_get_feature(NULL, &value));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_get_feature(policy_uri_new, NULL));
    TEST_ASSERT(MA_OK == ma_policy_uri_get_feature(policy_uri_new, &value));
    TEST_ASSERT(0 == strcmp(value, "test_feature"));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_get_category(NULL, &value));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_get_category(policy_uri_new, NULL));
    TEST_ASSERT(MA_OK == ma_policy_uri_get_category(policy_uri_new, &value));
    TEST_ASSERT(0 == strcmp(value, "test_category"));

    TEST_ASSERT(MA_ERROR_INVALIDARG  == ma_policy_uri_get_type(NULL, &value));
    TEST_ASSERT(MA_ERROR_INVALIDARG  == ma_policy_uri_get_type(policy_uri_new, NULL));
    TEST_ASSERT(MA_OK  == ma_policy_uri_get_type(policy_uri_new, &value));
    TEST_ASSERT(0 == strcmp(value, "test_type"));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_get_name(NULL, &value));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_get_name(policy_uri_new, NULL));
    TEST_ASSERT(MA_OK == ma_policy_uri_get_name(policy_uri_new, &value));
    TEST_ASSERT(0 == strcmp(value, "test_name"));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_get_pso_name(NULL, &value));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_get_pso_name(policy_uri_new, NULL));
    TEST_ASSERT(MA_OK == ma_policy_uri_get_pso_name(policy_uri_new, &value));
    TEST_ASSERT(0 == strcmp(value, "test_policy_name"));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_get_pso_param_int(NULL, &value));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_get_pso_param_int(policy_uri_new, NULL));
    TEST_ASSERT(MA_OK == ma_policy_uri_get_pso_param_int(policy_uri_new, &value));
    TEST_ASSERT(0 == strcmp(value, "1234"));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_get_pso_param_str(NULL, &value));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_get_pso_param_str(policy_uri_new, NULL));
    TEST_ASSERT(MA_OK == ma_policy_uri_get_pso_param_str(policy_uri_new, &value));
    TEST_ASSERT(0 == strcmp(value, "test_policy_parameter"));

    // Adding 10 references to the new URI object
    for(counter = 0 ; counter < 10; counter++)
    {
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_add_ref(NULL));
        TEST_ASSERT(MA_OK == ma_policy_uri_add_ref(policy_uri_new));
    }

    // Releasing 11 times to remove all references and freeing URI object
    for(counter = 0 ; counter < 11; counter++)
    {
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_release(NULL));
        TEST_ASSERT(MA_OK == ma_policy_uri_release(policy_uri_new));
    }
}

TEST(policy_uri_tests, uri_set_get_interface_tests)
{
    char *value = NULL;
    ma_policy_uri_t *policy_uri = NULL;
    ma_policy_uri_t *policy_uri_new = NULL;
    ma_uint8_t counter = 0;

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_create(NULL));
    TEST_ASSERT(MA_OK == ma_policy_uri_create(&policy_uri));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_set_software_id(NULL, "No software ID"));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_set_software_id(policy_uri, NULL));
    TEST_ASSERT(MA_OK == ma_policy_uri_set_software_id(policy_uri, "No software ID"));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_set_version(NULL, "version information not available"));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_set_version(policy_uri, NULL));
    TEST_ASSERT(MA_OK == ma_policy_uri_set_version(policy_uri, "version information not available"));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_set_user(NULL, "admin user 1"));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_set_user(policy_uri, NULL));
    TEST_ASSERT(MA_OK == ma_policy_uri_set_user(policy_uri, "admin user 1"));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_set_location_id(NULL, "Unkown location"));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_set_location_id(policy_uri, NULL));
    TEST_ASSERT(MA_OK == ma_policy_uri_set_location_id(policy_uri, "Unkown location"));

    {
        ma_array_t *user_list = NULL;
        TEST_ASSERT(MA_OK == ma_array_create(&user_list));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_set_user_list(policy_uri, NULL));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_set_user_list(NULL, user_list));
        TEST_ASSERT(MA_OK == ma_policy_uri_set_user_list(policy_uri, user_list));

        TEST_ASSERT(MA_OK == ma_array_release(user_list));
    }

    /* Policy information copying */
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_copy(NULL, &policy_uri_new));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_copy(policy_uri, NULL));
    TEST_ASSERT(MA_OK == ma_policy_uri_copy(policy_uri, &policy_uri_new));

    /* Releasing the first Policy URI */
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_release(NULL));
    TEST_ASSERT(MA_OK == ma_policy_uri_release(policy_uri));

    /* Verifying the policy information using get APIs (through copied URI Information object) */
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_get_software_id(NULL, &value));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_get_software_id(policy_uri_new, NULL));
    TEST_ASSERT(MA_OK == ma_policy_uri_get_software_id(policy_uri_new, &value));
    TEST_ASSERT(0 == strcmp(value, "No software ID"));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_get_version(NULL, &value));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_get_version(policy_uri_new, NULL));
    TEST_ASSERT(MA_OK == ma_policy_uri_get_version(policy_uri_new, &value));
    TEST_ASSERT(0 == strcmp(value, "version information not available"));

    TEST_ASSERT(MA_ERROR_INVALIDARG  == ma_policy_uri_get_user(NULL, &value));
    TEST_ASSERT(MA_ERROR_INVALIDARG  == ma_policy_uri_get_user(policy_uri_new, NULL));
    TEST_ASSERT(MA_OK  == ma_policy_uri_get_user(policy_uri_new, &value));
    TEST_ASSERT(0 == strcmp(value, "admin user 1"));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_get_location_id(NULL, &value));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_get_location_id(policy_uri_new, NULL));
    TEST_ASSERT(MA_OK == ma_policy_uri_get_location_id(policy_uri_new, &value));
    TEST_ASSERT(0 == strcmp(value, "Unkown location"));

    // Adding 10 references to the new URI object
    for(counter = 0 ; counter < 10; counter++)
    {
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_add_ref(NULL));
        TEST_ASSERT(MA_OK == ma_policy_uri_add_ref(policy_uri_new));
    }

    // Releasing 11 times to remove all references and freeing URI object
    for(counter = 0 ; counter < 11; counter++)
    {
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_uri_release(NULL));
        TEST_ASSERT(MA_OK == ma_policy_uri_release(policy_uri_new));
    }
}




