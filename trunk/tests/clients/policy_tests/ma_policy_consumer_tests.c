/*****************************************************************************
Policy Client - Policy Consumer Unit Tests

Policy Service is simulated in this test. For this unit test purposes, Policy
service performs INPROC communication with Policy client.
******************************************************************************/

#include "unity.h"
#include "unity_fixture.h"
#include "ma/policy/ma_policy.h"
#include "ma/internal/clients/policy/ma_policy_internal.h"
#include "ma/ma_message.h"
#include "ma/ma_msgbus.h"
#include "ma_ut_setup.h"
#include <string.h>

static const char *test_software_id = "MA Next Generation Software";
static const char *test_user_id = "Current Root User";
static const char *test_domain_id = "McAfee\\MIC";
static const char *test_location_id = "McAfee Bangalore";

static ma_client_t *g_ma_client = NULL;
static ma_msgbus_t *g_msgbus = NULL;
static ma_ut_setup_t *g_ut_setup = NULL;

static ma_uint8_t test_counter = 0;

static ma_variant_t *global_policy_bag_ptr = NULL;

static const char product_id[] = "ma.policy.client.test";

// Extern definition.
extern ma_error_t ma_policy_uri_create_from_table(ma_table_t *prop_table, ma_policy_uri_t **uri);

TEST_GROUP_RUNNER(policy_consumer_tests_group)
{
    do {RUN_TEST_CASE(policy_consumer_tests, policy_consumer_get_refresh_policy_test)} while (0);
}

/**************************************************************************
1) Creating the unit test context.
2) Retrieving underlying message bus instance.
3) Creating and starting message bus service (Property Service) using the
   message bus instance.
**************************************************************************/
TEST_SETUP(policy_consumer_tests)
{
    ma_ut_setup_create("policy_client.policy_consumer.tests", "ma.policy.client.test", &g_ut_setup);
    g_ma_client = ma_ut_setup_get_client(g_ut_setup);
    g_msgbus = ma_ut_setup_get_msgbus(g_ut_setup);
}

/**************************************************************************
1) Releasing the context.
**************************************************************************/
TEST_TEAR_DOWN(policy_consumer_tests)
{
    ma_ut_setup_release(g_ut_setup);
}


/******************************************************************************
Call back function to be executed when the Policy service receives a request
from Policy client for the policies.

This function performs following tasks -

TEST 1 -
1) The payload contains the URI Interface information and policy bag.
2) Verify that the URI information is correct and store the policy bag. This bag
   will be returned when the ma_policy_get API is called.
3) Return MA_OK response with no payload in message.

TEST 2 -
1) The payload contains the URI Interface information.
2) Verify that the URI information is correct and return the policy bag saved in
   TEST 1 as payload.

TEST 3 -
1) The payload contains the URI Interface information.
2) Verify that the URI information is correct.
3) Return MA_OK response with no payload in message.

TEST 4 -
1) There is no payload in the request.
2) Verify tthe reason for policy enforcement and product ID.
3) No response is required because it is send-and-forget type request.
******************************************************************************/
ma_error_t respond_to_policy_request(ma_msgbus_server_t *server,
                                     ma_message_t *request_payload,
                                     void *cb_data,
                                     ma_msgbus_client_request_t *c_request)
{
    // Extracting Request Type from the message
    {
        const char *string_val = NULL;

        TEST_ASSERT(MA_OK == ma_message_get_property(request_payload, "prop.key.policy_request_type", &string_val));

        // Verify that the request type
        switch(test_counter)
        {
            case 1 : TEST_ASSERT(0 ==strcmp("prop.value.set_policies_request", string_val));
                     break;

            case 2 : TEST_ASSERT(0 ==strcmp("prop.value.get_policies_request", string_val));
                     break;

            case 3 : TEST_ASSERT(0 ==strcmp("prop.value.refresh_policies_request", string_val));
                     break;

            case 4 : TEST_ASSERT(0 ==strcmp("prop.value.enforce_policies_request", string_val));
                     break;

            default: TEST_FAIL();
        }
    }

    // ma_policy_set API. In this case, parse the payload and store the policy bag
    if(test_counter == 1)
    {
        ma_variant_t *payload_variant = NULL;
        ma_array_t *payload_array = NULL;

        ma_variant_t *variant_ptr = NULL;
        ma_table_t *table_ptr = NULL;
        ma_policy_uri_t *uri_info = NULL;

        // Payload for response message
        ma_message_t *response_payload = NULL;

        TEST_ASSERT(MA_OK == ma_message_get_payload(request_payload, &payload_variant));

        // Retrieve array from variant and release variant
        TEST_ASSERT(MA_OK == ma_variant_get_array(payload_variant, &payload_array));
        TEST_ASSERT(MA_OK == ma_variant_release(payload_variant));

        // First element is URI interface table. Getting the info table from variant,
        // converting into to Policy URI (ma_policy_uri_t) and the verifying the URI
        // information
        TEST_ASSERT(MA_OK == ma_array_get_element_at(payload_array, 1, &variant_ptr));
        TEST_ASSERT(MA_OK == ma_variant_get_table(variant_ptr, &table_ptr));
        TEST_ASSERT(MA_OK == ma_policy_uri_create_from_table(table_ptr, &uri_info));

        {
            const char *string_val = NULL;
            TEST_ASSERT(MA_OK == ma_policy_uri_get_software_id(uri_info, &string_val));
            TEST_ASSERT(0 == strcmp(test_software_id, string_val));

            TEST_ASSERT(MA_OK == ma_policy_uri_get_user(uri_info, &string_val));
            TEST_ASSERT(0 == strcmp(test_user_id, string_val));

            TEST_ASSERT(MA_OK == ma_policy_uri_get_location_id(uri_info, &string_val));
            TEST_ASSERT(0 == strcmp(test_location_id, string_val));
        }

        TEST_ASSERT(MA_OK == ma_policy_uri_release(uri_info));
        TEST_ASSERT(MA_OK == ma_table_release(table_ptr));
        TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

        // Second element is policy data (bag). Retrieve the policy bag variant and store into
        // a global variable for later use.
        TEST_ASSERT(MA_OK == ma_array_get_element_at(payload_array, 2, &global_policy_bag_ptr));

        // Release the array
        TEST_ASSERT(MA_OK == ma_array_release(payload_array));

        // Creating a MA_OK response message and sending it to the policy client
        TEST_ASSERT(MA_OK == ma_message_create(&response_payload));
        TEST_ASSERT(MA_OK == ma_msgbus_server_client_request_post_result(c_request, MA_OK, response_payload));
        TEST_ASSERT(MA_OK == ma_message_release(response_payload));
    }

    // ma_policy_get API. In this case, return the policy bag (stored in TEST 1) in payload
    if(test_counter == 2)
    {
        ma_variant_t *payload_variant = NULL;

        ma_table_t *table_ptr = NULL;
        ma_policy_uri_t *uri_info = NULL;

        // Payload for response message
        ma_message_t *response_payload = NULL;

        TEST_ASSERT(MA_OK == ma_message_get_payload(request_payload, &payload_variant));

        // Retrieve info table from variant, converting into to Policy URI
        // (ma_policy_uri_t) and the verifying the URI information. Release variant.
        TEST_ASSERT(MA_OK == ma_variant_get_table(payload_variant, &table_ptr));
        TEST_ASSERT(MA_OK == ma_policy_uri_create_from_table(table_ptr, &uri_info))
        TEST_ASSERT(MA_OK == ma_variant_release(payload_variant));

        {
            const char *string_val = NULL;
            TEST_ASSERT(MA_OK == ma_policy_uri_get_software_id(uri_info, &string_val));
            TEST_ASSERT(0 == strcmp(test_software_id, string_val));

            TEST_ASSERT(MA_OK == ma_policy_uri_get_user(uri_info, &string_val));
            TEST_ASSERT(0 == strcmp(test_user_id, string_val));

            TEST_ASSERT(MA_OK == ma_policy_uri_get_location_id(uri_info, &string_val));
            TEST_ASSERT(0 == strcmp(test_location_id, string_val));
        }

        TEST_ASSERT(MA_OK == ma_policy_uri_release(uri_info));
        TEST_ASSERT(MA_OK == ma_table_release(table_ptr));

        // 1) Create a response message.
        // 2) Set the policy bag stored in TEST 1 as payload.
        // 3) Send the MA_OK response.
        // 4) Release message.
        TEST_ASSERT(MA_OK == ma_message_create(&response_payload));
        TEST_ASSERT(MA_OK == ma_message_set_payload(response_payload, global_policy_bag_ptr));
        TEST_ASSERT(MA_OK == ma_msgbus_server_client_request_post_result(c_request, MA_OK, response_payload));
        TEST_ASSERT(MA_OK == ma_message_release(response_payload));
    }

    // ma_policy_refresh API. In this case, return MA_OK response.
    if(test_counter == 3)
    {
        ma_variant_t *payload_variant = NULL;

        ma_table_t *table_ptr = NULL;
        ma_policy_uri_t *uri_info = NULL;

        // Payload for response message
        ma_message_t *response_payload = NULL;

        TEST_ASSERT(MA_OK == ma_message_get_payload(request_payload, &payload_variant));

        // Retrieve info table from variant, converting into to Policy URI
        // (ma_policy_uri_t) and the verifying the URI information. Release variant.
        TEST_ASSERT(MA_OK == ma_variant_get_table(payload_variant, &table_ptr));
        TEST_ASSERT(MA_OK == ma_policy_uri_create_from_table(table_ptr, &uri_info))
        TEST_ASSERT(MA_OK == ma_variant_release(payload_variant));

        {
            const char *string_val = NULL;
            TEST_ASSERT(MA_OK == ma_policy_uri_get_software_id(uri_info, &string_val));
            TEST_ASSERT(0 == strcmp(test_software_id, string_val));

            TEST_ASSERT(MA_OK == ma_policy_uri_get_user(uri_info, &string_val));
            TEST_ASSERT(0 == strcmp(test_user_id, string_val));

            TEST_ASSERT(MA_OK == ma_policy_uri_get_location_id(uri_info, &string_val));
            TEST_ASSERT(0 == strcmp(test_location_id, string_val));
        }

        TEST_ASSERT(MA_OK == ma_policy_uri_release(uri_info));
        TEST_ASSERT(MA_OK == ma_table_release(table_ptr));

        // Creating a MA_OK response message and sending it to the policy client
        TEST_ASSERT(MA_OK == ma_message_create(&response_payload));
        TEST_ASSERT(MA_OK == ma_msgbus_server_client_request_post_result(c_request, MA_OK, response_payload));
        TEST_ASSERT(MA_OK == ma_message_release(response_payload));
    }

    // ma_policy_enforce API. In this case, no reponse is required.
    if(test_counter == 4)
    {
        const char *string_val = NULL;

        TEST_ASSERT(MA_OK == ma_message_get_property(request_payload, "prop.key.policy_enforce_reason", &string_val));
        TEST_ASSERT(0 ==strcmp("prop.value.policy_timeout", string_val));

        TEST_ASSERT(MA_OK == ma_message_get_property(request_payload, "prop.key.initiator_id", &string_val));
        TEST_ASSERT(0 ==strcmp(product_id, string_val));
    }	
	
	return MA_OK;
}


TEST(policy_consumer_tests, policy_consumer_get_refresh_policy_test)
{
    // URI Interface, Policy URI Information and Policy Bag
    ma_policy_uri_t *uri_ptr = NULL;
    ma_policy_uri_t *policy_uri = NULL;
    ma_policy_bag_t *my_policy_bag = NULL;

    // For storing the Policy bag retrieved by ma_policy_get API
    ma_policy_bag_t *new_policy_bag = NULL;

    // Policy service object
    ma_msgbus_server_t *p_server = NULL;

    // Creating URI Interface Object
    TEST_ASSERT(ma_policy_uri_create(&uri_ptr) == MA_OK);

    {
        ma_policy_uri_set_software_id(uri_ptr, test_software_id);
        ma_policy_uri_set_user(uri_ptr, test_user_id);
        ma_policy_uri_set_location_id(uri_ptr, test_location_id);
    }

    // Creating Policy URI Information
    TEST_ASSERT(MA_OK == ma_policy_uri_create(&policy_uri));

    {
        TEST_ASSERT(MA_OK == ma_policy_uri_set_feature(policy_uri, "My Policy Features"));
        TEST_ASSERT(MA_OK == ma_policy_uri_set_category(policy_uri, "Special Category"));
        TEST_ASSERT(MA_OK == ma_policy_uri_set_type(policy_uri, "No Identification"));
        TEST_ASSERT(MA_OK == ma_policy_uri_set_name(policy_uri, "First Policy URI"));
    }

    // Creating a Policy Bag
    TEST_ASSERT(MA_OK ==  ma_policy_bag_create(&my_policy_bag));

    // Setting values in Policy bag
    {
        ma_variant_t *my_variant_ptr = NULL;

        TEST_ASSERT(MA_OK == ma_variant_create_from_string("VALUE 1", strlen("VALUE 1"), &my_variant_ptr));
        TEST_ASSERT(MA_OK == ma_policy_bag_set_value(my_policy_bag, policy_uri, "SECTION 1", "KEY 1", my_variant_ptr));
        TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));

        TEST_ASSERT(MA_OK == ma_variant_create_from_string("VALUE 2", strlen("VALUE 2"), &my_variant_ptr));
        TEST_ASSERT(MA_OK == ma_policy_bag_set_value(my_policy_bag, policy_uri, "SECTION 2", "KEY 2", my_variant_ptr));
        TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));

        TEST_ASSERT(MA_OK == ma_variant_create_from_string("VALUE 4", strlen("VALUE 4"), &my_variant_ptr));
        TEST_ASSERT(MA_OK == ma_policy_bag_set_value(my_policy_bag, policy_uri, "SECTION 1", "KEY 4", my_variant_ptr));
        TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));

        TEST_ASSERT(MA_OK == ma_variant_create_from_string("VALUE 6", strlen("VALUE 6"), &my_variant_ptr));
        TEST_ASSERT(MA_OK == ma_policy_bag_set_value(my_policy_bag, policy_uri, "SECTION 3", "KEY 6", my_variant_ptr));
        TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));

        TEST_ASSERT(MA_OK == ma_variant_create_from_string("VALUE 8", strlen("VALUE 8"), &my_variant_ptr));
        TEST_ASSERT(MA_OK == ma_policy_bag_set_value(my_policy_bag, policy_uri, "SECTION 3", "KEY 8", my_variant_ptr));
        TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));

        TEST_ASSERT(MA_OK == ma_variant_create_from_string("VALUE 10", strlen("VALUE 10"), &my_variant_ptr));
        TEST_ASSERT(MA_OK == ma_policy_bag_set_value(my_policy_bag, policy_uri, "SECTION 3", "KEY 10", my_variant_ptr));
        TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));
    }

    // Creating dummy policy service to send/receive the policies to/from policy
    // client when requested
    TEST_ASSERT(ma_msgbus_server_create(g_msgbus, "ma.service.policy", &p_server) == MA_OK);
    TEST_ASSERT(ma_msgbus_server_setopt(p_server, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_INPROC) == MA_OK);
    TEST_ASSERT(ma_msgbus_server_start(p_server, respond_to_policy_request, NULL) == MA_OK);

    /**************************************************************************
    TEST 1 - Sending the Policy Bag to Policy Service
    **************************************************************************/
    test_counter = 1;

    // Setting policies in the Policy service using ma_policy_set API
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_set(NULL, uri_ptr, my_policy_bag));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_set(g_ma_client, NULL, my_policy_bag));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_set(g_ma_client, uri_ptr, NULL));
    TEST_ASSERT(MA_OK == ma_policy_set(g_ma_client, uri_ptr, my_policy_bag));

    // Releasing Policy Bag
    TEST_ASSERT(MA_OK == ma_policy_bag_release(my_policy_bag));

    // The dummy policy service is designed to return the policy Bag when the ma_policy_get API
    // is called

    /**************************************************************************
    TEST 2 - Requesting Policy Bag from Policy Service
    **************************************************************************/
    test_counter = 2;

    // Requesting for policies (invalid and valid scenarios)
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_get_internal(g_ma_client, NULL, &new_policy_bag));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_get_internal(NULL, uri_ptr, &new_policy_bag));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_get_internal(g_ma_client, uri_ptr, NULL));
    TEST_ASSERT(MA_OK == ma_policy_get_internal(g_ma_client, uri_ptr, &new_policy_bag));

    // Verifying the information in policy bag
    {
        ma_buffer_t *my_buffer = NULL;
        const char *my_string = NULL;
        size_t my_string_size = 0;

        ma_variant_t *my_variant_ptr = NULL;

        TEST_ASSERT(MA_OK == ma_policy_bag_get_value(new_policy_bag, policy_uri, "SECTION 1", "KEY 1", &my_variant_ptr));
        ma_variant_get_string_buffer(my_variant_ptr, &my_buffer);
        ma_buffer_get_string(my_buffer, &my_string, &my_string_size);
        TEST_ASSERT(0 == strcmp(my_string, "VALUE 1"));
        TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
        TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));

        TEST_ASSERT(MA_OK == ma_policy_bag_get_value(new_policy_bag, policy_uri, "SECTION 2", "KEY 2", &my_variant_ptr));
        ma_variant_get_string_buffer(my_variant_ptr, &my_buffer);
        ma_buffer_get_string(my_buffer, &my_string, &my_string_size);
        TEST_ASSERT(0 == strcmp(my_string, "VALUE 2"));
        TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
        TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));

        TEST_ASSERT(MA_OK == ma_policy_bag_get_value(new_policy_bag, policy_uri, "SECTION 1", "KEY 4", &my_variant_ptr));
        ma_variant_get_string_buffer(my_variant_ptr, &my_buffer);
        ma_buffer_get_string(my_buffer, &my_string, &my_string_size);
        TEST_ASSERT(0 == strcmp(my_string, "VALUE 4"));
        TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
        TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));

        TEST_ASSERT(MA_OK == ma_policy_bag_get_value(new_policy_bag, policy_uri, "SECTION 3", "KEY 6", &my_variant_ptr));
        ma_variant_get_string_buffer(my_variant_ptr, &my_buffer);
        ma_buffer_get_string(my_buffer, &my_string, &my_string_size);
        TEST_ASSERT(0 == strcmp(my_string, "VALUE 6"));
        TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
        TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));

        TEST_ASSERT(MA_OK == ma_policy_bag_get_value(new_policy_bag, policy_uri, "SECTION 3", "KEY 8", &my_variant_ptr));
        ma_variant_get_string_buffer(my_variant_ptr, &my_buffer);
        ma_buffer_get_string(my_buffer, &my_string, &my_string_size);
        TEST_ASSERT(0 == strcmp(my_string, "VALUE 8"));
        TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
        TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));

        TEST_ASSERT(MA_OK == ma_policy_bag_get_value(new_policy_bag, policy_uri, "SECTION 3", "KEY 10", &my_variant_ptr));
        ma_variant_get_string_buffer(my_variant_ptr, &my_buffer);
        ma_buffer_get_string(my_buffer, &my_string, &my_string_size);
        TEST_ASSERT(0 == strcmp(my_string, "VALUE 10"));
        TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
        TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));
    }

    TEST_ASSERT(MA_OK == ma_policy_bag_release(new_policy_bag));

    /**************************************************************************
    TEST 3 - Policy Refresh Request
    **************************************************************************/
    test_counter = 3;

    // For refresh policy, only verify that the request with correct message type
    // is sent to the policy service.
    TEST_ASSERT(ma_policy_refresh(g_ma_client, NULL) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_policy_refresh(NULL, uri_ptr) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_policy_refresh(g_ma_client, uri_ptr) == MA_OK);


    /**************************************************************************
    TEST 4 - Policy Enforcement Request
    **************************************************************************/
    test_counter = 4;

    // For enforce policy, only verify that the request with correct message type
    // is sent to the policy service.
    TEST_ASSERT(ma_policy_enforce(NULL, product_id) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_policy_enforce(g_ma_client, product_id) == MA_OK);

    // Stopping and releasing server
    TEST_ASSERT(ma_msgbus_server_stop(p_server) == MA_OK);
    TEST_ASSERT(ma_msgbus_server_release(p_server) == MA_OK);

    // Releasing URI and Policy Bag
    //TEST_ASSERT(MA_OK == ma_policy_bag_release(my_policy_bag));
    TEST_ASSERT(MA_OK == ma_policy_uri_release(policy_uri));
    TEST_ASSERT(MA_OK == ma_policy_uri_release(uri_ptr));

    // Release the Global Policy Bag object. It job is done.
    TEST_ASSERT(MA_OK == ma_variant_release(global_policy_bag_ptr));
}

