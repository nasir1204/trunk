/*****************************************************************************
Policy Client - Policy Notification Unit Tests

Policy Service is simulated in this test. For this unit test purposes, Policy
service performs INPROC communication with Policy Client to publish the notification.
******************************************************************************/

#include "unity.h"
#include "unity_fixture.h"
#include "ma/policy/ma_policy.h"
#include "ma_ut_setup.h"
#include "ma/ma_message.h"
#include "ma/ma_msgbus.h"
#include <string.h>

#ifndef WIN32
    #include <unistd.h>
#else
    #include <Windows.h>
#endif

// When set to 1, it indicates that the policy client has received notification
// published by the policy service
static uint8_t notification_received_by_client = 0;

static ma_uint16_t counter = 0;

static ma_client_t *g_ma_client = NULL;
static ma_msgbus_t *g_msgbus = NULL;
static ma_ut_setup_t *g_ut_setup = NULL;

static const char product_id[] = "ma.policy.client.test";

struct LookupTable
{
    const char *test_software_id;
    ma_uint32_t register_notification_type;                     // Type for which notification is registered
    const char *publish_notification_type;                      // Type for which the notification is published by the policy service.
    ma_policy_notification_type_t received_notification_type;   // The notification type which the policy client should receive in the URI
    ma_uint32_t cb_data;
};

struct LookupTable const test_vector[] =
{
    { "Software ID 1",  MA_POLICY_TIMEOUT,                         "prop.value.notification_policy_timeout",    MA_POLICY_TIMEOUT,    0x11112222UL },
    { "Software ID 2",  MA_USER_CHANGED,                           "prop.value.notification_user_changed",      MA_USER_CHANGED,      0x32456789UL },
    { "Software ID 3",  MA_POLICY_CHANGED,                         "prop.value.notification_policy_changed",    MA_POLICY_CHANGED,    0x23232323UL },
    { "Software ID 4",  MA_LOCATION_CHANGED,                       "prop.value.notification_location_changed",  MA_LOCATION_CHANGED,  0x45454545UL },
    { "Software ID 5",  MA_SERVICE_STARTUP,                        "prop.value.notification_service_startup",   MA_SERVICE_STARTUP,   0x12345678UL },		
    { "Software ID 6",  MA_POLICY_CHANGED | MA_LOCATION_CHANGED,   "prop.value.notification_policy_changed",    MA_POLICY_CHANGED,    0x67676767UL },
    { "Software ID 7",  MA_POLICY_CHANGED | MA_LOCATION_CHANGED,   "prop.value.notification_location_changed",  MA_LOCATION_CHANGED,  0x89898989UL },
    { "Software ID 8",  MA_POLICY_NOTIFICATION_ALL,                "prop.value.notification_policy_changed",    MA_POLICY_CHANGED,    0x76655432UL },
    { "Software ID 9",  MA_POLICY_NOTIFICATION_ALL,                "prop.value.notification_location_changed",  MA_LOCATION_CHANGED,  0x23455678UL },
    { "Software ID 10", MA_POLICY_NOTIFICATION_ALL,                "prop.value.notification_user_changed",      MA_USER_CHANGED,      0x23456780UL },
    { "Software ID 11", MA_POLICY_NOTIFICATION_ALL,                "prop.value.notification_policy_timeout",    MA_POLICY_TIMEOUT,    0x00000001UL },
    { "Software ID 12", MA_POLICY_NOTIFICATION_ALL,                "prop.value.notification_service_startup",   MA_SERVICE_STARTUP,   0x76543210UL },
};

TEST_GROUP_RUNNER(policy_notification_tests_group)
{
    do {RUN_TEST_CASE(policy_notification_tests, policy_notification_callback_test)} while (0);
}


/**************************************************************************
1) Creating the unit test context.
2) Retrieving underlying message bus instance.
3) Creating and starting message bus service (Property Service) using the
   message bus instance.
**************************************************************************/
TEST_SETUP(policy_notification_tests)
{
    ma_ut_setup_create("policy_client.policy_notification.tests", "ma.policy.client.test", &g_ut_setup);
    g_ma_client = ma_ut_setup_get_client(g_ut_setup);
    g_msgbus = ma_ut_setup_get_msgbus(g_ut_setup);

}

/**************************************************************************
1) Releasing the context.
**************************************************************************/
TEST_TEAR_DOWN(policy_notification_tests)
{
 ma_ut_setup_release(g_ut_setup);
}

// Verify the Notification information sent to the Policy Client by the Policy Service.
ma_error_t notification_callback_function(ma_client_t *ma_client, const char *product_id, const ma_policy_uri_t *info_uri, void *cb_data)
{
    const char *string_val = NULL;
    ma_policy_notification_type_t notify_type = MA_POLICY_NOTIFICATION_ALL;
    ma_uint32_t session_id = 0;

    // Verifying notification type
    TEST_ASSERT(MA_OK == ma_policy_uri_get_notification_type(info_uri, &notify_type));
    TEST_ASSERT(test_vector[counter].received_notification_type == notify_type);

    // Verify the software ID information in the received notification
    TEST_ASSERT(MA_OK == ma_policy_uri_get_software_id(info_uri, &string_val));
    TEST_ASSERT(0 == strcmp(test_vector[counter].test_software_id, string_val));

    // Following fields only need to be verified if the notification type is not MA_POLICY_TIMEOUT
    if(strcmp(test_vector[counter].publish_notification_type, "prop.value.notification_policy_timeout") != 0)
    {
        // Verify the version information
        TEST_ASSERT(MA_OK == ma_policy_uri_get_version(info_uri, &string_val));
        TEST_ASSERT(0 == strcmp("5.0.2.31", string_val));

        // Verify the User ID information in the received notification
        TEST_ASSERT(MA_OK == ma_policy_uri_get_user(info_uri, &string_val));
        TEST_ASSERT(0 == strcmp("Privileged User", string_val));

        // Verify the session ID information in the received notification
        TEST_ASSERT(MA_OK == ma_policy_uri_get_user_session_id(info_uri, &session_id));
        TEST_ASSERT(123 == session_id);

        // Verify the Location ID Information in the received notification
        TEST_ASSERT(MA_OK == ma_policy_uri_get_location_id(info_uri, &string_val));
        TEST_ASSERT(0 == strcmp("MIC", string_val));
    }

    // Verifying that the callback data is correctly sent to the policy client
    TEST_ASSERT(test_vector[counter].cb_data == *(ma_uint32_t*)cb_data);

    // Indicate that the policy client received notification
    notification_received_by_client = 1;

    return MA_OK;
}


/*****************************************************************************
TEST SCENARIOS -
1) The list contains the software IDs, none of which matches the current software
   ID. In this case, Policy changed notification should not be received, all
   others should be received.
2) The list contains the software IDs, one of which matches the current software
   ID. In this case, all kinds of notification should be received.
3) Unregister the notification, then publish the notification and verify that the
   notification is not received by client.
******************************************************************************/
TEST(policy_notification_tests, policy_notification_callback_test)
{
    for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        ma_message_t* publisher_payload = NULL;
        ma_table_t *table_ptr;
        ma_variant_t *table_variant = NULL;
        ma_variant_t *variant_ptr = NULL;
        ma_array_t *array_ptr = NULL;

		// ma_policy_notification_register_callback invalid test
        TEST_ASSERT(ma_policy_register_notification_callback(NULL, product_id, MA_POLICY_TIMEOUT, notification_callback_function, NULL) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_policy_register_notification_callback(g_ma_client, product_id, (ma_uint32_t)0x1111, notification_callback_function, NULL) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_policy_register_notification_callback(g_ma_client, product_id, MA_USER_CHANGED, NULL, NULL) == MA_ERROR_INVALIDARG);

        // Registering callback
        TEST_ASSERT(ma_policy_register_notification_callback(g_ma_client, test_vector[counter].test_software_id, test_vector[counter].register_notification_type,
                                                             notification_callback_function, (void *)&test_vector[counter].cb_data) == MA_OK);

        // Trying to register same callback again. MA_ERROR_POLICY_CALLBACK_ALREADY_REGISTERED should be returned.
        TEST_ASSERT(ma_policy_register_notification_callback(g_ma_client,
                                                             test_vector[counter].test_software_id,
                                                             test_vector[counter].register_notification_type,
                                                             notification_callback_function,
                                                             (void *)&test_vector[counter].cb_data) == MA_ERROR_POLICY_CALLBACK_ALREADY_REGISTERED);

        // TEST 1 ---------------------------------------------------------------------

        // Creating message for publishing
        TEST_ASSERT(ma_message_create(&publisher_payload) == MA_OK);

        // Setting the message properties
        TEST_ASSERT(ma_message_set_property(publisher_payload, "prop.key.policy_notification", test_vector[counter].publish_notification_type) == MA_OK);

        // Creating and Setting the message payload
        ma_table_create(&table_ptr);

        ma_variant_create_from_string("5.0.2.31", strlen("5.0.2.31"), &variant_ptr);
        ma_table_add_entry(table_ptr, "version", variant_ptr);
        ma_variant_release(variant_ptr);

        ma_variant_create_from_string("Privileged User", strlen("Privileged User"), &variant_ptr);
        ma_table_add_entry(table_ptr, "user_id", variant_ptr);
        ma_variant_release(variant_ptr);

        ma_variant_create_from_string("123", strlen("123"), &variant_ptr);
        ma_table_add_entry(table_ptr, "session_id", variant_ptr);
        ma_variant_release(variant_ptr);

        ma_variant_create_from_string("MIC", strlen("MIC"), &variant_ptr);
        ma_table_add_entry(table_ptr, "location_id", variant_ptr);
        ma_variant_release(variant_ptr);

        // Adding list of software IDs, none of them matches the software ID of current product
        ma_array_create(&array_ptr);

        ma_variant_create_from_string("MY PRODUCT ID 1", strlen("MY PRODUCT ID 1"), &variant_ptr);
        ma_array_push(array_ptr, variant_ptr);
        ma_variant_release(variant_ptr);

        ma_variant_create_from_string("MY PRODUCT ID 2", strlen("MY PRODUCT ID 2"), &variant_ptr);
        ma_array_push(array_ptr, variant_ptr);
        ma_variant_release(variant_ptr);

        ma_variant_create_from_string("MY PRODUCT ID 3", strlen("MY PRODUCT ID 3"), &variant_ptr);
        ma_array_push(array_ptr, variant_ptr);
        ma_variant_release(variant_ptr);

        ma_variant_create_from_string("MY PRODUCT ID 4", strlen("MY PRODUCT ID 4"), &variant_ptr);
        ma_array_push(array_ptr, variant_ptr);
        ma_variant_release(variant_ptr);

        ma_variant_create_from_array(array_ptr, &variant_ptr);
        ma_array_release(array_ptr);

        ma_table_add_entry(table_ptr, "product_list", variant_ptr);
        ma_variant_release(variant_ptr);

        ma_variant_create_from_table(table_ptr, &table_variant);
        ma_table_release(table_ptr);

        ma_message_set_payload(publisher_payload, table_variant);
        ma_variant_release(table_variant);

        // Only applicable for the MA_POLICY_CHANGED and MA_POLICY_TIMEOUT scenario. Verify that the policy changed
        // notification is not published if the Software ID doesn't match any ID in the list
        if((strcmp(test_vector[counter].publish_notification_type, "prop.value.notification_policy_changed") == 0) ||
           (strcmp(test_vector[counter].publish_notification_type, "prop.value.notification_policy_timeout") == 0)
           )
        {
            notification_received_by_client = 0;

            TEST_ASSERT(ma_msgbus_publish(g_msgbus, "ma.policy.notification", MSGBUS_CONSUMER_REACH_INPROC, publisher_payload) == MA_OK);

            #ifdef WIN32
                Sleep(1000);
            #else
                sleep(1);
            #endif

            // Notification should not be received.
            TEST_ASSERT(notification_received_by_client == 0);
        }
        else
        {
            notification_received_by_client = 0;

            TEST_ASSERT(ma_msgbus_publish(g_msgbus, "ma.policy.notification", MSGBUS_CONSUMER_REACH_INPROC, publisher_payload) == MA_OK);

            while(!notification_received_by_client)
            {
                #ifdef WIN32
                    Sleep(1000);
                #else
                    sleep(1);
                #endif
            }

            // Notification should be received.
            TEST_ASSERT(notification_received_by_client == 1);
        }

        ma_message_release(publisher_payload);

        // TEST 2 ---------------------------------------------------------------------

        // Creating message for publishing
        TEST_ASSERT(ma_message_create(&publisher_payload) == MA_OK);

        // Setting the message properties
        TEST_ASSERT(ma_message_set_property(publisher_payload, "prop.key.policy_notification", test_vector[counter].publish_notification_type) == MA_OK);

        // Creating and Setting the message payload
        ma_table_create(&table_ptr);

        ma_variant_create_from_string("5.0.2.31", strlen("5.0.2.31"), &variant_ptr);
        ma_table_add_entry(table_ptr, "version", variant_ptr);
        ma_variant_release(variant_ptr);

        ma_variant_create_from_string("Privileged User", strlen("Privileged User"), &variant_ptr);
        ma_table_add_entry(table_ptr, "user_id", variant_ptr);
        ma_variant_release(variant_ptr);

        ma_variant_create_from_string("123", strlen("123"), &variant_ptr);
        ma_table_add_entry(table_ptr, "session_id", variant_ptr);
        ma_variant_release(variant_ptr);

        ma_variant_create_from_string("User is sleeping", strlen("User is sleeping"), &variant_ptr);
        ma_table_add_entry(table_ptr, "logon_state", variant_ptr);
        ma_variant_release(variant_ptr);

        ma_variant_create_from_string("MIC", strlen("MIC"), &variant_ptr);
        ma_table_add_entry(table_ptr, "location_id", variant_ptr);
        ma_variant_release(variant_ptr);

        // Refreshing the Product ID List - Adding the software ID of current product
        ma_array_create(&array_ptr);

        ma_variant_create_from_string("MY PRODUCT ID 1", strlen("MY PRODUCT ID 1"), &variant_ptr);
        ma_array_push(array_ptr, variant_ptr);
        ma_variant_release(variant_ptr);

        ma_variant_create_from_string("MY PRODUCT ID 2", strlen("MY PRODUCT ID 2"), &variant_ptr);
        ma_array_push(array_ptr, variant_ptr);
        ma_variant_release(variant_ptr);

        ma_variant_create_from_string("MY PRODUCT ID 3", strlen("MY PRODUCT ID 3"), &variant_ptr);
        ma_array_push(array_ptr, variant_ptr);
        ma_variant_release(variant_ptr);

        ma_variant_create_from_string("MY PRODUCT ID 4", strlen("MY PRODUCT ID 4"), &variant_ptr);
        ma_array_push(array_ptr, variant_ptr);
        ma_variant_release(variant_ptr);

        ma_variant_create_from_string(test_vector[counter].test_software_id, strlen(test_vector[counter].test_software_id), &variant_ptr);
        ma_array_push(array_ptr, variant_ptr);
        ma_variant_release(variant_ptr);

        ma_variant_create_from_array(array_ptr, &variant_ptr);
        ma_array_release(array_ptr);

        ma_table_add_entry(table_ptr, "product_list", variant_ptr);
        ma_variant_release(variant_ptr);

        ma_variant_create_from_table(table_ptr, &table_variant);
        ma_table_release(table_ptr);

        ma_message_set_payload(publisher_payload, table_variant);
        ma_variant_release(table_variant);

        notification_received_by_client = 0;

        TEST_ASSERT(ma_msgbus_publish(g_msgbus, "ma.policy.notification", MSGBUS_CONSUMER_REACH_INPROC, publisher_payload) == MA_OK);

        while(!notification_received_by_client)
        {
            #ifdef WIN32
                Sleep(1000);
            #else
                sleep(1);
            #endif
        }

        TEST_ASSERT(notification_received_by_client == 1);

        // TEST 3 ---------------------------------------------------------------------

        // Unregister call-back and publish same message. Policy client should not receive it.
        TEST_ASSERT(ma_policy_unregister_notification_callback(NULL, test_vector[counter].test_software_id) ==  MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_policy_unregister_notification_callback(g_ma_client, test_vector[counter].test_software_id) ==  MA_OK);

        notification_received_by_client = 0;

        // Client should not be able to listen to this message.
        TEST_ASSERT(ma_msgbus_publish(g_msgbus, "ma.policy.notification", MSGBUS_CONSUMER_REACH_INPROC, publisher_payload) == MA_OK);

        #ifdef WIN32
            Sleep(1000);
        #else
            sleep(1);
        #endif

        TEST_ASSERT(notification_received_by_client == 0);

        ma_message_release(publisher_payload);
    }
}


