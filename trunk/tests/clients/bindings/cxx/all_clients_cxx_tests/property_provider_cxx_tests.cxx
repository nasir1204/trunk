#include "ma/properties/property_provider.hxx"
#include "gtest/gtest.h"

const static char PID[] =           "MAPROVIDER_4500";
const static char OTHER_PID[] =     "TRAILRUNNER_50k";


struct my_provider : mileaccess::ma::property_provider {

    my_provider(mileaccess::ma::client const &c) : mileaccess::ma::property_provider(c) {
        register_provider(PID);
        register_provider(OTHER_PID);
    }

    ~my_provider() {
        unregister_provider(OTHER_PID);
        unregister_provider(PID);
    }

    virtual ma_error_t on_collect_properties(mileaccess::ma::client const &cli, std::string const &product_id, mileaccess::ma::property_bag &properties) {
		properties.get_type();
        if (product_id == OTHER_PID) {
            properties.add_property("General", "FreeDiskSpace", mileaccess::ma::variant("243GB"));
            properties.add_property("Special", "Free Willy", mileaccess::ma::variant("True"));
        } else if (product_id == PID) {
            properties.add_property("General", "Engine Version", mileaccess::ma::variant("1230"));
        }
        return MA_OK;
    }
};

TEST(property_provider, quick_setup) {

    mileaccess::ma::client c(PID);
    c.start();
    my_provider prv(c);
    c.stop();
    // run baby, run
}
