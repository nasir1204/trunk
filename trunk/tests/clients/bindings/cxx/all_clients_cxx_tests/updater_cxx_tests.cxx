#include "ma/updater/updater_handler.hxx"
#include "ma/updater/updater.hxx"
#include "gtest/gtest.h"

using mileaccess::ma::client;

using namespace mileaccess::ma::updater;


struct my_update_handler : mileaccess::ma::updater::updater_handler{

    my_update_handler(client const &cli) : mileaccess::ma::updater::updater_handler(cli) {        
    }

    ~my_update_handler() {        
    }

    virtual ::ma_error_t notify_cb(client const &cli, std::string const &product_id,  std::string const &update_type, ::ma_update_state_t state, std::string const &message, std::string const& extra_info, ::ma_updater_product_return_code_t &return_code) {
        return_code = MA_UPDATER_PRODUCT_RETURN_CODE_OK;
        return MA_OK;
    }

    virtual ::ma_error_t set_cb(client const &cli, std::string const &product_id, std::string const &update_type, std::string const &key, mileaccess::ma::variant const &value, ::ma_updater_product_return_code_t &return_code)  {
        std::string str = mileaccess::ma::get<std::string>(value);
        return_code = MA_UPDATER_PRODUCT_RETURN_CODE_OK;        
        return MA_OK;
    }

    virtual ::ma_error_t get_cb(client const &cli, std::string const &product_id, std::string const &update_type, std::string const &key, mileaccess::ma::variant &value, ::ma_updater_product_return_code_t &return_code)  {
        value = mileaccess::ma::variant("Hell");
        return_code = MA_UPDATER_PRODUCT_RETURN_CODE_OK;
        return MA_OK;
    }
	virtual ::ma_error_t updater_event_cb(client const &cli, std::string const &product_id, const updater_event &upd_event){
		return MA_OK;
	}
};


TEST(cxx_update_handler, create ) {

    client c("dong");
    c.start();
    my_update_handler uh(c);
    c.stop();
}

TEST(cxx_update_handler, update ) {

    client c("dong");
    c.start();
    updater upd(c);
    update_request request(MA_UPDATER_UPDATE_TYPE_NORMAL_UPDATE);
    mileaccess::ma::buffer session_id;

    upd.start_update(request, session_id);
    upd.stop_update(session_id);
    
    c.stop();
}
