#include "ma/scheduler/task_handler.hxx"
#include "gtest/gtest.h"

using mileaccess::ma::client;
using mileaccess::ma::scheduler::task;

using namespace mileaccess::ma::scheduler;

struct my_task_handler : mileaccess::ma::scheduler::task_handler {

    my_task_handler(client const &cli) : mileaccess::ma::scheduler::task_handler(cli) {
        register_task_handler("abd","update");
        register_task_handler("dong","uninstall");
    }

    ~my_task_handler() {
        unregister_task_handler("dong","uninstall");
        unregister_task_handler("abd","update");
    }

    virtual ma_error_t on_execute(client const &cli, std::string const &product_id, std::string const &task_type, task &t) {
        trigger_list tl = t.get_trigger_list();

        for (size_t i = 0; i != tl.size(); ++i) {
            trigger t = tl.at(i);
        }

        return MA_OK;
    }

    virtual ma_error_t on_stop(client const &ctx, std::string const &product_id, std::string const &task_type, task &t) {
        return MA_OK;

    }

    virtual ma_error_t on_remove(client const &ctx, std::string const &product_id, std::string const &task_type, task &t) {
        return MA_OK;

    }

    virtual ma_error_t on_update_status(client const &ctx, std::string const &product_id, std::string const &task_type, task &t) {
        return MA_OK;

    }

};


TEST(cxx_task_handler, create ) {

    client c("dong");
    c.start();
    my_task_handler th(c);
    c.stop();
}
