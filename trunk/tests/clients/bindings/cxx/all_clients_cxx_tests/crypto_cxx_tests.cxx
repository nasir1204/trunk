#include "ma/crypto/crypto.hxx"

#include "gtest/gtest.h"

using mileaccess::ma::client;

using namespace mileaccess::ma::crypto;


TEST(cxx_update_handler, encrypt ) {

    client c("dong");
    unsigned char x[10] = {0};
    c.start();
    encrypt(c,MA_CRYPTO_ALGO_TYPE_POLICY_AES128_STR,x,10);
    c.stop();
}

