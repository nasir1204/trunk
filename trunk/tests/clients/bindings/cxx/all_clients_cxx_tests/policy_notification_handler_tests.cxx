#include "ma/policies/policy_notification_handler.hxx"
#include "ma/policies/policy_bag.hxx"
#include "ma/policies/policy_bag_iterator.hxx"
#include "gtest/gtest.h"

struct my_policy_notification_handler : mileaccess::ma::policies::policy_notification_handler {

    my_policy_notification_handler(mileaccess::ma::client const &cli) : mileaccess::ma::policies::policy_notification_handler(cli) {
        this->register_policy_notification_handler("booh");
    }

    ~my_policy_notification_handler() {
        this->unregister_policy_notification_handler("booh");
    }


    virtual ma_error_t on_policy_notification(mileaccess::ma::client const &cli, std::string const &product_id, mileaccess::ma::policies::policy_uri const &uri) {

        mileaccess::ma::policies::policy_bag pb(cli,uri);

        mileaccess::ma::variant v = pb.get_value(NULL, "some_section", "some_key");

        for (mileaccess::ma::policies::policy_bag_iterator it( pb ); it; ++it) {
            (*it).uri;
            (*it).section_name;
            (*it).key_name;
            (*it).value;
        }

        return MA_OK;
    }

};


TEST(cxx_policy_notification_handler, null_test) {

    mileaccess::ma::client c("bah");
    c.start();
    my_policy_notification_handler pnh(c);
    c.stop();
}
