#include "ma/datachannel/datachannel_handler.hxx"
#include "ma/datachannel/datachannel.hxx"
#include "gtest/gtest.h"

using mileaccess::ma::client;

using namespace mileaccess::ma::datachannel;


struct my_dc_handler : mileaccess::ma::datachannel::datachannel_handler {

    my_dc_handler(client const &cli) : mileaccess::ma::datachannel::datachannel_handler(cli) {
        register_datachannel_message_handler("abd");
        register_notification_handler("abd");
        register_datachannel_message_handler("def");
        register_notification_handler("def");
    }

    ~my_dc_handler() {
        unregister_datachannel_message_handler("abd");
        unregister_notification_handler("abd");
        unregister_datachannel_message_handler("def");
        unregister_notification_handler("def");
    }

     //! you must implement this method in your derived class and use the above registration API to get notifications
    virtual void on_datachannel_notification(client const &c, std::string const &product_id, std::string const &message_id, ::ma_int64_t correlation_id, ::ma_datachannel_item_notification_t status, ::ma_error_t reason) {
    }

    //! you must implement this method in your derived class and use the above registration API to get messages
    virtual void on_datachannel_message(client const &c, std::string const &product_id, std::string const &message_id, ::ma_int64_t correlation_id, item message)  {
    }
};


TEST(cxx_dc_handler, create ) {

    client c("dong");
    c.start();
    my_dc_handler uh(c);
    c.stop();
}

TEST(cxx_dc_handler, send ) {

    client c("dong");
    c.start();
    datachannel dc(c);
    mileaccess::ma::variant v;
    item msg("aaa", v);
    dc.async_send("adb",msg);
    c.stop();
}
