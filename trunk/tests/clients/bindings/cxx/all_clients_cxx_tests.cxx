#include "gtest/gtest.h"

#ifdef MA_WINDOWS
#include <windows.h>
#endif
#ifdef _MSC_VER
# include <crtdbg.h> /* */
# define MA_TRACKLEAKS _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF)
#else
# define MA_TRACKLEAKS ((void *)0)
#endif

GTEST_API_ int main(int argc, char **argv)
{
    MA_TRACKLEAKS;
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}