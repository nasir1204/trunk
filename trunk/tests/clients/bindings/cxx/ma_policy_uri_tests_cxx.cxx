#include "ma/policies/policy_uri.hxx"
#include "gtest/gtest.h"

TEST(policy_uri_tests, set_get_policy_information_tests)
{
    ma_policy_uri_t *my_uri = NULL; // Underlying C URI object

	mileaccess::ma::policies::policy_uri my_policy_uri(my_uri);

	std::string test_feature = "test_feature";
	std::string test_category = "test_category";
	std::string test_type = "test_type";
	std::string test_name = "test_name";

	printf("0\n");
	my_policy_uri.set_feature(test_feature);
	printf("1\n");
	my_policy_uri.set_category(test_category); 
	printf("2\n");
	my_policy_uri.set_type(test_type);
	printf("3\n");
	my_policy_uri.set_name(test_name);
	printf("4\n");

    /* Cloning Policy information */
	mileaccess::ma::policies::policy_uri new_policy_uri = my_policy_uri.clone();

    /* Verifying the policy information using get APIs (through cloned object) */
	ASSERT_STREQ(test_feature.data(), new_policy_uri.get_feature().data());
	ASSERT_STREQ(test_category.data(), new_policy_uri.get_category().data());
	ASSERT_STREQ(test_type.data(), new_policy_uri.get_type().data());
	ASSERT_STREQ(test_name.data(), new_policy_uri.get_name().data());
}

TEST(policy_uri_tests, uri_set_get_interface_tests)
{
    ma_policy_uri_t *my_uri = NULL; // Underlying C URI object

	mileaccess::ma::policies::policy_uri my_policy_uri(my_uri);

	std::string test_software_id = "No software ID";
	std::string test_version = "version information not available";
	std::string test_user = "admin user 1";
	std::string test_location_id = "Unkown location";

	my_policy_uri.set_software_id(test_software_id); 
	my_policy_uri.set_version(test_version); 
	my_policy_uri.set_user(test_user);
	my_policy_uri.set_location_id(test_location_id);
	
    /* Cloning Policy information */
	mileaccess::ma::policies::policy_uri new_policy_uri = my_policy_uri.clone();

    /* Verifying the policy information using get APIs (through cloned object) */
	ASSERT_STREQ(test_software_id.data(), new_policy_uri.get_software_id().data());
	ASSERT_STREQ(test_version.data(), new_policy_uri.get_version().data());
	ASSERT_STREQ(test_user.data(), new_policy_uri.get_user().data());
	ASSERT_STREQ(test_location_id.data(), new_policy_uri.get_location_id().data());
}


