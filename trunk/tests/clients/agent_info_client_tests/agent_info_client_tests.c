/*****************************************************************************
Agent Information Client Unit Tests.

Test Approach - This test creates a dummy IO Service which responds to the
agent information requests.
******************************************************************************/
#include "unity.h"
#include "unity_fixture.h"
#include "ma/info/ma_info.h"
#include "ma/info/ma_product_info.h"
#include "ma/info/ma_macro_info.h"
#include "ma/ma_message.h"
#include "ma_ut_setup.h"
#include <string.h>
#include <uv.h>

#ifdef MA_WINDOWS
    #include <Windows.h>
#else
    #include <unistd.h>
#endif

// Global variable declaration
static ma_client_t *g_ma_client = NULL;
static ma_msgbus_t *g_msgbus = NULL;
static ma_ut_setup_t *g_ut_setup  = NULL;
static ma_msgbus_server_t *server = NULL;

static ma_bool_t done = 0;
static uv_cond_t cond;
static uv_mutex_t mux;

#define PRODUCT_ID "ma.client.tests"

// Global variable indicating the current test in execution
static ma_uint8_t test_counter = 0;

// Global variable to indicate that the registered callback is activated
static ma_bool_t callback_activated = MA_FALSE;

static ma_error_t server_cb(ma_msgbus_server_t *server, ma_message_t *request_message, void *cb_data, ma_msgbus_client_request_t *c_request);

// Test vector for ma_info_get API test
struct LookupTable
{
    char *key;
    char *value;
};

// All these fields maynot be strings in the actual agent information package, but they are considered strings
// here, for functionality verification.
static struct LookupTable const test_vector[] =
{
    { "AgentMode",                   "1"                                                 },
    { "Version",                     "5.0.0"                                             },
    { "GUID",                        "59E2EC22-7C08-11E3-1071-000C296277EA"              },
    { "Locale",                      "English"                                           },
    { "LogLocation",                 "C:\\ProgramData\\McAfee\\Agent\\logs"              },
    { "InstallLocation",             "C:\\Program Files (x86)\\McAfee\\Common Framework" },
    { "CryptoMode",                  "0"                                                 },
    { "DataLocation",                "C:\\ProgramData\\McAfee\\Agent\\data"              },
    { "EpoServerList",               "ServerList_1"                                      },
    { "EpoPortList",                 "8443"                                              },
    { "EpoServerLast_Used",          "172.16.196.17"                                     },
    { "EpoPortLastUsed",             "8443"                                              },
    { "LastASCTime",                 "20140114115029"                                    },
    { "LastPolicyUpdateTime",        "20140114115030"                                    },
    { "TenantId",                    "cdklnfdWERSf12323S"                                },
    { "EpoVersion",                  "5.1.1 Build 378"                                   },
    { "ASCInterval",                 "60 minutes"                                        },
    { "PolicyEnforceInterval",       "50 minutes"                                        },
    { "EventsUploadInterval",        "3 minutes"                                         },
    { "IPAddress",                   "10.213.254.69"                                     },
    { "HostName",                    "My-Desktop"                                        }
};

// Sample License information
static const char license[] = ".ldj;weldcn s,lrhewodfhwencs.ncpoae8yw38euoi3hr3pou349554u4op5;tm/.mf0435q43u 5943jto45p tyo54ui5p4jt645opt6u459tj64ml;f43kjt54potm4t;4otji45ot45";

TEST_GROUP_RUNNER(ma_agent_info_client_test_group)
{
    do {RUN_TEST_CASE(ma_agent_info_client_test, ma_agent_and_product_info_test)} while (0);
#ifdef MA_WINDOWS
    do {RUN_TEST_CASE(ma_agent_info_client_test, ma_macro_info_test)} while (0);
#endif
}

/**************************************************************************
1) Creating the unit test context.
2) Retrieving underlying message bus instance.
3) Creating and starting message bus service (IO Service) using the
   message bus instance.
**************************************************************************/
TEST_SETUP(ma_agent_info_client_test)
{
    uv_mutex_init(&mux);
    uv_cond_init(&cond);

    ma_ut_setup_create("agent_info_client.info_client.tests", PRODUCT_ID, &g_ut_setup);
    g_ma_client = ma_ut_setup_get_client(g_ut_setup);
    g_msgbus = ma_ut_setup_get_msgbus(g_ut_setup);
    ma_msgbus_server_create(g_msgbus, "ma.service.io", &server);
    ma_msgbus_server_start(server, server_cb, NULL);
}

/**************************************************************************
1) Stopping and releasing the message bus service (IO service).
2) Releasing the context.
**************************************************************************/
TEST_TEAR_DOWN(ma_agent_info_client_test)
{
    ma_msgbus_server_stop(server);
    ma_msgbus_server_release(server);
    ma_ut_setup_release(g_ut_setup);

    uv_cond_destroy(&cond);
    uv_mutex_destroy(&mux);
}

static void wait_until_done()
{
    uv_mutex_lock(&mux);
    while (!done)
    {
        uv_cond_wait(&cond,&mux);
    }
    uv_mutex_unlock(&mux);
}

static void set_done()
{
    uv_mutex_lock(&mux);
    done = MA_TRUE;
    uv_cond_signal(&cond);
    uv_mutex_unlock(&mux);
}

ma_error_t info_events_callback(ma_client_t *ma_client, const char *product_id, const char *info_event, void *cb_data)
{
    ma_uint8_t counter = 0;

    // Setting global variable to indicate that the callback is activated.
    callback_activated = MA_TRUE;

    TEST_ASSERT(0 == strcmp(PRODUCT_ID, product_id));
    TEST_ASSERT(0 == strcmp("AgentStarted", info_event));

    for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        ma_variant_t *variant_ptr = NULL;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_info_get(NULL, test_vector[counter].key, &variant_ptr));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_info_get(ma_client, NULL, &variant_ptr));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_info_get(ma_client, test_vector[counter].key, NULL));
        TEST_ASSERT(MA_OK == ma_info_get(ma_client, test_vector[counter].key, &variant_ptr));

        {
            ma_buffer_t *my_buffer = NULL;
            const char *my_string = NULL;
            size_t my_string_size = 0;

            TEST_ASSERT(MA_OK == ma_variant_get_string_buffer(variant_ptr, &my_buffer));
            TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

            TEST_ASSERT(MA_OK == ma_buffer_get_string(my_buffer, &my_string, &my_string_size));
            TEST_ASSERT(0 == strcmp(test_vector[counter].value, my_string));
            TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
        }
    }

    set_done();

    return MA_OK;
}

/**************************************************************************
Callback function to respond to requests from Info client.

Test 3 - Respond with license information in response payload.
Test 4 - Respond with error stating that the license information is not
         found.
**************************************************************************/
ma_error_t server_cb(ma_msgbus_server_t *server, ma_message_t *request_message, void *cb_data, ma_msgbus_client_request_t *c_request)
{
    if(test_counter == 3)
    {
        const char *string_val = NULL;
        ma_message_t *response_payload = NULL;
        ma_variant_t *variant_ptr = NULL;

        // Verifying that the IO Request Type is correct in the incoming message properties
        TEST_ASSERT(MA_OK == ma_message_get_property(request_message, "prop.key.io_request", &string_val));
        TEST_ASSERT(0 == strcmp("prop.value.get_product_license_state", string_val));

        // Verifying that the Software ID is correct in the incoming message properties
        TEST_ASSERT(MA_OK == ma_message_get_property(request_message, "prop.key.product_id", &string_val));
        TEST_ASSERT(0 == strcmp("My Product 1", string_val));

        // Respond with MA_OK message and license state information as payload
        TEST_ASSERT(MA_OK == ma_message_create(&response_payload));
        TEST_ASSERT(MA_OK == ma_variant_create_from_string(license, strlen(license), &variant_ptr));
        TEST_ASSERT(MA_OK == ma_message_set_payload(response_payload, variant_ptr));
        TEST_ASSERT(MA_OK == ma_msgbus_server_client_request_post_result(c_request, MA_OK, response_payload));
        TEST_ASSERT(MA_OK == ma_message_release(response_payload));
        TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));
    }

    if(test_counter == 4)
    {
        const char *string_val = NULL;
        ma_message_t *response_payload = NULL;
        ma_variant_t *variant_ptr = NULL;

        // Verifying that the IO Request Type is correct in the incoming message properties
        TEST_ASSERT(MA_OK == ma_message_get_property(request_message, "prop.key.io_request", &string_val));
        TEST_ASSERT(0 == strcmp("prop.value.get_product_license_state", string_val));

        // Verifying that the Software ID is correct in the incoming message properties
        TEST_ASSERT(MA_OK == ma_message_get_property(request_message, "prop.key.product_id", &string_val));
        TEST_ASSERT(0 == strcmp("My Product 1", string_val));

        // Respond with MA_ERROR_OBJECTNOTFOUND message and indicating that the license state
        // information is not found.
        TEST_ASSERT(MA_OK == ma_message_create(&response_payload));
        TEST_ASSERT(MA_OK == ma_variant_create_from_string(license, strlen(license), &variant_ptr));
        TEST_ASSERT(MA_OK == ma_message_set_payload(response_payload, variant_ptr));
        TEST_ASSERT(MA_OK == ma_msgbus_server_client_request_post_result(c_request, MA_ERROR_OBJECTNOTFOUND, response_payload));
        TEST_ASSERT(MA_OK == ma_message_release(response_payload));
        TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));
    }

    return MA_OK;
}


TEST(ma_agent_info_client_test, ma_agent_and_product_info_test)
{
    /**************************************************************************
    Test 1 - Verifying that the agent info client callback is activated if
    registered.
    **************************************************************************/
    test_counter = 1;

    callback_activated = MA_FALSE;

    // Trying to unregister Info Client callback, even before it is registered
    TEST_ASSERT(MA_ERROR_CLIENT_NOT_FOUND == ma_info_events_unregister_callback(g_ma_client, PRODUCT_ID));

    // Registration for Info Client callback
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_info_events_register_callback(NULL, PRODUCT_ID, info_events_callback, NULL));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_info_events_register_callback(g_ma_client, PRODUCT_ID, NULL, NULL));
    TEST_ASSERT(MA_OK == ma_info_events_register_callback(g_ma_client, PRODUCT_ID, info_events_callback, NULL));

    // Registering for same Info Client callback again
    TEST_ASSERT(MA_ERROR_ALREADY_REGISTERED == ma_info_events_register_callback(g_ma_client, PRODUCT_ID, info_events_callback, NULL));

    // Publish a message for an Info Event
    {
        ma_message_t *my_payload = NULL;
        ma_variant_t *table_variant_ptr = NULL;
        ma_variant_t *variant_ptr = NULL;
        ma_table_t *my_table = NULL;

        ma_uint8_t counter = 0;

        // Payload containing Agent Information (table)
        TEST_ASSERT(MA_OK == ma_message_create(&my_payload));

        TEST_ASSERT(MA_OK == ma_message_set_property(my_payload, "ma_info.msg.porp.event_type", "AgentStarted"));

        TEST_ASSERT(MA_OK == ma_table_create(&my_table));

        for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
        {
            TEST_ASSERT(MA_OK == ma_variant_create_from_string(test_vector[counter].value, strlen(test_vector[counter].value), &variant_ptr));
            TEST_ASSERT(MA_OK == ma_table_add_entry(my_table, test_vector[counter].key, variant_ptr));
            TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr)); variant_ptr = NULL;
        }

        TEST_ASSERT(MA_OK == ma_variant_create_from_table(my_table, &table_variant_ptr));
        TEST_ASSERT(MA_OK == ma_table_release(my_table)); my_table = NULL;

        TEST_ASSERT(MA_OK == ma_message_set_payload(my_payload, table_variant_ptr));
        TEST_ASSERT(MA_OK == ma_msgbus_publish(g_msgbus, "ma_info.events.publish", MSGBUS_CONSUMER_REACH_INPROC, my_payload));

        wait_until_done();

        TEST_ASSERT(MA_OK == ma_message_release(my_payload));
        TEST_ASSERT(MA_OK == ma_variant_release(table_variant_ptr));
    }

    // Verifying that registered callback is activated.
    TEST_ASSERT(MA_TRUE == callback_activated);

    /**************************************************************************
    Test 2 - Verifying that the agent info client callback is not activated if
    it is not registered.
    **************************************************************************/
    test_counter = 2;

    callback_activated = MA_FALSE;

    // Un-registering the callback (which was registered in scenario 1)
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_info_events_unregister_callback(NULL, PRODUCT_ID));
    TEST_ASSERT(MA_OK == ma_info_events_unregister_callback(g_ma_client, PRODUCT_ID));

    // Un-registering the same callback again
    TEST_ASSERT(MA_ERROR_CLIENT_NOT_FOUND == ma_info_events_unregister_callback(g_ma_client, PRODUCT_ID));

    // Publish a message for an Info Event
    {
        ma_message_t *my_payload = NULL;
        ma_variant_t *table_variant_ptr = NULL;
        ma_variant_t *variant_ptr = NULL;
        ma_table_t *my_table = NULL;

        ma_uint8_t counter = 0;

        // Payload containing Agent Information (table)
        TEST_ASSERT(MA_OK == ma_message_create(&my_payload));

        TEST_ASSERT(MA_OK == ma_message_set_property(my_payload, "ma_info.msg.porp.event_type", "AgentStarted"));

        TEST_ASSERT(MA_OK == ma_table_create(&my_table));

        for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
        {
            TEST_ASSERT(MA_OK == ma_variant_create_from_string(test_vector[counter].value, strlen(test_vector[counter].value), &variant_ptr));
            TEST_ASSERT(MA_OK == ma_table_add_entry(my_table, test_vector[counter].key, variant_ptr));
            TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr)); variant_ptr = NULL;
        }

        TEST_ASSERT(MA_OK == ma_variant_create_from_table(my_table, &table_variant_ptr));
        TEST_ASSERT(MA_OK == ma_table_release(my_table)); my_table = NULL;

        TEST_ASSERT(MA_OK == ma_message_set_payload(my_payload, table_variant_ptr));
        TEST_ASSERT(MA_OK == ma_msgbus_publish(g_msgbus, "ma_info.events.publish", MSGBUS_CONSUMER_REACH_INPROC, my_payload));

        // Sleep for some time for the published message to go through. Couldn't wait on mutex here,
        // as it is expected that the callback will never get triggered.
        #ifdef WIN32
            Sleep(1000);
        #else
            sleep(1);
        #endif

        TEST_ASSERT(MA_OK == ma_message_release(my_payload));
        TEST_ASSERT(MA_OK == ma_variant_release(table_variant_ptr));
    }

    // Verifying that registered callback is not activated.
    TEST_ASSERT(MA_FALSE == callback_activated);

    /**************************************************************************
    Test 3 - Verifying the scenario where IO service returns the license
    information in the response payload.
    **************************************************************************/
    test_counter = 3;

    {
        ma_buffer_t *license_buffer = NULL;

        const char *my_string = NULL;
        size_t my_string_size = 0;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_product_info_get_license_state(NULL, "My Product 1", &license_buffer));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_product_info_get_license_state(g_ma_client, NULL, &license_buffer));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_product_info_get_license_state(g_ma_client, "My Product 1", NULL));
        TEST_ASSERT(MA_OK == ma_product_info_get_license_state(g_ma_client, "My Product 1", &license_buffer));

        TEST_ASSERT(MA_OK == ma_buffer_get_string(license_buffer, &my_string, &my_string_size));
        TEST_ASSERT(0 == strcmp(license, my_string));
        TEST_ASSERT(MA_OK == ma_buffer_release(license_buffer));
    }

    /**************************************************************************
    Test 4 - Verifying the scenario where the license information is not found.
    **************************************************************************/
    test_counter = 4;

    {
        ma_buffer_t *license_buffer = NULL;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_product_info_get_license_state(NULL, "My Product 1", &license_buffer));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_product_info_get_license_state(g_ma_client, NULL, &license_buffer));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_product_info_get_license_state(g_ma_client, "My Product 1", NULL));
        TEST_ASSERT(MA_ERROR_LICENSE_STATE_NOT_FOUND == ma_product_info_get_license_state(g_ma_client, "My Product 1", &license_buffer));
    }
}

#ifdef MA_WINDOWS

// Verified by comparing with the values obtained from getenv() API. All parameters could
// not be verified through this method.
TEST(ma_agent_info_client_test, ma_macro_info_test)
{
    ma_buffer_t *my_buffer = NULL;
    const char *my_string = NULL;
    size_t my_string_size = 0;

    TEST_ASSERT(MA_OK == ma_macro_info_get(MA_AGENT_MACRO_COMPUTERNAME, &my_buffer));
    TEST_ASSERT(MA_OK == ma_buffer_get_string(my_buffer, &my_string, &my_string_size));
    printf("%s \t %d\n", my_string, my_string_size);
    TEST_ASSERT(0 == strcmp(getenv("COMPUTERNAME"), my_string));

    TEST_ASSERT(MA_OK == ma_macro_info_get(MA_AGENT_MACRO_USERNAME, &my_buffer));
    TEST_ASSERT(MA_OK == ma_buffer_get_string(my_buffer, &my_string, &my_string_size));
    printf("%s \t %d\n", my_string, my_string_size);
    TEST_ASSERT(0 == strcmp(getenv("USERNAME"), my_string));

    TEST_ASSERT(MA_OK == ma_macro_info_get(MA_AGENT_MACRO_DOMAINNAME, &my_buffer));
    TEST_ASSERT(MA_OK == ma_buffer_get_string(my_buffer, &my_string, &my_string_size));
    printf("%s \t %d\n", my_string, my_string_size);

    TEST_ASSERT(MA_OK == ma_macro_info_get(MA_AGENT_MACRO_SYS_DRIVE, &my_buffer));
    TEST_ASSERT(MA_OK == ma_buffer_get_string(my_buffer, &my_string, &my_string_size));
    printf("%s \t %d\n", my_string, my_string_size);
    TEST_ASSERT(0 == strcmp(getenv("HOMEDRIVE"), my_string));

    TEST_ASSERT(MA_OK == ma_macro_info_get(MA_AGENT_MACRO_SYS_ROOT_DRIVE, &my_buffer));
    TEST_ASSERT(MA_OK == ma_buffer_get_string(my_buffer, &my_string, &my_string_size));
    printf("%s \t %d\n", my_string, my_string_size);
    TEST_ASSERT(0 == strcmp(getenv("SystemRoot"), my_string));

    TEST_ASSERT(MA_OK == ma_macro_info_get(MA_AGENT_MACRO_SYS_DIR, &my_buffer));
    TEST_ASSERT(MA_OK == ma_buffer_get_string(my_buffer, &my_string, &my_string_size));
    printf("%s \t %d\n", my_string, my_string_size);

    TEST_ASSERT(MA_OK == ma_macro_info_get(MA_AGENT_MACRO_TEMP_DIR, &my_buffer));
    TEST_ASSERT(MA_OK == ma_buffer_get_string(my_buffer, &my_string, &my_string_size));
    printf("%s \t %d\n", my_string, my_string_size);
    TEST_ASSERT(0 == strcmp(getenv("TEMP"), my_string));

    TEST_ASSERT(MA_OK == ma_macro_info_get(MA_AGENT_MACRO_PROG_FILES_DIR, &my_buffer));
    TEST_ASSERT(MA_OK == ma_buffer_get_string(my_buffer, &my_string, &my_string_size));
    printf("%s \t %d\n", my_string, my_string_size);

    TEST_ASSERT(MA_OK == ma_macro_info_get(MA_AGENT_MACRO_PROG_FILES_COMMON_DIR, &my_buffer));
    TEST_ASSERT(MA_OK == ma_buffer_get_string(my_buffer, &my_string, &my_string_size));
    printf("%s \t %d\n", my_string, my_string_size);

    TEST_ASSERT(MA_OK == ma_macro_info_get(MA_AGENT_MACRO_MY_DOCUMENTS, &my_buffer));
    TEST_ASSERT(MA_OK == ma_buffer_get_string(my_buffer, &my_string, &my_string_size));
    printf("%s \t %d\n", my_string, my_string_size);
}

#endif
