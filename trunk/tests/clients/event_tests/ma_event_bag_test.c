/*****************************************************************************
Event Client Unit Tests for Common Event Format
******************************************************************************/
#include "unity.h"
#include "unity_fixture.h"
#include "ma/event/ma_event_client.h"
#include "ma/ma_msgbus.h"
#include "ma_ut_setup.h"
#include <string.h>
#include <uv.h>

// Global objects
static ma_client_t *g_ma_client = NULL;
static ma_msgbus_t *g_msgbus = NULL;
static ma_ut_setup_t *g_ut_setup = NULL;

static ma_msgbus_server_t *server = NULL;

static const char service_name[] = "ma.service.event";
static const char product_id[] = "ma.event.client.test";

// Test Scenario counter
static ma_uint8_t test_scenario = 0;

// Mutex variables
static ma_uint8_t done1 = 0;
static ma_uint8_t done2 = 0;
static uv_cond_t cond;
static uv_mutex_t mux;

/******************************************************************************
Callback functions and support functions.
******************************************************************************/
static void wait_until_done_1()
{
    uv_mutex_lock(&mux);
    while (!done1)
    {
        uv_cond_wait(&cond,&mux);
    }

    uv_mutex_unlock(&mux);
}

static void set_done_1()
{
    uv_mutex_lock(&mux);
    done1 = 1;
    uv_cond_signal(&cond);
    uv_mutex_unlock(&mux);
}

static void wait_until_done_2()
{
    uv_mutex_lock(&mux);
    while (!done2)
    {
        uv_cond_wait(&cond,&mux);
    }

    uv_mutex_unlock(&mux);
}

static void set_done_2()
{
    uv_mutex_lock(&mux);
    done2 = 1;
    uv_cond_signal(&cond);
    uv_mutex_unlock(&mux);
}

// Server callback function
static ma_error_t respond_to_event_client(ma_msgbus_server_t *server, ma_message_t *request_payload, void *cb_data, ma_msgbus_client_request_t *c_request);

// Notification callback function
static ma_error_t receive_published_common_event(ma_client_t *ma_client, const char *event_product_id, ma_event_bag_t *event_bag, void *cb_data);

// Callbacks bundle for registration
static struct ma_event_callbacks_s callbacks_bundle = { receive_published_common_event, NULL };

/******************************************************************************
This test verifies following 4 test scenarios. Scenarios are controlled based on
the value of "test_scenario" variable.

Scenario 1 - All Fields are present in the Event Bag (Message)
Scenario 2 - Event Message Product Version, Product Family, Event Message Common
             fields and Event Common Fields are not present in the event message.
Scenario 3 - Event Message and individual Event - custom fields are not present.
Scenario 4 - Empty event bag - no events in event message.
******************************************************************************/
TEST_GROUP_RUNNER(ma_event_client_test_group)
{
    test_scenario = 1;
    do {RUN_TEST_CASE(ma_event_client_tests, ma_event_and_event_message_test)} while (0);

    test_scenario = 2;
    do {RUN_TEST_CASE(ma_event_client_tests, ma_event_and_event_message_test)} while (0);

    test_scenario = 3;
    do {RUN_TEST_CASE(ma_event_client_tests, ma_event_and_event_message_test)} while (0);

    test_scenario = 4;
    do {RUN_TEST_CASE(ma_event_client_tests, ma_event_and_event_message_test)} while (0);
}

/**************************************************************************
1) Creating the unit test context.
2) Retrieving underlying message bus instance.
3) Creating and starting message bus service (Event Service) using the
   message bus instance.
**************************************************************************/
TEST_SETUP(ma_event_client_tests)
{
    uv_mutex_init(&mux);
    uv_cond_init(&cond);

    ma_ut_setup_create("event_client.event_bag.tests", product_id, &g_ut_setup);
    g_ma_client = ma_ut_setup_get_client(g_ut_setup);
    g_msgbus = ma_ut_setup_get_msgbus(g_ut_setup);
    ma_msgbus_server_create(g_msgbus, service_name, &server);
    ma_msgbus_server_start(server, respond_to_event_client, NULL);
}

/**************************************************************************
1) Stopping and releasing the message bus service (Event Service).
**************************************************************************/
TEST_TEAR_DOWN(ma_event_client_tests)
{
    ma_msgbus_server_stop(server);
    ma_msgbus_server_release(server);
    ma_ut_setup_release(g_ut_setup);

    uv_cond_destroy(&cond);
    uv_mutex_destroy(&mux);
}

/*
Unit test performs following functionality -
1) Creates and sends an Event Message consisting of the below mentioned content on the
   message bus.
2) Creates a server to accept the message and the verifies that all fields are successfully
   parsed from the message.
3) Note that the server is configured for the OUTPROC communication, but this test, creates
   client and server within the same process (INRPROC) to verify the event client functionality.

<?xml version="1.0" encoding="UTF-8"?>
<EPOEvent>
    <MachineInfo>
        <MachineName>NEPTUNE</MachineName>
        <AgentGUID>{F7D9D032-BC18-407C-A21A-C24E49973F57}</AgentGUID>
        <IPAddress>192.168.1.103</IPAddress>
        <OSName>Windows 7</OSName>
        <UserName>Craig</UserName>
        <TimeZoneBias>480</TimeZoneBias>
    </MachineInfo>
    <SoftwareInfo ProductName="VirusScan Enterprise" ProductVersion="8.7.0" ProductFamily="TVD">
        <CommonFields>
            <Analyzer>VIRUSCAN8700</Analyzer>
            <AnalyzerName>VirusScan Enterprise</AnalyzerName>
            <AnalyzerVersion>8.7</AnalyzerVersion>
            <AnalyzerHostName>TEST_BOX</AnalyzerHostName>
            <AnalyzerDATVersion>5917.0000</AnalyzerDATVersion>
            <AnalyzerEngineVersion>5400.1158</AnalyzerEngineVersion>
        </CommonFields>
        <CustomFields target="MyProductCustomFields">
            <ProductFieldOne>Product_Field_Value_1</ProductFieldOne>
        </CustomFieldsFields>
        <Event>
            <EventID>1027</EventID>
            <Severity>4</Severity>
            <GMTTime>2010-05-14T16:35:04</GMTTime>
            <CommonFields>
                <AnalyzerDetectionMethod>OAS</AnalyzerDetectionMethod>
                <TargetFileName>C:\WINDOWS\SYSTEM32\NTOSKRNL.EXE</TargetFileName>
                <ThreatCategory>av.detect</ThreatCategory>
                <ThreatName>Generic!atr</ThreatName>
                <ThreatType>trojan</ThreatType>
                <ThreatActionTaken>deleted</ThreatActionTaken>
                <ThreatHandled>1</ThreatHandled>
            </CommonFields>
            <CustomFields target="MyProductEvents">
                <EventFieldFive>Value5</EventFieldFive>
                <EventFieldSix>Value6</EventFieldSix>
                <EventFieldSeven>Value7</EventFieldSeven>
                <EventFieldEight>Value8</EventFieldEight>
            </CustomFieldsFields>
        </Event>
        <Event>
            <EventID>2034</EventID>
            <Severity>3</Severity>
            <GMTTime>2013-05-15T16:09:07</GMTTime>
            <CommonFields>
                <AnalyzerDetectionMethod>XYZ</AnalyzerDetectionMethod>
                <TargetFileName>C:\WINDOWS\SYSTEM32\ABCDLL.EXE</TargetFileName>
                <ThreatCategory>av.detect</ThreatCategory>
                <ThreatName>Custom!atr</ThreatName>
                <ThreatType>virus</ThreatType>
                <ThreatActionTaken>quarantine</ThreatActionTaken>
                <ThreatHandled>0</ThreatHandled>
            </CommonFields>
            <CustomFields target="MyProductEvents">
                <EventFieldOne>Value1</EventFieldOne>
                <EventFieldTwo>Value2</EventFieldTwo>
                <EventFieldThree>Value3</EventFieldThree>
                <EventFieldFour>Value4</EventFieldFour>
            </CustomFieldsFields>
        </Event>
    </SoftwareInfo>
</EPOEvent>

*/


// Common and Custom fields in the above mentioned Event Message XML are represented
// in the structure below -
struct event_info
{
    char *field_name;
    char *field_value;
};

const struct event_info event_message_common_fields[] =
      {
          { "Analyzer",              "VIRUSCAN8700"         },
          { "AnalyzerName",          "VirusScan Enterprise" },
          { "AnalyzerVersion",       "8.7"                  },
          { "AnalyzerHostName",      "TEST_BOX"             },
          { "AnalyzerDATVersion",    "5917.0000"            },
          { "AnalyzerEngineVersion", "5400.1158"            },
      };

const struct event_info event_message_custom_fields[] =
      {
          { "ProductFieldOne",     "Product_Field_Value_1"  },
      };

const struct event_info event_1_common_fields[] =
      {
          { "AnalyzerDetectionMethod", "OAS" },
          { "TargetFileName",          "C:\\WINDOWS\\SYSTEM32\\NTOSKRNL.EXE" },
          { "ThreatCategory",          "av.detect" },
          { "ThreatName",              "Generic!atr" },
          { "ThreatType",              "trojan" },
          { "ThreatActionTaken",       "deleted" },
          { "ThreatHandled",           "1" },
      };

const struct event_info event_1_custom_fields[] =
      {
          { "EventFieldFive",  "Value5" },
          { "EventFieldSix",   "Value6" },
          { "EventFieldSeven", "Value7" },
          { "EventFieldEight", "Value8" },
      };

const struct event_info event_2_common_fields[] =
      {
          { "AnalyzerDetectionMethod", "XYZ" },
          { "TargetFileName",          "C:\\WINDOWS\\SYSTEM32\\ABCDLL.EXE" },
          { "ThreatCategory",          "av.detect" },
          { "ThreatName",              "Custom!atr" },
          { "ThreatType",              "virus" },
          { "ThreatActionTaken",       "quarantine" },
          { "ThreatHandled",           "0" },
      };

const struct event_info event_2_custom_fields[] =
      {
          { "EventFieldOne",   "Value1" },
          { "EventFieldTwo",   "Value2" },
          { "EventFieldThree", "Value3" },
          { "EventFieldFour",  "Value4" },
      };

/******************************************************************************
Call back function to be executed when the Event Service receives an event bag
(payload) from the Event Client. This callback function verifies the Event Bag
parameters against the values sent by the event client.
******************************************************************************/
static ma_error_t respond_to_event_client(ma_msgbus_server_t *server,
                                          ma_message_t *request_payload,
                                          void *cb_data,
                                          ma_msgbus_client_request_t *c_request)
{
    /**************************************************************************
    Object pointer declarations.
    **************************************************************************/

    // Top level table variant and its table pointer
    ma_variant_t *event_bag_variant_obj = NULL;
    ma_table_t *event_bag_variant_ptr = NULL;

    // Event table variant and its table pointer
    ma_variant_t *event_table_variant_obj = NULL;
    ma_table_t *event_table_variant_ptr = NULL;

    // 1st level table variant and its table pointer
    ma_variant_t *table_variant_obj = NULL;
    ma_table_t *table_variant_ptr = NULL;

    // Event Array variant
    ma_variant_t *array_variant_obj = NULL;
    ma_array_t *event_array_ptr = NULL;

    // Single field variant
    ma_variant_t *variant_obj = NULL;
    ma_vartype_t vartype_obj = MA_VARTYPE_NULL;
    ma_buffer_t *buffer = NULL;
    const char *string_val = NULL;
    size_t string_size = 0;

    // Loop iterator
    ma_uint8_t counter = 0;

    // To store the integer values retrieved from variant.
    ma_uint32_t event_field_value = 0UL;

	// Handling the message payload
    if(request_payload == NULL)
    {
        return MA_ERROR_UNEXPECTED;
    }

    /**************************************************************************
    Verifying request type
    **************************************************************************/
    {
        const char *string_val = NULL;
        TEST_ASSERT(ma_message_get_property(request_payload, "prop.key.msg_type", &string_val) == MA_OK);
        TEST_ASSERT(0 == strcmp("prop.value.req.post_event", string_val));
    }

    /**************************************************************************
    Extracting top-level table variant from the message
    **************************************************************************/
    TEST_ASSERT(ma_message_get_payload(request_payload, &event_bag_variant_obj) == MA_OK);
    TEST_ASSERT(ma_variant_get_type(event_bag_variant_obj, &vartype_obj) == MA_OK);
    TEST_ASSERT(vartype_obj == MA_VARTYPE_TABLE);
    TEST_ASSERT(ma_variant_get_table(event_bag_variant_obj, &event_bag_variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_release(event_bag_variant_obj) == MA_OK);

    /**************************************************************************
    Verifying Product Name, Product Version and Product Family values
    **************************************************************************/
    TEST_ASSERT(ma_table_get_value(event_bag_variant_ptr, "ProductName", &variant_obj) == MA_OK);
    TEST_ASSERT(ma_variant_get_string_buffer(variant_obj, &buffer) == MA_OK);
    TEST_ASSERT(ma_buffer_get_string(buffer, &string_val, &string_size) == MA_OK);
    TEST_ASSERT(0 == strcmp("VirusScan Enterprise", string_val));
    TEST_ASSERT(ma_buffer_release(buffer) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_obj) == MA_OK);

    if((test_scenario == 1) || (test_scenario == 3) ||  (test_scenario == 4))
    {
        TEST_ASSERT(ma_table_get_value(event_bag_variant_ptr, "ProductVersion", &variant_obj) == MA_OK);
        TEST_ASSERT(ma_variant_get_string_buffer(variant_obj, &buffer) == MA_OK);
        TEST_ASSERT(ma_buffer_get_string(buffer, &string_val, &string_size) == MA_OK);
        TEST_ASSERT(0 == strcmp("8.7.0", string_val));
        TEST_ASSERT(ma_buffer_release(buffer) == MA_OK);
        TEST_ASSERT(ma_variant_release(variant_obj) == MA_OK);

        TEST_ASSERT(ma_table_get_value(event_bag_variant_ptr, "ProductFamily", &variant_obj) == MA_OK);
        TEST_ASSERT(ma_variant_get_string_buffer(variant_obj, &buffer) == MA_OK);
        TEST_ASSERT(ma_buffer_get_string(buffer, &string_val, &string_size) == MA_OK);
        TEST_ASSERT(0 == strcmp("TVD", string_val));
        TEST_ASSERT(ma_buffer_release(buffer) == MA_OK);
        TEST_ASSERT(ma_variant_release(variant_obj) == MA_OK);
    }
    else
    {
        TEST_ASSERT(ma_table_get_value(event_bag_variant_ptr, "ProductVersion", &variant_obj) == MA_ERROR_OBJECTNOTFOUND);
        TEST_ASSERT(ma_table_get_value(event_bag_variant_ptr, "ProductFamily", &variant_obj) == MA_ERROR_OBJECTNOTFOUND);
    }

    /**************************************************************************
    Fetching Event Message Common Fields and verifying the values
    **************************************************************************/
    if((test_scenario == 1) || (test_scenario == 3) ||  (test_scenario == 4))
    {
        TEST_ASSERT(ma_table_get_value(event_bag_variant_ptr, "CommonFields", &table_variant_obj) == MA_OK);
        TEST_ASSERT(ma_variant_get_type(table_variant_obj, &vartype_obj) == MA_OK);
        TEST_ASSERT(vartype_obj == MA_VARTYPE_TABLE);
        TEST_ASSERT(ma_variant_get_table(table_variant_obj, &table_variant_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_release(table_variant_obj) == MA_OK);

        for(counter = 0; counter < sizeof(event_message_common_fields)/sizeof(event_message_common_fields[0]); counter++)
        {
            TEST_ASSERT(ma_table_get_value(table_variant_ptr, event_message_common_fields[counter].field_name, &variant_obj) == MA_OK);
            TEST_ASSERT(ma_variant_get_string_buffer(variant_obj, &buffer) == MA_OK);
            TEST_ASSERT(ma_buffer_get_string(buffer, &string_val, &string_size) == MA_OK);
            TEST_ASSERT(0 == strcmp(event_message_common_fields[counter].field_value, string_val));
            TEST_ASSERT(ma_buffer_release(buffer) == MA_OK);
            TEST_ASSERT(ma_variant_release(variant_obj) == MA_OK);
        }

        TEST_ASSERT(ma_table_release(table_variant_ptr) == MA_OK);
    }
    else
    {
        TEST_ASSERT(ma_table_get_value(event_bag_variant_ptr, "CommonFields", &table_variant_obj) == MA_ERROR_OBJECTNOTFOUND);
    }

    if((test_scenario == 1) || (test_scenario == 2) ||  (test_scenario == 4))
    {
        /**************************************************************************
        Fetching Event Message Custom Target value
        **************************************************************************/
        TEST_ASSERT(ma_table_get_value(event_bag_variant_ptr, "target", &variant_obj) == MA_OK);
        TEST_ASSERT(ma_variant_get_string_buffer(variant_obj, &buffer) == MA_OK);
        TEST_ASSERT(ma_buffer_get_string(buffer, &string_val, &string_size) == MA_OK);
        TEST_ASSERT(0 == strcmp("MyProductCustomFields", string_val));
        TEST_ASSERT(ma_buffer_release(buffer) == MA_OK);
        TEST_ASSERT(ma_variant_release(variant_obj) == MA_OK);

        /**************************************************************************
        Fetching Event Message Custom Fields and verifying the values
        **************************************************************************/
        TEST_ASSERT(ma_table_get_value(event_bag_variant_ptr, "CustomFields", &table_variant_obj) == MA_OK);
        TEST_ASSERT(ma_variant_get_type(table_variant_obj, &vartype_obj) == MA_OK);
        TEST_ASSERT(vartype_obj == MA_VARTYPE_TABLE);
        TEST_ASSERT(ma_variant_get_table(table_variant_obj, &table_variant_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_release(table_variant_obj) == MA_OK);

        for(counter = 0; counter < sizeof(event_message_custom_fields)/sizeof(event_message_custom_fields[0]); counter++)
        {
            TEST_ASSERT(ma_table_get_value(table_variant_ptr, event_message_custom_fields[counter].field_name, &variant_obj) == MA_OK);
            TEST_ASSERT(ma_variant_get_string_buffer(variant_obj, &buffer) == MA_OK);
            TEST_ASSERT(ma_buffer_get_string(buffer, &string_val, &string_size) == MA_OK);
            TEST_ASSERT(0 == strcmp(event_message_custom_fields[counter].field_value, string_val));
            TEST_ASSERT(ma_buffer_release(buffer) == MA_OK);
            TEST_ASSERT(ma_variant_release(variant_obj) == MA_OK);
        }

        TEST_ASSERT(ma_table_release(table_variant_ptr) == MA_OK);
    }
    else
    {
        TEST_ASSERT(ma_table_get_value(event_bag_variant_ptr, "target", &variant_obj) == MA_ERROR_OBJECTNOTFOUND);
        TEST_ASSERT(ma_table_get_value(event_bag_variant_ptr, "CustomFields", &variant_obj) == MA_ERROR_OBJECTNOTFOUND);
    }

    /**************************************************************************
    Fetching Events array from the Table and then releasing the Table
    **************************************************************************/
    TEST_ASSERT(ma_table_get_value(event_bag_variant_ptr, "Events", &array_variant_obj) == MA_OK);
    TEST_ASSERT(ma_variant_get_type(array_variant_obj, &vartype_obj) == MA_OK);
    TEST_ASSERT(vartype_obj == MA_VARTYPE_ARRAY);
    TEST_ASSERT(ma_variant_get_array(array_variant_obj, &event_array_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_release(array_variant_obj) == MA_OK);

    // Verifying that the event array contains two events sent from the client. In scenario 4,
    // there are no events in event bag, therefore, size of event array should be 0.
    if((test_scenario == 1) || (test_scenario == 2) ||  (test_scenario == 3))
    {
        size_t size = 0;
        TEST_ASSERT(ma_array_size(event_array_ptr, &size) == MA_OK);
        TEST_ASSERT(2 == size);
    }
    else
    {
        size_t size = 2;
        TEST_ASSERT(ma_array_size(event_array_ptr, &size) == MA_OK);
        TEST_ASSERT(0 == size);
    }

    // Last variant retrieved from the top-level table, so the table pointer can be released.
    TEST_ASSERT(ma_table_release(event_bag_variant_ptr) == MA_OK);

    /**************************************************************************
    Fetching Event 1 from the array location 1
    **************************************************************************/
    if((test_scenario == 1) || (test_scenario == 2) ||  (test_scenario == 3))
    {
        TEST_ASSERT(ma_array_get_element_at(event_array_ptr, 1, &event_table_variant_obj) == MA_OK);
        TEST_ASSERT(ma_variant_get_type(event_table_variant_obj, &vartype_obj) == MA_OK);
        TEST_ASSERT(vartype_obj == MA_VARTYPE_TABLE);
        TEST_ASSERT(ma_variant_get_table(event_table_variant_obj, &event_table_variant_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_release(event_table_variant_obj) == MA_OK);
    }
    else
    {
        TEST_ASSERT(ma_array_get_element_at(event_array_ptr, 1, &event_table_variant_obj) == MA_ERROR_INVALIDARG);
    }

    /**************************************************************************
    Verifying Event 1 - Event ID, Severity, GMT Time and Local Time fields
    **************************************************************************/
    if((test_scenario == 1) || (test_scenario == 2) ||  (test_scenario == 3))
    {
        TEST_ASSERT(ma_table_get_value(event_table_variant_ptr, "EventID", &variant_obj) == MA_OK);
        TEST_ASSERT(ma_variant_get_uint32(variant_obj, &event_field_value) == MA_OK);
        TEST_ASSERT(event_field_value == 1027);

        TEST_ASSERT(ma_table_get_value(event_table_variant_ptr, "Severity", &variant_obj) == MA_OK);
        TEST_ASSERT(ma_variant_get_uint32(variant_obj, &event_field_value) == MA_OK);
        TEST_ASSERT(event_field_value == 4);

        // GMT Time and Local Time are added by client code, therefore the exact values cannot
        // be verified. This test verifies that these two fields are not NULL
        TEST_ASSERT(ma_table_get_value(event_table_variant_ptr, "GMTTime", &variant_obj) == MA_OK);
        TEST_ASSERT(ma_variant_get_string_buffer(variant_obj, &buffer) == MA_OK);
        TEST_ASSERT(ma_buffer_get_string(buffer, &string_val, &string_size) == MA_OK);
        TEST_ASSERT_NOT_NULL(string_val);
        TEST_ASSERT(ma_buffer_release(buffer) == MA_OK);
        TEST_ASSERT(ma_variant_release(variant_obj) == MA_OK);

        TEST_ASSERT(ma_table_get_value(event_table_variant_ptr, "LocalTime", &variant_obj) == MA_OK);
        TEST_ASSERT(ma_variant_get_string_buffer(variant_obj, &buffer) == MA_OK);
        TEST_ASSERT(ma_buffer_get_string(buffer, &string_val, &string_size) == MA_OK);
        TEST_ASSERT_NOT_NULL(string_val);
        TEST_ASSERT(ma_buffer_release(buffer) == MA_OK);
        TEST_ASSERT(ma_variant_release(variant_obj) == MA_OK);
    }

    /**************************************************************************
    Fetching Event 1 Common Fields and verifying the values
    **************************************************************************/
    if((test_scenario == 1) || (test_scenario == 3))
    {
        TEST_ASSERT(ma_table_get_value(event_table_variant_ptr, "CommonFields", &table_variant_obj) == MA_OK);
        TEST_ASSERT(ma_variant_get_type(table_variant_obj, &vartype_obj) == MA_OK);
        TEST_ASSERT(vartype_obj == MA_VARTYPE_TABLE);
        TEST_ASSERT(ma_variant_get_table(table_variant_obj, &table_variant_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_release(table_variant_obj) == MA_OK);

        for(counter = 0; counter < sizeof(event_1_common_fields)/sizeof(event_1_common_fields[0]); counter++)
        {
            TEST_ASSERT(ma_table_get_value(table_variant_ptr, event_1_common_fields[counter].field_name, &variant_obj) == MA_OK);
            TEST_ASSERT(ma_variant_get_string_buffer(variant_obj, &buffer) == MA_OK);
            TEST_ASSERT(ma_buffer_get_string(buffer, &string_val, &string_size) == MA_OK);
            TEST_ASSERT(0 == strcmp(event_1_common_fields[counter].field_value, string_val));
            TEST_ASSERT(ma_buffer_release(buffer) == MA_OK);
            TEST_ASSERT(ma_variant_release(variant_obj) == MA_OK);
        }

        TEST_ASSERT(ma_table_release(table_variant_ptr) == MA_OK);
    }

    if((test_scenario == 1) || (test_scenario == 2))
    {
        /**************************************************************************
        Fetching Event 1 Custom Target value
        **************************************************************************/
        TEST_ASSERT(ma_table_get_value(event_table_variant_ptr, "target", &variant_obj) == MA_OK);
        TEST_ASSERT(ma_variant_get_string_buffer(variant_obj, &buffer) == MA_OK);
        TEST_ASSERT(ma_buffer_get_string(buffer, &string_val, &string_size) == MA_OK);
        TEST_ASSERT(0 == strcmp("MyProductEvents", string_val));
        TEST_ASSERT(ma_buffer_release(buffer) == MA_OK);
        TEST_ASSERT(ma_variant_release(variant_obj) == MA_OK);

        /**************************************************************************
        Fetching Event 1 Custom Fields and verifying the values
        **************************************************************************/
        TEST_ASSERT(ma_table_get_value(event_table_variant_ptr, "CustomFields", &table_variant_obj) == MA_OK);
        TEST_ASSERT(ma_variant_get_type(table_variant_obj, &vartype_obj) == MA_OK);
        TEST_ASSERT(vartype_obj == MA_VARTYPE_TABLE);
        TEST_ASSERT(ma_variant_get_table(table_variant_obj, &table_variant_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_release(table_variant_obj) == MA_OK);

        for(counter = 0; counter < sizeof(event_1_custom_fields)/sizeof(event_1_custom_fields[0]); counter++)
        {
            TEST_ASSERT(ma_table_get_value(table_variant_ptr, event_1_custom_fields[counter].field_name, &variant_obj) == MA_OK);
            TEST_ASSERT(ma_variant_get_string_buffer(variant_obj, &buffer) == MA_OK);
            TEST_ASSERT(ma_buffer_get_string(buffer, &string_val, &string_size) == MA_OK);
            TEST_ASSERT(0 == strcmp(event_1_custom_fields[counter].field_value, string_val));
            TEST_ASSERT(ma_buffer_release(buffer) == MA_OK);
            TEST_ASSERT(ma_variant_release(variant_obj) == MA_OK);
        }

        TEST_ASSERT(ma_table_release(table_variant_ptr) == MA_OK);
    }

    // Complete Event is parsed, so release the corresponding pointer now.
    if((test_scenario == 1) || (test_scenario == 2) || (test_scenario == 3))
    {
        TEST_ASSERT(ma_table_release(event_table_variant_ptr) == MA_OK);
    }

    /**************************************************************************
    Fetching Event 2 from the array location 2
    **************************************************************************/
    if((test_scenario == 1) || (test_scenario == 2) ||  (test_scenario == 3))
    {
        TEST_ASSERT(ma_array_get_element_at(event_array_ptr, 2, &event_table_variant_obj) == MA_OK);
        TEST_ASSERT(ma_variant_get_type(event_table_variant_obj, &vartype_obj) == MA_OK);
        TEST_ASSERT(vartype_obj == MA_VARTYPE_TABLE);
        TEST_ASSERT(ma_variant_get_table(event_table_variant_obj, &event_table_variant_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_release(event_table_variant_obj) == MA_OK);
    }
    else
    {
        TEST_ASSERT(ma_array_get_element_at(event_array_ptr, 2, &event_table_variant_obj) == MA_ERROR_INVALIDARG);
    }

    /**************************************************************************
    Verifying Event 2 - Event ID, Severity, GMT Time and Local Time fields
    **************************************************************************/
    if((test_scenario == 1) || (test_scenario == 2) ||  (test_scenario == 3))
    {
        TEST_ASSERT(ma_table_get_value(event_table_variant_ptr, "EventID", &variant_obj) == MA_OK);
        TEST_ASSERT(ma_variant_get_uint32(variant_obj, &event_field_value) == MA_OK);
        TEST_ASSERT(event_field_value == 2034);

        TEST_ASSERT(ma_table_get_value(event_table_variant_ptr, "Severity", &variant_obj) == MA_OK);
        TEST_ASSERT(ma_variant_get_uint32(variant_obj, &event_field_value) == MA_OK);
        TEST_ASSERT(event_field_value == 3);

        // GMT Time and Local Time are added by client code, therefore the exact values cannot
        // be verified. This test verifies that these two fields are not NULL
        TEST_ASSERT(ma_table_get_value(event_table_variant_ptr, "GMTTime", &variant_obj) == MA_OK);
        TEST_ASSERT(ma_variant_get_string_buffer(variant_obj, &buffer) == MA_OK);
        TEST_ASSERT(ma_buffer_get_string(buffer, &string_val, &string_size) == MA_OK);
        TEST_ASSERT_NOT_NULL(string_val);
        TEST_ASSERT(ma_buffer_release(buffer) == MA_OK);
        TEST_ASSERT(ma_variant_release(variant_obj) == MA_OK);

        TEST_ASSERT(ma_table_get_value(event_table_variant_ptr, "LocalTime", &variant_obj) == MA_OK);
        TEST_ASSERT(ma_variant_get_string_buffer(variant_obj, &buffer) == MA_OK);
        TEST_ASSERT(ma_buffer_get_string(buffer, &string_val, &string_size) == MA_OK);
        TEST_ASSERT_NOT_NULL(string_val);
        TEST_ASSERT(ma_buffer_release(buffer) == MA_OK);
        TEST_ASSERT(ma_variant_release(variant_obj) == MA_OK);
    }

    /**************************************************************************
    Fetching Event 2 Common Fields and verifying the values
    **************************************************************************/
    if((test_scenario == 1) || (test_scenario == 3))
    {
        TEST_ASSERT(ma_table_get_value(event_table_variant_ptr, "CommonFields", &table_variant_obj) == MA_OK);
        TEST_ASSERT(ma_variant_get_type(table_variant_obj, &vartype_obj) == MA_OK);
        TEST_ASSERT(vartype_obj == MA_VARTYPE_TABLE);
        TEST_ASSERT(ma_variant_get_table(table_variant_obj, &table_variant_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_release(table_variant_obj) == MA_OK);

        for(counter = 0; counter < sizeof(event_2_common_fields)/sizeof(event_2_common_fields[0]); counter++)
        {
            TEST_ASSERT(ma_table_get_value(table_variant_ptr, event_2_common_fields[counter].field_name, &variant_obj) == MA_OK);
            TEST_ASSERT(ma_variant_get_string_buffer(variant_obj, &buffer) == MA_OK);
            TEST_ASSERT(ma_buffer_get_string(buffer, &string_val, &string_size) == MA_OK);
            TEST_ASSERT(0 == strcmp(event_2_common_fields[counter].field_value, string_val));
            TEST_ASSERT(ma_buffer_release(buffer) == MA_OK);
            TEST_ASSERT(ma_variant_release(variant_obj) == MA_OK);
        }

        TEST_ASSERT(ma_table_release(table_variant_ptr) == MA_OK);
    }

    if((test_scenario == 1) || (test_scenario == 2))
    {
        /**************************************************************************
        Fetching Event 1 Custom Target value
        **************************************************************************/
        TEST_ASSERT(ma_table_get_value(event_table_variant_ptr, "target", &variant_obj) == MA_OK);
        TEST_ASSERT(ma_variant_get_string_buffer(variant_obj, &buffer) == MA_OK);
        TEST_ASSERT(ma_buffer_get_string(buffer, &string_val, &string_size) == MA_OK);
        TEST_ASSERT(0 == strcmp("MyProductEvents", string_val));
        TEST_ASSERT(ma_buffer_release(buffer) == MA_OK);
        TEST_ASSERT(ma_variant_release(variant_obj) == MA_OK);

        /**************************************************************************
        Fetching Event 2 Custom Fields and verifying the values
        **************************************************************************/
        TEST_ASSERT(ma_table_get_value(event_table_variant_ptr, "CustomFields", &table_variant_obj) == MA_OK);
        TEST_ASSERT(ma_variant_get_type(table_variant_obj, &vartype_obj) == MA_OK);
        TEST_ASSERT(vartype_obj == MA_VARTYPE_TABLE);
        TEST_ASSERT(ma_variant_get_table(table_variant_obj, &table_variant_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_release(table_variant_obj) == MA_OK);

        for(counter = 0; counter < sizeof(event_2_custom_fields)/sizeof(event_2_custom_fields[0]); counter++)
        {
            TEST_ASSERT(ma_table_get_value(table_variant_ptr, event_2_custom_fields[counter].field_name, &variant_obj) == MA_OK);
            TEST_ASSERT(ma_variant_get_string_buffer(variant_obj, &buffer) == MA_OK);
            TEST_ASSERT(ma_buffer_get_string(buffer, &string_val, &string_size) == MA_OK);
            TEST_ASSERT(0 == strcmp(event_2_custom_fields[counter].field_value, string_val));
            TEST_ASSERT(ma_buffer_release(buffer) == MA_OK);
            TEST_ASSERT(ma_variant_release(variant_obj) == MA_OK);
        }

        TEST_ASSERT(ma_table_release(table_variant_ptr) == MA_OK);
    }

    // Complete Event is parsed, so release the corresponding pointer now.
    if((test_scenario == 1) || (test_scenario == 2) || (test_scenario == 3))
    {
        TEST_ASSERT(ma_table_release(event_table_variant_ptr) == MA_OK);
    }

    // Release the event array. Events array will be generated in first 3 scenarios only
    if((test_scenario == 1) || (test_scenario == 2) || (test_scenario == 3))
    {
        TEST_ASSERT(ma_array_release(event_array_ptr) == MA_OK);
    }

    set_done_1();

    // In Thread_IO mode, the event client sends a 'send-and-forget' type request to
    // the event service, therefore no response is required.
    return MA_OK;
}


/******************************************************************************
The event client publishes the event bag, other then, sending it to event
service. Other clients can register for the event callback (below is a sample
callback function implementation), and they will get a notification whenever an
event is published.

This callback function verifies that the event bag is successfully sent to the
registered clients.
******************************************************************************/
static ma_error_t receive_published_common_event(ma_client_t *ma_client, const char *event_product_id, ma_event_bag_t *event_bag, void *cb_data)
{
    // Loop iterator
    ma_uint8_t counter = 0;

    ma_event_t *my_event = NULL;

    // Event Bag cannot be NULL
    TEST_ASSERT_NOT_NULL(event_bag);

    // Verifying Product ID
    TEST_ASSERT(0 == strcmp(product_id, event_product_id));

    /**************************************************************************
    Verifying Product Name, Product Version and Product Family values
    **************************************************************************/
    {
        const char *string_val = NULL;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_bag_get_product_name(NULL, &string_val));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_bag_get_product_name(event_bag, NULL));
        TEST_ASSERT(MA_OK == ma_event_bag_get_product_name(event_bag, &string_val));
        TEST_ASSERT(0 == strcmp("VirusScan Enterprise", string_val));
    }

    if((test_scenario == 1) || (test_scenario == 3) ||  (test_scenario == 4))
    {
        {
            const char *string_val = NULL;

            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_bag_get_product_version(NULL, &string_val));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_bag_get_product_version(event_bag, NULL));
            TEST_ASSERT(MA_OK == ma_event_bag_get_product_version(event_bag, &string_val));
            TEST_ASSERT(0 == strcmp("8.7.0", string_val));
        }

        {
            const char *string_val = NULL;

            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_bag_get_product_family(NULL, &string_val));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_bag_get_product_family(event_bag, NULL));
            TEST_ASSERT(MA_OK == ma_event_bag_get_product_family(event_bag, &string_val));
            TEST_ASSERT(0 == strcmp("TVD", string_val));
        }
    }
    else
    {
        const char *string_val = NULL;

        TEST_ASSERT(MA_ERROR_EVENT_SETTING_NOT_FOUND == ma_event_bag_get_product_version(event_bag, &string_val));
        TEST_ASSERT(MA_ERROR_EVENT_SETTING_NOT_FOUND == ma_event_bag_get_product_family(event_bag, &string_val));
        TEST_ASSERT_NULL(string_val);
    }

    /**************************************************************************
    Fetching Event Message Common Fields and Custom Fields from the Event Bag
    for verification
    **************************************************************************/
    if((test_scenario == 1) || (test_scenario == 3) ||  (test_scenario == 4))
    {
        for(counter = 0; counter < sizeof(event_message_common_fields)/sizeof(event_message_common_fields[0]); counter++)
        {
            ma_variant_t *variant_obj = NULL;
            ma_buffer_t *buffer = NULL;
            const char *string_val = NULL;
            size_t string_size = 0;

            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_bag_get_common_field(NULL, event_message_common_fields[counter].field_name, &variant_obj));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_bag_get_common_field(event_bag, NULL, &variant_obj));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_bag_get_common_field(event_bag, event_message_common_fields[counter].field_name, NULL));
            TEST_ASSERT(MA_OK == ma_event_bag_get_common_field(event_bag, event_message_common_fields[counter].field_name, &variant_obj));

            TEST_ASSERT(MA_OK == ma_variant_get_string_buffer(variant_obj, &buffer));
            TEST_ASSERT(MA_OK == ma_buffer_get_string(buffer, &string_val, &string_size));
            TEST_ASSERT(0 == strcmp(event_message_common_fields[counter].field_value, string_val));
            TEST_ASSERT(MA_OK == ma_buffer_release(buffer));
            TEST_ASSERT(MA_OK == ma_variant_release(variant_obj));
            variant_obj = NULL;
        }
    }
    else
    {
        ma_variant_t *variant_obj = NULL;

        TEST_ASSERT(MA_ERROR_EVENT_SETTING_NOT_FOUND == ma_event_bag_get_common_field(event_bag, event_message_common_fields[0].field_name, &variant_obj));
        TEST_ASSERT_NULL(variant_obj);
    }

    if((test_scenario == 1) || (test_scenario == 2) ||  (test_scenario == 4))
    {
        // Verifying Custom table name
        const char *string_val = NULL;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_bag_get_custom_fields_table(NULL, &string_val));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_bag_get_custom_fields_table(event_bag, NULL));
        TEST_ASSERT(MA_OK == ma_event_bag_get_custom_fields_table(event_bag, &string_val));
        TEST_ASSERT(0 == strcmp("MyProductCustomFields", string_val));

        // Verifying custom fields
        for(counter = 0; counter < sizeof(event_message_custom_fields)/sizeof(event_message_custom_fields[0]); counter++)
        {
            ma_variant_t *variant_obj = NULL;
            ma_buffer_t *buffer = NULL;
            const char *string_val = NULL;
            size_t string_size = 0;

            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_bag_get_custom_field(NULL, event_message_custom_fields[counter].field_name, &variant_obj));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_bag_get_custom_field(event_bag, NULL, &variant_obj));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_bag_get_custom_field(event_bag, event_message_custom_fields[counter].field_name, NULL));
            TEST_ASSERT(MA_OK == ma_event_bag_get_custom_field(event_bag, event_message_custom_fields[counter].field_name, &variant_obj));

            TEST_ASSERT(MA_OK == ma_variant_get_string_buffer(variant_obj, &buffer));
            TEST_ASSERT(MA_OK == ma_buffer_get_string(buffer, &string_val, &string_size));
            TEST_ASSERT(0 == strcmp(event_message_custom_fields[counter].field_value, string_val));
            TEST_ASSERT(MA_OK == ma_buffer_release(buffer));
            TEST_ASSERT(MA_OK == ma_variant_release(variant_obj));
            variant_obj = NULL;
        }
    }
    else
    {
        ma_variant_t *variant_obj = NULL;
        const char *string_val = NULL;

        TEST_ASSERT(MA_ERROR_EVENT_SETTING_NOT_FOUND == ma_event_bag_get_custom_fields_table(event_bag, &string_val));
        TEST_ASSERT_NULL(string_val);

        TEST_ASSERT(MA_ERROR_EVENT_SETTING_NOT_FOUND == ma_event_bag_get_custom_field(event_bag, event_message_custom_fields[0].field_name, &variant_obj));
        TEST_ASSERT_NULL(variant_obj);
    }

    /**************************************************************************
    Retrieving the count of events and verifying it
    **************************************************************************/
    // Verifying that two events are stored. In scenario 4, there are no events
    // in event bag, therefore, size of event array should be 0.
    if((test_scenario == 1) || (test_scenario == 2) ||  (test_scenario == 3))
    {
        size_t count = 0;
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_bag_get_event_count(NULL, &count));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_bag_get_event_count(event_bag, NULL));
        TEST_ASSERT(MA_OK == ma_event_bag_get_event_count(event_bag, &count));
        TEST_ASSERT(2 == count);
    }
    else
    {
        size_t count = 2;
        TEST_ASSERT(MA_OK == ma_event_bag_get_event_count(event_bag, &count));
        TEST_ASSERT(0 == count);
    }

    /**************************************************************************
    Fetching Event 1 from the Index 0
    **************************************************************************/
    if((test_scenario == 1) || (test_scenario == 2) ||  (test_scenario == 3))
    {
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_bag_get_event(NULL, 0, &my_event));
        TEST_ASSERT(MA_ERROR_OUTOFBOUNDS == ma_event_bag_get_event(event_bag, 2, &my_event));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_bag_get_event(event_bag, 0, NULL));
        TEST_ASSERT(MA_OK == ma_event_bag_get_event(event_bag, 0, &my_event));
    }
    else
    {
        // No elements in the table
        TEST_ASSERT(MA_ERROR_OUTOFBOUNDS == ma_event_bag_get_event(event_bag, 0, &my_event));
    }

    /**************************************************************************
    Verifying Event 1 - Event ID and Severity
    **************************************************************************/
    if((test_scenario == 1) || (test_scenario == 2) ||  (test_scenario == 3))
    {
        {
            ma_uint32_t int_val = 0;

            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_get_id(NULL, &int_val));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_get_id(my_event, NULL));
            TEST_ASSERT(MA_OK == ma_event_get_id(my_event, &int_val));
            TEST_ASSERT(1027 == int_val);
        }

        {
            ma_event_severity_t severity = MA_EVENT_SEVERITY_INFORMATIONAL;

            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_get_severity(NULL, &severity));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_get_severity(my_event, NULL));
            TEST_ASSERT(MA_OK == ma_event_get_severity(my_event, &severity));
            TEST_ASSERT(MA_EVENT_SEVERITY_CRITICAL == severity);
        }
    }

    /**************************************************************************
    Fetching Event 1 Common Fields and Custom Fields for verification
    **************************************************************************/
    if((test_scenario == 1) || (test_scenario == 3))
    {
        for(counter = 0; counter < sizeof(event_1_common_fields)/sizeof(event_1_common_fields[0]); counter++)
        {
            ma_variant_t *variant_obj = NULL;
            ma_buffer_t *buffer = NULL;
            const char *string_val = NULL;
            size_t string_size = 0;

            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_get_common_field(NULL, event_1_common_fields[counter].field_name, &variant_obj));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_get_common_field(my_event, NULL, &variant_obj));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_get_common_field(my_event, event_1_common_fields[counter].field_name, NULL));
            TEST_ASSERT(MA_OK == ma_event_get_common_field(my_event, event_1_common_fields[counter].field_name, &variant_obj));

            TEST_ASSERT(MA_OK == ma_variant_get_string_buffer(variant_obj, &buffer));
            TEST_ASSERT(MA_OK == ma_buffer_get_string(buffer, &string_val, &string_size));
            TEST_ASSERT(0 == strcmp(event_1_common_fields[counter].field_value, string_val));
            TEST_ASSERT(MA_OK == ma_buffer_release(buffer));
            TEST_ASSERT(MA_OK == ma_variant_release(variant_obj));
            variant_obj = NULL;
        }
    }

    if(test_scenario == 2)
    {
        ma_variant_t *variant_obj = NULL;

        TEST_ASSERT(MA_ERROR_EVENT_SETTING_NOT_FOUND == ma_event_get_common_field(my_event, event_1_common_fields[0].field_name, &variant_obj));
        TEST_ASSERT_NULL(variant_obj);
    }

    // No event present
    if(test_scenario == 4)
    {
        ma_variant_t *variant_obj = NULL;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_get_common_field(my_event, event_1_common_fields[0].field_name, &variant_obj));
        TEST_ASSERT_NULL(variant_obj);
    }

    if((test_scenario == 1) || (test_scenario == 2))
    {
        // Verifying Custom table name
        const char *string_val = NULL;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_get_custom_fields_table(NULL, &string_val));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_get_custom_fields_table(my_event, NULL));
        TEST_ASSERT(MA_OK == ma_event_get_custom_fields_table(my_event, &string_val));
        TEST_ASSERT(0 == strcmp("MyProductEvents", string_val));

        // Verifying custom fields
        for(counter = 0; counter < sizeof(event_1_custom_fields)/sizeof(event_1_custom_fields[0]); counter++)
        {
            ma_variant_t *variant_obj = NULL;
            ma_buffer_t *buffer = NULL;
            const char *string_val = NULL;
            size_t string_size = 0;

            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_get_custom_field(NULL, event_1_custom_fields[counter].field_name, &variant_obj));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_get_custom_field(my_event, NULL, &variant_obj));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_get_custom_field(my_event, event_1_custom_fields[counter].field_name, NULL));
            TEST_ASSERT(MA_OK == ma_event_get_custom_field(my_event, event_1_custom_fields[counter].field_name, &variant_obj));

            TEST_ASSERT(MA_OK == ma_variant_get_string_buffer(variant_obj, &buffer));
            TEST_ASSERT(MA_OK == ma_buffer_get_string(buffer, &string_val, &string_size));
            TEST_ASSERT(0 == strcmp(event_1_custom_fields[counter].field_value, string_val));
            TEST_ASSERT(MA_OK == ma_buffer_release(buffer));
            TEST_ASSERT(MA_OK == ma_variant_release(variant_obj));
            variant_obj = NULL;
        }
    }

    if(test_scenario == 3)
    {
        ma_variant_t *variant_obj = NULL;
        const char *string_val = NULL;

        TEST_ASSERT(MA_ERROR_EVENT_SETTING_NOT_FOUND == ma_event_get_custom_fields_table(my_event, &string_val));
        TEST_ASSERT_NULL(string_val);

        TEST_ASSERT(MA_ERROR_EVENT_SETTING_NOT_FOUND == ma_event_get_custom_field(my_event, event_1_custom_fields[0].field_name, &variant_obj));
        TEST_ASSERT_NULL(variant_obj);
    }

    // No event present
    if(test_scenario == 4)
    {
        ma_variant_t *variant_obj = NULL;
        const char *string_val = NULL;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_get_custom_fields_table(my_event, &string_val));
        TEST_ASSERT_NULL(string_val);

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_get_custom_field(my_event, event_1_custom_fields[0].field_name, &variant_obj));
        TEST_ASSERT_NULL(variant_obj);
    }

    // Event 1 is completely verified, therefore, release that object
    if((test_scenario == 1) || (test_scenario == 2) ||  (test_scenario == 3))
    {
        TEST_ASSERT(MA_OK == ma_event_release(my_event));
        my_event = NULL;
    }
    else
    {
        TEST_ASSERT_NULL(my_event);
    }

    /**************************************************************************
    Fetching Event 2 from the Index 1
    **************************************************************************/
    if((test_scenario == 1) || (test_scenario == 2) ||  (test_scenario == 3))
    {
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_bag_get_event(NULL, 1, &my_event));
        TEST_ASSERT(MA_ERROR_OUTOFBOUNDS == ma_event_bag_get_event(event_bag, 2, &my_event));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_bag_get_event(event_bag, 1, NULL));
        TEST_ASSERT(MA_OK == ma_event_bag_get_event(event_bag, 1, &my_event));
    }
    else
    {
        // No elements in the table
        TEST_ASSERT(MA_ERROR_OUTOFBOUNDS == ma_event_bag_get_event(event_bag, 0, &my_event));
    }

    /**************************************************************************
    Verifying Event 2 - Event ID and Severity
    **************************************************************************/
    if((test_scenario == 1) || (test_scenario == 2) ||  (test_scenario == 3))
    {
        {
            ma_uint32_t int_val = 0;

            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_get_id(NULL, &int_val));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_get_id(my_event, NULL));
            TEST_ASSERT(MA_OK == ma_event_get_id(my_event, &int_val));
            TEST_ASSERT(2034 == int_val);
        }

        {
            ma_event_severity_t severity = MA_EVENT_SEVERITY_INFORMATIONAL;

            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_get_severity(NULL, &severity));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_get_severity(my_event, NULL));
            TEST_ASSERT(MA_OK == ma_event_get_severity(my_event, &severity));
            TEST_ASSERT(MA_EVENT_SEVERITY_MAJOR == severity);
        }
    }

    /**************************************************************************
    Fetching Event 2 Common Fields and Custom Fields for verification
    **************************************************************************/
    if((test_scenario == 1) || (test_scenario == 3))
    {
        for(counter = 0; counter < sizeof(event_2_common_fields)/sizeof(event_2_common_fields[0]); counter++)
        {
            ma_variant_t *variant_obj = NULL;
            ma_buffer_t *buffer = NULL;
            const char *string_val = NULL;
            size_t string_size = 0;

            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_get_common_field(NULL, event_2_common_fields[counter].field_name, &variant_obj));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_get_common_field(my_event, NULL, &variant_obj));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_get_common_field(my_event, event_2_common_fields[counter].field_name, NULL));
            TEST_ASSERT(MA_OK == ma_event_get_common_field(my_event, event_2_common_fields[counter].field_name, &variant_obj));

            TEST_ASSERT(MA_OK == ma_variant_get_string_buffer(variant_obj, &buffer));
            TEST_ASSERT(MA_OK == ma_buffer_get_string(buffer, &string_val, &string_size));
            TEST_ASSERT(0 == strcmp(event_2_common_fields[counter].field_value, string_val));
            TEST_ASSERT(MA_OK == ma_buffer_release(buffer));
            TEST_ASSERT(MA_OK == ma_variant_release(variant_obj));
            variant_obj = NULL;
        }
    }

    if(test_scenario == 2)
    {
        ma_variant_t *variant_obj = NULL;

        TEST_ASSERT(MA_ERROR_EVENT_SETTING_NOT_FOUND == ma_event_get_common_field(my_event, event_2_common_fields[0].field_name, &variant_obj));
        TEST_ASSERT_NULL(variant_obj);
    }

    // No event present
    if(test_scenario == 4)
    {
        ma_variant_t *variant_obj = NULL;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_get_common_field(my_event, event_2_common_fields[0].field_name, &variant_obj));
        TEST_ASSERT_NULL(variant_obj);
    }

    if((test_scenario == 1) || (test_scenario == 2))
    {
        // Verifying Custom table name
        const char *string_val = NULL;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_get_custom_fields_table(NULL, &string_val));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_get_custom_fields_table(my_event, NULL));
        TEST_ASSERT(MA_OK == ma_event_get_custom_fields_table(my_event, &string_val));
        TEST_ASSERT(0 == strcmp("MyProductEvents", string_val));

        // Verifying custom fields
        for(counter = 0; counter < sizeof(event_2_custom_fields)/sizeof(event_2_custom_fields[0]); counter++)
        {
            ma_variant_t *variant_obj = NULL;
            ma_buffer_t *buffer = NULL;
            const char *string_val = NULL;
            size_t string_size = 0;

            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_get_custom_field(NULL, event_2_custom_fields[counter].field_name, &variant_obj));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_get_custom_field(my_event, NULL, &variant_obj));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_get_custom_field(my_event, event_2_custom_fields[counter].field_name, NULL));
            TEST_ASSERT(MA_OK == ma_event_get_custom_field(my_event, event_2_custom_fields[counter].field_name, &variant_obj));

            TEST_ASSERT(MA_OK == ma_variant_get_string_buffer(variant_obj, &buffer));
            TEST_ASSERT(MA_OK == ma_buffer_get_string(buffer, &string_val, &string_size));
            TEST_ASSERT(0 == strcmp(event_2_custom_fields[counter].field_value, string_val));
            TEST_ASSERT(MA_OK == ma_buffer_release(buffer));
            TEST_ASSERT(MA_OK == ma_variant_release(variant_obj));
            variant_obj = NULL;
        }
    }

    if(test_scenario == 3)
    {
        ma_variant_t *variant_obj = NULL;
        const char *string_val = NULL;

        TEST_ASSERT(MA_ERROR_EVENT_SETTING_NOT_FOUND == ma_event_get_custom_fields_table(my_event, &string_val));
        TEST_ASSERT_NULL(string_val);

        TEST_ASSERT(MA_ERROR_EVENT_SETTING_NOT_FOUND == ma_event_get_custom_field(my_event, event_2_custom_fields[0].field_name, &variant_obj));
        TEST_ASSERT_NULL(variant_obj);
    }

    // No event present
    if(test_scenario == 4)
    {
        ma_variant_t *variant_obj = NULL;
        const char *string_val = NULL;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_get_custom_fields_table(my_event, &string_val));
        TEST_ASSERT_NULL(string_val);

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_get_custom_field(my_event, event_2_custom_fields[0].field_name, &variant_obj));
        TEST_ASSERT_NULL(variant_obj);
    }

    // Event 2 is completely verified, therefore, release that object
    if((test_scenario == 1) || (test_scenario == 2) ||  (test_scenario == 3))
    {
        TEST_ASSERT(MA_OK == ma_event_release(my_event));
        my_event = NULL;
    }
    else
    {
        TEST_ASSERT_NULL(my_event);
    }

    set_done_2();

    // In Thread_IO mode, the event client sends a 'send-and-forget' type request to
    // the event service, therefore no response is required.
    return MA_OK;
}


TEST(ma_event_client_tests, ma_event_and_event_message_test)
{
    // Object pointers
    ma_event_bag_t *event_bag_obj = NULL;
    ma_variant_t *variant_obj = NULL;
    ma_event_t *event_obj = NULL;

    // Loop iterator
    ma_uint8_t counter = 0;

    /**************************************************************************
    Creating event message
    **************************************************************************/
    TEST_ASSERT(ma_event_bag_create(NULL, "VirusScan Enterprise", &event_bag_obj) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_event_bag_create(g_ma_client, NULL, &event_bag_obj) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_event_bag_create(g_ma_client, "VirusScan Enterprise", NULL) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_event_bag_create(g_ma_client, "VirusScan Enterprise", &event_bag_obj) == MA_OK);

    /**************************************************************************
    Setting product version
    **************************************************************************/
    if(test_scenario != 2)
    {
        TEST_ASSERT(ma_event_bag_set_product_version(NULL, "8.7.0") == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_event_bag_set_product_version(event_bag_obj, NULL) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_event_bag_set_product_version(event_bag_obj, "8.7.0") == MA_OK);
    }

    /**************************************************************************
    Setting product family
    **************************************************************************/
    if(test_scenario != 2)
    {
        TEST_ASSERT(ma_event_bag_set_product_family(NULL, "TVD") == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_event_bag_set_product_family(event_bag_obj, NULL) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_event_bag_set_product_family(event_bag_obj, "TVD") == MA_OK);
    }

    /**************************************************************************
     Adding Event Message Common Fields
    **************************************************************************/
    if(test_scenario != 2)
    {
        for(counter = 0; counter < sizeof(event_message_common_fields)/sizeof(event_message_common_fields[0]); counter++)
        {
            ma_variant_create_from_string(event_message_common_fields[counter].field_value,
                                          strlen(event_message_common_fields[counter].field_value),
                                          &variant_obj);

            TEST_ASSERT(ma_event_bag_add_common_field(NULL, event_message_common_fields[counter].field_name, variant_obj) == MA_ERROR_INVALIDARG);
            TEST_ASSERT(ma_event_bag_add_common_field(event_bag_obj, NULL, variant_obj) == MA_ERROR_INVALIDARG);
            TEST_ASSERT(ma_event_bag_add_common_field(event_bag_obj, event_message_common_fields[counter].field_name, NULL) == MA_ERROR_INVALIDARG);
            TEST_ASSERT(ma_event_bag_add_common_field(event_bag_obj, event_message_common_fields[counter].field_name, variant_obj) == MA_OK);

            ma_variant_release(variant_obj);
        }

        // Trying to add an event message common field of non-string type
        ma_variant_create_from_uint8(0x12U, &variant_obj);
        TEST_ASSERT(ma_event_bag_add_common_field(event_bag_obj, "NonStringField", variant_obj) == MA_ERROR_EVENT_INVALID_VALUE_TYPE);
        ma_variant_release(variant_obj);
    }

    if(test_scenario != 3)
    {
        /**************************************************************************
         Adding Event Message Custom Fields Table
        **************************************************************************/
        TEST_ASSERT(ma_event_bag_set_custom_fields_table(event_bag_obj, NULL) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_event_bag_set_custom_fields_table(NULL, "MyProductCustomFields") == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_event_bag_set_custom_fields_table(event_bag_obj, "MyProductCustomFields") == MA_OK);

        /**************************************************************************
         Adding Event Message Custom Fields
        **************************************************************************/
        for(counter = 0; counter < sizeof(event_message_custom_fields)/sizeof(event_message_custom_fields[0]); counter++)
        {
            ma_variant_create_from_string(event_message_custom_fields[counter].field_value,
                                          strlen(event_message_custom_fields[counter].field_value),
                                          &variant_obj);

            TEST_ASSERT(ma_event_bag_add_custom_field(event_bag_obj, event_message_custom_fields[counter].field_name, NULL) == MA_ERROR_INVALIDARG);
            TEST_ASSERT(ma_event_bag_add_custom_field(event_bag_obj, NULL, variant_obj) == MA_ERROR_INVALIDARG);
            TEST_ASSERT(ma_event_bag_add_custom_field(NULL, event_message_custom_fields[counter].field_name, variant_obj) == MA_ERROR_INVALIDARG);
            TEST_ASSERT(ma_event_bag_add_custom_field(event_bag_obj, event_message_custom_fields[counter].field_name, variant_obj) == MA_OK);

            ma_variant_release(variant_obj);
        }

        // Trying to add an event message custom field of non-string type
        ma_variant_create_from_uint8(0x12U, &variant_obj);
        TEST_ASSERT(ma_event_bag_add_custom_field(event_bag_obj, "NonStringField", variant_obj) == MA_ERROR_EVENT_INVALID_VALUE_TYPE);
        ma_variant_release(variant_obj);
    }

    /**************************************************************************
     Creating Event 1
    **************************************************************************/
    if(test_scenario != 4)
    {
        TEST_ASSERT(ma_event_create(NULL, 1027, MA_EVENT_SEVERITY_CRITICAL, &event_obj) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_event_create(g_ma_client, 0, MA_EVENT_SEVERITY_CRITICAL, &event_obj) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_event_create(g_ma_client, 1027, MA_EVENT_SEVERITY_CRITICAL, NULL) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_event_create(g_ma_client, 1027, MA_EVENT_SEVERITY_CRITICAL, &event_obj) == MA_OK);

        /**************************************************************************
         Adding common fields for Event 1
        **************************************************************************/
        if((test_scenario == 1) || (test_scenario == 3))
        {
            for(counter = 0; counter < sizeof(event_1_common_fields)/sizeof(event_1_common_fields[0]); counter++)
            {
                ma_variant_create_from_string(event_1_common_fields[counter].field_value,
                                              strlen(event_1_common_fields[counter].field_value),
                                              &variant_obj);

                TEST_ASSERT(ma_event_add_common_field(NULL, event_1_common_fields[counter].field_name, variant_obj) == MA_ERROR_INVALIDARG);
                TEST_ASSERT(ma_event_add_common_field(event_obj, NULL, variant_obj) == MA_ERROR_INVALIDARG);
                TEST_ASSERT(ma_event_add_common_field(event_obj, event_1_common_fields[counter].field_name, NULL) == MA_ERROR_INVALIDARG);
                TEST_ASSERT(ma_event_add_common_field(event_obj, event_1_common_fields[counter].field_name, variant_obj) == MA_OK);

                ma_variant_release(variant_obj);
            }

            // Trying to add an event common field of non-string type
            ma_variant_create_from_uint8(0x12U, &variant_obj);
            TEST_ASSERT(ma_event_add_common_field(event_obj, "NonStringField", variant_obj) == MA_ERROR_EVENT_INVALID_VALUE_TYPE);
            ma_variant_release(variant_obj);
        }

        if((test_scenario == 1) || (test_scenario == 2))
        {
            /**************************************************************************
             Adding Event 1 - Custom Fields Table
            **************************************************************************/
            TEST_ASSERT(ma_event_set_custom_fields_table(event_obj, NULL) == MA_ERROR_INVALIDARG);
            TEST_ASSERT(ma_event_set_custom_fields_table(NULL, "MyProductEvents") == MA_ERROR_INVALIDARG);
            TEST_ASSERT(ma_event_set_custom_fields_table(event_obj, "MyProductEvents") == MA_OK);

            /**************************************************************************
             Adding Event 1 - Custom Fields
            **************************************************************************/
            for(counter = 0; counter < sizeof(event_1_custom_fields)/sizeof(event_1_custom_fields[0]); counter++)
            {
                ma_variant_create_from_string(event_1_custom_fields[counter].field_value,
                                              strlen(event_1_custom_fields[counter].field_value),
                                              &variant_obj);

                TEST_ASSERT(ma_event_add_custom_field(NULL, event_1_custom_fields[counter].field_name, variant_obj) == MA_ERROR_INVALIDARG);
                TEST_ASSERT(ma_event_add_custom_field(event_obj, NULL, variant_obj) == MA_ERROR_INVALIDARG);
                TEST_ASSERT(ma_event_add_custom_field(event_obj, event_1_custom_fields[counter].field_name, NULL) == MA_ERROR_INVALIDARG);
                TEST_ASSERT(ma_event_add_custom_field(event_obj, event_1_custom_fields[counter].field_name, variant_obj) == MA_OK);

                ma_variant_release(variant_obj);
            }

            // Trying to add an event common field of non-string type
            ma_variant_create_from_bool(MA_TRUE, &variant_obj);
            TEST_ASSERT(ma_event_add_custom_field(event_obj, "NonStringField", variant_obj) == MA_ERROR_EVENT_INVALID_VALUE_TYPE);
            ma_variant_release(variant_obj);
        }

        /**************************************************************************
         Adding Event 1 to message
        **************************************************************************/
        TEST_ASSERT(ma_event_bag_add_event(event_bag_obj, NULL) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_event_bag_add_event(NULL, event_obj) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_event_bag_add_event(event_bag_obj, event_obj) == MA_OK);

        /**************************************************************************
         Releasing Event 1 object after adding it to event message
        **************************************************************************/
        TEST_ASSERT(ma_event_release(NULL) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_event_release(event_obj) == MA_OK);
    }

    /**************************************************************************
     Creating Event 2
    **************************************************************************/
    if(test_scenario != 4)
    {
        TEST_ASSERT(ma_event_create(NULL, 2034, MA_EVENT_SEVERITY_MAJOR, &event_obj) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_event_create(g_ma_client, 0, MA_EVENT_SEVERITY_MAJOR, &event_obj) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_event_create(g_ma_client, 2034, MA_EVENT_SEVERITY_MAJOR, NULL) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_event_create(g_ma_client, 2034, MA_EVENT_SEVERITY_MAJOR, &event_obj) == MA_OK);

        /**************************************************************************
         Adding common fields for Event 2
        **************************************************************************/
        if((test_scenario == 1) || (test_scenario == 3))
        {
            for(counter = 0; counter < sizeof(event_2_common_fields)/sizeof(event_2_common_fields[0]); counter++)
            {
                ma_variant_create_from_string(event_2_common_fields[counter].field_value,
                                              strlen(event_2_common_fields[counter].field_value),
                                              &variant_obj);

                TEST_ASSERT(ma_event_add_common_field(NULL, event_2_common_fields[counter].field_name, variant_obj) == MA_ERROR_INVALIDARG);
                TEST_ASSERT(ma_event_add_common_field(event_obj, NULL, variant_obj) == MA_ERROR_INVALIDARG);
                TEST_ASSERT(ma_event_add_common_field(event_obj, event_2_common_fields[counter].field_name, NULL) == MA_ERROR_INVALIDARG);
                TEST_ASSERT(ma_event_add_common_field(event_obj, event_2_common_fields[counter].field_name, variant_obj) == MA_OK);

                ma_variant_release(variant_obj);
            }

            // Trying to add an event common field of non-string type
            ma_variant_create_from_int16(0xFAb5, &variant_obj);
            TEST_ASSERT(ma_event_add_common_field(event_obj, "NonStringField", variant_obj) == MA_ERROR_EVENT_INVALID_VALUE_TYPE);
            ma_variant_release(variant_obj);
        }

        if((test_scenario == 1) || (test_scenario == 2))
        {
            /**************************************************************************
             Adding Event 2 - Custom Fields Table
            **************************************************************************/
            TEST_ASSERT(ma_event_set_custom_fields_table(event_obj, NULL) == MA_ERROR_INVALIDARG);
            TEST_ASSERT(ma_event_set_custom_fields_table(NULL, "MyProductEvents") == MA_ERROR_INVALIDARG);
            TEST_ASSERT(ma_event_set_custom_fields_table(event_obj, "MyProductEvents") == MA_OK);

            /**************************************************************************
             Adding Event 2 - Custom Fields
            **************************************************************************/
            for(counter = 0; counter < sizeof(event_2_custom_fields)/sizeof(event_2_custom_fields[0]); counter++)
            {
                ma_variant_create_from_string(event_2_custom_fields[counter].field_value,
                                              strlen(event_2_custom_fields[counter].field_value),
                                              &variant_obj);

                TEST_ASSERT(ma_event_add_custom_field(NULL, event_2_custom_fields[counter].field_name, variant_obj) == MA_ERROR_INVALIDARG);
                TEST_ASSERT(ma_event_add_custom_field(event_obj, NULL, variant_obj) == MA_ERROR_INVALIDARG);
                TEST_ASSERT(ma_event_add_custom_field(event_obj, event_2_custom_fields[counter].field_name, NULL) == MA_ERROR_INVALIDARG);
                TEST_ASSERT(ma_event_add_custom_field(event_obj, event_2_custom_fields[counter].field_name, variant_obj) == MA_OK);

                ma_variant_release(variant_obj);
            }

            // Trying to add an event common field of non-string type
            ma_variant_create_from_uint16(0x1234, &variant_obj);
            TEST_ASSERT(ma_event_add_custom_field(event_obj, "NonStringField", variant_obj) == MA_ERROR_EVENT_INVALID_VALUE_TYPE);
            ma_variant_release(variant_obj);
        }

        /**************************************************************************
         Adding Event 2 to message
        **************************************************************************/
        TEST_ASSERT(ma_event_bag_add_event(event_bag_obj, NULL) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_event_bag_add_event(NULL, event_obj) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_event_bag_add_event(event_bag_obj, event_obj) == MA_OK);

        /**************************************************************************
         Releasing Event 2 object after adding it to event message
        **************************************************************************/
        TEST_ASSERT(ma_event_release(NULL) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_event_release(event_obj) == MA_OK);
    }

    /**************************************************************************
     Registering callback
    **************************************************************************/
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_register_callbacks(NULL, &callbacks_bundle, NULL));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_register_callbacks(g_ma_client, NULL, NULL));
    TEST_ASSERT(MA_OK == ma_event_register_callbacks(g_ma_client, &callbacks_bundle, NULL));

    /**************************************************************************
     Sending the Event message to dummy event service.
    **************************************************************************/
    // The server side callback function and the registered client callbacks have
    // to verify the received event bag contents. Wait for them to finish.
    done1 = 0;
    done2 = 0;

    TEST_ASSERT(ma_event_bag_send(NULL) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_event_bag_send(event_bag_obj) == MA_OK);

    // Wait until callbacks are completed
    wait_until_done_1();
    wait_until_done_2();

    /**************************************************************************
     Un-registering callback
    **************************************************************************/
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_unregister_callbacks(NULL));
    TEST_ASSERT(MA_OK == ma_event_unregister_callbacks(g_ma_client));

    /**************************************************************************
     Releasing event message
    **************************************************************************/
    TEST_ASSERT(ma_event_bag_release(NULL) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_event_bag_release(event_bag_obj) == MA_OK);
}










