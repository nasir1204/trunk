#ifndef MA_UT_SETUP_H_INCLUDED
#define MA_UT_SETUP_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_log.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_client.h"
#include "ma/ma_datastore.h"
#include "ma/internal/utils/database/ma_db.h"
#include "ma/internal/utils/context/ma_context.h"

MA_CPP(extern "C" {)

typedef struct ma_ut_setup_s ma_ut_setup_t;

ma_error_t ma_ut_setup_create(const char *ut_name, const char *product_id, ma_ut_setup_t **ut_setup);

void ma_ut_setup_release(ma_ut_setup_t *ut_setup);

ma_context_t *ma_ut_setup_get_context(ma_ut_setup_t *ut_setup);

ma_msgbus_t *ma_ut_setup_get_msgbus(ma_ut_setup_t *ut_setup);

ma_client_t *ma_ut_setup_get_client(ma_ut_setup_t *ut_setup);

ma_logger_t *ma_ut_setup_get_logger(ma_ut_setup_t *ut_setup); 

ma_ds_t *ma_ut_setup_get_datastore(ma_ut_setup_t *ut_setup); 
ma_db_t *ma_ut_setup_get_database(ma_ut_setup_t *ut_setup); 

typedef ma_error_t (*ma_ut_policy_buffer_provider_callback_t)(const char *section, const char *key, ma_buffer_t **value, void *cb_data);
void ma_ut_setup_set_policy_buffer_provider_callback(ma_ut_setup_t *ut_setup, ma_ut_policy_buffer_provider_callback_t cb, void *cb_data); 

typedef ma_error_t (*ma_ut_policy_variant_provider_callback_t)(const char *section, const char *key, ma_variant_t **value, void *cb_data);
void ma_ut_setup_set_policy_buffer_variant_callback(ma_ut_setup_t *ut_setup, ma_ut_policy_variant_provider_callback_t cb, void *cb_data); 

typedef ma_error_t (*ma_ut_policy_table_provider_callback_t)(const char *section, ma_table_t **value, void *cb_data);
void ma_ut_setup_set_policy_table_provider_callback(ma_ut_setup_t *ut_setup, ma_ut_policy_table_provider_callback_t cb, void *cb_data); 

MA_CPP(})

#endif /*MA_UT_SETUP_H_INCLUDED*/
