#ifndef MA_UT_CLIENT_SETUP_H_INCLUDED
#define MA_UT_CLIENT_SETUP_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/ma_log.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_client.h"
#include "ma/internal/utils/context/ma_context.h"

MA_CPP(extern "C" {)

typedef struct ma_ut_setup_s ma_ut_setup_t;

ma_error_t ma_ut_setup_create(const char *ut_name, const char *product_id, ma_ut_setup_t **ut_setup);

void ma_ut_setup_release(ma_ut_setup_t *ut_setup);

ma_context_t *ma_ut_setup_get_context(ma_ut_setup_t *ut_setup);

ma_msgbus_t *ma_ut_setup_get_msgbus(ma_ut_setup_t *ut_setup);

ma_client_t *ma_ut_setup_get_client(ma_ut_setup_t *ut_setup);

ma_logger_t *ma_ut_setup_get_logger(ma_ut_setup_t *ut_setup); 

MA_CPP(})

#endif /*MA_UT_CLIENT_SETUP_H_INCLUDED*/
