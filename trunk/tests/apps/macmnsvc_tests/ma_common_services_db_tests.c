/******************************************************************************
Broker DB unit tests.
******************************************************************************/

#include "unity.h"
#include "unity_fixture.h"
#include "unity_internals.h"
#include "ma/internal/utils/database/ma_db.h"
#include "ma/internal/apps/macmnsvc/ma_common_services_db.h"
#include <string.h>
#include <stdlib.h>

#ifndef WIN32
    #include <unistd.h>
#endif

TEST_GROUP_RUNNER(broker_db_test_group)
{
    do {RUN_TEST_CASE(broker_db_tests, db_add_and_remove_service_test)} while (0);
    do {RUN_TEST_CASE(broker_db_tests, db_add_and_remove_subscriber_test)} while (0);
    do {RUN_TEST_CASE(broker_db_tests, get_services_list_test)} while (0);
    do {RUN_TEST_CASE(broker_db_tests, get_services_list_test_regex)} while (0);
    do {RUN_TEST_CASE(broker_db_tests, get_subscribers_list_test)} while (0);
    do {RUN_TEST_CASE(broker_db_tests, get_subscribers_list_test_regex)} while (0);
}

static ma_db_t *db = NULL;
#define BROKER_DB_FILENAME  "BROKER_UT"

TEST_SETUP(broker_db_tests)
{
    unlink(BROKER_DB_FILENAME);
    TEST_ASSERT(MA_OK == ma_db_open(BROKER_DB_FILENAME, MA_DB_OPEN_READWRITE,&db));

    TEST_ASSERT(MA_ERROR_INVALIDARG == db_add_broker_instance(NULL));
    TEST_ASSERT(MA_OK == db_add_broker_instance(db));
}

TEST_TEAR_DOWN(broker_db_tests)
{
    TEST_ASSERT(MA_OK == ma_db_close(db));
    unlink(BROKER_DB_FILENAME);
}

// Lookup Table consisting of service information contained in Broker DB
struct LookupTable_1
{
    const char *test_service_name;
    const char *test_service_id;
    const char *test_product_id;
    const ma_uint32_t test_process_id;
    const char *test_host_name;
};

static struct LookupTable_1 const services[] =
{
    { "service.policy1",            "1",    "Product_1",  112,  "host 1" },
    { "service.property1",          "3",    "Product_2",  1343, "host 1" },
    { "service.event1",             "4",    "Product_3",  1445, "host 1" },
    { "service.scheduler",          "6",    "Product_1",  1232, "host 2" },
    { "service.event2",             "20",   "Product_2",  123,  "host 2" },
    { "service.event.my_event1",    "SEV5", "Product_3",  1232, "host 3" },
    { "service.event.my_event2",    "10",   "Product_3",  134,  "host 1" },
    { "service.policy2",            "7",    "Product_3",  1343, "host 3" },
    { "service.property2",          "11",   "Product_1",  1343, "host 3" },
    { "service.compat",             "25",   "Product_1",  134,  "host 3" },
    { "service.event3",             "100",  "Product_2",  1546, "host 1" },
    { "service.event4",             "75",   "Product_3",  1474, "host 1" },
};


// Lookup Table consisting of subscriber information contained in Broker DB
struct LookupTable_2
{
    const char *test_topic_name;
    const char *test_subscriber_id;
    const char *test_product_id;
    const ma_uint32_t test_process_id;
    const char *test_host_name;
};

static struct LookupTable_2 const subscribers[] =
{
    { "topic.1",   "1",    "Product_1",  112,  "host 1" },
    { "topic.2",   "3",    "Product_2",  1343, "host 1" },
    { "topic.3.1", "4",    "Product_3",  1445, "host 1" },
    { "topic.1",   "6",    "Product_1",  1232, "host 2" },
    { "topic.2",   "20",   "Product_2",  123,  "host 2" },
    { "topic.3.2", "SEV5", "Product_3",  1232, "host 3" },
    { "topic.1",   "10",   "Product_3",  134,  "host 1" },
    { "topic.4",   "7",    "Product_3",  1343, "host 3" },
    { "topic.3.3", "11",   "Product_1",  1343, "host 3" },
    { "topic.1",   "25",   "Product_1",  134,  "host 3" },
    { "topic.4",   "100",  "Product_2",  1546, "host 1" },
    { "topic.1",   "75",   "Product_3",  1474, "host 1" },
};


/* This function finds out that the service records returned by the
   db_get_all_services API are all expected records.
*/
static ma_int32_t service_list[sizeof(services)/sizeof(services[0])] = { 0 };

static ma_int32_t do_service_record_matching(const char *actual_service_name,
                                             const char *actual_service_id,
                                             const char *actual_product_id,
                                             const ma_uint32_t actual_process_id,
                                             const char *actual_host_name)
{
    ma_int32_t counter = 0;

    for(counter = 0; counter < sizeof(services)/sizeof(services[0]); counter++)
    {
        ma_uint8_t matched = 1;

        if(service_list[counter] == 1)
        {
            if(strcmp(services[counter].test_service_name, actual_service_name)) {  matched = 0;  }
            if(strcmp(services[counter].test_service_id, actual_service_id)) {  matched = 0;  }
            if(strcmp(services[counter].test_product_id, actual_product_id)) {  matched = 0;  }
            if(services[counter].test_process_id != actual_process_id) {  matched = 0;  }
            if(strcmp(services[counter].test_host_name, actual_host_name)) {  matched = 0;  }
        }

        if(service_list[counter] && matched)
        {
            service_list[counter] = 0;
            matched = 0;
            return 1;
        }
    }
    return 0;
}

/* This function finds out that the subscriber records returned by the
   db_get_all_subscriber API are all expected records.
*/
static ma_int32_t subscriber_list[sizeof(subscribers)/sizeof(subscribers[0])] = { 0 };

static ma_int32_t do_subscriber_record_matching(const char *actual_topic_name,
                                                const char *actual_subscriber_id,
                                                const char *actual_product_id,
                                                const ma_uint32_t actual_process_id,
                                                const char *actual_host_name)
{
    ma_int32_t counter = 0;

    for(counter = 0; counter < sizeof(subscribers)/sizeof(subscribers[0]); counter++)
    {
        ma_uint8_t matched = 1;

        if(subscriber_list[counter] == 1)
        {
            if(strcmp(subscribers[counter].test_topic_name, actual_topic_name)) {  matched = 0;  }
            if(strcmp(subscribers[counter].test_subscriber_id, actual_subscriber_id)) {  matched = 0;  }
            if(strcmp(subscribers[counter].test_product_id, actual_product_id)) {  matched = 0;  }
            if(subscribers[counter].test_process_id != actual_process_id) {  matched = 0;  }
            if(strcmp(subscribers[counter].test_host_name, actual_host_name)) {  matched = 0;  }
        }

        if(subscriber_list[counter] && matched)
        {
            subscriber_list[counter] = 0;
            matched = 0;
            return 1;
        }
    }
    return 0;
}


/******************************************************************************
Test for verifying the service addition/removal/retrieval APIs.
******************************************************************************/
TEST(broker_db_tests, db_add_and_remove_service_test)
{
    ma_int32_t counter = 0;
    ma_db_recordset_t *record_set = NULL;

    // Adding services to the DB
    for(counter = 0; counter < sizeof(services)/sizeof(services[0]); counter++)
    {
        TEST_ASSERT(MA_ERROR_INVALIDARG == db_add_service(NULL,
                                                          services[counter].test_service_name,
                                                          services[counter].test_service_id,
                                                          services[counter].test_product_id,
                                                          services[counter].test_process_id,
                                                          services[counter].test_host_name));

        TEST_ASSERT(MA_ERROR_INVALIDARG == db_add_service(db,
                                                          NULL,
                                                          services[counter].test_service_id,
                                                          services[counter].test_product_id,
                                                          services[counter].test_process_id,
                                                          services[counter].test_host_name));

        TEST_ASSERT(MA_ERROR_INVALIDARG == db_add_service(db,
                                                          services[counter].test_service_name,
                                                          NULL,
                                                          services[counter].test_product_id,
                                                          services[counter].test_process_id,
                                                          services[counter].test_host_name));

        TEST_ASSERT(MA_ERROR_INVALIDARG == db_add_service(db,
                                                          services[counter].test_service_name,
                                                          services[counter].test_service_id,
                                                          NULL,
                                                          services[counter].test_process_id,
                                                          services[counter].test_host_name));

        TEST_ASSERT(MA_ERROR_INVALIDARG == db_add_service(db,
                                                          services[counter].test_service_name,
                                                          services[counter].test_service_id,
                                                          services[counter].test_product_id,
                                                          0,
                                                          services[counter].test_host_name));

        TEST_ASSERT(MA_ERROR_INVALIDARG == db_add_service(db,
                                                          services[counter].test_service_name,
                                                          services[counter].test_service_id,
                                                          services[counter].test_product_id,
                                                          services[counter].test_process_id,
                                                          NULL));

        TEST_ASSERT(MA_OK == db_add_service(db,
                                            services[counter].test_service_name,
                                            services[counter].test_service_id,
                                            services[counter].test_product_id,
                                            services[counter].test_process_id,
                                            services[counter].test_host_name));
    }

    // Records inserted into DB doesn't get stored or retrieved in the same sequence,
    // therefore, one to one verification could not be performed.
    //
    // Setting all elements in service_list array to indicate that all elements have
    // to be retrieved yet. When an element is retrieved from the DB, corresponding
    // entry will be set to zero. Idea is that the db_get_all_services API should
    // return all records.
    for(counter = 0; counter < sizeof(services)/sizeof(services[0]); counter++)
    {
        service_list[counter] = 1;
    }

    // Getting list of all services
    TEST_ASSERT(MA_ERROR_INVALIDARG == db_get_all_services(NULL, &record_set));
    TEST_ASSERT(MA_ERROR_INVALIDARG == db_get_all_services(db, NULL));
    TEST_ASSERT(MA_OK == db_get_all_services(db, &record_set));

    // Reading records one by one from the record list retrieved from DB and
    // verifying them against expected values.
    while(MA_ERROR_NO_MORE_ITEMS != ma_db_recordset_next(record_set))
    {
        const char *service_name = NULL;
        const char *service_id = NULL;
        const char *product_id = NULL;
        ma_uint32_t process_id = 0;
        const char *host_name = NULL;

        ma_db_recordset_get_string(record_set, 1, &service_name);
        ma_db_recordset_get_string(record_set, 2, &service_id);
        ma_db_recordset_get_string(record_set, 3, &product_id);
        ma_db_recordset_get_int(record_set, 4, (int*)&process_id);
        ma_db_recordset_get_string(record_set, 5, &host_name);

        TEST_ASSERT(do_service_record_matching(service_name, service_id, product_id, process_id, host_name) == 1);
    }

    // Verifying that all records are retrieved by db_get_all_services
    // API
    for(counter = 0; counter < sizeof(service_list)/sizeof(service_list[0]); counter++)
    {
        TEST_ASSERT(service_list[counter] == 0);
    }

    ma_db_recordset_release(record_set);
    record_set = NULL;

    // Removing a service when, there is no service with that particular name
    TEST_ASSERT(MA_OK == db_remove_service(db, "No_Service_With_This_Name"));

    // Iterating through test list and Removing service records one by one
    for(counter = 0; counter < sizeof(services)/sizeof(services[0]); counter++)
    {
        TEST_ASSERT(MA_ERROR_INVALIDARG == db_remove_service(NULL, services[counter].test_service_name));
        TEST_ASSERT(MA_ERROR_INVALIDARG == db_remove_service(db, NULL));
        TEST_ASSERT(MA_OK == db_remove_service(db, services[counter].test_service_name));
    }

    // Again, getting list of all services after removing all records.
    TEST_ASSERT(MA_OK == db_get_all_services(db, &record_set));

    // No records should be returned
    TEST_ASSERT(MA_ERROR_NO_MORE_ITEMS == ma_db_recordset_next(record_set));

    ma_db_recordset_release(record_set);
    record_set = NULL;
}

/******************************************************************************
Test for verifying the subscriber addition/removal/retrieval APIs.
******************************************************************************/
TEST(broker_db_tests, db_add_and_remove_subscriber_test)
{
    ma_int32_t counter = 0;
    ma_db_recordset_t *record_set = NULL;

    // Adding subscribers to the DB
    for(counter = 0; counter < sizeof(subscribers)/sizeof(subscribers[0]); counter++)
    {
        TEST_ASSERT(MA_ERROR_INVALIDARG == db_add_subscriber(NULL,
                                                             subscribers[counter].test_topic_name,
                                                             subscribers[counter].test_subscriber_id,
                                                             subscribers[counter].test_product_id,
                                                             subscribers[counter].test_process_id,
                                                             subscribers[counter].test_host_name));

        TEST_ASSERT(MA_ERROR_INVALIDARG == db_add_subscriber(db,
                                                             NULL,
                                                             subscribers[counter].test_subscriber_id,
                                                             subscribers[counter].test_product_id,
                                                             subscribers[counter].test_process_id,
                                                             subscribers[counter].test_host_name));

        TEST_ASSERT(MA_ERROR_INVALIDARG == db_add_subscriber(db,
                                                             subscribers[counter].test_topic_name,
                                                             NULL,
                                                             subscribers[counter].test_product_id,
                                                             subscribers[counter].test_process_id,
                                                             subscribers[counter].test_host_name));

        TEST_ASSERT(MA_ERROR_INVALIDARG == db_add_subscriber(db,
                                                             subscribers[counter].test_topic_name,
                                                             subscribers[counter].test_subscriber_id,
                                                             NULL,
                                                             subscribers[counter].test_process_id,
                                                             subscribers[counter].test_host_name));

        TEST_ASSERT(MA_ERROR_INVALIDARG == db_add_subscriber(db,
                                                             subscribers[counter].test_topic_name,
                                                             subscribers[counter].test_subscriber_id,
                                                             subscribers[counter].test_product_id,
                                                             0,
                                                             subscribers[counter].test_host_name));

        TEST_ASSERT(MA_ERROR_INVALIDARG == db_add_subscriber(db,
                                                             subscribers[counter].test_topic_name,
                                                             subscribers[counter].test_subscriber_id,
                                                             subscribers[counter].test_product_id,
                                                             subscribers[counter].test_process_id,
                                                             NULL));

        TEST_ASSERT(MA_OK == db_add_subscriber(db,
                                               subscribers[counter].test_topic_name,
                                               subscribers[counter].test_subscriber_id,
                                               subscribers[counter].test_product_id,
                                               subscribers[counter].test_process_id,
                                               subscribers[counter].test_host_name));
    }

    // Records inserted into DB doesn't get stored or retrieved in the same sequence, therefore,
    // one to one verification could not be performed.
    //
    // Setting all elements in subscriber_list array to indicate that all elements have to be
    // retrieved yet. When an element is retrieved from the DB, corresponding entry will be
    // set to zero. Idea is that the db_get_all_subscribers API should return all records.
    /*for(counter = 0; counter < sizeof(subscribers)/sizeof(subscribers[0]); counter++)
    {
        subscriber_list[counter] = 1;
    }*/

    // Getting list of all subscribers
    //TEST_ASSERT(MA_ERROR_INVALIDARG == db_get_all_subscribers(NULL, &record_set));

    //TEST_ASSERT(MA_ERROR_INVALIDARG == db_get_all_subscribers(db, NULL));

    //TEST_ASSERT(MA_OK == db_get_all_subscribers(db, &record_set));

    // Reading records one by one from the record list retrieved from DB
    // and verifying them against expected values.
   /* while(MA_ERROR_NO_MORE_ITEMS != ma_db_recordset_next(record_set))
    {
        const char *topic_name = NULL;
        const char *subscriber_id = NULL;
        const char *product_id = NULL;
        ma_uint32_t process_id = 0;
        const char *host_name = NULL;

        ma_db_recordset_get_string(record_set, 1, &topic_name);
        ma_db_recordset_get_string(record_set, 2, &subscriber_id);
        ma_db_recordset_get_string(record_set, 3, &product_id);
        ma_db_recordset_get_int(record_set, 4, (int*)&process_id);
        ma_db_recordset_get_string(record_set, 5, &host_name);

        TEST_ASSERT(do_subscriber_record_matching(topic_name, subscriber_id, product_id, process_id, host_name) == 1);
    }*/

    // Verifying that all records are retrieved by db_get_all_subscribers
    // API
    /*for(counter = 0; counter < sizeof(subscriber_list)/sizeof(subscriber_list[0]); counter++)
    {
        TEST_ASSERT(subscriber_list[counter] == 0);
    }*/

    //ma_db_recordset_release(record_set);
    //record_set = NULL;

    // Removing a subscriber when, there is no subscriber with that particular information
    TEST_ASSERT(MA_OK == db_remove_subscriber(db, "my_subscriber", "Product_1", 123));

    // Iterating through test list and Removing subscriber records one by one
    for(counter = 0; counter < sizeof(subscribers)/sizeof(subscribers[0]); counter++)
    {
        TEST_ASSERT(MA_ERROR_INVALIDARG == db_remove_subscriber(NULL,
                                                                subscribers[counter].test_topic_name,
                                                                subscribers[counter].test_product_id,
                                                                subscribers[counter].test_process_id));

        TEST_ASSERT(MA_ERROR_INVALIDARG == db_remove_subscriber(db,
                                                                NULL,
                                                                subscribers[counter].test_product_id,
                                                                subscribers[counter].test_process_id));

        TEST_ASSERT(MA_ERROR_INVALIDARG == db_remove_subscriber(db,
                                                                subscribers[counter].test_topic_name,
                                                                NULL,
                                                                subscribers[counter].test_process_id));

        TEST_ASSERT(MA_ERROR_INVALIDARG == db_remove_subscriber(db,
                                                                subscribers[counter].test_topic_name,
                                                                subscribers[counter].test_product_id,
                                                                0));

        TEST_ASSERT(MA_OK == db_remove_subscriber(db,
                                                  subscribers[counter].test_topic_name,
                                                  subscribers[counter].test_product_id,
                                                  subscribers[counter].test_process_id));
    }

    // Again, getting list of all subscribers after removing all records.
    //TEST_ASSERT(MA_OK == db_get_all_subscribers(db, &record_set));

    // No records should be returned
    //TEST_ASSERT(MA_ERROR_NO_MORE_ITEMS == ma_db_recordset_next(record_set));

    //ma_db_recordset_release(record_set);
    //record_set = NULL;
}


/******************************************************************************
Verifying that the db_get_services_list API returns the list of services for
for any particular service name
******************************************************************************/
TEST(broker_db_tests, get_services_list_test)
{
    ma_int32_t counter = 0;
    ma_db_recordset_t *record_set = NULL;

    // Adding services to the DB
    for(counter = 0; counter < sizeof(services)/sizeof(services[0]); counter++)
    {
        TEST_ASSERT(MA_OK == db_add_service(db,
                                            services[counter].test_service_name,
                                            services[counter].test_service_id,
                                            services[counter].test_product_id,
                                            services[counter].test_process_id,
                                            services[counter].test_host_name));
    }

    // Iterating for services and retrieving services by service names
    for(counter = 0; counter < sizeof(services)/sizeof(services[0]); counter++)
    {
        ma_int32_t counter_1 = 0;

        TEST_ASSERT(MA_ERROR_INVALIDARG == db_get_services_list(NULL, services[counter].test_service_name, &record_set));
        TEST_ASSERT(MA_ERROR_INVALIDARG == db_get_services_list(db, NULL, &record_set));
        TEST_ASSERT(MA_ERROR_INVALIDARG == db_get_services_list(db, services[counter].test_service_name, NULL));
        TEST_ASSERT(MA_OK == db_get_services_list(db, services[counter].test_service_name, &record_set));

        // Finding records matching the 'service name'.
        for(counter_1 = 0; counter_1 < sizeof(services)/sizeof(services[0]); counter_1++)
        {
            if(strcmp(services[counter].test_service_name, services[counter_1].test_service_name) == 0)
            {
                service_list[counter_1] = 1;
            }
        }

        // Reading records one by one from the record list retrieved from DB
        // and verifying them against expected values.
        while(MA_ERROR_NO_MORE_ITEMS != ma_db_recordset_next(record_set))
        {
            const char *service_name = NULL;
            const char *service_id = NULL;
            const char *product_id = NULL;
            ma_uint32_t process_id = 0;
            const char *host_name = NULL;

            ma_db_recordset_get_string(record_set, 1, &service_name);
            ma_db_recordset_get_string(record_set, 2, &service_id);
            ma_db_recordset_get_string(record_set, 3, &product_id);
            ma_db_recordset_get_int(record_set, 4, (int*)&process_id);
            ma_db_recordset_get_string(record_set, 5, &host_name);

            TEST_ASSERT(do_service_record_matching(service_name, service_id, product_id, process_id, host_name) == 1);
        }

        ma_db_recordset_release(record_set);
        record_set = NULL;

        // Verifying that all records are retrieved by db_get_services_list
        // API
        for(counter = 0; counter < sizeof(service_list)/sizeof(service_list[0]); counter++)
        {
            TEST_ASSERT(service_list[counter] == 0);
        }
    }
}


/******************************************************************************
Verifying that the db_get_services_list API returns the list of services for
for any particular service name (regular expression based).
******************************************************************************/
TEST(broker_db_tests, get_services_list_test_regex)
{
    ma_db_recordset_t *record_set = NULL;

    // Adding services to the DB
    TEST_ASSERT(MA_OK == db_add_service(db, "service.event",           "20",   "Product_2",  123,  "host 2"));
    TEST_ASSERT(MA_OK == db_add_service(db, "service.event.my_event1", "SEV5", "Product_3",  1232, "host 3"));
    TEST_ASSERT(MA_OK == db_add_service(db, "service.event.my_event2", "10",   "Product_3",  134,  "host 1"));

    // Getting the list of services matching "service.event.*"
    TEST_ASSERT(MA_OK == db_get_services_list(db, "service.event.%", &record_set));

    // Reading records one by one from the record list retrieved from
    // DB and verifying the expected 2 records are returned. Note - Only
    // "service names" are verified here, other fields are already
    // verified in previous tests
    {
        const char *service_name = NULL;
        ma_bool_t first_found = MA_FALSE;
        ma_bool_t second_found = MA_FALSE;

        TEST_ASSERT(MA_OK == ma_db_recordset_next(record_set));
        ma_db_recordset_get_string(record_set, 1, &service_name);

        if(!strcmp("service.event.my_event1", service_name)) first_found = MA_TRUE;
        if(!strcmp("service.event.my_event2", service_name)) second_found = MA_TRUE;

        TEST_ASSERT(MA_OK == ma_db_recordset_next(record_set));
        ma_db_recordset_get_string(record_set, 1, &service_name);

        if(!strcmp("service.event.my_event1", service_name)) first_found = MA_TRUE;
        if(!strcmp("service.event.my_event2", service_name)) second_found = MA_TRUE;

        TEST_ASSERT(MA_TRUE == first_found);
        TEST_ASSERT(MA_TRUE == second_found);

        TEST_ASSERT(MA_ERROR_NO_MORE_ITEMS == ma_db_recordset_next(record_set));
    }

    ma_db_recordset_release(record_set);
    record_set = NULL;
}


/******************************************************************************
Verifying that the db_get_subscribers_list API returns the list of subscribers
for any particular topic
******************************************************************************/
TEST(broker_db_tests, get_subscribers_list_test)
{
    ma_int32_t counter = 0;
    ma_db_recordset_t *record_set = NULL;

    // Adding subscribers to the DB
    for(counter = 0; counter < sizeof(subscribers)/sizeof(subscribers[0]); counter++)
    {
        TEST_ASSERT(MA_OK == db_add_subscriber(db,
                                               subscribers[counter].test_topic_name,
                                               subscribers[counter].test_subscriber_id,
                                               subscribers[counter].test_product_id,
                                               subscribers[counter].test_process_id,
                                               subscribers[counter].test_host_name));
    }

    // Iterating for topics and retrieving records topic-wise
    for(counter = 0; counter < sizeof(subscribers)/sizeof(subscribers[0]); counter++)
    {
        ma_int32_t counter_1 = 0;

        TEST_ASSERT(MA_ERROR_INVALIDARG == db_get_subscribers_list(NULL,
                                                                   subscribers[counter].test_topic_name,
                                                                   &record_set));

        TEST_ASSERT(MA_ERROR_INVALIDARG == db_get_subscribers_list(db,
                                                                   NULL,
                                                                   &record_set));

        TEST_ASSERT(MA_ERROR_INVALIDARG == db_get_subscribers_list(db,
                                                                   subscribers[counter].test_topic_name,
                                                                   NULL));

        TEST_ASSERT(MA_OK == db_get_subscribers_list(db, subscribers[counter].test_topic_name, &record_set));

        // Finding records matching the 'topic name'.
        for(counter_1 = 0; counter_1 < sizeof(subscribers)/sizeof(subscribers[0]); counter_1++)
        {
            if(strcmp(subscribers[counter].test_topic_name, subscribers[counter_1].test_topic_name) == 0)
            {
                subscriber_list[counter_1] = 1;
            }
        }

        // Reading records one by one from the record list retrieved from DB
        // and verifying them against expected values.
        while(MA_ERROR_NO_MORE_ITEMS != ma_db_recordset_next(record_set))
        {
            const char *topic_name = NULL;
            const char *subscriber_id = NULL;
            const char *product_id = NULL;
            ma_uint32_t process_id = 0;
            const char *host_name = NULL;

            ma_db_recordset_get_string(record_set, 1, &topic_name);
            ma_db_recordset_get_string(record_set, 2, &subscriber_id);
            ma_db_recordset_get_string(record_set, 3, &product_id);
            ma_db_recordset_get_int(record_set, 4, (int*)&process_id);
            ma_db_recordset_get_string(record_set, 5, &host_name);

            // Marking that a record matching the topic is retrieved from the DB. If this API
            // returns 0, it means that an unintended record is retrieved from DB
            TEST_ASSERT(1 == do_subscriber_record_matching(topic_name, subscriber_id, product_id, process_id, host_name));
        }

        ma_db_recordset_release(record_set);
        record_set = NULL;

        // Verifying that all records are retrieved by db_get_all_subscribers
        // API. No matched record is pending.
        for(counter_1 = 0; counter_1 < sizeof(subscriber_list)/sizeof(subscriber_list[0]); counter_1++)
        {
            TEST_ASSERT(subscriber_list[counter] == 0);
        }
    }
}


/******************************************************************************
Verifying that the db_get_subscribers_list API returns the list of subscribers
for any particular topic (regular expression based).
******************************************************************************/
TEST(broker_db_tests, get_subscribers_list_test_regex)
{
    ma_int32_t counter = 0;
    ma_db_recordset_t *record_set = NULL;

    // Adding subscribers to the DB
    for(counter = 0; counter < sizeof(subscribers)/sizeof(subscribers[0]); counter++)
    {
        TEST_ASSERT(MA_OK == db_add_subscriber(db,
                                               subscribers[counter].test_topic_name,
                                               subscribers[counter].test_subscriber_id,
                                               subscribers[counter].test_product_id,
                                               subscribers[counter].test_process_id,
                                               subscribers[counter].test_host_name));
    }

    // Getting the list of subscribers matching "topic.3.*"
    TEST_ASSERT(MA_OK == db_get_subscribers_list(db, "topic.3.%", &record_set));

    // Reading records one by one from the record list retrieved from
    // DB and verifying the expected 3 records are returned. Note - Only
    // topic names and subscriber ID fields are verified here, other
    // fields are already verified in previous tests
    {
        const char *topic_name = NULL;
        const char *subscriber_id = NULL;

        ma_bool_t first_found = MA_FALSE;
        ma_bool_t second_found = MA_FALSE;
        ma_bool_t third_found = MA_FALSE;

        TEST_ASSERT(MA_OK == ma_db_recordset_next(record_set));
        ma_db_recordset_get_string(record_set, 1, &topic_name);
        ma_db_recordset_get_string(record_set, 2, &subscriber_id);

        if(!strcmp("topic.3.1", topic_name) && !strcmp("4", subscriber_id)) first_found = MA_TRUE;
        if(!strcmp("topic.3.2", topic_name) && !strcmp("SEV5", subscriber_id)) second_found = MA_TRUE;
        if(!strcmp("topic.3.3", topic_name) && !strcmp("11", subscriber_id)) third_found = MA_TRUE;


        TEST_ASSERT(MA_OK == ma_db_recordset_next(record_set));
        ma_db_recordset_get_string(record_set, 1, &topic_name);
        ma_db_recordset_get_string(record_set, 2, &subscriber_id);

        if(!strcmp("topic.3.1", topic_name) && !strcmp("4", subscriber_id)) first_found = MA_TRUE;
        if(!strcmp("topic.3.2", topic_name) && !strcmp("SEV5", subscriber_id)) second_found = MA_TRUE;
        if(!strcmp("topic.3.3", topic_name) && !strcmp("11", subscriber_id)) third_found = MA_TRUE;


        TEST_ASSERT(MA_OK == ma_db_recordset_next(record_set));
        ma_db_recordset_get_string(record_set, 1, &topic_name);
        ma_db_recordset_get_string(record_set, 2, &subscriber_id);

        if(!strcmp("topic.3.1", topic_name) && !strcmp("4", subscriber_id)) first_found = MA_TRUE;
        if(!strcmp("topic.3.2", topic_name) && !strcmp("SEV5", subscriber_id)) second_found = MA_TRUE;
        if(!strcmp("topic.3.3", topic_name) && !strcmp("11", subscriber_id)) third_found = MA_TRUE;

        TEST_ASSERT(MA_TRUE == first_found);
        TEST_ASSERT(MA_TRUE == second_found);
        TEST_ASSERT(MA_TRUE == third_found);

        TEST_ASSERT(MA_ERROR_NO_MORE_ITEMS == ma_db_recordset_next(record_set));
    }

    ma_db_recordset_release(record_set);
    record_set = NULL;
}

