#include "ma/internal/utils/platform/ma_system_property.h"
#include "ma/internal/utils/network/ma_net_interface.h"
#include "ma/internal/utils/platform/ma_net_interface_list_internal.h"
#include "ma/logger/ma_file_logger.h"
#include "ma/logger/ma_console_logger.h"
#include "ma/ma_log.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#define LOG_FACILITY_NAME "system_property_test"
ma_logger_t *logger = NULL;

#if defined(_WIN32)
	#define BASE_DIR ".\\"
#else
	#define BASE_DIR "/code/5.0/krishna_trunk//"
#endif

#ifdef WIN32
	#include <windows.h>
	int ma_wsa_startup()
	{
		WORD wVersionRequested;
		WSADATA wsaData;
		int err;
	/* Use the MAKEWORD(lowbyte, highbyte) macro declared in Windef.h */
		wVersionRequested = MAKEWORD(2, 2);

		err = WSAStartup(wVersionRequested, &wsaData);
		if (err != 0) { 
			return MA_ERROR_APIFAILED;
		}
		return MA_OK;
	}

	void ma_wsa_cleanup()
	{
		WSACleanup();
	}
#endif


int main()
{
	ma_error_t rc = MA_OK;
	ma_logger_t *logger = NULL;
	ma_system_property_t *sysprop_information = NULL;
	const ma_net_interface_list_t *net_interface_list = NULL;

	/*If ma_sysprop_information_t has to be make as const (to avoid user modification of internal pointer), then user need to use double pointer as follow & delete it again. */
	/*const ma_sysprop_information_t **  sysprop_information = (ma_sysprop_information_t**)malloc(sizeof(ma_sysprop_information_t*));*/
	/*To make it simple I have used non const API (need to be review )*/
	
	if(MA_OK == ma_file_logger_create(BASE_DIR,"sysprop",".log",(ma_file_logger_t **)&logger)) {
	/*if(MA_OK == ma_console_logger_create((ma_console_logger_t **)&logger)) {*/
	#if defined(WIN32)
		if(0 != ma_wsa_startup())
		{
			MA_LOG(logger, MA_LOG_SEV_ERROR, "Network Inerface property fetching failed ");
			return MA_ERROR_APIFAILED;
		}
	#endif	

		MA_LOG(logger, MA_LOG_SEV_TRACE, "1) -------------------------All system property fetching Started--------------------------------------");
		rc = ma_system_property_create(&sysprop_information, logger);
		if(MA_OK != rc)
			MA_LOG(logger, MA_LOG_SEV_ERROR, " system property object creation failed");
		rc = ma_system_property_scan(sysprop_information);
		if(MA_OK != rc)
			MA_LOG(logger, MA_LOG_SEV_ERROR, "scan failed");
		
		ma_system_property_os_get(sysprop_information, MA_TRUE);
		ma_system_property_cpu_get(sysprop_information, MA_TRUE);
		ma_system_property_memory_get(sysprop_information, MA_TRUE);
		ma_system_property_misc_get(sysprop_information, MA_TRUE);
		ma_system_property_disk_get(sysprop_information, MA_TRUE);
		ma_system_property_network_get(sysprop_information, MA_TRUE);
		ma_system_property_release(sysprop_information);
		MA_LOG(logger, MA_LOG_SEV_TRACE, "----------------------------------All system property fetching End-------------------------------------");

		MA_LOG(logger, MA_LOG_SEV_TRACE, "2)------------------------------- CPU system property fetching Started----------------------------------");
		rc = ma_system_property_create(  &sysprop_information, logger);
		if(MA_OK != rc)
			MA_LOG(logger, MA_LOG_SEV_ERROR, " system property object creation failed");
		if(NULL== ma_system_property_cpu_get(sysprop_information, MA_TRUE))
					MA_LOG(logger, MA_LOG_SEV_ERROR, "CPU system property fetching failed");
		ma_system_property_release(sysprop_information);
		MA_LOG(logger, MA_LOG_SEV_TRACE, "---------------------------------------CPU  system property fetching End--------------------------------");

		
		MA_LOG(logger, MA_LOG_SEV_TRACE, "3) -----------------------------------OS system property fetching Started--------------------------------");
		rc = ma_system_property_create( &sysprop_information, logger);
		if(MA_OK != rc)
			MA_LOG(logger, MA_LOG_SEV_ERROR, " system property object creation failed");
		if(NULL == ma_system_property_os_get(sysprop_information, MA_TRUE))
			MA_LOG(logger, MA_LOG_SEV_ERROR, "OS system property fetching failed");
		ma_system_property_release(sysprop_information);
		MA_LOG(logger, MA_LOG_SEV_TRACE, "------------------------------------------OS system property fetching End------------------------------------");

		MA_LOG(logger, MA_LOG_SEV_TRACE, "4)---------------------------------------- Misc system property fetching Started-----------------------------");
		rc = ma_system_property_create(&sysprop_information, logger);
		if(MA_OK != rc)
			MA_LOG(logger, MA_LOG_SEV_ERROR, " system property object creation failed");
		if(NULL ==  ma_system_property_misc_get(sysprop_information, MA_TRUE))
			MA_LOG(logger, MA_LOG_SEV_ERROR, "Misc system property fetching failed");
		ma_system_property_release(sysprop_information);
		MA_LOG(logger, MA_LOG_SEV_TRACE, "-----------------------------------------------Misc system property fetching End----------------------------");

		MA_LOG(logger, MA_LOG_SEV_TRACE, "5) ------------------------------------------Memory system property fetching Started--------------------------");
		rc = ma_system_property_create(&sysprop_information, logger);
		if(MA_OK != rc)
			MA_LOG(logger, MA_LOG_SEV_ERROR, " system property object creation failed");
		if(NULL == ma_system_property_memory_get(sysprop_information, MA_TRUE))
			MA_LOG(logger, MA_LOG_SEV_ERROR, "Memory system property fetching failed");
		ma_system_property_release(sysprop_information);
		MA_LOG(logger, MA_LOG_SEV_TRACE, "------------------------------------------------Memory system property fetching End----------------------------");

		MA_LOG(logger, MA_LOG_SEV_TRACE, "6)-------------------------------------------- Disc system property fetching Started----------------------------");
		rc = ma_system_property_create(&sysprop_information, logger);
		if(MA_OK != rc)
			MA_LOG(logger, MA_LOG_SEV_ERROR, " system property object creation failed");
		if(NULL == ma_system_property_disk_get(sysprop_information, MA_TRUE))
			MA_LOG(logger, MA_LOG_SEV_ERROR, "Disc system property fetching failed");
		ma_system_property_release(sysprop_information);
		MA_LOG(logger, MA_LOG_SEV_TRACE, "---------------------------------------------------Disc system property fetching End----------------------------");

		MA_LOG(logger, MA_LOG_SEV_TRACE, "7)--------------------------------------------- NW system property fetching Started-----------------------------");
		rc = ma_system_property_create(&sysprop_information, logger);
		if(MA_OK != rc)
			MA_LOG(logger, MA_LOG_SEV_ERROR, " system property object creation failed");
		if(NULL == ma_system_property_network_get(sysprop_information, MA_TRUE)){
			MA_LOG(logger, MA_LOG_SEV_ERROR, "NW system property fetching failed");
		}
		ma_system_property_release(sysprop_information);
		MA_LOG(logger, MA_LOG_SEV_TRACE, "---------------------------------------------------NW system property fetching End---------------------------------");

		ma_file_logger_release((ma_file_logger_t *)logger);
		/*ma_console_logger_release((ma_console_logger_t *)logger);*/

	#if defined (WIN32)
		ma_wsa_cleanup();
	#endif
	}
	else
		printf("Not able to open logger \n");
return 0;
}

