#if 0
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "unity.h"
#include "unity_internals.h" 
#include "unity_fixture.h"

#include "ma/../../src/apps/maconfig/ma_config_param.h"
#include "ma/../../src/apps/maconfig/ma_config_internal.h"
#include "ma/../../src/apps/maconfig/ma_agent_provisioner.h"

extern char *ma_optarg;     // global argument pointer
extern int  optind ;     // global argv index
TEST_GROUP_RUNNER(ma_config_param_tests_group) {
    printf(" Tests for config parameters ... \n");
    do {RUN_TEST_CASE(ma_config_param_tests, ma_config_param_provision_tests)} while (0);
    do {RUN_TEST_CASE(ma_config_param_tests, ma_config_param_start_tests)} while (0);
    do {RUN_TEST_CASE(ma_config_param_tests, ma_config_param_stop_tests)} while (0);
    do {RUN_TEST_CASE(ma_config_param_tests, ma_config_param_unmanaged_tests)} while (0);
    do {RUN_TEST_CASE(ma_config_param_tests, ma_config_param_keyimport_tests)} while (0);
}


TEST_SETUP(ma_config_param_tests) {
}

TEST_TEAR_DOWN(ma_config_param_tests) {
}

TEST(ma_config_param_tests, ma_config_param_provision_tests) {
	int argc = 5;
	char *argv[] = {"maconfig","-p", "-d" , "Anydirectoy", "-m"};
	int argc1 = 4;
	char *argv1[] = {"maconfig","-p", "-d" , "Anydirectoy"};
	int argc2 = 3;
	char *argv2[] = {"maconfig","-p", "-d" };
	int argc3 = 2;
	char *argv3[] = {"maconfig","-p" };

	int argc4 = 7;
	char *argv4[] = {"maconfig","-p", "-d" , "Anydirectoy", "-m", "-o", "nostart"};

	int argc5 = 7;
	char *argv5[] = {"maconfig","-p", "-d" , "Anydirectoy", "-m", "-o", "JunkOption"};

	int argc6 = 8;
	char *argv6[] = {"maconfig","-z", "-p", "-d" , "Anydirectoy", "-m", "-o", "JunkOption"};

	ma_config_param_t params = {0};
	TEST_ASSERT_MESSAGE(MA_OK == ma_config_param_init(&params), "ma_config_param_init Failed");
    TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == ma_config_param_read(argc, NULL, &params),"ma_config_param_read Failed") ;
	ma_config_param_deinit(&params);

	ma_optarg = NULL;	optind = 0;
	TEST_ASSERT_MESSAGE(MA_OK == ma_config_param_init(&params), "ma_config_param_init Failed");
	TEST_ASSERT_MESSAGE(MA_OK == ma_config_param_read(argc, argv, &params),"ma_config_param_read Failed") ;
	ma_config_param_deinit(&params);

	ma_optarg = NULL;	optind = 0;
	TEST_ASSERT_MESSAGE(MA_OK == ma_config_param_init(&params), "ma_config_param_init Failed");
	TEST_ASSERT_MESSAGE(MA_OK == ma_config_param_read(argc1, argv1, &params),"ma_config_param_read Failed") ;
	ma_config_param_deinit(&params);

	ma_optarg = NULL;	optind = 0;
	TEST_ASSERT_MESSAGE(MA_OK == ma_config_param_init(&params), "ma_config_param_init Failed");
	TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == ma_config_param_read(argc2, argv2, &params),"ma_config_param_read Failed") ;
	ma_config_param_deinit(&params);

	ma_optarg = NULL;	optind = 0;
	TEST_ASSERT_MESSAGE(MA_OK == ma_config_param_init(&params), "ma_config_param_init Failed");
	TEST_ASSERT_MESSAGE(MA_OK == ma_config_param_read(argc3, argv3, &params),"ma_config_param_read Failed") ;
	ma_config_param_deinit(&params);

	ma_optarg = NULL;	optind = 0;
	TEST_ASSERT_MESSAGE(MA_OK == ma_config_param_init(&params), "ma_config_param_init Failed");
	TEST_ASSERT_MESSAGE(MA_OK == ma_config_param_read(argc4, argv4, &params),"ma_config_param_read Failed") ;
	ma_config_param_deinit(&params);

	ma_optarg = NULL;	optind = 0;
	TEST_ASSERT_MESSAGE(MA_OK == ma_config_param_init(&params), "ma_config_param_init Failed");
	TEST_ASSERT_MESSAGE(MA_OK == ma_config_param_read(argc5, argv5, &params),"ma_config_param_read Failed") ;
	ma_config_param_deinit(&params);

	ma_optarg = NULL;	optind = 0;
	TEST_ASSERT_MESSAGE(MA_OK == ma_config_param_init(&params), "ma_config_param_init Failed");
	TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == ma_config_param_read(argc6, argv6, &params),"ma_config_param_read Failed") ;
	ma_config_param_deinit(&params);
	ma_optarg = NULL;	optind = 0;
}

TEST(ma_config_param_tests, ma_config_param_start_tests) {
	int argc = 2;
	char *argv[] = {"maconfig","-s"};

	int argc2 = 3;
	char *argv2[] = {"maconfig","-s", "JuncOption"};

	int argc3 = 2;
	char *argv3[] = {"maconfig","s"};

	ma_config_param_t params = {0};
	TEST_ASSERT_MESSAGE(MA_OK == ma_config_param_init(&params), "ma_config_param_init Failed");
    TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == ma_config_param_read(argc, NULL, &params),"ma_config_param_read Failed") ;
	ma_config_param_deinit(&params);

	ma_optarg = NULL;	optind = 0;
	TEST_ASSERT_MESSAGE(MA_OK == ma_config_param_init(&params), "ma_config_param_init Failed");
	TEST_ASSERT_MESSAGE(MA_OK == ma_config_param_read(argc, argv, &params),"ma_config_param_read Failed") ;
	ma_config_param_deinit(&params);

	ma_optarg = NULL;	optind = 0;
	TEST_ASSERT_MESSAGE(MA_OK == ma_config_param_init(&params), "ma_config_param_init Failed");
	TEST_ASSERT_MESSAGE(MA_OK == ma_config_param_read(argc2, argv2, &params),"ma_config_param_read Failed") ;
	ma_config_param_deinit(&params);

	ma_optarg = NULL;	optind = 0;
	TEST_ASSERT_MESSAGE(MA_OK == ma_config_param_init(&params), "ma_config_param_init Failed");
	TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == ma_config_param_read(argc3, argv3, &params),"ma_config_param_read Failed") ;
	ma_config_param_deinit(&params);
	ma_optarg = NULL;	optind = 0;
}

TEST(ma_config_param_tests, ma_config_param_stop_tests) {
	int argc = 2;
	char *argv[] = {"maconfig","-e"};

	int argc2 = 3;
	char *argv2[] = {"maconfig","-e", "JuncOption"};

	int argc3 = 2;
	char *argv3[] = {"maconfig","e"};

	ma_config_param_t params = {0};
	TEST_ASSERT_MESSAGE(MA_OK == ma_config_param_init(&params), "ma_config_param_init Failed");
    TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == ma_config_param_read(argc, NULL, &params),"ma_config_param_read Failed") ;
	ma_config_param_deinit(&params);

	ma_optarg = NULL;	optind = 0;
	TEST_ASSERT_MESSAGE(MA_OK == ma_config_param_init(&params), "ma_config_param_init Failed");
	TEST_ASSERT_MESSAGE(MA_OK == ma_config_param_read(argc, argv, &params),"ma_config_param_read Failed") ;
	ma_config_param_deinit(&params);

	ma_optarg = NULL;	optind = 0;
	TEST_ASSERT_MESSAGE(MA_OK == ma_config_param_init(&params), "ma_config_param_init Failed");
	TEST_ASSERT_MESSAGE(MA_OK == ma_config_param_read(argc2, argv2, &params),"ma_config_param_read Failed") ;
	ma_config_param_deinit(&params);

	ma_optarg = NULL;	optind = 0;
	TEST_ASSERT_MESSAGE(MA_OK == ma_config_param_init(&params), "ma_config_param_init Failed");
	TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == ma_config_param_read(argc3, argv3, &params),"ma_config_param_read Failed") ;
	ma_config_param_deinit(&params);
	ma_optarg = NULL;	optind = 0;
}

TEST(ma_config_param_tests, ma_config_param_unmanaged_tests) {
	int argc = 2;
	char *argv[] = {"maconfig","-u"};

	int argc2 = 3;
	char *argv2[] = {"maconfig","-u", "JuncOption"};

	int argc3 = 2;
	char *argv3[] = {"maconfig","u"};

	ma_config_param_t params = {0};
	TEST_ASSERT_MESSAGE(MA_OK == ma_config_param_init(&params), "ma_config_param_init Failed");
    TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == ma_config_param_read(argc, NULL, &params),"ma_config_param_read Failed") ;
	ma_config_param_deinit(&params);

	ma_optarg = NULL;	optind = 0;
	TEST_ASSERT_MESSAGE(MA_OK == ma_config_param_init(&params), "ma_config_param_init Failed");
	TEST_ASSERT_MESSAGE(MA_OK == ma_config_param_read(argc, argv, &params),"ma_config_param_read Failed") ;
	ma_config_param_deinit(&params);

	ma_optarg = NULL;	optind = 0;
	TEST_ASSERT_MESSAGE(MA_OK == ma_config_param_init(&params), "ma_config_param_init Failed");
	TEST_ASSERT_MESSAGE(MA_OK == ma_config_param_read(argc2, argv2, &params),"ma_config_param_read Failed") ;
	ma_config_param_deinit(&params);

	ma_optarg = NULL;	optind = 0;
	TEST_ASSERT_MESSAGE(MA_OK == ma_config_param_init(&params), "ma_config_param_init Failed");
	TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == ma_config_param_read(argc3, argv3, &params),"ma_config_param_read Failed") ;
	ma_config_param_deinit(&params);
	ma_optarg = NULL;	optind = 0;
}

TEST(ma_config_param_tests, ma_config_param_keyimport_tests) {
	int argc = 2;
	char *argv[] = {"maconfig","-k"};

	int argc2 = 3;
	char *argv2[] = {"maconfig","-k", "AnyDirectory"};

	int argc3 = 4;
	char *argv3[] = {"maconfig","-k", "-d", "AnyDirectory"};

	int argc4 = 3;
	char *argv4[] = {"maconfig","-k", "-d"};

	ma_config_param_t params = {0};
	TEST_ASSERT_MESSAGE(MA_OK == ma_config_param_init(&params), "ma_config_param_init Failed");
    TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == ma_config_param_read(argc, NULL, &params),"ma_config_param_read Failed") ;
	ma_config_param_deinit(&params);

	ma_optarg = NULL;	optind = 0;
	TEST_ASSERT_MESSAGE(MA_OK == ma_config_param_init(&params), "ma_config_param_init Failed");
	TEST_ASSERT_MESSAGE(MA_OK == ma_config_param_read(argc, argv, &params),"ma_config_param_read Failed") ;
	TEST_ASSERT_MESSAGE(MA_FALSE == ma_config_param_is_good(&params),"ma_config_param_read Failed") ;
	ma_config_param_deinit(&params);

	ma_optarg = NULL;	optind = 0;
	TEST_ASSERT_MESSAGE(MA_OK == ma_config_param_init(&params), "ma_config_param_init Failed");
	TEST_ASSERT_MESSAGE(MA_OK == ma_config_param_read(argc2, argv2, &params),"ma_config_param_read Failed") ;
	TEST_ASSERT_MESSAGE(MA_FALSE == ma_config_param_is_good(&params),"ma_config_param_read Failed") ;
	ma_config_param_deinit(&params);

	ma_optarg = NULL;	optind = 0;
	TEST_ASSERT_MESSAGE(MA_OK == ma_config_param_init(&params), "ma_config_param_init Failed");
	TEST_ASSERT_MESSAGE(MA_OK == ma_config_param_read(argc3, argv3, &params),"ma_config_param_read Failed") ;
	TEST_ASSERT_MESSAGE(MA_TRUE == ma_config_param_is_good(&params),"ma_config_param_read Failed") ;
	ma_config_param_deinit(&params);

	ma_optarg = NULL;	optind = 0;
	TEST_ASSERT_MESSAGE(MA_OK == ma_config_param_init(&params), "ma_config_param_init Failed");
	TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == ma_config_param_read(argc4, argv4, &params),"ma_config_param_read Failed") ;
	TEST_ASSERT_MESSAGE(MA_FALSE == ma_config_param_is_good(&params),"ma_config_param_read Failed") ;
	ma_config_param_deinit(&params);
	ma_optarg = NULL;	optind = 0;
}

#endif