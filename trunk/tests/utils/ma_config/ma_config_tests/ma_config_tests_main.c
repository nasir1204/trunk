/*****************************************************************************
Main Test runner for MA config unit tests.
******************************************************************************/
#include "unity.h"
#include "unity_fixture.h"
#include <stdlib.h>
#include <stdio.h>

#include "ma/internal/ma_macros.h"

static void run_all_tests(void)
{
	//do { RUN_TEST_GROUP(ma_config_param_tests_group);}while(0);
}


int main(int argc, char *argv[]) {
	int ret = 0;
	ret = UnityMain(argc, argv, run_all_tests);;
    return ret;
}


