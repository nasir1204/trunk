#if defined(MA_NETWORK_USE_MOCK)
#include "ma_mock_request.h"
#include "ma_mock_request_handler.h"
#include "ma/internal/utils/network/ma_url_request_handler.h"
#include "ma_url_utils.h"

MA_CPP(extern "C" {)

static ma_error_t ma_mock_request_release(ma_mock_url_request_t *url_request);
static ma_error_t ma_mock_request_set_url_auth_info(ma_mock_url_request_t *url_request, ma_url_request_auth_t *authenticate) ;
static ma_error_t ma_mock_request_set_ssl_info(ma_mock_url_request_t *url_request, ma_url_request_ssl_info_t *ssl_info);
static ma_error_t ma_mock_request_set_range_info(ma_mock_url_request_t *url_request, ma_url_request_range_t *range_info);
static ma_error_t ma_mock_request_set_proxy_info(ma_mock_url_request_t *url_request, ma_proxy_list_t *proxy) ;

static const struct ma_url_request_methods_s mock_request_methods = {
    &ma_mock_request_release,
    &ma_mock_request_set_url_auth_info,
    &ma_mock_request_set_ssl_info,
    &ma_mock_request_set_range_info,
    &ma_mock_request_set_proxy_info
};

ma_error_t ma_mock_url_request_create(const char *url, ma_mock_url_request_t **mock_url_request) {
    ma_error_t rc = MA_OK ;  
    ma_mock_url_request_t *url_request_tmp = NULL;
	ma_network_transfer_protocol_t protocol;
	if(MA_OK != (rc = ma_url_utils_get_protocol_type(url, &protocol)))         
        return rc ;
	url_request_tmp=(ma_mock_url_request_t *)calloc(1, sizeof(ma_mock_url_request_t)) ;
    if(!url_request_tmp)
        return MA_ERROR_OUTOFMEMORY ;
    url_request_tmp->base.url = strdup(url) ;
    url_request_tmp->auth = NULL ;
    url_request_tmp->proxy_list = NULL ;
    url_request_tmp->range_info = NULL ;
    url_request_tmp->ssl_info = NULL ;
    url_request_tmp->base.vtable = &mock_request_methods ;
	url_request_tmp->base.protocol=protocol;
    *mock_url_request = url_request_tmp ;
    return MA_OK;
}
ma_error_t ma_mock_request_release(ma_mock_url_request_t *url_request) {    
    if(url_request->base.url)  free(url_request->base.url) ;    
    free(url_request) ;
    return MA_OK ;
}

ma_error_t ma_mock_request_set_url_auth_info(ma_mock_url_request_t *url_request, ma_url_request_auth_t *authenticate) {    
    url_request->auth = authenticate ;
    return MA_OK ;
}

ma_error_t ma_mock_request_set_ssl_info(ma_mock_url_request_t *url_request, ma_url_request_ssl_info_t *ssl_info) {    
    url_request->ssl_info = ssl_info ;
    return MA_OK ;
}

ma_error_t ma_mock_request_set_range_info(ma_mock_url_request_t *url_request, ma_url_request_range_t *range_info) {    
    url_request->range_info = range_info ;
    return MA_OK ;
}

ma_error_t ma_mock_request_set_proxy_info(ma_mock_url_request_t *url_request, ma_proxy_list_t *proxy_list) {    
    url_request->proxy_list = proxy_list ;
    return MA_OK ;
}

MA_CPP(})

#endif  //// endif (MA_NETWORK_USE_MOCK)

