#ifndef MA_MOCK_REQUEST_H_INCLUDED
#define MA_MOCK_REQUEST_H_INCLUDED

#include "ma/ma_common.h"
#include "ma/internal/utils/network/ma_url_request_base.h"

MA_CPP(extern "C" {)

typedef struct ma_mock_url_request_s {
	ma_url_request_t base ;
	ma_url_request_auth_t *auth ;               //authentication -- user/passwd
    ma_url_request_ssl_info_t *ssl_info ;       //SSL info
    ma_proxy_list_t *proxy_list ;					//external proxy to be used    
	ma_url_request_range_t *range_info ;        //range -- start and end

	// for user to set so that user can verify it.
	// mock handler will do mock upload/download for these info.
	// tester needs to set these info.
	size_t block_size;
	size_t buffer_bytes_count;

	//content processed.
	size_t bytes_processed;

	//cancel requested.
	ma_bool_t cancel_request;
}ma_mock_url_request_t ;

ma_error_t ma_mock_url_request_create(const char *url, ma_mock_url_request_t **curl_url_request) ;

MA_CPP(})

#endif  //MA_CURL_REQUEST_H_INCLUDED