
#if defined (MA_WINDOWS) || defined(MACX)
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "unity.h"
#include "unity_internals.h" 
#include "unity_fixture.h"

#include "ma/proxy/ma_proxy.h"
#include "ma/internal/utils/proxy/ma_proxy_internal.h"

TEST_GROUP_RUNNER(ma_network_proxy_autodetect_test_group) {
    printf(" Tests for autotedect proxy ... \n");
    do {RUN_TEST_CASE(ma_network_proxy_autodetect_tests, ma_network_proxy_autodetect_test)} while (0);
}

TEST_SETUP(ma_network_proxy_autodetect_tests) {
}

TEST_TEAR_DOWN(ma_network_proxy_autodetect_tests) {
}

TEST(ma_network_proxy_autodetect_tests, ma_network_proxy_autodetect_test) {
    ma_proxy_list_t *proxy_list = NULL ;
    ma_proxy_t *get_proxy = NULL ;
	int index =1;
	ma_proxy_list_create(&proxy_list);
    //TEST_ASSERT_MESSAGE(MA_OK == ma_proxy_config_detect_system_proxy(NULL, "http://www.google.com", proxy_list),"auto detect proxy test failed") ;
    do {
		get_proxy = NULL;
        ma_proxy_list_get_proxy(proxy_list, index++, &get_proxy);
        if(get_proxy) {
			printf("host = %s\n", get_proxy->server) ;
			printf("port = %d\n", get_proxy->server_port) ;
			printf("user = %s\n", get_proxy->user_name) ;
			printf("password = %s\n", get_proxy->user_passwd) ;            
        }
		ma_proxy_release(get_proxy);		
    }	
    while(NULL != get_proxy) ;
	ma_proxy_list_release(proxy_list);

    index = 1;
	ma_proxy_list_create(&proxy_list);
    //TEST_ASSERT_MESSAGE(MA_OK == ma_proxy_config_detect_system_proxy(NULL, "ftp://www.google.com", proxy_list),"auto detect proxy test failed") ;
    do {
		get_proxy = NULL;
        ma_proxy_list_get_proxy(proxy_list, index++, &get_proxy);
        if(get_proxy) {
			printf("host = %s\n", get_proxy->server) ;
			printf("port = %d\n", get_proxy->server_port) ;
			printf("user = %s\n", get_proxy->user_name) ;
			printf("password = %s\n", get_proxy->user_passwd) ;            
        }
		ma_proxy_release(get_proxy);		
    }	
    while(NULL != get_proxy) ;
	ma_proxy_list_release(proxy_list);
}

#endif