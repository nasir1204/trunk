#include <string.h>
#include <stdlib.h>
#include <stdio.h>


#include <ma/internal/utils/network/ma_net_interface.h>

void fill_address_info(ma_net_address_info_t **addr ,char *ip_addr, char *mask_addr, char *net_addr, ma_uint32_t scope_id,ma_ip_family_t family) {
	if(addr == NULL)
		return;
	*addr = (ma_net_address_info_t *) calloc(1,sizeof(ma_net_address_info_t));
	if(*addr == NULL)
		return;
	
	strncpy((*addr)->ip_addr,ip_addr,MA_IPV6_ADDR_LEN);
	strncpy((*addr)->subnet_mask, mask_addr,MA_IPV6_ADDR_LEN);
	strncpy((*addr)->broadcast_addr,net_addr,MA_IPV6_ADDR_LEN);
	(*addr)->ipv6_scope_id = scope_id;
	(*addr)->family = family;
}


void create_test_net_interface(ma_net_interface_t **inf,ma_int32_t index) {
	if(inf == NULL)
		return;

	*inf = (ma_net_interface_t*) calloc(1,sizeof(ma_net_interface_t));
	if(*inf == NULL)
		return ;

	(*inf)->index = index;
}

