#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "unity.h"
#include "unity_fixture.h"
#include "unity_internals.h"

#include "ma//internal//utils//platform//ma_net_interface_list_internal.h"

void fill_address_info(ma_net_address_info_t **addr ,char *ip_addr, char *mask_addr, char *net_addr, ma_uint32_t scope_id,ma_ip_family_t family);
void create_test_net_interface(ma_net_interface_t **inf,ma_int32_t index) ;

TEST_GROUP_RUNNER(ma_net_interface_tests_group) {
	do {RUN_TEST_CASE(ma_net_interface_tests, ma_net_interface_list_create_test)} while (0);
	do {RUN_TEST_CASE(ma_net_interface_tests, ma_net_interface_list_release_test)} while (0);
	do {RUN_TEST_CASE(ma_net_interface_tests, ma_net_interface_list_add_interface_test)} while (0);
	do {RUN_TEST_CASE(ma_net_interface_tests, ma_get_sytem_network_family_test)} while (0);
	do {RUN_TEST_CASE(ma_net_interface_tests, ma_net_interface_create_test)} while (0);
	do {RUN_TEST_CASE(ma_net_interface_tests, ma_net_interface_release_test)} while (0);
	do {RUN_TEST_CASE(ma_net_interface_tests, ma_net_interface_set_ipfamily_test)} while (0);
	do {RUN_TEST_CASE(ma_net_interface_tests, ma_net_interface_add_address_test)} while (0);
	do {RUN_TEST_CASE(ma_net_interface_tests, ma_address_info_create_test)} while (0);
	do {RUN_TEST_CASE(ma_net_interface_tests, ma_address_info_release_test)} while (0);


}


TEST_SETUP(ma_net_interface_tests) {}
TEST_TEAR_DOWN(ma_net_interface_tests) {}


TEST(ma_net_interface_tests,ma_net_interface_list_create_test) {
	ma_error_t error = MA_OK;
	ma_net_interface_list_t *list = NULL;

	error = ma_net_interface_list_create(&list);
	TEST_ASSERT(error == MA_OK && list != NULL);

	error = ma_net_interface_list_release(list);
	TEST_ASSERT(error == MA_OK);
	list = NULL;

	error = ma_net_interface_list_create(NULL);
	TEST_ASSERT(error == MA_ERROR_INVALIDARG);
}

TEST(ma_net_interface_tests,ma_net_interface_list_release_test) {
	ma_error_t error = MA_OK;
	ma_net_interface_list_t *list = NULL;

	error = ma_net_interface_list_create(&list);
	TEST_ASSERT(error == MA_OK && list != NULL);

	error = ma_net_interface_list_release(list);
	TEST_ASSERT(error == MA_OK);
	list = NULL;

	error = ma_net_interface_list_release(list);
	TEST_ASSERT(error == MA_ERROR_INVALIDARG);
}

TEST(ma_net_interface_tests, ma_net_interface_list_add_interface_test) {
	ma_net_interface_list_t *list = NULL;
	ma_net_interface_t *inf = NULL;
	ma_net_address_info_t *addr = NULL;
	ma_error_t error = MA_OK;

	error = ma_net_interface_list_create(&list);
	TEST_ASSERT(error == MA_OK && list != NULL);

	//Create a interface of index 1 with a single ipv4 address and add it to the list
	fill_address_info(&addr, "1.1.1.1","255.255.255.0","1.1.1.255",0,MA_IPFAMILY_IPV4);
	TEST_ASSERT_MESSAGE(addr != NULL,"Creation of address info failed. Indicate out of memory");

	create_test_net_interface(&inf, 1);
	TEST_ASSERT_MESSAGE(inf != NULL, "Creation of interface failed because of out of memory");

	ma_net_interface_add_address(inf,&addr);
	TEST_ASSERT(addr == NULL);
	//add it to the list

	ma_net_interface_list_add_interface(list,&inf);
	TEST_ASSERT(inf == NULL && MA_SLIST_GET_COUNT(list,interfaces) == 1 );

	//Create another interface with different index and add it to the list
	fill_address_info(&addr, "2.2.2.2","255.255.255.0","1.1.1.255",0,MA_IPFAMILY_IPV4);
	TEST_ASSERT_MESSAGE(addr != NULL,"Creation of address info failed. Indicate out of memory");

	create_test_net_interface(&inf, 2);
	TEST_ASSERT_MESSAGE(inf != NULL, "Creation of interface failed because of out of memory");

	ma_net_interface_add_address(inf,&addr);
	TEST_ASSERT(addr == NULL);
	//add it to the list

	ma_net_interface_list_add_interface(list,&inf);
	TEST_ASSERT(inf == NULL && MA_SLIST_GET_COUNT(list,interfaces) == 2 );

	//Create an IPv6 and add it to interface with index 2
	fill_address_info(&addr, "fe80::aaaa:bbbb:cccc:dddd","N/A","N/A",3,MA_IPFAMILY_IPV6);
	TEST_ASSERT_MESSAGE(addr != NULL,"Creation of address info failed. Indicate out of memory");

	create_test_net_interface(&inf, 2);
	TEST_ASSERT_MESSAGE(inf != NULL, "Creation of interface failed because of out of memory");

	ma_net_interface_add_address(inf,&addr);
	TEST_ASSERT(addr == NULL);
	//add it to the list

	ma_net_interface_list_add_interface(list,&inf);
	TEST_ASSERT(inf == NULL && MA_SLIST_GET_COUNT(list,interfaces) == 2 );
	MA_SLIST_FOREACH(list,interfaces,inf){
		if(inf->index == 2) {
			TEST_ASSERT(MA_SLIST_GET_COUNT(inf,addresses)==2);
			TEST_ASSERT(inf->family == MA_IPFAMILY_DUAL);
		}
		if(inf->index == 1) {
			TEST_ASSERT(MA_SLIST_GET_COUNT(inf,addresses)==1);
			TEST_ASSERT(inf->family == MA_IPFAMILY_IPV4);
		}
	}


	error = ma_net_interface_list_release(list);
	TEST_ASSERT(error == MA_OK);
}
//
//ma_net_interface_list_scan
//ma_net_interface_list_reset
//ma_net_interface_list_rescan

TEST(ma_net_interface_tests,ma_get_sytem_network_family_test) {
	ma_net_interface_list_t *list = NULL;
	ma_net_interface_t *inf = NULL;
	ma_net_address_info_t *addr = NULL;
	ma_error_t error = MA_OK;
	ma_ip_family_t family;

	error = ma_net_interface_list_create(&list);
	TEST_ASSERT(error == MA_OK && list != NULL);

	error = ma_get_system_network_family(NULL,&family);
	TEST_ASSERT(error == MA_ERROR_INVALIDARG);

	error = ma_get_system_network_family(NULL,&family);
	TEST_ASSERT(error == MA_ERROR_INVALIDARG);

	error = ma_get_system_network_family(list,&family);
	TEST_ASSERT(error == MA_OK && family == MA_IPFAMILY_UNKNOWN);

	//Create a interface of index 1 with a single ipv4 address and add it to the list
	fill_address_info(&addr, "1.1.1.1","255.255.255.0","1.1.1.255",0,MA_IPFAMILY_IPV4);
	TEST_ASSERT_MESSAGE(addr != NULL,"Creation of address info failed. Indicate out of memory");

	create_test_net_interface(&inf, 1);
	TEST_ASSERT_MESSAGE(inf != NULL, "Creation of interface failed because of out of memory");

	ma_net_interface_add_address(inf,&addr);
	TEST_ASSERT(addr == NULL);
	//add it to the list

	ma_net_interface_list_add_interface(list,&inf);
	TEST_ASSERT(inf == NULL && MA_SLIST_GET_COUNT(list,interfaces) == 1 );
	MA_SLIST_FOREACH(list,interfaces,inf){
		if(inf->index == 1) {
			TEST_ASSERT(MA_SLIST_GET_COUNT(inf,addresses)==1);
			TEST_ASSERT(inf->family == MA_IPFAMILY_IPV4);
		}
	}

	error = ma_get_system_network_family(list,&family);
	TEST_ASSERT(error == MA_OK && family == MA_IPFAMILY_IPV4);

	//Create another interface with different index and add it to the list
	fill_address_info(&addr, "2.2.2.2","255.255.255.0","1.1.1.255",0,MA_IPFAMILY_IPV4);
	TEST_ASSERT_MESSAGE(addr != NULL,"Creation of address info failed. Indicate out of memory");

	create_test_net_interface(&inf, 2);
	TEST_ASSERT_MESSAGE(inf != NULL, "Creation of interface failed because of out of memory");

	ma_net_interface_add_address(inf,&addr);
	TEST_ASSERT(addr == NULL);
	//add it to the list

	ma_net_interface_list_add_interface(list,&inf);
	TEST_ASSERT(inf == NULL && MA_SLIST_GET_COUNT(list,interfaces) == 2 );
	MA_SLIST_FOREACH(list,interfaces,inf){
		if(inf->index == 1) {
			TEST_ASSERT(MA_SLIST_GET_COUNT(inf,addresses)==1);
			TEST_ASSERT(inf->family == MA_IPFAMILY_IPV4);
		}
		if(inf->index == 2) {
			TEST_ASSERT(MA_SLIST_GET_COUNT(inf,addresses)==1);
			TEST_ASSERT(inf->family == MA_IPFAMILY_IPV4);
		}
	}

	error = ma_get_system_network_family(list,&family);
	TEST_ASSERT(error == MA_OK && family == MA_IPFAMILY_IPV4);

	//Create an IPv6 and add it to interface with index 2
	fill_address_info(&addr, "fe80::aaaa:bbbb:cccc:dddd","N/A","N/A",3,MA_IPFAMILY_IPV6);
	TEST_ASSERT_MESSAGE(addr != NULL,"Creation of address info failed. Indicate out of memory");

	create_test_net_interface(&inf, 2);
	TEST_ASSERT_MESSAGE(inf != NULL, "Creation of interface failed because of out of memory");

	ma_net_interface_add_address(inf,&addr);
	TEST_ASSERT(addr == NULL);
	//add it to the list

	ma_net_interface_list_add_interface(list,&inf);
	TEST_ASSERT(inf == NULL && MA_SLIST_GET_COUNT(list,interfaces) == 2 );
	MA_SLIST_FOREACH(list,interfaces,inf){
		if(inf->index == 2) {
			TEST_ASSERT(MA_SLIST_GET_COUNT(inf,addresses)==2);
			TEST_ASSERT(inf->family == MA_IPFAMILY_DUAL);
		}
		if(inf->index == 1) {
			TEST_ASSERT(MA_SLIST_GET_COUNT(inf,addresses)==1);
			TEST_ASSERT(inf->family == MA_IPFAMILY_IPV4);
		}
	}

	error = ma_get_system_network_family(list,&family);
	TEST_ASSERT(error == MA_OK && family == MA_IPFAMILY_DUAL);

	//Create an IPv6 and add it to interface with index 2
	fill_address_info(&addr, "fe80::eeee:ffff:cccc:dddd","N/A","N/A",3,MA_IPFAMILY_IPV6);
	TEST_ASSERT_MESSAGE(addr != NULL,"Creation of address info failed. Indicate out of memory");

	create_test_net_interface(&inf, 1);
	TEST_ASSERT_MESSAGE(inf != NULL, "Creation of interface failed because of out of memory");

	ma_net_interface_add_address(inf,&addr);
	TEST_ASSERT(addr == NULL);
	//add it to the list

	ma_net_interface_list_add_interface(list,&inf);
	TEST_ASSERT(inf == NULL && MA_SLIST_GET_COUNT(list,interfaces) == 2 );
	MA_SLIST_FOREACH(list,interfaces,inf){
		if(inf->index == 2) {
			TEST_ASSERT(MA_SLIST_GET_COUNT(inf,addresses)==2);
			TEST_ASSERT(inf->family == MA_IPFAMILY_DUAL);
		}
		if(inf->index == 1) {
			TEST_ASSERT(MA_SLIST_GET_COUNT(inf,addresses)==2);
			TEST_ASSERT(inf->family == MA_IPFAMILY_DUAL);
		}
	}


	error = ma_get_system_network_family(list,&family);
	TEST_ASSERT(error == MA_OK && family == MA_IPFAMILY_DUAL);

	error = ma_net_interface_list_release(list);
	TEST_ASSERT(error == MA_OK);
	list = NULL;

	error = ma_net_interface_list_create(&list);
	TEST_ASSERT(error == MA_OK && list != NULL);


	//Create an IPv6 and add it to interface with index 2
	fill_address_info(&addr, "fe80::eeee:ffff:cccc:dddd","N/A","N/A",3,MA_IPFAMILY_IPV6);
	TEST_ASSERT_MESSAGE(addr != NULL,"Creation of address info failed. Indicate out of memory");

	create_test_net_interface(&inf, 1);
	TEST_ASSERT_MESSAGE(inf != NULL, "Creation of interface failed because of out of memory");

	ma_net_interface_add_address(inf,&addr);
	TEST_ASSERT(addr == NULL);
	//add it to the list

	ma_net_interface_list_add_interface(list,&inf);
	TEST_ASSERT(inf == NULL && MA_SLIST_GET_COUNT(list,interfaces) == 1 );
	MA_SLIST_FOREACH(list,interfaces,inf){
		if(inf->index == 1) {
			TEST_ASSERT(MA_SLIST_GET_COUNT(inf,addresses)==1);
			TEST_ASSERT(inf->family == MA_IPFAMILY_IPV6);
		}
	}

	error = ma_get_system_network_family(list,&family);
	TEST_ASSERT(error == MA_OK && family == MA_IPFAMILY_IPV6);
	//Create an IPv6 and add it to interface with index 2
	fill_address_info(&addr, "fe80::aaaa:bbbb:cccc:dddd","N/A","N/A",3,MA_IPFAMILY_IPV6);
	TEST_ASSERT_MESSAGE(addr != NULL,"Creation of address info failed. Indicate out of memory");

	create_test_net_interface(&inf, 2);
	TEST_ASSERT_MESSAGE(inf != NULL, "Creation of interface failed because of out of memory");

	ma_net_interface_add_address(inf,&addr);
	TEST_ASSERT(addr == NULL);
	//add it to the list

	ma_net_interface_list_add_interface(list,&inf);
	TEST_ASSERT(inf == NULL && MA_SLIST_GET_COUNT(list,interfaces) == 2 );
	MA_SLIST_FOREACH(list,interfaces,inf){
		if(inf->index == 1) {
			TEST_ASSERT(MA_SLIST_GET_COUNT(inf,addresses)==1);
			TEST_ASSERT(inf->family == MA_IPFAMILY_IPV6);
		}
		if(inf->index == 2) {
			TEST_ASSERT(MA_SLIST_GET_COUNT(inf,addresses)==1);
			TEST_ASSERT(inf->family == MA_IPFAMILY_IPV6);
		}
	}
	error = ma_get_system_network_family(list,&family);
	TEST_ASSERT(error == MA_OK && family == MA_IPFAMILY_IPV6);

	//Create another interface with different index and add it to the list
	fill_address_info(&addr, "2.2.2.2","255.255.255.0","1.1.1.255",0,MA_IPFAMILY_IPV4);
	TEST_ASSERT_MESSAGE(addr != NULL,"Creation of address info failed. Indicate out of memory");

	create_test_net_interface(&inf, 1);
	TEST_ASSERT_MESSAGE(inf != NULL, "Creation of interface failed because of out of memory");

	ma_net_interface_add_address(inf,&addr);
	TEST_ASSERT(addr == NULL);
	//add it to the list

	ma_net_interface_list_add_interface(list,&inf);
	TEST_ASSERT(inf == NULL && MA_SLIST_GET_COUNT(list,interfaces) == 2 );
	MA_SLIST_FOREACH(list,interfaces,inf){
		if(inf->index == 1) {
			TEST_ASSERT(MA_SLIST_GET_COUNT(inf,addresses)==2);
			TEST_ASSERT(inf->family == MA_IPFAMILY_DUAL);
		}
		if(inf->index == 2) {
			TEST_ASSERT(MA_SLIST_GET_COUNT(inf,addresses)==1);
			TEST_ASSERT(inf->family == MA_IPFAMILY_IPV6);
		}
	}
	error = ma_get_system_network_family(list,&family);
	TEST_ASSERT(error == MA_OK && family == MA_IPFAMILY_DUAL);

	error = ma_net_interface_list_release(list);
	TEST_ASSERT(error == MA_OK);

}


TEST(ma_net_interface_tests,ma_net_interface_create_test) {
	ma_error_t error ;
	ma_net_interface_t *inf = NULL;

	error = ma_net_interface_create(&inf);
	TEST_ASSERT(error == MA_OK && inf != NULL);

	error = ma_net_interface_release(inf);
	inf = NULL;

	error = ma_net_interface_create(NULL);
	TEST_ASSERT(error == MA_ERROR_INVALIDARG);
}

TEST(ma_net_interface_tests,ma_net_interface_release_test){
	ma_error_t error ;
	ma_net_interface_t *inf = NULL;

	error = ma_net_interface_create(&inf);
	TEST_ASSERT(error == MA_OK && inf != NULL);

	error = ma_net_interface_release(inf);
	TEST_ASSERT(error == MA_OK);
	inf = NULL;

	error = ma_net_interface_release(NULL);
	TEST_ASSERT(error == MA_ERROR_INVALIDARG);
}

TEST(ma_net_interface_tests,ma_net_interface_set_ipfamily_test) {
	ma_error_t error ;
	ma_net_interface_t * inf;

	error = ma_net_interface_create(&inf);
	TEST_ASSERT(error == MA_OK && inf->family == MA_IPFAMILY_UNKNOWN);

	error = ma_net_interface_set_ipfamily(inf,MA_IPFAMILY_UNKNOWN);
	TEST_ASSERT(error == MA_OK && inf->family == MA_IPFAMILY_UNKNOWN);

	error = ma_net_interface_set_ipfamily(inf,MA_IPFAMILY_IPV4);
	TEST_ASSERT(error == MA_OK && inf->family == MA_IPFAMILY_IPV4);

	error = ma_net_interface_set_ipfamily(inf,MA_IPFAMILY_UNKNOWN);
	TEST_ASSERT(error == MA_OK && inf->family == MA_IPFAMILY_UNKNOWN);

	error = ma_net_interface_set_ipfamily(inf,MA_IPFAMILY_IPV6);
	TEST_ASSERT(error == MA_OK && inf->family == MA_IPFAMILY_IPV6);

	error = ma_net_interface_set_ipfamily(inf,MA_IPFAMILY_UNKNOWN);
	TEST_ASSERT(error == MA_OK && inf->family == MA_IPFAMILY_UNKNOWN);

	error = ma_net_interface_set_ipfamily(inf,MA_IPFAMILY_DUAL);
	TEST_ASSERT(error == MA_OK && inf->family == MA_IPFAMILY_DUAL);

	error = ma_net_interface_set_ipfamily(inf,MA_IPFAMILY_UNKNOWN);
	TEST_ASSERT(error == MA_OK && inf->family == MA_IPFAMILY_UNKNOWN);

	error = ma_net_interface_set_ipfamily(inf,MA_IPFAMILY_IPV4);
	TEST_ASSERT(error == MA_OK && inf->family == MA_IPFAMILY_IPV4);

	error = ma_net_interface_set_ipfamily(inf,MA_IPFAMILY_IPV6);
	TEST_ASSERT(error == MA_OK && inf->family == MA_IPFAMILY_DUAL);

	error = ma_net_interface_set_ipfamily(inf,MA_IPFAMILY_UNKNOWN);
	TEST_ASSERT(error == MA_OK && inf->family == MA_IPFAMILY_UNKNOWN);

	error = ma_net_interface_set_ipfamily(inf,MA_IPFAMILY_IPV6);
	TEST_ASSERT(error == MA_OK && inf->family == MA_IPFAMILY_IPV6);
	error = ma_net_interface_set_ipfamily(inf,MA_IPFAMILY_IPV4);
	TEST_ASSERT(error == MA_OK && inf->family == MA_IPFAMILY_DUAL);
	error = ma_net_interface_release(inf);
}

TEST(ma_net_interface_tests,ma_net_interface_add_address_test) {
	ma_error_t error = MA_OK;
	ma_net_interface_t *inf = NULL;
	ma_net_address_info_t *addr = NULL;

	error = ma_net_interface_create(&inf);
	TEST_ASSERT(error == MA_OK && inf->family == MA_IPFAMILY_UNKNOWN);

	fill_address_info(&addr,"1.1.1.1","255.255.255.0","1.1.1.255",0,MA_IPFAMILY_IPV4);
	TEST_ASSERT_MESSAGE(addr != NULL, "Memory allocation failure");
	error = ma_net_interface_add_address(inf,&addr);
	TEST_ASSERT(error == MA_OK && MA_SLIST_GET_COUNT(inf,addresses) == 1 && inf->family == MA_IPFAMILY_IPV4 && addr == NULL);

	fill_address_info(&addr, "1.1.1.1","255.255.255.0","1.1.1.255",0,MA_IPFAMILY_IPV4);
	TEST_ASSERT_MESSAGE(addr != NULL, "Memory allocation failure");
	error = ma_net_interface_add_address(inf,&addr);
	TEST_ASSERT(error == MA_OK && MA_SLIST_GET_COUNT(inf,addresses) == 1 && inf->family == MA_IPFAMILY_IPV4 && addr == NULL);

	fill_address_info(&addr,"fe80::aaaa:bbbb:cccc:dddd","N/A","N/A",1,MA_IPFAMILY_IPV6);
	TEST_ASSERT_MESSAGE(addr != NULL, "Memory allocation failure");
	error = ma_net_interface_add_address(inf,&addr);
	TEST_ASSERT(error == MA_OK && MA_SLIST_GET_COUNT(inf,addresses) == 2 && inf->family == MA_IPFAMILY_DUAL && addr == NULL);

	fill_address_info(&addr,"fe80::aaaa:bbbb:cccc:dddd","N/A","N/A",1,MA_IPFAMILY_IPV6);
	TEST_ASSERT_MESSAGE(addr != NULL, "Memory allocation failure");
	error = ma_net_interface_add_address(inf,&addr);
	TEST_ASSERT(error == MA_OK && MA_SLIST_GET_COUNT(inf,addresses) == 2 && inf->family == MA_IPFAMILY_DUAL && addr == NULL);

	fill_address_info(&addr, "1.1.1.1","255.255.255.0","1.1.1.255",0,MA_IPFAMILY_IPV4);
	TEST_ASSERT_MESSAGE(addr != NULL, "Memory allocation failure");
	error = ma_net_interface_add_address(inf,&addr);
	TEST_ASSERT(error == MA_OK && MA_SLIST_GET_COUNT(inf,addresses) == 2 && inf->family == MA_IPFAMILY_DUAL && addr == NULL);

	fill_address_info(&addr,"2001:450:1f01:107::dada","N/A","N/A",1,MA_IPFAMILY_IPV6);
	TEST_ASSERT_MESSAGE(addr != NULL, "Memory allocation failure");
	error = ma_net_interface_add_address(inf,&addr);
	TEST_ASSERT(error == MA_OK && MA_SLIST_GET_COUNT(inf,addresses) == 3 && inf->family == MA_IPFAMILY_DUAL && addr == NULL);

	ma_net_interface_release(inf);
	inf = NULL;

	error = ma_net_interface_create(&inf);
	TEST_ASSERT(error == MA_OK && inf->family == MA_IPFAMILY_UNKNOWN);


	fill_address_info(&addr,"fe80::aaaa:bbbb:cccc:dddd","N/A","N/A",1,MA_IPFAMILY_IPV6);
	TEST_ASSERT_MESSAGE(addr != NULL, "Memory allocation failure");
	error = ma_net_interface_add_address(inf,&addr);
	TEST_ASSERT(error == MA_OK && MA_SLIST_GET_COUNT(inf,addresses) == 1 && inf->family == MA_IPFAMILY_IPV6 && addr == NULL);

	fill_address_info(&addr,"fe80::aaaa:bbbb:cccc:dddd","N/A","N/A",1,MA_IPFAMILY_IPV6);
	TEST_ASSERT_MESSAGE(addr != NULL, "Memory allocation failure");
	error = ma_net_interface_add_address(inf,&addr);
	TEST_ASSERT(error == MA_OK && MA_SLIST_GET_COUNT(inf,addresses) == 1 && inf->family == MA_IPFAMILY_IPV6 && addr == NULL);

	fill_address_info(&addr,"2001:450:1f01:107::dada","N/A","N/A",1,MA_IPFAMILY_IPV6);
	TEST_ASSERT_MESSAGE(addr != NULL, "Memory allocation failure");
	error = ma_net_interface_add_address(inf,&addr);
	TEST_ASSERT(error == MA_OK && MA_SLIST_GET_COUNT(inf,addresses) == 2 && inf->family == MA_IPFAMILY_IPV6 && addr == NULL);

	fill_address_info(&addr, "1.1.1.1","255.255.255.0","1.1.1.255",0,MA_IPFAMILY_IPV4);
	TEST_ASSERT_MESSAGE(addr != NULL, "Memory allocation failure");
	error = ma_net_interface_add_address(inf,&addr);
	TEST_ASSERT(error == MA_OK && MA_SLIST_GET_COUNT(inf,addresses) == 3 && inf->family == MA_IPFAMILY_DUAL && addr == NULL);

	fill_address_info(&addr, "1.1.1.1","255.255.255.0","1.1.1.255",0,MA_IPFAMILY_IPV4);
	TEST_ASSERT_MESSAGE(addr != NULL, "Memory allocation failure");
	error = ma_net_interface_add_address(inf,&addr);
	TEST_ASSERT(error == MA_OK && MA_SLIST_GET_COUNT(inf,addresses) == 3 && inf->family == MA_IPFAMILY_DUAL && addr == NULL);


	ma_net_interface_release(inf);
	inf = NULL;
}


TEST(ma_net_interface_tests,ma_address_info_create_test) {
	ma_error_t error = MA_OK;
	ma_net_address_info_t *addr = NULL;

	error = ma_address_info_create(&addr);
	TEST_ASSERT(error == MA_OK && addr != NULL);

	error = ma_address_info_release(addr);
	TEST_ASSERT(error == MA_OK);
	addr = NULL;

	error = ma_address_info_create(NULL);
	TEST_ASSERT(error == MA_ERROR_INVALIDARG);


}

TEST(ma_net_interface_tests,ma_address_info_release_test) {
	ma_error_t error = MA_OK;
	ma_net_address_info_t *addr = NULL;

	error = ma_address_info_create(&addr);
	TEST_ASSERT(error == MA_OK && addr != NULL);

	error = ma_address_info_release(addr);
	TEST_ASSERT(error == MA_OK);
	addr = NULL;

	error = ma_address_info_release(NULL);
	TEST_ASSERT(error == MA_ERROR_INVALIDARG);
}
