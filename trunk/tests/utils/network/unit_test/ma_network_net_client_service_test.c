#include "unity.h"
#include "unity_fixture.h"
#include "unity_internals.h"

#include "ma/internal/utils/network/ma_net_common.h"
#include "ma/internal/utils/network/ma_net_client_service.h"

ma_event_loop_t *event_loop ;

TEST_GROUP_RUNNER(ma_network_net_client_services_test_group) {
    do {RUN_TEST_CASE(ma_network_net_client_services_tests, ma_net_client_service_create_test)} while (0);
    do {RUN_TEST_CASE(ma_network_net_client_services_tests, ma_net_client_service_release_test)} while (0);
    do {RUN_TEST_CASE(ma_network_net_client_services_tests, ma_net_client_service_start_stop_test)} while (0);
}

TEST_SETUP(ma_network_net_client_services_tests) {
}

TEST_TEAR_DOWN(ma_network_net_client_services_tests) {
}

TEST(ma_network_net_client_services_tests, ma_net_client_service_create_test) {
    ma_net_client_service_t *net_service = NULL;

    TEST_ASSERT_MESSAGE(MA_OK == ma_event_loop_create(&event_loop),"Event loop create Failed") ;
    TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == ma_net_client_service_create(NULL, &net_service),"net client create Failed") ;
    TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == ma_net_client_service_create(event_loop, NULL),"net client create Failed");

    TEST_ASSERT_MESSAGE(MA_OK == ma_net_client_service_create(event_loop, &net_service),"net client create Failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_net_client_service_release(net_service),"net client release Failed");

	TEST_ASSERT_MESSAGE(MA_OK == ma_event_loop_release(event_loop),"Event loop release Failed");
}

TEST(ma_network_net_client_services_tests, ma_net_client_service_release_test) {
    ma_net_client_service_t *net_service = NULL;

    TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == ma_net_client_service_release(net_service),"net client release Failed") ;

    TEST_ASSERT_MESSAGE(MA_OK == ma_event_loop_create(&event_loop),"Event loop create Failed") ;

    TEST_ASSERT_MESSAGE(MA_OK == ma_net_client_service_create(event_loop, &net_service),"net client create Failed") ;
    TEST_ASSERT_MESSAGE(MA_OK == ma_net_client_service_release(net_service),"net client release Failed") ;
    net_service = NULL ;
    TEST_ASSERT_MESSAGE(MA_OK != ma_net_client_service_release(net_service),"net client release Failed") ;

	TEST_ASSERT_MESSAGE(MA_OK == ma_event_loop_release(event_loop),"Event loop release Failed");
}

TEST(ma_network_net_client_services_tests, ma_net_client_service_start_stop_test) {
    ma_net_client_service_t *net_service = NULL;

	// Start and stop fails when the service object is NULL
	TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == ma_net_client_service_start(net_service),"net client service start failed") ;
    TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == ma_net_client_service_stop(net_service),"net client service start failed") ;

    TEST_ASSERT_MESSAGE(MA_OK == ma_event_loop_create(&event_loop),"Event loop create Failed") ;
    TEST_ASSERT_MESSAGE(MA_OK == ma_net_client_service_create(event_loop, &net_service),"net client create Failed") ;
	TEST_ASSERT_MESSAGE(MA_OK == ma_net_client_service_start(net_service),"net client service start failed") ;
    TEST_ASSERT_MESSAGE(MA_OK == ma_net_client_service_stop(net_service),"net client service start failed") ;
    TEST_ASSERT_MESSAGE(MA_OK == ma_net_client_service_release(net_service),"net client release Failed") ;
	TEST_ASSERT_MESSAGE(MA_OK == ma_event_loop_release(event_loop),"Event loop release Failed");
}

