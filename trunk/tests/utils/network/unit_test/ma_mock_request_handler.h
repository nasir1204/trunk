#ifndef MA_MOCK_REQUEST_HANDLER_H_INCLUDED
#define MA_MOCK_REQUEST_HANDLER_H_INCLUDED

#include "ma/internal/utils/threading/ma_atomic.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/utils/network/ma_url_request_handler.h"

MA_CPP(extern "C" {)

typedef struct ma_mock_request_handler_s {
	ma_url_request_handler_t base ;
	//implemention data.
	ma_bool_t running_state ;
	ma_event_loop_t *ma_loop ;
	ma_atomic_counter_t running_handles ;   //currently active easy handles
    void *data ;							//any data??
}ma_mock_request_handler_t ;

ma_error_t ma_mock_request_handler_create(ma_event_loop_t* event_loop, ma_url_request_handler_t** mock_req_handler) ;

MA_CPP(})

#endif