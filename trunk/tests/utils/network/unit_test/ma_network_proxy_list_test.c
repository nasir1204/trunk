
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "unity.h"
#include "unity_internals.h" 
#include "unity_fixture.h"

#include "ma/internal/utils/proxy/ma_proxy_internal.h"

TEST_GROUP_RUNNER(ma_network_proxy_list_test_group) {
    printf(" Tests for proxy_list ... \n");
    do {RUN_TEST_CASE(ma_network_proxy_list_tests, ma_network_proxy_list_create_test)} while (0);
    do {RUN_TEST_CASE(ma_network_proxy_list_tests, ma_network_proxy_list_release_test)} while (0);
    do {RUN_TEST_CASE(ma_network_proxy_list_tests, ma_network_proxy_list_add_test)} while (0);
    do {RUN_TEST_CASE(ma_network_proxy_list_tests, ma_network_proxy_list_get_next_test)} while (0);
    do {RUN_TEST_CASE(ma_network_proxy_list_tests, ma_network_proxy_list_remove_test)} while (0);
}

TEST_SETUP(ma_network_proxy_list_tests) {
}

TEST_TEAR_DOWN(ma_network_proxy_list_tests) {
}

TEST(ma_network_proxy_list_tests, ma_network_proxy_list_create_test) {
    ma_proxy_list_t *proxy_list = NULL ;

    TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == ma_proxy_list_create(NULL), "proxy list create failed") ;
    TEST_ASSERT_MESSAGE(MA_OK == ma_proxy_list_create(&proxy_list), "proxy list create failed") ;
    if(proxy_list)
        ma_proxy_list_release(proxy_list) ;
}

TEST(ma_network_proxy_list_tests, ma_network_proxy_list_release_test) {
    ma_proxy_list_t *proxy_list = NULL ;
    TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == ma_proxy_list_release(NULL), "proxy list release failed") ;
    TEST_ASSERT_MESSAGE(MA_OK == ma_proxy_list_create(&proxy_list), "proxy list create failed") ;
    TEST_ASSERT_MESSAGE(MA_OK == ma_proxy_list_release(proxy_list), "proxy list release failed") ;
}

TEST(ma_network_proxy_list_tests, ma_network_proxy_list_add_test) {
    ma_proxy_list_t *proxy_list = NULL ;
    ma_proxy_t proxy = {0} ;

    TEST_ASSERT_MESSAGE(MA_OK == ma_proxy_list_create(&proxy_list), "proxy list create failed") ;

	proxy.server = "proxy1" ;
	proxy.server_port = 8881 ;
	proxy.is_auth_rqued = MA_TRUE;
	proxy.user_name = "userp1" ;
	proxy.user_passwd = "passwdp1" ;    
    TEST_ASSERT_MESSAGE(MA_OK == ma_proxy_list_add_proxy(proxy_list, &proxy), "proxy list add1 failed") ;
    
    TEST_ASSERT_MESSAGE(MA_OK == ma_proxy_list_release(proxy_list), "proxy list release failed") ;
}

TEST(ma_network_proxy_list_tests, ma_network_proxy_list_get_next_test){    
}

TEST(ma_network_proxy_list_tests, ma_network_proxy_list_remove_test) {
}


