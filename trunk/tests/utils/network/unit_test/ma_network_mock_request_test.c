#include "unity.h"
#include "unity_internals.h" 
#include "unity_fixture.h"

#include "ma/internal/utils/network/ma_net_common.h"
#include "ma_mock_request.h"
#include "ma/internal/ma_macros.h"

#define UPLOADFILE  "..\\..\\..\\tests\\utils\\network\\unit_test\\upload\\uploadfile"
#define DOWNLOADFILE  "..\\..\\..\\tests\\utils\\network\\unit_test\\download\\downloadfile"

#define UPLOADEDFILE  "..\\..\\..\\tests\\utils\\network\\unit_test\\upload\\uploaded_file"
#define DOWNLOADEDFILE  "..\\..\\..\\tests\\utils\\network\\unit_test\\download\\downloaded_file"

#define URL "http://localhost/downloadfile"
#define DOWNLOAD_URL "http://localhost/downloadfile"
#define UPLOAD_URL "http://localhost/uploadfile"
#define BLOCK_SIZE  10000

#ifdef MA_WINDOWS
#define SLEEP(x)    Sleep(x*1000)
#else
#include <unistd.h>
#define SLEEP(x)    sleep(x)
#endif

//ma_logger_t     *msgbus_logger = NULL ;
static void final_callback(ma_error_t, long, void *, ma_url_request_t *);

static void connect_cb(void *, ma_url_request_t *) ;
static int toexit = 0 ; 
ma_event_loop_t *loop = NULL;
ma_net_client_service_t *net_service = NULL;

#define TRANSFER_TIMEOUT    1000
#define CONNECT_TIMEOUT     300
#define VERBOSE     0

TEST_GROUP_RUNNER(ma_network_mock_test_group) {
    do {RUN_TEST_CASE(ma_network_mock_tests, ma_network_mock_create_release_request_test)} while (0);
    do {RUN_TEST_CASE(ma_network_mock_tests, ma_network_mock_request_set_option_test)} while (0);
    do {RUN_TEST_CASE(ma_network_mock_tests, ma_network_mock_request_download_test)} while (0);
    do {RUN_TEST_CASE(ma_network_mock_tests, ma_network_mock_request_upload_test)} while (0);
}

TEST_SETUP(ma_network_mock_tests) {
    TEST_ASSERT_MESSAGE(MA_OK == ma_event_loop_create(&loop),"Event loop create Failed") ;
    TEST_ASSERT_MESSAGE(MA_OK == ma_net_client_service_create(loop, &net_service),"net client create Failed") ;
    TEST_ASSERT_MESSAGE(MA_OK == ma_net_client_service_start(net_service),"net client start Failed") ;
}

TEST_TEAR_DOWN(ma_network_mock_tests) {
    TEST_ASSERT_MESSAGE(MA_OK == ma_net_client_service_stop(net_service),"net client stop Failed") ;
    TEST_ASSERT_MESSAGE(MA_OK == ma_net_client_service_release(net_service),"net client release Failed") ;
    TEST_ASSERT_MESSAGE(MA_OK == ma_event_loop_release(loop),"Event loop release Failed") ;
    unlink(UPLOADEDFILE) ;
    unlink(DOWNLOADEDFILE) ;
}

TEST(ma_network_mock_tests, ma_network_mock_create_release_request_test) {
    ma_url_request_t *url_request = NULL ;

    TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == ma_url_request_create(net_service, NULL, &url_request), "request create failed") ;
    TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == ma_url_request_create(NULL, URL, &url_request), "request create failed") ;
    TEST_ASSERT_MESSAGE(MA_OK == ma_url_request_create(net_service, URL, &url_request), "request create failed") ;
    TEST_ASSERT_MESSAGE(MA_ERROR_NETWORK_ACTIVE_URL_REQUEST == ma_url_request_release(url_request),"request release failed") ;
    url_request->request_state = MA_URL_REQUEST_STATE_COMPLETED ;
    TEST_ASSERT_MESSAGE(MA_OK == ma_url_request_release(url_request),"request release failed") ;
}

TEST(ma_network_mock_tests, ma_network_mock_request_set_option_test) { 
    ma_url_request_t *url_request = NULL ;

    TEST_ASSERT_MESSAGE(MA_OK == ma_url_request_create(net_service, URL, &url_request), "request create failed") ;
    TEST_ASSERT_MESSAGE(MA_OK == ma_url_request_set_option(url_request, MA_URL_REQUEST_OPTION_REQUEST_TYPE, (unsigned long)MA_URL_REQUEST_TYPE_GET ), "request set option failed") ;
    TEST_ASSERT_MESSAGE(MA_OK == ma_url_request_set_option(url_request, MA_URL_REQUEST_OPTION_VERBOSE_LOG, (unsigned long)VERBOSE ), "request set option failed") ;
    TEST_ASSERT_MESSAGE(MA_OK == ma_url_request_set_option(url_request, MA_URL_REQUEST_OPTION_TRANSFER_TIMEOUT, (unsigned long)TRANSFER_TIMEOUT ), "request set option failed") ;
    TEST_ASSERT_MESSAGE(MA_OK == ma_url_request_set_option(url_request, MA_URL_REQUEST_OPTION_CONNECT_TIMEOUT, (unsigned long)CONNECT_TIMEOUT ), "request set option failed") ;
    ((ma_mock_url_request_t *)url_request)->block_size = 10 ;
    ((ma_mock_url_request_t *)url_request)->bytes_processed = 0 ;
    ((ma_mock_url_request_t *)url_request)->buffer_bytes_count = 0 ;
    url_request->request_state = MA_URL_REQUEST_STATE_COMPLETED ;
    TEST_ASSERT_MESSAGE(MA_OK == ma_url_request_release(url_request),"request release failed") ;
}

TEST(ma_network_mock_tests, ma_network_mock_request_download_test) {
    ma_error_t ret_code = MA_OK ;
    ma_url_request_t *url_request = NULL ;
    ma_stream_t *w_stream = NULL ;
    ma_stream_t *r_stream = NULL ;
    ma_url_request_io_t *io_cbs = NULL ;
    ma_url_request_range_t *range = NULL ;
    ma_url_request_state_t req_state ;

    printf("mock download test ......... \n") ;
    TEST_ASSERT_MESSAGE(MA_OK == ma_url_request_create(net_service, URL, &url_request), "request create failed") ;
    TEST_ASSERT_MESSAGE(MA_OK == ma_url_request_set_option(url_request, MA_URL_REQUEST_OPTION_REQUEST_TYPE, (unsigned long)MA_URL_REQUEST_TYPE_GET ), "request set option failed") ;

    io_cbs = (ma_url_request_io_t *)calloc(1, sizeof(ma_url_request_io_t)) ;
    TEST_ASSERT_MESSAGE(MA_OK == ma_fstream_create(DOWNLOADEDFILE, MA_STREAM_ACCESS_FLAG_WRITE, NULL, &w_stream), "create write stream failed") ;
    TEST_ASSERT_MESSAGE(MA_OK == ma_fstream_create(DOWNLOADFILE, MA_STREAM_ACCESS_FLAG_READ, NULL, &r_stream), "create read stream failed") ;
    MA_URL_REQUEST_IO_INIT(io_cbs) ;
    MA_URL_REQUEST_IO_SET_STREAMS(io_cbs, r_stream, w_stream) ;
    TEST_ASSERT_MESSAGE(MA_OK == ma_url_request_set_io_callback(url_request, io_cbs), " request set iocb failed") ;

    ((ma_mock_url_request_t *)url_request)->block_size = BLOCK_SIZE ;
    ((ma_mock_url_request_t *)url_request)->bytes_processed = 0 ;
     r_stream->vtable->get_size(r_stream, &(((ma_mock_url_request_t *)url_request)->buffer_bytes_count)) ;

    range = (ma_url_request_range_t *)calloc(1, sizeof(ma_url_request_range_t)) ;
    MA_URL_RANGE_INIT(range) ;
    TEST_ASSERT_MESSAGE(MA_OK == ma_url_request_set_range_info(url_request, range), " request set range failed") ;

   
    TEST_ASSERT_MESSAGE(MA_OK == ma_url_request_send(url_request, final_callback, NULL), "request send failed") ;

    SLEEP(10) ;
    TEST_ASSERT_MESSAGE(MA_OK == ma_url_request_query_state(url_request, &req_state), "request query failed") ;
    printf("request state  %d\n", req_state) ;

    SLEEP(5) ;
    TEST_ASSERT_MESSAGE(MA_OK == ma_url_request_cancel(url_request), "request cancel failed");
    printf("request cancel, downloded %d of %d\n", ((ma_mock_url_request_t *)url_request)->range_info->start_position, ((ma_mock_url_request_t *)url_request)->buffer_bytes_count) ;
    
    while(url_request->request_state != MA_URL_REQUEST_STATE_CANCELLED) {
        printf("waiting for request cancel %d\n", url_request->request_state) ;
        SLEEP(3) ;
    }

    TEST_ASSERT_MESSAGE(MA_OK == ma_url_request_query_state(url_request, &req_state), "request query failed") ;
    printf("request state  %d\n", req_state) ;

    url_request->request_state = MA_URL_REQUEST_STATE_IDLE ;
    TEST_ASSERT_MESSAGE(MA_OK == ma_url_request_restart(url_request),"request restart failed") ;

    while(!toexit) ;

    TEST_ASSERT_MESSAGE(MA_OK == ma_url_request_query_state(url_request, &req_state), "request query failed") ;
    printf("request state  %d\n", req_state) ;


    ma_stream_release(r_stream) ;
    ma_stream_release(w_stream) ;
    free(io_cbs) ;
    free(range) ;
    ret_code = ma_url_request_release(url_request) ;
}

TEST(ma_network_mock_tests, ma_network_mock_request_upload_test) {
    ma_error_t ret_code = MA_OK ;
    ma_url_request_t *url_request = NULL ;
    ma_stream_t *w_stream = NULL ;
    ma_stream_t *r_stream = NULL ;
    ma_url_request_io_t *io_cbs = NULL ;
    ma_url_request_range_t *range = NULL ;
    ma_url_request_state_t req_state ;

    toexit = 0 ;

    printf("mock upload test ......... \n") ;
    TEST_ASSERT_MESSAGE(MA_OK == ma_url_request_create(net_service, UPLOAD_URL, &url_request), "request create failed") ;
    TEST_ASSERT_MESSAGE(MA_OK == ma_url_request_set_option(url_request, MA_URL_REQUEST_OPTION_REQUEST_TYPE, (unsigned long)MA_URL_REQUEST_TYPE_POST ), "request set option failed") ;

    io_cbs = (ma_url_request_io_t *)calloc(1, sizeof(ma_url_request_io_t)) ;
    TEST_ASSERT_MESSAGE(MA_OK == ma_fstream_create(UPLOADEDFILE, MA_STREAM_ACCESS_FLAG_WRITE, NULL, &w_stream), "create write stream failed") ;
    TEST_ASSERT_MESSAGE(MA_OK == ma_fstream_create(UPLOADFILE, MA_STREAM_ACCESS_FLAG_READ, NULL, &r_stream), "create read stream failed") ;
    MA_URL_REQUEST_IO_INIT(io_cbs) ;
    MA_URL_REQUEST_IO_SET_STREAMS(io_cbs, r_stream, w_stream) ;
    TEST_ASSERT_MESSAGE(MA_OK == ma_url_request_set_io_callback(url_request, io_cbs), " request set iocb failed") ;

    ((ma_mock_url_request_t *)url_request)->block_size = BLOCK_SIZE ;
    ((ma_mock_url_request_t *)url_request)->bytes_processed = 0 ;
     r_stream->vtable->get_size(r_stream, &(((ma_mock_url_request_t *)url_request)->buffer_bytes_count)) ;

    range = (ma_url_request_range_t *)calloc(1, sizeof(ma_url_request_range_t)) ;
    MA_URL_RANGE_INIT(range) ;
    TEST_ASSERT_MESSAGE(MA_OK == ma_url_request_set_range_info(url_request, range), " request set range failed") ;

   
    TEST_ASSERT_MESSAGE(MA_OK == ma_url_request_send(url_request, final_callback, NULL), "request send failed") ;

    SLEEP(10) ;
    TEST_ASSERT_MESSAGE(MA_OK == ma_url_request_query_state(url_request, &req_state), "request query failed") ;
    printf("request state  %d\n", req_state) ;

    SLEEP(5) ;
    TEST_ASSERT_MESSAGE(MA_OK == ma_url_request_cancel(url_request), "request cancel failed");
    
    while(url_request->request_state != MA_URL_REQUEST_STATE_CANCELLED) {
        printf("waiting for request cancel %d\n", url_request->request_state) ;
        SLEEP(3) ;
    }

    TEST_ASSERT_MESSAGE(MA_OK == ma_url_request_query_state(url_request, &req_state), "request query failed") ;
    printf("request state  %d\n", req_state) ;

    TEST_ASSERT_MESSAGE(MA_OK == ma_stream_seek(w_stream, 0), "stream reset failed") ;
    TEST_ASSERT_MESSAGE(MA_OK == ma_stream_seek(r_stream, 0), "stream reset failed") ;
    url_request->request_state = MA_URL_REQUEST_STATE_IDLE ;
    TEST_ASSERT_MESSAGE(MA_OK == ma_url_request_restart(url_request),"request restart failed") ;

    while(!toexit);

    TEST_ASSERT_MESSAGE(MA_OK == ma_url_request_query_state(url_request, &req_state), "request query failed") ;
    printf("request state  %d\n", req_state) ;

    ma_stream_release(r_stream) ;
    ma_stream_release(w_stream) ;
    free(io_cbs) ;
    free(range) ;
    ret_code = ma_url_request_release(url_request) ;
}


static void final_callback(ma_error_t status, long response, void *data, ma_url_request_t *url_request) {

	if(MA_ERROR_NETWORK_REQUEST_SUCCEEDED == status || MA_OK == status)
        printf("%s url request done\n", url_request->url) ;
    else
        printf("request for url %s failed with response code %d, %d \n", url_request->url, response, status) ;

    toexit = 1 ;
}

static void connect_cb(void *data, ma_url_request_t *request) {
    return ;
}

