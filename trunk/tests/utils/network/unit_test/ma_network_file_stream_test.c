
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include "unity.h"
#include "unity_fixture.h"
#include "unity_internals.h" 

#include "ma/internal/utils/network/ma_stream.h"

FILE *fpSetup = NULL;

TEST_GROUP_RUNNER(ma_network_file_stream_test_group) {
	
    do {RUN_TEST_CASE(ma_network_file_stream_tests, ma_file_stream_create_test)} while (0);
    do {RUN_TEST_CASE(ma_network_file_stream_tests, ma_stream_write_test)} while (0);
    do {RUN_TEST_CASE(ma_network_file_stream_tests, ma_stream_read_test)} while (0);
    do {RUN_TEST_CASE(ma_network_file_stream_tests, ma_stream_get_size_test)} while (0);
    do {RUN_TEST_CASE(ma_network_file_stream_tests, ma_stream_get_access_mode_test)} while (0);
    do {RUN_TEST_CASE(ma_network_file_stream_tests, ma_stream_release_test)} while (0);
}

TEST_SETUP(ma_network_file_stream_tests) {

	//Cleanup any files that would have stayed back
	remove("exists.txt");
	remove("dummy_file.txt");
	remove("runtime_file.txt");

	//Create files
	fpSetup = fopen("exists.txt","w");
	TEST_ASSERT(fpSetup != NULL);
	fclose(fpSetup);
	fpSetup = NULL;
	
	fpSetup = fopen("dummy_file.txt","w");
	TEST_ASSERT(fpSetup != NULL);
	fclose(fpSetup);
	fpSetup = NULL;
}

TEST_TEAR_DOWN(ma_network_file_stream_tests) {
	
	//Delete files
	remove("exists.txt");
	printf("cleaning up exists.txt\n");
	remove("dummy_file.txt");
	printf("cleaning up dummy_file.txt\n");
	remove("runtime_file.txt");
	printf("cleaning up runtime_file.txt\n");
}

TEST(ma_network_file_stream_tests, ma_file_stream_create_test) {
	ma_error_t error = MA_OK;
	ma_stream_t * stream = NULL;

	//exsiting file with different modes
	error = ma_file_stream_create("exists.txt",MA_STREAM_ACCESS_MODE_READ,&stream);
	TEST_ASSERT(error == MA_OK && stream != NULL);
	error = ma_stream_release(stream);
	TEST_ASSERT(error == MA_OK);
	stream = NULL;
	
	error = ma_file_stream_create("dummy_file.txt",MA_STREAM_ACCESS_MODE_WRITE,&stream);
	TEST_ASSERT(error == MA_OK && stream != NULL);
	error = ma_stream_release(stream);
	TEST_ASSERT(error == MA_OK);
	stream = NULL;

	error = ma_file_stream_create("exists.txt",MA_STREAM_ACCESS_MODE_APPEND,&stream);
	TEST_ASSERT(error == MA_OK && stream != NULL);
	error = ma_stream_release(stream);
	TEST_ASSERT(error == MA_OK);
	stream = NULL;
	
	//empty filename with different modes
	error = ma_file_stream_create("",MA_STREAM_ACCESS_MODE_READ,&stream);
	TEST_ASSERT(error == MA_ERROR_FILE_OPEN_FAILED && stream == NULL);
	
	error = ma_file_stream_create("",MA_STREAM_ACCESS_MODE_WRITE,&stream);
	TEST_ASSERT(error == MA_ERROR_FILE_OPEN_FAILED && stream == NULL);

	error = ma_file_stream_create("",MA_STREAM_ACCESS_MODE_APPEND,&stream);
	TEST_ASSERT(error == MA_ERROR_FILE_OPEN_FAILED && stream == NULL);
	
	//invalid argument test with different modes
	error = ma_file_stream_create("exists.txt",MA_STREAM_ACCESS_MODE_READ, NULL);
	TEST_ASSERT(error == MA_ERROR_INVALIDARG);

	error = ma_file_stream_create("exists.txt",MA_STREAM_ACCESS_MODE_WRITE, NULL);
	TEST_ASSERT(error == MA_ERROR_INVALIDARG);

	error = ma_file_stream_create("exists.txt",MA_STREAM_ACCESS_MODE_APPEND, NULL);
	TEST_ASSERT(error == MA_ERROR_INVALIDARG);

	error = ma_file_stream_create(NULL,MA_STREAM_ACCESS_MODE_READ, &stream);
	TEST_ASSERT(error == MA_ERROR_INVALIDARG);

	//non-existent file with different file modes
	error = ma_file_stream_create("runtime_file.txt",MA_STREAM_ACCESS_MODE_READ,&stream);
	TEST_ASSERT(error == MA_ERROR_FILE_OPEN_FAILED && stream == NULL);
	
	error = ma_file_stream_create("runtime_file.txt",MA_STREAM_ACCESS_MODE_WRITE,&stream);
	TEST_ASSERT(error == MA_OK && stream != NULL);
	error = ma_stream_release(stream);
	TEST_ASSERT(error == MA_OK);
	stream = NULL;
	remove("runtime_file.txt");

	error = ma_file_stream_create("runtime_file.txt",MA_STREAM_ACCESS_MODE_APPEND,&stream);
	TEST_ASSERT(error == MA_OK && stream != NULL);
	error = ma_stream_release(stream);
	TEST_ASSERT(error == MA_OK);
	stream = NULL;
	remove("runtime_file.txt");

	//Invalid modes - This will crash the program
	//error = ma_file_stream_create("exists.txt", (ma_stream_access_mode_t)(MA_STREAM_ACCESS_MODE_READ - 1), &stream);
	//TEST_ASSERT(error == MA_ERROR_INVALIDARG);
	//error = ma_file_stream_create("exists.txt", (ma_stream_access_mode_t)(MA_STREAM_ACCESS_MODE_APPEND + 1), &stream);
	//TEST_ASSERT(error == MA_ERROR_INVALIDARG);
}

TEST(ma_network_file_stream_tests, ma_stream_write_test) {
	ma_error_t error = MA_OK;
	ma_stream_t * stream = NULL;
	size_t written_bytes = 0;

	char data[51] = "This is a unit test text to be written into file..";
	char a = 'c';
	error = ma_file_stream_create("runtime_file.txt",MA_STREAM_ACCESS_MODE_WRITE,&stream);
	TEST_ASSERT(error == MA_OK && stream != NULL);
	//Pass invalid parameters
	//Null data
	error = ma_stream_write(stream,NULL,sizeof(char),strlen(data),&a,&written_bytes);
	TEST_ASSERT(error == MA_ERROR_INVALIDARG);
	//Null size
	error = ma_stream_write(stream,data,0,strlen(data),&a,&written_bytes);//Review 
	TEST_ASSERT(error == MA_OK && written_bytes==0);
	//Null amount of data to be written
	error = ma_stream_write(stream,data,sizeof(char),0,&a,&written_bytes);//Review - should return invalid argument
	TEST_ASSERT(error == MA_OK && written_bytes==0);
	//NULL written bytes
	error = ma_stream_write(stream,data,sizeof(char),strlen(data),&a,NULL);
	TEST_ASSERT(error == MA_ERROR_INVALIDARG);
	
	//Test with actual data being written into file
	error = ma_stream_write(stream,data,sizeof(char),strlen(data),&a,&written_bytes);
	TEST_ASSERT(error == MA_OK && written_bytes == strlen(data));
	//Test with actual data being written into file & user data as NULL
	error = ma_stream_write(stream,data,sizeof(char),strlen(data),NULL,&written_bytes);
	TEST_ASSERT(error == MA_OK && written_bytes == strlen(data));
	
	error = ma_stream_release(stream);
	TEST_ASSERT(error == MA_OK);
	stream = NULL;
	
	
	{
		struct stat st = {0};
		FILE *f = NULL;
		char buffer [MAX_SERVER_NAME_LEN] = {0};
		char temp_string[MAX_SERVER_NAME_LEN] = {0};
		
		TEST_ASSERT(stat("runtime_file.txt",&st) != -1);
		TEST_ASSERT(st.st_size == 100);
		f = fopen("runtime_file.txt","r");
		TEST_ASSERT(f != NULL);
		
		TEST_ASSERT(fread(buffer,sizeof(char),st.st_size,f) == 100);
		strcat(temp_string,data);
		strcat(temp_string,data);

		TEST_ASSERT(strcmp(temp_string,buffer) == 0);

	}
	
	error = ma_file_stream_create("runtime_file.txt",MA_STREAM_ACCESS_MODE_READ,&stream);
	TEST_ASSERT(error == MA_OK && stream != NULL);
	written_bytes = -1;
	error = ma_stream_write(stream,data,sizeof(char),strlen(data),NULL,&written_bytes);
	TEST_ASSERT(error  == MA_ERROR_INVALIDARG);

	error = ma_stream_release(stream);
	TEST_ASSERT(error == MA_OK);
	stream = NULL;

	remove("runtime_file.txt");

	//NULL Stream
	error = ma_stream_write(stream,data,sizeof(char),strlen(data),NULL,&written_bytes);
	TEST_ASSERT(error == MA_ERROR_INVALIDARG);
}

TEST(ma_network_file_stream_tests, ma_stream_read_test)
{
	ma_error_t error = MA_OK;
	ma_stream_t * stream = NULL;
	size_t read_bytes = 0;

	char data[51] = "This is a unit test text to be written into file..";
	char rdata[51] = {0};
	char a = 'c';
	
	error = ma_file_stream_create("runtime_file.txt",MA_STREAM_ACCESS_MODE_WRITE,&stream);
	TEST_ASSERT(error == MA_OK && stream != NULL);
	//Write the data once
	error = ma_stream_write(stream,data,sizeof(char),strlen(data),&a,&read_bytes);
	TEST_ASSERT(error == MA_OK && read_bytes == strlen(data));
	//Read the data once
	error = ma_stream_read(stream,rdata,sizeof(char),strlen(data),&a,&read_bytes);
	TEST_ASSERT(error == MA_ERROR_INVALIDARG);
	//Close the stream
	error = ma_stream_release(stream);
	TEST_ASSERT(error == MA_OK);
	stream = NULL;
	remove("runtime_file.txt");

	error = ma_file_stream_create("runtime_file.txt",MA_STREAM_ACCESS_MODE_APPEND,&stream);
	TEST_ASSERT(error == MA_OK && stream != NULL);
	//Write the data once
	error = ma_stream_write(stream,data,sizeof(char),strlen(data),&a,&read_bytes);
	TEST_ASSERT(error == MA_OK && read_bytes == strlen(data));
	//Read the data once
	error = ma_stream_read(stream,rdata,sizeof(char),strlen(data),&a,&read_bytes);
	TEST_ASSERT(error == MA_ERROR_INVALIDARG);
	//Close the stream
	error = ma_stream_release(stream);
	TEST_ASSERT(error == MA_OK);
	stream = NULL;


	error = ma_file_stream_create("runtime_file.txt",MA_STREAM_ACCESS_MODE_READ,&stream);
	TEST_ASSERT(error == MA_OK && stream != NULL);
	
	error = ma_stream_read(stream,rdata,sizeof(char),strlen(data),&a,&read_bytes);
	TEST_ASSERT(error == MA_OK && read_bytes == strlen(data));

	//Invalid arguments
	//Null string
	error = ma_stream_read(stream,NULL,sizeof(char),strlen(data),&a,&read_bytes);
	TEST_ASSERT(error == MA_ERROR_INVALIDARG);
	//Size = 0
	error = ma_stream_read(stream,rdata,0,strlen(data),&a,&read_bytes);//Review 
	TEST_ASSERT(error == MA_OK && read_bytes==0);
	//length = 0
	error = ma_stream_read(stream,rdata,sizeof(char),0,&a,&read_bytes); //Review 
	TEST_ASSERT(error == MA_OK && read_bytes==0);
	//read bytes = NULL
	error = ma_stream_read(stream,rdata,sizeof(char),strlen(data),&a,NULL);
	TEST_ASSERT(error == MA_ERROR_INVALIDARG);

	error = ma_stream_release(stream);
	TEST_ASSERT(error == MA_OK);
	stream = NULL;
	//Null stream
	error = ma_stream_read(stream,rdata,sizeof(char),strlen(data),&a,&read_bytes);
	TEST_ASSERT(error == MA_ERROR_INVALIDARG);

	remove("runtime_file.txt");
	
}

TEST(ma_network_file_stream_tests, ma_stream_get_size_test)
{
	ma_error_t error = MA_OK;
	ma_stream_t * stream = NULL;
	size_t read_bytes = 0;
	unsigned long size= 0;
	
	char data[51] = "This is a unit test text to be written into file..";
	char rdata[51] = {0};
	char a = 'c';

	error = ma_file_stream_create("runtime_file.txt",MA_STREAM_ACCESS_MODE_WRITE,&stream);
	TEST_ASSERT(error == MA_OK && stream != NULL);
	//Write the data once
	error = ma_stream_write(stream,data,sizeof(char),strlen(data),&a,&read_bytes);
	TEST_ASSERT(error == MA_OK && read_bytes == strlen(data));
	
	//Get sizes
	//error = ma_stream_get_size(stream,&size);
	//TEST_ASSERT(error == MA_OK && size == strlen(data));
	//Close the stream
	error = ma_stream_release(stream);
	TEST_ASSERT(error == MA_OK);
	stream = NULL;
		
	error = ma_file_stream_create("runtime_file.txt",MA_STREAM_ACCESS_MODE_READ,&stream);
	TEST_ASSERT(error == MA_OK && stream != NULL);
	//Get sizes
	error = ma_stream_get_size(stream,&size);
	TEST_ASSERT(error == MA_OK && size == strlen(data));

	//Invalid Arguments
	error = ma_stream_get_size(NULL,&size);
	TEST_ASSERT(error == MA_ERROR_INVALIDARG);

	error = ma_stream_get_size(stream,NULL);
	TEST_ASSERT(error == MA_ERROR_INVALIDARG);
	//Close the stream
	error = ma_stream_release(stream);
	TEST_ASSERT(error == MA_OK);
	stream = NULL;

	remove("runtime_file.txt");
}

TEST(ma_network_file_stream_tests, ma_stream_get_access_mode_test)
{
	ma_error_t error = MA_OK;
	ma_stream_t * stream = NULL;
	ma_stream_access_mode_t mode = (ma_stream_access_mode_t)-1;

	error = ma_file_stream_create("runtime_file.txt",MA_STREAM_ACCESS_MODE_WRITE,&stream);
	TEST_ASSERT(error == MA_OK && stream != NULL);
	//Get Access mode
	error = ma_stream_get_access_mode(stream,&mode);
	TEST_ASSERT(error == MA_OK && mode == MA_STREAM_ACCESS_MODE_WRITE);
	//Close the stream
	error = ma_stream_release(stream);
	TEST_ASSERT(error == MA_OK);
	stream = NULL;

	error = ma_file_stream_create("runtime_file.txt",MA_STREAM_ACCESS_MODE_READ,&stream);
	TEST_ASSERT(error == MA_OK && stream != NULL);
	//Get Access mode
	error = ma_stream_get_access_mode(stream,&mode);
	TEST_ASSERT(error == MA_OK && mode == MA_STREAM_ACCESS_MODE_READ);

	//Close the stream
	error = ma_stream_release(stream);
	TEST_ASSERT(error == MA_OK);
	stream = NULL;

	error = ma_file_stream_create("runtime_file.txt",MA_STREAM_ACCESS_MODE_APPEND,&stream);
	TEST_ASSERT(error == MA_OK && stream != NULL);
	//Get Access mode
	error = ma_stream_get_access_mode(stream,&mode);
	TEST_ASSERT(error == MA_OK && mode == MA_STREAM_ACCESS_MODE_APPEND);

	//Close the stream
	error = ma_stream_release(stream);
	TEST_ASSERT(error == MA_OK);
	stream = NULL;
	//Get Access mode
	error = ma_stream_get_access_mode(stream,&mode);
	TEST_ASSERT(error == MA_ERROR_INVALIDARG);
	error = ma_stream_get_access_mode(stream,NULL);
	TEST_ASSERT(error == MA_ERROR_INVALIDARG);
}

TEST(ma_network_file_stream_tests, ma_stream_release_test)
{
	ma_error_t error = MA_OK;
	ma_stream_t * stream = NULL;
	ma_stream_access_mode_t mode = (ma_stream_access_mode_t)-1;

	error = ma_file_stream_create("runtime_file.txt",MA_STREAM_ACCESS_MODE_WRITE,&stream);
	TEST_ASSERT(error == MA_OK && stream != NULL);
	
	//Close the stream
	error = ma_stream_release(stream);
	TEST_ASSERT(error == MA_OK);
	stream = NULL;
	error = ma_stream_release(stream);
	TEST_ASSERT(error == MA_ERROR_INVALIDARG);
	remove("runtime_file.txt");

}