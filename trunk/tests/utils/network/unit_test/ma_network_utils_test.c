
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include "unity.h"
#include "unity_internals.h" 
#include "unity_fixture.h"

#include "ma/internal/utils/network/ma_net_common.h"
#include "ma_url_utils.h"


TEST_GROUP_RUNNER(ma_network_url_utils_test_group) {
	
    do {RUN_TEST_CASE(ma_network_url_utils_tests, ma_url_utils_get_protocol_type_test)} while (0);
    do {RUN_TEST_CASE(ma_network_url_utils_tests, ma_url_utils_ip_address_create_test)} while (0);
    do {RUN_TEST_CASE(ma_network_url_utils_tests, ma_url_utils_ipv6_address_create_with_scope_test)} while (0);
    do {RUN_TEST_CASE(ma_network_url_utils_tests, ma_url_utils_ipv6_address_create_link_local_test)} while (0);
    do {RUN_TEST_CASE(ma_network_url_utils_tests, ma_url_utils_ipv6_address_create_site_local_test)} while (0);
    do {RUN_TEST_CASE(ma_network_url_utils_tests, ma_url_utils_ipv6_address_create_global_test)} while (0);
    do {RUN_TEST_CASE(ma_network_url_utils_tests, ma_url_utils_ip_address_release_test)} while (0);
    do {RUN_TEST_CASE(ma_network_url_utils_tests, ma_url_utils_url_info_create_test)} while (0);
    do {RUN_TEST_CASE(ma_network_url_utils_tests, ma_url_utils_url_info_create_test_with_uri)} while (0);
    do {RUN_TEST_CASE(ma_network_url_utils_tests, ma_url_utils_url_info_create_test_with_uri_multi_slash)} while (0);
    do {RUN_TEST_CASE(ma_network_url_utils_tests, ma_url_utils_url_info_create_test_with_multi_level_uri)} while (0);
    do {RUN_TEST_CASE(ma_network_url_utils_tests, ma_url_utils_url_info_create_test_with_multi_level_uri_multi_slash)} while (0);
    do {RUN_TEST_CASE(ma_network_url_utils_tests, ma_url_utils_url_info_release_test)} while (0);
	do {RUN_TEST_CASE(ma_network_url_utils_tests, ma_url_utils_url_info_create_test_with_uri_multi_space)} while (0);
}

TEST_SETUP(ma_network_url_utils_tests) {

}

TEST_TEAR_DOWN(ma_network_url_utils_tests) {

}

TEST(ma_network_url_utils_tests, ma_url_utils_get_protocol_type_test) {
	ma_error_t error = MA_OK;
	const char* httpUrl = "http://localhost:8080/";
	const char* httpsUrl = "https://localhost:8080/";
	const char* ftpUrl = "ftp://localhost:8080/";
	const char* uncUrl = "unc://localhost:8080/";
	const char* fileUrl = "file://C:\\programfiles\\";
	ma_network_transfer_protocol_t url_type = (ma_network_transfer_protocol_t) -1;

	//Invalid parameter
	error = ma_url_utils_get_protocol_type(NULL,&url_type);
	TEST_ASSERT(error == MA_ERROR_INVALIDARG);
	error = ma_url_utils_get_protocol_type(httpUrl,NULL);
	TEST_ASSERT(error == MA_ERROR_INVALIDARG);
	error = ma_url_utils_get_protocol_type(ftpUrl,NULL);
	TEST_ASSERT(error == MA_ERROR_INVALIDARG);
	error = ma_url_utils_get_protocol_type(uncUrl,NULL);
	TEST_ASSERT(error == MA_ERROR_INVALIDARG);
	error = ma_url_utils_get_protocol_type(fileUrl,NULL);
	TEST_ASSERT(error == MA_ERROR_INVALIDARG);
	error = ma_url_utils_get_protocol_type("",& url_type);
	TEST_ASSERT(error == MA_ERROR_NETWORK_INVALID_URL);


	//Valid parameters
	error = ma_url_utils_get_protocol_type(httpUrl,& url_type);
	TEST_ASSERT(error == MA_OK && url_type == MA_URL_TRANSER_PROTOCOL_HTTP);
	error = ma_url_utils_get_protocol_type(httpsUrl,& url_type);
	TEST_ASSERT(error == MA_OK && url_type == MA_URL_TRANSER_PROTOCOL_HTTP);
	error = ma_url_utils_get_protocol_type(ftpUrl,& url_type);
	TEST_ASSERT(error == MA_OK && url_type == MA_URL_TRANSER_PROTOCOL_FTP);
	error = ma_url_utils_get_protocol_type(uncUrl,& url_type);
	TEST_ASSERT(error == MA_OK && url_type == MA_URL_TRANSER_PROTOCOL_UNC);
	error = ma_url_utils_get_protocol_type(fileUrl,& url_type);
	TEST_ASSERT(error == MA_OK && url_type == MA_URL_TRANSER_PROTOCOL_FILE);

}
TEST(ma_network_url_utils_tests, ma_url_utils_ip_address_create_test) {
	ma_error_t error = MA_OK;
	ma_ip_address_t *ipaddr = NULL;
	ma_bool_t is_curl = MA_FALSE;
	
	//Null ip_address_t
	is_curl = MA_FALSE;

	error = ma_url_utils_ip_address_create("127.0.0.1",is_curl,NULL);
	TEST_ASSERT(error == MA_ERROR_INVALIDARG && ipaddr == NULL);

	is_curl = MA_TRUE;

	error = ma_url_utils_ip_address_create("127.0.0.1",is_curl,NULL);
	TEST_ASSERT(error == MA_ERROR_INVALIDARG && ipaddr == NULL);

	//NULL Ip
	is_curl = MA_FALSE;

	error = ma_url_utils_ip_address_create(NULL,is_curl,&ipaddr);
	TEST_ASSERT(error == MA_ERROR_INVALIDARG && ipaddr == NULL);

	is_curl = MA_TRUE;

	error = ma_url_utils_ip_address_create(NULL,is_curl,&ipaddr);
	TEST_ASSERT(error == MA_ERROR_INVALIDARG && ipaddr == NULL);

	//Empty ip
	is_curl = MA_FALSE;
	error = ma_url_utils_ip_address_create("",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_ERROR_INVALIDARG && ipaddr == NULL);

	is_curl = MA_TRUE;
	
	error = ma_url_utils_ip_address_create("",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_ERROR_INVALIDARG && ipaddr == NULL);
	
	//IPv4 
	is_curl = MA_FALSE;

	error = ma_url_utils_ip_address_create("127.0.0.1",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"127.0.0.1")==0 
		&& ipaddr->is_curl_addr == MA_TRUE && ipaddr->if_scopeid == 0);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	is_curl = MA_TRUE;

	error = ma_url_utils_ip_address_create("127.0.0.1",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"127.0.0.1")==0 
		&& ipaddr->is_curl_addr == MA_TRUE && ipaddr->if_scopeid == 0);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	//Huge Invalid Ipaddress
	{
		char ipaddress[IP_ADDRESS_LEN_MAX] = "fe80:0000:0000:0000:0000:0000:0000:1010:1010:1010";

		is_curl = MA_FALSE;
	
		error = ma_url_utils_ip_address_create(ipaddress,is_curl,&ipaddr);
		TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,ipaddress)==0 
			&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 0);

		error = ma_url_utils_ip_address_release(ipaddr);
		TEST_ASSERT(error == MA_OK);
		ipaddr = NULL;

		is_curl = MA_TRUE;
		
		//error = ma_url_utils_ip_address_create(ipaddress,is_curl,&ipaddr);
		//TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"[fe80:0000:0000:0000:0000:0000:0000:1010]")==0 
		//	&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 0);

		//error = ma_url_utils_ip_address_release(ipaddr);
		//TEST_ASSERT(error == MA_OK);
		//ipaddr = NULL;
	}


}
TEST(ma_network_url_utils_tests, ma_url_utils_ipv6_address_create_with_scope_test) {
	ma_error_t error = MA_OK;
	ma_ip_address_t *ipaddr = NULL;
	ma_bool_t is_curl = MA_FALSE;
	
	is_curl = MA_FALSE;
	
	error = ma_url_utils_ip_address_create("[fe80::1010%11]",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"fe80::1010%11")==0 
		&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 0);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	error = ma_url_utils_ip_address_create("[fe80:0:0:0:0:0:0:1010%11]",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"fe80:0:0:0:0:0:0:1010%11")==0 
		&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 0);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	error = ma_url_utils_ip_address_create("[fe80:0000:0000:0000:0000:0000:0000:1010%11]",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"fe80:0000:0000:0000:0000:0000:0000:1010%11")==0 
		&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 0);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	is_curl = MA_TRUE;
	
	error = ma_url_utils_ip_address_create("[fe80::1010%11]",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"[fe80::1010]")==0 
		&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 11);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	error = ma_url_utils_ip_address_create("[fe80:0:0:0:0:0:0:1010%11]",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"[fe80:0:0:0:0:0:0:1010]")==0 
		&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 11);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	error = ma_url_utils_ip_address_create("[fe80:0000:0000:0000:0000:0000:0000:1010%11]",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"[fe80:0000:0000:0000:0000:0000:0000:1010]")==0 
		&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 11);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	is_curl = MA_FALSE;
	
	error = ma_url_utils_ip_address_create("[fec0::1010%11]",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"fec0::1010%11")==0 
		&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 0);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	error = ma_url_utils_ip_address_create("[fec0:0:0:0:0:0:0:1010%11]",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"fec0:0:0:0:0:0:0:1010%11")==0 
		&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 0);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	error = ma_url_utils_ip_address_create("[fec0:0000:0000:0000:0000:0000:0000:1010%11]",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"fec0:0000:0000:0000:0000:0000:0000:1010%11")==0 
		&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 0);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	is_curl = MA_TRUE;
	
	error = ma_url_utils_ip_address_create("[fec0::1010%11]",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"[fec0::1010]")==0 
		&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 11);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	error = ma_url_utils_ip_address_create("[fec0:0:0:0:0:0:0:1010%11]",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"[fec0:0:0:0:0:0:0:1010]")==0 
		&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 11);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	error = ma_url_utils_ip_address_create("[fec0:0000:0000:0000:0000:0000:0000:1010%11]",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"[fec0:0000:0000:0000:0000:0000:0000:1010]")==0 
		&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 11);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

}
TEST(ma_network_url_utils_tests, ma_url_utils_ipv6_address_create_link_local_test) {
	ma_error_t error = MA_OK;
	ma_ip_address_t *ipaddr = NULL;
	ma_bool_t is_curl = MA_FALSE;
	//IPv6 link local
	is_curl = MA_FALSE;

	error = ma_url_utils_ip_address_create("fe80::1010",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"fe80::1010")==0 
		&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 0);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	error = ma_url_utils_ip_address_create("fe80:0:0:0:0:0:0:1010",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"fe80:0:0:0:0:0:0:1010")==0 
		&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 0);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	error = ma_url_utils_ip_address_create("fe80:0000:0000:0000:0000:0000:0000:1010",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"fe80:0000:0000:0000:0000:0000:0000:1010")==0 
		&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 0);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	error = ma_url_utils_ip_address_create("[fe80::1010]",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"fe80::1010")==0 
		&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 0);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	error = ma_url_utils_ip_address_create("[fe80:0:0:0:0:0:0:1010]",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"fe80:0:0:0:0:0:0:1010")==0 
		&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 0);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	error = ma_url_utils_ip_address_create("[fe80:0000:0000:0000:0000:0000:0000:1010]",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"fe80:0000:0000:0000:0000:0000:0000:1010")==0 
		&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 0);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	is_curl = MA_TRUE;

	error = ma_url_utils_ip_address_create("fe80::1010",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"[fe80::1010]")==0 
		&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 0);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	error = ma_url_utils_ip_address_create("fe80:0:0:0:0:0:0:1010",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"[fe80:0:0:0:0:0:0:1010]")==0 
		&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 0);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	error = ma_url_utils_ip_address_create("fe80:0000:0000:0000:0000:0000:0000:1010",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"[fe80:0000:0000:0000:0000:0000:0000:1010]")==0 
		&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 0);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	error = ma_url_utils_ip_address_create("[fe80::1010]",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"[fe80::1010]")==0 
		&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 0);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	error = ma_url_utils_ip_address_create("[fe80:0:0:0:0:0:0:1010]",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"[fe80:0:0:0:0:0:0:1010]")==0 
		&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 0);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	error = ma_url_utils_ip_address_create("[fe80:0000:0000:0000:0000:0000:0000:1010]",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"[fe80:0000:0000:0000:0000:0000:0000:1010]")==0 
		&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 0);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;



	
	is_curl = MA_FALSE;

	error = ma_url_utils_ip_address_create("FE80::1010",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"FE80::1010")==0 
		&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 0);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	error = ma_url_utils_ip_address_create("FE80:0:0:0:0:0:0:1010",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"FE80:0:0:0:0:0:0:1010")==0 
		&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 0);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	error = ma_url_utils_ip_address_create("FE80:0000:0000:0000:0000:0000:0000:1010",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"FE80:0000:0000:0000:0000:0000:0000:1010")==0 
		&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 0);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	error = ma_url_utils_ip_address_create("[FE80::1010]",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"FE80::1010")==0 
		&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 0);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	error = ma_url_utils_ip_address_create("[FE80:0:0:0:0:0:0:1010]",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"FE80:0:0:0:0:0:0:1010")==0 
		&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 0);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	error = ma_url_utils_ip_address_create("[FE80:0000:0000:0000:0000:0000:0000:1010]",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"FE80:0000:0000:0000:0000:0000:0000:1010")==0 
		&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 0);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	is_curl = MA_TRUE;

	error = ma_url_utils_ip_address_create("FE80::1010",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"[FE80::1010]")==0 
		&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 0);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	error = ma_url_utils_ip_address_create("FE80:0:0:0:0:0:0:1010",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"[FE80:0:0:0:0:0:0:1010]")==0 
		&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 0);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	error = ma_url_utils_ip_address_create("FE80:0000:0000:0000:0000:0000:0000:1010",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"[FE80:0000:0000:0000:0000:0000:0000:1010]")==0 
		&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 0);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	error = ma_url_utils_ip_address_create("[FE80::1010]",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"[FE80::1010]")==0 
		&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 0);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	error = ma_url_utils_ip_address_create("[FE80:0:0:0:0:0:0:1010]",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"[FE80:0:0:0:0:0:0:1010]")==0 
		&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 0);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	error = ma_url_utils_ip_address_create("[FE80:0000:0000:0000:0000:0000:0000:1010]",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"[FE80:0000:0000:0000:0000:0000:0000:1010]")==0 
		&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 0);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;
}
TEST(ma_network_url_utils_tests, ma_url_utils_ipv6_address_create_site_local_test) {
	ma_error_t error = MA_OK;
	ma_ip_address_t *ipaddr = NULL;
	ma_bool_t is_curl = MA_FALSE;
	//IPv6 site local
	is_curl = MA_FALSE;

	error = ma_url_utils_ip_address_create("fec0::1010",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"fec0::1010")==0 
		&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 0);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	error = ma_url_utils_ip_address_create("fec0:0:0:0:0:0:0:1010",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"fec0:0:0:0:0:0:0:1010")==0 
		&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 0);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	error = ma_url_utils_ip_address_create("fec0:0000:0000:0000:0000:0000:0000:1010",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"fec0:0000:0000:0000:0000:0000:0000:1010")==0 
		&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 0);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	error = ma_url_utils_ip_address_create("[fec0::1010]",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"fec0::1010")==0 
		&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 0);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	error = ma_url_utils_ip_address_create("[fec0:0:0:0:0:0:0:1010]",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"fec0:0:0:0:0:0:0:1010")==0 
		&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 0);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	error = ma_url_utils_ip_address_create("[fec0:0000:0000:0000:0000:0000:0000:1010]",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"fec0:0000:0000:0000:0000:0000:0000:1010")==0 
		&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 0);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	is_curl = MA_TRUE;

	error = ma_url_utils_ip_address_create("fec0::1010",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"[fec0::1010]")==0 
		&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 0);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	error = ma_url_utils_ip_address_create("fec0:0:0:0:0:0:0:1010",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"[fec0:0:0:0:0:0:0:1010]")==0 
		&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 0);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	error = ma_url_utils_ip_address_create("fec0:0000:0000:0000:0000:0000:0000:1010",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"[fec0:0000:0000:0000:0000:0000:0000:1010]")==0 
		&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 0);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;
	
	error = ma_url_utils_ip_address_create("[fec0::1010]",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"[fec0::1010]")==0 
		&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 0);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;


	error = ma_url_utils_ip_address_create("[fec0:0:0:0:0:0:0:1010]",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"[fec0:0:0:0:0:0:0:1010]")==0 
		&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 0);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;


	error = ma_url_utils_ip_address_create("[fec0:0000:0000:0000:0000:0000:0000:1010]",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL && strcmp(ipaddr->address,"[fec0:0000:0000:0000:0000:0000:0000:1010]")==0 
		&& ipaddr->is_curl_addr == is_curl && ipaddr->if_scopeid == 0);

	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;
}
TEST(ma_network_url_utils_tests, ma_url_utils_ipv6_address_create_global_test) {
	ma_error_t error = MA_OK;
	ma_ip_address_t *ipaddr = NULL;
	ma_bool_t is_curl = MA_FALSE;
	//IPv6 global
	is_curl = MA_FALSE;

	error = ma_url_utils_ip_address_create("2001:470:150:2501::1010",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL);
	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	error = ma_url_utils_ip_address_create("2001:470:150:2501:0:0:0:1010",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL);
	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	error = ma_url_utils_ip_address_create("2001:470:150:2501:0000:0000:0000:1010",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL);
	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	error = ma_url_utils_ip_address_create("[2001:470:150:2501::1010]",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL);
	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	error = ma_url_utils_ip_address_create("[2001:470:150:2501:0:0:0:1010]",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL);
	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	error = ma_url_utils_ip_address_create("[2001:470:150:2501:0000:0000:0000:1010]",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL);
	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	is_curl = MA_TRUE;

	error = ma_url_utils_ip_address_create("2001:470:150:2501::1010",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL);
	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	error = ma_url_utils_ip_address_create("2001:470:150:2501:0:0:0:1010",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL);
	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	error = ma_url_utils_ip_address_create("2001:470:150:2501:0000:0000:0000:1010",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL);
	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	error = ma_url_utils_ip_address_create("[2001:470:150:2501::1010]",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL);
	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	error = ma_url_utils_ip_address_create("[2001:470:150:2501:0:0:0:1010]",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL);
	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	error = ma_url_utils_ip_address_create("[2001:470:150:2501:0000:0000:0000:1010]",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL);
	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;
}
TEST(ma_network_url_utils_tests, ma_url_utils_ip_address_release_test) {
	ma_error_t error = MA_OK;
	ma_ip_address_t *ipaddr = NULL;
	ma_bool_t is_curl = MA_FALSE;
	
	error = ma_url_utils_ip_address_create("127.0.0.1",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL);
	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	is_curl = MA_TRUE;
	error = ma_url_utils_ip_address_create("127.0.0.1",is_curl,&ipaddr);
	TEST_ASSERT(error == MA_OK && ipaddr != NULL);
	error = ma_url_utils_ip_address_release(ipaddr);
	TEST_ASSERT(error == MA_OK);
	ipaddr = NULL;

	is_curl = MA_FALSE;
	
	error = ma_url_utils_ip_address_release(NULL);
	TEST_ASSERT(error == MA_ERROR_INVALIDARG);

}
ma_error_t validate_url_info(ma_url_info_t* url_info)
{
	if(url_info)
	{
		
	}
	return MA_ERROR_INVALIDARG;
}
TEST(ma_network_url_utils_tests, ma_url_utils_url_info_create_test) {
	ma_bool_t is_curl = MA_FALSE;
	ma_url_info_t* url_info = NULL;
	ma_error_t error = MA_OK;
	
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create(NULL,is_curl,&url_info);
		TEST_ASSERT(error == MA_ERROR_INVALIDARG && url_info == NULL);
	
		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create(NULL,is_curl,&url_info);
		TEST_ASSERT(error == MA_ERROR_INVALIDARG && url_info == NULL);
	
	}
	{	
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://localhost:80/" ,is_curl,NULL);
		TEST_ASSERT(error == MA_ERROR_INVALIDARG && url_info == NULL);
	
		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://localhost:80/" ,is_curl,NULL);
		TEST_ASSERT(error == MA_ERROR_INVALIDARG && url_info == NULL);
	}
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://localhost:80/" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://localhost:80/",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("localhost",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_FALSE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"localhost") == 0);
		TEST_ASSERT(url_info->host_ip_addr->is_curl_addr == MA_TRUE);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"80") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"") == 0 );
		
		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;

		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://localhost:80/" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://localhost:80/",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("localhost",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_FALSE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"localhost") == 0);
		TEST_ASSERT(url_info->host_ip_addr->is_curl_addr == MA_TRUE);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"80") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;
	}
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://localhost/" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://localhost/",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("localhost",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_FALSE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"localhost") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;

		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://localhost/" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://localhost/",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("localhost",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_FALSE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"localhost") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;
	}
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://127.0.0.1/" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://127.0.0.1/",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("127.0.0.1",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_FALSE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"127.0.0.1") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;

		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://127.0.0.1/" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://127.0.0.1/",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("127.0.0.1",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_FALSE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"127.0.0.1") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;
	}
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://127.0.0.1:8083/" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://127.0.0.1:8083/",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("127.0.0.1",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_FALSE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"127.0.0.1") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;

		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://127.0.0.1:8083/" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://127.0.0.1:8083/",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("127.0.0.1",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_FALSE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"127.0.0.1") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;
	}
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://[fe80:0:0:0:0:0:1010]:8083/" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://fe80:0:0:0:0:0:1010:8083/",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("[fe80:0:0:0:0:0:1010]",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"fe80:0:0:0:0:0:1010") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;

		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://[fe80:0:0:0:0:0:1010]:8083/" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://[fe80:0:0:0:0:0:1010]:8083/",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("[fe80:0:0:0:0:0:1010]",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"[fe80:0:0:0:0:0:1010]") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;
	}
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://[fe80::1010]:8083/" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://fe80::1010:8083/",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("[fe80::1010]",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"fe80::1010") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;

		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://[fe80:0:0:0:0:0:1010]:8083/" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://[fe80:0:0:0:0:0:1010]:8083/",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("[fe80:0:0:0:0:0:1010]",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"[fe80:0:0:0:0:0:1010]") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;
	}
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://[fe80:0:0:0:0:0:1010]:8083/" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://fe80:0:0:0:0:0:1010:8083/",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("[fe80:0:0:0:0:0:1010]",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"fe80:0:0:0:0:0:1010") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;

		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://[fe80:0:0:0:0:0:1010]:8083/" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://[fe80:0:0:0:0:0:1010]:8083/",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("[fe80:0:0:0:0:0:1010]",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"[fe80:0:0:0:0:0:1010]") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;
	}
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://[fec0:0:0:0:0:0:1010]:8083/" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://fec0:0:0:0:0:0:1010:8083/",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("[fec0:0:0:0:0:0:1010]",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"fec0:0:0:0:0:0:1010") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;

		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://[fec0:0:0:0:0:0:1010]:8083/" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://[fec0:0:0:0:0:0:1010]:8083/",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("[fec0:0:0:0:0:0:1010]",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"[fec0:0:0:0:0:0:1010]") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;
	}	
	//Without braces
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://fe80:0:0:0:0:0:1010:8083/" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://fe80:0:0:0:0:0:1010:8083/",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("fe80:0:0:0:0:0:1010",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"fe80:0:0:0:0:0:1010") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;

		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://fe80:0:0:0:0:0:1010:8083/" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://[fe80:0:0:0:0:0:1010]:8083/",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("fe80:0:0:0:0:0:1010",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"[fe80:0:0:0:0:0:1010]") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;
	}
	//Without braces
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://fe80::1010:8083/" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://fe80::1010:8083/",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("fe80::1010",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"fe80::1010") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;

		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://fe80::1010:8083/" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://[fe80::1010]:8083/",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("fe80::1010",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"[fe80::1010]") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;
	}
	//Without braces
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://fe80:::8083/" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://fe80:::8083/",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("fe80::",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"fe80::") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;

		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://fe80:::8083/" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://[fe80::]:8083/",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("fe80::",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"[fe80::]") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;
	}
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://fec0:0:0:0:0:0:1010:8083/" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://fec0:0:0:0:0:0:1010:8083/",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("fec0:0:0:0:0:0:1010",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"fec0:0:0:0:0:0:1010") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;

		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://fec0:0:0:0:0:0:1010:8083/" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://[fec0:0:0:0:0:0:1010]:8083/",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("fec0:0:0:0:0:0:1010",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"[fec0:0:0:0:0:0:1010]") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;
	}
	
	//Scope ID
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://[fe80:0:0:0:0:0:1010%11]:8083/" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://fe80:0:0:0:0:0:1010%11:8083/",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("[fe80:0:0:0:0:0:1010%11]",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"fe80:0:0:0:0:0:1010%11") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;

		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://[fe80:0:0:0:0:0:1010%11]:8083/" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://[fe80:0:0:0:0:0:1010]:8083/",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("[fe80:0:0:0:0:0:1010%11]",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"[fe80:0:0:0:0:0:1010]") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;
	}
}

TEST(ma_network_url_utils_tests, ma_url_utils_url_info_create_test_with_uri) {
	ma_bool_t is_curl = MA_FALSE;
	ma_url_info_t* url_info = NULL;
	ma_error_t error = MA_OK;
	
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://localhost:80/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://localhost:80/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("localhost",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_FALSE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"localhost") == 0);
		TEST_ASSERT(url_info->host_ip_addr->is_curl_addr == MA_TRUE);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"80") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"index.html") == 0 );
		
		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;

		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://localhost:80/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://localhost:80/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("localhost",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_FALSE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"localhost") == 0);
		TEST_ASSERT(url_info->host_ip_addr->is_curl_addr == MA_TRUE);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"80") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;
	}
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://localhost/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://localhost/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("localhost",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_FALSE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"localhost") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;

		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://localhost/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://localhost/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("localhost",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_FALSE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"localhost") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;
	}
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://127.0.0.1/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://127.0.0.1/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("127.0.0.1",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_FALSE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"127.0.0.1") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;

		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://127.0.0.1/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://127.0.0.1/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("127.0.0.1",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_FALSE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"127.0.0.1") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;
	}
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://127.0.0.1:8083/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://127.0.0.1:8083/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("127.0.0.1",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_FALSE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"127.0.0.1") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;

		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://127.0.0.1:8083/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://127.0.0.1:8083/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("127.0.0.1",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_FALSE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"127.0.0.1") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;
	}
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://[fe80:0:0:0:0:0:1010]:8083/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://fe80:0:0:0:0:0:1010:8083/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("[fe80:0:0:0:0:0:1010]",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"fe80:0:0:0:0:0:1010") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;

		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://[fe80:0:0:0:0:0:1010]:8083/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://[fe80:0:0:0:0:0:1010]:8083/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("[fe80:0:0:0:0:0:1010]",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"[fe80:0:0:0:0:0:1010]") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;
	}
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://[fec0:0:0:0:0:0:1010]:8083/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://fec0:0:0:0:0:0:1010:8083/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("[fec0:0:0:0:0:0:1010]",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"fec0:0:0:0:0:0:1010") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;

		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://[fec0:0:0:0:0:0:1010]:8083/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://[fec0:0:0:0:0:0:1010]:8083/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("[fec0:0:0:0:0:0:1010]",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"[fec0:0:0:0:0:0:1010]") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;
	}	
	//Without braces
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://fe80:0:0:0:0:0:1010:8083/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://fe80:0:0:0:0:0:1010:8083/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("fe80:0:0:0:0:0:1010",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"fe80:0:0:0:0:0:1010") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;

		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://fe80:0:0:0:0:0:1010:8083/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://[fe80:0:0:0:0:0:1010]:8083/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("fe80:0:0:0:0:0:1010",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"[fe80:0:0:0:0:0:1010]") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;
	}
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://fec0:0:0:0:0:0:1010:8083/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://fec0:0:0:0:0:0:1010:8083/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("fec0:0:0:0:0:0:1010",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"fec0:0:0:0:0:0:1010") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;

		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://fec0:0:0:0:0:0:1010:8083/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://[fec0:0:0:0:0:0:1010]:8083/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("fec0:0:0:0:0:0:1010",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"[fec0:0:0:0:0:0:1010]") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;
	}
	
	//Scope ID
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://[fe80:0:0:0:0:0:1010%11]:8083/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://fe80:0:0:0:0:0:1010%11:8083/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("[fe80:0:0:0:0:0:1010%11]",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"fe80:0:0:0:0:0:1010%11") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;

		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://[fe80:0:0:0:0:0:1010%11]:8083/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://[fe80:0:0:0:0:0:1010]:8083/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("[fe80:0:0:0:0:0:1010%11]",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"[fe80:0:0:0:0:0:1010]") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;
	}
}
TEST(ma_network_url_utils_tests, ma_url_utils_url_info_create_test_with_uri_multi_slash) {
	ma_bool_t is_curl = MA_FALSE;
	ma_url_info_t* url_info = NULL;
	ma_error_t error = MA_OK;
	
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://localhost:80//index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://localhost:80/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("localhost",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_FALSE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"localhost") == 0);
		TEST_ASSERT(url_info->host_ip_addr->is_curl_addr == MA_TRUE);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"80") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"index.html") == 0 );
		
		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;

		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://localhost:80//index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://localhost:80/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("localhost",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_FALSE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"localhost") == 0);
		TEST_ASSERT(url_info->host_ip_addr->is_curl_addr == MA_TRUE);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"80") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;
	}
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://localhost//index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://localhost/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("localhost",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_FALSE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"localhost") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;

		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://localhost//index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://localhost/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("localhost",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_FALSE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"localhost") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;
	}
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://127.0.0.1//index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://127.0.0.1/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("127.0.0.1",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_FALSE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"127.0.0.1") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;

		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://127.0.0.1//index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://127.0.0.1/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("127.0.0.1",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_FALSE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"127.0.0.1") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;
	}
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://127.0.0.1:8083//index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://127.0.0.1:8083/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("127.0.0.1",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_FALSE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"127.0.0.1") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;

		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://127.0.0.1:8083//index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://127.0.0.1:8083/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("127.0.0.1",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_FALSE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"127.0.0.1") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;
	}
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://[fe80:0:0:0:0:0:1010]:8083//index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://fe80:0:0:0:0:0:1010:8083/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("[fe80:0:0:0:0:0:1010]",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"fe80:0:0:0:0:0:1010") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;

		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://[fe80:0:0:0:0:0:1010]:8083//index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://[fe80:0:0:0:0:0:1010]:8083/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("[fe80:0:0:0:0:0:1010]",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"[fe80:0:0:0:0:0:1010]") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;
	}
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://[fec0:0:0:0:0:0:1010]:8083//index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://fec0:0:0:0:0:0:1010:8083/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("[fec0:0:0:0:0:0:1010]",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"fec0:0:0:0:0:0:1010") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;

		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://[fec0:0:0:0:0:0:1010]:8083//index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://[fec0:0:0:0:0:0:1010]:8083/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("[fec0:0:0:0:0:0:1010]",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"[fec0:0:0:0:0:0:1010]") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;
	}	
	//Without braces
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://fe80:0:0:0:0:0:1010:8083//index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://fe80:0:0:0:0:0:1010:8083/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("fe80:0:0:0:0:0:1010",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"fe80:0:0:0:0:0:1010") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;

		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://fe80:0:0:0:0:0:1010:8083//index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://[fe80:0:0:0:0:0:1010]:8083/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("fe80:0:0:0:0:0:1010",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"[fe80:0:0:0:0:0:1010]") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;
	}
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://fec0:0:0:0:0:0:1010:8083//index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://fec0:0:0:0:0:0:1010:8083/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("fec0:0:0:0:0:0:1010",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"fec0:0:0:0:0:0:1010") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;

		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://fec0:0:0:0:0:0:1010:8083//index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://[fec0:0:0:0:0:0:1010]:8083/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("fec0:0:0:0:0:0:1010",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"[fec0:0:0:0:0:0:1010]") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;
	}
	
	//Scope ID
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://[fe80:0:0:0:0:0:1010%11]:8083//index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://fe80:0:0:0:0:0:1010%11:8083/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("[fe80:0:0:0:0:0:1010%11]",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"fe80:0:0:0:0:0:1010%11") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;

		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://[fe80:0:0:0:0:0:1010%11]:8083//index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://[fe80:0:0:0:0:0:1010]:8083/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("[fe80:0:0:0:0:0:1010%11]",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"[fe80:0:0:0:0:0:1010]") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;
	}
}
TEST(ma_network_url_utils_tests, ma_url_utils_url_info_create_test_with_multi_level_uri) {
	ma_bool_t is_curl = MA_FALSE;
	ma_url_info_t* url_info = NULL;
	ma_error_t error = MA_OK;
	
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://localhost:80/MyApp/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://localhost:80/MyApp/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("localhost",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_FALSE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"localhost") == 0);
		TEST_ASSERT(url_info->host_ip_addr->is_curl_addr == MA_TRUE);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"80") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"MyApp/index.html") == 0 );
		
		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;

		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://localhost:80/MyApp/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://localhost:80/MyApp/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("localhost",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_FALSE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"localhost") == 0);
		TEST_ASSERT(url_info->host_ip_addr->is_curl_addr == MA_TRUE);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"80") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"MyApp/index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;
	}
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://localhost/MyApp/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://localhost/MyApp/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("localhost",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_FALSE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"localhost") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"MyApp/index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;

		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://localhost/MyApp/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://localhost/MyApp/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("localhost",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_FALSE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"localhost") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"MyApp/index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;
	}
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://127.0.0.1/MyApp/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://127.0.0.1/MyApp/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("127.0.0.1",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_FALSE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"127.0.0.1") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"MyApp/index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;

		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://127.0.0.1/MyApp/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://127.0.0.1/MyApp/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("127.0.0.1",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_FALSE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"127.0.0.1") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"MyApp/index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;
	}
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://127.0.0.1:8083/MyApp/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://127.0.0.1:8083/MyApp/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("127.0.0.1",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_FALSE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"127.0.0.1") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"MyApp/index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;

		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://127.0.0.1:8083/MyApp/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://127.0.0.1:8083/MyApp/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("127.0.0.1",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_FALSE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"127.0.0.1") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"MyApp/index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;
	}
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://[fe80:0:0:0:0:0:1010]:8083/MyApp/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://fe80:0:0:0:0:0:1010:8083/MyApp/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("[fe80:0:0:0:0:0:1010]",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"fe80:0:0:0:0:0:1010") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"MyApp/index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;

		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://[fe80:0:0:0:0:0:1010]:8083/MyApp/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://[fe80:0:0:0:0:0:1010]:8083/MyApp/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("[fe80:0:0:0:0:0:1010]",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"[fe80:0:0:0:0:0:1010]") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"MyApp/index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;
	}
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://[fec0:0:0:0:0:0:1010]:8083/MyApp/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://fec0:0:0:0:0:0:1010:8083/MyApp/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("[fec0:0:0:0:0:0:1010]",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"fec0:0:0:0:0:0:1010") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"MyApp/index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;

		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://[fec0:0:0:0:0:0:1010]:8083/MyApp/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://[fec0:0:0:0:0:0:1010]:8083/MyApp/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("[fec0:0:0:0:0:0:1010]",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"[fec0:0:0:0:0:0:1010]") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"MyApp/index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;
	}	
	//Without braces
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://fe80:0:0:0:0:0:1010:8083/MyApp/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://fe80:0:0:0:0:0:1010:8083/MyApp/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("fe80:0:0:0:0:0:1010",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"fe80:0:0:0:0:0:1010") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"MyApp/index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;

		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://fe80:0:0:0:0:0:1010:8083/MyApp/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://[fe80:0:0:0:0:0:1010]:8083/MyApp/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("fe80:0:0:0:0:0:1010",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"[fe80:0:0:0:0:0:1010]") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"MyApp/index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;
	}
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://fec0:0:0:0:0:0:1010:8083/MyApp/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://fec0:0:0:0:0:0:1010:8083/MyApp/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("fec0:0:0:0:0:0:1010",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"fec0:0:0:0:0:0:1010") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"MyApp/index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;

		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://fec0:0:0:0:0:0:1010:8083/MyApp/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://[fec0:0:0:0:0:0:1010]:8083/MyApp/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("fec0:0:0:0:0:0:1010",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"[fec0:0:0:0:0:0:1010]") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"MyApp/index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;
	}
	
	//Scope ID
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://[fe80:0:0:0:0:0:1010%11]:8083/MyApp/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://fe80:0:0:0:0:0:1010%11:8083/MyApp/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("[fe80:0:0:0:0:0:1010%11]",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"fe80:0:0:0:0:0:1010%11") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"MyApp/index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;

		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://[fe80:0:0:0:0:0:1010%11]:8083/MyApp/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://[fe80:0:0:0:0:0:1010]:8083/MyApp/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("[fe80:0:0:0:0:0:1010%11]",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"[fe80:0:0:0:0:0:1010]") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"MyApp/index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;
	}
}
TEST(ma_network_url_utils_tests, ma_url_utils_url_info_create_test_with_multi_level_uri_multi_slash) {
	ma_bool_t is_curl = MA_FALSE;
	ma_url_info_t* url_info = NULL;
	ma_error_t error = MA_OK;
	
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://localhost:80//MyApp/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://localhost:80/MyApp/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("localhost",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_FALSE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"localhost") == 0);
		TEST_ASSERT(url_info->host_ip_addr->is_curl_addr == MA_TRUE);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"80") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"MyApp/index.html") == 0 );
		
		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;

		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://localhost:80//MyApp/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://localhost:80/MyApp/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("localhost",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_FALSE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"localhost") == 0);
		TEST_ASSERT(url_info->host_ip_addr->is_curl_addr == MA_TRUE);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"80") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"MyApp/index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;
	}
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://localhost//MyApp/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://localhost/MyApp/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("localhost",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_FALSE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"localhost") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"MyApp/index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;

		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://localhost//MyApp/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://localhost/MyApp/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("localhost",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_FALSE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"localhost") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"MyApp/index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;
	}
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://127.0.0.1//MyApp/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://127.0.0.1/MyApp/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("127.0.0.1",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_FALSE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"127.0.0.1") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"MyApp/index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;

		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://127.0.0.1//MyApp/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://127.0.0.1/MyApp/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("127.0.0.1",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_FALSE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"127.0.0.1") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"MyApp/index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;
	}
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://127.0.0.1:8083//MyApp/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://127.0.0.1:8083/MyApp/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("127.0.0.1",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_FALSE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"127.0.0.1") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"MyApp/index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;

		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://127.0.0.1:8083//MyApp/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://127.0.0.1:8083/MyApp/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("127.0.0.1",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_FALSE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"127.0.0.1") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"MyApp/index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;
	}
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://[fe80:0:0:0:0:0:1010]:8083//MyApp/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://fe80:0:0:0:0:0:1010:8083/MyApp/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("[fe80:0:0:0:0:0:1010]",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"fe80:0:0:0:0:0:1010") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"MyApp/index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;

		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://[fe80:0:0:0:0:0:1010]:8083//MyApp/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://[fe80:0:0:0:0:0:1010]:8083/MyApp/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("[fe80:0:0:0:0:0:1010]",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"[fe80:0:0:0:0:0:1010]") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"MyApp/index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;
	}
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://[fec0:0:0:0:0:0:1010]:8083//MyApp/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://fec0:0:0:0:0:0:1010:8083/MyApp/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("[fec0:0:0:0:0:0:1010]",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"fec0:0:0:0:0:0:1010") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"MyApp/index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;

		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://[fec0:0:0:0:0:0:1010]:8083//MyApp/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://[fec0:0:0:0:0:0:1010]:8083/MyApp/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("[fec0:0:0:0:0:0:1010]",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"[fec0:0:0:0:0:0:1010]") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"MyApp/index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;
	}	
	//Without braces
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://fe80:0:0:0:0:0:1010:8083//MyApp/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://fe80:0:0:0:0:0:1010:8083/MyApp/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("fe80:0:0:0:0:0:1010",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"fe80:0:0:0:0:0:1010") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"MyApp/index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;

		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://fe80:0:0:0:0:0:1010:8083//MyApp/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://[fe80:0:0:0:0:0:1010]:8083/MyApp/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("fe80:0:0:0:0:0:1010",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"[fe80:0:0:0:0:0:1010]") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"MyApp/index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;
	}
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://fec0:0:0:0:0:0:1010:8083//MyApp/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://fec0:0:0:0:0:0:1010:8083/MyApp/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("fec0:0:0:0:0:0:1010",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"fec0:0:0:0:0:0:1010") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"MyApp/index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;

		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://fec0:0:0:0:0:0:1010:8083//MyApp/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://[fec0:0:0:0:0:0:1010]:8083/MyApp/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("fec0:0:0:0:0:0:1010",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"[fec0:0:0:0:0:0:1010]") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"MyApp/index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;
	}
	
	//Scope ID
	{
		is_curl = MA_FALSE;
		error = ma_url_utils_url_info_create("http://[fe80:0:0:0:0:0:1010%11]:8083//MyApp/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://fe80:0:0:0:0:0:1010%11:8083/MyApp/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("[fe80:0:0:0:0:0:1010%11]",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"fe80:0:0:0:0:0:1010%11") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"MyApp/index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;

		is_curl = MA_TRUE;
		error = ma_url_utils_url_info_create("http://[fe80:0:0:0:0:0:1010%11]:8083//MyApp/index.html" ,is_curl,&url_info);
		TEST_ASSERT(error == MA_OK && url_info != NULL );
		TEST_ASSERT(strcmp("http://[fe80:0:0:0:0:0:1010]:8083/MyApp/index.html",url_info->formatted_url) == 0 );
		TEST_ASSERT(strcmp("[fe80:0:0:0:0:0:1010%11]",url_info->host) == 0);
		TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
		TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
		TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"[fe80:0:0:0:0:0:1010]") == 0);
		TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
		TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
		TEST_ASSERT(strcmp(url_info->uri,"MyApp/index.html") == 0 );

		error = ma_url_utils_url_info_release(url_info);
		TEST_ASSERT(error == MA_OK);
		url_info = NULL;
	}
}
TEST(ma_network_url_utils_tests, ma_url_utils_url_info_release_test) {
	ma_error_t error = MA_OK;	
	ma_url_info_t* url_info = NULL;
	ma_bool_t is_curl = MA_FALSE;
	error = ma_url_utils_url_info_create("http://[fe80:0:0:0:0:0:1010%11]:8083//MyApp/index.html" ,is_curl,&url_info);
	TEST_ASSERT(error == MA_OK && url_info != NULL );
	TEST_ASSERT(strcmp("http://fe80:0:0:0:0:0:1010%11:8083/MyApp/index.html",url_info->formatted_url) == 0 );
	TEST_ASSERT(strcmp("[fe80:0:0:0:0:0:1010%11]",url_info->host) == 0);
	TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
	TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
	TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"fe80:0:0:0:0:0:1010%11") == 0);
	TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
	TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
	TEST_ASSERT(strcmp(url_info->uri,"MyApp/index.html") == 0 );
	
	error = ma_url_utils_url_info_release(url_info);
	TEST_ASSERT(error == MA_OK);
	url_info = NULL;	
	
	is_curl = MA_TRUE;
	error = ma_url_utils_url_info_create("http://[fe80:0:0:0:0:0:1010%11]:8083//MyApp/index.html" ,is_curl,&url_info);
	TEST_ASSERT(error == MA_OK && url_info != NULL );
	TEST_ASSERT(strcmp("http://[fe80:0:0:0:0:0:1010]:8083/MyApp/index.html",url_info->formatted_url) == 0 );
	TEST_ASSERT(strcmp("[fe80:0:0:0:0:0:1010%11]",url_info->host) == 0);
	TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
	TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
	TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"[fe80:0:0:0:0:0:1010]") == 0);
	TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
	TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
	TEST_ASSERT(strcmp(url_info->uri,"MyApp/index.html") == 0 );
	
	error = ma_url_utils_url_info_release(url_info);
	TEST_ASSERT(error == MA_OK);
	url_info = NULL;

	error = ma_url_utils_url_info_release(url_info);
	TEST_ASSERT(error == MA_ERROR_INVALIDARG);
}

TEST(ma_network_url_utils_tests, ma_url_utils_url_info_create_test_with_uri_multi_space) {
	ma_error_t error = MA_OK;	
	ma_url_info_t* url_info = NULL;
	ma_bool_t is_curl = MA_FALSE;
	is_curl = MA_TRUE;
	error = ma_url_utils_url_info_create("http://[fe80:0:0:0:0:0:1010%11]:8083//MyApp/index .html" ,is_curl,&url_info);
	TEST_ASSERT(error == MA_OK && url_info != NULL );
	TEST_ASSERT(strcmp("http://[fe80:0:0:0:0:0:1010]:8083/MyApp/index%20.html",url_info->formatted_url) == 0 );
	TEST_ASSERT(strcmp("[fe80:0:0:0:0:0:1010%11]",url_info->host) == 0);
	TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
	TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
	TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"[fe80:0:0:0:0:0:1010]") == 0);
	TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
	TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
	
	error = ma_url_utils_url_info_release(url_info);
	TEST_ASSERT(error == MA_OK);
	url_info = NULL;

	error = ma_url_utils_url_info_create("http://[fe80:0:0:0:0:0:1010%11]:8083//MyApp/Mctray Product/Mctray Exe" ,is_curl,&url_info);
	TEST_ASSERT(error == MA_OK && url_info != NULL );
	TEST_ASSERT(strcmp("http://[fe80:0:0:0:0:0:1010]:8083/MyApp/Mctray%20Product/Mctray%20Exe",url_info->formatted_url) == 0 );
	TEST_ASSERT(strcmp("[fe80:0:0:0:0:0:1010%11]",url_info->host) == 0);
	TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
	TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
	TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"[fe80:0:0:0:0:0:1010]") == 0);
	TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
	TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
	
	error = ma_url_utils_url_info_release(url_info);
	TEST_ASSERT(error == MA_OK);
	url_info = NULL;


	error = ma_url_utils_url_info_create("http://[fe80:0:0:0:0:0:1010%11]:8083//MyApp/Mctray   Product/Mctray  Exe" ,is_curl,&url_info);
	TEST_ASSERT(error == MA_OK && url_info != NULL );
	TEST_ASSERT(strcmp("http://[fe80:0:0:0:0:0:1010]:8083/MyApp/Mctray%20%20%20Product/Mctray%20%20Exe",url_info->formatted_url) == 0 );
	TEST_ASSERT(strcmp("[fe80:0:0:0:0:0:1010%11]",url_info->host) == 0);
	TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
	TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
	TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"[fe80:0:0:0:0:0:1010]") == 0);
	TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
	TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
	
	error = ma_url_utils_url_info_release(url_info);
	TEST_ASSERT(error == MA_OK);
	url_info = NULL;

	error = ma_url_utils_url_info_create("http://[fe80:0:0:0:0:0:1010%11]:8083//MyApp/Mctray    Product/Mctray  Exe  Test" ,is_curl,&url_info);
	TEST_ASSERT(error == MA_OK && url_info != NULL );
	TEST_ASSERT(strcmp("http://[fe80:0:0:0:0:0:1010]:8083/MyApp/Mctray%20%20%20%20Product/Mctray%20%20Exe%20%20Test",url_info->formatted_url) == 0 );
	TEST_ASSERT(strcmp("[fe80:0:0:0:0:0:1010%11]",url_info->host) == 0);
	TEST_ASSERT(url_info->is_ipv6_ddr == MA_TRUE);
	TEST_ASSERT(url_info->protocol == MA_URL_TRANSER_PROTOCOL_HTTP);
	TEST_ASSERT(strcmp(url_info->host_ip_addr->address,"[fe80:0:0:0:0:0:1010]") == 0);
	TEST_ASSERT(strcmp(url_info->porotol_str,"http")==0);
	TEST_ASSERT(strcmp(url_info->port,"8083") == 0 );
	
	error = ma_url_utils_url_info_release(url_info);
	TEST_ASSERT(error == MA_OK);
	url_info = NULL;
}
