// network_test.cpp : Defines the entry point for the console application.
//

//#include "stdafx.h"

#include "unity.h"
#include "unity_fixture.h"
#include "ma/ma_common.h"

#ifdef MA_WINDOWS
#include <windows.h>
#endif
#ifdef _MSC_VER
#include <crtdbg.h> /* */
# define MA_TRACKLEAKS _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF)
#else
# define MA_TRACKLEAKS ((void *)0)
#endif

static void run_all_tests(void) {
	do { RUN_TEST_GROUP(ma_network_url_utils_test_group);}while(0);
    do { RUN_TEST_GROUP(ma_network_proxy_list_test_group);}while(0);
    do { RUN_TEST_GROUP(ma_network_net_client_services_test_group);}while(0);
	do { RUN_TEST_GROUP(ma_net_interface_tests_group);}while(0);

	// This test should be executed manually
	//do { RUN_TEST_GROUP(ma_network_proxy_autodetect_test_group);}while(0);
    
	// Redundant test. Need not be executed.
	//do {RUN_TEST_GROUP(ma_network_mock_test_group);} while (0);
}

int main(int argc, char* argv[])
{
    MA_TRACKLEAKS;
    return UnityMain(argc, argv, run_all_tests);
}

