/******************************************************************************
Proxy - DB Manager tests
******************************************************************************/
#define UNITY_FIXTURE_MALLOC_OVERRIDES_H_ 1

#include "unity.h"
#include "unity_fixture.h"
#include "ma/internal/utils/database/ma_db.h"
#include "ma/internal/utils/proxy/ma_proxy_db_manager.h"
#include <string.h>
#include <stdlib.h>

#ifndef MA_WINDOWS
    #include <limits.h>
    #include <unistd.h>
#endif

TEST_GROUP_RUNNER(proxy_db_manager_test_group)
{
    do {RUN_TEST_CASE(proxy_db_manager_tests, add_and_remove_all_proxy_test)} while (0);
    do {RUN_TEST_CASE(proxy_db_manager_tests, add_and_remove_all_global_proxy_test)} while (0);
    do {RUN_TEST_CASE(proxy_db_manager_tests, add_and_remove_proxy_test)} while (0);
}

static ma_db_t *proxy_db = NULL;

#define PROXY_DB_FILENAME  "PROXY_DB"


TEST_SETUP(proxy_db_manager_tests)
{
    unlink(PROXY_DB_FILENAME);

    TEST_ASSERT(MA_OK == ma_db_open(PROXY_DB_FILENAME, MA_DB_OPEN_READWRITE, &proxy_db));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_db_create_schema(NULL));
    TEST_ASSERT(MA_OK == ma_proxy_db_create_schema(proxy_db));
}

TEST_TEAR_DOWN(proxy_db_manager_tests)
{
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_close(NULL));
    TEST_ASSERT(MA_OK == ma_db_close(proxy_db));
}

// Lookup Table consisting of Proxy Information
struct LookupTable_1
{
    const char *proxy_address;
    ma_int32_t proxy_port;
    ma_proxy_protocol_type_t proxy_type;
    ma_int32_t proxy_flag;
    ma_bool_t proxy_auth;
    const char *proxy_user_name;
    const char *proxy_password;
};

static struct LookupTable_1 const proxies[] =
{
    // Address          Port      Type                Flag   Auth      Username         Password
    {  "199.234.34.56", 10,       MA_PROXY_TYPE_FTP,  1,     MA_TRUE,  "Administrator", "wwehewlkheklneflke" },
    {  "199.202.18.34", 1000,     MA_PROXY_TYPE_BOTH, 2,     MA_TRUE,  "Normal User",   "dedjds98fudufdp#$#" },
    {  "199.234.34.34", 100000,   MA_PROXY_TYPE_HTTP, 1,     MA_FALSE, NULL,            NULL                 },
    {  "0.0.0.0",       1,        MA_PROXY_TYPE_FTP,  1,     MA_TRUE,  "Administrator", "#@$#@$#@$#@$#@$#@"  },
    {  "199.34.34.34",  83,       MA_PROXY_TYPE_HTTP, 2,     MA_TRUE,  "User 1",        "#@$#@$#@$#@$#@$#@"  },
    {  "127.0.0.1",     8443,     MA_PROXY_TYPE_BOTH, 1,     MA_FALSE, NULL,            "lefe;lfelmelfemfle" },

};

// Lookup Table consisting of Proxy Config Information
struct LookupTable_2
{
    ma_proxy_usage_type_t proxy_config_usage_type;
    ma_proxy_protocol_type_t proxy_config_proto_type;
    ma_bool_t proxy_config_auth;
    const char *proxy_config_user_name;
    const char *proxy_config_password;
    ma_bool_t proxy_config_local_proxy;
    ma_bool_t proxy_config_bypass_local;
    const char *proxy_config_exclusion_str;
};

static struct LookupTable_2 const proxy_configs[] =
{
    // Address                                Protocol Type        Authentication  Username         Password              Local Proxy Bypass Local Exclusion STR
    {  MA_PROXY_USAGE_TYPE_NONE,              MA_PROXY_TYPE_HTTP,  MA_FALSE,       "SuperUser",     "#@$#@$#@$#@$#@$#@",  MA_TRUE,    MA_FALSE,    "dmldmf3243##$"   },
    {  MA_PROXY_USAGE_TYPE_SYSTEM_CONFIGURED, MA_PROXY_TYPE_FTP,   MA_TRUE,        "Administrator", "wwehewlkheklneflke", MA_TRUE,    MA_TRUE,     "efmfe;fmefmefm"  },
    {  MA_PROXY_USAGE_TYPE_SERVER_CONFIGURED, MA_PROXY_TYPE_BOTH,  MA_TRUE,        "Normal User",   "dedjds98fudufdp#$#", MA_FALSE,   MA_FALSE,    "ef4io5867yh4nrlfmef eejr" },
    {  MA_PROXY_USAGE_TYPE_SYSTEM_CONFIGURED, MA_PROXY_TYPE_HTTP,  MA_FALSE,       NULL,            NULL,                 MA_FALSE,   MA_TRUE,     "efmfe;df   fm"   },
    {  MA_PROXY_USAGE_TYPE_NONE,              MA_PROXY_TYPE_FTP,   MA_TRUE,        "Administrator", "#@$#@$#@$#@$#@$#@",  MA_TRUE,    MA_FALSE,    "dmldmf3243##$"   },
    {  MA_PROXY_USAGE_TYPE_NONE,              MA_PROXY_TYPE_HTTP,  MA_FALSE,       NULL,            "#@$#@$#@$#@$#@$#@",  MA_TRUE,    MA_FALSE,    "doey43##$"       },
    {  MA_PROXY_USAGE_TYPE_SYSTEM_CONFIGURED, MA_PROXY_TYPE_HTTP,  MA_FALSE,       "New User",      NULL,                 MA_FALSE,   MA_TRUE,     "efm349857  fm"   },
    {  MA_PROXY_USAGE_TYPE_NONE,              MA_PROXY_TYPE_FTP,   MA_FALSE,       "SuperUser",     "#@$#@$#@$#@$#@$#@",  MA_TRUE,    MA_FALSE,    "dmldmf3243##$"   },
    {  MA_PROXY_USAGE_TYPE_NONE,              MA_PROXY_TYPE_FTP,   MA_FALSE,       NULL,            "#@$#@$#@$#@$#@$#@",  MA_TRUE,    MA_FALSE,    "dml4786548356435786893456438243##$"   },
    {  MA_PROXY_USAGE_TYPE_SYSTEM_CONFIGURED, MA_PROXY_TYPE_FTP,   MA_FALSE,       "New User",      NULL,                 MA_FALSE,   MA_TRUE,     "efm349857  fm"   },
};

/* This function finds out that the proxy retrieved in return for the
   ma_proxy_db_get_all_proxies query is one of the expected records.
*/
static ma_int32_t exp_proxy_list[sizeof(proxies)/sizeof(proxies[0])] = { 0 };

static ma_int32_t proxy_matching(const char *address, ma_int32_t port, ma_proxy_protocol_type_t type, ma_int32_t flag,
                                 ma_bool_t auth, const char *username, const char *password)
{
    ma_int32_t counter = 0;

    for(counter = 0; counter < sizeof(proxies)/sizeof(proxies[0]); counter++)
    {
        ma_uint8_t matched = 1;

        if(exp_proxy_list[counter] == 1)
        {
            if(strcmp(proxies[counter].proxy_address, address)) {  matched = 0;  }

            if(proxies[counter].proxy_port != port) {  matched = 0;  }

            if(proxies[counter].proxy_type != type) {  matched = 0;  }

            if(proxies[counter].proxy_flag != flag) {  matched = 0;  }

            if(proxies[counter].proxy_auth != auth) {  matched = 0;  }

            if(proxies[counter].proxy_auth)
            {
                if(strcmp(proxies[counter].proxy_user_name, username)) {  matched = 0;  }
                if(strcmp(proxies[counter].proxy_password, password)) {  matched = 0;  }
            }
            else
            {
                if(username) { matched = 0; }
                if(password) { matched = 0; }
            }
        }

        if(exp_proxy_list[counter] && matched)
        {
            exp_proxy_list[counter] = 0;
            matched = 0;
            return 1;
        }
    }
    return 0;
}


/******************************************************************************
Test for verifying the Proxy Addition and Remove All APIs
******************************************************************************/
TEST(proxy_db_manager_tests, add_and_remove_all_proxy_test)
{
    ma_uint8_t counter = 0;
    ma_proxy_list_t *proxy_list = NULL;
    size_t size_of_proxy_list = 0;

    // Adding Proxy entries to DB
    for(counter = 0; counter < sizeof(proxies)/sizeof(proxies[0]); counter++)
    {
        // Creating Proxy object and filling all parameters
        ma_proxy_t *my_proxy = NULL;

        TEST_ASSERT(MA_OK == ma_proxy_create(&my_proxy));
        TEST_ASSERT(MA_OK == ma_proxy_set_address(my_proxy, proxies[counter].proxy_address));
        TEST_ASSERT(MA_OK == ma_proxy_set_port(my_proxy, proxies[counter].proxy_port));
        TEST_ASSERT(MA_OK == ma_proxy_set_protocol_type(my_proxy, proxies[counter].proxy_type));
        TEST_ASSERT(MA_OK == ma_proxy_set_flag(my_proxy, proxies[counter].proxy_flag));
        TEST_ASSERT(MA_OK == ma_proxy_set_authentication(my_proxy, proxies[counter].proxy_auth,
                                                                   proxies[counter].proxy_user_name,
                                                                   proxies[counter].proxy_password));

        // Adding proxy to DB
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_db_add_proxy(NULL, my_proxy));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_db_add_proxy(proxy_db, NULL));
        TEST_ASSERT(MA_OK == ma_proxy_db_add_proxy(proxy_db, my_proxy));

        // Releasing Proxy object
        TEST_ASSERT(MA_OK == ma_proxy_release(my_proxy));
    }

    //-------------------------------------------------------------------------
    // Test for retrieving all proxy entries
    //-------------------------------------------------------------------------
    // Setting all records as expected records initially. Once, the proxy is matched from
    // the table, the corresponding record will be marked as 0. The complete list should
    // be 0 by the end of iteration.
    for(counter = 0; counter < sizeof(proxies)/sizeof(proxies[0]); counter++)
    {
        exp_proxy_list[counter] = 1;
    }

    // Getting all proxies from DB
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_db_get_all_proxies(NULL, 0, &proxy_list));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_db_get_all_proxies(proxy_db, 0, NULL));
    TEST_ASSERT(MA_OK == ma_proxy_db_get_all_proxies(proxy_db, 0, &proxy_list));

    // Getting the count of retrieved records
    TEST_ASSERT(MA_OK == ma_proxy_list_size(proxy_list, &size_of_proxy_list));

    // Verifying that correct proxies are retrieved from DB
    for(counter = 0; counter < size_of_proxy_list; counter++)
    {
        ma_proxy_t *new_proxy = NULL;

        const char *address = NULL;
        ma_int32_t port = 0;
        ma_proxy_protocol_type_t type = MA_PROXY_TYPE_BOTH;
        ma_int32_t flag = 0;
        ma_bool_t auth = 0;
        const char *username = NULL;
        const char *password = NULL;

        TEST_ASSERT(MA_OK == ma_proxy_list_get_proxy(proxy_list, counter, &new_proxy));
        TEST_ASSERT(MA_OK == ma_proxy_get_address(new_proxy, &address));
        TEST_ASSERT(MA_OK == ma_proxy_get_port(new_proxy, &port));
        TEST_ASSERT(MA_OK == ma_proxy_get_protocol_type(new_proxy, &type));
        TEST_ASSERT(MA_OK == ma_proxy_get_flag(new_proxy, &flag));
        TEST_ASSERT(MA_OK == ma_proxy_get_authentication(new_proxy, &auth, &username, &password));

        TEST_ASSERT(proxy_matching(address, port, type, flag, auth, username, password) == 1);

        TEST_ASSERT(MA_OK == ma_proxy_release(new_proxy));
        new_proxy = NULL;
    }

    // If any element in the array is not zero, it means that one of the intended proxy
    // record is not retrieved from the database
    for(counter = 0; counter < sizeof(exp_proxy_list)/sizeof(exp_proxy_list[0]); counter++)
    {
        TEST_ASSERT(exp_proxy_list[counter] == 0);
    }

    // Releasing the proxy list
    TEST_ASSERT(MA_OK == ma_proxy_list_release(proxy_list));
    proxy_list = NULL;

    //-------------------------------------------------------------------------
    // Test for retrieving all proxy entries which are Server Configured
    //-------------------------------------------------------------------------

    // Setting all proxy records (server configured) as expected records initially.
    // Once, the proxy is matched from the table, the corresponding record will
    // be marked as 0. The complete list should be 0 by the end of iteration.
    for(counter = 0; counter < sizeof(proxies)/sizeof(proxies[0]); counter++)
    {
        if(proxies[counter].proxy_flag == 1)
        {
            exp_proxy_list[counter] = 1;
        }
    }

    // Getting all server configured proxies from DB
    TEST_ASSERT(MA_OK == ma_proxy_db_get_all_proxies(proxy_db, 1, &proxy_list));

    // Getting the count of retrieved records
    TEST_ASSERT(MA_OK == ma_proxy_list_size(proxy_list, &size_of_proxy_list));

    // Verifying that correct server configured proxies are retrieved from DB
    for(counter = 0; counter < size_of_proxy_list; counter++)
    {
        ma_proxy_t *new_proxy = NULL;

        const char *address = NULL;
        ma_int32_t port = 0;
        ma_proxy_protocol_type_t type = MA_PROXY_TYPE_BOTH;
        ma_int32_t flag = 0;
        ma_bool_t auth = 0;
        const char *username = NULL;
        const char *password = NULL;

        TEST_ASSERT(MA_OK == ma_proxy_list_get_proxy(proxy_list, counter, &new_proxy));
        TEST_ASSERT(MA_OK == ma_proxy_get_address(new_proxy, &address));
        TEST_ASSERT(MA_OK == ma_proxy_get_port(new_proxy, &port));
        TEST_ASSERT(MA_OK == ma_proxy_get_protocol_type(new_proxy, &type));
        TEST_ASSERT(MA_OK == ma_proxy_get_flag(new_proxy, &flag));
        TEST_ASSERT(MA_OK == ma_proxy_get_authentication(new_proxy, &auth, &username, &password));

        TEST_ASSERT(proxy_matching(address, port, type, flag, auth, username, password) == 1);

        TEST_ASSERT(MA_OK == ma_proxy_release(new_proxy));
        new_proxy = NULL;
    }

    // If any element in the array is not zero, it means that one of the intended proxy
    // record is not retrieved from the database
    for(counter = 0; counter < sizeof(exp_proxy_list)/sizeof(exp_proxy_list[0]); counter++)
    {
        TEST_ASSERT(exp_proxy_list[counter] == 0);
    }

    // Releasing the proxy list
    TEST_ASSERT(MA_OK == ma_proxy_list_release(proxy_list));
    proxy_list = NULL;

    //-------------------------------------------------------------------------
    // Test for retrieving all proxy entries which are Local Configured
    //-------------------------------------------------------------------------

    // Setting all proxy records (local configured) as expected records initially.
    // Once, the proxy is matched from the table, the corresponding record will
    // be marked as 0. The complete list should be 0 by the end of iteration.
    for(counter = 0; counter < sizeof(proxies)/sizeof(proxies[0]); counter++)
    {
        if(proxies[counter].proxy_flag == 2)
        {
            exp_proxy_list[counter] = 1;
        }
    }

    // Getting all local configured proxies from DB
    TEST_ASSERT(MA_OK == ma_proxy_db_get_all_proxies(proxy_db, 2, &proxy_list));

    // Getting the count of retrieved records
    TEST_ASSERT(MA_OK == ma_proxy_list_size(proxy_list, &size_of_proxy_list));

    // Verifying that correct local configured proxies are retrieved from DB
    for(counter = 0; counter < size_of_proxy_list; counter++)
    {
        ma_proxy_t *new_proxy = NULL;

        const char *address = NULL;
        ma_int32_t port = 0;
        ma_proxy_protocol_type_t type = MA_PROXY_TYPE_BOTH;
        ma_int32_t flag = 0;
        ma_bool_t auth = 0;
        const char *username = NULL;
        const char *password = NULL;

        TEST_ASSERT(MA_OK == ma_proxy_list_get_proxy(proxy_list, counter, &new_proxy));
        TEST_ASSERT(MA_OK == ma_proxy_get_address(new_proxy, &address));
        TEST_ASSERT(MA_OK == ma_proxy_get_port(new_proxy, &port));
        TEST_ASSERT(MA_OK == ma_proxy_get_protocol_type(new_proxy, &type));
        TEST_ASSERT(MA_OK == ma_proxy_get_flag(new_proxy, &flag));
        TEST_ASSERT(MA_OK == ma_proxy_get_authentication(new_proxy, &auth, &username, &password));

        TEST_ASSERT(proxy_matching(address, port, type, flag, auth, username, password) == 1);

        TEST_ASSERT(MA_OK == ma_proxy_release(new_proxy));
        new_proxy = NULL;
    }

    // If any element in the array is not zero, it means that one of the intended proxy
    // record is not retrieved from the database
    for(counter = 0; counter < sizeof(exp_proxy_list)/sizeof(exp_proxy_list[0]); counter++)
    {
        TEST_ASSERT(exp_proxy_list[counter] == 0);
    }

    // Releasing the proxy list
    TEST_ASSERT(MA_OK == ma_proxy_list_release(proxy_list));
    proxy_list = NULL;

    //-------------------------------------------------------------------------
    // Removing all proxy records
    //-------------------------------------------------------------------------
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_db_remove_all_proxies(NULL));
    TEST_ASSERT(MA_OK == ma_proxy_db_remove_all_proxies(proxy_db));

    // Retrieving all proxy entries. Retrieved proxy list should be NULL i.e. no
    // proxy records.
    TEST_ASSERT(MA_OK == ma_proxy_db_get_all_proxies(proxy_db, 0, &proxy_list));
    TEST_ASSERT_NULL(proxy_list);
}


/******************************************************************************
Test for verifying the Proxy Addition and Remove All Global Proxies APIs
******************************************************************************/
TEST(proxy_db_manager_tests, add_and_remove_all_global_proxy_test)
{
    ma_uint8_t counter = 0;
    ma_proxy_list_t *proxy_list = NULL;
    size_t size_of_proxy_list = 0;

    // Adding Proxy entries to DB
    for(counter = 0; counter < sizeof(proxies)/sizeof(proxies[0]); counter++)
    {
        // Creating Proxy object and filling all parameters
        ma_proxy_t *my_proxy = NULL;

        TEST_ASSERT(MA_OK == ma_proxy_create(&my_proxy));
        TEST_ASSERT(MA_OK == ma_proxy_set_address(my_proxy, proxies[counter].proxy_address));
        TEST_ASSERT(MA_OK == ma_proxy_set_port(my_proxy, proxies[counter].proxy_port));
        TEST_ASSERT(MA_OK == ma_proxy_set_protocol_type(my_proxy, proxies[counter].proxy_type));
        TEST_ASSERT(MA_OK == ma_proxy_set_flag(my_proxy, proxies[counter].proxy_flag));
        TEST_ASSERT(MA_OK == ma_proxy_set_authentication(my_proxy, proxies[counter].proxy_auth,
                                                                   proxies[counter].proxy_user_name,
                                                                   proxies[counter].proxy_password));

        // Adding proxy to DB
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_db_add_proxy(NULL, my_proxy));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_db_add_proxy(proxy_db, NULL));
        TEST_ASSERT(MA_OK == ma_proxy_db_add_proxy(proxy_db, my_proxy));

        // Releasing Proxy object
        TEST_ASSERT(MA_OK == ma_proxy_release(my_proxy));
    }

    //-------------------------------------------------------------------------
    // Test for deleting all global proxy records.
    //-------------------------------------------------------------------------

    // Setting all proxy records (local configured) as expected records initially.
    // Once, the proxy is matched from the table, the corresponding record will
    // be marked as 0. The complete list should be 0 by the end of iteration.
    for(counter = 0; counter < sizeof(proxies)/sizeof(proxies[0]); counter++)
    {
        if(proxies[counter].proxy_flag == 2)
        {
            exp_proxy_list[counter] = 1;
        }
    }

    // Removing all Global proxy records
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_db_remove_all_global_proxies(NULL));
    TEST_ASSERT(MA_OK == ma_proxy_db_remove_all_global_proxies(proxy_db));

    // Retrieving all global proxy entries. Retrieved proxy list should be NULL
    // i.e. no proxy records.
    TEST_ASSERT(MA_OK == ma_proxy_db_get_all_proxies(proxy_db, 1, &proxy_list));
    TEST_ASSERT_NULL(proxy_list);

    // Getting all proxies from DB (only local proxies should be remaining in the
    // DB
    TEST_ASSERT(MA_OK == ma_proxy_db_get_all_proxies(proxy_db, 0, &proxy_list));

    // Getting the count of retrieved records
    TEST_ASSERT(MA_OK == ma_proxy_list_size(proxy_list, &size_of_proxy_list));

    // Verifying that only local configured proxies are remaining in the DB
    for(counter = 0; counter < size_of_proxy_list; counter++)
    {
        ma_proxy_t *new_proxy = NULL;

        const char *address = NULL;
        ma_int32_t port = 0;
        ma_proxy_protocol_type_t type = MA_PROXY_TYPE_BOTH;
        ma_int32_t flag = 0;
        ma_bool_t auth = 0;
        const char *username = NULL;
        const char *password = NULL;

        TEST_ASSERT(MA_OK == ma_proxy_list_get_proxy(proxy_list, counter, &new_proxy));
        TEST_ASSERT(MA_OK == ma_proxy_get_address(new_proxy, &address));
        TEST_ASSERT(MA_OK == ma_proxy_get_port(new_proxy, &port));
        TEST_ASSERT(MA_OK == ma_proxy_get_protocol_type(new_proxy, &type));
        TEST_ASSERT(MA_OK == ma_proxy_get_flag(new_proxy, &flag));
        TEST_ASSERT(MA_OK == ma_proxy_get_authentication(new_proxy, &auth, &username, &password));

        TEST_ASSERT(proxy_matching(address, port, type, flag, auth, username, password) == 1);

        TEST_ASSERT(MA_OK == ma_proxy_release(new_proxy));
        new_proxy = NULL;
    }

    // If any element in the array is not zero, it means that one of the intended proxy
    // record is not retrieved from the database
    for(counter = 0; counter < sizeof(exp_proxy_list)/sizeof(exp_proxy_list[0]); counter++)
    {
        TEST_ASSERT(exp_proxy_list[counter] == 0);
    }

    // Releasing the proxy list
    TEST_ASSERT(MA_OK == ma_proxy_list_release(proxy_list));
    proxy_list = NULL;

}


/******************************************************************************
Test for verifying the Proxy Addition and Removal APIs
******************************************************************************/
TEST(proxy_db_manager_tests, add_and_remove_proxy_test)
{
    ma_uint8_t counter = 0;
    ma_proxy_list_t *proxy_list = NULL;

    // Adding Proxy entries to DB
    for(counter = 0; counter < sizeof(proxies)/sizeof(proxies[0]); counter++)
    {
        // Creating Proxy object and filling all parameters
        ma_proxy_t *my_proxy = NULL;

        TEST_ASSERT(MA_OK == ma_proxy_create(&my_proxy));
        TEST_ASSERT(MA_OK == ma_proxy_set_address(my_proxy, proxies[counter].proxy_address));
        TEST_ASSERT(MA_OK == ma_proxy_set_port(my_proxy, proxies[counter].proxy_port));
        TEST_ASSERT(MA_OK == ma_proxy_set_protocol_type(my_proxy, proxies[counter].proxy_type));
        TEST_ASSERT(MA_OK == ma_proxy_set_flag(my_proxy, proxies[counter].proxy_flag));
        TEST_ASSERT(MA_OK == ma_proxy_set_authentication(my_proxy, proxies[counter].proxy_auth,
                                                                   proxies[counter].proxy_user_name,
                                                                   proxies[counter].proxy_password));

        // Adding proxy to DB
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_db_add_proxy(NULL, my_proxy));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_db_add_proxy(proxy_db, NULL));
        TEST_ASSERT(MA_OK == ma_proxy_db_add_proxy(proxy_db, my_proxy));

        // Removing proxy from DB
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_db_remove_proxy(NULL, my_proxy));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_db_remove_proxy(proxy_db, NULL));
        TEST_ASSERT(MA_OK == ma_proxy_db_remove_proxy(proxy_db, my_proxy));

        // Releasing Proxy object
        TEST_ASSERT(MA_OK == ma_proxy_release(my_proxy));
    }

    //-------------------------------------------------------------------------
    // As any proxy which is added to DB, is being removed simultaneously, there
    // should not be any proxy at the end of iteration.

    // Retrieving all proxy entries. Retrieved proxy list should be NULL i.e. no
    // proxy records.
    TEST_ASSERT(MA_OK == ma_proxy_db_get_all_proxies(proxy_db, 0, &proxy_list));
    TEST_ASSERT_NULL(proxy_list);
}


