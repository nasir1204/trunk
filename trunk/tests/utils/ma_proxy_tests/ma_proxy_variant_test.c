/*
UTs for proxy internal APIs
*/

#include "unity.h"
#include "unity_fixture.h"
#include "ma/internal/utils/proxy/ma_proxy_internal.h"
#include <string.h>

TEST_GROUP_RUNNER(ma_proxy_variant_group)
{
    do {RUN_TEST_CASE(ma_proxy_variant_tests, proxy_variant_test); } while (0);
    do {RUN_TEST_CASE(ma_proxy_variant_tests, proxy_variant_config_test); } while (0);
    do {RUN_TEST_CASE(ma_proxy_variant_tests, proxy_config_copy_test); } while (0);
}

TEST_SETUP(ma_proxy_variant_tests)
{
}

TEST_TEAR_DOWN(ma_proxy_variant_tests)
{
}

// Lookup Table consisting of Proxy Information
struct LookupTable
{
    const char *proxy_address;
    ma_int32_t proxy_port;
    ma_proxy_protocol_type_t proxy_type;
    ma_int32_t proxy_flag;
    ma_bool_t proxy_auth;
    const char *proxy_user_name;
    const char *proxy_password;
};

static struct LookupTable const proxies[] =
{
    // Address         Port       Type                Flag   Authentication      Username            Password
    {  "199.234.34.56", 10,       MA_PROXY_TYPE_FTP,  1,     MA_TRUE,            "Administrator",    "wwehewlkheklneflke" },
    {  "199.202.18.34", 1000,     MA_PROXY_TYPE_BOTH, 2,     MA_FALSE,           NULL,               "dedjds98fudufdp#$#" },
    {  "199.234.34.34", 100000,   MA_PROXY_TYPE_HTTP, 1,     MA_FALSE,           "Administrator",    NULL                 },
    {  "199.234.34.56", 1097,     MA_PROXY_TYPE_FTP,  1,     MA_TRUE,            "Administrator",    "wwehewlkheklneflke" },
};


// Lookup Table consisting of Proxy Config Information
struct LookupTable_config
{
    ma_proxy_usage_type_t proxy_config_usage_type;
    ma_proxy_protocol_type_t proxy_config_proto_type;
    ma_bool_t proxy_config_auth;
    const char *proxy_config_user_name;
    const char *proxy_config_password;
    ma_bool_t proxy_config_local_proxy;
    ma_bool_t proxy_config_bypass_local;
    const char *proxy_config_exclusion_str;
};

static struct LookupTable_config const proxy_configs[] =
{
    // Address                                Protocol Type       Authentication  Username         Password              Local Proxy Bypass Local Exclusion STR
    {  MA_PROXY_USAGE_TYPE_SYSTEM_CONFIGURED, MA_PROXY_TYPE_FTP,  MA_TRUE,        "Administrator", "wwehewlkheklneflke", MA_TRUE,    MA_TRUE,     "efmfe;fmefmefm"  },
    {  MA_PROXY_USAGE_TYPE_SERVER_CONFIGURED, MA_PROXY_TYPE_BOTH, MA_TRUE,        "Normal User",   "dedjds98fudufdp#$#", MA_FALSE,   MA_FALSE,    "efmfe;fmef eejr" },
    {  MA_PROXY_USAGE_TYPE_SYSTEM_CONFIGURED, MA_PROXY_TYPE_HTTP, MA_FALSE,       NULL,            NULL,                 MA_FALSE,   MA_TRUE,     "efmfe;df   fm"   },
    {  MA_PROXY_USAGE_TYPE_NONE,              MA_PROXY_TYPE_FTP,  MA_TRUE,        "Administrator", "#@$#@$#@$#@$#@$#@",  MA_TRUE,    MA_FALSE,    "dmldmf3243##$"   },
};


TEST(ma_proxy_variant_tests, proxy_variant_test)
{
    ma_proxy_t *my_proxy = NULL;
    ma_proxy_t *new_proxy = NULL;

    // Proxy variant
    ma_variant_t *proxy_variant = NULL;

    ma_uint8_t counter = 0;

    for(counter = 0; counter < sizeof(proxies)/sizeof(proxies[0]); counter++)
    {
        // Creating proxy object and setting the parameters
        TEST_ASSERT(MA_OK == ma_proxy_create(&my_proxy));
        TEST_ASSERT(MA_OK == ma_proxy_set_address(my_proxy, proxies[counter].proxy_address));
        TEST_ASSERT(MA_OK == ma_proxy_set_port(my_proxy, proxies[counter].proxy_port));
        TEST_ASSERT(MA_OK == ma_proxy_set_protocol_type(my_proxy, proxies[counter].proxy_type));
        TEST_ASSERT(MA_OK == ma_proxy_set_flag(my_proxy, proxies[counter].proxy_flag));
        TEST_ASSERT(MA_OK == ma_proxy_set_authentication(my_proxy, proxies[counter].proxy_auth,
                                                                   proxies[counter].proxy_user_name,
                                                                   proxies[counter].proxy_password));

        // Convert proxy object to variant and then release proxy
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_as_variant(NULL, &proxy_variant));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_as_variant(my_proxy, NULL));
        TEST_ASSERT(MA_OK == ma_proxy_as_variant(my_proxy, &proxy_variant));
        TEST_ASSERT(MA_OK == ma_proxy_release(my_proxy));

        // Obtain proxy object from variant and then, release the variant
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_from_variant(NULL, &new_proxy));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_from_variant(proxy_variant, NULL));
        TEST_ASSERT(MA_OK == ma_proxy_from_variant(proxy_variant, &new_proxy));
        TEST_ASSERT(MA_OK == ma_variant_release(proxy_variant));

        // Verifying the parameters
        {
            const char *address = NULL;
            TEST_ASSERT(MA_OK == ma_proxy_get_address(new_proxy, &address));
            TEST_ASSERT(strcmp(proxies[counter].proxy_address, address) == 0);
        }

        {
            ma_int32_t port = 0;
            TEST_ASSERT(MA_OK == ma_proxy_get_port(new_proxy, &port));
            TEST_ASSERT(proxies[counter].proxy_port == port);
        }

        {
            ma_proxy_protocol_type_t type = MA_PROXY_TYPE_BOTH;
            TEST_ASSERT(MA_OK == ma_proxy_get_protocol_type(new_proxy, &type));
            TEST_ASSERT(proxies[counter].proxy_type == type);
        }

        {
            ma_int32_t flag = 0;
            TEST_ASSERT(MA_OK == ma_proxy_get_flag(new_proxy, &flag));
            TEST_ASSERT(proxies[counter].proxy_flag == flag);
        }

        {
            ma_bool_t auth = MA_FALSE;
            const char *username = NULL;
            const char *password = NULL;
            TEST_ASSERT(MA_OK == ma_proxy_get_authentication(new_proxy, &auth, &username, &password));
            TEST_ASSERT(proxies[counter].proxy_auth == auth);

            if(proxies[counter].proxy_auth)
            {
                TEST_ASSERT(strcmp(proxies[counter].proxy_user_name, username) == 0);
                TEST_ASSERT(strcmp(proxies[counter].proxy_password, password) == 0);
            }
            else
            {
                TEST_ASSERT_NULL(username);
                TEST_ASSERT_NULL(password);
            }
        }

        TEST_ASSERT(MA_OK == ma_proxy_release(new_proxy));
    }
}

TEST(ma_proxy_variant_tests, proxy_variant_config_test)
{
    ma_uint8_t counter = 0;
    ma_proxy_config_t *my_proxy_config = NULL;
    ma_proxy_config_t *new_proxy_config = NULL;

    // Proxy config variant
    ma_variant_t *proxy_config_variant = NULL;

    for(counter = 0; counter < sizeof(proxy_configs)/sizeof(proxy_configs[0]); counter++)
    {
        ma_proxy_list_t *my_proxy_list = NULL;

        // Creating proxy config object and setting the parameters
        {
            TEST_ASSERT(MA_OK == ma_proxy_config_create(&my_proxy_config));
            TEST_ASSERT(MA_OK == ma_proxy_config_set_proxy_usage_type(my_proxy_config, proxy_configs[counter].proxy_config_usage_type));
            TEST_ASSERT(MA_OK == ma_proxy_config_set_system_proxy_authentication(my_proxy_config,
                                                                                 proxy_configs[counter].proxy_config_proto_type,
                                                                                 proxy_configs[counter].proxy_config_auth,
                                                                                 proxy_configs[counter].proxy_config_user_name,
                                                                                 proxy_configs[counter].proxy_config_password));
            TEST_ASSERT(MA_OK == ma_proxy_config_set_allow_local_proxy_configuration(my_proxy_config, proxy_configs[counter].proxy_config_local_proxy));
            TEST_ASSERT(MA_OK == ma_proxy_config_set_bypass_local(my_proxy_config, proxy_configs[counter].proxy_config_bypass_local));
            TEST_ASSERT(MA_OK == ma_proxy_config_set_exclusion_urls(my_proxy_config, proxy_configs[counter].proxy_config_exclusion_str));

            {
                ma_proxy_t * my_proxy = NULL;

                // Creating a proxy setting, adding it to proxy list and then releasing proxy object.
                TEST_ASSERT(MA_OK == ma_proxy_create(&my_proxy));
                TEST_ASSERT(MA_OK == ma_proxy_set_address(my_proxy, "199.16.234.34"));
                TEST_ASSERT(MA_OK == ma_proxy_set_port(my_proxy, 8440));
                TEST_ASSERT(MA_OK == ma_proxy_set_protocol_type(my_proxy, MA_PROXY_TYPE_FTP));
                TEST_ASSERT(MA_OK == ma_proxy_set_flag(my_proxy, 1));
                TEST_ASSERT(MA_OK == ma_proxy_set_authentication(my_proxy, MA_FALSE, NULL, NULL));

                // Creating Proxy List
                TEST_ASSERT(MA_OK == ma_proxy_list_create(&my_proxy_list));
                TEST_ASSERT(MA_OK == ma_proxy_list_add_proxy(my_proxy_list, my_proxy));
                TEST_ASSERT(MA_OK == ma_proxy_release(my_proxy));

                // Setting proxy list in proxy configuration
                TEST_ASSERT(MA_OK == ma_proxy_config_set_proxy_list(my_proxy_config, my_proxy_list));
            }
        }

        // Convert proxy config object to variant and then release proxy config object
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_config_as_variant(NULL, &proxy_config_variant));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_config_as_variant(my_proxy_config, NULL));
        TEST_ASSERT(MA_OK == ma_proxy_config_as_variant(my_proxy_config, &proxy_config_variant));
        TEST_ASSERT(MA_OK == ma_proxy_config_release(my_proxy_config));

        // Obtain proxy config object from variant and then, release the variant
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_config_from_variant(NULL, &new_proxy_config));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_config_from_variant(proxy_config_variant, NULL));
        TEST_ASSERT(MA_OK == ma_proxy_config_from_variant(proxy_config_variant, &new_proxy_config));
        TEST_ASSERT(MA_OK == ma_variant_release(proxy_config_variant));

        // Verifying the parameters
        {
            ma_proxy_usage_type_t type = MA_PROXY_USAGE_TYPE_SYSTEM_CONFIGURED;
            TEST_ASSERT(MA_OK == ma_proxy_config_get_proxy_usage_type(new_proxy_config, &type));
            TEST_ASSERT(proxy_configs[counter].proxy_config_usage_type == type);
        }

        {
            ma_bool_t auth = MA_FALSE;
            const char *username = NULL;
            const char *password = NULL;
            TEST_ASSERT(MA_OK == ma_proxy_config_get_system_proxy_authentication(new_proxy_config, proxy_configs[counter].proxy_config_proto_type, &auth, &username, &password));
            TEST_ASSERT(proxy_configs[counter].proxy_config_auth == auth);

            if(proxy_configs[counter].proxy_config_auth)
            {
                TEST_ASSERT(strcmp(proxy_configs[counter].proxy_config_user_name, username) == 0);
                TEST_ASSERT(strcmp(proxy_configs[counter].proxy_config_password, password) == 0);
            }
            else
            {
                TEST_ASSERT_NULL(username);
                TEST_ASSERT_NULL(password);
            }
        }

        {
            ma_bool_t allow_or_not = MA_FALSE;
            TEST_ASSERT(MA_OK == ma_proxy_config_get_allow_local_proxy_configuration(new_proxy_config, &allow_or_not));
            TEST_ASSERT(proxy_configs[counter].proxy_config_local_proxy == allow_or_not);
        }

        {
            ma_bool_t bypass_local = MA_TRUE;
            TEST_ASSERT(MA_OK == ma_proxy_config_get_bypass_local(new_proxy_config, &bypass_local));
            TEST_ASSERT(proxy_configs[counter].proxy_config_bypass_local == bypass_local);
        }

        {
            const char *exclusion_str = NULL;
            TEST_ASSERT(MA_OK == ma_proxy_config_get_exclusion_urls(new_proxy_config, &exclusion_str));
            TEST_ASSERT(strcmp(proxy_configs[counter].proxy_config_exclusion_str, exclusion_str) == 0);
        }

        {
            ma_proxy_list_t *new_proxy_list = NULL;
            ma_proxy_t *new_proxy = NULL;

            TEST_ASSERT(MA_OK == ma_proxy_config_get_proxy_list(new_proxy_config, &new_proxy_list));

            // Retrieving one and only element from the list
            TEST_ASSERT(MA_OK == ma_proxy_list_get_proxy(new_proxy_list, 0, &new_proxy));

            {
                const char *address = NULL;
                TEST_ASSERT(MA_OK == ma_proxy_get_address(new_proxy, &address));
                TEST_ASSERT(strcmp("199.16.234.34", address) == 0);
            }

            {
                ma_int32_t port = 0;
                TEST_ASSERT(MA_OK == ma_proxy_get_port(new_proxy, &port));
                TEST_ASSERT(8440 == port);
            }

            {
                ma_proxy_protocol_type_t type = MA_PROXY_TYPE_BOTH;
                TEST_ASSERT(MA_OK == ma_proxy_get_protocol_type(new_proxy, &type));
                TEST_ASSERT(MA_PROXY_TYPE_FTP == type);
            }

            {
                ma_int32_t flag = 0;
                TEST_ASSERT(MA_OK == ma_proxy_get_flag(new_proxy, &flag));
            }

            {
                ma_bool_t auth = MA_FALSE;
                const char *username = NULL;
                const char *password = NULL;
                TEST_ASSERT(MA_OK == ma_proxy_get_authentication(new_proxy, &auth, &username, &password));
                TEST_ASSERT(MA_FALSE == auth);
                TEST_ASSERT_NULL(username);
                TEST_ASSERT_NULL(password);
            }

            TEST_ASSERT(MA_OK == ma_proxy_release(new_proxy));
            TEST_ASSERT(MA_OK == ma_proxy_list_release(new_proxy_list));
        }

        TEST_ASSERT(MA_OK == ma_proxy_config_release(new_proxy_config));
    }
}


TEST(ma_proxy_variant_tests, proxy_config_copy_test)
{
    ma_uint8_t counter = 0;
    ma_proxy_config_t *my_proxy_config = NULL;
    ma_proxy_config_t *new_proxy_config = NULL;

    for(counter = 0; counter < sizeof(proxy_configs)/sizeof(proxy_configs[0]); counter++)
    {
        // Creating proxy config object and setting the parameters
        {
            TEST_ASSERT(MA_OK == ma_proxy_config_create(&my_proxy_config));
            TEST_ASSERT(MA_OK == ma_proxy_config_set_proxy_usage_type(my_proxy_config, proxy_configs[counter].proxy_config_usage_type));
            TEST_ASSERT(MA_OK == ma_proxy_config_set_system_proxy_authentication(my_proxy_config,
                                                                                 proxy_configs[counter].proxy_config_proto_type,
                                                                                 proxy_configs[counter].proxy_config_auth,
                                                                                 proxy_configs[counter].proxy_config_user_name,
                                                                                 proxy_configs[counter].proxy_config_password));
            TEST_ASSERT(MA_OK == ma_proxy_config_set_allow_local_proxy_configuration(my_proxy_config, proxy_configs[counter].proxy_config_local_proxy));
            TEST_ASSERT(MA_OK == ma_proxy_config_set_bypass_local(my_proxy_config, proxy_configs[counter].proxy_config_bypass_local));
            TEST_ASSERT(MA_OK == ma_proxy_config_set_exclusion_urls(my_proxy_config, proxy_configs[counter].proxy_config_exclusion_str));

            {
                ma_proxy_t * my_proxy = NULL;
                ma_proxy_list_t *my_proxy_list = NULL;

                // Creating a proxy setting, adding it to proxy list and then releasing proxy object.
                TEST_ASSERT(MA_OK == ma_proxy_create(&my_proxy));
                TEST_ASSERT(MA_OK == ma_proxy_set_address(my_proxy, "199.16.234.34"));
                TEST_ASSERT(MA_OK == ma_proxy_set_port(my_proxy, 8440));
                TEST_ASSERT(MA_OK == ma_proxy_set_protocol_type(my_proxy, MA_PROXY_TYPE_FTP));
                TEST_ASSERT(MA_OK == ma_proxy_set_flag(my_proxy, 1));
                TEST_ASSERT(MA_OK == ma_proxy_set_authentication(my_proxy, MA_FALSE, NULL, NULL));

                // Creating Proxy List
                TEST_ASSERT(MA_OK == ma_proxy_list_create(&my_proxy_list));
                TEST_ASSERT(MA_OK == ma_proxy_list_add_proxy(my_proxy_list, my_proxy));
                TEST_ASSERT(MA_OK == ma_proxy_release(my_proxy));

                // Setting proxy list in proxy configuration
                TEST_ASSERT(MA_OK == ma_proxy_config_set_proxy_list(my_proxy_config, my_proxy_list));
            }
        }

        // Copying the proxy config object and then, releasing the first proxy config object
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_config_copy(NULL, &new_proxy_config));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_config_copy(my_proxy_config, NULL));
        TEST_ASSERT(MA_OK == ma_proxy_config_copy(my_proxy_config, &new_proxy_config));
        TEST_ASSERT(MA_OK == ma_proxy_config_release(my_proxy_config));

        // Verifying the parameters with new proxy config object
        {
            ma_proxy_usage_type_t type = MA_PROXY_USAGE_TYPE_SYSTEM_CONFIGURED;
            TEST_ASSERT(MA_OK == ma_proxy_config_get_proxy_usage_type(new_proxy_config, &type));
            TEST_ASSERT(proxy_configs[counter].proxy_config_usage_type == type);
        }

        {
            ma_bool_t auth = MA_FALSE;
            const char *username = NULL;
            const char *password = NULL;
            TEST_ASSERT(MA_OK == ma_proxy_config_get_system_proxy_authentication(new_proxy_config, proxy_configs[counter].proxy_config_proto_type, &auth, &username, &password));
            TEST_ASSERT(proxy_configs[counter].proxy_config_auth == auth);

            if(proxy_configs[counter].proxy_config_auth)
            {
                TEST_ASSERT(strcmp(proxy_configs[counter].proxy_config_user_name, username) == 0);
                TEST_ASSERT(strcmp(proxy_configs[counter].proxy_config_password, password) == 0);
            }
            else
            {
                TEST_ASSERT_NULL(username);
                TEST_ASSERT_NULL(password);
            }
        }

        {
            ma_bool_t allow_or_not = MA_FALSE;
            TEST_ASSERT(MA_OK == ma_proxy_config_get_allow_local_proxy_configuration(new_proxy_config, &allow_or_not));
            TEST_ASSERT(proxy_configs[counter].proxy_config_local_proxy == allow_or_not);
        }

        {
            ma_bool_t bypass_local = MA_TRUE;
            TEST_ASSERT(MA_OK == ma_proxy_config_get_bypass_local(new_proxy_config, &bypass_local));
            TEST_ASSERT(proxy_configs[counter].proxy_config_bypass_local == bypass_local);
        }

        {
            const char *exclusion_str = NULL;
            TEST_ASSERT(MA_OK == ma_proxy_config_get_exclusion_urls(new_proxy_config, &exclusion_str));
            TEST_ASSERT(strcmp(proxy_configs[counter].proxy_config_exclusion_str, exclusion_str) == 0);
        }

        {
            ma_proxy_list_t *new_proxy_list = NULL;
            ma_proxy_t *new_proxy = NULL;

            TEST_ASSERT(MA_OK == ma_proxy_config_get_proxy_list(new_proxy_config, &new_proxy_list));

            // Retrieving one and only element from the list
            TEST_ASSERT(MA_OK == ma_proxy_list_get_proxy(new_proxy_list, 0, &new_proxy));

            {
                const char *address = NULL;
                TEST_ASSERT(MA_OK == ma_proxy_get_address(new_proxy, &address));
                TEST_ASSERT(strcmp("199.16.234.34", address) == 0);
            }

            {
                ma_int32_t port = 0;
                TEST_ASSERT(MA_OK == ma_proxy_get_port(new_proxy, &port));
                TEST_ASSERT(8440 == port);
            }

            {
                ma_proxy_protocol_type_t type = MA_PROXY_TYPE_BOTH;
                TEST_ASSERT(MA_OK == ma_proxy_get_protocol_type(new_proxy, &type));
                TEST_ASSERT(MA_PROXY_TYPE_FTP == type);
            }

            {
                ma_int32_t flag = 0;
                TEST_ASSERT(MA_OK == ma_proxy_get_flag(new_proxy, &flag));
            }

            {
                ma_bool_t auth = MA_FALSE;
                const char *username = NULL;
                const char *password = NULL;
                TEST_ASSERT(MA_OK == ma_proxy_get_authentication(new_proxy, &auth, &username, &password));
                TEST_ASSERT(MA_FALSE == auth);
                TEST_ASSERT_NULL(username);
                TEST_ASSERT_NULL(password);
            }

            TEST_ASSERT(MA_OK == ma_proxy_release(new_proxy));
            TEST_ASSERT(MA_OK == ma_proxy_list_release(new_proxy_list));
        }

        TEST_ASSERT(MA_OK == ma_proxy_config_release(new_proxy_config));
    }
}




