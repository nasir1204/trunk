/*
Proxy Config UTs
*/

#include "unity.h"
#include "unity_fixture.h"
#include "ma/proxy/ma_proxy_config.h"
#include <string.h>

TEST_GROUP_RUNNER(ma_proxy_config_tests_group)
{
    do {RUN_TEST_CASE(ma_proxy_config_tests, proxy_config_create_release_test); } while (0);
}

TEST_SETUP(ma_proxy_config_tests)
{
}

TEST_TEAR_DOWN(ma_proxy_config_tests)
{
}

// Lookup Table consisting of Proxy Config Information
struct LookupTable
{
    ma_proxy_usage_type_t proxy_config_usage_type;
    ma_proxy_protocol_type_t proxy_config_proto_type;
    ma_bool_t proxy_config_auth;
    const char *proxy_config_user_name;
    const char *proxy_config_password;
    ma_bool_t proxy_config_local_proxy;
    ma_bool_t proxy_config_bypass_local;
    const char *proxy_config_exclusion_str;
};

static struct LookupTable const proxy_configs[] =
{
    // Address                                Protocol Type        Authentication  Username         Password              Local Proxy Bypass Local Exclusion STR
    {  MA_PROXY_USAGE_TYPE_NONE,              MA_PROXY_TYPE_HTTP,  MA_FALSE,       "SuperUser",     "#@$#@$#@$#@$#@$#@",  MA_TRUE,    MA_FALSE,    "dmldmf3243##$"   },
    {  MA_PROXY_USAGE_TYPE_SYSTEM_CONFIGURED, MA_PROXY_TYPE_FTP,   MA_TRUE,        "Administrator", "wwehewlkheklneflke", MA_TRUE,    MA_TRUE,     "efmfe;fmefmefm"  },
    {  MA_PROXY_USAGE_TYPE_SERVER_CONFIGURED, MA_PROXY_TYPE_BOTH,  MA_TRUE,        "Normal User",   "dedjds98fudufdp#$#", MA_FALSE,   MA_FALSE,    "ef4io5867yh4nrlfmef eejr" },
    {  MA_PROXY_USAGE_TYPE_SYSTEM_CONFIGURED, MA_PROXY_TYPE_HTTP,  MA_FALSE,       NULL,            NULL,                 MA_FALSE,   MA_TRUE,     "efmfe;df   fm"   },
    {  MA_PROXY_USAGE_TYPE_NONE,              MA_PROXY_TYPE_FTP,   MA_TRUE,        "Administrator", "#@$#@$#@$#@$#@$#@",  MA_TRUE,    MA_FALSE,    "dmldmf3243##$"   },
    {  MA_PROXY_USAGE_TYPE_NONE,              MA_PROXY_TYPE_HTTP,  MA_FALSE,       NULL,            "#@$#@$#@$#@$#@$#@",  MA_TRUE,    MA_FALSE,    "doey43##$"       },
    {  MA_PROXY_USAGE_TYPE_SYSTEM_CONFIGURED, MA_PROXY_TYPE_HTTP,  MA_FALSE,       "New User",      NULL,                 MA_FALSE,   MA_TRUE,     "efm349857  fm"   },
    {  MA_PROXY_USAGE_TYPE_NONE,              MA_PROXY_TYPE_FTP,   MA_FALSE,       "SuperUser",     "#@$#@$#@$#@$#@$#@",  MA_TRUE,    MA_FALSE,    "dmldmf3243##$"   },
    {  MA_PROXY_USAGE_TYPE_NONE,              MA_PROXY_TYPE_FTP,   MA_FALSE,       NULL,            "#@$#@$#@$#@$#@$#@",  MA_TRUE,    MA_FALSE,    "dml4786548356435786893456438243##$"   },
    {  MA_PROXY_USAGE_TYPE_SYSTEM_CONFIGURED, MA_PROXY_TYPE_FTP,   MA_FALSE,       "New User",      NULL,                 MA_FALSE,   MA_TRUE,     "efm349857  fm"   },
};

TEST(ma_proxy_config_tests, proxy_config_create_release_test)
{
    ma_uint8_t counter = 0;

    ma_proxy_config_t *my_proxy_config = NULL;

    for(counter = 0; counter < sizeof(proxy_configs)/sizeof(proxy_configs[0]); counter++)
    {
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_config_create(NULL));
        TEST_ASSERT(MA_OK == ma_proxy_config_create(&my_proxy_config));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_config_set_proxy_usage_type(NULL, proxy_configs[counter].proxy_config_usage_type));
        TEST_ASSERT(MA_OK == ma_proxy_config_set_proxy_usage_type(my_proxy_config, proxy_configs[counter].proxy_config_usage_type));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_config_set_system_proxy_authentication(NULL,
                                                                                           proxy_configs[counter].proxy_config_proto_type,
                                                                                           proxy_configs[counter].proxy_config_auth,
                                                                                           proxy_configs[counter].proxy_config_user_name,
                                                                                           proxy_configs[counter].proxy_config_password));

        TEST_ASSERT(MA_OK == ma_proxy_config_set_system_proxy_authentication(my_proxy_config,
                                                                             proxy_configs[counter].proxy_config_proto_type,
                                                                             proxy_configs[counter].proxy_config_auth,
                                                                             proxy_configs[counter].proxy_config_user_name,
                                                                             proxy_configs[counter].proxy_config_password));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_config_set_allow_local_proxy_configuration(NULL, proxy_configs[counter].proxy_config_local_proxy));
        TEST_ASSERT(MA_OK == ma_proxy_config_set_allow_local_proxy_configuration(my_proxy_config, proxy_configs[counter].proxy_config_local_proxy));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_config_set_bypass_local(NULL, proxy_configs[counter].proxy_config_bypass_local));
        TEST_ASSERT(MA_OK == ma_proxy_config_set_bypass_local(my_proxy_config, proxy_configs[counter].proxy_config_bypass_local));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_config_set_exclusion_urls(NULL, proxy_configs[counter].proxy_config_exclusion_str));
        TEST_ASSERT(MA_OK == ma_proxy_config_set_exclusion_urls(my_proxy_config, NULL));
        TEST_ASSERT(MA_OK == ma_proxy_config_set_exclusion_urls(my_proxy_config, proxy_configs[counter].proxy_config_exclusion_str));

        {
            ma_proxy_usage_type_t type = MA_PROXY_USAGE_TYPE_SYSTEM_CONFIGURED;
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_config_get_proxy_usage_type(NULL, &type));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_config_get_proxy_usage_type(my_proxy_config, NULL));
            TEST_ASSERT(MA_OK == ma_proxy_config_get_proxy_usage_type(my_proxy_config, &type));
            TEST_ASSERT(proxy_configs[counter].proxy_config_usage_type == type);
        }

        {
            ma_bool_t auth = MA_FALSE;
            const char *username = NULL;
            const char *password = NULL;
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_config_get_system_proxy_authentication(NULL, proxy_configs[counter].proxy_config_proto_type, &auth, &username, &password));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_config_get_system_proxy_authentication(my_proxy_config, proxy_configs[counter].proxy_config_proto_type, NULL, &username, &password));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_config_get_system_proxy_authentication(my_proxy_config, proxy_configs[counter].proxy_config_proto_type, &auth, NULL, &password));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_config_get_system_proxy_authentication(my_proxy_config, proxy_configs[counter].proxy_config_proto_type, &auth, &username, NULL));
            TEST_ASSERT(MA_OK == ma_proxy_config_get_system_proxy_authentication(my_proxy_config, proxy_configs[counter].proxy_config_proto_type, &auth, &username, &password));

            TEST_ASSERT(proxy_configs[counter].proxy_config_auth == auth);

            // If authentication is enabled, verify both username and password.
            // If authentication is disabled, but both username and password are given, verify those.
            // Otherwise, verify that username and password remain initialized to NULL
            if(proxy_configs[counter].proxy_config_auth)
            {
                TEST_ASSERT(strcmp(proxy_configs[counter].proxy_config_user_name, username) == 0);
                TEST_ASSERT(strcmp(proxy_configs[counter].proxy_config_password, password) == 0);
            }
            else if (proxy_configs[counter].proxy_config_user_name && proxy_configs[counter].proxy_config_password)
            {
                TEST_ASSERT(strcmp(proxy_configs[counter].proxy_config_user_name, username) == 0);
                TEST_ASSERT(strcmp(proxy_configs[counter].proxy_config_password, password) == 0);
            }
            else
            {
                TEST_ASSERT_NULL(username);
                TEST_ASSERT_NULL(password);
            }
        }

        {
            ma_bool_t allow_or_not = MA_FALSE;
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_config_get_allow_local_proxy_configuration(NULL, &allow_or_not));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_config_get_allow_local_proxy_configuration(my_proxy_config, NULL));
            TEST_ASSERT(MA_OK == ma_proxy_config_get_allow_local_proxy_configuration(my_proxy_config, &allow_or_not));
            TEST_ASSERT(proxy_configs[counter].proxy_config_local_proxy == allow_or_not);
        }

        {
            ma_bool_t bypass_local = MA_TRUE;
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_config_get_bypass_local(NULL, &bypass_local));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_config_get_bypass_local(my_proxy_config, NULL));
            TEST_ASSERT(MA_OK == ma_proxy_config_get_bypass_local(my_proxy_config, &bypass_local));
            TEST_ASSERT(proxy_configs[counter].proxy_config_bypass_local == bypass_local);
        }

        {
            const char *exclusion_str = NULL;
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_config_get_exclusion_urls(NULL, &exclusion_str));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_config_get_exclusion_urls(my_proxy_config, NULL));
            TEST_ASSERT(MA_OK == ma_proxy_config_get_exclusion_urls(my_proxy_config, &exclusion_str));
            TEST_ASSERT(strcmp(proxy_configs[counter].proxy_config_exclusion_str, exclusion_str) == 0);
        }

        // Set/get Proxy List APIs
        {
            ma_proxy_list_t *my_proxy_list = NULL;
            ma_proxy_t *my_proxy = NULL;

            ma_proxy_list_t *new_proxy_list = NULL; // Retrieved proxy list
            ma_proxy_t *new_proxy = NULL; // Retrieved proxy object
            size_t size = 0;

            // Creating Proxy List
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_list_create(NULL));
            TEST_ASSERT(MA_OK == ma_proxy_list_create(&my_proxy_list));

            // Creating a proxy setting, adding it to list and then releasing proxy object.
            TEST_ASSERT(MA_OK == ma_proxy_create(&my_proxy));
            TEST_ASSERT(MA_OK == ma_proxy_set_address(my_proxy, "199.16.234.34"));
            TEST_ASSERT(MA_OK == ma_proxy_set_port(my_proxy, 8440));
            TEST_ASSERT(MA_OK == ma_proxy_set_protocol_type(my_proxy, MA_PROXY_TYPE_FTP));
            TEST_ASSERT(MA_OK == ma_proxy_set_flag(my_proxy, 1));
            TEST_ASSERT(MA_OK == ma_proxy_set_authentication(my_proxy, MA_FALSE, NULL, NULL));

            TEST_ASSERT(MA_OK == ma_proxy_list_add_proxy(my_proxy_list, my_proxy));
            TEST_ASSERT(MA_OK == ma_proxy_release(my_proxy));

            // Size is 1
            TEST_ASSERT(MA_OK == ma_proxy_list_size(my_proxy_list, &size));
            TEST_ASSERT(1 == size);

            // Setting proxy list in proxy configuration
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_config_set_proxy_list(NULL, my_proxy_list));
            TEST_ASSERT(MA_OK == ma_proxy_config_set_proxy_list(my_proxy_config, my_proxy_list));

            // Retrieving proxy list from proxy configuration
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_config_get_proxy_list(NULL, &new_proxy_list));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_config_get_proxy_list(my_proxy_config, NULL));
            TEST_ASSERT(MA_OK == ma_proxy_config_get_proxy_list(my_proxy_config, &new_proxy_list));

            // Retrieving one and only element from the list
            TEST_ASSERT(MA_OK == ma_proxy_list_get_proxy(new_proxy_list, 0, &new_proxy));

            {
                const char *address = NULL;
                TEST_ASSERT(MA_OK == ma_proxy_get_address(new_proxy, &address));
                TEST_ASSERT(strcmp("199.16.234.34", address) == 0);
            }

            {
                ma_int32_t port = 0;
                TEST_ASSERT(MA_OK == ma_proxy_get_port(new_proxy, &port));
                TEST_ASSERT(8440 == port);
            }

            {
                ma_proxy_protocol_type_t type = MA_PROXY_TYPE_BOTH;
                TEST_ASSERT(MA_OK == ma_proxy_get_protocol_type(new_proxy, &type));
                TEST_ASSERT(MA_PROXY_TYPE_FTP == type);
            }

            {
                ma_int32_t flag = 0;
                TEST_ASSERT(MA_OK == ma_proxy_get_flag(new_proxy, &flag));
            }

            {
                ma_bool_t auth = MA_FALSE;
                const char *username = NULL;
                const char *password = NULL;
                TEST_ASSERT(MA_OK == ma_proxy_get_authentication(new_proxy, &auth, &username, &password));
                TEST_ASSERT(MA_FALSE == auth);
                TEST_ASSERT_NULL(username);
                TEST_ASSERT_NULL(password);
            }

            TEST_ASSERT(MA_OK == ma_proxy_release(new_proxy));
            TEST_ASSERT(MA_OK == ma_proxy_list_release(new_proxy_list));
        }


        // Adding 10 reference to the proxy config object and then, releasing it 11 times.
        for(counter = 0; counter < 10; counter++)
        {
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_config_add_ref(NULL));
            TEST_ASSERT(MA_OK == ma_proxy_config_add_ref(my_proxy_config));
        }

        for(counter = 0; counter < 11; counter++)
        {
            // Proxy Config Release
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_config_release(NULL));
            TEST_ASSERT(MA_OK == ma_proxy_config_release(my_proxy_config));
        }
    }

    // Username and password should be provided, if the authentication is set to MA_TRUE
    {
        ma_proxy_config_t *my_proxy_config = NULL;

        TEST_ASSERT(MA_OK == ma_proxy_config_create(&my_proxy_config));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_config_set_system_proxy_authentication(my_proxy_config,
                                                                                           MA_PROXY_TYPE_FTP,
                                                                                           MA_TRUE,
                                                                                           NULL,
                                                                                           "eljml;ekrnjr;ln cv;e098"));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_proxy_config_set_system_proxy_authentication(my_proxy_config,
                                                                                           MA_PROXY_TYPE_HTTP,
                                                                                           MA_TRUE,
                                                                                           "Trusted User",
                                                                                           NULL));
        TEST_ASSERT(MA_OK == ma_proxy_config_release(my_proxy_config));
    }
}


