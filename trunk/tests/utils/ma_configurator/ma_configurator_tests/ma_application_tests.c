#include "unity.h"
#include "unity_fixture.h"
#include "unity_internals.h" 

#include "ma/internal/utils/app_info/ma_registry_store.h"
#include "ma/internal/utils/app_info/ma_application.h"
#include "ma_application_internal.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


ma_error_t ma_application_create(ma_application_t **application);
ma_error_t ma_application_release(ma_application_t *application);
ma_error_t ma_application_set(ma_application_t *application, const char *key, const char *value);
ma_error_t ma_application_integration_set(ma_application_t *application, const char *key, const ma_int32_t value);

TEST_GROUP_RUNNER(ma_application_tests_group) {
    do {RUN_TEST_CASE(ma_application_tests, ma_application_create_tests)} while (0);    
    do {RUN_TEST_CASE(ma_application_tests, ma_application_release_tests)} while (0);    
}

TEST_SETUP(ma_application_tests) {}

TEST_TEAR_DOWN(ma_application_tests) {}



TEST(ma_application_tests, ma_application_create_tests) {
	ma_application_t *application = NULL;
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_application_create(NULL));
	TEST_ASSERT(MA_OK == ma_application_create(&application));
	TEST_ASSERT(MA_OK == ma_application_release(application));
	application = NULL;
}

TEST(ma_application_tests, ma_application_release_tests) {
	ma_application_t *application = NULL;
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_application_release(NULL));
	TEST_ASSERT(MA_OK == ma_application_create(&application));
	TEST_ASSERT(MA_OK == ma_application_release(application));
	application = NULL;
}

