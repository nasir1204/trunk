#include "ma/internal/utils/app_info/ma_registry_store.h"
#include "ma/internal/utils/app_info/ma_application.h"
#include "ma_application_internal.h"

#include "unity.h"
#include "unity_fixture.h"
#include "unity_internals.h" 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern ma_bool_t isSystem64Bit();

#ifdef MA_WINDOWS
#include <Windows.h>
#include "ma/datastore/ma_ds_registry.h"
#endif
#include "configurator_tests.h"



TEST_GROUP_RUNNER(ma_registry_store_tests_group) { 
	do {RUN_TEST_CASE(ma_registry_store_tests, ma_registry_store_create_test)} while (0);   
	do {RUN_TEST_CASE(ma_registry_store_tests, ma_registry_store_release_test)} while (0);  
	do {RUN_TEST_CASE(ma_registry_store_tests, ma_registry_store_scan_test)} while (0);  
	do {RUN_TEST_CASE(ma_registry_store_tests, ma_registry_store_get_agent_install_path_test)} while (0);  
	do {RUN_TEST_CASE(ma_registry_store_tests, ma_registry_store_get_agent_data_path_test)} while (0);  
	do {RUN_TEST_CASE(ma_registry_store_tests, ma_registry_store_get_application_test)} while (0);   
	do {RUN_TEST_CASE(ma_registry_store_tests, ma_registry_store_get_next_application_test)} while (0);   
}

TEST_SETUP(ma_registry_store_tests) {

}

TEST_TEAR_DOWN(ma_registry_store_tests) {

}


#ifdef MA_WINDOWS
ma_error_t verify_application(const char *application_id, ma_int32_t registered_services, ma_int32_t bitness) {
	if(0 == strcmp(application_id, "Application_1")) {
#ifdef MA_WINDOWS
		TEST_ASSERT(bitness == 1);
#else
		TEST_ASSERT(bitness == 0);
#endif
		return (registered_services == (MA_REGISTERED_DATACHANNEL_SERVICE | MA_REGISTERED_EVENT_SERVICE)) == MA_TRUE? MA_OK : MA_ERROR_APIFAILED;
	}
	else if(0 == strcmp(application_id, "Application_2")) {
#ifdef MA_WINDOWS
		TEST_ASSERT(bitness == 1);
#else
		TEST_ASSERT(bitness == 0);
#endif
		return (registered_services == (MA_REGISTERED_POLICY_SERVICE | MA_REGISTERED_PROPERTY_SERVICE)) == MA_TRUE? MA_OK : MA_ERROR_APIFAILED;
	}
	else if(0 == strcmp(application_id, "Application_3")) {
#ifdef MA_WINDOWS
		TEST_ASSERT(bitness == 1);
#else
		TEST_ASSERT(bitness == 0);
#endif
		return (registered_services == (MA_REGISTERED_SCHEDULER_SERVICE | MA_REGISTERED_UPDATER_SERVICE)) == MA_TRUE? MA_OK : MA_ERROR_APIFAILED;
	}
	else if(0 == strcmp(application_id, "Application_4")) {
#ifdef MA_WINDOWS
		TEST_ASSERT(bitness == 1);
#else
		TEST_ASSERT(bitness == 0);
#endif
		return (registered_services == (MA_REGISTERED_DATACHANNEL_SERVICE | MA_REGISTERED_EVENT_SERVICE | MA_REGISTERED_SCHEDULER_SERVICE | MA_REGISTERED_UPDATER_SERVICE | MA_REGISTERED_POLICY_SERVICE | MA_REGISTERED_PROPERTY_SERVICE)) == MA_TRUE? MA_OK : MA_ERROR_APIFAILED;
	}
	else if(0 == strcmp(application_id, "xApplication_1")) {
#ifdef MA_WINDOWS
		TEST_ASSERT(bitness == 2);
#else
		TEST_ASSERT(bitness == 0);
#endif
		return (registered_services == (MA_REGISTERED_DATACHANNEL_SERVICE | MA_REGISTERED_EVENT_SERVICE)) == MA_TRUE? MA_OK : MA_ERROR_APIFAILED;
	}
	else if(0 == strcmp(application_id, "xApplication_2")) {
#ifdef MA_WINDOWS
		TEST_ASSERT(bitness == 2);
#else
		TEST_ASSERT(bitness == 0);
#endif
		return (registered_services == (MA_REGISTERED_POLICY_SERVICE | MA_REGISTERED_PROPERTY_SERVICE)) == MA_TRUE? MA_OK : MA_ERROR_APIFAILED;
	}
	else if(0 == strcmp(application_id, "xApplication_3")) {
#ifdef MA_WINDOWS
		TEST_ASSERT(bitness == 2);
#else
		TEST_ASSERT(bitness == 0);
#endif
		return (registered_services == (MA_REGISTERED_SCHEDULER_SERVICE | MA_REGISTERED_UPDATER_SERVICE)) == MA_TRUE? MA_OK : MA_ERROR_APIFAILED;
	}
	else if(0 == strcmp(application_id, "xApplication_4")) {
#ifdef MA_WINDOWS
		TEST_ASSERT(bitness == 2);
#else
		TEST_ASSERT(bitness == 0);
#endif
		return (registered_services == (MA_REGISTERED_DATACHANNEL_SERVICE | MA_REGISTERED_EVENT_SERVICE | MA_REGISTERED_SCHEDULER_SERVICE | MA_REGISTERED_UPDATER_SERVICE | MA_REGISTERED_POLICY_SERVICE | MA_REGISTERED_PROPERTY_SERVICE)) == MA_TRUE? MA_OK : MA_ERROR_APIFAILED;
	}
	else 
		return MA_ERROR_APIFAILED;
}
ma_error_t verify_registry_store(ma_registry_store_t *reg_store) {
	const ma_application_t *application = NULL;
	int i = 0;
	MA_REGISTRY_STORE_FOR_EACH_APPLICATION(reg_store, application) {
		const char *software_id = ma_application_get_software_id(application);
		ma_int32_t integration_method = ma_application_get_integration_method(application);

		TEST_ASSERT(integration_method == MA_INTEGRATION_METHOD_MESSAGE_BASED);
		
	}
	return MA_OK;
}
#else
ma_error_t verify_registry_store(ma_registry_store_t *reg_store) {
	return MA_OK;
}
#endif

TEST(ma_registry_store_tests, ma_registry_store_create_test) {
	ma_registry_store_t *reg_store = NULL;
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_registry_store_create(NULL));
	TEST_ASSERT(MA_OK == ma_registry_store_create(&reg_store));
	TEST_ASSERT(MA_OK == ma_registry_store_release(reg_store));
}
TEST(ma_registry_store_tests, ma_registry_store_release_test) {
	ma_registry_store_t *reg_store = NULL;
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_registry_store_release(NULL));
	TEST_ASSERT(MA_OK == ma_registry_store_create(&reg_store));
	TEST_ASSERT(MA_OK == ma_registry_store_release(reg_store));

	TEST_ASSERT(MA_OK == ma_registry_store_create(&reg_store));
	TEST_ASSERT(MA_OK == ma_registry_store_scan(reg_store));
	TEST_ASSERT(MA_OK == ma_registry_store_release(reg_store));
}
TEST(ma_registry_store_tests, ma_registry_store_scan_test) {
	ma_registry_store_t *reg_store = NULL;
	
	TEST_ASSERT(MA_OK == ma_registry_store_create(&reg_store));
	TEST_ASSERT(MA_OK == ma_registry_store_scan(reg_store));
	TEST_ASSERT(MA_OK == verify_registry_store(reg_store));
	TEST_ASSERT(MA_OK == ma_registry_store_release(reg_store));
}
TEST(ma_registry_store_tests, ma_registry_store_get_agent_install_path_test) {
	ma_registry_store_t *reg_store = NULL;
	
	TEST_ASSERT(MA_OK == ma_registry_store_create(&reg_store));
	TEST_ASSERT(MA_OK == ma_registry_store_scan(reg_store))
	TEST_ASSERT(0 == strcmp(TEST_AGENT_INSTALL_PATH,ma_registry_store_get_agent_install_path(reg_store)));
	TEST_ASSERT(NULL == ma_registry_store_get_agent_install_path(NULL));
	TEST_ASSERT(MA_OK == ma_registry_store_release(reg_store));


}
TEST(ma_registry_store_tests, ma_registry_store_get_agent_data_path_test) {
	ma_registry_store_t *reg_store = NULL;
	
	TEST_ASSERT(MA_OK == ma_registry_store_create(&reg_store));
	TEST_ASSERT(MA_OK == ma_registry_store_scan(reg_store));
	TEST_ASSERT(0 == strcmp(TEST_AGENT_DATA_PATH,ma_registry_store_get_agent_data_path(reg_store)));
	TEST_ASSERT(NULL == ma_registry_store_get_agent_data_path(NULL));
	TEST_ASSERT(MA_OK == ma_registry_store_release(reg_store));

}
TEST(ma_registry_store_tests, ma_registry_store_get_application_test) {
	ma_registry_store_t *reg_store = NULL;
	const ma_application_t *application = NULL;

	TEST_ASSERT(MA_OK == ma_registry_store_create(&reg_store));
	TEST_ASSERT(MA_OK == ma_registry_store_scan(reg_store));
	TEST_ASSERT(NULL == (application = ma_registry_store_get_application(NULL, "Application_1")));
	TEST_ASSERT(NULL == (application = ma_registry_store_get_application(reg_store, "Application1")));
	TEST_ASSERT(NULL == (application = ma_registry_store_get_application(NULL, "Application_1")));
	
	TEST_ASSERT(NULL != (application = ma_registry_store_get_application(reg_store, "Application_1")));
	//TEST_ASSERT(ma_application_get_registered_services(application) == (MA_REGISTERED_DATACHANNEL_SERVICE | MA_REGISTERED_EVENT_SERVICE));
        //#ifdef MA_WINDOWS
	//TEST_ASSERT(ma_application_get_bitness(application) == 1);
        //#else
	//TEST_ASSERT(ma_application_get_bitness(application) == 0);
        //#endif
	TEST_ASSERT(MA_OK == ma_registry_store_release(reg_store));

}
TEST(ma_registry_store_tests, ma_registry_store_get_next_application_test) {
	ma_registry_store_t *reg_store = NULL;
	const ma_application_t *application = NULL;
	int  i = 0;
	
	TEST_ASSERT(MA_OK == ma_registry_store_create(&reg_store));
	TEST_ASSERT(MA_OK == ma_registry_store_scan(reg_store));
	MA_REGISTRY_STORE_FOR_EACH_APPLICATION(reg_store, application) {
		i++;
	}
	if(isSystem64Bit()){
		TEST_ASSERT(i == 8);
	}
	else {
#ifdef MA_WINDOWS
		TEST_ASSERT(i == 4);
#else
		TEST_ASSERT(i == 9);
#endif
	}
	TEST_ASSERT(MA_OK == ma_registry_store_release(reg_store));
}

