#ifndef CONFIGURATOR_TESTS_H_
#define CONFIGURATOR_TESTS_H_

#include "ma/ma_common.h"

MA_CPP( extern "C" {) 

#define TEST_AGENT_GUID "ekrfhn43hrlhfn34kh4ifn4krn43"

#ifdef MA_WINDOWS
#define TEST_AGENT_ROOT	".\\AGENT\\"
#define TEST_AGENT_DATA_PATH ".\\AGENT\\"
#define TEST_AGENT_SETUP_DATA_PATH ".\\AGENT\\db"
#define TEST_AGENT_INSTALL_PATH ".\\AGENT\\"
#else
#define TEST_AGENT_ROOT	"./AGENT/"
#define TEST_AGENT_DATA_PATH "./AGENT/"
#define TEST_AGENT_SETUP_DATA_PATH "./AGENT/db"
#define TEST_AGENT_INSTALL_PATH "./AGENT/"
#endif

MA_CPP(})

#endif
