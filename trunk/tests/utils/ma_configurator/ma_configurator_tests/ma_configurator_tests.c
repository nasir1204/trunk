/*****************************************************************************
MA configurator Unit Tests.
******************************************************************************/
#include "unity.h"
#include "unity_fixture.h"
#include "unity_internals.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/utils/configurator/ma_configurator_utils.h"
#include "configurator_tests.h"
#include <string.h>

TEST_GROUP_RUNNER(ma_configurator_tests_group)
{
    do {RUN_TEST_CASE(ma_configurator_tests, ma_configurator_create_release_test)} while (0);
}

/**************************************************************************
1) Creating the unit test context.
2) Retrieving underlying message bus instance.
3) Creating and starting message bus service (Property Service) using the
   message bus instance.
**************************************************************************/
TEST_SETUP(ma_configurator_tests)
{
}

/**************************************************************************
1) Stopping and releasing the message bus service (Property service).
2) Releasing the context.
**************************************************************************/
TEST_TEAR_DOWN(ma_configurator_tests)
{
}

/**************************************************************************
MA Configurator create/release tests
**************************************************************************/
TEST(ma_configurator_tests, ma_configurator_create_release_test)
{
    ma_configurator_t *configurator_obj = NULL;

    ma_ds_t *datastore_obj = NULL;
    ma_ds_t *scheduler_datastore_obj = NULL;
    ma_db_t *policy_datastore_obj = NULL;
    ma_datastore_configuration_request_t datastore_required = {MA_TRUE, MA_TRUE, MA_TRUE, MA_TRUE, MA_TRUE};

    ma_buffer_t *buffer = NULL;
    char *string_val = NULL;
    size_t string_size = 0;
    ma_int16_t value_stored_in_db = 0;

    // Dummy agent Configuration
    ma_agent_configuration_t agent_config_obj = { 5, 10, 15, TEST_AGENT_GUID };

    TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == ma_configurator_create(&agent_config_obj, NULL), "ma_configurator_create failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_configurator_create(&agent_config_obj, &configurator_obj), "ma_configurator_create_failed");

	TEST_ASSERT_MESSAGE(MA_OK == ma_configurator_intialize_datastores(configurator_obj, &datastore_required), "ma_configurator_intialize_datastores failed");
    
    TEST_ASSERT_NULL_MESSAGE(ma_configurator_get_agent_install_path(NULL), "ma_configurator_get_agent_install_path API failed");
    TEST_ASSERT_MESSAGE(0 == strcmp(TEST_AGENT_INSTALL_PATH, ma_configurator_get_agent_install_path(configurator_obj)), "ma_configurator_get_agent_install_path API failed");

    TEST_ASSERT_NULL_MESSAGE(ma_configurator_get_agent_data_path(NULL), "ma_configurator_get_agent_data_path API failed");
    TEST_ASSERT_MESSAGE(0 == strcmp(TEST_AGENT_DATA_PATH, ma_configurator_get_agent_data_path(configurator_obj)), "ma_configurator_get_agent_install_path API failed");

    TEST_ASSERT_MESSAGE(0 == strcmp("EPOAGENT3000", ma_configurator_get_agent_software_id(NULL)), "ma_configurator_get_agent_software_id API failed");

    TEST_ASSERT_NULL_MESSAGE(ma_configurator_get_agent_guid(NULL), "ma_configurator_get_agent_guid API failed");
    TEST_ASSERT_EQUAL_MEMORY_MESSAGE(TEST_AGENT_GUID, ma_configurator_get_agent_guid(configurator_obj), strlen(TEST_AGENT_GUID), \
                                     "ma_configurator_get_agent_guid API failed");

    TEST_ASSERT_NULL_MESSAGE(ma_configurator_get_datastore(NULL), "ma_configurator_get_datastore API failed");
    datastore_obj = ma_configurator_get_datastore(configurator_obj);
    TEST_ASSERT_NOT_NULL_MESSAGE(datastore_obj, "ma_configurator_get_datastore API failed");

    
    TEST_ASSERT_NULL_MESSAGE(ma_configurator_get_scheduler_datastore(NULL), "ma_configurator_get_scheduler_datastore API failed");
    scheduler_datastore_obj = ma_configurator_get_scheduler_datastore(configurator_obj);
    
    TEST_ASSERT_NULL_MESSAGE(ma_configurator_get_policies_database(NULL), "ma_configurator_get_policies_database API failed");
    policy_datastore_obj = ma_configurator_get_policies_database(configurator_obj);
    TEST_ASSERT_MESSAGE(policy_datastore_obj, "ma_configurator_get_policies_database API failed");

    
    // Configuring agent policies
    TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == ma_configurator_configure_agent_policies(NULL), "Agent policies could not be configured");
    TEST_ASSERT_MESSAGE(MA_OK == ma_configurator_configure_agent_policies(configurator_obj), "Agent policies could not be configured");

    // Retrieving the agent configuration from DB for verification
    TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_str(datastore_obj, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_INSTALL_PATH_STR, &buffer), "Key not found in DB");
    ma_buffer_get_string(buffer, &string_val, &string_size);
    TEST_ASSERT_MESSAGE(0 == strcmp(TEST_AGENT_INSTALL_PATH, string_val), "Install Path is not configured correctly in DB");
    ma_buffer_release(buffer);

    TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_str(datastore_obj,MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_DATA_PATH_STR, &buffer), "Key not found in DB");
    ma_buffer_get_string(buffer, &string_val, &string_size);
    TEST_ASSERT_MESSAGE(0 == strcmp(TEST_AGENT_DATA_PATH, string_val), "Data Path is not configured correctly in DB");
    ma_buffer_release(buffer);

    TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_int16(datastore_obj, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_MODE_INT, &value_stored_in_db), "Key not found in DB");
    TEST_ASSERT_MESSAGE(5 == value_stored_in_db, "Agent Mode is not configured correctly in DB");

    TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_str(datastore_obj, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_SOFTWARE_ID_STR , &buffer), "Key not found in DB");
    ma_buffer_get_string(buffer, &string_val, &string_size);
    TEST_ASSERT_MESSAGE(0 == strcmp("EPOAGENT3000", string_val), "Agent Software ID is not configured correctly in DB");
    ma_buffer_release(buffer);

    TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_str(datastore_obj, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_GUID_STR, &buffer), "Key not found in DB");
    ma_buffer_get_string(buffer, &string_val, &string_size);
    TEST_ASSERT_MESSAGE(0 == strcmp(TEST_AGENT_GUID, string_val), "Agent GUID is not configured correctly in DB");
    ma_buffer_release(buffer);

    TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_int16(datastore_obj, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_CRYPTO_MODE_INT, &value_stored_in_db), "Key not found in DB");
    TEST_ASSERT_MESSAGE(10 == value_stored_in_db, "Crypto Mode is not configured correctly in DB");

    TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_int16(datastore_obj, MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_CRYPTO_ROLE_INT, &value_stored_in_db), "Key not found in DB");
    TEST_ASSERT_MESSAGE(15 == value_stored_in_db, "Crypto Mode is not configured correctly in DB");


    // Releasing configurator
    TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == ma_configurator_release(NULL), "ma_configurator_release failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_configurator_release(configurator_obj), "ma_configurator_release failed");
}










