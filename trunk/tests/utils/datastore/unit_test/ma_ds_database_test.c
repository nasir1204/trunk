/*****************************************************************************
Datastore unit tests
******************************************************************************/
#include "unity.h"
#include "unity_fixture.h"
#include "ma/datastore/ma_ds_database.h"
#include "ma/ma_common.h"
#include <string.h>

#ifndef MA_WINDOWS
    #include <unistd.h>
    #include <wchar.h>
#endif

#define LOCAL_DATABASE_NAME_FOR_UT    "l_datastoreut.db"
#define GLOBAL_DATABASE_NAME_FOR_UT   "g_datastoreut.db"
#define POLICIES_INSTANCE_NAME        "Policies"
#define TEMP_INSTANCE_NAME            "TempData"

static ma_db_t *g_database_handle = NULL;
static ma_ds_t *g_ds = NULL;
static ma_ds_database_t *ds_database = NULL;

// Large arrays declared globally. Local declaration may corrupt stack.
unsigned char msg[1024 * 1024] = {0};
unsigned char msg1[1024 * 1024] = {0};

TEST_GROUP_RUNNER(ma_ds_database_tests_group)
{
    do {RUN_TEST_CASE(ma_ds_database_tests, verify_ds_database_create_release)} while (0);
    do {RUN_TEST_CASE(ma_ds_database_tests, verify_ds_set_get_string_apis)} while (0);
    do {RUN_TEST_CASE(ma_ds_database_tests, verify_ds_set_get_blob_apis)} while (0);
    do {RUN_TEST_CASE(ma_ds_database_tests, verify_ds_set_get_int_apis)} while (0);
    do {RUN_TEST_CASE(ma_ds_database_tests, verify_ds_set_get_variant_apis)} while (0);
    do {RUN_TEST_CASE(ma_ds_database_tests, verify_ds_remove_apis)} while (0);
    do {RUN_TEST_CASE(ma_ds_database_tests, verify_ds_iterator_tests)} while (0);
    do {RUN_TEST_CASE(ma_ds_database_tests, verify_ds_transaction_test)} while (0);
}



TEST_SETUP(ma_ds_database_tests) {

    Unity.TestFile = "ma_ds_database_test.c";
    /* Remove if any from previous run */
    unlink(GLOBAL_DATABASE_NAME_FOR_UT);
    TEST_ASSERT_MESSAGE(MA_OK ==  ma_db_open(GLOBAL_DATABASE_NAME_FOR_UT, MA_DB_OPEN_READWRITE, &g_database_handle), "ma_db_open failed to open global database database");

    TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == ma_ds_database_create(NULL , "Policies" , &ds_database), "ma_ds_database_create failed to create the instance");
    TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == ma_ds_database_create(g_database_handle , NULL , &ds_database), "ma_ds_database_create failed to create the instance");
    TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == ma_ds_database_create(g_database_handle , "Policies" , NULL), "ma_ds_database_create failed to create the instance");
    TEST_ASSERT_MESSAGE(MA_OK ==  ma_ds_database_create(g_database_handle , "Policies" , &ds_database), "ma_ds_database_create failed to create the instance");
    g_ds = (ma_ds_t *)ds_database;
}

TEST_TEAR_DOWN(ma_ds_database_tests) {

    /* Remove instance */
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ds_database_delete_instance(NULL));
    TEST_ASSERT(MA_OK == ma_ds_database_delete_instance(ds_database));

    /*Remove the files which this group created */
    TEST_ASSERT_MESSAGE(MA_OK ==  ma_db_close(g_database_handle), "ma_db_close failed to close the global datatbase");
    TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG ==  ma_ds_database_release(NULL), "ma_ds_database failed to release the instance");

    TEST_ASSERT_MESSAGE(MA_OK ==  ma_ds_database_release((ma_ds_database_t*)g_ds), "ma_ds_database failed to release the instance");

    unlink(GLOBAL_DATABASE_NAME_FOR_UT);
}

TEST(ma_ds_database_tests, verify_ds_database_create_release) {
    ma_ds_database_t *ds_policies = NULL; 
    ma_ds_database_t *ds_temp = NULL;

    /*ma_ds_database_create - negative */
    /* Negative  , first argument NULL */
    TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == ma_ds_database_create(NULL , POLICIES_INSTANCE_NAME , &ds_policies) , "ma_ds_database open should return invalid argument on first param being NULL");

    /* Negative  , second argument NULL */
    TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == ma_ds_database_create(g_database_handle , NULL , &ds_policies) , "ma_ds_database open should return invalid argument on second param being NULL");

    /* Negative  , second argument empty string */
    TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == ma_ds_database_create(g_database_handle , "" , &ds_policies) , "ma_ds_database open should return invalid argument on instance name being empty");

    /* Negative  , third argument NULL */
    TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == ma_ds_database_create(g_database_handle , POLICIES_INSTANCE_NAME , NULL) , "ma_ds_database open should return invalid argument on out param being null");

    /*ma_ds_database_release - negative */
    TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == ma_ds_database_release(NULL) , "ma_ds_database_release should return invalid argument on param being null");

    /* positive  , creating a policy instance*/
    TEST_ASSERT_MESSAGE(MA_OK == ma_ds_database_create(g_database_handle , POLICIES_INSTANCE_NAME , &ds_policies) , "ma_ds_database_create failed to create the policies instance on global db");

    TEST_ASSERT_MESSAGE(MA_OK == ma_ds_database_release(ds_policies) , "ma_ds_database_release failed to release the policies instance on global db");

    /* positive  , creating a policy instance again , it should pass as it already exists*/
    TEST_ASSERT_MESSAGE(MA_OK == ma_ds_database_create(g_database_handle , POLICIES_INSTANCE_NAME , &ds_policies) , "ma_ds_database_create failed to create the policies instance second time on global db");

    TEST_ASSERT_MESSAGE(MA_OK == ma_ds_database_release(ds_policies) , "ma_ds_database_release failed to release the policies instance on global db");

    /* positive creating a temp instance */
    TEST_ASSERT_MESSAGE(MA_OK == ma_ds_database_create(g_database_handle , TEMP_INSTANCE_NAME , &ds_temp) , "ma_ds_database_create failed to create the temp instance on global db");

    TEST_ASSERT_MESSAGE(MA_OK == ma_ds_database_release(ds_temp) , "ma_ds_database_release failed to release the temp instance on global db");
}


TEST(ma_ds_database_tests, verify_ds_set_get_string_apis) {
    ma_buffer_t *value = NULL ;
    ma_wbuffer_t *w_value = NULL ;
    const char *ptr = NULL;
    ma_wchar_t *ptr_w = NULL;
    size_t size = 0 ;


    /* datastore  is NULL */
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_str(NULL, "AntiVirus Policy\\PATH_NOT_EXISTS", "Is Scan Enabled" , &value));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_set_str(NULL, "AntiVirus Policy\\PATH_NOT_EXISTS", "Is Scan Enabled" , "1" , -1));

    /* Path is NULL */
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_str(g_ds,NULL, "Is Scan Enabled" , &value));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_set_str(g_ds, NULL, "Is Scan Enabled" , "1" , -1));

    /* Key is NULL */
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_str(g_ds,"AntiVirus Policy\\General", NULL , &value));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_set_str(g_ds, "AntiVirus Policy\\PATH_NOT_EXISTS", NULL , "1" , -1));

    /* out param is NULL */
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_str(g_ds,"AntiVirus Policy\\General", "Is Scan Enabled" ,NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_set_str(g_ds,"AntiVirus Policy\\General", "Is Scan Enabled" ,NULL , -1));

    /* When the path don't exists*/
    TEST_ASSERT_EQUAL_INT(MA_ERROR_DS_PATH_NOT_FOUND, ma_ds_get_str(g_ds, "AntiVirus Policy\\PATH_NOT_EXISTS", "Is Scan Enabled" , &value));
    TEST_ASSERT_NULL(value);

    /* When the path exists but key doesn't exists*/
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_str(g_ds, "AntiVirus Policy\\PATH_EXISTS", "Key_1" , "value",-1));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_DS_KEY_NOT_FOUND, ma_ds_get_str(g_ds, "AntiVirus Policy\\PATH_EXISTS", "Key_2" , &value));
    TEST_ASSERT_NULL(value);

    /* set and get it using length passed */
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_str(g_ds, "AntiVirus Policy\\PATH_EXISTS", "Key_1" , "value",3));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_get_str(g_ds, "AntiVirus Policy\\PATH_EXISTS", "Key_1" , &value));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_buffer_get_string(value,&ptr, &size));
    TEST_ASSERT_EQUAL_INT(0,strcmp(ptr,"val"));
    TEST_ASSERT_EQUAL_INT(3,size);
    ma_buffer_release(value);

    value = NULL ;

	/*Setting and getting empty strings*/
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_str(g_ds, "AntiVirus Policy\\PATH_EXISTS", "EmptyStringKey","",0));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_get_str(g_ds, "AntiVirus Policy\\PATH_EXISTS", "EmptyStringKey",&value)); 
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_buffer_get_string(value,&ptr, &size));
    TEST_ASSERT_EQUAL_INT(0,strcmp(ptr,""));
    TEST_ASSERT_EQUAL_INT(0,size);
    ma_buffer_release(value);

    value = NULL ;
    /* Set and get utf-8*/
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_str(g_ds, "AntiVirus Policy\\General", "Is Scan Enabled" , "I am enabled" , -1));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_get_str(g_ds, "AntiVirus Policy\\General", "Is Scan Enabled" , &value));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_buffer_get_string(value,&ptr, &size));
    TEST_ASSERT_EQUAL_INT(0,strcmp(ptr,"I am enabled"));
    ma_buffer_release(value);
    value = NULL ;

     /* Set and get wide char*/
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_set_wstr(NULL, "AntiVirus Policy\\General", "Is Scan Enabled" , L"I am enabled" , -1));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_set_wstr(g_ds, NULL, "Is Scan Enabled" , L"I am enabled" , -1));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_set_wstr(g_ds, "AntiVirus Policy\\General", NULL, L"I am enabled" , -1));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_set_wstr(g_ds, "AntiVirus Policy\\General", "Is Scan Enabled" , NULL , -1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_wstr(g_ds, "AntiVirus Policy\\General", "Is Scan Enabled" , L"I am enabled" , -1));

    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_wstr(NULL, "AntiVirus Policy\\General", "Is Scan Enabled" , &w_value));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_wstr(g_ds, NULL, "Is Scan Enabled" , &w_value));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_wstr(g_ds, "AntiVirus Policy\\General", NULL , &w_value));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_wstr(g_ds, "AntiVirus Policy\\General", "Is Scan Enabled" , NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_get_wstr(g_ds, "AntiVirus Policy\\General", "Is Scan Enabled" , &w_value));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_wbuffer_get_string(w_value,&ptr_w, &size));
    TEST_ASSERT_EQUAL_INT(0,wcscmp(ptr_w, L"I am enabled"));
    ma_wbuffer_release(w_value);
    value = NULL ;
}

TEST(ma_ds_database_tests, verify_ds_set_get_blob_apis)
{
    ma_buffer_t *value = NULL ;
    unsigned char *ptr =  NULL;
    char buff[128] = {0};
    size_t size = 0 ;

    /* datastore  is NULL */
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_blob(NULL, "AntiVirus Policy\\PATH_NOT_EXISTS", "Is Scan Enabled" , &value));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_set_blob(NULL, "AntiVirus Policy\\PATH_NOT_EXISTS", "Is Scan Enabled" , "1", 1));

    /* Path is NULL */
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_blob(g_ds,NULL, "Is Scan Enabled" , &value));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_set_blob(g_ds, NULL, "Is Scan Enabled" , "1" , 1));

    /* Key is NULL */
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_blob(g_ds,"AntiVirus Policy\\General", NULL , &value));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_set_blob(g_ds, "AntiVirus Policy\\PATH_NOT_EXISTS", NULL , "1" , -1));

    /* out param is NULL */
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_blob(g_ds,"AntiVirus Policy\\General", "Is Scan Enabled" ,NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_set_blob(g_ds,"AntiVirus Policy\\General", "Is Scan Enabled" ,NULL , 1));

    /* When the path don't exists*/
    TEST_ASSERT_EQUAL_INT(MA_ERROR_DS_PATH_NOT_FOUND, ma_ds_get_str(g_ds, "AntiVirus Policy\\PATH_NOT_EXISTS", "Is Scan Enabled" , &value));
    TEST_ASSERT_NULL(value);

    /* When the path exists but key doesn't exists*/
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_blob(g_ds, "AntiVirus Policy\\PATH_EXISTS", "Key_1" , "value",5));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_DS_KEY_NOT_FOUND, ma_ds_get_blob(g_ds, "AntiVirus Policy\\PATH_EXISTS", "Key_2" , &value));
    TEST_ASSERT_NULL(value);

    /* Set and get */
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_blob(g_ds, "AntiVirus Policy\\General", "Is Scan Enabled" , "LOL" , 3));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_get_blob(g_ds, "AntiVirus Policy\\General", "Is Scan Enabled" , &value));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_buffer_get_raw(value,&ptr, &size));
    TEST_ASSERT_EQUAL_INT(3,size);
    memcpy(buff,ptr,size);
    TEST_ASSERT_EQUAL_INT(0,strcmp(buff,"LOL"));
    ma_buffer_release(value);

    /*
    Set and get for large data. Note that msg[], msg1[] are large arrays therefore,
    they are declared globally, not on stack.
    */
    {
        ma_uint64_t counter = 0;

        for(counter = 0 ; counter < ((1024 * 1024) - 1); counter++)
        {
            msg[counter] = 'a';
        };

        TEST_ASSERT(MA_OK == ma_ds_set_blob(g_ds, "AntiVirus Policy\\General", "Large_Data" , msg, (1024 * 1024)));
        TEST_ASSERT(MA_OK == ma_ds_get_blob(g_ds, "AntiVirus Policy\\General", "Large_Data", &value));
        TEST_ASSERT(MA_OK == ma_buffer_get_raw(value, &ptr, &size));
        TEST_ASSERT(1024 * 1024 == size);
        memcpy(msg1, ptr, size);
        TEST_ASSERT(0 == memcmp(msg1, msg, 1024 * 1024));
        ma_buffer_release(value);
    }
}


TEST(ma_ds_database_tests, verify_ds_set_get_int_apis) {
    ma_int16_t value_16 = 0 ;
    ma_int32_t value_32 = 0 ;
    ma_int64_t value_64 = 0 ;


    /* datastore  is NULL */
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_int16(NULL, "AntiVirus Policy\\PATH_NOT_EXISTS", "Is Scan Enabled" , &value_16));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_set_int(NULL, "AntiVirus Policy\\PATH_NOT_EXISTS", "Is Scan Enabled" , value_16));

    /* Path is NULL */
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_int16(g_ds,NULL, "Is Scan Enabled" , &value_16));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_set_int(g_ds, NULL ,"Is Scan Enabled" , value_16));

    /* Key is NULL */
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_int16(g_ds,"AntiVirus Policy\\General", NULL , &value_16));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_set_int(g_ds, "AntiVirus Policy\\General" ,NULL , value_16));

    /* out param is NULL */
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_int16(g_ds,"AntiVirus Policy\\General", "Is Scan Enabled" ,NULL));

    /* When the path don't exists*/
    TEST_ASSERT_EQUAL_INT(MA_ERROR_DS_PATH_NOT_FOUND, ma_ds_get_int16(g_ds, "AntiVirus Policy\\PATH_NOT_EXISTS", "Is Scan Enabled" , &value_16));

    /* When the path exists but key doesn't exists*/
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_int(g_ds, "AntiVirus Policy\\PATH_EXISTS", "Key_1" , 123456));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_DS_KEY_NOT_FOUND, ma_ds_get_int16(g_ds, "AntiVirus Policy\\PATH_EXISTS", "Key_2" , &value_16));

    /* Set and get */
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_int(g_ds, "AntiVirus Policy\\General", "Is Scan Enabled" , 12345));

    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_int16(NULL, "AntiVirus Policy\\General", "Is Scan Enabled" , &value_16));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_int16(g_ds,  NULL, "Is Scan Enabled" , &value_16));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_int16(g_ds, "AntiVirus Policy\\General", NULL , &value_16));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_int16(g_ds, "AntiVirus Policy\\General", "Is Scan Enabled" , NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_get_int16(g_ds, "AntiVirus Policy\\General", "Is Scan Enabled" , &value_16));
    TEST_ASSERT_EQUAL_INT16(12345,value_16);


    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_int32(NULL, "AntiVirus Policy\\General", "Is Scan Enabled" , &value_32));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_int32(g_ds, NULL, "Is Scan Enabled" , &value_32));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_int32(g_ds, "AntiVirus Policy\\General", NULL , &value_32));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_int32(g_ds, "AntiVirus Policy\\General", "Is Scan Enabled" , NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_get_int32(g_ds, "AntiVirus Policy\\General", "Is Scan Enabled" , &value_32));
    TEST_ASSERT_EQUAL_INT32(12345,value_32);

    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_int64(NULL, "AntiVirus Policy\\General", "Is Scan Enabled" , &value_64));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_int64(g_ds, NULL, "Is Scan Enabled" , &value_64));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_int64(g_ds, "AntiVirus Policy\\General", NULL , &value_64));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_int64(g_ds, "AntiVirus Policy\\General", "Is Scan Enabled" , NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_get_int64(g_ds, "AntiVirus Policy\\General", "Is Scan Enabled" , &value_64));
    TEST_ASSERT_EQUAL_INT64(12345,value_64);
}

TEST(ma_ds_database_tests, verify_ds_set_get_variant_apis) {

    ma_variant_t *variant_ptr = NULL;
    ma_buffer_t *buffer_ptr = NULL;
    const char *string_val = NULL;
    size_t string_size = 0;
    ma_bool_t is_exist = MA_TRUE;

    TEST_ASSERT(MA_OK == ma_variant_create_from_string("my password - 3457y34o5y430", sizeof("my password - 3457y34o5y430"), &variant_ptr));

    // Test for "path exist" APIs (path is not created yet)
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ds_is_path_exists(NULL, "AntiVirus Policy\\Passwords", &is_exist));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ds_is_path_exists(g_ds, NULL, &is_exist));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ds_is_path_exists(g_ds, "AntiVirus Policy\\Passwords", NULL));
    TEST_ASSERT(MA_OK == ma_ds_is_path_exists(g_ds, "AntiVirus Policy\\Passwords", &is_exist));
    TEST_ASSERT(MA_FALSE == is_exist);

    is_exist = MA_TRUE;
    // Test for "key exist" APIs (key under test is not created yet)
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ds_is_key_exists(NULL, "AntiVirus Policy\\Passwords", "password", &is_exist));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ds_is_key_exists(g_ds, NULL, "password", &is_exist));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ds_is_key_exists(g_ds, "AntiVirus Policy\\Passwords", NULL, &is_exist));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ds_is_key_exists(g_ds, "AntiVirus Policy\\Passwords", "password", NULL));
    TEST_ASSERT(MA_OK == ma_ds_is_key_exists(g_ds, "AntiVirus Policy\\Passwords", "password", &is_exist));
    TEST_ASSERT(MA_FALSE == is_exist);

    // Create a variant type element and add it to DB
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ds_set_variant(NULL, "AntiVirus Policy\\Passwords", "password", variant_ptr));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ds_set_variant(g_ds, NULL, "password", variant_ptr));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ds_set_variant(g_ds, "AntiVirus Policy\\Passwords", NULL, variant_ptr));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ds_set_variant(g_ds, "AntiVirus Policy\\Passwords", "password", NULL));

    TEST_ASSERT(MA_OK == ma_ds_set_variant(g_ds, "AntiVirus Policy\\Passwords", "password", variant_ptr));
    ma_variant_release(variant_ptr);

    // Path and Key should exist now
    TEST_ASSERT(MA_OK == ma_ds_is_path_exists(g_ds, "AntiVirus Policy\\Passwords", &is_exist));
    TEST_ASSERT(MA_TRUE == is_exist);

    is_exist = MA_FALSE;
    TEST_ASSERT(MA_OK == ma_ds_is_key_exists(g_ds, "AntiVirus Policy\\Passwords", "password", &is_exist));
    TEST_ASSERT(MA_TRUE == is_exist);

    // Retrieving variant from DB
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ds_get_variant(NULL, "AntiVirus Policy\\Passwords", "password", MA_VARTYPE_STRING, &variant_ptr));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ds_get_variant(g_ds, NULL, "password", MA_VARTYPE_STRING, &variant_ptr));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ds_get_variant(g_ds, "AntiVirus Policy\\Passwords", NULL, MA_VARTYPE_STRING, &variant_ptr));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ds_get_variant(g_ds, "AntiVirus Policy\\Passwords", "password", MA_VARTYPE_STRING, NULL));
    TEST_ASSERT(MA_OK == ma_ds_get_variant(g_ds, "AntiVirus Policy\\Passwords", "password", MA_VARTYPE_STRING, &variant_ptr));
    TEST_ASSERT(MA_OK == ma_variant_get_string_buffer(variant_ptr, &buffer_ptr));
    TEST_ASSERT(MA_OK == ma_buffer_get_string(buffer_ptr, &string_val, &string_size));
    TEST_ASSERT(0 == strcmp("my password - 3457y34o5y430", string_val));
    TEST_ASSERT(strlen("my password - 3457y34o5y430") == string_size);

    ma_buffer_release(buffer_ptr);
    ma_variant_release(variant_ptr);
}


TEST(ma_ds_database_tests, verify_ds_remove_apis) {
     ma_buffer_t *value = NULL ;

    /* datastore  is NULL */
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_rem(NULL, "AntiVirus Policy\\PATH_NOT_EXISTS", "Is Scan Enabled" , 0));

    /* Path is NULL */
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_rem(g_ds,NULL, "Is Scan Enabled" , 0));

    /* Key is NULL */
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_rem(g_ds,"AntiVirus Policy\\General", NULL , 0));

    /* When the path don't exists*/
    TEST_ASSERT_EQUAL_INT(MA_ERROR_DS_PATH_NOT_FOUND, ma_ds_rem(g_ds, "AntiVirus Policy\\PATH_NOT_EXISTS", "Is Scan Enabled" , 0));


    /* Single remove */
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_blob(g_ds, "AntiVirus Policy\\PATH_EXISTS", "A" , "AA",2));

    //Remove
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_rem(g_ds, "AntiVirus Policy\\PATH_EXISTS", "A" , 0));

    //Try to get
    TEST_ASSERT_EQUAL_INT(MA_ERROR_DS_KEY_NOT_FOUND , ma_ds_get_blob(g_ds, "AntiVirus Policy\\PATH_EXISTS", "A" , &value));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_DS_KEY_NOT_FOUND , ma_ds_get_blob(g_ds, "AntiVirus Policy\\PATH_EXISTS", "B" , &value));


    /*Multi remove */
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_blob(g_ds, "AntiVirus Policy\\PATH_EXISTS", "A" , "AA",2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_blob(g_ds, "AntiVirus Policy\\PATH_EXISTS", "B" , "BB",2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_blob(g_ds, "AntiVirus Policy\\PATH_EXISTS", "C" , "CC",2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_blob(g_ds, "AntiVirus Policy", "D" , "DD",2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_blob(g_ds, "AntiVirus Policy2\\PATH_EXISTS", "E" , "EE",2));

    //Verifying recursive remove. All children below node - "AntiVirus Policy" should be removed
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_rem(g_ds, "AntiVirus Policy", NULL , 1));

    //Try to get
    TEST_ASSERT_EQUAL_INT(MA_ERROR_DS_PATH_NOT_FOUND, ma_ds_get_blob(g_ds, "AntiVirus Policy\\PATH_EXISTS", "A" , &value));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_DS_PATH_NOT_FOUND, ma_ds_get_blob(g_ds, "AntiVirus Policy", "D" , &value));

    // "AntiVirus Policy2" should not be deleted
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_get_blob(g_ds, "AntiVirus Policy2\\PATH_EXISTS", "E" , &value));

    ma_buffer_release(value);
}


TEST(ma_ds_database_tests, verify_ds_transaction_test) {
    ma_int16_t value_16 = 0 ;
    ma_ds_database_t *ds = (ma_ds_database_t *)g_ds;

    /* datastore  is NULL */
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_database_transaction_begin(NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_database_transaction_end(NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_database_transaction_cancel(NULL));

    /*Positive test case - transaction begin ,
      values should not reflect will in actual database - manual verification but it will be in transaction */
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_database_transaction_begin(ds));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_int(g_ds, "AntiVirus Policy\\General", "A" , 123));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_int(g_ds, "AntiVirus Policy\\General", "B" , 1234));

    //now try to get , here it will get the values but it will not reflect in the database-manually you can check
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_get_int16(g_ds, "AntiVirus Policy\\General", "A" , &value_16));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_get_int16(g_ds, "AntiVirus Policy\\General", "B" , &value_16));

    //commit and get
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_database_transaction_end(ds));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_get_int16(g_ds, "AntiVirus Policy\\General", "A" , &value_16));
    TEST_ASSERT_EQUAL_INT(123,value_16);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_get_int16(g_ds, "AntiVirus Policy\\General", "B" , &value_16));
    TEST_ASSERT_EQUAL_INT(1234,value_16);

    /*Positive test case - transaction begin , values should not reflect and cancel it , it should not reflect till the time it is ended */
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_database_transaction_begin(ds));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_int(g_ds, "AntiVirus Policy\\General", "C" , 123));

    //cancel it and try to get it
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_database_transaction_cancel(ds));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_DS_KEY_NOT_FOUND, ma_ds_get_int16(g_ds, "AntiVirus Policy\\General", "C" , &value_16));

}

TEST(ma_ds_database_tests, verify_ds_iterator_tests) {
    ma_ds_iterator_t *it = NULL;
    ma_buffer_t *buffer = NULL;
    char *sub_paths[]= {"SiteList_1","SiteList_2", "SiteList_3", "SiteList_4"};
    char *sub_keys[]= {"GlobalVersion", "SiteListVersion", "Source"};
    size_t count_sub_paths=4 , count_sub_keys = 3 , i = 0;


     /* Set up the database*/
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_str(g_ds, "MA\\AH\\", "SiteListVersion", "2000112" , -1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_str(g_ds, "MA\\AH\\", "GlobalVersion", "2222222", -1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_str(g_ds, "MA\\AH\\", "Source", "2222222", -1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_str(g_ds, "MA\\AH\\SiteList_1", "Name", "Site_1", -1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_str(g_ds, "MA\\AH\\SiteList_2", "Name", "Site_2", -1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_str(g_ds, "MA\\AH\\SiteList_3", "Name", "Site_3", -1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_str(g_ds, "MA\\AH\\SiteList_1\\OneMoreSubPath", "Name", "Site_1", -1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_str(g_ds, "MA\\AH\\SiteList_2\\OneMoreSubPath", "Name", "Site_2", -1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_str(g_ds, "MA\\AH\\SiteList_4\\OneMoreSubPath", "Name", "Site_2", -1));

    /*Path test */
    TEST_ASSERT_EQUAL_INT(MA_ERROR_PRECONDITION, ma_ds_iterator_create(NULL, 1, "MA\\AH", &it));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_PRECONDITION, ma_ds_iterator_create(g_ds, 1, NULL, &it));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_PRECONDITION, ma_ds_iterator_create(g_ds, 1, "MA\\AH", NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_iterator_create(g_ds, 1, "MA\\AH", &it));
    for(i = 0 ; i < count_sub_paths;i++) {
        const char *value  = NULL;
        size_t size = 0;
        TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_iterator_get_next(it, &buffer));
        TEST_ASSERT_EQUAL_INT(MA_OK, ma_buffer_get_string(buffer,&value,&size));
        TEST_ASSERT_EQUAL_INT(0, strcmp(sub_paths[i],value));
        ma_buffer_release(buffer),buffer = NULL;
    }

    TEST_ASSERT_EQUAL_INT(MA_ERROR_PRECONDITION, ma_ds_iterator_get_next(NULL,&buffer));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_PRECONDITION, ma_ds_iterator_get_next(it,NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_NO_MORE_ITEMS, ma_ds_iterator_get_next(it,&buffer));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_ds_iterator_release(it));

    /*Keys test */
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_iterator_create(g_ds, 0, "MA\\AH", &it));
    for(i = 0 ; i < count_sub_keys;i++) {
        const char *value  = NULL;
        size_t size = 0;
        TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_iterator_get_next(it, &buffer));
        TEST_ASSERT_EQUAL_INT(MA_OK, ma_buffer_get_string(buffer,&value,&size));
        TEST_ASSERT_EQUAL_INT(0, strcmp(sub_keys[i],value));
        ma_buffer_release(buffer),buffer = NULL;
    }
    TEST_ASSERT_EQUAL_INT(MA_ERROR_NO_MORE_ITEMS, ma_ds_iterator_get_next(it,&buffer));

    TEST_ASSERT_EQUAL_INT(MA_ERROR_PRECONDITION,ma_ds_iterator_release(NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_ds_iterator_release(it));
}

