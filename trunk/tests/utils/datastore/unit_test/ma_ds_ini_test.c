/*****************************************************************************
INI unit tests
******************************************************************************/

#include "unity.h"
#include "unity_fixture.h"

#include "ma/ma_datastore.h"
#include "ma/datastore/ma_ds_ini.h"

#include <string.h>

#ifndef MA_WINDOWS
#include <unistd.h>
#include <wchar.h>
#endif

#ifdef MA_WINDOWS
    #define ut_ini_file_path ".\\my_policies\\policies.ini"
#else
    #define ut_ini_file_path "./my_policies/policies.ini"
#endif

static ma_ds_t *g_ds = NULL ;

TEST_GROUP_RUNNER(ma_ds_ini_tests_group)
{
    do {RUN_TEST_CASE(ma_ds_ini_tests_group, verify_ds_set_get_string_apis)} while (0);
    do {RUN_TEST_CASE(ma_ds_ini_tests_group, verify_ds_set_get_blob_apis)} while (0);
    do {RUN_TEST_CASE(ma_ds_ini_tests_group, verify_ds_set_get_int_apis)} while (0);
    do {RUN_TEST_CASE(ma_ds_ini_tests_group, verify_ds_remove_apis)} while (0);
}

TEST_SETUP(ma_ds_ini_tests_group) {
    ma_ds_ini_t *ds_ini = NULL ;
    Unity.TestFile = "ma_ds_ini_test.c";

    /*Remove the files from prevous run ifany */
    unlink(ut_ini_file_path);
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ds_ini_open(NULL, 0, &ds_ini));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ds_ini_open(ut_ini_file_path, 0, NULL));
    TEST_ASSERT(MA_OK == ma_ds_ini_open(ut_ini_file_path, 0, &ds_ini));
    g_ds = (ma_ds_t*)ds_ini;
}

TEST_TEAR_DOWN(ma_ds_ini_tests_group) {
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ds_ini_release(NULL));
    TEST_ASSERT(MA_OK == ma_ds_ini_release((ma_ds_ini_t*)g_ds));

    /*Remove the files which this group created */
    unlink(ut_ini_file_path);
}



TEST(ma_ds_ini_tests_group, verify_ds_set_get_string_apis) {
    ma_buffer_t *value = NULL ;
    ma_wbuffer_t *w_value = NULL;
    const char *ptr = NULL;
    ma_wchar_t *ptr_w = NULL;
    size_t size = 0 ;

    /* datastore  is NULL */
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_str(NULL, "AntiVirus Policy\\PATH_NOT_EXISTS", "Is Scan Enabled" , &value));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_set_str(NULL, "AntiVirus Policy\\PATH_NOT_EXISTS", "Is Scan Enabled" , "1" , -1));

    /* Path is NULL */
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_str(g_ds,NULL, "Is Scan Enabled" , &value));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_set_str(g_ds, NULL, "Is Scan Enabled" , "1" , -1));

    /* Key is NULL */
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_str(g_ds,"AntiVirus Policy\\General", NULL , &value));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_set_str(g_ds, "AntiVirus Policy\\PATH_NOT_EXISTS", NULL , "1" , -1));

    /* out param is NULL */
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_str(g_ds,"AntiVirus Policy\\General", "Is Scan Enabled" ,NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_set_str(g_ds,"AntiVirus Policy\\General", "Is Scan Enabled" ,NULL , -1));

    /* When the path dont exists*/
    TEST_ASSERT_EQUAL_INT(MA_ERROR_DS_PATH_NOT_FOUND, ma_ds_get_str(g_ds, "AntiVirus Policy\\PATH_NOT_EXISTS", "Is Scan Enabled" , &value));
    TEST_ASSERT_NULL(value);

    /* When the path exists but key doesn exists*/
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_str(g_ds, "AntiVirus Policy\\PATH_EXISTS", "Key_1" , "value",-1));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_DS_KEY_NOT_FOUND, ma_ds_get_str(g_ds, "AntiVirus Policy\\PATH_EXISTS", "Key_2" , &value));
    TEST_ASSERT_NULL(value);

    /* set and get it using length passed */
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_str(g_ds, "AntiVirus Policy\\PATH_EXISTS", "Key_1" , "value",3));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_get_str(g_ds, "AntiVirus Policy\\PATH_EXISTS", "Key_1" , &value));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_buffer_get_string(value, &ptr, &size));
    TEST_ASSERT_EQUAL_INT(0,strcmp(ptr,"val"));
    TEST_ASSERT_EQUAL_INT(3,size);
    ma_buffer_release(value);

    value = NULL ;
    /* Set- get utf8 */
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_str(g_ds, "AntiVirus Policy\\General", "Is Scan Enabled" , "I am enabled" , -1));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_get_str(g_ds, "AntiVirus Policy\\General", "Is Scan Enabled" , &value));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_buffer_get_string(value, &ptr, &size));
    TEST_ASSERT_EQUAL_INT(0,strcmp(ptr,"I am enabled"));
    ma_buffer_release(value);
    value = NULL ;

     /* Set and get wide char*/
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_wstr(g_ds, "AntiVirus Policy\\General", "Is Scan Enabled" , L"I am enabled" , -1));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_get_wstr(g_ds, "AntiVirus Policy\\General", "Is Scan Enabled" , &w_value));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_wbuffer_get_string(w_value, &ptr_w, &size));
    TEST_ASSERT_EQUAL_INT(0,wcscmp(ptr_w, L"I am enabled"));
    ma_wbuffer_release(w_value);
    value = NULL ;
}


TEST(ma_ds_ini_tests_group, verify_ds_set_get_blob_apis) {
    ma_buffer_t *value = NULL ;
    const unsigned char *ptr =  NULL;
    char buff[128] = {0};
    size_t size = 0 ;

    /* datastore  is NULL */
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_blob(NULL, "AntiVirus Policy\\PATH_NOT_EXISTS", "Is Scan Enabled" , &value));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_set_blob(NULL, "AntiVirus Policy\\PATH_NOT_EXISTS", "Is Scan Enabled" , "1", 1));

    /* Path is NULL */
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_blob(g_ds,NULL, "Is Scan Enabled" , &value));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_set_blob(g_ds, NULL, "Is Scan Enabled" , "1" , 1));

    /* Key is NULL */
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_blob(g_ds,"AntiVirus Policy\\General", NULL , &value));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_set_blob(g_ds, "AntiVirus Policy\\PATH_NOT_EXISTS", NULL , "1" , -1));

    /* out param is NULL */
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_blob(g_ds,"AntiVirus Policy\\General", "Is Scan Enabled" ,NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_set_blob(g_ds,"AntiVirus Policy\\General", "Is Scan Enabled" ,NULL , 1));

    /* When the path dont exists*/
    TEST_ASSERT_EQUAL_INT(MA_ERROR_DS_PATH_NOT_FOUND, ma_ds_get_str(g_ds, "AntiVirus Policy\\PATH_NOT_EXISTS", "Is Scan Enabled" , &value));
    TEST_ASSERT_NULL(value);

    /* When the path exists but key doesn exists*/
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_blob(g_ds, "AntiVirus Policy\\PATH_EXISTS", "Key_1" , "value",5));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_DS_KEY_NOT_FOUND, ma_ds_get_blob(g_ds, "AntiVirus Policy\\PATH_EXISTS", "Key_2" , &value));
    TEST_ASSERT_NULL(value);

    /* Set and get*/
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_blob(g_ds, "AntiVirus Policy\\General", "Is Scan Enabled" , "LOL" , 3));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_get_blob(g_ds, "AntiVirus Policy\\General", "Is Scan Enabled" , &value));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_buffer_get_raw(value, &ptr, &size));
    TEST_ASSERT_EQUAL_INT(3,size);
    memcpy(buff,ptr,size);
    TEST_ASSERT_EQUAL_INT(0,strcmp(buff,"LOL"));
    ma_buffer_release(value);
}


TEST(ma_ds_ini_tests_group, verify_ds_set_get_int_apis) {
    ma_int16_t value_16 = 0 ;
    ma_int32_t value_32 = 0 ;
    ma_int64_t value_64 = 0 ;

    /* datastore  is NULL */
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_int16(NULL, "AntiVirus Policy\\PATH_NOT_EXISTS", "Is Scan Enabled" , &value_16));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_set_int(NULL, "AntiVirus Policy\\PATH_NOT_EXISTS", "Is Scan Enabled" , value_16));

    /* Path is NULL */
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_int16(g_ds,NULL, "Is Scan Enabled" , &value_16));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_set_int(g_ds, NULL ,"Is Scan Enabled" , value_16));

    /* Key is NULL */
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_int16(g_ds,"AntiVirus Policy\\General", NULL , &value_16));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_set_int(g_ds, "AntiVirus Policy\\General" ,NULL , value_16));

    /* out param is NULL */
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_int16(g_ds,"AntiVirus Policy\\General", "Is Scan Enabled" ,NULL));

    /* When the path dont exists*/
    TEST_ASSERT_EQUAL_INT(MA_ERROR_DS_PATH_NOT_FOUND, ma_ds_get_int16(g_ds, "AntiVirus Policy\\PATH_NOT_EXISTS", "Is Scan Enabled" , &value_16));

    /* When the path exists but key doesn exists*/
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_int(g_ds, "AntiVirus Policy\\PATH_EXISTS", "Key_1" , 123456));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_DS_KEY_NOT_FOUND, ma_ds_get_int16(g_ds, "AntiVirus Policy\\PATH_EXISTS", "Key_2" , &value_16));

    /* Set and get*/
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_int(g_ds, "AntiVirus Policy\\General", "Is Scan Enabled" , -12345));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_get_int16(g_ds, "AntiVirus Policy\\General", "Is Scan Enabled" , &value_16));
    TEST_ASSERT_EQUAL_INT16(-12345,value_16);

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_get_int32(g_ds, "AntiVirus Policy\\General", "Is Scan Enabled" , &value_32));
    TEST_ASSERT_EQUAL_INT32(-12345, value_32);

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_get_int64(g_ds, "AntiVirus Policy\\General", "Is Scan Enabled" , &value_64));
    TEST_ASSERT_EQUAL_INT64(-12345, value_64);
}

TEST(ma_ds_ini_tests_group, verify_ds_remove_apis) {
     ma_buffer_t *value = NULL ;

    /* datastore  is NULL */
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_rem(NULL, "AntiVirus Policy\\PATH_NOT_EXISTS", "Is Scan Enabled" , 0));

    /* Path is NULL */
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_rem(g_ds,NULL, "Is Scan Enabled" , 0));

    /* Key is NULL */
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_rem(g_ds,"AntiVirus Policy\\General", NULL , 0));


    /* When the path dont exists*/
    TEST_ASSERT_EQUAL_INT(MA_ERROR_DS_PATH_NOT_FOUND, ma_ds_rem(g_ds, "AntiVirus Policy\\PATH_NOT_EXISTS", "Is Scan Enabled" , 0));

    /* Single remove */
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_blob(g_ds, "AntiVirus Policy\\PATH_EXISTS", "A" , "AA",2));

    //Remove
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_rem(g_ds, "AntiVirus Policy\\PATH_EXISTS", "A" , 0));

    //Try to get
    TEST_ASSERT_EQUAL_INT(MA_ERROR_DS_KEY_NOT_FOUND , ma_ds_get_blob(g_ds, "AntiVirus Policy\\PATH_EXISTS", "A" , &value));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_DS_KEY_NOT_FOUND , ma_ds_get_blob(g_ds, "AntiVirus Policy\\PATH_EXISTS", "B" , &value));

    /*Multi remove */
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_blob(g_ds, "AntiVirus Policy\\PATH_EXISTS", "A" , "AA",2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_blob(g_ds, "AntiVirus Policy\\PATH_EXISTS", "B" , "BB",2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_blob(g_ds, "AntiVirus Policy\\PATH_EXISTS", "C" , "CC",2));
    //rec -Remove
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_rem(g_ds, "AntiVirus Policy\\PATH_EXISTS", NULL , 1));
    //Try to get
    TEST_ASSERT_EQUAL_INT(MA_ERROR_DS_PATH_NOT_FOUND , ma_ds_get_blob(g_ds, "AntiVirus Policy\\PATH_EXISTS", "A" , &value));
}


