/*****************************************************************************
Datastore unit tests
******************************************************************************/
#define UNITY_FIXTURE_MALLOC_OVERRIDES_H_ 1

#include "unity.h"
#include "unity_fixture.h"

#include "ma/ma_datastore.h"
#include "ma/datastore/ma_ds.h"
#include "ma/datastore/ma_ds_mapper.h"
#include "ma_ds_test.h"

#include <string.h>
#include <stdio.h>


static ma_ds_mapper_t *g_mapper = NULL ;
static ma_ds_test_t *g_db_datastore = NULL  , *g_ini_datastore  = NULL , *g_reg_datastore = NULL ;

TEST_GROUP_RUNNER(ma_ds_mapper_tests_group) {
    do {RUN_TEST_CASE(ma_ds_mapper_tests, verify_open_release)} while (0);
    do {RUN_TEST_CASE(ma_ds_mapper_tests, verify_add)} while (0);
    do {RUN_TEST_CASE(ma_ds_mapper_tests, verify_remove)} while (0);
    do {RUN_TEST_CASE(ma_ds_mapper_tests, verify_set_apis)} while (0);
    do {RUN_TEST_CASE(ma_ds_mapper_tests, verify_get_apis)} while (0);
    do {RUN_TEST_CASE(ma_ds_mapper_tests, verify_remove_apis)} while (0);
}

static ma_error_t ds_test_reg_set(ma_ds_t *datastore , const char  *path , const char *key, ma_variant_t *value  ) {
    ma_ds_test_t *self = (ma_ds_test_t *)datastore ;
    const char *ptr = NULL ;

    /*Making sure that mapper have passed it to the right person */
    TEST_ASSERT_MESSAGE(MA_DS_TYPE_REG == self->ds_type  , "Failed to map the set apis for registry");

    /*Making sure that mapper have removed the MAPPER_PREFIX from the string */
    ptr = strstr(path , MA_DS_MAPPER_PREFIX_REG) ;
    TEST_ASSERT_MESSAGE( NULL == ptr ,  "Failed to remove the prefix for registry");
    return MA_OK ;
}
static ma_error_t ds_test_reg_get(ma_ds_t *datastore , const char  *path , const char *key , ma_vartype_t type, ma_variant_t **value) {
     ma_ds_test_t *self = (ma_ds_test_t *)datastore ;
    const char *ptr = NULL ;

    /*Making sure that mapper have passed it to the right person */
    TEST_ASSERT_MESSAGE(MA_DS_TYPE_REG == self->ds_type  , "Failed to map the set apis for registry");

    /*Making sure that mapper have removed the MAPPER_PREFIX from the string */
    ptr = strstr(path , MA_DS_MAPPER_PREFIX_REG) ;
    TEST_ASSERT_MESSAGE( NULL == ptr ,  "Failed to remove the prefix for registry");

    /*Fake the return type as NOT OK, else front end API's will start looking for dat */
    return MA_ERROR_DS_PATH_NOT_FOUND ;
}
static ma_error_t ds_test_reg_remove(ma_ds_t *datastore , const char  *path , const char *key ) {
     ma_ds_test_t *self = (ma_ds_test_t *)datastore ;
    const char *ptr = NULL ;

    /*Making sure that mapper have passed it to the right person */
    TEST_ASSERT_MESSAGE(MA_DS_TYPE_REG == self->ds_type  , "Failed to map the set apis for registry");

    /*Making sure that mapper have removed the MAPPER_PREFIX from the string */
    ptr = strstr(path , MA_DS_MAPPER_PREFIX_REG) ;
    TEST_ASSERT_MESSAGE( NULL == ptr ,  "Failed to remove the prefix for registry");
    return MA_OK ;
}

static ma_error_t ds_test_ini_set(ma_ds_t *datastore , const char  *path , const char *key, ma_variant_t *value  ) {
    ma_ds_test_t *self = (ma_ds_test_t *)datastore ;
    const char *ptr = NULL ;

    /*Making sure that mapper have passed it to the right person */
    TEST_ASSERT_MESSAGE(MA_DS_TYPE_INI == self->ds_type  , "Failed to map the set apis for ini");

    /*Making sure that mapper have removed the MAPPER_PREFIX from the string */
    ptr = strstr(path , MA_DS_MAPPER_PREFIX_INI) ;
    TEST_ASSERT_MESSAGE( NULL == ptr ,  "Failed to remove the prefix for ini");
    return MA_OK ;
}

static ma_error_t ds_test_ini_get(ma_ds_t *datastore , const char  *path , const char *key , ma_vartype_t type, ma_variant_t **value) {
     ma_ds_test_t *self = (ma_ds_test_t *)datastore ;
    const char *ptr = NULL ;

    /*Making sure that mapper have passed it to the right person */
    TEST_ASSERT_MESSAGE(MA_DS_TYPE_INI == self->ds_type  , "Failed to map the set apis for ini");

    /*Making sure that mapper have removed the MAPPER_PREFIX from the string */
    ptr = strstr(path , MA_DS_MAPPER_PREFIX_INI) ;
    TEST_ASSERT_MESSAGE( NULL == ptr ,  "Failed to remove the prefix for ini");


    /*Fake the return type as NOT OK, else front end API's will start looking for dat */
    return MA_ERROR_DS_PATH_NOT_FOUND ;
    return MA_OK ;
}
static ma_error_t ds_test_ini_remove(ma_ds_t *datastore , const char  *path , const char *key) {
     ma_ds_test_t *self = (ma_ds_test_t *)datastore ;
    const char *ptr = NULL ;

    /*Making sure that mapper have passed it to the right person */
    TEST_ASSERT_MESSAGE(MA_DS_TYPE_INI == self->ds_type  , "Failed to map the set apis for ini");

    /*Making sure that mapper have removed the MAPPER_PREFIX from the string */
    ptr = strstr(path , MA_DS_MAPPER_PREFIX_INI) ;
    TEST_ASSERT_MESSAGE( NULL == ptr ,  "Failed to remove the prefix for ini");

    return MA_OK ;
}

static ma_error_t ds_test_db_set(ma_ds_t *datastore , const char  *path , const char *key, ma_variant_t *value  ) {
      ma_ds_test_t *self = (ma_ds_test_t *)datastore ;
    const char *ptr = NULL ;

    /*Making sure that mapper have passed it to the right person */
    TEST_ASSERT_MESSAGE(MA_DS_TYPE_DBH == self->ds_type  , "Failed to map the set apis for db");

    /*Making sure that mapper have removed the MAPPER_PREFIX from the string */
    ptr = strstr(path , MA_DS_MAPPER_PREFIX_DBH) ;
    TEST_ASSERT_MESSAGE( NULL == ptr ,  "Failed to remove the prefix for db");
    return MA_OK ;
}
static ma_error_t ds_test_db_get(ma_ds_t *datastore , const char  *path , const char *key , ma_vartype_t type, ma_variant_t **value) {
     ma_ds_test_t *self = (ma_ds_test_t *)datastore ;
    const char *ptr = NULL ;

    /*Making sure that mapper have passed it to the right person */
    TEST_ASSERT_MESSAGE(MA_DS_TYPE_DBH == self->ds_type  , "Failed to map the set apis for db");

    /*Making sure that mapper have removed the MAPPER_PREFIX from the string */
    ptr = strstr(path , MA_DS_MAPPER_PREFIX_DBH) ;
    TEST_ASSERT_MESSAGE( NULL == ptr ,  "Failed to remove the prefix for db");

    /*Fake the return type as NOT OK, else front end API's will start looking for dat */
    return MA_ERROR_DS_PATH_NOT_FOUND ;
}
static ma_error_t ds_test_db_remove(ma_ds_t *datastore , const char  *path , const char *key ) {
      ma_ds_test_t *self = (ma_ds_test_t *)datastore ;
    const char *ptr = NULL ;

    /*Making sure that mapper have passed it to the right person */
    TEST_ASSERT_MESSAGE(MA_DS_TYPE_DBH == self->ds_type  , "Failed to map the set apis for db");

    /*Making sure that mapper have removed the MAPPER_PREFIX from the string */
    ptr = strstr(path , MA_DS_MAPPER_PREFIX_DBH) ;
    TEST_ASSERT_MESSAGE( NULL == ptr ,  "Failed to remove the prefix for db");
    return MA_OK ;
}

static const ma_ds_methods_t g_reg_methods = {
    &ds_test_reg_set,
    &ds_test_reg_get,
    &ds_test_reg_remove,
    NULL
};

static const ma_ds_methods_t g_ini_methods = {
    &ds_test_ini_set,
    &ds_test_ini_get,
    &ds_test_ini_remove,
    NULL
};

static const ma_ds_methods_t g_db_methods = {
    &ds_test_db_set,
    &ds_test_db_get,
    &ds_test_db_remove,
    NULL
};



TEST_SETUP(ma_ds_mapper_tests) {
    Unity.TestFile = "ma_ds_mapper_test.c";

    TEST_ASSERT_MESSAGE(MA_OK  ==  ma_ds_test_open(MA_DS_TYPE_DBH , &g_db_datastore)  , "Test datastore db  creation failed " );
    TEST_ASSERT_MESSAGE(MA_OK  ==  ma_ds_test_open(MA_DS_TYPE_INI , &g_ini_datastore)  , "Test datastore reg creation failed " );
    TEST_ASSERT_MESSAGE(MA_OK  ==  ma_ds_test_open(MA_DS_TYPE_REG , &g_reg_datastore)  , "Test datastore ini creation failed " );

    ma_ds_test_set_ds_methods(g_db_datastore , &g_db_methods);
    ma_ds_test_set_ds_methods(g_ini_datastore , &g_ini_methods);
    ma_ds_test_set_ds_methods(g_reg_datastore , &g_reg_methods);

    TEST_ASSERT_MESSAGE(MA_OK  == ma_ds_mapper_create(&g_mapper) , "Failed to create the mapper datastore");
    TEST_ASSERT_MESSAGE(MA_OK  == ma_ds_mapper_ds_add(g_mapper , MA_DS_TYPE_DBH , (ma_ds_t *)g_db_datastore , NULL) , "Failed to add db datastore in mapper ");
    TEST_ASSERT_MESSAGE(MA_OK  == ma_ds_mapper_ds_add(g_mapper , MA_DS_TYPE_INI , (ma_ds_t *)g_ini_datastore , NULL) , "Failed to add ini datastore in mapper ");
    TEST_ASSERT_MESSAGE(MA_OK  == ma_ds_mapper_ds_add(g_mapper , MA_DS_TYPE_REG , (ma_ds_t *) g_reg_datastore , NULL) , "Failed to add reg datastore in mapper ");


}

TEST_TEAR_DOWN(ma_ds_mapper_tests) {
    /*Remove the files which this group created */
    TEST_ASSERT_MESSAGE(MA_OK  == ma_ds_mapper_ds_remove(g_mapper , (ma_ds_t *)g_db_datastore ) , "Failed to remove db datastore from mapper");
    TEST_ASSERT_MESSAGE(MA_OK  == ma_ds_mapper_ds_remove(g_mapper , (ma_ds_t *)g_ini_datastore ) , "Failed to remove ini datastore from mapper");
    TEST_ASSERT_MESSAGE(MA_OK  == ma_ds_mapper_ds_remove(g_mapper , (ma_ds_t *)g_reg_datastore ) , "Failed to remove ini datastore from mapper");
    ma_ds_mapper_release(g_mapper);
    TEST_ASSERT_MESSAGE(MA_OK  ==  ma_ds_test_release(g_db_datastore)  , "Test datastore db  release failed " );
    TEST_ASSERT_MESSAGE(MA_OK  ==  ma_ds_test_release(g_ini_datastore)  , "Test datastore reg  release failed " );
    TEST_ASSERT_MESSAGE(MA_OK  ==  ma_ds_test_release(g_reg_datastore)  , "Test datastore ini  release failed " );

}

TEST(ma_ds_mapper_tests, verify_open_release) {
    ma_ds_mapper_t *mapper = NULL ;
    /*Negative - NULL pointer , argument*/
    TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG  == ma_ds_mapper_create(NULL) , "It should return invalid arg when it is null");
    TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG  == ma_ds_mapper_release(NULL) , "It should return invalid arg when it is null");
    /*Positive- Valid argument */
    TEST_ASSERT_MESSAGE(MA_OK  == ma_ds_mapper_create(&mapper) , "Failed to create the mapper datastore");
    TEST_ASSERT_MESSAGE(MA_OK  == ma_ds_mapper_release(mapper) , "Failed to release the mapper datastore");
}

TEST(ma_ds_mapper_tests, verify_add) {
    ma_ds_mapper_t *mapper = NULL ;
    ma_ds_test_t *db_datastore = NULL ;

    TEST_ASSERT_MESSAGE(MA_OK  ==  ma_ds_test_open(MA_DS_TYPE_DBH , &db_datastore)  , "Test datastore creation failed " );
    TEST_ASSERT_MESSAGE(MA_OK  == ma_ds_mapper_create(&mapper) , "Failed to create the mapper datastore");

    /* Negative - Null argument , first argument */
    TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG  == ma_ds_mapper_ds_add(NULL , MA_DS_TYPE_DBH  , (ma_ds_t *)db_datastore , "BLAHBLAH") , "It should return invalid arg");

    /* Negative Null argument , third argument */
    TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG  == ma_ds_mapper_ds_add(mapper , MA_DS_TYPE_DBH  , NULL , "BLAHBLAH") , "It should return invalid arg");

    /* Positive - Null argument , fourth argument , should be positive as it is optional */
    TEST_ASSERT_MESSAGE(MA_OK  == ma_ds_mapper_ds_add( mapper , MA_DS_TYPE_DBH  , (ma_ds_t *)db_datastore , NULL) , "It should NOT return invalid arg as fourth is optional");

    /*Cleanup*/
    TEST_ASSERT_MESSAGE(MA_OK  == ma_ds_test_release( db_datastore) , "Release of test datastore failed");
    TEST_ASSERT_MESSAGE(MA_OK  == ma_ds_mapper_release(mapper) , "Failed to release the mapper datastore");
}

TEST(ma_ds_mapper_tests, verify_remove) {
    ma_ds_mapper_t *mapper = NULL ;
    ma_ds_test_t *db_datastore = NULL ;

    TEST_ASSERT_MESSAGE(MA_OK  ==  ma_ds_test_open(MA_DS_TYPE_DBH , &db_datastore)  , "Test datastore creation failed " );
    TEST_ASSERT_MESSAGE(MA_OK  == ma_ds_mapper_create(&mapper) , "Failed to create the mapper datastore");

    /* neagtive - Null argument , first argument */
    TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG  == ma_ds_mapper_ds_remove(NULL , (ma_ds_t *)db_datastore ) , "It should return invalid arg");

    /* Negative - Null argument , second argument */
    TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG  == ma_ds_mapper_ds_remove(mapper , NULL) , "It should return invalid arg");

    /* Positive - Not added , still MA_OK , as we are not doing anything with that */
    TEST_ASSERT_MESSAGE(MA_OK  == ma_ds_mapper_ds_remove(mapper , (ma_ds_t *)db_datastore) , "Failed to remove empty mapper");

    /* Positive - valid aarguments */
    TEST_ASSERT_MESSAGE(MA_OK  == ma_ds_mapper_ds_add(mapper , MA_DS_TYPE_DBH  , (ma_ds_t *)db_datastore , NULL) , "Failed to add datastore in mapper");

    TEST_ASSERT_MESSAGE(MA_OK  == ma_ds_mapper_ds_remove(mapper , (ma_ds_t *)db_datastore ) , "Failed to remove datastore from mapper");

    /*Cleanup*/
    TEST_ASSERT_MESSAGE(MA_OK  == ma_ds_test_release( db_datastore) , "Release of test datastore failed");
    TEST_ASSERT_MESSAGE(MA_OK  == ma_ds_mapper_release(mapper) , "Failed to release the mapper datastore");
}

TEST(ma_ds_mapper_tests, verify_set_apis) {

    /*Registry set */
    ma_ds_set_int((ma_ds_t *)g_mapper, "MREG\\Network Associates\\Software\\" , "AntivirusEnabled", 12);

    /*INI set */
    ma_ds_set_int((ma_ds_t *)g_mapper, "MINI\\Network Associates\\Software\\" , "AntivirusEnabled", 12);

    /*DB set */
    ma_ds_set_int((ma_ds_t *)g_mapper, "MDBH\\Network Associates\\Software\\" , "AntivirusEnabled", 12);

}

TEST(ma_ds_mapper_tests, verify_get_apis) {
     int value ;

    /*Registry get */
    ma_ds_get_int32((ma_ds_t *)g_mapper, "MREG\\Network Associates\\Software\\" , "AntivirusEnabled", &value);

    /*INI get */
    ma_ds_get_int32((ma_ds_t *)g_mapper, "MINI\\Network Associates\\Software\\" , "AntivirusEnabled", &value);

    /*DB get */
    ma_ds_get_int32((ma_ds_t *)g_mapper, "MDBH\\Network Associates\\Software\\" , "AntivirusEnabled", &value);


}

TEST(ma_ds_mapper_tests, verify_remove_apis) {

    /*Registry remove */
    ma_ds_rem((ma_ds_t *)g_mapper, "MREG\\Network Associates\\Software\\" , "AntivirusEnabled",0);

    /*Ini Remove */
    ma_ds_rem((ma_ds_t *)g_mapper, "MINI\\Network Associates\\Software\\" , "AntivirusEnabled",0);

    /*DB remove */
    ma_ds_rem((ma_ds_t *)g_mapper, "MDBH\\Network Associates\\Software\\" , "AntivirusEnabled",0);

}
