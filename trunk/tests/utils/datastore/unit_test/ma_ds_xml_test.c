/*****************************************************************************
XML Datastore unit tests.
*****************************************************************************/
#include "unity.h"
#include "unity_fixture.h"
#include "ma/datastore/ma_ds_xml.h"
#include "ma/internal/utils/xml/ma_xml.h"
#include <string.h>

#ifndef MA_WINDOWS
   #include <unistd.h>
   #include <wchar.h>
#endif

static const char *xml_file = "./xml_database_file.xml";
static ma_ds_t *g_ds = NULL;

TEST_GROUP_RUNNER(ma_ds_xml_tests_group)
{
    do {RUN_TEST_CASE(ma_ds_xml_tests_group, verify_ds_xml_api)} while (0);
    do {RUN_TEST_CASE(ma_ds_xml_tests_group, verify_sitelist_xml_api)} while (0);
}

TEST_SETUP(ma_ds_xml_tests_group)
{

}

TEST_TEAR_DOWN(ma_ds_xml_tests_group)
{

}


/* The below XML is the test XML. It shows the hierarchy of elements (and their values)
   which are stored and retrieved in this test.

<Properties>
    <MachineName>IMToolComputer</MachineName>
    <MachineID>{01234567-89AB-CDEF-0123-456789ABCDEF}</MachineID>
    <PropsVersion>20051208224530</PropsVersion>
    <ComputerProperties>
        <PlatformID>Windows 7</PlatformID>
        <ComputerName>Lab Computer</ComputerName>
        <CPUinfo>
            <CPUType>Intel Pentium III or newer</CPUType>
            <NumOfCPU>4</NumOfCPU>
            <CPUSpeed>3220</CPUSpeed>
            <CPUSerialNumber>N/A</CPUSerialNumber>
        </CPUinfo>
        <OSInfo>
            <OSType>Windows 7 SP2</OSType>
            <OSPlatform>Server</OSPlatform>
            <OSVersion>5.0</OSVersion>
            <OSBuildNum>2195</OSBuildNum>
            <OSCsdVersion>Service Pack 4</OSCsdVersion>
        </OSInfo>
        <TotalPhysicalMemory>804270080</TotalPhysicalMemory>
        <FreeMemory>268947456</FreeMemory>
        <TimeZone>Pacific Standard Time</TimeZone>
        <DefaultLangID>0409</DefaultLangID>
        <EmailAddress>MA@McAfee.com</EmailAddress>
        <OSOEMId>51876-335-4615354-05779</OSOEMId>
        <LastUpdate>12/08/2005 14:45:30</LastUpdate>
        <USERInfo>
            <UserName>McAfee Agent 5.0 User</UserName>
            <DomainName>Internal</DomainName>
        </USERInfo>
    </ComputerProperties>
</Properties>
*/


TEST(ma_ds_xml_tests_group, verify_ds_xml_api)
{
    ma_ds_xml_t *ds_xml = NULL;

    unlink(xml_file);

    TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == ma_ds_xml_open(NULL, &ds_xml), "ma_ds_xml_open failed");
    TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == ma_ds_xml_open("", &ds_xml), "ma_ds_xml_open failed");
    TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == ma_ds_xml_open(xml_file, NULL), "ma_ds_xml_open failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_ds_xml_open(xml_file, &ds_xml), "ma_ds_xml_open failed");
    g_ds = (ma_ds_t*)ds_xml;

    // Storing the values in XML
    {
        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_set_str(g_ds, "Properties", "MachineName", "IMToolComputer", strlen("IMToolComputer")), \
                                                   "String value couldn't be stored in XML");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_set_wstr(g_ds, "Properties", "MachineID", L"{01234567-89AB-CDEF-0123-456789ABCDEF}", wcslen(L"{01234567-89AB-CDEF-0123-456789ABCDEF}")), \
                                                    "Wide String value couldn't be stored in XML");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_set_str(g_ds, "Properties", "PropsVersion", "20051208224530", strlen("20051208224530")), \
                                                   "String value couldn't be stored in XML");


        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_set_str(g_ds, "Properties\\ComputerProperties", "PlatformID", "Windows 7", strlen("Windows 7")), \
                                                   "String value couldn't be stored in XML");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_set_str(g_ds, "Properties\\ComputerProperties", "ComputerName", "Lab Computer", strlen("Lab Computer")), \
                                                   "String value couldn't be stored in XML");


        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_set_str(g_ds, "Properties\\ComputerProperties\\CPUinfo", "CPUType", "Intel Pentium III or newer", strlen("Intel Pentium III or newer")), \
                                                   "String value couldn't be stored in XML");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_set_int(g_ds, "Properties\\ComputerProperties\\CPUinfo", "NumOfCPU", 4), \
                                                   "Integer value couldn't be stored in XML");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_set_int(g_ds, "Properties\\ComputerProperties\\CPUinfo", "CPUSpeed", 3220), \
                                                   "Integer value couldn't be stored in XML");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_set_str(g_ds, "Properties\\ComputerProperties\\CPUinfo", "CPUSerialNumber", "N/A", strlen("N/A")), \
                                                   "String value couldn't be stored in XML");


        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_set_str(g_ds, "Properties\\ComputerProperties\\OSInfo", "OSType", "Windows 7 SP2", strlen("Windows 7 SP2")), \
                                                   "String value couldn't be stored in XML");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_set_str(g_ds, "Properties\\ComputerProperties\\OSInfo", "OSPlatform", "Server", strlen("Server")), \
                                                   "String value couldn't be stored in XML");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_set_str(g_ds, "Properties\\ComputerProperties\\OSInfo", "OSVersion", "5.0", strlen("5.0")), \
                                                   "String value couldn't be stored in XML");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_set_int(g_ds, "Properties\\ComputerProperties\\OSInfo", "OSBuildNum", 2195), \
                                                   "Integer value couldn't be stored in XML");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_set_str(g_ds, "Properties\\ComputerProperties\\OSInfo", "OSCsdVersion", "Service Pack 4", strlen("Service Pack 4")), \
                                                   "String value couldn't be stored in XML");


        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_set_int(g_ds, "Properties\\ComputerProperties", "TotalPhysicalMemory", 804270080), \
                                                   "Integer value couldn't be stored in XML");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_set_int(g_ds, "Properties\\ComputerProperties", "FreeMemory", 268947456), \
                                                   "Integer value couldn't be stored in XML");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_set_str(g_ds, "Properties\\ComputerProperties", "TimeZone", "Pacific Standard Time", strlen("Pacific Standard Time")), \
                                                   "String value couldn't be stored in XML");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_set_str(g_ds, "Properties\\ComputerProperties", "DefaultLangID", "0409", strlen("0409")), \
                                                   "String value couldn't be stored in XML");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_set_wstr(g_ds, "Properties\\ComputerProperties", "EmailAddress", L"MA@McAfee.com", wcslen(L"MA@McAfee.com")), \
                                                    "Wide String value couldn't be stored in XML");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_set_wstr(g_ds, "Properties\\ComputerProperties", "OSOEMId", L"51876-335-4615354-05779", wcslen(L"51876-335-4615354-05779")), \
                                                    "Wide String value couldn't be stored in XML");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_set_wstr(g_ds, "Properties\\ComputerProperties", "LastUpdate", L"12/08/2005 14:45:30", wcslen(L"12/08/2005 14:45:30")), \
                                                    "Wide String value couldn't be stored in XML");


        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_set_str(g_ds, "Properties\\ComputerProperties\\USERInfo", "UserName", "McAfee Agent 5.0 User", strlen("McAfee Agent 5.0 User")), \
                                                   "String value couldn't be stored in XML");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_set_str(g_ds, "Properties\\ComputerProperties\\USERInfo", "DomainName", "Internal", strlen("Internal")), \
                                                   "String value couldn't be stored in XML");
    }

    // Retrieving the values stored in the XML (individually)
    {
        ma_buffer_t *buffer = NULL;
        const char *string_val = NULL;
        size_t string_size = 0;

        ma_wbuffer_t *wbuffer = NULL;
        const wchar_t *wstring_val = NULL;
        size_t wstring_size = 0;

        ma_int16_t bit16_val = 0;
        ma_int32_t bit32_val = 0L;
        ma_int64_t bit64_val = 0LL;

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_str(g_ds, "Properties", "MachineName", &buffer), "Node could not be retrieved");
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("IMToolComputer", string_val, strlen("IMToolComputer"), "Node value could not be retrieved correctly");
        ma_buffer_release(buffer);

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_wstr(g_ds, "Properties", "MachineID", &wbuffer), "Node could not be retrieved");
        ma_wbuffer_get_string(wbuffer, &wstring_val, &wstring_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE(L"{01234567-89AB-CDEF-0123-456789ABCDEF}", wstring_val, wcslen(L"{01234567-89AB-CDEF-0123-456789ABCDEF}"), "Node value could not be retrieved correctly");
        ma_wbuffer_release(wbuffer);

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_str(g_ds, "Properties", "PropsVersion", &buffer), "Node could not be retrieved");
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("20051208224530", string_val, strlen("20051208224530"), "Node value could not be retrieved correctly");
        ma_buffer_release(buffer);

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_str(g_ds, "Properties\\ComputerProperties", "PlatformID", &buffer), "Node could not be retrieved");
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("Windows 7", string_val, strlen("Windows 7"), "Node value could not be retrieved correctly");
        ma_buffer_release(buffer);

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_str(g_ds, "Properties\\ComputerProperties", "ComputerName", &buffer), "Node could not be retrieved");
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("Lab Computer", string_val, strlen("Lab Computer"), "Node value could not be retrieved correctly");
        ma_buffer_release(buffer);

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_str(g_ds, "Properties\\ComputerProperties\\CPUinfo", "CPUType", &buffer), "Node could not be retrieved");
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("Intel Pentium III or newer", string_val, strlen("Intel Pentium III or newer"), "Node value could not be retrieved correctly");
        ma_buffer_release(buffer);

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_int16(g_ds, "Properties\\ComputerProperties\\CPUinfo", "NumOfCPU", &bit16_val), "Node could not be retrieved");
        TEST_ASSERT_MESSAGE(4 == bit16_val, "Node value could not be retrieved correctly");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_int32(g_ds, "Properties\\ComputerProperties\\CPUinfo", "CPUSpeed", &bit32_val), "Node could not be retrieved");
        TEST_ASSERT_MESSAGE(3220 == bit32_val, "Node value could not be retrieved correctly");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_str(g_ds, "Properties\\ComputerProperties\\CPUinfo", "CPUSerialNumber", &buffer), "Node could not be retrieved");
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("N/A", string_val, strlen("N/A"), "Node value could not be retrieved correctly");
        ma_buffer_release(buffer);

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_str(g_ds, "Properties\\ComputerProperties\\OSInfo", "OSType", &buffer), "Node could not be retrieved");
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("Windows 7 SP2", string_val, strlen("Windows 7 SP2"), "Node value could not be retrieved correctly");
        ma_buffer_release(buffer);

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_str(g_ds, "Properties\\ComputerProperties\\OSInfo", "OSPlatform", &buffer), "Node could not be retrieved");
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("Server", string_val, strlen("Server"), "Node value could not be retrieved correctly");
        ma_buffer_release(buffer);

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_str(g_ds, "Properties\\ComputerProperties\\OSInfo", "OSVersion", &buffer), "Node could not be retrieved");
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("5.0", string_val, strlen("5.0"), "Node value could not be retrieved correctly");
        ma_buffer_release(buffer);

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_int16(g_ds, "Properties\\ComputerProperties\\OSInfo", "OSBuildNum", &bit16_val), "Node could not be retrieved");
        TEST_ASSERT_MESSAGE(2195 == bit16_val, "Node value could not be retrieved correctly");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_str(g_ds, "Properties\\ComputerProperties\\OSInfo", "OSCsdVersion", &buffer), "Node could not be retrieved");
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("Service Pack 4", string_val, strlen("Service Pack 4"), "Node value could not be retrieved correctly");
        ma_buffer_release(buffer);

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_int64(g_ds, "Properties\\ComputerProperties", "TotalPhysicalMemory", &bit64_val), "Node could not be retrieved");
        TEST_ASSERT_MESSAGE(804270080 == bit64_val, "Node value could not be retrieved correctly");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_int64(g_ds, "Properties\\ComputerProperties", "FreeMemory", &bit64_val), "Node could not be retrieved");
        TEST_ASSERT_MESSAGE(268947456 == bit64_val, "Node value could not be retrieved correctly");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_str(g_ds, "Properties\\ComputerProperties", "TimeZone", &buffer), "Node could not be retrieved");
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("Pacific Standard Time", string_val, strlen("Pacific Standard Time"), "Node value could not be retrieved correctly");
        ma_buffer_release(buffer);

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_str(g_ds, "Properties\\ComputerProperties", "DefaultLangID", &buffer), "Node could not be retrieved");
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("0409", string_val, strlen("0409"), "Node value could not be retrieved correctly");
        ma_buffer_release(buffer);

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_wstr(g_ds, "Properties\\ComputerProperties", "EmailAddress", &wbuffer), "Node could not be retrieved");
        ma_wbuffer_get_string(wbuffer, &wstring_val, &wstring_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE(L"MA@McAfee.com", wstring_val, wcslen(L"MA@McAfee.com"), "Node value could not be retrieved correctly");
        ma_wbuffer_release(wbuffer);

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_wstr(g_ds, "Properties\\ComputerProperties", "OSOEMId", &wbuffer), "Node could not be retrieved");
        ma_wbuffer_get_string(wbuffer, &wstring_val, &wstring_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE(L"51876-335-4615354-05779", wstring_val, wcslen(L"51876-335-4615354-05779"), "Node value could not be retrieved correctly");
        ma_wbuffer_release(wbuffer);

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_wstr(g_ds, "Properties\\ComputerProperties", "LastUpdate", &wbuffer), "Node could not be retrieved");
        ma_wbuffer_get_string(wbuffer, &wstring_val, &wstring_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE(L"12/08/2005 14:45:30", wstring_val, wcslen(L"12/08/2005 14:45:30"), "Node value could not be retrieved correctly");
        ma_wbuffer_release(wbuffer);

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_str(g_ds, "Properties\\ComputerProperties\\USERInfo", "UserName", &buffer), "Node could not be retrieved");
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("McAfee Agent 5.0 User", string_val, strlen("McAfee Agent 5.0 User"), "Node value could not be retrieved correctly");
        ma_buffer_release(buffer);

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_str(g_ds, "Properties\\ComputerProperties\\USERInfo", "DomainName", &buffer), "Node could not be retrieved");
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("Internal", string_val, strlen("Internal"), "Node value could not be retrieved correctly");
        ma_buffer_release(buffer);
    }

    // Iterating the element hierarchy. Verifying that the DS iterator correctly parses the element hierarchy.
    {
        ma_buffer_t *buffer = NULL;
        const char *string_val = NULL;
        size_t string_size = 0;

        ma_ds_iterator_t *iterator = NULL;

        // Creating iterator for top-level "Properties"
        TEST_ASSERT(MA_ERROR_PRECONDITION == ma_ds_iterator_create(NULL, 1, "Properties", &iterator));
        TEST_ASSERT(MA_ERROR_PRECONDITION == ma_ds_iterator_create(g_ds, 1, NULL, &iterator));
        TEST_ASSERT(MA_ERROR_PRECONDITION == ma_ds_iterator_create(g_ds, 1, "Properties", NULL));
        TEST_ASSERT(MA_OK == ma_ds_iterator_create(g_ds, 1, "Properties", &iterator));

        TEST_ASSERT(MA_ERROR_PRECONDITION == ma_ds_iterator_get_next(NULL, &buffer));
        TEST_ASSERT(MA_ERROR_PRECONDITION == ma_ds_iterator_get_next(iterator, NULL));
        TEST_ASSERT(MA_OK == ma_ds_iterator_get_next(iterator, &buffer));
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("MachineName", string_val, strlen("MachineName"), "XML Element hierarchy not parsed correctly");
        ma_buffer_release(buffer);

        ma_ds_iterator_get_next(iterator, &buffer);
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("MachineID", string_val, strlen("MachineID"), "XML Element hierarchy not parsed correctly");
        ma_buffer_release(buffer);

        ma_ds_iterator_get_next(iterator, &buffer);
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("PropsVersion", string_val, strlen("PropsVersion"), "XML Element hierarchy not parsed correctly");
        ma_buffer_release(buffer);

        ma_ds_iterator_get_next(iterator, &buffer);
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("ComputerProperties", string_val, strlen("ComputerProperties"), "XML Element hierarchy not parsed correctly");
        ma_buffer_release(buffer);

        // No more items should be there are top-level
        TEST_ASSERT_MESSAGE(MA_ERROR_NO_MORE_ITEMS == ma_ds_iterator_get_next(iterator, &buffer), "XML Element hierarchy not parsed correctly");
        ma_ds_iterator_release(NULL);
        ma_ds_iterator_release(iterator);

        // Creating iterator for "Properties\ComputerProperties"
        ma_ds_iterator_create(g_ds, 1, "Properties\\ComputerProperties", &iterator);

        ma_ds_iterator_get_next(iterator, &buffer);
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("PlatformID", string_val, strlen("PlatformID"), "XML Element hierarchy not parsed correctly");
        ma_buffer_release(buffer);

        ma_ds_iterator_get_next(iterator, &buffer);
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("ComputerName", string_val, strlen("ComputerName"), "XML Element hierarchy not parsed correctly");
        ma_buffer_release(buffer);

        ma_ds_iterator_get_next(iterator, &buffer);
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("CPUinfo", string_val, strlen("CPUinfo"), "XML Element hierarchy not parsed correctly");
        ma_buffer_release(buffer);

        ma_ds_iterator_get_next(iterator, &buffer);
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("OSInfo", string_val, strlen("OSInfo"), "XML Element hierarchy not parsed correctly");
        ma_buffer_release(buffer);

        ma_ds_iterator_get_next(iterator, &buffer);
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("TotalPhysicalMemory", string_val, strlen("TotalPhysicalMemory"), "XML Element hierarchy not parsed correctly");
        ma_buffer_release(buffer);

        ma_ds_iterator_get_next(iterator, &buffer);
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("FreeMemory", string_val, strlen("FreeMemory"), "XML Element hierarchy not parsed correctly");
        ma_buffer_release(buffer);

        ma_ds_iterator_get_next(iterator, &buffer);
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("TimeZone", string_val, strlen("TimeZone"), "XML Element hierarchy not parsed correctly");
        ma_buffer_release(buffer);

        ma_ds_iterator_get_next(iterator, &buffer);
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("DefaultLangID", string_val, strlen("DefaultLangID"), "XML Element hierarchy not parsed correctly");
        ma_buffer_release(buffer);

        ma_ds_iterator_get_next(iterator, &buffer);
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("EmailAddress", string_val, strlen("EmailAddress"), "XML Element hierarchy not parsed correctly");
        ma_buffer_release(buffer);

        ma_ds_iterator_get_next(iterator, &buffer);
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("OSOEMId", string_val, strlen("OSOEMId"), "XML Element hierarchy not parsed correctly");
        ma_buffer_release(buffer);

        ma_ds_iterator_get_next(iterator, &buffer);
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("LastUpdate", string_val, strlen("LastUpdate"), "XML Element hierarchy not parsed correctly");
        ma_buffer_release(buffer);

        ma_ds_iterator_get_next(iterator, &buffer);
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("USERInfo", string_val, strlen("USERInfo"), "XML Element hierarchy not parsed correctly");
        ma_buffer_release(buffer);

        // No more items in Properties\ComputerProperties
        TEST_ASSERT_MESSAGE(MA_ERROR_NO_MORE_ITEMS == ma_ds_iterator_get_next(iterator, &buffer), "XML Element hierarchy not parsed correctly");
        ma_ds_iterator_release(iterator);

        // Creating iterator for "Properties\ComputerProperties\CPUinfo"
        ma_ds_iterator_create(g_ds, 1, "Properties\\ComputerProperties\\CPUinfo", &iterator);

        ma_ds_iterator_get_next(iterator, &buffer);
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("CPUType", string_val, strlen("CPUType"), "XML Element hierarchy not parsed correctly");
        ma_buffer_release(buffer);

        ma_ds_iterator_get_next(iterator, &buffer);
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("NumOfCPU", string_val, strlen("NumOfCPU"), "XML Element hierarchy not parsed correctly");
        ma_buffer_release(buffer);

        ma_ds_iterator_get_next(iterator, &buffer);
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("CPUSpeed", string_val, strlen("CPUSpeed"), "XML Element hierarchy not parsed correctly");
        ma_buffer_release(buffer);

        ma_ds_iterator_get_next(iterator, &buffer);
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("CPUSerialNumber", string_val, strlen("CPUSerialNumber"), "XML Element hierarchy not parsed correctly");
        ma_buffer_release(buffer);

        // No more items in Properties\ComputerProperties\CPUinfo
        TEST_ASSERT_MESSAGE(MA_ERROR_NO_MORE_ITEMS == ma_ds_iterator_get_next(iterator, &buffer), "XML Element hierarchy not parsed correctly");
        ma_ds_iterator_release(iterator);

        // Creating iterator for "Properties\ComputerProperties\OSInfo"
        ma_ds_iterator_create(g_ds, 1, "Properties\\ComputerProperties\\OSInfo", &iterator);

        ma_ds_iterator_get_next(iterator, &buffer);
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("OSType", string_val, strlen("OSType"), "XML Element hierarchy not parsed correctly");
        ma_buffer_release(buffer);

        ma_ds_iterator_get_next(iterator, &buffer);
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("OSPlatform", string_val, strlen("OSPlatform"), "XML Element hierarchy not parsed correctly");
        ma_buffer_release(buffer);

        ma_ds_iterator_get_next(iterator, &buffer);
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("OSVersion", string_val, strlen("OSVersion"), "XML Element hierarchy not parsed correctly");
        ma_buffer_release(buffer);

        ma_ds_iterator_get_next(iterator, &buffer);
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("OSBuildNum", string_val, strlen("OSBuildNum"), "XML Element hierarchy not parsed correctly");
        ma_buffer_release(buffer);

        ma_ds_iterator_get_next(iterator, &buffer);
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("OSCsdVersion", string_val, strlen("OSCsdVersion"), "XML Element hierarchy not parsed correctly");
        ma_buffer_release(buffer);

        // No more items in Properties\ComputerProperties\CPUinfo
        TEST_ASSERT_MESSAGE(MA_ERROR_NO_MORE_ITEMS == ma_ds_iterator_get_next(iterator, &buffer), "XML Element hierarchy not parsed correctly");
        ma_ds_iterator_release(iterator);

        // Creating iterator for "Properties\ComputerProperties\USERInfo"
        ma_ds_iterator_create(g_ds, 1, "Properties\\ComputerProperties\\USERInfo", &iterator);

        ma_ds_iterator_get_next(iterator, &buffer);
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("UserName", string_val, strlen("UserName"), "XML Element hierarchy not parsed correctly");
        ma_buffer_release(buffer);

        ma_ds_iterator_get_next(iterator, &buffer);
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("DomainName", string_val, strlen("DomainName"), "XML Element hierarchy not parsed correctly");
        ma_buffer_release(buffer);

        // No more items in Properties\ComputerProperties\CPUinfo
        TEST_ASSERT_MESSAGE(MA_ERROR_NO_MORE_ITEMS == ma_ds_iterator_get_next(iterator, &buffer), "XML Element hierarchy not parsed correctly");
        ma_ds_iterator_release(iterator);
    }

    // Modifying the values in XML. Call the datastore set functions to set the XML keys to different values.
    {
        // Storing first value as variant
        {
            ma_variant_t *my_variant_ptr = NULL;
            TEST_ASSERT(MA_OK == ma_variant_create_from_string("MANextGenComputer", strlen("MANextGenComputer"), &my_variant_ptr));
            TEST_ASSERT_MESSAGE(MA_OK == ma_ds_set_variant(g_ds, "Properties", "MachineName", my_variant_ptr), "String value couldn't be stored in XML");
            TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));
        }

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_set_wstr(g_ds, "Properties", "MachineID", L"{12345-89AB-CDEF-0123-456789ABCDEF}", wcslen(L"{12345-89AB-CDEF-0123-456789ABCDEF}")), \
                                                    "Wide String value couldn't be stored in XML");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_set_str(g_ds, "Properties", "PropsVersion", "234234324", strlen("234234324")), \
                                                   "String value couldn't be stored in XML");


        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_set_str(g_ds, "Properties\\ComputerProperties", "PlatformID", "Windows 8", strlen("Windows 8")), \
                                                   "String value couldn't be stored in XML");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_set_str(g_ds, "Properties\\ComputerProperties", "ComputerName", "My Lab Computer", strlen("My Lab Computer")), \
                                                   "String value couldn't be stored in XML");


        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_set_str(g_ds, "Properties\\ComputerProperties\\CPUinfo", "CPUType", "ARM Processor (Quad Core)", strlen("ARM Processor (Quad Core)")), \
                                                   "String value couldn't be stored in XML");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_set_int(g_ds, "Properties\\ComputerProperties\\CPUinfo", "NumOfCPU", 8), \
                                                   "Integer value couldn't be stored in XML");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_set_int(g_ds, "Properties\\ComputerProperties\\CPUinfo", "CPUSpeed", 4000), \
                                                   "Integer value couldn't be stored in XML");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_set_str(g_ds, "Properties\\ComputerProperties\\CPUinfo", "CPUSerialNumber", "43242343", strlen("43242343")), \
                                                   "String value couldn't be stored in XML");


        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_set_str(g_ds, "Properties\\ComputerProperties\\OSInfo", "OSType", "Windows 8 SP1", strlen("Windows 8 SP1")), \
                                                   "String value couldn't be stored in XML");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_set_str(g_ds, "Properties\\ComputerProperties\\OSInfo", "OSPlatform", "Enterprise", strlen("Enterprise")), \
                                                   "String value couldn't be stored in XML");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_set_str(g_ds, "Properties\\ComputerProperties\\OSInfo", "OSVersion", "8.1", strlen("8.1")), \
                                                   "String value couldn't be stored in XML");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_set_int(g_ds, "Properties\\ComputerProperties\\OSInfo", "OSBuildNum", 120), \
                                                   "Integer value couldn't be stored in XML");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_set_str(g_ds, "Properties\\ComputerProperties\\OSInfo", "OSCsdVersion", "Service Pack 1", strlen("Service Pack 1")), \
                                                   "String value couldn't be stored in XML");


        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_set_int(g_ds, "Properties\\ComputerProperties", "TotalPhysicalMemory", 804270080), \
                                                   "Integer value couldn't be stored in XML");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_set_int(g_ds, "Properties\\ComputerProperties", "FreeMemory", 268947456), \
                                                   "Integer value couldn't be stored in XML");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_set_str(g_ds, "Properties\\ComputerProperties", "TimeZone", "Greenwich Mean Time (GMT)", strlen("Greenwich Mean Time (GMT)")), \
                                                   "String value couldn't be stored in XML");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_set_str(g_ds, "Properties\\ComputerProperties", "DefaultLangID", "0409", strlen("0409")), \
                                                   "String value couldn't be stored in XML");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_set_wstr(g_ds, "Properties\\ComputerProperties", "EmailAddress", L"MA-NextGen@McAfee.com", wcslen(L"MA-NextGen@McAfee.com")), \
                                                    "Wide String value couldn't be stored in XML");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_set_wstr(g_ds, "Properties\\ComputerProperties", "OSOEMId", L"51876-335-4615354-05779", wcslen(L"51876-335-4615354-05779")), \
                                                    "Wide String value couldn't be stored in XML");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_set_wstr(g_ds, "Properties\\ComputerProperties", "LastUpdate", L"12/08/2005 14:45:30", wcslen(L"12/08/2005 14:45:30")), \
                                                    "Wide String value couldn't be stored in XML");


        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_set_str(g_ds, "Properties\\ComputerProperties\\USERInfo", "UserName", "McAfee Agent 5.0 User 1", strlen("McAfee Agent 5.0 User 1")), \
                                                   "String value couldn't be stored in XML");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_set_str(g_ds, "Properties\\ComputerProperties\\USERInfo", "DomainName", "External-Customer", strlen("External-Customer")), \
                                                   "String value couldn't be stored in XML");
    }

    // Retrieving the values stored in the XML (individually). Modified values should be returned.
    {
        ma_buffer_t *buffer = NULL;
        const char *string_val = NULL;
        size_t string_size = 0;

        ma_wbuffer_t *wbuffer = NULL;
        const wchar_t *wstring_val = NULL;
        size_t wstring_size = 0;

        ma_int16_t bit16_val = 0;
        ma_int32_t bit32_val = 0L;
        ma_int64_t bit64_val = 0LL;

        {
            ma_variant_t *my_variant_ptr = NULL;

            TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_variant(g_ds, "Properties", "MachineName", MA_VARTYPE_STRING, &my_variant_ptr), "Node could not be retrieved");
            TEST_ASSERT(MA_OK == ma_variant_get_string_buffer(my_variant_ptr, &buffer));
            TEST_ASSERT(MA_OK == ma_buffer_get_string(buffer, &string_val, &string_size));
            TEST_ASSERT_EQUAL_MEMORY_MESSAGE("MANextGenComputer", string_val, strlen("MANextGenComputer"), "Node value could not be retrieved correctly");
            ma_buffer_release(buffer);
            ma_variant_release(my_variant_ptr);
        }

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_wstr(g_ds, "Properties", "MachineID", &wbuffer), "Node could not be retrieved");
        ma_wbuffer_get_string(wbuffer, &wstring_val, &wstring_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE(L"{12345-89AB-CDEF-0123-456789ABCDEF}", wstring_val, wcslen(L"{12345-89AB-CDEF-0123-456789ABCDEF}"), "Node value could not be retrieved correctly");
        ma_wbuffer_release(wbuffer);

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_str(g_ds, "Properties", "PropsVersion", &buffer), "Node could not be retrieved");
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("234234324", string_val, strlen("234234324"), "Node value could not be retrieved correctly");
        ma_buffer_release(buffer);

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_str(g_ds, "Properties\\ComputerProperties", "PlatformID", &buffer), "Node could not be retrieved");
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("Windows 8", string_val, strlen("Windows 8"), "Node value could not be retrieved correctly");
        ma_buffer_release(buffer);

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_str(g_ds, "Properties\\ComputerProperties", "ComputerName", &buffer), "Node could not be retrieved");
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("My Lab Computer", string_val, strlen("My Lab Computer"), "Node value could not be retrieved correctly");
        ma_buffer_release(buffer);

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_str(g_ds, "Properties\\ComputerProperties\\CPUinfo", "CPUType", &buffer), "Node could not be retrieved");
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("ARM Processor (Quad Core)", string_val, strlen("ARM Processor (Quad Core)"), "Node value could not be retrieved correctly");
        ma_buffer_release(buffer);

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_int16(g_ds, "Properties\\ComputerProperties\\CPUinfo", "NumOfCPU", &bit16_val), "Node could not be retrieved");
        TEST_ASSERT_MESSAGE(8 == bit16_val, "Node value could not be retrieved correctly");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_int32(g_ds, "Properties\\ComputerProperties\\CPUinfo", "CPUSpeed", &bit32_val), "Node could not be retrieved");
        TEST_ASSERT_MESSAGE(4000 == bit32_val, "Node value could not be retrieved correctly");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_str(g_ds, "Properties\\ComputerProperties\\CPUinfo", "CPUSerialNumber", &buffer), "Node could not be retrieved");
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("43242343", string_val, strlen("43242343"), "Node value could not be retrieved correctly");
        ma_buffer_release(buffer);

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_str(g_ds, "Properties\\ComputerProperties\\OSInfo", "OSType", &buffer), "Node could not be retrieved");
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("Windows 8 SP1", string_val, strlen("Windows 8 SP1"), "Node value could not be retrieved correctly");
        ma_buffer_release(buffer);

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_str(g_ds, "Properties\\ComputerProperties\\OSInfo", "OSPlatform", &buffer), "Node could not be retrieved");
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("Enterprise", string_val, strlen("Enterprise"), "Node value could not be retrieved correctly");
        ma_buffer_release(buffer);

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_str(g_ds, "Properties\\ComputerProperties\\OSInfo", "OSVersion", &buffer), "Node could not be retrieved");
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("8.1", string_val, strlen("8.1"), "Node value could not be retrieved correctly");
        ma_buffer_release(buffer);

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_int16(g_ds, "Properties\\ComputerProperties\\OSInfo", "OSBuildNum", &bit16_val), "Node could not be retrieved");
        TEST_ASSERT_MESSAGE(120 == bit16_val, "Node value could not be retrieved correctly");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_str(g_ds, "Properties\\ComputerProperties\\OSInfo", "OSCsdVersion", &buffer), "Node could not be retrieved");
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("Service Pack 1", string_val, strlen("Service Pack 1"), "Node value could not be retrieved correctly");
        ma_buffer_release(buffer);

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_int64(g_ds, "Properties\\ComputerProperties", "TotalPhysicalMemory", &bit64_val), "Node could not be retrieved");
        TEST_ASSERT_MESSAGE(804270080 == bit64_val, "Node value could not be retrieved correctly");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_int64(g_ds, "Properties\\ComputerProperties", "FreeMemory", &bit64_val), "Node could not be retrieved");
        TEST_ASSERT_MESSAGE(268947456 == bit64_val, "Node value could not be retrieved correctly");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_str(g_ds, "Properties\\ComputerProperties", "TimeZone", &buffer), "Node could not be retrieved");
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("Greenwich Mean Time (GMT)", string_val, strlen("Greenwich Mean Time (GMT)"), "Node value could not be retrieved correctly");
        ma_buffer_release(buffer);

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_str(g_ds, "Properties\\ComputerProperties", "DefaultLangID", &buffer), "Node could not be retrieved");
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("0409", string_val, strlen("0409"), "Node value could not be retrieved correctly");
        ma_buffer_release(buffer);

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_wstr(g_ds, "Properties\\ComputerProperties", "EmailAddress", &wbuffer), "Node could not be retrieved");
        ma_wbuffer_get_string(wbuffer, &wstring_val, &wstring_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE(L"MA-NextGen@McAfee.com", wstring_val, wcslen(L"MA-NextGen@McAfee.com"), "Node value could not be retrieved correctly");
        ma_wbuffer_release(wbuffer);

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_wstr(g_ds, "Properties\\ComputerProperties", "OSOEMId", &wbuffer), "Node could not be retrieved");
        ma_wbuffer_get_string(wbuffer, &wstring_val, &wstring_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE(L"51876-335-4615354-05779", wstring_val, wcslen(L"51876-335-4615354-05779"), "Node value could not be retrieved correctly");
        ma_wbuffer_release(wbuffer);

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_wstr(g_ds, "Properties\\ComputerProperties", "LastUpdate", &wbuffer), "Node could not be retrieved");
        ma_wbuffer_get_string(wbuffer, &wstring_val, &wstring_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE(L"12/08/2005 14:45:30", wstring_val, wcslen(L"12/08/2005 14:45:30"), "Node value could not be retrieved correctly");
        ma_wbuffer_release(wbuffer);

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_str(g_ds, "Properties\\ComputerProperties\\USERInfo", "UserName", &buffer), "Node could not be retrieved");
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("McAfee Agent 5.0 User 1", string_val, strlen("McAfee Agent 5.0 User 1"), "Node value could not be retrieved correctly");
        ma_buffer_release(buffer);

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_get_str(g_ds, "Properties\\ComputerProperties\\USERInfo", "DomainName", &buffer), "Node could not be retrieved");
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("External-Customer", string_val, strlen("External-Customer"), "Node value could not be retrieved correctly");
        ma_buffer_release(buffer);
    }


    // 1) Removing nodes from XML one by one.
    // 2) For each node, trying to retrieve the value of node and verifying that the API returns MA_ERROR_DS_KEY_NOT_FOUND.
    {
        ma_buffer_t *buffer = NULL;

        ma_wbuffer_t *wbuffer = NULL;

        ma_int16_t bit16_val = 0;
        ma_int32_t bit32_val = 0L;
        ma_int64_t bit64_val = 0LL;

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_rem(g_ds, "Properties", "MachineName", 0), "ma_ds_rem failed");
        TEST_ASSERT_MESSAGE(MA_ERROR_DS_KEY_NOT_FOUND == ma_ds_get_str(g_ds, "Properties", "MachineName", &buffer), "Node could not be removed");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_rem(g_ds, "Properties", "MachineID", 0), "ma_ds_rem failed");
        TEST_ASSERT_MESSAGE(MA_ERROR_DS_KEY_NOT_FOUND == ma_ds_get_wstr(g_ds, "Properties", "MachineID", &wbuffer), "Node could not be removed");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_rem(g_ds, "Properties", "PropsVersion", 0), "ma_ds_rem failed");
        TEST_ASSERT_MESSAGE(MA_ERROR_DS_KEY_NOT_FOUND == ma_ds_get_str(g_ds, "Properties", "PropsVersion", &buffer), "Node could not be removed");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_rem(g_ds, "Properties\\ComputerProperties", "PlatformID", 0), "ma_ds_rem failed");
        TEST_ASSERT_MESSAGE(MA_ERROR_DS_KEY_NOT_FOUND == ma_ds_get_str(g_ds, "Properties\\ComputerProperties", "PlatformID", &buffer), "Node could not be removed");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_rem(g_ds, "Properties\\ComputerProperties", "ComputerName", 0), "ma_ds_rem failed");
        TEST_ASSERT_MESSAGE(MA_ERROR_DS_KEY_NOT_FOUND == ma_ds_get_str(g_ds, "Properties\\ComputerProperties", "ComputerName", &buffer), "Node could not be removed");

        // Recursive remove
        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_rem(g_ds, "Properties\\ComputerProperties\\CPUinfo", NULL, 1), "ma_ds_rem failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_DS_KEY_NOT_FOUND == ma_ds_get_str(g_ds, "Properties\\ComputerProperties\\CPUinfo", "CPUType", &buffer), "Node could not be removed");
        TEST_ASSERT_MESSAGE(MA_ERROR_DS_KEY_NOT_FOUND == ma_ds_get_int16(g_ds, "Properties\\ComputerProperties\\CPUinfo", "NumOfCPU", &bit16_val), "Node could not be removed");
        TEST_ASSERT_MESSAGE(MA_ERROR_DS_KEY_NOT_FOUND == ma_ds_get_int32(g_ds, "Properties\\ComputerProperties\\CPUinfo", "CPUSpeed", &bit32_val), "Node could not be removed");
        TEST_ASSERT_MESSAGE(MA_ERROR_DS_KEY_NOT_FOUND == ma_ds_get_str(g_ds, "Properties\\ComputerProperties\\CPUinfo", "CPUSerialNumber", &buffer), "Node could not be removed");

        // Recursive remove
        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_rem(g_ds, "Properties\\ComputerProperties\\OSInfo", NULL, 1), "ma_ds_rem failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_DS_KEY_NOT_FOUND == ma_ds_get_str(g_ds, "Properties\\ComputerProperties\\OSInfo", "OSType", &buffer), "Node could not be removed");
        TEST_ASSERT_MESSAGE(MA_ERROR_DS_KEY_NOT_FOUND == ma_ds_get_str(g_ds, "Properties\\ComputerProperties\\OSInfo", "OSPlatform", &buffer), "Node could not be removed");
        TEST_ASSERT_MESSAGE(MA_ERROR_DS_KEY_NOT_FOUND == ma_ds_get_str(g_ds, "Properties\\ComputerProperties\\OSInfo", "OSVersion", &buffer), "Node could not be removed");
        TEST_ASSERT_MESSAGE(MA_ERROR_DS_KEY_NOT_FOUND == ma_ds_get_int16(g_ds, "Properties\\ComputerProperties\\OSInfo", "OSBuildNum", &bit16_val), "Node could not be removed");
        TEST_ASSERT_MESSAGE(MA_ERROR_DS_KEY_NOT_FOUND == ma_ds_get_str(g_ds, "Properties\\ComputerProperties\\OSInfo", "OSCsdVersion", &buffer), "Node could not be removed");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_rem(g_ds, "Properties\\ComputerProperties", "TotalPhysicalMemory", 0), "ma_ds_rem failed");
        TEST_ASSERT_MESSAGE(MA_ERROR_DS_KEY_NOT_FOUND == ma_ds_get_int64(g_ds, "Properties\\ComputerProperties", "TotalPhysicalMemory", &bit64_val), "Node could not be removed");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_rem(g_ds, "Properties\\ComputerProperties", "FreeMemory", 0), "ma_ds_rem failed");
        TEST_ASSERT_MESSAGE(MA_ERROR_DS_KEY_NOT_FOUND == ma_ds_get_int64(g_ds, "Properties\\ComputerProperties", "FreeMemory", &bit64_val), "Node could not be removed");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_rem(g_ds, "Properties\\ComputerProperties", "TimeZone", 0), "ma_ds_rem failed");
        TEST_ASSERT_MESSAGE(MA_ERROR_DS_KEY_NOT_FOUND == ma_ds_get_str(g_ds, "Properties\\ComputerProperties", "TimeZone", &buffer), "Node could not be removed");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_rem(g_ds, "Properties\\ComputerProperties", "DefaultLangID", 0), "ma_ds_rem failed");
        TEST_ASSERT_MESSAGE(MA_ERROR_DS_KEY_NOT_FOUND == ma_ds_get_str(g_ds, "Properties\\ComputerProperties", "DefaultLangID", &buffer), "Node could not be removed");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_rem(g_ds, "Properties\\ComputerProperties", "EmailAddress", 0), "ma_ds_rem failed");
        TEST_ASSERT_MESSAGE(MA_ERROR_DS_KEY_NOT_FOUND == ma_ds_get_wstr(g_ds, "Properties\\ComputerProperties", "EmailAddress", &wbuffer), "Node could not be removed");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_rem(g_ds, "Properties\\ComputerProperties", "OSOEMId", 0), "ma_ds_rem failed");
        TEST_ASSERT_MESSAGE(MA_ERROR_DS_KEY_NOT_FOUND == ma_ds_get_wstr(g_ds, "Properties\\ComputerProperties", "OSOEMId", &wbuffer), "Node could not be removed");

        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_rem(g_ds, "Properties\\ComputerProperties", "LastUpdate", 0), "ma_ds_rem failed");
        TEST_ASSERT_MESSAGE(MA_ERROR_DS_KEY_NOT_FOUND == ma_ds_get_wstr(g_ds, "Properties\\ComputerProperties", "LastUpdate", &wbuffer), "Node could not be removed");

        // Recursive remove
        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_rem(g_ds, "Properties\\ComputerProperties\\USERInfo", NULL, 1), "ma_ds_rem failed");
        TEST_ASSERT_MESSAGE(MA_ERROR_DS_KEY_NOT_FOUND == ma_ds_get_str(g_ds, "Properties\\ComputerProperties\\USERInfo", "UserName", &buffer), "Node could not be removed");
        TEST_ASSERT_MESSAGE(MA_ERROR_DS_KEY_NOT_FOUND == ma_ds_get_str(g_ds, "Properties\\ComputerProperties\\USERInfo", "DomainName", &buffer), "Node could not be removed");

        // Removing base node 'ComputerProperties'
        TEST_ASSERT_MESSAGE(MA_OK == ma_ds_rem(g_ds, "Properties\\ComputerProperties", NULL, 1), "ma_ds_rem failed");
    }


    // Iterating the hierarchy to verify that there are no elements left under "Properties" root node
    {
        ma_ds_iterator_t *iterator = NULL;
        ma_buffer_t *buffer = NULL;

        ma_ds_iterator_create(g_ds, 1, "Properties", &iterator);

        TEST_ASSERT_MESSAGE(MA_ERROR_NO_MORE_ITEMS == ma_ds_iterator_get_next(iterator, &buffer), "All elements under root node could not be deleted");

        ma_ds_iterator_release(iterator);
    }

    TEST_ASSERT_MESSAGE(MA_ERROR_PRECONDITION == ma_ds_release(NULL), "ma_ds_release failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_ds_release(g_ds), "ma_ds_release failed");

    // Unlink the XML at the end of the test
    unlink(xml_file);
}


/*
<ns:SiteLists xmlns:ns="naSiteList" GlobalVersion="RZt0OwBL2YjWVQwlV0yaehiglqqusxai33IL+rSl7k4=" LocalVersion="00000000000000" Type="Client"><CaBundle><CaCertificate>MIIEWzCCAsOgAwIBAgIIRqerDl1bcvcwDQYJKoZIhvcNAQELBQAwRDEPMA0GA1UE
CgwGTWNBZmVlMQ4wDAYDVQQLDAVPcmlvbjEhMB8GA1UEAwwYT3Jpb25fQ0FfU2hy
ZWUtRklQUy1pcHY2MB4XDTcwMDEwMTAwMDAwMFoXDTQzMDMxMzA5NTUxNFowRDEP
MA0GA1UECgwGTWNBZmVlMQ4wDAYDVQQLDAVPcmlvbjEhMB8GA1UEAwwYT3Jpb25f
Q0FfU2hyZWUtRklQUy1pcHY2MIIBoDANBgkqhkiG9w0BAQEFAAOCAY0AMIIBiAKC
AYEAkrnNfJDxvlCovfryNb9QjTQ1yI+jd+mVhh82QBvmOlUYy/+tRlj90DNK7zPP
Gf6zUDarFwFWwE8/s00rwV/Lhd0XLBW8LS609bZdJKkbvte7TVB/WMP62ATV2VbA
FDxV8oNWXoTel+H5GQWTG+ynFO9zBa5JcLC3TYaqQpC47gXouWQg00H4a15ae87b
9kyPDRsvoiVHpZlPKxgtVJ/Kookc5LqsY59o6WjhBbz6mmcB/8pdHjp2vXegsiLh
2fAG1XJmNsoHgCU1dINEIGRVdezmytmJg9Cxh1YvRScPW67NuFWtS4SAhytSbsFl
+Lt5HFVzWLhlXwmPiiUyvIqGH0YwJuq9XdBEDKsFc2UjhuDxMObCF5EWVoZkTmjb
XNdWqFusFVnJyEx/qORl+DH9ZqY3POORUsKayWVaGBWeFEFSs3Fu55M54ub/lICg
9RsrqkyhbwOljfMMoI84DOL6G81WQbJGLOUDpwNZs+lZW5VOc5rLz47xzBRag/ya
m5P9AgEDo1MwUTAPBgNVHRMBAf8EBTADAQH/MB0GA1UdDgQWBBRSt+g3i14Ro3jc
QCtb40ebuVz5cDAfBgNVHSMEGDAWgBRSt+g3i14Ro3jcQCtb40ebuVz5cDANBgkq
hkiG9w0BAQsFAAOCAYEAehbnwMAgblnCKtDPyRjO7LUAHPErkK9zJ+bwdYUZGiCu
MBt77Dff2futFPK2T7fCJjunpfO3R0mm02Qv7DVXVOgPkNhPvPFa/35KaZ5gNJxN
8GoR+82BXtfQiebfi4cghuJNhhb1hwFpWP1PHjXp7ieEThOJYc6rG7ldOPwngewd
BA+BUe987Oq7wbYO/Ka9qix0WzVU3E9xtbCQ+4BlVX29ofQuTpe7Uu9l6Cr1OLug
R4Rw9FjSQA5TszB3IuuxXW6BLJICiClR0On64ZGuJCA4B2CVxvERjqcT3fWxlA2q
QFWjQAuRT9iplVvtIeC6LmNIiBIeDUgzapx6E2TVWj49OIOqbHT6kDKq8FDjC50g
gfwquUfBDzQm95reuHNNstQ4qmL1FO6cvGmpMciuuJZvhRAY23URLllTSDwpGq0A
d/0iWAXCXc+lm+h/AcdTIY5EqdiSkRPJz02dg5XFS5ee4UsSsrd9DMgqgUd/jXuD
KU1wrUEGB9yRi31KNWSu</CaCertificate><CaCertificate>MIIEVTCCAr2gAwIBAgIICAHuFBgjiB4wDQYJKoZIhvcNAQELBQAwRDEPMA0GA1UE
CgwGTWNBZmVlMQ4wDAYDVQQLDAVPcmlvbjEhMB8GA1UEAwwYT3Jpb25fQ0FfU2hy
ZWUtRklQUy1pcHY2MB4XDTcwMDEwMTAwMDAwMFoXDTQzMDMxMjA5NTc0NlowPjEP
MA0GA1UECgwGTWNBZmVlMQswCQYDVQQLDAJBSDEeMBwGA1UEAwwVQUhfQ0FfU2hy
ZWUtRklQUy1pcHY2MIIBoDANBgkqhkiG9w0BAQEFAAOCAY0AMIIBiAKCAYEAptgM
Bbd7WqsQgQJp1JQW70hTQ9FYrmfnXORFNYw5v2Y5Hf0e0PZcLn3ssL08ajTkJQl3
pWMQEh+S3EhqitLByT43U5v5f9+OqlnV41Qg4j4VYq0cbAf89jurjKM/3121dnNB
nB0FCCK0ZhELkZtk1wD7hjny3RJzZIprJtka1R/iCM/mvFUsImJmjyLTM+ma7GCl
dJsBhCqDGufPjWNIyRYDT+PP8R4q79BzfE7/tn9D7PdrQjOw0uqvOCTC1nzAudFF
+JBWKgaELpIqCvk8+JboHfXSjyMhxNN5TOLUuIWfkvo67f+nc8R4YAHVhfZqmPby
80w2ycf9LRDcA1Oig/KOeQ2vXr1NdjPNp4sYqoiJxa90Bv6DRW/9erTcjCWU/A45
+DIwUqwMBrohnCefsmdLro1FDjgr30pDm+dwjFhnonfzV7No2juRz3LzBUZwIkn8
0ShL3wXIgNju61Zp14DWzSrFo4l610LmzFOERqh4yBRp2eAbgWFKHgE68fnNAgED
o1MwUTAPBgNVHRMBAf8EBTADAQH/MB0GA1UdDgQWBBTyEQicczOTdySRfxe8cOOd
oAoBLDAfBgNVHSMEGDAWgBRSt+g3i14Ro3jcQCtb40ebuVz5cDANBgkqhkiG9w0B
AQsFAAOCAYEAQw49dmz1j02d9y6GWBQmaEhwyyeOzQ4olRliiwUgvJrx6nouypuy
d7UPpXlpvjGQdf60My0FNtpy8uzfkvQRqq7vQpy8yVChcWtyizjRs74iIGJwF/Md
x8ZVj3byQy65VkQmrRQUVN5GvLzTvHYBltgqAqpCjm8CqOvgWhLazUrmc69hIvUs
m+LeraP8lqzXhH/pONpx1tr5Hbn7+6XQ59uQweH2WU2bDWQPcEp1y7iwqUBm1gge
e1DRUfArWJMU6ltKCImvy1iquafKyZKRYksU/69oXMdEeHDJgGmEivummPljYE1F
cHkRYmpijTfOkVd9uvTD6I9my5GKaDU8pWM7rJsLrEDqieERH5XLMWV4QxQkbhqQ
m46JB9eAKnFFd0dbRIPOh7MAvQc9vMj5iBsKagY/NHURzUmW+ng4ZAZ4jMyRcTaA
0c53q2uCOQmGowUIsTyPVtNGUbPx75ymUemOVzb0++DJ4hgs3t6/2kO3zEjNeXOQ
dANDByf9i7zy</CaCertificate></CaBundle><Policies><Setting name="OverwriteClientSites">1</Setting><Setting name="nMaxHopLimit">15</Setting><Setting name="nMaxPingTimeout">30</Setting><Setting name="uiFindNearestMethod">0</Setting></Policies><SiteList Default="1" Name="SomeGUID"><HttpSite Type="fallback" Name="McAfeeHttp" Order="2" Enabled="1" Local="0" Server="update.nai.com:80"><RelativePath>Products/CommonUpdater</RelativePath><UseAuth>0</UseAuth><UserName></UserName><Password Encrypted="1">f2mwBTzPQdtnY6QNOsVexH9psAU8z0HbZ2OkDTrFXsR/abAFPM9B3Q==</Password></HttpSite><HttpSite Type="repository" Name="HTTP_REPO" Order="1" Enabled="0" Local="0" Server="172.16.220.129:80"><RelativePath></RelativePath><UseAuth>1</UseAuth><UserName>Rajeev-2008\administrator</UserName><Password Encrypted="1">sK7UJvxKwn8llvwzPf+WUX9psAU8z0HbZ2OkDTrFXsR/abAFPM9B3Q==</Password></HttpSite><SpipeSite Type="master" Name="ePO_SHREE-FIPS-IPV6" Order="1" Enabled="1" Local="0" SecurePort="443" Server="Shree-FIPS-ipv6:80" ServerName="SHREE-FIPS-IPV6:80" ServerIP="172.16.196.138:80" Version="5.0.0"><RelativePath>Software</RelativePath><UseAuth>0</UseAuth><UserName></UserName><Password Encrypted="1">f2mwBTzPQdtnY6QNOsVexH9psAU8z0HbZ2OkDTrFXsR/abAFPM9B3Q==</Password></SpipeSite></SiteList></ns:SiteLists>
*/

const unsigned char g_sitelist[] =
"\x3c\x6e\x73\x3a\x53\x69\x74\x65\x4c\x69\x73\x74\x73\x20\x78\x6d\x6c\x6e\x73\x3a\x6e\x73\x3d\x22\x6e\x61\
\x53\x69\x74\x65\x4c\x69\x73\x74\x22\x20\x47\x6c\x6f\x62\x61\x6c\x56\x65\x72\x73\x69\x6f\x6e\x3d\x22\x52\x5a\x74\x30\x4f\x77\x42\x4c\x32\x59\x6a\x57\x56\x51\x77\x6c\x56\x30\x79\x61\x65\x68\x69\x67\x6c\x71\x71\x75\x73\x78\x61\x69\x33\x33\x49\x4c\x2b\x72\x53\
\x6c\x37\x6b\x34\x3d\x22\x20\x4c\x6f\x63\x61\x6c\x56\x65\x72\x73\x69\x6f\x6e\x3d\x22\x30\x30\x30\x30\x30\x30\x30\x30\x30\x30\x30\x30\x30\x30\x22\x20\x54\x79\x70\x65\x3d\x22\x43\x6c\x69\x65\x6e\x74\x22\x3e\x3c\x43\x61\x42\x75\x6e\x64\x6c\x65\x3e\x3c\x43\x61\
\x43\x65\x72\x74\x69\x66\x69\x63\x61\x74\x65\x3e\x4d\x49\x49\x45\x57\x7a\x43\x43\x41\x73\x4f\x67\x41\x77\x49\x42\x41\x67\x49\x49\x52\x71\x65\x72\x44\x6c\x31\x62\x63\x76\x63\x77\x44\x51\x59\x4a\x4b\x6f\x5a\x49\x68\x76\x63\x4e\x41\x51\x45\x4c\x42\x51\x41\x77\
\x52\x44\x45\x50\x4d\x41\x30\x47\x41\x31\x55\x45\x0a\x43\x67\x77\x47\x54\x57\x4e\x42\x5a\x6d\x56\x6c\x4d\x51\x34\x77\x44\x41\x59\x44\x56\x51\x51\x4c\x44\x41\x56\x50\x63\x6d\x6c\x76\x62\x6a\x45\x68\x4d\x42\x38\x47\x41\x31\x55\x45\x41\x77\x77\x59\x54\x33\x4a\
\x70\x62\x32\x35\x66\x51\x30\x46\x66\x55\x32\x68\x79\x0a\x5a\x57\x55\x74\x52\x6b\x6c\x51\x55\x79\x31\x70\x63\x48\x59\x32\x4d\x42\x34\x58\x44\x54\x63\x77\x4d\x44\x45\x77\x4d\x54\x41\x77\x4d\x44\x41\x77\x4d\x46\x6f\x58\x44\x54\x51\x7a\x4d\x44\x4d\x78\x4d\x7a\
\x41\x35\x4e\x54\x55\x78\x4e\x46\x6f\x77\x52\x44\x45\x50\x0a\x4d\x41\x30\x47\x41\x31\x55\x45\x43\x67\x77\x47\x54\x57\x4e\x42\x5a\x6d\x56\x6c\x4d\x51\x34\x77\x44\x41\x59\x44\x56\x51\x51\x4c\x44\x41\x56\x50\x63\x6d\x6c\x76\x62\x6a\x45\x68\x4d\x42\x38\x47\x41\
\x31\x55\x45\x41\x77\x77\x59\x54\x33\x4a\x70\x62\x32\x35\x66\x0a\x51\x30\x46\x66\x55\x32\x68\x79\x5a\x57\x55\x74\x52\x6b\x6c\x51\x55\x79\x31\x70\x63\x48\x59\x32\x4d\x49\x49\x42\x6f\x44\x41\x4e\x42\x67\x6b\x71\x68\x6b\x69\x47\x39\x77\x30\x42\x41\x51\x45\x46\
\x41\x41\x4f\x43\x41\x59\x30\x41\x4d\x49\x49\x42\x69\x41\x4b\x43\x0a\x41\x59\x45\x41\x6b\x72\x6e\x4e\x66\x4a\x44\x78\x76\x6c\x43\x6f\x76\x66\x72\x79\x4e\x62\x39\x51\x6a\x54\x51\x31\x79\x49\x2b\x6a\x64\x2b\x6d\x56\x68\x68\x38\x32\x51\x42\x76\x6d\x4f\x6c\x55\
\x59\x79\x2f\x2b\x74\x52\x6c\x6a\x39\x30\x44\x4e\x4b\x37\x7a\x50\x50\x0a\x47\x66\x36\x7a\x55\x44\x61\x72\x46\x77\x46\x57\x77\x45\x38\x2f\x73\x30\x30\x72\x77\x56\x2f\x4c\x68\x64\x30\x58\x4c\x42\x57\x38\x4c\x53\x36\x30\x39\x62\x5a\x64\x4a\x4b\x6b\x62\x76\x74\
\x65\x37\x54\x56\x42\x2f\x57\x4d\x50\x36\x32\x41\x54\x56\x32\x56\x62\x41\x0a\x46\x44\x78\x56\x38\x6f\x4e\x57\x58\x6f\x54\x65\x6c\x2b\x48\x35\x47\x51\x57\x54\x47\x2b\x79\x6e\x46\x4f\x39\x7a\x42\x61\x35\x4a\x63\x4c\x43\x33\x54\x59\x61\x71\x51\x70\x43\x34\x37\
\x67\x58\x6f\x75\x57\x51\x67\x30\x30\x48\x34\x61\x31\x35\x61\x65\x38\x37\x62\x0a\x39\x6b\x79\x50\x44\x52\x73\x76\x6f\x69\x56\x48\x70\x5a\x6c\x50\x4b\x78\x67\x74\x56\x4a\x2f\x4b\x6f\x6f\x6b\x63\x35\x4c\x71\x73\x59\x35\x39\x6f\x36\x57\x6a\x68\x42\x62\x7a\x36\
\x6d\x6d\x63\x42\x2f\x38\x70\x64\x48\x6a\x70\x32\x76\x58\x65\x67\x73\x69\x4c\x68\x0a\x32\x66\x41\x47\x31\x58\x4a\x6d\x4e\x73\x6f\x48\x67\x43\x55\x31\x64\x49\x4e\x45\x49\x47\x52\x56\x64\x65\x7a\x6d\x79\x74\x6d\x4a\x67\x39\x43\x78\x68\x31\x59\x76\x52\x53\x63\
\x50\x57\x36\x37\x4e\x75\x46\x57\x74\x53\x34\x53\x41\x68\x79\x74\x53\x62\x73\x46\x6c\x0a\x2b\x4c\x74\x35\x48\x46\x56\x7a\x57\x4c\x68\x6c\x58\x77\x6d\x50\x69\x69\x55\x79\x76\x49\x71\x47\x48\x30\x59\x77\x4a\x75\x71\x39\x58\x64\x42\x45\x44\x4b\x73\x46\x63\x32\
\x55\x6a\x68\x75\x44\x78\x4d\x4f\x62\x43\x46\x35\x45\x57\x56\x6f\x5a\x6b\x54\x6d\x6a\x62\x0a\x58\x4e\x64\x57\x71\x46\x75\x73\x46\x56\x6e\x4a\x79\x45\x78\x2f\x71\x4f\x52\x6c\x2b\x44\x48\x39\x5a\x71\x59\x33\x50\x4f\x4f\x52\x55\x73\x4b\x61\x79\x57\x56\x61\x47\
\x42\x57\x65\x46\x45\x46\x53\x73\x33\x46\x75\x35\x35\x4d\x35\x34\x75\x62\x2f\x6c\x49\x43\x67\x0a\x39\x52\x73\x72\x71\x6b\x79\x68\x62\x77\x4f\x6c\x6a\x66\x4d\x4d\x6f\x49\x38\x34\x44\x4f\x4c\x36\x47\x38\x31\x57\x51\x62\x4a\x47\x4c\x4f\x55\x44\x70\x77\x4e\x5a\
\x73\x2b\x6c\x5a\x57\x35\x56\x4f\x63\x35\x72\x4c\x7a\x34\x37\x78\x7a\x42\x52\x61\x67\x2f\x79\x61\x0a\x6d\x35\x50\x39\x41\x67\x45\x44\x6f\x31\x4d\x77\x55\x54\x41\x50\x42\x67\x4e\x56\x48\x52\x4d\x42\x41\x66\x38\x45\x42\x54\x41\x44\x41\x51\x48\x2f\x4d\x42\x30\
\x47\x41\x31\x55\x64\x44\x67\x51\x57\x42\x42\x52\x53\x74\x2b\x67\x33\x69\x31\x34\x52\x6f\x33\x6a\x63\x0a\x51\x43\x74\x62\x34\x30\x65\x62\x75\x56\x7a\x35\x63\x44\x41\x66\x42\x67\x4e\x56\x48\x53\x4d\x45\x47\x44\x41\x57\x67\x42\x52\x53\x74\x2b\x67\x33\x69\x31\
\x34\x52\x6f\x33\x6a\x63\x51\x43\x74\x62\x34\x30\x65\x62\x75\x56\x7a\x35\x63\x44\x41\x4e\x42\x67\x6b\x71\x0a\x68\x6b\x69\x47\x39\x77\x30\x42\x41\x51\x73\x46\x41\x41\x4f\x43\x41\x59\x45\x41\x65\x68\x62\x6e\x77\x4d\x41\x67\x62\x6c\x6e\x43\x4b\x74\x44\x50\x79\
\x52\x6a\x4f\x37\x4c\x55\x41\x48\x50\x45\x72\x6b\x4b\x39\x7a\x4a\x2b\x62\x77\x64\x59\x55\x5a\x47\x69\x43\x75\x0a\x4d\x42\x74\x37\x37\x44\x66\x66\x32\x66\x75\x74\x46\x50\x4b\x32\x54\x37\x66\x43\x4a\x6a\x75\x6e\x70\x66\x4f\x33\x52\x30\x6d\x6d\x30\x32\x51\x76\
\x37\x44\x56\x58\x56\x4f\x67\x50\x6b\x4e\x68\x50\x76\x50\x46\x61\x2f\x33\x35\x4b\x61\x5a\x35\x67\x4e\x4a\x78\x4e\x0a\x38\x47\x6f\x52\x2b\x38\x32\x42\x58\x74\x66\x51\x69\x65\x62\x66\x69\x34\x63\x67\x68\x75\x4a\x4e\x68\x68\x62\x31\x68\x77\x46\x70\x57\x50\x31\
\x50\x48\x6a\x58\x70\x37\x69\x65\x45\x54\x68\x4f\x4a\x59\x63\x36\x72\x47\x37\x6c\x64\x4f\x50\x77\x6e\x67\x65\x77\x64\x0a\x42\x41\x2b\x42\x55\x65\x39\x38\x37\x4f\x71\x37\x77\x62\x59\x4f\x2f\x4b\x61\x39\x71\x69\x78\x30\x57\x7a\x56\x55\x33\x45\x39\x78\x74\x62\
\x43\x51\x2b\x34\x42\x6c\x56\x58\x32\x39\x6f\x66\x51\x75\x54\x70\x65\x37\x55\x75\x39\x6c\x36\x43\x72\x31\x4f\x4c\x75\x67\x0a\x52\x34\x52\x77\x39\x46\x6a\x53\x51\x41\x35\x54\x73\x7a\x42\x33\x49\x75\x75\x78\x58\x57\x36\x42\x4c\x4a\x49\x43\x69\x43\x6c\x52\x30\
\x4f\x6e\x36\x34\x5a\x47\x75\x4a\x43\x41\x34\x42\x32\x43\x56\x78\x76\x45\x52\x6a\x71\x63\x54\x33\x66\x57\x78\x6c\x41\x32\x71\x0a\x51\x46\x57\x6a\x51\x41\x75\x52\x54\x39\x69\x70\x6c\x56\x76\x74\x49\x65\x43\x36\x4c\x6d\x4e\x49\x69\x42\x49\x65\x44\x55\x67\x7a\
\x61\x70\x78\x36\x45\x32\x54\x56\x57\x6a\x34\x39\x4f\x49\x4f\x71\x62\x48\x54\x36\x6b\x44\x4b\x71\x38\x46\x44\x6a\x43\x35\x30\x67\x0a\x67\x66\x77\x71\x75\x55\x66\x42\x44\x7a\x51\x6d\x39\x35\x72\x65\x75\x48\x4e\x4e\x73\x74\x51\x34\x71\x6d\x4c\x31\x46\x4f\x36\
\x63\x76\x47\x6d\x70\x4d\x63\x69\x75\x75\x4a\x5a\x76\x68\x52\x41\x59\x32\x33\x55\x52\x4c\x6c\x6c\x54\x53\x44\x77\x70\x47\x71\x30\x41\x0a\x64\x2f\x30\x69\x57\x41\x58\x43\x58\x63\x2b\x6c\x6d\x2b\x68\x2f\x41\x63\x64\x54\x49\x59\x35\x45\x71\x64\x69\x53\x6b\x52\
\x50\x4a\x7a\x30\x32\x64\x67\x35\x58\x46\x53\x35\x65\x65\x34\x55\x73\x53\x73\x72\x64\x39\x44\x4d\x67\x71\x67\x55\x64\x2f\x6a\x58\x75\x44\x0a\x4b\x55\x31\x77\x72\x55\x45\x47\x42\x39\x79\x52\x69\x33\x31\x4b\x4e\x57\x53\x75\x3c\x2f\x43\x61\x43\x65\x72\x74\x69\
\x66\x69\x63\x61\x74\x65\x3e\x3c\x43\x61\x43\x65\x72\x74\x69\x66\x69\x63\x61\x74\x65\x3e\x4d\x49\x49\x45\x56\x54\x43\x43\x41\x72\x32\x67\x41\x77\x49\x42\x41\x67\x49\x49\x43\x41\x48\x75\x46\x42\x67\x6a\x69\x42\x34\x77\x44\x51\x59\x4a\x4b\x6f\x5a\x49\x68\x76\
\x63\x4e\x41\x51\x45\x4c\x42\x51\x41\x77\x52\x44\x45\x50\x4d\x41\x30\x47\x41\x31\x55\x45\x0a\x43\x67\x77\x47\x54\x57\x4e\x42\x5a\x6d\x56\x6c\x4d\x51\x34\x77\x44\x41\x59\x44\x56\x51\x51\x4c\x44\x41\x56\x50\x63\x6d\x6c\x76\x62\x6a\x45\x68\x4d\x42\x38\x47\x41\
\x31\x55\x45\x41\x77\x77\x59\x54\x33\x4a\x70\x62\x32\x35\x66\x51\x30\x46\x66\x55\x32\x68\x79\x0a\x5a\x57\x55\x74\x52\x6b\x6c\x51\x55\x79\x31\x70\x63\x48\x59\x32\x4d\x42\x34\x58\x44\x54\x63\x77\x4d\x44\x45\x77\x4d\x54\x41\x77\x4d\x44\x41\x77\x4d\x46\x6f\x58\
\x44\x54\x51\x7a\x4d\x44\x4d\x78\x4d\x6a\x41\x35\x4e\x54\x63\x30\x4e\x6c\x6f\x77\x50\x6a\x45\x50\x0a\x4d\x41\x30\x47\x41\x31\x55\x45\x43\x67\x77\x47\x54\x57\x4e\x42\x5a\x6d\x56\x6c\x4d\x51\x73\x77\x43\x51\x59\x44\x56\x51\x51\x4c\x44\x41\x4a\x42\x53\x44\x45\
\x65\x4d\x42\x77\x47\x41\x31\x55\x45\x41\x77\x77\x56\x51\x55\x68\x66\x51\x30\x46\x66\x55\x32\x68\x79\x0a\x5a\x57\x55\x74\x52\x6b\x6c\x51\x55\x79\x31\x70\x63\x48\x59\x32\x4d\x49\x49\x42\x6f\x44\x41\x4e\x42\x67\x6b\x71\x68\x6b\x69\x47\x39\x77\x30\x42\x41\x51\
\x45\x46\x41\x41\x4f\x43\x41\x59\x30\x41\x4d\x49\x49\x42\x69\x41\x4b\x43\x41\x59\x45\x41\x70\x74\x67\x4d\x0a\x42\x62\x64\x37\x57\x71\x73\x51\x67\x51\x4a\x70\x31\x4a\x51\x57\x37\x30\x68\x54\x51\x39\x46\x59\x72\x6d\x66\x6e\x58\x4f\x52\x46\x4e\x59\x77\x35\x76\
\x32\x59\x35\x48\x66\x30\x65\x30\x50\x5a\x63\x4c\x6e\x33\x73\x73\x4c\x30\x38\x61\x6a\x54\x6b\x4a\x51\x6c\x33\x0a\x70\x57\x4d\x51\x45\x68\x2b\x53\x33\x45\x68\x71\x69\x74\x4c\x42\x79\x54\x34\x33\x55\x35\x76\x35\x66\x39\x2b\x4f\x71\x6c\x6e\x56\x34\x31\x51\x67\
\x34\x6a\x34\x56\x59\x71\x30\x63\x62\x41\x66\x38\x39\x6a\x75\x72\x6a\x4b\x4d\x2f\x33\x31\x32\x31\x64\x6e\x4e\x42\x0a\x6e\x42\x30\x46\x43\x43\x4b\x30\x5a\x68\x45\x4c\x6b\x5a\x74\x6b\x31\x77\x44\x37\x68\x6a\x6e\x79\x33\x52\x4a\x7a\x5a\x49\x70\x72\x4a\x74\x6b\
\x61\x31\x52\x2f\x69\x43\x4d\x2f\x6d\x76\x46\x55\x73\x49\x6d\x4a\x6d\x6a\x79\x4c\x54\x4d\x2b\x6d\x61\x37\x47\x43\x6c\x0a\x64\x4a\x73\x42\x68\x43\x71\x44\x47\x75\x66\x50\x6a\x57\x4e\x49\x79\x52\x59\x44\x54\x2b\x50\x50\x38\x52\x34\x71\x37\x39\x42\x7a\x66\x45\
\x37\x2f\x74\x6e\x39\x44\x37\x50\x64\x72\x51\x6a\x4f\x77\x30\x75\x71\x76\x4f\x43\x54\x43\x31\x6e\x7a\x41\x75\x64\x46\x46\x0a\x2b\x4a\x42\x57\x4b\x67\x61\x45\x4c\x70\x49\x71\x43\x76\x6b\x38\x2b\x4a\x62\x6f\x48\x66\x58\x53\x6a\x79\x4d\x68\x78\x4e\x4e\x35\x54\
\x4f\x4c\x55\x75\x49\x57\x66\x6b\x76\x6f\x36\x37\x66\x2b\x6e\x63\x38\x52\x34\x59\x41\x48\x56\x68\x66\x5a\x71\x6d\x50\x62\x79\x0a\x38\x30\x77\x32\x79\x63\x66\x39\x4c\x52\x44\x63\x41\x31\x4f\x69\x67\x2f\x4b\x4f\x65\x51\x32\x76\x58\x72\x31\x4e\x64\x6a\x50\x4e\
\x70\x34\x73\x59\x71\x6f\x69\x4a\x78\x61\x39\x30\x42\x76\x36\x44\x52\x57\x2f\x39\x65\x72\x54\x63\x6a\x43\x57\x55\x2f\x41\x34\x35\x0a\x2b\x44\x49\x77\x55\x71\x77\x4d\x42\x72\x6f\x68\x6e\x43\x65\x66\x73\x6d\x64\x4c\x72\x6f\x31\x46\x44\x6a\x67\x72\x33\x30\x70\
\x44\x6d\x2b\x64\x77\x6a\x46\x68\x6e\x6f\x6e\x66\x7a\x56\x37\x4e\x6f\x32\x6a\x75\x52\x7a\x33\x4c\x7a\x42\x55\x5a\x77\x49\x6b\x6e\x38\x0a\x30\x53\x68\x4c\x33\x77\x58\x49\x67\x4e\x6a\x75\x36\x31\x5a\x70\x31\x34\x44\x57\x7a\x53\x72\x46\x6f\x34\x6c\x36\x31\x30\
\x4c\x6d\x7a\x46\x4f\x45\x52\x71\x68\x34\x79\x42\x52\x70\x32\x65\x41\x62\x67\x57\x46\x4b\x48\x67\x45\x36\x38\x66\x6e\x4e\x41\x67\x45\x44\x0a\x6f\x31\x4d\x77\x55\x54\x41\x50\x42\x67\x4e\x56\x48\x52\x4d\x42\x41\x66\x38\x45\x42\x54\x41\x44\x41\x51\x48\x2f\x4d\
\x42\x30\x47\x41\x31\x55\x64\x44\x67\x51\x57\x42\x42\x54\x79\x45\x51\x69\x63\x63\x7a\x4f\x54\x64\x79\x53\x52\x66\x78\x65\x38\x63\x4f\x4f\x64\x0a\x6f\x41\x6f\x42\x4c\x44\x41\x66\x42\x67\x4e\x56\x48\x53\x4d\x45\x47\x44\x41\x57\x67\x42\x52\x53\x74\x2b\x67\x33\
\x69\x31\x34\x52\x6f\x33\x6a\x63\x51\x43\x74\x62\x34\x30\x65\x62\x75\x56\x7a\x35\x63\x44\x41\x4e\x42\x67\x6b\x71\x68\x6b\x69\x47\x39\x77\x30\x42\x0a\x41\x51\x73\x46\x41\x41\x4f\x43\x41\x59\x45\x41\x51\x77\x34\x39\x64\x6d\x7a\x31\x6a\x30\x32\x64\x39\x79\x36\
\x47\x57\x42\x51\x6d\x61\x45\x68\x77\x79\x79\x65\x4f\x7a\x51\x34\x6f\x6c\x52\x6c\x69\x69\x77\x55\x67\x76\x4a\x72\x78\x36\x6e\x6f\x75\x79\x70\x75\x79\x0a\x64\x37\x55\x50\x70\x58\x6c\x70\x76\x6a\x47\x51\x64\x66\x36\x30\x4d\x79\x30\x46\x4e\x74\x70\x79\x38\x75\
\x7a\x66\x6b\x76\x51\x52\x71\x71\x37\x76\x51\x70\x79\x38\x79\x56\x43\x68\x63\x57\x74\x79\x69\x7a\x6a\x52\x73\x37\x34\x69\x49\x47\x4a\x77\x46\x2f\x4d\x64\x0a\x78\x38\x5a\x56\x6a\x33\x62\x79\x51\x79\x36\x35\x56\x6b\x51\x6d\x72\x52\x51\x55\x56\x4e\x35\x47\x76\
\x4c\x7a\x54\x76\x48\x59\x42\x6c\x74\x67\x71\x41\x71\x70\x43\x6a\x6d\x38\x43\x71\x4f\x76\x67\x57\x68\x4c\x61\x7a\x55\x72\x6d\x63\x36\x39\x68\x49\x76\x55\x73\x0a\x6d\x2b\x4c\x65\x72\x61\x50\x38\x6c\x71\x7a\x58\x68\x48\x2f\x70\x4f\x4e\x70\x78\x31\x74\x72\x35\
\x48\x62\x6e\x37\x2b\x36\x58\x51\x35\x39\x75\x51\x77\x65\x48\x32\x57\x55\x32\x62\x44\x57\x51\x50\x63\x45\x70\x31\x79\x37\x69\x77\x71\x55\x42\x6d\x31\x67\x67\x65\x0a\x65\x31\x44\x52\x55\x66\x41\x72\x57\x4a\x4d\x55\x36\x6c\x74\x4b\x43\x49\x6d\x76\x79\x31\x69\
\x71\x75\x61\x66\x4b\x79\x5a\x4b\x52\x59\x6b\x73\x55\x2f\x36\x39\x6f\x58\x4d\x64\x45\x65\x48\x44\x4a\x67\x47\x6d\x45\x69\x76\x75\x6d\x6d\x50\x6c\x6a\x59\x45\x31\x46\x0a\x63\x48\x6b\x52\x59\x6d\x70\x69\x6a\x54\x66\x4f\x6b\x56\x64\x39\x75\x76\x54\x44\x36\x49\
\x39\x6d\x79\x35\x47\x4b\x61\x44\x55\x38\x70\x57\x4d\x37\x72\x4a\x73\x4c\x72\x45\x44\x71\x69\x65\x45\x52\x48\x35\x58\x4c\x4d\x57\x56\x34\x51\x78\x51\x6b\x62\x68\x71\x51\x0a\x6d\x34\x36\x4a\x42\x39\x65\x41\x4b\x6e\x46\x46\x64\x30\x64\x62\x52\x49\x50\x4f\x68\
\x37\x4d\x41\x76\x51\x63\x39\x76\x4d\x6a\x35\x69\x42\x73\x4b\x61\x67\x59\x2f\x4e\x48\x55\x52\x7a\x55\x6d\x57\x2b\x6e\x67\x34\x5a\x41\x5a\x34\x6a\x4d\x79\x52\x63\x54\x61\x41\x0a\x30\x63\x35\x33\x71\x32\x75\x43\x4f\x51\x6d\x47\x6f\x77\x55\x49\x73\x54\x79\x50\
\x56\x74\x4e\x47\x55\x62\x50\x78\x37\x35\x79\x6d\x55\x65\x6d\x4f\x56\x7a\x62\x30\x2b\x2b\x44\x4a\x34\x68\x67\x73\x33\x74\x36\x2f\x32\x6b\x4f\x33\x7a\x45\x6a\x4e\x65\x58\x4f\x51\x0a\x64\x41\x4e\x44\x42\x79\x66\x39\x69\x37\x7a\x79\x3c\x2f\x43\x61\x43\x65\x72\
\x74\x69\x66\x69\x63\x61\x74\x65\x3e\x3c\x2f\x43\x61\x42\x75\x6e\x64\x6c\x65\x3e\x3c\x50\x6f\x6c\x69\x63\x69\x65\x73\x3e\x3c\x53\x65\x74\x74\x69\x6e\x67\x20\x6e\x61\x6d\x65\x3d\x22\x4f\x76\x65\x72\x77\x72\x69\x74\x65\x43\x6c\x69\x65\x6e\x74\x53\x69\x74\x65\
\x73\x22\x3e\x31\x3c\x2f\x53\x65\x74\x74\x69\x6e\x67\x3e\x3c\x53\x65\x74\x74\x69\x6e\x67\x20\x6e\x61\x6d\x65\x3d\x22\x6e\x4d\x61\x78\x48\x6f\x70\x4c\x69\x6d\x69\x74\x22\x3e\x31\x35\x3c\x2f\x53\x65\x74\x74\x69\x6e\x67\x3e\x3c\x53\x65\x74\x74\x69\x6e\x67\x20\
\x6e\x61\x6d\x65\x3d\x22\x6e\x4d\x61\x78\x50\x69\x6e\x67\x54\x69\x6d\x65\x6f\x75\x74\x22\x3e\x33\x30\x3c\x2f\x53\x65\x74\x74\x69\x6e\x67\x3e\x3c\x53\x65\x74\x74\x69\x6e\x67\x20\x6e\x61\x6d\x65\x3d\x22\x75\x69\x46\x69\x6e\x64\x4e\x65\x61\x72\x65\x73\x74\x4d\
\x65\x74\x68\x6f\x64\x22\x3e\x30\x3c\x2f\x53\x65\x74\x74\x69\x6e\x67\x3e\x3c\x2f\x50\x6f\x6c\x69\x63\x69\x65\x73\x3e\x3c\x53\x69\x74\x65\x4c\x69\x73\x74\x20\x44\x65\x66\x61\x75\x6c\x74\x3d\x22\x31\x22\x20\x4e\x61\x6d\x65\x3d\x22\x53\x6f\x6d\x65\x47\x55\x49\
\x44\x22\x3e\x3c\x48\x74\x74\x70\x53\x69\x74\x65\x20\x54\x79\x70\x65\x3d\x22\x66\x61\x6c\x6c\x62\x61\x63\x6b\x22\x20\x4e\x61\x6d\x65\x3d\x22\x4d\x63\x41\x66\x65\x65\x48\x74\x74\x70\x22\x20\x4f\x72\x64\x65\x72\x3d\x22\x32\x22\x20\x45\x6e\x61\x62\x6c\x65\x64\
\x3d\x22\x31\x22\x20\x4c\x6f\x63\x61\x6c\x3d\x22\x30\x22\x20\x53\x65\x72\x76\x65\x72\x3d\x22\x75\x70\x64\x61\x74\x65\x2e\x6e\x61\x69\x2e\x63\x6f\x6d\x3a\x38\x30\x22\x3e\x3c\x52\x65\x6c\x61\x74\x69\x76\x65\x50\x61\x74\x68\x3e\x50\x72\x6f\x64\x75\x63\x74\x73\
\x2f\x43\x6f\x6d\x6d\x6f\x6e\x55\x70\x64\x61\x74\x65\x72\x3c\x2f\x52\x65\x6c\x61\x74\x69\x76\x65\x50\x61\x74\x68\x3e\x3c\x55\x73\x65\x41\x75\x74\x68\x3e\x30\x3c\x2f\x55\x73\x65\x41\x75\x74\x68\x3e\x3c\x55\x73\x65\x72\x4e\x61\x6d\x65\x3e\x3c\x2f\x55\x73\x65\
\x72\x4e\x61\x6d\x65\x3e\x3c\x50\x61\x73\x73\x77\x6f\x72\x64\x20\x45\x6e\x63\x72\x79\x70\x74\x65\x64\x3d\x22\x31\x22\x3e\x66\x32\x6d\x77\x42\x54\x7a\x50\x51\x64\x74\x6e\x59\x36\x51\x4e\x4f\x73\x56\x65\x78\x48\x39\x70\x73\x41\x55\x38\x7a\x30\x48\x62\x5a\x32\
\x4f\x6b\x44\x54\x72\x46\x58\x73\x52\x2f\x61\x62\x41\x46\x50\x4d\x39\x42\x33\x51\x3d\x3d\x3c\x2f\x50\x61\x73\x73\x77\x6f\x72\x64\x3e\x3c\x2f\x48\x74\x74\x70\x53\x69\x74\x65\x3e\x3c\x48\x74\x74\x70\x53\x69\x74\x65\x20\x54\x79\x70\x65\x3d\x22\x72\x65\x70\x6f\
\x73\x69\x74\x6f\x72\x79\x22\x20\x4e\x61\x6d\x65\x3d\x22\x48\x54\x54\x50\x5f\x52\x45\x50\x4f\x22\x20\x4f\x72\x64\x65\x72\x3d\x22\x31\x22\x20\x45\x6e\x61\x62\x6c\x65\x64\x3d\x22\x30\x22\x20\x4c\x6f\x63\x61\x6c\x3d\x22\x30\x22\x20\x53\x65\x72\x76\x65\x72\x3d\
\x22\x31\x37\x32\x2e\x31\x36\x2e\x32\x32\x30\x2e\x31\x32\x39\x3a\x38\x30\x22\x3e\x3c\x52\x65\x6c\x61\x74\x69\x76\x65\x50\x61\x74\x68\x3e\x3c\x2f\x52\x65\x6c\x61\x74\x69\x76\x65\x50\x61\x74\x68\x3e\x3c\x55\x73\x65\x41\x75\x74\x68\x3e\x31\x3c\x2f\x55\x73\x65\
\x41\x75\x74\x68\x3e\x3c\x55\x73\x65\x72\x4e\x61\x6d\x65\x3e\x52\x61\x6a\x65\x65\x76\x2d\x32\x30\x30\x38\x5c\x61\x64\x6d\x69\x6e\x69\x73\x74\x72\x61\x74\x6f\x72\x3c\x2f\x55\x73\x65\x72\x4e\x61\x6d\x65\x3e\x3c\x50\x61\x73\x73\x77\x6f\x72\x64\x20\x45\x6e\x63\
\x72\x79\x70\x74\x65\x64\x3d\x22\x31\x22\x3e\x73\x4b\x37\x55\x4a\x76\x78\x4b\x77\x6e\x38\x6c\x6c\x76\x77\x7a\x50\x66\x2b\x57\x55\x58\x39\x70\x73\x41\x55\x38\x7a\x30\x48\x62\x5a\x32\x4f\x6b\x44\x54\x72\x46\x58\x73\x52\x2f\x61\x62\x41\x46\x50\x4d\x39\x42\x33\
\x51\x3d\x3d\x3c\x2f\x50\x61\x73\x73\x77\x6f\x72\x64\x3e\x3c\x2f\x48\x74\x74\x70\x53\x69\x74\x65\x3e\x3c\x53\x70\x69\x70\x65\x53\x69\x74\x65\x20\x54\x79\x70\x65\x3d\x22\x6d\x61\x73\x74\x65\x72\x22\x20\x4e\x61\x6d\x65\x3d\x22\x65\x50\x4f\x5f\x53\x48\x52\x45\
\x45\x2d\x46\x49\x50\x53\x2d\x49\x50\x56\x36\x22\x20\x4f\x72\x64\x65\x72\x3d\x22\x31\x22\x20\x45\x6e\x61\x62\x6c\x65\x64\x3d\x22\x31\x22\x20\x4c\x6f\x63\x61\x6c\x3d\x22\x30\x22\x20\x53\x65\x63\x75\x72\x65\x50\x6f\x72\x74\x3d\x22\x34\x34\x33\x22\x20\x53\x65\
\x72\x76\x65\x72\x3d\x22\x53\x68\x72\x65\x65\x2d\x46\x49\x50\x53\x2d\x69\x70\x76\x36\x3a\x38\x30\x22\x20\x53\x65\x72\x76\x65\x72\x4e\x61\x6d\x65\x3d\x22\x53\x48\x52\x45\x45\x2d\x46\x49\x50\x53\x2d\x49\x50\x56\x36\x3a\x38\x30\x22\x20\x53\x65\x72\x76\x65\x72\
\x49\x50\x3d\x22\x31\x37\x32\x2e\x31\x36\x2e\x31\x39\x36\x2e\x31\x33\x38\x3a\x38\x30\x22\x20\x56\x65\x72\x73\x69\x6f\x6e\x3d\x22\x35\x2e\x30\x2e\x30\x22\x3e\x3c\x52\x65\x6c\x61\x74\x69\x76\x65\x50\x61\x74\x68\x3e\x53\x6f\x66\x74\x77\x61\x72\x65\x3c\x2f\x52\
\x65\x6c\x61\x74\x69\x76\x65\x50\x61\x74\x68\x3e\x3c\x55\x73\x65\x41\x75\x74\x68\x3e\x30\x3c\x2f\x55\x73\x65\x41\x75\x74\x68\x3e\x3c\x55\x73\x65\x72\x4e\x61\x6d\x65\x3e\x3c\x2f\x55\x73\x65\x72\x4e\x61\x6d\x65\x3e\x3c\x50\x61\x73\x73\x77\x6f\x72\x64\x20\x45\
\x6e\x63\x72\x79\x70\x74\x65\x64\x3d\x22\x31\x22\x3e\x66\x32\x6d\x77\x42\x54\x7a\x50\x51\x64\x74\x6e\x59\x36\x51\x4e\x4f\x73\x56\x65\x78\x48\x39\x70\x73\x41\x55\x38\x7a\x30\x48\x62\x5a\x32\x4f\x6b\x44\x54\x72\x46\x58\x73\x52\x2f\x61\x62\x41\x46\x50\x4d\x39\
\x42\x33\x51\x3d\x3d\x3c\x2f\x50\x61\x73\x73\x77\x6f\x72\x64\x3e\x3c\x2f\x53\x70\x69\x70\x65\x53\x69\x74\x65\x3e\x3c\x2f\x53\x69\x74\x65\x4c\x69\x73\x74\x3e\x3c\x2f\x6e\x73\x3a\x53\x69\x74\x65\x4c\x69\x73\x74\x73\x3e";

TEST(ma_ds_xml_tests_group, verify_sitelist_xml_api)
{
    ma_xml_t *xml = NULL;
    ma_ds_xml_t *ds_xml = NULL;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_xml_create(&xml));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_xml_load_from_buffer(xml, (const char*)g_sitelist, sizeof(g_sitelist)));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_xml_save_to_file(xml, xml_file));

    TEST_ASSERT_MESSAGE(MA_OK == ma_ds_xml_open(xml_file, &ds_xml), "ma_ds_xml_open failed");
    g_ds = (ma_ds_t*)ds_xml;

    // Iterating the element hierarchy. Verifying that the DS iterator correctly parses the element hierarchy.
    {
        ma_buffer_t *buffer = NULL;
        const char *string_val = NULL;
        size_t string_size = 0;

        ma_ds_iterator_t *iterator = NULL;

        // Creating iterator for top-level "ns:SiteLists"
        ma_ds_iterator_create(g_ds, 1, "ns:SiteLists", &iterator);

        ma_ds_iterator_get_next(iterator, &buffer);
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("CaBundle", string_val, strlen("CaBundle"), "XML Element hierarchy not parsed correctly");
        ma_buffer_release(buffer);

        ma_ds_iterator_get_next(iterator, &buffer);
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("Policies", string_val, strlen("Policies"), "XML Element hierarchy not parsed correctly");
        ma_buffer_release(buffer);

        ma_ds_iterator_get_next(iterator, &buffer);
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("SiteList", string_val, strlen("SiteList"), "XML Element hierarchy not parsed correctly");
        ma_buffer_release(buffer);

        // No more items in top-level "ns:SiteLists"
        TEST_ASSERT_MESSAGE(MA_ERROR_NO_MORE_ITEMS == ma_ds_iterator_get_next(iterator, &buffer), "XML Element hierarchy not parsed correctly");
        ma_ds_iterator_release(iterator);

        // Creating iterator for "ns:SiteLists\\CaBundle"
        ma_ds_iterator_create(g_ds, 1, "ns:SiteLists\\CaBundle", &iterator);

        ma_ds_iterator_get_next(iterator, &buffer);
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("CaCertificate", string_val, strlen("CaCertificate"), "XML Element hierarchy not parsed correctly");
        ma_buffer_release(buffer);

        ma_ds_iterator_get_next(iterator, &buffer);
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("CaCertificate", string_val, strlen("CaCertificate"), "XML Element hierarchy not parsed correctly");
        ma_buffer_release(buffer);

        // No more items in "ns:SiteLists\\CaBundle"
        TEST_ASSERT_MESSAGE(MA_ERROR_NO_MORE_ITEMS == ma_ds_iterator_get_next(iterator, &buffer), "XML Element hierarchy not parsed correctly");
        ma_ds_iterator_release(iterator);

        // Creating iterator for "ns:SiteLists\\SiteList"
        ma_ds_iterator_create(g_ds, 1, "ns:SiteLists\\SiteList", &iterator);

        ma_ds_iterator_get_next(iterator, &buffer);
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("HttpSite", string_val, strlen("HttpSite"), "XML Element hierarchy not parsed correctly");
        ma_buffer_release(buffer);

        ma_ds_iterator_get_next(iterator, &buffer);
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("HttpSite", string_val, strlen("HttpSite"), "XML Element hierarchy not parsed correctly");
        ma_buffer_release(buffer);

        ma_ds_iterator_get_next(iterator, &buffer);
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("SpipeSite", string_val, strlen("SpipeSite"), "XML Element hierarchy not parsed correctly");
        ma_buffer_release(buffer);

        // No more items in "ns:SiteLists\\SiteList"
        TEST_ASSERT_MESSAGE(MA_ERROR_NO_MORE_ITEMS == ma_ds_iterator_get_next(iterator, &buffer), "XML Element hierarchy not parsed correctly");
        ma_ds_iterator_release(iterator);

        // Creating iterator for "ns:SiteLists\\SiteList\\HttpSite"
        ma_ds_iterator_create(g_ds, 1, "ns:SiteLists\\SiteList\\HttpSite", &iterator);

        ma_ds_iterator_get_next(iterator, &buffer);
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("RelativePath", string_val, strlen("RelativePath"), "XML Element hierarchy not parsed correctly");
        ma_buffer_release(buffer);

        ma_ds_iterator_get_next(iterator, &buffer);
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("UseAuth", string_val, strlen("UseAuth"), "XML Element hierarchy not parsed correctly");
        ma_buffer_release(buffer);

        ma_ds_iterator_get_next(iterator, &buffer);
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("UserName", string_val, strlen("UserName"), "XML Element hierarchy not parsed correctly");
        ma_buffer_release(buffer);

        ma_ds_iterator_get_next(iterator, &buffer);
        ma_buffer_get_string(buffer, &string_val, &string_size);
        TEST_ASSERT_EQUAL_MEMORY_MESSAGE("Password", string_val, strlen("Password"), "XML Element hierarchy not parsed correctly");
        ma_buffer_release(buffer);

        // No more items in "ns:SiteLists\\SiteList\\HttpSite"
        TEST_ASSERT_MESSAGE(MA_ERROR_NO_MORE_ITEMS == ma_ds_iterator_get_next(iterator, &buffer), "XML Element hierarchy not parsed correctly");
        ma_ds_iterator_release(iterator);
    }

    TEST_ASSERT_MESSAGE(MA_ERROR_PRECONDITION == ma_ds_release(NULL), "ma_ds_release failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_ds_release(g_ds), "ma_ds_release failed");

    TEST_ASSERT_MESSAGE(MA_OK == ma_xml_release(xml), "ma_xml_release failed");

    // Unlink the XML at the end of the test
    unlink(xml_file);
}
