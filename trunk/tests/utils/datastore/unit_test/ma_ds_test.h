#include "ma/datastore/ma_ds.h"
#include "ma/datastore/ma_ds_mapper.h"

#include <stdlib.h>

/* Implementation of test datastore , can be used for testing the datastore , provide your set methods */

typedef struct ma_ds_test_s {
    ma_ds_t         datastore_impl ;
    ma_ds_type_t    ds_type ;
}ma_ds_test_t ;

ma_error_t ma_ds_test_open(ma_ds_type_t ds_type  , ma_ds_test_t **datastore) {
    if(datastore) {
        ma_ds_test_t *self = (ma_ds_test_t *)calloc(1 , sizeof(ma_ds_test_t));
        if(!self) return MA_ERROR_OUTOFMEMORY ;
        self->ds_type = ds_type ;
        ma_ds_init((ma_ds_t *)self , NULL, self);
        *datastore = self ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG;

}

ma_error_t ma_ds_test_set_ds_methods(ma_ds_test_t *datastore , const ma_ds_methods_t *methods) {
    if(datastore && methods ) {
        datastore->datastore_impl.methods = methods ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_ds_test_release(ma_ds_test_t *datastore) {
    if(datastore) {
        free(datastore);
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG;
}

