/*****************************************************************************
Registry datastore unit tests
******************************************************************************/
#include "unity.h"
#include "unity_fixture.h"

#include "ma/ma_datastore.h"
#include "ma/datastore/ma_ds.h"
#include "ma/datastore/ma_ds_registry.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <Windows.h>

#define ut_ini_file_path "./policies.ini"

static ma_ds_t *g_ds = NULL ;

#define TEST_ROOT_KEY "Software\\McAfee_T1\\McTray\\ds_registry_test"

extern ma_error_t ma_registry_delete_tree(void* , const char *, ma_bool_t );

TEST_GROUP_RUNNER(ma_ds_registry_tests_group) {
    do {RUN_TEST_CASE(ma_ds_registry_tests_group, verify_ds_set_get_string_apis)} while (0);
    do {RUN_TEST_CASE(ma_ds_registry_tests_group, verify_ds_set_get_variant_apis)} while (0);
    do {RUN_TEST_CASE(ma_ds_registry_tests_group, verify_ds_set_get_blob_apis)} while (0);
    do {RUN_TEST_CASE(ma_ds_registry_tests_group, verify_ds_set_get_int_apis)} while (0);
    do {RUN_TEST_CASE(ma_ds_registry_tests_group, verify_ds_remove_apis)} while (0);
    do {RUN_TEST_CASE(ma_ds_registry_tests_group, verify_ds_iterator_tests)} while (0);
    do {RUN_TEST_CASE(ma_ds_registry_tests_group, verify_ds_set_get_without_root)} while (0);
    do {RUN_TEST_CASE(ma_ds_registry_view_tests_group, verify_ds_view_change)} while (0);
}

TEST_SETUP(ma_ds_registry_tests_group) {
    ma_ds_registry_t *ds_reg = NULL ;
    Unity.TestFile = "ma_ds_registry_test.c";

    /*Remove the keys from prevous run if any */
    ma_registry_delete_tree(HKEY_CURRENT_USER, TEST_ROOT_KEY, MA_TRUE);

    //  While opening registry, do not create registry if it is not existing previously.
    TEST_ASSERT(MA_ERROR_DS_REG_OPEN_FAILED == ma_ds_registry_open(HKEY_CURRENT_USER, "Software\\ABC\\XYZ\\", MA_TRUE, MA_FALSE, KEY_ALL_ACCESS, &ds_reg));

    //  While opening registry, create registry if it doesn't exist previous
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_registry_open(HKEY_CURRENT_USER, TEST_ROOT_KEY,MA_TRUE, MA_TRUE, KEY_ALL_ACCESS, NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_ds_registry_open(HKEY_CURRENT_USER, TEST_ROOT_KEY, MA_TRUE, MA_TRUE,KEY_ALL_ACCESS, &ds_reg));
    g_ds = (ma_ds_t*)ds_reg;
}

TEST_TEAR_DOWN(ma_ds_registry_tests_group) {
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_ds_registry_release(NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_ds_registry_release((ma_ds_registry_t*)g_ds));
    /*Remove the key which this group created */
    ma_registry_delete_tree(HKEY_CURRENT_USER , "Software\\McAfee_T1", MA_TRUE);
}

TEST(ma_ds_registry_tests_group, verify_ds_set_get_string_apis) {
    ma_buffer_t *value = NULL ;
    ma_wbuffer_t *w_value = NULL;
    char *ptr = NULL;
    ma_wchar_t *ptr_w = NULL;
    size_t size = 0 ;

    ma_bool_t is_exists = MA_TRUE;


    /* datastore  is NULL */
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_str(NULL, "AntiVirus Policy\\PATH_NOT_EXISTS", "Is Scan Enabled" , &value));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_set_str(NULL, "AntiVirus Policy\\PATH_NOT_EXISTS", "Is Scan Enabled" , "1" , -1));

    /* Path is NULL */
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_str(g_ds,NULL, "Is Scan Enabled" , &value));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_set_str(g_ds, NULL, "Is Scan Enabled" , "1" , -1));

    /* Key is NULL */
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_str(g_ds,"AntiVirus Policy\\General", NULL , &value));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_set_str(g_ds, "AntiVirus Policy\\PATH_NOT_EXISTS", NULL , "1" , -1));

    /* out param is NULL */
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_str(g_ds,"AntiVirus Policy\\General", "Is Scan Enabled" ,NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_set_str(g_ds,"AntiVirus Policy\\General", "Is Scan Enabled" ,NULL , -1));

    /* When the path dont exists*/
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_is_path_exists(NULL, "AntiVirus Policy\\PATH_NOT_EXISTS", &is_exists));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_is_path_exists(g_ds, NULL, &is_exists));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_is_path_exists(g_ds, "AntiVirus Policy\\PATH_NOT_EXISTS", NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_DS_PATH_NOT_FOUND, ma_ds_is_path_exists(g_ds, "AntiVirus Policy\\PATH_NOT_EXISTS", &is_exists));
    TEST_ASSERT(MA_FALSE == is_exists);

    TEST_ASSERT_EQUAL_INT(MA_ERROR_DS_PATH_NOT_FOUND, ma_ds_get_str(g_ds, "AntiVirus Policy\\PATH_NOT_EXISTS", "Is Scan Enabled" , &value));
    TEST_ASSERT_NULL(value);



    /* When the path exists but key doesn exists*/
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_str(g_ds, "AntiVirus Policy\\PATH_EXISTS", "Key_1" , "value",-1));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_DS_KEY_NOT_FOUND, ma_ds_get_str(g_ds, "AntiVirus Policy\\PATH_EXISTS", "Key_2" , &value));
    TEST_ASSERT_NULL(value);

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_is_path_exists(g_ds, "AntiVirus Policy\\PATH_EXISTS", &is_exists));
    TEST_ASSERT(MA_TRUE == is_exists);

    /* set and get it using length passed */
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_str(g_ds, "AntiVirus Policy\\PATH_EXISTS", "Key_1" , "value",3));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_get_str(g_ds, "AntiVirus Policy\\PATH_EXISTS", "Key_1" , &value));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_buffer_get_string(value,&ptr, &size));
    TEST_ASSERT_EQUAL_INT(0,strcmp(ptr,"val"));
    TEST_ASSERT_EQUAL_INT(3,size);
    ma_buffer_release(value);

    value = NULL ;
    /* Set and get utf-8*/
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_str(g_ds, "AntiVirus Policy\\General", "Is Scan Enabled" , "I am enabled" , -1));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_get_str(g_ds, "AntiVirus Policy\\General", "Is Scan Enabled" , &value));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_buffer_get_string(value,&ptr, &size));
    TEST_ASSERT_EQUAL_INT(0,strcmp(ptr,"I am enabled"));
    ma_buffer_release(value);
    value = NULL ;


    /* Set and get utf-8*/
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_wstr(g_ds, "AntiVirus Policy\\General", "Is Scan Enabled" , L"I am enabled" , -1));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_get_wstr(g_ds, "AntiVirus Policy\\General", "Is Scan Enabled" , &w_value));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_wbuffer_get_string(w_value,&ptr_w, &size));
    TEST_ASSERT_EQUAL_INT(0,wcscmp(ptr_w,L"I am enabled"));
    ma_wbuffer_release(w_value);
    value = NULL ;

}

TEST(ma_ds_registry_tests_group, verify_ds_set_get_variant_apis)
{
    ma_variant_t *my_variant_ptr = NULL;
    ma_buffer_t *my_buffer = NULL;
    const char *string_val = NULL;
    size_t string_size = 0;

    ma_bool_t is_exists = MA_TRUE;

    /* When the path dont exists*/
    TEST_ASSERT(MA_ERROR_DS_PATH_NOT_FOUND == ma_ds_is_path_exists(g_ds, "AntiVirus Policy\\PATH_NOT_EXISTS", &is_exists));
    TEST_ASSERT(MA_FALSE == is_exists);

    TEST_ASSERT(MA_ERROR_DS_PATH_NOT_FOUND == ma_ds_get_variant(g_ds, "AntiVirus Policy\\PATH_NOT_EXISTS", "Is Scan Enabled", MA_VARTYPE_STRING, &my_variant_ptr));
    TEST_ASSERT_NULL(my_variant_ptr);

    {
        /* Adding value */
        TEST_ASSERT(MA_OK == ma_variant_create_from_string("TEST_VALUE", strlen("TEST_VALUE"), &my_variant_ptr));
        TEST_ASSERT(MA_OK == ma_ds_set_variant(g_ds, "AntiVirus Policy\\PATH_EXISTS", "Key_1" , my_variant_ptr));
        TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));

        TEST_ASSERT(MA_OK == ma_ds_is_path_exists(g_ds, "AntiVirus Policy\\PATH_EXISTS", &is_exists));
        TEST_ASSERT(MA_TRUE == is_exists);

        /* getting value */
        TEST_ASSERT(MA_OK == ma_ds_get_variant(g_ds, "AntiVirus Policy\\PATH_EXISTS", "Key_1" , MA_VARTYPE_STRING, &my_variant_ptr));
        TEST_ASSERT(MA_OK == ma_variant_get_string_buffer(my_variant_ptr, &my_buffer));
        TEST_ASSERT(MA_OK == ma_buffer_get_string(my_buffer, &string_val, &string_size));
        TEST_ASSERT(0 == strcmp("TEST_VALUE", string_val));
        TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
        TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));
    }

    {
        ma_array_t *my_array = NULL;
        ma_variant_t *array_variant = NULL;

        TEST_ASSERT(MA_OK == ma_array_create(&my_array));

        /* Adding value to array */
        TEST_ASSERT(MA_OK == ma_variant_create_from_string("TEST_VALUE_1", strlen("TEST_VALUE_1"), &my_variant_ptr));
        TEST_ASSERT(MA_OK == ma_array_push(my_array, my_variant_ptr));
        TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));

        TEST_ASSERT(MA_OK == ma_variant_create_from_string("TEST_VALUE_2", strlen("TEST_VALUE_2"), &my_variant_ptr));
        TEST_ASSERT(MA_OK == ma_array_push(my_array, my_variant_ptr));
        TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));

        TEST_ASSERT(MA_OK == ma_variant_create_from_string("TEST_VALUE_3", strlen("TEST_VALUE_3"), &my_variant_ptr));
        TEST_ASSERT(MA_OK == ma_array_push(my_array, my_variant_ptr));
        TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));

        TEST_ASSERT(MA_OK == ma_variant_create_from_array(my_array, &array_variant));
        TEST_ASSERT(MA_OK == ma_array_release(my_array));

        TEST_ASSERT(MA_OK == ma_ds_set_variant(g_ds, "AntiVirus Policy\\PATH_EXISTS", "Key_1" , array_variant));
        TEST_ASSERT(MA_OK == ma_variant_release(array_variant));

        TEST_ASSERT(MA_OK == ma_ds_is_path_exists(g_ds, "AntiVirus Policy\\PATH_EXISTS", &is_exists));
        TEST_ASSERT(MA_TRUE == is_exists);

        /* getting value */
        TEST_ASSERT(MA_OK == ma_ds_get_variant(g_ds, "AntiVirus Policy\\PATH_EXISTS", "Key_1" , MA_VARTYPE_ARRAY, &array_variant));
        TEST_ASSERT(MA_OK == ma_variant_get_array(array_variant, &my_array));
        TEST_ASSERT(MA_OK == ma_variant_release(array_variant));

        {
              TEST_ASSERT(MA_OK == ma_array_get_element_at(my_array, 1, &my_variant_ptr));
            TEST_ASSERT(MA_OK == ma_variant_get_string_buffer(my_variant_ptr, &my_buffer));
            TEST_ASSERT(MA_OK == ma_buffer_get_string(my_buffer, &string_val, &string_size));
            TEST_ASSERT(0 == strcmp("TEST_VALUE_1", string_val));
            TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
            TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));
        }

        {
              TEST_ASSERT(MA_OK == ma_array_get_element_at(my_array, 2, &my_variant_ptr));
            TEST_ASSERT(MA_OK == ma_variant_get_string_buffer(my_variant_ptr, &my_buffer));
            TEST_ASSERT(MA_OK == ma_buffer_get_string(my_buffer, &string_val, &string_size));
            TEST_ASSERT(0 == strcmp("TEST_VALUE_2", string_val));
            TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
            TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));
        }

        {
              TEST_ASSERT(MA_OK == ma_array_get_element_at(my_array, 3, &my_variant_ptr));
            TEST_ASSERT(MA_OK == ma_variant_get_string_buffer(my_variant_ptr, &my_buffer));
            TEST_ASSERT(MA_OK == ma_buffer_get_string(my_buffer, &string_val, &string_size));
            TEST_ASSERT(0 == strcmp("TEST_VALUE_3", string_val));
            TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
            TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));
        }

        TEST_ASSERT(MA_OK == ma_array_release(my_array));
    }
}



TEST(ma_ds_registry_tests_group, verify_ds_set_get_blob_apis) {
    ma_buffer_t *value = NULL ;
    unsigned char *ptr =  NULL;
    char buff[128] = {0};
    size_t size = 0 ;

    /* datastore  is NULL */
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_blob(NULL, "AntiVirus Policy\\PATH_NOT_EXISTS", "Is Scan Enabled" , &value));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_set_blob(NULL, "AntiVirus Policy\\PATH_NOT_EXISTS", "Is Scan Enabled" , "1", 1));

    /* Path is NULL */
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_blob(g_ds,NULL, "Is Scan Enabled" , &value));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_set_blob(g_ds, NULL, "Is Scan Enabled" , "1" , 1));

    /* Key is NULL */
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_blob(g_ds,"AntiVirus Policy\\General", NULL , &value));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_set_blob(g_ds, "AntiVirus Policy\\PATH_NOT_EXISTS", NULL , "1" , -1));

    /* out param is NULL */
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_blob(g_ds,"AntiVirus Policy\\General", "Is Scan Enabled" ,NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_set_blob(g_ds,"AntiVirus Policy\\General", "Is Scan Enabled" ,NULL , 1));

    /* When the path dont exists*/
    TEST_ASSERT_EQUAL_INT(MA_ERROR_DS_PATH_NOT_FOUND, ma_ds_get_str(g_ds, "AntiVirus Policy\\PATH_NOT_EXISTS", "Is Scan Enabled" , &value));
    TEST_ASSERT_NULL(value);

    /* When the path exists but key doesn exists*/
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_blob(g_ds, "AntiVirus Policy\\PATH_EXISTS", "Key_1" , "value",5));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_DS_KEY_NOT_FOUND, ma_ds_get_blob(g_ds, "AntiVirus Policy\\PATH_EXISTS", "Key_2" , &value));
    TEST_ASSERT_NULL(value);

    /* Set and get*/
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_blob(g_ds, "AntiVirus Policy\\General", "Is Scan Enabled" , "LOL" , 3));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_get_blob(g_ds, "AntiVirus Policy\\General", "Is Scan Enabled" , &value));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_buffer_get_raw(value,&ptr, &size));
    TEST_ASSERT_EQUAL_INT(3,size);
    memcpy(buff,ptr,size);
    TEST_ASSERT_EQUAL_INT(0,strcmp(buff,"LOL"));
    ma_buffer_release(value);
}


TEST(ma_ds_registry_tests_group, verify_ds_set_get_int_apis) {
    ma_int16_t value_16 = 0 ;
    ma_int32_t value_32 = 0 ;
    ma_int64_t value_64 = 0 ;


    /* datastore  is NULL */
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_int16(NULL, "AntiVirus Policy\\PATH_NOT_EXISTS", "Is Scan Enabled" , &value_16));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_set_int(NULL, "AntiVirus Policy\\PATH_NOT_EXISTS", "Is Scan Enabled" , value_16));

    /* Path is NULL */
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_int16(g_ds,NULL, "Is Scan Enabled" , &value_16));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_set_int(g_ds, NULL ,"Is Scan Enabled" , value_16));

    /* Key is NULL */
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_int16(g_ds,"AntiVirus Policy\\General", NULL , &value_16));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_set_int(g_ds, "AntiVirus Policy\\General" ,NULL , value_16));

    /* out param is NULL */
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_get_int16(g_ds,"AntiVirus Policy\\General", "Is Scan Enabled" ,NULL));

    /* When the path dont exists*/
    TEST_ASSERT_EQUAL_INT(MA_ERROR_DS_PATH_NOT_FOUND, ma_ds_get_int16(g_ds, "AntiVirus Policy\\PATH_NOT_EXISTS", "Is Scan Enabled" , &value_16));

    /* When the path exists but key doesn exists*/
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_int(g_ds, "AntiVirus Policy\\PATH_EXISTS", "Key_1" , 123456));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_DS_KEY_NOT_FOUND, ma_ds_get_int16(g_ds, "AntiVirus Policy\\PATH_EXISTS", "Key_2" , &value_16));

    /* Set and get*/
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_int(g_ds, "AntiVirus Policy\\General", "Is Scan Enabled" , 12345));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_get_int16(g_ds, "AntiVirus Policy\\General", "Is Scan Enabled" , &value_16));
    TEST_ASSERT_EQUAL_INT16(12345,value_16);

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_get_int32(g_ds, "AntiVirus Policy\\General", "Is Scan Enabled" , &value_32));
    TEST_ASSERT_EQUAL_INT32(12345,value_32);

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_get_int64(g_ds, "AntiVirus Policy\\General", "Is Scan Enabled" , &value_64));
    TEST_ASSERT_EQUAL_INT64(12345,value_64);

}

TEST(ma_ds_registry_tests_group, verify_ds_remove_apis) {
     ma_buffer_t *value = NULL ;

     ma_bool_t is_exists = MA_FALSE;

    /* datastore  is NULL */
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_rem(NULL, "AntiVirus Policy\\PATH_NOT_EXISTS", "Is Scan Enabled" , 0));

    /* Path is NULL */
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_rem(g_ds,NULL, "Is Scan Enabled" , 0));

    /* Key is NULL */
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_rem(g_ds,"AntiVirus Policy\\General", NULL , 0));

    /* When the path dont exists*/
    TEST_ASSERT_EQUAL_INT(MA_ERROR_DS_PATH_NOT_FOUND, ma_ds_rem(g_ds, "AntiVirus Policy\\PATH_NOT_EXISTS", "Is Scan Enabled" , 0));

    /* Single remove */
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_blob(g_ds, "AntiVirus Policy\\PATH_EXISTS", "A" , "AA",2));

    // Key Exists
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_is_key_exists(NULL, "AntiVirus Policy\\PATH_EXISTS", "A", &is_exists));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_is_key_exists(g_ds, NULL, "A", &is_exists));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_is_key_exists(g_ds, "AntiVirus Policy\\PATH_EXISTS", NULL, &is_exists));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_ds_is_key_exists(g_ds, "AntiVirus Policy\\PATH_EXISTS", "A", NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_is_key_exists(g_ds, "AntiVirus Policy\\PATH_EXISTS", "A", &is_exists));
    TEST_ASSERT(MA_TRUE == is_exists);

    //Remove
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_rem(g_ds, "AntiVirus Policy\\PATH_EXISTS", "A" , 0));

    //Try to get
    TEST_ASSERT_EQUAL_INT(MA_ERROR_DS_KEY_NOT_FOUND , ma_ds_get_blob(g_ds, "AntiVirus Policy\\PATH_EXISTS", "A" , &value));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_DS_KEY_NOT_FOUND , ma_ds_get_blob(g_ds, "AntiVirus Policy\\PATH_EXISTS", "B" , &value));

    // Key doesn't exist
    TEST_ASSERT_EQUAL_INT(MA_ERROR_DS_KEY_NOT_FOUND, ma_ds_is_key_exists(g_ds, "AntiVirus Policy\\PATH_EXISTS", "A", &is_exists));
    TEST_ASSERT(MA_FALSE == is_exists);

    /*Multi remove */
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_blob(g_ds, "AntiVirus Policy\\PATH_EXISTS", "A" , "AA",2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_blob(g_ds, "AntiVirus Policy\\PATH_EXISTS", "B" , "BB",2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_blob(g_ds, "AntiVirus Policy\\PATH_EXISTS", "C" , "CC",2));
    //rec -Remove
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_rem(g_ds, "AntiVirus Policy\\PATH_EXISTS", NULL , 1));
    //Try to get
    TEST_ASSERT_EQUAL_INT(MA_ERROR_DS_PATH_NOT_FOUND , ma_ds_get_blob(g_ds, "AntiVirus Policy\\PATH_EXISTS", "A" , &value));


}

TEST(ma_ds_registry_tests_group, verify_ds_set_get_without_root) {
    ma_int16_t value_16 = 0 ;
    ma_ds_registry_t *ds = NULL ;

    /*Remove the keys from prevous run ifany */
    ma_registry_delete_tree(HKEY_CURRENT_USER , "Software\\McAfee_T2\\McTray\\ds_registry_test\\AntiVirus Policy\\General", MA_TRUE);
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_ds_registry_open(HKEY_CURRENT_USER, NULL,MA_TRUE,MA_TRUE,KEY_ALL_ACCESS,&ds));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_int((ma_ds_t*)ds, "Software\\McAfee_T2\\McTray\\ds_registry_test\\AntiVirus Policy\\General", "Is Scan Enabled" , 12345));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_get_int16((ma_ds_t *)ds, "Software\\McAfee_T2\\McTray\\ds_registry_test\\AntiVirus Policy\\General", "Is Scan Enabled" , &value_16));
    TEST_ASSERT_EQUAL_INT16(12345,value_16);

    ma_ds_registry_release(ds);

     /*Remove the key which this group created */
    ma_registry_delete_tree(HKEY_CURRENT_USER , "Software\\McAfee_T2", MA_TRUE);
}


TEST(ma_ds_registry_tests_group, verify_ds_iterator_tests) {
    ma_ds_iterator_t *it = NULL;
    ma_buffer_t *buffer = NULL;
    char *sub_paths[]= {"SiteList_1","SiteList_2", "SiteList_3"};
    char *sub_keys[]= {"SiteListVersion", "GlobalVersion", "Source"};
    size_t count_sub_paths=3 , count_sub_keys = 3 , i = 0;


     /* Set up the registry*/
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_str(g_ds, "MA\\AH\\", "SiteListVersion", "2000112" , -1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_str(g_ds, "MA\\AH\\", "GlobalVersion", "2222222", -1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_str(g_ds, "MA\\AH\\", "Source", "2222222", -1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_str(g_ds, "MA\\AH\\SiteList_1", "Name", "Site_1", -1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_str(g_ds, "MA\\AH\\SiteList_2", "Name", "Site_2", -1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_str(g_ds, "MA\\AH\\SiteList_3", "Name", "Site_3", -1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_str(g_ds, "MA\\AH\\SiteList_1\\OneMoreSubPath", "Name", "Site_1", -1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_str(g_ds, "MA\\AH\\SiteList_2\\OneMoreSubPath", "Name", "Site_2", -1));

    /*Path test */
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_iterator_create(g_ds, 1, "MA\\AH", &it));
    for(i = 0 ; i < count_sub_paths;i++) {
        char *value  = NULL;
        size_t size = 0;
        TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_iterator_get_next(it, &buffer));
        TEST_ASSERT_EQUAL_INT(MA_OK, ma_buffer_get_string(buffer,&value,&size));
        TEST_ASSERT_EQUAL_INT(0, strcmp(sub_paths[i],value));
        ma_buffer_release(buffer),buffer = NULL;
    }
    TEST_ASSERT_EQUAL_INT(MA_ERROR_DS_NO_MORE_ITEMS, ma_ds_iterator_get_next(it,&buffer));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_ds_iterator_release(it));

    /*Keys test */
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_iterator_create(g_ds, 0, "MA\\AH", &it));
    for(i = 0 ; i < count_sub_keys;i++) {
        char *value  = NULL;
        size_t size = 0;
        TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_iterator_get_next(it, &buffer));
        TEST_ASSERT_EQUAL_INT(MA_OK, ma_buffer_get_string(buffer,&value,&size));
        TEST_ASSERT_EQUAL_INT(0, strcmp(sub_keys[i],value));
        ma_buffer_release(buffer),buffer = NULL;
    }
    TEST_ASSERT_EQUAL_INT(MA_ERROR_DS_NO_MORE_ITEMS, ma_ds_iterator_get_next(it,&buffer));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_ds_iterator_release(it));
}

#define VIEW_TEST_KEY        "Software\\McAfee_T1"



TEST_SETUP(ma_ds_registry_view_tests_group) {
    RegDeleteTreeA(HKEY_LOCAL_MACHINE , VIEW_TEST_KEY);
}

TEST_TEAR_DOWN(ma_ds_registry_view_tests_group) {
    /*Remove the key which this group created */
    ma_registry_delete_tree(HKEY_LOCAL_MACHINE , "Software\\McAfee_T1",MA_TRUE);
}

#ifdef WIN64
#define VIEW_TEST_SUBKEY    "x64"
#else
#define VIEW_TEST_SUBKEY    "x86"
#endif

TEST(ma_ds_registry_view_tests_group, verify_ds_view_change) {
    ma_ds_registry_t *ds_reg = NULL ;
    ma_int64_t value = 0;
    Unity.TestFile = "ma_ds_registry_test.c";
    /*Remove the keys from prevous run ifany */
    //Open the default view at VIEW_TEST_KEY in x86
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_ds_registry_open(HKEY_LOCAL_MACHINE, VIEW_TEST_KEY, MA_TRUE,MA_TRUE,KEY_ALL_ACCESS, &ds_reg));
    g_ds = (ma_ds_t*)ds_reg;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_set_int(g_ds, VIEW_TEST_SUBKEY, "UnitTesting", 1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ds_get_int64(g_ds, VIEW_TEST_SUBKEY, "UnitTesting", &value));
    TEST_ASSERT_EQUAL_INT(1, value);
    /*Close the ds*/
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_ds_registry_release((ma_ds_registry_t*)g_ds));

    /*Open the alternate view and check for the path, it shouldn't exist*/
    value = 0;
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_ds_registry_open(HKEY_LOCAL_MACHINE, VIEW_TEST_KEY, MA_FALSE, MA_TRUE,KEY_ALL_ACCESS,&ds_reg));
    g_ds = (ma_ds_t*)ds_reg;
    TEST_ASSERT_EQUAL_INT(MA_ERROR_DS_PATH_NOT_FOUND, ma_ds_get_int64(g_ds, VIEW_TEST_SUBKEY, "UnitTesting", &value));
    /*Close the ds*/
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_ds_registry_release((ma_ds_registry_t*)g_ds));
}

