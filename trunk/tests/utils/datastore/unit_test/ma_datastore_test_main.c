/*****************************************************************************
Main Test runner for all datatstiore  unit tests.
******************************************************************************/

#include "unity.h"
#include "unity_fixture.h"
#include "ma/ma_common.h"

#ifdef MA_WINDOWS
#include <windows.h>
#endif
#ifdef _MSC_VER
# include <crtdbg.h> /* */
# define MA_TRACKLEAKS _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF)
#else
# define MA_TRACKLEAKS ((void *)0)
#endif


static void run_all_tests(void)
{    
   do {RUN_TEST_GROUP(ma_ds_mapper_tests_group); } while(0); 
   do {RUN_TEST_GROUP(ma_ds_ini_tests_group); } while(0);
#ifdef MA_WINDOWS
   do {RUN_TEST_GROUP(ma_ds_registry_tests_group); } while(0);
#endif       
   do {RUN_TEST_GROUP(ma_ds_database_tests_group); } while (0); 
   do {RUN_TEST_GROUP(ma_ds_xml_tests_group); } while (0);
}

int main(int argc, char *argv[]) {
    MA_TRACKLEAKS;
    return UnityMain(argc, argv, run_all_tests);
}


