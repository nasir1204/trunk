/*****************************************************************************
Database tests.
******************************************************************************/

#include "unity.h"
#include "unity_internals.h"
#include "unity_fixture.h"
#include "ma/internal/utils/database/ma_db_statement.h"
#include <string.h>

#ifndef MA_WINDOWS
#include <unistd.h>
#endif

// SQL queries
#define CREATE_MPA_TABLE_STMT "CREATE TABLE IF NOT EXISTS MA_POLICY_OBJECT(PO_ID INTEGER NOT NULL UNIQUE,PO_NAME TEXT NOT NULL,PO_FEATURE TEXT NOT NULL, PO_CATEGORY TEXT NOT NULL, PO_TYPE TEXT NOT NULL, PO_DIGEST TEXT NOT NULL, PO_VALUE INTEGER NOT NULL, PRIMARY KEY (PO_ID) ON CONFLICT REPLACE)"
#define ADD_DATA_MPA_STMT "INSERT INTO MA_POLICY_OBJECT VALUES(?, ?, ?, ?, ?, ?, ?)"
#define GET_ALL_PO_DATA "SELECT * FROM MA_POLICY_OBJECT WHERE PO_ID = ?"

// SQL queries when complete data is stored in one array variant
#define CREATE_MPA_TABLE_STMT_1 "CREATE TABLE IF NOT EXISTS MA_POLICY_OBJECT(PO_ID INTEGER NOT NULL UNIQUE, PO_DATA TEXT NOT NULL, PRIMARY KEY (PO_ID) ON CONFLICT REPLACE)"
#define ADD_DATA_MPA_STMT_1 "INSERT INTO MA_POLICY_OBJECT VALUES(?, ?)"
#define GET_ALL_PO_DATA_1 "SELECT * FROM MA_POLICY_OBJECT WHERE PO_ID = ?"

struct LookupTable
{
    int test_po_id; // INT
    const char *test_po_name; // STRING
    const char *test_po_feature; // STRING
    const char *test_po_category; // STRING
    const char *test_po_type; // VARIANT
    const char *test_po_digest; // BLOB
    ma_int64_t test_po_value; // LONG
};

static struct LookupTable const test_vector[] =
{
    { 10, "Policy 1",  "Feature 4", "Category 1", "Normal",   "8f36f236c1e6382d7bb0b632a52e0e203fc71fac525c838bb31474470bfba853", -3648334 },
    { 2,  "Policy 2",  "Feature 1", "Category 3", "Critical", "fklehflfnlefeklcndcndklfhnc,dsncdslkfndsklfnsdlknfdsklfdsklfdsnf", 0        },
    { 3,  "Policy 3",  "Feature 1", "Category 2", "Ignore",   "vdldmlsldfo4349u43p34oro34rm34r34nrl4kn3lkn3l5kn34lk5n345l34k5n3", 2337     },
    { 4,  "Policy 4",  "Feature 2", "Category 3", "Normal",   "sdlfudods,fewrfsdlfhsdflkehjfelkcsd,fneoinefsd.fnslfje470bfba853", 32473    },
    { 5,  "Policy 5",  "Feature 1", "Category 4", "Default",  "8f36f236c1e6382d7bb0b632a52e0e20sjsdklehfoiefnsdfnhdskffkjsdjs53", 3678032  },
    { 6,  "Policy 9",  "Feature 3", "Category 1", "Normal",   "8f36f236c1e6382d7bb0bdwkjldhnlwdfnf,mnbcmsvcsajdvdfhjfdwjhdwjdwj", -1211    },
    { 7,  "Policy 8",  "Feature 1", "Category 2", "Critical", "8f36f236c1e6382d7bb0b632a52e0e203fc71fac525c838bb31474470bfba853", 23876    },
    { 9,  "Policy 7",  "Feature 3", "Category 3", "Major",    "dkwedbwekdwebdkhdwelkdyewhde0e203fc71fac525c838bb3wedewkjjewkjew", 23562    },
    { 11, "Policy 2",  "Feature 1", "Category 3", "Default",  "wdwedwerwrewrerd7bb0b632a52e0e203fc71facdewwerewrewrwerererereer", 100      },
    { 12, "Policy 10", "Feature 2", "Category 1", "Normal",   "8f36f236c1e63823wedwdewdwdwdwedewfc71fac525c838bb31474470bfba853", -3434    },
    { 8,  "Policy 6",  "Feature 2", "Category 5", "Normal",   "8f36f236c1e6382dqwdewedwdwffrwerwedwdwerwerd838bb31474470bfba853", 99       },
};


TEST_GROUP_RUNNER(ma_database_tests_group)
{
    do {RUN_TEST_CASE(ma_database_tests, db_open_close_test)} while (0);
    //do {RUN_TEST_CASE(ma_database_tests, db_statement_and_recordset_test)} while (0);
    do {RUN_TEST_CASE(ma_database_tests, db_statement_and_recordset_test_using_array_variant)} while (0);
}

TEST_SETUP(ma_database_tests)
{
}

TEST_TEAR_DOWN(ma_database_tests)
{
}


/*****************************************************************************
DB Open and Close API tests.
******************************************************************************/
TEST(ma_database_tests, db_open_close_test)
{
    ma_db_t *my_db = NULL;
    unlink("MY_DB");

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_open(NULL, MA_DB_OPEN_READWRITE, &my_db));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_open("MY_DB", MA_DB_OPEN_READWRITE, NULL));
    TEST_ASSERT(MA_OK == ma_db_open("MY_DB", MA_DB_OPEN_READWRITE, &my_db));

    // Transaction Begin and End
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_transaction_begin(NULL));
    TEST_ASSERT(MA_OK == ma_db_transaction_begin(my_db));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_transaction_end(NULL));
    TEST_ASSERT(MA_OK == ma_db_transaction_end(my_db));

    // Transaction Begin and Cancel
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_transaction_begin(NULL));
    TEST_ASSERT(MA_OK == ma_db_transaction_begin(my_db));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_transaction_cancel(NULL));
    TEST_ASSERT(MA_OK == ma_db_transaction_cancel(my_db));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_close(NULL));
    TEST_ASSERT(MA_OK == ma_db_close(my_db));

    TEST_ASSERT(MA_OK == ma_db_open("MY_DB", MA_DB_OPEN_READONLY, &my_db));
    TEST_ASSERT(MA_OK == ma_db_close(my_db));

    // Remove the file created in this test
    unlink("MY_DB");
}


/*****************************************************************************
DB Statement and Recordset tests.
******************************************************************************/
TEST(ma_database_tests, db_statement_and_recordset_test)
{
    ma_int8_t counter = 0;

    ma_db_t *db_handle = NULL;
    ma_db_statement_t *db_stmt = NULL;

    unlink("POLICY_DB");

    // Creating DB
    TEST_ASSERT(MA_OK == ma_db_open("POLICY_DB", MA_DB_OPEN_READWRITE, &db_handle));

    // Creating DB Statement to add table
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_statement_create(NULL, CREATE_MPA_TABLE_STMT, &db_stmt));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_statement_create(db_handle, NULL, &db_stmt));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_statement_create(db_handle, CREATE_MPA_TABLE_STMT, NULL));
    TEST_ASSERT(MA_ERROR_DB_CREATE_STATEMENT_FAILED == ma_db_statement_create(db_handle, "ewriyeoirehrfioerhfnelfherlkfer", &db_stmt)); // Incorrect SQL query. Create failed
    TEST_ASSERT(MA_OK == ma_db_statement_create(db_handle, CREATE_MPA_TABLE_STMT, &db_stmt));

    // Executing the Statement to add table
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_statement_execute(NULL));
    TEST_ASSERT(MA_OK == ma_db_statement_execute(db_stmt));

    // Releasing DB Statement to add table
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_statement_release(NULL));
    TEST_ASSERT(MA_OK == ma_db_statement_release(db_stmt));
    db_stmt = NULL;

    // Creating DB Statement to add elements to table
    TEST_ASSERT(MA_OK == ma_db_statement_create(db_handle, ADD_DATA_MPA_STMT, &db_stmt));

    // Iterating for adding data into the table
    for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        ma_variant_t *my_variant = NULL;

        // PO_ID Column index 1
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_statement_set_int(NULL, 1, test_vector[counter].test_po_id));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_statement_set_int(db_stmt, 0, test_vector[counter].test_po_id));
        TEST_ASSERT(MA_OK == ma_db_statement_set_int(db_stmt, 1, test_vector[counter].test_po_id));

        // PO_NAME Column index 2
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_statement_set_string(NULL, 2, 1, test_vector[counter].test_po_name, strlen(test_vector[counter].test_po_name)));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_statement_set_string(db_stmt, 0, 1, test_vector[counter].test_po_name, strlen(test_vector[counter].test_po_name)));
        TEST_ASSERT(MA_OK == ma_db_statement_set_string(db_stmt, 2, 1, test_vector[counter].test_po_name, strlen(test_vector[counter].test_po_name)));

        // PO_FEATURE Column index 3
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_statement_set_string(NULL, 3, 0, test_vector[counter].test_po_feature, strlen(test_vector[counter].test_po_feature)));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_statement_set_string(db_stmt, 0, 0, test_vector[counter].test_po_feature, strlen(test_vector[counter].test_po_feature)));
        TEST_ASSERT(MA_OK == ma_db_statement_set_string(db_stmt, 3, 0, test_vector[counter].test_po_feature, strlen(test_vector[counter].test_po_feature)));

        // PO_CATEGORY Column index 4
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_statement_set_string(NULL, 4, 0, test_vector[counter].test_po_category, strlen(test_vector[counter].test_po_category)));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_statement_set_string(db_stmt, 0, 0, test_vector[counter].test_po_category, strlen(test_vector[counter].test_po_category)));
        TEST_ASSERT(MA_OK == ma_db_statement_set_string(db_stmt, 4, 0, test_vector[counter].test_po_category, strlen(test_vector[counter].test_po_category)));

        // PO_TYPE Column index 5
        TEST_ASSERT(MA_OK == ma_variant_create_from_string(test_vector[counter].test_po_type, strlen(test_vector[counter].test_po_type), &my_variant));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_statement_set_variant(NULL, 5, my_variant));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_statement_set_variant(db_stmt, 0, my_variant));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_statement_set_variant(db_stmt, 5, NULL));
        TEST_ASSERT(MA_OK == ma_db_statement_set_variant(db_stmt, 5, my_variant));
        TEST_ASSERT(MA_OK == ma_variant_release(my_variant));

        // PO_DIGEST Column index 6
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_statement_set_blob(NULL, 6, 0, test_vector[counter].test_po_digest, strlen(test_vector[counter].test_po_digest)));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_statement_set_blob(db_stmt, 0, 0, test_vector[counter].test_po_digest, strlen(test_vector[counter].test_po_digest)));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_statement_set_blob(db_stmt, 6, 0, NULL, strlen(test_vector[counter].test_po_digest)));
        TEST_ASSERT(MA_OK == ma_db_statement_set_blob(db_stmt, 6, 0, test_vector[counter].test_po_digest, strlen(test_vector[counter].test_po_digest)));

        // PO_VALUE Column index 7
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_statement_set_long(NULL, 7, test_vector[counter].test_po_value));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_statement_set_long(db_stmt, 0, test_vector[counter].test_po_value));
        TEST_ASSERT(MA_OK == ma_db_statement_set_long(db_stmt, 7, test_vector[counter].test_po_value));

        // Executing the Statement to add table
        TEST_ASSERT(MA_OK == ma_db_statement_execute(db_stmt));
    }

    // Releasing DB Statement (for adding elements to the table)
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_statement_release(NULL));
    TEST_ASSERT(MA_OK == ma_db_statement_release(db_stmt));
    db_stmt = NULL;

    // Close DB (Read-Write Mode)
    TEST_ASSERT(MA_OK == ma_db_close(db_handle));
    db_handle = NULL;

    // Open DB in read-only mode
    TEST_ASSERT(MA_OK == ma_db_open("POLICY_DB", MA_DB_OPEN_READONLY, &db_handle));

    // Creating DB Statement for Retrieving Data from table
    TEST_ASSERT(MA_OK == ma_db_statement_create(db_handle, GET_ALL_PO_DATA, &db_stmt));

    // Iterating for retrieving data from the table (in reverse order)
    for(counter = (sizeof(test_vector)/sizeof(test_vector[0]) - 1); counter >= 0; counter--)
    {
        ma_db_recordset_t *db_record = NULL;


        // Setting PO ID value
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_statement_set_int(NULL, 1, test_vector[counter].test_po_id));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_statement_set_int(db_stmt, 0, test_vector[counter].test_po_id));
        TEST_ASSERT(MA_OK == ma_db_statement_set_int(db_stmt, 1, test_vector[counter].test_po_id));

        // Executing query to retrieve one record from table
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_statement_execute_query(NULL, &db_record));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_statement_execute_query(db_stmt, NULL));
        TEST_ASSERT(MA_OK == ma_db_statement_execute_query(db_stmt, &db_record));

        // Verifying Column Count
        {
            int column_count = 0;

            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_recordset_get_column_count(NULL, &column_count));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_recordset_get_column_count(db_record, NULL));
            TEST_ASSERT(MA_OK == ma_db_recordset_get_column_count(db_record, &column_count));
            TEST_ASSERT(7 == column_count);
        }

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_recordset_next(NULL));

        // Retrieving one record
        TEST_ASSERT(MA_OK == ma_db_recordset_next(db_record));

        {
            // Verifying Column Names
            {
                const char *column_name = NULL;

                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_recordset_get_column_name(NULL, 1, &column_name));
                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_recordset_get_column_name(db_record, 0, &column_name));
                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_recordset_get_column_name(db_record, 1, NULL));
                TEST_ASSERT(MA_OK == ma_db_recordset_get_column_name(db_record, 1, &column_name));
                TEST_ASSERT(0 == strcmp("PO_ID", column_name));

                TEST_ASSERT(MA_OK == ma_db_recordset_get_column_name(db_record, 2, &column_name));
                TEST_ASSERT(0 == strcmp("PO_NAME", column_name));

                TEST_ASSERT(MA_OK == ma_db_recordset_get_column_name(db_record, 3, &column_name));
                TEST_ASSERT(0 == strcmp("PO_FEATURE", column_name));

                TEST_ASSERT(MA_OK == ma_db_recordset_get_column_name(db_record, 4, &column_name));
                TEST_ASSERT(0 == strcmp("PO_CATEGORY", column_name));

                TEST_ASSERT(MA_OK == ma_db_recordset_get_column_name(db_record, 5, &column_name));
                TEST_ASSERT(0 == strcmp("PO_TYPE", column_name));

                TEST_ASSERT(MA_OK == ma_db_recordset_get_column_name(db_record, 6, &column_name));
                TEST_ASSERT(0 == strcmp("PO_DIGEST", column_name));

                TEST_ASSERT(MA_OK == ma_db_recordset_get_column_name(db_record, 7, &column_name));
                TEST_ASSERT(0 == strcmp("PO_VALUE", column_name));
            }

            // Verifying PO_ID
            {
                int int_val = 0;

                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_recordset_get_int(NULL, 1, &int_val));
                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_recordset_get_int(db_record, 0, &int_val));
                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_recordset_get_int(db_record, 1, NULL));
                TEST_ASSERT(MA_OK == ma_db_recordset_get_int(db_record, 1, &int_val));
                TEST_ASSERT(test_vector[counter].test_po_id == int_val);
            }

            // Verifying PO_NAME
            {
                const char *string_val = NULL;

                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_recordset_get_string(NULL, 2, &string_val));
                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_recordset_get_string(db_record, 0, &string_val));
                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_recordset_get_string(db_record, 2, NULL));
                TEST_ASSERT(MA_OK == ma_db_recordset_get_string(db_record, 2, &string_val));
                TEST_ASSERT(0 == strcmp(test_vector[counter].test_po_name, string_val));
            }

            // Verifying PO_FEATURE
            {
                const char *string_val = NULL;

                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_recordset_get_string(NULL, 3, &string_val));
                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_recordset_get_string(db_record, 0, &string_val));
                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_recordset_get_string(db_record, 3, NULL));
                TEST_ASSERT(MA_OK == ma_db_recordset_get_string(db_record, 3, &string_val));
                TEST_ASSERT(0 == strcmp(test_vector[counter].test_po_feature, string_val));
            }

            // Verifying PO_CATEGORY
            {
                const char *string_val = NULL;

                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_recordset_get_string(NULL, 4, &string_val));
                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_recordset_get_string(db_record, 0, &string_val));
                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_recordset_get_string(db_record, 4, NULL));
                TEST_ASSERT(MA_OK == ma_db_recordset_get_string(db_record, 4, &string_val));
                TEST_ASSERT(0 == strcmp(test_vector[counter].test_po_category, string_val));
            }

            // Verifying PO_TYPE
            {
                ma_variant_t *my_variant = NULL;

                ma_buffer_t *my_buffer = NULL;
                const char *string_val = NULL;
                size_t string_size = 0;

                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_recordset_get_variant(NULL, 5, MA_VARTYPE_STRING, &my_variant));
                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_recordset_get_variant(db_record, 0, MA_VARTYPE_STRING, &my_variant));
                TEST_ASSERT(MA_ERROR_UNEXPECTED == ma_db_recordset_get_variant(db_record, 5, MA_VARTYPE_ARRAY, &my_variant));
                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_recordset_get_variant(db_record, 5, MA_VARTYPE_STRING, NULL));
                TEST_ASSERT(MA_OK == ma_db_recordset_get_variant(db_record, 5, MA_VARTYPE_STRING, &my_variant));

                TEST_ASSERT(MA_OK == ma_variant_get_string_buffer(my_variant, &my_buffer));
                TEST_ASSERT(MA_OK == ma_buffer_get_string(my_buffer, &string_val, &string_size));
                TEST_ASSERT(0 == strcmp(test_vector[counter].test_po_type, string_val));

                TEST_ASSERT(MA_OK == ma_variant_release(my_variant));
                TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
            }

            // Verifying PO_DIGEST
            {
                const void *blob_val = NULL;
                size_t blob_size = 0;

                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_recordset_get_blob(NULL, 6, &blob_val, &blob_size));
                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_recordset_get_blob(db_record, 0, &blob_val, &blob_size));
                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_recordset_get_blob(db_record, 6, NULL, &blob_size));
                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_recordset_get_blob(db_record, 6, &blob_val, NULL));
                TEST_ASSERT(MA_OK == ma_db_recordset_get_blob(db_record, 6, &blob_val, &blob_size));
                TEST_ASSERT(0 == memcmp(test_vector[counter].test_po_digest, blob_val, strlen(test_vector[counter].test_po_digest)));
            }

            // Verifying PO_VALUE
            {
                ma_int64_t long_val = 0;

                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_recordset_get_long(NULL, 7, &long_val));
                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_recordset_get_long(db_record, 0, &long_val));
                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_recordset_get_long(db_record, 7, NULL));
                TEST_ASSERT(MA_OK == ma_db_recordset_get_long(db_record, 7, &long_val));
                TEST_ASSERT(test_vector[counter].test_po_value == long_val);
            }
        }

        // Only one record exist per PO ID
        TEST_ASSERT(MA_ERROR_NO_MORE_ITEMS == ma_db_recordset_next(db_record));

        // Release the record
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_recordset_release(NULL));
        TEST_ASSERT(MA_OK == ma_db_recordset_release(db_record));
    }

    // Releasing DB Statement for retrieving data from table
    TEST_ASSERT(MA_OK == ma_db_statement_release(db_stmt));
    db_stmt = NULL;

    // Close DB
    TEST_ASSERT(MA_OK == ma_db_close(db_handle));
    unlink("POLICY_DB");
}


/*****************************************************************************
DB Statement and Recordset tests where the multiple record columns are stored
in a array variant which will occupy single entry in the DB.
******************************************************************************/
TEST(ma_database_tests, db_statement_and_recordset_test_using_array_variant)
{
    ma_int8_t counter = 0;

    ma_db_t *db_handle = NULL;
    ma_db_statement_t *db_stmt = NULL;

    unlink("POLICY_DB");

    // Creating DB
    TEST_ASSERT(MA_OK == ma_db_open("POLICY_DB", MA_DB_OPEN_READWRITE, &db_handle));

    // Creating DB Statement to add table
    TEST_ASSERT(MA_OK == ma_db_statement_create(db_handle, CREATE_MPA_TABLE_STMT_1, &db_stmt));

    // Executing the Statement to add table
    TEST_ASSERT(MA_OK == ma_db_statement_execute(db_stmt));

    // Releasing DB Statement to add table
    TEST_ASSERT(MA_OK == ma_db_statement_release(db_stmt));
    db_stmt = NULL;

    // Creating DB Statement to add elements to table
    TEST_ASSERT(MA_OK == ma_db_statement_create(db_handle, ADD_DATA_MPA_STMT_1, &db_stmt));

    // Iterating for adding data into the table
    for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        ma_variant_t *array_variant = NULL;

        // PO_ID Column index 1
        TEST_ASSERT(MA_OK == ma_db_statement_set_int(db_stmt, 1, test_vector[counter].test_po_id));

        {
            ma_array_t *data_array = NULL;
            ma_variant_t *my_variant_ptr = NULL;

            TEST_ASSERT(MA_OK == ma_array_create(&data_array));

            TEST_ASSERT(MA_OK == ma_variant_create_from_string(test_vector[counter].test_po_name, strlen(test_vector[counter].test_po_name), &my_variant_ptr));
            TEST_ASSERT(MA_OK == ma_array_push(data_array, my_variant_ptr));
            TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));

            TEST_ASSERT(MA_OK == ma_variant_create_from_string(test_vector[counter].test_po_feature, strlen(test_vector[counter].test_po_feature), &my_variant_ptr));
            TEST_ASSERT(MA_OK == ma_array_push(data_array, my_variant_ptr));
            TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));

            TEST_ASSERT(MA_OK == ma_variant_create_from_string(test_vector[counter].test_po_category, strlen(test_vector[counter].test_po_category), &my_variant_ptr));
            TEST_ASSERT(MA_OK == ma_array_push(data_array, my_variant_ptr));
            TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));

            TEST_ASSERT(MA_OK == ma_variant_create_from_string(test_vector[counter].test_po_type, strlen(test_vector[counter].test_po_type), &my_variant_ptr));
            TEST_ASSERT(MA_OK == ma_array_push(data_array, my_variant_ptr));
            TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));

            TEST_ASSERT(MA_OK == ma_variant_create_from_string(test_vector[counter].test_po_digest, strlen(test_vector[counter].test_po_digest), &my_variant_ptr));
            TEST_ASSERT(MA_OK == ma_array_push(data_array, my_variant_ptr));
            TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));

            TEST_ASSERT(MA_OK == ma_variant_create_from_array(data_array, &array_variant));
            TEST_ASSERT(MA_OK == ma_array_release(data_array));
        }

        // PO_DATA Column index 2
        TEST_ASSERT(MA_OK == ma_db_statement_set_variant(db_stmt, 2, array_variant));
        TEST_ASSERT(MA_OK == ma_variant_release(array_variant));

        // Executing the Statement to add table
        TEST_ASSERT(MA_OK == ma_db_statement_execute(db_stmt));
    }

    // Releasing DB Statement (for adding elements to the table)
    TEST_ASSERT(MA_OK == ma_db_statement_release(db_stmt));
    db_stmt = NULL;

    // Creating DB Statement for Retrieving Data from table
    TEST_ASSERT(MA_OK == ma_db_statement_create(db_handle, GET_ALL_PO_DATA_1, &db_stmt));

    // Iterating for retrieving data from the table (in reverse order)
    for(counter = (sizeof(test_vector)/sizeof(test_vector[0]) - 1); counter >= 0; counter--)
    {
        ma_db_recordset_t *db_record = NULL;

        // Setting PO ID value
        TEST_ASSERT(MA_OK == ma_db_statement_set_int(db_stmt, 1, test_vector[counter].test_po_id));

        // Executing query to retrieve one record from table
        TEST_ASSERT(MA_OK == ma_db_statement_execute_query(db_stmt, &db_record));

        // Verifying Column Count (Column count should be 2 PO_ID and PO_DATA
        {
            int column_count = 0;

            TEST_ASSERT(MA_OK == ma_db_recordset_get_column_count(db_record, &column_count));
            TEST_ASSERT(2 == column_count);
        }

        // Retrieving one record
        TEST_ASSERT(MA_OK == ma_db_recordset_next(db_record));

        {
            // Verifying Column Names
            {
                const char *column_name = NULL;

                TEST_ASSERT(MA_OK == ma_db_recordset_get_column_name(db_record, 1, &column_name));
                TEST_ASSERT(0 == strcmp("PO_ID", column_name));

                TEST_ASSERT(MA_OK == ma_db_recordset_get_column_name(db_record, 2, &column_name));
                TEST_ASSERT(0 == strcmp("PO_DATA", column_name));
            }

            // Verifying PO_ID
            {
                int int_val = 0;

                TEST_ASSERT(MA_OK == ma_db_recordset_get_int(db_record, 1, &int_val));
                TEST_ASSERT(test_vector[counter].test_po_id == int_val);
            }

            // Verifying PO_DATA
            {
                ma_variant_t *array_variant = NULL;
                ma_array_t *data_array = NULL;
                ma_variant_t *my_variant_ptr = NULL;

                ma_buffer_t *my_buffer = NULL;
                const char *string_val = NULL;
                size_t string_size = 0;

                TEST_ASSERT(MA_OK == ma_db_recordset_get_variant(db_record, 2, MA_VARTYPE_ARRAY, &array_variant));
                TEST_ASSERT(MA_OK == ma_variant_get_array(array_variant, &data_array));
                TEST_ASSERT(MA_OK == ma_variant_release(array_variant));

                // PO_NAME
                TEST_ASSERT(MA_OK == ma_array_get_element_at(data_array, 1, &my_variant_ptr));
                TEST_ASSERT(MA_OK == ma_variant_get_string_buffer(my_variant_ptr, &my_buffer));
                TEST_ASSERT(MA_OK == ma_buffer_get_string(my_buffer, &string_val, &string_size));
                TEST_ASSERT(0 == strcmp(test_vector[counter].test_po_name, string_val));
                TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));
                TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));

                // PO_FEATURE
                TEST_ASSERT(MA_OK == ma_array_get_element_at(data_array, 2, &my_variant_ptr));
                TEST_ASSERT(MA_OK == ma_variant_get_string_buffer(my_variant_ptr, &my_buffer));
                TEST_ASSERT(MA_OK == ma_buffer_get_string(my_buffer, &string_val, &string_size));
                TEST_ASSERT(0 == strcmp(test_vector[counter].test_po_feature, string_val));
                TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));
                TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));

                // PO_CATEGORY
                TEST_ASSERT(MA_OK == ma_array_get_element_at(data_array, 3, &my_variant_ptr));
                TEST_ASSERT(MA_OK == ma_variant_get_string_buffer(my_variant_ptr, &my_buffer));
                TEST_ASSERT(MA_OK == ma_buffer_get_string(my_buffer, &string_val, &string_size));
                TEST_ASSERT(0 == strcmp(test_vector[counter].test_po_category, string_val));
                TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));
                TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));

                // PO_TYPE
                TEST_ASSERT(MA_OK == ma_array_get_element_at(data_array, 4, &my_variant_ptr));
                TEST_ASSERT(MA_OK == ma_variant_get_string_buffer(my_variant_ptr, &my_buffer));
                TEST_ASSERT(MA_OK == ma_buffer_get_string(my_buffer, &string_val, &string_size));
                TEST_ASSERT(0 == strcmp(test_vector[counter].test_po_type, string_val));
                TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));
                TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));

                // PO_DIGEST
                TEST_ASSERT(MA_OK == ma_array_get_element_at(data_array, 5, &my_variant_ptr));
                TEST_ASSERT(MA_OK == ma_variant_get_string_buffer(my_variant_ptr, &my_buffer));
                TEST_ASSERT(MA_OK == ma_buffer_get_string(my_buffer, &string_val, &string_size));
                TEST_ASSERT(0 == strcmp(test_vector[counter].test_po_digest, string_val));
                TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));
                TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));

                // Release array
                TEST_ASSERT(MA_OK == ma_array_release(data_array));
            }
        }

        // Only one record exist per PO ID
        TEST_ASSERT(MA_ERROR_NO_MORE_ITEMS == ma_db_recordset_next(db_record));

        // Release the record
        TEST_ASSERT(MA_OK == ma_db_recordset_release(db_record));
    }

    // Releasing DB Statement for retrieving data from table
    TEST_ASSERT(MA_OK == ma_db_statement_release(db_stmt));
    db_stmt = NULL;

    // Close DB
    TEST_ASSERT(MA_OK == ma_db_close(db_handle));
    unlink("POLICY_DB");
}
