#include "ma/repository/ma_repository.h"
//#include "ma/internal/services/repository/ma_repository_internal.h"
ma_repository_t *repo_google = NULL;
ma_repository_t *repo_fallback = NULL;
ma_repository_t *repo_already_calculated = NULL;
ma_repository_t *repo_yahoo = NULL;
ma_repository_t *repo_mc = NULL;
ma_repository_t *repo_local = NULL;
ma_repository_t *repo_mc1, *repo_mc2, *nodomain, *repo_mc3, *repo_mc4, *repo_mc5, *repo_mc6, *repo_mc7, *repo_mc8, *repo_mc9, *repo_mc10, *repo_mc11, *repo_mc12, *repo_mc13, *repo_mc14, *repo_mc15, *repo_mc16, *repo_mc17, *repo_mc18, *repo_mc19, *repo_mc20, *repo_mc21, *repo_mc22;

ma_repository_t *B_repo_mc1, *B_repo_mc2, *B_repo_mc3, *B_repo_mc4, *B_repo_mc5, *B_repo_mc6, *B_repo_mc7;
void init_ut_balaji_repository_data(){
	ma_repository_create(&B_repo_mc1);
	ma_repository_set_type(B_repo_mc1, MA_REPOSITORY_TYPE_FALLBACK);
	ma_repository_set_url_type(B_repo_mc1, MA_REPOSITORY_URL_TYPE_HTTP) ;
	ma_repository_set_name(B_repo_mc1, "McAfeeHttp");
	ma_repository_set_server_fqdn(B_repo_mc1, "update.nai.com");
	ma_repository_set_enabled(B_repo_mc1, 1);
	ma_repository_set_pingtime(B_repo_mc1, 2147483647);
	ma_repository_set_subnetdistance(B_repo_mc1, 2147483647);
	ma_repository_set_state(B_repo_mc1, MA_REPOSITORY_STATE_ADDED) ;

	ma_repository_create(&B_repo_mc2);
	ma_repository_set_type(B_repo_mc2, MA_REPOSITORY_TYPE_REPO);
	ma_repository_set_url_type(B_repo_mc2, MA_REPOSITORY_URL_TYPE_HTTP) ;
	ma_repository_set_name(B_repo_mc2, "BalajiHTTP");
	ma_repository_set_server_fqdn(B_repo_mc2, "10.213.254.21");
	ma_repository_set_enabled(B_repo_mc2, 1);
	ma_repository_set_pingtime(B_repo_mc2, 30000);
	ma_repository_set_subnetdistance(B_repo_mc2, 15);
	ma_repository_set_state(B_repo_mc2, MA_REPOSITORY_STATE_ADDED) ;

	ma_repository_create(&B_repo_mc3);
	ma_repository_set_type(B_repo_mc3, MA_REPOSITORY_TYPE_REPO);
	ma_repository_set_url_type(B_repo_mc3, MA_REPOSITORY_URL_TYPE_UNC) ;
	ma_repository_set_name(B_repo_mc3, "BalajiUNC");
	ma_repository_set_server_fqdn(B_repo_mc3, "10.213.254.21");
	ma_repository_set_enabled(B_repo_mc3, 1);
	ma_repository_set_pingtime(B_repo_mc3, 30000);
	ma_repository_set_subnetdistance(B_repo_mc3, 15);
	ma_repository_set_state(B_repo_mc3, MA_REPOSITORY_STATE_ADDED) ;

	ma_repository_create(&B_repo_mc4);
	ma_repository_set_type(B_repo_mc4, MA_REPOSITORY_TYPE_REPO);
	ma_repository_set_url_type(B_repo_mc4, MA_REPOSITORY_URL_TYPE_UNC) ;
	ma_repository_set_name(B_repo_mc4, "DomainUNC");
	ma_repository_set_server_fqdn(B_repo_mc4, "10.213.254.88");
	ma_repository_set_enabled(B_repo_mc4, 1);
	ma_repository_set_pingtime(B_repo_mc4, 30000);
	ma_repository_set_subnetdistance(B_repo_mc4, 15);
	ma_repository_set_state(B_repo_mc4, MA_REPOSITORY_STATE_ADDED) ;

	ma_repository_create(&B_repo_mc5);
	ma_repository_set_type(B_repo_mc5, MA_REPOSITORY_TYPE_REPO);
	ma_repository_set_url_type(B_repo_mc5, MA_REPOSITORY_URL_TYPE_UNC) ;
	ma_repository_set_name(B_repo_mc5, "MOVERepo");
	ma_repository_set_server_fqdn(B_repo_mc5, "10.213.239.60");
	ma_repository_set_enabled(B_repo_mc5, 1);
	ma_repository_set_pingtime(B_repo_mc5, 30000);
	ma_repository_set_subnetdistance(B_repo_mc5, 15);
	ma_repository_set_state(B_repo_mc5, MA_REPOSITORY_STATE_ADDED) ;


	ma_repository_create(&B_repo_mc6);
	ma_repository_set_type(B_repo_mc6, MA_REPOSITORY_TYPE_REPO);
	ma_repository_set_url_type(B_repo_mc6, MA_REPOSITORY_URL_TYPE_FTP) ;
	ma_repository_set_name(B_repo_mc6, "DummyFTP");
	ma_repository_set_server_fqdn(B_repo_mc6, "10.213.254.21");
	ma_repository_set_enabled(B_repo_mc6, 1);
	ma_repository_set_pingtime(B_repo_mc6, 30000);
	ma_repository_set_subnetdistance(B_repo_mc6, 15);
	ma_repository_set_state(B_repo_mc6, MA_REPOSITORY_STATE_ADDED) ;

	ma_repository_create(&B_repo_mc7);
	ma_repository_set_type(B_repo_mc7, MA_REPOSITORY_TYPE_MASTER);
	ma_repository_set_url_type(B_repo_mc7, MA_REPOSITORY_URL_TYPE_SPIPE) ;
	ma_repository_set_name(B_repo_mc7, "ePO_BKS52EPO");
	ma_repository_set_server_fqdn(B_repo_mc7, "BKS52EPO");
	ma_repository_set_server_ip(B_repo_mc7, "10.213.254.46");
	ma_repository_set_enabled(B_repo_mc7, 1);
	ma_repository_set_pingtime(B_repo_mc7, 30000);
	ma_repository_set_subnetdistance(B_repo_mc7, 15);
	ma_repository_set_state(B_repo_mc7, MA_REPOSITORY_STATE_ADDED) ;

}


void init_ut_repository_data(){
/*----------------Google.com-------------*/
	
	ma_repository_create(&repo_fallback);
	ma_repository_set_type(repo_fallback, MA_REPOSITORY_TYPE_FALLBACK);
	ma_repository_set_url_type(repo_fallback, MA_REPOSITORY_URL_TYPE_SPIPE) ;
	ma_repository_set_name(repo_fallback, "fallback");
	ma_repository_set_server_name(repo_fallback, "www.naiMcafee.com");
	ma_repository_set_server_ip(repo_fallback, "www.naiMcafee.com");
	ma_repository_set_server_path(repo_fallback, "pathXXXXXXXXX");
	ma_repository_set_domain_name(repo_fallback, "domainXXXXXXXX");
	ma_repository_set_user_name(repo_fallback, "usrnameXXXXXXXXX");
	ma_repository_set_password(repo_fallback, "passwdXXXXXX");
	ma_repository_set_enabled(repo_fallback, 1);
	ma_repository_set_pingtime(repo_fallback, 30000);
	ma_repository_set_subnetdistance(repo_fallback, 15);
	ma_repository_set_state(repo_fallback, MA_REPOSITORY_STATE_ADDED) ;

	ma_repository_create(&repo_already_calculated);
	ma_repository_set_type(repo_already_calculated, MA_REPOSITORY_TYPE_REPO);
	ma_repository_set_url_type(repo_already_calculated, MA_REPOSITORY_URL_TYPE_SPIPE) ;
	ma_repository_set_name(repo_already_calculated, "repo_already_calculated");
	ma_repository_set_server_name(repo_already_calculated, "www.google.com");
	ma_repository_set_server_ip(repo_already_calculated, "www.google.com");
	ma_repository_set_server_path(repo_already_calculated, "pathXXXXXXXXX");
	ma_repository_set_domain_name(repo_already_calculated, "domainXXXXXXXX");
	ma_repository_set_user_name(repo_already_calculated, "usrnameXXXXXXXXX");
	ma_repository_set_password(repo_already_calculated, "passwdXXXXXX");
	ma_repository_set_enabled(repo_already_calculated, 1);
	ma_repository_set_pingtime(repo_already_calculated, 30000);
	ma_repository_set_subnetdistance(repo_already_calculated, 15);
	ma_repository_set_state(repo_already_calculated, MA_REPOSITORY_STATE_DEFAULT) ;

	ma_repository_create(&repo_google);
	ma_repository_set_type(repo_google, MA_REPOSITORY_TYPE_REPO);
	ma_repository_set_url_type(repo_google, MA_REPOSITORY_URL_TYPE_SPIPE) ;
	ma_repository_set_name(repo_google, "GOOGLE");
	ma_repository_set_server_name(repo_google, "www.google.com");
	ma_repository_set_server_ip(repo_google, "www.google.com");
	ma_repository_set_server_path(repo_google, "pathXXXXXXXXX");
	ma_repository_set_domain_name(repo_google, "domainXXXXXXXX");
	ma_repository_set_user_name(repo_google, "usrnameXXXXXXXXX");
	ma_repository_set_password(repo_google, "passwdXXXXXX");
	ma_repository_set_enabled(repo_google, 1);
	ma_repository_set_pingtime(repo_google, 30000);
	ma_repository_set_subnetdistance(repo_google, 15);
	ma_repository_set_state(repo_google, MA_REPOSITORY_STATE_ADDED) ;

	ma_repository_create(&repo_yahoo);
	ma_repository_set_type(repo_yahoo, MA_REPOSITORY_TYPE_REPO);
	ma_repository_set_url_type(repo_yahoo, MA_REPOSITORY_URL_TYPE_SPIPE) ;
	ma_repository_set_name(repo_yahoo, "YAHOO");
	ma_repository_set_server_name(repo_yahoo, "www.google.com");
	ma_repository_set_server_ip(repo_yahoo, "www.google.com");
	ma_repository_set_server_path(repo_yahoo, "pathXXXXXXXXX");
	ma_repository_set_domain_name(repo_yahoo, "domainXXXXXXXX");
	ma_repository_set_user_name(repo_yahoo, "usrnameXXXXXXXXX");
	ma_repository_set_password(repo_yahoo, "passwdXXXXXX");
	ma_repository_set_enabled(repo_yahoo, 1);
	ma_repository_set_pingtime(repo_yahoo, 30000);
	ma_repository_set_subnetdistance(repo_yahoo, 15);
	ma_repository_set_state(repo_yahoo, MA_REPOSITORY_STATE_ADDED) ;

	ma_repository_create(&repo_mc);
	ma_repository_set_type(repo_mc, MA_REPOSITORY_TYPE_REPO);
	ma_repository_set_url_type(repo_mc, MA_REPOSITORY_URL_TYPE_SPIPE) ;
	ma_repository_set_name(repo_mc, "McAfee");
	ma_repository_set_server_name(repo_mc, "www.mileaccess.com");
	ma_repository_set_server_ip(repo_yahoo, "www.mileaccess.com");
	ma_repository_set_server_path(repo_mc, "pathXXXXXXXXX");
	ma_repository_set_domain_name(repo_mc, "domainXXXXXXXX");
	ma_repository_set_user_name(repo_mc, "usrnameXXXXXXXXX");
	ma_repository_set_password(repo_mc, "passwdXXXXXX");
	ma_repository_set_enabled(repo_mc, 1);
	ma_repository_set_pingtime(repo_mc, 30000);
	ma_repository_set_subnetdistance(repo_mc, 15);
	ma_repository_set_state(repo_mc, MA_REPOSITORY_STATE_ADDED) ;


	ma_repository_create(&repo_local);
	ma_repository_set_type(repo_local, MA_REPOSITORY_TYPE_REPO);
	ma_repository_set_url_type(repo_local, MA_REPOSITORY_URL_TYPE_SPIPE) ;
	ma_repository_set_name(repo_local, "local url");
	ma_repository_set_server_name(repo_local, "10.213.5.57");
	//ma_repository_set_server_name(repo_local, "10.213.254.14");
	//ma_repository_set_server_ip(repo_yahoo, "1.1.1.1");
	ma_repository_set_server_path(repo_local, "pathXXXXXXXXX");
	ma_repository_set_domain_name(repo_local, "domainXXXXXXXX");
	ma_repository_set_user_name(repo_local, "usrnameXXXXXXXXX");
	ma_repository_set_password(repo_local, "passwdXXXXXX");
	ma_repository_set_enabled(repo_local, 1);
	ma_repository_set_pingtime(repo_local, 30000);
	ma_repository_set_subnetdistance(repo_local, 15);
	ma_repository_set_state(repo_local, MA_REPOSITORY_STATE_ADDED) ;

	ma_repository_create(&repo_mc1);
	ma_repository_set_type(repo_mc1, MA_REPOSITORY_TYPE_REPO);
	ma_repository_set_url_type(repo_mc1, MA_REPOSITORY_URL_TYPE_SPIPE) ;
	ma_repository_set_name(repo_mc1, "McAfee1");
	ma_repository_set_server_name(repo_mc1, "10.213.5.57");
	//ma_repository_set_server_ip(repo_mc1, "2.2.2.2");
	ma_repository_set_server_path(repo_mc1, "pathXXXXXXXXX");
	ma_repository_set_domain_name(repo_mc1, "domainXXXXXXXX");
	ma_repository_set_user_name(repo_mc1, "usrnameXXXXXXXXX");
	ma_repository_set_password(repo_mc1, "passwdXXXXXX");
	ma_repository_set_enabled(repo_mc1, 1);
	ma_repository_set_pingtime(repo_mc1, 30000);
	ma_repository_set_subnetdistance(repo_mc1, 15);
	ma_repository_set_state(repo_mc1, MA_REPOSITORY_STATE_ADDED) ;

	ma_repository_create(&repo_mc2);
	ma_repository_set_type(repo_mc2, MA_REPOSITORY_TYPE_REPO);
	ma_repository_set_url_type(repo_mc2, MA_REPOSITORY_URL_TYPE_SPIPE) ;
	ma_repository_set_name(repo_mc2, "McAfee2");
	ma_repository_set_server_name(repo_mc2, "10.213.5.57");
	ma_repository_set_server_path(repo_mc2, "pathXXXXXXXXX");
	ma_repository_set_domain_name(repo_mc2, "domainXXXXXXXX");
	ma_repository_set_user_name(repo_mc2, "usrnameXXXXXXXXX");
	ma_repository_set_password(repo_mc2, "passwdXXXXXX");
	ma_repository_set_enabled(repo_mc2, 1);
	ma_repository_set_pingtime(repo_mc2, 30000);
	ma_repository_set_subnetdistance(repo_mc2, 15);
	ma_repository_set_state(repo_mc2, MA_REPOSITORY_STATE_ADDED) ;

	ma_repository_create(&repo_mc3);
	ma_repository_set_type(repo_mc3, MA_REPOSITORY_TYPE_REPO);
	ma_repository_set_url_type(repo_mc3, MA_REPOSITORY_URL_TYPE_SPIPE) ;
	ma_repository_set_name(repo_mc3, "McAfee3");
	ma_repository_set_server_name(repo_mc3, "www.mileaccess3.com");
	//ma_repository_set_server_ip(repo_mc3, "3.3.3.3");
	ma_repository_set_server_path(repo_mc3, "pathXXXXXXXXX");
	ma_repository_set_domain_name(repo_mc3, "domainXXXXXXXX");
	ma_repository_set_user_name(repo_mc3, "usrnameXXXXXXXXX");
	ma_repository_set_password(repo_mc3, "passwdXXXXXX");
	ma_repository_set_enabled(repo_mc3, 1);
	ma_repository_set_pingtime(repo_mc3, 30000);
	ma_repository_set_subnetdistance(repo_mc3, 15);
	ma_repository_set_state(repo_mc3, MA_REPOSITORY_STATE_ADDED) ;


	ma_repository_create(&repo_mc4);
	ma_repository_set_type(repo_mc4, MA_REPOSITORY_TYPE_REPO);
	ma_repository_set_url_type(repo_mc4, MA_REPOSITORY_URL_TYPE_SPIPE) ;
	ma_repository_set_name(repo_mc4, "McAfee4");
	ma_repository_set_server_name(repo_mc4, "www.mileaccess4.com");
	//ma_repository_set_server_ip(repo_yahoo, "1.1.1.1");
	ma_repository_set_server_path(repo_mc4, "pathXXXXXXXXX");
	ma_repository_set_domain_name(repo_mc4, "domainXXXXXXXX");
	ma_repository_set_user_name(repo_mc4, "usrnameXXXXXXXXX");
	ma_repository_set_password(repo_mc4, "passwdXXXXXX");
	ma_repository_set_enabled(repo_mc4, 1);
	ma_repository_set_pingtime(repo_mc4, 30000);
	ma_repository_set_subnetdistance(repo_mc4, 15);
	ma_repository_set_state(repo_mc4, MA_REPOSITORY_STATE_ADDED) ;


	ma_repository_create(&repo_mc5);
	ma_repository_set_type(repo_mc5, MA_REPOSITORY_TYPE_REPO);
	ma_repository_set_url_type(repo_mc5, MA_REPOSITORY_URL_TYPE_SPIPE) ;
	ma_repository_set_name(repo_mc5, "zinga");
	ma_repository_set_server_name(repo_mc5, "www.zinga.com");
	ma_repository_set_server_ip(repo_yahoo, "1.1.1.1");
	ma_repository_set_server_path(repo_mc5, "pathXXXXXXXXX");
	ma_repository_set_domain_name(repo_mc5, "domainXXXXXXXX");
	ma_repository_set_user_name(repo_mc5, "usrnameXXXXXXXXX");
	ma_repository_set_password(repo_mc5, "passwdXXXXXX");
	ma_repository_set_enabled(repo_mc5, 1);
	ma_repository_set_pingtime(repo_mc5, 30000);
	ma_repository_set_subnetdistance(repo_mc5, 15);
	ma_repository_set_state(repo_mc5, MA_REPOSITORY_STATE_ADDED) ;


	ma_repository_create(&repo_mc6);
	ma_repository_set_type(repo_mc6, MA_REPOSITORY_TYPE_REPO);
	ma_repository_set_url_type(repo_mc6, MA_REPOSITORY_URL_TYPE_SPIPE) ;
	ma_repository_set_name(repo_mc6, "visa");
	ma_repository_set_server_name(repo_mc6, "www.visa.com");
	ma_repository_set_server_ip(repo_yahoo, "1.1.1.1");
	ma_repository_set_server_path(repo_mc6, "pathXXXXXXXXX");
	ma_repository_set_domain_name(repo_mc6, "domainXXXXXXXX");
	ma_repository_set_user_name(repo_mc6, "usrnameXXXXXXXXX");
	ma_repository_set_password(repo_mc6, "passwdXXXXXX");
	ma_repository_set_enabled(repo_mc6, 1);
	ma_repository_set_pingtime(repo_mc6, 30000);
	ma_repository_set_subnetdistance(repo_mc6, 15);
	ma_repository_set_state(repo_mc6, MA_REPOSITORY_STATE_ADDED) ;


	ma_repository_create(&repo_mc7);
	ma_repository_set_type(repo_mc7, MA_REPOSITORY_TYPE_REPO);
	ma_repository_set_url_type(repo_mc7, MA_REPOSITORY_URL_TYPE_SPIPE) ;
	ma_repository_set_name(repo_mc7, "nsa");
	ma_repository_set_server_name(repo_mc7, "www.nsa.com");
	ma_repository_set_server_ip(repo_yahoo, "1.1.1.1");
	ma_repository_set_server_path(repo_mc7, "pathXXXXXXXXX");
	ma_repository_set_domain_name(repo_mc7, "domainXXXXXXXX");
	ma_repository_set_user_name(repo_mc7, "usrnameXXXXXXXXX");
	ma_repository_set_password(repo_mc7, "passwdXXXXXX");
	ma_repository_set_enabled(repo_mc7, 1);
	ma_repository_set_pingtime(repo_mc7, 30000);
	ma_repository_set_subnetdistance(repo_mc7, 15);
	ma_repository_set_state(repo_mc7, MA_REPOSITORY_STATE_ADDED) ;


	ma_repository_create(&repo_mc8);
	ma_repository_set_type(repo_mc8, MA_REPOSITORY_TYPE_REPO);
	ma_repository_set_url_type(repo_mc8, MA_REPOSITORY_URL_TYPE_SPIPE) ;
	ma_repository_set_name(repo_mc8, "ebay");
	ma_repository_set_server_name(repo_mc8, "www.ebay.com");
	ma_repository_set_server_ip(repo_yahoo, "1.1.1.1");
	ma_repository_set_server_path(repo_mc8, "pathXXXXXXXXX");
	ma_repository_set_domain_name(repo_mc8, "domainXXXXXXXX");
	ma_repository_set_user_name(repo_mc8, "usrnameXXXXXXXXX");
	ma_repository_set_password(repo_mc8, "passwdXXXXXX");
	ma_repository_set_enabled(repo_mc8, 1);
	ma_repository_set_pingtime(repo_mc8, 30000);
	ma_repository_set_subnetdistance(repo_mc8, 15);
	ma_repository_set_state(repo_mc8, MA_REPOSITORY_STATE_ADDED) ;


	ma_repository_create(&repo_mc9);
	ma_repository_set_type(repo_mc9, MA_REPOSITORY_TYPE_REPO);
	ma_repository_set_url_type(repo_mc9, MA_REPOSITORY_URL_TYPE_SPIPE) ;
	ma_repository_set_name(repo_mc9, "paypal");
	ma_repository_set_server_name(repo_mc9, "www.paypal.com");
	ma_repository_set_server_ip(repo_yahoo, "1.1.1.1");
	ma_repository_set_server_path(repo_mc9, "pathXXXXXXXXX");
	ma_repository_set_domain_name(repo_mc9, "domainXXXXXXXX");
	ma_repository_set_user_name(repo_mc9, "usrnameXXXXXXXXX");
	ma_repository_set_password(repo_mc9, "passwdXXXXXX");
	ma_repository_set_enabled(repo_mc9, 1);
	ma_repository_set_pingtime(repo_mc9, 30000);
	ma_repository_set_subnetdistance(repo_mc9, 15);
	ma_repository_set_state(repo_mc9, MA_REPOSITORY_STATE_ADDED) ;

	ma_repository_create(&repo_mc10);
	ma_repository_set_type(repo_mc10, MA_REPOSITORY_TYPE_REPO);
	ma_repository_set_url_type(repo_mc10, MA_REPOSITORY_URL_TYPE_SPIPE) ;
	ma_repository_set_name(repo_mc10, "irctc");
	ma_repository_set_server_name(repo_mc10, "www.irctc.com");
	ma_repository_set_server_ip(repo_yahoo, "1.1.1.1");
	ma_repository_set_server_path(repo_mc10, "pathXXXXXXXXX");
	ma_repository_set_domain_name(repo_mc10, "domainXXXXXXXX");
	ma_repository_set_user_name(repo_mc10, "usrnameXXXXXXXXX");
	ma_repository_set_password(repo_mc10, "passwdXXXXXX");
	ma_repository_set_enabled(repo_mc10, 1);
	ma_repository_set_pingtime(repo_mc10, 30000);
	ma_repository_set_subnetdistance(repo_mc10, 15);
	ma_repository_set_state(repo_mc10, MA_REPOSITORY_STATE_ADDED) ;

	ma_repository_create(&repo_mc11);
	ma_repository_set_type(repo_mc11, MA_REPOSITORY_TYPE_REPO);
	ma_repository_set_url_type(repo_mc11, MA_REPOSITORY_URL_TYPE_SPIPE) ;
	ma_repository_set_name(repo_mc11, "a");
	ma_repository_set_server_name(repo_mc11, "www.a.com");
	ma_repository_set_server_ip(repo_yahoo, "1.1.1.1");
	ma_repository_set_server_path(repo_mc11, "pathXXXXXXXXX");
	ma_repository_set_domain_name(repo_mc11, "domainXXXXXXXX");
	ma_repository_set_user_name(repo_mc11, "usrnameXXXXXXXXX");
	ma_repository_set_password(repo_mc11, "passwdXXXXXX");
	ma_repository_set_enabled(repo_mc11, 1);
	ma_repository_set_pingtime(repo_mc11, 30000);
	ma_repository_set_subnetdistance(repo_mc11, 15);
	ma_repository_set_state(repo_mc11, MA_REPOSITORY_STATE_ADDED) ;

	ma_repository_create(&repo_mc12);
	ma_repository_set_type(repo_mc12, MA_REPOSITORY_TYPE_REPO);
	ma_repository_set_url_type(repo_mc12, MA_REPOSITORY_URL_TYPE_SPIPE) ;
	ma_repository_set_name(repo_mc12, "b");
	ma_repository_set_server_name(repo_mc12, "www.b.com");
	ma_repository_set_server_ip(repo_yahoo, "1.1.1.1");
	ma_repository_set_server_path(repo_mc12, "pathXXXXXXXXX");
	ma_repository_set_domain_name(repo_mc12, "domainXXXXXXXX");
	ma_repository_set_user_name(repo_mc12, "usrnameXXXXXXXXX");
	ma_repository_set_password(repo_mc12, "passwdXXXXXX");
	ma_repository_set_enabled(repo_mc12, 1);
	ma_repository_set_pingtime(repo_mc12, 30000);
	ma_repository_set_subnetdistance(repo_mc12, 15);
	ma_repository_set_state(repo_mc12, MA_REPOSITORY_STATE_ADDED) ;

	ma_repository_create(&repo_mc13);
	ma_repository_set_type(repo_mc13, MA_REPOSITORY_TYPE_REPO);
	ma_repository_set_url_type(repo_mc13, MA_REPOSITORY_URL_TYPE_SPIPE) ;
	ma_repository_set_name(repo_mc13, "i1");
	ma_repository_set_server_name(repo_mc13, "www.i1.com");
	ma_repository_set_server_ip(repo_yahoo, "1.1.1.1");
	ma_repository_set_server_path(repo_mc13, "pathXXXXXXXXX");
	ma_repository_set_domain_name(repo_mc13, "domainXXXXXXXX");
	ma_repository_set_user_name(repo_mc13, "usrnameXXXXXXXXX");
	ma_repository_set_password(repo_mc13, "passwdXXXXXX");
	ma_repository_set_enabled(repo_mc13, 1);
	ma_repository_set_pingtime(repo_mc13, 30000);
	ma_repository_set_subnetdistance(repo_mc13, 15);
	ma_repository_set_state(repo_mc13, MA_REPOSITORY_STATE_ADDED) ;

	ma_repository_create(&repo_mc14);
	ma_repository_set_type(repo_mc14, MA_REPOSITORY_TYPE_REPO);
	ma_repository_set_url_type(repo_mc14, MA_REPOSITORY_URL_TYPE_SPIPE) ;
	ma_repository_set_name(repo_mc14, "i2");
	ma_repository_set_server_name(repo_mc14, "www.i2.com");
	ma_repository_set_server_ip(repo_yahoo, "1.1.1.1");
	ma_repository_set_server_path(repo_mc14, "pathXXXXXXXXX");
	ma_repository_set_domain_name(repo_mc14, "domainXXXXXXXX");
	ma_repository_set_user_name(repo_mc14, "usrnameXXXXXXXXX");
	ma_repository_set_password(repo_mc14, "passwdXXXXXX");
	ma_repository_set_enabled(repo_mc14, 1);
	ma_repository_set_pingtime(repo_mc14, 30000);
	ma_repository_set_subnetdistance(repo_mc14, 15);
	ma_repository_set_state(repo_mc14, MA_REPOSITORY_STATE_ADDED) ;

	ma_repository_create(&repo_mc15);
	ma_repository_set_type(repo_mc15, MA_REPOSITORY_TYPE_REPO);
	ma_repository_set_url_type(repo_mc15, MA_REPOSITORY_URL_TYPE_SPIPE) ;
	ma_repository_set_name(repo_mc15, "jda");
	ma_repository_set_server_name(repo_mc15, "www.jda.com");
	ma_repository_set_server_ip(repo_yahoo, "1.1.1.1");
	ma_repository_set_server_path(repo_mc15, "pathXXXXXXXXX");
	ma_repository_set_domain_name(repo_mc15, "domainXXXXXXXX");
	ma_repository_set_user_name(repo_mc15, "usrnameXXXXXXXXX");
	ma_repository_set_password(repo_mc15, "passwdXXXXXX");
	ma_repository_set_enabled(repo_mc15, 1);
	ma_repository_set_pingtime(repo_mc15, 30000);
	ma_repository_set_subnetdistance(repo_mc15, 15);
	ma_repository_set_state(repo_mc15, MA_REPOSITORY_STATE_ADDED) ;

	ma_repository_create(&repo_mc16);
	ma_repository_set_type(repo_mc16, MA_REPOSITORY_TYPE_REPO);
	ma_repository_set_url_type(repo_mc16, MA_REPOSITORY_URL_TYPE_SPIPE) ;
	ma_repository_set_name(repo_mc16, "lda");
	ma_repository_set_server_name(repo_mc16, "www.lda.com");
	ma_repository_set_server_ip(repo_yahoo, "1.1.1.1");
	ma_repository_set_server_path(repo_mc16, "pathXXXXXXXXX");
	ma_repository_set_domain_name(repo_mc16, "domainXXXXXXXX");
	ma_repository_set_user_name(repo_mc16, "usrnameXXXXXXXXX");
	ma_repository_set_password(repo_mc16, "passwdXXXXXX");
	ma_repository_set_enabled(repo_mc16, 1);
	ma_repository_set_pingtime(repo_mc16, 30000);
	ma_repository_set_subnetdistance(repo_mc16, 15);
	ma_repository_set_state(repo_mc16, MA_REPOSITORY_STATE_ADDED) ;

	ma_repository_create(&repo_mc17);
	ma_repository_set_type(repo_mc17, MA_REPOSITORY_TYPE_REPO);
	ma_repository_set_url_type(repo_mc17, MA_REPOSITORY_URL_TYPE_SPIPE) ;
	ma_repository_set_name(repo_mc17, "ms");
	ma_repository_set_server_name(repo_mc17, "www.ms.com");
	ma_repository_set_server_ip(repo_yahoo, "1.1.1.1");
	ma_repository_set_server_path(repo_mc17, "pathXXXXXXXXX");
	ma_repository_set_domain_name(repo_mc17, "domainXXXXXXXX");
	ma_repository_set_user_name(repo_mc17, "usrnameXXXXXXXXX");
	ma_repository_set_password(repo_mc17, "passwdXXXXXX");
	ma_repository_set_enabled(repo_mc17, 1);
	ma_repository_set_pingtime(repo_mc17, 30000);
	ma_repository_set_subnetdistance(repo_mc17, 15);
	ma_repository_set_state(repo_mc17, MA_REPOSITORY_STATE_ADDED) ;

	ma_repository_create(&repo_mc18);
	ma_repository_set_type(repo_mc18, MA_REPOSITORY_TYPE_REPO);
	ma_repository_set_url_type(repo_mc18, MA_REPOSITORY_URL_TYPE_SPIPE) ;
	ma_repository_set_name(repo_mc18, "nonon");
	ma_repository_set_server_name(repo_mc18, "www.nonon.com");
	ma_repository_set_server_ip(repo_yahoo, "1.1.1.1");
	ma_repository_set_server_path(repo_mc18, "pathXXXXXXXXX");
	ma_repository_set_domain_name(repo_mc18, "domainXXXXXXXX");
	ma_repository_set_user_name(repo_mc18, "usrnameXXXXXXXXX");
	ma_repository_set_password(repo_mc18, "passwdXXXXXX");
	ma_repository_set_enabled(repo_mc18, 1);
	ma_repository_set_pingtime(repo_mc18, 30000);
	ma_repository_set_subnetdistance(repo_mc18, 15);
	ma_repository_set_state(repo_mc18, MA_REPOSITORY_STATE_ADDED) ;


	ma_repository_create(&repo_mc19);
	ma_repository_set_type(repo_mc19, MA_REPOSITORY_TYPE_REPO);
	ma_repository_set_url_type(repo_mc19, MA_REPOSITORY_URL_TYPE_SPIPE) ;
	ma_repository_set_name(repo_mc19, "yes");
	ma_repository_set_server_name(repo_mc19, "www.yes.com");
	ma_repository_set_server_ip(repo_yahoo, "1.1.1.1");
	ma_repository_set_server_path(repo_mc19, "pathXXXXXXXXX");
	ma_repository_set_domain_name(repo_mc19, "domainXXXXXXXX");
	ma_repository_set_user_name(repo_mc19, "usrnameXXXXXXXXX");
	ma_repository_set_password(repo_mc19, "passwdXXXXXX");
	ma_repository_set_enabled(repo_mc19, 1);
	ma_repository_set_pingtime(repo_mc19, 30000);
	ma_repository_set_subnetdistance(repo_mc19, 15);
	ma_repository_set_state(repo_mc19, MA_REPOSITORY_STATE_ADDED) ;


	ma_repository_create(&repo_mc20);
	ma_repository_set_type(repo_mc20, MA_REPOSITORY_TYPE_REPO);
	ma_repository_set_url_type(repo_mc20, MA_REPOSITORY_URL_TYPE_SPIPE) ;
	ma_repository_set_name(repo_mc20, "no");
	ma_repository_set_server_name(repo_mc20, "www.no.com");
	ma_repository_set_server_ip(repo_yahoo, "1.1.1.1");
	ma_repository_set_server_path(repo_mc20, "pathXXXXXXXXX");
	ma_repository_set_domain_name(repo_mc20, "domainXXXXXXXX");
	ma_repository_set_user_name(repo_mc20, "usrnameXXXXXXXXX");
	ma_repository_set_password(repo_mc20, "passwdXXXXXX");
	ma_repository_set_enabled(repo_mc20, 1);
	ma_repository_set_pingtime(repo_mc20, 30000);
	ma_repository_set_subnetdistance(repo_mc20, 15);
	ma_repository_set_state(repo_mc20, MA_REPOSITORY_STATE_ADDED) ;


	ma_repository_create(&repo_mc21);
	ma_repository_set_type(repo_mc21, MA_REPOSITORY_TYPE_REPO);
	ma_repository_set_url_type(repo_mc21, MA_REPOSITORY_URL_TYPE_SPIPE) ;
	ma_repository_set_name(repo_mc21, "krishna");
	ma_repository_set_server_name(repo_mc21, "www.krishna.com");
	ma_repository_set_server_ip(repo_yahoo, "1.1.1.1");
	ma_repository_set_server_path(repo_mc21, "pathXXXXXXXXX");
	ma_repository_set_domain_name(repo_mc21, "domainXXXXXXXX");
	ma_repository_set_user_name(repo_mc21, "usrnameXXXXXXXXX");
	ma_repository_set_password(repo_mc21, "passwdXXXXXX");
	ma_repository_set_enabled(repo_mc21, 1);
	ma_repository_set_pingtime(repo_mc21, 30000);
	ma_repository_set_subnetdistance(repo_mc21, 15);
	ma_repository_set_state(repo_mc21, MA_REPOSITORY_STATE_ADDED) ;


	ma_repository_create(&repo_mc22);
	ma_repository_set_type(repo_mc22, MA_REPOSITORY_TYPE_REPO);
	ma_repository_set_url_type(repo_mc22, MA_REPOSITORY_URL_TYPE_SPIPE) ;
	ma_repository_set_name(repo_mc22, "bt");
	ma_repository_set_server_name(repo_mc22, "www.bt.com");
	ma_repository_set_server_ip(repo_yahoo, "1.1.1.1");
	ma_repository_set_server_path(repo_mc22, "pathXXXXXXXXX");
	ma_repository_set_domain_name(repo_mc22, "domainXXXXXXXX");
	ma_repository_set_user_name(repo_mc22, "usrnameXXXXXXXXX");
	ma_repository_set_password(repo_mc22, "passwdXXXXXX");
	ma_repository_set_enabled(repo_mc22, 1);
	ma_repository_set_pingtime(repo_mc22, 30000);
	ma_repository_set_subnetdistance(repo_mc22, 15);
	ma_repository_set_state(repo_mc22, MA_REPOSITORY_STATE_ADDED) ;

	ma_repository_create(&nodomain);
	ma_repository_set_type(nodomain, MA_REPOSITORY_TYPE_REPO);
	ma_repository_set_url_type(nodomain, MA_REPOSITORY_URL_TYPE_SPIPE) ;
	ma_repository_set_name(nodomain, "nodomain");
	ma_repository_set_server_name(nodomain, "www.nodomain1122ccc.com");
	ma_repository_set_server_path(nodomain, "pathXXXXXXXXX");
	ma_repository_set_domain_name(nodomain, "domainXXXXXXXX");
	ma_repository_set_user_name(nodomain, "usrnameXXXXXXXXX");
	ma_repository_set_password(nodomain, "passwdXXXXXX");
	ma_repository_set_enabled(nodomain, 1);
	ma_repository_set_pingtime(nodomain, 30000);
	ma_repository_set_subnetdistance(nodomain, 15);
	ma_repository_set_state(nodomain, MA_REPOSITORY_STATE_ADDED) ;

	

}

#if 0
ma_repository_t repo_yahoo = {0};
ma_repository_t repo_mileaccess = {0};
ma_repository_t repo1 = {0};
ma_repository_t repo2 = {0};
ma_repository_t repo3 = {0};
ma_repository_t repo4 = {0};
ma_repository_t repo5 = {0};
ma_repository_t repo6 = {0};
ma_repository_t repo7 = {0};
ma_repository_t repo8 = {0};
ma_repository_t repo9 = {0};
ma_repository_t repo10 = {0};
ma_repository_t repo11 = {0};
ma_repository_t repo12 = {0};
ma_repository_t repo13 = {0};
ma_repository_t repo14 = {0};
ma_repository_t repo15 = {0};

ma_repository_t repo16 = {0};
ma_repository_t repo17 = {0};
ma_repository_t repo18= {0};
ma_repository_t repo19 = {0};
ma_repository_t repo20= {0};
ma_repository_t repo21 = {0};
ma_repository_t repo22 = {0};
ma_repository_t repo23= {0};
ma_repository_t repo24= {0};
ma_repository_t repo25 = {0};
ma_repository_t repo26 = {0};
ma_repository_t repo27 = {0};
ma_repository_t repo28 = {0};
ma_repository_t repo29 = {0};
ma_repository_t repo30 = {0};

void init_ut_db(){
/*----------------Google.com-------------*/
	repo_google.name = "google";
	repo_google.repo_type = MA_REPOSITORY_TYPE_MASTER;
	repo_google.url_type = MA_REPOSITORY_TYPE_HTTP;
	repo_google.sever = "www.google.com";
	repo_google.repo_namespace = MA_REPOSITORY_NAMESPACE_GLOBAL;
	repo_google.enabled = 1;
	repo_google.path = "Junk Path";
	repo_google.auth_rqrd = 0;
	repo_google.use_loggedon_account = 0;
	repo_google.is_passwd_encrypted = 0;
	repo_google.ping_count = -1;
	repo_google.subnet_distance = -1;
	repo_google.sitelist_order = 0;

/*----------------yahoo.com-------------*/
	repo_yahoo.name = "yahoo";
	repo_yahoo.repo_type = MA_REPOSITORY_TYPE_MASTER;
	repo_yahoo.url_type = MA_REPOSITORY_TYPE_HTTP;
	repo_yahoo.sever = "www.yahoo.com";
	repo_yahoo.repo_namespace = MA_REPOSITORY_NAMESPACE_GLOBAL;
	repo_yahoo.enabled = 1;
	repo_yahoo.path = "Junk Path";
	repo_yahoo.auth_rqrd = 0;
	repo_yahoo.use_loggedon_account = 0;
	repo_yahoo.is_passwd_encrypted = 0;
	repo_yahoo.ping_count = -1;
	repo_yahoo.subnet_distance = -1;
	repo_yahoo.sitelist_order = 0;

/*----------------mileaccess.com-------------*/
	repo_mileaccess.name = "mileaccess";
	repo_mileaccess.repo_type = MA_REPOSITORY_TYPE_MASTER;
	repo_mileaccess.url_type = MA_REPOSITORY_TYPE_HTTP;
	repo_mileaccess.sever = "www.mileaccess.com";
	repo_mileaccess.repo_namespace = MA_REPOSITORY_NAMESPACE_GLOBAL;
	repo_mileaccess.enabled = 1;
	repo_mileaccess.path = "Junk Path";
	repo_mileaccess.auth_rqrd = 0;
	repo_mileaccess.use_loggedon_account = 0;
	repo_mileaccess.is_passwd_encrypted = 0;
	repo_mileaccess.ping_count = -1;
	repo_mileaccess.subnet_distance = -1;
	repo_mileaccess.sitelist_order = 0;

/*----------------mileaccess1.com-------------*/
	repo1.name = "mileaccess1";
	repo1.repo_type = MA_REPOSITORY_TYPE_MASTER;
	repo1.url_type = MA_REPOSITORY_TYPE_HTTP;
	repo1.sever = "www.mileaccess1.com";
	repo1.repo_namespace = MA_REPOSITORY_NAMESPACE_GLOBAL;
	repo1.enabled = 1;
	repo1.path = "Junk Path";
	repo1.auth_rqrd = 0;
	repo1.use_loggedon_account = 0;
	repo1.is_passwd_encrypted = 0;
	repo1.ping_count = -1;
	repo1.subnet_distance = -1;
	repo1.sitelist_order = 0;

/*----------------zinga.com-------------*/
	repo2.name = "zinga";
	repo2.repo_type = MA_REPOSITORY_TYPE_MASTER;
	repo2.url_type = MA_REPOSITORY_TYPE_HTTP;
	repo2.sever = "www.zinga.com";
	repo2.repo_namespace = MA_REPOSITORY_NAMESPACE_GLOBAL;
	repo2.enabled = 1;
	repo2.path = "Junk Path";
	repo2.auth_rqrd = 0;
	repo2.use_loggedon_account = 0;
	repo2.is_passwd_encrypted = 0;
	repo2.ping_count = -1;
	repo2.subnet_distance = -1;
	repo2.sitelist_order = 0;

/*----------------ebay.com-------------*/
	repo3.name = "ebay";
	repo3.repo_type = MA_REPOSITORY_TYPE_MASTER;
	repo3.url_type = MA_REPOSITORY_TYPE_HTTP;
	repo3.sever = "www.ebay.com";
	repo3.repo_namespace = MA_REPOSITORY_NAMESPACE_GLOBAL;
	repo3.enabled = 1;
	repo3.path = "Junk Path";
	repo3.auth_rqrd = 0;
	repo3.use_loggedon_account = 0;
	repo3.is_passwd_encrypted = 0;
	repo3.ping_count = -1;
	repo3.subnet_distance = -1;
	repo3.sitelist_order = 0;

	/*----------------ms.com-------------*/
	repo4.name = "ms";
	repo4.repo_type = MA_REPOSITORY_TYPE_MASTER;
	repo4.url_type = MA_REPOSITORY_TYPE_HTTP;
	repo4.sever = "www.ms.com";
	repo4.repo_namespace = MA_REPOSITORY_NAMESPACE_GLOBAL;
	repo4.enabled = 1;
	repo4.path = "Junk Path";
	repo4.auth_rqrd = 0;
	repo4.use_loggedon_account = 0;
	repo4.is_passwd_encrypted = 0;
	repo4.ping_count = -1;
	repo4.subnet_distance = -1;
	repo4.sitelist_order = 0;

	/*----------------gs.com-------------*/
	repo5.name = "gs";
	repo5.repo_type = MA_REPOSITORY_TYPE_MASTER;
	repo5.url_type = MA_REPOSITORY_TYPE_HTTP;
	repo5.sever = "www.gs.com";
	repo5.repo_namespace = MA_REPOSITORY_NAMESPACE_GLOBAL;
	repo5.enabled = 1;
	repo5.path = "Junk Path";
	repo5.auth_rqrd = 0;
	repo5.use_loggedon_account = 0;
	repo5.is_passwd_encrypted = 0;
	repo5.ping_count = -1;
	repo5.subnet_distance = -1;
	repo5.sitelist_order = 0;

	/*----------------abc.com-------------*/
	repo6.name = "abc";
	repo6.repo_type = MA_REPOSITORY_TYPE_MASTER;
	repo6.url_type = MA_REPOSITORY_TYPE_HTTP;
	repo6.sever = "www.abc.com";
	repo6.repo_namespace = MA_REPOSITORY_NAMESPACE_GLOBAL;
	repo6.enabled = 1;
	repo6.path = "Junk Path";
	repo6.auth_rqrd = 0;
	repo6.use_loggedon_account = 0;
	repo6.is_passwd_encrypted = 0;
	repo6.ping_count = -1;
	repo6.subnet_distance = -1;
	repo6.sitelist_order = 0;

	/*----------------facebook.com-------------*/
	repo7.name = "facebook";
	repo7.repo_type = MA_REPOSITORY_TYPE_MASTER;
	repo7.url_type = MA_REPOSITORY_TYPE_HTTP;
	repo7.sever = "www.facebook.com";
	repo7.repo_namespace = MA_REPOSITORY_NAMESPACE_GLOBAL;
	repo7.enabled = 1;
	repo7.path = "Junk Path";
	repo7.auth_rqrd = 0;
	repo7.use_loggedon_account = 0;
	repo7.is_passwd_encrypted = 0;
	repo7.ping_count = -1;
	repo7.subnet_distance = -1;
	repo7.sitelist_order = 0;

	/*----------------gmail.com-------------*/
	repo8.name = "gmail";
	repo8.repo_type = MA_REPOSITORY_TYPE_MASTER;
	repo8.url_type = MA_REPOSITORY_TYPE_HTTP;
	repo8.sever = "www.gmail.com";
	repo8.repo_namespace = MA_REPOSITORY_NAMESPACE_GLOBAL;
	repo8.enabled = 1;
	repo8.path = "Junk Path";
	repo8.auth_rqrd = 0;
	repo8.use_loggedon_account = 0;
	repo8.is_passwd_encrypted = 0;
	repo8.ping_count = -1;
	repo8.subnet_distance = -1;
	repo8.sitelist_order = 0;

	/*----------------sony.com-------------*/
	repo9.name = "sony";
	repo9.repo_type = MA_REPOSITORY_TYPE_MASTER;
	repo9.url_type = MA_REPOSITORY_TYPE_HTTP;
	repo9.sever = "www.sony.com";
	repo9.repo_namespace = MA_REPOSITORY_NAMESPACE_GLOBAL;
	repo9.enabled = 1;
	repo9.path = "Junk Path";
	repo9.auth_rqrd = 0;
	repo9.use_loggedon_account = 0;
	repo9.is_passwd_encrypted = 0;
	repo9.ping_count = -1;
	repo9.subnet_distance = -1;
	repo9.sitelist_order = 0;

	/*----------------Invalid.com-------------*/
	repo10.name = "Invalid";
	repo10.repo_type = MA_REPOSITORY_TYPE_MASTER;
	repo10.url_type = MA_REPOSITORY_TYPE_HTTP;
	repo10.sever = "www.Invalid.com";
	repo10.repo_namespace = MA_REPOSITORY_NAMESPACE_GLOBAL;
	repo10.enabled = 1;
	repo10.path = "Junk Path";
	repo10.auth_rqrd = 0;
	repo10.use_loggedon_account = 0;
	repo10.is_passwd_encrypted = 0;
	repo10.ping_count = -1;
	repo10.subnet_distance = -1;
	repo10.sitelist_order = 0;

	/*----------------apple.com-------------*/
	repo11.name = "apple";
	repo11.repo_type = MA_REPOSITORY_TYPE_MASTER;
	repo11.url_type = MA_REPOSITORY_TYPE_HTTP;
	repo11.sever = "www.apple.com";
	repo11.repo_namespace = MA_REPOSITORY_NAMESPACE_GLOBAL;
	repo11.enabled = 1;
	repo11.path = "Junk Path";
	repo11.auth_rqrd = 0;
	repo11.use_loggedon_account = 0;
	repo11.is_passwd_encrypted = 0;
	repo11.ping_count = -1;
	repo11.subnet_distance = -1;
	repo11.sitelist_order = 0;

	/*----------------i2.com-------------*/
	repo12.name = "i2";
	repo12.repo_type = MA_REPOSITORY_TYPE_MASTER;
	repo12.url_type = MA_REPOSITORY_TYPE_HTTP;
	repo12.sever = "www.i2.com";
	repo12.repo_namespace = MA_REPOSITORY_NAMESPACE_GLOBAL;
	repo12.enabled = 1;
	repo12.path = "Junk Path";
	repo12.auth_rqrd = 0;
	repo12.use_loggedon_account = 0;
	repo12.is_passwd_encrypted = 0;
	repo12.ping_count = -1;
	repo12.subnet_distance = -1;
	repo12.sitelist_order = 0;

	/*----------------orkut.com-------------*/
	repo13.name = "orkut";
	repo13.repo_type = MA_REPOSITORY_TYPE_MASTER;
	repo13.url_type = MA_REPOSITORY_TYPE_HTTP;
	repo13.sever = "www.orkut.com";
	repo13.repo_namespace = MA_REPOSITORY_NAMESPACE_GLOBAL;
	repo13.enabled = 1;
	repo13.path = "Junk Path";
	repo13.auth_rqrd = 0;
	repo13.use_loggedon_account = 0;
	repo13.is_passwd_encrypted = 0;
	repo13.ping_count = -1;
	repo13.subnet_distance = -1;
	repo13.sitelist_order = 0;

	/*----------------youtube.com-------------*/
	repo14.name = "youtube";
	repo14.repo_type = MA_REPOSITORY_TYPE_MASTER;
	repo14.url_type = MA_REPOSITORY_TYPE_HTTP;
	repo14.sever = "www.youtube.com";
	repo14.repo_namespace = MA_REPOSITORY_NAMESPACE_GLOBAL;
	repo14.enabled = 1;
	repo14.path = "Junk Path";
	repo14.auth_rqrd = 0;
	repo14.use_loggedon_account = 0;
	repo14.is_passwd_encrypted = 0;
	repo14.ping_count = -1;
	repo14.subnet_distance = -1;
	repo14.sitelist_order = 0;

	/*----------------imdb.com-------------*/
	repo15.name = "imdb";
	repo15.repo_type = MA_REPOSITORY_TYPE_MASTER;
	repo15.url_type = MA_REPOSITORY_TYPE_HTTP;
	repo15.sever = "www.imdb.com";
	repo15.repo_namespace = MA_REPOSITORY_NAMESPACE_GLOBAL;
	repo15.enabled = 1;
	repo15.path = "Junk Path";
	repo15.auth_rqrd = 0;
	repo15.use_loggedon_account = 0;
	repo15.is_passwd_encrypted = 0;
	repo15.ping_count = -1;
	repo15.subnet_distance = -1;
	repo15.sitelist_order = 0;

	/*----------------a.com-------------*/
	repo16.name = "a";
	repo16.repo_type = MA_REPOSITORY_TYPE_MASTER;
	repo16.url_type = MA_REPOSITORY_TYPE_HTTP;
	repo16.sever = "www.a.com";
	repo16.repo_namespace = MA_REPOSITORY_NAMESPACE_GLOBAL;
	repo16.enabled = 1;
	repo16.path = "Junk Path";
	repo16.auth_rqrd = 0;
	repo16.use_loggedon_account = 0;
	repo16.is_passwd_encrypted = 0;
	repo16.ping_count = -1;
	repo16.subnet_distance = -1;
	repo16.sitelist_order = 0;

	/*----------------b.com-------------*/
	repo17.name = "b";
	repo17.repo_type = MA_REPOSITORY_TYPE_MASTER;
	repo17.url_type = MA_REPOSITORY_TYPE_HTTP;
	repo17.sever = "www.b.com";
	repo17.repo_namespace = MA_REPOSITORY_NAMESPACE_GLOBAL;
	repo17.enabled = 1;
	repo17.path = "Junk Path";
	repo17.auth_rqrd = 0;
	repo17.use_loggedon_account = 0;
	repo17.is_passwd_encrypted = 0;
	repo17.ping_count = -1;
	repo17.subnet_distance = -1;
	repo17.sitelist_order = 0;
	/*----------------c.com-------------*/
	repo18.name = "c";
	repo18.repo_type = MA_REPOSITORY_TYPE_MASTER;
	repo18.url_type = MA_REPOSITORY_TYPE_HTTP;
	repo18.sever = "www.c.com";
	repo18.repo_namespace = MA_REPOSITORY_NAMESPACE_GLOBAL;
	repo18.enabled = 1;
	repo18.path = "Junk Path";
	repo18.auth_rqrd = 0;
	repo18.use_loggedon_account = 0;
	repo18.is_passwd_encrypted = 0;
	repo18.ping_count = -1;
	repo18.subnet_distance = -1;
	repo18.sitelist_order = 0;
	/*----------------d.com-------------*/
	repo19.name = "d";
	repo19.repo_type = MA_REPOSITORY_TYPE_MASTER;
	repo19.url_type = MA_REPOSITORY_TYPE_HTTP;
	repo19.sever = "www.d.com";
	repo19.repo_namespace = MA_REPOSITORY_NAMESPACE_GLOBAL;
	repo19.enabled = 1;
	repo19.path = "Junk Path";
	repo19.auth_rqrd = 0;
	repo19.use_loggedon_account = 0;
	repo19.is_passwd_encrypted = 0;
	repo19.ping_count = -1;
	repo19.subnet_distance = -1;
	repo19.sitelist_order = 0;
	/*----------------e.com-------------*/
	repo20.name = "e";
	repo20.repo_type = MA_REPOSITORY_TYPE_MASTER;
	repo20.url_type = MA_REPOSITORY_TYPE_HTTP;
	repo20.sever = "www.e.com";
	repo20.repo_namespace = MA_REPOSITORY_NAMESPACE_GLOBAL;
	repo20.enabled = 1;
	repo20.path = "Junk Path";
	repo20.auth_rqrd = 0;
	repo20.use_loggedon_account = 0;
	repo20.is_passwd_encrypted = 0;
	repo20.ping_count = -1;
	repo20.subnet_distance = -1;
	repo20.sitelist_order = 0;
	/*----------------f.com-------------*/
	repo21.name = "f";
	repo21.repo_type = MA_REPOSITORY_TYPE_MASTER;
	repo21.url_type = MA_REPOSITORY_TYPE_HTTP;
	repo21.sever = "www.f.com";
	repo21.repo_namespace = MA_REPOSITORY_NAMESPACE_GLOBAL;
	repo21.enabled = 1;
	repo21.path = "Junk Path";
	repo21.auth_rqrd = 0;
	repo21.use_loggedon_account = 0;
	repo21.is_passwd_encrypted = 0;
	repo21.ping_count = -1;
	repo21.subnet_distance = -1;
	repo21.sitelist_order = 0;
	
	/*----------------g.com-------------*/
	repo22.name = "g";
	repo22.repo_type = MA_REPOSITORY_TYPE_MASTER;
	repo22.url_type = MA_REPOSITORY_TYPE_HTTP;
	repo22.sever = "www.g.com";
	repo22.repo_namespace = MA_REPOSITORY_NAMESPACE_GLOBAL;
	repo22.enabled = 1;
	repo22.path = "Junk Path";
	repo22.auth_rqrd = 0;
	repo22.use_loggedon_account = 0;
	repo22.is_passwd_encrypted = 0;
	repo22.ping_count = -1;
	repo22.subnet_distance = -1;
	repo22.sitelist_order = 0;
	/*----------------h.com-------------*/
	repo23.name = "h";
	repo23.repo_type = MA_REPOSITORY_TYPE_MASTER;
	repo23.url_type = MA_REPOSITORY_TYPE_HTTP;
	repo23.sever = "www.h.com";
	repo23.repo_namespace = MA_REPOSITORY_NAMESPACE_GLOBAL;
	repo23.enabled = 1;
	repo23.path = "Junk Path";
	repo23.auth_rqrd = 0;
	repo23.use_loggedon_account = 0;
	repo23.is_passwd_encrypted = 0;
	repo23.ping_count = -1;
	repo23.subnet_distance = -1;
	repo23.sitelist_order = 0;
	/*----------------i.com-------------*/
	repo24.name = "i";
	repo24.repo_type = MA_REPOSITORY_TYPE_MASTER;
	repo24.url_type = MA_REPOSITORY_TYPE_HTTP;
	repo24.sever = "www.i.com";
	repo24.repo_namespace = MA_REPOSITORY_NAMESPACE_GLOBAL;
	repo24.enabled = 1;
	repo24.path = "Junk Path";
	repo24.auth_rqrd = 0;
	repo24.use_loggedon_account = 0;
	repo24.is_passwd_encrypted = 0;
	repo24.ping_count = -1;
	repo24.subnet_distance = -1;
	repo24.sitelist_order = 0;
	/*----------------j.com-------------*/
	repo25.name = "j";
	repo25.repo_type = MA_REPOSITORY_TYPE_MASTER;
	repo25.url_type = MA_REPOSITORY_TYPE_HTTP;
	repo25.sever = "www.j.com";
	repo25.repo_namespace = MA_REPOSITORY_NAMESPACE_GLOBAL;
	repo25.enabled = 1;
	repo25.path = "Junk Path";
	repo25.auth_rqrd = 0;
	repo25.use_loggedon_account = 0;
	repo25.is_passwd_encrypted = 0;
	repo25.ping_count = -1;
	repo25.subnet_distance = -1;
	repo25.sitelist_order = 0;
	/*----------------k.com-------------*/
	repo26.name = "k";
	repo26.repo_type = MA_REPOSITORY_TYPE_MASTER;
	repo26.url_type = MA_REPOSITORY_TYPE_HTTP;
	repo26.sever = "www.k.com";
	repo26.repo_namespace = MA_REPOSITORY_NAMESPACE_GLOBAL;
	repo26.enabled = 1;
	repo26.path = "Junk Path";
	repo26.auth_rqrd = 0;
	repo26.use_loggedon_account = 0;
	repo26.is_passwd_encrypted = 0;
	repo26.ping_count = -1;
	repo26.subnet_distance = -1;
	repo26.sitelist_order = 0;

	/*----------------l.com-------------*/
	repo27.name = "l";
	repo27.repo_type = MA_REPOSITORY_TYPE_MASTER;
	repo27.url_type = MA_REPOSITORY_TYPE_HTTP;
	repo27.sever = "www.l.com";
	repo27.repo_namespace = MA_REPOSITORY_NAMESPACE_GLOBAL;
	repo27.enabled = 1;
	repo27.path = "Junk Path";
	repo27.auth_rqrd = 0;
	repo27.use_loggedon_account = 0;
	repo27.is_passwd_encrypted = 0;
	repo27.ping_count = -1;
	repo27.subnet_distance = -1;
	repo27.sitelist_order = 0;

	/*----------------m.com-------------*/
	repo28.name = "10.213.254.45";
	repo28.repo_type = MA_REPOSITORY_TYPE_MASTER;
	repo28.url_type = MA_REPOSITORY_TYPE_HTTP;
	repo28.sever = "10.213.254.45";
	repo28.repo_namespace = MA_REPOSITORY_NAMESPACE_GLOBAL;
	repo28.enabled = 1;
	repo28.path = "Junk Path";
	repo28.auth_rqrd = 0;
	repo28.use_loggedon_account = 0;
	repo28.is_passwd_encrypted = 0;
	repo28.ping_count = -1;
	repo28.subnet_distance = -1;
	repo28.sitelist_order = 0;

	/*----------------n.com-------------*/
	repo29.name = "10.213.254.44";
	repo29.repo_type = MA_REPOSITORY_TYPE_MASTER;
	repo29.url_type = MA_REPOSITORY_TYPE_HTTP;
	repo29.sever = "10.213.254.44";
	repo29.repo_namespace = MA_REPOSITORY_NAMESPACE_GLOBAL;
	repo29.enabled = 1;
	repo29.path = "Junk Path";
	repo29.auth_rqrd = 0;
	repo29.use_loggedon_account = 0;
	repo29.is_passwd_encrypted = 0;
	repo29.ping_count = -1;
	repo29.subnet_distance = -1;
	repo29.sitelist_order = 0;

	/*----------------o.com-------------*/
	repo30.name = "10.213.254.43";
	repo30.repo_type = MA_REPOSITORY_TYPE_MASTER;
	repo30.url_type = MA_REPOSITORY_TYPE_HTTP;
	repo30.sever = "10.213.254.43";
	repo30.repo_namespace = MA_REPOSITORY_NAMESPACE_GLOBAL;
	repo30.enabled = 1;
	repo30.path = "Junk Path";
	repo30.auth_rqrd = 0;
	repo30.use_loggedon_account = 0;
	repo30.is_passwd_encrypted = 0;
	repo30.ping_count = -1;
	repo30.subnet_distance = -1;
	repo30.sitelist_order = 0;

}
#endif
