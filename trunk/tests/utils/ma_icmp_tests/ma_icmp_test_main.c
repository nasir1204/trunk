#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ma/internal/utils/icmp/ma_icmp.h"
#include "ma/repository/ma_repository.h"
#include "ma/internal/utils/repository/ma_repository_db_manager.h"
#include "../../../src/services/repository/ma_repository_service_internal.h"
#include "ma/logger/ma_file_logger.h"
#include "ma/logger/ma_console_logger.h"
#include "ma/internal/utils/repository/ma_repository_internal.h"
#include "../../../src/utils/icmp/ma_icmp_internal.h"
#include "ma_ut_setup.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#if defined(_WIN32)
	#define BASE_DIR ".\\"
#else
	#define BASE_DIR "/var//"
#endif

#define MA_ICMP_DB_FILENAME  "MA_ICMP.db"

ma_ut_setup_t *g_ut_setup = NULL;

void init_ut_repository_data();
void init_ut_balaji_repository_data();
ma_logger_t *g_icmp_test_logger = NULL;
extern ma_repository_t *repo_google, *repo_yahoo,*repo_mc, *repo_local, *repo_already_calculated,  *repo_fallback;
extern ma_repository_t *repo_mc1, *nodomain, *repo_mc2, *repo_mc3, *repo_mc4, *repo_mc5, *repo_mc6, *repo_mc7, *repo_mc8, *repo_mc9, *repo_mc10, *repo_mc11, *repo_mc12, *repo_mc13, *repo_mc14, *repo_mc15, *repo_mc16, *repo_mc17, *repo_mc18, *repo_mc19, *repo_mc20, *repo_mc21, *repo_mc22;
extern ma_repository_t *B_repo_mc1, *B_repo_mc2, *B_repo_mc3, *B_repo_mc4, *B_repo_mc5, *B_repo_mc6, *B_repo_mc7;
void logger_create(){

}

ma_error_t fill_repository_db(){
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	ma_db_t *db_handle = ma_ut_setup_get_database(g_ut_setup);
	//unlink(MA_ICMP_DB_FILENAME) ;
	init_ut_repository_data();
	init_ut_balaji_repository_data();
	//if((MA_OK == (rc = ma_db_initialize()))  && (MA_OK == (rc = ma_db_open(MA_ICMP_DB_FILENAME, &db_handle))) && (MA_OK == (rc = ma_repository_db_create_schema(db_handle)))) {
		rc = ma_repository_db_remove_all_repositories(db_handle);
		rc = ma_repository_db_add_repository(db_handle, repo_fallback);
		rc = ma_repository_db_add_repository(db_handle, repo_already_calculated);
		rc = ma_repository_db_add_repository(db_handle, repo_google);
		rc = ma_repository_db_add_repository(db_handle, repo_yahoo);
		rc = ma_repository_db_add_repository(db_handle, repo_mc);
		rc = ma_repository_db_add_repository(db_handle, repo_local);
		rc = ma_repository_db_add_repository(db_handle, repo_mc1);
		rc = ma_repository_db_add_repository(db_handle, repo_mc2);
		rc = ma_repository_db_add_repository(db_handle, repo_mc3);
		rc = ma_repository_db_add_repository(db_handle, repo_mc4);
		rc = ma_repository_db_add_repository(db_handle, repo_mc5);
		rc = ma_repository_db_add_repository(db_handle, repo_mc6);
		rc = ma_repository_db_add_repository(db_handle, repo_mc7);
		rc = ma_repository_db_add_repository(db_handle, repo_mc8);
		rc = ma_repository_db_add_repository(db_handle, repo_mc9);
		rc = ma_repository_db_add_repository(db_handle, repo_mc10);
		rc = ma_repository_db_add_repository(db_handle, repo_mc11);
		rc = ma_repository_db_add_repository(db_handle, repo_mc12);
		rc = ma_repository_db_add_repository(db_handle, repo_mc13);
		rc = ma_repository_db_add_repository(db_handle, repo_mc14);
		rc = ma_repository_db_add_repository(db_handle, repo_mc15);
		rc = ma_repository_db_add_repository(db_handle, repo_mc16);
		rc = ma_repository_db_add_repository(db_handle, repo_mc17);
		rc = ma_repository_db_add_repository(db_handle, repo_mc18);
		rc = ma_repository_db_add_repository(db_handle, repo_mc19);
		rc = ma_repository_db_add_repository(db_handle, repo_mc20);
		rc = ma_repository_db_add_repository(db_handle, repo_mc21);
		rc = ma_repository_db_add_repository(db_handle, repo_mc22);
		rc = ma_repository_db_add_repository(db_handle, nodomain);

		rc = ma_repository_db_add_repository(db_handle, B_repo_mc1);
		rc = ma_repository_db_add_repository(db_handle, B_repo_mc2);
		rc = ma_repository_db_add_repository(db_handle, B_repo_mc3);
		rc = ma_repository_db_add_repository(db_handle, B_repo_mc4);
		rc = ma_repository_db_add_repository(db_handle, B_repo_mc5);
		rc = ma_repository_db_add_repository(db_handle, B_repo_mc6);
		rc = ma_repository_db_add_repository(db_handle, B_repo_mc7);


//	}
	
	return rc;
}

#if defined(MA_WINDOWS)
	#include <windows.h>
	int ma_wsa_startup()
	{
		WORD wVersionRequested;
		WSADATA wsaData;
		int err;
	/* Use the MAKEWORD(lowbyte, highbyte) macro declared in Windef.h */
		wVersionRequested = MAKEWORD(2, 2);

		err = WSAStartup(wVersionRequested, &wsaData);
		if (err != 0) { 
			return MA_ERROR_APIFAILED;
		}
		return MA_OK;
	}

	void ma_wsa_cleanup()
	{
		WSACleanup();
	}
#endif

static ma_icmp_methods_t methods = {
	ma_repository_rank_sitelist_get,
	ma_repository_rank_sitelist_update,
	ma_repository_rank_stop
};

ma_error_t  ma_repository_rank_create(ma_repository_service_t *repository_service, ma_repository_rank_t **respository_rank);
static void uv_watcher_close_cb(uv_handle_t* handle){
	return;
}

int main()
{
	ma_error_t rc = MA_ERROR_INVALIDARG ;
	ma_repository_rank_type_t rank_type = MA_REPOSITORY_RANK_BY_PING_TIME;
	//ma_repository_rank_type_t rank_type = MA_REPOSITORY_RANK_BY_SUBNET_DISTANCE;
	ma_repository_rank_t *respository_rank = NULL;
	ma_repository_service_t *repo_service;

	if(MA_OK == ma_file_logger_create(BASE_DIR,"icmp",".log",(ma_file_logger_t **)&g_icmp_test_logger)) {
#if defined(MA_WINDOWS)
		if(0 != ma_wsa_startup())
		{
			MA_LOG(g_icmp_test_logger, MA_LOG_SEV_ERROR, "Network Interface property fetching failed ");
			return MA_ERROR_APIFAILED;
		}
#endif
		ma_ut_setup_create("event_client.event_bag.tests", "ma.icmp.test", &g_ut_setup);
		ma_repository_service_create(MA_REPOSITORY_SERVICE_NAME_STR, &repo_service);
        ma_service_configure((ma_service_t*)repo_service,ma_ut_setup_get_context(g_ut_setup), MA_SERVICE_CONFIG_NEW);
		fill_repository_db();
		rc = ma_repository_rank_create(repo_service, &respository_rank);
		rc = ma_repository_rank_start(respository_rank, rank_type);
		
#if defined(MA_WINDOWS)
		/*Sleep(1*1000);
		rc = ma_repository_rank_stop(respository_rank);
		rc = ma_repository_rank_start(respository_rank, rank_type);*/
		/*Give time to execute uv blocked calls. */
		Sleep(3000*1000);
#endif
	}
	printf("ICMP test return status : %d \n", rc);
	return 0;
}

