/******************************************************************************
Repository - DB Manager tests
******************************************************************************/
#define UNITY_FIXTURE_MALLOC_OVERRIDES_H_ 1

#include "unity.h"
#include "unity_fixture.h"
#include "unity_internals.h"
#include "ma/internal/utils/database/ma_db.h"
#include "ma/internal/utils/repository/ma_repository_db_manager.h"
#include "../tests/utils/macrypto_test/ma_crypto_tests_setup.h"
#include <string.h>
#include <stdlib.h>

#ifndef MA_WINDOWS
    #include <limits.h>
    #include <unistd.h>
#endif

TEST_GROUP_RUNNER(repository_db_manager_test_group)
{
    do {RUN_TEST_CASE(repository_db_manager_tests, add_and_remove_repository_test)} while (0);
    do {RUN_TEST_CASE(repository_db_manager_tests, add_and_remove_all_repository_test)} while (0);
    do {RUN_TEST_CASE(repository_db_manager_tests, add_and_remove_all_global_repository_test)} while (0);
    do {RUN_TEST_CASE(repository_db_manager_tests, add_and_remove_all_local_repository_test)} while (0);
    do {RUN_TEST_CASE(repository_db_manager_tests, set_and_get_repository_state_test)} while (0);
    do {RUN_TEST_CASE(repository_db_manager_tests, get_repositories_test)} while (0);
    do {RUN_TEST_CASE(repository_db_manager_tests, get_spipe_repositories_test)} while (0);
    do {RUN_TEST_CASE(repository_db_manager_tests, set_repository_enabled_test)} while (0);
    do {RUN_TEST_CASE(repository_db_manager_tests, repository_db_set_all_repositories_enabled_test)} while (0);
    do {RUN_TEST_CASE(repository_db_manager_tests, set_repository_ping_count_test)} while (0);
    do {RUN_TEST_CASE(repository_db_manager_tests, set_repository_subnet_distance_test)} while (0);
    do {RUN_TEST_CASE(repository_db_manager_tests, set_repository_sitelistorder_test)} while (0);
    do {RUN_TEST_CASE(repository_db_manager_tests, set_local_repository_test)} while (0);
}

static ma_db_t *repo_db = NULL;
static ma_crypto_t *crypto_object = NULL;

#define REPOSITORY_DB_FILENAME  "REPO_DB"


TEST_SETUP(repository_db_manager_tests)
{
    unlink(REPOSITORY_DB_FILENAME);

    TEST_ASSERT(MA_OK == ma_db_open(REPOSITORY_DB_FILENAME, MA_DB_OPEN_READWRITE, &repo_db));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_create_schema(NULL));
    TEST_ASSERT(MA_OK == ma_repository_db_create_schema(repo_db));
}

TEST_TEAR_DOWN(repository_db_manager_tests)
{
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_db_close(NULL));
    TEST_ASSERT(MA_OK == ma_db_close(repo_db));
}


// Lookup Table consisting of Repository Information
struct LookupTable
{
    const char *repo_name;
    ma_repository_type_t repo_type;
    ma_repository_url_type_t repo_url_type;
    ma_repository_namespace_t repo_namespace;
    ma_proxy_usage_type_t repo_proxy_usage_type;
    ma_repository_auth_t repo_auth;
    ma_bool_t repo_enabled;
    const char *repo_server_fqdn;
    const char *repo_server_name;
    const char *repo_server_ip;
    ma_uint32_t repo_port;
    const char *repo_server_path;
    const char *repo_domain_name;
    const char *repo_user_name;
    const char *repo_password;
    ma_uint32_t repo_pingtime;
    ma_uint32_t repo_subnetdistance;
    ma_uint32_t repo_siteorder;
    ma_repository_state_t repo_state;
};

static struct LookupTable const repositories[] =
{
    // Name            Type                         URL Type                      Namespace                       Proxy Usage Type                       Authentication                    Enabled   Server FQDN        Server Name   Server IP        Port       Server Path                              Domain Name    Username              Password              Pingtime   Subnet Distance   Siteorder   State
    {  "Repository_1", MA_REPOSITORY_TYPE_MASTER,   MA_REPOSITORY_URL_TYPE_FTP,   MA_REPOSITORY_NAMESPACE_LOCAL,  MA_PROXY_USAGE_TYPE_SERVER_CONFIGURED, MA_REPOSITORY_AUTH_CREDENTIALS,   MA_TRUE,  "Server_fqdn_1",   "Server_1",   "192.127.221.1", 32000,    "ftp:\\\\corp.mileaccess.com",                "My_Domain",   "Administrator",      "123j2h3,n32klh3k",    1000,      4,                3,          MA_REPOSITORY_STATE_ADDED   },
    {  "Repository_2", MA_REPOSITORY_TYPE_FALLBACK, MA_REPOSITORY_URL_TYPE_HTTP,  MA_REPOSITORY_NAMESPACE_GLOBAL, MA_PROXY_USAGE_TYPE_SYSTEM_CONFIGURED, MA_REPOSITORY_AUTH_NONE,          MA_FALSE, "Server_fqdn_2",   "Server_2",   "172.20.154.12", 80,       "http:\\\\www.repository.com\\internal",  "My_Domain",   "Low Priority User",  "#$#$#%$%$FFR",        200,       40,               30,         MA_REPOSITORY_STATE_DEFAULT },
    {  "Repository_6", MA_REPOSITORY_TYPE_REPO,     MA_REPOSITORY_URL_TYPE_SA,    MA_REPOSITORY_NAMESPACE_LOCAL,  MA_PROXY_USAGE_TYPE_NONE,              MA_REPOSITORY_AUTH_LOGGEDON_USER, MA_FALSE, "Server_fqdn_6",   "Server_6",   "10.213.254.84", 8080,     "ftp:\\\\my_ftp_repository\\help",        "My_Domain",   "Super User",         "d$#%FWE#$%FWE",       200,       10,               5,          MA_REPOSITORY_STATE_DEFAULT },
    {  "Repository_3", MA_REPOSITORY_TYPE_FALLBACK, MA_REPOSITORY_URL_TYPE_FTP,   MA_REPOSITORY_NAMESPACE_GLOBAL, MA_PROXY_USAGE_TYPE_SYSTEM_CONFIGURED, MA_REPOSITORY_AUTH_ANONYMOUS,     MA_TRUE,  "Server_fqdn_3",   "Server_3",   "142.15.230.21", 8080,     "ftp:\\\\my_ftp_repository_1",            "My_Domain",   "Administrator",      "SD#RED#RE@#@#@CCE",   3000,      100,              1,          MA_REPOSITORY_STATE_ALTERED },
    {  "Repository_4", MA_REPOSITORY_TYPE_REPO,     MA_REPOSITORY_URL_TYPE_SPIPE, MA_REPOSITORY_NAMESPACE_GLOBAL, MA_PROXY_USAGE_TYPE_SERVER_CONFIGURED, MA_REPOSITORY_AUTH_LOGGEDON_USER, MA_FALSE, "Server_fqdn_4",   "Server_4",   "178.32.220.20", 8440,     "ftp:\\\\my_ftp_repository",              "My_Domain",   "Normal User",        "@@DX#CEF$FEFEF$#R",   500,       0,                7,          MA_REPOSITORY_STATE_DEFAULT },
    {  "Repository_5", MA_REPOSITORY_TYPE_MASTER,   MA_REPOSITORY_URL_TYPE_UNC,   MA_REPOSITORY_NAMESPACE_LOCAL,  MA_PROXY_USAGE_TYPE_SERVER_CONFIGURED, MA_REPOSITORY_AUTH_ANONYMOUS,     MA_TRUE,  "Server_fqdn_5",   "Server_5",   "192.16.200.10", 2000,     "ftp:\\\\my_ftp_repository",              "My_Domain",   "Least Access User",  "@@#@#@#@#@#@#@#@#@#", 900,       1000,             10,         MA_REPOSITORY_STATE_ALTERED },
};


/******************************************************************************
Test for verifying the Repository Addition and Removal APIs
******************************************************************************/
TEST(repository_db_manager_tests, add_and_remove_repository_test)
{
    ma_uint8_t counter = 0;

    // Adding Repositories to DB
    for(counter = 0; counter < sizeof(repositories)/sizeof(repositories[0]); counter++)
    {
        // Creating repository and filling all parameters
        ma_repository_t *my_repository = NULL;

        TEST_ASSERT(MA_OK == ma_repository_create(&my_repository));
        TEST_ASSERT(MA_OK == ma_repository_set_name(my_repository, repositories[counter].repo_name));
        TEST_ASSERT(MA_OK == ma_repository_set_type(my_repository, repositories[counter].repo_type));
        TEST_ASSERT(MA_OK == ma_repository_set_url_type(my_repository, repositories[counter].repo_url_type));
        TEST_ASSERT(MA_OK == ma_repository_set_namespace(my_repository, repositories[counter].repo_namespace));
        TEST_ASSERT(MA_OK == ma_repository_set_proxy_usage_type(my_repository, repositories[counter].repo_proxy_usage_type));
        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, repositories[counter].repo_auth));
        TEST_ASSERT(MA_OK == ma_repository_set_enabled(my_repository, repositories[counter].repo_enabled));

        if(repositories[counter].repo_url_type != MA_REPOSITORY_URL_TYPE_LOCAL)
        {
            TEST_ASSERT(MA_OK == ma_repository_set_server_fqdn(my_repository, repositories[counter].repo_server_fqdn));
        }

        if((repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_SPIPE) || (repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_SA))
        {
            TEST_ASSERT(MA_OK == ma_repository_set_server_name(my_repository, repositories[counter].repo_server_name));
            TEST_ASSERT(MA_OK == ma_repository_set_server_ip(my_repository, repositories[counter].repo_server_ip));
        }

        if((repositories[counter].repo_url_type != MA_REPOSITORY_URL_TYPE_UNC) && (repositories[counter].repo_url_type != MA_REPOSITORY_URL_TYPE_LOCAL))
        {
            TEST_ASSERT(MA_OK == ma_repository_set_port(my_repository, repositories[counter].repo_port));
        }

        TEST_ASSERT(MA_OK == ma_repository_set_server_path(my_repository, repositories[counter].repo_server_path));

        if((repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_UNC) || (repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_LOCAL))
        {
            TEST_ASSERT(MA_OK == ma_repository_set_domain_name(my_repository, repositories[counter].repo_domain_name));
        }

        TEST_ASSERT(MA_OK == ma_repository_set_user_name(my_repository, repositories[counter].repo_user_name));
        TEST_ASSERT(MA_OK == ma_repository_set_password(my_repository, repositories[counter].repo_password));
        TEST_ASSERT(MA_OK == ma_repository_set_pingtime(my_repository, repositories[counter].repo_pingtime));
        TEST_ASSERT(MA_OK == ma_repository_set_subnetdistance(my_repository, repositories[counter].repo_subnetdistance));
        TEST_ASSERT(MA_OK == ma_repository_set_siteorder(my_repository, repositories[counter].repo_siteorder));
        TEST_ASSERT(MA_OK == ma_repository_set_state(my_repository, repositories[counter].repo_state));

        // Adding repositories to DB
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_add_repository(NULL, my_repository));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_add_repository(repo_db, NULL));
        TEST_ASSERT(MA_OK == ma_repository_db_add_repository(repo_db, my_repository));

        // Getting Repository Name
        {
            char *server = NULL;

            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_get_repository_fqdn(NULL, repositories[counter].repo_name, &server));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_get_repository_fqdn(repo_db, NULL, &server));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_get_repository_fqdn(repo_db, repositories[counter].repo_name, NULL));
            TEST_ASSERT(MA_ERROR_OBJECTNOTFOUND == ma_repository_db_get_repository_fqdn(repo_db, "Non-Existent Repository", &server));
            TEST_ASSERT(MA_OK == ma_repository_db_get_repository_fqdn(repo_db, repositories[counter].repo_name, &server));
            TEST_ASSERT(strcmp(server, repositories[counter].repo_server_fqdn) == 0);
            free(server); server = NULL;
        }

        // Getting Repository Path
        {
            char *path = NULL;

            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_get_repository_path(NULL, repositories[counter].repo_name, &path));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_get_repository_path(repo_db, NULL, &path));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_get_repository_path(repo_db, repositories[counter].repo_name, NULL));
            TEST_ASSERT(MA_ERROR_OBJECTNOTFOUND == ma_repository_db_get_repository_path(repo_db, "Non-Existent Repository", &path));
            TEST_ASSERT(MA_OK == ma_repository_db_get_repository_path(repo_db, repositories[counter].repo_name, &path));
            TEST_ASSERT(strcmp(path, repositories[counter].repo_server_path) == 0);
            free(path); path = NULL;
        }

        // Getting Repository Server Name and Server IP
        if((repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_SPIPE) || (repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_SA))
        {
            char *server = NULL;
            char *ip = NULL;

            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_get_repository_server(NULL, repositories[counter].repo_name, &server));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_get_repository_server(repo_db, NULL, &server));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_get_repository_server(repo_db, repositories[counter].repo_name, NULL));
            TEST_ASSERT(MA_ERROR_OBJECTNOTFOUND == ma_repository_db_get_repository_server(repo_db, "Non-Existent Repository", &server));
            TEST_ASSERT(MA_OK == ma_repository_db_get_repository_server(repo_db, repositories[counter].repo_name, &server));
            TEST_ASSERT(strcmp(server, repositories[counter].repo_server_name) == 0);
            free(server); server = NULL;

            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_get_repository_ip(NULL, repositories[counter].repo_name, &ip));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_get_repository_ip(repo_db, NULL, &ip));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_get_repository_ip(repo_db, repositories[counter].repo_name, NULL));
            TEST_ASSERT(MA_ERROR_OBJECTNOTFOUND == ma_repository_db_get_repository_ip(repo_db, "Non-Existent Repository", &ip));
            TEST_ASSERT(MA_OK == ma_repository_db_get_repository_ip(repo_db, repositories[counter].repo_name, &ip));
            TEST_ASSERT(strcmp(ip, repositories[counter].repo_server_ip) == 0);
            free(ip); ip = NULL;
        }

        //ma_repository_db_get_repository_server

        // Verifying ma_repository_db_get_url_type_by_server() API using Server Name, Server FQDN and Server IP
        {
            ma_db_recordset_t *record_set = NULL;
            int int_val = -1;

            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_get_url_type_by_server(NULL, repositories[counter].repo_server_fqdn, &record_set));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_get_url_type_by_server(repo_db, NULL, &record_set));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_get_url_type_by_server(repo_db, repositories[counter].repo_server_fqdn, NULL));
            TEST_ASSERT(MA_OK == ma_repository_db_get_url_type_by_server(repo_db, repositories[counter].repo_server_fqdn, &record_set));

            TEST_ASSERT(MA_OK == ma_db_recordset_next(record_set));
            {
                ma_db_recordset_get_int(record_set, 1, &int_val);
                TEST_ASSERT(repositories[counter].repo_url_type == int_val);
            }

            TEST_ASSERT(MA_OK == ma_db_recordset_release(record_set));

            // Server IP and Server Name are only applicable for SPIPE and SA repositories, therefore, search based on these two parameters is
            // also applicable for these twp repository types only
            if((repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_SPIPE) || (repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_SA))
            {
                TEST_ASSERT(MA_OK == ma_repository_db_get_url_type_by_server(repo_db, repositories[counter].repo_server_name, &record_set));

                TEST_ASSERT(MA_OK == ma_db_recordset_next(record_set));
                {
                    ma_db_recordset_get_int(record_set, 1, &int_val);
                    TEST_ASSERT(repositories[counter].repo_url_type == int_val);
                }

                TEST_ASSERT(MA_OK == ma_db_recordset_release(record_set));

                TEST_ASSERT(MA_OK == ma_repository_db_get_url_type_by_server(repo_db, repositories[counter].repo_server_ip, &record_set));

                TEST_ASSERT(MA_OK == ma_db_recordset_next(record_set));
                {
                    ma_db_recordset_get_int(record_set, 1, &int_val);
                    TEST_ASSERT(repositories[counter].repo_url_type == int_val);
                }

                TEST_ASSERT(MA_OK == ma_db_recordset_release(record_set));
            }
        }

        // Removing Repository by Name
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_remove_repository_by_name(NULL, repositories[counter].repo_name));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_remove_repository_by_name(repo_db, NULL));
        TEST_ASSERT(MA_OK == ma_repository_db_remove_repository_by_name(repo_db, repositories[counter].repo_name));

        // Trying to retrieve repository after removal. It should return NULL.
        {
            char *server = NULL;
            TEST_ASSERT(MA_ERROR_OBJECTNOTFOUND == ma_repository_db_get_repository_fqdn(repo_db, repositories[counter].repo_name, &server));
            TEST_ASSERT_NULL(server);
        }

        // Releasing Repository
        TEST_ASSERT(MA_OK == ma_repository_release(my_repository));
    }
}


/******************************************************************************
Test for verifying the Remove All Repositories APIs
******************************************************************************/
TEST(repository_db_manager_tests, add_and_remove_all_repository_test)
{
    ma_uint8_t counter = 0;
    char *server = NULL;

    // Adding Repositories to DB
    for(counter = 0; counter < sizeof(repositories)/sizeof(repositories[0]); counter++)
    {
        // Creating repository and filling all parameters
        ma_repository_t *my_repository = NULL;

        TEST_ASSERT(MA_OK == ma_repository_create(&my_repository));
        TEST_ASSERT(MA_OK == ma_repository_set_name(my_repository, repositories[counter].repo_name));
        TEST_ASSERT(MA_OK == ma_repository_set_type(my_repository, repositories[counter].repo_type));
        TEST_ASSERT(MA_OK == ma_repository_set_url_type(my_repository, repositories[counter].repo_url_type));
        TEST_ASSERT(MA_OK == ma_repository_set_namespace(my_repository, repositories[counter].repo_namespace));
        TEST_ASSERT(MA_OK == ma_repository_set_proxy_usage_type(my_repository, repositories[counter].repo_proxy_usage_type));
        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, repositories[counter].repo_auth));
        TEST_ASSERT(MA_OK == ma_repository_set_enabled(my_repository, repositories[counter].repo_enabled));

        if(repositories[counter].repo_url_type != MA_REPOSITORY_URL_TYPE_LOCAL)
        {
            TEST_ASSERT(MA_OK == ma_repository_set_server_fqdn(my_repository, repositories[counter].repo_server_fqdn));
        }

        if((repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_SPIPE) || (repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_SA))
        {
            TEST_ASSERT(MA_OK == ma_repository_set_server_name(my_repository, repositories[counter].repo_server_name));
            TEST_ASSERT(MA_OK == ma_repository_set_server_ip(my_repository, repositories[counter].repo_server_ip));
        }

        if((repositories[counter].repo_url_type != MA_REPOSITORY_URL_TYPE_UNC) && (repositories[counter].repo_url_type != MA_REPOSITORY_URL_TYPE_LOCAL))
        {
            TEST_ASSERT(MA_OK == ma_repository_set_port(my_repository, repositories[counter].repo_port));
        }

        TEST_ASSERT(MA_OK == ma_repository_set_server_path(my_repository, repositories[counter].repo_server_path));

        if((repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_UNC) || (repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_LOCAL))
        {
            TEST_ASSERT(MA_OK == ma_repository_set_domain_name(my_repository, repositories[counter].repo_domain_name));
        }

        TEST_ASSERT(MA_OK == ma_repository_set_user_name(my_repository, repositories[counter].repo_user_name));
        TEST_ASSERT(MA_OK == ma_repository_set_password(my_repository, repositories[counter].repo_password));
        TEST_ASSERT(MA_OK == ma_repository_set_pingtime(my_repository, repositories[counter].repo_pingtime));
        TEST_ASSERT(MA_OK == ma_repository_set_subnetdistance(my_repository, repositories[counter].repo_subnetdistance));
        TEST_ASSERT(MA_OK == ma_repository_set_siteorder(my_repository, repositories[counter].repo_siteorder));
        TEST_ASSERT(MA_OK == ma_repository_set_state(my_repository, repositories[counter].repo_state));

        // Adding repositories to DB
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_add_repository(NULL, my_repository));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_add_repository(repo_db, NULL));
        TEST_ASSERT(MA_OK == ma_repository_db_add_repository(repo_db, my_repository));

        // Releasing Repository object after adding it to DB
        TEST_ASSERT(MA_OK == ma_repository_release(my_repository));
    }

    // Removing All Repositories
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_remove_all_repositories(NULL));
    TEST_ASSERT(MA_OK == ma_repository_db_remove_all_repositories(repo_db));

    // Trying to retrieve repository after removal. It should return NULL.
    for(counter = 0; counter < sizeof(repositories)/sizeof(repositories[0]); counter++)
    {
        server = NULL;
        TEST_ASSERT(MA_ERROR_OBJECTNOTFOUND == ma_repository_db_get_repository_fqdn(repo_db, repositories[counter].repo_name, &server));
        TEST_ASSERT_NULL(server);
    }
}


/******************************************************************************
Test for verifying the Remove All Global Repositories APIs
******************************************************************************/
TEST(repository_db_manager_tests, add_and_remove_all_global_repository_test)
{
    ma_uint8_t counter = 0;
    char *server = NULL;

    // Adding Repositories to DB
    for(counter = 0; counter < sizeof(repositories)/sizeof(repositories[0]); counter++)
    {
        // Creating repository and filling all parameters
        ma_repository_t *my_repository = NULL;

        TEST_ASSERT(MA_OK == ma_repository_create(&my_repository));
        TEST_ASSERT(MA_OK == ma_repository_set_name(my_repository, repositories[counter].repo_name));
        TEST_ASSERT(MA_OK == ma_repository_set_type(my_repository, repositories[counter].repo_type));
        TEST_ASSERT(MA_OK == ma_repository_set_url_type(my_repository, repositories[counter].repo_url_type));
        TEST_ASSERT(MA_OK == ma_repository_set_namespace(my_repository, repositories[counter].repo_namespace));
        TEST_ASSERT(MA_OK == ma_repository_set_proxy_usage_type(my_repository, repositories[counter].repo_proxy_usage_type));
        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, repositories[counter].repo_auth));
        TEST_ASSERT(MA_OK == ma_repository_set_enabled(my_repository, repositories[counter].repo_enabled));

        if(repositories[counter].repo_url_type != MA_REPOSITORY_URL_TYPE_LOCAL)
        {
            TEST_ASSERT(MA_OK == ma_repository_set_server_fqdn(my_repository, repositories[counter].repo_server_fqdn));
        }

        if((repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_SPIPE) || (repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_SA))
        {
            TEST_ASSERT(MA_OK == ma_repository_set_server_name(my_repository, repositories[counter].repo_server_name));
            TEST_ASSERT(MA_OK == ma_repository_set_server_ip(my_repository, repositories[counter].repo_server_ip));
        }

        if((repositories[counter].repo_url_type != MA_REPOSITORY_URL_TYPE_UNC) && (repositories[counter].repo_url_type != MA_REPOSITORY_URL_TYPE_LOCAL))
        {
            TEST_ASSERT(MA_OK == ma_repository_set_port(my_repository, repositories[counter].repo_port));
        }

        TEST_ASSERT(MA_OK == ma_repository_set_server_path(my_repository, repositories[counter].repo_server_path));

        if((repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_UNC) || (repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_LOCAL))
        {
            TEST_ASSERT(MA_OK == ma_repository_set_domain_name(my_repository, repositories[counter].repo_domain_name));
        }

        TEST_ASSERT(MA_OK == ma_repository_set_user_name(my_repository, repositories[counter].repo_user_name));
        TEST_ASSERT(MA_OK == ma_repository_set_password(my_repository, repositories[counter].repo_password));
        TEST_ASSERT(MA_OK == ma_repository_set_pingtime(my_repository, repositories[counter].repo_pingtime));
        TEST_ASSERT(MA_OK == ma_repository_set_subnetdistance(my_repository, repositories[counter].repo_subnetdistance));
        TEST_ASSERT(MA_OK == ma_repository_set_siteorder(my_repository, repositories[counter].repo_siteorder));
        TEST_ASSERT(MA_OK == ma_repository_set_state(my_repository, repositories[counter].repo_state));

        // Adding repositories to DB
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_add_repository(NULL, my_repository));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_add_repository(repo_db, NULL));
        TEST_ASSERT(MA_OK == ma_repository_db_add_repository(repo_db, my_repository));

        // Releasing Repository object after adding it to DB
        TEST_ASSERT(MA_OK == ma_repository_release(my_repository));
    }

    // Removing All Global Repositories
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_remove_all_global_repositories(NULL));
    TEST_ASSERT(MA_OK == ma_repository_db_remove_all_global_repositories(repo_db));

    // Trying to retrieve repository after removal. It should return NULL.
    for(counter = 0; counter < sizeof(repositories)/sizeof(repositories[0]); counter++)
    {
        server = NULL;

        if(repositories[counter].repo_namespace == MA_REPOSITORY_NAMESPACE_LOCAL)
        {
            TEST_ASSERT(MA_OK == ma_repository_db_get_repository_fqdn(repo_db, repositories[counter].repo_name, &server));
            TEST_ASSERT(strcmp(server, repositories[counter].repo_server_fqdn) == 0);
            free(server); server = NULL;
        }

        if(repositories[counter].repo_namespace == MA_REPOSITORY_NAMESPACE_GLOBAL)
        {
            TEST_ASSERT(MA_ERROR_OBJECTNOTFOUND == ma_repository_db_get_repository_fqdn(repo_db, repositories[counter].repo_name, &server));
            TEST_ASSERT_NULL(server);
        }
    }
}


/******************************************************************************
Test for verifying the Remove All Local Repositories APIs
******************************************************************************/
TEST(repository_db_manager_tests, add_and_remove_all_local_repository_test)
{
    ma_uint8_t counter = 0;
    char *server = NULL;

    // Adding Repositories to DB
    for(counter = 0; counter < sizeof(repositories)/sizeof(repositories[0]); counter++)
    {
        // Creating repository and filling all parameters
        ma_repository_t *my_repository = NULL;

        TEST_ASSERT(MA_OK == ma_repository_create(&my_repository));
        TEST_ASSERT(MA_OK == ma_repository_set_name(my_repository, repositories[counter].repo_name));
        TEST_ASSERT(MA_OK == ma_repository_set_type(my_repository, repositories[counter].repo_type));
        TEST_ASSERT(MA_OK == ma_repository_set_url_type(my_repository, repositories[counter].repo_url_type));
        TEST_ASSERT(MA_OK == ma_repository_set_namespace(my_repository, repositories[counter].repo_namespace));
        TEST_ASSERT(MA_OK == ma_repository_set_proxy_usage_type(my_repository, repositories[counter].repo_proxy_usage_type));
        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, repositories[counter].repo_auth));
        TEST_ASSERT(MA_OK == ma_repository_set_enabled(my_repository, repositories[counter].repo_enabled));

        if(repositories[counter].repo_url_type != MA_REPOSITORY_URL_TYPE_LOCAL)
        {
            TEST_ASSERT(MA_OK == ma_repository_set_server_fqdn(my_repository, repositories[counter].repo_server_fqdn));
        }

        if((repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_SPIPE) || (repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_SA))
        {
            TEST_ASSERT(MA_OK == ma_repository_set_server_name(my_repository, repositories[counter].repo_server_name));
            TEST_ASSERT(MA_OK == ma_repository_set_server_ip(my_repository, repositories[counter].repo_server_ip));
        }

        if((repositories[counter].repo_url_type != MA_REPOSITORY_URL_TYPE_UNC) && (repositories[counter].repo_url_type != MA_REPOSITORY_URL_TYPE_LOCAL))
        {
            TEST_ASSERT(MA_OK == ma_repository_set_port(my_repository, repositories[counter].repo_port));
        }

        TEST_ASSERT(MA_OK == ma_repository_set_server_path(my_repository, repositories[counter].repo_server_path));

        if((repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_UNC) || (repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_LOCAL))
        {
            TEST_ASSERT(MA_OK == ma_repository_set_domain_name(my_repository, repositories[counter].repo_domain_name));
        }

        TEST_ASSERT(MA_OK == ma_repository_set_user_name(my_repository, repositories[counter].repo_user_name));
        TEST_ASSERT(MA_OK == ma_repository_set_password(my_repository, repositories[counter].repo_password));
        TEST_ASSERT(MA_OK == ma_repository_set_pingtime(my_repository, repositories[counter].repo_pingtime));
        TEST_ASSERT(MA_OK == ma_repository_set_subnetdistance(my_repository, repositories[counter].repo_subnetdistance));
        TEST_ASSERT(MA_OK == ma_repository_set_siteorder(my_repository, repositories[counter].repo_siteorder));
        TEST_ASSERT(MA_OK == ma_repository_set_state(my_repository, repositories[counter].repo_state));

        // Adding repositories to DB
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_add_repository(NULL, my_repository));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_add_repository(repo_db, NULL));
        TEST_ASSERT(MA_OK == ma_repository_db_add_repository(repo_db, my_repository));

        // Releasing Repository object after adding it to DB
        TEST_ASSERT(MA_OK == ma_repository_release(my_repository));
    }

    // Removing all local Repositories
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_remove_all_local_repositories(NULL));
    TEST_ASSERT(MA_OK == ma_repository_db_remove_all_local_repositories(repo_db));

    // Trying to retrieve repository after removal. It should return NULL.
    for(counter = 0; counter < sizeof(repositories)/sizeof(repositories[0]); counter++)
    {
        server = NULL;

        if(repositories[counter].repo_namespace == MA_REPOSITORY_NAMESPACE_GLOBAL)
        {
            TEST_ASSERT(MA_OK == ma_repository_db_get_repository_fqdn(repo_db, repositories[counter].repo_name, &server));
            TEST_ASSERT(strcmp(server, repositories[counter].repo_server_fqdn) == 0);
            free(server); server = NULL;
        }

        if(repositories[counter].repo_namespace == MA_REPOSITORY_NAMESPACE_LOCAL)
        {
            TEST_ASSERT(MA_ERROR_OBJECTNOTFOUND == ma_repository_db_get_repository_fqdn(repo_db, repositories[counter].repo_name, &server));
            TEST_ASSERT_NULL(server);
        }
    }
}



/******************************************************************************
Test for verifying the ma_repository_db_set_all_repository_state and
ma_repository_db_get_repository_state APIs
******************************************************************************/
TEST(repository_db_manager_tests, set_and_get_repository_state_test)
{
    ma_uint8_t counter = 0;
    char *server = NULL;

    // Adding Repositories to DB
    for(counter = 0; counter < sizeof(repositories)/sizeof(repositories[0]); counter++)
    {
        // Creating repository and filling all parameters
        ma_repository_t *my_repository = NULL;

        TEST_ASSERT(MA_OK == ma_repository_create(&my_repository));
        TEST_ASSERT(MA_OK == ma_repository_set_name(my_repository, repositories[counter].repo_name));
        TEST_ASSERT(MA_OK == ma_repository_set_type(my_repository, repositories[counter].repo_type));
        TEST_ASSERT(MA_OK == ma_repository_set_url_type(my_repository, repositories[counter].repo_url_type));
        TEST_ASSERT(MA_OK == ma_repository_set_namespace(my_repository, repositories[counter].repo_namespace));
        TEST_ASSERT(MA_OK == ma_repository_set_proxy_usage_type(my_repository, repositories[counter].repo_proxy_usage_type));
        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, repositories[counter].repo_auth));
        TEST_ASSERT(MA_OK == ma_repository_set_enabled(my_repository, repositories[counter].repo_enabled));

        if(repositories[counter].repo_url_type != MA_REPOSITORY_URL_TYPE_LOCAL)
        {
            TEST_ASSERT(MA_OK == ma_repository_set_server_fqdn(my_repository, repositories[counter].repo_server_fqdn));
        }

        if((repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_SPIPE) || (repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_SA))
        {
            TEST_ASSERT(MA_OK == ma_repository_set_server_name(my_repository, repositories[counter].repo_server_name));
            TEST_ASSERT(MA_OK == ma_repository_set_server_ip(my_repository, repositories[counter].repo_server_ip));
        }

        if((repositories[counter].repo_url_type != MA_REPOSITORY_URL_TYPE_UNC) && (repositories[counter].repo_url_type != MA_REPOSITORY_URL_TYPE_LOCAL))
        {
            TEST_ASSERT(MA_OK == ma_repository_set_port(my_repository, repositories[counter].repo_port));
        }

        TEST_ASSERT(MA_OK == ma_repository_set_server_path(my_repository, repositories[counter].repo_server_path));

        if((repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_UNC) || (repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_LOCAL))
        {
            TEST_ASSERT(MA_OK == ma_repository_set_domain_name(my_repository, repositories[counter].repo_domain_name));
        }

        TEST_ASSERT(MA_OK == ma_repository_set_user_name(my_repository, repositories[counter].repo_user_name));
        TEST_ASSERT(MA_OK == ma_repository_set_password(my_repository, repositories[counter].repo_password));
        TEST_ASSERT(MA_OK == ma_repository_set_pingtime(my_repository, repositories[counter].repo_pingtime));
        TEST_ASSERT(MA_OK == ma_repository_set_subnetdistance(my_repository, repositories[counter].repo_subnetdistance));
        TEST_ASSERT(MA_OK == ma_repository_set_siteorder(my_repository, repositories[counter].repo_siteorder));
        TEST_ASSERT(MA_OK == ma_repository_set_state(my_repository, repositories[counter].repo_state));

        // Adding repositories to DB
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_add_repository(NULL, my_repository));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_add_repository(repo_db, NULL));
        TEST_ASSERT(MA_OK == ma_repository_db_add_repository(repo_db, my_repository));

        // Releasing Repository object after adding it to DB
        TEST_ASSERT(MA_OK == ma_repository_release(my_repository));
    }

    // Verifying repository state
    for(counter = 0; counter < sizeof(repositories)/sizeof(repositories[0]); counter++)
    {
        ma_repository_state_t my_state = MA_REPOSITORY_STATE_REMOVED;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_get_repository_state(NULL, repositories[counter].repo_name, &my_state));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_get_repository_state(repo_db, NULL, &my_state));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_get_repository_state(repo_db, repositories[counter].repo_name, NULL));
        TEST_ASSERT(MA_ERROR_OBJECTNOTFOUND == ma_repository_db_get_repository_state(repo_db, "non_existent_repo", &my_state));

        TEST_ASSERT(MA_OK == ma_repository_db_get_repository_state(repo_db, repositories[counter].repo_name, &my_state));
        TEST_ASSERT(repositories[counter].repo_state == my_state);
    }

    // Setting repository state for all repositories to MA_REPOSITORY_STATE_ALTERED
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_set_all_repository_state(NULL, MA_REPOSITORY_STATE_ALTERED));
    TEST_ASSERT(MA_OK == ma_repository_db_set_all_repository_state(repo_db, MA_REPOSITORY_STATE_ALTERED));

    // Verifying repository state for all repositories is set to MA_REPOSITORY_STATE_ALTERED (except for fallback repositories)
    for(counter = 0; counter < sizeof(repositories)/sizeof(repositories[0]); counter++)
    {
        ma_repository_state_t my_state = MA_REPOSITORY_STATE_REMOVED;

        if(repositories[counter].repo_type != MA_REPOSITORY_TYPE_FALLBACK)
        {
            TEST_ASSERT(MA_OK == ma_repository_db_get_repository_state(repo_db, repositories[counter].repo_name, &my_state));
            TEST_ASSERT(MA_REPOSITORY_STATE_ALTERED == my_state);
        }
        else
        {
            TEST_ASSERT(MA_OK == ma_repository_db_get_repository_state(repo_db, repositories[counter].repo_name, &my_state));
            TEST_ASSERT(repositories[counter].repo_state == my_state);
        }
    }

    // Setting repository state for all repositories to MA_REPOSITORY_STATE_ADDED
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_set_all_repository_state(NULL, MA_REPOSITORY_STATE_ADDED));
    TEST_ASSERT(MA_OK == ma_repository_db_set_all_repository_state(repo_db, MA_REPOSITORY_STATE_ADDED));

    // Verifying repository state for all repositories is set to MA_REPOSITORY_STATE_ADDED (except for fallback repositories)
    for(counter = 0; counter < sizeof(repositories)/sizeof(repositories[0]); counter++)
    {
        ma_repository_state_t my_state = MA_REPOSITORY_STATE_REMOVED;

        if(repositories[counter].repo_type != MA_REPOSITORY_TYPE_FALLBACK)
        {
            TEST_ASSERT(MA_OK == ma_repository_db_get_repository_state(repo_db, repositories[counter].repo_name, &my_state));
            TEST_ASSERT(MA_REPOSITORY_STATE_ADDED == my_state);
        }
        else
        {
            TEST_ASSERT(MA_OK == ma_repository_db_get_repository_state(repo_db, repositories[counter].repo_name, &my_state));
            TEST_ASSERT(repositories[counter].repo_state == my_state);
        }
    }

    // Removing All Repositories
    TEST_ASSERT(MA_OK == ma_repository_db_remove_all_repositories(repo_db));
}


/******************************************************************************
Test for verifying the ma_repository_db_get_repositories API
******************************************************************************/
TEST(repository_db_manager_tests, get_repositories_test)
{
    ma_uint8_t counter = 0;

    // Crypto create
    create_macrypto_object_with_keys(MA_CRYPTO_MODE_NON_FIPS, MA_CRYPTO_ROLE_USER, MA_CRYPTO_AGENT_MANAGED, &crypto_object);

    // Adding Repositories to DB
    for(counter = 0; counter < sizeof(repositories)/sizeof(repositories[0]); counter++)
    {
        // Creating repository and filling all parameters
        ma_repository_t *my_repository = NULL;

        TEST_ASSERT(MA_OK == ma_repository_create(&my_repository));
        TEST_ASSERT(MA_OK == ma_repository_set_name(my_repository, repositories[counter].repo_name));
        TEST_ASSERT(MA_OK == ma_repository_set_type(my_repository, repositories[counter].repo_type));
        TEST_ASSERT(MA_OK == ma_repository_set_url_type(my_repository, repositories[counter].repo_url_type));
        TEST_ASSERT(MA_OK == ma_repository_set_namespace(my_repository, repositories[counter].repo_namespace));
        TEST_ASSERT(MA_OK == ma_repository_set_proxy_usage_type(my_repository, repositories[counter].repo_proxy_usage_type));
        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, repositories[counter].repo_auth));
        TEST_ASSERT(MA_OK == ma_repository_set_enabled(my_repository, repositories[counter].repo_enabled));

        if(repositories[counter].repo_url_type != MA_REPOSITORY_URL_TYPE_LOCAL)
        {
            TEST_ASSERT(MA_OK == ma_repository_set_server_fqdn(my_repository, repositories[counter].repo_server_fqdn));
        }

        if((repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_SPIPE) || (repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_SA))
        {
            TEST_ASSERT(MA_OK == ma_repository_set_server_name(my_repository, repositories[counter].repo_server_name));
            TEST_ASSERT(MA_OK == ma_repository_set_server_ip(my_repository, repositories[counter].repo_server_ip));
        }

        if((repositories[counter].repo_url_type != MA_REPOSITORY_URL_TYPE_UNC) && (repositories[counter].repo_url_type != MA_REPOSITORY_URL_TYPE_LOCAL))
        {
            TEST_ASSERT(MA_OK == ma_repository_set_port(my_repository, repositories[counter].repo_port));
        }

        TEST_ASSERT(MA_OK == ma_repository_set_server_path(my_repository, repositories[counter].repo_server_path));

        if((repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_UNC) || (repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_LOCAL))
        {
            TEST_ASSERT(MA_OK == ma_repository_set_domain_name(my_repository, repositories[counter].repo_domain_name));
        }

        TEST_ASSERT(MA_OK == ma_repository_set_user_name(my_repository, repositories[counter].repo_user_name));
        TEST_ASSERT(MA_OK == ma_repository_set_password(my_repository, repositories[counter].repo_password));
        TEST_ASSERT(MA_OK == ma_repository_set_pingtime(my_repository, repositories[counter].repo_pingtime));
        TEST_ASSERT(MA_OK == ma_repository_set_subnetdistance(my_repository, repositories[counter].repo_subnetdistance));
        TEST_ASSERT(MA_OK == ma_repository_set_siteorder(my_repository, repositories[counter].repo_siteorder));
        TEST_ASSERT(MA_OK == ma_repository_set_state(my_repository, repositories[counter].repo_state));

        // Adding repositories to DB
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_add_repository(NULL, my_repository));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_add_repository(repo_db, NULL));
        TEST_ASSERT(MA_OK == ma_repository_db_add_repository(repo_db, my_repository));

        // Releasing Repository object after adding it to DB
        TEST_ASSERT(MA_OK == ma_repository_release(my_repository));
    }

    // Retrieving repository by Ping Time (Expected sequence - Repository_6, Repository_4,
    // Repository_5, Repository_1, Repository_2, Respository_3).
    //
    // Note -
    // 1. Only few repository parameters relevant to this test, are verified here.
    // 2. Fallback repositories comes last in ascending order of priority because, their
    //    pingtime value is changed to INT_MAX before being written into DB.
    {
        ma_repository_list_t *repo_list = NULL;
        size_t size_of_repo_list = 0;

        ma_repository_t *new_repository = NULL;
        ma_repository_type_t new_repository_type = MA_REPOSITORY_TYPE_FALLBACK;
        ma_repository_state_t new_repository_state = MA_REPOSITORY_STATE_ADDED;
        const char *new_repository_name = NULL;
        ma_repository_namespace_t new_repository_namespace = MA_REPOSITORY_NAMESPACE_GLOBAL;
        ma_proxy_usage_type_t new_proxy_usage_type = MA_PROXY_USAGE_TYPE_SYSTEM_CONFIGURED;
        ma_repository_url_type_t new_repository_url_type = MA_REPOSITORY_URL_TYPE_HTTP;
        ma_uint32_t new_pingtime = 234;
        ma_uint32_t new_subnetdistance = 10;
        ma_uint32_t new_siteorder = 10;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_get_repositories(NULL, crypto_object, "*", MA_REPOSITORY_RANK_BY_PING_TIME, &repo_list));
         TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_get_repositories(repo_db, crypto_object, "*", MA_REPOSITORY_RANK_BY_PING_TIME, NULL));
        TEST_ASSERT(MA_OK == ma_repository_db_get_repositories(repo_db, crypto_object, "*", MA_REPOSITORY_RANK_BY_PING_TIME, &repo_list));

        TEST_ASSERT(MA_OK == ma_repository_list_get_repositories_count(repo_list, &size_of_repo_list));
        TEST_ASSERT(sizeof(repositories)/sizeof(repositories[0]) == size_of_repo_list);

        {
            TEST_ASSERT(MA_OK == ma_repository_list_get_repository_by_index(repo_list, 0, &new_repository));

            TEST_ASSERT(MA_OK == ma_repository_get_type(new_repository, &new_repository_type));
            TEST_ASSERT(MA_REPOSITORY_TYPE_REPO == new_repository_type);

            TEST_ASSERT(MA_OK == ma_repository_get_state(new_repository, &new_repository_state));
            TEST_ASSERT(MA_REPOSITORY_STATE_DEFAULT == new_repository_state);

            TEST_ASSERT(MA_OK == ma_repository_get_name(new_repository, &new_repository_name));
            TEST_ASSERT(strcmp("Repository_6", new_repository_name) == 0);

            TEST_ASSERT(MA_OK == ma_repository_get_namespace(new_repository, &new_repository_namespace));
            TEST_ASSERT(MA_REPOSITORY_NAMESPACE_LOCAL == new_repository_namespace);

            TEST_ASSERT(MA_OK == ma_repository_get_proxy_usage_type(new_repository, &new_proxy_usage_type));
            TEST_ASSERT(MA_PROXY_USAGE_TYPE_NONE == new_proxy_usage_type);

            TEST_ASSERT(MA_OK == ma_repository_get_url_type(new_repository, &new_repository_url_type));
            TEST_ASSERT(MA_REPOSITORY_URL_TYPE_SA == new_repository_url_type);

            TEST_ASSERT(MA_OK == ma_repository_get_pingtime(new_repository, &new_pingtime));
            TEST_ASSERT(200 == new_pingtime);

            TEST_ASSERT(MA_OK == ma_repository_get_subnetdistance(new_repository, &new_subnetdistance));
            TEST_ASSERT(10 == new_subnetdistance);

            TEST_ASSERT(MA_OK == ma_repository_get_siteorder(new_repository, &new_siteorder));
            TEST_ASSERT(5 == new_siteorder);

            TEST_ASSERT(MA_OK == ma_repository_release(new_repository));
            new_repository = NULL;
        }

        {
            TEST_ASSERT(MA_OK == ma_repository_list_get_repository_by_index(repo_list, 1, &new_repository));

            TEST_ASSERT(MA_OK == ma_repository_get_type(new_repository, &new_repository_type));
            TEST_ASSERT(MA_REPOSITORY_TYPE_REPO == new_repository_type);

            TEST_ASSERT(MA_OK == ma_repository_get_state(new_repository, &new_repository_state));
            TEST_ASSERT(MA_REPOSITORY_STATE_DEFAULT == new_repository_state);

            TEST_ASSERT(MA_OK == ma_repository_get_name(new_repository, &new_repository_name));
            TEST_ASSERT(strcmp("Repository_4", new_repository_name) == 0);

            TEST_ASSERT(MA_OK == ma_repository_get_namespace(new_repository, &new_repository_namespace));
            TEST_ASSERT(MA_REPOSITORY_NAMESPACE_GLOBAL == new_repository_namespace);

            TEST_ASSERT(MA_OK == ma_repository_get_proxy_usage_type(new_repository, &new_proxy_usage_type));
            TEST_ASSERT(MA_PROXY_USAGE_TYPE_SERVER_CONFIGURED == new_proxy_usage_type);

            TEST_ASSERT(MA_OK == ma_repository_get_url_type(new_repository, &new_repository_url_type));
            TEST_ASSERT(MA_REPOSITORY_URL_TYPE_SPIPE == new_repository_url_type);

            TEST_ASSERT(MA_OK == ma_repository_get_pingtime(new_repository, &new_pingtime));
            TEST_ASSERT(500 == new_pingtime);

            TEST_ASSERT(MA_OK == ma_repository_get_subnetdistance(new_repository, &new_subnetdistance));
            TEST_ASSERT(0 == new_subnetdistance);

            TEST_ASSERT(MA_OK == ma_repository_get_siteorder(new_repository, &new_siteorder));
            TEST_ASSERT(7 == new_siteorder);

            TEST_ASSERT(MA_OK == ma_repository_release(new_repository));
            new_repository = NULL;
        }

        {
            TEST_ASSERT(MA_OK == ma_repository_list_get_repository_by_index(repo_list, 2, &new_repository));

            TEST_ASSERT(MA_OK == ma_repository_get_type(new_repository, &new_repository_type));
            TEST_ASSERT(MA_REPOSITORY_TYPE_MASTER == new_repository_type);

            TEST_ASSERT(MA_OK == ma_repository_get_state(new_repository, &new_repository_state));
            TEST_ASSERT(MA_REPOSITORY_STATE_ALTERED == new_repository_state);

            TEST_ASSERT(MA_OK == ma_repository_get_name(new_repository, &new_repository_name));
            TEST_ASSERT(strcmp("Repository_5", new_repository_name) == 0);

            TEST_ASSERT(MA_OK == ma_repository_get_namespace(new_repository, &new_repository_namespace));
            TEST_ASSERT(MA_REPOSITORY_NAMESPACE_LOCAL == new_repository_namespace);

            TEST_ASSERT(MA_OK == ma_repository_get_proxy_usage_type(new_repository, &new_proxy_usage_type));
            TEST_ASSERT(MA_PROXY_USAGE_TYPE_SERVER_CONFIGURED == new_proxy_usage_type);

            TEST_ASSERT(MA_OK == ma_repository_get_url_type(new_repository, &new_repository_url_type));
            TEST_ASSERT(MA_REPOSITORY_URL_TYPE_UNC == new_repository_url_type);

            TEST_ASSERT(MA_OK == ma_repository_get_pingtime(new_repository, &new_pingtime));
            TEST_ASSERT(900 == new_pingtime);

            TEST_ASSERT(MA_OK == ma_repository_get_subnetdistance(new_repository, &new_subnetdistance));
            TEST_ASSERT(1000 == new_subnetdistance);

            TEST_ASSERT(MA_OK == ma_repository_get_siteorder(new_repository, &new_siteorder));
            TEST_ASSERT(10 == new_siteorder);

            TEST_ASSERT(MA_OK == ma_repository_release(new_repository));
            new_repository = NULL;
        }

        {
            TEST_ASSERT(MA_OK == ma_repository_list_get_repository_by_index(repo_list, 3, &new_repository));

            TEST_ASSERT(MA_OK == ma_repository_get_type(new_repository, &new_repository_type));
            TEST_ASSERT(MA_REPOSITORY_TYPE_MASTER == new_repository_type);

            TEST_ASSERT(MA_OK == ma_repository_get_state(new_repository, &new_repository_state));
            TEST_ASSERT(MA_REPOSITORY_STATE_ADDED == new_repository_state);

            TEST_ASSERT(MA_OK == ma_repository_get_name(new_repository, &new_repository_name));
            TEST_ASSERT(strcmp("Repository_1", new_repository_name) == 0);

            TEST_ASSERT(MA_OK == ma_repository_get_namespace(new_repository, &new_repository_namespace));
            TEST_ASSERT(MA_REPOSITORY_NAMESPACE_LOCAL == new_repository_namespace);

            TEST_ASSERT(MA_OK == ma_repository_get_proxy_usage_type(new_repository, &new_proxy_usage_type));
            TEST_ASSERT(MA_PROXY_USAGE_TYPE_SERVER_CONFIGURED == new_proxy_usage_type);

            TEST_ASSERT(MA_OK == ma_repository_get_url_type(new_repository, &new_repository_url_type));
            TEST_ASSERT(MA_REPOSITORY_URL_TYPE_FTP == new_repository_url_type);

            TEST_ASSERT(MA_OK == ma_repository_get_pingtime(new_repository, &new_pingtime));
            TEST_ASSERT(1000 == new_pingtime);

            TEST_ASSERT(MA_OK == ma_repository_get_subnetdistance(new_repository, &new_subnetdistance));
            TEST_ASSERT(4 == new_subnetdistance);

            TEST_ASSERT(MA_OK == ma_repository_get_siteorder(new_repository, &new_siteorder));
            TEST_ASSERT(3 == new_siteorder);

            TEST_ASSERT(MA_OK == ma_repository_release(new_repository));
            new_repository = NULL;
        }

        {
            TEST_ASSERT(MA_OK == ma_repository_list_get_repository_by_index(repo_list, 4, &new_repository));

            TEST_ASSERT(MA_OK == ma_repository_get_type(new_repository, &new_repository_type));
            TEST_ASSERT(MA_REPOSITORY_TYPE_FALLBACK == new_repository_type);

            TEST_ASSERT(MA_OK == ma_repository_get_state(new_repository, &new_repository_state));
            TEST_ASSERT(MA_REPOSITORY_STATE_DEFAULT == new_repository_state);

            TEST_ASSERT(MA_OK == ma_repository_get_name(new_repository, &new_repository_name));
            TEST_ASSERT(strcmp("Repository_2", new_repository_name) == 0);

            TEST_ASSERT(MA_OK == ma_repository_get_namespace(new_repository, &new_repository_namespace));
            TEST_ASSERT(MA_REPOSITORY_NAMESPACE_GLOBAL == new_repository_namespace);

            TEST_ASSERT(MA_OK == ma_repository_get_proxy_usage_type(new_repository, &new_proxy_usage_type));
            TEST_ASSERT(MA_PROXY_USAGE_TYPE_SYSTEM_CONFIGURED == new_proxy_usage_type);

            TEST_ASSERT(MA_OK == ma_repository_get_url_type(new_repository, &new_repository_url_type));
            TEST_ASSERT(MA_REPOSITORY_URL_TYPE_HTTP == new_repository_url_type);

            // Pingtime and Subnet distance for a fallback repository gets changed to INT_MAX
            // before being written into DB
            TEST_ASSERT(MA_OK == ma_repository_get_pingtime(new_repository, &new_pingtime));
            TEST_ASSERT(INT_MAX == new_pingtime);

            TEST_ASSERT(MA_OK == ma_repository_get_subnetdistance(new_repository, &new_subnetdistance));
            TEST_ASSERT(INT_MAX == new_subnetdistance);

            TEST_ASSERT(MA_OK == ma_repository_get_siteorder(new_repository, &new_siteorder));
            TEST_ASSERT(30 == new_siteorder);

            TEST_ASSERT(MA_OK == ma_repository_release(new_repository));
            new_repository = NULL;
        }

        {
            TEST_ASSERT(MA_OK == ma_repository_list_get_repository_by_index(repo_list, 5, &new_repository));

            TEST_ASSERT(MA_OK == ma_repository_get_type(new_repository, &new_repository_type));
            TEST_ASSERT(MA_REPOSITORY_TYPE_FALLBACK == new_repository_type);

            TEST_ASSERT(MA_OK == ma_repository_get_state(new_repository, &new_repository_state));
            TEST_ASSERT(MA_REPOSITORY_STATE_ALTERED == new_repository_state);

            TEST_ASSERT(MA_OK == ma_repository_get_name(new_repository, &new_repository_name));
            TEST_ASSERT(strcmp("Repository_3", new_repository_name) == 0);

            TEST_ASSERT(MA_OK == ma_repository_get_namespace(new_repository, &new_repository_namespace));
            TEST_ASSERT(MA_REPOSITORY_NAMESPACE_GLOBAL == new_repository_namespace);

            TEST_ASSERT(MA_OK == ma_repository_get_proxy_usage_type(new_repository, &new_proxy_usage_type));
            TEST_ASSERT(MA_PROXY_USAGE_TYPE_SYSTEM_CONFIGURED == new_proxy_usage_type);

            TEST_ASSERT(MA_OK == ma_repository_get_url_type(new_repository, &new_repository_url_type));
            TEST_ASSERT(MA_REPOSITORY_URL_TYPE_FTP == new_repository_url_type);

            // Pingtime and Subnet distance for a fallback repository gets changed to INT_MAX
            // before being written into DB
            TEST_ASSERT(MA_OK == ma_repository_get_pingtime(new_repository, &new_pingtime));
            TEST_ASSERT(INT_MAX == new_pingtime);

            TEST_ASSERT(MA_OK == ma_repository_get_subnetdistance(new_repository, &new_subnetdistance));
            TEST_ASSERT(INT_MAX == new_subnetdistance);

            TEST_ASSERT(MA_OK == ma_repository_get_siteorder(new_repository, &new_siteorder));
            TEST_ASSERT(1 == new_siteorder);

            TEST_ASSERT(MA_OK == ma_repository_release(new_repository));
            new_repository = NULL;
        }

        // Release repository list
        TEST_ASSERT(MA_OK == ma_repository_list_release(repo_list));
        repo_list = NULL;
    }

    // Retrieving repository by Subnet Distance (Expected sequence - Repository_4, Repository_1,
    // Repository_6, Repository_5, Repository_2, Respository_3).
    //
    // Note -
    // 1. Only few repository parameters relevant to this test, are verified here.
    // 2. Fallback repositories comes last in ascending order of priority because, their subnet
    //    distance value is changed to INT_MAX before being written into DB.
    {
        ma_repository_list_t *repo_list = NULL;
        size_t size_of_repo_list = 0;

        ma_repository_t *new_repository = NULL;
        ma_repository_type_t new_repository_type = MA_REPOSITORY_TYPE_FALLBACK;
        ma_repository_state_t new_repository_state = MA_REPOSITORY_STATE_ADDED;
        const char *new_repository_name = NULL;
        ma_repository_namespace_t new_repository_namespace = MA_REPOSITORY_NAMESPACE_GLOBAL;
        ma_proxy_usage_type_t new_proxy_usage_type = MA_PROXY_USAGE_TYPE_SYSTEM_CONFIGURED;
        ma_repository_url_type_t new_repository_url_type = MA_REPOSITORY_URL_TYPE_HTTP;
        ma_uint32_t new_pingtime = 234;
        ma_uint32_t new_subnetdistance = 10;
        ma_uint32_t new_siteorder = 10;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_get_repositories(NULL, crypto_object, "*", MA_REPOSITORY_RANK_BY_SUBNET_DISTANCE, &repo_list));
         TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_get_repositories(repo_db, crypto_object, "*", MA_REPOSITORY_RANK_BY_SUBNET_DISTANCE, NULL));
        TEST_ASSERT(MA_OK == ma_repository_db_get_repositories(repo_db, crypto_object, "*", MA_REPOSITORY_RANK_BY_SUBNET_DISTANCE, &repo_list));

        TEST_ASSERT(MA_OK == ma_repository_list_get_repositories_count(repo_list, &size_of_repo_list));
        TEST_ASSERT(sizeof(repositories)/sizeof(repositories[0]) == size_of_repo_list);

        {
            TEST_ASSERT(MA_OK == ma_repository_list_get_repository_by_index(repo_list, 0, &new_repository));

            TEST_ASSERT(MA_OK == ma_repository_get_type(new_repository, &new_repository_type));
            TEST_ASSERT(MA_REPOSITORY_TYPE_REPO == new_repository_type);

            TEST_ASSERT(MA_OK == ma_repository_get_state(new_repository, &new_repository_state));
            TEST_ASSERT(MA_REPOSITORY_STATE_DEFAULT == new_repository_state);

            TEST_ASSERT(MA_OK == ma_repository_get_name(new_repository, &new_repository_name));
            TEST_ASSERT(strcmp("Repository_4", new_repository_name) == 0);

            TEST_ASSERT(MA_OK == ma_repository_get_namespace(new_repository, &new_repository_namespace));
            TEST_ASSERT(MA_REPOSITORY_NAMESPACE_GLOBAL == new_repository_namespace);

            TEST_ASSERT(MA_OK == ma_repository_get_proxy_usage_type(new_repository, &new_proxy_usage_type));
            TEST_ASSERT(MA_PROXY_USAGE_TYPE_SERVER_CONFIGURED == new_proxy_usage_type);

            TEST_ASSERT(MA_OK == ma_repository_get_url_type(new_repository, &new_repository_url_type));
            TEST_ASSERT(MA_REPOSITORY_URL_TYPE_SPIPE == new_repository_url_type);

            TEST_ASSERT(MA_OK == ma_repository_get_pingtime(new_repository, &new_pingtime));
            TEST_ASSERT(500 == new_pingtime);

            TEST_ASSERT(MA_OK == ma_repository_get_subnetdistance(new_repository, &new_subnetdistance));
            TEST_ASSERT(0 == new_subnetdistance);

            TEST_ASSERT(MA_OK == ma_repository_get_siteorder(new_repository, &new_siteorder));
            TEST_ASSERT(7 == new_siteorder);

            TEST_ASSERT(MA_OK == ma_repository_release(new_repository));
            new_repository = NULL;
        }

        {
            TEST_ASSERT(MA_OK == ma_repository_list_get_repository_by_index(repo_list, 1, &new_repository));

            TEST_ASSERT(MA_OK == ma_repository_get_type(new_repository, &new_repository_type));
            TEST_ASSERT(MA_REPOSITORY_TYPE_MASTER == new_repository_type);

            TEST_ASSERT(MA_OK == ma_repository_get_state(new_repository, &new_repository_state));
            TEST_ASSERT(MA_REPOSITORY_STATE_ADDED == new_repository_state);

            TEST_ASSERT(MA_OK == ma_repository_get_name(new_repository, &new_repository_name));
            TEST_ASSERT(strcmp("Repository_1", new_repository_name) == 0);

            TEST_ASSERT(MA_OK == ma_repository_get_namespace(new_repository, &new_repository_namespace));
            TEST_ASSERT(MA_REPOSITORY_NAMESPACE_LOCAL == new_repository_namespace);

            TEST_ASSERT(MA_OK == ma_repository_get_proxy_usage_type(new_repository, &new_proxy_usage_type));
            TEST_ASSERT(MA_PROXY_USAGE_TYPE_SERVER_CONFIGURED == new_proxy_usage_type);

            TEST_ASSERT(MA_OK == ma_repository_get_url_type(new_repository, &new_repository_url_type));
            TEST_ASSERT(MA_REPOSITORY_URL_TYPE_FTP == new_repository_url_type);

            TEST_ASSERT(MA_OK == ma_repository_get_pingtime(new_repository, &new_pingtime));
            TEST_ASSERT(1000 == new_pingtime);

            TEST_ASSERT(MA_OK == ma_repository_get_subnetdistance(new_repository, &new_subnetdistance));
            TEST_ASSERT(4 == new_subnetdistance);

            TEST_ASSERT(MA_OK == ma_repository_get_siteorder(new_repository, &new_siteorder));
            TEST_ASSERT(3 == new_siteorder);

            TEST_ASSERT(MA_OK == ma_repository_release(new_repository));
            new_repository = NULL;
        }

        {
            TEST_ASSERT(MA_OK == ma_repository_list_get_repository_by_index(repo_list, 2, &new_repository));

            TEST_ASSERT(MA_OK == ma_repository_get_type(new_repository, &new_repository_type));
            TEST_ASSERT(MA_REPOSITORY_TYPE_REPO == new_repository_type);

            TEST_ASSERT(MA_OK == ma_repository_get_state(new_repository, &new_repository_state));
            TEST_ASSERT(MA_REPOSITORY_STATE_DEFAULT == new_repository_state);

            TEST_ASSERT(MA_OK == ma_repository_get_name(new_repository, &new_repository_name));
            TEST_ASSERT(strcmp("Repository_6", new_repository_name) == 0);

            TEST_ASSERT(MA_OK == ma_repository_get_namespace(new_repository, &new_repository_namespace));
            TEST_ASSERT(MA_REPOSITORY_NAMESPACE_LOCAL == new_repository_namespace);

            TEST_ASSERT(MA_OK == ma_repository_get_proxy_usage_type(new_repository, &new_proxy_usage_type));
            TEST_ASSERT(MA_PROXY_USAGE_TYPE_NONE == new_proxy_usage_type);

            TEST_ASSERT(MA_OK == ma_repository_get_url_type(new_repository, &new_repository_url_type));
            TEST_ASSERT(MA_REPOSITORY_URL_TYPE_SA == new_repository_url_type);

            TEST_ASSERT(MA_OK == ma_repository_get_pingtime(new_repository, &new_pingtime));
            TEST_ASSERT(200 == new_pingtime);

            TEST_ASSERT(MA_OK == ma_repository_get_subnetdistance(new_repository, &new_subnetdistance));
            TEST_ASSERT(10 == new_subnetdistance);

            TEST_ASSERT(MA_OK == ma_repository_get_siteorder(new_repository, &new_siteorder));
            TEST_ASSERT(5 == new_siteorder);

            TEST_ASSERT(MA_OK == ma_repository_release(new_repository));
            new_repository = NULL;
        }

        {
            TEST_ASSERT(MA_OK == ma_repository_list_get_repository_by_index(repo_list, 3, &new_repository));

            TEST_ASSERT(MA_OK == ma_repository_get_type(new_repository, &new_repository_type));
            TEST_ASSERT(MA_REPOSITORY_TYPE_MASTER == new_repository_type);

            TEST_ASSERT(MA_OK == ma_repository_get_state(new_repository, &new_repository_state));
            TEST_ASSERT(MA_REPOSITORY_STATE_ALTERED == new_repository_state);

            TEST_ASSERT(MA_OK == ma_repository_get_name(new_repository, &new_repository_name));
            TEST_ASSERT(strcmp("Repository_5", new_repository_name) == 0);

            TEST_ASSERT(MA_OK == ma_repository_get_namespace(new_repository, &new_repository_namespace));
            TEST_ASSERT(MA_REPOSITORY_NAMESPACE_LOCAL == new_repository_namespace);

            TEST_ASSERT(MA_OK == ma_repository_get_proxy_usage_type(new_repository, &new_proxy_usage_type));
            TEST_ASSERT(MA_PROXY_USAGE_TYPE_SERVER_CONFIGURED == new_proxy_usage_type);

            TEST_ASSERT(MA_OK == ma_repository_get_url_type(new_repository, &new_repository_url_type));
            TEST_ASSERT(MA_REPOSITORY_URL_TYPE_UNC == new_repository_url_type);

            TEST_ASSERT(MA_OK == ma_repository_get_pingtime(new_repository, &new_pingtime));
            TEST_ASSERT(900 == new_pingtime);

            TEST_ASSERT(MA_OK == ma_repository_get_subnetdistance(new_repository, &new_subnetdistance));
            TEST_ASSERT(1000 == new_subnetdistance);

            TEST_ASSERT(MA_OK == ma_repository_get_siteorder(new_repository, &new_siteorder));
            TEST_ASSERT(10 == new_siteorder);

            TEST_ASSERT(MA_OK == ma_repository_release(new_repository));
            new_repository = NULL;
        }

        {
            TEST_ASSERT(MA_OK == ma_repository_list_get_repository_by_index(repo_list, 4, &new_repository));

            TEST_ASSERT(MA_OK == ma_repository_get_type(new_repository, &new_repository_type));
            TEST_ASSERT(MA_REPOSITORY_TYPE_FALLBACK == new_repository_type);

            TEST_ASSERT(MA_OK == ma_repository_get_state(new_repository, &new_repository_state));
            TEST_ASSERT(MA_REPOSITORY_STATE_DEFAULT == new_repository_state);

            TEST_ASSERT(MA_OK == ma_repository_get_name(new_repository, &new_repository_name));
            TEST_ASSERT(strcmp("Repository_2", new_repository_name) == 0);

            TEST_ASSERT(MA_OK == ma_repository_get_namespace(new_repository, &new_repository_namespace));
            TEST_ASSERT(MA_REPOSITORY_NAMESPACE_GLOBAL == new_repository_namespace);

            TEST_ASSERT(MA_OK == ma_repository_get_proxy_usage_type(new_repository, &new_proxy_usage_type));
            TEST_ASSERT(MA_PROXY_USAGE_TYPE_SYSTEM_CONFIGURED == new_proxy_usage_type);

            TEST_ASSERT(MA_OK == ma_repository_get_url_type(new_repository, &new_repository_url_type));
            TEST_ASSERT(MA_REPOSITORY_URL_TYPE_HTTP == new_repository_url_type);

            // Pingtime and Subnet distance for a fallback repository gets changed to INT_MAX
            // before being written into DB
            TEST_ASSERT(MA_OK == ma_repository_get_pingtime(new_repository, &new_pingtime));
            TEST_ASSERT(INT_MAX == new_pingtime);

            TEST_ASSERT(MA_OK == ma_repository_get_subnetdistance(new_repository, &new_subnetdistance));
            TEST_ASSERT(INT_MAX == new_subnetdistance);

            TEST_ASSERT(MA_OK == ma_repository_get_siteorder(new_repository, &new_siteorder));
            TEST_ASSERT(30 == new_siteorder);

            TEST_ASSERT(MA_OK == ma_repository_release(new_repository));
            new_repository = NULL;
        }

        {
            TEST_ASSERT(MA_OK == ma_repository_list_get_repository_by_index(repo_list, 5, &new_repository));

            TEST_ASSERT(MA_OK == ma_repository_get_type(new_repository, &new_repository_type));
            TEST_ASSERT(MA_REPOSITORY_TYPE_FALLBACK == new_repository_type);

            TEST_ASSERT(MA_OK == ma_repository_get_state(new_repository, &new_repository_state));
            TEST_ASSERT(MA_REPOSITORY_STATE_ALTERED == new_repository_state);

            TEST_ASSERT(MA_OK == ma_repository_get_name(new_repository, &new_repository_name));
            TEST_ASSERT(strcmp("Repository_3", new_repository_name) == 0);

            TEST_ASSERT(MA_OK == ma_repository_get_namespace(new_repository, &new_repository_namespace));
            TEST_ASSERT(MA_REPOSITORY_NAMESPACE_GLOBAL == new_repository_namespace);

            TEST_ASSERT(MA_OK == ma_repository_get_proxy_usage_type(new_repository, &new_proxy_usage_type));
            TEST_ASSERT(MA_PROXY_USAGE_TYPE_SYSTEM_CONFIGURED == new_proxy_usage_type);

            TEST_ASSERT(MA_OK == ma_repository_get_url_type(new_repository, &new_repository_url_type));
            TEST_ASSERT(MA_REPOSITORY_URL_TYPE_FTP == new_repository_url_type);

            // Pingtime and Subnet distance for a fallback repository gets changed to INT_MAX
            // before being written into DB
            TEST_ASSERT(MA_OK == ma_repository_get_pingtime(new_repository, &new_pingtime));
            TEST_ASSERT(INT_MAX == new_pingtime);

            TEST_ASSERT(MA_OK == ma_repository_get_subnetdistance(new_repository, &new_subnetdistance));
            TEST_ASSERT(INT_MAX == new_subnetdistance);

            TEST_ASSERT(MA_OK == ma_repository_get_siteorder(new_repository, &new_siteorder));
            TEST_ASSERT(1 == new_siteorder);

            TEST_ASSERT(MA_OK == ma_repository_release(new_repository));
            new_repository = NULL;
        }

        // Release repository list
        TEST_ASSERT(MA_OK == ma_repository_list_release(repo_list));
        repo_list = NULL;
    }

    // Retrieving repository by SiteOrder (Expected sequence - Repository_3, Repository_1,
    // Repository_6, Repository_4, Repository_5, Respository_2).
    //
    // Note -
    // Only few repository parameters relevant to this test, are verified here.
    {
        ma_repository_list_t *repo_list = NULL;
        size_t size_of_repo_list = 0;

        ma_repository_t *new_repository = NULL;
        ma_repository_type_t new_repository_type = MA_REPOSITORY_TYPE_FALLBACK;
        ma_repository_state_t new_repository_state = MA_REPOSITORY_STATE_ADDED;
        const char *new_repository_name = NULL;
        ma_repository_namespace_t new_repository_namespace = MA_REPOSITORY_NAMESPACE_GLOBAL;
        ma_proxy_usage_type_t new_proxy_usage_type = MA_PROXY_USAGE_TYPE_SYSTEM_CONFIGURED;
        ma_repository_url_type_t new_repository_url_type = MA_REPOSITORY_URL_TYPE_HTTP;
        ma_uint32_t new_pingtime = 234;
        ma_uint32_t new_subnetdistance = 10;
        ma_uint32_t new_siteorder = 10;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_get_repositories(NULL, crypto_object, "*", MA_REPOSITORY_RANK_BY_SITELIST_ORDER, &repo_list));
         TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_get_repositories(repo_db, crypto_object, "*", MA_REPOSITORY_RANK_BY_SITELIST_ORDER, NULL));
        TEST_ASSERT(MA_OK == ma_repository_db_get_repositories(repo_db, crypto_object, "*", MA_REPOSITORY_RANK_BY_SITELIST_ORDER, &repo_list));

        TEST_ASSERT(MA_OK == ma_repository_list_get_repositories_count(repo_list, &size_of_repo_list));
        TEST_ASSERT(sizeof(repositories)/sizeof(repositories[0]) == size_of_repo_list);

        {
            TEST_ASSERT(MA_OK == ma_repository_list_get_repository_by_index(repo_list, 0, &new_repository));

            TEST_ASSERT(MA_OK == ma_repository_get_type(new_repository, &new_repository_type));
            TEST_ASSERT(MA_REPOSITORY_TYPE_FALLBACK == new_repository_type);

            TEST_ASSERT(MA_OK == ma_repository_get_state(new_repository, &new_repository_state));
            TEST_ASSERT(MA_REPOSITORY_STATE_ALTERED == new_repository_state);

            TEST_ASSERT(MA_OK == ma_repository_get_name(new_repository, &new_repository_name));
            TEST_ASSERT(strcmp("Repository_3", new_repository_name) == 0);

            TEST_ASSERT(MA_OK == ma_repository_get_namespace(new_repository, &new_repository_namespace));
            TEST_ASSERT(MA_REPOSITORY_NAMESPACE_GLOBAL == new_repository_namespace);

            TEST_ASSERT(MA_OK == ma_repository_get_proxy_usage_type(new_repository, &new_proxy_usage_type));
            TEST_ASSERT(MA_PROXY_USAGE_TYPE_SYSTEM_CONFIGURED == new_proxy_usage_type);

            TEST_ASSERT(MA_OK == ma_repository_get_url_type(new_repository, &new_repository_url_type));
            TEST_ASSERT(MA_REPOSITORY_URL_TYPE_FTP == new_repository_url_type);

            // Pingtime and Subnet distance for a fallback repository gets changed to INT_MAX
            // before being written into DB
            TEST_ASSERT(MA_OK == ma_repository_get_pingtime(new_repository, &new_pingtime));
            TEST_ASSERT(INT_MAX == new_pingtime);

            TEST_ASSERT(MA_OK == ma_repository_get_subnetdistance(new_repository, &new_subnetdistance));
            TEST_ASSERT(INT_MAX == new_subnetdistance);

            TEST_ASSERT(MA_OK == ma_repository_get_siteorder(new_repository, &new_siteorder));
            TEST_ASSERT(1 == new_siteorder);

            TEST_ASSERT(MA_OK == ma_repository_release(new_repository));
            new_repository = NULL;
        }

        {
            TEST_ASSERT(MA_OK == ma_repository_list_get_repository_by_index(repo_list, 1, &new_repository));

            TEST_ASSERT(MA_OK == ma_repository_get_type(new_repository, &new_repository_type));
            TEST_ASSERT(MA_REPOSITORY_TYPE_MASTER == new_repository_type);

            TEST_ASSERT(MA_OK == ma_repository_get_state(new_repository, &new_repository_state));
            TEST_ASSERT(MA_REPOSITORY_STATE_ADDED == new_repository_state);

            TEST_ASSERT(MA_OK == ma_repository_get_name(new_repository, &new_repository_name));
            TEST_ASSERT(strcmp("Repository_1", new_repository_name) == 0);

            TEST_ASSERT(MA_OK == ma_repository_get_namespace(new_repository, &new_repository_namespace));
            TEST_ASSERT(MA_REPOSITORY_NAMESPACE_LOCAL == new_repository_namespace);

            TEST_ASSERT(MA_OK == ma_repository_get_proxy_usage_type(new_repository, &new_proxy_usage_type));
            TEST_ASSERT(MA_PROXY_USAGE_TYPE_SERVER_CONFIGURED == new_proxy_usage_type);

            TEST_ASSERT(MA_OK == ma_repository_get_url_type(new_repository, &new_repository_url_type));
            TEST_ASSERT(MA_REPOSITORY_URL_TYPE_FTP == new_repository_url_type);

            TEST_ASSERT(MA_OK == ma_repository_get_pingtime(new_repository, &new_pingtime));
            TEST_ASSERT(1000 == new_pingtime);

            TEST_ASSERT(MA_OK == ma_repository_get_subnetdistance(new_repository, &new_subnetdistance));
            TEST_ASSERT(4 == new_subnetdistance);

            TEST_ASSERT(MA_OK == ma_repository_get_siteorder(new_repository, &new_siteorder));
            TEST_ASSERT(3 == new_siteorder);

            TEST_ASSERT(MA_OK == ma_repository_release(new_repository));
            new_repository = NULL;
        }

        {
            TEST_ASSERT(MA_OK == ma_repository_list_get_repository_by_index(repo_list, 2, &new_repository));

            TEST_ASSERT(MA_OK == ma_repository_get_type(new_repository, &new_repository_type));
            TEST_ASSERT(MA_REPOSITORY_TYPE_REPO == new_repository_type);

            TEST_ASSERT(MA_OK == ma_repository_get_state(new_repository, &new_repository_state));
            TEST_ASSERT(MA_REPOSITORY_STATE_DEFAULT == new_repository_state);

            TEST_ASSERT(MA_OK == ma_repository_get_name(new_repository, &new_repository_name));
            TEST_ASSERT(strcmp("Repository_6", new_repository_name) == 0);

            TEST_ASSERT(MA_OK == ma_repository_get_namespace(new_repository, &new_repository_namespace));
            TEST_ASSERT(MA_REPOSITORY_NAMESPACE_LOCAL == new_repository_namespace);

            TEST_ASSERT(MA_OK == ma_repository_get_proxy_usage_type(new_repository, &new_proxy_usage_type));
            TEST_ASSERT(MA_PROXY_USAGE_TYPE_NONE == new_proxy_usage_type);

            TEST_ASSERT(MA_OK == ma_repository_get_url_type(new_repository, &new_repository_url_type));
            TEST_ASSERT(MA_REPOSITORY_URL_TYPE_SA == new_repository_url_type);

            TEST_ASSERT(MA_OK == ma_repository_get_pingtime(new_repository, &new_pingtime));
            TEST_ASSERT(200 == new_pingtime);

            TEST_ASSERT(MA_OK == ma_repository_get_subnetdistance(new_repository, &new_subnetdistance));
            TEST_ASSERT(10 == new_subnetdistance);

            TEST_ASSERT(MA_OK == ma_repository_get_siteorder(new_repository, &new_siteorder));
            TEST_ASSERT(5 == new_siteorder);

            TEST_ASSERT(MA_OK == ma_repository_release(new_repository));
            new_repository = NULL;
        }

        {
            TEST_ASSERT(MA_OK == ma_repository_list_get_repository_by_index(repo_list, 3, &new_repository));

            TEST_ASSERT(MA_OK == ma_repository_get_type(new_repository, &new_repository_type));
            TEST_ASSERT(MA_REPOSITORY_TYPE_REPO == new_repository_type);

            TEST_ASSERT(MA_OK == ma_repository_get_state(new_repository, &new_repository_state));
            TEST_ASSERT(MA_REPOSITORY_STATE_DEFAULT == new_repository_state);

            TEST_ASSERT(MA_OK == ma_repository_get_name(new_repository, &new_repository_name));
            TEST_ASSERT(strcmp("Repository_4", new_repository_name) == 0);

            TEST_ASSERT(MA_OK == ma_repository_get_namespace(new_repository, &new_repository_namespace));
            TEST_ASSERT(MA_REPOSITORY_NAMESPACE_GLOBAL == new_repository_namespace);

            TEST_ASSERT(MA_OK == ma_repository_get_proxy_usage_type(new_repository, &new_proxy_usage_type));
            TEST_ASSERT(MA_PROXY_USAGE_TYPE_SERVER_CONFIGURED == new_proxy_usage_type);

            TEST_ASSERT(MA_OK == ma_repository_get_url_type(new_repository, &new_repository_url_type));
            TEST_ASSERT(MA_REPOSITORY_URL_TYPE_SPIPE == new_repository_url_type);

            TEST_ASSERT(MA_OK == ma_repository_get_pingtime(new_repository, &new_pingtime));
            TEST_ASSERT(500 == new_pingtime);

            TEST_ASSERT(MA_OK == ma_repository_get_subnetdistance(new_repository, &new_subnetdistance));
            TEST_ASSERT(0 == new_subnetdistance);

            TEST_ASSERT(MA_OK == ma_repository_get_siteorder(new_repository, &new_siteorder));
            TEST_ASSERT(7 == new_siteorder);

            TEST_ASSERT(MA_OK == ma_repository_release(new_repository));
            new_repository = NULL;
        }

        {
            TEST_ASSERT(MA_OK == ma_repository_list_get_repository_by_index(repo_list, 4, &new_repository));

            TEST_ASSERT(MA_OK == ma_repository_get_type(new_repository, &new_repository_type));
            TEST_ASSERT(MA_REPOSITORY_TYPE_MASTER == new_repository_type);

            TEST_ASSERT(MA_OK == ma_repository_get_state(new_repository, &new_repository_state));
            TEST_ASSERT(MA_REPOSITORY_STATE_ALTERED == new_repository_state);

            TEST_ASSERT(MA_OK == ma_repository_get_name(new_repository, &new_repository_name));
            TEST_ASSERT(strcmp("Repository_5", new_repository_name) == 0);

            TEST_ASSERT(MA_OK == ma_repository_get_namespace(new_repository, &new_repository_namespace));
            TEST_ASSERT(MA_REPOSITORY_NAMESPACE_LOCAL == new_repository_namespace);

            TEST_ASSERT(MA_OK == ma_repository_get_proxy_usage_type(new_repository, &new_proxy_usage_type));
            TEST_ASSERT(MA_PROXY_USAGE_TYPE_SERVER_CONFIGURED == new_proxy_usage_type);

            TEST_ASSERT(MA_OK == ma_repository_get_url_type(new_repository, &new_repository_url_type));
            TEST_ASSERT(MA_REPOSITORY_URL_TYPE_UNC == new_repository_url_type);

            TEST_ASSERT(MA_OK == ma_repository_get_pingtime(new_repository, &new_pingtime));
            TEST_ASSERT(900 == new_pingtime);

            TEST_ASSERT(MA_OK == ma_repository_get_subnetdistance(new_repository, &new_subnetdistance));
            TEST_ASSERT(1000 == new_subnetdistance);

            TEST_ASSERT(MA_OK == ma_repository_get_siteorder(new_repository, &new_siteorder));
            TEST_ASSERT(10 == new_siteorder);

            TEST_ASSERT(MA_OK == ma_repository_release(new_repository));
            new_repository = NULL;
        }

        {
            TEST_ASSERT(MA_OK == ma_repository_list_get_repository_by_index(repo_list, 5, &new_repository));

            TEST_ASSERT(MA_OK == ma_repository_get_type(new_repository, &new_repository_type));
            TEST_ASSERT(MA_REPOSITORY_TYPE_FALLBACK == new_repository_type);

            TEST_ASSERT(MA_OK == ma_repository_get_state(new_repository, &new_repository_state));
            TEST_ASSERT(MA_REPOSITORY_STATE_DEFAULT == new_repository_state);

            TEST_ASSERT(MA_OK == ma_repository_get_name(new_repository, &new_repository_name));
            TEST_ASSERT(strcmp("Repository_2", new_repository_name) == 0);

            TEST_ASSERT(MA_OK == ma_repository_get_namespace(new_repository, &new_repository_namespace));
            TEST_ASSERT(MA_REPOSITORY_NAMESPACE_GLOBAL == new_repository_namespace);

            TEST_ASSERT(MA_OK == ma_repository_get_proxy_usage_type(new_repository, &new_proxy_usage_type));
            TEST_ASSERT(MA_PROXY_USAGE_TYPE_SYSTEM_CONFIGURED == new_proxy_usage_type);

            TEST_ASSERT(MA_OK == ma_repository_get_url_type(new_repository, &new_repository_url_type));
            TEST_ASSERT(MA_REPOSITORY_URL_TYPE_HTTP == new_repository_url_type);

            // Pingtime and Subnet distance for a fallback repository gets changed to INT_MAX
            // before being written into DB
            TEST_ASSERT(MA_OK == ma_repository_get_pingtime(new_repository, &new_pingtime));
            TEST_ASSERT(INT_MAX == new_pingtime);

            TEST_ASSERT(MA_OK == ma_repository_get_subnetdistance(new_repository, &new_subnetdistance));
            TEST_ASSERT(INT_MAX == new_subnetdistance);

            TEST_ASSERT(MA_OK == ma_repository_get_siteorder(new_repository, &new_siteorder));
            TEST_ASSERT(30 == new_siteorder);

            TEST_ASSERT(MA_OK == ma_repository_release(new_repository));
            new_repository = NULL;
        }

        // Release repository list
        TEST_ASSERT(MA_OK == ma_repository_list_release(repo_list));
        repo_list = NULL;
    }

    // Removing All Repositories
    TEST_ASSERT(MA_OK == ma_repository_db_remove_all_repositories(repo_db));

    // Release crypto
    release_macrypto_object(MA_CRYPTO_MODE_NON_FIPS, MA_CRYPTO_ROLE_USER,MA_CRYPTO_AGENT_MANAGED, crypto_object);
    crypto_object = NULL;
}


/******************************************************************************
Test for verifying the ma_repository_db_get_spipe_repositories API
******************************************************************************/
TEST(repository_db_manager_tests, get_spipe_repositories_test)
{
    ma_uint8_t counter = 0;

    // Adding Repositories to DB
    for(counter = 0; counter < sizeof(repositories)/sizeof(repositories[0]); counter++)
    {
        // Creating repository and filling all parameters
        ma_repository_t *my_repository = NULL;

        TEST_ASSERT(MA_OK == ma_repository_create(&my_repository));
        TEST_ASSERT(MA_OK == ma_repository_set_name(my_repository, repositories[counter].repo_name));
        TEST_ASSERT(MA_OK == ma_repository_set_type(my_repository, repositories[counter].repo_type));
        TEST_ASSERT(MA_OK == ma_repository_set_url_type(my_repository, repositories[counter].repo_url_type));
        TEST_ASSERT(MA_OK == ma_repository_set_namespace(my_repository, repositories[counter].repo_namespace));
        TEST_ASSERT(MA_OK == ma_repository_set_proxy_usage_type(my_repository, repositories[counter].repo_proxy_usage_type));
        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, repositories[counter].repo_auth));
        TEST_ASSERT(MA_OK == ma_repository_set_enabled(my_repository, repositories[counter].repo_enabled));

        if(repositories[counter].repo_url_type != MA_REPOSITORY_URL_TYPE_LOCAL)
        {
            TEST_ASSERT(MA_OK == ma_repository_set_server_fqdn(my_repository, repositories[counter].repo_server_fqdn));
        }

        if((repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_SPIPE) || (repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_SA))
        {
            TEST_ASSERT(MA_OK == ma_repository_set_server_name(my_repository, repositories[counter].repo_server_name));
            TEST_ASSERT(MA_OK == ma_repository_set_server_ip(my_repository, repositories[counter].repo_server_ip));
        }

        if((repositories[counter].repo_url_type != MA_REPOSITORY_URL_TYPE_UNC) && (repositories[counter].repo_url_type != MA_REPOSITORY_URL_TYPE_LOCAL))
        {
            TEST_ASSERT(MA_OK == ma_repository_set_port(my_repository, repositories[counter].repo_port));
        }

        TEST_ASSERT(MA_OK == ma_repository_set_server_path(my_repository, repositories[counter].repo_server_path));

        if((repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_UNC) || (repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_LOCAL))
        {
            TEST_ASSERT(MA_OK == ma_repository_set_domain_name(my_repository, repositories[counter].repo_domain_name));
        }

        TEST_ASSERT(MA_OK == ma_repository_set_user_name(my_repository, repositories[counter].repo_user_name));
        TEST_ASSERT(MA_OK == ma_repository_set_password(my_repository, repositories[counter].repo_password));
        TEST_ASSERT(MA_OK == ma_repository_set_pingtime(my_repository, repositories[counter].repo_pingtime));
        TEST_ASSERT(MA_OK == ma_repository_set_subnetdistance(my_repository, repositories[counter].repo_subnetdistance));
        TEST_ASSERT(MA_OK == ma_repository_set_siteorder(my_repository, repositories[counter].repo_siteorder));
        TEST_ASSERT(MA_OK == ma_repository_set_state(my_repository, repositories[counter].repo_state));

        // Adding repositories to DB
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_add_repository(NULL, my_repository));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_add_repository(repo_db, NULL));
        TEST_ASSERT(MA_OK == ma_repository_db_add_repository(repo_db, my_repository));

        // Releasing Repository object after adding it to DB
        TEST_ASSERT(MA_OK == ma_repository_release(my_repository));
    }

    // Retrieving repositories where repository URL type is SPIPE. Note that only one repository
    // (Repository_4) should be retrieved.
    {
        ma_db_recordset_t *record_set = NULL;
        char *string_val = NULL;
        int int_val = -1;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_get_spipe_repositories(repo_db, NULL));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_get_spipe_repositories(NULL, &record_set));
        TEST_ASSERT(MA_OK == ma_repository_db_get_spipe_repositories(repo_db, &record_set));

        TEST_ASSERT(MA_OK == ma_db_recordset_next(record_set));
        {
            ma_db_recordset_get_string(record_set, 1, &string_val);
            TEST_ASSERT(strcmp("Repository_4", string_val) == 0);

            ma_db_recordset_get_int(record_set, 2, &int_val);
            TEST_ASSERT(MA_REPOSITORY_TYPE_REPO == int_val);

            ma_db_recordset_get_int(record_set, 3, &int_val);
            TEST_ASSERT(MA_REPOSITORY_URL_TYPE_SPIPE == int_val);

            ma_db_recordset_get_int(record_set, 4, &int_val);
            TEST_ASSERT(MA_REPOSITORY_NAMESPACE_GLOBAL == int_val);

            ma_db_recordset_get_int(record_set, 5, &int_val);
            TEST_ASSERT(MA_PROXY_USAGE_TYPE_SERVER_CONFIGURED == int_val);

            ma_db_recordset_get_int(record_set, 18, &int_val);
            TEST_ASSERT(500 == int_val);

            ma_db_recordset_get_int(record_set, 19, &int_val);
            TEST_ASSERT(0 == int_val);

            ma_db_recordset_get_int(record_set, 20, &int_val);
            TEST_ASSERT(7 == int_val);
        }

        // No more records retrieved from database.
        TEST_ASSERT(MA_ERROR_NO_MORE_ITEMS == ma_db_recordset_next(record_set));

        ma_db_recordset_release(record_set);
        record_set = NULL;
    }

    // Removing All Repositories
    TEST_ASSERT(MA_OK == ma_repository_db_remove_all_repositories(repo_db));
}

/******************************************************************************
Test for verifying the ma_repository_db_set_repository_enabled API.

1) Adding repositories with repository state as enabled.
2) Disabling the repository using ma_repository_db_set_repository_enabled API.
3) Then, retrieving repository list using ma_repository_db_get_repositories API.
4) Call ma_repository_get_enabled() API to verify that all retrieved repositories
   are disabled.
5) Enabling the repository using ma_repository_db_set_repository_enabled API.
6) Then, retrieving repositories using ma_repository_db_get_repositories API.
7) Call ma_repository_get_enabled() API to verify that all retrieved repositories
   are enabled.
******************************************************************************/
TEST(repository_db_manager_tests, set_repository_enabled_test)
{
    ma_uint8_t counter = 0;
    ma_repository_list_t *repo_list = NULL;

    // Adding Repositories to DB with repository state as enabled
    for(counter = 0; counter < sizeof(repositories)/sizeof(repositories[0]); counter++)
    {
        // Creating repository and filling all parameters
        ma_repository_t *my_repository = NULL;

        TEST_ASSERT(MA_OK == ma_repository_create(&my_repository));
        TEST_ASSERT(MA_OK == ma_repository_set_name(my_repository, repositories[counter].repo_name));
        TEST_ASSERT(MA_OK == ma_repository_set_type(my_repository, repositories[counter].repo_type));
        TEST_ASSERT(MA_OK == ma_repository_set_url_type(my_repository, repositories[counter].repo_url_type));
        TEST_ASSERT(MA_OK == ma_repository_set_namespace(my_repository, repositories[counter].repo_namespace));
        TEST_ASSERT(MA_OK == ma_repository_set_proxy_usage_type(my_repository, repositories[counter].repo_proxy_usage_type));
        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, repositories[counter].repo_auth));
        TEST_ASSERT(MA_OK == ma_repository_set_enabled(my_repository, MA_TRUE));

        if(repositories[counter].repo_url_type != MA_REPOSITORY_URL_TYPE_LOCAL)
        {
            TEST_ASSERT(MA_OK == ma_repository_set_server_fqdn(my_repository, repositories[counter].repo_server_fqdn));
        }

        if((repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_SPIPE) || (repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_SA))
        {
            TEST_ASSERT(MA_OK == ma_repository_set_server_name(my_repository, repositories[counter].repo_server_name));
            TEST_ASSERT(MA_OK == ma_repository_set_server_ip(my_repository, repositories[counter].repo_server_ip));
        }

        if((repositories[counter].repo_url_type != MA_REPOSITORY_URL_TYPE_UNC) && (repositories[counter].repo_url_type != MA_REPOSITORY_URL_TYPE_LOCAL))
        {
            TEST_ASSERT(MA_OK == ma_repository_set_port(my_repository, repositories[counter].repo_port));
        }

        TEST_ASSERT(MA_OK == ma_repository_set_server_path(my_repository, repositories[counter].repo_server_path));

        if((repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_UNC) || (repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_LOCAL))
        {
            TEST_ASSERT(MA_OK == ma_repository_set_domain_name(my_repository, repositories[counter].repo_domain_name));
        }

        TEST_ASSERT(MA_OK == ma_repository_set_user_name(my_repository, repositories[counter].repo_user_name));
        TEST_ASSERT(MA_OK == ma_repository_set_password(my_repository, repositories[counter].repo_password));
        TEST_ASSERT(MA_OK == ma_repository_set_pingtime(my_repository, repositories[counter].repo_pingtime));
        TEST_ASSERT(MA_OK == ma_repository_set_subnetdistance(my_repository, repositories[counter].repo_subnetdistance));
        TEST_ASSERT(MA_OK == ma_repository_set_siteorder(my_repository, repositories[counter].repo_siteorder));
        TEST_ASSERT(MA_OK == ma_repository_set_state(my_repository, repositories[counter].repo_state));

        // Adding repositories to DB
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_add_repository(NULL, my_repository));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_add_repository(repo_db, NULL));
        TEST_ASSERT(MA_OK == ma_repository_db_add_repository(repo_db, my_repository));

        // Releasing Repository object after adding it to DB
        TEST_ASSERT(MA_OK == ma_repository_release(my_repository));
    }

    // Setting the repository state to disabled
    for(counter = 0; counter < sizeof(repositories)/sizeof(repositories[0]); counter++)
    {
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_set_repository_enabled(NULL, repositories[counter].repo_name, MA_FALSE));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_set_repository_enabled(repo_db, NULL, MA_FALSE));
        TEST_ASSERT(MA_OK == ma_repository_db_set_repository_enabled(repo_db, repositories[counter].repo_name, MA_FALSE));
    }

    // Retrieving repository by Ping Time. Verifying all 6 records have repository state as disabled.
     TEST_ASSERT(MA_OK == ma_repository_db_get_repositories(repo_db, crypto_object, "*", MA_REPOSITORY_RANK_BY_PING_TIME, &repo_list));

    for(counter = 0; counter < sizeof(repositories)/sizeof(repositories[0]); counter++)
    {
        ma_repository_t *new_repository = NULL;
        ma_bool_t is_enabled = MA_TRUE;

        TEST_ASSERT(MA_OK == ma_repository_list_get_repository_by_index(repo_list, counter, &new_repository));

        TEST_ASSERT(MA_OK == ma_repository_get_enabled(new_repository, &is_enabled));
        TEST_ASSERT(MA_FALSE == is_enabled);

        TEST_ASSERT(MA_OK == ma_repository_release(new_repository));
    }

    // Release repository list
    TEST_ASSERT(MA_OK == ma_repository_list_release(repo_list));

    // Setting the repository state to enabled
    for(counter = 0; counter < sizeof(repositories)/sizeof(repositories[0]); counter++)
    {
        TEST_ASSERT(MA_OK == ma_repository_db_set_repository_enabled(repo_db, repositories[counter].repo_name, MA_TRUE));
    }

    // Retrieving repository by Ping Time. Verifying all 6 records have repository state as enabled.
     TEST_ASSERT(MA_OK == ma_repository_db_get_repositories(repo_db, crypto_object, "*", MA_REPOSITORY_RANK_BY_PING_TIME, &repo_list));

    for(counter = 0; counter < sizeof(repositories)/sizeof(repositories[0]); counter++)
    {
        ma_repository_t *new_repository = NULL;
        ma_bool_t is_enabled = MA_TRUE;

        TEST_ASSERT(MA_OK == ma_repository_list_get_repository_by_index(repo_list, counter, &new_repository));

        TEST_ASSERT(MA_OK == ma_repository_get_enabled(new_repository, &is_enabled));
        TEST_ASSERT(MA_TRUE == is_enabled);

        TEST_ASSERT(MA_OK == ma_repository_release(new_repository));
    }

    // Release repository list
    TEST_ASSERT(MA_OK == ma_repository_list_release(repo_list));

    // Removing All Repositories
    TEST_ASSERT(MA_OK == ma_repository_db_remove_all_repositories(repo_db));
}


/******************************************************************************
Test for verifying the ma_repository_db_set_all_repositories_enabled API.

1) Adding repositories with repository state as disabled.
2) Enabling all repositories by calling the ma_repository_db_set_repository_enabled
   API.
3) Then, retrieving repositories using ma_repository_db_get_repositories API.
4) Call ma_repository_get_enabled() API to verify that all retrieved repositories
   are enabled (return MA_TRUE).
******************************************************************************/
TEST(repository_db_manager_tests, repository_db_set_all_repositories_enabled_test)
{
    ma_uint8_t counter = 0;

    ma_repository_list_t *repo_list = NULL;

    // Adding Repositories to DB with repository state as disabled
    for(counter = 0; counter < sizeof(repositories)/sizeof(repositories[0]); counter++)
    {
        // Creating repository and filling all parameters
        ma_repository_t *my_repository = NULL;

        TEST_ASSERT(MA_OK == ma_repository_create(&my_repository));
        TEST_ASSERT(MA_OK == ma_repository_set_name(my_repository, repositories[counter].repo_name));
        TEST_ASSERT(MA_OK == ma_repository_set_type(my_repository, repositories[counter].repo_type));
        TEST_ASSERT(MA_OK == ma_repository_set_url_type(my_repository, repositories[counter].repo_url_type));
        TEST_ASSERT(MA_OK == ma_repository_set_namespace(my_repository, repositories[counter].repo_namespace));
        TEST_ASSERT(MA_OK == ma_repository_set_proxy_usage_type(my_repository, repositories[counter].repo_proxy_usage_type));
        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, repositories[counter].repo_auth));
        TEST_ASSERT(MA_OK == ma_repository_set_enabled(my_repository, MA_FALSE));

        if(repositories[counter].repo_url_type != MA_REPOSITORY_URL_TYPE_LOCAL)
        {
            TEST_ASSERT(MA_OK == ma_repository_set_server_fqdn(my_repository, repositories[counter].repo_server_fqdn));
        }

        if((repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_SPIPE) || (repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_SA))
        {
            TEST_ASSERT(MA_OK == ma_repository_set_server_name(my_repository, repositories[counter].repo_server_name));
            TEST_ASSERT(MA_OK == ma_repository_set_server_ip(my_repository, repositories[counter].repo_server_ip));
        }

        if((repositories[counter].repo_url_type != MA_REPOSITORY_URL_TYPE_UNC) && (repositories[counter].repo_url_type != MA_REPOSITORY_URL_TYPE_LOCAL))
        {
            TEST_ASSERT(MA_OK == ma_repository_set_port(my_repository, repositories[counter].repo_port));
        }

        TEST_ASSERT(MA_OK == ma_repository_set_server_path(my_repository, repositories[counter].repo_server_path));

        if((repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_UNC) || (repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_LOCAL))
        {
            TEST_ASSERT(MA_OK == ma_repository_set_domain_name(my_repository, repositories[counter].repo_domain_name));
        }

        TEST_ASSERT(MA_OK == ma_repository_set_user_name(my_repository, repositories[counter].repo_user_name));
        TEST_ASSERT(MA_OK == ma_repository_set_password(my_repository, repositories[counter].repo_password));
        TEST_ASSERT(MA_OK == ma_repository_set_pingtime(my_repository, repositories[counter].repo_pingtime));
        TEST_ASSERT(MA_OK == ma_repository_set_subnetdistance(my_repository, repositories[counter].repo_subnetdistance));
        TEST_ASSERT(MA_OK == ma_repository_set_siteorder(my_repository, repositories[counter].repo_siteorder));
        TEST_ASSERT(MA_OK == ma_repository_set_state(my_repository, repositories[counter].repo_state));

        // Adding repositories to DB
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_add_repository(NULL, my_repository));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_add_repository(repo_db, NULL));
        TEST_ASSERT(MA_OK == ma_repository_db_add_repository(repo_db, my_repository));

        // Releasing Repository object after adding it to DB
        TEST_ASSERT(MA_OK == ma_repository_release(my_repository));
    }

    // Setting all repositories to enabled
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_set_all_repositories_enabled(NULL, MA_TRUE));
    TEST_ASSERT(MA_OK == ma_repository_db_set_all_repositories_enabled(repo_db, MA_TRUE));

    // Retrieving repository by Ping Time. Verifying all 6 records have repository state as enabled.
     TEST_ASSERT(MA_OK == ma_repository_db_get_repositories(repo_db, crypto_object, "*", MA_REPOSITORY_RANK_BY_PING_TIME, &repo_list));

    for(counter = 0; counter < sizeof(repositories)/sizeof(repositories[0]); counter++)
    {
        ma_repository_t *new_repository = NULL;
        ma_bool_t is_enabled = MA_TRUE;

        TEST_ASSERT(MA_OK == ma_repository_list_get_repository_by_index(repo_list, counter, &new_repository));

        TEST_ASSERT(MA_OK == ma_repository_get_enabled(new_repository, &is_enabled));
        TEST_ASSERT(MA_TRUE == is_enabled);

        TEST_ASSERT(MA_OK == ma_repository_release(new_repository));
    }

    // Release repository list
    TEST_ASSERT(MA_OK == ma_repository_list_release(repo_list));

    // Removing All Repositories
    TEST_ASSERT(MA_OK == ma_repository_db_remove_all_repositories(repo_db));
}

/******************************************************************************
Test for verifying the ma_repository_db_set_repository_ping_count API
******************************************************************************/
TEST(repository_db_manager_tests, set_repository_ping_count_test)
{
    ma_uint8_t counter = 0;

    ma_repository_list_t *repo_list = NULL;

    // Adding Repositories to DB
    for(counter = 0; counter < sizeof(repositories)/sizeof(repositories[0]); counter++)
    {
        // Creating repository and filling all parameters
        ma_repository_t *my_repository = NULL;

        TEST_ASSERT(MA_OK == ma_repository_create(&my_repository));
        TEST_ASSERT(MA_OK == ma_repository_set_name(my_repository, repositories[counter].repo_name));
        TEST_ASSERT(MA_OK == ma_repository_set_type(my_repository, repositories[counter].repo_type));
        TEST_ASSERT(MA_OK == ma_repository_set_url_type(my_repository, repositories[counter].repo_url_type));
        TEST_ASSERT(MA_OK == ma_repository_set_namespace(my_repository, repositories[counter].repo_namespace));
        TEST_ASSERT(MA_OK == ma_repository_set_proxy_usage_type(my_repository, repositories[counter].repo_proxy_usage_type));
        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, repositories[counter].repo_auth));
        TEST_ASSERT(MA_OK == ma_repository_set_enabled(my_repository, repositories[counter].repo_enabled));

        if(repositories[counter].repo_url_type != MA_REPOSITORY_URL_TYPE_LOCAL)
        {
            TEST_ASSERT(MA_OK == ma_repository_set_server_fqdn(my_repository, repositories[counter].repo_server_fqdn));
        }

        if((repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_SPIPE) || (repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_SA))
        {
            TEST_ASSERT(MA_OK == ma_repository_set_server_name(my_repository, repositories[counter].repo_server_name));
            TEST_ASSERT(MA_OK == ma_repository_set_server_ip(my_repository, repositories[counter].repo_server_ip));
        }

        if((repositories[counter].repo_url_type != MA_REPOSITORY_URL_TYPE_UNC) && (repositories[counter].repo_url_type != MA_REPOSITORY_URL_TYPE_LOCAL))
        {
            TEST_ASSERT(MA_OK == ma_repository_set_port(my_repository, repositories[counter].repo_port));
        }

        TEST_ASSERT(MA_OK == ma_repository_set_server_path(my_repository, repositories[counter].repo_server_path));

        if((repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_UNC) || (repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_LOCAL))
        {
            TEST_ASSERT(MA_OK == ma_repository_set_domain_name(my_repository, repositories[counter].repo_domain_name));
        }

        TEST_ASSERT(MA_OK == ma_repository_set_user_name(my_repository, repositories[counter].repo_user_name));
        TEST_ASSERT(MA_OK == ma_repository_set_password(my_repository, repositories[counter].repo_password));
        TEST_ASSERT(MA_OK == ma_repository_set_pingtime(my_repository, repositories[counter].repo_pingtime));
        TEST_ASSERT(MA_OK == ma_repository_set_subnetdistance(my_repository, repositories[counter].repo_subnetdistance));
        TEST_ASSERT(MA_OK == ma_repository_set_siteorder(my_repository, repositories[counter].repo_siteorder));
        TEST_ASSERT(MA_OK == ma_repository_set_state(my_repository, repositories[counter].repo_state));

        // Adding repositories to DB
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_add_repository(NULL, my_repository));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_add_repository(repo_db, NULL));
        TEST_ASSERT(MA_OK == ma_repository_db_add_repository(repo_db, my_repository));

        // Releasing Repository object after adding it to DB
        TEST_ASSERT(MA_OK == ma_repository_release(my_repository));
    }

    // Setting new ping count (Repository_1 = 6, Repository_2 = 4....., Repository_6 = 1)
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_set_repository_ping_count(NULL, "Repository_5", 5));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_set_repository_ping_count(repo_db, NULL, 5));

    TEST_ASSERT(MA_OK == ma_repository_db_set_repository_ping_count(repo_db, "Repository_1", 6));
    TEST_ASSERT(MA_OK == ma_repository_db_set_repository_ping_count(repo_db, "Repository_2", 5));
    TEST_ASSERT(MA_OK == ma_repository_db_set_repository_ping_count(repo_db, "Repository_3", 4));
    TEST_ASSERT(MA_OK == ma_repository_db_set_repository_ping_count(repo_db, "Repository_4", 3));
    TEST_ASSERT(MA_OK == ma_repository_db_set_repository_ping_count(repo_db, "Repository_5", 2));
    TEST_ASSERT(MA_OK == ma_repository_db_set_repository_ping_count(repo_db, "Repository_6", 1));

    // Retrieving repository by Ping Time (Expected sequence - Repository_6, Repository_5, .....Respository_1)
    // Note that all repository parameters are not verified here. It makes the test too long. Only few
    // parameters relevant to this test, are verified.
    {
        ma_repository_list_t *repo_list = NULL;
        size_t size_of_repo_list = 0;

        ma_repository_t *new_repository = NULL;
        const char *new_repository_name = NULL;
        ma_uint32_t new_pingtime = 234;

        TEST_ASSERT(MA_OK == ma_repository_db_get_repositories(repo_db, crypto_object, "*", MA_REPOSITORY_RANK_BY_PING_TIME, &repo_list));

        TEST_ASSERT(MA_OK == ma_repository_list_get_repositories_count(repo_list, &size_of_repo_list));
        TEST_ASSERT(sizeof(repositories)/sizeof(repositories[0]) == size_of_repo_list);

        {
            TEST_ASSERT(MA_OK == ma_repository_list_get_repository_by_index(repo_list, 0, &new_repository));

            TEST_ASSERT(MA_OK == ma_repository_get_name(new_repository, &new_repository_name));
            TEST_ASSERT(strcmp("Repository_6", new_repository_name) == 0);

            TEST_ASSERT(MA_OK == ma_repository_get_pingtime(new_repository, &new_pingtime));
            TEST_ASSERT(1 == new_pingtime);

            TEST_ASSERT(MA_OK == ma_repository_release(new_repository));
            new_repository = NULL;
        }

        {
            TEST_ASSERT(MA_OK == ma_repository_list_get_repository_by_index(repo_list, 1, &new_repository));

            TEST_ASSERT(MA_OK == ma_repository_get_name(new_repository, &new_repository_name));
            TEST_ASSERT(strcmp("Repository_5", new_repository_name) == 0);

            TEST_ASSERT(MA_OK == ma_repository_get_pingtime(new_repository, &new_pingtime));
            TEST_ASSERT(2 == new_pingtime);

            TEST_ASSERT(MA_OK == ma_repository_release(new_repository));
            new_repository = NULL;
        }

        {
            TEST_ASSERT(MA_OK == ma_repository_list_get_repository_by_index(repo_list, 2, &new_repository));

            TEST_ASSERT(MA_OK == ma_repository_get_name(new_repository, &new_repository_name));
            TEST_ASSERT(strcmp("Repository_4", new_repository_name) == 0);

            TEST_ASSERT(MA_OK == ma_repository_get_pingtime(new_repository, &new_pingtime));
            TEST_ASSERT(3 == new_pingtime);

            TEST_ASSERT(MA_OK == ma_repository_release(new_repository));
            new_repository = NULL;
        }

        {
            TEST_ASSERT(MA_OK == ma_repository_list_get_repository_by_index(repo_list, 3, &new_repository));

            TEST_ASSERT(MA_OK == ma_repository_get_name(new_repository, &new_repository_name));
            TEST_ASSERT(strcmp("Repository_3", new_repository_name) == 0);

            TEST_ASSERT(MA_OK == ma_repository_get_pingtime(new_repository, &new_pingtime));
            TEST_ASSERT(4 == new_pingtime);

            TEST_ASSERT(MA_OK == ma_repository_release(new_repository));
            new_repository = NULL;
        }

        {
            TEST_ASSERT(MA_OK == ma_repository_list_get_repository_by_index(repo_list, 4, &new_repository));

            TEST_ASSERT(MA_OK == ma_repository_get_name(new_repository, &new_repository_name));
            TEST_ASSERT(strcmp("Repository_2", new_repository_name) == 0);

            TEST_ASSERT(MA_OK == ma_repository_get_pingtime(new_repository, &new_pingtime));
            TEST_ASSERT(5 == new_pingtime);

            TEST_ASSERT(MA_OK == ma_repository_release(new_repository));
            new_repository = NULL;
        }

        {
            TEST_ASSERT(MA_OK == ma_repository_list_get_repository_by_index(repo_list, 5, &new_repository));

            TEST_ASSERT(MA_OK == ma_repository_get_name(new_repository, &new_repository_name));
            TEST_ASSERT(strcmp("Repository_1", new_repository_name) == 0);

            TEST_ASSERT(MA_OK == ma_repository_get_pingtime(new_repository, &new_pingtime));
            TEST_ASSERT(6 == new_pingtime);

            TEST_ASSERT(MA_OK == ma_repository_release(new_repository));
            new_repository = NULL;
        }

        // Repository List Release
        TEST_ASSERT(MA_OK == ma_repository_list_release(repo_list));
        repo_list = NULL;
    }

    // Verifying ping count using ma_repository_db_get_repository_ping_count() API
    {
        ma_int32_t my_value = -1;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_get_repository_ping_count(NULL, "Repository_1", &my_value));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_get_repository_ping_count(repo_db, NULL, &my_value));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_get_repository_ping_count(repo_db, "Repository_1", NULL));

        TEST_ASSERT(MA_OK == ma_repository_db_get_repository_ping_count(repo_db, "Repository_1", &my_value));
        TEST_ASSERT(6 == my_value);

        TEST_ASSERT(MA_OK == ma_repository_db_get_repository_ping_count(repo_db, "Repository_2", &my_value));
        TEST_ASSERT(5 == my_value);

        TEST_ASSERT(MA_OK == ma_repository_db_get_repository_ping_count(repo_db, "Repository_3", &my_value));
        TEST_ASSERT(4 == my_value);

        TEST_ASSERT(MA_OK == ma_repository_db_get_repository_ping_count(repo_db, "Repository_4", &my_value));
        TEST_ASSERT(3 == my_value);

        TEST_ASSERT(MA_OK == ma_repository_db_get_repository_ping_count(repo_db, "Repository_5", &my_value));
        TEST_ASSERT(2 == my_value);

        TEST_ASSERT(MA_OK == ma_repository_db_get_repository_ping_count(repo_db, "Repository_6", &my_value));
        TEST_ASSERT(1 == my_value);
    }

	// Removing All Repositories
    TEST_ASSERT(MA_OK == ma_repository_db_remove_all_repositories(repo_db));
}


/******************************************************************************
Test for verifying the ma_repository_db_set_repository_subnet_distance API
******************************************************************************/
TEST(repository_db_manager_tests, set_repository_subnet_distance_test)
{
    ma_uint8_t counter = 0;

    ma_repository_list_t *repo_list = NULL;

    // Adding Repositories to DB
    for(counter = 0; counter < sizeof(repositories)/sizeof(repositories[0]); counter++)
    {
        // Creating repository and filling all parameters
        ma_repository_t *my_repository = NULL;

        TEST_ASSERT(MA_OK == ma_repository_create(&my_repository));
        TEST_ASSERT(MA_OK == ma_repository_set_name(my_repository, repositories[counter].repo_name));
        TEST_ASSERT(MA_OK == ma_repository_set_type(my_repository, repositories[counter].repo_type));
        TEST_ASSERT(MA_OK == ma_repository_set_url_type(my_repository, repositories[counter].repo_url_type));
        TEST_ASSERT(MA_OK == ma_repository_set_namespace(my_repository, repositories[counter].repo_namespace));
        TEST_ASSERT(MA_OK == ma_repository_set_proxy_usage_type(my_repository, repositories[counter].repo_proxy_usage_type));
        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, repositories[counter].repo_auth));
        TEST_ASSERT(MA_OK == ma_repository_set_enabled(my_repository, repositories[counter].repo_enabled));

        if(repositories[counter].repo_url_type != MA_REPOSITORY_URL_TYPE_LOCAL)
        {
            TEST_ASSERT(MA_OK == ma_repository_set_server_fqdn(my_repository, repositories[counter].repo_server_fqdn));
        }

        if((repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_SPIPE) || (repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_SA))
        {
            TEST_ASSERT(MA_OK == ma_repository_set_server_name(my_repository, repositories[counter].repo_server_name));
            TEST_ASSERT(MA_OK == ma_repository_set_server_ip(my_repository, repositories[counter].repo_server_ip));
        }

        if((repositories[counter].repo_url_type != MA_REPOSITORY_URL_TYPE_UNC) && (repositories[counter].repo_url_type != MA_REPOSITORY_URL_TYPE_LOCAL))
        {
            TEST_ASSERT(MA_OK == ma_repository_set_port(my_repository, repositories[counter].repo_port));
        }

        TEST_ASSERT(MA_OK == ma_repository_set_server_path(my_repository, repositories[counter].repo_server_path));

        if((repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_UNC) || (repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_LOCAL))
        {
            TEST_ASSERT(MA_OK == ma_repository_set_domain_name(my_repository, repositories[counter].repo_domain_name));
        }

        TEST_ASSERT(MA_OK == ma_repository_set_user_name(my_repository, repositories[counter].repo_user_name));
        TEST_ASSERT(MA_OK == ma_repository_set_password(my_repository, repositories[counter].repo_password));
        TEST_ASSERT(MA_OK == ma_repository_set_pingtime(my_repository, repositories[counter].repo_pingtime));
        TEST_ASSERT(MA_OK == ma_repository_set_subnetdistance(my_repository, repositories[counter].repo_subnetdistance));
        TEST_ASSERT(MA_OK == ma_repository_set_siteorder(my_repository, repositories[counter].repo_siteorder));
        TEST_ASSERT(MA_OK == ma_repository_set_state(my_repository, repositories[counter].repo_state));

        // Adding repositories to DB
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_add_repository(NULL, my_repository));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_add_repository(repo_db, NULL));
        TEST_ASSERT(MA_OK == ma_repository_db_add_repository(repo_db, my_repository));

        // Releasing Repository object after adding it to DB
        TEST_ASSERT(MA_OK == ma_repository_release(my_repository));
    }

    // Setting new subnet distance (Repository_1 = 60, Repository_2 = 50, Repository_3 = 40....., Repository_6 = 10)
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_set_repository_subnet_distance(NULL, "Repository_5", 50));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_set_repository_subnet_distance(repo_db, NULL, 50));

    TEST_ASSERT(MA_OK == ma_repository_db_set_repository_subnet_distance(repo_db, "Repository_1", 60));
    TEST_ASSERT(MA_OK == ma_repository_db_set_repository_subnet_distance(repo_db, "Repository_2", 50));
    TEST_ASSERT(MA_OK == ma_repository_db_set_repository_subnet_distance(repo_db, "Repository_3", 40));
    TEST_ASSERT(MA_OK == ma_repository_db_set_repository_subnet_distance(repo_db, "Repository_4", 30));
    TEST_ASSERT(MA_OK == ma_repository_db_set_repository_subnet_distance(repo_db, "Repository_5", 20));
    TEST_ASSERT(MA_OK == ma_repository_db_set_repository_subnet_distance(repo_db, "Repository_6", 10));

    // Retrieving repository by Subnet Distance (Expected sequence - Repository_6, Repository_5, .........Respository_1)
    // Note that all repository parameters are not verified here. It makes the test too long. Only few
    // parameters relevant to this test, are verified.
    {
        ma_repository_list_t *repo_list = NULL;
        size_t size_of_repo_list = 0;

        ma_repository_t *new_repository = NULL;
        const char *new_repository_name = NULL;
        ma_uint32_t new_subnetdistance = 234;

        TEST_ASSERT(MA_OK == ma_repository_db_get_repositories(repo_db, crypto_object, "*", MA_REPOSITORY_RANK_BY_SUBNET_DISTANCE, &repo_list));

        TEST_ASSERT(MA_OK == ma_repository_list_get_repositories_count(repo_list, &size_of_repo_list));
        TEST_ASSERT(sizeof(repositories)/sizeof(repositories[0]) == size_of_repo_list);

        {
            TEST_ASSERT(MA_OK == ma_repository_list_get_repository_by_index(repo_list, 0, &new_repository));

            TEST_ASSERT(MA_OK == ma_repository_get_name(new_repository, &new_repository_name));
            TEST_ASSERT(strcmp("Repository_6", new_repository_name) == 0);

            TEST_ASSERT(MA_OK == ma_repository_get_subnetdistance(new_repository, &new_subnetdistance));
            TEST_ASSERT(10 == new_subnetdistance);

            TEST_ASSERT(MA_OK == ma_repository_release(new_repository));
            new_repository = NULL;
        }

        {
            TEST_ASSERT(MA_OK == ma_repository_list_get_repository_by_index(repo_list, 1, &new_repository));

            TEST_ASSERT(MA_OK == ma_repository_get_name(new_repository, &new_repository_name));
            TEST_ASSERT(strcmp("Repository_5", new_repository_name) == 0);

            TEST_ASSERT(MA_OK == ma_repository_get_subnetdistance(new_repository, &new_subnetdistance));
            TEST_ASSERT(20 == new_subnetdistance);

            TEST_ASSERT(MA_OK == ma_repository_release(new_repository));
            new_repository = NULL;
        }

        {
            TEST_ASSERT(MA_OK == ma_repository_list_get_repository_by_index(repo_list, 2, &new_repository));

            TEST_ASSERT(MA_OK == ma_repository_get_name(new_repository, &new_repository_name));
            TEST_ASSERT(strcmp("Repository_4", new_repository_name) == 0);

            TEST_ASSERT(MA_OK == ma_repository_get_subnetdistance(new_repository, &new_subnetdistance));
            TEST_ASSERT(30 == new_subnetdistance);

            TEST_ASSERT(MA_OK == ma_repository_release(new_repository));
            new_repository = NULL;
        }

        {
            TEST_ASSERT(MA_OK == ma_repository_list_get_repository_by_index(repo_list, 3, &new_repository));

            TEST_ASSERT(MA_OK == ma_repository_get_name(new_repository, &new_repository_name));
            TEST_ASSERT(strcmp("Repository_3", new_repository_name) == 0);

            TEST_ASSERT(MA_OK == ma_repository_get_subnetdistance(new_repository, &new_subnetdistance));
            TEST_ASSERT(40 == new_subnetdistance);

            TEST_ASSERT(MA_OK == ma_repository_release(new_repository));
            new_repository = NULL;
        }

        {
            TEST_ASSERT(MA_OK == ma_repository_list_get_repository_by_index(repo_list, 4, &new_repository));

            TEST_ASSERT(MA_OK == ma_repository_get_name(new_repository, &new_repository_name));
            TEST_ASSERT(strcmp("Repository_2", new_repository_name) == 0);

            TEST_ASSERT(MA_OK == ma_repository_get_subnetdistance(new_repository, &new_subnetdistance));
            TEST_ASSERT(50 == new_subnetdistance);

            TEST_ASSERT(MA_OK == ma_repository_release(new_repository));
            new_repository = NULL;
        }

        {
            TEST_ASSERT(MA_OK == ma_repository_list_get_repository_by_index(repo_list, 5, &new_repository));

            TEST_ASSERT(MA_OK == ma_repository_get_name(new_repository, &new_repository_name));
            TEST_ASSERT(strcmp("Repository_1", new_repository_name) == 0);

            TEST_ASSERT(MA_OK == ma_repository_get_subnetdistance(new_repository, &new_subnetdistance));
            TEST_ASSERT(60 == new_subnetdistance);

            TEST_ASSERT(MA_OK == ma_repository_release(new_repository));
            new_repository = NULL;
        }

        // Repository List Release
        TEST_ASSERT(MA_OK == ma_repository_list_release(repo_list));
        repo_list = NULL;
    }

    // Verifying subnet distance using ma_repository_db_set_repository_subnet_distance() API
    {
        ma_int32_t my_value = -1;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_get_repository_subnet_distance(NULL, "Repository_1", &my_value));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_get_repository_subnet_distance(repo_db, NULL, &my_value));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_get_repository_subnet_distance(repo_db, "Repository_1", NULL));

        TEST_ASSERT(MA_OK == ma_repository_db_get_repository_subnet_distance(repo_db, "Repository_1", &my_value));
        TEST_ASSERT(60 == my_value);

        TEST_ASSERT(MA_OK == ma_repository_db_get_repository_subnet_distance(repo_db, "Repository_2", &my_value));
        TEST_ASSERT(50 == my_value);

        TEST_ASSERT(MA_OK == ma_repository_db_get_repository_subnet_distance(repo_db, "Repository_3", &my_value));
        TEST_ASSERT(40 == my_value);

        TEST_ASSERT(MA_OK == ma_repository_db_get_repository_subnet_distance(repo_db, "Repository_4", &my_value));
        TEST_ASSERT(30 == my_value);

        TEST_ASSERT(MA_OK == ma_repository_db_get_repository_subnet_distance(repo_db, "Repository_5", &my_value));
        TEST_ASSERT(20 == my_value);

        TEST_ASSERT(MA_OK == ma_repository_db_get_repository_subnet_distance(repo_db, "Repository_6", &my_value));
        TEST_ASSERT(10 == my_value);
    }

    // Removing All Repositories
    TEST_ASSERT(MA_OK == ma_repository_db_remove_all_repositories(repo_db));
}


/******************************************************************************
Test for verifying the ma_repository_db_set_repository_sitelistorder API
******************************************************************************/
TEST(repository_db_manager_tests, set_repository_sitelistorder_test)
{
    ma_uint8_t counter = 0;

    ma_repository_list_t *repo_list = NULL;

    // Adding Repositories to DB
    for(counter = 0; counter < sizeof(repositories)/sizeof(repositories[0]); counter++)
    {
        // Creating repository and filling all parameters
        ma_repository_t *my_repository = NULL;

        TEST_ASSERT(MA_OK == ma_repository_create(&my_repository));
        TEST_ASSERT(MA_OK == ma_repository_set_name(my_repository, repositories[counter].repo_name));
        TEST_ASSERT(MA_OK == ma_repository_set_type(my_repository, repositories[counter].repo_type));
        TEST_ASSERT(MA_OK == ma_repository_set_url_type(my_repository, repositories[counter].repo_url_type));
        TEST_ASSERT(MA_OK == ma_repository_set_namespace(my_repository, repositories[counter].repo_namespace));
        TEST_ASSERT(MA_OK == ma_repository_set_proxy_usage_type(my_repository, repositories[counter].repo_proxy_usage_type));
        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, repositories[counter].repo_auth));
        TEST_ASSERT(MA_OK == ma_repository_set_enabled(my_repository, repositories[counter].repo_enabled));

        if(repositories[counter].repo_url_type != MA_REPOSITORY_URL_TYPE_LOCAL)
        {
            TEST_ASSERT(MA_OK == ma_repository_set_server_fqdn(my_repository, repositories[counter].repo_server_fqdn));
        }

        if((repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_SPIPE) || (repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_SA))
        {
            TEST_ASSERT(MA_OK == ma_repository_set_server_name(my_repository, repositories[counter].repo_server_name));
            TEST_ASSERT(MA_OK == ma_repository_set_server_ip(my_repository, repositories[counter].repo_server_ip));
        }

        if((repositories[counter].repo_url_type != MA_REPOSITORY_URL_TYPE_UNC) && (repositories[counter].repo_url_type != MA_REPOSITORY_URL_TYPE_LOCAL))
        {
            TEST_ASSERT(MA_OK == ma_repository_set_port(my_repository, repositories[counter].repo_port));
        }

        TEST_ASSERT(MA_OK == ma_repository_set_server_path(my_repository, repositories[counter].repo_server_path));

        if((repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_UNC) || (repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_LOCAL))
        {
            TEST_ASSERT(MA_OK == ma_repository_set_domain_name(my_repository, repositories[counter].repo_domain_name));
        }

        TEST_ASSERT(MA_OK == ma_repository_set_user_name(my_repository, repositories[counter].repo_user_name));
        TEST_ASSERT(MA_OK == ma_repository_set_password(my_repository, repositories[counter].repo_password));
        TEST_ASSERT(MA_OK == ma_repository_set_pingtime(my_repository, repositories[counter].repo_pingtime));
        TEST_ASSERT(MA_OK == ma_repository_set_subnetdistance(my_repository, repositories[counter].repo_subnetdistance));
        TEST_ASSERT(MA_OK == ma_repository_set_siteorder(my_repository, repositories[counter].repo_siteorder));
        TEST_ASSERT(MA_OK == ma_repository_set_state(my_repository, repositories[counter].repo_state));

        // Adding repositories to DB
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_add_repository(NULL, my_repository));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_add_repository(repo_db, NULL));
        TEST_ASSERT(MA_OK == ma_repository_db_add_repository(repo_db, my_repository));

        // Releasing Repository object after adding it to DB
        TEST_ASSERT(MA_OK == ma_repository_release(my_repository));
    }

    // Setting new sitelist order (Repository_1 = 60, Repository_2 = 50, ....., Repository_6 = 10)
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_set_repository_sitelistorder(NULL, "Repository_5", 50));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_set_repository_sitelistorder(repo_db, NULL, 50));

    TEST_ASSERT(MA_OK == ma_repository_db_set_repository_sitelistorder(repo_db, "Repository_1", 60));
    TEST_ASSERT(MA_OK == ma_repository_db_set_repository_sitelistorder(repo_db, "Repository_2", 50));
    TEST_ASSERT(MA_OK == ma_repository_db_set_repository_sitelistorder(repo_db, "Repository_3", 40));
    TEST_ASSERT(MA_OK == ma_repository_db_set_repository_sitelistorder(repo_db, "Repository_4", 30));
    TEST_ASSERT(MA_OK == ma_repository_db_set_repository_sitelistorder(repo_db, "Repository_5", 20));
    TEST_ASSERT(MA_OK == ma_repository_db_set_repository_sitelistorder(repo_db, "Repository_6", 10));

    // Retrieving repository by Sitelist Order (Expected sequence - Repository_6, Repository_5, .....Respository_1)
    // Note that all repository parameters are not verified here. It makes the test too long. Only few
    // parameters relevant to this test, are verified.
   {
        ma_repository_list_t *repo_list = NULL;
        size_t size_of_repo_list = 0;

        ma_repository_t *new_repository = NULL;
        const char *new_repository_name = NULL;
        ma_uint32_t new_siteorder = 50;

        TEST_ASSERT(MA_OK == ma_repository_db_get_repositories(repo_db, crypto_object, "*", MA_REPOSITORY_RANK_BY_SITELIST_ORDER, &repo_list));

        TEST_ASSERT(MA_OK == ma_repository_list_get_repositories_count(repo_list, &size_of_repo_list));
        TEST_ASSERT(sizeof(repositories)/sizeof(repositories[0]) == size_of_repo_list);

        {
            TEST_ASSERT(MA_OK == ma_repository_list_get_repository_by_index(repo_list, 0, &new_repository));

            TEST_ASSERT(MA_OK == ma_repository_get_name(new_repository, &new_repository_name));
            TEST_ASSERT(strcmp("Repository_6", new_repository_name) == 0);

            TEST_ASSERT(MA_OK == ma_repository_get_siteorder(new_repository, &new_siteorder));
            TEST_ASSERT(10 == new_siteorder);

            TEST_ASSERT(MA_OK == ma_repository_release(new_repository));
            new_repository = NULL;
        }

        {
            TEST_ASSERT(MA_OK == ma_repository_list_get_repository_by_index(repo_list, 1, &new_repository));

            TEST_ASSERT(MA_OK == ma_repository_get_name(new_repository, &new_repository_name));
            TEST_ASSERT(strcmp("Repository_5", new_repository_name) == 0);

            TEST_ASSERT(MA_OK == ma_repository_get_siteorder(new_repository, &new_siteorder));
            TEST_ASSERT(20 == new_siteorder);

            TEST_ASSERT(MA_OK == ma_repository_release(new_repository));
            new_repository = NULL;
        }

        {
            TEST_ASSERT(MA_OK == ma_repository_list_get_repository_by_index(repo_list, 2, &new_repository));

            TEST_ASSERT(MA_OK == ma_repository_get_name(new_repository, &new_repository_name));
            TEST_ASSERT(strcmp("Repository_4", new_repository_name) == 0);

            TEST_ASSERT(MA_OK == ma_repository_get_siteorder(new_repository, &new_siteorder));
            TEST_ASSERT(30 == new_siteorder);

            TEST_ASSERT(MA_OK == ma_repository_release(new_repository));
            new_repository = NULL;
        }

        {
            TEST_ASSERT(MA_OK == ma_repository_list_get_repository_by_index(repo_list, 3, &new_repository));

            TEST_ASSERT(MA_OK == ma_repository_get_name(new_repository, &new_repository_name));
            TEST_ASSERT(strcmp("Repository_3", new_repository_name) == 0);

            TEST_ASSERT(MA_OK == ma_repository_get_siteorder(new_repository, &new_siteorder));
            TEST_ASSERT(40 == new_siteorder);

            TEST_ASSERT(MA_OK == ma_repository_release(new_repository));
            new_repository = NULL;
        }

        {
            TEST_ASSERT(MA_OK == ma_repository_list_get_repository_by_index(repo_list, 4, &new_repository));

            TEST_ASSERT(MA_OK == ma_repository_get_name(new_repository, &new_repository_name));
            TEST_ASSERT(strcmp("Repository_2", new_repository_name) == 0);

            TEST_ASSERT(MA_OK == ma_repository_get_siteorder(new_repository, &new_siteorder));
            TEST_ASSERT(50 == new_siteorder);

            TEST_ASSERT(MA_OK == ma_repository_release(new_repository));
            new_repository = NULL;
        }

        {
            TEST_ASSERT(MA_OK == ma_repository_list_get_repository_by_index(repo_list, 5, &new_repository));

            TEST_ASSERT(MA_OK == ma_repository_get_name(new_repository, &new_repository_name));
            TEST_ASSERT(strcmp("Repository_1", new_repository_name) == 0);

            TEST_ASSERT(MA_OK == ma_repository_get_siteorder(new_repository, &new_siteorder));
            TEST_ASSERT(60 == new_siteorder);

            TEST_ASSERT(MA_OK == ma_repository_release(new_repository));
            new_repository = NULL;
        }

        // Repository List Release
        TEST_ASSERT(MA_OK == ma_repository_list_release(repo_list));
        repo_list = NULL;
    }

    // Removing All Repositories
    TEST_ASSERT(MA_OK == ma_repository_db_remove_all_repositories(repo_db));
}


/******************************************************************************
Test for verifying the ma_repository_db_set_local_repositories API and the
ma_repository_db_remove_repositories_by_state API.

All local repositories should be marked for removal and should get removed
when ma_repository_db_remove_repositories_by_state is called.

Name             Namespace    Initial State      Final State    Removed (Yes/No)
--------------------------------------------------------------------------------
Repository 1     Local        Added              Removed        Yes
Repository 2     Global       Removed            Removed        No
Repository 3     Global       Altered            Altered        No
Repository 4     Global       Default            Default        No
Repository 5     Local        Altered            Removed        Yes
Repository 6     Local        Default            Removed        Yes
******************************************************************************/
TEST(repository_db_manager_tests, set_local_repository_test)
{
    ma_uint8_t counter = 0;

    // Adding Repositories to DB
    for(counter = 0; counter < sizeof(repositories)/sizeof(repositories[0]); counter++)
    {
        // Creating repository and filling all parameters
        ma_repository_t *my_repository = NULL;

        TEST_ASSERT(MA_OK == ma_repository_create(&my_repository));
        TEST_ASSERT(MA_OK == ma_repository_set_name(my_repository, repositories[counter].repo_name));
        TEST_ASSERT(MA_OK == ma_repository_set_type(my_repository, repositories[counter].repo_type));
        TEST_ASSERT(MA_OK == ma_repository_set_url_type(my_repository, repositories[counter].repo_url_type));
        TEST_ASSERT(MA_OK == ma_repository_set_namespace(my_repository, repositories[counter].repo_namespace));
        TEST_ASSERT(MA_OK == ma_repository_set_proxy_usage_type(my_repository, repositories[counter].repo_proxy_usage_type));
        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, repositories[counter].repo_auth));
        TEST_ASSERT(MA_OK == ma_repository_set_enabled(my_repository, repositories[counter].repo_enabled));

        if(repositories[counter].repo_url_type != MA_REPOSITORY_URL_TYPE_LOCAL)
        {
            TEST_ASSERT(MA_OK == ma_repository_set_server_fqdn(my_repository, repositories[counter].repo_server_fqdn));
        }

        if((repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_SPIPE) || (repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_SA))
        {
            TEST_ASSERT(MA_OK == ma_repository_set_server_name(my_repository, repositories[counter].repo_server_name));
            TEST_ASSERT(MA_OK == ma_repository_set_server_ip(my_repository, repositories[counter].repo_server_ip));
        }

        if((repositories[counter].repo_url_type != MA_REPOSITORY_URL_TYPE_UNC) && (repositories[counter].repo_url_type != MA_REPOSITORY_URL_TYPE_LOCAL))
        {
            TEST_ASSERT(MA_OK == ma_repository_set_port(my_repository, repositories[counter].repo_port));
        }

        TEST_ASSERT(MA_OK == ma_repository_set_server_path(my_repository, repositories[counter].repo_server_path));

        if((repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_UNC) || (repositories[counter].repo_url_type == MA_REPOSITORY_URL_TYPE_LOCAL))
        {
            TEST_ASSERT(MA_OK == ma_repository_set_domain_name(my_repository, repositories[counter].repo_domain_name));
        }

        TEST_ASSERT(MA_OK == ma_repository_set_user_name(my_repository, repositories[counter].repo_user_name));
        TEST_ASSERT(MA_OK == ma_repository_set_password(my_repository, repositories[counter].repo_password));
        TEST_ASSERT(MA_OK == ma_repository_set_pingtime(my_repository, repositories[counter].repo_pingtime));
        TEST_ASSERT(MA_OK == ma_repository_set_subnetdistance(my_repository, repositories[counter].repo_subnetdistance));
        TEST_ASSERT(MA_OK == ma_repository_set_siteorder(my_repository, repositories[counter].repo_siteorder));
        TEST_ASSERT(MA_OK == ma_repository_set_state(my_repository, repositories[counter].repo_state));

        // Adding repositories to DB
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_add_repository(NULL, my_repository));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_add_repository(repo_db, NULL));
        TEST_ASSERT(MA_OK == ma_repository_db_add_repository(repo_db, my_repository));

        // Releasing Repository object after adding it to DB
        TEST_ASSERT(MA_OK == ma_repository_release(my_repository));
    }

    // Logic - Repository 1, 2, 5, 6 should get removed.
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_set_local_repositories(NULL));
    TEST_ASSERT(MA_OK == ma_repository_db_set_local_repositories(repo_db));

    // Removing repositories by state
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_db_remove_repositories_by_state(NULL));
    TEST_ASSERT(MA_OK == ma_repository_db_remove_repositories_by_state(repo_db));

    // Retrieving repositories by name
    {
        char *server = NULL;
        TEST_ASSERT(MA_ERROR_OBJECTNOTFOUND == ma_repository_db_get_repository_fqdn(repo_db, "Repository_1", &server));
        TEST_ASSERT_NULL(server);

        // Global repository should not be removed, even if its state is "Removed"
        TEST_ASSERT(MA_OK == ma_repository_db_get_repository_fqdn(repo_db, "Repository_2", &server));
        TEST_ASSERT(0 == strcmp("Server_fqdn_2", server));
        free(server); server = NULL;

        TEST_ASSERT(MA_OK == ma_repository_db_get_repository_fqdn(repo_db, "Repository_3", &server));
        TEST_ASSERT(0 == strcmp("Server_fqdn_3", server));
        free(server); server = NULL;

        TEST_ASSERT(MA_OK == ma_repository_db_get_repository_fqdn(repo_db, "Repository_4", &server));
        TEST_ASSERT(0 == strcmp("Server_fqdn_4", server));
        free(server); server = NULL;

        TEST_ASSERT(MA_ERROR_OBJECTNOTFOUND == ma_repository_db_get_repository_fqdn(repo_db, "Repository_5", &server));
        TEST_ASSERT_NULL(server);

        TEST_ASSERT(MA_ERROR_OBJECTNOTFOUND == ma_repository_db_get_repository_fqdn(repo_db, "Repository_6", &server));
        TEST_ASSERT_NULL(server);
    }

    // Removing All Repositories
    TEST_ASSERT(MA_OK == ma_repository_db_remove_all_repositories(repo_db));
}







