/*****************************************************************************
Repository List Unit Tests.
******************************************************************************/

#include "unity.h"
#include "unity_fixture.h"
#include "unity_internals.h"
#include "ma/repository/ma_repository_list.h"
#include "ma/internal/utils/repository/ma_repository_internal.h"
#include <string.h>


ma_uint8_t test_scenario = 0;


TEST_GROUP_RUNNER(ma_repository_list_test_group)
{
    do {RUN_TEST_CASE(repository_list_tests, repository_list_create_release_test)} while (0);

    test_scenario = 1;
    do {RUN_TEST_CASE(repository_list_tests, repository_variant_test)} while (0);

    test_scenario = 2;
    do {RUN_TEST_CASE(repository_list_tests, repository_variant_test)} while (0);

    test_scenario = 3;
    do {RUN_TEST_CASE(repository_list_tests, repository_variant_test)} while (0);

    test_scenario = 4;
    do {RUN_TEST_CASE(repository_list_tests, repository_variant_test)} while (0);

    test_scenario = 5;
    do {RUN_TEST_CASE(repository_list_tests, repository_variant_test)} while (0);

    test_scenario = 6;
    do {RUN_TEST_CASE(repository_list_tests, repository_variant_test)} while (0);

    do {RUN_TEST_CASE(repository_list_tests, repository_list_variant_test)} while (0);
}

TEST_SETUP(repository_list_tests)
{
}

TEST_TEAR_DOWN(repository_list_tests)
{
}

static const char *rep_name_list[] = { "Repository_1", "Repository_2", "Repository_3", "Repository_4", "Repository_5",
                                       "Repository_6", "Repository_7", "Repository_8", "Repository_9", "Repository_10" };


/**************************************************************************
Test covering the Repository List APIs
**************************************************************************/
TEST(repository_list_tests, repository_list_create_release_test)
{
    ma_uint8_t counter = 0;

    ma_repository_t *repositories[sizeof(rep_name_list)/sizeof(rep_name_list[0])];

    // Repository List
    size_t repository_count = 100;
    ma_repository_list_t *my_repository_list = NULL;
    ma_repository_list_t *new_repository_list = NULL;

    // Creating repositories and setting the repository names
    for(counter = 0; counter < sizeof(rep_name_list)/sizeof(rep_name_list[0]); counter++)
    {
        TEST_ASSERT(MA_OK == ma_repository_create(&repositories[counter]));
        TEST_ASSERT(MA_OK == ma_repository_set_name(repositories[counter], rep_name_list[counter]));
    }

    // Creating repository list
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_list_create(NULL));
    TEST_ASSERT(MA_OK == ma_repository_list_create(&my_repository_list));

    // Taking the count of repositories in the repository list. The initial count should be zero.
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_list_get_repositories_count(my_repository_list, NULL));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_list_get_repositories_count(NULL, &repository_count));
    TEST_ASSERT(MA_OK == ma_repository_list_get_repositories_count(my_repository_list, &repository_count));
    TEST_ASSERT(0 == repository_count);

    // Adding a Repository where Repository Name is not yet set
    {
        ma_repository_t *dummy_repository = NULL;

        TEST_ASSERT(MA_OK == ma_repository_create(&dummy_repository));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_list_add_repository(my_repository_list, dummy_repository));
        TEST_ASSERT(MA_OK == ma_repository_release(dummy_repository));
    }

    // Adding repositories to repository list. Verifying that the repository count is increased in each
    // iteration
    for(counter = 0; counter < sizeof(rep_name_list)/sizeof(rep_name_list[0]); counter++)
    {
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_list_add_repository(NULL, repositories[counter]));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_list_add_repository(my_repository_list, NULL));
        TEST_ASSERT(MA_OK == ma_repository_list_add_repository(my_repository_list, repositories[counter]));

        TEST_ASSERT(MA_OK == ma_repository_list_get_repositories_count(my_repository_list, &repository_count));
        TEST_ASSERT((counter + 1) == repository_count);
    }

    // Copying the repository list into a new repository list and releasing the first repository list
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_list_copy(NULL, &new_repository_list));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_list_copy(my_repository_list, NULL));
    TEST_ASSERT(MA_OK == ma_repository_list_copy(my_repository_list, &new_repository_list));

    // Releasing first Repository List
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_list_release(NULL));
    TEST_ASSERT(MA_OK == ma_repository_list_release(my_repository_list));

    // Searching repository by index in new repository list
    {
        ma_repository_t *return_repository = NULL;
        const char *my_repository_name = NULL;

        // Indexes are from (1 <= x <= N) not (0 <= x <= N-1)
        for(counter = 0; counter < sizeof(rep_name_list)/sizeof(rep_name_list[0]); counter++)
        {
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_list_get_repository_by_index(NULL, counter, &return_repository));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_list_get_repository_by_index(new_repository_list, counter, NULL));
            TEST_ASSERT(MA_OK == ma_repository_list_get_repository_by_index(new_repository_list, counter, &return_repository));

            TEST_ASSERT(MA_OK == ma_repository_get_name(return_repository, &my_repository_name));
            TEST_ASSERT(strcmp(rep_name_list[counter], my_repository_name) == 0);
            TEST_ASSERT(MA_OK == ma_repository_release(return_repository));
            return_repository = NULL;
        }

        // Checking that API returns MA_ERROR_INVALIDARG for invalid indexes (negative and "size of array" indexes)
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_list_get_repository_by_index(new_repository_list, -1, &return_repository));
        TEST_ASSERT_NULL(return_repository);
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_list_get_repository_by_index(new_repository_list, sizeof(rep_name_list)/sizeof(rep_name_list[0]), &return_repository));
        TEST_ASSERT_NULL(return_repository);
    }

    // Searching repository by repository name in new (copied) repository list
    {
        ma_repository_t *return_repository = NULL;
        const char *repo_name = NULL;

        for(counter = 0; counter < sizeof(rep_name_list)/sizeof(rep_name_list[0]); counter++)
        {
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_list_get_repository_by_name(NULL, rep_name_list[counter], &return_repository));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_list_get_repository_by_name(new_repository_list, NULL, &return_repository));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_list_get_repository_by_name(new_repository_list, rep_name_list[counter], NULL));
            TEST_ASSERT(MA_OK == ma_repository_list_get_repository_by_name(new_repository_list, rep_name_list[counter], &return_repository));

            TEST_ASSERT(MA_OK == ma_repository_get_name(return_repository, &repo_name));
            TEST_ASSERT(strcmp(repo_name, rep_name_list[counter]) == 0);

            TEST_ASSERT(MA_OK == ma_repository_release(return_repository));
            return_repository = NULL;
        }

        // Searching for non-existent repository
        TEST_ASSERT(MA_OK == ma_repository_list_get_repository_by_name(new_repository_list, "Repository_11", &return_repository));
        TEST_ASSERT_NULL(return_repository);
    }

    // Removing a Repository where Repository Name is not yet set
    {
        ma_repository_t *dummy_repository = NULL;

        TEST_ASSERT(MA_OK == ma_repository_create(&dummy_repository));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_list_remove_repository(new_repository_list, dummy_repository));
        TEST_ASSERT(MA_OK == ma_repository_release(dummy_repository));
    }

    // Removing repositories from repository list. Verifying that the repository count remains same (repositories
    // are flagged to be removed (which cannot be verified here)
    for(counter = 0; counter < sizeof(rep_name_list)/sizeof(rep_name_list[0]); counter++)
    {
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_list_remove_repository(NULL, repositories[counter]));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_list_remove_repository(new_repository_list, NULL));
        TEST_ASSERT(MA_OK == ma_repository_list_remove_repository(new_repository_list, repositories[counter]));

        TEST_ASSERT(MA_OK == ma_repository_list_get_repositories_count(new_repository_list, &repository_count));
        TEST_ASSERT(sizeof(rep_name_list)/sizeof(rep_name_list[0]) == repository_count);
    }

    // Releasing new Repository List
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_list_release(NULL));
    TEST_ASSERT(MA_OK == ma_repository_list_release(new_repository_list));

    // Releasing repositories
    for(counter = 0; counter < sizeof(rep_name_list)/sizeof(rep_name_list[0]); counter++)
    {
        TEST_ASSERT(MA_OK == ma_repository_release(repositories[counter]));
    }
}


/**************************************************************************
Test covering Repository->Variant->Repository APIs
**************************************************************************/
TEST(repository_list_tests, repository_variant_test)
{
    ma_repository_t *my_repository = NULL;
    ma_variant_t *my_repo_variant = NULL;
    ma_repository_t *new_repository = NULL;

    // Test for Empty Repository (Repository with no fields set)
    {
        TEST_ASSERT(MA_OK == ma_repository_create(&my_repository));
        TEST_ASSERT(MA_OK == ma_repository_set_name(my_repository, "Current Repository"));
        TEST_ASSERT(MA_OK == ma_repository_as_variant(my_repository, &my_repo_variant));
        TEST_ASSERT(MA_OK == ma_repository_release(my_repository));
        TEST_ASSERT(MA_OK == ma_repository_from_variant(my_repo_variant, &new_repository));
        TEST_ASSERT(MA_OK == ma_variant_release(my_repo_variant));

        // Verifying the retrieved parameters
        {
            const char *my_string_value = NULL;
            TEST_ASSERT(MA_OK == ma_repository_get_name(new_repository, &my_string_value));
            TEST_ASSERT(0 == strcmp("Current Repository", my_string_value));
        }

        TEST_ASSERT(MA_OK == ma_repository_release(new_repository));
    }

    // Creating repository and filling all parameters. After adding each
    // parameter, verifying that the repository can be converted to variant
    // with incomplete parameter set (releasing this variant, as it is
    // temporary only).
    TEST_ASSERT(MA_OK == ma_repository_create(&my_repository));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_as_variant(my_repository, &my_repo_variant));

    TEST_ASSERT(MA_OK == ma_repository_set_name(my_repository, "My Repository"));
    TEST_ASSERT(MA_OK == ma_repository_as_variant(my_repository, &my_repo_variant));
    TEST_ASSERT(MA_OK == ma_variant_release(my_repo_variant));

    TEST_ASSERT(MA_OK == ma_repository_set_type(my_repository, MA_REPOSITORY_TYPE_FALLBACK));
    TEST_ASSERT(MA_OK == ma_repository_as_variant(my_repository, &my_repo_variant));
    TEST_ASSERT(MA_OK == ma_variant_release(my_repo_variant));

    // Setting URL Type
    if(test_scenario == 1) { TEST_ASSERT(MA_OK == ma_repository_set_url_type(my_repository, MA_REPOSITORY_URL_TYPE_HTTP)); }
    if(test_scenario == 2) { TEST_ASSERT(MA_OK == ma_repository_set_url_type(my_repository, MA_REPOSITORY_URL_TYPE_FTP)); }
    if(test_scenario == 3) { TEST_ASSERT(MA_OK == ma_repository_set_url_type(my_repository, MA_REPOSITORY_URL_TYPE_SPIPE)); }
    if(test_scenario == 4) { TEST_ASSERT(MA_OK == ma_repository_set_url_type(my_repository, MA_REPOSITORY_URL_TYPE_SA)); }
    if(test_scenario == 5) { TEST_ASSERT(MA_OK == ma_repository_set_url_type(my_repository, MA_REPOSITORY_URL_TYPE_UNC)); }
    if(test_scenario == 6) { TEST_ASSERT(MA_OK == ma_repository_set_url_type(my_repository, MA_REPOSITORY_URL_TYPE_LOCAL)); }
    TEST_ASSERT(MA_OK == ma_repository_as_variant(my_repository, &my_repo_variant));
    TEST_ASSERT(MA_OK == ma_variant_release(my_repo_variant));

    TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, MA_REPOSITORY_AUTH_NONE));
    TEST_ASSERT(MA_OK == ma_repository_as_variant(my_repository, &my_repo_variant));
    TEST_ASSERT(MA_OK == ma_variant_release(my_repo_variant));

    TEST_ASSERT(MA_OK == ma_repository_set_namespace(my_repository, MA_REPOSITORY_NAMESPACE_GLOBAL));
    TEST_ASSERT(MA_OK == ma_repository_as_variant(my_repository, &my_repo_variant));
    TEST_ASSERT(MA_OK == ma_variant_release(my_repo_variant));

    TEST_ASSERT(MA_OK == ma_repository_set_enabled(my_repository, MA_FALSE));
    TEST_ASSERT(MA_OK == ma_repository_as_variant(my_repository, &my_repo_variant));
    TEST_ASSERT(MA_OK == ma_variant_release(my_repo_variant));

    TEST_ASSERT(MA_OK == ma_repository_set_state(my_repository, MA_REPOSITORY_STATE_REMOVED));
    TEST_ASSERT(MA_OK == ma_repository_as_variant(my_repository, &my_repo_variant));
    TEST_ASSERT(MA_OK == ma_variant_release(my_repo_variant));

    TEST_ASSERT(MA_OK == ma_repository_set_pingtime(my_repository, 2000));
    TEST_ASSERT(MA_OK == ma_repository_as_variant(my_repository, &my_repo_variant));
    TEST_ASSERT(MA_OK == ma_variant_release(my_repo_variant));

    TEST_ASSERT(MA_OK == ma_repository_set_subnetdistance(my_repository, 40));
    TEST_ASSERT(MA_OK == ma_repository_as_variant(my_repository, &my_repo_variant));
    TEST_ASSERT(MA_OK == ma_variant_release(my_repo_variant));

    TEST_ASSERT(MA_OK == ma_repository_set_siteorder(my_repository, 3));
    TEST_ASSERT(MA_OK == ma_repository_as_variant(my_repository, &my_repo_variant));
    TEST_ASSERT(MA_OK == ma_variant_release(my_repo_variant));

    TEST_ASSERT(MA_OK == ma_repository_set_proxy_usage_type(my_repository, MA_PROXY_USAGE_TYPE_SERVER_CONFIGURED));
    TEST_ASSERT(MA_OK == ma_repository_as_variant(my_repository, &my_repo_variant));
    TEST_ASSERT(MA_OK == ma_variant_release(my_repo_variant));

    if((test_scenario == 1) || (test_scenario == 2)) // HTTP or FTP
    {
        TEST_ASSERT(MA_OK == ma_repository_set_server_fqdn(my_repository, "Server_fqdn_1"));
        TEST_ASSERT(MA_OK == ma_repository_as_variant(my_repository, &my_repo_variant));
        TEST_ASSERT(MA_OK == ma_variant_release(my_repo_variant));

        // Not applicable for HTTP and FTP
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_server_ip(my_repository, "127.0.0.1"));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_server_name(my_repository, "Primary server (Always on)"));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_ssl_port(my_repository, 8085));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_domain_name(my_repository, "Old_Domain"));

        TEST_ASSERT(MA_OK == ma_repository_set_server_path(my_repository, "http:\\\\www.repository.com"));
        TEST_ASSERT(MA_OK == ma_repository_as_variant(my_repository, &my_repo_variant));
        TEST_ASSERT(MA_OK == ma_variant_release(my_repo_variant));

        TEST_ASSERT(MA_OK == ma_repository_set_port(my_repository, 8080));
        TEST_ASSERT(MA_OK == ma_repository_as_variant(my_repository, &my_repo_variant));
        TEST_ASSERT(MA_OK == ma_variant_release(my_repo_variant));

        TEST_ASSERT(MA_OK == ma_repository_set_user_name(my_repository, "Low Priority User"));
        TEST_ASSERT(MA_OK == ma_repository_as_variant(my_repository, &my_repo_variant));
        TEST_ASSERT(MA_OK == ma_variant_release(my_repo_variant));

        TEST_ASSERT(MA_OK == ma_repository_set_password(my_repository, "#$#$#%$%$FFR"));
        TEST_ASSERT(MA_OK == ma_repository_as_variant(my_repository, &my_repo_variant));
        TEST_ASSERT(MA_OK == ma_variant_release(my_repo_variant));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_server_ip(my_repository, "127.0.0.1"));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_server_name(my_repository, "Primary server (Always on)"));
    }

    if((test_scenario == 3) || (test_scenario == 4)) // SPIPE and SA
    {
        TEST_ASSERT(MA_OK == ma_repository_set_server_fqdn(my_repository, "Server_fqdn_2"));
        TEST_ASSERT(MA_OK == ma_repository_as_variant(my_repository, &my_repo_variant));
        TEST_ASSERT(MA_OK == ma_variant_release(my_repo_variant));

        TEST_ASSERT(MA_OK == ma_repository_set_server_ip(my_repository, "127.0.0.1"));
        TEST_ASSERT(MA_OK == ma_repository_as_variant(my_repository, &my_repo_variant));
        TEST_ASSERT(MA_OK == ma_variant_release(my_repo_variant));

        TEST_ASSERT(MA_OK == ma_repository_set_server_name(my_repository, "Primary server (Always on)"));
        TEST_ASSERT(MA_OK == ma_repository_as_variant(my_repository, &my_repo_variant));
        TEST_ASSERT(MA_OK == ma_variant_release(my_repo_variant));

        TEST_ASSERT(MA_OK == ma_repository_set_server_path(my_repository, "http:\\\\www.repository.com\\external"));
        TEST_ASSERT(MA_OK == ma_repository_as_variant(my_repository, &my_repo_variant));
        TEST_ASSERT(MA_OK == ma_variant_release(my_repo_variant));

        TEST_ASSERT(MA_OK == ma_repository_set_port(my_repository, 8080));
        TEST_ASSERT(MA_OK == ma_repository_as_variant(my_repository, &my_repo_variant));
        TEST_ASSERT(MA_OK == ma_variant_release(my_repo_variant));

        TEST_ASSERT(MA_OK == ma_repository_set_ssl_port(my_repository, 8081));
        TEST_ASSERT(MA_OK == ma_repository_as_variant(my_repository, &my_repo_variant));
        TEST_ASSERT(MA_OK == ma_variant_release(my_repo_variant));

        TEST_ASSERT(MA_OK == ma_repository_set_user_name(my_repository, "Super User"));
        TEST_ASSERT(MA_OK == ma_repository_as_variant(my_repository, &my_repo_variant));
        TEST_ASSERT(MA_OK == ma_variant_release(my_repo_variant));

        TEST_ASSERT(MA_OK == ma_repository_set_password(my_repository, "#$@#$#@$@#$#@$@#$@#"));
        TEST_ASSERT(MA_OK == ma_repository_as_variant(my_repository, &my_repo_variant));
        TEST_ASSERT(MA_OK == ma_variant_release(my_repo_variant));

        // Not applicable for SPIPE and SA
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_domain_name(my_repository, "Old_Domain"));
    }

    if(test_scenario == 5) // UNC
    {
        TEST_ASSERT(MA_OK == ma_repository_set_server_fqdn(my_repository, "Server_fqdn_3"));
        TEST_ASSERT(MA_OK == ma_repository_as_variant(my_repository, &my_repo_variant));
        TEST_ASSERT(MA_OK == ma_variant_release(my_repo_variant));

        TEST_ASSERT(MA_OK == ma_repository_set_server_path(my_repository, "http:\\\\www.repository.com\\internal"));
        TEST_ASSERT(MA_OK == ma_repository_as_variant(my_repository, &my_repo_variant));
        TEST_ASSERT(MA_OK == ma_variant_release(my_repo_variant));

        TEST_ASSERT(MA_OK == ma_repository_set_domain_name(my_repository, "My_Domain"));
        TEST_ASSERT(MA_OK == ma_repository_as_variant(my_repository, &my_repo_variant));
        TEST_ASSERT(MA_OK == ma_variant_release(my_repo_variant));

        TEST_ASSERT(MA_OK == ma_repository_set_user_name(my_repository, "Admin User"));
        TEST_ASSERT(MA_OK == ma_repository_as_variant(my_repository, &my_repo_variant));
        TEST_ASSERT(MA_OK == ma_variant_release(my_repo_variant));

        TEST_ASSERT(MA_OK == ma_repository_set_password(my_repository, "#$#$#26532357324"));
        TEST_ASSERT(MA_OK == ma_repository_as_variant(my_repository, &my_repo_variant));
        TEST_ASSERT(MA_OK == ma_variant_release(my_repo_variant));
    }

    if(test_scenario == 6) // Local
    {
        // FQDN, Port and SSL Port is not applicable for LOCAL type
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_server_fqdn(my_repository, "Server_fqdn_4"));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_port(my_repository, 8080));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_ssl_port(my_repository, 8081));

        TEST_ASSERT(MA_OK == ma_repository_set_server_path(my_repository, "http:\\\\corp.mileaccess.com"));
        TEST_ASSERT(MA_OK == ma_repository_as_variant(my_repository, &my_repo_variant));
        TEST_ASSERT(MA_OK == ma_variant_release(my_repo_variant));

        TEST_ASSERT(MA_OK == ma_repository_set_domain_name(my_repository, "My_Domain"));
        TEST_ASSERT(MA_OK == ma_repository_as_variant(my_repository, &my_repo_variant));
        TEST_ASSERT(MA_OK == ma_variant_release(my_repo_variant));

        TEST_ASSERT(MA_OK == ma_repository_set_user_name(my_repository, "Normal User"));
        TEST_ASSERT(MA_OK == ma_repository_as_variant(my_repository, &my_repo_variant));
        TEST_ASSERT(MA_OK == ma_variant_release(my_repo_variant));

        TEST_ASSERT(MA_OK == ma_repository_set_password(my_repository, "38947398lk432n4kl3n3lkn3lk4n3lkn3"));
        TEST_ASSERT(MA_OK == ma_repository_as_variant(my_repository, &my_repo_variant));
        TEST_ASSERT(MA_OK == ma_variant_release(my_repo_variant));
    }

    // Converting Repository to Variant and then, releasing repository
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_as_variant(my_repository, NULL));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_as_variant(NULL, &my_repo_variant));
    TEST_ASSERT(MA_OK == ma_repository_as_variant(my_repository, &my_repo_variant));
    TEST_ASSERT(MA_OK == ma_repository_release(my_repository));

    // Retrieving Repository from Variant and then, releasing variant
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_from_variant(NULL, &new_repository));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_from_variant(my_repo_variant, NULL));
    TEST_ASSERT(MA_OK == ma_repository_from_variant(my_repo_variant, &new_repository));
    TEST_ASSERT(MA_OK == ma_variant_release(my_repo_variant));

    // Verifying the retrieved parameters
    {
        const char *my_string_value = NULL;
        TEST_ASSERT(MA_OK == ma_repository_get_name(new_repository, &my_string_value));
        TEST_ASSERT(0 == strcmp("My Repository", my_string_value));
    }

    {
        ma_repository_type_t type = MA_REPOSITORY_TYPE_MASTER;
        TEST_ASSERT(MA_OK == ma_repository_get_type(new_repository, &type));
        TEST_ASSERT(type == MA_REPOSITORY_TYPE_FALLBACK);
    }

    {
        ma_repository_url_type_t type = MA_REPOSITORY_URL_TYPE_FTP;
        TEST_ASSERT(MA_OK == ma_repository_get_url_type(new_repository, &type));

        if(test_scenario == 1) { TEST_ASSERT(type == MA_REPOSITORY_URL_TYPE_HTTP); }
        if(test_scenario == 2) { TEST_ASSERT(type == MA_REPOSITORY_URL_TYPE_FTP); }
        if(test_scenario == 3) { TEST_ASSERT(type == MA_REPOSITORY_URL_TYPE_SPIPE); }
        if(test_scenario == 4) { TEST_ASSERT(type == MA_REPOSITORY_URL_TYPE_SA); }
        if(test_scenario == 5) { TEST_ASSERT(type == MA_REPOSITORY_URL_TYPE_UNC); }
        if(test_scenario == 6) { TEST_ASSERT(type == MA_REPOSITORY_URL_TYPE_LOCAL); }
    }

    {
        ma_repository_auth_t type = MA_REPOSITORY_AUTH_CREDENTIALS;
        TEST_ASSERT(MA_OK == ma_repository_get_authentication(new_repository, &type));
        TEST_ASSERT(type == MA_REPOSITORY_AUTH_NONE);
    }

    {
        ma_repository_namespace_t type = MA_REPOSITORY_NAMESPACE_LOCAL;
        TEST_ASSERT(MA_OK == ma_repository_get_namespace(new_repository, &type));
        TEST_ASSERT(type == MA_REPOSITORY_NAMESPACE_GLOBAL);
    }

    {
        ma_bool_t value = MA_TRUE;
        TEST_ASSERT(MA_OK == ma_repository_get_enabled(new_repository, &value));
        TEST_ASSERT(value == MA_FALSE);
    }

    {
        ma_repository_state_t state = MA_REPOSITORY_STATE_DEFAULT;
        TEST_ASSERT(MA_OK == ma_repository_get_state(new_repository, &state));
        TEST_ASSERT(MA_REPOSITORY_STATE_REMOVED == state);
    }

    {
        ma_uint32_t pingtime = 0;
        TEST_ASSERT(MA_OK == ma_repository_get_pingtime(new_repository, &pingtime));
        TEST_ASSERT(2000 == pingtime);
    }

    {
        ma_uint32_t subnetdistance = 0;
        TEST_ASSERT(MA_OK == ma_repository_get_subnetdistance(new_repository, &subnetdistance));
        TEST_ASSERT(40 == subnetdistance);
    }

    {
        ma_uint32_t sitelistorder = 0;
        TEST_ASSERT(MA_OK == ma_repository_get_siteorder(new_repository, &sitelistorder));
        TEST_ASSERT(3 == sitelistorder);
    }

    {
        ma_proxy_usage_type_t my_proxy_usage_type = MA_PROXY_USAGE_TYPE_SYSTEM_CONFIGURED;
        TEST_ASSERT(MA_OK == ma_repository_get_proxy_usage_type(new_repository, &my_proxy_usage_type));
        TEST_ASSERT(MA_PROXY_USAGE_TYPE_SERVER_CONFIGURED == my_proxy_usage_type);
    }

    {
        const char *my_string_value = NULL;

        // FQDN is not applicable for Local URL type
        if(test_scenario == 6) { TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_server_fqdn(new_repository, &my_string_value)); }
        else { TEST_ASSERT(MA_OK == ma_repository_get_server_fqdn(new_repository, &my_string_value)); }

        if(test_scenario == 1) { TEST_ASSERT(0 == strcmp("Server_fqdn_1", my_string_value)); }
        if(test_scenario == 2) { TEST_ASSERT(0 == strcmp("Server_fqdn_1", my_string_value)); }
        if(test_scenario == 3) { TEST_ASSERT(0 == strcmp("Server_fqdn_2", my_string_value)); }
        if(test_scenario == 4) { TEST_ASSERT(0 == strcmp("Server_fqdn_2", my_string_value)); }
        if(test_scenario == 5) { TEST_ASSERT(0 == strcmp("Server_fqdn_3", my_string_value)); }
    }

    {
        const char *my_server_path = NULL;
        TEST_ASSERT(MA_OK == ma_repository_get_server_path(new_repository, &my_server_path));
        if(test_scenario == 1) { TEST_ASSERT(0 == strcmp("http:\\\\www.repository.com", my_server_path)); }
        if(test_scenario == 2) { TEST_ASSERT(0 == strcmp("http:\\\\www.repository.com", my_server_path)); }
        if(test_scenario == 3) { TEST_ASSERT(0 == strcmp("http:\\\\www.repository.com\\external", my_server_path)); }
        if(test_scenario == 4) { TEST_ASSERT(0 == strcmp("http:\\\\www.repository.com\\external", my_server_path)); }
        if(test_scenario == 5) { TEST_ASSERT(0 == strcmp("http:\\\\www.repository.com\\internal", my_server_path)); }
        if(test_scenario == 6) { TEST_ASSERT(0 == strcmp("http:\\\\corp.mileaccess.com", my_server_path)); }
    }

    // Port is not applicable for LOCAL and UNC
    {
        ma_uint32_t my_port = 23456;
        if((test_scenario == 1) || (test_scenario == 2) || (test_scenario == 3) || (test_scenario == 4))
        {
            TEST_ASSERT(MA_OK == ma_repository_get_port(new_repository, &my_port));
            TEST_ASSERT(8080 == my_port);
        }
        else
        {
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_port(new_repository, &my_port));
        }
    }

    // SSL Port is only applicable for only SPIPE and SA type
    {
        ma_uint32_t my_port = 23456;
        if((test_scenario == 3) || (test_scenario == 4))
        {
            TEST_ASSERT(MA_OK == ma_repository_get_ssl_port(new_repository, &my_port));
            TEST_ASSERT(8081 == my_port);
        }
        else
        {
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_ssl_port(new_repository, &my_port));
        }
    }

    // Server Name and Server IP is only applicable for only SPIPE and SA type
    {
        const char *server_name = NULL;
        const char *server_ip = NULL;

        if((test_scenario == 3) || (test_scenario == 4))
        {
            TEST_ASSERT(MA_OK == ma_repository_get_server_name(new_repository, &server_name));
            TEST_ASSERT(MA_OK == ma_repository_get_server_ip(new_repository, &server_ip));

            TEST_ASSERT(0 == strcmp("Primary server (Always on)", server_name));
            TEST_ASSERT(0 == strcmp("127.0.0.1", server_ip));
        }
        else
        {
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_server_name(new_repository, &server_name));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_server_ip(new_repository, &server_ip));
        };
    }

    {
        const char *my_user_name = NULL;
        const char *my_password = NULL;

        TEST_ASSERT(MA_OK == ma_repository_get_user_name(new_repository, &my_user_name));
        TEST_ASSERT(MA_OK == ma_repository_get_password(new_repository, &my_password));

        if((test_scenario == 1) || (test_scenario == 2))
        {
            TEST_ASSERT(0 == strcmp("Low Priority User", my_user_name));
            TEST_ASSERT(0 == strcmp("#$#$#%$%$FFR", my_password));
        }

        if((test_scenario == 3) || (test_scenario == 4))
        {
            TEST_ASSERT(0 == strcmp("Super User", my_user_name));
            TEST_ASSERT(0 == strcmp("#$@#$#@$@#$#@$@#$@#", my_password));
        }

        if(test_scenario == 5)
        {
            TEST_ASSERT(0 == strcmp("Admin User", my_user_name));
            TEST_ASSERT(0 == strcmp("#$#$#26532357324", my_password));
        }

        if(test_scenario == 6)
        {
            TEST_ASSERT(0 == strcmp("Normal User", my_user_name));
            TEST_ASSERT(0 == strcmp("38947398lk432n4kl3n3lkn3lk4n3lkn3", my_password));
        }
    }

    // Domain Name is only applicable for only UNC and Local type
    {
        const char *domain_name = NULL;

        if((test_scenario == 5) || (test_scenario == 6))
        {
            TEST_ASSERT(MA_OK == ma_repository_get_domain_name(new_repository, &domain_name));
            TEST_ASSERT(0 == strcmp("My_Domain", domain_name));
        }
        else
        {
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_domain_name(new_repository, &domain_name));
        };
    }

    // Releasing Repository
    TEST_ASSERT(MA_OK == ma_repository_release(new_repository));
}


/**************************************************************************
Test covering Repository List -> Variant -> Repository List APIs
**************************************************************************/
TEST(repository_list_tests, repository_list_variant_test)
{
    ma_uint8_t counter = 0;

    ma_repository_t *repositories[sizeof(rep_name_list)/sizeof(rep_name_list[0])];

    // Repository List
    ma_repository_list_t *my_repository_list = NULL;
    ma_repository_list_t *new_repository_list = NULL;

    ma_variant_t *my_repo_list_variant = NULL;

    // Creating repositories and setting the repository names
    for(counter = 0; counter < sizeof(rep_name_list)/sizeof(rep_name_list[0]); counter++)
    {
        TEST_ASSERT(MA_OK == ma_repository_create(&repositories[counter]));
        TEST_ASSERT(MA_OK == ma_repository_set_name(repositories[counter], rep_name_list[counter]));
    }

    // Creating repository list
    TEST_ASSERT(MA_OK == ma_repository_list_create(&my_repository_list));

    // Adding repositories to repository list. Verifying that the repository count is increased in each
    // iteration
    for(counter = 0; counter < sizeof(rep_name_list)/sizeof(rep_name_list[0]); counter++)
    {
        TEST_ASSERT(MA_OK == ma_repository_list_add_repository(my_repository_list, repositories[counter]));
    }

    // Creating variant from repository list and then, releasing repository list
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_list_as_variant(NULL, &my_repo_list_variant));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_list_as_variant(my_repository_list, NULL));
    TEST_ASSERT(MA_OK == ma_repository_list_as_variant(my_repository_list, &my_repo_list_variant));
    TEST_ASSERT(MA_OK == ma_repository_list_release(my_repository_list));

    // Retrieving repository list from variant and then, releasing variant
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_list_from_variant(NULL, &new_repository_list));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_list_from_variant(my_repo_list_variant, NULL));
    TEST_ASSERT(MA_OK == ma_repository_list_from_variant(my_repo_list_variant, &new_repository_list));
    TEST_ASSERT(MA_OK == ma_variant_release(my_repo_list_variant));

    // Searching repository by index and verifying that the repository list is correctly retrieved from variant
    {
        ma_repository_t *return_repository = NULL;
        const char *my_repository_name = NULL;

        // Indexes are from (0 <= x <= N-1)
        for(counter = 0; counter < sizeof(rep_name_list)/sizeof(rep_name_list[0]); counter++)
        {
            TEST_ASSERT(MA_OK == ma_repository_list_get_repository_by_index(new_repository_list, counter, &return_repository));

            TEST_ASSERT(MA_OK == ma_repository_get_name(return_repository, &my_repository_name));
            TEST_ASSERT(strcmp(rep_name_list[counter], my_repository_name) == 0);
            TEST_ASSERT(MA_OK == ma_repository_release(return_repository));
            return_repository = NULL;
        }
    }

    // Releasing Repository List
    TEST_ASSERT(MA_OK == ma_repository_list_release(new_repository_list));

    // Releasing repositories
    for(counter = 0; counter < sizeof(rep_name_list)/sizeof(rep_name_list[0]); counter++)
    {
        TEST_ASSERT(MA_OK == ma_repository_release(repositories[counter]));
    }
}









