/*****************************************************************************
Repository Unit Tests.
******************************************************************************/

#include "unity.h"
#include "unity_fixture.h"
#include "unity_internals.h"
#include "ma/repository/ma_repository.h"
#include <string.h>

TEST_GROUP_RUNNER(ma_repository_test_group)
{
    do {RUN_TEST_CASE(repository_tests, repository_set_get_test_1)} while (0);
    do {RUN_TEST_CASE(repository_tests, repository_set_get_test_2)} while (0);
    do {RUN_TEST_CASE(repository_tests, repository_validation_test)} while (0);
}

TEST_SETUP(repository_tests)
{

}

TEST_TEAR_DOWN(repository_tests)
{
}


/**************************************************************************
Test covering the repository set/get APIs
**************************************************************************/
TEST(repository_tests, repository_set_get_test_1)
{
    ma_repository_t *my_repository = NULL;

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_create(NULL));
    TEST_ASSERT(MA_OK == ma_repository_create(&my_repository));

    {
        ma_repository_type_t my_repository_type;
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_type(NULL, MA_REPOSITORY_TYPE_REPO));
        TEST_ASSERT(MA_OK == ma_repository_set_type(my_repository, MA_REPOSITORY_TYPE_REPO));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_type(NULL, &my_repository_type));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_type(my_repository, NULL));
        TEST_ASSERT(MA_OK == ma_repository_get_type(my_repository, &my_repository_type));
        TEST_ASSERT(MA_REPOSITORY_TYPE_REPO == my_repository_type);

        TEST_ASSERT(MA_OK == ma_repository_set_type(my_repository, MA_REPOSITORY_TYPE_MASTER));
        TEST_ASSERT(MA_OK == ma_repository_get_type(my_repository, &my_repository_type));
        TEST_ASSERT(MA_REPOSITORY_TYPE_MASTER == my_repository_type);

        TEST_ASSERT(MA_OK == ma_repository_set_type(my_repository, MA_REPOSITORY_TYPE_FALLBACK));
        TEST_ASSERT(MA_OK == ma_repository_get_type(my_repository, &my_repository_type));
        TEST_ASSERT(MA_REPOSITORY_TYPE_FALLBACK == my_repository_type);
    }

    {
        ma_repository_state_t my_repository_state;
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_state(NULL, MA_REPOSITORY_STATE_DEFAULT));
        TEST_ASSERT(MA_OK == ma_repository_set_state(my_repository, MA_REPOSITORY_STATE_DEFAULT));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_state(NULL, &my_repository_state));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_state(my_repository, NULL));
        TEST_ASSERT(MA_OK == ma_repository_get_state(my_repository, &my_repository_state));
        TEST_ASSERT(MA_REPOSITORY_STATE_DEFAULT == my_repository_state);

        TEST_ASSERT(MA_OK == ma_repository_set_state(my_repository, MA_REPOSITORY_STATE_REMOVED));
        TEST_ASSERT(MA_OK == ma_repository_get_state(my_repository, &my_repository_state));
        TEST_ASSERT(MA_REPOSITORY_STATE_REMOVED == my_repository_state);

        TEST_ASSERT(MA_OK == ma_repository_set_state(my_repository, MA_REPOSITORY_STATE_ALTERED));
        TEST_ASSERT(MA_OK == ma_repository_get_state(my_repository, &my_repository_state));
        TEST_ASSERT(MA_REPOSITORY_STATE_ALTERED == my_repository_state);

        TEST_ASSERT(MA_OK == ma_repository_set_state(my_repository, MA_REPOSITORY_STATE_ADDED));
        TEST_ASSERT(MA_OK == ma_repository_get_state(my_repository, &my_repository_state));
        TEST_ASSERT(MA_REPOSITORY_STATE_ADDED == my_repository_state);
    }

    {
        const char *my_repository_name = NULL;
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_name(NULL, "Repository_01"));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_name(my_repository, NULL));
        TEST_ASSERT(MA_OK == ma_repository_set_name(my_repository, "Repository_01"));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_name(NULL, &my_repository_name));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_name(my_repository, NULL));
        TEST_ASSERT(MA_OK == ma_repository_get_name(my_repository, &my_repository_name));
        TEST_ASSERT(strcmp("Repository_01", my_repository_name) == 0);

        TEST_ASSERT(MA_OK == ma_repository_set_name(my_repository, "Repository_02"));
        TEST_ASSERT(MA_OK == ma_repository_get_name(my_repository, &my_repository_name));
        TEST_ASSERT(strcmp("Repository_02", my_repository_name) == 0);

    }

    {
        ma_bool_t is_enabled = MA_FALSE;
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_enabled(NULL, MA_TRUE));
        TEST_ASSERT(MA_OK == ma_repository_set_enabled(my_repository, MA_TRUE));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_enabled(NULL, &is_enabled));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_enabled(my_repository, NULL));
        TEST_ASSERT(MA_OK == ma_repository_get_enabled(my_repository, &is_enabled));

        TEST_ASSERT(MA_TRUE == is_enabled);

        TEST_ASSERT(MA_OK == ma_repository_set_enabled(my_repository, MA_FALSE));
        TEST_ASSERT(MA_OK == ma_repository_get_enabled(my_repository, &is_enabled));
        TEST_ASSERT(MA_FALSE == is_enabled);
    }

    {
        ma_repository_namespace_t my_repository_namespace;
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_namespace(NULL, MA_REPOSITORY_NAMESPACE_LOCAL));
        TEST_ASSERT(MA_OK == ma_repository_set_namespace(my_repository, MA_REPOSITORY_NAMESPACE_LOCAL));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_namespace(NULL, &my_repository_namespace));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_namespace(my_repository, NULL));
        TEST_ASSERT(MA_OK == ma_repository_get_namespace(my_repository, &my_repository_namespace));
        TEST_ASSERT(MA_REPOSITORY_NAMESPACE_LOCAL == my_repository_namespace);

        TEST_ASSERT(MA_OK == ma_repository_set_namespace(my_repository, MA_REPOSITORY_NAMESPACE_GLOBAL));
        TEST_ASSERT(MA_OK == ma_repository_get_namespace(my_repository, &my_repository_namespace));
        TEST_ASSERT(MA_REPOSITORY_NAMESPACE_GLOBAL == my_repository_namespace);
    }

    {
        ma_proxy_usage_type_t my_proxy_usage_type;
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_proxy_usage_type(NULL, MA_PROXY_USAGE_TYPE_SERVER_CONFIGURED));
        TEST_ASSERT(MA_OK == ma_repository_set_proxy_usage_type(my_repository, MA_PROXY_USAGE_TYPE_SERVER_CONFIGURED));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_proxy_usage_type(NULL, &my_proxy_usage_type));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_proxy_usage_type(my_repository, NULL));
        TEST_ASSERT(MA_OK == ma_repository_get_proxy_usage_type(my_repository, &my_proxy_usage_type));
        TEST_ASSERT(MA_PROXY_USAGE_TYPE_SERVER_CONFIGURED == my_proxy_usage_type);

        TEST_ASSERT(MA_OK == ma_repository_set_proxy_usage_type(my_repository, MA_PROXY_USAGE_TYPE_SYSTEM_CONFIGURED));
        TEST_ASSERT(MA_OK == ma_repository_get_proxy_usage_type(my_repository, &my_proxy_usage_type));
        TEST_ASSERT(MA_PROXY_USAGE_TYPE_SYSTEM_CONFIGURED == my_proxy_usage_type);

        TEST_ASSERT(MA_OK == ma_repository_set_proxy_usage_type(my_repository, MA_PROXY_USAGE_TYPE_NONE));
        TEST_ASSERT(MA_OK == ma_repository_get_proxy_usage_type(my_repository, &my_proxy_usage_type));
        TEST_ASSERT(MA_PROXY_USAGE_TYPE_NONE == my_proxy_usage_type);
    }

    {
        ma_uint32_t my_pingtime = 234;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_pingtime(NULL, 999));
        TEST_ASSERT(MA_OK == ma_repository_set_pingtime(my_repository, 999));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_pingtime(NULL, &my_pingtime));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_pingtime(my_repository, NULL));
        TEST_ASSERT(MA_OK == ma_repository_get_pingtime(my_repository, &my_pingtime));
        TEST_ASSERT(999 == my_pingtime);

        TEST_ASSERT(MA_OK == ma_repository_set_pingtime(my_repository, 333));
        TEST_ASSERT(MA_OK == ma_repository_get_pingtime(my_repository, &my_pingtime));
        TEST_ASSERT(333 == my_pingtime);
    }

    {
        ma_uint32_t my_subnetdistance = 10;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_subnetdistance(NULL, 15));
        TEST_ASSERT(MA_OK == ma_repository_set_subnetdistance(my_repository, 15));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_subnetdistance(NULL, &my_subnetdistance));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_subnetdistance(my_repository, NULL));
        TEST_ASSERT(MA_OK == ma_repository_get_subnetdistance(my_repository, &my_subnetdistance));
        TEST_ASSERT(15 == my_subnetdistance);

        TEST_ASSERT(MA_OK == ma_repository_set_subnetdistance(my_repository, 100));
        TEST_ASSERT(MA_OK == ma_repository_get_subnetdistance(my_repository, &my_subnetdistance));
        TEST_ASSERT(100 == my_subnetdistance);
    }

    {
        ma_uint32_t my_siteorder = 10;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_siteorder(NULL, 5));
        TEST_ASSERT(MA_OK == ma_repository_set_siteorder(my_repository, 5));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_siteorder(NULL, &my_siteorder));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_siteorder(my_repository, NULL));
        TEST_ASSERT(MA_OK == ma_repository_get_siteorder(my_repository, &my_siteorder));
        TEST_ASSERT(5 == my_siteorder);

        TEST_ASSERT(MA_OK == ma_repository_set_siteorder(my_repository, 7));
        TEST_ASSERT(MA_OK == ma_repository_get_siteorder(my_repository, &my_siteorder));
        TEST_ASSERT(7 == my_siteorder);
    }

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_release(NULL));
    TEST_ASSERT(MA_OK == ma_repository_release(my_repository));
}


/**************************************************************************
Test covering the repository set/get APIs
**************************************************************************/
TEST(repository_tests, repository_set_get_test_2)
{
    ma_repository_t *my_repository = NULL;

    // HTTP
    TEST_ASSERT(MA_OK == ma_repository_create(&my_repository));

    {
        ma_repository_url_type_t my_repository_url_type;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_url_type(NULL, MA_REPOSITORY_URL_TYPE_HTTP));
        TEST_ASSERT(MA_OK == ma_repository_set_url_type(my_repository, MA_REPOSITORY_URL_TYPE_HTTP));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_url_type(NULL, &my_repository_url_type));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_url_type(my_repository, NULL));
        TEST_ASSERT(MA_OK == ma_repository_get_url_type(my_repository, &my_repository_url_type));
        TEST_ASSERT(MA_REPOSITORY_URL_TYPE_HTTP == my_repository_url_type);
    }

    {
        const char *my_server_fqdn = NULL;

        // Trying to retrieve fqdn without setting it first
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_server_fqdn(my_repository, &my_server_fqdn));
        TEST_ASSERT_NULL(my_server_fqdn);

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_server_fqdn(NULL, "https:\\\\corp.mileaccess.com"));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_server_fqdn(my_repository, NULL));
        TEST_ASSERT(MA_OK == ma_repository_set_server_fqdn(my_repository, "https:\\\\corp.mileaccess.com"));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_server_fqdn(NULL, &my_server_fqdn));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_server_fqdn(my_repository, NULL));
        TEST_ASSERT(MA_OK == ma_repository_get_server_fqdn(my_repository, &my_server_fqdn));
        TEST_ASSERT(strcmp("https:\\\\corp.mileaccess.com", my_server_fqdn) == 0);

        TEST_ASSERT(MA_OK == ma_repository_set_server_fqdn(my_repository, "https:\\\\internal.mileaccess.com\\help"));
        TEST_ASSERT(MA_OK == ma_repository_get_server_fqdn(my_repository, &my_server_fqdn));
        TEST_ASSERT(strcmp("https:\\\\internal.mileaccess.com\\help", my_server_fqdn) == 0);
    }

    {
        const char *my_server_path = NULL;

        // Trying to retrieve server path without setting it first
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_server_path(my_repository, &my_server_path));
        TEST_ASSERT_NULL(my_server_path);

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_server_path(NULL, "https:\\\\192.16.220.345:8443\\core"));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_server_path(my_repository, NULL));
        TEST_ASSERT(MA_OK == ma_repository_set_server_path(my_repository, "https:\\\\192.16.220.345:8443\\core"));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_server_path(NULL, &my_server_path));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_server_path(my_repository, NULL));
        TEST_ASSERT(MA_OK == ma_repository_get_server_path(my_repository, &my_server_path));
        TEST_ASSERT(strcmp("https:\\\\192.16.220.345:8443\\core", my_server_path) == 0);

        TEST_ASSERT(MA_OK == ma_repository_set_server_path(my_repository, "https:\\\\192.46.100.345:123\\core"));
        TEST_ASSERT(MA_OK == ma_repository_get_server_path(my_repository, &my_server_path));
        TEST_ASSERT(strcmp("https:\\\\192.46.100.345:123\\core", my_server_path) == 0);
    }

    {
        ma_repository_auth_t my_repository_auth = MA_REPOSITORY_AUTH_LOGGEDON_USER;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_authentication(NULL, MA_REPOSITORY_AUTH_NONE));
        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, MA_REPOSITORY_AUTH_NONE));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_authentication(NULL, &my_repository_auth));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_authentication(my_repository, NULL));
        TEST_ASSERT(MA_OK == ma_repository_get_authentication(my_repository, &my_repository_auth));
        TEST_ASSERT(MA_REPOSITORY_AUTH_NONE == my_repository_auth);

        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, MA_REPOSITORY_AUTH_ANONYMOUS));
        TEST_ASSERT(MA_OK == ma_repository_get_authentication(my_repository, &my_repository_auth));
        TEST_ASSERT(MA_REPOSITORY_AUTH_ANONYMOUS == my_repository_auth);

        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, MA_REPOSITORY_AUTH_LOGGEDON_USER));
        TEST_ASSERT(MA_OK == ma_repository_get_authentication(my_repository, &my_repository_auth));
        TEST_ASSERT(MA_REPOSITORY_AUTH_LOGGEDON_USER == my_repository_auth);

        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, MA_REPOSITORY_AUTH_CREDENTIALS));
        TEST_ASSERT(MA_OK == ma_repository_get_authentication(my_repository, &my_repository_auth));
        TEST_ASSERT(MA_REPOSITORY_AUTH_CREDENTIALS == my_repository_auth);
    }

    {
        ma_uint32_t my_port = 23456;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_port(NULL, 12345));
        TEST_ASSERT(MA_OK == ma_repository_set_port(my_repository, 12345));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_port(NULL, &my_port));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_port(my_repository, NULL));
        TEST_ASSERT(MA_OK == ma_repository_get_port(my_repository, &my_port));
        TEST_ASSERT(12345 == my_port);

        TEST_ASSERT(MA_OK == ma_repository_set_port(my_repository, 67890));
        TEST_ASSERT(MA_OK == ma_repository_get_port(my_repository, &my_port));
        TEST_ASSERT(67890 == my_port);
    }

    {
        const char *my_user_name = NULL;

        // Trying to get username, when it is not yet set
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_user_name(my_repository, &my_user_name));
        TEST_ASSERT_NULL(my_user_name);

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_user_name(NULL, "admin_user_10"));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_user_name(my_repository, NULL));
        TEST_ASSERT(MA_OK == ma_repository_set_user_name(my_repository, "admin_user_10"));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_user_name(NULL, &my_user_name));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_user_name(my_repository, NULL));
        TEST_ASSERT(MA_OK == ma_repository_get_user_name(my_repository, &my_user_name));
        TEST_ASSERT(strcmp("admin_user_10", my_user_name) == 0);

        TEST_ASSERT(MA_OK == ma_repository_set_user_name(my_repository, "Normal User"));
        TEST_ASSERT(MA_OK == ma_repository_get_user_name(my_repository, &my_user_name));
        TEST_ASSERT(strcmp("Normal User", my_user_name) == 0);
    }

    {
        const char *my_password = NULL;

        // Trying to get password, when it is not yet set
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_password(my_repository, &my_password));
        TEST_ASSERT_NULL(my_password);

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_password(NULL, "edfjnelmn232323n$%$%$%"));
        TEST_ASSERT(MA_OK == ma_repository_set_password(my_repository, NULL));
        TEST_ASSERT(MA_OK == ma_repository_set_password(my_repository, "edfjnelmn232323n$%$%$%"));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_password(NULL, &my_password));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_password(my_repository, NULL));
        TEST_ASSERT(MA_OK == ma_repository_get_password(my_repository, &my_password));
        TEST_ASSERT(strcmp("edfjnelmn232323n$%$%$%", my_password) == 0);

        TEST_ASSERT(MA_OK == ma_repository_set_password(my_repository, "3398638638638643"));
        TEST_ASSERT(MA_OK == ma_repository_get_password(my_repository, &my_password));
        TEST_ASSERT(strcmp("3398638638638643", my_password) == 0);

        // For handling "Anonymous" User
        TEST_ASSERT(MA_OK == ma_repository_set_password(my_repository, NULL));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_password(my_repository, &my_password));
        TEST_ASSERT_NULL(my_password);
    }

    // Following fields are not applicable for HTTP
    {
        const char *value = NULL;
        ma_uint32_t my_ssl_port = 22;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_server_name(my_repository, "server_12345"));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_server_name(my_repository, &value));
        TEST_ASSERT_NULL(value);

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_server_ip(my_repository, "127.0.1.2"));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_server_ip(my_repository, &value));
        TEST_ASSERT_NULL(value);

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_domain_name(my_repository, "non-coe"));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_domain_name(my_repository, &value));
        TEST_ASSERT_NULL(value);

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_ssl_port(my_repository, 1000));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_ssl_port(my_repository, &my_ssl_port));
    }

    TEST_ASSERT(MA_OK == ma_repository_release(my_repository));


    // FTP
    TEST_ASSERT(MA_OK == ma_repository_create(&my_repository));

    {
        ma_repository_url_type_t my_repository_url_type;
        TEST_ASSERT(MA_OK == ma_repository_set_url_type(my_repository, MA_REPOSITORY_URL_TYPE_FTP));
        TEST_ASSERT(MA_OK == ma_repository_get_url_type(my_repository, &my_repository_url_type));
        TEST_ASSERT(MA_REPOSITORY_URL_TYPE_FTP == my_repository_url_type);
    }

    {
        const char *my_server_fqdn = NULL;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_server_fqdn(my_repository, &my_server_fqdn));
        TEST_ASSERT_NULL(my_server_fqdn);

        TEST_ASSERT(MA_OK == ma_repository_set_server_fqdn(my_repository, "https:\\\\internal.mileaccess.com"));
        TEST_ASSERT(MA_OK == ma_repository_get_server_fqdn(my_repository, &my_server_fqdn));
        TEST_ASSERT(strcmp("https:\\\\internal.mileaccess.com", my_server_fqdn) == 0);

        TEST_ASSERT(MA_OK == ma_repository_set_server_fqdn(my_repository, "https:\\\\internal.mileaccess.com\\help"));
        TEST_ASSERT(MA_OK == ma_repository_get_server_fqdn(my_repository, &my_server_fqdn));
        TEST_ASSERT(strcmp("https:\\\\internal.mileaccess.com\\help", my_server_fqdn) == 0);
    }

    {
        const char *my_server_path = NULL;

        // Trying to retrieve server path without setting it first
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_server_path(my_repository, &my_server_path));
        TEST_ASSERT_NULL(my_server_path);

        TEST_ASSERT(MA_OK == ma_repository_set_server_path(my_repository, "https:\\\\192.16.220.153:80\\core"));
        TEST_ASSERT(MA_OK == ma_repository_get_server_path(my_repository, &my_server_path));
        TEST_ASSERT(strcmp("https:\\\\192.16.220.153:80\\core", my_server_path) == 0);

        TEST_ASSERT(MA_OK == ma_repository_set_server_path(my_repository, "https:\\\\www.google.co.in\\index.html"));
        TEST_ASSERT(MA_OK == ma_repository_get_server_path(my_repository, &my_server_path));
        TEST_ASSERT(strcmp("https:\\\\www.google.co.in\\index.html", my_server_path) == 0);
    }

    {
        ma_repository_auth_t my_repository_auth;

        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, MA_REPOSITORY_AUTH_NONE));
        TEST_ASSERT(MA_OK == ma_repository_get_authentication(my_repository, &my_repository_auth));
        TEST_ASSERT(MA_REPOSITORY_AUTH_NONE == my_repository_auth);

        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, MA_REPOSITORY_AUTH_ANONYMOUS));
        TEST_ASSERT(MA_OK == ma_repository_get_authentication(my_repository, &my_repository_auth));
        TEST_ASSERT(MA_REPOSITORY_AUTH_ANONYMOUS == my_repository_auth);

        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, MA_REPOSITORY_AUTH_LOGGEDON_USER));
        TEST_ASSERT(MA_OK == ma_repository_get_authentication(my_repository, &my_repository_auth));
        TEST_ASSERT(MA_REPOSITORY_AUTH_LOGGEDON_USER == my_repository_auth);

        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, MA_REPOSITORY_AUTH_CREDENTIALS));
        TEST_ASSERT(MA_OK == ma_repository_get_authentication(my_repository, &my_repository_auth));
        TEST_ASSERT(MA_REPOSITORY_AUTH_CREDENTIALS == my_repository_auth);
    }

    {
        ma_uint32_t my_port = 12323;

        TEST_ASSERT(MA_OK == ma_repository_set_port(my_repository, 6543));
        TEST_ASSERT(MA_OK == ma_repository_get_port(my_repository, &my_port));
        TEST_ASSERT(6543 == my_port);
    }

    {
        const char *my_user_name = NULL;

        // Tring to get username, when it is not yet set
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_user_name(my_repository, &my_user_name));
        TEST_ASSERT_NULL(my_user_name);

        TEST_ASSERT(MA_OK == ma_repository_set_user_name(my_repository, "normal user"));
        TEST_ASSERT(MA_OK == ma_repository_get_user_name(my_repository, &my_user_name));
        TEST_ASSERT(strcmp("normal user", my_user_name) == 0);

        TEST_ASSERT(MA_OK == ma_repository_set_user_name(my_repository, "Primary User"));
        TEST_ASSERT(MA_OK == ma_repository_get_user_name(my_repository, &my_user_name));
        TEST_ASSERT(strcmp("Primary User", my_user_name) == 0);
    }

    {
        const char *my_password = NULL;

        // Tring to get password, when it is not yet set
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_password(my_repository, &my_password));
        TEST_ASSERT_NULL(my_password);

        TEST_ASSERT(MA_OK == ma_repository_set_password(my_repository, "edfjnelmn232323n$%$%$%"));
        TEST_ASSERT(MA_OK == ma_repository_get_password(my_repository, &my_password));
        TEST_ASSERT(strcmp("edfjnelmn232323n$%$%$%", my_password) == 0);

        TEST_ASSERT(MA_OK == ma_repository_set_password(my_repository, "3398638638638643"));
        TEST_ASSERT(MA_OK == ma_repository_get_password(my_repository, &my_password));
        TEST_ASSERT(strcmp("3398638638638643", my_password) == 0);

        TEST_ASSERT(MA_OK == ma_repository_set_password(my_repository, NULL));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_password(my_repository, &my_password));
        TEST_ASSERT_NULL(my_password);
    }

    // Following fields are not applicable for FTP
    {
        const char *value = NULL;
        ma_uint32_t my_ssl_port = 22;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_server_name(my_repository, "server_12345"));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_server_name(my_repository, &value));
        TEST_ASSERT_NULL(value);

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_server_ip(my_repository, "127.0.1.2"));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_server_ip(my_repository, &value));
        TEST_ASSERT_NULL(value);

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_domain_name(my_repository, "non-coe"));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_domain_name(my_repository, &value));
        TEST_ASSERT_NULL(value);

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_ssl_port(my_repository, 1000));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_ssl_port(my_repository, &my_ssl_port));
    }

    TEST_ASSERT(MA_OK == ma_repository_release(my_repository));


    // SPIPE
    TEST_ASSERT(MA_OK == ma_repository_create(&my_repository));

    {
        ma_repository_url_type_t my_repository_url_type;

        TEST_ASSERT(MA_OK == ma_repository_set_url_type(my_repository, MA_REPOSITORY_URL_TYPE_SPIPE));
        TEST_ASSERT(MA_OK == ma_repository_get_url_type(my_repository, &my_repository_url_type));
        TEST_ASSERT(MA_REPOSITORY_URL_TYPE_SPIPE == my_repository_url_type);
    }

    {
        const char *my_server_fqdn = NULL;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_server_fqdn(my_repository, &my_server_fqdn));
        TEST_ASSERT_NULL(my_server_fqdn);

        TEST_ASSERT(MA_OK == ma_repository_set_server_fqdn(my_repository, "https:\\\\external.mileaccess.com"));
        TEST_ASSERT(MA_OK == ma_repository_get_server_fqdn(my_repository, &my_server_fqdn));
        TEST_ASSERT(strcmp("https:\\\\external.mileaccess.com", my_server_fqdn) == 0);

        TEST_ASSERT(MA_OK == ma_repository_set_server_fqdn(my_repository, "https:\\\\internal.mileaccess.com\\help"));
        TEST_ASSERT(MA_OK == ma_repository_get_server_fqdn(my_repository, &my_server_fqdn));
        TEST_ASSERT(strcmp("https:\\\\internal.mileaccess.com\\help", my_server_fqdn) == 0);
    }

    {
        const char *my_server_name = NULL;

        // Getting Server Name, before setting it.
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_server_name(my_repository, &my_server_name));
        TEST_ASSERT_NULL(my_server_name);

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_server_name(NULL, "server_12345"));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_server_name(my_repository, NULL));
        TEST_ASSERT(MA_OK == ma_repository_set_server_name(my_repository, "server_12345"));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_server_name(NULL, &my_server_name));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_server_name(my_repository, NULL));
        TEST_ASSERT(MA_OK == ma_repository_get_server_name(my_repository, &my_server_name));
        TEST_ASSERT(strcmp("server_12345", my_server_name) == 0);

        TEST_ASSERT(MA_OK == ma_repository_set_server_name(my_repository, "primary_server"));
        TEST_ASSERT(MA_OK == ma_repository_get_server_name(my_repository, &my_server_name));
        TEST_ASSERT(strcmp("primary_server", my_server_name) == 0);
    }

    {
        const char *my_server_ip = NULL;

        // Getting Server IP, before setting it.
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_server_ip(my_repository, &my_server_ip));
        TEST_ASSERT_NULL(my_server_ip);

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_server_ip(NULL, "192.168.234.127"));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_server_ip(my_repository, NULL));
        TEST_ASSERT(MA_OK == ma_repository_set_server_ip(my_repository, "192.168.234.127"));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_server_ip(NULL, &my_server_ip));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_server_ip(my_repository, NULL));
        TEST_ASSERT(MA_OK == ma_repository_get_server_ip(my_repository, &my_server_ip));
        TEST_ASSERT(strcmp("192.168.234.127", my_server_ip) == 0);

        TEST_ASSERT(MA_OK == ma_repository_set_server_ip(my_repository, "127.0.0.1"));
        TEST_ASSERT(MA_OK == ma_repository_get_server_ip(my_repository, &my_server_ip));
        TEST_ASSERT(strcmp("127.0.0.1", my_server_ip) == 0);
    }

    {
        const char *my_server_path = NULL;

        // Trying to retrieve server path without setting it first
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_server_path(my_repository, &my_server_path));
        TEST_ASSERT_NULL(my_server_path);

        TEST_ASSERT(MA_OK == ma_repository_set_server_path(my_repository, "https:\\\\192.16.110.153:80\\core"));
        TEST_ASSERT(MA_OK == ma_repository_get_server_path(my_repository, &my_server_path));
        TEST_ASSERT(strcmp("https:\\\\192.16.110.153:80\\core", my_server_path) == 0);

        TEST_ASSERT(MA_OK == ma_repository_set_server_path(my_repository, "http:\\\\en.wikipedia.org\\wiki\\Main_Page"));
        TEST_ASSERT(MA_OK == ma_repository_get_server_path(my_repository, &my_server_path));
        TEST_ASSERT(strcmp("http:\\\\en.wikipedia.org\\wiki\\Main_Page", my_server_path) == 0);
    }

    {
        ma_repository_auth_t my_repository_auth;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_authentication(NULL, MA_REPOSITORY_AUTH_NONE));
        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, MA_REPOSITORY_AUTH_NONE));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_authentication(NULL, &my_repository_auth));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_authentication(my_repository, NULL));
        TEST_ASSERT(MA_OK == ma_repository_get_authentication(my_repository, &my_repository_auth));
        TEST_ASSERT(MA_REPOSITORY_AUTH_NONE == my_repository_auth);

        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, MA_REPOSITORY_AUTH_ANONYMOUS));
        TEST_ASSERT(MA_OK == ma_repository_get_authentication(my_repository, &my_repository_auth));
        TEST_ASSERT(MA_REPOSITORY_AUTH_ANONYMOUS == my_repository_auth);

        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, MA_REPOSITORY_AUTH_LOGGEDON_USER));
        TEST_ASSERT(MA_OK == ma_repository_get_authentication(my_repository, &my_repository_auth));
        TEST_ASSERT(MA_REPOSITORY_AUTH_LOGGEDON_USER == my_repository_auth);

        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, MA_REPOSITORY_AUTH_CREDENTIALS));
        TEST_ASSERT(MA_OK == ma_repository_get_authentication(my_repository, &my_repository_auth));
        TEST_ASSERT(MA_REPOSITORY_AUTH_CREDENTIALS == my_repository_auth);
    }

    {
        ma_uint32_t my_port = 12323;

        TEST_ASSERT(MA_OK == ma_repository_set_port(my_repository, 0));
        TEST_ASSERT(MA_OK == ma_repository_get_port(my_repository, &my_port));
        TEST_ASSERT(0 == my_port);
    }

    {
        ma_uint32_t my_ssl_port = 22;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_ssl_port(NULL, 83));
        TEST_ASSERT(MA_OK == ma_repository_set_ssl_port(my_repository, 83));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_ssl_port(NULL, &my_ssl_port));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_ssl_port(my_repository, NULL));
        TEST_ASSERT(MA_OK == ma_repository_get_ssl_port(my_repository, &my_ssl_port));
        TEST_ASSERT(83 == my_ssl_port);

        TEST_ASSERT(MA_OK == ma_repository_set_ssl_port(my_repository, 8443));
        TEST_ASSERT(MA_OK == ma_repository_get_ssl_port(my_repository, &my_ssl_port));
        TEST_ASSERT(8443 == my_ssl_port);
    }

    {
        const char *my_user_name = NULL;

        // Trying to get username, when it is not yet set
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_user_name(my_repository, &my_user_name));
        TEST_ASSERT_NULL(my_user_name);

        TEST_ASSERT(MA_OK == ma_repository_set_user_name(my_repository, "normal user"));
        TEST_ASSERT(MA_OK == ma_repository_get_user_name(my_repository, &my_user_name));
        TEST_ASSERT(strcmp("normal user", my_user_name) == 0);

        TEST_ASSERT(MA_OK == ma_repository_set_user_name(my_repository, "Primary User"));
        TEST_ASSERT(MA_OK == ma_repository_get_user_name(my_repository, &my_user_name));
        TEST_ASSERT(strcmp("Primary User", my_user_name) == 0);
    }

    {
        const char *my_password = NULL;

        // Trying to get password, when it is not yet set
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_password(my_repository, &my_password));
        TEST_ASSERT_NULL(my_password);

        TEST_ASSERT(MA_OK == ma_repository_set_password(my_repository, "edfjnelmn232323n$%$%$%"));
        TEST_ASSERT(MA_OK == ma_repository_get_password(my_repository, &my_password));
        TEST_ASSERT(strcmp("edfjnelmn232323n$%$%$%", my_password) == 0);

        TEST_ASSERT(MA_OK == ma_repository_set_password(my_repository, "3398638638638643"));
        TEST_ASSERT(MA_OK == ma_repository_get_password(my_repository, &my_password));
        TEST_ASSERT(strcmp("3398638638638643", my_password) == 0);

        TEST_ASSERT(MA_OK == ma_repository_set_password(my_repository, NULL));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_password(my_repository, &my_password));
        TEST_ASSERT_NULL(my_password);
    }

    // Domain Name is not applicable for SPIPE
    {
        const char *my_domain_name = NULL;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_domain_name(my_repository, "non-coe"));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_domain_name(my_repository, &my_domain_name));
    }

    TEST_ASSERT(MA_OK == ma_repository_release(my_repository));

    // SA
    TEST_ASSERT(MA_OK == ma_repository_create(&my_repository));

    {
        ma_repository_url_type_t my_repository_url_type;
        TEST_ASSERT(MA_OK == ma_repository_set_url_type(my_repository, MA_REPOSITORY_URL_TYPE_SA));
        TEST_ASSERT(MA_OK == ma_repository_get_url_type(my_repository, &my_repository_url_type));
        TEST_ASSERT(MA_REPOSITORY_URL_TYPE_SA == my_repository_url_type);
    }

    {
        const char *my_server_fqdn = NULL;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_server_fqdn(my_repository, &my_server_fqdn));
        TEST_ASSERT_NULL(my_server_fqdn);

        TEST_ASSERT(MA_OK == ma_repository_set_server_fqdn(my_repository, "https:\\\\corp.mileaccess.com"));
        TEST_ASSERT(MA_OK == ma_repository_get_server_fqdn(my_repository, &my_server_fqdn));
        TEST_ASSERT(strcmp("https:\\\\corp.mileaccess.com", my_server_fqdn) == 0);

        TEST_ASSERT(MA_OK == ma_repository_set_server_fqdn(my_repository, "https:\\\\internal.mileaccess.com\\help"));
        TEST_ASSERT(MA_OK == ma_repository_get_server_fqdn(my_repository, &my_server_fqdn));
        TEST_ASSERT(strcmp("https:\\\\internal.mileaccess.com\\help", my_server_fqdn) == 0);
    }

    {
        const char *my_server_name = NULL;

        // Getting Server Name, before setting it.
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_server_name(my_repository, &my_server_name));
        TEST_ASSERT_NULL(my_server_name);

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_server_name(NULL, "server_4567"));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_server_name(my_repository, NULL));
        TEST_ASSERT(MA_OK == ma_repository_set_server_name(my_repository, "server_45678"));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_server_name(NULL, &my_server_name));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_server_name(my_repository, NULL));
        TEST_ASSERT(MA_OK == ma_repository_get_server_name(my_repository, &my_server_name));
        TEST_ASSERT(strcmp("server_45678", my_server_name) == 0);

        TEST_ASSERT(MA_OK == ma_repository_set_server_name(my_repository, "primary_server"));
        TEST_ASSERT(MA_OK == ma_repository_get_server_name(my_repository, &my_server_name));
        TEST_ASSERT(strcmp("primary_server", my_server_name) == 0);
    }

    {
        const char *my_server_ip = NULL;

        // Getting Server IP, before setting it.
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_server_ip(my_repository, &my_server_ip));
        TEST_ASSERT_NULL(my_server_ip);

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_server_ip(NULL, "192.168.234.127"));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_server_ip(my_repository, NULL));
        TEST_ASSERT(MA_OK == ma_repository_set_server_ip(my_repository, "192.168.234.127"));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_server_ip(NULL, &my_server_ip));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_server_ip(my_repository, NULL));
        TEST_ASSERT(MA_OK == ma_repository_get_server_ip(my_repository, &my_server_ip));
        TEST_ASSERT(strcmp("192.168.234.127", my_server_ip) == 0);

        TEST_ASSERT(MA_OK == ma_repository_set_server_ip(my_repository, "127.0.0.1"));
        TEST_ASSERT(MA_OK == ma_repository_get_server_ip(my_repository, &my_server_ip));
        TEST_ASSERT(strcmp("127.0.0.1", my_server_ip) == 0);
    }

    {
        const char *my_server_path = NULL;

        // Trying to retrieve server path without setting it first
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_server_path(my_repository, &my_server_path));
        TEST_ASSERT_NULL(my_server_path);

        TEST_ASSERT(MA_OK == ma_repository_set_server_path(my_repository, "https:\\\\10.213.254.8:80\\core"));
        TEST_ASSERT(MA_OK == ma_repository_get_server_path(my_repository, &my_server_path));
        TEST_ASSERT(strcmp("https:\\\\10.213.254.8:80\\core", my_server_path) == 0);

        TEST_ASSERT(MA_OK == ma_repository_set_server_path(my_repository, "http:\\\\en.wikipedia.org\\wiki\\Main_Page"));
        TEST_ASSERT(MA_OK == ma_repository_get_server_path(my_repository, &my_server_path));
        TEST_ASSERT(strcmp("http:\\\\en.wikipedia.org\\wiki\\Main_Page", my_server_path) == 0);
    }

    {
        ma_repository_auth_t my_repository_auth;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_authentication(NULL, MA_REPOSITORY_AUTH_ANONYMOUS));
        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, MA_REPOSITORY_AUTH_ANONYMOUS));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_authentication(NULL, &my_repository_auth));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_authentication(my_repository, NULL));
        TEST_ASSERT(MA_OK == ma_repository_get_authentication(my_repository, &my_repository_auth));
        TEST_ASSERT(MA_REPOSITORY_AUTH_ANONYMOUS == my_repository_auth);

        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, MA_REPOSITORY_AUTH_NONE));
        TEST_ASSERT(MA_OK == ma_repository_get_authentication(my_repository, &my_repository_auth));
        TEST_ASSERT(MA_REPOSITORY_AUTH_NONE == my_repository_auth);

        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, MA_REPOSITORY_AUTH_LOGGEDON_USER));
        TEST_ASSERT(MA_OK == ma_repository_get_authentication(my_repository, &my_repository_auth));
        TEST_ASSERT(MA_REPOSITORY_AUTH_LOGGEDON_USER == my_repository_auth);
    }

    {
        ma_uint32_t my_port = 12323;

        TEST_ASSERT(MA_OK == ma_repository_set_port(my_repository, 1000));
        TEST_ASSERT(MA_OK == ma_repository_get_port(my_repository, &my_port));
        TEST_ASSERT(1000 == my_port);
    }

    {
        ma_uint32_t my_ssl_port = 22;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_ssl_port(NULL, 8443));
        TEST_ASSERT(MA_OK == ma_repository_set_ssl_port(my_repository, 8443));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_ssl_port(NULL, &my_ssl_port));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_ssl_port(my_repository, NULL));
        TEST_ASSERT(MA_OK == ma_repository_get_ssl_port(my_repository, &my_ssl_port));
        TEST_ASSERT(8443 == my_ssl_port);
    }

    {
        const char *my_user_name = NULL;

        // Tring to get username, when it is not yet set
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_user_name(my_repository, &my_user_name));
        TEST_ASSERT_NULL(my_user_name);

        TEST_ASSERT(MA_OK == ma_repository_set_user_name(my_repository, "super user"));
        TEST_ASSERT(MA_OK == ma_repository_get_user_name(my_repository, &my_user_name));
        TEST_ASSERT(strcmp("super user", my_user_name) == 0);

        TEST_ASSERT(MA_OK == ma_repository_set_user_name(my_repository, "Primary User"));
        TEST_ASSERT(MA_OK == ma_repository_get_user_name(my_repository, &my_user_name));
        TEST_ASSERT(strcmp("Primary User", my_user_name) == 0);
    }

    {
        const char *my_password = NULL;

        // Tring to get password, when it is not yet set
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_password(my_repository, &my_password));
        TEST_ASSERT_NULL(my_password);

        TEST_ASSERT(MA_OK == ma_repository_set_password(my_repository, "e3;3iprk3;r3lkr;3rk34i"));
        TEST_ASSERT(MA_OK == ma_repository_get_password(my_repository, &my_password));
        TEST_ASSERT(strcmp("e3;3iprk3;r3lkr;3rk34i", my_password) == 0);

        TEST_ASSERT(MA_OK == ma_repository_set_password(my_repository, "3398638638638643"));
        TEST_ASSERT(MA_OK == ma_repository_get_password(my_repository, &my_password));
        TEST_ASSERT(strcmp("3398638638638643", my_password) == 0);

        TEST_ASSERT(MA_OK == ma_repository_set_password(my_repository, NULL));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_password(my_repository, &my_password));
        TEST_ASSERT_NULL(my_password);
    }

    // Domain Name is not applicable for SA
    {
        const char *my_domain_name = NULL;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_domain_name(my_repository, "non-coe"));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_domain_name(my_repository, &my_domain_name));
    }

    TEST_ASSERT(MA_OK == ma_repository_release(my_repository));

    // UNC
    TEST_ASSERT(MA_OK == ma_repository_create(&my_repository));

    {
        ma_repository_url_type_t my_repository_url_type;
        TEST_ASSERT(MA_OK == ma_repository_set_url_type(my_repository, MA_REPOSITORY_URL_TYPE_UNC));
        TEST_ASSERT(MA_OK == ma_repository_get_url_type(my_repository, &my_repository_url_type));
        TEST_ASSERT(MA_REPOSITORY_URL_TYPE_UNC == my_repository_url_type);
    }

    {
        const char *my_server_fqdn = NULL;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_server_fqdn(my_repository, &my_server_fqdn));
        TEST_ASSERT_NULL(my_server_fqdn);

        TEST_ASSERT(MA_OK == ma_repository_set_server_fqdn(my_repository, "https:\\\\unc.mileaccess.com"));
        TEST_ASSERT(MA_OK == ma_repository_get_server_fqdn(my_repository, &my_server_fqdn));
        TEST_ASSERT(strcmp("https:\\\\unc.mileaccess.com", my_server_fqdn) == 0);

        TEST_ASSERT(MA_OK == ma_repository_set_server_fqdn(my_repository, "https:\\\\internal.mileaccess.com\\help"));
        TEST_ASSERT(MA_OK == ma_repository_get_server_fqdn(my_repository, &my_server_fqdn));
        TEST_ASSERT(strcmp("https:\\\\internal.mileaccess.com\\help", my_server_fqdn) == 0);
    }

    {
        const char *my_server_path = NULL;

        // Trying to retrieve server path without setting it first
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_server_path(my_repository, &my_server_path));
        TEST_ASSERT_NULL(my_server_path);

        TEST_ASSERT(MA_OK == ma_repository_set_server_path(my_repository, "https:\\\\192.16.110.153:80\\core"));
        TEST_ASSERT(MA_OK == ma_repository_get_server_path(my_repository, &my_server_path));
        TEST_ASSERT(strcmp("https:\\\\192.16.110.153:80\\core", my_server_path) == 0);

        TEST_ASSERT(MA_OK == ma_repository_set_server_path(my_repository, "http:\\\\en.wikipedia.org\\wiki\\Main_Page"));
        TEST_ASSERT(MA_OK == ma_repository_get_server_path(my_repository, &my_server_path));
        TEST_ASSERT(strcmp("http:\\\\en.wikipedia.org\\wiki\\Main_Page", my_server_path) == 0);
    }

    {
        ma_repository_auth_t my_repository_auth;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_authentication(NULL, MA_REPOSITORY_AUTH_NONE));
        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, MA_REPOSITORY_AUTH_NONE));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_authentication(NULL, &my_repository_auth));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_authentication(my_repository, NULL));
        TEST_ASSERT(MA_OK == ma_repository_get_authentication(my_repository, &my_repository_auth));
        TEST_ASSERT(MA_REPOSITORY_AUTH_NONE == my_repository_auth);

        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, MA_REPOSITORY_AUTH_ANONYMOUS));
        TEST_ASSERT(MA_OK == ma_repository_get_authentication(my_repository, &my_repository_auth));
        TEST_ASSERT(MA_REPOSITORY_AUTH_ANONYMOUS == my_repository_auth);

        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, MA_REPOSITORY_AUTH_LOGGEDON_USER));
        TEST_ASSERT(MA_OK == ma_repository_get_authentication(my_repository, &my_repository_auth));
        TEST_ASSERT(MA_REPOSITORY_AUTH_LOGGEDON_USER == my_repository_auth);

        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, MA_REPOSITORY_AUTH_CREDENTIALS));
        TEST_ASSERT(MA_OK == ma_repository_get_authentication(my_repository, &my_repository_auth));
        TEST_ASSERT(MA_REPOSITORY_AUTH_CREDENTIALS == my_repository_auth);
    }

    {
        const char *my_user_name = NULL;

        // Tring to get username, when it is not yet set
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_user_name(my_repository, &my_user_name));
        TEST_ASSERT_NULL(my_user_name);

        TEST_ASSERT(MA_OK == ma_repository_set_user_name(my_repository, "normal user"));
        TEST_ASSERT(MA_OK == ma_repository_get_user_name(my_repository, &my_user_name));
        TEST_ASSERT(strcmp("normal user", my_user_name) == 0);

        TEST_ASSERT(MA_OK == ma_repository_set_user_name(my_repository, "Primary User"));
        TEST_ASSERT(MA_OK == ma_repository_get_user_name(my_repository, &my_user_name));
        TEST_ASSERT(strcmp("Primary User", my_user_name) == 0);
    }

    {
        const char *my_password = NULL;

        // Tring to get password, when it is not yet set
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_password(my_repository, &my_password));
        TEST_ASSERT_NULL(my_password);

        TEST_ASSERT(MA_OK == ma_repository_set_password(my_repository, "edfjnelmn232323n$%$%$%"));
        TEST_ASSERT(MA_OK == ma_repository_get_password(my_repository, &my_password));
        TEST_ASSERT(strcmp("edfjnelmn232323n$%$%$%", my_password) == 0);

        TEST_ASSERT(MA_OK == ma_repository_set_password(my_repository, "3398638638638643"));
        TEST_ASSERT(MA_OK == ma_repository_get_password(my_repository, &my_password));
        TEST_ASSERT(strcmp("3398638638638643", my_password) == 0);

        TEST_ASSERT(MA_OK == ma_repository_set_password(my_repository, NULL));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_password(my_repository, &my_password));
        TEST_ASSERT_NULL(my_password);
    }

    {
        const char *my_domain_name = NULL;

        // Getting domain name before setting
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_domain_name(my_repository, &my_domain_name));
        TEST_ASSERT_NULL(my_domain_name);

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_domain_name(NULL, "nai-corp"));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_domain_name(my_repository, NULL));
        TEST_ASSERT(MA_OK == ma_repository_set_domain_name(my_repository, "nai-corp"));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_domain_name(NULL, &my_domain_name));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_domain_name(my_repository, NULL));
        TEST_ASSERT(MA_OK == ma_repository_get_domain_name(my_repository, &my_domain_name));
        TEST_ASSERT(strcmp("nai-corp", my_domain_name) == 0);

        TEST_ASSERT(MA_OK == ma_repository_set_domain_name(my_repository, "WorkGroup"));
        TEST_ASSERT(MA_OK == ma_repository_get_domain_name(my_repository, &my_domain_name));
        TEST_ASSERT(strcmp("WorkGroup", my_domain_name) == 0);
    }

    // Following fields are not applicable for UNC
    {
        const char *value = NULL;
        ma_uint32_t my_port = 0;
        ma_uint32_t my_ssl_port = 22;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_server_name(my_repository, "server_12345"));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_server_name(my_repository, &value));
        TEST_ASSERT_NULL(value);

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_server_ip(my_repository, "127.0.1.2"));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_server_ip(my_repository, &value));
        TEST_ASSERT_NULL(value);

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_port(my_repository, 8080));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_port(my_repository, &my_ssl_port));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_ssl_port(my_repository, 8081));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_ssl_port(my_repository, &my_ssl_port));
    }

    TEST_ASSERT(MA_OK == ma_repository_release(my_repository));


    // LOCAL
    TEST_ASSERT(MA_OK == ma_repository_create(&my_repository));

    {
        ma_repository_url_type_t my_repository_url_type;

        TEST_ASSERT(MA_OK == ma_repository_set_url_type(my_repository, MA_REPOSITORY_URL_TYPE_LOCAL));
        TEST_ASSERT(MA_OK == ma_repository_get_url_type(my_repository, &my_repository_url_type));
        TEST_ASSERT(MA_REPOSITORY_URL_TYPE_LOCAL == my_repository_url_type);
    }

    {
        const char *my_server_path = NULL;

        // Trying to retrieve server path without setting it first
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_server_path(my_repository, &my_server_path));
        TEST_ASSERT_NULL(my_server_path);

        TEST_ASSERT(MA_OK == ma_repository_set_server_path(my_repository, "https:\\\\192.16.110.153:80\\core"));
        TEST_ASSERT(MA_OK == ma_repository_get_server_path(my_repository, &my_server_path));
        TEST_ASSERT(strcmp("https:\\\\192.16.110.153:80\\core", my_server_path) == 0);

        TEST_ASSERT(MA_OK == ma_repository_set_server_path(my_repository, "http:\\\\en.wikipedia.org\\wiki\\Main_Page"));
        TEST_ASSERT(MA_OK == ma_repository_get_server_path(my_repository, &my_server_path));
        TEST_ASSERT(strcmp("http:\\\\en.wikipedia.org\\wiki\\Main_Page", my_server_path) == 0);
    }

    {
        ma_repository_auth_t my_repository_auth;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_authentication(NULL, MA_REPOSITORY_AUTH_NONE));
        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, MA_REPOSITORY_AUTH_NONE));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_authentication(NULL, &my_repository_auth));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_authentication(my_repository, NULL));
        TEST_ASSERT(MA_OK == ma_repository_get_authentication(my_repository, &my_repository_auth));
        TEST_ASSERT(MA_REPOSITORY_AUTH_NONE == my_repository_auth);

        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, MA_REPOSITORY_AUTH_ANONYMOUS));
        TEST_ASSERT(MA_OK == ma_repository_get_authentication(my_repository, &my_repository_auth));
        TEST_ASSERT(MA_REPOSITORY_AUTH_ANONYMOUS == my_repository_auth);

        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, MA_REPOSITORY_AUTH_LOGGEDON_USER));
        TEST_ASSERT(MA_OK == ma_repository_get_authentication(my_repository, &my_repository_auth));
        TEST_ASSERT(MA_REPOSITORY_AUTH_LOGGEDON_USER == my_repository_auth);

        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, MA_REPOSITORY_AUTH_CREDENTIALS));
        TEST_ASSERT(MA_OK == ma_repository_get_authentication(my_repository, &my_repository_auth));
        TEST_ASSERT(MA_REPOSITORY_AUTH_CREDENTIALS == my_repository_auth);
    }

    {
        const char *my_user_name = NULL;

        // Tring to get username, when it is not yet set
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_user_name(my_repository, &my_user_name));
        TEST_ASSERT_NULL(my_user_name);

        TEST_ASSERT(MA_OK == ma_repository_set_user_name(my_repository, "normal user"));
        TEST_ASSERT(MA_OK == ma_repository_get_user_name(my_repository, &my_user_name));
        TEST_ASSERT(strcmp("normal user", my_user_name) == 0);

        TEST_ASSERT(MA_OK == ma_repository_set_user_name(my_repository, "Primary User"));
        TEST_ASSERT(MA_OK == ma_repository_get_user_name(my_repository, &my_user_name));
        TEST_ASSERT(strcmp("Primary User", my_user_name) == 0);
    }

    {
        const char *my_password = NULL;

        // Tring to get password, when it is not yet set
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_password(my_repository, &my_password));
        TEST_ASSERT_NULL(my_password);

        TEST_ASSERT(MA_OK == ma_repository_set_password(my_repository, "edfjnelmn232323n$%$%$%"));
        TEST_ASSERT(MA_OK == ma_repository_get_password(my_repository, &my_password));
        TEST_ASSERT(strcmp("edfjnelmn232323n$%$%$%", my_password) == 0);

        TEST_ASSERT(MA_OK == ma_repository_set_password(my_repository, "3398638638638643"));
        TEST_ASSERT(MA_OK == ma_repository_get_password(my_repository, &my_password));
        TEST_ASSERT(strcmp("3398638638638643", my_password) == 0);

        TEST_ASSERT(MA_OK == ma_repository_set_password(my_repository, NULL));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_password(my_repository, &my_password));
        TEST_ASSERT_NULL(my_password);
    }

    {
        const char *my_domain_name = NULL;

        // Getting domain name before setting
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_domain_name(my_repository, &my_domain_name));
        TEST_ASSERT_NULL(my_domain_name);

        TEST_ASSERT(MA_OK == ma_repository_set_domain_name(my_repository, "non-coe"));
        TEST_ASSERT(MA_OK == ma_repository_get_domain_name(my_repository, &my_domain_name));
        TEST_ASSERT(strcmp("non-coe", my_domain_name) == 0);

        TEST_ASSERT(MA_OK == ma_repository_set_domain_name(my_repository, "Home-Group"));
        TEST_ASSERT(MA_OK == ma_repository_get_domain_name(my_repository, &my_domain_name));
        TEST_ASSERT(strcmp("Home-Group", my_domain_name) == 0);
    }

    // Following fields are not applicable for LOCAL
    {
        const char *value = NULL;
        ma_uint32_t my_port = 0;
        ma_uint32_t my_ssl_port = 22;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_server_fqdn(my_repository, "https:\\\\unc.mileaccess.com"));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_server_fqdn(my_repository, &value));
        TEST_ASSERT_NULL(value);

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_server_name(my_repository, "server_12345"));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_server_name(my_repository, &value));
        TEST_ASSERT_NULL(value);

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_server_ip(my_repository, "127.0.1.2"));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_server_ip(my_repository, &value));
        TEST_ASSERT_NULL(value);

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_port(my_repository, 8080));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_port(my_repository, &my_ssl_port));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_ssl_port(my_repository, 8081));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_get_ssl_port(my_repository, &my_ssl_port));
    }

    TEST_ASSERT(MA_OK == ma_repository_release(my_repository));
}


/**************************************************************************
Test covering the repository validation. Covering various scenarios where
the repository can be valid or invalid.
**************************************************************************/
TEST(repository_tests, repository_validation_test)
{
    // No repository
    {
        ma_repository_t *my_repository = NULL;
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_validate(my_repository));
    }

    // Valid repository but no parameters filled-in
    {
        ma_repository_t *my_repository = NULL;
        TEST_ASSERT(MA_OK == ma_repository_create(&my_repository));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_validate(my_repository));
        TEST_ASSERT(MA_OK == ma_repository_release(my_repository));
    }

    // Repository name is not set
    {
        ma_repository_t *my_repository = NULL;
        TEST_ASSERT(MA_OK == ma_repository_create(&my_repository));
        TEST_ASSERT(MA_OK == ma_repository_set_url_type(my_repository, MA_REPOSITORY_URL_TYPE_HTTP));
        TEST_ASSERT(MA_OK == ma_repository_set_type(my_repository, MA_REPOSITORY_TYPE_REPO));

        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, MA_REPOSITORY_AUTH_CREDENTIALS));
        TEST_ASSERT(MA_OK == ma_repository_set_namespace(my_repository, MA_REPOSITORY_NAMESPACE_LOCAL));
        TEST_ASSERT(MA_OK == ma_repository_set_proxy_usage_type(my_repository, MA_PROXY_USAGE_TYPE_SERVER_CONFIGURED));

        TEST_ASSERT(MA_OK == ma_repository_set_server_fqdn(my_repository, "http:\\\\www.server_12345.com"));
        TEST_ASSERT(MA_OK == ma_repository_set_server_path(my_repository, "https:\\\\192.16.220.345:8443\\core"));

        TEST_ASSERT(MA_OK == ma_repository_set_user_name(my_repository, "admin_user_10"));
        TEST_ASSERT(MA_OK == ma_repository_set_password(my_repository, "edfjnelmn232323n$%$%$%"));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_validate(my_repository));
        TEST_ASSERT(MA_OK == ma_repository_release(my_repository));
    }

    // URL Type is not valid
    {
        ma_repository_t *my_repository = NULL;
        TEST_ASSERT(MA_OK == ma_repository_create(&my_repository));
        TEST_ASSERT(MA_OK == ma_repository_set_name(my_repository, "Repository_01"));
        TEST_ASSERT(MA_OK == ma_repository_set_url_type(my_repository, MA_REPOSITORY_URL_TYPE_END));
        TEST_ASSERT(MA_OK == ma_repository_set_type(my_repository, MA_REPOSITORY_TYPE_MASTER));

        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, MA_REPOSITORY_AUTH_ANONYMOUS));
        TEST_ASSERT(MA_OK == ma_repository_set_namespace(my_repository, MA_REPOSITORY_NAMESPACE_GLOBAL));
        TEST_ASSERT(MA_OK == ma_repository_set_proxy_usage_type(my_repository, MA_PROXY_USAGE_TYPE_SYSTEM_CONFIGURED));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_server_fqdn(my_repository, "www.mileaccess.com/internal"));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_server_path(my_repository, "https:\\\\192.16.220.345:8443\\core"));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_validate(my_repository));
        TEST_ASSERT(MA_OK == ma_repository_release(my_repository));
    }

    // Repository Type is not valid
    {
        ma_repository_t *my_repository = NULL;
        TEST_ASSERT(MA_OK == ma_repository_create(&my_repository));
        TEST_ASSERT(MA_OK == ma_repository_set_name(my_repository, "Repository_02"));
        TEST_ASSERT(MA_OK == ma_repository_set_url_type(my_repository, MA_REPOSITORY_URL_TYPE_FTP));
        TEST_ASSERT(MA_OK == ma_repository_set_type(my_repository, MA_REPOSITORY_TYPE_END));

        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, MA_REPOSITORY_AUTH_LOGGEDON_USER));
        TEST_ASSERT(MA_OK == ma_repository_set_namespace(my_repository, MA_REPOSITORY_NAMESPACE_GLOBAL));
        TEST_ASSERT(MA_OK == ma_repository_set_proxy_usage_type(my_repository, MA_PROXY_USAGE_TYPE_NONE));

        TEST_ASSERT(MA_OK == ma_repository_set_server_fqdn(my_repository, "server_fqdn_1"));
        TEST_ASSERT(MA_OK == ma_repository_set_server_path(my_repository, "https:\\\\192.16.220.345:8443\\core"));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_validate(my_repository));
        TEST_ASSERT(MA_OK == ma_repository_release(my_repository));
    }

    // Authentication Type is not valid
    {
        ma_repository_t *my_repository = NULL;
        TEST_ASSERT(MA_OK == ma_repository_create(&my_repository));
        TEST_ASSERT(MA_OK == ma_repository_set_name(my_repository, "Repository_03"));
        TEST_ASSERT(MA_OK == ma_repository_set_url_type(my_repository, MA_REPOSITORY_URL_TYPE_UNC));
        TEST_ASSERT(MA_OK == ma_repository_set_type(my_repository, MA_REPOSITORY_TYPE_FALLBACK));

        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, MA_REPOSITORY_AUTH_END));
        TEST_ASSERT(MA_OK == ma_repository_set_namespace(my_repository, MA_REPOSITORY_NAMESPACE_GLOBAL));
        TEST_ASSERT(MA_OK == ma_repository_set_proxy_usage_type(my_repository, MA_PROXY_USAGE_TYPE_NONE));

        TEST_ASSERT(MA_OK == ma_repository_set_server_fqdn(my_repository, "https:\\\\secure.mileaccess.com"));
        TEST_ASSERT(MA_OK == ma_repository_set_server_path(my_repository, "https:\\\\192.16.220.345:8443\\core"));

        TEST_ASSERT(MA_OK == ma_repository_set_user_name(my_repository, "admin_user_10"));
        TEST_ASSERT(MA_OK == ma_repository_set_domain_name(my_repository, "nai-corp"));
        TEST_ASSERT(MA_OK == ma_repository_set_password(my_repository, "edfjnelmn232323n$%$%$%"));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_validate(my_repository));
        TEST_ASSERT(MA_OK == ma_repository_release(my_repository));
    }

    // Namespace Type is not valid
    {
        ma_repository_t *my_repository = NULL;
        TEST_ASSERT(MA_OK == ma_repository_create(&my_repository));
        TEST_ASSERT(MA_OK == ma_repository_set_name(my_repository, "Repository_04"));
        TEST_ASSERT(MA_OK == ma_repository_set_url_type(my_repository, MA_REPOSITORY_URL_TYPE_UNC));
        TEST_ASSERT(MA_OK == ma_repository_set_type(my_repository, MA_REPOSITORY_TYPE_FALLBACK));

        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, MA_REPOSITORY_AUTH_NONE));
        TEST_ASSERT(MA_OK == ma_repository_set_namespace(my_repository, MA_REPOSITORY_NAMESPACE_END));
        TEST_ASSERT(MA_OK == ma_repository_set_proxy_usage_type(my_repository, MA_PROXY_USAGE_TYPE_NONE));

        TEST_ASSERT(MA_OK == ma_repository_set_server_fqdn(my_repository, "server_12345"));
        TEST_ASSERT(MA_OK == ma_repository_set_server_path(my_repository, "https:\\\\192.16.220.345:8443\\core"));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_validate(my_repository));
        TEST_ASSERT(MA_OK == ma_repository_release(my_repository));
    }

    // Proxy Type is not valid
    {
        ma_repository_t *my_repository = NULL;
        TEST_ASSERT(MA_OK == ma_repository_create(&my_repository));
        TEST_ASSERT(MA_OK == ma_repository_set_name(my_repository, "Repository_05"));
        TEST_ASSERT(MA_OK == ma_repository_set_url_type(my_repository, MA_REPOSITORY_URL_TYPE_UNC));
        TEST_ASSERT(MA_OK == ma_repository_set_type(my_repository, MA_REPOSITORY_TYPE_FALLBACK));

        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, MA_REPOSITORY_AUTH_NONE));
        TEST_ASSERT(MA_OK == ma_repository_set_namespace(my_repository, MA_REPOSITORY_NAMESPACE_GLOBAL));
        TEST_ASSERT(MA_OK == ma_repository_set_proxy_usage_type(my_repository, MA_PROXY_USAGE_TYPE_END));

        TEST_ASSERT(MA_OK == ma_repository_set_server_fqdn(my_repository, "https:\\\\secure.mileaccess.server.com"));
        TEST_ASSERT(MA_OK == ma_repository_set_server_path(my_repository, "https:\\\\192.16.220.345:8443\\core"));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_validate(my_repository));
        TEST_ASSERT(MA_OK == ma_repository_release(my_repository));
    }

    // Server name is not provided for a HTTP repository
    {
        ma_repository_t *my_repository = NULL;
        TEST_ASSERT(MA_OK == ma_repository_create(&my_repository));
        TEST_ASSERT(MA_OK == ma_repository_set_name(my_repository, "Repository_06"));
        TEST_ASSERT(MA_OK == ma_repository_set_url_type(my_repository, MA_REPOSITORY_URL_TYPE_HTTP));
        TEST_ASSERT(MA_OK == ma_repository_set_type(my_repository, MA_REPOSITORY_TYPE_FALLBACK));

        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, MA_REPOSITORY_AUTH_NONE));
        TEST_ASSERT(MA_OK == ma_repository_set_namespace(my_repository, MA_REPOSITORY_NAMESPACE_GLOBAL));
        TEST_ASSERT(MA_OK == ma_repository_set_proxy_usage_type(my_repository, MA_PROXY_USAGE_TYPE_SERVER_CONFIGURED));

        TEST_ASSERT(MA_OK == ma_repository_set_server_path(my_repository, "https:\\\\192.16.220.345:8443\\core"));

        TEST_ASSERT(MA_OK == ma_repository_set_user_name(my_repository, "admin_user_10"));
        TEST_ASSERT(MA_OK == ma_repository_set_password(my_repository, "edfjnelmn232323n$%$%$%"));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_validate(my_repository));
        TEST_ASSERT(MA_OK == ma_repository_release(my_repository));
    }

    // Server name is not provided for a FTP repository
    {
        ma_repository_t *my_repository = NULL;
        TEST_ASSERT(MA_OK == ma_repository_create(&my_repository));
        TEST_ASSERT(MA_OK == ma_repository_set_name(my_repository, "Repository_07"));
        TEST_ASSERT(MA_OK == ma_repository_set_url_type(my_repository, MA_REPOSITORY_URL_TYPE_FTP));
        TEST_ASSERT(MA_OK == ma_repository_set_type(my_repository, MA_REPOSITORY_TYPE_FALLBACK));

        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, MA_REPOSITORY_AUTH_ANONYMOUS));
        TEST_ASSERT(MA_OK == ma_repository_set_namespace(my_repository, MA_REPOSITORY_NAMESPACE_GLOBAL));
        TEST_ASSERT(MA_OK == ma_repository_set_proxy_usage_type(my_repository, MA_PROXY_USAGE_TYPE_SERVER_CONFIGURED));

        TEST_ASSERT(MA_OK == ma_repository_set_server_path(my_repository, "https:\\\\192.16.220.345:8443\\core"));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_validate(my_repository));
        TEST_ASSERT(MA_OK == ma_repository_release(my_repository));
    }

    // Server path is not provided for a LOCAL repository
    {
        ma_repository_t *my_repository = NULL;
        TEST_ASSERT(MA_OK == ma_repository_create(&my_repository));
        TEST_ASSERT(MA_OK == ma_repository_set_name(my_repository, "Repository_08"));
        TEST_ASSERT(MA_OK == ma_repository_set_url_type(my_repository, MA_REPOSITORY_URL_TYPE_LOCAL));
        TEST_ASSERT(MA_OK == ma_repository_set_type(my_repository, MA_REPOSITORY_TYPE_FALLBACK));

        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, MA_REPOSITORY_AUTH_LOGGEDON_USER));
        TEST_ASSERT(MA_OK == ma_repository_set_namespace(my_repository, MA_REPOSITORY_NAMESPACE_GLOBAL));
        TEST_ASSERT(MA_OK == ma_repository_set_proxy_usage_type(my_repository, MA_PROXY_USAGE_TYPE_SERVER_CONFIGURED));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_validate(my_repository));
        TEST_ASSERT(MA_OK == ma_repository_release(my_repository));
    }

    // Server name is not provided for a SPIPE repository
    {
        ma_repository_t *my_repository = NULL;
        TEST_ASSERT(MA_OK == ma_repository_create(&my_repository));
        TEST_ASSERT(MA_OK == ma_repository_set_name(my_repository, "Repository_09"));
        TEST_ASSERT(MA_OK == ma_repository_set_url_type(my_repository, MA_REPOSITORY_URL_TYPE_SPIPE));
        TEST_ASSERT(MA_OK == ma_repository_set_type(my_repository, MA_REPOSITORY_TYPE_REPO));

        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, MA_REPOSITORY_AUTH_NONE));
        TEST_ASSERT(MA_OK == ma_repository_set_namespace(my_repository, MA_REPOSITORY_NAMESPACE_GLOBAL));
        TEST_ASSERT(MA_OK == ma_repository_set_proxy_usage_type(my_repository, MA_PROXY_USAGE_TYPE_SYSTEM_CONFIGURED));

        TEST_ASSERT(MA_OK == ma_repository_set_server_path(my_repository, "https:\\\\192.16.220.345:8443\\core"));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_validate(my_repository));
        TEST_ASSERT(MA_OK == ma_repository_release(my_repository));
    }

    // when the repository type is HTTP or FTP, but username and password is not provided
    {
        ma_repository_t *my_repository = NULL;
        TEST_ASSERT(MA_OK == ma_repository_create(&my_repository));
        TEST_ASSERT(MA_OK == ma_repository_set_name(my_repository, "Repository_10"));
        TEST_ASSERT(MA_OK == ma_repository_set_url_type(my_repository, MA_REPOSITORY_URL_TYPE_FTP));
        TEST_ASSERT(MA_OK == ma_repository_set_type(my_repository, MA_REPOSITORY_TYPE_REPO));

        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, MA_REPOSITORY_AUTH_CREDENTIALS));
        TEST_ASSERT(MA_OK == ma_repository_set_namespace(my_repository, MA_REPOSITORY_NAMESPACE_GLOBAL));
        TEST_ASSERT(MA_OK == ma_repository_set_proxy_usage_type(my_repository, MA_PROXY_USAGE_TYPE_SYSTEM_CONFIGURED));

        TEST_ASSERT(MA_OK == ma_repository_set_server_fqdn(my_repository, "dummy_server_fqdn"));
        TEST_ASSERT(MA_OK == ma_repository_set_server_path(my_repository, "https:\\\\192.16.220.345:8443\\core"));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_validate(my_repository));
        TEST_ASSERT(MA_OK == ma_repository_release(my_repository));
    }

    // when the repository type is HTTP or FTP, but authentication is not through credentials, the validation should
    // go through
    {
        ma_repository_t *my_repository = NULL;
        TEST_ASSERT(MA_OK == ma_repository_create(&my_repository));
        TEST_ASSERT(MA_OK == ma_repository_set_name(my_repository, "Repository_11"));
        TEST_ASSERT(MA_OK == ma_repository_set_url_type(my_repository, MA_REPOSITORY_URL_TYPE_HTTP));
        TEST_ASSERT(MA_OK == ma_repository_set_type(my_repository, MA_REPOSITORY_TYPE_REPO));

        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, MA_REPOSITORY_AUTH_LOGGEDON_USER));
        TEST_ASSERT(MA_OK == ma_repository_set_namespace(my_repository, MA_REPOSITORY_NAMESPACE_GLOBAL));
        TEST_ASSERT(MA_OK == ma_repository_set_proxy_usage_type(my_repository, MA_PROXY_USAGE_TYPE_SYSTEM_CONFIGURED));

        TEST_ASSERT(MA_OK == ma_repository_set_server_fqdn(my_repository, "server_12345"));

        TEST_ASSERT(MA_OK == ma_repository_validate(my_repository));
        TEST_ASSERT(MA_OK == ma_repository_release(my_repository));
    }

    // when the repository type is UNC, domain name is also needed for authentication
    {
        ma_repository_t *my_repository = NULL;
        TEST_ASSERT(MA_OK == ma_repository_create(&my_repository));
        TEST_ASSERT(MA_OK == ma_repository_set_name(my_repository, "Repository_12"));
        TEST_ASSERT(MA_OK == ma_repository_set_url_type(my_repository, MA_REPOSITORY_URL_TYPE_UNC));
        TEST_ASSERT(MA_OK == ma_repository_set_type(my_repository, MA_REPOSITORY_TYPE_REPO));

        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, MA_REPOSITORY_AUTH_CREDENTIALS));
        TEST_ASSERT(MA_OK == ma_repository_set_namespace(my_repository, MA_REPOSITORY_NAMESPACE_GLOBAL));
        TEST_ASSERT(MA_OK == ma_repository_set_proxy_usage_type(my_repository, MA_PROXY_USAGE_TYPE_SYSTEM_CONFIGURED));

        TEST_ASSERT(MA_OK == ma_repository_set_server_fqdn(my_repository, "server_12345"));

        TEST_ASSERT(MA_OK == ma_repository_set_user_name(my_repository, "admin_user_10"));
        TEST_ASSERT(MA_OK == ma_repository_set_domain_name(my_repository, "nai-corp"));
        TEST_ASSERT(MA_OK == ma_repository_set_password(my_repository, "edfjnelmn232323n$%$%$%"));

        TEST_ASSERT(MA_OK == ma_repository_validate(my_repository));
        TEST_ASSERT(MA_OK == ma_repository_release(my_repository));
    }

    // when the repository type is Local, domain name is also needed for authentication
    {
        ma_repository_t *my_repository = NULL;
        TEST_ASSERT(MA_OK == ma_repository_create(&my_repository));
        TEST_ASSERT(MA_OK == ma_repository_set_name(my_repository, "Repository_13"));
        TEST_ASSERT(MA_OK == ma_repository_set_url_type(my_repository, MA_REPOSITORY_URL_TYPE_LOCAL));
        TEST_ASSERT(MA_OK == ma_repository_set_type(my_repository, MA_REPOSITORY_TYPE_REPO));

        TEST_ASSERT(MA_OK == ma_repository_set_authentication(my_repository, MA_REPOSITORY_AUTH_CREDENTIALS));
        TEST_ASSERT(MA_OK == ma_repository_set_namespace(my_repository, MA_REPOSITORY_NAMESPACE_GLOBAL));
        TEST_ASSERT(MA_OK == ma_repository_set_proxy_usage_type(my_repository, MA_PROXY_USAGE_TYPE_SYSTEM_CONFIGURED));

        // FQDN is not applicable for Local Repository
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_set_server_fqdn(my_repository, "server_12345"));
        TEST_ASSERT(MA_OK == ma_repository_set_server_path(my_repository, "https:\\\\192.16.220.345:8443\\core"));

        TEST_ASSERT(MA_OK == ma_repository_set_user_name(my_repository, "admin_user_10"));
        TEST_ASSERT(MA_OK == ma_repository_set_domain_name(my_repository, "nai-corp"));
        TEST_ASSERT(MA_OK == ma_repository_set_password(my_repository, "edfjnelmn232323n$%$%$%"));

        TEST_ASSERT(MA_OK == ma_repository_validate(my_repository));
        TEST_ASSERT(MA_OK == ma_repository_release(my_repository));
    }
}



