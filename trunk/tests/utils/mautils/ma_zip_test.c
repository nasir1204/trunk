
#define UNITY_FIXTURE_MALLOC_OVERRIDES_H_
#include "unity.h"
#include "unity_fixture.h"
#include "unity_internals.h" 
#include "zlib.h"
#include "unzip.h"
#include "ma/ma_common.h"
#ifdef MA_WINDOWS
#include<windows.h>
#endif
#include <stdio.h>
#include <assert.h>

const char *dir_name=".\\TestKeys.zip";
const char *dest_dir=".\\TestKeysUnzip";
const char *dest_dird=L".\\TestKeysUnzip";
char *dir1="TestKeysUnzip";
char *dir2="Keys";
//char *command="cd Test";
//char *command_compress="compact /c /s:Test";
//char *command_uncompress="compact /u /s:Test";

TEST_GROUP_RUNNER(ma_zip_test_group) {
	do {RUN_TEST_CASE(ma_zip_test_tests, ma_zip_test);}while(0);
}

TEST_SETUP(ma_zip_test_tests) {
}

TEST_TEAR_DOWN(ma_zip_test_tests) {
}


TEST(ma_zip_test_tests, ma_zip_test)
{
	char str1[100], str2[100];
	//Test if zip file name is given as NULL
	TEST_ASSERT(MA_ERROR_APIFAILED==ma_unzip(NULL, NULL));

	//Test if zipped file name does not exist
	TEST_ASSERT(MA_ERROR_APIFAILED==ma_unzip("Invalid_File_Name", dest_dir));

	//Test if dest file name does not exist
	TEST_ASSERT(MA_ERROR_APIFAILED==ma_unzip(dir_name, "Invalid_File_Name"));

	//Test if Destination file name is given as NULL
	TEST_ASSERT(MA_OK==ma_unzip(dir_name, NULL));

	//Test if both file name are given
	CreateDirectory(dest_dird, NULL);
	TEST_ASSERT_EQUAL(MA_OK,ma_unzip(dir_name, dest_dir));

	//Clean up after unzipping the folder
	sprintf(str1, "rmdir /s /q %s", dir1);
	sprintf(str1, "rmdir /s /q %s", dir2);
	system(str1);
	system(str2);

	/*char destdirname[1000];
	gzFile *fi;
	FILE *fp1, *fp2;
	int i =0;
	char x[100]="ABCDEFGHIJksjgfvwsgfiuwegfiuwegfjwegfjgewrjkfgrwekjfaskjdfakdjsfkasfkkjfvkjfjkf";
	//TEST_ASSERT_EQUAL(0, mymkdir(dir_name));
#ifdef MA_WINDOWS
	
	fp1=fopen(file_name1, "wb");
	for(i = 0; i < 1024*300; i++)
		fwrite(x, sizeof(x[0]), sizeof(x)/sizeof(x[0]), fp1);
	fp2=fopen(file_name2, "wb");
	for(i = 0; i < 1024*300; i++)
		fwrite(x, sizeof(x[0]), sizeof(x)/sizeof(x[0]), fp2);
	fclose(fp1);
	fclose(fp2);
	
	i=0;
	system(command_compress);
	fi = (gzFile *)gzopen("file.gz","wb");
	gzwrite(*fi,"my decompressed data",strlen("my decompressed data"));
	gzclose(*fi);
	//ma_unzip("C:\SVN\MA\trunk\tests\utils\mautils\unit_test\Test\Test1.txt", NULL);
	//def(fp1, fp2, 0);
	
	//system(command_uncompress);
//	RemoveDirectory((LPCWSTR*)dir_name);
#endif*/

}