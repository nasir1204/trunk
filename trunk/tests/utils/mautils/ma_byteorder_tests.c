/*
Byte Order UTs
*/


#include "unity.h"
#include "unity_fixture.h"
#include "ma/internal/utils/text/ma_byteorder.h"

TEST_GROUP_RUNNER(ma_byteorder_test_group)
{
    do {RUN_TEST_CASE(ma_byteorder_tests, ma_byteorder_type_size_test)} while (0);
    do {RUN_TEST_CASE(ma_byteorder_tests, ma_byteorder_uint16_test)} while (0);
    do {RUN_TEST_CASE(ma_byteorder_tests, ma_byteorder_uint32_test)} while (0);
    do {RUN_TEST_CASE(ma_byteorder_tests, ma_byteorder_uint64_test)} while (0);
}

TEST_SETUP(ma_byteorder_tests) {}

TEST_TEAR_DOWN(ma_byteorder_tests) {}

TEST(ma_byteorder_tests, ma_byteorder_type_size_test)
{
    TEST_ASSERT(1 == sizeof(ma_uint8_t));
    TEST_ASSERT(2 == sizeof(ma_uint16_t));
    TEST_ASSERT(4 == sizeof(ma_uint32_t));
    TEST_ASSERT(8 == sizeof(ma_uint64_t));
}

TEST(ma_byteorder_tests, ma_byteorder_uint16_test)
{
    ma_uint16_t host_format = 0x1234;
    ma_uint16_t host_reverse_format = 0x3412;

    if(ma_byteorder_is_host_le())
    {
        TEST_ASSERT(host_format == ma_byteorder_host_to_le_uint16(host_format));
        TEST_ASSERT(host_reverse_format == ma_byteorder_host_to_be_uint16(host_format));
        TEST_ASSERT(host_format == ma_byteorder_le_to_host_uint16(host_format));
        TEST_ASSERT(host_reverse_format == ma_byteorder_be_to_host_uint16(host_format));
        TEST_ASSERT(host_format == ma_byteorder_be_to_host_uint16(ma_byteorder_host_to_be_uint16(host_format)));
        TEST_ASSERT(host_format == ma_byteorder_le_to_host_uint16(ma_byteorder_le_to_host_uint16(host_format)));
    }
    else
    {
        TEST_ASSERT(host_reverse_format == ma_byteorder_host_to_le_uint16(host_format));
        TEST_ASSERT(host_format == ma_byteorder_host_to_be_uint16(host_format));
        TEST_ASSERT(host_reverse_format == ma_byteorder_le_to_host_uint16(host_format));
        TEST_ASSERT(host_format == ma_byteorder_be_to_host_uint16(host_format));
        TEST_ASSERT(host_format == ma_byteorder_be_to_host_uint16(ma_byteorder_host_to_be_uint16(host_format)));
        TEST_ASSERT(host_format == ma_byteorder_le_to_host_uint16(ma_byteorder_le_to_host_uint16(host_format)));
    }
}

TEST(ma_byteorder_tests, ma_byteorder_uint32_test)
{
    ma_uint32_t host_format = 0x12345678UL;
    ma_uint32_t host_reverse_format = 0x78563412UL;

    if(ma_byteorder_is_host_le())
    {
        TEST_ASSERT(host_format == ma_byteorder_host_to_le_uint32(host_format));
        TEST_ASSERT(host_reverse_format == ma_byteorder_host_to_be_uint32(host_format));
        TEST_ASSERT(host_format == ma_byteorder_le_to_host_uint32(host_format));
        TEST_ASSERT(host_reverse_format == ma_byteorder_be_to_host_uint32(host_format));
        TEST_ASSERT(host_format == ma_byteorder_be_to_host_uint32(ma_byteorder_host_to_be_uint32(host_format)));
        TEST_ASSERT(host_format == ma_byteorder_le_to_host_uint32(ma_byteorder_le_to_host_uint32(host_format)));
    }
    else
    {
        TEST_ASSERT(host_reverse_format == ma_byteorder_host_to_le_uint32(host_format));
        TEST_ASSERT(host_format == ma_byteorder_host_to_be_uint32(host_format));
        TEST_ASSERT(host_reverse_format == ma_byteorder_le_to_host_uint32(host_format));
        TEST_ASSERT(host_format == ma_byteorder_be_to_host_uint32(host_format));
        TEST_ASSERT(host_format == ma_byteorder_be_to_host_uint32(ma_byteorder_host_to_be_uint32(host_format)));
        TEST_ASSERT(host_format == ma_byteorder_le_to_host_uint32(ma_byteorder_le_to_host_uint32(host_format)));
    }
}


TEST(ma_byteorder_tests, ma_byteorder_uint64_test)
{
    ma_uint64_t host_format = 0x12AB34CD560978EFULL;
    ma_uint64_t host_reverse_format = 0xEF780956CD34AB12ULL;

    if(ma_byteorder_is_host_le())
    {
        TEST_ASSERT(host_format == ma_byteorder_host_to_le_uint64(host_format));
        TEST_ASSERT(host_reverse_format == ma_byteorder_host_to_be_uint64(host_format));
        TEST_ASSERT(host_format == ma_byteorder_le_to_host_uint64(host_format));
        TEST_ASSERT(host_reverse_format == ma_byteorder_be_to_host_uint64(host_format));
        TEST_ASSERT(host_format == ma_byteorder_be_to_host_uint64(ma_byteorder_host_to_be_uint64(host_format)));
        TEST_ASSERT(host_format == ma_byteorder_le_to_host_uint64(ma_byteorder_le_to_host_uint64(host_format)));
    }
    else
    {
        TEST_ASSERT(host_reverse_format == ma_byteorder_host_to_le_uint64(host_format));
        TEST_ASSERT(host_format == ma_byteorder_host_to_be_uint64(host_format));
        TEST_ASSERT(host_reverse_format == ma_byteorder_le_to_host_uint64(host_format));
        TEST_ASSERT(host_format == ma_byteorder_be_to_host_uint64(host_format));
        TEST_ASSERT(host_format == ma_byteorder_be_to_host_uint64(ma_byteorder_host_to_be_uint64(host_format)));
        TEST_ASSERT(host_format == ma_byteorder_le_to_host_uint64(ma_byteorder_le_to_host_uint64(host_format)));
    }
}