/*
Hash Utility UTs
*/
#include "unity.h"
#include "unity_fixture.h"
#include "ma/internal/utils/datastructures/ma_hash.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

TEST_GROUP_RUNNER(ma_hash_test_group)
{
    do {RUN_TEST_CASE(ma_hash_tests, ma_hash_test)} while (0);
}

TEST_SETUP(ma_hash_tests) {}

TEST_TEAR_DOWN(ma_hash_tests) {}

TEST(ma_hash_tests, ma_hash_test)
{
    ma_uint32_t hash1 = 0;
	ma_uint32_t hash2 = 0;
	const char *key = "ld;fjndscndsofkehcptjhfndverpogthrevnlkfgklrsthnytrbfb,.bjfopgtyiputyoitujhrtbf.m,bf,.gbmfghljrthypoutgjeorgnmeg,.mdf,. \
					  vfmdbopgure0t89weurwqohnwqkdnwklcnoewkrh9wpruwrw890qudfwejfnlf,dgvgndfgoperth98ertuerogtjernglkerngerpogtijhreptu8ret8we90 \
					  tehtnkregnfertioperutoert908erutewtjerglerkgmeropgiuert08rtery98tgher9pthertoperhngerklgn";
	
	// If key is NULL, the hash is 0
	TEST_ASSERT(0 == ma_hash_compute(NULL, 0));

	// Verify that 1 Byte change, changes the Hash
	hash1 = ma_hash_compute("This is McAfee Agent 5.0", strlen("This is McAfee Agent 5.0"));
	hash2 = ma_hash_compute("This is McAfee Agent 5.1", strlen("This is McAfee Agent 5.1"));
	TEST_ASSERT_NOT_EQUAL(hash1, hash2);
	
	// Verify that string and its reverse string doesn't have same hash
	hash1 = ma_hash_compute("This is McAfee Agent 5.1", strlen("This is McAfee Agent 5.1"));
	hash2 = ma_hash_compute("1.5 tnegA eefAcM si sihT", strlen("1.5 tnegA eefAcM si sihT"));
	TEST_ASSERT_NOT_EQUAL(hash1, hash2);	

	// Hash for 1 byte
	hash1 = ma_hash_compute("T", strlen("T"));
	hash2 = ma_hash_compute("t", strlen("t"));
	TEST_ASSERT_NOT_EQUAL(hash1, hash2);

	// Hash for large dataset. Verifying that if hash is taken multiple times, it always return same value for same input
	hash1 = ma_hash_compute(key, strlen(key));
	hash2 = ma_hash_compute(key, strlen(key));
	TEST_ASSERT(hash1 == hash2);

	hash2 = ma_hash_compute(key, strlen(key));
	TEST_ASSERT(hash1 == hash2);

	hash2 = ma_hash_compute(key, strlen(key));
	TEST_ASSERT(hash1 == hash2);

	hash2 = ma_hash_compute(key, strlen(key));
	TEST_ASSERT(hash1 == hash2);
}

