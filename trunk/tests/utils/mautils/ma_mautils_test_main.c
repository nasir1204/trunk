#include "unity.h"
#include "unity_fixture.h"
#include "ma/ma_common.h"

#ifdef MA_WINDOWS
#include <windows.h>
#endif
#ifdef _MSC_VER
# include <crtdbg.h> /* */
# define MA_TRACKLEAKS _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF)
#else
# define MA_TRACKLEAKS ((void *)0)
#endif

static void run_all_tests(void)
{
    do { RUN_TEST_GROUP(ma_ini_parser_test_group); } while(0);
    do { RUN_TEST_GROUP(ma_byteorder_test_group); } while(0);
    do { RUN_TEST_GROUP(ma_bytbuffer_test_group); } while(0);
    do { RUN_TEST_GROUP(ma_bytestream_test_group); } while(0);
    do { RUN_TEST_GROUP(ma_hash_test_group); } while(0);
    do { RUN_TEST_GROUP(ma_temp_buffer_test_group); } while(0);
    do { RUN_TEST_GROUP(getopt_test_group); } while(0);	
    do { RUN_TEST_GROUP(ma_vector_test_group); } while(0);
    do { RUN_TEST_GROUP(ma_sys_api_test_group); } while(0);
    do { RUN_TEST_GROUP(ma_uuid_test_group); } while(0);
    do { RUN_TEST_GROUP(ma_slist_test_group); } while(0);
    do { RUN_TEST_GROUP(ma_map_test_group); } while(0);
    do { RUN_TEST_GROUP(json_utils_test_group) ; } while (0);
    do { RUN_TEST_GROUP(ma_regex_test_group) ; } while (0);
    do { RUN_TEST_GROUP(ma_fs_utils_test_group) ; } while (0);
    do { RUN_TEST_GROUP(ma_conf_test_group) ; } while (0);
    do { RUN_TEST_GROUP(ma_acl_test_group) ; } while (0);
}

int main(int argc, char* argv[])
{
    MA_TRACKLEAKS;
    return UnityMain(argc, argv, run_all_tests);
}

