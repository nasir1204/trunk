#include "ma/internal/utils/conf/ma_conf.h"
#include "ma/internal/utils/conf/ma_conf_log.h"

#include "unity.h"
#include "unity_fixture.h"
#include "unity_internals.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#if defined(MA_WINDOWS)
#define MA_EXPECTED_INSTALL_PATH_STR                "C:\\Program Files\\McAfee\\Agent"
#define MA_EXPECTED_LIB_PATH_STR                    "C:\\Program Files\\McAfee\\Agent"
#define MA_EXPECTED_DATA_PATH_STR                   "C:\\ProgramData\McAfee\\Agent\\"
#define MA_EXPECTED_DB_PATH_STR                     "C:\\Program Files\\McAfee\\Agent\\db"
#define MA_EXPECTED_LOGS_PATH_STR                   "C:\\ProgramData\McAfee\\Agent\\logs"
#define MA_EXPECTED_CERTSTORE_PATH_STR              "C:\\Program Files\\McAfee\\Agent\\certstore"
#define MA_EXPECTED_KEYSTORE_PATH_STR               "C:\\ProgramData\McAfee\\Agent\\keystore"
#endif


TEST_GROUP_RUNNER(ma_conf_test_group) {
    do {RUN_TEST_CASE(ma_conf_tests, ma_conf_get_apis_test)} while (0);    	
}

TEST_SETUP(ma_conf_tests) {}

TEST_TEAR_DOWN(ma_conf_tests) {}

/*TBD - needs to write the complete UT's */
TEST(ma_conf_tests, ma_conf_get_apis_test) {
    /*ma_temp_buffer_t path = MA_TEMP_BUFFER_INIT;
    ma_conf_get_install_path(&path);
    printf("install path = %s\n", (char *)ma_temp_buffer_get(&path));
    ma_temp_buffer_uninit(&path);
    ma_temp_buffer_init(&path);
    ma_conf_get_data_path(&path);
    printf("data path = %s\n", (char *)ma_temp_buffer_get(&path));
    ma_temp_buffer_uninit(&path);
    ma_temp_buffer_init(&path);
    ma_conf_get_logs_path(&path);
    printf("logs path = %s\n", (char *)ma_temp_buffer_get(&path));
    ma_temp_buffer_uninit(&path);
    ma_temp_buffer_init(&path);
    ma_conf_get_agent_log_file_name(&path);
    printf("log file name agent= %s\n", (char *)ma_temp_buffer_get(&path));
    ma_temp_buffer_uninit(&path);    
    */
}
