#define UNITY_FIXTURE_MALLOC_OVERRIDES_H_

#include "unity.h"
#include "unity_fixture.h"
#include "unity_internals.h" 

#include "ma/ma_common.h"
#include "ma/internal/utils/platform/ma_path_utils.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"

#ifdef MA_WINDOWS
#include <Windows.h>
#include <process.h>
#else
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>
#endif


#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>


TEST_GROUP_RUNNER(ma_path_utils_api_test_group) {
	do {RUN_TEST_CASE(ma_path_utils_api_tests, ma_pid_to_path_test);}while(0);
}

TEST_SETUP(ma_path_utils_api_tests) {
}

TEST_TEAR_DOWN(ma_path_utils_api_tests) {
}

TEST(ma_path_utils_api_tests, ma_pid_to_path_test) {
    long pid = getpid();
    ma_temp_buffer_t path = MA_TEMP_BUFFER_INIT;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_pid_to_path(pid, &path));
    printf("path = %s\n", (char*)ma_temp_buffer_get(&path));
    ma_temp_buffer_uninit(&path);
}

