/*
FS Utils UTs
*/
#include "unity.h"
#include "unity_fixture.h"
#include "ma/internal/utils/filesystem/ma_fs_utils.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/ma_macros.h"

#ifdef MA_WINDOWS
	#define TEST_DATA_DIR	".\\MAUTILS_TEST_DATA\\Test Data\\"
	#define TEST_DATA_DIR_EMPTY	 ".\\MAUTILS_TEST_DATA\\Test Data Empty\\"
	#define TEST_DATA_DIR_LONG	 ".\\MAUTILS_TEST_DATA\\This is a long directory creted to verify ma_fs_tils api's______________________\\"
	#define TEST_DATA_DIR_LONG_FILE	 ".\\MAUTILS_TEST_DATA\\Test Data Long"
	#define TEST_DATA_DIR_NUMERIC ".\\MAUTILS_TEST_DATA\\65478964\\"
	#define TEST_DATA_DIR_SPECIAL_CHAR ".\\MAUTILS_TEST_DATA\\^&$#^^$$$##\\"

	#define TEST_AGENT_ROOT	".\\AGENT\\"
	#define TEST_DIR1	".\\DIR1\\"
	#define TEST_DIR2	".\\DIR2\\"
	#define TEST_DIR3	".\\DIR3\\"
	#define TEST_DIR4	".\\DIR4\\"
	#define TEST_DATA	".\\DATA\\"
#else
	#define TEST_DATA_DIR	"./MAUTILS_TEST_DATA/Test Data/"
	#define TEST_DATA_DIR_EMPTY	 "./MAUTILS_TEST_DATA/Test Data Empty/"
	#define TEST_DATA_DIR_LONG	 "./MAUTILS_TEST_DATA/This is a long directory creted to verify ma_fs_tils api's______________________\\"
	#define TEST_DATA_DIR_LONG_FILE	 "./MAUTILS_TEST_DATA/Test Data Long"
	#define TEST_DATA_DIR_NUMERIC "./MAUTILS_TEST_DATA/65478964/"
	#define TEST_DATA_DIR_SPECIAL_CHAR "./MAUTILS_TEST_DATA/^&$#^^$$$##/"

	#define TEST_AGENT_ROOT	"./AGENT/"
	#define TEST_DIR1	"./DIR1/"
	#define TEST_DIR2	"./DIR2/"
	#define TEST_DIR3	"./DIR3/"
	#define TEST_DIR4	"./DIR4/"
	#define TEST_DATA	"./DATA/"
#endif

#define TEST_FILE_NAME	"fs_utils_test_data"
#define TEST_FILE_NAME_EMPTY	"fs_utils_empty_test_data"
#define TEST_FILE_NAME_DIR_LONG	"fs_utils_test_data_dir_long"
#define TEST_FILE_NAME_LONG	"this is a long test file created to verify ma_fs_utils api's ...............................................end"
#define TEST_FILE_NAME_NUMERIC	"78456189765"
#define TEST_FILE_NAME_SPECIAL_CHAR	"%^$#^$#&!@^#)$#"
#define NEW_TEST_FILE_NAME	"fs_utils_new_test_data"

static unsigned char test_data[] = {"This is McAfee Agent Software Testing"};
static unsigned char test_data_empty[] = {""};
static unsigned char test_data_long[] = {"This is a very very long message. "
                                           "The quick brown fox jumps over the lazy dog. "
                                           "We all go a little mad sometimes."
                                           "You can't handle the truth!"
                                           "You had me at 'hello'"
                                           "Show me the money"
                                           "And in the morning, I'm making waffles!"
                                           "Game over, man! Game over!"
                                           "I love the smell of napalm in the morning...Smells like victory."
                                           "I'll be back."
                                           "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"};
static unsigned char test_data_numeric[] = {"87667656487998654654245447979908765"};
static unsigned char test_data_special_char[] = {"&^(*&(^&^&^))(J&^%$$E@#@!@#!@#!@$#@#$%$%$%$^%$^%$^"};

static uv_loop_t *uv_loop = NULL;
static char source[MA_MAX_PATH_LEN] = {0}, destination[MA_MAX_PATH_LEN] = {0};

static void remove_test_folder()
{
	//char command[1024];

	//#ifdef MA_WINDOWS
	//	MA_MSC_SELECT(_snprintf, snprintf)(command, 1024, "%s %s", "rmdir /S /Q", TEST_AGENT_ROOT);
	//#else
	//	MA_MSC_SELECT(_snprintf, snprintf)(command, 1024, "%s %s", "rm -rf ", TEST_AGENT_ROOT);
	//#endif
	//system(command);

	TEST_ASSERT(MA_FALSE == ma_fs_remove_recursive_dir(NULL, TEST_AGENT_ROOT));
	TEST_ASSERT(MA_FALSE == ma_fs_remove_recursive_dir(uv_loop, NULL));
	TEST_ASSERT(MA_TRUE == ma_fs_remove_recursive_dir(uv_loop, TEST_AGENT_ROOT));

	// Root directory is deleted now, therefore, ma_fs_is_dir() should return false
	TEST_ASSERT(MA_FALSE == ma_fs_is_dir(uv_loop, TEST_AGENT_ROOT));

	// Calling delete again, after delete once, should return false
	//TEST_ASSERT(MA_FALSE == ma_fs_remove_recursive_dir(uv_loop, TEST_AGENT_ROOT));
}

static void create_test_folder()
{
	char command[1024];

	//remove_test_folder();

    MA_MSC_SELECT(_snprintf, snprintf)(command, 1024, "%s %s", "mkdir", TEST_AGENT_ROOT);
    system(command);
}

TEST_GROUP_RUNNER(ma_fs_utils_test_group)
{
	// do {RUN_TEST_CASE(ma_fs_utils_test, verify_file_size)} while (0);
    //do {RUN_TEST_CASE(ma_fs_utils_test, verify_file_write_data)} while (0);
	//do {RUN_TEST_CASE(ma_fs_utils_test, verify_file_copy)} while (0);
	//do {RUN_TEST_CASE(ma_fs_utils_test, verify_file_move)} while (0);
}

TEST_SETUP(ma_fs_utils_test)
{
	create_test_folder();

	uv_loop = uv_loop_new();
	TEST_ASSERT_NOT_NULL(uv_loop);

	// Tests to verify 'ma_fs_make_recursive_dir' API
	MA_MSC_SELECT(_snprintf,snprintf)(source, MA_MAX_PATH_LEN, "%s%c%s%c%s", TEST_AGENT_ROOT, MA_PATH_SEPARATOR, TEST_DIR1, MA_PATH_SEPARATOR, TEST_DATA);
	ma_fs_make_recursive_dir(uv_loop, NULL, source);

	TEST_ASSERT(MA_FALSE == ma_fs_is_dir(NULL, source));
	TEST_ASSERT(MA_FALSE == ma_fs_is_dir(uv_loop, NULL));
	TEST_ASSERT(MA_TRUE == ma_fs_is_dir(uv_loop, source));

	// Is file exists should return false for directory
	TEST_ASSERT(MA_FALSE == ma_fs_is_file_exists(NULL, source));
	TEST_ASSERT(MA_FALSE == ma_fs_is_file_exists(uv_loop, NULL));
	//TEST_ASSERT(MA_FALSE == ma_fs_is_file_exists(uv_loop, source)); // is directory

	MA_MSC_SELECT(_snprintf,snprintf)(source, MA_MAX_PATH_LEN, "%s%c%s", TEST_DIR2, MA_PATH_SEPARATOR, TEST_DATA);
	ma_fs_make_recursive_dir(uv_loop, TEST_AGENT_ROOT, source);

	// Making full path for ma_fs_is_dir verification
	MA_MSC_SELECT(_snprintf,snprintf)(source, MA_MAX_PATH_LEN, "%s%c%s%c%s", TEST_AGENT_ROOT, MA_PATH_SEPARATOR, TEST_DIR2, MA_PATH_SEPARATOR, TEST_DATA);
	TEST_ASSERT(MA_TRUE == ma_fs_is_dir(uv_loop, source));

	MA_MSC_SELECT(_snprintf,snprintf)(source, MA_MAX_PATH_LEN, "%s%c%s", TEST_DIR3, MA_PATH_SEPARATOR, TEST_DATA);
	// FAIL-> ma_fs_make_recursive_dir(NULL, TEST_AGENT_ROOT, source);

	MA_MSC_SELECT(_snprintf,snprintf)(source, MA_MAX_PATH_LEN, "%s%c%s%c%s", TEST_AGENT_ROOT, MA_PATH_SEPARATOR, TEST_DIR4, MA_PATH_SEPARATOR, TEST_DATA);
	// FAIL-> ma_fs_make_recursive_dir(uv_loop, TEST_AGENT_ROOT, NULL);
	TEST_ASSERT(MA_FALSE == ma_fs_is_dir(uv_loop, source));

	// Creates the test data
	MA_MSC_SELECT(_snprintf,snprintf)(source, MA_MAX_PATH_LEN, "%s", TEST_DATA_DIR);
	MA_MSC_SELECT(_snprintf,snprintf)(destination, MA_MAX_PATH_LEN, "%s%c%s%c%s", TEST_AGENT_ROOT, MA_PATH_SEPARATOR, TEST_DATA_DIR, MA_PATH_SEPARATOR, TEST_FILE_NAME);
	ma_fs_make_recursive_dir(uv_loop, TEST_AGENT_ROOT, source);
	TEST_ASSERT( MA_TRUE == ma_fs_write_data(uv_loop, destination, test_data, strlen((char*)test_data)));

	MA_MSC_SELECT(_snprintf,snprintf)(source, MA_MAX_PATH_LEN, "%s", TEST_DATA_DIR_EMPTY);
	MA_MSC_SELECT(_snprintf,snprintf)(destination, MA_MAX_PATH_LEN, "%s%c%s%c%s", TEST_AGENT_ROOT, MA_PATH_SEPARATOR, TEST_DATA_DIR_EMPTY, MA_PATH_SEPARATOR, TEST_FILE_NAME_EMPTY);
	ma_fs_make_recursive_dir(uv_loop, TEST_AGENT_ROOT, source);
	TEST_ASSERT( MA_TRUE == ma_fs_write_data(uv_loop, destination, test_data_empty, strlen((char*)test_data_empty)));

	MA_MSC_SELECT(_snprintf,snprintf)(source, MA_MAX_PATH_LEN, "%s", TEST_DATA_DIR_LONG_FILE);
	MA_MSC_SELECT(_snprintf,snprintf)(destination, MA_MAX_PATH_LEN, "%s%c%s%c%s", TEST_AGENT_ROOT, MA_PATH_SEPARATOR, TEST_DATA_DIR_LONG_FILE, MA_PATH_SEPARATOR, TEST_FILE_NAME_LONG);
	ma_fs_make_recursive_dir(uv_loop, TEST_AGENT_ROOT, source);
	TEST_ASSERT( MA_TRUE == ma_fs_write_data(uv_loop, destination, test_data_long, strlen((char*)test_data_long)));

	MA_MSC_SELECT(_snprintf,snprintf)(source, MA_MAX_PATH_LEN, "%s", TEST_DATA_DIR_LONG);
	MA_MSC_SELECT(_snprintf,snprintf)(destination, MA_MAX_PATH_LEN, "%s%c%s%c%s", TEST_AGENT_ROOT, MA_PATH_SEPARATOR, TEST_DATA_DIR_LONG, MA_PATH_SEPARATOR, TEST_FILE_NAME_DIR_LONG);
	ma_fs_make_recursive_dir(uv_loop, TEST_AGENT_ROOT, source);
	TEST_ASSERT( MA_TRUE == ma_fs_write_data(uv_loop, destination, test_data_long, strlen((char*)test_data_long)));

	MA_MSC_SELECT(_snprintf,snprintf)(source, MA_MAX_PATH_LEN, "%s", TEST_DATA_DIR_NUMERIC);
	MA_MSC_SELECT(_snprintf,snprintf)(destination, MA_MAX_PATH_LEN, "%s%c%s%c%s", TEST_AGENT_ROOT, MA_PATH_SEPARATOR, TEST_DATA_DIR_NUMERIC, MA_PATH_SEPARATOR, TEST_FILE_NAME_NUMERIC);
	ma_fs_make_recursive_dir(uv_loop, TEST_AGENT_ROOT, source);
	TEST_ASSERT( MA_TRUE == ma_fs_write_data(uv_loop, destination, test_data_numeric, strlen((char*)test_data_numeric)));

	MA_MSC_SELECT(_snprintf,snprintf)(source, MA_MAX_PATH_LEN, "%s", TEST_DATA_DIR_SPECIAL_CHAR);
	MA_MSC_SELECT(_snprintf,snprintf)(destination, MA_MAX_PATH_LEN, "%s%c%s%c%s", TEST_AGENT_ROOT, MA_PATH_SEPARATOR, TEST_DATA_DIR_SPECIAL_CHAR, MA_PATH_SEPARATOR, TEST_FILE_NAME_SPECIAL_CHAR);
	ma_fs_make_recursive_dir(uv_loop, TEST_AGENT_ROOT, source);
	TEST_ASSERT( MA_TRUE == ma_fs_write_data(uv_loop, destination, test_data_special_char, strlen((char*)test_data_special_char)));
}

TEST_TEAR_DOWN(ma_fs_utils_test)
{
	remove_test_folder();
	uv_loop_delete(uv_loop);
	uv_loop = NULL;
}

TEST(ma_fs_utils_test, verify_file_size)
{
	size_t size1 = 0;

	// Tests to verify 'ma_fs_is_file_exists' API
	MA_MSC_SELECT(_snprintf,snprintf)(destination, MA_MAX_PATH_LEN, "%s%c%s%c%s%c%s", TEST_AGENT_ROOT, MA_PATH_SEPARATOR, TEST_DIR1, MA_PATH_SEPARATOR, TEST_DATA, MA_PATH_SEPARATOR, TEST_FILE_NAME);
	TEST_ASSERT( MA_FALSE == ma_fs_is_file_exists(NULL, destination));
	TEST_ASSERT( MA_FALSE == ma_fs_is_file_exists(uv_loop, NULL));
	TEST_ASSERT( MA_FALSE == ma_fs_is_file_exists(uv_loop, destination));

	// Tests to verify 'ma_fs_get_file_size' API
	size1 = ma_fs_get_file_size(NULL, destination);
	TEST_ASSERT(-1 == size1);

	size1 = ma_fs_get_file_size(uv_loop, NULL);
	TEST_ASSERT(-1 == size1);

	// FAIL-> (Memory leak)
	size1 = ma_fs_get_file_size(uv_loop, destination);
	TEST_ASSERT(-1 == size1);

	MA_MSC_SELECT(_snprintf,snprintf)(destination, MA_MAX_PATH_LEN, "%s%c%s%c%s", TEST_AGENT_ROOT, MA_PATH_SEPARATOR, TEST_DATA_DIR, MA_PATH_SEPARATOR, TEST_FILE_NAME);
	TEST_ASSERT(MA_TRUE == ma_fs_is_file_exists(uv_loop, destination));
	size1 = ma_fs_get_file_size(uv_loop, destination);
	TEST_ASSERT(-1 != size1);

	MA_MSC_SELECT(_snprintf,snprintf)(destination, MA_MAX_PATH_LEN, "%s%c%s%c%s", TEST_AGENT_ROOT, MA_PATH_SEPARATOR, TEST_DATA_DIR_EMPTY, MA_PATH_SEPARATOR, TEST_FILE_NAME_EMPTY);
	TEST_ASSERT( MA_TRUE == ma_fs_is_file_exists(uv_loop, destination));
	size1 = ma_fs_get_file_size(uv_loop, destination);
	TEST_ASSERT(0 == size1);

	MA_MSC_SELECT(_snprintf,snprintf)(destination, MA_MAX_PATH_LEN, "%s%c%s%c%s", TEST_AGENT_ROOT, MA_PATH_SEPARATOR, TEST_DATA_DIR_LONG_FILE, MA_PATH_SEPARATOR, TEST_FILE_NAME_LONG);
	TEST_ASSERT( MA_TRUE == ma_fs_is_file_exists(uv_loop, destination));
	size1 = ma_fs_get_file_size(uv_loop, destination);
	TEST_ASSERT(-1 != size1);
	TEST_ASSERT(size1 == strlen((char*)test_data_long));

	MA_MSC_SELECT(_snprintf,snprintf)(destination, MA_MAX_PATH_LEN, "%s%c%s%c%s", TEST_AGENT_ROOT, MA_PATH_SEPARATOR, TEST_DATA_DIR_LONG, MA_PATH_SEPARATOR, TEST_FILE_NAME_DIR_LONG);
	TEST_ASSERT( MA_TRUE == ma_fs_is_file_exists(uv_loop, destination));
	size1 = ma_fs_get_file_size(uv_loop, destination);
	TEST_ASSERT(-1 != size1);
	TEST_ASSERT(size1 == strlen((char*)test_data_long));

	MA_MSC_SELECT(_snprintf,snprintf)(destination, MA_MAX_PATH_LEN, "%s%c%s%c%s", TEST_AGENT_ROOT, MA_PATH_SEPARATOR, TEST_DATA_DIR_NUMERIC, MA_PATH_SEPARATOR, TEST_FILE_NAME_NUMERIC);
	TEST_ASSERT( MA_TRUE == ma_fs_is_file_exists(uv_loop, destination));
	size1 = ma_fs_get_file_size(uv_loop, destination);
	TEST_ASSERT(-1 != size1);
	TEST_ASSERT(size1 == strlen((char*)test_data_numeric));

	MA_MSC_SELECT(_snprintf,snprintf)(destination, MA_MAX_PATH_LEN, "%s%c%s%c%s", TEST_AGENT_ROOT, MA_PATH_SEPARATOR, TEST_DATA_DIR_SPECIAL_CHAR, MA_PATH_SEPARATOR, TEST_FILE_NAME_SPECIAL_CHAR);
	TEST_ASSERT( MA_TRUE == ma_fs_is_file_exists(uv_loop, destination));
	size1 = ma_fs_get_file_size(uv_loop, destination);
	TEST_ASSERT(-1 != size1);
	TEST_ASSERT(size1 == strlen((char*)test_data_special_char));
}

TEST(ma_fs_utils_test, verify_file_write_data)
{
	size_t size1 = 0;

	MA_MSC_SELECT(_snprintf,snprintf)(source, MA_MAX_PATH_LEN, "%s%c%s%c%s", TEST_AGENT_ROOT, MA_PATH_SEPARATOR, TEST_DATA_DIR, MA_PATH_SEPARATOR, TEST_FILE_NAME);
	MA_MSC_SELECT(_snprintf,snprintf)(destination, MA_MAX_PATH_LEN, "%s%c%s%c%s%c%s", TEST_AGENT_ROOT, MA_PATH_SEPARATOR, TEST_DIR1, MA_PATH_SEPARATOR, TEST_DATA, MA_PATH_SEPARATOR, TEST_FILE_NAME);

	// Tests to verify 'ma_fs_write_data' API
	TEST_ASSERT( MA_TRUE == ma_fs_copy_file(uv_loop, source, destination));
	size1 = ma_fs_get_file_size(uv_loop, destination);

	TEST_ASSERT( MA_FALSE == ma_fs_write_data(NULL, destination, test_data, strlen((char*)test_data)));
	TEST_ASSERT( size1 == ma_fs_get_file_size(uv_loop, source));

	TEST_ASSERT( MA_FALSE == ma_fs_write_data(uv_loop, NULL, test_data, strlen((char*)test_data)));
	TEST_ASSERT( size1 == ma_fs_get_file_size(uv_loop, source));

	TEST_ASSERT( MA_FALSE == ma_fs_write_data(uv_loop, destination, NULL, strlen((char*)test_data)));
	TEST_ASSERT( size1 == ma_fs_get_file_size(uv_loop, source));
}

TEST(ma_fs_utils_test, verify_file_copy)
{
	size_t size1 = 0, size2 = 0;

	// Tests to verify 'ma_fs_copy_file' API
	MA_MSC_SELECT(_snprintf,snprintf)(source, MA_MAX_PATH_LEN, "%s%c%s%c%s", TEST_AGENT_ROOT, MA_PATH_SEPARATOR, TEST_DATA_DIR, MA_PATH_SEPARATOR, TEST_FILE_NAME);
	MA_MSC_SELECT(_snprintf,snprintf)(destination, MA_MAX_PATH_LEN, "%s%c%s%c%s%c%s", TEST_AGENT_ROOT, MA_PATH_SEPARATOR, TEST_DIR1, MA_PATH_SEPARATOR, TEST_DATA, MA_PATH_SEPARATOR, TEST_FILE_NAME);

	TEST_ASSERT( MA_FALSE == ma_fs_copy_file(NULL, source, destination));
	TEST_ASSERT( MA_FALSE == ma_fs_is_file_exists(uv_loop, destination));

	TEST_ASSERT( MA_FALSE == ma_fs_copy_file(uv_loop, NULL, destination));
	TEST_ASSERT( MA_FALSE == ma_fs_is_file_exists(uv_loop, destination));

	TEST_ASSERT( MA_FALSE == ma_fs_copy_file(uv_loop, source, NULL));
	TEST_ASSERT( MA_FALSE == ma_fs_is_file_exists(uv_loop, destination));

	TEST_ASSERT( MA_TRUE == ma_fs_copy_file(uv_loop, source, destination));
	TEST_ASSERT( MA_TRUE == ma_fs_is_file_exists(uv_loop, destination));

	size1 = ma_fs_get_file_size(uv_loop, source);
	size2 = ma_fs_get_file_size(uv_loop, destination);
	TEST_ASSERT(0 != size1);
	TEST_ASSERT(0 != size2);
	TEST_ASSERT(size1 == size2);

	MA_MSC_SELECT(_snprintf,snprintf)(source, MA_MAX_PATH_LEN, "%s%c%s%c%s", TEST_AGENT_ROOT, MA_PATH_SEPARATOR, TEST_DATA_DIR_EMPTY, MA_PATH_SEPARATOR, TEST_FILE_NAME_EMPTY);
	MA_MSC_SELECT(_snprintf,snprintf)(destination, MA_MAX_PATH_LEN, "%s%c%s%c%s%c%s", TEST_AGENT_ROOT, MA_PATH_SEPARATOR, TEST_DIR1, MA_PATH_SEPARATOR, TEST_DATA, MA_PATH_SEPARATOR, TEST_FILE_NAME_EMPTY);
	TEST_ASSERT( MA_TRUE == ma_fs_copy_file(uv_loop, source, destination));
	TEST_ASSERT( MA_TRUE == ma_fs_is_file_exists(uv_loop, destination));

	size1 = ma_fs_get_file_size(uv_loop, source);
	size2 = ma_fs_get_file_size(uv_loop, destination);
	TEST_ASSERT(0 == size1);
	TEST_ASSERT(0 == size2);
	TEST_ASSERT(size1 == size2);

	MA_MSC_SELECT(_snprintf,snprintf)(source, MA_MAX_PATH_LEN, "%s%c%s%c%s", TEST_AGENT_ROOT, MA_PATH_SEPARATOR, TEST_DATA_DIR_LONG_FILE, MA_PATH_SEPARATOR, TEST_FILE_NAME_LONG);
	MA_MSC_SELECT(_snprintf,snprintf)(destination, MA_MAX_PATH_LEN, "%s%c%s%c%s%c%s", TEST_AGENT_ROOT, MA_PATH_SEPARATOR, TEST_DIR1, MA_PATH_SEPARATOR, TEST_DATA, MA_PATH_SEPARATOR, TEST_FILE_NAME_LONG);
	TEST_ASSERT( MA_TRUE == ma_fs_copy_file(uv_loop, source, destination));
	TEST_ASSERT( MA_TRUE == ma_fs_is_file_exists(uv_loop, destination));

	size1 = ma_fs_get_file_size(uv_loop, source);
	size2 = ma_fs_get_file_size(uv_loop, destination);
	TEST_ASSERT(0 != size1);
	TEST_ASSERT(0 != size2);
	TEST_ASSERT(size1 == size2);

	MA_MSC_SELECT(_snprintf,snprintf)(source, MA_MAX_PATH_LEN, "%s%c%s%c%s", TEST_AGENT_ROOT, MA_PATH_SEPARATOR, TEST_DATA_DIR_LONG, MA_PATH_SEPARATOR, TEST_FILE_NAME_DIR_LONG);
	MA_MSC_SELECT(_snprintf,snprintf)(destination, MA_MAX_PATH_LEN, "%s%c%s%c%s%c%s", TEST_AGENT_ROOT, MA_PATH_SEPARATOR, TEST_DIR1, MA_PATH_SEPARATOR, TEST_DATA, MA_PATH_SEPARATOR, TEST_FILE_NAME_DIR_LONG);
	TEST_ASSERT( MA_TRUE == ma_fs_copy_file(uv_loop, source, destination));
	TEST_ASSERT( MA_TRUE == ma_fs_is_file_exists(uv_loop, destination));

	size1 = ma_fs_get_file_size(uv_loop, source);
	size2 = ma_fs_get_file_size(uv_loop, destination);
	TEST_ASSERT(0 != size1);
	TEST_ASSERT(0 != size2);
	TEST_ASSERT(size1 == size2);

	MA_MSC_SELECT(_snprintf,snprintf)(source, MA_MAX_PATH_LEN, "%s%c%s%c%s", TEST_AGENT_ROOT, MA_PATH_SEPARATOR, TEST_DATA_DIR_NUMERIC, MA_PATH_SEPARATOR, TEST_FILE_NAME_NUMERIC);
	MA_MSC_SELECT(_snprintf,snprintf)(destination, MA_MAX_PATH_LEN, "%s%c%s%c%s%c%s", TEST_AGENT_ROOT, MA_PATH_SEPARATOR, TEST_DIR1, MA_PATH_SEPARATOR, TEST_DATA, MA_PATH_SEPARATOR, TEST_FILE_NAME_NUMERIC);
	TEST_ASSERT( MA_TRUE == ma_fs_copy_file(uv_loop, source, destination));
	TEST_ASSERT( MA_TRUE == ma_fs_is_file_exists(uv_loop, destination));

	size1 = ma_fs_get_file_size(uv_loop, source);
	size2 = ma_fs_get_file_size(uv_loop, destination);
	TEST_ASSERT(0 != size1);
	TEST_ASSERT(0 != size2);
	TEST_ASSERT(size1 == size2);

	MA_MSC_SELECT(_snprintf,snprintf)(source, MA_MAX_PATH_LEN, "%s%c%s%c%s", TEST_AGENT_ROOT, MA_PATH_SEPARATOR, TEST_DATA_DIR_SPECIAL_CHAR, MA_PATH_SEPARATOR, TEST_FILE_NAME_SPECIAL_CHAR);
	MA_MSC_SELECT(_snprintf,snprintf)(destination, MA_MAX_PATH_LEN, "%s%c%s%c%s%c%s", TEST_AGENT_ROOT, MA_PATH_SEPARATOR, TEST_DIR1, MA_PATH_SEPARATOR, TEST_DATA, MA_PATH_SEPARATOR, TEST_FILE_NAME_SPECIAL_CHAR);
	TEST_ASSERT( MA_TRUE == ma_fs_copy_file(uv_loop, source, destination));
	TEST_ASSERT( MA_TRUE == ma_fs_is_file_exists(uv_loop, destination));

	size1 = ma_fs_get_file_size(uv_loop, source);
	size2 = ma_fs_get_file_size(uv_loop, destination);
	TEST_ASSERT(0 != size1);
	TEST_ASSERT(0 != size2);
	TEST_ASSERT( size1 == size2);
}

TEST(ma_fs_utils_test, verify_file_move)
{
	size_t size1 = 0;

	// Tests to verify 'ma_fs_move_file' & 'ma_fs_remove_file' API
	MA_MSC_SELECT(_snprintf,snprintf)(source, MA_MAX_PATH_LEN, "%s%c%s%c%s", TEST_AGENT_ROOT, MA_PATH_SEPARATOR, TEST_DATA_DIR, MA_PATH_SEPARATOR, TEST_FILE_NAME);
	MA_MSC_SELECT(_snprintf,snprintf)(destination, MA_MAX_PATH_LEN, "%s%c%s%c%s%c%s", TEST_AGENT_ROOT, MA_PATH_SEPARATOR, TEST_DIR2, MA_PATH_SEPARATOR, TEST_DATA, MA_PATH_SEPARATOR, TEST_FILE_NAME);

	TEST_ASSERT( 0 != (size1 = ma_fs_get_file_size(uv_loop, source)));

	TEST_ASSERT( MA_FALSE == ma_fs_move_file(NULL, source, destination));
	TEST_ASSERT( MA_TRUE == ma_fs_is_file_exists(uv_loop, source));
	TEST_ASSERT( MA_FALSE == ma_fs_is_file_exists(uv_loop, destination));

	TEST_ASSERT( MA_FALSE == ma_fs_move_file(uv_loop, NULL, destination));
	TEST_ASSERT( MA_TRUE == ma_fs_is_file_exists(uv_loop, source));
	TEST_ASSERT( MA_FALSE == ma_fs_is_file_exists(uv_loop, destination));

	TEST_ASSERT( MA_FALSE == ma_fs_move_file(uv_loop, source, NULL));
	TEST_ASSERT( MA_TRUE == ma_fs_is_file_exists(uv_loop, source));
	TEST_ASSERT( MA_FALSE == ma_fs_is_file_exists(uv_loop, destination));

	TEST_ASSERT( MA_TRUE == ma_fs_move_file(uv_loop, source, destination));
	TEST_ASSERT( MA_FALSE == ma_fs_is_file_exists(uv_loop, source));
	TEST_ASSERT( MA_TRUE == ma_fs_is_file_exists(uv_loop, destination));
	TEST_ASSERT( size1 == ma_fs_get_file_size(uv_loop, destination));

	TEST_ASSERT( MA_FALSE == ma_fs_remove_file(NULL, destination));
	TEST_ASSERT( MA_TRUE == ma_fs_is_file_exists(uv_loop, destination));

	TEST_ASSERT( MA_FALSE == ma_fs_remove_file(uv_loop, NULL));
	TEST_ASSERT( MA_TRUE == ma_fs_is_file_exists(uv_loop, destination));

	TEST_ASSERT( MA_TRUE == ma_fs_remove_file(uv_loop, destination));
	TEST_ASSERT( MA_FALSE == ma_fs_is_file_exists(uv_loop, destination));

	// Try removing a file that doesn't exist
	TEST_ASSERT( MA_TRUE == ma_fs_remove_file(uv_loop, source));

	MA_MSC_SELECT(_snprintf,snprintf)(source, MA_MAX_PATH_LEN, "%s%c%s%c%s", TEST_AGENT_ROOT, MA_PATH_SEPARATOR, TEST_DATA_DIR_EMPTY, MA_PATH_SEPARATOR, TEST_FILE_NAME_EMPTY);
	MA_MSC_SELECT(_snprintf,snprintf)(destination, MA_MAX_PATH_LEN, "%s%c%s%c%s%c%s", TEST_AGENT_ROOT, MA_PATH_SEPARATOR, TEST_DIR2, MA_PATH_SEPARATOR, TEST_DATA, MA_PATH_SEPARATOR, TEST_FILE_NAME_EMPTY);
	TEST_ASSERT( 0 == (size1 = ma_fs_get_file_size(uv_loop, source)));
	TEST_ASSERT( MA_TRUE == ma_fs_move_file(uv_loop, source, destination));
	TEST_ASSERT( MA_FALSE == ma_fs_is_file_exists(uv_loop, source));
	TEST_ASSERT( MA_TRUE == ma_fs_is_file_exists(uv_loop, destination));
	TEST_ASSERT( size1 == ma_fs_get_file_size(uv_loop, destination));

	TEST_ASSERT( MA_TRUE == ma_fs_remove_file(uv_loop, destination));
	TEST_ASSERT( MA_FALSE == ma_fs_is_file_exists(uv_loop, destination));

	MA_MSC_SELECT(_snprintf,snprintf)(source, MA_MAX_PATH_LEN, "%s%c%s%c%s", TEST_AGENT_ROOT, MA_PATH_SEPARATOR, TEST_DATA_DIR_LONG, MA_PATH_SEPARATOR, TEST_FILE_NAME_DIR_LONG);
	MA_MSC_SELECT(_snprintf,snprintf)(destination, MA_MAX_PATH_LEN, "%s%c%s%c%s%c%s", TEST_AGENT_ROOT, MA_PATH_SEPARATOR, TEST_DIR2, MA_PATH_SEPARATOR, TEST_DATA, MA_PATH_SEPARATOR, TEST_FILE_NAME_DIR_LONG);
	size1 = ma_fs_get_file_size(uv_loop, source);
	TEST_ASSERT(0 != size1);
	TEST_ASSERT( MA_TRUE == ma_fs_move_file(uv_loop, source, destination));
	TEST_ASSERT( MA_FALSE == ma_fs_is_file_exists(uv_loop, source));
	TEST_ASSERT( MA_TRUE == ma_fs_is_file_exists(uv_loop, destination));
	TEST_ASSERT( size1 == ma_fs_get_file_size(uv_loop, destination));

	TEST_ASSERT( MA_TRUE == ma_fs_remove_file(uv_loop, destination));
	TEST_ASSERT( MA_FALSE == ma_fs_is_file_exists(uv_loop, destination));

	MA_MSC_SELECT(_snprintf,snprintf)(source, MA_MAX_PATH_LEN, "%s%c%s%c%s", TEST_AGENT_ROOT, MA_PATH_SEPARATOR, TEST_DATA_DIR_LONG_FILE, MA_PATH_SEPARATOR, TEST_FILE_NAME_LONG);
	MA_MSC_SELECT(_snprintf,snprintf)(destination, MA_MAX_PATH_LEN, "%s%c%s%c%s%c%s", TEST_AGENT_ROOT, MA_PATH_SEPARATOR, TEST_DIR2, MA_PATH_SEPARATOR, TEST_DATA, MA_PATH_SEPARATOR, TEST_FILE_NAME_LONG);
	size1 = ma_fs_get_file_size(uv_loop, source);
	TEST_ASSERT(0 != size1);
	TEST_ASSERT( MA_TRUE == ma_fs_move_file(uv_loop, source, destination));
	TEST_ASSERT( MA_FALSE == ma_fs_is_file_exists(uv_loop, source));
	TEST_ASSERT( MA_TRUE == ma_fs_is_file_exists(uv_loop, destination));
	TEST_ASSERT( size1 == ma_fs_get_file_size(uv_loop, destination));

	TEST_ASSERT( MA_TRUE == ma_fs_remove_file(uv_loop, destination));
	TEST_ASSERT( MA_FALSE == ma_fs_is_file_exists(uv_loop, destination));

	MA_MSC_SELECT(_snprintf,snprintf)(source, MA_MAX_PATH_LEN, "%s%c%s%c%s", TEST_AGENT_ROOT, MA_PATH_SEPARATOR, TEST_DATA_DIR_NUMERIC, MA_PATH_SEPARATOR, TEST_FILE_NAME_NUMERIC);
	MA_MSC_SELECT(_snprintf,snprintf)(destination, MA_MAX_PATH_LEN, "%s%c%s%c%s%c%s", TEST_AGENT_ROOT, MA_PATH_SEPARATOR, TEST_DIR2, MA_PATH_SEPARATOR, TEST_DATA, MA_PATH_SEPARATOR, TEST_FILE_NAME_NUMERIC);
	size1 = ma_fs_get_file_size(uv_loop, source);
	TEST_ASSERT(0 != size1);
	TEST_ASSERT( MA_TRUE == ma_fs_move_file(uv_loop, source, destination));
	TEST_ASSERT( MA_FALSE == ma_fs_is_file_exists(uv_loop, source));
	TEST_ASSERT( MA_TRUE == ma_fs_is_file_exists(uv_loop, destination));
	TEST_ASSERT( size1 == ma_fs_get_file_size(uv_loop, destination));

	TEST_ASSERT( MA_TRUE == ma_fs_remove_file(uv_loop, destination));
	TEST_ASSERT( MA_FALSE == ma_fs_is_file_exists(uv_loop, destination));

	MA_MSC_SELECT(_snprintf,snprintf)(source, MA_MAX_PATH_LEN, "%s%c%s%c%s", TEST_AGENT_ROOT, MA_PATH_SEPARATOR, TEST_DATA_DIR_SPECIAL_CHAR, MA_PATH_SEPARATOR, TEST_FILE_NAME_SPECIAL_CHAR);
	MA_MSC_SELECT(_snprintf,snprintf)(destination, MA_MAX_PATH_LEN, "%s%c%s%c%s%c%s", TEST_AGENT_ROOT, MA_PATH_SEPARATOR, TEST_DIR2, MA_PATH_SEPARATOR, TEST_DATA, MA_PATH_SEPARATOR, TEST_FILE_NAME_SPECIAL_CHAR);
	TEST_ASSERT( 0 != (size1 = ma_fs_get_file_size(uv_loop, source)));
	TEST_ASSERT( MA_TRUE == ma_fs_move_file(uv_loop, source, destination));
	TEST_ASSERT( MA_FALSE == ma_fs_is_file_exists(uv_loop, source));
	TEST_ASSERT( MA_TRUE == ma_fs_is_file_exists(uv_loop, destination));
	TEST_ASSERT( size1 == ma_fs_get_file_size(uv_loop, destination));

	TEST_ASSERT( MA_TRUE == ma_fs_remove_file(uv_loop, destination));
	TEST_ASSERT( MA_FALSE == ma_fs_is_file_exists(uv_loop, destination));
}