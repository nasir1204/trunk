#include "unity.h"
#include "unity_fixture.h"
#include "ma/internal/utils/datastructures/ma_bytebuffer.h"
#include <string.h>
#include <stdio.h>

TEST_GROUP_RUNNER(ma_bytbuffer_test_group)
{
    do {RUN_TEST_CASE(ma_bytbuffer_tests, ma_bytebuffer_create_test)} while (0);
    do {RUN_TEST_CASE(ma_bytbuffer_tests, ma_bytebuffer_append_bytes_test)} while (0);
    do {RUN_TEST_CASE(ma_bytbuffer_tests, ma_bytebuffer_resize_test)} while (0);
    do {RUN_TEST_CASE(ma_bytbuffer_tests, ma_bytebuffer_set_bytes_test)} while (0);
}

TEST_SETUP(ma_bytbuffer_tests) {}
TEST_TEAR_DOWN(ma_bytbuffer_tests) {}


TEST(ma_bytbuffer_tests, ma_bytebuffer_create_test)
{
    unsigned char *buffer=(unsigned char*)"ma_bytebuffer_create_test";
    ma_bytebuffer_t *tmp=NULL;
    ma_error_t err=MA_OK;
    unsigned char *buffer_back=NULL;
    size_t len=0;
    size_t size=0;

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_bytebuffer_create(strlen((char*)buffer),NULL));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_bytebuffer_create(0,&tmp));
    err=ma_bytebuffer_create(strlen((char*)buffer),&tmp);
    TEST_ASSERT_MESSAGE( MA_OK == err && NULL != tmp, "failed to create a ma_bytebuffer_t");

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_bytebuffer_append_bytes(NULL,buffer,strlen( (char*) buffer)));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_bytebuffer_append_bytes(tmp,NULL,strlen( (char*) buffer)));
    err=ma_bytebuffer_append_bytes(tmp,buffer,strlen( (char*) buffer));
    TEST_ASSERT_MESSAGE( MA_OK == err, "failed to create a ma_bytebuffer_t");

    TEST_ASSERT(-1 == ma_bytebuffer_get_size(NULL));
    len=ma_bytebuffer_get_size(tmp);
    TEST_ASSERT_MESSAGE(MA_OK == err && strlen((char*)buffer) == len, "failed to get length");

    size=ma_bytebuffer_get_size(tmp);
    TEST_ASSERT_MESSAGE(MA_OK == err && strlen((char*)buffer) == size, "failed to get size");

    buffer_back=(unsigned char*)ma_bytebuffer_get_bytes(tmp);
    TEST_ASSERT_MESSAGE(MA_OK == err && 0 == memcmp(buffer_back,buffer,strlen((char*)buffer)), "failed to get buffer back.");

    // Increase one reference count
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_bytebuffer_add_ref(NULL));
    TEST_ASSERT(MA_OK == ma_bytebuffer_add_ref(tmp));

    // Decreasing one reference count
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_bytebuffer_release(NULL));
    err=ma_bytebuffer_release(tmp);
    TEST_ASSERT_MESSAGE(MA_OK == err, "failed to release bytebuffer.");

    // Actually releasing buffer
    TEST_ASSERT(MA_OK == ma_bytebuffer_release(tmp));
}

TEST(ma_bytbuffer_tests, ma_bytebuffer_append_bytes_test)
{
    {
        unsigned char *buffer=(unsigned char*)"0123456789";
        ma_bytebuffer_t *tmp=NULL;
        ma_error_t err=MA_OK;
        unsigned char *buffer_back=NULL;
        size_t size=0;
        size_t capacity=0;
        int ok=-1;
        err=ma_bytebuffer_create(10,&tmp);
        TEST_ASSERT_MESSAGE( MA_OK == err && NULL != tmp, "failed to create a ma_bytebuffer_t");

        size=ma_bytebuffer_get_size(tmp);
        TEST_ASSERT_MESSAGE(0 == size, "failed to get length");

        err=ma_bytebuffer_append_bytes(tmp,buffer,strlen((char*)buffer));
        TEST_ASSERT_MESSAGE(MA_OK == err, "failed to append");

        size=ma_bytebuffer_get_size(tmp);
        TEST_ASSERT_MESSAGE(10 == size, "failed to get length");

        TEST_ASSERT(-1 == ma_bytebuffer_get_capacity(NULL));
        capacity=ma_bytebuffer_get_capacity(tmp);
        TEST_ASSERT_MESSAGE(10 == capacity, "failed to get size");

        buffer_back=(unsigned char*)ma_bytebuffer_get_bytes(tmp);
        ok=memcmp(buffer_back,buffer,10);
        TEST_ASSERT_MESSAGE(MA_OK == err && 0 == ok, "failed to get buffer back.");

        err=ma_bytebuffer_release(tmp);
        TEST_ASSERT_MESSAGE(MA_OK == err, "failed to release bytebuffer.");
    }
    {
        unsigned char *buffer=(unsigned char*)"0123456789";
        unsigned char *buffer1=(unsigned char*)"012345";
        unsigned char *buffer3=(unsigned char*)"0123";
        ma_bytebuffer_t *tmp=NULL;
        ma_error_t err=MA_OK;
        size_t size=0;
        size_t capacity=0;
        err=ma_bytebuffer_create(15,&tmp);
        TEST_ASSERT_MESSAGE( MA_OK == err && NULL != tmp, "failed to create a ma_bytebuffer_t");

        size=ma_bytebuffer_get_size(tmp);
        TEST_ASSERT_MESSAGE(MA_OK == err && 0 == size, "failed to get length");

        capacity=ma_bytebuffer_get_capacity(tmp);
        TEST_ASSERT_MESSAGE( 15 == capacity, "failed to get size");

        err=ma_bytebuffer_append_bytes(tmp,buffer,strlen((char*)buffer));
        TEST_ASSERT_MESSAGE(MA_OK == err, "failed to  append");

        size=ma_bytebuffer_get_size(tmp);
        TEST_ASSERT_MESSAGE(10 == size, "invalid length");

        err=ma_bytebuffer_append_bytes(tmp,buffer1,strlen((char*)buffer1) );
        TEST_ASSERT_MESSAGE(MA_ERROR_INSUFFICIENT_SIZE == err, "Invalid append, as no space");

        err=ma_bytebuffer_append_bytes(tmp,buffer3,strlen((char*)buffer3) );
        TEST_ASSERT_MESSAGE(MA_OK == err, "Append failed");

        size=ma_bytebuffer_get_size(tmp);
        TEST_ASSERT_MESSAGE( 14 == size, "Invalid length.");

        capacity=ma_bytebuffer_get_capacity(tmp);
        TEST_ASSERT_MESSAGE(15 == capacity, "Invalid size.");

        err=ma_bytebuffer_release(tmp);
        TEST_ASSERT_MESSAGE(MA_OK == err, "failed to release bytebuffer.");
    }
    {
        unsigned char *buffer=(unsigned char*)"0123456789";
        unsigned char *buffer1=(unsigned char*)"012345";
        unsigned char *buffer3=(unsigned char*)"01234";
        ma_bytebuffer_t *tmp=NULL;
        ma_error_t err=MA_OK;
        size_t size=0;
        size_t capacity=0;
        err=ma_bytebuffer_create(15,&tmp);
        TEST_ASSERT_MESSAGE( MA_OK == err && NULL != tmp, "failed to create a ma_bytebuffer_t");

        size=ma_bytebuffer_get_size(tmp);
        TEST_ASSERT_MESSAGE(0 == size, "failed to get length");

        capacity=ma_bytebuffer_get_capacity(tmp);
        TEST_ASSERT_MESSAGE(15 == capacity, "failed to get size");

        err=ma_bytebuffer_append_bytes(tmp,buffer,strlen((char*)buffer));
        TEST_ASSERT_MESSAGE(MA_OK == err, "failed to  append");

        size=ma_bytebuffer_get_size(tmp);
        TEST_ASSERT_MESSAGE(10 == size, "invalid length");

        err=ma_bytebuffer_append_bytes(tmp,buffer1,strlen((char*)buffer1) );
        TEST_ASSERT_MESSAGE(MA_ERROR_INSUFFICIENT_SIZE == err, "Invalid append, as no space");

        err=ma_bytebuffer_append_bytes(tmp,buffer3,strlen((char*)buffer3) );
        TEST_ASSERT_MESSAGE(MA_OK == err, "Append failed");

        size=ma_bytebuffer_get_size(tmp);
        TEST_ASSERT_MESSAGE(MA_OK == err && 15 == size, "Invalid length.");

        capacity=ma_bytebuffer_get_capacity(tmp);
        TEST_ASSERT_MESSAGE(15 == capacity, "Invalid size.");

        err=ma_bytebuffer_release(tmp);
        TEST_ASSERT_MESSAGE(MA_OK == err, "failed to release bytebuffer.");
    }
}

TEST(ma_bytbuffer_tests, ma_bytebuffer_resize_test)
{
    {
        unsigned char *buffer_buffer1=(unsigned char*)"0123456789012345";
        unsigned char *buffer_buffer1_buffer3=(unsigned char*)"012345678901234501234";
        unsigned char *buffer=(unsigned char*)"0123456789";
        unsigned char *buffer1=(unsigned char*)"012345";
        unsigned char *buffer3=(unsigned char*)"01234";
        ma_bytebuffer_t *tmp=NULL;
        ma_error_t err=MA_OK;
        unsigned char *buffer_back=NULL;
        size_t size=0;
        size_t capacity=0;
        err=ma_bytebuffer_create(15,&tmp);
        TEST_ASSERT_MESSAGE( MA_OK == err && NULL != tmp, "failed to create a ma_bytebuffer_t");

        size=ma_bytebuffer_get_size(tmp);
        TEST_ASSERT_MESSAGE(0 == size, "invalid length");

        capacity=ma_bytebuffer_get_capacity(tmp);
        TEST_ASSERT_MESSAGE(15 == capacity, "invlid capacity");

        err=ma_bytebuffer_append_bytes(tmp,buffer,strlen((char*)buffer));
        TEST_ASSERT_MESSAGE(MA_OK == err, "failed to  append");

        size=ma_bytebuffer_get_size(tmp);
        TEST_ASSERT_MESSAGE(10 == size, "invalid size");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == ma_bytebuffer_reserve(NULL, 100), "ma_bytebuffer_reserve failed");
        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == ma_bytebuffer_reserve(tmp, 0), "ma_bytebuffer_reserve failed");
        TEST_ASSERT_MESSAGE(MA_ERROR_INSUFFICIENT_SIZE == ma_bytebuffer_reserve(tmp,size-1), "reserve passed,invalid capacity");

        err=ma_bytebuffer_reserve(tmp,size);
        TEST_ASSERT_MESSAGE(MA_OK == err, "reserve failed, valid capacity");

        size=ma_bytebuffer_get_size(tmp);
        TEST_ASSERT_MESSAGE(10 == size, "length lost.");

        capacity=ma_bytebuffer_get_capacity(tmp);
        TEST_ASSERT_MESSAGE( 10 == capacity, "Invalid capacity.");

        buffer_back=(unsigned char*)ma_bytebuffer_get_bytes(tmp);
        TEST_ASSERT_MESSAGE(MA_OK == err && 0 == memcmp(buffer_back,buffer,10), "Invalid buffer back.");

        //resize it to 10+6
        err=ma_bytebuffer_reserve(tmp,strlen((char*)buffer_buffer1));
        TEST_ASSERT_MESSAGE(MA_OK == err, "reserve failed");

        size=ma_bytebuffer_get_size(tmp);
        TEST_ASSERT_MESSAGE(strlen((char*)buffer) == size, "Invalid size.");

        capacity=ma_bytebuffer_get_capacity(tmp);
        TEST_ASSERT_MESSAGE(strlen((char*)buffer_buffer1) == capacity, "Invalid size.");

        err=ma_bytebuffer_append_bytes(tmp,buffer1,strlen((char*)buffer1));
        TEST_ASSERT_MESSAGE(MA_OK == err, "failed to  append");

        buffer_back=(unsigned char*)ma_bytebuffer_get_bytes(tmp);
        TEST_ASSERT_MESSAGE(MA_OK == err && 0 == memcmp(buffer_back,buffer_buffer1,strlen((char*)buffer_buffer1)), "invalid buffer back.");

        //resize to append buffer3
        err=ma_bytebuffer_reserve(tmp,strlen((char*)buffer_buffer1_buffer3));
        TEST_ASSERT_MESSAGE(MA_OK == err, "reserve failed");

        size=ma_bytebuffer_get_size(tmp);
        TEST_ASSERT_MESSAGE(MA_OK == err && strlen((char*)buffer_buffer1) == size, "Invalid size.");

        capacity=ma_bytebuffer_get_capacity(tmp);
        TEST_ASSERT_MESSAGE(strlen((char*)buffer_buffer1_buffer3) == capacity, "Invalid capacity.");

        err=ma_bytebuffer_append_bytes(tmp,buffer1,strlen((char*)buffer3));
        TEST_ASSERT_MESSAGE(MA_OK == err, "failed to append");

        buffer_back=(unsigned char*)ma_bytebuffer_get_bytes(tmp);
        TEST_ASSERT_MESSAGE(MA_OK == err && 0 == memcmp(buffer_back,buffer_buffer1_buffer3,strlen((char*)buffer_buffer1_buffer3)), "failed to get buffer back.");

        err=ma_bytebuffer_release(tmp);
        TEST_ASSERT_MESSAGE(MA_OK == err, "failed to release bytebuffer.");
    }
}

TEST(ma_bytbuffer_tests, ma_bytebuffer_set_bytes_test)
{
    {
        unsigned char *buffer_buffer1_at_start=(unsigned char*)"ABC345";
        unsigned char *buffer_buffer3_at_offset4=(unsigned char*)"ABC3EF";
        unsigned char *buffer=(unsigned char*)"012345";
        unsigned char *buffer1=(unsigned char*)"ABC";
        unsigned char *buffer3=(unsigned char*)"EF";
        ma_bytebuffer_t *tmp=NULL;
        ma_error_t err=MA_OK;
        unsigned char *buffer_back=NULL;

        err=ma_bytebuffer_create(strlen((char*)buffer),&tmp);
        TEST_ASSERT_MESSAGE( MA_OK == err && NULL != tmp, "failed to create a ma_bytebuffer_t");

        err=ma_bytebuffer_append_bytes(tmp,buffer,strlen((char*)buffer));
        TEST_ASSERT_MESSAGE(MA_OK == err, "failed to  append");

        TEST_ASSERT(NULL == ma_bytebuffer_get_bytes(NULL));
        buffer_back=(unsigned char*)ma_bytebuffer_get_bytes(tmp);
        TEST_ASSERT_MESSAGE(MA_OK == err && 0 == memcmp(buffer_back,buffer,strlen((char*)buffer)), "failed to get buffer back.");

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_bytebuffer_set_bytes(NULL,0,buffer1,strlen((char*)buffer1)));
        TEST_ASSERT(MA_ERROR_INSUFFICIENT_SIZE == ma_bytebuffer_set_bytes(tmp,1,buffer1,strlen((char*)buffer)));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_bytebuffer_set_bytes(tmp,1,NULL,strlen((char*)buffer1)));
        err=ma_bytebuffer_set_bytes(tmp,0,buffer1,strlen((char*)buffer1));

        buffer_back=(unsigned char*)ma_bytebuffer_get_bytes(tmp);
        TEST_ASSERT_MESSAGE(MA_OK == err && 0 == memcmp(buffer_back,buffer_buffer1_at_start,strlen((char*)buffer_buffer1_at_start)), "failed to get buffer back.");

        err=ma_bytebuffer_set_bytes(tmp,4,buffer3,strlen((char*)buffer3));

        buffer_back=(unsigned char*)ma_bytebuffer_get_bytes(tmp);
        TEST_ASSERT_MESSAGE(MA_OK == err && 0 == memcmp(buffer_back,buffer_buffer3_at_offset4,strlen((char*)buffer_buffer3_at_offset4)), "failed to get buffer back.");

        err=ma_bytebuffer_release(tmp);
        TEST_ASSERT_MESSAGE(MA_OK == err, "failed to release bytebuffer.");
    }

    {
        unsigned char *buffer_buffer3_at_offset4=(unsigned char*)"0123ABCDE";
        unsigned char *buffer=(unsigned char*)"01234";
        unsigned char *buffer3=(unsigned char*)"ABCDE";
        ma_bytebuffer_t *tmp=NULL;
        ma_error_t err=MA_OK;
        unsigned char *buffer_back=NULL;
        size_t len=0;

        err=ma_bytebuffer_create(strlen((char*)buffer_buffer3_at_offset4),&tmp);
        TEST_ASSERT_MESSAGE( MA_OK == err && NULL != tmp, "failed to create a ma_bytebuffer_t");

        err=ma_bytebuffer_append_bytes(tmp,buffer,strlen((char*)buffer));
        TEST_ASSERT_MESSAGE(MA_OK == err, "failed to  append");

        len=ma_bytebuffer_get_size(tmp);
        TEST_ASSERT_MESSAGE(MA_OK == err && strlen((char*)buffer) == len, "Invalid length.");

        buffer_back=(unsigned char*)ma_bytebuffer_get_bytes(tmp);
        TEST_ASSERT_MESSAGE(MA_OK == err && 0 == memcmp(buffer_back,buffer,strlen((char*)buffer)), "failed to get buffer back.");

        err=ma_bytebuffer_set_bytes(tmp,4,buffer3,strlen((char*)buffer3));

        len=ma_bytebuffer_get_size(tmp);
        TEST_ASSERT_MESSAGE(MA_OK == err && strlen((char*)buffer_buffer3_at_offset4) == len, "Invalid length.");

        buffer_back=(unsigned char*)ma_bytebuffer_get_bytes(tmp);
        TEST_ASSERT_MESSAGE(MA_OK == err && 0 == memcmp(buffer_back,buffer_buffer3_at_offset4,strlen((char*)buffer_buffer3_at_offset4)), "failed to get buffer back.");

        err=ma_bytebuffer_release(tmp);
        TEST_ASSERT_MESSAGE(MA_OK == err, "failed to release bytebuffer.");
    }

    {
        unsigned char *buffer=(unsigned char*)"0123456789";
        ma_bytebuffer_t *my_buffer = NULL;
        TEST_ASSERT(MA_OK ==  ma_bytebuffer_create(20, &my_buffer));
        TEST_ASSERT(MA_OK == ma_bytebuffer_set_bytes(my_buffer, 0, buffer, strlen((char*)buffer)));
        TEST_ASSERT(0 == memcmp(buffer, ma_bytebuffer_get_bytes(my_buffer), ma_bytebuffer_get_size(my_buffer)));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_bytebuffer_shrink(NULL, strlen((char*)buffer) - 1));

        // More than capacity
        TEST_ASSERT(MA_ERROR_INSUFFICIENT_SIZE == ma_bytebuffer_shrink(my_buffer, 21));

        // Shrinking by 2 bytes
        TEST_ASSERT(MA_OK == ma_bytebuffer_shrink(my_buffer, strlen((char*)buffer) - 2));

        // Verifying size is reduced by 2 bytes
        TEST_ASSERT(ma_bytebuffer_get_size(my_buffer) == (strlen((char*)buffer) - 2));

        // Data in the reduced buffer is untampered
        TEST_ASSERT(0 == memcmp(buffer, ma_bytebuffer_get_bytes(my_buffer), ma_bytebuffer_get_size(my_buffer)));

        // Release
        TEST_ASSERT(MA_OK == ma_bytebuffer_release(my_buffer));
    }
}
