#include "unity.h"
#include "unity_fixture.h"
#include "ma/internal/utils/acl/ma_acl.h"
#include "ma/internal/ma_macros.h"

#ifdef MA_WINDOWS
#include <Windows.h>
ma_int64_t permissions = FILE_ALL_ACCESS;
ma_int64_t inhertiance = CONTAINER_INHERIT_ACE | OBJECT_INHERIT_ACE;
const char *sid_name = "macmnsvc";
#else
ma_int64_t permissions = 0755;
ma_int64_t inhertiance = 0;
const char *sid_name = "mfe";
#endif

TEST_GROUP_RUNNER(ma_acl_test_group)
{
	do {RUN_TEST_CASE(ma_acl_utils_test, test_acl)} while (0);
}

TEST_SETUP(ma_acl_utils_test)
{
}

TEST_TEAR_DOWN(ma_acl_utils_test)
{
}

TEST(ma_acl_utils_test, test_acl)
{
    ma_acl_t *acl = NULL;
    /*TEST_ASSERT(MA_OK == ma_acl_create(MA_SID_LOCAL, "macmnsvc", &acl));
    TEST_ASSERT(MA_OK == ma_acl_add(acl, MA_TRUE, permissions,inhertiance));
    TEST_ASSERT(MA_OK == ma_acl_release(acl));
    */
}
