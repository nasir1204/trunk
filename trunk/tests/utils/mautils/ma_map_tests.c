/******************************************************************************
MAP unit tests
******************************************************************************/
#define UNITY_FIXTURE_MALLOC_OVERRIDES_H_

#include "unity.h"
#include "unity_fixture.h"
#include "unity_internals.h"

#include "ma/ma_common.h"
#include "ma/internal/utils/datastructures/ma_map.h"

#include <stdlib.h>
#include <string.h>

TEST_GROUP_RUNNER(ma_map_test_group)
{
    do {RUN_TEST_CASE(ma_map_tests, no_value_constructor_destructor) } while (0);
    do {RUN_TEST_CASE(ma_map_tests, explicit_constructor_destructor) } while (0);
}

TEST_SETUP(ma_map_tests)
{
}


TEST_TEAR_DOWN(ma_map_tests)
{
}

// Container to be stored in the map.
typedef struct
{
    int value1;
    char *value2;
} my_type;

// Test values mapping to container.
char *keys[] = { "first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth" };
my_type values[] = {
                        { 100, "hello world" },
                        { 200, "hello world" },
                        { -5, "rgljr;lg" },
                        { 2345, "r;gjr;lgjrtl;gkjmrtglkrtjgltrg" },
                        { -1000, "#$%$#%$^%$^%$^%^6" },
                        { 0, "34547984754905740" },
                        { 145, "sadmwnkd f.,erhrgnglkrjgnvgernkltgrjkvlm,.snmA?eliujefnfnfv.,sel;rhn v,.gnrgmerlg vm;elrmr" },
                        { 13456, "2333334843834348843-02383428234023472398472396392-47-" },
                        { 50000, "" },
                        { -3333, "                     " },
                  };


MA_MAP_DECLARE(my_map_1, char *, my_type);
MA_MAP_DEFINE(my_map_1, char *, strdup, free, my_type, , , strcmp);

// Test for verifying the Map functionality when no explicit constructor
// or destructor is provided.
TEST(ma_map_tests, no_value_constructor_destructor)
{
    int counter = 0;
    size_t size_of_map = -1;
    MA_MAP_T(my_map_1) *map_ptr = NULL;

    MA_MAP_CREATE(my_map_1, map_ptr);

    // Initial map size should be 0
    MA_MAP_SIZE(my_map_1, map_ptr, size_of_map);
    //printf("size_of_map - %d\n", size_of_map);
    TEST_ASSERT(0 == size_of_map);

    // Adding elements into the map one by one. The number of
    // elements in the map should increase by 1 in each iteration.
    for(counter = 0; counter < sizeof(keys)/sizeof(keys[0]); counter++)
    {
        my_type return_value = { 5, "erlejh;" };

        MA_MAP_ADD_ENTRY(my_map_1, map_ptr, keys[counter], values[counter]);

        MA_MAP_SIZE(my_map_1, map_ptr, size_of_map);
        //printf("size_of_map - %d\n", size_of_map);
        TEST_ASSERT((counter + 1) == size_of_map);

        MA_MAP_GET_VALUE(my_map_1, map_ptr, keys[counter], return_value);
        TEST_ASSERT(values[counter].value1 == return_value.value1);
        TEST_ASSERT(0 == strcmp(values[counter].value2, return_value.value2));
    }

    // Removing elements from the map one by one. The number of
    // elements in the map should reduce by 1 in each iteration.
    for(counter = sizeof(keys)/sizeof(keys[0]) - 1; counter >= 0; counter--)
    {
        MA_MAP_REMOVE_ENTRY(my_map_1, map_ptr, keys[counter]);
        MA_MAP_SIZE(my_map_1, map_ptr, size_of_map);
        //printf("size_of_map - %d\n", size_of_map);
        TEST_ASSERT(counter == size_of_map);

        // Trying to remove a non-existing element from the map.
        // The size of the map is not reduced.
        MA_MAP_REMOVE_ENTRY(my_map_1, map_ptr, keys[counter]);
        MA_MAP_SIZE(my_map_1, map_ptr, size_of_map);
        TEST_ASSERT(counter == size_of_map);
    }

    // Verifying all elements are removed
    MA_MAP_SIZE(my_map_1, map_ptr, size_of_map);
    TEST_ASSERT(0 == size_of_map);

    // Again adding elements into the map
    for(counter = 0; counter < sizeof(keys)/sizeof(keys[0]); counter++)
    {
        MA_MAP_ADD_ENTRY(my_map_1, map_ptr, keys[counter], values[counter]);
    }

    // Clearing map
    MA_MAP_CLEAR(my_map_1, map_ptr);

    // Verifying map size is zero
    MA_MAP_SIZE(my_map_1, map_ptr, size_of_map);
    TEST_ASSERT(0 == size_of_map);

    MA_MAP_RELEASE(my_map_1, map_ptr);
}

// User defined copy constructor
my_type *my_copy_constructor_func(my_type *current_my_type_ptr)
{
    my_type *my_type_ptr = NULL;
    my_type_ptr = (my_type *)calloc(sizeof(my_type), 1);
    my_type_ptr->value1 = current_my_type_ptr->value1;
    my_type_ptr->value2 = strdup(current_my_type_ptr->value2);
    return my_type_ptr;
}

// User defined destructor for de-allocating memory
void my_destructor_func(my_type *my_type_ptr)
{
    if(my_type_ptr)
    {
        free(my_type_ptr->value2);
        free(my_type_ptr);
    }
}

MA_MAP_DECLARE(my_map_2, char *, my_type *);
MA_MAP_DEFINE(my_map_2, char *, strdup, free, my_type *, my_copy_constructor_func, my_destructor_func, strcmp);

// Test for verifying the Map functionality when a user defined constructor
// and destructor is provided.
TEST(ma_map_tests, explicit_constructor_destructor)
{
    int counter = 0;
    size_t size_of_map = -1;
    MA_MAP_T(my_map_2) *map_ptr = NULL;

    MA_MAP_CREATE(my_map_2, map_ptr);

    // Initial map size should be 0
    MA_MAP_SIZE(my_map_2, map_ptr, size_of_map);
    //printf("size_of_map - %d\n", size_of_map);
    TEST_ASSERT(0 == size_of_map);

    // Adding elements into the map one by one. The number of
    // elements in the map should increase by 1 in each iteration.
    for(counter = 0; counter < sizeof(keys)/sizeof(keys[0]); counter++)
    {
        my_type *return_value = NULL;

        MA_MAP_ADD_ENTRY(my_map_2, map_ptr, keys[counter], &values[counter]);

        MA_MAP_SIZE(my_map_2, map_ptr, size_of_map);
        //printf("size_of_map - %d\n", size_of_map);
        TEST_ASSERT((counter + 1) == size_of_map);

        MA_MAP_GET_VALUE(my_map_2, map_ptr, keys[counter], return_value);

        TEST_ASSERT(values[counter].value1 == return_value->value1);
        TEST_ASSERT(0 == strcmp(values[counter].value2, return_value->value2));
    }

    // Removing elements from the map one by one. The number of
    // elements in the map should reduce by 1 in each iteration.
    for(counter = sizeof(keys)/sizeof(keys[0]) - 1; counter >= 0; counter--)
    {
        MA_MAP_REMOVE_ENTRY(my_map_2, map_ptr, keys[counter]);
        MA_MAP_SIZE(my_map_2, map_ptr, size_of_map);
        //printf("size_of_map - %d\n", size_of_map);
        TEST_ASSERT(counter == size_of_map);

        // Trying to remove a non-existing element from the map.
        // The size of the map is not reduced.
        MA_MAP_REMOVE_ENTRY(my_map_2, map_ptr, keys[counter]);
        MA_MAP_SIZE(my_map_2, map_ptr, size_of_map);
        TEST_ASSERT(counter == size_of_map);
    }

    // Verifying all elements are removed
    MA_MAP_SIZE(my_map_2, map_ptr, size_of_map);
    TEST_ASSERT(0 == size_of_map);

    // Again adding elements into the map
    for(counter = 0; counter < sizeof(keys)/sizeof(keys[0]); counter++)
    {
        MA_MAP_ADD_ENTRY(my_map_2, map_ptr, keys[counter], &values[counter]);
    }

    // Clearing map
    MA_MAP_CLEAR(my_map_2, map_ptr);

    // Verifying map size is zero
    MA_MAP_SIZE(my_map_2, map_ptr, size_of_map);
    TEST_ASSERT(0 == size_of_map);

    MA_MAP_RELEASE(my_map_2, map_ptr);
}

