/*
Temp Buffer UTs
*/

#include "unity.h"
#include "unity_fixture.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include <stdio.h>
#include <string.h>

TEST_GROUP_RUNNER(ma_temp_buffer_test_group)
{
    do {RUN_TEST_CASE(ma_temp_buffer_tests, ma_temp_buffer_test_static_buffer)} while (0);
    do {RUN_TEST_CASE(ma_temp_buffer_tests, ma_temp_buffer_test_dynamic_buffer)} while (0);
}

TEST_SETUP(ma_temp_buffer_tests) {}

TEST_TEAR_DOWN(ma_temp_buffer_tests) {}

TEST(ma_temp_buffer_tests, ma_temp_buffer_test_static_buffer)
{
    ma_temp_buffer_t first_buffer = {0};
    ma_temp_buffer_t second_buffer = {0};

    // Initial buffer capacity is equal to static capacity
    ma_temp_buffer_init(&first_buffer);
    TEST_ASSERT(MA_TEMP_BUFFER_STATIC_CAPACITY == ma_temp_buffer_capacity(&first_buffer));

    // Writing into the buffer
    sprintf((char*)ma_temp_buffer_get(&first_buffer), "%s", "This is McAfee Agent 5.0 (Next Generation) !!!!!!");

    // Copying the source buffer into another buffer
    ma_temp_buffer_copy(&second_buffer, &first_buffer);

    // Un-init first buffer
    ma_temp_buffer_uninit(&first_buffer);

    // Retrieving String through second buffer
    TEST_ASSERT(0 == strcmp("This is McAfee Agent 5.0 (Next Generation) !!!!!!", (char*)ma_temp_buffer_get(&second_buffer)));

    // Un-init second buffer
    ma_temp_buffer_uninit(&second_buffer);
}

TEST(ma_temp_buffer_tests, ma_temp_buffer_test_dynamic_buffer)
{
    ma_temp_buffer_t first_buffer = {0};
    ma_temp_buffer_t second_buffer = {0};

    const char *buffer = "ld;fjndscndsofkehcptjhfndverpogthrevnlkfgklrsthnytrbfb,.bjfopgtyiputyoitujhrtbf.m,bf,.gbmfghljrthypoutgjeorgnmeg,.mdf,. \
                          vfmdbopgure0t89weurwqohnwqkdnwklcnoewkrh9wpruwrw890qudfwejfnlf,dgvgndfgoperth98ertuerogtjernglkerngerpogtijhreptu8ret8we90 \
                          tehtnkregnfertioperutoert908erutewtjerglerkgmeropgiuert08rtery98tgher9pthertoperhngerklgn";


    // Initial buffer capacity is equal to static capacity
    ma_temp_buffer_init(&first_buffer);
    TEST_ASSERT(MA_TEMP_BUFFER_STATIC_CAPACITY == ma_temp_buffer_capacity(&first_buffer));

    // Increasing the capacity of the buffer. Verifying that the capacity is increased
    ma_temp_buffer_reserve(&first_buffer, 500);
    TEST_ASSERT(500 == ma_temp_buffer_capacity(&first_buffer));

    // Writing into the buffer
    sprintf((char*)ma_temp_buffer_get(&first_buffer), buffer);

    // Copying the source buffer into another buffer
    ma_temp_buffer_copy(&second_buffer, &first_buffer);

    // Un-init first buffer
    ma_temp_buffer_uninit(&first_buffer);

    // Retrieving String through second buffer
    TEST_ASSERT(0 == strcmp(buffer, (char*)ma_temp_buffer_get(&second_buffer)));

    // Un-init second buffer
    ma_temp_buffer_uninit(&second_buffer);
}
