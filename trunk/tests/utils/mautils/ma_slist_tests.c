/******************************************************************************
S-List unit tests
******************************************************************************/
#define UNITY_FIXTURE_MALLOC_OVERRIDES_H_

#include "unity.h"
#include "unity_fixture.h"
#include "unity_internals.h"

#include "ma/ma_common.h"
#include "ma/internal/utils/datastructures/ma_slist.h"

#include <stdlib.h>

TEST_GROUP_RUNNER(ma_slist_test_group)
{
    do {RUN_TEST_CASE(ma_slist_tests, push_back_pop_front_test) } while (0);
    do {RUN_TEST_CASE(ma_slist_tests, push_front_pop_back_test) } while (0);
    do {RUN_TEST_CASE(ma_slist_tests, slist_clear_test) } while (0);
    do {RUN_TEST_CASE(ma_slist_tests, slist_remove_node_test) } while (0);
    do {RUN_TEST_CASE(ma_slist_tests, slist_get_next_get_prev_node_test) } while (0);
}

// Node structure
struct my_node
{
    int data;
    MA_SLIST_NODE_DEFINE(struct my_node);
};

// List structure
struct my_list
{
    MA_SLIST_DEFINE(p_list, struct my_node);
};

// Test vector
int test_vector[] = { 123, 234, -12, -45, 0, 4567, 123, 56, 11, 100 };

// List pointer
struct my_list *list_handle = NULL;

// Initializing the list
TEST_SETUP(ma_slist_tests)
{
    list_handle = (struct my_list *)calloc(1, sizeof(struct my_list));
    MA_SLIST_INIT(list_handle, p_list);
}


// Freeing the list
TEST_TEAR_DOWN(ma_slist_tests)
{
    free(list_handle);
}

// Test for verifying the S-List for Push Back and Pop Front functionality
TEST(ma_slist_tests, push_back_pop_front_test)
{
    int counter = 0;
    struct my_node *node_handle = NULL;

    // Verifying that the list is empty
    TEST_ASSERT(1 == MA_SLIST_EMPTY(list_handle, p_list));

    // Creating and adding nodes at the back of the list
    for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        node_handle = (struct my_node *)calloc(1, sizeof(struct my_node));
        node_handle->data = test_vector[counter];
        MA_SLIST_NODE_INIT(node_handle);

        // Add the node into the list
        MA_SLIST_PUSH_BACK(list_handle, p_list, node_handle);

        // Verifying that the list is not empty now
        TEST_ASSERT(0 == MA_SLIST_EMPTY(list_handle, p_list));

        // Verifying the count of number of elements
        TEST_ASSERT((counter + 1) == MA_SLIST_GET_COUNT(list_handle, p_list));
    }

    // 1) Popping the node from the front of the list and verifying the values against
    //    the expected test vector.
    // 2) Verifying that the size of slist is reduced by 1 in each iteration.
    // 3) De-allocating memory for the node
    for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        MA_SLIST_POP_FRONT(list_handle, p_list, node_handle);

        TEST_ASSERT(test_vector[counter] == node_handle->data);

        TEST_ASSERT((sizeof(test_vector)/sizeof(test_vector[0]) - counter - 1) == MA_SLIST_GET_COUNT(list_handle, p_list));

        free(node_handle);
    }
}


// Test for verifying the S-List for Push Front and Pop Back functionality
TEST(ma_slist_tests, push_front_pop_back_test)
{
    int counter = 0;
    struct my_node *node_handle = NULL;

    // Verifying that the list is empty
    TEST_ASSERT(1 == MA_SLIST_EMPTY(list_handle, p_list));

    // Creating and adding nodes at the front of the list
    for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        node_handle = (struct my_node *)calloc(1, sizeof(struct my_node));
        node_handle->data = test_vector[counter];
        MA_SLIST_NODE_INIT(node_handle);

        // Add the node into the list
        MA_SLIST_PUSH_FRONT(list_handle, p_list, node_handle);

        // Verifying that the list is not empty now
        TEST_ASSERT(0 == MA_SLIST_EMPTY(list_handle, p_list));

        // Verifying the count of number of elements
        TEST_ASSERT((counter + 1) == MA_SLIST_GET_COUNT(list_handle, p_list));
    }

    // 1) Popping the node from the back of the list and verifying the values against
    //    the expected test vector.
    // 2) Verifying that the size of slist is reduced by 1 in each iteration.
    // 3) De-allocating memory for the node
    for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        MA_SLIST_POP_BACK(list_handle, p_list, node_handle);

        TEST_ASSERT(test_vector[counter] == node_handle->data);

        TEST_ASSERT((sizeof(test_vector)/sizeof(test_vector[0]) - counter - 1) == MA_SLIST_GET_COUNT(list_handle, p_list));

        free(node_handle);
    }
}


void release_func(struct my_node *node_handle)
{
    free(node_handle);
}

// Test for verifying the S-List clear function removes all elements from the list
TEST(ma_slist_tests, slist_clear_test)
{
    int counter = 0;
    struct my_node *node_handle = NULL;

    // Verifying that the list is empty
    TEST_ASSERT(1 == MA_SLIST_EMPTY(list_handle, p_list));

    // Creating and adding nodes at the back of the list
    for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        node_handle = (struct my_node *)calloc(1, sizeof(struct my_node));
        node_handle->data = test_vector[counter];
        MA_SLIST_NODE_INIT(node_handle);

        // Add the node into the list
        MA_SLIST_PUSH_BACK(list_handle, p_list, node_handle);

        // Verifying that the list is not empty now
        TEST_ASSERT(0 == MA_SLIST_EMPTY(list_handle, p_list));

        // Verifying the count of number of elements
        TEST_ASSERT((counter + 1) == MA_SLIST_GET_COUNT(list_handle, p_list));
    }

    // Clearing the list
    MA_SLIST_CLEAR(list_handle, p_list, struct my_node, release_func);

    // Verifying the count of number of elements is zero
    TEST_ASSERT(0 == MA_SLIST_GET_COUNT(list_handle, p_list));

    // Verifying that the list is empty
    TEST_ASSERT(1 == MA_SLIST_EMPTY(list_handle, p_list));
}


// Test for verifying the S-List remove node function
TEST(ma_slist_tests, slist_remove_node_test)
{
    int counter = 0;
    struct my_node *node_handle = NULL;

    // Verifying that the list is empty
    TEST_ASSERT(1 == MA_SLIST_EMPTY(list_handle, p_list));

    // (1) Removing one by one from head side

    // Creating and adding nodes at the back of the list
    for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        node_handle = (struct my_node *)calloc(1, sizeof(struct my_node));
        node_handle->data = test_vector[counter];
        MA_SLIST_NODE_INIT(node_handle);

        // Add the node into the list
        MA_SLIST_PUSH_BACK(list_handle, p_list, node_handle);

        // Verifying that the list is not empty now
        TEST_ASSERT(0 == MA_SLIST_EMPTY(list_handle, p_list));

        // Verifying the count of number of elements
        TEST_ASSERT((counter + 1) == MA_SLIST_GET_COUNT(list_handle, p_list));
    }

    counter = sizeof(test_vector)/sizeof(test_vector[0]);

    // Removing the nodes one by one starting from the first node in the list.
    while((node_handle = MA_SLIST_GET_HEAD(list_handle, p_list)) != NULL)
    {
        MA_SLIST_REMOVE_NODE(list_handle, p_list, node_handle, struct my_node);
		--counter;
        TEST_ASSERT(counter == MA_SLIST_GET_COUNT(list_handle, p_list));
        free(node_handle);
    }

    // Verifying the count of number of elements is zero
    TEST_ASSERT(0 == MA_SLIST_GET_COUNT(list_handle, p_list));

    // Verifying that the list is empty
    TEST_ASSERT(1 == MA_SLIST_EMPTY(list_handle, p_list));

    // (2) Removing one by one from tail side

    // Creating and adding nodes at the back of the list
    for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        node_handle = (struct my_node *)calloc(1, sizeof(struct my_node));
        node_handle->data = test_vector[counter];
        MA_SLIST_NODE_INIT(node_handle);

        // Add the node into the list
        MA_SLIST_PUSH_BACK(list_handle, p_list, node_handle);

        // Verifying that the list is not empty now
        TEST_ASSERT(0 == MA_SLIST_EMPTY(list_handle, p_list));

        // Verifying the count of number of elements
        TEST_ASSERT((counter + 1) == MA_SLIST_GET_COUNT(list_handle, p_list));
    }

    counter = sizeof(test_vector)/sizeof(test_vector[0]);

    // Removing the nodes one by one starting from the last node in the list.
    while((node_handle = MA_SLIST_GET_TAIL(list_handle, p_list)) != NULL)
    {
        MA_SLIST_REMOVE_NODE(list_handle, p_list, node_handle, struct my_node);
        TEST_ASSERT(--counter == MA_SLIST_GET_COUNT(list_handle, p_list));
        free(node_handle);
    }

    // Verifying the count of number of elements is zero
    TEST_ASSERT(0 == MA_SLIST_GET_COUNT(list_handle, p_list));

    // Verifying that the list is empty
    TEST_ASSERT(1 == MA_SLIST_EMPTY(list_handle, p_list));
}


// Test for verifying the functionality of get next and get previous node APIs
TEST(ma_slist_tests, slist_get_next_get_prev_node_test)
{
    int counter = 0;
    struct my_node *node_handle = NULL;
    struct my_node *temp_node = NULL;

    // Verifying that the list is empty
    TEST_ASSERT(1 == MA_SLIST_EMPTY(list_handle, p_list));

    // Creating and adding nodes at the back of the list
    for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        node_handle = (struct my_node *)calloc(1, sizeof(struct my_node));
        node_handle->data = test_vector[counter];
        MA_SLIST_NODE_INIT(node_handle);

        // Add the node into the list
        MA_SLIST_PUSH_BACK(list_handle, p_list, node_handle);

        // Verifying that the list is not empty now
        TEST_ASSERT(0 == MA_SLIST_EMPTY(list_handle, p_list));

        // Verifying the count of number of elements
        TEST_ASSERT((counter + 1) == MA_SLIST_GET_COUNT(list_handle, p_list));
    }

    // 1) Iterate through the list. For each node, verify that the correct value of next node
    //    is returned. For last node, the next node value should be NULL.
    // 2) Iterate through the list. For each node, verify that the correct value of previous
    //    node is returned. For first node, the previous node value should be NULL.
    counter = 0;

    MA_SLIST_FOREACH(list_handle, p_list, node_handle)
    {
        temp_node = MA_SLIST_GET_NEXT_NODE(node_handle);

        if(counter == sizeof(test_vector)/sizeof(test_vector[0]) - 1) {    TEST_ASSERT_NULL(temp_node); }
        else { TEST_ASSERT(temp_node->data == test_vector[counter + 1]); }
        counter++;
    }

    counter = 0;

    MA_SLIST_FOREACH(list_handle, p_list, node_handle)
    {
        MA_SLIST_GET_PREV_NODE(list_handle, node_handle, temp_node, p_list);

        if(counter == 0) { TEST_ASSERT_NULL(temp_node); }
        else { TEST_ASSERT(temp_node->data == test_vector[counter - 1]); }
        counter++;
    }

    // Clearing the list
    MA_SLIST_CLEAR(list_handle, p_list, struct my_node, release_func);

    // Verifying the count of number of elements is zero
    TEST_ASSERT(0 == MA_SLIST_GET_COUNT(list_handle, p_list));

    // Verifying that the list is empty
    TEST_ASSERT(1 == MA_SLIST_EMPTY(list_handle, p_list));
}

