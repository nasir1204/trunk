/******************************************************************************
RegEx unit tests
******************************************************************************/
#include "unity.h"
#include "unity_fixture.h"
#include "ma/internal/utils/regex/ma_regex.h"

TEST_GROUP_RUNNER(ma_regex_test_group)
{
    do {RUN_TEST_CASE(ma_regex_tests, plaintext_tests) } while (0);
    do {RUN_TEST_CASE(ma_regex_tests, regex_test) } while (0);
}

static ma_regex_t *regex = NULL;

TEST_SETUP(ma_regex_tests) {}

TEST_TEAR_DOWN(ma_regex_tests) {}

TEST(ma_regex_tests, plaintext_tests)
{
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_regex_create(NULL, &regex));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_regex_create("ma_policy_timeout", NULL));
    TEST_ASSERT(MA_OK == ma_regex_create("ma_policy_timeout" , &regex));

    TEST_ASSERT(MA_FALSE == ma_regex_match(NULL, "ma_policy_timeout"));
    TEST_ASSERT(MA_FALSE == ma_regex_match(regex, NULL));
    TEST_ASSERT(MA_TRUE == ma_regex_match(regex, "ma_policy_timeout"));
    TEST_ASSERT(MA_FALSE == ma_regex_match(regex, "ma_policy_timeout_1"));
    TEST_ASSERT(MA_FALSE == ma_regex_match(regex, "ma.policy_timeout"));
    TEST_ASSERT(MA_FALSE == ma_regex_match(regex, "ma_policy.timeout"));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_regex_release(NULL));
    TEST_ASSERT(MA_OK == ma_regex_release(regex));

    TEST_ASSERT(MA_OK == ma_regex_create("ma.policy.timeout" , &regex));
    TEST_ASSERT(MA_TRUE == ma_regex_match(regex, "ma.policy.timeout"))
    TEST_ASSERT(MA_OK == ma_regex_release(regex));
}

TEST(ma_regex_tests, regex_test)
{
    TEST_ASSERT(MA_OK == ma_regex_create("ma.*", &regex));
    TEST_ASSERT(MA_TRUE == ma_regex_match(regex, "ma.policy"));
    TEST_ASSERT(MA_TRUE == ma_regex_match(regex, "ma.policy.timeout"));
    TEST_ASSERT(MA_TRUE == ma_regex_match(regex, "ma.policy.*"));
    TEST_ASSERT(MA_TRUE == ma_regex_match(regex, "ma.policy.my_policy.*"));
   // TEST_ASSERT(MA_FALSE == ma_regex_match(regex, "ma_policy"));
    TEST_ASSERT(MA_TRUE == ma_regex_match(regex, "ma.policy.my_policy"));
    TEST_ASSERT(MA_FALSE == ma_regex_match(regex , "ma"));
    TEST_ASSERT(MA_OK == ma_regex_release(regex));

    TEST_ASSERT(MA_OK == ma_regex_create("ma.policy.*", &regex));
    TEST_ASSERT(MA_TRUE == ma_regex_match(regex, "ma.policy.timeout"));
    TEST_ASSERT(MA_FALSE == ma_regex_match(regex, "ma.policy"));
    TEST_ASSERT(MA_TRUE == ma_regex_match(regex, "ma.policy.*"));
    TEST_ASSERT(MA_FALSE == ma_regex_match(regex, "ma.*"));
    TEST_ASSERT(MA_OK == ma_regex_release(regex));

    TEST_ASSERT(MA_OK == ma_regex_create("ma.service.policy.*", &regex));
    TEST_ASSERT(MA_FALSE == ma_regex_match(regex, "ma.service.policy"));
    TEST_ASSERT(MA_TRUE == ma_regex_match(regex, "ma.service.policy.policy1"));
    TEST_ASSERT(MA_FALSE == ma_regex_match(regex, "ma.service"));
    TEST_ASSERT(MA_OK == ma_regex_release(regex));
}

