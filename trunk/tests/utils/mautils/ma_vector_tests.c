#define UNITY_FIXTURE_MALLOC_OVERRIDES_H_

#include "unity.h"
#include "unity_fixture.h"
#include "unity_internals.h" 

#include "ma/ma_common.h"
#include "ma/internal/utils/datastructures/ma_vector.h"

#include <stdlib.h>

TEST_GROUP_RUNNER(ma_vector_test_group) {
    RUN_TEST_CASE(ma_vector_tests, basic_type_test)
	do {RUN_TEST_CASE(ma_vector_tests, user_defined_type_test)} while (0);   
}

TEST_SETUP(ma_vector_tests) {
}

TEST_TEAR_DOWN(ma_vector_tests) {
}

typedef struct int_arr_s {
    ma_vector_define(basic, int);
} int_arr_t;

static void int_release(int a) {

}

int int_compare(const void *a, const void *b) {
    return (*(int*)a - *(int*)b);
}

TEST(ma_vector_tests, basic_type_test) {
    int_arr_t head;
    int i = 0;

    /* vector creat and reserve */
    ma_vector_init(&head, 4, basic, int);
    ma_vector_reserve(&head, 4, basic, int);

    TEST_ASSERT_EQUAL_INT(1, ma_vector_empty(&head, basic));    
    TEST_ASSERT_EQUAL_INT(0, ma_vector_get_size(&head, basic));
    TEST_ASSERT_EQUAL_INT(8, ma_vector_get_capacity(&head, basic));

    /* Set data */
    for(i=4; i>=0;i--) {        
        ma_vector_push_back(&head, i, int, basic);
    }
    
    ma_vector_sort(&head, int, basic, int_compare);

    TEST_ASSERT_EQUAL_INT(0, ma_vector_empty(&head, basic));    
    TEST_ASSERT_EQUAL_INT(5, ma_vector_get_size(&head, basic));
    TEST_ASSERT_EQUAL_INT(8, ma_vector_get_capacity(&head, basic));

    /* Get data */
    for(i=0; i<5;i++) {        
        TEST_ASSERT_EQUAL_INT(i, ma_vector_at(&head, i, int, basic));
    }

    ma_vector_remove(&head, 2, int, basic, int_release);    
    ma_vector_sort(&head, int, basic, int_compare);

    TEST_ASSERT_EQUAL_INT(4, ma_vector_get_size(&head, basic));

    TEST_ASSERT_EQUAL_INT(0, ma_vector_at(&head, 0, int, basic));
    TEST_ASSERT_EQUAL_INT(1, ma_vector_at(&head, 1, int, basic));
    TEST_ASSERT_EQUAL_INT(3, ma_vector_at(&head, 2, int, basic));
    TEST_ASSERT_EQUAL_INT(4, ma_vector_at(&head, 3, int, basic));

    ma_vector_clear(&head, int, basic, int_release);    
}

typedef struct test_s {
    int i;
    char *s;
} test_t;

typedef struct test_array_s {
    ma_vector_define(myvec, test_t*);
} test_array_t;

static void test_create(test_t **t, int i) {
    *t = (test_t*)calloc(1, sizeof(test_t));
    (*t)->i = i;
    (*t)->s = (char*)calloc(5, sizeof(char));
}

static void test_release(test_t *t) {
    if(t->s) free(t->s);
    free(t);
}

int test_compare(const void *a, const void *b) {
    test_t **t1 = (test_t**)a;
    test_t **t2 = (test_t**)b;
    return ((*t1)->i - (*t2)->i);
}

TEST(ma_vector_tests, user_defined_type_test) {
    test_array_t    test_array;
    test_t *test[5];
    int i = 0;
    ma_vector_init(&test_array, 4, myvec, test_t*); 
        
    TEST_ASSERT_EQUAL_INT(1, ma_vector_empty(&test_array, myvec));
    TEST_ASSERT_EQUAL_INT(0, ma_vector_get_size(&test_array, myvec));
    TEST_ASSERT_EQUAL_INT(4, ma_vector_get_capacity(&test_array, myvec));
    
    i = ma_vector_get_capacity(&test_array,myvec);
    i = ma_vector_get_size(&test_array, myvec);

    test_create(&test[3], 3);
    ma_vector_push_back(&test_array, test[3], test_t*, myvec);

    test_create(&test[0], 0);
    ma_vector_push_back(&test_array, test[0], test_t*, myvec);

    test_create(&test[4], 4);
    ma_vector_push_back(&test_array, test[4], test_t*, myvec);

    test_create(&test[1], 1);
    ma_vector_push_back(&test_array, test[1], test_t*, myvec);

    test_create(&test[2], 2);
    ma_vector_push_back(&test_array, test[2], test_t*, myvec);
    
    ma_vector_sort(&test_array, test_t*, myvec, test_compare);

    TEST_ASSERT_EQUAL_INT(0, ma_vector_empty(&test_array, myvec));
    TEST_ASSERT_EQUAL_INT(5, ma_vector_get_size(&test_array, myvec));
    TEST_ASSERT_EQUAL_INT(8, ma_vector_get_capacity(&test_array, myvec));

    for(i=0; i<5;i++) {        
        TEST_ASSERT_EQUAL_INT(i, ma_vector_at(&test_array, i, test_t*, myvec)->i);
    }

    ma_vector_remove(&test_array, test[3], test_t*, myvec, test_release);    
    ma_vector_sort(&test_array, test_t*, myvec, test_compare);

    TEST_ASSERT_EQUAL_INT(4, ma_vector_get_size(&test_array, myvec));

    TEST_ASSERT_EQUAL_INT(0, ma_vector_at(&test_array, 0, test_t*, myvec)->i);
    TEST_ASSERT_EQUAL_INT(1, ma_vector_at(&test_array, 1, test_t*, myvec)->i);
    TEST_ASSERT_EQUAL_INT(2, ma_vector_at(&test_array, 2, test_t*, myvec)->i);
    TEST_ASSERT_EQUAL_INT(4, ma_vector_at(&test_array, 3, test_t*, myvec)->i);
        
    ma_vector_clear(&test_array, test_t*, myvec, test_release);

    TEST_ASSERT_EQUAL_INT(0, ma_vector_get_size(&test_array, myvec));
}