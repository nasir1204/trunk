/*
Byte stream Unit Tests
*/

#include "unity.h"
#include "unity_fixture.h"
#include "ma/internal/utils/datastructures/ma_bytebuffer.h"
#include "ma/internal/utils/datastructures/ma_stream.h"
#include "ma/internal/utils/datastructures/ma_bytestream.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#ifndef MA_WINDOWS
    #include <unistd.h>
#endif

struct test_data
{
    ma_uint16_t b;
    ma_uint32_t c;
    ma_uint64_t d;
    char buffer[11];
};

struct test_data1
{
    ma_uint16_t b;
    ma_uint32_t c;
    ma_uint64_t d;
    char buffer[21];
};


// No. of iterations for which test is to be executed.
ma_uint32_t num_iterations = 0;

TEST_GROUP_RUNNER(ma_bytestream_test_group)
{
    do {RUN_TEST_CASE(ma_bytestream_tests, ma_bytestream_create_test)} while (0);
    do {RUN_TEST_CASE(ma_bytestream_tests, ma_bytestream_get_buffer_test)} while (0);

    // For 10 records
    num_iterations = 10;

    do {RUN_TEST_CASE(ma_bytestream_tests, ma_bytestream_mstream_le_write_read_test)} while (0);
    do {RUN_TEST_CASE(ma_bytestream_tests, ma_bytestream_mstream_be_write_read_test)} while (0);
    do {RUN_TEST_CASE(ma_bytestream_tests, ma_bytestream_fstream_le_write_read_test)} while (0);
    do {RUN_TEST_CASE(ma_bytestream_tests, ma_bytestream_fstream_be_write_read_test)} while (0);

    // For 50 records
    num_iterations = 50;

    do {RUN_TEST_CASE(ma_bytestream_tests, ma_bytestream_mstream_le_write_read_test)} while (0);
    do {RUN_TEST_CASE(ma_bytestream_tests, ma_bytestream_mstream_be_write_read_test)} while (0);
    do {RUN_TEST_CASE(ma_bytestream_tests, ma_bytestream_fstream_le_write_read_test)} while (0);
    do {RUN_TEST_CASE(ma_bytestream_tests, ma_bytestream_fstream_be_write_read_test)} while (0);

    // For 100 records
    num_iterations = 100;

    do {RUN_TEST_CASE(ma_bytestream_tests, ma_bytestream_mstream_le_write_read_test)} while (0);
    do {RUN_TEST_CASE(ma_bytestream_tests, ma_bytestream_mstream_be_write_read_test)} while (0);
    do {RUN_TEST_CASE(ma_bytestream_tests, ma_bytestream_fstream_le_write_read_test)} while (0);
    do {RUN_TEST_CASE(ma_bytestream_tests, ma_bytestream_fstream_be_write_read_test)} while (0);
}

TEST_SETUP(ma_bytestream_tests)
{
    unlink("test.me");
}
TEST_TEAR_DOWN(ma_bytestream_tests)
{
    unlink("test.me");
}


// Dummy callback for Net stream
size_t ma_netstream_callback(const unsigned char *data, size_t size, void *userdata)
{
    // Assuming that the size bytes are written to the stream
    return size;
}

TEST(ma_bytestream_tests, ma_bytestream_create_test)
{
    // For memory stream
    {
        ma_stream_t *stream = NULL;
        ma_bytestream_t *bytestream = NULL;
        ma_uint8_t counter = 0;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_mstream_create(0, NULL, &stream));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_mstream_create(100, NULL, NULL));
        TEST_ASSERT(MA_OK == ma_mstream_create(100, NULL, &stream));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_bytestream_create(MA_LITTLE_ENDIAN, 0, NULL));
        TEST_ASSERT(MA_OK == ma_bytestream_create(MA_LITTLE_ENDIAN, 0, &bytestream));

        TEST_ASSERT(-1 == ma_bytestream_get_size(NULL));
        TEST_ASSERT(0 == ma_bytestream_get_size(bytestream));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_bytestream_set_stream(NULL, stream));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_bytestream_set_stream(bytestream, NULL));
        TEST_ASSERT(MA_OK == ma_bytestream_set_stream(bytestream, stream));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_bytestream_release(NULL));
        TEST_ASSERT(MA_OK == ma_bytestream_release(bytestream));

        // Adding 10 references
        for(counter = 0; counter < 10; counter++)
        {
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_stream_add_ref(NULL));
            TEST_ASSERT(MA_OK == ma_stream_add_ref(stream));
        }

        // Releasing 11 times to cleanup
        for(counter = 0; counter < 11; counter++)
        {
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_stream_release(NULL));
            TEST_ASSERT(MA_OK == ma_stream_release(stream));
        }
    }

    // For File stream
    {
        ma_stream_t *stream = NULL;
        ma_bytestream_t *bytestream = NULL;
        ma_uint8_t counter = 0;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_fstream_create(NULL, MA_STREAM_ACCESS_FLAG_WRITE, NULL, &stream));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_fstream_create("test.me", MA_STREAM_ACCESS_FLAG_WRITE, NULL, NULL));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_fstream_create("test.me", 0, NULL, &stream));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_fstream_create("test.me", 8, NULL, &stream));
        TEST_ASSERT(MA_OK == ma_fstream_create("test.me", MA_STREAM_ACCESS_FLAG_WRITE, NULL, &stream));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_bytestream_create(MA_LITTLE_ENDIAN, 0, NULL));
        TEST_ASSERT(MA_OK == ma_bytestream_create(MA_LITTLE_ENDIAN, 0, &bytestream));

        TEST_ASSERT(-1 == ma_bytestream_get_size(NULL));
        TEST_ASSERT(0 == ma_bytestream_get_size(bytestream));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_bytestream_set_stream(NULL, stream));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_bytestream_set_stream(bytestream, NULL));
        TEST_ASSERT(MA_OK == ma_bytestream_set_stream(bytestream, stream));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_bytestream_release(NULL));
        TEST_ASSERT(MA_OK == ma_bytestream_release(bytestream));

        // Adding 10 references
        for(counter = 0; counter < 10; counter++)
        {
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_stream_add_ref(NULL));
            TEST_ASSERT(MA_OK == ma_stream_add_ref(stream));
        }

        // Releasing 11 times to cleanup
        for(counter = 0; counter < 11; counter++)
        {
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_stream_release(NULL));
            TEST_ASSERT(MA_OK == ma_stream_release(stream));
        }
    }

    // For Net stream
    {
        ma_stream_t *stream = NULL;
        ma_bytestream_t *bytestream = NULL;
        ma_uint8_t counter = 0;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_netstream_create(NULL, NULL, &stream));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_netstream_create(ma_netstream_callback, NULL, NULL));
        TEST_ASSERT(MA_OK == ma_netstream_create(ma_netstream_callback, NULL, &stream));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_bytestream_create(MA_LITTLE_ENDIAN, 0, NULL));
        TEST_ASSERT(MA_OK == ma_bytestream_create(MA_LITTLE_ENDIAN, 0, &bytestream));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_bytestream_set_stream(NULL, stream));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_bytestream_set_stream(bytestream, NULL));
        TEST_ASSERT(MA_OK == ma_bytestream_set_stream(bytestream, stream));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_bytestream_release(NULL));
        TEST_ASSERT(MA_OK == ma_bytestream_release(bytestream));

        // Adding 10 references
        for(counter = 0; counter < 10; counter++)
        {
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_stream_add_ref(NULL));
            TEST_ASSERT(MA_OK == ma_stream_add_ref(stream));
        }

        // Releasing 11 times to cleanup
        for(counter = 0; counter < 11; counter++)
        {
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_stream_release(NULL));
            TEST_ASSERT(MA_OK == ma_stream_release(stream));
        }
    }
}

TEST(ma_bytestream_tests, ma_bytestream_get_buffer_test)
{
    ma_stream_t *stream = NULL;
    ma_bytestream_t *bytestream = NULL;
    ma_bytebuffer_t *my_buffer = NULL;

    // For Memory stream
    {
        // Creating a byte stream out of memory stream
        TEST_ASSERT(MA_OK == ma_mstream_create(100, NULL, &stream));
        TEST_ASSERT(MA_OK == ma_bytestream_create(MA_LITTLE_ENDIAN, 100, &bytestream));
        TEST_ASSERT(MA_OK == ma_bytestream_set_stream(bytestream, stream));

        // Writing into stream
        TEST_ASSERT(MA_OK == ma_bytestream_write_buffer(bytestream, (const unsigned char *)"This is McAfee Agent 5.0", strlen("This is McAfee Agent 5.0"), NULL));

        // Retrieving the byte buffer and verifying.
        TEST_ASSERT_NULL(ma_bytestream_get_buffer(NULL));
        
		my_buffer = ma_bytestream_get_buffer(bytestream);
		TEST_ASSERT_NOT_NULL(my_buffer);
        TEST_ASSERT(0 == strcmp("This is McAfee Agent 5.0", (char *)ma_bytebuffer_get_bytes(my_buffer)));

        // Releasing memory
        TEST_ASSERT(MA_OK == ma_bytebuffer_release(my_buffer));
        TEST_ASSERT(MA_OK == ma_bytestream_release(bytestream));
        TEST_ASSERT(MA_OK == ma_stream_release(stream));
    }

    // For File stream
    {
        // Creating a byte stream out of File stream (write mode)
        TEST_ASSERT(MA_OK == ma_fstream_create("test.me", MA_STREAM_ACCESS_FLAG_WRITE, NULL, &stream));
        TEST_ASSERT(MA_OK == ma_bytestream_create(MA_LITTLE_ENDIAN, 100, &bytestream));
        TEST_ASSERT(MA_OK == ma_bytestream_set_stream(bytestream, stream));

        // Writing into stream
        TEST_ASSERT(MA_OK == ma_bytestream_write_buffer(bytestream, (const unsigned char *)"This is McAfee Agent 5.0", strlen("This is McAfee Agent 5.0"), NULL));

        // Closing the stream
        TEST_ASSERT(MA_OK == ma_bytestream_release(bytestream));
        TEST_ASSERT(MA_OK == ma_stream_release(stream));

        // Opening the File stream again in read mode
        TEST_ASSERT(MA_OK == ma_fstream_create("test.me", MA_STREAM_ACCESS_FLAG_READ, NULL, &stream));
        TEST_ASSERT(MA_OK == ma_bytestream_create(MA_LITTLE_ENDIAN, 100, &bytestream));
        TEST_ASSERT(MA_OK == ma_bytestream_set_stream(bytestream, stream));

        // Retrieving the byte buffer and verifying.
        TEST_ASSERT_NULL(ma_bytestream_get_buffer(NULL));

		my_buffer = ma_bytestream_get_buffer(bytestream);
        TEST_ASSERT_NOT_NULL(my_buffer);
        TEST_ASSERT(0 == strcmp("This is McAfee Agent 5.0", (char *)ma_bytebuffer_get_bytes(my_buffer)));

        // Releasing memory
        TEST_ASSERT(MA_OK == ma_bytebuffer_release(my_buffer));
        TEST_ASSERT(MA_OK == ma_bytestream_release(bytestream));
        TEST_ASSERT(MA_OK == ma_stream_release(stream));
    }
}

void stream_rw_test(ma_bytestream_endianness_t endianness, ma_stream_t *stream, char *filename)
{
    struct test_data obj = {3213, 3213213UL, 23453423843452ULL, "0123456789" };
    struct test_data1 obj1 = {3233, 2313213UL, 5435235287532ULL, "0123456789ABCDEFGHI" };

    ma_uint8_t i = 0;

    ma_bytestream_t *bytestream = NULL;

    TEST_ASSERT_NOT_NULL(stream);
    TEST_ASSERT(MA_OK == ma_bytestream_create(endianness, 0, &bytestream));
    TEST_ASSERT(MA_OK == ma_bytestream_set_stream(bytestream, stream));

    // Writing data "num_iterations" times
    while(i < num_iterations)
    {
        size_t len = 0;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_bytestream_write_uint16(NULL, obj.b));
        TEST_ASSERT(MA_OK == ma_bytestream_write_uint16(bytestream, obj.b));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_bytestream_write_uint32(NULL, obj.c));
        TEST_ASSERT(MA_OK == ma_bytestream_write_uint32(bytestream, obj.c));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_bytestream_write_uint64(NULL, obj.d));
        TEST_ASSERT(MA_OK == ma_bytestream_write_uint64(bytestream, obj.d));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_bytestream_write_buffer(NULL, (unsigned char*)obj.buffer, strlen(obj.buffer), &len));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_bytestream_write_buffer(bytestream, NULL, strlen(obj.buffer), &len));
        TEST_ASSERT(MA_OK == ma_bytestream_write_buffer(bytestream, (unsigned char*)obj.buffer, strlen(obj.buffer), &len));
        TEST_ASSERT(len == strlen(obj.buffer))

        i++;
    }

    // Moving stream to the start
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_bytestream_seek(NULL, 0));
    TEST_ASSERT(MA_OK == ma_bytestream_seek(bytestream, 0));

    i = 0;

    // Reading data "num_iterations" times
    while(i < num_iterations)
    {
        size_t len = 0;
        struct test_data res;
        memset(&res, 0, sizeof(res));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_bytestream_read_uint16(NULL, &res.b));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_bytestream_read_uint16(bytestream, NULL));
        TEST_ASSERT(MA_OK == ma_bytestream_read_uint16(bytestream, &res.b));
        TEST_ASSERT(obj.b == res.b);

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_bytestream_read_uint32(NULL, &res.c));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_bytestream_read_uint32(bytestream, NULL));
        TEST_ASSERT(MA_OK == ma_bytestream_read_uint32(bytestream, &res.c));
        TEST_ASSERT(obj.c == res.c);

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_bytestream_read_uint64(NULL, &res.d));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_bytestream_read_uint64(bytestream, NULL));
        TEST_ASSERT(MA_OK == ma_bytestream_read_uint64(bytestream, &res.d));
        TEST_ASSERT(obj.d == res.d);

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_bytestream_read_buffer(NULL, (unsigned char*)res.buffer, strlen(obj.buffer), &len));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_bytestream_read_buffer(bytestream, NULL, strlen(obj.buffer), &len));
        TEST_ASSERT(MA_OK == ma_bytestream_read_buffer(bytestream, (unsigned char*)res.buffer, strlen(obj.buffer), &len));
        TEST_ASSERT(0 == strcmp(obj.buffer, res.buffer));
        TEST_ASSERT(len == strlen(obj.buffer));

        i++;
    }

    // Trying to read after EOF
    {
        size_t len = 0;
        struct test_data res;
        memset(&res, 0, sizeof(res));

        TEST_ASSERT(MA_ERROR_STREAM_EOF == ma_bytestream_read_buffer(bytestream, (unsigned char*)res.buffer, 1, &len));
    }

    // Moving the stream to start and getting the size (It should be equal to size of "num_iterations" records)
    {
        TEST_ASSERT(MA_OK == ma_bytestream_seek(bytestream, 0));

        TEST_ASSERT(-1 == ma_bytestream_get_size(NULL));

        // sizeof(struct) cannot be taken here, coz' of structure padding.
        TEST_ASSERT(((sizeof(obj.b) + sizeof(obj.c) + sizeof(obj.d) + sizeof(obj.buffer) - 1) * num_iterations) == ma_bytestream_get_size(bytestream));
    }

    i = 0;

    // Reading data "num_iterations" times after moving the pointer back to verify that seek works correctly
    while(i < num_iterations)
    {
        size_t len = 0;
        struct test_data res;
        memset(&res, 0, sizeof(res));

        TEST_ASSERT(MA_OK == ma_bytestream_read_uint16(bytestream, &res.b));
        TEST_ASSERT(obj.b == res.b);

        TEST_ASSERT(MA_OK == ma_bytestream_read_uint32(bytestream, &res.c));
        TEST_ASSERT(obj.c == res.c);

        TEST_ASSERT(MA_OK == ma_bytestream_read_uint64(bytestream, &res.d));
        TEST_ASSERT(obj.d == res.d);

        TEST_ASSERT(MA_OK == ma_bytestream_read_buffer(bytestream, (unsigned char*)res.buffer, strlen(obj.buffer), &len));
        TEST_ASSERT(0 == strcmp(obj.buffer, res.buffer));
        TEST_ASSERT(len == strlen(obj.buffer));

        i++;
    }

    // Moving pointer at the end of stream
    ma_bytestream_seek(bytestream, ma_bytestream_get_size(bytestream));

    i = 0;

    // Writing another data block "num_iterations" times at the end of previous stream
    while(i < num_iterations)
    {
        size_t len = 0;

        TEST_ASSERT(MA_OK == ma_bytestream_write_uint16(bytestream, obj1.b));
        TEST_ASSERT(MA_OK == ma_bytestream_write_uint32(bytestream, obj1.c));
        TEST_ASSERT(MA_OK == ma_bytestream_write_uint64(bytestream, obj1.d));
        TEST_ASSERT(MA_OK == ma_bytestream_write_buffer(bytestream, (unsigned char*)obj1.buffer, strlen(obj1.buffer), &len));
        TEST_ASSERT(len == strlen(obj1.buffer))

        i++;
    }

    // Moving the stream to start and getting the size (It should be equal to size of "num_iterations" records)
    {
        size_t size = 0;

        TEST_ASSERT(MA_OK == ma_bytestream_seek(bytestream, 0));

        // Combined size of both structures ("num_iterations" records each). sizeof(struct) cannot be taken here, coz'
        // of structure padding.
        size = (sizeof(obj.b) + sizeof(obj.c) + sizeof(obj.d) + sizeof(obj.buffer) +
                sizeof(obj1.b) + sizeof(obj1.c) + sizeof(obj1.d) + sizeof(obj1.buffer) - 2) * num_iterations;

		size = ma_bytestream_get_size(bytestream);
        TEST_ASSERT(0 != size);
    }

    i = 0;

    // Reading data "num_iterations" times. It should contain contents corresponding to first structure
    while(i < num_iterations)
    {
        size_t len = 0;
        struct test_data res;
        memset(&res, 0, sizeof(res));

        TEST_ASSERT(MA_OK == ma_bytestream_read_uint16(bytestream, &res.b));
        TEST_ASSERT(obj.b == res.b);

        TEST_ASSERT(MA_OK == ma_bytestream_read_uint32(bytestream, &res.c));
        TEST_ASSERT(obj.c == res.c);

        TEST_ASSERT(MA_OK == ma_bytestream_read_uint64(bytestream, &res.d));
        TEST_ASSERT(obj.d == res.d);

        TEST_ASSERT(MA_OK == ma_bytestream_read_buffer(bytestream, (unsigned char*)res.buffer, strlen(obj.buffer), &len));
        TEST_ASSERT(0 == strcmp(obj.buffer, res.buffer));
        TEST_ASSERT(len == strlen(obj.buffer));

        i++;
    }

    // Reading data "num_iterations" times again. It should contain contents corresponding to second structure.
    while(i < num_iterations)
    {
        size_t len = 0;
        struct test_data res;
        memset(&res, 0, sizeof(res));

        TEST_ASSERT(MA_OK == ma_bytestream_read_uint16(bytestream, &res.b));
        TEST_ASSERT(obj1.b == res.b);

        TEST_ASSERT(MA_OK == ma_bytestream_read_uint32(bytestream, &res.c));
        TEST_ASSERT(obj1.c == res.c);

        TEST_ASSERT(MA_OK == ma_bytestream_read_uint64(bytestream, &res.d));
        TEST_ASSERT(obj1.d == res.d);

        TEST_ASSERT(MA_OK == ma_bytestream_read_buffer(bytestream, (unsigned char*)res.buffer, strlen(obj1.buffer), &len));
        TEST_ASSERT(0 == strcmp(obj1.buffer, res.buffer));
        TEST_ASSERT(len == strlen(obj1.buffer));

        i++;
    }

    // Cleanup
    TEST_ASSERT(MA_OK == ma_bytestream_release(bytestream));
    TEST_ASSERT(MA_OK == ma_stream_release(stream));
}

TEST(ma_bytestream_tests, ma_bytestream_mstream_le_write_read_test)
{
    ma_stream_t *stream = NULL;
    TEST_ASSERT(MA_OK == ma_mstream_create(100, NULL, &stream));
    stream_rw_test(MA_LITTLE_ENDIAN, stream, NULL);
}

TEST(ma_bytestream_tests, ma_bytestream_mstream_be_write_read_test)
{
    ma_stream_t *stream = NULL;
    TEST_ASSERT(MA_OK == ma_mstream_create(100, NULL, &stream));
    stream_rw_test(MA_BIG_ENDIAN, stream, NULL);
}

TEST(ma_bytestream_tests, ma_bytestream_fstream_le_write_read_test)
{
    ma_stream_t *stream = NULL;
    TEST_ASSERT(MA_OK == ma_fstream_create("test.me", MA_STREAM_ACCESS_FLAG_READ | MA_STREAM_ACCESS_FLAG_WRITE, NULL, &stream));
    stream_rw_test(MA_LITTLE_ENDIAN, stream, "test.me");
}

TEST(ma_bytestream_tests, ma_bytestream_fstream_be_write_read_test)
{
    ma_stream_t *stream = NULL;
    TEST_ASSERT(MA_OK == ma_fstream_create("test.me", MA_STREAM_ACCESS_FLAG_READ | MA_STREAM_ACCESS_FLAG_WRITE,NULL, &stream));
    stream_rw_test(MA_BIG_ENDIAN, stream, "test.me");
}


