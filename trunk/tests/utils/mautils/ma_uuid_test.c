/*
UUID Tests
*/

#include "unity.h"
#include "unity_fixture.h"
#include "ma/internal/utils/uuid/ma_uuid.h"


TEST_GROUP_RUNNER(ma_uuid_test_group)
{
    do {RUN_TEST_CASE(ma_uuid_tests, ma_uuid_gen_test)} while (0);
}

TEST_SETUP(ma_uuid_tests) {}

TEST_TEAR_DOWN(ma_uuid_tests) {}

TEST(ma_uuid_tests, ma_uuid_gen_test)
{
    char uuid1[UUID_LEN_TXT] = {0};
    char uuid2[UUID_LEN_TXT] = {0};

    // Verifying two UUID generated for same inputs are different (unique UUIDs
    // generated everytime).
    TEST_ASSERT(MA_OK == ma_uuid_generate(1, (unsigned char *)"12345", uuid1));
    TEST_ASSERT(MA_OK == ma_uuid_generate(1, (unsigned char *)"12345", uuid2));
    TEST_ASSERT(uuid1 != uuid2);

    // Use MAC address or not (unique UUIDs generated everytime).
    TEST_ASSERT(MA_OK == ma_uuid_generate(1, (unsigned char *)"ABCDEF", uuid1));
    TEST_ASSERT(MA_OK == ma_uuid_generate(0, (unsigned char *)"ABCDEF", uuid2));
    TEST_ASSERT(uuid1 != uuid2);

    // Unique UUIDs generated everytime.
    TEST_ASSERT(MA_OK == ma_uuid_generate(1, (unsigned char *)"ABCDEF", uuid1));
    TEST_ASSERT(MA_OK == ma_uuid_generate(1, NULL, uuid2));
    TEST_ASSERT(uuid1 != uuid2);

    // Unique UUIDs generated everytime.
    TEST_ASSERT(MA_OK == ma_uuid_generate(1, NULL, uuid1));
    TEST_ASSERT(MA_OK == ma_uuid_generate(1, NULL, uuid2));
    TEST_ASSERT(uuid1 != uuid2);

    // Unique UUIDs generated everytime.
    TEST_ASSERT(MA_OK == ma_uuid_generate(0, (unsigned char *)"!@#$%", uuid1));
    TEST_ASSERT(MA_OK == ma_uuid_generate(0, (unsigned char *)"%^&*(", uuid2));
    TEST_ASSERT(uuid1 != uuid2);

    // Unique UUIDs generated everytime.
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_uuid_generate(1, (unsigned char *)"%^&*(", NULL));
}



