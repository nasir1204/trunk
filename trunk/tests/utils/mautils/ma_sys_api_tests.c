#define UNITY_FIXTURE_MALLOC_OVERRIDES_H_

#include "unity.h"
#include "unity_fixture.h"
#include "unity_internals.h" 

#include "ma/ma_common.h"
#include "ma/internal/utils/platform/ma_sys_apis.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/utils/text/ma_utf8.h"
#include <string.h>
#include <wchar.h>

#ifdef MA_WINDOWS
#include <Windows.h>
#else
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>
#endif

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>


#define MAX_SERVER_NAME_LEN 1024

TEST_GROUP_RUNNER(ma_sys_api_test_group) {
    do {RUN_TEST_CASE(ma_sys_api_tests, ma_free_disk_space_test);}while(0);
	do {RUN_TEST_CASE(ma_sys_api_tests, ma_strnlen_test);}while(0);
	do {RUN_TEST_CASE(filesystem_iterator_tests, ma_filesystem_iterator_test)}while(0);
#ifdef MA_WINDOWS
	do {RUN_TEST_CASE(ma_sys_api_tests, ma_registry_delete_tree_test);}while(0);
#endif
}

TEST_SETUP(ma_sys_api_tests) {
}

TEST_TEAR_DOWN(ma_sys_api_tests) {
}

#ifdef MA_WINDOWS
#define DIR_NAME_FOR_FREE_SPACE_CHECK   "C:\\Windows\\Temp"
#else
#define DIR_NAME_FOR_FREE_SPACE_CHECK   "/opt/"
#endif

TEST(ma_sys_api_tests, ma_free_disk_space_test) {
     TEST_ASSERT(0 != ma_get_disk_free_space(DIR_NAME_FOR_FREE_SPACE_CHECK));
}

TEST(ma_sys_api_tests, ma_strnlen_test) {
    char *str = "Const String"; /* length 12 */
    char *str1  = (char *)calloc(13, sizeof(char));

    TEST_ASSERT_EQUAL_INT(8, ma_strnlen(str, 8));
    TEST_ASSERT_EQUAL_INT(12, ma_strnlen(str, 12));
    TEST_ASSERT_EQUAL_INT(12, ma_strnlen(str, 15));

    strncpy(str1, str, 12);
    TEST_ASSERT_EQUAL_INT(6, ma_strnlen(str1, 6));
    TEST_ASSERT_EQUAL_INT(12, ma_strnlen(str1, 12));
    TEST_ASSERT_EQUAL_INT(12, ma_strnlen(str1, 28));

    free(str1);
}

#ifdef MA_WINDOWS
#include "ma/datastore/ma_ds_registry.h"

extern ma_error_t ma_registry_delete_tree(void* hKeyRoot, const char *path, ma_bool_t b_default_view);

ma_ds_t *g_ds = NULL;
TEST(ma_sys_api_tests, ma_registry_delete_tree_test) {
	ma_ds_registry_t *registry_datastore = NULL;
	if(MA_OK == ma_ds_registry_open(HKEY_LOCAL_MACHINE, "SOFTWARE\\McAfee_T1\\", MA_TRUE, MA_TRUE,KEY_ALL_ACCESS, &registry_datastore)) {
		ma_int32_t value;
		size_t size = 0;
		g_ds = (ma_ds_t *) registry_datastore;

		ma_ds_set_str(g_ds, "Application1", "Data Path", "1", strlen("1"));
		ma_ds_set_int(g_ds,"Application1\\Property Provider", "Use_Ipc", 1);
		ma_ds_set_str(g_ds, "Application2", "Data Path", "1", strlen("1"));
		ma_ds_set_int(g_ds,"Application2\\Property Provider", "Use_Ipc", 1);
		
		TEST_ASSERT(MA_OK == ma_ds_get_int32(g_ds,"Application1\\Property Provider", "Use_Ipc", & value));
		TEST_ASSERT(MA_OK == ma_ds_get_int32(g_ds,"Application2\\Property Provider", "Use_Ipc", & value));
		TEST_ASSERT(MA_OK == ma_registry_delete_tree(HKEY_LOCAL_MACHINE, "SOFTWARE\\McAFee_T1\\Application2", MA_TRUE));
		TEST_ASSERT(MA_OK == ma_ds_get_int32(g_ds,"Application1\\Property Provider", "Use_Ipc", & value));
		TEST_ASSERT(MA_ERROR_DS_PATH_NOT_FOUND == ma_ds_get_int32(g_ds,"Application2\\Property Provider", "Use_Ipc", & value));
		ma_registry_delete_tree(HKEY_LOCAL_MACHINE, "SOFTWARE\\McAfee_T1", MA_TRUE);
		ma_ds_release(g_ds);
	}
	if(MA_OK == ma_ds_registry_open(HKEY_LOCAL_MACHINE, "SOFTWARE\\McAfee_T1\\", MA_FALSE, MA_TRUE,KEY_ALL_ACCESS, &registry_datastore)) {
		ma_int32_t value;
		size_t size = 0;
		g_ds = (ma_ds_t *) registry_datastore;

		ma_ds_set_str(g_ds, "Application1", "Data Path", "1", strlen("1"));
		ma_ds_set_int(g_ds,"Application1\\Property Provider", "Use_Ipc", 1);
		ma_ds_set_str(g_ds, "Application2", "Data Path", "1", strlen("1"));
		ma_ds_set_int(g_ds,"Application2\\Property Provider", "Use_Ipc", 1);
		
		TEST_ASSERT(MA_OK == ma_ds_get_int32(g_ds,"Application1\\Property Provider", "Use_Ipc", & value));
		TEST_ASSERT(MA_OK == ma_ds_get_int32(g_ds,"Application2\\Property Provider", "Use_Ipc", & value));
		TEST_ASSERT(MA_OK == ma_registry_delete_tree(HKEY_LOCAL_MACHINE, "SOFTWARE\\McAFee_T1\\Application2", MA_FALSE));
		TEST_ASSERT(MA_OK == ma_ds_get_int32(g_ds,"Application1\\Property Provider", "Use_Ipc", & value));
		TEST_ASSERT(MA_ERROR_DS_PATH_NOT_FOUND == ma_ds_get_int32(g_ds,"Application2\\Property Provider", "Use_Ipc", & value));
		ma_registry_delete_tree(HKEY_LOCAL_MACHINE, "SOFTWARE\\McAfee_T1", MA_FALSE);
		ma_ds_release(g_ds);
	}
}
#endif

static ma_error_t get_temp_directory(ma_temp_buffer_t *buffer) {
#ifdef MA_WINDOWS
	HMODULE hModule = NULL;
	int i = 0;
	char *p = NULL;
	if(!GetModuleFileNameA(hModule,(char *) ma_temp_buffer_get(buffer), ma_temp_buffer_capacity(buffer))) {
		int a = GetLastError();
		return MA_ERROR_APIFAILED;
	}
	p = (char *)ma_temp_buffer_get(buffer);
	for( i = strlen(p) - 1; i > 0 ; i--) {
		if(p[i]=='\\') {
			p[i] = '\0';
			break;
		}
	}
#else
	char *temp_path = tempnam(P_tmpdir, "mfe");
	size_t len = 0;
	if(temp_path == NULL) {
		return MA_ERROR_APIFAILED;
	}
	len = strlen(temp_path);
	ma_temp_buffer_reserve(buffer, len);
	strncpy((char*)ma_temp_buffer_get(buffer), temp_path, len);
	free(temp_path);
#endif
	return MA_OK;
}

static ma_error_t create_directory(const char *path, const char* directory_name) {
	ma_temp_buffer_t buffer;
	char *p = NULL;
	ma_temp_buffer_init(&buffer);
	form_path(&buffer, path, directory_name,MA_FALSE);
	p = (char*) ma_temp_buffer_get(&buffer);
#ifdef MA_WINDOWS
	if(!CreateDirectoryA(p, NULL)) {
		DWORD error = GetLastError();
		if(error != ERROR_ALREADY_EXISTS && error != ERROR_ACCESS_DENIED)
			return MA_ERROR_APIFAILED;
	}
#else
	printf("creating directory : %s\n", p);
	if(mkdir(p, S_IRWXU) != 0){
		printf("error creating directory : %s\n",strerror(errno));
		return MA_ERROR_APIFAILED;
	}
#endif
	ma_temp_buffer_uninit(&buffer);
	return MA_OK;
}


static ma_error_t recursive_remove_directory(const char *path) {
	
	ma_temp_buffer_t directory_buffer;
	ma_temp_buffer_init(&directory_buffer);
	form_path(&directory_buffer, path, "", MA_FALSE);
	
#ifdef MA_WINDOWS
	{
		WIN32_FIND_DATAA fdd; 
		HANDLE hFindFile = INVALID_HANDLE_VALUE;
		char *p_directory_buffer = NULL;
		form_path(&directory_buffer, (char*)ma_temp_buffer_get(&directory_buffer), "*", MA_FALSE);
		p_directory_buffer = (char*)ma_temp_buffer_get(&directory_buffer);
		hFindFile = FindFirstFileA(p_directory_buffer, &fdd);
		if(hFindFile != INVALID_HANDLE_VALUE) {
			ma_temp_buffer_t subfolder_path;
			ma_temp_buffer_init(&subfolder_path);
			do{
				if(0 == strcmp(fdd.cFileName, ".") || 0 == strcmp(fdd.cFileName,"..")){
					FindNextFileA(hFindFile, &fdd);
					continue;
				}
				form_path(&subfolder_path, path, fdd.cFileName,MA_FALSE);
				if(fdd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
					recursive_remove_directory((char*)ma_temp_buffer_get(&subfolder_path));
				}
				else {
					unlink((char*)ma_temp_buffer_get(&subfolder_path));
				}
			}while(FindNextFileA(hFindFile, &fdd));
			ma_temp_buffer_uninit(&subfolder_path);
			FindClose(hFindFile);
		}
		RemoveDirectoryA(path);
	}
#else
	{
		DIR *p_dir;
		struct dirent *p_ent;

		p_dir = opendir((char*)ma_temp_buffer_get(&directory_buffer));
		if(p_dir) {
			while(NULL != (p_ent = readdir(p_dir))){
				ma_temp_buffer_t temp_buffer;
				struct stat st;
				char *p_temp_buffer = NULL;
				ma_temp_buffer_init(&temp_buffer);
				ma_temp_buffer_reserve(&temp_buffer, strlen(path) + strlen(p_ent->d_name));
				form_path(&temp_buffer, path, p_ent->d_name,MA_FALSE);
				p_temp_buffer = (char *) ma_temp_buffer_get(&temp_buffer);
				if(	-1 == stat(p_temp_buffer, &st) ||
					0 == strcmp(p_ent->d_name,".") ||
					0 == strcmp(p_ent->d_name,"..")) {
					ma_temp_buffer_uninit(&temp_buffer);
					continue;
				}
				if (!S_ISDIR(st.st_mode) ) {
					unlink(p_temp_buffer);
				}
				else{
					recursive_remove_directory(p_temp_buffer);
				}
				rmdir(path);
				ma_temp_buffer_uninit(&temp_buffer);
			}
			closedir(p_dir);
		}
	}
#endif
	ma_temp_buffer_uninit(&directory_buffer);
	return MA_OK;
}

char *file_system_test_path = NULL;

TEST_SETUP(filesystem_iterator_tests) {
	ma_temp_buffer_t buffer_temp = MA_TEMP_BUFFER_INIT;
	ma_temp_buffer_t buffer = MA_TEMP_BUFFER_INIT;
	get_temp_directory(&buffer);
	create_directory((char*)ma_temp_buffer_get(&buffer),"");
	create_directory((char*)ma_temp_buffer_get(&buffer), "TEST");
	//ma_temp_buffer_reserve will remove existing buffer
	ma_temp_buffer_copy(&buffer_temp, &buffer);
	ma_temp_buffer_reserve(&buffer,strlen((char*)ma_temp_buffer_get(&buffer))+ 5);
	ma_temp_buffer_copy(&buffer, &buffer_temp);
	form_path(&buffer, (char*) ma_temp_buffer_get(&buffer), "TEST",MA_FALSE);
	file_system_test_path = strdup((char*)ma_temp_buffer_get(&buffer));
	create_directory(file_system_test_path, "0409");
	create_directory(file_system_test_path, "0410");
	create_directory(file_system_test_path, "0411");
	ma_temp_buffer_uninit(&buffer);
}

TEST_TEAR_DOWN(filesystem_iterator_tests) {
	recursive_remove_directory(file_system_test_path);
	free(file_system_test_path);
	file_system_test_path = NULL;
}

TEST(filesystem_iterator_tests, ma_filesystem_iterator_test) {

	ma_filesystem_iterator_t *iterator = NULL;
	ma_temp_buffer_t buffer;
	wchar_t buf[MAX_SERVER_NAME_LEN] = {0};
	size_t count = 0;
	ma_temp_buffer_init(&buffer);
	TEST_ASSERT(MA_OK == ma_filesystem_iterator_create(file_system_test_path, &iterator));
	
	// The sequence is not guaranteed, therefore, compare the retrieved directory name against
	// all expected directory names.
	TEST_ASSERT(MA_OK == ma_filesystem_iterator_get_next(iterator, &buffer));
	ma_utf8_to_wide_char(buf, MAX_SERVER_NAME_LEN, (char*) ma_temp_buffer_get(&buffer), MAX_SERVER_NAME_LEN);
	TEST_ASSERT(0 == wcscmp(L"0409", buf) || 0 == wcscmp(L"0410", buf)|| 0 == wcscmp(L"0411", buf));
	TEST_ASSERT(MA_OK == ma_filesystem_iterator_get_next(iterator, &buffer));
	ma_utf8_to_wide_char(buf, MAX_SERVER_NAME_LEN, (char*) ma_temp_buffer_get(&buffer), MAX_SERVER_NAME_LEN);
	TEST_ASSERT(0 == wcscmp(L"0409", buf) || 0 == wcscmp(L"0410", buf)|| 0 == wcscmp(L"0411", buf));
	TEST_ASSERT(MA_OK == ma_filesystem_iterator_get_next(iterator, &buffer));
	ma_utf8_to_wide_char(buf, MAX_SERVER_NAME_LEN, (char*) ma_temp_buffer_get(&buffer), MAX_SERVER_NAME_LEN);
	TEST_ASSERT(0 == wcscmp(L"0409", buf) || 0 == wcscmp(L"0410", buf)|| 0 == wcscmp(L"0411", buf));
	TEST_ASSERT(MA_ERROR_NO_MORE_ITEMS == ma_filesystem_iterator_get_next(iterator, &buffer));
	TEST_ASSERT(MA_OK == ma_filesystem_iterator_release(iterator));
	ma_temp_buffer_uninit(&buffer);	
	ma_filesystem_iterator_count(file_system_test_path, MA_FILESYSTEM_ITERATE_DIRECTORY, &count);
	TEST_ASSERT(3 == count);
	ma_filesystem_iterator_count(file_system_test_path, MA_FILESYSTEM_ITERATE_FILES, &count);
	TEST_ASSERT(0 == count);
}
