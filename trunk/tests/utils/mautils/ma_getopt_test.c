/*
getopt utility tests
*/

#include "unity.h"
#include "unity_fixture.h"
#include "ma/internal/utils/getopt/ma_getopt.h"
#include <string.h>


TEST_GROUP_RUNNER(getopt_test_group)
{
    do {RUN_TEST_CASE(getopt_tests, getopt_test)} while (0);
}


TEST_SETUP(getopt_tests) {}

TEST_TEAR_DOWN(getopt_tests) {}

TEST(getopt_tests, getopt_test)
{
    // When argument count is 0 (No arguments)
    {
        int my_argc = 0;
        char *my_argv[] = { "messagebus.exe", "-e", "ASYNC_CLIENT", "-F", "20", "-n", "7", "-r", "OUTPROC", "-t", "THREAD_POOL", "-m", "10", "-f", "100", "-S", "MY_SERVER1", "-k" };

        TEST_ASSERT(EOF == ma_getopt(my_argc, my_argv, "e:n:r:t:F:f:m:S:T:h"));
        TEST_ASSERT_NULL(ma_getopt_str());
        TEST_ASSERT_NULL(ma_getoptarg());
    }

    // When argument count is 1 (First argument is the executable name, therefore, for option processing, more than 1 arguments should be there)
    {
        int my_argc = 1;
        char *my_argv[] = { "messagebus.exe", "-e", "ASYNC_CLIENT", "-F", "20", "-n", "7", "-r", "OUTPROC", "-t", "THREAD_POOL", "-m", "10", "-f", "100", "-S", "MY_SERVER1", "-k" };

        TEST_ASSERT(EOF == ma_getopt(my_argc, my_argv, "e:n:r:t:F:f:m:S:T:h"));
        TEST_ASSERT_NULL(ma_getopt_str());
        TEST_ASSERT_NULL(ma_getoptarg());
    }

    // When argument count is > 1, but argument list is empty
    {
        int my_argc = 2;

        TEST_ASSERT(EOF == ma_getopt(my_argc, NULL, "e:n:r:t:F:f:m:S:T:h"));
        TEST_ASSERT_NULL(ma_getopt_str());
        TEST_ASSERT_NULL(ma_getoptarg());
    }

    // 1. Multiple arguments in the argument list
    // 2. An option which is not present in the option list
    // 3. Option Delimiter (--)
    {
        char *my_argv[] = { "messagebus.exe", "-e", "ASYNC_CLIENT", "-F", "20", "-n", "7", "-r", "OUTPROC", "-t", "THREAD_POOL", "-m", "10", "-f", "100", "-k" "Not an expected flag", "-S", "MY_SERVER1", "--", "-T", "6000" };

        TEST_ASSERT('e' == ma_getopt(sizeof(my_argv)/sizeof(my_argv[0]), my_argv, "e:n:r:t:F:f:m:S:T:h"));
        TEST_ASSERT(0 == strcmp("e", ma_getopt_str()));
        TEST_ASSERT(0 == strcmp("ASYNC_CLIENT", ma_getoptarg()));

        TEST_ASSERT('F' == ma_getopt(sizeof(my_argv)/sizeof(my_argv[0]), my_argv, "e:n:r:t:F:f:m:S:T:h"));
        TEST_ASSERT(0 == strcmp("F", ma_getopt_str()));
        TEST_ASSERT(0 == strcmp("20", ma_getoptarg()));

        TEST_ASSERT('n' == ma_getopt(sizeof(my_argv)/sizeof(my_argv[0]), my_argv, "e:n:r:t:F:f:m:S:T:h"));
        TEST_ASSERT(0 == strcmp("n", ma_getopt_str()));
        TEST_ASSERT(0 == strcmp("7", ma_getoptarg()));

        TEST_ASSERT('r' == ma_getopt(sizeof(my_argv)/sizeof(my_argv[0]), my_argv, "e:n:r:t:F:f:m:S:T:h"));
        TEST_ASSERT(0 == strcmp("r", ma_getopt_str()));
        TEST_ASSERT(0 == strcmp("OUTPROC", ma_getoptarg()));

        TEST_ASSERT('t' == ma_getopt(sizeof(my_argv)/sizeof(my_argv[0]), my_argv, "e:n:r:t:F:f:m:S:T:h"));
        TEST_ASSERT(0 == strcmp("t", ma_getopt_str()));
        TEST_ASSERT(0 == strcmp("THREAD_POOL", ma_getoptarg()));

        TEST_ASSERT('m' == ma_getopt(sizeof(my_argv)/sizeof(my_argv[0]), my_argv, "e:n:r:t:F:f:m:S:T:h"));
        TEST_ASSERT(0 == strcmp("m", ma_getopt_str()));
        TEST_ASSERT(0 == strcmp("10", ma_getoptarg()));

        TEST_ASSERT('f' == ma_getopt(sizeof(my_argv)/sizeof(my_argv[0]), my_argv, "e:n:r:t:F:f:m:S:T:h"));
        TEST_ASSERT(0 == strcmp("f", ma_getopt_str()));
        TEST_ASSERT(0 == strcmp("100", ma_getoptarg()));

        // '+' should be returned for any option which is not there in optstring
        TEST_ASSERT('+' == ma_getopt(sizeof(my_argv)/sizeof(my_argv[0]), my_argv, "e:n:r:t:F:f:m:S:T:h"));

        TEST_ASSERT('S' == ma_getopt(sizeof(my_argv)/sizeof(my_argv[0]), my_argv, "e:n:r:t:F:f:m:S:T:h"));
        TEST_ASSERT(0 == strcmp("S", ma_getopt_str()));
        TEST_ASSERT(0 == strcmp("MY_SERVER1", ma_getoptarg()));

        // End of options
        TEST_ASSERT(EOF == ma_getopt(sizeof(my_argv)/sizeof(my_argv[0]), my_argv, "e:n:r:t:F:f:m:S:T:h"));
        TEST_ASSERT_NULL(ma_getopt_str());
        //TEST_ASSERT_NULL(ma_getoptarg());

        // Calling ma_getopt() again should return EOF
        //TEST_ASSERT(EOF == ma_getopt(sizeof(my_argv)/sizeof(my_argv[0]), my_argv, "e:n:r:t:F:f:m:S:T:h"));


        // -T option is after delimiter (--) and should not be parsed.
    }
}



