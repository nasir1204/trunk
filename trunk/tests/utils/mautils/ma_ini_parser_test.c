/*
INI Parser utility UTs
*/

#include "unity.h"
#include "unity_fixture.h"
#include "ma/internal/utils/text/ma_ini_parser.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static char buffer[]
= "\
#\n\
# test data.\n\
#\n\
Version=20120330075958\n\
List=F1HjtTzscUsaIb27nQtAdoFh8RI=,H7LGlPbFtbARjG6MsNaop1QOFaU=,Z7TiHcFSj+akkDwCGqanABMavX7uSBb7/NNxjlB8Qyk=,q8Cuu4WYZQEk83cm+7B/dQH6lVpxnqbTrlEKP2x3wK4=,t02sSQTtWffocwfRV+03Coo6P0Y=,w+o9878uc1l8PtsKAxW6zP8J5huS4OSVKKtXe2XBVKI=\n\
[RepoKeyHashList]\n\
Version=20120330075958\n\
List=F1HjtTzscUsaIb27nQtAdoFh8RI=,H7LGlPbFtbARjG6MsNaop1QOFaU=,Z7TiHcFSj+akkDwCGqanABMavX7uSBb7/NNxjlB8Qyk=,q8Cuu4WYZQEk83cm+7B/dQH6lVpxnqbTrlEKP2x3wK4=,t02sSQTtWffocwfRV+03Coo6P0Y=,w+o9878uc1l8PtsKAxW6zP8J5huS4OSVKKtXe2XBVKI=\n\
[KeyData]\n\
\n\
\n\
KeyCount=6\n\
Key0Hash=F1HjtTzscUsaIb27nQtAdoFh8RI=\n\
Key0Data=BAD0ihpQx1bXZlTE2qRlOdKXjO18B4bqVG2HedxV1FNrB78y1K1ZE5l+cwrU8prCpRHWvXcZkiRt6NFGb/z3Xm7eG1ADqtJlYlrpaqYp9FJAHdH9rrj27hGz3QuMpp+2taiav72lfw1RgXFGTYw5sA0kVaEBK0t012PGIfsOGBSMewCgzeohMq1rNwa79hQlKvvHM4Q6aXMD+wQ/Kc+p9kku9Jwoj8g0ueoLC+r/9t1ufvWhKcR+qwmWfsBI6rMN4wuq7wIXPlNOl7mi+pLqYV4myueEvAHITJDYh7PLQysa/Y9siiPRHGxL5+B6fsU/st0JSvcM/lnXeLOLm0uYTEgVn9nGYc0Mqo7mnMJFnTYuNOmzlEL0P49gBACs40kIvyOd4LETiQPUOM/6DgNAttDXWrWmTOmDfxLUXn1SaMfIXdi9UKVCljv/4DJbHCY0X+FuMGbnvrMCvtvHUaoTDHz5w6ECdaQvFVQ844gkRnUWJFwz8qljP6G9ogEbBLVDWwvbzpqPljqJ+nllhatv1bs47OyVtAFRfVdu0w==\n\
Key1Hash=H7LGlPbFtbARjG6MsNaop1QOFaU=\n\
Key1Data=BAD6LZCm0gA9nALbG4C1Kcsr035/z30S7RwvEvcR+NtAItcnR+Zy6qrBuVMcn21OFnnftKSPuetlRzf4jXnGYh6rsbzD981q0JaNaLX73bW+Dszc/nvKq+rTDEDFc3SB1bY0+SzJNItSWgLJFEPRjadc1aJuTyuHSxJKXS8VliEa+wCg/3hun4P8w5G7HqXQ1KR/nbjT66kD/2JXaGGKudO6sTIv7hxl2oK64hq1MXQ2LVHJ37t3ECro+75plCffUQQe9RHwRJRfsHEoVZ7+4x3K+kdM+vr1d+/G+VGcq38e6otLX282ul4dUy3cLS8OvpknfuOjj5D1uMosQX1QFBt90Bzvzytn9oxG/la5qxURGP95UMfCoa3XBACiJ8f3qTvuFz47ieRONJW7Y7XFjzYDA1FqCsBy2licrofqWHSxDBMJZa0wWqkyg6FymsPMHkgjsnyOD4VDyCC5D6d0wi1oTB7Fdv+lDaJ/cvD8lmOsjKZP4uUm3C0dkDFtMjzyH7T1cTedKLjbHJ0tlu3SU6UoLhxUqJ52pm3KGA==\n\
Key2Hash=Z7TiHcFSj+akkDwCGqanABMavX7uSBb7/NNxjlB8Qyk=\n\
Key2Data=UlNBMTCCAQoCggEBAK+3mX6nlwqLerVYO4iWRKkPVAbn9gsPLyf62YfmnyNnWh1UJBoyPKRJcO8fW0ViVxXdTJHXM1yu5NSrVrnvU1TSdYIGI/uv6jUGP8JNtBNbk7gVqbtUItpz7VM2B+uFJnx8L7hrizOF0u0MopcrCmbX1WSGzqmrVS92zrdE/N+ejIc86O9ynBv0pfdlv7L1A1KFg2zdVqupocA6zIQM+pFORdoO075hsnY0ejWBX75a7ZtjxN1XYoTa/3h8vCp5ScPrqnR2ytZXF2dF13YxbWj7fmLn7zk7DGsvooL0qYP0b3DTWRh2qaUy9mvea3QXjz1wz7r07PR7mk6gRTVswgsCAwEAAQ==\n\
Key3Hash=q8Cuu4WYZQEk83cm+7B/dQH6lVpxnqbTrlEKP2x3wK4=\n\
Key3Data=UlNBMTCCAQoCggEBAM9+UWbDbFqCnNz0NDyUzxbPZmNlgTezv1Wsec9ImqFRE5Oyc7HZrrBLATaQw9hCkdZZu3COn2/udBWpJAv1M7QUtn3slhjdNwGIRYdwH+/pDml+zz/dwR0PJpGtdKW5reLRgp4Kqa4WWHUCMm3PoA/pYyi8VfaOGt3Sqb/9JLMfux7GO39yk3PD7Ju1Vir266m63kBpc7bvfncUxYR5XJPv+xf84VYC7A0L7yovqWPiO98fWPCwFM6jvr5QCY2XDIs85Lp3XYOiZq/Tae6nmC+hfWmAm7RRQ3Tq54EUP/kwckZgxzCuYv+CMi2t/jOb7jDus8TTnBeJoeiADWGdE5kCAwEAAQ==\n\
Key4Hash=t02sSQTtWffocwfRV+03Coo6P0Y=\n\
Key4Data=BACb//D91X7AS+WltWK6AGCmmi5kKQah7supKbRoL8kTRRR8UzKzmUU62y2azX1FdjkAdRHKcxLzsxexr7pAqyTVwWL0glHYjz6T5cnD/hKjk+PzXfodHKHKcAAJLzJUM9fS0VsrTc0qca4pEHBRkdU0Bg3mS6vXHyj90yZOnz4XQQCgnDMl5dRw64GvY/o3knzP1/QRXAkD+wVTGMRaSs/7Juvg/jb7tAU8iSxhBK3TrPdSqF3m4TDtAZ8djGtwb4+vRVt+yQoTin8kYW0pTtVaMHoreF/U8CeIDEKVpT/lfyj8wU/B/B3Pbr7yN9oadO2/Nl0M8OunFRWdsJWTX3a2YikLBY9hNx/4lZisVZDehEvXkuq9NkVhA/9qv/Bs6DfNKLJ8qOESJfK+7jGYb3LZQHMwrNhmJyH1tJHzlqG/96Rfae/eaNQYgQaC9Hzt82hKPovuGacTN4TfxuAEIONCctTV6IwOm57yXtG1hPYbxecxiGfA/zZWl39MzvH/9eNCY+oGCGHl1EtOvNdAuJnWN2dE+aJSmhgTqw==\n\
Key5Hash=w+o9878uc1l8PtsKAxW6zP8J5huS4OSVKKtXe2XBVKI=\n\
Key5DataUlNBMTCCAQoCggEBAMFiJ6dUmyLGfjNShUA1dm0xNHgh/KSkgy2nit5R8GQ5qe76CMxtuRr0c5/KrV9qAcf29NPFR74tibdZEY5ZfPp53C2cMmiYWshcpdVyG1GW42his3GIb0WP7+LJoB+qJvhnhrMkOdLf+Yh64Qxzyq9pcCDKFsbuimDmkrZHvVNRCEKK/PyMzqWGZwXjSPoX18Jbvfe/uXBWjqh6D0o6jUoaTTj0Vn1GaNuGNrOCbwEPuDk4coCKUIUg+uGN6OdG95P86BGsP4wriJtEK5m8QxzV1Y0+D92xuRvnk9NnaLthp6dVyglNfFD92WlSrm61Z/SDMOCQBeBeOmCn5VcggWMCAwEAAQ==";

TEST_GROUP_RUNNER(ma_ini_parser_test_group)
{
    do {RUN_TEST_CASE(ma_ini_parser_tests, ma_ini_parser_test)} while (0);
}

TEST_SETUP(ma_ini_parser_tests) {}

TEST_TEAR_DOWN(ma_ini_parser_tests) {}

int my_ini_handler(const char *section, const char *key, const char *value, void *userdata)
{
    printf("\n");
    printf("[%s] \n", section);
    printf("%s = %s \n", key, value);
    printf("\n");
    return 0;
}

TEST(ma_ini_parser_tests, ma_ini_parser_test)
{
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ini_parser_start(NULL, strlen(buffer), my_ini_handler, NULL));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ini_parser_start(buffer, 0, my_ini_handler, NULL));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ini_parser_start(buffer, strlen(buffer), NULL, NULL));
    TEST_ASSERT(MA_OK == ma_ini_parser_start(buffer, strlen(buffer), my_ini_handler, NULL));
}

