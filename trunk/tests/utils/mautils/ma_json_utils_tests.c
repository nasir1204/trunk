/*
JSON utils tests
*/

#include "unity.h"
#include "unity_fixture.h"
#include "ma/internal/utils/json/ma_json_utils.h"
#include <string.h>


TEST_GROUP_RUNNER(json_utils_test_group)
{
    do {RUN_TEST_CASE(json_tests, create_json)} while (0);
    do {RUN_TEST_CASE(json_tests, add_elements)} while (0);
    do {RUN_TEST_CASE(json_tests, get_json_data)} while (0);

    do {RUN_TEST_CASE(json_tests, create_json_array)} while (0);
    do {RUN_TEST_CASE(json_tests, add_json_objects)} while (0);
    do {RUN_TEST_CASE(json_tests, get_json_array_data)} while (0);

    do {RUN_TEST_CASE(json_tests, json_release)} while (0);
    do {RUN_TEST_CASE(json_tests, json_array_release)} while (0);
}


TEST_SETUP(json_tests) {}

TEST_TEAR_DOWN(json_tests) {}

static ma_json_t *json = NULL;
static ma_json_array_t *json_array = NULL;

TEST(json_tests, create_json)
{
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_json_create(NULL));
    TEST_ASSERT(MA_OK == ma_json_create(&json));
    TEST_ASSERT_NOT_NULL(json);
}

TEST(json_tests, add_elements)
{
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_json_add_element(NULL, "Time", "2013-11-26T18:29:00"));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_json_add_element(json, NULL, "2013-11-26T18:29:00"));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_json_add_element(json, "Time", NULL));
    TEST_ASSERT(MA_OK == ma_json_add_element(json, "Time", "2013-11-26T18:29:00"));
    TEST_ASSERT(MA_OK == ma_json_add_element(json, "Severity", "Info"));
    TEST_ASSERT(MA_OK == ma_json_add_element(json, "Facility", "io.service"));
    TEST_ASSERT(MA_OK == ma_json_add_element(json, "Message", "configured io service"));
}

// Expected data based on the assumption that the data will be retrieved from the underlying table in the same order everytime.
const static char expected_data[] = "{\"Facility\":\"io.service\",\"Message\":\"configured io service\",\"Severity\":\"Info\",\"Time\":\"2013-11-26T18:29:00\"}";

TEST(json_tests, get_json_data)
{
    ma_bytebuffer_t *buffer = NULL;

    TEST_ASSERT(MA_OK == ma_bytebuffer_create(1024, &buffer));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_json_get_data(NULL, buffer));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_json_get_data(json, NULL));
    TEST_ASSERT(MA_OK == ma_json_get_data(json, buffer));

    TEST_ASSERT(0 == strcmp(expected_data, (char *)ma_bytebuffer_get_bytes(buffer)));
    TEST_ASSERT(MA_OK == ma_bytebuffer_release(buffer));
}

TEST(json_tests, create_json_array)
{
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_json_array_create(NULL));
    TEST_ASSERT(MA_OK == ma_json_array_create(&json_array));
    TEST_ASSERT_NOT_NULL(json_array);
}

TEST(json_tests, add_json_objects)
{
    // Is empty
    TEST_ASSERT(MA_TRUE == ma_json_array_is_empty(NULL));
    TEST_ASSERT(MA_TRUE == ma_json_array_is_empty(json_array));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_json_array_add_object(NULL, json));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_json_array_add_object(json_array, NULL));

    // Adding 5 JSON records into an array
    TEST_ASSERT(MA_OK == ma_json_array_add_object(json_array, json));
    TEST_ASSERT(MA_OK == ma_json_array_add_object(json_array, json));
    TEST_ASSERT(MA_OK == ma_json_array_add_object(json_array, json));
    TEST_ASSERT(MA_OK == ma_json_array_add_object(json_array, json));
    TEST_ASSERT(MA_OK == ma_json_array_add_object(json_array, json));

    // Is empty
    TEST_ASSERT(MA_FALSE == ma_json_array_is_empty(json_array));
}

const static char expected_array_data[] = "[{\"Facility\":\"io.service\",\"Message\":\"configured io service\",\"Severity\":\"Info\",\"Time\":\"2013-11-26T18:29:00\"},{\"Facility\":\"io.service\",\"Message\":\"configured io service\",\"Severity\":\"Info\",\"Time\":\"2013-11-26T18:29:00\"},{\"Facility\":\"io.service\",\"Message\":\"configured io service\",\"Severity\":\"Info\",\"Time\":\"2013-11-26T18:29:00\"},{\"Facility\":\"io.service\",\"Message\":\"configured io service\",\"Severity\":\"Info\",\"Time\":\"2013-11-26T18:29:00\"},{\"Facility\":\"io.service\",\"Message\":\"configured io service\",\"Severity\":\"Info\",\"Time\":\"2013-11-26T18:29:00\"}]";

TEST(json_tests, get_json_array_data)
{
    ma_bytebuffer_t *buffer = NULL;

    TEST_ASSERT(MA_OK == ma_bytebuffer_create(1024, &buffer));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_json_array_get_data(NULL, buffer));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_json_array_get_data(json_array, NULL));
    TEST_ASSERT(MA_OK == ma_json_array_get_data(json_array, buffer));

    TEST_ASSERT(0 == strcmp(expected_array_data, (char *)ma_bytebuffer_get_bytes(buffer)));
    TEST_ASSERT(MA_OK == ma_bytebuffer_release(buffer));
}

TEST(json_tests, json_array_release)
{
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_json_array_release(NULL));
    TEST_ASSERT(MA_OK == ma_json_array_release(json_array));
}

TEST(json_tests, json_release)
{
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_json_release(NULL));
    TEST_ASSERT(MA_OK == ma_json_release(json));
}
