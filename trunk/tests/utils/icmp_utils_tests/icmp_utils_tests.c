/*
ICMP UTs
*/

#include "unity.h"
#include "unity_fixture.h"
#include "ma/proxy/ma_proxy.h"
#include <string.h>
#include "ma/internal/utils/icmp/ma_icmp.h"
#include "../../../src/utils/icmp/ma_icmp_internal.h"

#include "ma/internal/defs/ma_object_defs.h"
#include "ma_ut_setup.h"

#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/core/msgbus/ma_msgbus_private.h"
#include "ma/internal/utils/repository/ma_repository_internal.h"
#include "ma/internal/services/repository/ma_repository_rank.h"

static ma_event_loop_t *ma_ev_loop = 0;
static ma_logger_t *logger = 0;

ma_context_t *g_ma_context = NULL;
ma_ut_setup_t *g_ut_setup = NULL;

TEST_GROUP_RUNNER(ma_icmp_tests_group)
{
	do {RUN_TEST_CASE(ma_icmp_tests, ma_icmp_url_list_create_release_test); } while (0);
	do {RUN_TEST_CASE(ma_icmp_tests, ma_icmp_policy_test); } while (0);
	do {RUN_TEST_CASE(ma_icmp_tests, ma_icmp_context_test); } while (0);
	 
}

TEST_SETUP(ma_icmp_tests)
{
	/* Set up ut context*/
	ma_ut_setup_create("icmp.tests", "ma.utils.icmp.test", &g_ut_setup);
    g_ma_context = ma_ut_setup_get_context(g_ut_setup);
}

TEST_TEAR_DOWN(ma_icmp_tests)
{
}

TEST(ma_icmp_tests, ma_icmp_url_list_create_release_test)
{
	ma_icmp_url_list_t *icmp_url_list = NULL;

	ma_icmp_url_list_create(&icmp_url_list);

	ma_icmp_url_list_release(icmp_url_list);
}

TEST(ma_icmp_tests, ma_icmp_policy_test)
{
	ma_icmp_policy_t *icmp_policy = NULL;
	ma_repository_rank_type_t rank_type = MA_REPOSITORY_RANK_BY_SUBNET_DISTANCE;

	ma_icmp_policy_create(icmp_policy, 250, 2500, rank_type);

}

TEST(ma_icmp_tests, ma_icmp_context_test)
{
	ma_icmp_context_t *icmp_context = NULL;
	ma_icmp_policy_t icmp_policy;
	ma_repository_rank_type_t rank_type = {MA_REPOSITORY_RANK_BY_SUBNET_DISTANCE};
	ma_icmp_methods_t icmp_methods;

	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_icmp_context_create(NULL, NULL));
	TEST_ASSERT(MA_OK == ma_icmp_context_create(&icmp_context, NULL));
	TEST_ASSERT(NULL == ma_icmp_context_get_data(icmp_context));

	ma_icmp_context_set_logger(icmp_context, MA_CONTEXT_GET_LOGGER(g_ma_context));
	ma_icmp_context_set_uv_loop(icmp_context,ma_event_loop_get_uv_loop(ma_msgbus_get_event_loop(MA_CONTEXT_GET_MSGBUS(g_ma_context))));

	
	icmp_policy.max_ping_time = 250;
	icmp_policy.max_subnet_distance = 5;
	icmp_policy.rank_type = rank_type;

	ma_icmp_context_set_policy(icmp_context, icmp_policy);

	ma_icmp_context_set_data(icmp_context, (void *)"icmp_test");

	//Set methods
	icmp_methods.get = ma_repository_rank_sitelist_get;
	icmp_methods.update = ma_repository_rank_sitelist_update;
	icmp_methods.stop = ma_repository_rank_stop;
	ma_icmp_context_set_method(icmp_context, &icmp_methods);
	
	{
		ma_icmp_policy_t tmp_icmp_policy;
		ma_icmp_methods_t tmp_methods;
		//get methods
		TEST_ASSERT(0 == strcmp("icmp_test",(const char*) ma_icmp_context_get_data(icmp_context)));
		TEST_ASSERT(MA_OK == ma_icmp_context_get_policy(icmp_context,&tmp_icmp_policy));
		TEST_ASSERT(tmp_icmp_policy.max_ping_time == icmp_policy.max_ping_time);
		TEST_ASSERT(tmp_icmp_policy.max_subnet_distance == icmp_policy.max_subnet_distance);
		TEST_ASSERT(tmp_icmp_policy.rank_type == icmp_policy.rank_type);
		//ma_icmp_context_get_methods(icmp_context, &tmp_methods);
		//TEST_ASSERT(tmp_methods.get == icmp_methods.get);


	}



}


