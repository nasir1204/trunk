/*****************************************************************************
Serialization and Deserialization Library unit tests covering the table variants.
******************************************************************************/
#include "unity.h"
#include "unity_fixture.h"
#include "ma/internal/utils/serialization/ma_serialize.h"
#include "ma/internal/utils/serialization/ma_deserialize.h"
#include <string.h>
#include <wchar.h>

TEST_GROUP_RUNNER(ma_serialize_table_test_group)
{
    do {RUN_TEST_CASE(ma_serialize_table_tests, ma_serializer_table_test_mix_variant_types)} while (0);
    do {RUN_TEST_CASE(ma_serialize_table_tests, ma_serializer_table_test_key_string_value_wstring)} while (0);
    do {RUN_TEST_CASE(ma_serialize_table_tests, ma_serializer_table_test_key_wstring_value_string)} while (0);
    do {RUN_TEST_CASE(ma_serialize_table_tests, ma_serializer_table_test_key_wstring_value_wstring)} while (0);
    do {RUN_TEST_CASE(ma_serialize_table_tests, ma_serializer_table_test_key_value_mix_character_width)} while (0);
}

TEST_SETUP(ma_serialize_table_tests)
{
}

TEST_TEAR_DOWN(ma_serialize_table_tests)
{
}

/*
Unit tests for serialization/de-serialization APIs for table variant.
*/

TEST(ma_serialize_table_tests, ma_serializer_table_test_mix_variant_types)
{
    /* 'ma_variant_t' type Pointer for storing the base address of variant object */
    ma_variant_t *variant_ptr_to_be_serialized = NULL;

    /* 'ma_variant_t' type Pointer for storing the table variant address to be serialized */
    ma_variant_t *table_variant_ptr_serialized = NULL;

    /* 'ma_variant_t' type Pointer for storing the table variant address to be deserialized */
    ma_variant_t *table_variant_ptr_deserialized = NULL;

    /* 'ma_serializer_t' type Pointer for storing the base address of serializer object */
    ma_serializer_t *serializer_ptr = NULL;

    /* 'ma_serializer_t' type Pointer for storing the base address of serializer object */
    ma_deserializer_t *deserializer_ptr = NULL;

    size_t actual_buf_length = 0;

    /* To hold the serialized buffer and its size */
    char *actual_serialized_buf = NULL;
    size_t serialized_buf_size = 0;

    ma_table_t *table_ptr = NULL;

    /* Equality Comparison Result */
    ma_bool_t comparison_result = 0;

    /* Sample Raw Buffer. To be used while creating raw buffer variant */
    ma_uint8_t raw_buffer[] = { 0x12, 0x23, 0x34, 0x45, 0x56, 0x67, 0x78, 0x89, 0x90 };

    /* Creating table of variants */
    TEST_ASSERT(ma_table_create(&table_ptr) == MA_OK);
    TEST_ASSERT(ma_serializer_create(&serializer_ptr) == MA_OK);

    /*
    Creating variants of various types -> pushing them into the table
    */

    /* NULL Variant */
    TEST_ASSERT(ma_variant_create(&variant_ptr_to_be_serialized) == MA_OK);
    TEST_ASSERT(ma_table_add_entry(table_ptr, "First Entry", variant_ptr_to_be_serialized) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_to_be_serialized) == MA_OK);

    /* 64 Bit unsigned integer Variant */
    TEST_ASSERT(ma_variant_create_from_uint64(0x12345678ABCDEF09ULL, &variant_ptr_to_be_serialized) == MA_OK);
    TEST_ASSERT(ma_table_add_entry(table_ptr, "Second Entry", variant_ptr_to_be_serialized) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_to_be_serialized) == MA_OK);

    /* 64 Bit signed integer Variant */
    TEST_ASSERT(ma_variant_create_from_int64(0x12345678ABCDEF09LL, &variant_ptr_to_be_serialized) == MA_OK);
    TEST_ASSERT(ma_table_add_entry(table_ptr, "Third Entry", variant_ptr_to_be_serialized) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_to_be_serialized) == MA_OK);

    /* Double Variant */
    TEST_ASSERT(ma_variant_create_from_double(1.23e+18, &variant_ptr_to_be_serialized) == MA_OK);
    TEST_ASSERT(ma_table_add_entry(table_ptr, "Fourth Entry", variant_ptr_to_be_serialized) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_to_be_serialized) == MA_OK);

    /* 32 Bit unsigned integer Variant */
    TEST_ASSERT(ma_variant_create_from_uint32(0x1234ABCDUL, &variant_ptr_to_be_serialized) == MA_OK);
    TEST_ASSERT(ma_table_add_entry(table_ptr, "Fifth Entry", variant_ptr_to_be_serialized) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_to_be_serialized) == MA_OK);

    /* 32 Bit signed integer Variant */
    TEST_ASSERT(ma_variant_create_from_int32(0xEFAB0123, &variant_ptr_to_be_serialized) == MA_OK);
    TEST_ASSERT(ma_table_add_entry(table_ptr, "Sixth Entry", variant_ptr_to_be_serialized) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_to_be_serialized) == MA_OK);

    /* Float Variant */
    TEST_ASSERT(ma_variant_create_from_float(123.45f, &variant_ptr_to_be_serialized) == MA_OK);
    TEST_ASSERT(ma_table_add_entry(table_ptr, "Seventh Entry", variant_ptr_to_be_serialized) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_to_be_serialized) == MA_OK);

    /* 16 Bit unsigned integer Variant */
    TEST_ASSERT(ma_variant_create_from_uint16(0x1234, &variant_ptr_to_be_serialized) == MA_OK);
    TEST_ASSERT(ma_table_add_entry(table_ptr, "Eighth Entry", variant_ptr_to_be_serialized) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_to_be_serialized) == MA_OK);

    /* 16 Bit signed integer Variant */
    TEST_ASSERT(ma_variant_create_from_int16(0xEFAB, &variant_ptr_to_be_serialized) == MA_OK);
    TEST_ASSERT(ma_table_add_entry(table_ptr, "Ninth Entry", variant_ptr_to_be_serialized) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_to_be_serialized) == MA_OK);

    /* 8 Bit unsigned integer Variant */
    TEST_ASSERT(ma_variant_create_from_uint8(0x12, &variant_ptr_to_be_serialized) == MA_OK);
    TEST_ASSERT(ma_table_add_entry(table_ptr, "Tenth Entry", variant_ptr_to_be_serialized) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_to_be_serialized) == MA_OK);

    /* 8 Bit signed integer Variant */
    TEST_ASSERT(ma_variant_create_from_int8(0xEF, &variant_ptr_to_be_serialized) == MA_OK);
    TEST_ASSERT(ma_table_add_entry(table_ptr, "Eleventh Entry", variant_ptr_to_be_serialized) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_to_be_serialized) == MA_OK);

    /* Boolean Variant */
    TEST_ASSERT(ma_variant_create_from_bool(123, &variant_ptr_to_be_serialized) == MA_OK);
    TEST_ASSERT(ma_table_add_entry(table_ptr, "Twelfth Entry", variant_ptr_to_be_serialized) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_to_be_serialized) == MA_OK);

    /* String Variant */
    TEST_ASSERT(ma_variant_create_from_string("MA Security Agent", 17, &variant_ptr_to_be_serialized) == MA_OK);
    TEST_ASSERT(ma_table_add_entry(table_ptr, "Thirteenth Entry", variant_ptr_to_be_serialized) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_to_be_serialized) == MA_OK);

    /* Wide Character String Variant */
    TEST_ASSERT(ma_variant_create_from_wstring(L"MA Security Agent$%$%\x002B", 22, &variant_ptr_to_be_serialized) == MA_OK);
    TEST_ASSERT(ma_table_add_entry(table_ptr, "Fourteenth Entry", variant_ptr_to_be_serialized) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_to_be_serialized) == MA_OK);

    /* Raw Data Variant */
    TEST_ASSERT(ma_variant_create_from_raw(raw_buffer, sizeof(raw_buffer)/sizeof(raw_buffer[0]), &variant_ptr_to_be_serialized) == MA_OK);
    TEST_ASSERT(ma_table_add_entry(table_ptr, "Fifteenth Entry", variant_ptr_to_be_serialized) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_to_be_serialized) == MA_OK);

    /* Creating Variant from the table */
    TEST_ASSERT(ma_variant_create_from_table(table_ptr, &table_variant_ptr_serialized) == MA_OK);

    /* A variant has been created from table, therefore, table can be released */
    TEST_ASSERT(ma_table_release(table_ptr)== MA_OK);

    /* Adding table variant to serializer */
    TEST_ASSERT(ma_serializer_add_variant(serializer_ptr, table_variant_ptr_serialized) == MA_OK);

    /* Retrieving size of serialized stream */
    TEST_ASSERT(ma_serializer_get(serializer_ptr, &actual_serialized_buf, &serialized_buf_size) == MA_OK);
    TEST_ASSERT(ma_serializer_length(serializer_ptr, &actual_buf_length) == MA_OK);
    TEST_ASSERT(serialized_buf_size == actual_buf_length);

    /* De-serializing the stream -> retrieving the table variant -> comparing it against the
       original table variant. As the equality comparison of table variants is already verified
       in the variant library tests, here, the variant comparison will be performed to ensure
       that the variant is retrieved correctly from the serialized stream. */
    TEST_ASSERT(ma_deserializer_create(&deserializer_ptr) == MA_OK);
    TEST_ASSERT(ma_deserializer_put(deserializer_ptr, actual_serialized_buf, serialized_buf_size) == MA_OK);
    TEST_ASSERT(ma_deserializer_get_next(deserializer_ptr, &table_variant_ptr_deserialized) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(table_variant_ptr_serialized, table_variant_ptr_deserialized, &comparison_result) == MA_OK);
    TEST_ASSERT(MA_TRUE == comparison_result);

    /* Releasing variant used for serialization and deserialization */
    TEST_ASSERT(ma_variant_release(table_variant_ptr_serialized) == MA_OK);
    TEST_ASSERT(ma_variant_release(table_variant_ptr_deserialized) == MA_OK);

    TEST_ASSERT(ma_serializer_destroy(serializer_ptr) == MA_OK);
    TEST_ASSERT(ma_deserializer_destroy(deserializer_ptr) == MA_OK);
}

/*
Serializing a list of table variants where each table contains one key-value pair (key -> String, Value -> Wstring).
*/
TEST(ma_serialize_table_tests, ma_serializer_table_test_key_string_value_wstring)
{
    ma_uint8_t counter = 0;

    /* Serialization and de-serialization object pointers */
    ma_serializer_t *serializer_ptr = NULL;
    ma_deserializer_t *deserializer_ptr = NULL;

    size_t actual_buf_length = 0;

    /* To hold the serialized buffer and its size */
    char *actual_serialized_buf = NULL;
    size_t serialized_buf_size = 0;

    // Keys
    const char *keys[5] = { "first_element", "second_element", "third_element", "fourth_element", "fifth_element" };

    // Values
    const wchar_t values[][10] = {
                                     { 0x9053, 0x53EF, 0x9053, 0x975E, 0x5E38, 0x02EB, 0x00 },
                                     { 0x1234, 0x0021, 0x007D, 0x004B, 0x02B4, 0x9053, 0x00 },
                                     { 0x0263, 0x53EF, 0x02BB, 0x975E, 0x5E38, 0x9053, 0x00 },
                                     { 0x02CF, 0x02C3, 0x9053, 0x02BC, 0x5E38, 0x9053, 0x00 },
                                     { 0x9053, 0x53EF, 0x02FE, 0x02B2, 0x5E38, 0x025D, 0x00 },
                                };

    // Creating serializer
    TEST_ASSERT(ma_serializer_create(&serializer_ptr) == MA_OK);

    // Creating table -> converting table to variant and adding it to serializer
    for(counter = 0; counter < sizeof(keys)/sizeof(keys[0]); counter++)
    {
        ma_variant_t *variant_ptr = NULL;
        ma_table_t *table_ptr = NULL;
        ma_variant_t *table_variant_ptr = NULL;

        /* Creating table storing single wchar variant */
        TEST_ASSERT(ma_table_create(&table_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_create_from_wstring(values[counter], wcslen(values[counter]), &variant_ptr) == MA_OK);
        TEST_ASSERT(ma_table_add_entry(table_ptr, keys[counter], variant_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

        /* Creating Variant from the table */
        TEST_ASSERT(ma_variant_create_from_table(table_ptr, &table_variant_ptr) == MA_OK);

        /* A variant has been created from table, therefore, table can be released */
        TEST_ASSERT(ma_table_release(table_ptr) == MA_OK);

        /* Adding table variant to serializer */
        TEST_ASSERT(ma_serializer_add_variant(serializer_ptr, table_variant_ptr) == MA_OK);

        /* Releasing variant */
        TEST_ASSERT(ma_variant_release(table_variant_ptr) == MA_OK);
    }

    /* Retrieving size of serialized stream */
    TEST_ASSERT(ma_serializer_get(serializer_ptr, &actual_serialized_buf, &serialized_buf_size) == MA_OK);
    TEST_ASSERT(ma_serializer_length(serializer_ptr, &actual_buf_length) == MA_OK);
    TEST_ASSERT(serialized_buf_size == actual_buf_length);

    /* De-serializing the stream -> Retrieving the variant and verifying the values against the original */
    TEST_ASSERT(ma_deserializer_create(&deserializer_ptr) == MA_OK);
    TEST_ASSERT(ma_deserializer_put(deserializer_ptr, actual_serialized_buf, serialized_buf_size) == MA_OK);

    for(counter = 0; counter < sizeof(keys)/sizeof(keys[0]); counter++)
    {
        ma_variant_t *variant_ptr = NULL;
        ma_table_t *table_ptr = NULL;
        ma_variant_t *table_variant_ptr = NULL;
        size_t size = 0;

        ma_wbuffer_t *wbuffer_ptr = NULL;
        const wchar_t *wbuffer_val = NULL;

        TEST_ASSERT(ma_deserializer_get_next(deserializer_ptr, &table_variant_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_get_table(table_variant_ptr, &table_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_release(table_variant_ptr) == MA_OK);

        // Table should hold only one element
        TEST_ASSERT(ma_table_size(table_ptr, &size) == MA_OK);
        TEST_ASSERT(1 == size);

        TEST_ASSERT(ma_table_get_value(table_ptr, keys[counter], &variant_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_get_wstring_buffer(variant_ptr, &wbuffer_ptr) == MA_OK);
        TEST_ASSERT(ma_wbuffer_get_string(wbuffer_ptr, &wbuffer_val, &size) == MA_OK);

        TEST_ASSERT(0 == wcscmp(wbuffer_val, values[counter]));

        ma_wbuffer_release(wbuffer_ptr);
        ma_variant_release(variant_ptr);
        ma_table_release(table_ptr);
    }

    // Destroying Serializer and Deserializer
    TEST_ASSERT(ma_serializer_destroy(serializer_ptr) == MA_OK);
    TEST_ASSERT(ma_deserializer_destroy(deserializer_ptr) == MA_OK);
}


/*
Serializing a list of table variants where each table contains one key-value pair (key -> Wstring, Value -> String).
*/
TEST(ma_serialize_table_tests, ma_serializer_table_test_key_wstring_value_string)
{
    ma_uint8_t counter = 0;

    /* Serialization and de-serialization object pointers */
    ma_serializer_t *serializer_ptr = NULL;
    ma_deserializer_t *deserializer_ptr = NULL;

    size_t actual_buf_length = 0;

    /* To hold the serialized buffer and its size */
    char *actual_serialized_buf = NULL;
    size_t serialized_buf_size = 0;

    /// Keys
    const wchar_t keys[][10] = {
                                   { 0x9053, 0x53EF, 0x9053, 0x975E, 0x04B1, 0x02EB, 0x00 },
                                   { 0x1234, 0x0021, 0x007D, 0x004B, 0x02B4, 0x9053, 0x00 },
                                   { 0x0263, 0x53EF, 0x02BB, 0x975E, 0x5E38, 0x0631, 0x00 },
                                   { 0x02CF, 0x02C3, 0x9053, 0x02BC, 0x067F, 0x9053, 0x00 },
                                   { 0x9053, 0x53EF, 0x02FE, 0x02B2, 0x5E38, 0x025D, 0x00 },
                              };

    // Values
    const char *values[5] = { "first_element", "second_element", "third_element", "fourth_element", "fifth_element" };

    // Creating serializer
    TEST_ASSERT(ma_serializer_create(&serializer_ptr) == MA_OK);

    // Creating table -> converting table to variant and adding it to serializer
    for(counter = 0; counter < sizeof(keys)/sizeof(keys[0]); counter++)
    {
        ma_variant_t *variant_ptr = NULL;
        ma_table_t *table_ptr = NULL;
        ma_variant_t *table_variant_ptr = NULL;

        /* Creating table storing single wchar variant */
        TEST_ASSERT(ma_table_create(&table_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_create_from_string(values[counter], strlen(values[counter]), &variant_ptr) == MA_OK);
        TEST_ASSERT(ma_table_add_wentry(table_ptr, keys[counter], variant_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

        /* Creating Variant from the table */
        TEST_ASSERT(ma_variant_create_from_table(table_ptr, &table_variant_ptr) == MA_OK);

        /* A variant has been created from table, therefore, table can be released */
        TEST_ASSERT(ma_table_release(table_ptr) == MA_OK);

        /* Adding table variant to serializer */
        TEST_ASSERT(ma_serializer_add_variant(serializer_ptr, table_variant_ptr) == MA_OK);

        /* Releasing variant */
        TEST_ASSERT(ma_variant_release(table_variant_ptr) == MA_OK);
    }

    /* Retrieving size of serialized stream */
    TEST_ASSERT(ma_serializer_get(serializer_ptr, &actual_serialized_buf, &serialized_buf_size) == MA_OK);
    TEST_ASSERT(ma_serializer_length(serializer_ptr, &actual_buf_length) == MA_OK);
    TEST_ASSERT(serialized_buf_size == actual_buf_length);

    /* De-serializing the stream -> Retrieving the variant and verifying the values against the original */
    TEST_ASSERT(ma_deserializer_create(&deserializer_ptr) == MA_OK);
    TEST_ASSERT(ma_deserializer_put(deserializer_ptr, actual_serialized_buf, serialized_buf_size) == MA_OK);

    for(counter = 0; counter < sizeof(keys)/sizeof(keys[0]); counter++)
    {
        ma_variant_t *variant_ptr = NULL;
        ma_table_t *table_ptr = NULL;
        ma_variant_t *table_variant_ptr = NULL;
        size_t size = 0;

        ma_buffer_t *buffer_ptr = NULL;
        const char *buffer_val = NULL;

        TEST_ASSERT(ma_deserializer_get_next(deserializer_ptr, &table_variant_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_get_table(table_variant_ptr, &table_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_release(table_variant_ptr) == MA_OK);

        // Table should hold only one element
        TEST_ASSERT(ma_table_size(table_ptr, &size) == MA_OK);
        TEST_ASSERT(1 == size);

        TEST_ASSERT(ma_table_get_wvalue(table_ptr, keys[counter], &variant_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_get_string_buffer(variant_ptr, &buffer_ptr) == MA_OK);
        TEST_ASSERT(ma_buffer_get_string(buffer_ptr, &buffer_val, &size) == MA_OK);

        TEST_ASSERT(0 == strcmp(buffer_val, values[counter]));

        ma_buffer_release(buffer_ptr);
        ma_variant_release(variant_ptr);
        ma_table_release(table_ptr);
    }

    // Destroying Serializer and Deserializer
    TEST_ASSERT(ma_serializer_destroy(serializer_ptr) == MA_OK);
    TEST_ASSERT(ma_deserializer_destroy(deserializer_ptr) == MA_OK);
}

/*
Serializing a list of table variants where each table contains one key-value pair (key -> Wstring, Value -> Wstring).
*/
TEST(ma_serialize_table_tests, ma_serializer_table_test_key_wstring_value_wstring)
{
    ma_uint8_t counter = 0;

    /* Serialization and de-serialization object pointers */
    ma_serializer_t *serializer_ptr = NULL;
    ma_deserializer_t *deserializer_ptr = NULL;

    size_t actual_buf_length = 0;

    /* To hold the serialized buffer and its size */
    char *actual_serialized_buf = NULL;
    size_t serialized_buf_size = 0;

    // Keys
    const wchar_t keys[][10] = {
                                      { 0x9053, 0x53EF, 0x9053, 0x975E, 0x5E38, 0x02EB, 0x00 },
                                   { 0x1234, 0x0021, 0x007D, 0x004B, 0x02B4, 0x9053, 0x00 },
                                   { 0x0263, 0x53EF, 0x02BB, 0x975E, 0x5E38, 0x04B1, 0x00 },
                                   { 0x02CF, 0x02C3, 0x9053, 0x02BC, 0x072A, 0x9053, 0x00 },
                                   { 0x9053, 0x53EF, 0x02FE, 0x02B2, 0x5E38, 0x025D, 0x00 },
                               };

    // Values
    const wchar_t values[][10] = {
                                     { 0x9053, 0x53EF, 0x9053, 0x975E, 0x05D5, 0x02EB, 0x00 },
                                     { 0x1234, 0x0021, 0x007D, 0x005C, 0x02B4, 0x9053, 0x00 },
                                     { 0x0263, 0x53EF, 0x02B3, 0x975E, 0x060D, 0x0631, 0x00 },
                                     { 0x02CF, 0x0251, 0x9053, 0x02BC, 0x5E38, 0x9053, 0x00 },
                                     { 0x9053, 0x53EF, 0x02FE, 0x02B2, 0x072A, 0x025D, 0x00 },
                                 };

    // Creating serializer
    TEST_ASSERT(ma_serializer_create(&serializer_ptr) == MA_OK);

    // Creating table -> converting table to variant and adding it to serializer
    for(counter = 0; counter < sizeof(keys)/sizeof(keys[0]); counter++)
    {
        ma_variant_t *variant_ptr = NULL;
        ma_table_t *table_ptr = NULL;
        ma_variant_t *table_variant_ptr = NULL;

        /* Creating table storing single wchar variant */
        TEST_ASSERT(ma_table_create(&table_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_create_from_wstring(values[counter], wcslen(values[counter]), &variant_ptr) == MA_OK);
        TEST_ASSERT(ma_table_add_wentry(table_ptr, keys[counter], variant_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

        /* Creating Variant from the table */
        TEST_ASSERT(ma_variant_create_from_table(table_ptr, &table_variant_ptr) == MA_OK);

        /* A variant has been created from table, therefore, table can be released */
        TEST_ASSERT(ma_table_release(table_ptr) == MA_OK);

        /* Adding table variant to serializer */
        TEST_ASSERT(ma_serializer_add_variant(serializer_ptr, table_variant_ptr) == MA_OK);

        /* Releasing variant */
        TEST_ASSERT(ma_variant_release(table_variant_ptr) == MA_OK);
    }

    /* Retrieving size of serialized stream */
    TEST_ASSERT(ma_serializer_get(serializer_ptr, &actual_serialized_buf, &serialized_buf_size) == MA_OK);
    TEST_ASSERT(ma_serializer_length(serializer_ptr, &actual_buf_length) == MA_OK);
    TEST_ASSERT(serialized_buf_size == actual_buf_length);

    /* De-serializing the stream -> Retrieving the variant and verifying the values against the original */
    TEST_ASSERT(ma_deserializer_create(&deserializer_ptr) == MA_OK);
    TEST_ASSERT(ma_deserializer_put(deserializer_ptr, actual_serialized_buf, serialized_buf_size) == MA_OK);

    for(counter = 0; counter < sizeof(keys)/sizeof(keys[0]); counter++)
    {
        ma_variant_t *variant_ptr = NULL;
        ma_table_t *table_ptr = NULL;
        ma_variant_t *table_variant_ptr = NULL;
        size_t size = 0;

        ma_wbuffer_t *wbuffer_ptr = NULL;
        const wchar_t *wbuffer_val = NULL;

        TEST_ASSERT(ma_deserializer_get_next(deserializer_ptr, &table_variant_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_get_table(table_variant_ptr, &table_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_release(table_variant_ptr) == MA_OK);

        // Table should hold only one element
        TEST_ASSERT(ma_table_size(table_ptr, &size) == MA_OK);
        TEST_ASSERT(1 == size);

        TEST_ASSERT(ma_table_get_wvalue(table_ptr, keys[counter], &variant_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_get_wstring_buffer(variant_ptr, &wbuffer_ptr) == MA_OK);
        TEST_ASSERT(ma_wbuffer_get_string(wbuffer_ptr, &wbuffer_val, &size) == MA_OK);

        TEST_ASSERT(0 == wcscmp(wbuffer_val, values[counter]));

        ma_wbuffer_release(wbuffer_ptr);
        ma_variant_release(variant_ptr);
        ma_table_release(table_ptr);
    }

    // Destroying Serializer and Deserializer
    TEST_ASSERT(ma_serializer_destroy(serializer_ptr) == MA_OK);
    TEST_ASSERT(ma_deserializer_destroy(deserializer_ptr) == MA_OK);
}


/*
Serializing a list of table variants where each table contains one key-value pair (both keys and values can be either string or wide-string).
*/
TEST(ma_serialize_table_tests, ma_serializer_table_test_key_value_mix_character_width)
{
    ma_uint8_t counter = 0;

    /* Serialization and de-serialization object pointers */
    ma_serializer_t *serializer_ptr = NULL;
    ma_deserializer_t *deserializer_ptr = NULL;

    size_t actual_buf_length = 0;

    /* To hold the serialized buffer and its size */
    char *actual_serialized_buf = NULL;
    size_t serialized_buf_size = 0;

    // Table Pointer and Table Variant Pointer
    ma_table_t *table_ptr = NULL;
    ma_variant_t *table_variant_ptr = NULL;
    size_t table_size = 0;

    // Keys
    const char *char_keys[5] = { "first_element", "second_element", "third_element", "fourth_element", "fifth_element" };

    const wchar_t wchar_keys[][10] = {
                                         { 0x9053, 0x53EF, 0x9053, 0x975E, 0x5E38, 0x02EB, 0x00 },
                                         { 0x1234, 0x0021, 0x007D, 0x004B, 0x02B4, 0x9053, 0x00 },
                                         { 0x0263, 0x53EF, 0x02BB, 0x975E, 0x5E38, 0x04B1, 0x00 },
                                         { 0x02CF, 0x02C3, 0x9053, 0x02BC, 0x072A, 0x9053, 0x00 },
                                         { 0x9053, 0x53EF, 0x02FE, 0x02B2, 0x5E38, 0x025D, 0x00 },
                                     };

    // Values
    const char *char_values[5] = { "first_value", "second_value", "third_value", "fourth_value", "fifth_value" };

    const wchar_t wchar_values[][10] = {
                                           { 0x9053, 0x53EF, 0x9053, 0x975E, 0x05D5, 0x02EB, 0x00 },
                                           { 0x1234, 0x0021, 0x007D, 0x005C, 0x02B4, 0x9053, 0x00 },
                                           { 0x0263, 0x53EF, 0x02B3, 0x975E, 0x060D, 0x0631, 0x00 },
                                           { 0x02CF, 0x0251, 0x9053, 0x02BC, 0x5E38, 0x9053, 0x00 },
                                           { 0x9053, 0x53EF, 0x02FE, 0x02B2, 0x072A, 0x025D, 0x00 },
                                      };

    // Creating serializer
    TEST_ASSERT(ma_serializer_create(&serializer_ptr) == MA_OK);

    // Creating Table
    TEST_ASSERT(ma_table_create(&table_ptr) == MA_OK);

    // Adding mix of string and wide char strings key-value pairs in the table
    for(counter = 0; counter < sizeof(char_keys)/sizeof(char_keys[0]); counter++)
    {
        ma_variant_t *variant_ptr = NULL;

        // Adding a wide-string variant value for string key
        TEST_ASSERT(ma_variant_create_from_wstring(wchar_values[counter], wcslen(wchar_values[counter]), &variant_ptr) == MA_OK);
        TEST_ASSERT(ma_table_add_entry(table_ptr, char_keys[counter], variant_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

        // Adding a string variant value for wide-string key
        TEST_ASSERT(ma_variant_create_from_string(char_values[counter], strlen(char_values[counter]), &variant_ptr) == MA_OK);
        TEST_ASSERT(ma_table_add_wentry(table_ptr, wchar_keys[counter], variant_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);
    }

    // Verifing that table contains 10 elements
    TEST_ASSERT(ma_table_size(table_ptr, &table_size) == MA_OK);
    TEST_ASSERT(10 == table_size);

    // Creating variant from the table and then, releasing table
    TEST_ASSERT(ma_variant_create_from_table(table_ptr, &table_variant_ptr) == MA_OK);
    TEST_ASSERT(ma_table_release(table_ptr) == MA_OK);

    // Adding table variant to serializer and releasing variant
    TEST_ASSERT(ma_serializer_add_variant(serializer_ptr, table_variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_release(table_variant_ptr) == MA_OK);

    // Retrieving size of serialized stream
    TEST_ASSERT(ma_serializer_get(serializer_ptr, &actual_serialized_buf, &serialized_buf_size) == MA_OK);
    TEST_ASSERT(ma_serializer_length(serializer_ptr, &actual_buf_length) == MA_OK);
    TEST_ASSERT(serialized_buf_size == actual_buf_length);

    // De-serializing the stream -> Retrieving the variant
    TEST_ASSERT(ma_deserializer_create(&deserializer_ptr) == MA_OK);
    TEST_ASSERT(ma_deserializer_put(deserializer_ptr, actual_serialized_buf, serialized_buf_size) == MA_OK);

    // Retrieving the table from variant
    TEST_ASSERT(ma_deserializer_get_next(deserializer_ptr, &table_variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_table(table_variant_ptr, &table_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_release(table_variant_ptr) == MA_OK);

    // Verifing that the retrieved table contains 10 elements
    TEST_ASSERT(ma_table_size(table_ptr, &table_size) == MA_OK);
    TEST_ASSERT(10 == table_size);

    // Verifying the table elements (keys - wstring, values - string)
    for(counter = 0; counter < sizeof(wchar_keys)/sizeof(wchar_keys[0]); counter++)
    {
        ma_variant_t *variant_ptr = NULL;
        size_t size = 0;

        ma_buffer_t *buffer_ptr = NULL;
        const char *buffer_val = NULL;

        TEST_ASSERT(ma_table_get_wvalue(table_ptr, wchar_keys[counter], &variant_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_get_string_buffer(variant_ptr, &buffer_ptr) == MA_OK);
        TEST_ASSERT(ma_buffer_get_string(buffer_ptr, &buffer_val, &size) == MA_OK);

        TEST_ASSERT(0 == strcmp(buffer_val, char_values[counter]));

        ma_buffer_release(buffer_ptr);
        ma_variant_release(variant_ptr);
    }

    // Verifying the table elements (keys - string, values - wstring)
    for(counter = 0; counter < sizeof(char_keys)/sizeof(char_keys[0]); counter++)
    {
        ma_variant_t *variant_ptr = NULL;
        size_t size = 0;

        ma_wbuffer_t *wbuffer_ptr = NULL;
        const wchar_t *wbuffer_val = NULL;

        TEST_ASSERT(ma_table_get_value(table_ptr, char_keys[counter], &variant_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_get_wstring_buffer(variant_ptr, &wbuffer_ptr) == MA_OK);
        TEST_ASSERT(ma_wbuffer_get_string(wbuffer_ptr, &wbuffer_val, &size) == MA_OK);

        TEST_ASSERT(0 == wcscmp(wbuffer_val, wchar_values[counter]));

        ma_wbuffer_release(wbuffer_ptr);
        ma_variant_release(variant_ptr);
    }

    TEST_ASSERT(ma_table_release(table_ptr) == MA_OK);

    // Destroying Serializer and Deserializer
    TEST_ASSERT(ma_serializer_destroy(serializer_ptr) == MA_OK);
    TEST_ASSERT(ma_deserializer_destroy(deserializer_ptr) == MA_OK);
}


