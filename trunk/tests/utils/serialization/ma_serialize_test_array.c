/*****************************************************************************
Serialization Library unit tests covering the array of variants.
******************************************************************************/
#include "unity.h"
#include "unity_fixture.h"
#include "ma/internal/utils/serialization/ma_serialize.h"
#include "ma/internal/utils/serialization/ma_deserialize.h"
#include <string.h>
#include <wchar.h>

TEST_GROUP_RUNNER(ma_serialize_array_test_group)
{
    do {RUN_TEST_CASE(ma_serialize_array_tests, ma_serializer_empty_array_test)} while (0);
    do {RUN_TEST_CASE(ma_serialize_array_tests, ma_serializer_array_of_mix_variants_test_01)} while (0);
    do {RUN_TEST_CASE(ma_serialize_array_tests, ma_serializer_array_of_mix_variants_test_02)} while (0);
    do {RUN_TEST_CASE(ma_serialize_array_tests, ma_serializer_array_of_wide_chars)} while (0);
    do {RUN_TEST_CASE(ma_serialize_array_tests, ma_serializer_table_test_array_of_tables)} while (0);
}

TEST_SETUP(ma_serialize_array_tests)
{
}

TEST_TEAR_DOWN(ma_serialize_array_tests)
{
}

/*
Serialization/De-serialization unit tests for an array of zero elements.
*/
TEST(ma_serialize_array_tests, ma_serializer_empty_array_test)
{
    size_t actual_buf_length = 0;

    /* 'ma_variant_t' type Pointer for storing the base address of variant
    object to be serialized */
    ma_variant_t *variant_ptr_serialized = NULL;

    /* 'ma_variant_t' type Pointer for storing the base address of variant (of array)
    object (Serialized and deserialized) */
    ma_variant_t *array_variant_ptr_serialized = NULL;
    ma_variant_t *array_variant_ptr_deserialized = NULL;

    /* 'ma_array_t' array type pointer */
    ma_array_t *array_ptr_serialized = NULL;

    /* 'ma_serializer_t' type Pointer for storing the base address of serializer object */
    ma_serializer_t *serializer_ptr = NULL;

    /* 'ma_serializer_t' type Pointer for storing the base address of de-serializer object */
    ma_deserializer_t *deserializer_ptr = NULL;

    /* To hold the serialized buffer and its size */
    char *actual_serialized_buf = NULL;
    size_t serialized_buf_size = 0;

    /* Equality Comparison Result */
    ma_bool_t comparison_result = 0;

    TEST_ASSERT(ma_serializer_create(NULL) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_serializer_create(&serializer_ptr) == MA_OK);

    TEST_ASSERT(ma_deserializer_create(NULL) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_deserializer_create(&deserializer_ptr) == MA_OK);

    /* Creating array of variants */
    TEST_ASSERT(ma_array_create(&array_ptr_serialized) == MA_OK);

    /* Creating Variant from the array */
    TEST_ASSERT(ma_variant_create_from_array(array_ptr_serialized, &array_variant_ptr_serialized) == MA_OK);

    // Variant is created from array, therefore, array can be released
    TEST_ASSERT(ma_array_release(array_ptr_serialized) == MA_OK);

    /* Adding variant (of array) to serializer */
    TEST_ASSERT(ma_serializer_add_variant(NULL, array_variant_ptr_serialized) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_serializer_add_variant(serializer_ptr, NULL) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_serializer_add_variant(serializer_ptr, array_variant_ptr_serialized) == MA_OK);

    /* Retrieving size of serialized stream */
    TEST_ASSERT(ma_serializer_get(NULL, &actual_serialized_buf, &serialized_buf_size) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_serializer_get(serializer_ptr, NULL, &serialized_buf_size) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_serializer_get(serializer_ptr, &actual_serialized_buf, NULL) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_serializer_get(serializer_ptr, &actual_serialized_buf, &serialized_buf_size) == MA_OK);
    TEST_ASSERT(ma_serializer_length(NULL, &actual_buf_length) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_serializer_length(serializer_ptr, NULL) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_serializer_length(serializer_ptr, &actual_buf_length) == MA_OK);
    TEST_ASSERT(serialized_buf_size == actual_buf_length);

    /* De-serializing the stream -> retrieving the variant (of array) -> comparing it against the
       original variant (of array). As the equality comparison of variant (of arrays) is already
       verified in the variant library tests, here, the variant comparison will be performed to
       ensure that the variant is retrieved correctly from the serialized stream. */
    TEST_ASSERT(ma_deserializer_put(NULL, actual_serialized_buf, serialized_buf_size) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_deserializer_put(deserializer_ptr, NULL, serialized_buf_size) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_deserializer_put(deserializer_ptr, actual_serialized_buf, serialized_buf_size) == MA_OK);
    TEST_ASSERT(ma_deserializer_get_next(NULL, &array_variant_ptr_deserialized) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_deserializer_get_next(deserializer_ptr, NULL) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_deserializer_get_next(deserializer_ptr, &array_variant_ptr_deserialized) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(array_variant_ptr_serialized, array_variant_ptr_deserialized, &comparison_result) == MA_OK);
    TEST_ASSERT(MA_TRUE == comparison_result);

    // Releasing variant used for serialization and deserialization
    TEST_ASSERT(ma_variant_release(array_variant_ptr_serialized) == MA_OK);
    TEST_ASSERT(ma_variant_release(array_variant_ptr_deserialized) == MA_OK);

    TEST_ASSERT(ma_serializer_destroy(NULL) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_serializer_destroy(serializer_ptr) == MA_OK);

    TEST_ASSERT(ma_deserializer_destroy(NULL) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_deserializer_destroy(deserializer_ptr) == MA_OK);
}

/*
Serialization/De-serialization unit tests for an array of variants (different types).
Array contains less than 15 variants.
*/
TEST(ma_serialize_array_tests, ma_serializer_array_of_mix_variants_test_01)
{
    size_t actual_buf_length = 0;

    /* 'ma_variant_t' type Pointer for storing the base address of variant
    object to be serialized */
    ma_variant_t *variant_ptr_serialized = NULL;

    /* 'ma_variant_t' type Pointer for storing the base address of variant (of array)
    object (Serialized and deserialized) */
    ma_variant_t *array_variant_ptr_serialized = NULL;

    ma_variant_t *array_variant_ptr_deserialized = NULL;

    /* 'ma_array_t' array type pointer */
    ma_array_t *array_ptr_serialized = NULL;

    /* 'ma_serializer_t' type Pointer for storing the base address of serializer object */
    ma_serializer_t *serializer_ptr = NULL;

    /* 'ma_serializer_t' type Pointer for storing the base address of de-serializer object */
    ma_deserializer_t *deserializer_ptr = NULL;

    /* To hold the serialized buffer and its size */
    char *actual_serialized_buf = NULL;
    size_t serialized_buf_size = 0;

    /* Equality Comparison Result */
    ma_bool_t comparison_result = 0;

    TEST_ASSERT(ma_serializer_create(&serializer_ptr) == MA_OK);
    TEST_ASSERT(ma_deserializer_create(&deserializer_ptr) == MA_OK);

    /* Creating array of variants */
    TEST_ASSERT(ma_array_create(&array_ptr_serialized) == MA_OK);

    /*
    Creating variants of various types -> pushing them into the array -> Creating variant from array
    */

    /* NULL Variant */
    TEST_ASSERT(ma_variant_create(&variant_ptr_serialized) == MA_OK);

    TEST_ASSERT(ma_array_push(array_ptr_serialized, variant_ptr_serialized) == MA_OK);

    TEST_ASSERT(ma_variant_release(variant_ptr_serialized) == MA_OK);


    /* 64 Bit unsigned integer Variant */
    TEST_ASSERT(ma_variant_create_from_uint64(0x12345678ABCDEF09ULL, &variant_ptr_serialized) == MA_OK);
    TEST_ASSERT(ma_array_push(array_ptr_serialized, variant_ptr_serialized) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_serialized) == MA_OK);

    /* 64 Bit signed integer Variant */
    TEST_ASSERT(ma_variant_create_from_int64(0xABCDEF0123456789LL, &variant_ptr_serialized) == MA_OK);
    TEST_ASSERT(ma_array_push(array_ptr_serialized, variant_ptr_serialized) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_serialized) == MA_OK);


    /* Double Variant */
    TEST_ASSERT(ma_variant_create_from_double(1.23e+18, &variant_ptr_serialized) == MA_OK);
    TEST_ASSERT(ma_array_push(array_ptr_serialized, variant_ptr_serialized) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_serialized) == MA_OK);


    /* 32 Bit unsigned integer Variant */
    TEST_ASSERT(ma_variant_create_from_uint32(0x1234ABCDUL, &variant_ptr_serialized) == MA_OK);
    TEST_ASSERT(ma_array_push(array_ptr_serialized, variant_ptr_serialized) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_serialized) == MA_OK);


    /* 32 Bit signed integer Variant */
    TEST_ASSERT(ma_variant_create_from_int32(0xEFAB0123L, &variant_ptr_serialized) == MA_OK);
    TEST_ASSERT(ma_array_push(array_ptr_serialized, variant_ptr_serialized) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_serialized) == MA_OK);


    /* Float Variant */
    TEST_ASSERT(ma_variant_create_from_float(123.45f, &variant_ptr_serialized) == MA_OK);
    TEST_ASSERT(ma_array_push(array_ptr_serialized, variant_ptr_serialized) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_serialized) == MA_OK);


    /* 16 Bit unsigned integer Variant */
    TEST_ASSERT(ma_variant_create_from_uint16(0x1234, &variant_ptr_serialized) == MA_OK);
    TEST_ASSERT(ma_array_push(array_ptr_serialized, variant_ptr_serialized) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_serialized) == MA_OK);


    /* 16 Bit signed integer Variant */
    TEST_ASSERT(ma_variant_create_from_int16(0xEFAB, &variant_ptr_serialized) == MA_OK);
    TEST_ASSERT(ma_array_push(array_ptr_serialized, variant_ptr_serialized) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_serialized) == MA_OK);


    /* 8 Bit unsigned integer Variant */
    TEST_ASSERT(ma_variant_create_from_uint8(0xCC, &variant_ptr_serialized) == MA_OK);
    TEST_ASSERT(ma_array_push(array_ptr_serialized, variant_ptr_serialized) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_serialized) == MA_OK);


    /* 8 Bit signed integer Variant */
    TEST_ASSERT(ma_variant_create_from_int8(0xEF, &variant_ptr_serialized) == MA_OK);
    TEST_ASSERT(ma_array_push(array_ptr_serialized, variant_ptr_serialized) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_serialized) == MA_OK);


    /* Boolean Variant */
    TEST_ASSERT(ma_variant_create_from_bool(123, &variant_ptr_serialized) == MA_OK);
    TEST_ASSERT(ma_array_push(array_ptr_serialized, variant_ptr_serialized) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_serialized) == MA_OK);

    /* Creating Variant from the array */
    TEST_ASSERT(ma_variant_create_from_array(array_ptr_serialized, &array_variant_ptr_serialized) == MA_OK);

    // Variant is created from array, therefore, array can be released
    TEST_ASSERT(ma_array_release(array_ptr_serialized) == MA_OK);

    /* Adding variant (of array) to serializer */
    TEST_ASSERT(ma_serializer_add_variant(serializer_ptr, array_variant_ptr_serialized) == MA_OK);

    /* Retrieving size of serialized stream */
    TEST_ASSERT(ma_serializer_get(serializer_ptr, &actual_serialized_buf, &serialized_buf_size) == MA_OK);
    TEST_ASSERT(ma_serializer_length(serializer_ptr, &actual_buf_length) == MA_OK);
    TEST_ASSERT(serialized_buf_size == actual_buf_length);

    /* De-serializing the stream -> retrieving the variant (of array) -> comparing it against the
       original variant (of array). As the equality comparison of variant (of arrays) is already
       verified in the variant library tests, here, the variant comparison will be performed to
       ensure that the variant is retrieved correctly from the serialized stream. */
    TEST_ASSERT(ma_deserializer_put(deserializer_ptr, actual_serialized_buf, serialized_buf_size) == MA_OK);
    TEST_ASSERT(ma_deserializer_get_next(deserializer_ptr, &array_variant_ptr_deserialized) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(array_variant_ptr_serialized, array_variant_ptr_deserialized, &comparison_result) == MA_OK);
    TEST_ASSERT(MA_TRUE == comparison_result);


    // Releasing variant used for serialization and deserialization
    TEST_ASSERT(ma_variant_release(array_variant_ptr_serialized) == MA_OK);
    TEST_ASSERT(ma_variant_release(array_variant_ptr_deserialized) == MA_OK);

    TEST_ASSERT(ma_serializer_destroy(serializer_ptr) == MA_OK);
    TEST_ASSERT(ma_deserializer_destroy(deserializer_ptr) == MA_OK);
}


/*
Unit tests for following serialization APIs for a array of variants (different types). Array
contains around 50 elements.
1) ma_serializer_create
2) ma_serializer_add_variant
3) ma_serializer_get
4) ma_serializer_destroy
5) ma_serializer_length
*/
TEST(ma_serialize_array_tests, ma_serializer_array_of_mix_variants_test_02)
{
    /* Loop iterator */
    ma_uint8_t counter = 0;

    size_t actual_buf_length = 0;

    /* 'ma_variant_t' type Pointer for storing the base address of variant
    object to be serialized */
    ma_variant_t *variant_ptr_serialized = NULL;

    /* 'ma_variant_t' type Pointer for storing the base address of variant (of array)
    object (Serialized and deserialized) */
    ma_variant_t *array_variant_ptr_serialized = NULL;

    ma_variant_t *array_variant_ptr_deserialized = NULL;

    /* 'ma_array_t' array type pointer */
    ma_array_t *array_ptr_serialized = NULL;

    /* 'ma_serializer_t' type Pointer for storing the base address of serializer object */
    ma_serializer_t *serializer_ptr = NULL;

    /* 'ma_serializer_t' type Pointer for storing the base address of de-serializer object */
    ma_deserializer_t *deserializer_ptr = NULL;

    /* To hold the serialized buffer and its size */
    char *actual_serialized_buf = NULL;
    size_t serialized_buf_size = 0;

    /* Equality Comparison Result */
    ma_bool_t comparison_result = 0;

    ma_uint64_t test_vector_1[] = {
                                  0xFFFFFFFFFFFFFFFFULL, 0x1234567890ABCDEFULL,
                                  0xFEDCBA0987654321ULL, 0x1234567891234567ULL,
                                  0x0101010101010101ULL, 0x1010101010101010ULL,
                                  0x3456789012345678ULL, 0x3453453453453451ULL,
                                  };

    ma_int64_t test_vector_2[] = {
                                 0xFFFFFFFFFFFFFFFFLL, 0x1234567890ABCDEFLL,
                                 0xFEDCBA0987654321LL, 0x1234567891234567LL,
                                 0x0101010101010101LL, 0x1010101010101010LL,
                                 0x3456789012345678LL, 0x3453453453453451LL,
                                 };

    ma_double_t test_vector_3[] = { 23.456, 1.001e11, 1e-308, 1e-100, 0.1234, 1e+308, -123.456, -2345.56}; // -123.4567 8 };

    ma_uint32_t test_vector_4[] = { 0xFFFFFFFFUL, 0x12345678UL, 0xFEDCBA09UL, 0x87654321UL,
                                    0x01010101UL, 0x10101010UL, 0x34567890UL };

    ma_int32_t test_vector_5[] = { 0xFFFFFFFFL, 0x12345678L, 0xFEDCBA09L, 0x87654321L,
                                   0x01010101L, 0x10101010L, 0x34567890L };

    ma_float_t test_vector_6[] = { 0.0f, 23.456f, -123.45f, 1.001f, 2345.56f,  0.12f, 1e+3f};

    ma_uint16_t test_vector_7[] = { 0xFFFF, 0x5678, 0xFEDC, 0x8765, 0x0101, 0x1010, 0xCDEF };

    ma_int16_t test_vector_8[] = { 0xFFFF, 0x1234, 0xFEDC, 0x4321, 0x0101, 0x1010, 0xCDEF };

    TEST_ASSERT(ma_serializer_create(&serializer_ptr) == MA_OK);
    TEST_ASSERT(ma_deserializer_create(&deserializer_ptr) == MA_OK);

    /* Creating array of variants */
    TEST_ASSERT(ma_array_create(&array_ptr_serialized) == MA_OK);

    /*
    Creating variants of various types -> pushing them into the array -> Creating variant from array
    */

    for(counter = 0; counter < sizeof(test_vector_1)/sizeof(test_vector_1[0]); counter++)
    {
        TEST_ASSERT(ma_variant_create_from_uint64(test_vector_1[counter], &variant_ptr_serialized) == MA_OK);
        TEST_ASSERT(ma_array_push(array_ptr_serialized, variant_ptr_serialized) == MA_OK);
        TEST_ASSERT(ma_variant_release(variant_ptr_serialized) == MA_OK);
    }

    for(counter = 0; counter < sizeof(test_vector_2)/sizeof(test_vector_2[0]); counter++)
    {
        TEST_ASSERT(ma_variant_create_from_int64(test_vector_2[counter], &variant_ptr_serialized) == MA_OK);
        TEST_ASSERT(ma_array_push(array_ptr_serialized, variant_ptr_serialized) == MA_OK);
        TEST_ASSERT(ma_variant_release(variant_ptr_serialized) == MA_OK);
    }

    for(counter = 0; counter < sizeof(test_vector_3)/sizeof(test_vector_3[0]); counter++)
    {
        TEST_ASSERT(ma_variant_create_from_double(test_vector_3[counter], &variant_ptr_serialized) == MA_OK);

        TEST_ASSERT(ma_array_push(array_ptr_serialized, variant_ptr_serialized) == MA_OK);

        TEST_ASSERT(ma_variant_release(variant_ptr_serialized) == MA_OK);
    }

    for(counter = 0; counter < sizeof(test_vector_4)/sizeof(test_vector_4[0]); counter++)
    {
        TEST_ASSERT(ma_variant_create_from_uint32(test_vector_4[counter], &variant_ptr_serialized) == MA_OK);
        TEST_ASSERT(ma_array_push(array_ptr_serialized, variant_ptr_serialized) == MA_OK);
        TEST_ASSERT(ma_variant_release(variant_ptr_serialized) == MA_OK);
    }

    for(counter = 0; counter < sizeof(test_vector_5)/sizeof(test_vector_5[0]); counter++)
    {
        TEST_ASSERT(ma_variant_create_from_int32(test_vector_5[counter], &variant_ptr_serialized) == MA_OK);
        TEST_ASSERT(ma_array_push(array_ptr_serialized, variant_ptr_serialized) == MA_OK);
        TEST_ASSERT(ma_variant_release(variant_ptr_serialized) == MA_OK);
    }

    for(counter = 0; counter < sizeof(test_vector_6)/sizeof(test_vector_6[0]); counter++)
    {
        TEST_ASSERT(ma_variant_create_from_float(test_vector_6[counter], &variant_ptr_serialized) == MA_OK);
        TEST_ASSERT(ma_array_push(array_ptr_serialized, variant_ptr_serialized) == MA_OK);
        TEST_ASSERT(ma_variant_release(variant_ptr_serialized) == MA_OK);
    }

    for(counter = 0; counter < sizeof(test_vector_7)/sizeof(test_vector_7[0]); counter++)
    {
        TEST_ASSERT(ma_variant_create_from_uint16(test_vector_7[counter], &variant_ptr_serialized) == MA_OK);
        TEST_ASSERT(ma_array_push(array_ptr_serialized, variant_ptr_serialized) == MA_OK);
        TEST_ASSERT(ma_variant_release(variant_ptr_serialized) == MA_OK);
    }

    for(counter = 0; counter < sizeof(test_vector_8)/sizeof(test_vector_8[0]); counter++)
    {
        TEST_ASSERT(ma_variant_create_from_int16(test_vector_8[counter], &variant_ptr_serialized) == MA_OK);
        TEST_ASSERT(ma_array_push(array_ptr_serialized, variant_ptr_serialized) == MA_OK);
        TEST_ASSERT(ma_variant_release(variant_ptr_serialized) == MA_OK);
    }

    /* Creating Variant from the array */
    TEST_ASSERT(ma_variant_create_from_array(array_ptr_serialized, &array_variant_ptr_serialized) == MA_OK);

    // Variant is created from array, therefore, array can be released
    TEST_ASSERT(ma_array_release(array_ptr_serialized) == MA_OK);

    /* Adding variant (of array) to serializer */
    TEST_ASSERT(ma_serializer_add_variant(serializer_ptr, array_variant_ptr_serialized) == MA_OK);

    /* Retrieving size of serialized stream */
    TEST_ASSERT(ma_serializer_get(serializer_ptr, &actual_serialized_buf, &serialized_buf_size) == MA_OK);
    TEST_ASSERT(ma_serializer_length(serializer_ptr, &actual_buf_length) == MA_OK);
    TEST_ASSERT(serialized_buf_size == actual_buf_length);

    /* De-serializing the stream -> retrieving the variant (of array) -> comparing it against the
       original variant (of array). As the equality comparison of variant (of arrays) is already
       verified in the variant library tests, here, the variant comparison will be performed to
       ensure that the variant is retrieved correctly from the serialized stream. */
    TEST_ASSERT(ma_deserializer_put(deserializer_ptr, actual_serialized_buf, serialized_buf_size) == MA_OK);
    TEST_ASSERT(ma_deserializer_get_next(deserializer_ptr, &array_variant_ptr_deserialized) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(array_variant_ptr_serialized, array_variant_ptr_deserialized, &comparison_result) == MA_OK);
    TEST_ASSERT(MA_TRUE == comparison_result);

    // Releasing variant used for serialization and deserialization
    TEST_ASSERT(ma_variant_release(array_variant_ptr_serialized) == MA_OK);
    TEST_ASSERT(ma_variant_release(array_variant_ptr_deserialized) == MA_OK);

    TEST_ASSERT(ma_serializer_destroy(serializer_ptr) == MA_OK);
    TEST_ASSERT(ma_deserializer_destroy(deserializer_ptr) == MA_OK);
}

/*
Serializing an array of wide char variants
*/
TEST(ma_serialize_array_tests, ma_serializer_array_of_wide_chars)
{
    ma_uint8_t counter = 0;

    // Serialization and de-serialization object pointers
    ma_serializer_t *serializer_ptr = NULL;
    ma_deserializer_t *deserializer_ptr = NULL;

    // Array variant
    ma_variant_t *array_variant_ptr = NULL;
    ma_array_t *array_ptr = NULL;

    size_t actual_buf_length = 0;

    // To hold the serialized buffer and its size
    char *actual_serialized_buf = NULL;
    size_t serialized_buf_size = 0;

    // Values
    wchar_t values[5][10] = {
                                { 0x9053, 0x53EF, 0x9053, 0x975E, 0x5E38, 0x02EB, 0x00 },
                                { 0x1234, 0x0021, 0x007D, 0x004B, 0x02B4, 0x9053, 0x00 },
                                { 0x0263, 0x53EF, 0x02BB, 0x975E, 0x5E38, 0x9053, 0x00 },
                                { 0x02CF, 0x02C3, 0x9053, 0x02BC, 0x5E38, 0x9053, 0x00 },
                                { 0x9053, 0x53EF, 0x02FE, 0x02B2, 0x5E38, 0x025D, 0x00 },
                            };

    // Creating serializer
    TEST_ASSERT(ma_serializer_create(&serializer_ptr) == MA_OK);

    // Creating array
    TEST_ASSERT(ma_array_create(&array_ptr) == MA_OK);

    // Pushing wide char variants into the array
    for(counter = 0; counter < 5; counter++)
    {
        ma_variant_t *variant_ptr = NULL;

        TEST_ASSERT(ma_variant_create_from_wstring(values[counter], wcslen(values[counter]), &variant_ptr) == MA_OK);
        TEST_ASSERT(ma_array_push(array_ptr, variant_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);
    }

    // Convering array to variant and then, releasing array
    TEST_ASSERT(ma_variant_create_from_array(array_ptr, &array_variant_ptr) == MA_OK);
    TEST_ASSERT(ma_array_release(array_ptr) == MA_OK);

    // Adding array variant to serializer and then, releasing array variant
    TEST_ASSERT(ma_serializer_add_variant(serializer_ptr, array_variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_release(array_variant_ptr) == MA_OK);

    /* Retrieving size of serialized stream */
    TEST_ASSERT(ma_serializer_get(serializer_ptr, &actual_serialized_buf, &serialized_buf_size) == MA_OK);
    TEST_ASSERT(ma_serializer_length(serializer_ptr, &actual_buf_length) == MA_OK);
    TEST_ASSERT(serialized_buf_size == actual_buf_length);

    /* De-serializing the stream -> Retrieving the variant and verifying the values against the original */
    TEST_ASSERT(ma_deserializer_create(&deserializer_ptr) == MA_OK);
    TEST_ASSERT(ma_deserializer_put(deserializer_ptr, actual_serialized_buf, serialized_buf_size) == MA_OK);

    TEST_ASSERT(ma_deserializer_get_next(deserializer_ptr, &array_variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_array(array_variant_ptr, &array_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_release(array_variant_ptr) == MA_OK);

    for(counter = 0; counter < 5; counter++)
    {
        ma_variant_t *variant_ptr = NULL;

        ma_wbuffer_t *wbuffer_ptr = NULL;
        const wchar_t *wbuffer_val = NULL;
        size_t size = 0;

        TEST_ASSERT(ma_array_get_element_at(array_ptr, counter + 1, &variant_ptr) == MA_OK); // Array index starts at 1
        TEST_ASSERT(ma_variant_get_wstring_buffer(variant_ptr, &wbuffer_ptr) == MA_OK);
        TEST_ASSERT(ma_wbuffer_get_string(wbuffer_ptr, &wbuffer_val, &size) == MA_OK);

        TEST_ASSERT(0 == wcscmp(wbuffer_val, values[counter]));

        ma_wbuffer_release(wbuffer_ptr);
        ma_variant_release(variant_ptr);
    }

    TEST_ASSERT(ma_array_release(array_ptr) == MA_OK);

    // Destroying Serializer and Deserializer
    TEST_ASSERT(ma_serializer_destroy(serializer_ptr) == MA_OK);
    TEST_ASSERT(ma_deserializer_destroy(deserializer_ptr) == MA_OK);
}


/*
Serializing a array of table variants where each table contains one key-value pair (key -> String, Value -> Wstring).
*/
TEST(ma_serialize_array_tests, ma_serializer_table_test_array_of_tables)
{
    ma_uint8_t counter = 0;

    /* Serialization and de-serialization object pointers */
    ma_serializer_t *serializer_ptr = NULL;
    ma_deserializer_t *deserializer_ptr = NULL;

    // Array pointer and array variant pointer
    ma_array_t *array_ptr = NULL;
    ma_variant_t *array_variant_ptr = NULL;

    size_t actual_buf_length = 0;

    /* To hold the serialized buffer and its size */
    char *actual_serialized_buf = NULL;
    size_t serialized_buf_size = 0;

    // Keys
    const char *keys[5] = { "first_element", "second_element", "third_element", "fourth_element", "fifth_element" };

    // Values
    const wchar_t values[][10] = {
                                     { 0x9053, 0x53EF, 0x9053, 0x975E, 0x5E38, 0x02EB, 0x00 },
                                     { 0x1234, 0x0021, 0x007D, 0x004B, 0x02B4, 0x9053, 0x00 },
                                     { 0x0263, 0x53EF, 0x02BB, 0x975E, 0x5E38, 0x9053, 0x00 },
                                     { 0x02CF, 0x02C3, 0x9053, 0x02BC, 0x5E38, 0x9053, 0x00 },
                                     { 0x9053, 0x53EF, 0x02FE, 0x02B2, 0x5E38, 0x025D, 0x00 },
                                };

    // Creating serializer
    TEST_ASSERT(ma_serializer_create(&serializer_ptr) == MA_OK);

    // Creating array
    TEST_ASSERT(ma_array_create(&array_ptr) == MA_OK);

    // Creating table -> converting table to variant -> pusing them into array
    for(counter = 0; counter < sizeof(keys)/sizeof(keys[0]); counter++)
    {
        ma_variant_t *variant_ptr = NULL;
        ma_table_t *table_ptr = NULL;
        ma_variant_t *table_variant_ptr = NULL;

        // Creating table storing single wchar variant
        TEST_ASSERT(ma_table_create(&table_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_create_from_wstring(values[counter], wcslen(values[counter]), &variant_ptr) == MA_OK);
        TEST_ASSERT(ma_table_add_entry(table_ptr, keys[counter], variant_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

        // Creating variant from the table
        TEST_ASSERT(ma_variant_create_from_table(table_ptr, &table_variant_ptr) == MA_OK);

        // A variant has been created from table, therefore, table can be released
        TEST_ASSERT(ma_table_release(table_ptr) == MA_OK);

        // Pushing table variant into the array
        TEST_ASSERT(ma_array_push(array_ptr, table_variant_ptr) == MA_OK);

        // Releasing variant
        TEST_ASSERT(ma_variant_release(table_variant_ptr) == MA_OK);
    }

    // Covering array to variant -> releasing array -> adding array variant to
    // serializer -> releasing array variant
    TEST_ASSERT(ma_variant_create_from_array(array_ptr, &array_variant_ptr) == MA_OK);
    TEST_ASSERT(ma_array_release(array_ptr) == MA_OK);
    TEST_ASSERT(ma_serializer_add_variant(serializer_ptr, array_variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_release(array_variant_ptr) == MA_OK);

    /* Retrieving size of serialized stream */
    TEST_ASSERT(ma_serializer_get(serializer_ptr, &actual_serialized_buf, &serialized_buf_size) == MA_OK);
    TEST_ASSERT(ma_serializer_length(serializer_ptr, &actual_buf_length) == MA_OK);
    TEST_ASSERT(serialized_buf_size == actual_buf_length);

    /* De-serializing the stream -> Retrieving the variant and verifying the values against the original */
    TEST_ASSERT(ma_deserializer_create(&deserializer_ptr) == MA_OK);
    TEST_ASSERT(ma_deserializer_put(deserializer_ptr, actual_serialized_buf, serialized_buf_size) == MA_OK);

    // Retrieving array variant from deserializer
    TEST_ASSERT(ma_deserializer_get_next(deserializer_ptr, &array_variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_array(array_variant_ptr, &array_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_release(array_variant_ptr) == MA_OK);

    for(counter = 0; counter < sizeof(keys)/sizeof(keys[0]); counter++)
    {
        ma_variant_t *variant_ptr = NULL;
        ma_table_t *table_ptr = NULL;
        ma_variant_t *table_variant_ptr = NULL;
        size_t size = 0;

        ma_wbuffer_t *wbuffer_ptr = NULL;
        const wchar_t *wbuffer_val = NULL;

        // Retrieving table variant at each array position
        TEST_ASSERT(ma_array_get_element_at(array_ptr, counter + 1, &table_variant_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_get_table(table_variant_ptr, &table_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_release(table_variant_ptr) == MA_OK);

        // Table should hold only one element
        TEST_ASSERT(ma_table_size(table_ptr, &size) == MA_OK);
        TEST_ASSERT(1 == size);

        TEST_ASSERT(ma_table_get_value(table_ptr, keys[counter], &variant_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_get_wstring_buffer(variant_ptr, &wbuffer_ptr) == MA_OK);
        TEST_ASSERT(ma_wbuffer_get_string(wbuffer_ptr, &wbuffer_val, &size) == MA_OK);

        TEST_ASSERT(0 == wcscmp(wbuffer_val, values[counter]));

        ma_wbuffer_release(wbuffer_ptr);
        ma_variant_release(variant_ptr);
        ma_table_release(table_ptr);
    }

    TEST_ASSERT(ma_array_release(array_ptr) == MA_OK);

    // Destroying Serializer and Deserializer
    TEST_ASSERT(ma_serializer_destroy(serializer_ptr) == MA_OK);
    TEST_ASSERT(ma_deserializer_destroy(deserializer_ptr) == MA_OK);
}





