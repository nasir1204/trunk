/*****************************************************************************
Serialization and De-serialization Library unit tests covering the raw buffers.
******************************************************************************/
#include "unity.h"
#include "unity_fixture.h"
#include "ma/internal/utils/serialization/ma_serialize.h"
#include "ma/internal/utils/serialization/ma_deserialize.h"

TEST_GROUP_RUNNER(ma_serialize_raw_buffer_test_group)
{
    do {RUN_TEST_CASE(ma_serialize_raw_buffer_tests, ma_serializer_raw_buffer_test_char_stream)} while (0);
    do {RUN_TEST_CASE(ma_serialize_raw_buffer_tests, ma_serializer_raw_buffer_test_string)} while (0);
    do {RUN_TEST_CASE(ma_serialize_raw_buffer_tests, ma_serializer_raw_buffer_test_raw_stream)} while (0);
    do {RUN_TEST_CASE(ma_serialize_raw_buffer_tests, ma_serializer_reset_test)} while (0);
}

TEST_SETUP(ma_serialize_raw_buffer_tests)
{
}

TEST_TEAR_DOWN(ma_serialize_raw_buffer_tests)
{
}

/*
Unit tests for serialization/de-serialization APIs for raw buffer variant (containing
character stream).
*/
TEST(ma_serialize_raw_buffer_tests, ma_serializer_raw_buffer_test_char_stream)
{
    /* 'ma_variant_t' type Pointer for storing the variant address to be serialized */
    ma_variant_t *variant_ptr_to_be_serialized = NULL;

    /* 'ma_variant_t' type Pointer for storing the variant address to be deserialized */
    ma_variant_t *variant_ptr_to_be_deserialized = NULL;

    /* 'ma_serializer_t' type Pointer for storing the base address of serializer object */
    ma_serializer_t *serializer_ptr = NULL;

    /* 'ma_serializer_t' type Pointer for storing the base address of serializer object */
    ma_deserializer_t *deserializer_ptr = NULL;

    /* To hold the serialized buffer and its size */
    char *actual_serialized_buf = NULL;
    size_t serialized_buf_size = 0;

    /* To hold the serialized buffer size returned by ma_serializer_length API */
    size_t actual_buf_length = 0;

    /* Equality Comparison Result */
    ma_bool_t comparison_result = 0;

    ma_uint8_t raw_buffer[] = { 0x12, 0x23, 0x34, 0x45, 0x56, 0x67, 0x78, 0x89, 0x90, ' ', 'a',
                                'c', 0x87, ' ', 'A', 'B', 'C', 'M', 'A', ' ', 'S', 'e', 'c', 'u',
                                'r', 'i', 't', 'y', ' ', 'A', 'g', 'e', 'n', 't', 0x23,
                                '!', '@', '#', '$', '%', '^', '&', '*', '!', '@', '#', '$',
                                '%', '^', 'F', 'G', 'j', 'i', 'z', 'T', 'h', 'i', 's', ' ',
                                'i', 's', '/', ']', ' ', 'a', ' ', 's', 'e', 'r',
                                'i', 'a', 'l', 'i', 'z', 'e', 'r', ' ', 'r', 'a', 'w', ' ',
                                'b', 'u', 'f', 'f', 'e', 'r' };

    /*--------------------------------------------------------------------------*/
    /* Creating serializer and de-serializer */
    TEST_ASSERT(ma_serializer_create(&serializer_ptr) == MA_OK);

    /* Creating Raw Buffer variant */
    TEST_ASSERT(ma_variant_create_from_raw(raw_buffer, sizeof(raw_buffer)/sizeof(raw_buffer[0]), &variant_ptr_to_be_serialized) == MA_OK);

    /* Adding raw buffer variant to serializer */
    TEST_ASSERT(ma_serializer_add_variant(serializer_ptr, variant_ptr_to_be_serialized) == MA_OK);

    /* Retrieving size of serialized stream */
    TEST_ASSERT(ma_serializer_get(serializer_ptr, &actual_serialized_buf, &serialized_buf_size) == MA_OK);
    TEST_ASSERT(ma_serializer_length(serializer_ptr, &actual_buf_length) == MA_OK);
    TEST_ASSERT(serialized_buf_size == actual_buf_length);

    /* De-serializing the stream -> retrieving the variant -> comparing it against the original
       variant. As the equality comparison of variant (raw buffer) is already verified in the
       variant library tests, here, the variant comparison will be performed to ensure that the
       variant is retrieved correctly from the serialized stream. */
    TEST_ASSERT(ma_deserializer_create(&deserializer_ptr) == MA_OK);
    TEST_ASSERT(ma_deserializer_put(deserializer_ptr, actual_serialized_buf, serialized_buf_size) == MA_OK);
    TEST_ASSERT(ma_deserializer_get_next(deserializer_ptr, &variant_ptr_to_be_deserialized) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_to_be_serialized, variant_ptr_to_be_deserialized, &comparison_result) == MA_OK);
    TEST_ASSERT(MA_TRUE == comparison_result);

    // Releasing variant used for serialization and deserialization
    TEST_ASSERT(ma_variant_release(variant_ptr_to_be_serialized) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_to_be_deserialized) == MA_OK);

    TEST_ASSERT(ma_serializer_destroy(serializer_ptr) == MA_OK);
    TEST_ASSERT(ma_deserializer_destroy(deserializer_ptr) == MA_OK);
}

/*
Unit tests for serialization/de-serialization APIs for raw buffer variant (containing
strings).
*/
TEST(ma_serialize_raw_buffer_tests, ma_serializer_raw_buffer_test_string)
{
    /* 'ma_variant_t' type Pointer for storing the variant address to be serialized */
    ma_variant_t *variant_ptr_to_be_serialized = NULL;

    /* 'ma_variant_t' type Pointer for storing the variant address to be deserialized */
    ma_variant_t *variant_ptr_to_be_deserialized = NULL;

    /* 'ma_serializer_t' type Pointer for storing the base address of serializer object */
    ma_serializer_t *serializer_ptr = NULL;

    /* 'ma_serializer_t' type Pointer for storing the base address of serializer object */
    ma_deserializer_t *deserializer_ptr = NULL;

    /* Loop iterator */
    ma_uint8_t counter = 0;

    /* To hold the serialized buffer and its size */
    char *actual_serialized_buf = NULL;
    size_t serialized_buf_size = 0;

    /* To hold the serialized buffer size returned by ma_serializer_length API */
    size_t actual_buf_length = 0;

    /* Equality Comparison Result */
    ma_bool_t comparison_result = 0;

    struct LookupTable
    {
        const char *test_buffer;            // Test Raw Buffer
        size_t test_buffer_length;          // Size of test raw buffer
    };

    const struct LookupTable test_vector[] =
    {
        { "MA Security Agent",                                                                    17 },
        { "McAfee Security 12345654569587Agent",                                                  35 },
        { "MCAFEE SECURITY AGENT",                                                                 6 },
        { "MCAFEE SECURITY AGENT 1.0",                                                            25 },
        { "$2345678901234567890",                                                                 20 },
        { "!@@#$%%^^&*&&(*(**(**)()",                                                             24 },
        { "~BCD1234$%@$%",                                                                        12 },
        { "~!@#$%^&*$^%^%$^#^%&^%&^%",                                                            25 },
        { " BCDEFGHIJKLMNOPQRSTUVWXYZabcdefghi jklmnopqrstuvwxyz1234567890!@#$%^&*()_+-=<>?:\"",  82 },
        { "",                                                                                      0 },
    };

    /*--------------------------------------------------------------------------*/
    for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        /* Creating Serializer */
        TEST_ASSERT(ma_serializer_create(&serializer_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_create_from_raw(test_vector[counter].test_buffer, test_vector[counter].test_buffer_length, &variant_ptr_to_be_serialized) == MA_OK);

        /* Adding raw buffer variant to serializer */
        TEST_ASSERT(ma_serializer_add_variant(serializer_ptr, variant_ptr_to_be_serialized) == MA_OK);

        /* Retrieving size of serialized stream */
        TEST_ASSERT(ma_serializer_get(serializer_ptr, &actual_serialized_buf, &serialized_buf_size) == MA_OK);
        TEST_ASSERT(ma_serializer_length(serializer_ptr, &actual_buf_length) == MA_OK);
        TEST_ASSERT(serialized_buf_size == actual_buf_length);

        /* De-serializing the stream -> retrieving the variant -> comparing it against the original
           variant. As the equality comparison of variant (raw buffer) is already verified in the
           variant library tests, here, the variant comparison will be performed to ensure that the
           variant is retrieved correctly from the serialized stream. */
        TEST_ASSERT(ma_deserializer_create(&deserializer_ptr) == MA_OK);
        TEST_ASSERT(ma_deserializer_put(deserializer_ptr, actual_serialized_buf, serialized_buf_size) == MA_OK);
        TEST_ASSERT(ma_deserializer_get_next(deserializer_ptr, &variant_ptr_to_be_deserialized) == MA_OK);
        TEST_ASSERT(ma_variant_is_equal(variant_ptr_to_be_serialized, variant_ptr_to_be_deserialized, &comparison_result) == MA_OK);
        TEST_ASSERT(MA_TRUE == comparison_result);

        TEST_ASSERT(ma_serializer_destroy(serializer_ptr) == MA_OK);
        TEST_ASSERT(ma_deserializer_destroy(deserializer_ptr) == MA_OK);

        /* Deallocating variant */
        ma_variant_release(variant_ptr_to_be_deserialized);

        /* Deallocating variant */
        ma_variant_release(variant_ptr_to_be_serialized);
    }
}

/*
Unit tests for serialization/de-serialization APIs for raw buffer variant (containing
raw data stream). Raw data stream is created by considering a structure instance as a
sequence of bytes.
*/
TEST(ma_serialize_raw_buffer_tests, ma_serializer_raw_buffer_test_raw_stream)
{
    /* 'ma_variant_t' type Pointer for storing the variant address to be serialized */
    ma_variant_t *variant_ptr_to_be_serialized = NULL;

    /* 'ma_variant_t' type Pointer for storing the variant address to be deserialized */
    ma_variant_t *variant_ptr_to_be_deserialized = NULL;

    /* 'ma_serializer_t' type Pointer for storing the base address of serializer object */
    ma_serializer_t *serializer_ptr = NULL;

    /* 'ma_serializer_t' type Pointer for storing the base address of serializer object */
    ma_deserializer_t *deserializer_ptr = NULL;

    /* To hold the serialized buffer and its size */
    char *actual_serialized_buf = NULL;
    size_t serialized_buf_size = 0;

    /* To hold the serialized buffer size returned by ma_serializer_length API */
    size_t actual_buf_length = 0;

    /* Equality Comparison Result */
    ma_bool_t comparison_result = 0;

    /* An instance of this structure acts as the raw buffer stream */
    struct test_struct
    {
        ma_uint8_t int_data;
        char char_data[300];
        ma_float_t float_data;
        ma_bool_t boolean_data;
    };

    struct test_struct raw_buffer = { 0x12,
                                      "This is a Next-Gen McAfee Agent software - Variant Library raw buffer test."
                                      "#$@#%$#%$#^%^%^%&\045435435345454uy32[312y3o2i334343rj 43irh4jr4ijr4[prj34r"
                                      "efoehrjoeirhjeohjnls[pei32hnelme;lcjepoeup28939pedklncel;fkjcrepkfm clekfwef"
                                      "!@#$%^&*()_+01234567890123456789",
                                      -2.3f,
                                      1};


    /*--------------------------------------------------------------------------*/
    /* Creating serializer and de-serializer */
    TEST_ASSERT(ma_serializer_create(&serializer_ptr) == MA_OK);

    /* Creating Raw Buffer variant */
    TEST_ASSERT(ma_variant_create_from_raw((const void *)&raw_buffer, sizeof(raw_buffer), &variant_ptr_to_be_serialized) == MA_OK);

    /* Adding raw buffer variant to serializer */
    TEST_ASSERT(ma_serializer_add_variant(serializer_ptr, variant_ptr_to_be_serialized) == MA_OK);

    /* Retrieving size of serialized stream */
    TEST_ASSERT(ma_serializer_get(serializer_ptr, &actual_serialized_buf, &serialized_buf_size) == MA_OK);
    TEST_ASSERT(ma_serializer_length(serializer_ptr, &actual_buf_length) == MA_OK);
    TEST_ASSERT(serialized_buf_size == actual_buf_length);

    /* De-serializing the stream -> retrieving the variant -> comparing it against the original
       variant. As the equality comparison of variant (raw buffer) is already verified in the
       variant library tests, here, the variant comparison will be performed to ensure that the
       variant is retrieved correctly from the serialized stream. */
    TEST_ASSERT(ma_deserializer_create(&deserializer_ptr) == MA_OK);
    TEST_ASSERT(ma_deserializer_put(deserializer_ptr, actual_serialized_buf, serialized_buf_size) == MA_OK);
    TEST_ASSERT(ma_deserializer_get_next(deserializer_ptr, &variant_ptr_to_be_deserialized) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_to_be_serialized, variant_ptr_to_be_deserialized, &comparison_result) == MA_OK);
    TEST_ASSERT(1 == comparison_result);

    // Releasing variant used for serialization and deserialization
    TEST_ASSERT(ma_variant_release(variant_ptr_to_be_serialized) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_to_be_deserialized) == MA_OK);

    TEST_ASSERT(ma_serializer_destroy(serializer_ptr) == MA_OK);
    TEST_ASSERT(ma_deserializer_destroy(deserializer_ptr) == MA_OK);
}

TEST(ma_serialize_raw_buffer_tests, ma_serializer_reset_test)
{
    ma_serializer_t *serializer = NULL;

    TEST_ASSERT(ma_serializer_create(&serializer) == MA_OK);
    TEST_ASSERT(ma_serializer_reset(NULL) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_serializer_reset(serializer) == MA_OK);
    TEST_ASSERT(ma_serializer_destroy(serializer) == MA_OK);
}


