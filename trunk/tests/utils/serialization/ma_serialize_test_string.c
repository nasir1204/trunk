/*****************************************************************************
Serialization and Deserialization Library unit tests covering the single
character and wide-character strings.
******************************************************************************/
#include "unity.h"
#include "unity_fixture.h"
#include "ma/internal/utils/serialization/ma_serialize.h"
#include "ma/internal/utils/serialization/ma_deserialize.h"
#include <string.h>
#include <wchar.h>

TEST_GROUP_RUNNER(ma_serialize_string_test_group)
{
    do {RUN_TEST_CASE(ma_serialize_string_tests, empty_string)} while (0);
    do {RUN_TEST_CASE(ma_serialize_string_tests, ma_serializer_string_test_char_stream)} while (0);

    #ifdef MA_HAS_WCHAR_T
        do {RUN_TEST_CASE(ma_serialize_string_tests, ma_serializer_string_test_wchar_stream)} while (0);
    #endif

}

TEST_SETUP(ma_serialize_string_tests)
{
}

TEST_TEAR_DOWN(ma_serialize_string_tests)
{
}

TEST(ma_serialize_string_tests, empty_string) {
    /* Equality Comparison Result */
    ma_bool_t comparison_result = 0;

    /* 'ma_variant_t' type Pointer for storing the variant address to be deserialized */
    ma_variant_t *variant_ptr_to_be_deserialized = NULL;

    /* 'ma_serializer_t' type Pointer for storing the base address of serializer object */
    ma_serializer_t *serializer_ptr = NULL;

    /* 'ma_serializer_t' type Pointer for storing the base address of serializer object */
    ma_deserializer_t *deserializer_ptr = NULL;

    ma_variant_t *empty_string = NULL;
    /* To hold the serialized buffer and its size */
    char *actual_serialized_buf = NULL;
    size_t serialized_buf_size = 0;

    TEST_ASSERT(MA_OK == ma_variant_create_from_string("", strlen(""), &empty_string));

    /* Creating Serializer */
    TEST_ASSERT(ma_serializer_create(&serializer_ptr) == MA_OK);
    TEST_ASSERT(ma_deserializer_create(&deserializer_ptr) == MA_OK);

    /* Adding string variant to serializer */
    TEST_ASSERT(ma_serializer_add_variant(serializer_ptr, empty_string) == MA_OK);

    /* Retrieving serialized stream */
    TEST_ASSERT(ma_serializer_get(serializer_ptr, &actual_serialized_buf, &serialized_buf_size) == MA_OK);
    TEST_ASSERT(ma_deserializer_put(deserializer_ptr, actual_serialized_buf, serialized_buf_size) == MA_OK);
    TEST_ASSERT(ma_deserializer_get_next(deserializer_ptr, &variant_ptr_to_be_deserialized) == MA_OK);

    TEST_ASSERT(ma_variant_is_equal(empty_string, variant_ptr_to_be_deserialized, &comparison_result) == MA_OK);

    TEST_ASSERT(MA_TRUE == comparison_result);

    TEST_ASSERT(MA_OK == ma_variant_release(empty_string));
    TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr_to_be_deserialized));

    TEST_ASSERT(ma_serializer_destroy(serializer_ptr) == MA_OK);
    TEST_ASSERT(ma_deserializer_destroy(deserializer_ptr) == MA_OK);
}

/*
Unit tests for serialization/de-serialization APIs for character string.
*/
TEST(ma_serialize_string_tests, ma_serializer_string_test_char_stream)
{
    /* 'ma_variant_t' type Pointer for storing the variant address to be serialized */
    ma_variant_t *variant_ptr_to_be_serialized = NULL;

    /* 'ma_variant_t' type Pointer for storing the variant address to be deserialized */
    ma_variant_t *variant_ptr_to_be_deserialized = NULL;

    /* 'ma_serializer_t' type Pointer for storing the base address of serializer object */
    ma_serializer_t *serializer_ptr = NULL;

    /* 'ma_serializer_t' type Pointer for storing the base address of serializer object */
    ma_deserializer_t *deserializer_ptr = NULL;

    /* Loop iterator */
    ma_uint8_t counter = 0;

    /* To hold the serialized buffer and its size */
    char *actual_serialized_buf = NULL;
    size_t serialized_buf_size = 0;

    /* Equality Comparison Result */
    ma_bool_t comparison_result = 0;


    struct LookupTable
    {
        const char *test_string;    // Test String passed to the ma_variant_create_from_string macro.
        size_t test_length;         // Test String length passed to the ma_variant_create_from_string macro.
    };

    const struct LookupTable test_vector[] =
    {
        { "MA Security Agent",                                                                    17 },
        { "McAfee Security 12345654569587Agent",                                                  35 },
        { "MCAFEE SECURITY AGENT",                                                                 6 },
        { "MCAFEE SECURITY AGENT 1.0",                                                           100 },
        { "12345678901234567890",                                                                 30 },
        { "!@@#$%%^^&*&&(*(**(**)()",                                                             24 },
        { "ABCD1234$%@$%",                                                                        12 },
        { "~!@#$%^&*$^%^%$^#^%&^%&^%",                                                            25 },
        { "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghi jklmnopqrstuvwxyz1234567890!@#$%^&*()_+-=<>?:\"",  82 },
        { "z",                                                                                     1 },
        { "",                                                                                      0 },
    };

    /*--------------------------------------------------------------------------*/
    for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        /* Creating Serializer */
        TEST_ASSERT(ma_serializer_create(&serializer_ptr) == MA_OK);

        /* Creating string variant */
        TEST_ASSERT(ma_variant_create_from_string(test_vector[counter].test_string, test_vector[counter].test_length, &variant_ptr_to_be_serialized) == MA_OK);

        /* Adding string variant to serializer */
        TEST_ASSERT(ma_serializer_add_variant(serializer_ptr, variant_ptr_to_be_serialized) == MA_OK);

        /* Retrieving serialized stream */
        TEST_ASSERT(ma_serializer_get(serializer_ptr, &actual_serialized_buf, &serialized_buf_size) == MA_OK);

        /* De-serializing the stream -> retrieving the string variant -> comparing it against the
           original string variant. */
        TEST_ASSERT(ma_deserializer_create(&deserializer_ptr) == MA_OK);
        TEST_ASSERT(ma_deserializer_put(deserializer_ptr, actual_serialized_buf, serialized_buf_size) == MA_OK);
        TEST_ASSERT(ma_deserializer_get_next(deserializer_ptr, &variant_ptr_to_be_deserialized) == MA_OK);
        TEST_ASSERT(ma_variant_is_equal(variant_ptr_to_be_serialized, variant_ptr_to_be_deserialized, &comparison_result) == MA_OK);
        TEST_ASSERT(MA_TRUE == comparison_result);

        // Compare n bytes bacause the serialized string may be lesser than the input string if the length given to
        // ma_variant_create_from_string API is less
        {
            ma_buffer_t *buffer_ptr = NULL;
            const char *buffer_val = NULL;
            size_t size = 0;

            TEST_ASSERT(ma_variant_get_string_buffer(variant_ptr_to_be_deserialized, &buffer_ptr) == MA_OK);
            TEST_ASSERT(ma_buffer_get_string(buffer_ptr, &buffer_val, &size) == MA_OK);
            TEST_ASSERT(0 == strncmp(buffer_val, test_vector[counter].test_string, test_vector[counter].test_length));
            TEST_ASSERT(ma_buffer_release(buffer_ptr) == MA_OK);
        }

        // Deallocation
        TEST_ASSERT(ma_serializer_destroy(serializer_ptr) == MA_OK);
        TEST_ASSERT(ma_deserializer_destroy(deserializer_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_release(variant_ptr_to_be_serialized) == MA_OK);
        TEST_ASSERT(ma_variant_release(variant_ptr_to_be_deserialized) == MA_OK);
    }
}

#ifdef MA_HAS_WCHAR_T

/*
Unit tests for serialization/de-serialization APIs for wide-character string.
*/
TEST(ma_serialize_string_tests, ma_serializer_string_test_wchar_stream)
{
    /* 'ma_variant_t' type Pointer for storing the variant address to be serialized */
    ma_variant_t *variant_ptr_to_be_serialized = NULL;

    /* 'ma_variant_t' type Pointer for storing the variant address to be deserialized */
    ma_variant_t *variant_ptr_to_be_deserialized = NULL;

    /* 'ma_serializer_t' type Pointer for storing the base address of serializer object */
    ma_serializer_t *serializer_ptr = NULL;

    /* 'ma_serializer_t' type Pointer for storing the base address of serializer object */
    ma_deserializer_t *deserializer_ptr = NULL;

    /* Loop iterator */
    ma_uint8_t counter = 0;

    /* To hold the serialized buffer and its size */
    char *actual_serialized_buf = NULL;
    size_t serialized_buf_size = 0;

    /* Equality Comparison Result */
    ma_bool_t comparison_result = 0;

    struct LookupTable
    {
        const wchar_t *test_string;    // Test String passed to the ma_variant_create_from_wstring macro.
        size_t test_length;            // Test String length passed to the ma_variant_create_from_wstring macro.
    };

    const struct LookupTable test_vector[] =
    {
        { L"MA Security Agent",                                                                    17 },
        { L"McAfee Security 12345654569587Agent",                                                  35 },
        { L"MCAFEE SECURITY AGENT",                                                                 6 },
        { L"MCAFEE SECURITY AGENT 1.0",                                                           100 },
        { L"12345678901234567890\x002C",                                                           30 },
        { L"!@@#$%%^^&*&&(*(**(**)(\x0041\x030A",                                                  24 },
        { L"ABCD1234$%@$%",                                                                        12 },
        { L"~!@#$%^&*$^%^%$^#^%&^%&^%",                                                            25 },
        { L"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghi jklmnopqrstuvwxyz1234567890!@#$%^&*()_+-=<>?:\"",  82 },
        { L"",                                                                                      0 },
    };

    /*--------------------------------------------------------------------------*/
    for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        /* Creating Serializer */
        TEST_ASSERT(ma_serializer_create(&serializer_ptr) == MA_OK);

        /* Creating string variant */
        TEST_ASSERT(ma_variant_create_from_wstring(test_vector[counter].test_string, test_vector[counter].test_length, &variant_ptr_to_be_serialized) == MA_OK);

        /* Adding string variant to serializer */
        TEST_ASSERT(ma_serializer_add_variant(serializer_ptr, variant_ptr_to_be_serialized) == MA_OK);

        /* Retrieving serialized stream */
        TEST_ASSERT(ma_serializer_get(serializer_ptr, &actual_serialized_buf, &serialized_buf_size) == MA_OK);

        /* De-serializing the stream -> retrieving the string variant -> comparing it against the
           original string variant. */
        TEST_ASSERT(ma_deserializer_create(&deserializer_ptr) == MA_OK);
        TEST_ASSERT(ma_deserializer_put(deserializer_ptr, actual_serialized_buf, serialized_buf_size) == MA_OK);
        TEST_ASSERT(ma_deserializer_get_next(deserializer_ptr, &variant_ptr_to_be_deserialized) == MA_OK);

        TEST_ASSERT(ma_variant_is_equal(variant_ptr_to_be_serialized, variant_ptr_to_be_deserialized, &comparison_result) == MA_OK);
        TEST_ASSERT(MA_TRUE == comparison_result);

        {
            ma_wbuffer_t *wbuffer_ptr = NULL;
            const wchar_t *wbuffer_val = NULL;
            size_t size = 0;

            TEST_ASSERT(ma_variant_get_wstring_buffer(variant_ptr_to_be_deserialized, &wbuffer_ptr) == MA_OK);
            TEST_ASSERT(ma_wbuffer_get_string(wbuffer_ptr, &wbuffer_val, &size) == MA_OK);

            TEST_ASSERT(0 == wcsncmp(wbuffer_val, test_vector[counter].test_string, test_vector[counter].test_length));

            TEST_ASSERT(ma_wbuffer_release(wbuffer_ptr) == MA_OK);

        }

        // Deallocation
        TEST_ASSERT(ma_serializer_destroy(serializer_ptr) == MA_OK);
        TEST_ASSERT(ma_deserializer_destroy(deserializer_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_release(variant_ptr_to_be_serialized) == MA_OK);
        TEST_ASSERT(ma_variant_release(variant_ptr_to_be_deserialized) == MA_OK);
    }
}

#endif


