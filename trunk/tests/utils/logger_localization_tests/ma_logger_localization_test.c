/*****************************************************************************
Logger unit tests
******************************************************************************/

#include "unity.h"
#include "unity_fixture.h"

#include "ma_ut_setup.h"
#include "ma_logger_service.h"
#include "ma/logger/ma_file_logger.h"
#include "ma/logger/ma_console_logger.h"

#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/defs/ma_logger_defs.h"
#include "ma_logger_localization_tests.h"
#include "ma/internal/utils/configurator/ma_configurator.h"

#ifndef MA_WINDOWS
#include <unistd.h>
#else
#include <windows.h>
#endif

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif

#define LOG_FACILITY_NAME "Logger_Test_Facility"

// Global objects
static ma_ut_setup_t *g_ut_setup = NULL;
static ma_service_t *ma_logger_service;

static const char service_name[] = "ma.service.logger";
static const char product_id[] = "ma.logger.service.test";

ma_logger_test_message_t *logger_test_message = NULL;
ma_locales_table_t1 locale;

typedef struct ut_logger_policy_s 
{
    const char *filter_pattern;
	const char *format_pattern;
	const char *log_size_limit;
	const char *app_logging_enable;
	const char *rollover_enable;
	const char *max_rollovers;
	const char *is_log_recording_enabled;
	const char *is_log_recording_debug_enabled;
	const char *log_records_size;
	const char *enable_lang_selection;

}ut_logger_policy_t;

static ut_logger_policy_t logger_policy = 
{
	"Logger_Test_Facility.*",
	DEFAULT_FILE_LOG_FORMAT_PATTERN,
	"10",
	"1",
	"1",
	"5",
	"1",
	"1",
	"200",
	"1",
};


ma_bool_t validate_resource_id(char *res_id)
{
	ma_bool_t msg_id_valid = MA_FALSE;
	ma_int32_t index;
	char *str_id_start = NULL, *comment_start = NULL;

	/* verify if the resource id represents a string in string table. This is done with the assumption that all the string id's will start with 'IDS_'*/
	for(index = 0; index < (sizeof(string_identifier_keys)/ sizeof(char*)) && (msg_id_valid == MA_FALSE); index++)
	{
		if((str_id_start = strstr(res_id, string_identifier_keys[index])) != NULL)
		{
			msg_id_valid = MA_TRUE;
		}
	}
	
	if(msg_id_valid == MA_TRUE)
	{
		// check if line is commented
		if(multi_line_comment_started == MA_FALSE)
		{
			if(strstr(res_id, multi_line_comment_start_identifier))
			{
				multi_line_comment_started = MA_TRUE;
			}
		}

		if(multi_line_comment_started == MA_TRUE)
		{
			if(strstr(res_id, multi_line_comment_end_identifier))
			{
				msg_id_valid = MA_FALSE;
			}
		}
	}

	if(msg_id_valid == MA_TRUE)
	{
		if((comment_start = strstr(res_id, line_comment_start_identifier)) != NULL)
		{
			if(comment_start < str_id_start)
			{
				msg_id_valid = MA_FALSE;
			}
		}
	}
	
	return msg_id_valid;
}

ma_bool_t validate_resource_value(char *res_id, char *res_value)
{
	ma_bool_t msg_id_valid = MA_FALSE;
	
	char *str_id_start = NULL, *comment_start = NULL;
	
	if((str_id_start = strstr(res_value, res_id)) != NULL)
	{
		msg_id_valid = MA_TRUE;
	}
	
	if(msg_id_valid == MA_TRUE)
	{
		// check if line is commented
		if(multi_line_comment_started == MA_FALSE)
		{
			if(strstr(res_id, multi_line_comment_start_identifier))
			{
				multi_line_comment_started = MA_TRUE;
			}
		}

		if(multi_line_comment_started == MA_TRUE)
		{
			if(strstr(res_id, multi_line_comment_end_identifier))
			{
				msg_id_valid = MA_FALSE;
			}
		}
	}

	if(msg_id_valid == MA_TRUE)
	{
		if((comment_start = strstr(res_id, line_comment_start_identifier)) != NULL)
		{
			if(comment_start < str_id_start)
			{
				msg_id_valid = MA_FALSE;
			}
		}
	}
	
	return msg_id_valid;
}

ma_error_t parse_resource(char *resource, char **res_name, ma_int32_t *res_id)
{
	ma_error_t rc = MA_ERROR_UNEXPECTED;

	ma_uint32_t index = 0, temp_index = 0;
	char *temp, *str_id_start = NULL;

	if(resource && res_name && res_id)
	{
		rc = MA_OK;

		*res_name = (char*)calloc(1, strlen(resource));
		temp = (char*)calloc(1, strlen(resource));

		for(index = 0; index < (sizeof(string_identifier_keys)/ sizeof(char*)) && (str_id_start == NULL); index++)
		{
			str_id_start = strstr(resource, string_identifier_keys[index]);
		}
		if(str_id_start != NULL)
		{
			for(index = 0; index < strlen(str_id_start) && str_id_start[index] != ' '; index++)
			{
				(*res_name)[index] = str_id_start[index];
			}
			for(; index < strlen(str_id_start) && str_id_start[index] == ' '; index++)
			{
			}

			for(temp_index = 0; index < strlen(str_id_start); index++ )
			{
				temp[temp_index++] = str_id_start[index];
			}

			*res_id = atoi(temp);
		}

		free(temp);
		temp = NULL;
	}
	return rc;
}

ma_error_t parse_resource_value(FILE *fp, char *resource, char *res_name, char **res_value)
{
	ma_error_t rc = MA_ERROR_UNEXPECTED;

	ma_uint32_t index = 0;
	char *temp, *temp_line = NULL;
	if(resource && res_name && res_value)
	{
		rc = MA_OK;
		
		/* parse up to the resource id*/
		temp = strstr(resource, res_name);
		
		/* parse up to the message start */
		while((temp!= NULL) && ((temp = strstr(temp, "\"")) == NULL))
		{ 
			temp_line = temp = (char *)calloc(1, MA_MAX_BUFFER_LEN);
			if(fgets(temp, MA_MAX_BUFFER_LEN, fp) == NULL)
			{
				return MA_ERROR_UNEXPECTED;
			}
		}
		/* ignore starting '"' character */
		temp++;
		
		strcpy(resource, temp);
		
		/* parse up to the message end */
		while((resource!= NULL) && ((temp = strstr(resource, "\"")) == NULL))
		{
			if(fgets(temp, MA_MAX_BUFFER_LEN, fp) == NULL)
			{
				return MA_ERROR_UNEXPECTED;
			}
			strcat(resource, temp);
		}

		(*res_value) = (char*)calloc(1, strlen(resource));
		for(index = 0; ((index < strlen(resource) - 2)) ; index++)
		{
			(*res_value)[index] = resource[index];
		}

		if(temp_line)
		{
			free(temp_line);
			temp_line = NULL;
		}
	}
	return rc;
}

ma_error_t get_string_count(ma_int32_t *msg_count)
{
	ma_error_t rc = MA_ERROR_UNEXPECTED;

	FILE *fp = NULL;
	char *res_id;
	char path[MA_MAX_PATH_LEN];
	
	MA_MSC_SELECT(_snprintf, snprintf)(path, MA_MAX_PATH_LEN, "%s%c%s", TEST_AGENT_RESOURCE_FILE_PATH, MA_PATH_SEPARATOR, resource_header_file);
	if((fp = fopen(path, "r")))
	{
		res_id = (char *)calloc(1, MA_MAX_BUFFER_LEN);

		while(fgets(res_id, MA_MAX_BUFFER_LEN, fp))
		{
			if(validate_resource_id(res_id) == MA_TRUE)
			{
				(*msg_count)++;
			}
		}

		free(res_id);
		res_id = NULL;
				
		fclose(fp);
		fp = NULL;

		rc = MA_OK;
	}
	return rc;
}

ma_error_t read_all_message_ids(char ***msg_ids, ma_int32_t *msg_count)
{
	ma_error_t rc = MA_ERROR_UNEXPECTED;

	FILE *fp = NULL;
	char *res_id;
	char path[MA_MAX_PATH_LEN];
	
	ma_int32_t temp_index = 0, temp_id;
	
	
	if( MA_OK == (rc = get_string_count(msg_count)))
	{
		MA_MSC_SELECT(_snprintf, snprintf)(path, MA_MAX_PATH_LEN, "%s%c%s", TEST_AGENT_RESOURCE_FILE_PATH, MA_PATH_SEPARATOR,resource_header_file);

		if((fp = fopen(path, "r")))
		{
			*msg_ids = (char **)calloc(*msg_count, sizeof(char *));
			res_id = (char *)calloc(1, MA_MAX_BUFFER_LEN);

			while(fgets(res_id, MA_MAX_BUFFER_LEN, fp))
			{
				if(validate_resource_id(res_id) == MA_TRUE)
				{
					parse_resource(res_id, &((*msg_ids)[temp_index++]), &temp_id);
				}
			}

			free(res_id);
			res_id = NULL;

			fclose(fp);
			fp = NULL;
			rc = MA_OK;
		}
	}
	return rc;
}

ma_error_t read_all_messages(char ***messages, char **msg_ids, ma_int32_t msg_count)
{
	ma_error_t rc = MA_ERROR_UNEXPECTED;

	FILE *fp = NULL;	
	char path[MA_MAX_PATH_LEN];
	char *resource, *temp_message, *temp_message2;
	ma_int32_t index = 0;

	*messages = (char**) calloc(msg_count, sizeof(char*));
	MA_MSC_SELECT(_snprintf, snprintf)(path, MA_MAX_PATH_LEN, "%s%c%s", TEST_AGENT_RESOURCE_FILE_PATH, MA_PATH_SEPARATOR,resource_definition_file);
	
	if((fp = fopen(path, "r")))
	{
		resource = (char *)calloc(1, MA_MAX_BUFFER_LEN);
		temp_message2 = (char *)calloc(1, MA_MAX_BUFFER_LEN);

		while(fgets(resource, MA_MAX_BUFFER_LEN, fp))
		{
			for(index = 0; index < msg_count; index++)
			{
				if(msg_ids[index] != NULL)
				{
					strcpy(temp_message2, msg_ids[index]);
					temp_message2 = strcat(temp_message2, " ");
					if(validate_resource_value(temp_message2, resource))
					{	
						if(parse_resource_value(fp, resource, msg_ids[index], &temp_message) == MA_OK) 
						{
							if(temp_message != NULL)
							{
								(*messages)[index] = (char*)calloc(1, (strlen(temp_message) + 1));
								strncpy((*messages)[index], temp_message, strlen(temp_message));

								free(temp_message);
								temp_message = NULL;
							}
							break;
						}
					}
				}
				else
				{
					//rc = MA_ERROR_INVALIDARG;
				}
			}
		}

		free(resource);
		resource = NULL;

		free(temp_message2);
		temp_message2 = NULL;

		rc = MA_OK;
		fclose(fp);
		fp = NULL;
	}
	return rc;
}

ma_bool_t is_exempted(char *message_id)
{
	ma_bool_t ret = MA_FALSE;
	ma_int32_t index = 0;

	for(index = 0; index < (sizeof(exceptions))/(sizeof(char*)); index++)
	{
		if(strcmp(exceptions[index], message_id) == 0)
		{
			return MA_TRUE;
		}
	}

	return ret;
}

ma_variant_t* create_param(char *msg)
{
	ma_variant_t *var = NULL;
	ma_int32_t param_value_int = 4567;
	char *param_value_str = "test";

	if(msg && (msg[0] == '%'))
	{
		if(msg[1] == 's')
		{
			ma_variant_create_from_string(param_value_str, strlen(param_value_str), &var);
		}
		else if((msg[1] == 'd') || (msg[1] == 'l' && msg[2] == 'd' ) || (msg[1] == 'x') ||
			(msg[1] == '4' && msg[2] == 'x') || (msg[1] == '0' && msg[2] == 'x'))
		{
			ma_variant_create_from_int32(param_value_int, &var);
		}
		else if(msg[1] == '1')
		{
		
		}
		else if(msg[1] == '2')
		{
		
		}
	}
	return var;
}

ma_error_t insert_params(message_struct_t *message)
{
	char *msg = NULL;
	ma_variant_t *var = NULL;

	ma_array_create(&(message->arguments));

	msg = message->message;
	while((msg!= NULL) && ((msg = strstr(msg, "%")) != NULL))
	{
		var = create_param(msg);
		ma_array_push((message->arguments), var);
		ma_variant_release(var);
		msg++;
	}

	return MA_OK;
}

ma_error_t read_value1(char *ini_file, char *key, char **value)
{
	ma_error_t rc = MA_ERROR_UNEXPECTED;
#ifdef MA_WINDOWS
	if(ini_file && key && value)
	{
		*value = (char *)calloc(1, MA_MAX_LEN);
		if((*value))
		{
			if(0 != GetPrivateProfileStringA("STRINGTABLE", key, NULL, *value, MA_MAX_LEN, ini_file)) 
			{
				rc = MA_OK;
			}
		}
		else
		{
			rc = MA_ERROR_OUTOFMEMORY;
		}
	}
	else
	{
		rc = MA_ERROR_INVALIDARG;
	}
#endif
	return rc;
}

ma_error_t init_hash_keys(ma_logger_test_message_t *test_message)
{
	ma_error_t rc = MA_ERROR_UNEXPECTED;
#ifdef MA_WINDOWS
	char buffer[MA_MAX_PATH_LEN] = {0};
	char *keys_collection = NULL, *temp = NULL, *temp_key = NULL, *temp_value = NULL;	
	ma_int32_t index = 0, key_count = 0;
	ma_bool_t found = MA_FALSE;
	keys_collection = (char*)calloc(1, test_message->count * MAX_KEY_SIZE);
	temp = keys_collection;
	
	if(test_message && keys_collection)
	{
		MA_MSC_SELECT(_snprintf, snprintf)(buffer, MA_MAX_PATH_LEN, "%s%s%c%s", TEST_AGENT_ROOT, "0409", MA_PATH_SEPARATOR, res_ini_file);
		
		if(0 != GetPrivateProfileStringA(section, NULL, NULL, keys_collection, test_message->count * MAX_KEY_SIZE, buffer)) 
		{
			while(keys_collection[0] != NULL)
			{
				found = MA_FALSE;
				temp_key = (char*)calloc(1, MAX_KEY_SIZE);
				strncpy(temp_key, keys_collection, MAX_KEY_SIZE - 1);
				
				keys_collection += strlen(temp_key) + 1;
					
				if(MA_OK == (rc = read_value1(buffer, temp_key, &temp_value)))
				{
					for(index = 0; index < test_message->count; index ++) 
					{
						if(strcmp(test_message->message_collection[index].message, temp_value) == 0)
						{
							found = MA_TRUE;
							test_message->message_collection[index].hash_key = temp_key;
							key_count++;
							break;
						}
					}
				}

				if(temp_value)
				{
					free(temp_value);
					temp_value = NULL;
				}
				
				if((found == MA_FALSE) && temp_key)
				{
					free(temp_key);
					temp_key = NULL;
				}
			}

			if(key_count == test_message->count)
			{
				rc = MA_OK;
			}
		}
	}
	else
	{
		rc = MA_ERROR_OUTOFMEMORY;
	}

	if(temp)
	{
		free(temp);
		temp = NULL;
	}
#else
	rc = MA_OK;
#endif
	return rc;
}

/* Reads all the messages to be translated and logged from resource file. */
ma_error_t logger_test_message_create(ma_logger_test_message_t **test_message)
{
	ma_error_t rc = MA_ERROR_UNEXPECTED;
	
	ma_int32_t index = 0, count = 0;	
	char **message_ids, **messages;

	if(test_message != NULL)
	{
		if(MA_OK == (rc = read_all_message_ids(&message_ids, &count)))
		{
			if(MA_OK == (rc = read_all_messages(&messages, message_ids, count)))
			{
				*test_message = (ma_logger_test_message_t*)calloc(1, sizeof(ma_logger_test_message_t));
				(*test_message)->message_collection = (message_struct_t *) calloc(count, sizeof(message_struct_t));	
				(*test_message)->count = 0;

				for(index = 0; index < count; index++) 
				{	
					if(message_ids[index] != NULL && is_exempted(message_ids[index]) == MA_FALSE &&
						messages[index] != NULL )
					{
						(*test_message)->message_collection[((*test_message)->count)].msg_id = message_ids[index];
						(*test_message)->message_collection[((*test_message)->count)].message = messages[index];
						insert_params(&((*test_message)->message_collection[((*test_message)->count)]));
						((*test_message)->count)++;
					}
					else
					{
						if(message_ids[index])
						{
							free(message_ids[index]);
							message_ids[index] = NULL;
						}

						if(messages[index])
						{
							free(messages[index]);
							messages[index] = NULL;
						}
					}
				}
				
				free(message_ids);
				message_ids = NULL;

				free(messages);
				messages = NULL;
				
				if(MA_OK != (rc = init_hash_keys(*test_message)))
				{
				}
			}

			if(message_ids)
			{
				free(message_ids);
				message_ids = NULL;
			}
		}
	}
	return rc;
}

ma_error_t logger_test_message_release(ma_logger_test_message_t *test_message)
{
	ma_error_t rc = MA_ERROR_UNEXPECTED;
	ma_int16_t index = 0;

	for(index = 0; index < test_message->count; index ++)
	{
		if(test_message->message_collection[index].msg_id)
		{
			free(test_message->message_collection[index].msg_id);
			test_message->message_collection[index].msg_id = NULL;
		}

		if(test_message->message_collection[index].message)
		{
			free(test_message->message_collection[index].message);
			test_message->message_collection[index].message = NULL;
		}

		if(test_message->message_collection[index].hash_key)
		{
			free(test_message->message_collection[index].hash_key);
			test_message->message_collection[index].hash_key = NULL;
		}

		if(test_message->message_collection[index].param1)
		{
			ma_variant_release(test_message->message_collection[index].param1);
			test_message->message_collection[index].param1 = NULL;
		}

		if(test_message->message_collection[index].arguments)
		{
			ma_array_release(test_message->message_collection[index].arguments);
			test_message->message_collection[index].arguments = NULL;
		}

		if(test_message->message_collection[index].wchar_message)
		{
			free(test_message->message_collection[index].wchar_message);
			test_message->message_collection[index].wchar_message = NULL;
		}
	}

	if(test_message->message_collection)
	{
		free(test_message->message_collection);
		test_message->message_collection = NULL;
	}

	if(test_message)
	{
		free(test_message);
		test_message = NULL;
	}

	return rc;
}

void* get_value_from_variant(ma_variant_t *variant, void **value)
{
	ma_error_t rc = MA_ERROR_UNEXPECTED;
	ma_vartype_t param_type;
	ma_buffer_t *tempBuf;
	size_t size = 0;
	ma_int32_t int_val;
	
	ma_variant_get_type(variant, &param_type);
	if(param_type == MA_VARTYPE_STRING) 
	{
		if( MA_OK == (rc = ma_variant_get_string_buffer(variant, &tempBuf))) 
		{
			rc = ma_buffer_get_string(tempBuf, (const char**)value, &size);
			ma_buffer_release(tempBuf);
			return value;
		}
	}
	else if(param_type == MA_VARTYPE_INT32) 
	{
		rc = ma_variant_get_int32(variant, &int_val);
		return (void*)int_val;
	}	
	return NULL;
}

ma_error_t read_value(char *ini_file, char *key, char **value){
	ma_error_t rc = MA_ERROR_UNEXPECTED;
#ifdef MA_WINDOWS
	if(ini_file && key && value)
	{
		*value = (char *)calloc(1, MA_MAX_LEN);
		if((*value))
		{
			if(0 != GetPrivateProfileStringA(section, key, NULL, *value, MA_MAX_LEN, ini_file)) 
			{
				rc = MA_OK;
			}
		}
		else
		{
			rc = MA_ERROR_OUTOFMEMORY;
		}
	}
	else
	{
		rc = MA_ERROR_INVALIDARG;
	}
#endif
	return rc;
}

ma_error_t translate_message(ma_logger_test_message_t *logger_test_message, char *original_message, char *lang_id, char **translated_message)
{
	ma_error_t rc = MA_ERROR_UNEXPECTED;
	char *data = NULL;
	int index = 0, dictionary_index = -1;
	char buffer[MA_MAX_PATH_LEN] = {0};
	
	for(index = 0; index < logger_test_message->count; index++) 
	{
		if(logger_test_message->message_collection[index].message != NULL)
		{
			if(0 == strcmp(original_message, logger_test_message->message_collection[index].message)) 
			{
				dictionary_index = index;
				break;
			}
		}
	}

	if(index != -1)
	{
		MA_MSC_SELECT(_snprintf, snprintf)(buffer, MA_MAX_PATH_LEN, "%s%s%c%s", TEST_AGENT_ROOT, lang_id, MA_PATH_SEPARATOR, res_ini_file);
	
		if(MA_OK == (rc = read_value(buffer, logger_test_message->message_collection[dictionary_index].hash_key, &data)))
		{
			*translated_message = (char*)calloc(1, strlen(data) +1);
			strncpy(*translated_message, data, strlen(data));
		}

		if(data != NULL)
		{
			free(data);
			data = NULL;
		}
	}

	return rc;
}

ma_error_t get_actual_message(ma_logger_test_message_t *logger_test_message, message_struct_t *message, char *lang_id, char ***actual_message, size_t *message_count ){
	ma_error_t rc = MA_ERROR_UNEXPECTED;	
	ma_variant_t *param1 = NULL, *param2 = NULL, *param3 = NULL, *param4 = NULL;
	void *value1 = NULL, *value2 = NULL, *value3 = NULL, *value4 = NULL;
	char *translated_message = NULL;
	ma_vartype_t param_type;
	int index = 0;
	size_t size; 
	
	(*actual_message) = (char **)calloc(logger_test_message->count, sizeof(char*));

	for(index = 0; index < logger_test_message->count; index++) 
	{		
		ma_array_size(message[index].arguments, &size);
		if(size >= 1) 
		{
			ma_array_get_element_at(message[index].arguments, 1, &param1);

			ma_variant_get_type(param1, &param_type);
			if(param_type == MA_VARTYPE_STRING) 
			{
				get_value_from_variant(param1, &value1);
			}
			else 
			{
				value1 = get_value_from_variant(param1, &value1);
			}
			ma_variant_release(param1);
		}
		if(size >= 2) 
		{
			ma_array_get_element_at(message[index].arguments, 2, &param2);
			ma_variant_get_type(param2, &param_type);
			if(param_type == MA_VARTYPE_STRING) 
			{
				get_value_from_variant(param2, &value2);
			}
			else 
			{
				value2 = get_value_from_variant(param2, &value2);
			}
			ma_variant_release(param2);
		}
		if(size >= 3) 
		{
			ma_array_get_element_at(message[index].arguments, 3, &param3);
			ma_variant_get_type(param3, &param_type);
			if(param_type == MA_VARTYPE_STRING) 
			{
				get_value_from_variant(param3, &value3);
			}
			else 
			{
				value3 = get_value_from_variant(param3, &value3);
			}
			ma_variant_release(param3);
		}
		if(size >= 4) 
		{
			ma_array_get_element_at(message[index].arguments, 4, &param4);
			ma_variant_get_type(param4, &param_type);
			if(param_type == MA_VARTYPE_STRING) 
			{
				get_value_from_variant(param4, &value4);
			}
			else 
			{
				value4 = get_value_from_variant(param4, &value4);
			}
			ma_variant_release(param4);
		}

		(*actual_message)[index] = (char *)calloc(1024, sizeof(char));
		if(MA_OK == (rc = translate_message(logger_test_message, message[index].message, lang_id, &translated_message)))
		{
			if(size == 0) 
			{
				MA_MSC_SELECT(_snprintf, snprintf)((*actual_message)[index], 1024, translated_message);
			}
			if(size == 1) 
			{
				MA_MSC_SELECT(_snprintf, snprintf)((*actual_message)[index], 1024, translated_message, value1);
			}
			else if(size == 2) 
			{
				MA_MSC_SELECT(_snprintf, snprintf)((*actual_message)[index], 1024, translated_message, value1, value2);
			}
			else if(size == 3) {
				MA_MSC_SELECT(_snprintf, snprintf)((*actual_message)[index], 1024, translated_message, value1, value2, value3);
			}
			else if(size == 4) 
			{
				MA_MSC_SELECT(_snprintf, snprintf)((*actual_message)[index], 1024, translated_message, value1, value2, value3, value4);
			}
		}
		if(translated_message)
		{
			free(translated_message);
			translated_message = NULL;
		}
	}
	*message_count = index;
	return rc;
}

ma_error_t parse_messages(FILE *fp, char *log_facility_name, char ***messages, size_t *message_count, size_t max_count)
{
	ma_error_t rc = MA_OK;
	char buffer[1024];	
	char *temp;
	int index = 0;
	
	(*messages) = (char **)calloc(max_count, sizeof(char *));
	while(!feof(fp)) 
	{
		if(NULL != fgets(buffer, MA_MAX_BUFFER_LEN, fp)) 
		{
			temp = strstr(buffer, log_facility_name);
			if(temp)
			{
				temp += strlen(log_facility_name) + 11;

				if(index < max_count)
				{
					(*messages)[index] = (char *) calloc(1, strlen(temp));				
					strncpy((*messages)[index], temp, strlen(temp));

					index ++;
				}
				else
				{
					return MA_ERROR_UNEXPECTED; 
				}
			}
		}
	}
	*message_count = index;
	return rc;
}

ma_error_t get_messages_logged(char *log_filename, char *log_facility_name, char ***messages, size_t *message_count, size_t max_count)
{
	ma_error_t rc = MA_ERROR_UNEXPECTED;

	FILE *fp = NULL;
	if((fp = fopen(log_filename, "r")) != NULL)
	{
		rc = parse_messages(fp, log_facility_name, messages, message_count, max_count);
		fclose(fp);
	}
	return rc;
}

ma_error_t verify_message(ma_logger_test_message_t *logger_test_message, message_struct_t *message, ma_locales_table_t1 *locale, char *log_facility_name, char *log_filename){
	ma_error_t rc = MA_ERROR_UNEXPECTED;
#ifdef MA_WINDOWS
	char **actual_message = NULL;
	char **logged_message= NULL;
	size_t actual_msg_count = 0, logged_msg_count = 0;
	size_t index = 0, i = 0;


	if( MA_OK == (rc = get_actual_message(logger_test_message, message, locale->code, &actual_message, &actual_msg_count)) &&
		MA_OK == (rc = get_messages_logged(log_filename, log_facility_name, &logged_message, &logged_msg_count, logger_test_message->count)))
	{
		if(actual_msg_count == logged_msg_count)
		{
			rc = MA_OK;
			for(index = 0; index < actual_msg_count; index++)
			{
				if(	strlen(actual_message[index]) <= 0 ||
					strlen(logged_message[index]) <= 0 ||
					0 != strncmp(actual_message[index], logged_message[index], strlen(actual_message[index])))
				{
					rc = MA_ERROR_UNEXPECTED;
					break;
				}
			}
		}
	}
	
	if(actual_message)
	{
		for(index = 0; index < actual_msg_count; index++)
		{
			free(actual_message[index]);
			actual_message[index] = NULL;
		}
		free(actual_message);
		actual_message = NULL;
	}

	if(logged_message) 
	{
		for(index = 0; index < logged_msg_count; index++)
		{
			free(logged_message[index]);
			logged_message[index] = NULL;
		}
		free(logged_message);
		logged_message = NULL;
	}
#else
	rc = MA_OK;
#endif

	return rc;
}

static ma_error_t ma_set_logger_policy_callback(const char *section, const char *key, ma_buffer_t **value, void *cb_data) 
{    
    ma_error_t rc = MA_OK;
	ut_logger_policy_t *policy = (ut_logger_policy_t *)cb_data;

    if(!strcmp(MA_LOGGER_SERVICE_SECTION_NAME_STR, section)) 
	{
        ma_buffer_create(value, 30);
        if(!strcmp(MA_LOGGER_KEY_FILTER_PATTERN_STR, key)) 
			ma_buffer_set(*value, policy->filter_pattern, strlen(policy->filter_pattern));
		else if(!strcmp(MA_LOGGER_KEY_FORMAT_PATTERN_STR, key))
			ma_buffer_set(*value, policy->format_pattern, strlen(policy->format_pattern));
		else if(!strcmp(MA_LOGGER_KEY_LOG_SIZE_LIMIT_INT, key)) 
			ma_buffer_set(*value, policy->log_size_limit, strlen(policy->log_size_limit));
		else if(!strcmp(MA_LOGGER_KEY_LOG_APP_LOGGING_ENABLED_INT, key)) 
			ma_buffer_set(*value, policy->app_logging_enable, strlen(policy->app_logging_enable));
		else if(!strcmp(MA_LOGGER_KEY_LOG_ENABLE_ROLLOVER_INT, key)) 
			ma_buffer_set(*value, policy->rollover_enable, strlen(policy->rollover_enable));
		else if(!strcmp(MA_LOGGER_KEY_LOG_MAX_ROLLOVER_INT, key)) 
            ma_buffer_set(*value, policy->max_rollovers, strlen(policy->max_rollovers));
		else if(!strcmp(MA_LOGGER_KEY_IS_LOG_RECORDING_ENABLED_INT, key)) 
			ma_buffer_set(*value, policy->is_log_recording_enabled, strlen(policy->is_log_recording_enabled));		
		else if(!strcmp(MA_LOGGER_KEY_LOG_RECORDS_SIZE_INT, key)) 
			ma_buffer_set(*value, policy->log_records_size, strlen(policy->log_records_size));
        else
        {
			ma_buffer_release(*value);
			rc = MA_ERROR_OBJECTNOTFOUND;
		}
    }
	else if(!strcmp(MA_AGENT_LANGUAGE_OPTIONS_SECTION_NAME_STR, section)) 
	{
		ma_buffer_create(value, 10);
		 if(!strcmp(MA_AGENT_ENABLE_LANG_SELECTION_STR, key)) 
			 ma_buffer_set(*value, policy->enable_lang_selection, strlen(policy->enable_lang_selection));
		 else if(!strcmp(MA_AGENT_LANGUAGE_SELECTION_STR, key)) 
			 ma_buffer_set(*value, locale.code, strlen(locale.code));
		 else
		 {
			 ma_buffer_release(*value);
            rc = MA_ERROR_OBJECTNOTFOUND;
		 }
	}
    return rc;
}

TEST_GROUP_RUNNER(ma_logger_localization_tests_group) 
{
	int lang_index = 0;
	
	logger_test_message_create(&logger_test_message);
	ma_ut_setup_create("logger.service.tests", product_id, &g_ut_setup);
	ma_ut_setup_set_policy_buffer_provider_callback(g_ut_setup, ma_set_logger_policy_callback, &logger_policy);

	for(lang_index = 0; lang_index < (sizeof(lang_id_table1)/sizeof(lang_id_table1[0])); lang_index++)
	{
		locale = lang_id_table1[lang_index];
		do {RUN_TEST_CASE(ma_logger_localization_tests, verify_file_logger_localization)} while (0);
	}

	ma_ut_setup_release(g_ut_setup);
	
	if(logger_test_message != NULL)
	{
		logger_test_message_release(logger_test_message);
	}
}

TEST_SETUP(ma_logger_localization_tests) 
{
	ma_variant_t *value_var = NULL;
	FILE *fp = NULL;
    /*Clean up the old one's , if by chance they are there */
    	
	unlink("test_file_logger.log");
	
    Unity.TestFile = "ma_file_logger_localization_test.c";
	
	// This is a work around to clear the contents of file 'logger.service.tests.log'. 
	// unlink() fails because the logger instance is not yet released. 
	/*if(fp = fopen("logger.service.tests.log", "w"))
	{
		fclose(fp);
		fp = NULL;
	}*/

	TEST_ASSERT(MA_OK == ma_logger_service_create(product_id, &ma_logger_service));
	
	ma_ds_set_str(ma_configurator_get_datastore(MA_CONTEXT_GET_CONFIGURATOR(ma_ut_setup_get_context(g_ut_setup))), 
		MA_CONFIGURATION_PATH_NAME_STR, MA_CONFIGURATION_KEY_AGENT_LANGUAGE_STR, locale.code, strlen(locale.code));
	
	//FAIL -> Memory Leak
	TEST_ASSERT(MA_OK == ma_logger_service->methods->configure(ma_logger_service, ma_ut_setup_get_context(g_ut_setup), MA_SERVICE_CONFIG_NEW));
	TEST_ASSERT(MA_OK == ma_logger_service->methods->start(ma_logger_service));
}

TEST_TEAR_DOWN(ma_logger_localization_tests) 
{
	FILE *fp = NULL;
    /*Remove the files which this group created */

	unlink("test_file_logger.log");

	if(ma_logger_service)
	{
		TEST_ASSERT(MA_OK == ma_logger_service->methods->stop(ma_logger_service));
		ma_logger_service->methods->destroy(ma_logger_service);
		ma_logger_service = NULL;
	}

	// This is a work around to clear the contents of file 'logger.service.tests.log'. 
	// unlink() fails because the logger instance is not yet released. 
	if(fp = fopen("logger.service.tests.log", "w"))
	{
		fclose(fp);
		fp = NULL;
	}
}

void* get_variant_value(ma_variant_t *variant, void **value)
{
	ma_error_t rc = MA_ERROR_UNEXPECTED;
	ma_vartype_t param_type;
	ma_buffer_t *tempBuf;
	size_t size = 0;
	ma_int32_t int_val;

	ma_variant_get_type(variant, &param_type);
	if(param_type == MA_VARTYPE_STRING) 
	{
		if( MA_OK == (rc = ma_variant_get_string_buffer(variant, &tempBuf))) 
		{
			rc = ma_buffer_get_string(tempBuf, (const char**)value, &size);
			ma_buffer_release(tempBuf);
			return value;
		}
	}
	else if(param_type == MA_VARTYPE_INT32) {
		rc = ma_variant_get_int32(variant, &int_val);
		return (void*)int_val;
	}
	return NULL;
}

TEST(ma_logger_localization_tests, verify_file_logger_localization)
{
    ma_file_logger_t *file_logger = NULL;	
    message_struct_t *test_message = NULL;
	
	int index = 0;
	ma_variant_t *param1, *param2, *param3, *param4;
	void *value1, *value2, *value3, *value4; 	
	size_t size = 0;
	ma_vartype_t param_type;
	
	file_logger = (ma_file_logger_t *)ma_ut_setup_get_logger(g_ut_setup);	
	test_message = logger_test_message->message_collection;
	for(index = 0; index < logger_test_message->count; index++) 
	{
		ma_array_size(test_message[index].arguments, &size);		
		if(size == 0) 
		{			
			MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_CRITICAL | MA_LOG_FLAG_LOCALIZE, test_message[index].message);
		}
		else if(size == 1) 
		{
			TEST_ASSERT(MA_OK == ma_array_get_element_at(test_message[index].arguments, 1, &param1));
			
			ma_variant_get_type(param1, &param_type);
			if(param_type == MA_VARTYPE_STRING) 
			{
				get_variant_value(param1, &value1);
			}
			else 
			{
				value1 = get_variant_value(param1, &value1);
			}
			ma_variant_release(param1);

			MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_CRITICAL | MA_LOG_FLAG_LOCALIZE, test_message[index].message, value1);

		}
		else if(size == 2) 
		{
			TEST_ASSERT(MA_OK == ma_array_get_element_at(test_message[index].arguments, 1, &param1));
			ma_variant_get_type(param1, &param_type);
			if(param_type == MA_VARTYPE_STRING) 
			{
				get_variant_value(param1, &value1);
			}
			else 
			{
				value1 = get_variant_value(param1, &value1);
			}
			ma_variant_release(param1);

			TEST_ASSERT(MA_OK == ma_array_get_element_at(test_message[index].arguments, 2, &param2));
			ma_variant_get_type(param2, &param_type);
			if(param_type == MA_VARTYPE_STRING) 
			{
				get_variant_value(param2, &value2);
			}
			else 
			{
				value2 = get_variant_value(param2, &value2);
			}
			ma_variant_release(param2);

			MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_CRITICAL | MA_LOG_FLAG_LOCALIZE, test_message[index].message, value1, value2);
		}
		else if(size == 3) 
		{
			TEST_ASSERT(MA_OK == ma_array_get_element_at(test_message[index].arguments, 1, &param1));
			ma_variant_get_type(param1, &param_type);
			if(param_type == MA_VARTYPE_STRING) {
				get_variant_value(param1, &value1);
			}
			else {
				value1 = get_variant_value(param1, &value1);
			}
			ma_variant_release(param1);

			TEST_ASSERT(MA_OK == ma_array_get_element_at(test_message[index].arguments, 2, &param2));
			ma_variant_get_type(param2, &param_type);
			if(param_type == MA_VARTYPE_STRING) {
				get_variant_value(param2, &value2);
			}
			else {
				value2 = get_variant_value(param2, &value2);
			}
			ma_variant_release(param2);

			TEST_ASSERT(MA_OK == ma_array_get_element_at(test_message[index].arguments, 3, &param3));
			ma_variant_get_type(param3, &param_type);
			if(param_type == MA_VARTYPE_STRING) 
			{
				get_variant_value(param3, &value3);
			}
			else 
			{
				value3 = get_variant_value(param3, &value3);
			}
			ma_variant_release(param3);

			MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_CRITICAL | MA_LOG_FLAG_LOCALIZE, test_message[index].message, value1, value2, value3);
		}
		else if(size == 4) 
		{
			TEST_ASSERT(MA_OK == ma_array_get_element_at(test_message[index].arguments, 1, &param1));
			ma_variant_get_type(param1, &param_type);
			if(param_type == MA_VARTYPE_STRING) 
			{
				get_variant_value(param1, &value1);
			}
			else 
			{
				value1 = get_variant_value(param1, &value1);
			}
			ma_variant_release(param1);

			TEST_ASSERT(MA_OK == ma_array_get_element_at(test_message[index].arguments, 2, &param2));
			ma_variant_get_type(param2, &param_type);
			if(param_type == MA_VARTYPE_STRING) 
			{
				get_variant_value(param2, &value2);
			}
			else 
			{
				value2 = get_variant_value(param2, &value2);
			}
			ma_variant_release(param2);

			TEST_ASSERT(MA_OK == ma_array_get_element_at(test_message[index].arguments, 3, &param3));
			ma_variant_get_type(param3, &param_type);
			if(param_type == MA_VARTYPE_STRING) 
			{
				get_variant_value(param3, &value3);
			}
			else 
			{
				value3 = get_variant_value(param3, &value3);
			}
			ma_variant_release(param3);

			TEST_ASSERT(MA_OK == ma_array_get_element_at(test_message[index].arguments, 4, &param4));
			ma_variant_get_type(param4, &param_type);
			if(param_type == MA_VARTYPE_STRING) 
			{
				get_variant_value(param4, &value4);
			}
			else 
			{
				value4 = get_variant_value(param4, &value4);
			}
			ma_variant_release(param4);

			MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_CRITICAL | MA_LOG_FLAG_LOCALIZE, test_message[index].message, value1, value2, value3, value4);
		}
	}
	
	TEST_ASSERT(MA_OK == verify_message(logger_test_message, test_message, &locale, LOG_FACILITY_NAME, "logger.service.tests.log"));
	
	// Below messages were exempted from verification since the parsing logic in 'verify_message' doesn't consider escape characters.	
	MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_CRITICAL | MA_LOG_FLAG_LOCALIZE, "Options\nFramePkg.exe /Install=Updater [/SiteInfo=<SiteList.xml w/ repos>]\n\tInstall, don�t change mode (default is unmanaged)\nFramePkg.exe [/Install=Agent [/SiteInfo=<SiteList.xml w/ ePO svr>]]\n\tInstall, turn on managed mode\nFramePkg.exe /Remove=Agent /Upgrade [/SiteInfo=<SiteList.xml w/ repos>]\n\tInstall, turn off managed mode\n{<InstPath>\\FrmInst.exe|FramePkg.exe} /Remove=Updater\n\tRemove if no clients\nFramePkg.exe /Install=Agent /ForceInstall [/SiteInfo=<SiteList.xml w/ ePO svr>]\n\tRemove, install in managed mode\nFramePkg.exe /Install=Updater /ForceInstall [/SiteInfo=<SiteList.xml w/ repos>]\n\tRemove, install in unmanaged mode\n<InstPath>\\FrmInst.exe /Install=Agent /SiteInfo=<SiteList.xml w/ ePO svr>\n\tTurn on managed mode\n{<InstPath>\\FrmInst.exe|FramePkg.exe} /Remove=Agent [/SiteInfo=<SiteList.xml w/ repos]\n\tTurn off managed mode\n<InstPath>\\FrmInst.exe /SiteInfo=<SiteList.xml>\n\tChange ePO svr or repos\n<any> /Silent\n\tSilent\n<clean install> [/InstDir=<InstPath>][/DataDir=<DataPath>]\n\tPaths");
	MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_CRITICAL | MA_LOG_FLAG_LOCALIZE, "Impersonating user: ""%s""", "test");
	MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_CRITICAL | MA_LOG_FLAG_LOCALIZE, "Expecting '/<option>[=""value""]' but got '%1'.", "test");
	//MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_CRITICAL | MA_LOG_FLAG_LOCALIZE, "Could not create instance of <%s> using %s (%d)", "test1", "test2", 45);
	MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_CRITICAL | MA_LOG_FLAG_LOCALIZE, "Added a new task %s to Scheduler's task list ", "test");
	MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_CRITICAL | MA_LOG_FLAG_LOCALIZE, "The %s task will be re-tried in %s minutes because %s is in progress. ", "test2", "10", "test1");
}

