@echo off
set MA_BASE=%~dp0..\..\..
set out_dir="%1"
set res_to_ini_path="%2"
set res_dll_path="%3"
%res_to_ini_path%\ma_restoini.exe %res_dll_path%
logger_test_tools\lws\lwsconsole.exe /O=Data\LWS /I=%res_dll_path% /L="en-US,cs,da,de,es-es,fi,fr,it,ja,ko,nb-NO,nl,pl,pt-BR,pt-PT,ru,sv,tr,zh-CN,zh-TW" /D=%out_dir% /V /S