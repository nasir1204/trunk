#ifndef LOGGER_TESTS_H_
#define LOGGER_TESTS_H_

#include "ma/ma_common.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/services/ma_service.h"
#include "ma/ma_variant.h"
#include "ma/internal/defs/ma_general_defs.h"

MA_CPP( extern "C" {) 

#define TEST_AGENT_GUID "ekrfhn43hrlhfn34kh4ifn4krn43"
#define MAX_KEY_SIZE 15

static const char section[] = "STRINGTABLE";
static const char res_ini_file[] = "AgentRes.ini";

#ifdef LOGGER_LOCALIZATION_TEST_64_BIT
#ifdef _DEBUG
#define RES_TO_INI_EXE_PATH ".\\logger_test_tools\\ma_restoini\\Debug\\x64"
#else
#define RES_TO_INI_EXE_PATH ".\\logger_test_tools\\ma_restoini\\Release\\x64"
#endif
#else
#ifdef _DEBUG
#define RES_TO_INI_EXE_PATH ".\\logger_test_tools\\ma_restoini\\Debug"
#else
#define RES_TO_INI_EXE_PATH ".\\logger_test_tools\\ma_restoini\\Release"
#endif
#endif

#ifdef _DEBUG
#define RES_DLL_PATH "Data\\Resource\\Debug\\0409"
#else
#define RES_DLL_PATH "Data\\Resource\\Release\\0409"
#endif

#ifdef MA_WINDOWS
#define TEST_AGENT_ROOT	".\\AGENT\\"
#define TEST_AGENT_DATA_PATH ".\\AGENT\\"
#define TEST_AGENT_SETUP_DATA_PATH ".\\AGENT\\db"
#define TEST_AGENT_INSTALL_PATH ".\\AGENT\\"
#define TEST_AGENT_INI_PATH	".\\Data"
#define TEST_AGENT_RESOURCE_FILE_PATH	".\\Data\\Resource"
#define TEST_INI_OUT_DIR	".\\L10N_OUTDIR"
#else
#define TEST_AGENT_ROOT	"./AGENT/"
#define TEST_AGENT_DATA_PATH "./AGENT/"
#define TEST_AGENT_SETUP_DATA_PATH "./AGENT/db"
#define TEST_AGENT_INSTALL_PATH "./AGENT/"
// ****************************************************************************
#define TEST_AGENT_INI_PATH	"./Data/"
#define TEST_AGENT_RESOURCE_FILE_PATH	"./Data/Resource"
#endif

typedef struct ma_locales_table_s1{
    char* code;
    char* name;
	char* tag;
} ma_locales_table_t1;

static ma_locales_table_t1 lang_id_table1[] =
{	
    { "0409", "English", "en-US" },
    { "0404", "Chinese(TW)", "zh-TW" },
    { "0405", "Czech", "cs" },
    { "0406", "Danish", "da" },
    { "0407", "German", "de" },
    { "040A", "Spanish", "es-es" },
    { "040B", "Finnish", "fi" },
    { "040C", "French", "fr" },
    { "0410", "Italian", "it" },
    { "0411", "Japanese", "ja" },
    { "0412", "Korean", "ko" },
    { "0413", "Dutch", "nl" },
    { "0414", "Norwegian", "nb-NO" },
    { "0415", "Polish", "pl" },
    { "0416", "Portugese-Brazil", "pt-BR" },
    { "0419", "Russian", "ru" },
    { "041D", "Swedish", "sv" },
    { "041F", "Turkish", "tr" },    
    { "0816", "Portugese-Portugal", "pt-PT" },
	{ "0804", "Chinese(ZH)", "zh-CN" },
};

typedef struct message_dictionary_s
{   
	char **key_collection;
	char **value_collection;
}message_dictionary_t;

static const char resource_header_file[] = "Agentres.h";
static const char resource_definition_file[] = "AgentRes.rc";

static const char *string_identifier_keys[] = {"IDS_"};
static const char multi_line_comment_start_identifier[] = {"/*"};
static const char multi_line_comment_end_identifier[] = {"/*"};
static const char line_comment_start_identifier[] = {"//"};

static const char *exceptions[] = 
{
	"IDS_INSTALL_INVALID_PARAMETERS",
	"IDS_INSTALL_IMPERSONATE",
	"IDS_OPTION_SYNTAX1",
	"IDS_SCH_ADD_NEW_TASK",
	"IDS_TASK_RUN_LATER"
};

typedef struct message_struct_s
{   
	char *message;
	wchar_t *wchar_message;
	ma_array_t *arguments;
	ma_variant_t *param1;
	char *msg_id;
	char *hash_key;

}message_struct_t;

typedef struct ma_logger_test_message_s
{
	ma_int32_t count;
	char *lang_id;
	message_struct_t *message_collection;

}ma_logger_test_message_t;

static ma_bool_t multi_line_comment_started = MA_FALSE;

MA_CPP(})

#endif