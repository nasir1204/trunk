#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "unity.h"
#include "unity_internals.h" 
#include "unity_fixture.h"
#include "ma_ut_setup.h"
#include "ma/ma_common.h"
#include "ma/internal/ma_macros.h"
#include "ma/../../src/apps/cmdagent/ma_cmdagent.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/utils/configurator/ma_configurator_utils.h"
#include "ma/../../src/utils/app_info/ma_application_internal.h"
//#include "ma/datastore/ma_ds_database.h"
#include "ma/datastore/ma_ds.h"
#include "ma/internal/ma_strdef.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/utils/xml/ma_xml.h"
#include "ma/internal/utils/xml/ma_xml_node.h"


#ifdef MA_WINDOWS
	#include "ma/datastore/ma_ds_registry.h"
    #include <windows.h>
	//#include <Winreg.h>
#else
#endif
#ifdef MA_WINDOWS
#define TEST_AGENT_ROOT	".\\AGENT\\"
#define TEST_AGENT_DATA_PATH ".\\AGENT\\"
#define TEST_AGENT_SETUP_DATA_PATH ".\\AGENT\\db"
#define TEST_AGENT_INSTALL_PATH ".\\AGENT\\"
#else
#define TEST_AGENT_ROOT	"./AGENT/"
#define TEST_AGENT_DATA_PATH "./AGENT/"
#define TEST_AGENT_SETUP_DATA_PATH "./AGENT/db"
#define TEST_AGENT_INSTALL_PATH "./AGENT/"
#endif
#define POLICY_DB_FILENAME  "POLICY"

#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include "ma/internal/utils/filesystem/path.h"

#ifdef _MSC_VER
    #include <crtdbg.h> /* */
    #define MA_TRACKLEAKS _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF)
#else
    #define MA_TRACKLEAKS ((void *)0)
#endif

#ifdef MA_WINDOWS
ma_ds_t *g_ds_default_view = NULL;
ma_ds_t *g_ds_alternate_view = NULL;
#endif

#ifdef _WIN64
#define MA_AGENT_BIT_AFFINITY 2
#elif _WIN32
#define MA_AGENT_BIT_AFFINITY 1
#else
#endif

#define TEST_AGENT_GUID "ekrfhn43hrlhfn34kh4ifn4krn43"
//static ma_ut_setup_t    *g_ut_setup=NULL;
//static ma_msgbus_t *g_msgbus = NULL;
//static ma_client_t *g_ma_client = NULL;
static ma_bool_t Policy_refresh=MA_FALSE,Policy_enforce=MA_FALSE,Property_cb=MA_FALSE, EventFor_cb=MA_FALSE;


ma_bool_t isSystem64Bit() {
#ifdef MA_WINDOWS

    UINT size = 0;
    HMODULE hm_k32 = (HMODULE) 0;
    FARPROC proc;
	TCHAR tmp[1];
	OSVERSIONINFOW g_osvi = {0};
	ma_bool_t ret = MA_FALSE;

	g_osvi.dwOSVersionInfoSize = sizeof( OSVERSIONINFO );
    GetVersionEx(&g_osvi);
    // GetSystemWow64Directory() requires at least Win XP or Win 2003 Server
    // and 64-bit is potentially only on at least XP.
    if (g_osvi.dwMajorVersion >= 4 || (g_osvi.dwMajorVersion >= 3 && g_osvi.dwMinorVersion >= 1))
    {
        // Since delay loading Kernel32.dll is not supported, we need to
        // check for the presence of GetSystemWow64Directory manually.
        hm_k32 = LoadLibrary(L"Kernel32.dll");
        if (hm_k32)
        {
            proc = GetProcAddress(hm_k32, "GetSystemWow64DirectoryW");
            if (proc)
            {
                // GetSystemWow64Directory always fails on 32 bit operating systems.
                // It returns 0 on failure or the length, so we must give it a chance
                // to copy at least 1 character into the buffer so that we can dif-
                // ferentiate between an error code and a size of 0.
                size = ((UINT (__stdcall *)(LPTSTR, UINT)) proc)(tmp, 1);
                if (size > 0)
                    ret = MA_TRUE;
            }
            FreeLibrary(hm_k32);
        }
    }
    return ret;
#endif
	return MA_FALSE;
}

static void add_agent_configuration(const char *scan_path1) {
	char command[1024] = {0};
	ma_xml_t *config_xml = NULL;
	ma_xml_node_t *config_root = NULL;

	ma_temp_buffer_t buffer;
	ma_temp_buffer_init(&buffer);
#ifdef MA_WINDOWS
	form_path(&buffer, TEST_AGENT_ROOT, "EPOAGENT3000", MA_FALSE);
#else
	form_path(&buffer, MA_REGISTRY_STORE_SCAN_PATH, "EPOAGENT3000", MA_FALSE);
#endif
    MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s %s", "mkdir" , (char*)ma_temp_buffer_get(&buffer));
	system(command);

	if(MA_OK == ma_xml_create(&config_xml)) {
		if(MA_OK == ma_xml_construct(config_xml)) {
			if(NULL != (config_root = ma_xml_get_node(config_xml))) {
				ma_xml_node_t *node = NULL;
				if(MA_OK == ma_xml_node_create(config_root, MA_REGISTRY_STORE_XML_PATH, &node)) {
					ma_xml_node_t *config_node  = NULL;
					ma_error_t error = MA_ERROR_APIFAILED;
					if(MA_OK == (error = ma_xml_node_create(node, MA_APPLICATION_INFO_DATA_DIR, &config_node))) {
						ma_xml_node_set_data(config_node, TEST_AGENT_DATA_PATH);
					}
					if(MA_OK == error  && MA_OK == (error = ma_xml_node_create(node, MA_APPLICATION_INFO_INSTALL_DIR, &config_node))) {
						ma_xml_node_set_data(config_node, TEST_AGENT_INSTALL_PATH);
					}
					if(MA_OK == error  && MA_OK == (error = ma_xml_node_create(node, MA_APPLICATION_INTEGRATION_INFO_SOFTWARE_ID, &config_node))) {
						ma_xml_node_set_data(config_node, "EPOAGENT3000");
					}
					if(MA_OK == error  && MA_OK == (error = ma_xml_node_create(node, MA_APPLICATION_INTEGRATION_INFO_DATACHANNEL_CONSUMER, &config_node))) {
						ma_xml_node_set_data(config_node, "1");
					}
					if(MA_OK == error  && MA_OK == (error = ma_xml_node_create(node, MA_APPLICATION_INTEGRATION_INFO_EVENT_GENERATOR, &config_node))) {
						ma_xml_node_set_data(config_node, "1");
					}
					if(MA_OK == error  && MA_OK == (error = ma_xml_node_create(node, MA_APPLICATION_INTEGRATION_INFO_POLICY_CONSUMER, &config_node))) {
						ma_xml_node_set_data(config_node, "1");
					}
					if(MA_OK == error  && MA_OK == (error = ma_xml_node_create(node, MA_APPLICATION_INTEGRATION_INFO_PROPERTY_PROVIDER, &config_node))) {
						ma_xml_node_set_data(config_node, "1");
					}
					if(MA_OK == error  && MA_OK == (error = ma_xml_node_create(node, MA_APPLICATION_INTEGRATION_INFO_SCHEDULER_CONSUMER, &config_node))) {
						ma_xml_node_set_data(config_node, "1");
					}
					if(MA_OK == error  && MA_OK == (error = ma_xml_node_create(node, MA_APPLICATION_INTEGRATION_INFO_UPDATER_CONSUMER, &config_node))) {
						ma_xml_node_set_data(config_node, "1");
					}
					if(MA_OK == error ){
						form_path(&buffer, (char *)ma_temp_buffer_get(&buffer), "config.xml",MA_FALSE);
						ma_xml_save_to_file(config_xml, (char *)ma_temp_buffer_get(&buffer));
					}
				}
			}
		}
		ma_xml_release(config_xml);
	}

	ma_temp_buffer_uninit(&buffer);
}

static void add_software_to_configuration(const char *scan_path1, const char *software_id, ma_int32_t registered_services){
	char command[1024] = {0};
	ma_xml_t *config_xml = NULL;
	ma_xml_node_t *config_root = NULL;

	ma_temp_buffer_t buffer;
	ma_temp_buffer_init(&buffer);
	form_path(&buffer, MA_REGISTRY_STORE_SCAN_PATH, software_id, MA_FALSE);
#ifdef MA_WINDOWS
	form_path(&buffer, TEST_AGENT_ROOT, software_id, MA_FALSE);
#else
	form_path(&buffer, MA_REGISTRY_STORE_SCAN_PATH, software_id, MA_FALSE);
#endif
    MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s %s", "mkdir" , (char*)ma_temp_buffer_get(&buffer));
	system(command);

	if(MA_OK == ma_xml_create(&config_xml)) {
		if(MA_OK == ma_xml_construct(config_xml)) {
			if(NULL != (config_root = ma_xml_get_node(config_xml))) {
				ma_xml_node_t *node = NULL;
				if(MA_OK == ma_xml_node_create(config_root, MA_REGISTRY_STORE_XML_PATH, &node)) {
					ma_xml_node_t *config_node  = NULL;
					ma_error_t error = MA_ERROR_APIFAILED;

					if(MA_OK == (error = ma_xml_node_create(node, MA_APPLICATION_INTEGRATION_INFO_SOFTWARE_ID, &config_node))) {
						ma_xml_node_set_data(config_node, software_id);
					}
					if(MA_OK == error  && (MA_REGISTERED_DATACHANNEL_SERVICE == (registered_services & MA_REGISTERED_DATACHANNEL_SERVICE)) && MA_OK == (error = ma_xml_node_create(node, MA_APPLICATION_INTEGRATION_INFO_DATACHANNEL_CONSUMER, &config_node))) {
						ma_xml_node_set_data(config_node, "1");
					}
					if(MA_OK == error  && (MA_REGISTERED_EVENT_SERVICE == (registered_services & MA_REGISTERED_EVENT_SERVICE)) && MA_OK == (error = ma_xml_node_create(node, MA_APPLICATION_INTEGRATION_INFO_EVENT_GENERATOR, &config_node))) {
						ma_xml_node_set_data(config_node, "1");
					}
					if(MA_OK == error  && (MA_REGISTERED_POLICY_SERVICE == (registered_services & MA_REGISTERED_POLICY_SERVICE)) && MA_OK == (error = ma_xml_node_create(node, MA_APPLICATION_INTEGRATION_INFO_POLICY_CONSUMER, &config_node))) {
						ma_xml_node_set_data(config_node, "1");
					}
					if(MA_OK == error && (MA_REGISTERED_PROPERTY_SERVICE == (registered_services & MA_REGISTERED_PROPERTY_SERVICE))   && MA_OK == (error = ma_xml_node_create(node, MA_APPLICATION_INTEGRATION_INFO_PROPERTY_PROVIDER, &config_node))) {
						ma_xml_node_set_data(config_node, "1");
					}
					if(MA_OK == error && (MA_REGISTERED_SCHEDULER_SERVICE == (registered_services & MA_REGISTERED_SCHEDULER_SERVICE))  && MA_OK == (error = ma_xml_node_create(node, MA_APPLICATION_INTEGRATION_INFO_SCHEDULER_CONSUMER, &config_node))) {
						ma_xml_node_set_data(config_node, "1");
					}
					if(MA_OK == error && (MA_REGISTERED_UPDATER_SERVICE == (registered_services & MA_REGISTERED_UPDATER_SERVICE))  && MA_OK == (error = ma_xml_node_create(node, MA_APPLICATION_INTEGRATION_INFO_UPDATER_CONSUMER, &config_node))) {
						ma_xml_node_set_data(config_node, "1");
					}
					if(MA_OK == error ){
						form_path(&buffer, (char *)ma_temp_buffer_get(&buffer), "config.xml",MA_FALSE);
						ma_xml_save_to_file(config_xml, (char *)ma_temp_buffer_get(&buffer));
					}
				}
			}
		}
		ma_xml_release(config_xml);
	}

	ma_temp_buffer_uninit(&buffer);
}

static void fill_agent_information(ma_ds_t *ds) {
	ma_ds_set_str(ds, MA_REGISTRY_STORE_AGENT_SCAN_PATH, MA_APPLICATION_INFO_DATA_DIR, TEST_AGENT_DATA_PATH, strlen(TEST_AGENT_DATA_PATH));
	ma_ds_set_str(ds, MA_REGISTRY_STORE_AGENT_SCAN_PATH, MA_APPLICATION_INFO_INSTALL_DIR, TEST_AGENT_INSTALL_PATH, strlen(TEST_AGENT_INSTALL_PATH));
}



static void create_application_list_in_x64_view(ma_ds_t *ds) {
	/*Created First APP*/
	ma_temp_buffer_t buffer;
	ma_temp_buffer_init(&buffer);

	form_path(&buffer, MA_REGISTRY_STORE_AGENT_SCAN_PATH, "Applications", MA_TRUE);
	form_path(&buffer, (char *) ma_temp_buffer_get(&buffer), "xApplication_1", MA_TRUE);
	ma_ds_set_str(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SOFTWARE_ID, "xApplication_1", strlen("xApplication_1"));
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_DATACHANNEL_CONSUMER, 1);
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_EVENT_GENERATOR, 1);

	form_path(&buffer, MA_REGISTRY_STORE_AGENT_SCAN_PATH, "Applications", MA_TRUE);
	form_path(&buffer, (char *) ma_temp_buffer_get(&buffer), "xApplication_2", MA_TRUE);
	ma_ds_set_str(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SOFTWARE_ID, "xApplication_2", strlen("xApplication_2"));
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_POLICY_CONSUMER, 1);
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_PROPERTY_PROVIDER, 1);

	form_path(&buffer, MA_REGISTRY_STORE_AGENT_SCAN_PATH, "Applications", MA_TRUE);
	form_path(&buffer, (char *) ma_temp_buffer_get(&buffer), "xApplication_3", MA_TRUE);
	ma_ds_set_str(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SOFTWARE_ID, "xApplication_3", strlen("xApplication_3"));
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SCHEDULER_CONSUMER, 1);
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_UPDATER_CONSUMER, 1);

	form_path(&buffer, MA_REGISTRY_STORE_AGENT_SCAN_PATH, "Applications", MA_TRUE);
	form_path(&buffer, (char *) ma_temp_buffer_get(&buffer), "xApplication_4", MA_TRUE);
	ma_ds_set_str(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SOFTWARE_ID, "xApplication_4", strlen("xApplication_4"));
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_DATACHANNEL_CONSUMER, 1);
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_EVENT_GENERATOR, 1);
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_POLICY_CONSUMER, 1);
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_PROPERTY_PROVIDER, 1);
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SCHEDULER_CONSUMER, 1);
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_UPDATER_CONSUMER, 1);

	ma_temp_buffer_uninit(&buffer);
}

static void create_application_list_in_x86_view(ma_ds_t *ds) {
	/*Created First APP*/
	ma_temp_buffer_t buffer;
	ma_temp_buffer_init(&buffer);

	form_path(&buffer, MA_REGISTRY_STORE_AGENT_SCAN_PATH, "Applications", MA_TRUE);
	form_path(&buffer, (char *) ma_temp_buffer_get(&buffer), "Application_1", MA_TRUE);
	ma_ds_set_str(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SOFTWARE_ID, "Application_1", strlen("Application_1"));
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_DATACHANNEL_CONSUMER, 1);
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_EVENT_GENERATOR, 1);

	form_path(&buffer, MA_REGISTRY_STORE_AGENT_SCAN_PATH, "Applications", MA_TRUE);
	form_path(&buffer, (char *) ma_temp_buffer_get(&buffer), "Application_2", MA_TRUE);
	ma_ds_set_str(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SOFTWARE_ID, "Application_2", strlen("Application_2"));
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_POLICY_CONSUMER, 1);
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_PROPERTY_PROVIDER, 1);

	form_path(&buffer, MA_REGISTRY_STORE_AGENT_SCAN_PATH, "Applications", MA_TRUE);
	form_path(&buffer, (char *) ma_temp_buffer_get(&buffer), "Application_3", MA_TRUE);
	ma_ds_set_str(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SOFTWARE_ID, "Application_3", strlen("Application_3"));
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SCHEDULER_CONSUMER, 1);
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_UPDATER_CONSUMER, 1);

	form_path(&buffer, MA_REGISTRY_STORE_AGENT_SCAN_PATH, "Applications", MA_TRUE);
	form_path(&buffer, (char *) ma_temp_buffer_get(&buffer), "Application_4", MA_TRUE);
	ma_ds_set_str(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SOFTWARE_ID, "Application_4", strlen("Application_4"));
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_DATACHANNEL_CONSUMER, 1);
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_EVENT_GENERATOR, 1);
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_POLICY_CONSUMER, 1);
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_PROPERTY_PROVIDER, 1);
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SCHEDULER_CONSUMER, 1);
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_UPDATER_CONSUMER, 1);

	ma_temp_buffer_uninit(&buffer);
}


static void populate_applications(ma_ds_t *ds, ma_bool_t is_64_bit_os, ma_bool_t default_view){
	/*
	 * First create the Agent information DATAPATH and INSTALLPATH
	 * In x86 Os, create directly
	 * In x64 Os, create in default view if Agent Affinity is x64
	 * In x64 OS, create in x86 view if Agent Affinity is x86
	 */
	if(default_view) {
		fill_agent_information(ds);
	}
	switch(MA_AGENT_BIT_AFFINITY) {
		case 1: // x86
			{
				if(is_64_bit_os && !default_view) {
					create_application_list_in_x64_view(ds);
				}
				else if(is_64_bit_os && default_view) {
					create_application_list_in_x86_view(ds);
				}
			}
			break;
		case 2: //x64
			{
				if(is_64_bit_os && default_view) {
					create_application_list_in_x64_view(ds);
				}
				else if(is_64_bit_os && !default_view) {
					create_application_list_in_x86_view(ds);
				}
			}
			break;
		default:
			break;
	}
}


static void do_registry_setup() {
	char command[1024];
#ifdef MA_WINDOWS
	ma_ds_registry_t *g_registry_default = NULL;
	#ifdef MA_WINDOWS
		MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s %s", "rmdir /S /Q" , TEST_AGENT_ROOT);
	#else
		MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s %s", "rm -rf " , TEST_AGENT_ROOT);
	#endif
	system(command);
	if(MA_OK == ma_ds_registry_open(HKEY_LOCAL_MACHINE,MA_REGISTRY_STORE_SCAN_PATH, MA_TRUE, MA_TRUE, KEY_ALL_ACCESS , &g_registry_default)) {
		populate_applications((ma_ds_t *) g_registry_default, isSystem64Bit(), MA_TRUE);
		ma_ds_registry_release(g_registry_default);
	}
	if(isSystem64Bit() == MA_TRUE) {
		if(MA_OK == ma_ds_registry_open(HKEY_LOCAL_MACHINE,MA_REGISTRY_STORE_SCAN_PATH, MA_FALSE, MA_TRUE,KEY_ALL_ACCESS , &g_registry_default)) {
			populate_applications((ma_ds_t *) g_registry_default, isSystem64Bit(), MA_FALSE);
			ma_ds_registry_release(g_registry_default);
		}
	}
#endif

    MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s %s", "mkdir" , TEST_AGENT_ROOT);
    system(command);
    MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s %s", "mkdir" , TEST_AGENT_SETUP_DATA_PATH);
    system(command);
    MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s %s", "mkdir" , TEST_AGENT_INSTALL_PATH);
    system(command);

#ifdef MA_WINDOWS
	MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s", "CD");
#else
	MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s", "pwd");
#endif
    system(command);

#ifndef MA_WINDOWS
	add_agent_configuration(MA_REGISTRY_STORE_SCAN_PATH);
	add_software_to_configuration(MA_REGISTRY_STORE_SCAN_PATH, "Application_1", MA_REGISTERED_DATACHANNEL_SERVICE | MA_REGISTERED_EVENT_SERVICE);
	add_software_to_configuration(MA_REGISTRY_STORE_SCAN_PATH, "Application_2", MA_REGISTERED_POLICY_SERVICE | MA_REGISTERED_PROPERTY_SERVICE);
	add_software_to_configuration(MA_REGISTRY_STORE_SCAN_PATH, "Application_3", MA_REGISTERED_SCHEDULER_SERVICE| MA_REGISTERED_UPDATER_SERVICE);
	add_software_to_configuration(MA_REGISTRY_STORE_SCAN_PATH, "Application_4", MA_REGISTERED_ALL);
	add_software_to_configuration(MA_REGISTRY_STORE_SCAN_PATH, "xApplication_1", MA_REGISTERED_DATACHANNEL_SERVICE | MA_REGISTERED_EVENT_SERVICE);
	add_software_to_configuration(MA_REGISTRY_STORE_SCAN_PATH, "xApplication_2", MA_REGISTERED_POLICY_SERVICE | MA_REGISTERED_PROPERTY_SERVICE);
	add_software_to_configuration(MA_REGISTRY_STORE_SCAN_PATH, "xApplication_3", MA_REGISTERED_SCHEDULER_SERVICE| MA_REGISTERED_UPDATER_SERVICE);
	add_software_to_configuration(MA_REGISTRY_STORE_SCAN_PATH, "xApplication_4", MA_REGISTERED_ALL);
#endif
}

static void do_registry_cleanup() {
	char command[1024]={0};
#ifdef MA_WINDOWS
	ma_ds_registry_t *g_registry_default = NULL;
	if(MA_OK == ma_ds_registry_open(HKEY_LOCAL_MACHINE,MA_REGISTRY_STORE_SCAN_PATH, MA_TRUE,MA_TRUE,KEY_ALL_ACCESS ,  &g_registry_default)) {
		ma_ds_rem((ma_ds_t*)g_registry_default, "McAfee_T1", NULL, MA_TRUE);
		ma_ds_registry_release(g_registry_default);
	}
	if(isSystem64Bit() == MA_TRUE) {
		if(MA_OK == ma_ds_registry_open(HKEY_LOCAL_MACHINE,MA_REGISTRY_STORE_SCAN_PATH, MA_FALSE, MA_TRUE, KEY_ALL_ACCESS , &g_registry_default)) {
			ma_ds_rem((ma_ds_t *)g_registry_default, "McAfee_T1", NULL, MA_TRUE);
			ma_ds_registry_release(g_registry_default);
		}
	}
	MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s %s", "rmdir /S /Q" , TEST_AGENT_ROOT);
#else
	MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s %s", "rm -rf " , TEST_AGENT_ROOT);
#endif
	system(command);
}


//Call back function for policy enforcement request by CmdAgent
ma_error_t respond_to_policy_enforce_request(ma_msgbus_server_t *server, ma_message_t *request_payload, void *cb_data, ma_msgbus_client_request_t *c_request)
{ 
	char *string_val = NULL;
	ma_message_t *response_payload = NULL;
	printf("Policy enforce reuest API is hit");
    ma_message_get_property(request_payload, "prop.key.policy_request_type", &string_val);

    // Verify the request type
    TEST_ASSERT_EQUAL_MEMORY_MESSAGE("prop.value.enforce_policies_request", string_val,
                                             strlen("prop.value.enforce_policies_request"), \
                                             "Correct Request Type is not sent to Policy Service.");
	ma_message_get_property(request_payload, "prop.key.policy_enforce_reason", &string_val);

    // Verify the other request type
    TEST_ASSERT_EQUAL_MEMORY_MESSAGE("prop.value.policy_timeout", string_val,
                                             strlen("prop.value.policy_timeout"), \
                                             "prop.value.policy_timeout");
	Policy_enforce=MA_TRUE;
	//Creating response message and sending it to the policy client
    ma_message_create(&response_payload);
    ma_msgbus_server_client_request_post_result(c_request, MA_OK, response_payload);
    ma_message_release(response_payload);
	return MA_OK;
}

//Call back function for policy refresh request by CmdAgent
ma_error_t respond_to_policy_refresh_request(ma_msgbus_server_t *server, ma_message_t *request_payload, void *cb_data, ma_msgbus_client_request_t *c_request)
{ 
	char *string_val = NULL;
	ma_message_t *response_payload = NULL;
	printf("Policy refresh reuest API is hit");
    ma_message_get_property(request_payload, "prop.key.policy_request_type", &string_val);

    //Verify the request type
    TEST_ASSERT_EQUAL_MEMORY_MESSAGE("prop.value.refresh_policies_request", string_val,
                                             strlen("prop.value.refresh_policies_request"), \
                                             "Correct Request Type is not sent to Policy Service.");
	Policy_refresh=MA_TRUE;
	//Creating response message and sending it to the policy client
    ma_message_create(&response_payload);
    ma_msgbus_server_client_request_post_result(c_request, MA_OK, response_payload);
    ma_message_release(response_payload);
	return MA_OK;
}

//Call back function for property request by CmdAgent
ma_error_t respond_to_property_request(ma_msgbus_server_t *server, ma_message_t *request_payload, void *cb_data, ma_msgbus_client_request_t *c_request)
{
	char *string_val=NULL;
	ma_message_t *response_payload = NULL;
	printf("Property reuest API is hit");
    ma_message_get_property(request_payload, "prop.key.msg_type", &string_val);

    //Verify the request type
    TEST_ASSERT_EQUAL_MEMORY_MESSAGE("prop.key.proprerty_collect", string_val,
                                             strlen("prop.key.proprerty_collect"), \
                                             "Correct Request Type is not sent to Property Service.");
	ma_message_get_property(request_payload, "PROPS_TYPE", &string_val);

    //Verify the other request type
    TEST_ASSERT_EQUAL_MEMORY_MESSAGE("1", string_val,
                                             strlen("1"), \
                                             "Correct Request Type is not sent to Property Service.");
	Property_cb=MA_TRUE;
    ma_message_create(&response_payload);
	ma_msgbus_server_client_request_post_result(c_request, MA_OK, response_payload);
    ma_message_release(response_payload);
    return MA_OK;
}

//Call back function for forward events request by CmdAgent
ma_error_t respond_to_event_forward_request(ma_msgbus_server_t *server, ma_message_t *request_payload, void *cb_data, ma_msgbus_client_request_t *c_request)
{
	char *string_val=NULL;
	ma_message_t *response_payload = NULL;
	printf("Event forward reuest API is hit");
    ma_message_get_property(request_payload, "prop.key.msg_type", &string_val);

    //Verify the request type
    TEST_ASSERT_EQUAL_MEMORY_MESSAGE("prop.value.req.upload_event", string_val,
                                             strlen("prop.value.req.upload_event"), \
                                             "Correct Request Type is not sent to Event Service.");
	ma_message_get_property(request_payload, "prop.key.event_priority", &string_val);
    //Verify the other request type
    TEST_ASSERT_EQUAL_MEMORY_MESSAGE("1", string_val,
                                             strlen("1"), \
                                             "Correct Request Type is not sent to Event Service.");
	EventFor_cb=MA_TRUE;
    ma_message_create(&response_payload);
	ma_msgbus_server_client_request_post_result(c_request, MA_OK, response_payload);
    ma_message_release(response_payload);
    return MA_OK;
}

TEST_GROUP_RUNNER(ma_cmdagent_tests_group) {
	do {RUN_TEST_CASE(ma_cmdagent_tests, cmdagent_create_release_test)} while (0);
	//do {RUN_TEST_CASE(ma_cmdagent_tests, cmdagent_read_param_test)} while (0);
}

TEST_SETUP(ma_cmdagent_tests)
{
	ma_ds_t *datastore_obj = NULL;
	ma_buffer_t *buffer = NULL;
	ma_configurator_t *configurator_obj = NULL;
	ma_agent_configuration_t agent_config_obj = { 5, 10, 15, TEST_AGENT_GUID };
	ma_datastore_configuration_request_t datastore_required = {MA_TRUE, MA_TRUE, MA_TRUE, MA_TRUE, MA_TRUE};
	//Create the registry set up for dummy agent
	do_registry_setup();
	// Dummy agent Configuration
	TEST_ASSERT_MESSAGE(MA_OK == ma_configurator_create(&agent_config_obj, &configurator_obj), "ma_configurator_create_failed");
	TEST_ASSERT_MESSAGE(MA_OK == ma_configurator_intialize_datastores(configurator_obj, &datastore_required), "ma_configurator_intialize_datastores failed");
	datastore_obj = ma_configurator_get_datastore(configurator_obj); 
	TEST_ASSERT_MESSAGE(MA_OK == ma_configurator_configure_agent_policies(configurator_obj), "Agent policies could not be configured");
}

TEST_TEAR_DOWN(ma_cmdagent_tests)
{
	do_registry_cleanup();
}

TEST(ma_cmdagent_tests, cmdagent_create_release_test)
{
	char *product_id="cmdagent";
	
	//int i=0;
	ma_cmdagent_t *cmdagent;
	ma_msgbus_server_t *p_server = NULL;

	//Test case for creating cmdagent
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_cmdagent_create(product_id, NULL));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_cmdagent_create(NULL, &cmdagent));
	TEST_ASSERT(MA_OK == ma_cmdagent_create(MA_CMDAGENT_PRODUCT_ID_STR, &cmdagent));

	// Creating dummy policy service to send the refresh policy request to policy server
    // client when requested
    TEST_ASSERT_MESSAGE(ma_msgbus_server_create(cmdagent->msgbus, "ma.service.policy", &p_server) == MA_OK, \
                        "Dummy policy service couldn't be created");

    TEST_ASSERT_MESSAGE(ma_msgbus_server_setopt(p_server, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_INPROC) == MA_OK, \
                        "Dummy policy service couldn't be configured");

    TEST_ASSERT_MESSAGE(ma_msgbus_server_start(p_server, respond_to_policy_refresh_request, NULL) == MA_OK, \
                        "Dummy policy service couldn't be started");
	TEST_ASSERT_FALSE(Policy_refresh);
	TEST_ASSERT(MA_OK == ma_cmdagent_send_refresh_policies(cmdagent));
	//Verifying whether call back function is called
	TEST_ASSERT_TRUE(Policy_refresh);
	Policy_refresh=MA_FALSE;
	TEST_ASSERT_MESSAGE(ma_msgbus_server_stop(p_server) == MA_OK, "Dummy policy service couldn't be stopped");
    TEST_ASSERT_MESSAGE(ma_msgbus_server_release(p_server) == MA_OK, "Dummy policy service couldn't be released");

	//Create dummy property service
	TEST_ASSERT_MESSAGE(ma_msgbus_server_create(cmdagent->msgbus, "ma.service.property", &p_server) == MA_OK, \
                        "Dummy property service couldn't be created");
    TEST_ASSERT_MESSAGE(ma_msgbus_server_setopt(p_server, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_INPROC) == MA_OK, \
                        "Dummy property service couldn't be configured");
    TEST_ASSERT_MESSAGE(ma_msgbus_server_start(p_server, respond_to_property_request, NULL) == MA_OK, \
                        "Dummy property service couldn't be started");
	//send request for collect and send property by cmdagent
	TEST_ASSERT_FALSE(Property_cb);
	TEST_ASSERT(MA_OK == ma_cmdagent_send_collect_properties(cmdagent));
	//Verifying whether call back function is called
	TEST_ASSERT_TRUE(Property_cb);

	//Creating dummy policy service for sending enforcing policy request
	TEST_ASSERT_MESSAGE(ma_msgbus_server_create(cmdagent->msgbus, "ma.service.policy", &p_server) == MA_OK, \
                        "Dummy policy service couldn't be created");

    TEST_ASSERT_MESSAGE(ma_msgbus_server_setopt(p_server, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_INPROC) == MA_OK, \
                        "Dummy policy service couldn't be configured");

    TEST_ASSERT_MESSAGE(ma_msgbus_server_start(p_server, respond_to_policy_enforce_request, NULL) == MA_OK, \
                        "Dummy policy service couldn't be started");
	TEST_ASSERT_FALSE(Policy_enforce);
	TEST_ASSERT(MA_OK == ma_cmdagent_send_enforce_policies(cmdagent));
	//Verifying whether call back function is called
	TEST_ASSERT_TRUE(Policy_enforce);
	TEST_ASSERT_MESSAGE(ma_msgbus_server_stop(p_server) == MA_OK, "Dummy policy service couldn't be stopped");
    TEST_ASSERT_MESSAGE(ma_msgbus_server_release(p_server) == MA_OK, "Dummy policy service couldn't be released");

	//Create dummy event service
	TEST_ASSERT_MESSAGE(ma_msgbus_server_create(cmdagent->msgbus, "ma.service.event", &p_server) == MA_OK, \
                        "Dummy event service couldn't be created");

    TEST_ASSERT_MESSAGE(ma_msgbus_server_setopt(p_server, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_INPROC) == MA_OK, \
                        "Dummy event service couldn't be configured");

    TEST_ASSERT_MESSAGE(ma_msgbus_server_start(p_server, respond_to_event_forward_request, NULL) == MA_OK, \
                        "Dummy event service couldn't be started");
	TEST_ASSERT_FALSE(EventFor_cb);
	TEST_ASSERT(MA_OK == ma_cmdagent_send_forward_events(cmdagent));
	//Verifying whether call back function is called
	TEST_ASSERT_TRUE(EventFor_cb);
	TEST_ASSERT_MESSAGE(ma_msgbus_server_stop(p_server) == MA_OK, "Dummy policy service couldn't be stopped");
    TEST_ASSERT_MESSAGE(ma_msgbus_server_release(p_server) == MA_OK, "Dummy policy service couldn't be released");

	TEST_ASSERT(MA_ERROR_INVALIDARG ==ma_cmdagent_release(NULL));
	TEST_ASSERT(MA_OK == ma_cmdagent_release(cmdagent));
	
}