#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "unity.h"
#include "unity_internals.h" 
#include "unity_fixture.h"

#include "ma/../../src/apps/cmdagent/ma_cmdagent.h"

extern char *ma_optarg;     // global argument pointer
extern int  optind ;     // global argv index
static char *cmdagent_argv[5][20]={{"cmdagent", "-p"},
						           {"cmdagent", "-c"},
						           {"cmdagent", "-e"},
						           {"cmdagent", "-f"},
						           {"cmdagent", "-s"}			 
};
static char *cmdagent_argv1[5][20]={{"cmdagent", "/p"},
						           {"cmdagent", "/c"},
						           {"cmdagent", "/e"},
						           {"cmdagent", "/f"},
						           {"cmdagent", "/s"}			 
};

static char *cmdagent_argv2[5][20]={{"cmdagent", "-S"},
						           {"cmdagent", "-C"},
						           {"cmdagent", "-E"},
						           {"cmdagent", "-F"},
						           {"cmdagent", "-S"}			 
};

char *str="pcefs";

TEST_GROUP_RUNNER(ma_cmdagent_param_tests_group) {
	do {RUN_TEST_CASE(ma_cmdagent_param_tests, cmdagent_read_param_test)} while (0);
}

TEST_SETUP(ma_cmdagent_param_tests)
{
		
}

TEST_TEAR_DOWN(ma_cmdagent_param_tests)
{
}

TEST(ma_cmdagent_param_tests, cmdagent_read_param_test) {
	int argc1 = 2;
	int argc3 = 3;
	char *argv2[] = {"cmdagent","-pe"};
	char *argv3[] = {"cmdagent","-p", "-e"};
	char *argv4[] = {"cmdagent","-x"};

	int counter = 0;
	char params = 0;
	
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_cmdagent_read_param(argc1, NULL, &params));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_cmdagent_read_param(0, cmdagent_argv, &params));
	ma_optarg = NULL;	optind = 0;
	for(counter=0; counter<5; counter++)
	{
		TEST_ASSERT(MA_OK == ma_cmdagent_read_param(argc1, cmdagent_argv[counter], &params));
		TEST_ASSERT_EQUAL(str[counter],params);
		ma_optarg = NULL;	optind = 0;
	}
	counter=0;
	//Cmdagent should not accept argument in capital letter as -P
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_cmdagent_read_param(argc1, cmdagent_argv1[counter], &params));
	ma_optarg = NULL;	optind = 0;
	counter=0;
	//Cmdagent does not accept argument as /p
	TEST_ASSERT(MA_ERROR_NOT_SUPPORTED == ma_cmdagent_read_param(argc1, cmdagent_argv2[counter], &params));
	ma_optarg = NULL;	optind = 0;
	//Cmdagent does not accept two argument as -pe
	TEST_ASSERT(MA_ERROR_NOT_SUPPORTED == ma_cmdagent_read_param(argc1, argv2, &params));
	ma_optarg = NULL;	optind = 0;
	//Cmdagent does not accept two argument as -p -e
	TEST_ASSERT(MA_ERROR_NOT_SUPPORTED == ma_cmdagent_read_param(argc3, argv3, &params));
	ma_optarg = NULL;	optind = 0;
	//Cmdagent does not accept the wrong argument e.g. -x and will return error
	TEST_ASSERT(MA_ERROR_NOT_SUPPORTED == ma_cmdagent_read_param(argc1, argv4, &params));
	ma_optarg = NULL;	optind = 0;
	//Cmdagent accepts -h as help and will return error
	TEST_ASSERT(MA_ERROR_NOT_SUPPORTED == ma_cmdagent_read_param(argc1, argv4, &params));
	ma_optarg = NULL;	optind = 0;


}

