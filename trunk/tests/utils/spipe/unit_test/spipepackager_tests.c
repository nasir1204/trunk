#include <string.h>
#include <stdlib.h>

#include "unity.h"
#include "unity_fixture.h"
#include "unity_internals.h" 

#include "tests/utils/macrypto_test/ma_crypto_tests_setup.h"
#include "ma/internal/utils/spipe/ma_spipe_package.h"
#include "ma/logger/ma_console_logger.h"

#ifndef MA_WINDOWS
#include <unistd.h>
#endif

ma_logger_t *console_logger = NULL;

struct info_datas{
	char *key;
	char *value;	
};

struct spipepkg_property{
	char *senderguid;
	char *packageguid;
	char *computername;
};

unsigned char ping_spipe_buffer[]={
"\xfa\xe5\xab\xaa\xaa\xca\x33\xab\xaa\xaa\xab\xaa\xaa\xaa\xaa\xaa\xaa\xaa\x6d\xaa\xaa\xaa\xef\xeb\xec\x9c\x99\x9b\x9c\xe8\x87\xe8\x99\x92\xef\x87\x9e\x9a\x9f\xee\x87\x93\xee\x93\xee\x87\x9b\xe8\x92\x92\x9e\x9d\xef\x93\x9a\x99\xec\xe8\xaa\xaa\xaa\xaa\xaa\xaa\
\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xcf\xcf\xcf\xc8\x9e\x9c\xcf\xc9\x87\x99\xcc\xc9\x9e\x87\x9e\x92\x9d\x98\x87\xcb\x9e\xce\x9a\x87\x9b\xcb\x9a\x99\xc8\x98\xcb\x9f\x9f\xcb\x9e\x9e\xaa\xaa\xaa\xaa\xaa\xaa\
\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xa8\xaa\xaa\xaa\xeb\xf9\xe8\x87\xfd\xe3\xe4\x98\xe1\x92\x87\xe2\xf3\xfa\xfc\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\
\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\x20\x00\x00\x00\x41\x4d\x54\x4d\x47\x4d\x54\x5f\x43\x4c\x49\x45\x4e\x54\x5f\x4c\x4f\x47\
\x52\x45\x53\x50\x4f\x4e\x53\x45\x2e\x43\x4f\x55\x4e\x54\x02\x00\x00\x00\x31\x00\x0b\x00\x00\x00\x50\x61\x63\x6b\x61\x67\x65\x54\x79\x70\x65\x0c\x00\x00\x00\x4d\x73\x67\x52\x65\x73\x70\x6f\x6e\x73\x65\x00\x0f\x00\x00\x00\x54\x72\x61\x6e\x73\x61\x63\x74\x69\
\x6f\x6e\x47\x55\x49\x44\x27\x00\x00\x00\x7b\x44\x35\x34\x39\x31\x41\x38\x31\x2d\x38\x44\x44\x31\x2d\x34\x37\x38\x46\x2d\x39\x42\x45\x37\x2d\x43\x31\x39\x36\x43\x39\x39\x30\x39\x36\x38\x37\x7d\x00\x05\x00\x00\x00\x54\x79\x70\x65\x73\x1b\x00\x00\x00\x41\x4d\
\x54\x4d\x47\x4d\x54\x5f\x43\x4c\x49\x45\x4e\x54\x5f\x4c\x4f\x47\x52\x45\x53\x50\x4f\x4e\x53\x45\x00\x04\x01\x00\x00\xbf\x00\x00\x00\x78\x01\x63\x64\x90\x61\x70\xf4\x0d\xf1\x75\xf7\x0d\x89\x77\xf6\xf1\x74\xf5\x0b\x89\xf7\xf1\x77\x0f\x72\x0d\x0e\xf0\xf7\x0b\
\x76\xd5\x33\x7c\xc0\xc0\xc0\x50\xc8\xc0\xc8\xd0\x60\xc5\x09\x64\x01\x91\x2c\x4c\xb5\xa1\x81\x81\x81\x95\xa9\x8e\xa9\x89\x41\xbc\xa1\x8e\x81\x85\x89\x15\x90\x6f\x28\x05\x93\xc5\x34\x4b\xad\xda\xcd\xcc\xd8\xd8\xd4\xd1\xc9\x58\xd7\xd2\xd0\xdc\x52\xd7\xc4\xcd\
\xc2\x42\xd7\xc9\xdc\xdc\x42\xd7\xc5\xd9\xc0\xd5\xd2\xd2\xc0\xcd\xc9\xc0\xd2\xad\x36\x1f\x68\x87\x22\x10\xe3\x45\xff\x81\x40\x19\xa8\xc2\x2f\x5f\xc1\x33\xaf\x24\x35\x47\x23\x48\x53\x01\x68\xb3\x82\x73\x51\x6a\x4a\x6a\x5e\x49\x66\x62\x4e\xb1\x42\x4a\x6a\x5a\
\x66\x5e\x6a\x0a\x13\x50\x19\x32\xba\x05\x14\x60\x64\x08\x66\x28\x61\x28\x62\xc8\x64\xc8\x63\x48\x67\x70\x03\x00\x29\xbf\x36\xb7\x00\x01\x00\x00\x78\x3d\xbd\x80\x51\x42\x03\x48\x60\xbe\x24\x7c\xc2\x67\x90\x6d\x60\x95\x43\x86\xa5\x95\x11\xea\x69\xa3\x04\x4d\
\xc5\xb4\x1e\x34\xf4\x8b\x7e\xc7\x85\x1f\x93\x60\xf7\xfd\x01\x30\x6d\xce\x29\x09\xdd\x49\x4f\x59\x20\x0b\x1a\x1a\x74\xf6\xcf\x81\xeb\xbb\x3b\x32\x90\xd7\xc2\x11\x2d\x77\x22\x32\x3c\x5d\xc6\x70\x77\x4a\x2b\xa4\x9d\x3c\xb5\x27\x0f\x94\x6a\x19\xf4\x7d\x75\x40\
\xd0\x11\x70\xcf\xac\x4e\x6d\xd1\x31\xf1\x35\xcb\x39\xe9\x7f\xfc\x97\x5d\xa0\x63\x58\x4f\x9d\xff\xe2\x89\x3b\xdb\xdb\x07\x4e\x2c\x5a\x01\x55\x8d\x67\x83\xeb\x37\x35\xe6\xe3\x1d\x62\x0a\xf3\x71\x35\x08\x30\xf6\x0b\xee\x57\x2c\x16\xc0\xf1\x0c\xc2\xa3\x79\xce\
\x94\x33\x4b\x3a\xf0\x9f\x46\xc0\x6a\x30\xb6\x41\x9e\xa3\x35\x57\x32\xa5\x06\xbc\x1d\x42\x3d\x06\x62\x5c\x61\x40\x51\xf4\x22\xf0\x22\x81\xe4\xc6\x90\xd9\x10\x7a\x04\x47\x28\x26\x83\x68\xcf\xeb\xfd\xfc\xe6\x96\xd1\x89\x21\x8e\xe6\x56\x0b\x7f\xeb\x89\x68\xa8\
\x65\xc8\x47\xe4\x67\xe8\x58\x0a\x53\x21\xeb\xad\x8e\x7b\x44\x55\xa3\xd5\xb6\x91\xac\x1f\x52\xf2\x65\xe8\xa6\x1b\xd1\xa3\x04\x2f\x1b\xf6\x94\xb1"
};

TEST_GROUP_RUNNER(ma_spipe_package_test_group) {
	/*unsigned char buffer[4096] = {0};
    FILE *fp = fopen("C:\\spipe1001", "rb");
    fread(buffer,1,4096,fp);
    fclose(fp);*/
    do {RUN_TEST_CASE(ma_spipe_package_tests, ma_spipe_package_spipe_noinfo_nodata_test)} while (0);    
	do {RUN_TEST_CASE(ma_spipe_package_tests, ma_spipe_package_spipe_info_nodata_create_test)} while (0);    
	do {RUN_TEST_CASE(ma_spipe_package_tests, ma_spipe_package_spipe_noinfo_data_create_test)} while (0);    	
	do {RUN_TEST_CASE(ma_spipe_package_tests, ma_spipe_package_spipe_info_data_create_test)} while (0);    
	do {RUN_TEST_CASE(ma_spipe_package_tests, ma_spipe_package_spipe_duplicate_info_data_create_test)} while (0);    
	do {RUN_TEST_CASE(ma_spipe_package_tests, ma_spipe_package_spipe_info_data_create_test_withfstream)} while (0);    
	do {RUN_TEST_CASE(ma_spipe_package_tests, ma_spipe_package_epospipe_teardown_test)} while (0);    
	do {RUN_TEST_CASE(ma_spipe_package_tests, ma_spipe_package_epospipe_parse_test)} while (0);    	
}

ma_crypto_t *cryptshim=NULL;
TEST_SETUP(ma_spipe_package_tests) {
	ma_console_logger_create((ma_console_logger_t**)&console_logger);
	unlink("spipetest");
	create_macrypto_object_with_keys(MA_CRYPTO_MODE_NON_FIPS,MA_CRYPTO_ROLE_OFFICER,MA_CRYPTO_AGENT_MANAGED,&cryptshim);
	TEST_ASSERT_MESSAGE(cryptshim != NULL , "failed to create  cryptshim instance");
}

TEST_TEAR_DOWN(ma_spipe_package_tests) {
	ma_console_logger_release((ma_console_logger_t*)console_logger);
	release_macrypto_object(MA_CRYPTO_MODE_NON_FIPS,MA_CRYPTO_ROLE_OFFICER,MA_CRYPTO_AGENT_MANAGED,cryptshim);
	unlink("spipetest");
}

ma_crypto_verify_key_type_t spipe_verify_key;
ma_crypto_verify_key_type_t MA_SPIPE_VERIFY_KEY(){
	return spipe_verify_key;
}

TEST(ma_spipe_package_tests, ma_spipe_package_spipe_noinfo_nodata_test) {	
	
	ma_error_t err=MA_OK;	
	ma_spipe_package_t *pkg=NULL;
	ma_spipe_package_t *pkg1=NULL;
	ma_stream_t *pkgstream=NULL;
	size_t size=0;
	ma_spipe_package_type_t package_type;

	struct spipepkg_property props={"{senderguid}","{packageguid}","test_computer"};	
	const unsigned  char *propback=NULL;
		
	spipe_verify_key=MA_CRYPTO_VERIFY_AGENT_PUBLIC_KEY;
	
	// Negative scenarios
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_spipe_package_create(NULL,&pkg));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_spipe_package_create(cryptshim,NULL));

	err=ma_spipe_package_create(cryptshim,&pkg);
	TEST_ASSERT_MESSAGE(MA_OK == err && pkg != NULL , "failed to create spipe package instance");

	err=ma_spipe_package_create(cryptshim,&pkg1);
	TEST_ASSERT_MESSAGE(MA_OK == err && pkg1 != NULL , "failed to create spipe package instance");

	err=ma_mstream_create(1014,NULL,&pkgstream);
	TEST_ASSERT_MESSAGE(MA_OK == err && pkgstream != NULL , "failed to create stream");

	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_spipe_package_set_package_type(NULL,MA_SPIPE_PACKAGE_TYPE_DATA));
	err=ma_spipe_package_set_package_type(pkg,MA_SPIPE_PACKAGE_TYPE_DATA);
	TEST_ASSERT_MESSAGE(MA_OK == err, "set package type failure");

	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_spipe_package_set_header_property(NULL,MA_SPIPE_PROPERTY_SENDER_GUID,props.senderguid));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_spipe_package_set_header_property(pkg,MA_SPIPE_PROPERTY_SENDER_GUID,NULL));
	TEST_ASSERT(MA_ERROR_INVALID_OPTION == ma_spipe_package_set_header_property(pkg,(ma_spipe_package_property_t )-1,props.senderguid));

	err=ma_spipe_package_set_header_property(pkg,MA_SPIPE_PROPERTY_SENDER_GUID,props.senderguid);
	TEST_ASSERT_MESSAGE(MA_OK == err, "set sender guid failure");

	err=ma_spipe_package_set_header_property(pkg,MA_SPIPE_PROPERTY_PACKAGE_GUID,props.packageguid);
	TEST_ASSERT_MESSAGE(MA_OK == err, "set package guid failure");

	err=ma_spipe_package_set_header_property(pkg,MA_SPIPE_PROPERTY_COMPUTER_NAME,props.computername);
	TEST_ASSERT_MESSAGE(MA_OK == err, "set computer proerty failure");

	err=ma_spipe_package_set_header_property(pkg,MA_SPIPE_PROPERTY_COMPUTER_NAME,props.computername);
	TEST_ASSERT_MESSAGE(MA_OK == err, "set computer proerty failure");
	
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_spipe_package_set_logger(NULL, console_logger));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_spipe_package_set_logger(pkg, NULL));
	TEST_ASSERT(MA_OK == ma_spipe_package_set_logger(pkg, console_logger));

	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_spipe_package_serialize(NULL,pkgstream));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_spipe_package_serialize(pkg,NULL));
	err=ma_spipe_package_serialize(pkg,pkgstream);
	TEST_ASSERT_MESSAGE(MA_OK == err, "serialize pkg failure");
		
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_spipe_package_deserialize(NULL,pkgstream));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_spipe_package_deserialize(pkg1,NULL));
	err=ma_spipe_package_deserialize(pkg1,pkgstream);
	TEST_ASSERT_MESSAGE(MA_OK == err, "setting the pkg failure");
	
	package_type = (ma_spipe_package_type_t)-1;
	TEST_ASSERT(MA_OK == ma_spipe_package_get_package_type(pkg1,&package_type));
	TEST_ASSERT(package_type == MA_SPIPE_PACKAGE_TYPE_DATA);

	//get the property back.

	// Negative scenarios
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_spipe_package_get_header_property(NULL,MA_SPIPE_PROPERTY_PACKAGE_GUID,&propback,&size));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_spipe_package_get_header_property(pkg1,MA_SPIPE_PROPERTY_PACKAGE_GUID,NULL,&size));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_spipe_package_get_header_property(pkg1,MA_SPIPE_PROPERTY_PACKAGE_GUID,&propback,NULL));
	TEST_ASSERT(MA_ERROR_INVALID_OPTION == ma_spipe_package_get_header_property(pkg1,(ma_spipe_package_property_t)-1,&propback,&size));

	err=ma_spipe_package_get_header_property(pkg1,MA_SPIPE_PROPERTY_PACKAGE_GUID,&propback,&size);
	TEST_ASSERT_MESSAGE(MA_OK == err, "get package guid failure");

	err=ma_spipe_package_get_header_property(pkg1,MA_SPIPE_PROPERTY_COMPUTER_NAME,&propback,&size);
	TEST_ASSERT_MESSAGE(MA_OK == err, "get computer proerty failure");
		
	err=ma_spipe_package_get_header_property(pkg1,MA_SPIPE_PROPERTY_COMPUTER_NAME,&propback,&size);
	TEST_ASSERT_MESSAGE(MA_OK == err, "get computer proerty failure");

	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_spipe_package_release(NULL));
	TEST_ASSERT(MA_OK == ma_spipe_package_release(pkg));
	TEST_ASSERT(MA_OK == ma_spipe_package_release(pkg1));
	TEST_ASSERT(MA_OK == ma_stream_release(pkgstream));
	
}

TEST(ma_spipe_package_tests, ma_spipe_package_spipe_info_nodata_create_test) {	
	
	ma_error_t err=MA_OK;	
	ma_spipe_package_t *pkg=NULL;
	ma_spipe_package_t *pkg1=NULL;
	ma_stream_t *pkgstream=NULL;
	int i=0;
	ma_spipe_package_type_t package_type;

	struct info_datas infos[2]={
		{"info1","info1value"},
		{"info2","info2value"}
	};
	
	spipe_verify_key=MA_CRYPTO_VERIFY_AGENT_PUBLIC_KEY;
		
	err=ma_spipe_package_create(cryptshim,&pkg);
	TEST_ASSERT_MESSAGE(MA_OK == err && pkg != NULL , "failed to create spipe package instance");

	err=ma_spipe_package_create(cryptshim,&pkg1);
	TEST_ASSERT_MESSAGE(MA_OK == err && pkg1 != NULL , "failed to create spipe package instance");

	err=ma_mstream_create(1014,NULL,&pkgstream);
	TEST_ASSERT_MESSAGE(MA_OK == err && pkgstream != NULL , "failed to create stream");

	err=ma_spipe_package_set_package_type(pkg,MA_SPIPE_PACKAGE_TYPE_DATA);
	TEST_ASSERT_MESSAGE(MA_OK == err, "set package type failure");
	
	i=0;
	
	// Negative scenarios 
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_spipe_package_add_info(NULL,infos[i].key,(unsigned char*)infos[i].value,strlen(infos[i].value)));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_spipe_package_add_info(pkg,NULL,(unsigned char*)infos[i].value,strlen(infos[i].value)));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_spipe_package_add_info(pkg,infos[i].key,NULL,0));

	while(i<2){
		err=ma_spipe_package_add_info(pkg,infos[i].key,(unsigned char*)infos[i].value,strlen(infos[i].value) );
		TEST_ASSERT_MESSAGE(MA_OK == err, "setting info fails");
		i++;
	}

	err=ma_spipe_package_serialize(pkg,pkgstream);
	TEST_ASSERT_MESSAGE(MA_OK == err, "getting the pkg failure");

	err=ma_spipe_package_deserialize(pkg1,pkgstream);
	TEST_ASSERT_MESSAGE(MA_OK == err, "setting the pkg failure");
	
	package_type = (ma_spipe_package_type_t)-1;
	TEST_ASSERT(MA_OK == ma_spipe_package_get_package_type(pkg1,&package_type));
	TEST_ASSERT(package_type == MA_SPIPE_PACKAGE_TYPE_DATA);

	i=0;
	while(i<2){
		const unsigned char *value=NULL;
		size_t size=0;
		// Negative scenarios 
		TEST_ASSERT(MA_ERROR_INVALIDARG == ma_spipe_package_get_info(NULL,infos[i].key,&value,&size));
		TEST_ASSERT(MA_ERROR_INVALIDARG == ma_spipe_package_get_info(pkg1,NULL,&value,&size));
		TEST_ASSERT(MA_ERROR_INVALIDARG == ma_spipe_package_get_info(pkg1,infos[i].key,NULL,0));

		err=ma_spipe_package_get_info(pkg1,infos[i].key,&value,&size);
		TEST_ASSERT_MESSAGE(MA_OK == err && 0 == memcmp(value,infos[i].value,size), "getting info fails");		
		i++;
	}

	{
		const unsigned char *value=NULL;
		size_t size=0;
		err=ma_spipe_package_get_info(pkg1,"nonexitsingkey",&value,&size);
		TEST_ASSERT_MESSAGE(MA_OK != err, "getting info fails");		
	}
		
	TEST_ASSERT(MA_OK == ma_spipe_package_release(pkg));
	TEST_ASSERT(MA_OK == ma_spipe_package_release(pkg1));
	TEST_ASSERT(MA_OK == ma_stream_release(pkgstream));
}

TEST(ma_spipe_package_tests, ma_spipe_package_spipe_noinfo_data_create_test) {	
	
	ma_error_t err=MA_OK;	
	ma_spipe_package_t *pkg=NULL;
	ma_spipe_package_t *pkg1=NULL;
	ma_stream_t *pkgstream=NULL;
	int i=0;
	size_t data_element_count = 0;
	ma_spipe_package_type_t package_type;
	struct info_datas datas[2]={
		{"data1","mydata1"},
		{"data2","mydata2"}
	};

	spipe_verify_key=MA_CRYPTO_VERIFY_AGENT_PUBLIC_KEY;
		
	err=ma_spipe_package_create(cryptshim,&pkg);
	TEST_ASSERT_MESSAGE(MA_OK == err && pkg != NULL , "failed to create spipe package instance");

	err=ma_spipe_package_create(cryptshim,&pkg1);
	TEST_ASSERT_MESSAGE(MA_OK == err && pkg1 != NULL , "failed to create spipe package instance");

	err=ma_mstream_create(1014,NULL,&pkgstream);
	TEST_ASSERT_MESSAGE(MA_OK == err && pkgstream != NULL , "failed to create stream");

	err=ma_spipe_package_set_package_type(pkg,MA_SPIPE_PACKAGE_TYPE_DATA);
	TEST_ASSERT_MESSAGE(MA_OK == err, "set package type failure");
		
	i=0;
	// Negative scenarios 
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_spipe_package_add_data(NULL,datas[i].key,datas[i].value,strlen(datas[i].value)));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_spipe_package_add_data(pkg,NULL,datas[i].value,strlen(datas[i].value)));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_spipe_package_add_data(pkg,datas[i].key,NULL,0));

	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_spipe_package_get_data_element_count(NULL, &data_element_count));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_spipe_package_get_data_element_count(pkg, NULL));
	
	data_element_count = -1;
	TEST_ASSERT(MA_OK == ma_spipe_package_get_data_element_count(pkg, &data_element_count));
	TEST_ASSERT(0 == data_element_count);
	while(i<2){
		err=ma_spipe_package_add_data(pkg,datas[i].key,datas[i].value,strlen(datas[i].value) );
		TEST_ASSERT_MESSAGE(MA_OK == err, "setting data fails");
		i++;
	}

	data_element_count = -1;
	TEST_ASSERT(MA_OK == ma_spipe_package_get_data_element_count(pkg, &data_element_count));
	TEST_ASSERT(2 == data_element_count);
	
	err=ma_spipe_package_serialize(pkg,pkgstream);
	TEST_ASSERT_MESSAGE(MA_OK == err, "getting the pkg failure");

	err=ma_spipe_package_deserialize(pkg1,pkgstream);
	TEST_ASSERT_MESSAGE(MA_OK == err, "setting the pkg failure");

	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_spipe_package_get_package_type(NULL,&package_type));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_spipe_package_get_package_type(pkg1,NULL));
	
	package_type = (ma_spipe_package_type_t)-1;
	TEST_ASSERT(MA_OK == ma_spipe_package_get_package_type(pkg1,&package_type));
	TEST_ASSERT(package_type == MA_SPIPE_PACKAGE_TYPE_DATA);

	data_element_count = -1;
	TEST_ASSERT(MA_OK == ma_spipe_package_get_data_element_count(pkg1, &data_element_count));
	TEST_ASSERT(2 == data_element_count);

	i=0;
	
	while(i<2){
		void *value=NULL;
		size_t size=0;
		// Negative scenarios 
		TEST_ASSERT(MA_ERROR_INVALIDARG == ma_spipe_package_get_data(NULL,datas[i].key,&value,&size));
		TEST_ASSERT(MA_ERROR_INVALIDARG == ma_spipe_package_get_data(pkg1,NULL,&value,&size));
		TEST_ASSERT(MA_ERROR_INVALIDARG == ma_spipe_package_get_data(pkg1,datas[i].key,NULL,&size));
		TEST_ASSERT(MA_ERROR_INVALIDARG == ma_spipe_package_get_data(pkg1,datas[i].key,&value,NULL));

		err=ma_spipe_package_get_data(pkg1,datas[i].key,&value,&size);
		TEST_ASSERT_MESSAGE(MA_OK == err && value, "getting data fails");		
		TEST_ASSERT_MESSAGE( 0 == memcmp(datas[i].value,value,size), "getting data fails");		
		i++;
	}
	{
		void *value=NULL;
		size_t size=0;
		err=ma_spipe_package_get_data(pkg1,"nonexistingkey",&value,&size);
		TEST_ASSERT_MESSAGE(MA_OK != err, "getting data for nonexistingkey");		
	}

	TEST_ASSERT(MA_OK == ma_spipe_package_release(pkg));
	TEST_ASSERT(MA_OK == ma_spipe_package_release(pkg1));
	TEST_ASSERT(MA_OK == ma_stream_release(pkgstream));
}

TEST(ma_spipe_package_tests,ma_spipe_package_spipe_info_data_create_test_withfstream) {
	ma_error_t err=MA_OK;	
	ma_spipe_package_t *pkg=NULL;
	ma_spipe_package_t *pkg1=NULL;
	ma_stream_t *pkgstream=NULL;
	size_t data_element_count;
	int i=0;
	ma_spipe_package_type_t package_type;

	struct info_datas infos[2]={
		{"info","info1value"},
		{"info1","info2value"}
	};
	struct info_datas datas[2]={
		{"data","fsdfasdfdsfasdfffasfdfsafsfteetewrtrtwgfdgfgertrwttrwer13213132313235544545435455465456547777777777777777777777777777777777777777777777777777777777777777777777777777777777"},
		{"data1","mydatdsadasdasdsadasdsadsadasdasdsaddasdasdsadasdasdsadasdasdasdasdsarewr2354535346a2"}
	};

	spipe_verify_key=MA_CRYPTO_VERIFY_AGENT_PUBLIC_KEY;
		
	err=ma_spipe_package_create(cryptshim,&pkg);
	TEST_ASSERT_MESSAGE(MA_OK == err && pkg != NULL , "failed to create spipe package instance");

	err=ma_spipe_package_create(cryptshim,&pkg1);
	TEST_ASSERT_MESSAGE(MA_OK == err && pkg1 != NULL , "failed to create spipe package instance");

	err=ma_fstream_create("spipetest",MA_STREAM_ACCESS_FLAG_READ | MA_STREAM_ACCESS_FLAG_WRITE,NULL,&pkgstream);
	TEST_ASSERT_MESSAGE(MA_OK == err && pkgstream != NULL , "failed to create stream");

	err=ma_spipe_package_set_package_type(pkg,MA_SPIPE_PACKAGE_TYPE_DATA);
	TEST_ASSERT_MESSAGE(MA_OK == err, "set package type failure");
		
	i=0;
	while(i<2){
		err=ma_spipe_package_add_info(pkg,infos[i].key,(unsigned char*)infos[i].value,strlen(infos[i].value));
		TEST_ASSERT_MESSAGE(MA_OK == err, "setting info fails");
		i++;
	}

	i=0;
	while(i<2){
		err=ma_spipe_package_add_data(pkg,datas[i].key,datas[i].value,strlen(datas[i].value) );
		TEST_ASSERT_MESSAGE(MA_OK == err, "setting data fails");
		i++;
	}
		
	err=ma_spipe_package_serialize(pkg,pkgstream);
	TEST_ASSERT_MESSAGE(MA_OK == err, "getting the pkg failure");

	err=ma_spipe_package_deserialize(pkg1,pkgstream);
	TEST_ASSERT_MESSAGE(MA_OK == err, "setting the pkg failure");
	
	package_type = (ma_spipe_package_type_t)-1;
	TEST_ASSERT(MA_OK == ma_spipe_package_get_package_type(pkg1,&package_type));
	TEST_ASSERT(package_type == MA_SPIPE_PACKAGE_TYPE_DATA);

	data_element_count = -1;
	TEST_ASSERT(MA_OK == ma_spipe_package_get_data_element_count(pkg1, &data_element_count));
	TEST_ASSERT(2 == data_element_count);

	i=0;
	while(i<2){
		unsigned char *value=NULL;
		size_t size=0;
		err=ma_spipe_package_get_info(pkg1,infos[i].key,&value,&size);
		TEST_ASSERT_MESSAGE(MA_OK == err && 0 == memcmp(value,infos[i].value,size), "getting info fails");		
		i++;
	}

	i=0;
	while(i<2){
		void *data=NULL;
		size_t size=0;		
		err=ma_spipe_package_get_data(pkg1,datas[i].key,&data,&size);
		TEST_ASSERT_MESSAGE(MA_OK == err && data, "getting data fails");		
		TEST_ASSERT_MESSAGE( 0 == memcmp(datas[i].value,data,size), "getting data fails");		
		i++;
	}
	TEST_ASSERT(MA_OK == ma_spipe_package_release(pkg));
	TEST_ASSERT(MA_OK == ma_spipe_package_release(pkg1));
	TEST_ASSERT(MA_OK == ma_stream_release(pkgstream));
}

TEST(ma_spipe_package_tests, ma_spipe_package_spipe_info_data_create_test) {	
	
	ma_error_t err=MA_OK;	
	ma_spipe_package_t *pkg=NULL;
	ma_spipe_package_t *pkg1=NULL;
	ma_stream_t *pkgstream=NULL;
	size_t data_element_count;
	int i=0;
	ma_spipe_package_type_t package_type;

	struct info_datas infos[4]={
		{"info1","info1value"},
		{"info2","info2value"},
		{"","info1value"},		
		{"info3",""}
	};
	struct info_datas datas[4]={
		{"data1","fsdfasdfdsfasdfffasfdfsafsfteetewrtrtwgfdgfgertrwttrwer13213132313235544545435455465456547777777777777777777777777777777777777777777777777777777777777777777777777777777777"},
		{"data2","mydatdsadasdasdsadasdsadsadasdasdsaddasdasdsadasdasdsadasdasdasdasdsarewr2354535346a2"},
		{"","fsafsaffsdffadsffdasfdfds"},
		{"data4",""}
	};

	spipe_verify_key=MA_CRYPTO_VERIFY_AGENT_PUBLIC_KEY;
		
	err=ma_spipe_package_create(cryptshim,&pkg);
	TEST_ASSERT_MESSAGE(MA_OK == err && pkg != NULL , "failed to create spipe package instance");

	err=ma_spipe_package_create(cryptshim,&pkg1);
	TEST_ASSERT_MESSAGE(MA_OK == err && pkg1 != NULL , "failed to create spipe package instance");

	err=ma_mstream_create(1014,NULL,&pkgstream);
	TEST_ASSERT_MESSAGE(MA_OK == err && pkgstream != NULL , "failed to create stream");

	err=ma_spipe_package_set_package_type(pkg,MA_SPIPE_PACKAGE_TYPE_DATA);
	TEST_ASSERT_MESSAGE(MA_OK == err, "set package type failure");
		
	i=0;
	while(i<2){
		err=ma_spipe_package_add_info(pkg,infos[i].key,(unsigned char*)infos[i].value,strlen(infos[i].value));
		TEST_ASSERT_MESSAGE(MA_OK == err, "setting info fails");
		i++;
	}	
	while(i<4){
		err=ma_spipe_package_add_info(pkg,infos[i].key,(unsigned char*)infos[i].value,strlen(infos[i].value));
		TEST_ASSERT_MESSAGE(MA_OK != err, "setting info pass for invalid input");
		i++;
	}

	i=0;
	while(i<2){
		err=ma_spipe_package_add_data(pkg,datas[i].key,datas[i].value,strlen(datas[i].value) );
		TEST_ASSERT_MESSAGE(MA_OK == err, "setting data fails");
		i++;
	}
	while(i<4){
		err=ma_spipe_package_add_data(pkg,datas[i].key,datas[i].value,strlen(datas[i].value) );
		TEST_ASSERT_MESSAGE(MA_OK != err, "setting info pass for invalid input");
		i++;
	}

	err=ma_spipe_package_serialize(pkg,pkgstream);
	TEST_ASSERT_MESSAGE(MA_OK == err, "getting the pkg failure");

	err=ma_spipe_package_deserialize(pkg1,pkgstream);
	TEST_ASSERT_MESSAGE(MA_OK == err, "setting the pkg failure");
	
	package_type = (ma_spipe_package_type_t)-1;
	TEST_ASSERT(MA_OK == ma_spipe_package_get_package_type(pkg1,&package_type));
	TEST_ASSERT(package_type == MA_SPIPE_PACKAGE_TYPE_DATA);

	data_element_count = -1;
	TEST_ASSERT(MA_OK == ma_spipe_package_get_data_element_count(pkg1, &data_element_count));
	TEST_ASSERT(2 == data_element_count);

	i=0;
	while(i<2){
		unsigned char *value=NULL;
		size_t size=0;
		err=ma_spipe_package_get_info(pkg1,infos[i].key,&value,&size);
		TEST_ASSERT_MESSAGE(MA_OK == err && 0 == memcmp(value,infos[i].value,size), "getting info fails");		
		i++;
	}

	i=0;
	while(i<2){
		void *data=NULL;
		size_t size=0;		
		err=ma_spipe_package_get_data(pkg1,datas[i].key,&data,&size);
		TEST_ASSERT_MESSAGE(MA_OK == err && data, "getting data fails");		
		TEST_ASSERT_MESSAGE( 0 == memcmp(datas[i].value,data,size), "getting data fails");		
		i++;
	}
	TEST_ASSERT(MA_OK == ma_spipe_package_release(pkg));
	TEST_ASSERT(MA_OK == ma_spipe_package_release(pkg1));
	TEST_ASSERT(MA_OK == ma_stream_release(pkgstream));
}

TEST(ma_spipe_package_tests, ma_spipe_package_spipe_duplicate_info_data_create_test){
	ma_error_t err=MA_OK;	
	ma_spipe_package_t *pkg=NULL;
	ma_spipe_package_t *pkg1=NULL;
	ma_stream_t *pkgstream=NULL;
	size_t data_element_count;
	int i=0;
	ma_spipe_package_type_t package_type;

	struct info_datas infos[2]={
		{"info","info1value"},
		{"info","info2value"}		
	};
	struct info_datas datas[2]={
		{"data","fsdfasdfdsfasdfffasfdfsafsfteetewrtrtwgfdgfgertrwttrwer13213132313235544545435455465456547777777777777777777777777777777777777777777777777777777777777777777777777777777777"},
		{"data","mydatdsadasdasdsadasdsadsadasdasdsaddasdasdsadasdasdsadasdasdasdasdsarewr2354535346a2"},		
	};

	spipe_verify_key=MA_CRYPTO_VERIFY_AGENT_PUBLIC_KEY;
		
	err=ma_spipe_package_create(cryptshim,&pkg);
	TEST_ASSERT_MESSAGE(MA_OK == err && pkg != NULL , "failed to create spipe package instance");

	err=ma_spipe_package_create(cryptshim,&pkg1);
	TEST_ASSERT_MESSAGE(MA_OK == err && pkg1 != NULL , "failed to create spipe package instance");

	err=ma_mstream_create(1014,NULL,&pkgstream);
	TEST_ASSERT_MESSAGE(MA_OK == err && pkgstream != NULL , "failed to create stream");

	err=ma_spipe_package_set_package_type(pkg,MA_SPIPE_PACKAGE_TYPE_DATA);
	TEST_ASSERT_MESSAGE(MA_OK == err, "set package type failure");
		
	i=0;
	while(i<2){
		err=ma_spipe_package_add_info(pkg,infos[i].key,(unsigned char*)infos[i].value,strlen(infos[i].value));
		TEST_ASSERT_MESSAGE(MA_OK == err, "setting info fails");
		i++;
	}		

	i=0;
	while(i<2){
		err=ma_spipe_package_add_data(pkg,datas[i].key,datas[i].value,strlen(datas[i].value) );
		TEST_ASSERT_MESSAGE(MA_OK == err, "setting data fails");
		i++;
	}	

	err=ma_spipe_package_serialize(pkg,pkgstream);
	TEST_ASSERT_MESSAGE(MA_OK == err, "getting the pkg failure");

	err=ma_spipe_package_deserialize(pkg1,pkgstream);
	TEST_ASSERT_MESSAGE(MA_OK == err, "setting the pkg failure");
	
	package_type = (ma_spipe_package_type_t)-1;
	TEST_ASSERT(MA_OK == ma_spipe_package_get_package_type(pkg1,&package_type));
	TEST_ASSERT(package_type == MA_SPIPE_PACKAGE_TYPE_DATA);

	data_element_count = -1;
	TEST_ASSERT(MA_OK == ma_spipe_package_get_data_element_count(pkg1, &data_element_count));
	TEST_ASSERT(1 == data_element_count);

	i=0;
	while(i<2){
		unsigned char *value=NULL;
		size_t size=0;
		err=ma_spipe_package_get_info(pkg1,infos[i].key,&value,&size);
		TEST_ASSERT_MESSAGE(MA_OK == err && 0 == memcmp(value,infos[0].value,size), "getting info fails");		
		i++;
	}

	i=0;
	while(i<2){
		void *data=NULL;
		size_t size=0;		
		err=ma_spipe_package_get_data(pkg1,datas[i].key,&data,&size);
		TEST_ASSERT_MESSAGE(MA_OK == err && data, "getting data fails");		
		TEST_ASSERT_MESSAGE( 0 == memcmp(datas[0].value,data,size), "getting data fails");		
		i++;
	}
	TEST_ASSERT(MA_OK == ma_spipe_package_release(pkg));
	TEST_ASSERT(MA_OK == ma_spipe_package_release(pkg1));
	TEST_ASSERT(MA_OK == ma_stream_release(pkgstream));
}

void spipe_tear_down_test(ma_stream_t *pkgstream,size_t infocount,size_t datacount){

	ma_error_t err=MA_OK;	
	ma_spipe_package_t *pkg=NULL;
	ma_spipe_package_t *pkg1=NULL;
	size_t data_element_count;
	int i=1;
	unsigned char data_buffer[40000];
	unsigned char info_buffer[1000];
	ma_spipe_package_type_t package_type;

	if(infocount >=  200 || datacount >= 200)
		return;
	spipe_verify_key=MA_CRYPTO_VERIFY_AGENT_PUBLIC_KEY;
		
	err=ma_spipe_package_create(cryptshim,&pkg);
	TEST_ASSERT_MESSAGE(MA_OK == err && pkg != NULL , "failed to create spipe package instance");

	err=ma_spipe_package_create(cryptshim,&pkg1);
	TEST_ASSERT_MESSAGE(MA_OK == err && pkg1 != NULL , "failed to create spipe package instance");

	err=ma_spipe_package_set_package_type(pkg,MA_SPIPE_PACKAGE_TYPE_DATA);
	TEST_ASSERT_MESSAGE(MA_OK == err, "set package type failure");
		
	memset(data_buffer,'x',40000);
	memset(info_buffer,'y',1000);
	i=1;
	while(i<infocount){
		char key[200]={0};				
		memcpy(key,info_buffer,i);
		err=ma_spipe_package_add_info(pkg,key,(unsigned char*)info_buffer,1000-i*2);
		TEST_ASSERT_MESSAGE(MA_OK == err, "setting info fails");
		i++;
	}

	i=1;
	while(i<datacount){
		char key[200]={0};				
		memcpy(key,data_buffer,i);
		err=ma_spipe_package_add_data(pkg,key,data_buffer,40000-i*10);
		TEST_ASSERT_MESSAGE(MA_OK == err, "setting data fails");
		i++;
	}
	
	err=ma_spipe_package_serialize(pkg,pkgstream);
	TEST_ASSERT_MESSAGE(MA_OK == err, "getting the pkg failure");

	err=ma_spipe_package_deserialize(pkg1,pkgstream);
	TEST_ASSERT_MESSAGE(MA_OK == err, "setting the pkg failure");
	
	package_type = (ma_spipe_package_type_t)-1;
	TEST_ASSERT(MA_OK == ma_spipe_package_get_package_type(pkg1,&package_type));
	TEST_ASSERT(package_type == MA_SPIPE_PACKAGE_TYPE_DATA);

	data_element_count = -1;
	TEST_ASSERT(MA_OK == ma_spipe_package_get_data_element_count(pkg, &data_element_count));
	TEST_ASSERT(((datacount > 0) ? (datacount - 1) : 0) == data_element_count);

	err=ma_spipe_package_deserialize(pkg1,pkgstream);
	TEST_ASSERT_MESSAGE(MA_OK == err, "setting the pkg failure");

	i=1;
	while(i<infocount){
		char key[200]={0};		
		unsigned char *value=NULL;
		size_t size=0;
		memcpy(key,info_buffer,i);
		err=ma_spipe_package_get_info(pkg1,key,&value,&size);
		TEST_ASSERT_MESSAGE(MA_OK == err && size == 1000-i*2 && 0 == memcmp(value,info_buffer,size), "getting info fails");		
		i++;
	}

	i=1;
	while(i<datacount){
		char key[200]={0};
		void *data=NULL;
		size_t size=0;			
		memcpy(key,data_buffer,i);	
		err=ma_spipe_package_get_data(pkg1,key,&data,&size);
		TEST_ASSERT_MESSAGE(MA_OK == err && data, "getting data fails");		
		TEST_ASSERT_MESSAGE( size == 40000-i*10 && 0 == memcmp(data,data_buffer,size), "getting data fails");		
		i++;
	}
	TEST_ASSERT(MA_OK == ma_spipe_package_release(pkg));
	TEST_ASSERT(MA_OK == ma_spipe_package_release(pkg1));
	TEST_ASSERT(MA_OK == ma_stream_release(pkgstream));
}

//void spipe_tear_down_test(ma_stream_t *pkgstream,size_t infocount,size_t datacount)
TEST(ma_spipe_package_tests, ma_spipe_package_epospipe_teardown_test) {
	ma_stream_t *pkgstream=NULL;
	ma_error_t err=MA_OK;

	{
		err=ma_fstream_create("spipetest",MA_STREAM_ACCESS_FLAG_READ | MA_STREAM_ACCESS_FLAG_WRITE,NULL,&pkgstream);
		TEST_ASSERT_MESSAGE(MA_OK == err && pkgstream != NULL , "failed to create stream");
		spipe_tear_down_test(pkgstream,0,199);
	}
	
	{
		err=ma_fstream_create("spipetest",MA_STREAM_ACCESS_FLAG_READ | MA_STREAM_ACCESS_FLAG_WRITE,NULL,&pkgstream);
		TEST_ASSERT_MESSAGE(MA_OK == err && pkgstream != NULL , "failed to create stream");
		spipe_tear_down_test(pkgstream,199,0);
	}
	
	{
		err=ma_fstream_create("spipetest",MA_STREAM_ACCESS_FLAG_READ | MA_STREAM_ACCESS_FLAG_WRITE,NULL,&pkgstream);
		TEST_ASSERT_MESSAGE(MA_OK == err && pkgstream != NULL , "failed to create stream");
		spipe_tear_down_test(pkgstream,199,199);
	}
	{		
		err=ma_mstream_create(100,NULL,&pkgstream);
		TEST_ASSERT_MESSAGE(MA_OK == err && pkgstream != NULL , "failed to create stream");
		spipe_tear_down_test(pkgstream,1,199);
	}
	
	{
		err=ma_mstream_create(100,NULL,&pkgstream);
		TEST_ASSERT_MESSAGE(MA_OK == err && pkgstream != NULL , "failed to create stream");
		spipe_tear_down_test(pkgstream,199,1);
	}
	
	{
		err=ma_mstream_create(100,NULL,&pkgstream);
		TEST_ASSERT_MESSAGE(MA_OK == err && pkgstream != NULL , "failed to create stream");
		spipe_tear_down_test(pkgstream,199,199);
	}
}

TEST(ma_spipe_package_tests, ma_spipe_package_epospipe_parse_test) {
	ma_error_t err=MA_OK;	
	ma_spipe_package_t *pkgr=NULL;	
	ma_stream_t *pkg=NULL;
	int i=0;
	size_t written=0;

	spipe_verify_key=MA_CRYPTO_VERIFY_SERVER_PUBLIC_KEY;
		
	err=ma_spipe_package_create(cryptshim,&pkgr);
	TEST_ASSERT_MESSAGE(MA_OK == err && pkgr != NULL , "failed to create spipe package instance");
	
	ma_mstream_create(5000,NULL,&pkg);
	err=ma_stream_write(pkg,ping_spipe_buffer,sizeof(ping_spipe_buffer),&written);
	TEST_ASSERT_MESSAGE(MA_OK == err && written == sizeof(ping_spipe_buffer), "failed to write");

	err=ma_spipe_package_deserialize(pkgr,pkg);
	TEST_ASSERT(MA_OK == ma_spipe_package_release(pkgr));	
	TEST_ASSERT(MA_OK == ma_stream_release(pkg));
}

