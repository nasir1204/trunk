/*****************************************************************************
Logger unit tests (File logging verification for large number of threads
simultaneously logging using a single file logger.
******************************************************************************/

//#include <uv.h>

#define MAX_THREADS 1000
/*
 * Global logger Handle
 */
ma_file_logger_t *thread_file_logger = NULL;

/* Switch from one thread to another when sleep happens */
DWORD WINAPI ma_thread(LPVOID lpParam)
{
    /* No filter applied. All log messages should be logged */

    MA_LOG((ma_logger_t *)thread_file_logger, MA_LOG_SEV_NOTICE, "Notice logged by File Logger");

    #ifdef MA_WINDOWS
        Sleep(1000);
    #endif

    MA_LOG((ma_logger_t *)thread_file_logger, MA_LOG_SEV_DEBUG, "Debug message logged by File Logger");

    #ifdef MA_WINDOWS
        Sleep(1000);
    #endif

    MA_LOG((ma_logger_t *)thread_file_logger, MA_LOG_SEV_INFO, "Information message logged by File Logger");

    #ifdef MA_WINDOWS
        Sleep(1000);
    #endif

    return 0;
}

int ma_logger_threading_test(void)
{
    DWORD dw_thread_id, dw_thread_param = 1;
    HANDLE h_thread[MAX_THREADS] = {0};
    ma_uint16_t thread_ctr = 0;

    /* Modifying the rollover policy */
    ma_file_logger_policy_t logger_policy = { (char*)DEFAULT_FILE_LOG_FORMAT_PATTERN, 100, MA_TRUE, { "Rolling", 10 } };

    /* File Logger */
    TEST_ASSERT_MESSAGE(ma_file_logger_create(NULL, "test_file_logger", ".log", &thread_file_logger) == MA_OK, \
                        "File Logger could not be created");

    ma_file_logger_set_policy(thread_file_logger, &logger_policy);

    Unity.TestFile = "ma_logger_threading_test.c";
    UnityBegin();

    for(thread_ctr = 0; thread_ctr < MAX_THREADS; thread_ctr++)
    {
        h_thread[thread_ctr] = CreateThread(NULL, 0, ma_thread, &thread_ctr, 0, &dw_thread_id);

        TEST_ASSERT_NOT_NULL_MESSAGE(h_thread[thread_ctr], "Thread could not be created");
    }

	/* Wait for all threads to complete */
	for(thread_ctr = 0; thread_ctr < MAX_THREADS; thread_ctr++)
	{
		WaitForSingleObject(h_thread[thread_ctr], INFINITE);
	}

    for(thread_ctr = 0; thread_ctr < MAX_THREADS; thread_ctr++)
    {
        CloseHandle(h_thread[thread_ctr]);
    }

    ma_logger_dec_ref((ma_logger_t *)thread_file_logger);
    return (UnityEnd());
}



