/*
Unit tests for Logger DB APIs
*/


#include "unity.h"
#include "unity_fixture.h"

#include "ma/internal/utils/logger/ma_logger_internal.h"
#include "ma/internal/services/io/ma_logger_db.h"


#ifndef MA_WINDOWS
#include <unistd.h>
#endif


#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif

#define LOG_FACILITY_NAME "Logger_Test_Facility"
static ma_db_t *db = NULL;
#define LOGGER_DB_FILENAME  "LOGGER"

TEST_GROUP_RUNNER(logger_db_tests_group)
{
    do {RUN_TEST_CASE(logger_db_tests, add_and_retrieve_log_test)} while (0);
	do {RUN_TEST_CASE(logger_db_tests, adjust_logs_tests)} while (0);
}

TEST_SETUP(logger_db_tests)
{
    Unity.TestFile = "ma_logger_db_test.c";
    unlink(LOGGER_DB_FILENAME);
}

TEST_TEAR_DOWN(logger_db_tests)
{

}

static struct LookupTable
{    
    const char *datetime;
    const char *severity;
    const char *facility;
    const char *message;
};

static struct LookupTable logs[] =
{
    { "2012-03-19T22:00:00", "Critical", LOG_FACILITY_NAME, "This is McAfee Agent Software Testing"},
    { "2013-01-08T23:40:16", "Error", LOG_FACILITY_NAME, "This is a very very long message. "
                                              "The quick brown fox jumps over the lazy dog. "
                                              "We all go a little mad sometimes."
                                              "You can't handle the truth!"
                                              "You had me at 'hello'"
                                              "Show me the money"
                                              "And in the morning, I'm making waffles!"
                                              "Game over, man! Game over!"
                                              "I love the smell of napalm in the morning...Smells like victory."
                                              "I'll be back."
                                              "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"},
    { "2013-01-08T23:40:46", "Warning", LOG_FACILITY_NAME, "&^(*&(^&^&^))(J&^%$$E@#@!@#!@#!@$#@#$%$%$%$^%$^%$^"},
    { "2013-01-08T23:42:23", "Critical", LOG_FACILITY_NAME, "This is McAfee Agent Software Testing"},
    { "2013-01-08T23:43:56", "Critical", LOG_FACILITY_NAME, "This is McAfee Agent Software Testing"},
    { "2013-01-08T23:45:56", "", LOG_FACILITY_NAME, "This is McAfee Agent Software Testing"},
    { "2013-01-08T23:47:56", "Warning", "", "This is McAfee Agent Software Testing"},
    { "2013-01-08T23:50:56", "Warning", LOG_FACILITY_NAME, ""},
};

TEST(logger_db_tests, add_and_retrieve_log_test)
{
    const char *severity = ma_log_get_severity_label(MA_LOG_SEV_NOTICE);
    ma_db_recordset_t *db_record = NULL;
    int index = 0, row_count = 0;

    const char *time_str = "2013-1-8T23:40:56";

    // Open DB in read-write mode to write log data
    TEST_ASSERT(MA_OK == ma_db_open(LOGGER_DB_FILENAME,MA_DB_OPEN_READWRITE, &db));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_logger_db_schema_create(NULL));
    TEST_ASSERT(MA_OK == ma_logger_db_schema_create(db));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_logger_db_add_log(NULL, time_str, severity, LOG_FACILITY_NAME, "This is McAfee Agent Software Testing"));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_logger_db_add_log(db, time_str, NULL, LOG_FACILITY_NAME, "This is McAfee Agent Software Testing"));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_logger_db_add_log(db, time_str, severity, NULL, "This is McAfee Agent Software Testing"));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_logger_db_add_log(db, time_str, severity, LOG_FACILITY_NAME, NULL));

    for(index = 0; index < (sizeof(logs)/sizeof(logs[0])); index++)
    {
        TEST_ASSERT(MA_OK == ma_logger_db_add_log(db, logs[index].datetime, logs[index].severity, logs[index].facility, logs[index].message));
    }

    // Close DB
    TEST_ASSERT(MA_OK == ma_db_close(db));

    // Open DB in readonly mode to fetch the logged data
    TEST_ASSERT(MA_OK == ma_db_open(LOGGER_DB_FILENAME,MA_DB_OPEN_READONLY, &db));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_logger_db_get_logs(NULL, &db_record));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_logger_db_get_logs(db, NULL));
    TEST_ASSERT(MA_OK == ma_logger_db_get_logs(db, &db_record));

    // Verifying that all records are retrieved from DB
    while((MA_ERROR_NO_MORE_ITEMS != ma_db_recordset_next(db_record)))
    {
        const char *date_time = NULL, *severity = NULL, *facility = NULL, *message = NULL;
        TEST_ASSERT(MA_OK == ma_db_recordset_get_string(db_record, 1, &date_time));
        TEST_ASSERT(MA_OK == ma_db_recordset_get_string(db_record, 2, &severity));
        TEST_ASSERT(MA_OK == ma_db_recordset_get_string(db_record, 3, &facility));
        TEST_ASSERT(MA_OK == ma_db_recordset_get_string(db_record, 4, &message));

        TEST_ASSERT(0 == strcmp(logs[row_count].datetime, date_time));
        TEST_ASSERT(0 == strcmp(logs[row_count].severity, severity));
        TEST_ASSERT(0 == strcmp(logs[row_count].facility, facility));
        TEST_ASSERT(0 == strcmp(logs[row_count].message, message));

        row_count++;
    }

    ma_db_recordset_release(db_record);

    TEST_ASSERT((sizeof(logs)/sizeof(logs[0])) == row_count);

    // Close DB
    TEST_ASSERT(MA_OK == ma_db_close(db));
}

static int get_number_of_records()
{
	int row_count = 0;
	ma_db_recordset_t *db_record = NULL;
	ma_db_t *db_instance;

	// Open DB in readonly mode to fetch the logged data
    ma_db_open(LOGGER_DB_FILENAME,MA_DB_OPEN_READONLY, &db_instance);
	ma_logger_db_get_logs(db_instance, &db_record);

    // Verifying that all records are retrieved from DB
    while((MA_ERROR_NO_MORE_ITEMS != ma_db_recordset_next(db_record)))
    {   
        row_count++;
    }
    ma_db_recordset_release(db_record);

    // Close DB
    ma_db_close(db_instance);
	
	return row_count;
}

TEST(logger_db_tests, adjust_logs_tests)
{
	const char *severity = ma_log_get_severity_label(MA_LOG_SEV_NOTICE);
    int index = 0, row_count = 0;

    TEST_ASSERT(MA_OK == ma_db_open(LOGGER_DB_FILENAME,MA_DB_OPEN_READWRITE, &db));
	TEST_ASSERT(MA_OK == ma_logger_db_schema_create(db));
	for(index = 0; index < (sizeof(logs)/sizeof(logs[0])); index++)
    {
        TEST_ASSERT(MA_OK == ma_logger_db_add_log(db, logs[index].datetime, logs[index].severity, logs[index].facility, logs[index].message));
    }
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_logger_db_adjust_logs(NULL, 5));	
    TEST_ASSERT(MA_OK == ma_db_close(db));
	TEST_ASSERT((sizeof(logs)/sizeof(logs[0])) == get_number_of_records());

	TEST_ASSERT(MA_OK == ma_db_open(LOGGER_DB_FILENAME,MA_DB_OPEN_READWRITE, &db));    
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_logger_db_adjust_logs(db, -1));	
    TEST_ASSERT(MA_OK == ma_db_close(db));
	TEST_ASSERT((sizeof(logs)/sizeof(logs[0])) == get_number_of_records());

	TEST_ASSERT(MA_OK == ma_db_open(LOGGER_DB_FILENAME,MA_DB_OPEN_READWRITE, &db));    
	TEST_ASSERT(MA_OK == ma_logger_db_adjust_logs(db, (sizeof(logs)/sizeof(logs[0])) + 10));	
    TEST_ASSERT(MA_OK == ma_db_close(db));
	TEST_ASSERT((sizeof(logs)/sizeof(logs[0])) == get_number_of_records());

	TEST_ASSERT(MA_OK == ma_db_open(LOGGER_DB_FILENAME,MA_DB_OPEN_READWRITE, &db));    
	TEST_ASSERT(MA_OK == ma_logger_db_adjust_logs(db, 5));	
    TEST_ASSERT(MA_OK == ma_db_close(db));
	TEST_ASSERT(5 == get_number_of_records());
}

