/*****************************************************************************
Logger unit tests (in multi-threaded environment)
******************************************************************************/

#include "unity.h"
#include "unity_fixture.h"
#include "ma/logger/ma_file_logger.h"
#include "ma/logger/ma_console_logger.h"

#include "ma/logger/ma_logger.h"
#include <uv.h>
#include "ma/internal/utils/logger/ma_logger_internal.h"
#ifndef MA_WINDOWS
    #include<unistd.h>
#endif

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif

#define LOG_FACILITY_NAME "Logger_Test_Facility"
static ma_file_logger_policy_t logger_policy = { MA_TRUE, (char*)DEFAULT_FILE_LOG_FORMAT_PATTERN, 2, MA_FALSE, { "previous", 5 } };
TEST_GROUP_RUNNER(ma_log_threading_tests_group) {
    do {RUN_TEST_CASE(ma_log_threading_tests, console_logging_3_threads_different_logger_handle)} while (0);
    do {RUN_TEST_CASE(ma_log_threading_tests, file_logging_3_threads_different_logger_handle)} while (0);
    do {RUN_TEST_CASE(ma_log_threading_tests, console_logging_3_threads_same_logger_handle)} while (0);
    do {RUN_TEST_CASE(ma_log_threading_tests, file_logging_3_threads_same_logger_handle)} while (0);

}

TEST_SETUP(ma_log_threading_tests) {
    /*Clean up the old one's , if by chance they are there */
    unlink("test_file_logger.log");

    Unity.TestFile = "ma_log_threading_test.c";


}

TEST_TEAR_DOWN(ma_log_threading_tests) {
    /*Remove the files which this group created */
    unlink("test_file_logger.log");

}


/* Global logger, for scenario where same logger is used across threads */
ma_logger_t *global_logger = NULL;

void  console_logging_thread_1(void *param) {
    ma_console_logger_t *console_logger = NULL;
    ma_log_filter_t *filter = NULL;

    ma_uint8_t counter = 0;

    TEST_ASSERT_MESSAGE(ma_console_logger_create(&console_logger) == MA_OK, \
                        "Console logger could not be created");

    TEST_ASSERT_MESSAGE(ma_generic_log_filter_create("*.Critical" , &filter) == MA_OK,
                        "Filter could not be created");

    TEST_ASSERT_MESSAGE(ma_logger_add_filter((ma_logger_t *)console_logger, filter) == MA_OK, \
                        "Filter could not be added for Console Logger");

    for(counter = 0; counter < 10; counter++)
    {
        MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Logging by console logger 1 - "
                                                                   "This is a very very long message. "
                                                                   "The quick brown fox jumps over the lazy dog. "
                                                                   "We all go a little mad sometimes."
                                                                   "You can't handle the truth!"
                                                                   "You had me at 'hello'"
                                                                   "Show me the money"
                                                                   "And in the morning, I'm making waffles!"
                                                                   "Game over, man! Game over!"
                                                                   "I love the smell of napalm in the morning...Smells like victory."
                                                                   "I'll be back."
                                                                   "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'\n");
    }

    /* Destroying filter */
    ma_log_filter_release(filter);

    /* Destroying Logger */
    ma_logger_dec_ref((ma_logger_t *)console_logger);

    return ;
}


void  console_logging_thread_2(void *param) {
    ma_console_logger_t *console_logger = NULL;
    ma_log_filter_t *filter = NULL;

    ma_uint8_t counter = 0;

    TEST_ASSERT_MESSAGE(ma_console_logger_create(&console_logger) == MA_OK, \
                        "Console logger could not be created");

    TEST_ASSERT_MESSAGE(ma_generic_log_filter_create("*.Info" , &filter) == MA_OK,
                        "Filter could not be created");

    TEST_ASSERT_MESSAGE(ma_logger_add_filter((ma_logger_t *)console_logger, filter) == MA_OK, \
                        "Filter could not be added for Console Logger");

    for(counter = 0; counter < 10; counter++)
    {
       MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_INFO, "Logging by console logger 2 - "
                                                              "00000000000000000000000000000000000000000000000000"
                                                              "11111111111111111111111111111111111111111111111111"
                                                              "22222222222222222222222222222222222222222222222222"
                                                              "33333333333333333333333333333333333333333333333333"
                                                              "44444444444444444444444444444444444444444444444444"
                                                              "55555555555555555555555555555555555555555555555555"
                                                              "66666666666666666666666666666666666666666666666666"
                                                              "77777777777777777777777777777777777777777777777777"
                                                              "88888888888888888888888888888888888888888888888888"
                                                              "99999999999999999999999999999999999999999999999999\n");
    }


    /* Destroying filter */
    ma_log_filter_release(filter);

    /* Destroying Logger */
    ma_logger_dec_ref((ma_logger_t *)console_logger);

    return ;
}

void  console_logging_thread_3(void *param) {
    ma_console_logger_t *console_logger = NULL;
    ma_log_filter_t *filter = NULL;

    ma_uint8_t counter = 0;

    TEST_ASSERT_MESSAGE(ma_console_logger_create(&console_logger) == MA_OK, \
                        "Console logger could not be created");

    TEST_ASSERT_MESSAGE(ma_generic_log_filter_create("*.Debug" , &filter) == MA_OK,
                        "Filter could not be created");

    TEST_ASSERT_MESSAGE(ma_logger_add_filter((ma_logger_t *)console_logger, filter) == MA_OK, \
                        "Filter could not be added for Console Logger");

    for(counter = 0; counter < 10; counter++)
    {
       MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_DEBUG, "Logging by console logger 3 - "
                                                               "@#@$#@$#%$%$^$^%^&^%&^*^%$#%#$@#$@#@!#@#$#@$#%$#%$"
                                                               "<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
                                                               "{{{{{{{{{{{{{{{{{{{{}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}"
                                                               "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$"
                                                               "!@#$#$#%$%$^%^&^*&^*&(*(*)(*)((_(*&*&^%^%$^$%$#@#$\n");
    }


    /* Destroying filter */
    ma_log_filter_release(filter);

    /* Destroying Logger */
    ma_logger_dec_ref((ma_logger_t *)console_logger);

    return ;
}

void file_logging_thread_1(void *param) {
    ma_file_logger_t *file_logger = NULL;
    ma_log_filter_t *filter = NULL;

    ma_uint8_t counter = 0;

    TEST_ASSERT_MESSAGE(ma_file_logger_create(NULL, "test_file_logger", ".log", &file_logger) == MA_OK, \
                        "File Logger could not be created");
	TEST_ASSERT(MA_OK == ma_file_logger_set_policy(file_logger, &logger_policy));

    TEST_ASSERT_MESSAGE(ma_generic_log_filter_create("*.Critical" , &filter) == MA_OK,
                        "Filter could not be created");

    TEST_ASSERT_MESSAGE(ma_logger_add_filter((ma_logger_t *)file_logger, filter) == MA_OK, \
                        "Filter could not be added for File Logger");

    for(counter = 0; counter < 10; counter++)
    {
        MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_CRITICAL, "Logging by file logger 1 - "
                                                                "This is a very very long message. "
                                                                "The quick brown fox jumps over the lazy dog. "
                                                                "We all go a little mad sometimes."
                                                                "You can't handle the truth!"
                                                                "You had me at 'hello'"
                                                                "Show me the money"
                                                                "And in the morning, I'm making waffles!"
                                                                "Game over, man! Game over!"
                                                                "I love the smell of napalm in the morning...Smells like victory."
                                                                "I'll be back."
                                                                "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'\n");
    }

    /* Destroying filter */
    ma_log_filter_release(filter);

    /* Destroying Logger */
    ma_logger_dec_ref((ma_logger_t *)file_logger);

    return ;
}


void file_logging_thread_2(void *param) {

    ma_file_logger_t *file_logger = NULL;
    ma_log_filter_t *filter = NULL;

    ma_uint8_t counter = 0;

    TEST_ASSERT_MESSAGE(ma_file_logger_create(NULL, "test_file_logger", ".log", &file_logger) == MA_OK, \
                        "File Logger could not be created");
	TEST_ASSERT(MA_OK == ma_file_logger_set_policy(file_logger, &logger_policy));

    TEST_ASSERT_MESSAGE(ma_generic_log_filter_create("*.Info" , &filter) == MA_OK,
                        "Filter could not be created");

    TEST_ASSERT_MESSAGE(ma_logger_add_filter((ma_logger_t *)file_logger, filter) == MA_OK, \
                        "Filter could not be added for File Logger");

    for(counter = 0; counter < 10; counter++)
    {
       MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_INFO, "Logging by file logger 2 - "
                                                           "00000000000000000000000000000000000000000000000000"
                                                           "11111111111111111111111111111111111111111111111111"
                                                           "22222222222222222222222222222222222222222222222222"
                                                           "33333333333333333333333333333333333333333333333333"
                                                           "44444444444444444444444444444444444444444444444444"
                                                           "55555555555555555555555555555555555555555555555555"
                                                           "66666666666666666666666666666666666666666666666666"
                                                           "77777777777777777777777777777777777777777777777777"
                                                           "88888888888888888888888888888888888888888888888888"
                                                           "99999999999999999999999999999999999999999999999999\n");
    }


    /* Destroying filter */
    ma_log_filter_release(filter);

    /* Destroying Logger */
    ma_logger_dec_ref((ma_logger_t *)file_logger);

    return ;
}

void file_logging_thread_3(void *param) {
    ma_file_logger_t *file_logger = NULL;
    ma_log_filter_t *filter = NULL;

    ma_uint8_t counter = 0;

    TEST_ASSERT_MESSAGE(ma_file_logger_create(NULL, "test_file_logger", ".log", &file_logger) == MA_OK, \
                        "File Logger could not be created");
	TEST_ASSERT(MA_OK == ma_file_logger_set_policy(file_logger, &logger_policy));

    TEST_ASSERT_MESSAGE(ma_generic_log_filter_create("*.Debug" , &filter) == MA_OK,
                        "Filter could not be created");

    TEST_ASSERT_MESSAGE(ma_logger_add_filter((ma_logger_t *)file_logger, filter) == MA_OK, \
                        "Filter could not be added for File Logger");

    for(counter = 0; counter < 10; counter++)
    {
       MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_DEBUG, "Logging by file logger 3 - "
                                                            "@#@$#@$#%$%$^$^%^&^%&^*^%$#%#$@#$@#@!#@#$#@$#%$#%$"
                                                            "<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
                                                            "{{{{{{{{{{{{{{{{{{{{}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}"
                                                            "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$"
                                                            "!@#$#$#%$%$^%^&^*&^*&(*(*)(*)((_(*&*&^%^%$^$%$#@#$\n");
    }

    /* Destroying filter */
    ma_log_filter_release(filter);

    /* Destroying Logger */
    ma_logger_dec_ref((ma_logger_t *)file_logger);

    return ;
}


void general_logging_thread_1(void *param) {
    ma_uint8_t counter = 0;

    for(counter = 0; counter < 10; counter++)
    {
        MA_LOG(global_logger, MA_LOG_SEV_DEBUG, "Logging by general logger 1 - "
                                                "This is a very very long message. "
                                                "The quick brown fox jumps over the lazy dog. "
                                                "We all go a little mad sometimes."
                                                "You can't handle the truth!"
                                                "You had me at 'hello'"
                                                "Show me the money"
                                                "And in the morning, I'm making waffles!"
                                                "Game over, man! Game over!"
                                                "I love the smell of napalm in the morning...Smells like victory."
                                                "I'll be back."
                                                "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'\n");
    }

    return ;
}


void general_logging_thread_2(void *param) {
    ma_uint8_t counter = 0;

    for(counter = 0; counter < 10; counter++)
    {
       MA_LOG(global_logger, MA_LOG_SEV_PERFORMANCE, "Logging by general logger 2 - "
                                                     "00000000000000000000000000000000000000000000000000"
                                                     "11111111111111111111111111111111111111111111111111"
                                                     "22222222222222222222222222222222222222222222222222"
                                                     "33333333333333333333333333333333333333333333333333"
                                                     "44444444444444444444444444444444444444444444444444"
                                                     "55555555555555555555555555555555555555555555555555"
                                                     "66666666666666666666666666666666666666666666666666"
                                                     "77777777777777777777777777777777777777777777777777"
                                                     "88888888888888888888888888888888888888888888888888"
                                                     "99999999999999999999999999999999999999999999999999\n");
    }

    return ;
}

void general_logging_thread_3(void *param) {
    ma_uint8_t counter = 0;

    for(counter = 0; counter < 10; counter++)
    {
       MA_LOG(global_logger, MA_LOG_SEV_WARNING, "Logging by general logger 3 - "
                                                 "@#@$#@$#%$%$^$^%^&^%&^*^%$#%#$@#$@#@!#@#$#@$#%$#%$"
                                                 "<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
                                                 "{{{{{{{{{{{{{{{{{{{{}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}"
                                                 "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$"
                                                 "!@#$#$#%$%$^%^&^*&^*&(*(*)(*)((_(*&*&^%^%$^$%$#@#$\n");
    }

    return ;
}

/*
Verify the console logging between three threads. This test verifies that the
long log messages are not trimmed when thread switching occurs.

Scenario - Each thread opens its own logger handle, writes on console and closes
the handle.
*/
TEST(ma_log_threading_tests, console_logging_3_threads_different_logger_handle) {

    uv_thread_t tid_1 , tid_2 , tid_3 ;

    /* Creating thread 1 */
    TEST_ASSERT_MESSAGE( 0 == uv_thread_create(&tid_1 , console_logging_thread_1 , NULL) , "Thread could not be created");

    /* Creating thread 2 */
    TEST_ASSERT_MESSAGE( 0 == uv_thread_create(&tid_2 , console_logging_thread_2 , NULL) , "Thread could not be created");

    /* Creating thread 3 */
    TEST_ASSERT_MESSAGE( 0 == uv_thread_create(&tid_3 , console_logging_thread_3 , NULL) , "Thread could not be created");

    // Wait until all threads have terminated.
    uv_thread_join(&tid_1);     uv_thread_join(&tid_2);   uv_thread_join(&tid_3);
}

/*
Verify the file logging between three threads. This test verifies that the
long log messages are not trimmed when thread switching occurs.

Scenario - Each thread opens its own logger handle, writes on file and closes
the handle.
*/
TEST(ma_log_threading_tests, file_logging_3_threads_different_logger_handle) {
    uv_thread_t tid_1 , tid_2 , tid_3 ;

    /* Creating thread 1 */
    TEST_ASSERT_MESSAGE( 0 == uv_thread_create(&tid_1 , file_logging_thread_1 , NULL) , "Thread could not be created");

    /* Creating thread 2 */
    TEST_ASSERT_MESSAGE( 0 == uv_thread_create(&tid_2 , file_logging_thread_2 , NULL) , "Thread could not be created");

    /* Creating thread 3 */
    TEST_ASSERT_MESSAGE( 0 == uv_thread_create(&tid_3 , file_logging_thread_3 , NULL) , "Thread could not be created");

    // Wait until all threads have terminated.
    uv_thread_join(&tid_1);     uv_thread_join(&tid_2);   uv_thread_join(&tid_3);

}

/*
Verify the console logging between three threads. This test verifies that the
long log messages are not trimmed when thread switching occurs.

Scenario - Each thread uses the same global logger handle, and writes to console.
*/
TEST(ma_log_threading_tests, console_logging_3_threads_same_logger_handle) {
    ma_log_filter_t *filter = NULL;

    /* Thread handles */
    uv_thread_t tid_1 , tid_2 , tid_3 ;

    /* Creating logger for debug, performance and warning logging */
    TEST_ASSERT_MESSAGE(ma_console_logger_create((ma_console_logger_t**)&global_logger) == MA_OK, \
                        "Console logger could not be created");

    TEST_ASSERT_MESSAGE(ma_generic_log_filter_create("*.Debug|*.Warning|*.Performance" , &filter) == MA_OK,
                        "Filter could not be created");

    TEST_ASSERT_MESSAGE(ma_logger_add_filter(global_logger, filter) == MA_OK, \
                        "Filter could not be added for Console Logger");

    /* Creating thread 1 */
    TEST_ASSERT_MESSAGE( 0 == uv_thread_create(&tid_1 , general_logging_thread_1 , NULL) , "Thread could not be created");

    /* Creating thread 2 */
    TEST_ASSERT_MESSAGE( 0 == uv_thread_create(&tid_2 , general_logging_thread_2 , NULL) , "Thread could not be created");

    /* Creating thread 3 */
    TEST_ASSERT_MESSAGE( 0 == uv_thread_create(&tid_3 , general_logging_thread_3 , NULL) , "Thread could not be created");

    // Wait until all threads have terminated.
    uv_thread_join(&tid_1);     uv_thread_join(&tid_2);   uv_thread_join(&tid_3);


    /* Destroying filter */
    ma_log_filter_release(filter);

    /* Destroying Logger */
    ma_logger_dec_ref(global_logger);
}

/*
Verify the file logging between three threads. This test verifies that the
long log messages are not trimmed when thread switching occurs.

Scenario - Each thread uses the same global logger handle, and writes to file.
*/
TEST(ma_log_threading_tests, file_logging_3_threads_same_logger_handle) {
    ma_log_filter_t *filter = NULL;

    /* Thread handles */
    uv_thread_t tid_1 , tid_2 , tid_3 ;

    /* Creating logger for debug, performance and warning logging */
    TEST_ASSERT_MESSAGE(ma_file_logger_create(NULL, "test_file_logger", ".log", (ma_file_logger_t**)&global_logger) == MA_OK, \
                        "File Logger could not be created");
	TEST_ASSERT(MA_OK == ma_file_logger_set_policy((ma_file_logger_t*)global_logger, &logger_policy));

    TEST_ASSERT_MESSAGE(ma_generic_log_filter_create("*.Debug|*.Warning|*.Performance" , &filter) == MA_OK,
                        "Filter could not be created");

    TEST_ASSERT_MESSAGE(ma_logger_add_filter(global_logger, filter) == MA_OK, \
                        "Filter could not be added for File Logger");

    /* Creating thread 1 */
    TEST_ASSERT_MESSAGE( 0 == uv_thread_create(&tid_1 , general_logging_thread_1 , NULL) , "Thread could not be created");

    /* Creating thread 2 */
    TEST_ASSERT_MESSAGE( 0 == uv_thread_create(&tid_2 , general_logging_thread_2 , NULL) , "Thread could not be created");


    /* Creating thread 3 */
    TEST_ASSERT_MESSAGE( 0 == uv_thread_create(&tid_3 , general_logging_thread_3 , NULL) , "Thread could not be created");


    // Wait until all threads have terminated.
    uv_thread_join(&tid_1);     uv_thread_join(&tid_2);   uv_thread_join(&tid_3);


    /* Destroying filter */
    ma_log_filter_release(filter);

    /* Destroying Logger */
    ma_logger_dec_ref(global_logger);
}



