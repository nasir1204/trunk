/*****************************************************************************
Logger unit tests covering the log msgs.
******************************************************************************/



#include "unity.h"
#include "unity_fixture.h"

#include <stdlib.h>
#include <string.h>

#include "ma/internal/utils/logger/ma_logger_internal.h"
#include "ma/logger/ma_log_filter.h"

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif

#define LOG_FACILITY_NAME "Logger_Test_Facility"

TEST_GROUP_RUNNER(ma_log_msg_test_group) {
    do {RUN_TEST_CASE(ma_log_msg_tests, verify_log_msg_data)} while (0);
    do {RUN_TEST_CASE(ma_log_msg_tests, verify_wchar_log_msg_data)} while (0);
}

TEST_SETUP(ma_log_msg_tests) {
    Unity.TestFile = "ma_log_msg_test.c";
}

TEST_TEAR_DOWN(ma_log_msg_tests) {

}

 /* Log message content */
ma_log_msg_t log_msg =
{
    MA_LOG_SEV_NOTICE,                         // Severity
    "MA Logger Test Facility",                 // Facility
    0,                                         // Not wide char
    (const void *)"This is McAfee Agent Software Testing",   // Message
    1234,                                      // PID
    5678,                                      // Thread ID
    { 2013, 1, 8, 23, 40, 56, 857 },           // Date and Time
#ifdef MA_WINDOWS
    "Test Station 10",                         // Windows Station
    9999,                                      // Session ID
#else
    9999,                                      // UID
#endif
    "QA Machine",                              // Machine Name
    "Utility Test",                            // File Name
    "Formatting Function",                     // Function Name
    100 
};                                     // Line Number

ma_log_msg_t log_wchar_msg =
{
    MA_LOG_SEV_NOTICE,                         // Severity
    "MA Logger Test Facility",                 // Facility
    1,                                         // Not wide char
    L"This is McAfee Agent Software Testing for wchar msg",   // Message
    1234,                                      // PID
    5678,                                      // Thread ID
    { 2013, 1, 8, 23, 40, 56, 857 },           // Date and Time
#ifdef MA_WINDOWS
    "Test Station 10",                         // Windows Station
    9999,                                      // Session ID
#else
    9999,                                      // UID
#endif
    "QA Machine",                              // Machine Name
    "Utility Test",                            // File Name
    "Formatting Function",                     // Function Name
    100 
};  

TEST(ma_log_msg_tests, verify_log_msg_data) {
    ma_log_severity_t sev;
    ma_buffer_t *buffer = NULL;
    const char *str = NULL;
    size_t size = 0;
    ma_bool_t is_wchar = MA_FALSE;

	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_log_msg_get_severity(NULL, &sev));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_log_msg_get_severity(&log_msg, NULL));
    TEST_ASSERT_MESSAGE(MA_OK == ma_log_msg_get_severity(&log_msg, &sev), "ma_log_msg_get_severity Failed");
    TEST_ASSERT(sev == MA_LOG_SEV_NOTICE);

	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_log_msg_get_severity_str(NULL, &buffer));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_log_msg_get_severity_str(&log_msg, NULL));
    TEST_ASSERT_MESSAGE(MA_OK == ma_log_msg_get_severity_str(&log_msg, &buffer), "ma_log_msg_get_severity_str Failed");
    ma_buffer_get_string(buffer, &str, &size);
    TEST_ASSERT(!strcmp(str, "Notice"));
    ma_buffer_release(buffer); buffer = NULL;

	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_log_msg_get_facility(NULL, &buffer));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_log_msg_get_facility(&log_msg, NULL));
    TEST_ASSERT_MESSAGE(MA_OK == ma_log_msg_get_facility(&log_msg, &buffer), "ma_log_msg_get_facility Failed");
    ma_buffer_get_string(buffer, &str, &size);
    TEST_ASSERT(!strcmp(str, "MA Logger Test Facility"));
    ma_buffer_release(buffer); buffer = NULL;

	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_log_msg_is_wchar_message(NULL, &is_wchar));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_log_msg_is_wchar_message(&log_msg, NULL));
	is_wchar = MA_TRUE;
    TEST_ASSERT_MESSAGE(MA_OK == ma_log_msg_is_wchar_message(&log_msg, &is_wchar), "ma_log_msg_is_wchar_message Failed");
    TEST_ASSERT(is_wchar == MA_FALSE);   

	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_log_msg_get_message(NULL, &buffer));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_log_msg_get_message(&log_msg, NULL));
	TEST_ASSERT_MESSAGE(MA_OK == ma_log_msg_get_message(&log_msg, &buffer), "ma_log_msg_get_message Failed");
    ma_buffer_get_string(buffer, &str, &size);
    TEST_ASSERT(!strcmp(str, "This is McAfee Agent Software Testing"));
    ma_buffer_release(buffer); buffer = NULL;
}

TEST(ma_log_msg_tests, verify_wchar_log_msg_data) {
    ma_log_severity_t sev;
    ma_buffer_t *buffer = NULL;
    ma_wbuffer_t *w_buffer = NULL;
    const char *str = NULL;
    size_t size = 0;
    const wchar_t *w_str = NULL;
    ma_bool_t is_wchar = MA_FALSE;

	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_log_msg_get_severity(NULL, &sev));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_log_msg_get_severity(&log_wchar_msg, NULL));
    TEST_ASSERT_MESSAGE(MA_OK == ma_log_msg_get_severity(&log_wchar_msg, &sev), "ma_log_msg_get_severity Failed");
    TEST_ASSERT(sev == MA_LOG_SEV_NOTICE);

	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_log_msg_get_severity_str(NULL, &buffer));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_log_msg_get_severity_str(&log_wchar_msg, NULL));
    TEST_ASSERT_MESSAGE(MA_OK == ma_log_msg_get_severity_str(&log_wchar_msg, &buffer), "ma_log_msg_get_severity_str Failed");
    ma_buffer_get_string(buffer, &str, &size);
    TEST_ASSERT(!strcmp(str, "Notice"));
    ma_buffer_release(buffer); buffer = NULL;

	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_log_msg_get_facility(NULL, &buffer));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_log_msg_get_facility(&log_wchar_msg, NULL));
    TEST_ASSERT_MESSAGE(MA_OK == ma_log_msg_get_facility(&log_wchar_msg, &buffer), "ma_log_msg_get_facility Failed");
    ma_buffer_get_string(buffer, &str, &size);
    TEST_ASSERT(!strcmp(str, "MA Logger Test Facility"));
    ma_buffer_release(buffer); buffer = NULL;

	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_log_msg_is_wchar_message(NULL, &is_wchar));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_log_msg_is_wchar_message(&log_wchar_msg, NULL));
	is_wchar = MA_FALSE;
    TEST_ASSERT_MESSAGE(MA_OK == ma_log_msg_is_wchar_message(&log_wchar_msg, &is_wchar), "ma_log_msg_is_wchar_message Failed");
    TEST_ASSERT(is_wchar == MA_TRUE);   

	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_log_msg_get_wchar_message(NULL, &w_buffer));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_log_msg_get_wchar_message(&log_wchar_msg, NULL));
	TEST_ASSERT_MESSAGE(MA_OK == ma_log_msg_get_wchar_message(&log_wchar_msg, &w_buffer), "ma_log_msg_get_wchar_message Failed");
    ma_wbuffer_get_string(w_buffer, &w_str, &size);
    TEST_ASSERT(!wcscmp(w_str, L"This is McAfee Agent Software Testing for wchar msg"));
    ma_wbuffer_release(w_buffer); w_buffer = NULL;
}