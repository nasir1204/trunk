
/*****************************************************************************
Logger unit tests
******************************************************************************/


#include "unity.h"
#include "unity_fixture.h"


#include "ma/logger/ma_file_logger.h"
#include "ma/logger/ma_console_logger.h"
#include "ma/internal/utils/logger/ma_logger_internal.h"

#ifndef MA_WINDOWS
#include <unistd.h>
#endif


#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif

#define LOG_FACILITY_NAME "Logger_Test_Facility"

static const char *callback_message = "This is a call back message forwarded by ma_logger";
static const wchar_t *w_callback_message = L"This is a call back message forwarded by ma_logger";
static ma_bool_t is_visitor_called = MA_FALSE;
static ma_bool_t is_localizer_called = MA_FALSE;
static ma_file_logger_policy_t logger_policy = { MA_TRUE, (char*)DEFAULT_FILE_LOG_FORMAT_PATTERN, 2, MA_FALSE, { "previous", 5 } };

TEST_GROUP_RUNNER(ma_log_tests_group) {
    do {RUN_TEST_CASE(ma_log_tests, verify_basic_logging)} while (0);
    do {RUN_TEST_CASE(ma_log_tests, verify_multiple_file_loggers)} while (0);
    do {RUN_TEST_CASE(ma_log_tests, verify_multiple_console_loggers)} while (0);
    do {RUN_TEST_CASE(ma_log_tests, verify_additional_log_parameters)} while (0);
    do {RUN_TEST_CASE(ma_log_tests, register_deregister_cbs)} while (0);
}

TEST_SETUP(ma_log_tests) {
    /*Clean up the old one's , if by chance they are there */
    unlink("test_file_logger.log");

    Unity.TestFile = "ma_log_test.c";

}

TEST_TEAR_DOWN(ma_log_tests) {
    /*Remove the files which this group created */
    unlink("test_file_logger.log");

}

/*
Verify the basic functionality by logging the same messages on file and console.
Covers - Long messages, wide characters, special characters logging on console
and in file.
*/
TEST(ma_log_tests, verify_basic_logging)
{
    ma_file_logger_t *file_logger = NULL;
    ma_console_logger_t *console_logger = NULL;
    ma_log_filter_t *filter = NULL;
	
	ma_log_msg_t log_msg = {0};
	
    /* File Logger creation - Invalid scenarios */
    TEST_ASSERT_MESSAGE(ma_file_logger_create(NULL, NULL, NULL, NULL) == MA_ERROR_INVALIDARG, \
                        "ma_file_logger_create doesn't return MA_ERROR_INVALIDARG");

    TEST_ASSERT_MESSAGE(ma_file_logger_create(NULL, NULL, ".log", &file_logger) == MA_ERROR_INVALIDARG, \
                        "ma_file_logger_create doesn't return MA_ERROR_INVALIDARG");

    TEST_ASSERT_MESSAGE(ma_file_logger_create(NULL, "test_file_logger", ".log", NULL) == MA_ERROR_INVALIDARG, \
                        "ma_file_logger_create doesn't return MA_ERROR_INVALIDARG");

    /* Console Logger creation - Invalid scenarios */
    TEST_ASSERT_MESSAGE(ma_console_logger_create(NULL) == MA_ERROR_INVALIDARG, \
                        "ma_console_logger_create doesn't return MA_ERROR_INVALIDARG");

    /* Valid File Logger creation. */
    TEST_ASSERT_MESSAGE(ma_file_logger_create(NULL, "test_file_logger", ".log", &file_logger) == MA_OK, \
                        "File Logger could not be created");
	TEST_ASSERT(MA_OK == ma_file_logger_set_policy(file_logger, &logger_policy));

    /* Valid Console Logger creation */
    TEST_ASSERT_MESSAGE(ma_console_logger_create(&console_logger) == MA_OK, \
                        "Console logger could not be created");

    /* Filter creation */
    TEST_ASSERT_MESSAGE(ma_generic_log_filter_create("Logger_Test_Facility.*" , &filter) == MA_OK,
                        "Filter could not be created");

    /* Adding filter with the logger - Invalid scenarios */
    TEST_ASSERT_MESSAGE(ma_logger_add_filter(NULL, filter) == MA_ERROR_INVALIDARG, \
                        "ma_logger_add_filter doesn't return MA_ERROR_INVALIDARG for invalid arguments");

    TEST_ASSERT_MESSAGE(ma_logger_add_filter((ma_logger_t *)file_logger, NULL) == MA_ERROR_INVALIDARG, \
                        "ma_logger_add_filter doesn't return MA_ERROR_INVALIDARG for invalid arguments");

    TEST_ASSERT_MESSAGE(ma_logger_add_filter((ma_logger_t *)console_logger, NULL) == MA_ERROR_INVALIDARG, \
                        "ma_logger_add_filter doesn't return MA_ERROR_INVALIDARG for invalid arguments");

    /* Adding filter with the logger - Valid scenarios */
    TEST_ASSERT_MESSAGE(ma_logger_add_filter((ma_logger_t *)file_logger, filter) == MA_OK, \
                        "Filter could not be added for File Logger");

    TEST_ASSERT_MESSAGE(ma_logger_add_filter((ma_logger_t *)console_logger, filter) == MA_OK, \
                        "Filter could not be added for Console Logger");

	/* Negative scenario */
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_log(NULL, MA_LOG_SEV_CRITICAL, "ma_log_test", "ma_log_test.c", 10, "verify_basic_logging","test message"));

    /* Regular message */
    MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_CRITICAL, "Critical Error to be logged to file");
    MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Critical Error to be logged to console");

	/*Exact 256 characters*/
	MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_CRITICAL, "%s%s%s%s%s%s",
		"Line with 50 chars................................",
		"Line with 50 chars................................",
		"Line with 50 chars................................",
		"Line with 50 chars................................",
		"Line with 50 chars................................",
		"Ends..");

    MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_CRITICAL, "%s%s %s%s %s%s %s%s", "This is a very formatted message. ",
        "The quick brown fox jumps over the lazy dog. ",
        "We all go a little mad sometimes.",
        "You can't handle the truth!",
        "You had me at 'hello'",
        "Show me the money",
        "And in the morning, I'm making waffles!",

        "Game over, man! Game over!"
        "I love the smell of napalm in the morning...Smells like victory."
        "I'll be back."
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'");



    /* Long log message */
    MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_CRITICAL, "This is a very very long message. "
                                              "The quick brown fox jumps over the lazy dog. "
                                              "We all go a little mad sometimes."
                                              "You can't handle the truth!"
                                              "You had me at 'hello'"
                                              "Show me the money"
                                              "And in the morning, I'm making waffles!"
                                              "Game over, man! Game over!"
                                              "I love the smell of napalm in the morning...Smells like victory."
                                              "I'll be back."
                                              "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'");
    MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "This is a very very long message. "
                                              "The quick brown fox jumps over the lazy dog. "
                                              "We all go a little mad sometimes."
                                              "You can't handle the truth!"
                                              "You had me at 'hello'"
                                              "Show me the money"
                                              "And in the morning, I'm making waffles!"
                                              "Game over, man! Game over!"
                                              "I love the smell of napalm in the morning...Smells like victory."
                                              "I'll be back."
                                              "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'");


    /* Wide character */
	
	/* Negative scenario */
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_wlog(NULL, MA_LOG_SEV_CRITICAL, "ma_log_test", "ma_log_test.c", 10, "verify_basic_logging",L"test message"));

    MA_WLOG((ma_logger_t *)file_logger, MA_LOG_SEV_CRITICAL, L"WWWWWWW--This is a very very long message.#@#$%^&*("
                                              L"The quick brown fox jumps over the lazy dog. "
                                              L"We all go a little mad sometimes."
                                              L"You can't handle the truth!");
    MA_WLOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, L"WWWWWWW--This is a very very long message.#@#$%^&*("
                                              L"The quick brown fox jumps over the lazy dog. "
                                              L"We all go a little mad sometimes."
                                              L"You can't handle the truth!");

	/*Exact 512 characters*/
	MA_WLOG((ma_logger_t *)file_logger, MA_LOG_SEV_CRITICAL, L"%s%s%s%s%s%s%s%s%s%s%s",
		"Line with 50 chars................................",
		"Line with 50 chars................................",
		"Line with 50 chars................................",
		"Line with 50 chars................................",
		"Line with 50 chars................................",
		"Line with 50 chars................................",
		"Line with 50 chars................................",
		"Line with 50 chars................................",
		"Line with 50 chars................................",
		"Line with 50 chars................................",
		"Ends........");

    MA_WLOG((ma_logger_t *)file_logger, MA_LOG_SEV_CRITICAL, L"%s%s %s%s %s%s %s%s", L"This is a very formatted message. ",
        L"The quick brown fox jumps over the lazy dog. ",
        L"We all go a little mad sometimes.",
        L"You can't handle the truth!",
        L"You had me at 'hello'",
        L"Show me the money",
        L"And in the morning, I'm making waffles!",

        L"Game over, man! Game over!"
        L"I love the smell of napalm in the morning...Smells like victory."
        L"I'll be back."
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"LMy Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"LMy Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"LMy Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"LMy Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"LMy Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"LMy Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"LMy Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"LMy Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"LMy Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"LMy Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"LMy Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"LMy Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"LMy Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"LMy Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"LMy Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"LMy Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"LMy Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"LMy Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"LMy Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"LMy Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"LMy Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"LMy Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"LMy Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"LMy Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"LMy Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
        L"LMy Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'");


    /* Special characters */
    MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_CRITICAL, "@#@#@#$%$^%$^&%*&&^*&*(*()(*)(*)\"(&:{}[]<>?");
    MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "@#@#@#$%$^%$^&%*&&^*&*(*()(*)(*)\"(&:{}[]<>?");
	
	memset(&log_msg, 0, sizeof(ma_log_msg_t));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ((ma_logger_t *)file_logger)->methods->on_log_message(NULL, &log_msg));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ((ma_logger_t *)file_logger)->methods->on_log_message((ma_logger_t *)file_logger, NULL));

	TEST_ASSERT(MA_ERROR_INVALIDARG == ((ma_logger_t *)console_logger)->methods->on_log_message(NULL, &log_msg));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ((ma_logger_t *)console_logger)->methods->on_log_message((ma_logger_t *)console_logger, NULL));

    /* Destroying filter */
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_logger_remove_filter(NULL));
	TEST_ASSERT(MA_OK == ma_logger_remove_filter((ma_logger_t *)file_logger));
	TEST_ASSERT(MA_OK == ma_logger_remove_filter((ma_logger_t *)console_logger));
	((ma_log_filter_t *)filter)->methods->accept(NULL, &log_msg);
	((ma_log_filter_t *)filter)->methods->accept(filter, NULL);
	((ma_log_filter_t *)filter)->methods->destroy(NULL);
	ma_log_filter_release(filter);
    

	//FAIL-> TEST_ASSERT(MA_ERROR_INVALIDARG == ma_logger_inc_ref(NULL));
	TEST_ASSERT(MA_OK == ma_logger_inc_ref((ma_logger_t *)file_logger));
	//FAIL-> TEST_ASSERT(MA_ERROR_INVALIDARG == ma_logger_dec_ref(NULL));
	TEST_ASSERT(MA_OK == ma_logger_dec_ref((ma_logger_t *)file_logger));
	((ma_logger_t *)file_logger)->methods->release(NULL);

	//FAIL-> TEST_ASSERT(MA_ERROR_INVALIDARG == ma_logger_inc_ref(NULL));
	TEST_ASSERT(MA_OK == ma_logger_inc_ref((ma_logger_t *)console_logger));
	//FAIL-> TEST_ASSERT(MA_ERROR_INVALIDARG == ma_logger_dec_ref(NULL));
	TEST_ASSERT(MA_OK == ma_logger_dec_ref((ma_logger_t *)console_logger));
	((ma_logger_t *)console_logger)->methods->release(NULL);

	/* Destroying Logger */
	TEST_ASSERT(MA_OK == ma_file_logger_release(file_logger));
	TEST_ASSERT(MA_OK == ma_console_logger_release(console_logger));
}

/*
Verify logging by various file logger instances for different severity.
*/
TEST(ma_log_tests, verify_multiple_file_loggers)
{
    ma_file_logger_t *file_logger_1 = NULL;
    ma_file_logger_t *file_logger_2 = NULL;
    ma_file_logger_t *file_logger_3 = NULL;

    ma_log_filter_t *filter = NULL;

    /* File Logger 1 */
    TEST_ASSERT_MESSAGE(ma_file_logger_create(NULL, "test_file_logger", ".log", &file_logger_1) == MA_OK, \
                        "File Logger 1 could not be created");
	TEST_ASSERT(MA_OK == ma_file_logger_set_policy(file_logger_1, &logger_policy));

    TEST_ASSERT_MESSAGE(ma_generic_log_filter_create("*.Critical" , &filter) == MA_OK,
                        "Filter could not be created");

    TEST_ASSERT_MESSAGE(ma_logger_add_filter((ma_logger_t *)file_logger_1, filter) == MA_OK, \
                        "Filter could not be added for File Logger 1");

    /* File Logger 2 */
    TEST_ASSERT_MESSAGE(ma_file_logger_create(NULL, "test_file_logger", ".log", &file_logger_2) == MA_OK, \
                        "File Logger 2 could not be created");
	TEST_ASSERT(MA_OK == ma_file_logger_set_policy(file_logger_2, &logger_policy));

    TEST_ASSERT_MESSAGE(ma_generic_log_filter_create("*.Warning" , &filter) == MA_OK,
                        "Filter could not be created");

    TEST_ASSERT_MESSAGE(ma_logger_add_filter((ma_logger_t *)file_logger_2, filter) == MA_OK, \
                        "Filter could not be added for File Logger 2");

    /* File Logger 3 */
    TEST_ASSERT_MESSAGE(ma_file_logger_create(NULL, "test_file_logger", ".log", &file_logger_3) == MA_OK, \
                        "File Logger 3 could not be created");
	TEST_ASSERT(MA_OK == ma_file_logger_set_policy(file_logger_3, &logger_policy));

    TEST_ASSERT_MESSAGE(ma_generic_log_filter_create("*.Debug" , &filter) == MA_OK,
                        "Filter could not be created");

    TEST_ASSERT_MESSAGE(ma_logger_add_filter((ma_logger_t *)file_logger_3, filter) == MA_OK, \
                        "Filter could not be added for File Logger 3");

    /* Logging messages */
    MA_LOG((ma_logger_t *)file_logger_1, MA_LOG_SEV_CRITICAL, "Critical Error logged by File Logger 1");
    MA_LOG((ma_logger_t *)file_logger_1, MA_LOG_SEV_ERROR, "Error should not be logged by File Logger 1");
    MA_LOG((ma_logger_t *)file_logger_1, MA_LOG_SEV_WARNING, "Warning should not be logged by File Logger 1");
    MA_LOG((ma_logger_t *)file_logger_1, MA_LOG_SEV_INFO, "Information message should not be logged by File Logger 1");
    MA_LOG((ma_logger_t *)file_logger_1, MA_LOG_SEV_NOTICE, "Notice should not be logged by File Logger 1");
    MA_LOG((ma_logger_t *)file_logger_1, MA_LOG_SEV_DEBUG, "Debug message should not be logged by File Logger 1");
    MA_LOG((ma_logger_t *)file_logger_1, MA_LOG_SEV_TRACE, "Trace message should not be logged by File Logger 1");
    MA_LOG((ma_logger_t *)file_logger_1, MA_LOG_SEV_PERFORMANCE, "Performance message should not be logged by File Logger 1");

    MA_LOG((ma_logger_t *)file_logger_2, MA_LOG_SEV_CRITICAL, "Critical Error logged by File Logger 2");
    MA_LOG((ma_logger_t *)file_logger_2, MA_LOG_SEV_ERROR, "Error logged by File Logger 2");
    MA_LOG((ma_logger_t *)file_logger_2, MA_LOG_SEV_WARNING, "Warning logged by File Logger 2");
    MA_LOG((ma_logger_t *)file_logger_2, MA_LOG_SEV_INFO, "Information message should not be logged by File Logger 2");
    MA_LOG((ma_logger_t *)file_logger_2, MA_LOG_SEV_NOTICE, "Notice should not be logged by File Logger 2");
    MA_LOG((ma_logger_t *)file_logger_2, MA_LOG_SEV_DEBUG, "Debug message should not be logged by File Logger 2");
    MA_LOG((ma_logger_t *)file_logger_2, MA_LOG_SEV_TRACE, "Trace message should not be logged by File Logger 2");
    MA_LOG((ma_logger_t *)file_logger_2, MA_LOG_SEV_PERFORMANCE, "Performance message should not be logged by File Logger 2");

    MA_LOG((ma_logger_t *)file_logger_3, MA_LOG_SEV_CRITICAL, "Critical Error logged by File Logger 3");
    MA_LOG((ma_logger_t *)file_logger_3, MA_LOG_SEV_ERROR, "Error logged by File Logger 3");
    MA_LOG((ma_logger_t *)file_logger_3, MA_LOG_SEV_WARNING, "Warning logged by File Logger 3");
    MA_LOG((ma_logger_t *)file_logger_3, MA_LOG_SEV_INFO, "Information message logged by File Logger 3");
    MA_LOG((ma_logger_t *)file_logger_3, MA_LOG_SEV_NOTICE, "Notice logged by File Logger 3");
    MA_LOG((ma_logger_t *)file_logger_3, MA_LOG_SEV_DEBUG, "Debug message logged by File Logger 3");
    MA_LOG((ma_logger_t *)file_logger_3, MA_LOG_SEV_TRACE, "Trace message should not be logged by File Logger 3");
    MA_LOG((ma_logger_t *)file_logger_3, MA_LOG_SEV_PERFORMANCE, "Performance message should not be logged by File Logger 3");

    /* Gettting the filter associated with the logger and removing the filter so that all messages
       are accepted */

	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_logger_get_filter(NULL, &filter));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_logger_get_filter((ma_logger_t *)file_logger_1, NULL));
    /* File Logger 1 */
    TEST_ASSERT_MESSAGE(ma_logger_get_filter((ma_logger_t *)file_logger_1, &filter) == MA_OK,
                        "Filter associated with File Logger 1 could not be retrieved");

    TEST_ASSERT_MESSAGE(ma_logger_remove_filter((ma_logger_t *)file_logger_1) == MA_OK, \
                        "Filter associated with File Logger 1 could not be removed");

    ma_log_filter_release(filter);

    /* File Logger 2 */
    TEST_ASSERT_MESSAGE(ma_logger_get_filter((ma_logger_t *)file_logger_2, &filter) == MA_OK,
                        "Filter associated with File Logger 2 could not be retrieved");

    TEST_ASSERT_MESSAGE(ma_logger_remove_filter((ma_logger_t *)file_logger_2) == MA_OK, \
                        "Filter associated with File Logger 2 could not be removed");

    ma_log_filter_release(filter);

    /* File Logger 3 */
    TEST_ASSERT_MESSAGE(ma_logger_get_filter((ma_logger_t *)file_logger_3, &filter) == MA_OK,
                        "Filter associated with File Logger 3 could not be retrieved");

    TEST_ASSERT_MESSAGE(ma_logger_remove_filter((ma_logger_t *)file_logger_3) == MA_OK, \
                        "Filter associated with File Logger 3 could not be removed");

    ma_log_filter_release(filter);

    /* Logging messages */
    MA_LOG((ma_logger_t *)file_logger_1, MA_LOG_SEV_CRITICAL, "Critical Error logged by File Logger 1");
    MA_LOG((ma_logger_t *)file_logger_1, MA_LOG_SEV_ERROR, "Error logged by File Logger 1");
    MA_LOG((ma_logger_t *)file_logger_1, MA_LOG_SEV_WARNING, "Warning logged by File Logger 1");
    MA_LOG((ma_logger_t *)file_logger_1, MA_LOG_SEV_INFO, "Information message logged by File Logger 1");
    MA_LOG((ma_logger_t *)file_logger_1, MA_LOG_SEV_NOTICE, "Notice logged by File Logger 1");
    MA_LOG((ma_logger_t *)file_logger_1, MA_LOG_SEV_DEBUG, "Debug message logged by File Logger 1");
    MA_LOG((ma_logger_t *)file_logger_1, MA_LOG_SEV_TRACE, "Trace message logged by File Logger 1");
    MA_LOG((ma_logger_t *)file_logger_1, MA_LOG_SEV_PERFORMANCE, "Performance message logged by File Logger 1");

    MA_LOG((ma_logger_t *)file_logger_2, MA_LOG_SEV_CRITICAL, "Critical Error logged by File Logger 2");
    MA_LOG((ma_logger_t *)file_logger_2, MA_LOG_SEV_ERROR, "Error logged by File Logger 2");
    MA_LOG((ma_logger_t *)file_logger_2, MA_LOG_SEV_WARNING, "Warning logged by File Logger 2");
    MA_LOG((ma_logger_t *)file_logger_2, MA_LOG_SEV_INFO, "Information message logged by File Logger 2");
    MA_LOG((ma_logger_t *)file_logger_2, MA_LOG_SEV_NOTICE, "Notice logged by File Logger 2");
    MA_LOG((ma_logger_t *)file_logger_2, MA_LOG_SEV_DEBUG, "Debug message logged by File Logger 2");
    MA_LOG((ma_logger_t *)file_logger_2, MA_LOG_SEV_TRACE, "Trace message logged by File Logger 2");
    MA_LOG((ma_logger_t *)file_logger_2, MA_LOG_SEV_PERFORMANCE, "Performance message logged by File Logger 2");

    MA_LOG((ma_logger_t *)file_logger_3, MA_LOG_SEV_CRITICAL, "Critical Error logged by File Logger 3");
    MA_LOG((ma_logger_t *)file_logger_3, MA_LOG_SEV_ERROR, "Error logged by File Logger 3");
    MA_LOG((ma_logger_t *)file_logger_3, MA_LOG_SEV_WARNING, "Warning logged by File Logger 3");
    MA_LOG((ma_logger_t *)file_logger_3, MA_LOG_SEV_INFO, "Information message logged by File Logger 3");
    MA_LOG((ma_logger_t *)file_logger_3, MA_LOG_SEV_NOTICE, "Notice logged by File Logger 3");
    MA_LOG((ma_logger_t *)file_logger_3, MA_LOG_SEV_DEBUG, "Debug message logged by File Logger 3");
    MA_LOG((ma_logger_t *)file_logger_3, MA_LOG_SEV_TRACE, "Trace message logged by File Logger 3");
    MA_LOG((ma_logger_t *)file_logger_3, MA_LOG_SEV_PERFORMANCE, "Performance message logged by File Logger 3");

    /* Destroying Loggers */
    ma_logger_dec_ref((ma_logger_t *)file_logger_1);
    ma_logger_dec_ref((ma_logger_t *)file_logger_2);
    ma_logger_dec_ref((ma_logger_t *)file_logger_3);
}


/*
Verify logging by various console logger instances for different severity.
*/
TEST(ma_log_tests, verify_multiple_console_loggers)
{
    ma_console_logger_t *console_logger_1 = NULL;
    ma_console_logger_t *console_logger_2 = NULL;
    ma_console_logger_t *console_logger_3 = NULL;

    ma_log_filter_t *filter = NULL;

    /* Console Logger 1 */
    TEST_ASSERT_MESSAGE(ma_console_logger_create(&console_logger_1) == MA_OK, \
                        "Console Logger 1 could not be created");

    TEST_ASSERT_MESSAGE(ma_generic_log_filter_create("*.Critical" , &filter) == MA_OK,
                        "Filter could not be created");

    TEST_ASSERT_MESSAGE(ma_logger_add_filter((ma_logger_t *)console_logger_1, filter) == MA_OK, \
                        "Filter could not be added for Console Logger 1");

    /* Console Logger 2 */
    TEST_ASSERT_MESSAGE(ma_console_logger_create(&console_logger_2) == MA_OK, \
                        "Console Logger 2 could not be created");

    TEST_ASSERT_MESSAGE(ma_generic_log_filter_create("*.Warning" , &filter) == MA_OK,
                        "Filter could not be created");

    TEST_ASSERT_MESSAGE(ma_logger_add_filter((ma_logger_t *)console_logger_2, filter) == MA_OK, \
                        "Filter could not be added for Console Logger 2");

    /* Console Logger 3 */
    TEST_ASSERT_MESSAGE(ma_console_logger_create(&console_logger_3) == MA_OK, \
                        "Console Logger 3 could not be created");

    TEST_ASSERT_MESSAGE(ma_generic_log_filter_create("*.Debug" , &filter) == MA_OK,
                        "Filter could not be created");

    TEST_ASSERT_MESSAGE(ma_logger_add_filter((ma_logger_t *)console_logger_3, filter) == MA_OK, \
                        "Filter could not be added for Console Logger 3");

    /* Logging messages */
    MA_LOG((ma_logger_t *)console_logger_1, MA_LOG_SEV_CRITICAL, "Critical Error logged by Console Logger 1");
    MA_LOG((ma_logger_t *)console_logger_1, MA_LOG_SEV_ERROR, "Error should not be logged by Console Logger 1");
    MA_LOG((ma_logger_t *)console_logger_1, MA_LOG_SEV_WARNING, "Warning should not be logged by Console Logger 1");
    MA_LOG((ma_logger_t *)console_logger_1, MA_LOG_SEV_INFO, "Information message should not be logged by Console Logger 1");
    MA_LOG((ma_logger_t *)console_logger_1, MA_LOG_SEV_NOTICE, "Notice should not be logged by Console Logger 1");
    MA_LOG((ma_logger_t *)console_logger_1, MA_LOG_SEV_DEBUG, "Debug message should not be logged by Console Logger 1");
    MA_LOG((ma_logger_t *)console_logger_1, MA_LOG_SEV_TRACE, "Trace message should not be logged by Console Logger 1");
    MA_LOG((ma_logger_t *)console_logger_1, MA_LOG_SEV_PERFORMANCE, "Performance message should not be logged by Console Logger 1");

    MA_LOG((ma_logger_t *)console_logger_2, MA_LOG_SEV_CRITICAL, "Critical Error logged by Console Logger 2");
    MA_LOG((ma_logger_t *)console_logger_2, MA_LOG_SEV_ERROR, "Error logged by Console Logger 2");
    MA_LOG((ma_logger_t *)console_logger_2, MA_LOG_SEV_WARNING, "Warning logged by Console Logger 2");
    MA_LOG((ma_logger_t *)console_logger_2, MA_LOG_SEV_INFO, "Information message should not be logged by Console Logger 2");
    MA_LOG((ma_logger_t *)console_logger_2, MA_LOG_SEV_NOTICE, "Notice should not be logged by Console Logger 2");
    MA_LOG((ma_logger_t *)console_logger_2, MA_LOG_SEV_DEBUG, "Debug message should not be logged by Console Logger 2");
    MA_LOG((ma_logger_t *)console_logger_2, MA_LOG_SEV_TRACE, "Trace message should not be logged by Console Logger 2");
    MA_LOG((ma_logger_t *)console_logger_2, MA_LOG_SEV_PERFORMANCE, "Performance message should not be logged by Console Logger 2");

    MA_LOG((ma_logger_t *)console_logger_3, MA_LOG_SEV_CRITICAL, "Critical Error logged by Console Logger 3");
    MA_LOG((ma_logger_t *)console_logger_3, MA_LOG_SEV_ERROR, "Error logged by Console Logger 3");
    MA_LOG((ma_logger_t *)console_logger_3, MA_LOG_SEV_WARNING, "Warning logged by Console Logger 3");
    MA_LOG((ma_logger_t *)console_logger_3, MA_LOG_SEV_INFO, "Information message logged by Console Logger 3");
    MA_LOG((ma_logger_t *)console_logger_3, MA_LOG_SEV_NOTICE, "Notice logged by Console Logger 3");
    MA_LOG((ma_logger_t *)console_logger_3, MA_LOG_SEV_DEBUG, "Debug message logged by Console Logger 3");
    MA_LOG((ma_logger_t *)console_logger_3, MA_LOG_SEV_TRACE, "Trace message should not be logged by Console Logger 3");
    MA_LOG((ma_logger_t *)console_logger_3, MA_LOG_SEV_PERFORMANCE, "Performance message should not be logged by Console Logger 3");

    /* Gettting the filter associated with the logger and removing the filter so that all messages
       are accepted */

    /* Console Logger 1 */
    TEST_ASSERT_MESSAGE(ma_logger_get_filter((ma_logger_t *)console_logger_1, &filter) == MA_OK,
                        "Filter associated with Console Logger 1 could not be retrieved");

    TEST_ASSERT_MESSAGE(ma_logger_remove_filter((ma_logger_t *)console_logger_1) == MA_OK, \
                        "Filter associated with Console Logger 1 could not be removed");

    ma_log_filter_release(filter);

    /* Console Logger 2 */
    TEST_ASSERT_MESSAGE(ma_logger_get_filter((ma_logger_t *)console_logger_2, &filter) == MA_OK,
                        "Filter associated with Console Logger 2 could not be retrieved");

    TEST_ASSERT_MESSAGE(ma_logger_remove_filter((ma_logger_t *)console_logger_2) == MA_OK, \
                        "Filter associated with Console Logger 2 could not be removed");

    ma_log_filter_release(filter);

    /* Console Logger 3 */
    TEST_ASSERT_MESSAGE(ma_logger_get_filter((ma_logger_t *)console_logger_3, &filter) == MA_OK,
                        "Filter associated with Console Logger 3 could not be retrieved");

    TEST_ASSERT_MESSAGE(ma_logger_remove_filter((ma_logger_t *)console_logger_3) == MA_OK, \
                        "Filter associated with Console Logger 3 could not be removed");

    ma_log_filter_release(filter);

    /* Logging messages */
    MA_LOG((ma_logger_t *)console_logger_1, MA_LOG_SEV_CRITICAL, "Critical Error logged by Console Logger 1");
    MA_LOG((ma_logger_t *)console_logger_1, MA_LOG_SEV_ERROR, "Error logged by Console Logger 1");
    MA_LOG((ma_logger_t *)console_logger_1, MA_LOG_SEV_WARNING, "Warning logged by Console Logger 1");
    MA_LOG((ma_logger_t *)console_logger_1, MA_LOG_SEV_INFO, "Information message logged by Console Logger 1");
    MA_LOG((ma_logger_t *)console_logger_1, MA_LOG_SEV_NOTICE, "Notice logged by Console Logger 1");
    MA_LOG((ma_logger_t *)console_logger_1, MA_LOG_SEV_DEBUG, "Debug message logged by Console Logger 1");
    MA_LOG((ma_logger_t *)console_logger_1, MA_LOG_SEV_TRACE, "Trace message logged by Console Logger 1");
    MA_LOG((ma_logger_t *)console_logger_1, MA_LOG_SEV_PERFORMANCE, "Performance message logged by Console Logger 1");

    MA_LOG((ma_logger_t *)console_logger_2, MA_LOG_SEV_CRITICAL, "Critical Error logged by Console Logger 2");
    MA_LOG((ma_logger_t *)console_logger_2, MA_LOG_SEV_ERROR, "Error logged by Console Logger 2");
    MA_LOG((ma_logger_t *)console_logger_2, MA_LOG_SEV_WARNING, "Warning logged by Console Logger 2");
    MA_LOG((ma_logger_t *)console_logger_2, MA_LOG_SEV_INFO, "Information message logged by Console Logger 2");
    MA_LOG((ma_logger_t *)console_logger_2, MA_LOG_SEV_NOTICE, "Notice logged by Console Logger 2");
    MA_LOG((ma_logger_t *)console_logger_2, MA_LOG_SEV_DEBUG, "Debug message logged by Console Logger 2");
    MA_LOG((ma_logger_t *)console_logger_2, MA_LOG_SEV_TRACE, "Trace message logged by Console Logger 2");
    MA_LOG((ma_logger_t *)console_logger_2, MA_LOG_SEV_PERFORMANCE, "Performance message logged by Console Logger 2");

    MA_LOG((ma_logger_t *)console_logger_3, MA_LOG_SEV_CRITICAL, "Critical Error logged by Console Logger 3");
    MA_LOG((ma_logger_t *)console_logger_3, MA_LOG_SEV_ERROR, "Error logged by Console Logger 3");
    MA_LOG((ma_logger_t *)console_logger_3, MA_LOG_SEV_WARNING, "Warning logged by Console Logger 3");
    MA_LOG((ma_logger_t *)console_logger_3, MA_LOG_SEV_INFO, "Information message logged by Console Logger 3");
    MA_LOG((ma_logger_t *)console_logger_3, MA_LOG_SEV_NOTICE, "Notice logged by Console Logger 3");
    MA_LOG((ma_logger_t *)console_logger_3, MA_LOG_SEV_DEBUG, "Debug message logged by Console Logger 3");
    MA_LOG((ma_logger_t *)console_logger_3, MA_LOG_SEV_TRACE, "Trace message logged by Console Logger 3");
    MA_LOG((ma_logger_t *)console_logger_3, MA_LOG_SEV_PERFORMANCE, "Performance message logged by Console Logger 3");

    /* Destroying Loggers */
    ma_logger_dec_ref((ma_logger_t *)console_logger_1);
    ma_logger_dec_ref((ma_logger_t *)console_logger_2);
    ma_logger_dec_ref((ma_logger_t *)console_logger_3);
}


/*
Verify that the basic functionality by logging the same messages on file and console.
Covers - Long messages, wide characters, special characters logging on console
and in file.
*/
TEST(ma_log_tests, verify_additional_log_parameters)
{
    ma_file_logger_t *file_logger = NULL;
    ma_console_logger_t *console_logger = NULL;

    /* File Logger and Console Logger creation. */
    TEST_ASSERT_MESSAGE(ma_file_logger_create(NULL, "test_file_logger", ".log", &file_logger) == MA_OK, \
                        "File Logger could not be created");
	TEST_ASSERT(MA_OK == ma_file_logger_set_policy(file_logger, &logger_policy));

    TEST_ASSERT_MESSAGE(ma_console_logger_create(&console_logger) == MA_OK, \
                        "Console logger could not be created");

    /* No filter is applied, so all messages should get logged */

    /* Logging 1 additional parameter */
    MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_CRITICAL, "Logging 1 additional parameter to file - %d", 12345);
    MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Logging 1 additional parameter to console - %d", 12345);
	
    /* Logging 2 additional parameters */
    MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_CRITICAL, "Logging 2 additional parameters to file - %d %c", 213213213, 'a');
    MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Logging 2 additional parameters to console - %d %c", 213213213, 'a');

    /* Logging 3 additional parameters */
    MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_CRITICAL, "Logging 3 additional parameters to file - %d %c %s", 3434345, '$', "McAfee agent software");
    MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Logging 3 additional parameters to console - %d %c %s", 3434345, '$', "McAfee agent software");

    /* Logging different POD types */
    MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_CRITICAL, "Logging different POD types to file - %d %c %s %f %lf", 3242343, '@', "NextGen MA Logger", 123.45f, 1.9e11);
    MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Logging different POD types to console - %d %c %s %f %lf", 3242343, '@', "NextGen MA Logger", 123.45f, 1.9e11);

    /* Destroying Logger */
    ma_logger_dec_ref((ma_logger_t *)file_logger);
    ma_logger_dec_ref((ma_logger_t *)console_logger);
}

static void publish_log_message(const ma_log_msg_t *log_msg, void *data) 
{
	if(log_msg && data && (0 == strcmp(log_msg->message.utf8_message, callback_message))) 
	{
        is_visitor_called = MA_TRUE;
    }
}

static ma_error_t localize_msg_cb(const char *msg, ma_buffer_t **localized_msg, void *data) 
{
    if(msg && localized_msg && data && (0 == strcmp(msg, callback_message)))
	{
		ma_variant_t *variant = NULL;
		if(MA_OK == ma_variant_create_from_string(msg, strlen(msg), &variant)) 
		{
			ma_variant_get_string_buffer(variant, localized_msg);
            ma_variant_release(variant);
		}
		is_localizer_called = MA_TRUE;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

void wait_milliseconds( unsigned long wait_milli)
{
#ifdef WIN32
	Sleep(wait_milli);
#else
	sleep(wait_milli/1000);
#endif
}
TEST(ma_log_tests, register_deregister_cbs) {
    ma_file_logger_t *file_logger = NULL;
	ma_uint32_t wait_count = 10;
	ma_log_msg_t log_msg = {0};

    /* Valid File Logger creation. */
    TEST_ASSERT_MESSAGE(ma_file_logger_create(NULL, "test_file_logger", ".log", &file_logger) == MA_OK, \
                        "File Logger could not be created");
	TEST_ASSERT(MA_OK == ma_file_logger_set_policy(file_logger, &logger_policy));

	MA_LOG((ma_logger_t*)file_logger, MA_LOG_SEV_DEBUG|MA_LOG_FLAG_LOCALIZE, callback_message);
	wait_count = 5;
	while(wait_count > 0)
	{
		wait_milliseconds(1000);
		wait_count--;
	}
	TEST_ASSERT(MA_FALSE == is_visitor_called);
	TEST_ASSERT(MA_FALSE == is_localizer_called);

	MA_WLOG((ma_logger_t*)file_logger, MA_LOG_SEV_DEBUG|MA_LOG_FLAG_LOCALIZE, w_callback_message);
	wait_count = 5;
	while(wait_count > 0)
	{
		wait_milliseconds(1000);
		wait_count--;
	}
	TEST_ASSERT(MA_FALSE == is_visitor_called);
	TEST_ASSERT(MA_FALSE == is_localizer_called);

	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_logger_register_visitor(NULL, &publish_log_message, file_logger));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_logger_register_visitor((ma_logger_t*)file_logger, NULL, file_logger));
	TEST_ASSERT(MA_OK == ma_logger_register_visitor((ma_logger_t*)file_logger, &publish_log_message, NULL));
    TEST_ASSERT(MA_OK == ma_logger_register_visitor((ma_logger_t*)file_logger, &publish_log_message, file_logger));

	memset(&log_msg, 0, sizeof(ma_log_msg_t));
	log_msg.message.utf8_message = "message to verify ma_logger_pass_message_to_visitor_api";
	ma_logger_pass_message_to_visitor((ma_logger_t*)file_logger, NULL);
	ma_logger_pass_message_to_visitor(NULL, &log_msg);
	ma_logger_pass_message_to_visitor((ma_logger_t*)file_logger, &log_msg);

	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_logger_register_localizer(NULL, &localize_msg_cb, file_logger));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_logger_register_localizer((ma_logger_t*)file_logger, NULL, file_logger));
	TEST_ASSERT(MA_OK == ma_logger_register_localizer((ma_logger_t*)file_logger, &localize_msg_cb, NULL));
    TEST_ASSERT(MA_OK == ma_logger_register_localizer((ma_logger_t*)file_logger, &localize_msg_cb, file_logger));

	ma_logger_pass_message_to_visitor((ma_logger_t*)file_logger, &log_msg);

	MA_LOG((ma_logger_t*)file_logger, MA_LOG_SEV_DEBUG|MA_LOG_FLAG_LOCALIZE, "this is a random message to verify localize callback returns MA_ERROR_INVALIDARG");
	MA_LOG((ma_logger_t*)file_logger, MA_LOG_SEV_DEBUG|MA_LOG_FLAG_LOCALIZE, callback_message);
	
	wait_count = 10;
	while(wait_count > 0 && ((is_visitor_called == MA_FALSE) ||(is_localizer_called == MA_FALSE)))
	{
		wait_milliseconds(1000);
		wait_count--;
	}
	TEST_ASSERT(MA_TRUE == is_visitor_called);
	TEST_ASSERT(MA_TRUE == is_localizer_called);

	MA_WLOG((ma_logger_t*)file_logger, MA_LOG_SEV_DEBUG|MA_LOG_FLAG_LOCALIZE, L"this is a random message to verify localize callback returns MA_ERROR_INVALIDARG");
	MA_WLOG((ma_logger_t*)file_logger, MA_LOG_SEV_DEBUG|MA_LOG_FLAG_LOCALIZE, w_callback_message);
	
	wait_count = 10;
	while(wait_count > 0 && ((is_visitor_called == MA_FALSE) ||(is_localizer_called == MA_FALSE)))
	{
		wait_milliseconds(1000);
		wait_count--;
	}
	TEST_ASSERT(MA_TRUE == is_visitor_called);
	TEST_ASSERT(MA_TRUE == is_localizer_called);

	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_logger_unregister_localizer(NULL));
	TEST_ASSERT(MA_OK == ma_logger_unregister_localizer((ma_logger_t*)file_logger));

	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_logger_unregister_visitor(NULL));
	TEST_ASSERT(MA_OK == ma_logger_unregister_visitor((ma_logger_t*)file_logger));
 
    /* Destroying Logger */
    ma_logger_dec_ref((ma_logger_t *)file_logger);
}

