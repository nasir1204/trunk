/*****************************************************************************
Logger unit tests covering the logger collection.
******************************************************************************/
#include "unity.h"
#include "unity_fixture.h"

#include "ma/logger/ma_file_logger.h"
#include "ma/logger/ma_console_logger.h"
#include "ma/internal/utils/logger/ma_logger_internal.h"
#include "ma/logger/ma_logger_collector.h"

#ifndef MA_WINDOWS
#include <unistd.h>
#endif

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif

#define LOG_FACILITY_NAME "Logger_Test_Facility"

ma_file_logger_policy_t logger_policy = { MA_TRUE, (char*)DEFAULT_FILE_LOG_FORMAT_PATTERN, 2, MA_FALSE, { "previous", 5 } };

TEST_GROUP_RUNNER(ma_logger_collector_tests_group) {
    do {RUN_TEST_CASE(ma_logger_collector_tests, verify_basic_collection_logging)} while (0);
    do {RUN_TEST_CASE(ma_logger_collector_tests, verify_multiple_file_console_logger_collection)} while (0);
    do {RUN_TEST_CASE(ma_logger_collector_tests, verify_logger_logger_collection_filter)} while (0);
}

TEST_SETUP(ma_logger_collector_tests) {
    /*Clean up the old one's , if by chance they are there */
    unlink("test_file_logger.log");

    Unity.TestFile = "ma_logger_collector_test.c";
}

TEST_TEAR_DOWN(ma_logger_collector_tests) {
    /*Remove the files which this group created */
    unlink("test_file_logger.log");
}

/*
Verify the basic collector functionality by logging the same messages on collection
of file and console logger.
Covers - Long messages, wide characters, special characters logging on console
and in file.
*/
TEST(ma_logger_collector_tests, verify_basic_collection_logging)
{
    ma_file_logger_t *file_logger = NULL;
    ma_console_logger_t *console_logger = NULL;
    ma_logger_collector_t *logger_collector = NULL;
    ma_logger_t *general_logger = NULL;

    /* File Logger and Console Logger creation. */
    TEST_ASSERT_MESSAGE(ma_file_logger_create(NULL, "test_file_logger", ".log", &file_logger) == MA_OK, \
                        "File Logger could not be created");
	TEST_ASSERT(MA_OK == ma_file_logger_set_policy(file_logger, &logger_policy));

    TEST_ASSERT_MESSAGE(ma_console_logger_create(&console_logger) == MA_OK, \
                        "Console logger could not be created");

    /* Invalid Logger collector creation */
    TEST_ASSERT_MESSAGE(ma_logger_collector_create(NULL) == MA_ERROR_INVALIDARG, \
                        "ma_logger_collector_create doesn't return MA_ERROR_INVALIDARG for invalid arguments");

    /* Creating valid logger collector */
    TEST_ASSERT_MESSAGE(ma_logger_collector_create(&logger_collector) == MA_OK, \
                        "Logger collection could not be created");

    /* Adding file and console logger to logger collector */
    TEST_ASSERT_MESSAGE(ma_logger_collector_add_logger(logger_collector, NULL) == MA_ERROR_INVALIDARG, \
                        "ma_logger_collector_add_logger doesn't return MA_ERROR_INVALIDARG");

    TEST_ASSERT_MESSAGE(ma_logger_collector_add_logger(NULL, (ma_logger_t *)file_logger) == MA_ERROR_INVALIDARG, \
                        "ma_logger_collector_add_logger doesn't return MA_ERROR_INVALIDARG");

    TEST_ASSERT_MESSAGE(ma_logger_collector_add_logger(logger_collector, (ma_logger_t *)file_logger) == MA_OK, \
                        "File Logger could not be added to logger collection");

    TEST_ASSERT_MESSAGE(ma_logger_collector_add_logger(logger_collector, (ma_logger_t *)console_logger) == MA_OK, \
                        "Console Logger could not be added to logger collection");

    /* Getting collection logger */
    TEST_ASSERT_MESSAGE(ma_logger_collector_get_impl(logger_collector, &general_logger) == MA_OK, \
                        "Logger of collection cannot be retrieved");

    /* Logging to collection of loggers */

    /* Regular message */
    MA_LOG(general_logger, MA_LOG_SEV_CRITICAL, "Critical Error to be logged to file and console");

    /* Long log message */
    MA_LOG(general_logger, MA_LOG_SEV_CRITICAL, "This is a very very long message. "
                                                "The quick brown fox jumps over the lazy dog. "
                                                "We all go a little mad sometimes."
                                                "You can't handle the truth!"
                                                "You had me at 'hello'"
                                                "Show me the money"
                                                "And in the morning, I'm making waffles!"
                                                "Game over, man! Game over!"
                                                "I love the smell of napalm in the morning...Smells like victory."
                                                "I'll be back."
                                                "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'");

    /* Wide character */
    MA_WLOG(general_logger, MA_LOG_SEV_CRITICAL, L"This is a very very long message.#@#$%^&*("
                                                 L"The quick brown fox jumps over the lazy dog. "
                                                 L"We all go a little mad sometimes."
                                                 L"You can't handle the truth!");

    /* Special characters */
    MA_LOG(general_logger, MA_LOG_SEV_CRITICAL, "@#@#@#$%$^%$^&%*&&^*&*(*()(*)(*)\"(&:{}[]<>?");

    /* Destroying Logger collection and logger */
    ma_logger_dec_ref(general_logger);
    ma_logger_collector_release(logger_collector);
    ma_logger_dec_ref((ma_logger_t *)file_logger);
    ma_logger_dec_ref((ma_logger_t *)console_logger);
}

/*
Verify logging by various file logger instances for different severity. Then,
removing the all loggers from logger collection andd verifying that no log
messages are logged.
*/
TEST(ma_logger_collector_tests, verify_multiple_file_console_logger_collection)
{
    // File and console logger object pointers
    ma_file_logger_t *file_logger_1 = NULL;
    ma_file_logger_t *file_logger_2 = NULL;
    ma_file_logger_t *file_logger_3 = NULL;
    ma_console_logger_t *console_logger_1 = NULL;
    ma_console_logger_t *console_logger_2 = NULL;
    ma_console_logger_t *console_logger_3 = NULL;

    ma_logger_collector_t *logger_collector = NULL;
    ma_logger_t *general_logger = NULL;

    // Filter objects
    ma_log_filter_t *filter_1 = NULL;
    ma_log_filter_t *filter_2 = NULL;
    ma_log_filter_t *filter_3 = NULL;

    /* File Logger 1 and Console Logger 1 */
    TEST_ASSERT_MESSAGE(ma_file_logger_create(NULL, "test_file_logger", ".log", &file_logger_1) == MA_OK, \
                        "File Logger 1 could not be created");
	TEST_ASSERT(MA_OK == ma_file_logger_set_policy(file_logger_1, &logger_policy));

    TEST_ASSERT_MESSAGE(ma_console_logger_create(&console_logger_1) == MA_OK, \
                        "Console Logger 1 could not be created");

    TEST_ASSERT_MESSAGE(ma_generic_log_filter_create("*.Critical" , &filter_1) == MA_OK,
                        "Filter could not be created");

    TEST_ASSERT_MESSAGE(ma_logger_add_filter((ma_logger_t *)file_logger_1, filter_1) == MA_OK, \
                        "Filter could not be added for File Logger 1");

    TEST_ASSERT_MESSAGE(ma_logger_add_filter((ma_logger_t *)console_logger_1, filter_1) == MA_OK, \
                        "Filter could not be added for Console Logger 1");

    /* File Logger 2 and Console Logger 2 */
    TEST_ASSERT_MESSAGE(ma_file_logger_create(NULL, "test_file_logger", ".log", &file_logger_2) == MA_OK, \
                        "File Logger 2 could not be created");
	TEST_ASSERT(MA_OK == ma_file_logger_set_policy(file_logger_2, &logger_policy));

    TEST_ASSERT_MESSAGE(ma_console_logger_create(&console_logger_2) == MA_OK, \
                        "Console Logger 2 could not be created");

    TEST_ASSERT_MESSAGE(ma_generic_log_filter_create("*.Warning" , &filter_2) == MA_OK,
                        "Filter could not be created");

    TEST_ASSERT_MESSAGE(ma_logger_add_filter((ma_logger_t *)file_logger_2, filter_2) == MA_OK, \
                        "Filter could not be added for File Logger 2");

    TEST_ASSERT_MESSAGE(ma_logger_add_filter((ma_logger_t *)console_logger_2, filter_2) == MA_OK, \
                        "Filter could not be added for Console Logger 2");

    /* File Logger 3 and Console Logger 3 */
    TEST_ASSERT_MESSAGE(ma_file_logger_create(NULL, "test_file_logger", ".log", &file_logger_3) == MA_OK, \
                        "File Logger 3 could not be created");
	TEST_ASSERT(MA_OK == ma_file_logger_set_policy(file_logger_3, &logger_policy));

    TEST_ASSERT_MESSAGE(ma_console_logger_create(&console_logger_3) == MA_OK, \
                        "Console Logger 3 could not be created");

    TEST_ASSERT_MESSAGE(ma_generic_log_filter_create("*.Debug" , &filter_3) == MA_OK,
                        "Filter could not be created");

    TEST_ASSERT_MESSAGE(ma_logger_add_filter((ma_logger_t *)file_logger_3, filter_3) == MA_OK, \
                        "Filter could not be added for File Logger 3");

    TEST_ASSERT_MESSAGE(ma_logger_add_filter((ma_logger_t *)console_logger_3, filter_3) == MA_OK, \
                        "Filter could not be added for Console Logger 3");

    /* Creating logger collector */
    TEST_ASSERT_MESSAGE(ma_logger_collector_create(&logger_collector) == MA_OK, \
                        "Logger collection could not be created");

    /* Adding loggers to logger collector */
    TEST_ASSERT_MESSAGE(ma_logger_collector_add_logger(logger_collector, (ma_logger_t *)file_logger_1) == MA_OK, \
                        "File Logger 1 could not be added to logger collection");

    TEST_ASSERT_MESSAGE(ma_logger_collector_add_logger(logger_collector, (ma_logger_t *)file_logger_2) == MA_OK, \
                        "File Logger 2 could not be added to logger collection");

    TEST_ASSERT_MESSAGE(ma_logger_collector_add_logger(logger_collector, (ma_logger_t *)file_logger_3) == MA_OK, \
                        "File Logger 3 could not be added to logger collection");

    TEST_ASSERT_MESSAGE(ma_logger_collector_add_logger(logger_collector, (ma_logger_t *)console_logger_1) == MA_OK, \
                        "Console Logger 1 could not be added to logger collection");

    TEST_ASSERT_MESSAGE(ma_logger_collector_add_logger(logger_collector, (ma_logger_t *)console_logger_2) == MA_OK, \
                        "Console Logger 2 could not be added to logger collection");

    TEST_ASSERT_MESSAGE(ma_logger_collector_add_logger(logger_collector, (ma_logger_t *)console_logger_3) == MA_OK, \
                        "Console Logger 3 could not be added to logger collection");

    /* Getting collection logger */
    TEST_ASSERT_MESSAGE(ma_logger_collector_get_impl(logger_collector, &general_logger) == MA_OK, \
                        "Logger of collection cannot be retrieved");

    /* Logging messages */
    MA_LOG(general_logger, MA_LOG_SEV_CRITICAL, "Critical Error logged by Logger Collection");
    MA_LOG(general_logger, MA_LOG_SEV_ERROR, "Error logged by Logger Collection");
    MA_LOG(general_logger, MA_LOG_SEV_WARNING, "Warning logged by Logger Collection");
    MA_LOG(general_logger, MA_LOG_SEV_INFO, "Information message logged by Logger Collection");
    MA_LOG(general_logger, MA_LOG_SEV_NOTICE, "Notice logged by Logger Collection");
    MA_LOG(general_logger, MA_LOG_SEV_DEBUG, "Debug message logged by Logger Collection");
    MA_LOG(general_logger, MA_LOG_SEV_PERFORMANCE, "Performance message logged by Logger Collection");

    /* Removing all loggers from collection */
    TEST_ASSERT_MESSAGE(ma_logger_collector_remove_logger(logger_collector, (ma_logger_t *)file_logger_1) == MA_OK, \
                        "File Logger 1 could not be removed from logger collection");

    TEST_ASSERT_MESSAGE(ma_logger_collector_remove_logger(logger_collector, (ma_logger_t *)file_logger_2) == MA_OK, \
                        "File Logger 2 could not be removed from logger collection");

    TEST_ASSERT_MESSAGE(ma_logger_collector_remove_logger(logger_collector, (ma_logger_t *)file_logger_3) == MA_OK, \
                        "File Logger 3 could not be removed from logger collection");

    TEST_ASSERT_MESSAGE(ma_logger_collector_remove_logger(logger_collector, (ma_logger_t *)console_logger_1) == MA_OK, \
                        "Console Logger 1 could not be removed from logger collection");

    TEST_ASSERT_MESSAGE(ma_logger_collector_remove_logger(logger_collector, (ma_logger_t *)console_logger_2) == MA_OK, \
                        "Console Logger 2 could not be removed from logger collection");

    TEST_ASSERT_MESSAGE(ma_logger_collector_remove_logger(logger_collector, (ma_logger_t *)console_logger_3) == MA_OK, \
                        "Console Logger 3 could not be removed from logger collection");

    /* Trying to log message through logger collection. Messages should not be logged because
       all loggers are already removed from the collection */
    MA_LOG(general_logger, MA_LOG_SEV_CRITICAL, "Critical Error - should not be logged by Logger Collection");
    MA_LOG(general_logger, MA_LOG_SEV_ERROR, "Error - should not be logged by Logger Collection");
    MA_LOG(general_logger, MA_LOG_SEV_WARNING, "Warning - should not be logged by Logger Collection");
    MA_LOG(general_logger, MA_LOG_SEV_INFO, "Information message - should not be logged by Logger Collection");
    MA_LOG(general_logger, MA_LOG_SEV_NOTICE, "Notice - should not be logged by Logger Collection");
    MA_LOG(general_logger, MA_LOG_SEV_DEBUG, "Debug message - should not be logged by Logger Collection");
    MA_LOG(general_logger, MA_LOG_SEV_PERFORMANCE, "Performance message - should not be logged by Logger Collection");

    /* Destroying Loggers and collection */
    ma_log_filter_release(filter_1);
    ma_log_filter_release(filter_2);
    ma_log_filter_release(filter_3);
    ma_logger_dec_ref(general_logger);
    ma_logger_collector_release(logger_collector);
    ma_logger_dec_ref((ma_logger_t *)file_logger_1);
    ma_logger_dec_ref((ma_logger_t *)file_logger_2);
    ma_logger_dec_ref((ma_logger_t *)file_logger_3);
    ma_logger_dec_ref((ma_logger_t *)console_logger_1);
    ma_logger_dec_ref((ma_logger_t *)console_logger_2);
    ma_logger_dec_ref((ma_logger_t *)console_logger_3);
}



/*
Verify logging performed by the collection logger when a filter is applied
on the logger derieved from the collection.
1) Create a collection logger containing the console loggers of different
   severity.
2) Apply the filter over collection logger.
3) See that the collection logger filter overrides the individual logger filters.
*/
TEST(ma_logger_collector_tests, verify_logger_logger_collection_filter)
{
    // Console logger object pointers
    ma_console_logger_t *console_logger_1 = NULL;
    ma_console_logger_t *console_logger_2 = NULL;
    ma_console_logger_t *console_logger_3 = NULL;

    ma_logger_collector_t *logger_collector = NULL;
    ma_logger_t *general_logger = NULL;

    // Filter objects
    ma_log_filter_t *filter_1 = NULL;
    ma_log_filter_t *filter_2 = NULL;
    ma_log_filter_t *filter_3 = NULL;
    ma_log_filter_t *filter_4 = NULL;

    /* Console Logger 1 */
    TEST_ASSERT_MESSAGE(ma_console_logger_create(&console_logger_1) == MA_OK, \
                        "Console Logger 1 could not be created");

    TEST_ASSERT_MESSAGE(ma_generic_log_filter_create("*.Error" , &filter_1) == MA_OK,
                        "Filter could not be created");

    TEST_ASSERT_MESSAGE(ma_logger_add_filter((ma_logger_t *)console_logger_1, filter_1) == MA_OK, \
                        "Filter could not be added for Console Logger 1");

    /* Console Logger 2 */
    TEST_ASSERT_MESSAGE(ma_console_logger_create(&console_logger_2) == MA_OK, \
                        "Console Logger 2 could not be created");

    TEST_ASSERT_MESSAGE(ma_generic_log_filter_create("*.Warning" , &filter_2) == MA_OK,
                        "Filter could not be created");

    TEST_ASSERT_MESSAGE(ma_logger_add_filter((ma_logger_t *)console_logger_2, filter_2) == MA_OK, \
                        "Filter could not be added for Console Logger 2");

    /* Console Logger 3 */
    TEST_ASSERT_MESSAGE(ma_console_logger_create(&console_logger_3) == MA_OK, \
                        "Console Logger 3 could not be created");

    TEST_ASSERT_MESSAGE(ma_generic_log_filter_create("*.Debug" , &filter_3) == MA_OK,
                        "Filter could not be created");

    TEST_ASSERT_MESSAGE(ma_logger_add_filter((ma_logger_t *)console_logger_3, filter_3) == MA_OK, \
                        "Filter could not be added for Console Logger 3");

    /* Creating logger collector */
    TEST_ASSERT_MESSAGE(ma_logger_collector_create(&logger_collector) == MA_OK, \
                        "Logger collection could not be created");

    /* Adding loggers to logger collector */
    TEST_ASSERT_MESSAGE(ma_logger_collector_add_logger(logger_collector, (ma_logger_t *)console_logger_1) == MA_OK, \
                        "Console Logger 1 could not be added to logger collection");

    TEST_ASSERT_MESSAGE(ma_logger_collector_add_logger(logger_collector, (ma_logger_t *)console_logger_2) == MA_OK, \
                        "Console Logger 2 could not be added to logger collection");

    TEST_ASSERT_MESSAGE(ma_logger_collector_add_logger(logger_collector, (ma_logger_t *)console_logger_3) == MA_OK, \
                        "Console Logger 3 could not be added to logger collection");

    /* Getting collection logger */
    TEST_ASSERT_MESSAGE(ma_logger_collector_get_impl(logger_collector, &general_logger) == MA_OK, \
                        "Logger of collection cannot be retrieved");

    /* Applying filter over collection logger */
    TEST_ASSERT_MESSAGE(ma_generic_log_filter_create("*.Critical" , &filter_4) == MA_OK,
                        "Filter could not be created");

    TEST_ASSERT_MESSAGE(ma_logger_add_filter(general_logger, filter_4) == MA_OK, \
                        "Filter could not be added for Collection Logger");

    /* Logging messages */
    MA_LOG(general_logger, MA_LOG_SEV_CRITICAL, "Critical Error logged by Logger Collection");
    MA_LOG(general_logger, MA_LOG_SEV_ERROR, "Error logged by Logger Collection");
    MA_LOG(general_logger, MA_LOG_SEV_WARNING, "Warning logged by Logger Collection");
    MA_LOG(general_logger, MA_LOG_SEV_INFO, "Information message logged by Logger Collection");
    MA_LOG(general_logger, MA_LOG_SEV_NOTICE, "Notice logged by Logger Collection");
    MA_LOG(general_logger, MA_LOG_SEV_DEBUG, "Debug message logged by Logger Collection");
    MA_LOG(general_logger, MA_LOG_SEV_PERFORMANCE, "Performance message logged by Logger Collection");

    /* Removing all loggers from collection */
    TEST_ASSERT_MESSAGE(ma_logger_collector_remove_logger(logger_collector, (ma_logger_t *)console_logger_1) == MA_OK, \
                        "Console Logger 1 could not be removed from logger collection");

    TEST_ASSERT_MESSAGE(ma_logger_collector_remove_logger(logger_collector, (ma_logger_t *)console_logger_2) == MA_OK, \
                        "Console Logger 2 could not be removed from logger collection");

    TEST_ASSERT_MESSAGE(ma_logger_collector_remove_logger(logger_collector, (ma_logger_t *)console_logger_3) == MA_OK, \
                        "Console Logger 3 could not be removed from logger collection");

    /* Trying to log message through logger collection. Messages should not be logged because
       all loggers are already removed from the collection */
    MA_LOG(general_logger, MA_LOG_SEV_CRITICAL, "Critical Error - should not be logged by Logger Collection");
    MA_LOG(general_logger, MA_LOG_SEV_ERROR, "Error - should not be logged by Logger Collection");
    MA_LOG(general_logger, MA_LOG_SEV_WARNING, "Warning - should not be logged by Logger Collection");
    MA_LOG(general_logger, MA_LOG_SEV_INFO, "Information message - should not be logged by Logger Collection");
    MA_LOG(general_logger, MA_LOG_SEV_NOTICE, "Notice - should not be logged by Logger Collection");
    MA_LOG(general_logger, MA_LOG_SEV_DEBUG, "Debug message - should not be logged by Logger Collection");
    MA_LOG(general_logger, MA_LOG_SEV_PERFORMANCE, "Performance message - should not be logged by Logger Collection");

    /* Destroying Loggers and collection */
    ma_log_filter_release(filter_1);
    ma_log_filter_release(filter_2);
    ma_log_filter_release(filter_3);
    ma_log_filter_release(filter_4);
    ma_logger_dec_ref(general_logger);
    ma_logger_collector_release(logger_collector);
    ma_logger_dec_ref((ma_logger_t *)console_logger_1);
    ma_logger_dec_ref((ma_logger_t *)console_logger_2);
    ma_logger_dec_ref((ma_logger_t *)console_logger_3);
}

