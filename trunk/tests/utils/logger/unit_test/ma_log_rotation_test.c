#include "unity.h"
#include "unity_fixture.h"


#include "ma/logger/ma_file_logger.h"


#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif

#include <sys/stat.h>

#ifndef MA_WINDOWS
#include <sys/types.h>
#include <unistd.h>
#endif

#define LOG_FACILITY_NAME "Logger_Test_Facility"

TEST_GROUP_RUNNER(ma_log_rotation_test_group)
{
    do {RUN_TEST_CASE(ma_log_rotation_tests, check_log_rotation)} while (0);
}

TEST_SETUP(ma_log_rotation_tests)
{
    Unity.TestFile = "ma_log_rotation_test.c";
}

TEST_TEAR_DOWN(ma_log_rotation_tests)
{
}


TEST(ma_log_rotation_tests, check_log_rotation)
{
    struct stat st = {0};
    ma_file_logger_t *file_logger  = NULL;

    int i = 0 ;

    char msg1[1024] = {0};
    char msg2[1024] = {0};
    char msg3[1024] = {0};
    char msg4[1024] = {0};
    char msg5[1024] = {0};
    char msg6[1024] = {0};
    char msg7[1024] = {0};
    char msg8[1024] = {0};
    char msg9[1024] = {0};
    char msg10[1024] = {0};
    char msg11[1024] = {0};
    char msg12[1024] = {0};
    char msg13[1024] = {0};

    // 1KB messages
    for(i = 0 ; i < 1023 ; i++) msg1[i] = 'a';
    for(i = 0 ; i < 1023 ; i++) msg2[i] = 'b';
    for(i = 0 ; i < 1023 ; i++) msg3[i] = 'c';
    for(i = 0 ; i < 1023 ; i++) msg4[i] = 'd';
    for(i = 0 ; i < 1023 ; i++) msg5[i] = 'e';
    for(i = 0 ; i < 1023 ; i++) msg6[i] = 'f';
    for(i = 0 ; i < 1023 ; i++) msg7[i] = 'g';
    for(i = 0 ; i < 1023 ; i++) msg8[i] = 'h';
    for(i = 0 ; i < 1023 ; i++) msg9[i] = 'i';
    for(i = 0 ; i < 1023 ; i++) msg10[i] = 'j';
    for(i = 0 ; i < 1023 ; i++) msg11[i] = 'k';
    for(i = 0 ; i < 1023 ; i++) msg12[i] = 'l';
    for(i = 0 ; i < 1023 ; i++) msg13[i] = 'm';

    /**************************************************************************
    TEST 1 -
    maximum rollovers = 0
    log size = 5 MB

    Only one log file should be created
    **************************************************************************/
    {
        ma_file_logger_policy_t logger_policy = { MA_TRUE, (char*)DEFAULT_FILE_LOG_FORMAT_PATTERN, 5, MA_TRUE, { "rolling", 0 } };

        TEST_ASSERT_MESSAGE(ma_file_logger_create(NULL, "my_5MB_file_logger", ".log", &file_logger) == MA_OK, "File Logger could not be created");
        TEST_ASSERT(MA_OK == ma_file_logger_set_policy(file_logger, &logger_policy));

        // Writing more than 5 MB data
        for(i = 0 ; i < 1024; i++) MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_DEBUG, msg1);
        for(i = 0 ; i < 1024; i++) MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_DEBUG, msg2);
        for(i = 0 ; i < 1024; i++) MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_DEBUG, msg3);
        for(i = 0 ; i < 1024; i++) MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_DEBUG, msg4);
        for(i = 0 ; i < 1024; i++) MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_DEBUG, msg5);
        for(i = 0 ; i < 1024; i++) MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_DEBUG, msg6);
        for(i = 0 ; i < 1024; i++) MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_DEBUG, msg7);
        for(i = 0 ; i < 1024; i++) MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_DEBUG, msg8);
        for(i = 0 ; i < 1024; i++) MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_DEBUG, msg9);
        for(i = 0 ; i < 1024; i++) MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_DEBUG, msg10);
        for(i = 0 ; i < 1024; i++) MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_DEBUG, msg11);
        for(i = 0 ; i < 1024; i++) MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_DEBUG, msg12);
        for(i = 0 ; i < 1024; i++) MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_DEBUG, msg13);

        TEST_ASSERT_MESSAGE(0 == stat("my_5MB_file_logger.log", &st), "Rollback of file must have failed ");
        TEST_ASSERT_MESSAGE(-1 == stat("my_5MB_file_logger_rolling_1.log", &st), "Rollback of file must have failed ");

        ma_file_logger_release(file_logger);
    }

    /**************************************************************************
    TEST 2 -
    maximum rollovers = 5
    log size = 10 MB

    Log should be rolled maximum 5 times. Maximum 6 log files should be created.
    ***************************************************************************/
    {
        ma_file_logger_policy_t logger_policy = { MA_TRUE, (char*)DEFAULT_FILE_LOG_FORMAT_PATTERN, 10, MA_TRUE, { "backup", 5 } };

        TEST_ASSERT_MESSAGE(ma_file_logger_create(NULL, "my_10MB_file_logger", ".log", &file_logger) == MA_OK, "File Logger could not be created");
        TEST_ASSERT(MA_OK == ma_file_logger_set_policy(file_logger, &logger_policy));

        // Writing more than 70 MB data
        for(i = 0 ; i < 5 * 1024; i++) MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_DEBUG, msg1);
        for(i = 0 ; i < 5 * 1024; i++) MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_DEBUG, msg2);
        for(i = 0 ; i < 5 * 1024; i++) MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_DEBUG, msg3);
        for(i = 0 ; i < 5 * 1024; i++) MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_DEBUG, msg4);
        for(i = 0 ; i < 5 * 1024; i++) MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_DEBUG, msg5);
        for(i = 0 ; i < 5 * 1024; i++) MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_DEBUG, msg6);
        for(i = 0 ; i < 5 * 1024; i++) MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_DEBUG, msg7);
        for(i = 0 ; i < 5 * 1024; i++) MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_DEBUG, msg8);
        for(i = 0 ; i < 5 * 1024; i++) MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_DEBUG, msg9);
        for(i = 0 ; i < 5 * 1024; i++) MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_DEBUG, msg10);
        for(i = 0 ; i < 5 * 1024; i++) MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_DEBUG, msg11);
        for(i = 0 ; i < 5 * 1024; i++) MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_DEBUG, msg12);
        for(i = 0 ; i < 5 * 1024; i++) MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_DEBUG, msg13);

        TEST_ASSERT_MESSAGE(0 == stat("my_10MB_file_logger.log", &st), "Rollback of file must have failed ");
        TEST_ASSERT_MESSAGE(0 == stat("my_10MB_file_logger_backup_1.log", &st), "Rollback of file must have failed ");
        TEST_ASSERT_MESSAGE(0 == stat("my_10MB_file_logger_backup_2.log", &st), "Rollback of file must have failed ");
        TEST_ASSERT_MESSAGE(0 == stat("my_10MB_file_logger_backup_3.log", &st), "Rollback of file must have failed ");
        TEST_ASSERT_MESSAGE(0 == stat("my_10MB_file_logger_backup_4.log", &st), "Rollback of file must have failed ");
        TEST_ASSERT_MESSAGE(0 == stat("my_10MB_file_logger_backup_5.log", &st), "Rollback of file must have failed ");
        TEST_ASSERT_MESSAGE(-1 == stat("my_10MB_file_logger_backup_6.log", &st), "Rollback of file must have failed ");

        ma_file_logger_release(file_logger);
    }

    /**************************************************************************
    TEST 3 -
    maximum rollovers set as 5. but rolling is disabled.
    log size = 2 MB

    Log should not be rolled. Maximum 1 log file should be created.
    ***************************************************************************/
    {
        ma_file_logger_policy_t logger_policy = { MA_TRUE, (char*)DEFAULT_FILE_LOG_FORMAT_PATTERN, 2, MA_FALSE, { "previous", 5 } };

        TEST_ASSERT_MESSAGE(ma_file_logger_create(NULL, "my_2MB_file_logger", ".log", &file_logger) == MA_OK, "File Logger could not be created");
        TEST_ASSERT(MA_OK == ma_file_logger_set_policy(file_logger, &logger_policy));

        // Writing more than 70 MB data
        for(i = 0 ; i < 5 * 1024; i++) MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_DEBUG, msg1);
        for(i = 0 ; i < 5 * 1024; i++) MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_DEBUG, msg2);
        for(i = 0 ; i < 5 * 1024; i++) MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_DEBUG, msg3);
        for(i = 0 ; i < 5 * 1024; i++) MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_DEBUG, msg4);
        for(i = 0 ; i < 5 * 1024; i++) MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_DEBUG, msg5);
        for(i = 0 ; i < 5 * 1024; i++) MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_DEBUG, msg6);
        for(i = 0 ; i < 5 * 1024; i++) MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_DEBUG, msg7);
        for(i = 0 ; i < 5 * 1024; i++) MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_DEBUG, msg8);
        for(i = 0 ; i < 5 * 1024; i++) MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_DEBUG, msg9);
        for(i = 0 ; i < 5 * 1024; i++) MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_DEBUG, msg10);
        for(i = 0 ; i < 5 * 1024; i++) MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_DEBUG, msg11);
        for(i = 0 ; i < 5 * 1024; i++) MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_DEBUG, msg12);
        for(i = 0 ; i < 5 * 1024; i++) MA_LOG((ma_logger_t *)file_logger, MA_LOG_SEV_DEBUG, msg13);

        TEST_ASSERT_MESSAGE(0 == stat("my_2MB_file_logger.log", &st), "Rollback of file must have failed ");
        TEST_ASSERT_MESSAGE(-1 == stat("my_2MB_file_logger_previous_1.log", &st), "Rollback of file must have failed ");

        ma_file_logger_release(file_logger);

    }

    // Removing files created in this UT
    unlink("my_5MB_file_logger.log");
    unlink("my_10MB_file_logger.log");
    unlink("my_10MB_file_logger_backup_1.log");
    unlink("my_10MB_file_logger_backup_2.log");
    unlink("my_10MB_file_logger_backup_3.log");
    unlink("my_10MB_file_logger_backup_4.log");
    unlink("my_10MB_file_logger_backup_5.log");
    unlink("my_2MB_file_logger.log");
}



