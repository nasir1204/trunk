
/*****************************************************************************
Logger unit tests
******************************************************************************/


#include "unity.h"
#include "unity_fixture.h"
#include <string.h>

#include "ma/logger/ma_file_logger.h"
#include "ma/logger/ma_console_logger.h"
#include "ma/internal/utils/logger/ma_logger_internal.h"
#include "ma/logger/ma_mailslot_logger.h"
#include <time.h>
#include <Windows.h>

#ifndef MA_WINDOWS
#include <unistd.h>
#endif


#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif

#define LOG_FACILITY_NAME "Logger_Test_Facility"
#define aviv_file "\\\\.\\mailslot\\AVITrace"
#define TEST_FILE_LOG_FORMAT_PATTERN             \
                "%d %+t %p %f.%s: %m"

TEST_GROUP_RUNNER(ma_logger_mailslot_tests_group) {
	do {RUN_TEST_CASE(ma_logger_mailslot_tests, verify_basic_logging)} while (0);
	do {RUN_TEST_CASE(ma_logger_mailslot_tests, verify_basic_logging1)} while (0);
	do {RUN_TEST_CASE(ma_logger_mailslot_tests, verify_basic_logging_policy)} while (0);
	do {RUN_TEST_CASE(ma_logger_mailslot_tests, verify_basic_logging_all)} while (0);
	do {RUN_TEST_CASE(ma_logger_mailslot_tests, verify_basic_logging_withconsole)} while (0);
	do {RUN_TEST_CASE(ma_logger_mailslot_tests, verify_basic_logging_getpolicy)} while (0);
	do {RUN_TEST_CASE(ma_logger_mailslot_tests, verify_basic_logging_filter)} while (0);
	//do {RUN_TEST_CASE(ma_logger_mailslot_tests, verify_basic_logging_loop)} while (0);
}

TEST_SETUP(ma_logger_mailslot_tests) {
   

}

TEST_TEAR_DOWN(ma_logger_mailslot_tests) {
    
}

/*
Verify the basic functionality by logging the same messages on file and console.
Covers - Long messages, wide characters, special characters logging on console
and in file.
*/


TEST(ma_logger_mailslot_tests, verify_basic_logging)
{
    ma_mail_slot_logger_t *mail_slot = NULL;
    
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_mail_slot_logger_create(NULL, &mail_slot));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_mail_slot_logger_create(aviv_file, NULL));

    TEST_ASSERT_MESSAGE(ma_mail_slot_logger_create(aviv_file, &mail_slot) == MA_OK, \
                        "mail slot Logger could not be created");
   	
	MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_CRITICAL, "Critical Error logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_ERROR, "Error should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_WARNING, "Warning should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_INFO, "Information message should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_NOTICE, "Notice should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_DEBUG, "Debug message should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_TRACE, "Trace message should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_PERFORMANCE, "Performance message should not be logged by mail slot Logger 1");


	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_mail_slot_logger_release(NULL));
    TEST_ASSERT_MESSAGE(ma_mail_slot_logger_release(mail_slot) == MA_OK, \
                        "mail slot Logger could not be released");
}

TEST(ma_logger_mailslot_tests, verify_basic_logging1)
{
    ma_mail_slot_logger_t *mail_slot = NULL;
     
    TEST_ASSERT_MESSAGE(ma_mail_slot_logger_create(aviv_file, &mail_slot) == MA_OK, \
                        "mail slot Logger could not be created");
   	
	MA_WLOG((ma_logger_t *)mail_slot, MA_LOG_SEV_CRITICAL, L"Critical Error logged by mail slot Logger 1");
    MA_WLOG((ma_logger_t *)mail_slot, MA_LOG_SEV_ERROR, L"Error should not be logged by mail slot Logger 1");
    MA_WLOG((ma_logger_t *)mail_slot, MA_LOG_SEV_WARNING, L"Warning should not be logged by mail slot Logger 1");
    MA_WLOG((ma_logger_t *)mail_slot, MA_LOG_SEV_INFO, L"Information message should not be logged by mail slot Logger 1");
    MA_WLOG((ma_logger_t *)mail_slot, MA_LOG_SEV_NOTICE, L"Notice should not be logged by mail slot Logger 1");
    MA_WLOG((ma_logger_t *)mail_slot, MA_LOG_SEV_DEBUG, L"Debug message should not be logged by mail slot Logger 1");
    MA_WLOG((ma_logger_t *)mail_slot, MA_LOG_SEV_TRACE, L"Trace message should not be logged by mail slot Logger 1");
    MA_WLOG((ma_logger_t *)mail_slot, MA_LOG_SEV_PERFORMANCE, L"Performance message should not be logged by mail slot Logger 1");


    TEST_ASSERT_MESSAGE(ma_mail_slot_logger_release(mail_slot) == MA_OK, \
                        "mail slot Logger could not be released");

   
}

TEST(ma_logger_mailslot_tests, verify_basic_logging_policy)
{
    ma_mail_slot_logger_t *mail_slot = NULL;
    ma_mail_slot_logger_policy_t policy_mail;
	policy_mail.format_pattern = TEST_FILE_LOG_FORMAT_PATTERN;
    TEST_ASSERT_MESSAGE(ma_mail_slot_logger_create(aviv_file, &mail_slot) == MA_OK, \
                        "mail slot Logger could not be created");
   	
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_mail_slot_logger_set_policy(NULL, &policy_mail));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_mail_slot_logger_set_policy(mail_slot, NULL));
	TEST_ASSERT_MESSAGE(ma_mail_slot_logger_set_policy(mail_slot,&policy_mail)== MA_OK, \
                        "mail slot Logger could not set policy");

	MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_CRITICAL, "Critical Error logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_ERROR, "Error should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_WARNING, "Warning should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_INFO, "Information message should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_NOTICE, "Notice should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_DEBUG, "Debug message should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_TRACE, "Trace message should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_PERFORMANCE, "Performance message should not be logged by mail slot Logger 1");

	policy_mail.format_pattern =  "%+t %p %f.%s: %m";
	TEST_ASSERT_MESSAGE(ma_mail_slot_logger_set_policy(mail_slot,&policy_mail)== MA_OK, \
                        "mail slot Logger could not set policy");

	MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_CRITICAL, "Critical Error logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_ERROR, "Error should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_WARNING, "Warning should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_INFO, "Information message should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_NOTICE, "Notice should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_DEBUG, "Debug message should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_TRACE, "Trace message should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_PERFORMANCE, "Performance message should not be logged by mail slot Logger 1");

	policy_mail.format_pattern =  "%d %p %f.%s: %m";
	TEST_ASSERT_MESSAGE(ma_mail_slot_logger_set_policy(mail_slot,&policy_mail)== MA_OK, \
                        "mail slot Logger could not set policy");
	MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_CRITICAL, "Critical Error logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_ERROR, "Error should not be logged by mail slot Logger 1");

	policy_mail.format_pattern =  "%f.%s: %m";
	TEST_ASSERT_MESSAGE(ma_mail_slot_logger_set_policy(mail_slot,&policy_mail)== MA_OK, \
                        "mail slot Logger could not set policy");
	MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_CRITICAL, "Critical Error logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_ERROR, "Error should not be logged by mail slot Logger 1");

	policy_mail.format_pattern =  "%s: %m";
	TEST_ASSERT_MESSAGE(ma_mail_slot_logger_set_policy(mail_slot,&policy_mail)== MA_OK, \
                        "mail slot Logger could not set policy");
	MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_TRACE, "Trace message should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_PERFORMANCE, "Performance message should not be logged by mail slot Logger 1");

	policy_mail.format_pattern =  " %m";
	TEST_ASSERT_MESSAGE(ma_mail_slot_logger_set_policy(mail_slot,&policy_mail)== MA_OK, \
                        "mail slot Logger could not set policy");
	MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_TRACE, "Trace message should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_PERFORMANCE, "Performance message should not be logged by mail slot Logger 1");

    TEST_ASSERT_MESSAGE(ma_mail_slot_logger_release(mail_slot) == MA_OK, \
                        "mail slot Logger could not be released");
}

TEST(ma_logger_mailslot_tests, verify_basic_logging_all)
{
    ma_mail_slot_logger_t *mail_slot = NULL;
    ma_mail_slot_logger_policy_t policy_mail;
	policy_mail.format_pattern = "%d %+t %p(%d) %f.%s: %m";

    TEST_ASSERT_MESSAGE(ma_mail_slot_logger_create(aviv_file, &mail_slot) == MA_OK, \
                        "mail slot Logger could not be created");
   	
	TEST_ASSERT_MESSAGE(ma_mail_slot_logger_set_policy(mail_slot,&policy_mail)== MA_OK, \
                        "mail slot Logger could not set policy");

	 /* Long log message */
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_CRITICAL,"This is a very very long message. "
                                              "The quick brown fox jumps over the lazy dog. "
                                              "We all go a little mad sometimes."
                                              "You can't handle the truth!"
                                              "You had me at 'hello'"
                                              "Show me the money"
                                              "And in the morning, I'm making waffles!"
                                              "Game over, man! Game over!"
                                              "I love the smell of napalm in the morning...Smells like victory."
                                              "I'll be back."
                                              "My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
											  "We all go a little mad sometimes.");
    
    /* Wide character */
    MA_WLOG((ma_logger_t *)mail_slot, MA_LOG_SEV_CRITICAL, L"WWWWWWW--This is a very very long message.#@#$%^&*("
                                              L"The quick brown fox jumps over the lazy dog. "
                                              L"We all go a little mad sometimes."
                                              L"You can't handle the truth!");
 
	MA_WLOG((ma_logger_t *)mail_slot, MA_LOG_SEV_CRITICAL,L"This is a very very long message. "
                                              L"The quick brown fox jumps over the lazy dog. "
                                              L"We all go a little mad sometimes."
                                              L"You can't handle the truth!"
                                              L"You had me at 'hello'"
                                              L"Show me the money"
                                              L"And in the morning, I'm making waffles!"
                                              L"Game over, man! Game over!"
                                              L"I love the smell of napalm in the morning...Smells like victory."
                                              L"I'll be back."
                                              L"My Mama always said, 'Life was like a box of chocolates; you never know what you're gonna get.'"
											  L"We all go a little mad sometimes.");
    /* Special characters */
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_CRITICAL, "@#@#@#$%$^%$^&%%*&&^*&*(*()(*)(*)\"(&:{}[]<>?");
   
	 MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_CRITICAL, "Print special charecters %%%%%%%%%%%% \\\\");

    TEST_ASSERT_MESSAGE(ma_mail_slot_logger_release(mail_slot) == MA_OK, \
                        "mail slot Logger could not be released");
}


TEST(ma_logger_mailslot_tests, verify_basic_logging_withconsole)
{
    ma_mail_slot_logger_t *mail_slot = NULL;
	ma_console_logger_t *console_logger = NULL;
    ma_mail_slot_logger_policy_t policy_mail;
	policy_mail.format_pattern = TEST_FILE_LOG_FORMAT_PATTERN;

    TEST_ASSERT_MESSAGE(ma_mail_slot_logger_create(aviv_file, &mail_slot) == MA_OK, \
                        "mail slot Logger could not be created");
   	
	 /* Valid Console Logger creation */
    TEST_ASSERT_MESSAGE(ma_console_logger_create(&console_logger) == MA_OK, \
                        "Console logger could not be created");

	MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_CRITICAL, "Critical Error logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_ERROR, "Error should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_WARNING, "Warning should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_INFO, "Information message should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_NOTICE, "Notice should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_DEBUG, "Debug message should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_TRACE, "Trace message should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_PERFORMANCE, "Performance message should not be logged by mail slot Logger 1");
	MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_CRITICAL, "@#@#@#$%$^%$^&%%*&&^*&*(*()(*)(*)\"(&:{}[]<>?");

	MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "Critical Error logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_ERROR, "Error should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_WARNING, "Warning should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_INFO, "Information message should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_NOTICE, "Notice should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_DEBUG, "Debug message should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_TRACE, "Trace message should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_PERFORMANCE, "Performance message should not be logged by mail slot Logger 1");
	MA_LOG((ma_logger_t *)console_logger, MA_LOG_SEV_CRITICAL, "@#@#@#$%$^%$^&%%*&&^*&*(*()(*)(*)\"(&:{}[]<>?");

    TEST_ASSERT_MESSAGE(ma_mail_slot_logger_release(mail_slot) == MA_OK, \
                        "mail slot Logger could not be released");
	 ma_logger_dec_ref((ma_logger_t *)console_logger);
}

TEST(ma_logger_mailslot_tests, verify_basic_logging_getpolicy)
{
    ma_mail_slot_logger_t *mail_slot = NULL;
	ma_mail_slot_logger_policy_t policy_mail;
	ma_mail_slot_logger_policy_t *test_policy;
	policy_mail.format_pattern = TEST_FILE_LOG_FORMAT_PATTERN;
	

    TEST_ASSERT_MESSAGE(ma_mail_slot_logger_create(aviv_file, &mail_slot) == MA_OK, \
                        "mail slot Logger could not be created");
   	
	
	TEST_ASSERT_MESSAGE(ma_mail_slot_logger_set_policy(mail_slot,&policy_mail)== MA_OK, \
                        "mail slot Logger could not set policy");
	
	MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_CRITICAL, "Critical Error logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_ERROR, "Error should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_WARNING, "Warning should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_INFO, "Information message should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_NOTICE, "Notice should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_DEBUG, "Debug message should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_TRACE, "Trace message should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_PERFORMANCE, "Performance message should not be logged by mail slot Logger 1");
	MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_CRITICAL, "@#@#@#$%$^%$^&%%*&&^*&*(*()(*)(*)\"(&:{}[]<>?");

	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_mail_slot_logger_get_policy(NULL, &test_policy));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_mail_slot_logger_get_policy(mail_slot, NULL));
	TEST_ASSERT_MESSAGE(ma_mail_slot_logger_get_policy(mail_slot,&test_policy)== MA_OK, \
                        "mail slot Logger could not set policy");

	TEST_ASSERT_MESSAGE(strcmp(test_policy->format_pattern,TEST_FILE_LOG_FORMAT_PATTERN)== MA_OK,"Set and get policy's are diffrent");
	TEST_ASSERT_MESSAGE(ma_mail_slot_logger_release(mail_slot) == MA_OK, \
                        "mail slot Logger could not be released");
	
}

TEST(ma_logger_mailslot_tests, verify_basic_logging_filter)
{
    ma_mail_slot_logger_t *mail_slot = NULL;
	ma_log_filter_t *filter = NULL;
     
    TEST_ASSERT_MESSAGE(ma_mail_slot_logger_create(aviv_file, &mail_slot) == MA_OK, \
                        "mail slot Logger could not be created");
   	 /* Filter creation */
    TEST_ASSERT_MESSAGE(ma_generic_log_filter_create("Logger_Test_Facility.Critical" , &filter) == MA_OK,
                        "Filter could not be created");
	TEST_ASSERT_MESSAGE(ma_logger_add_filter((ma_logger_t *)mail_slot, filter) == MA_OK, \
                        "Filter could not be added for File Logger");

	MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_CRITICAL, "Critical Error logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_ERROR, "Error should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_WARNING, "Warning should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_INFO, "Information message should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_NOTICE, "Notice should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_DEBUG, "Debug message should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_TRACE, "Trace message should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_PERFORMANCE, "Performance message should not be logged by mail slot Logger 1");
	ma_log_filter_release(filter);
	
	TEST_ASSERT_MESSAGE(ma_generic_log_filter_create("Logger_Test_Facility.Error" , &filter) == MA_OK,
                        "Filter could not be created");
	TEST_ASSERT_MESSAGE(ma_logger_add_filter((ma_logger_t *)mail_slot, filter) == MA_OK, \
                        "Filter could not be added for File Logger");
	MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_CRITICAL, "Critical Error logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_ERROR, "Error should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_WARNING, "Warning should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_INFO, "Information message should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_NOTICE, "Notice should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_DEBUG, "Debug message should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_TRACE, "Trace message should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_PERFORMANCE, "Performance message should not be logged by mail slot Logger 1");
	/* Destroying filter */
    ma_log_filter_release(filter);

	TEST_ASSERT_MESSAGE(ma_generic_log_filter_create("Logger_Test_Facility.Warning" , &filter) == MA_OK,
                        "Filter could not be created");
	TEST_ASSERT_MESSAGE(ma_logger_add_filter((ma_logger_t *)mail_slot, filter) == MA_OK, \
                        "Filter could not be added for File Logger");
	MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_CRITICAL, "Critical Error logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_ERROR, "Error should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_WARNING, "Warning should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_INFO, "Information message should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_NOTICE, "Notice should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_DEBUG, "Debug message should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_TRACE, "Trace message should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_PERFORMANCE, "Performance message should not be logged by mail slot Logger 1");
	/* Destroying filter */
    ma_log_filter_release(filter);

	
	TEST_ASSERT_MESSAGE(ma_generic_log_filter_create("Logger_Test_Facility.Notice" , &filter) == MA_OK,
                        "Filter could not be created");
	TEST_ASSERT_MESSAGE(ma_logger_add_filter((ma_logger_t *)mail_slot, filter) == MA_OK, \
                        "Filter could not be added for File Logger");
	MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_CRITICAL, "Critical Error logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_ERROR, "Error should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_WARNING, "Warning should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_INFO, "Information message should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_NOTICE, "Notice should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_DEBUG, "Debug message should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_TRACE, "Trace message should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_PERFORMANCE, "Performance message should not be logged by mail slot Logger 1");
	/* Destroying filter */
    ma_log_filter_release(filter);

	
	TEST_ASSERT_MESSAGE(ma_generic_log_filter_create("Logger_Test_Facility.Info" , &filter) == MA_OK,
                        "Filter could not be created");
	TEST_ASSERT_MESSAGE(ma_logger_add_filter((ma_logger_t *)mail_slot, filter) == MA_OK, \
                        "Filter could not be added for File Logger");
	MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_CRITICAL, "Critical Error logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_ERROR, "Error should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_WARNING, "Warning should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_INFO, "Information message should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_NOTICE, "Notice should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_DEBUG, "Debug message should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_TRACE, "Trace message should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_PERFORMANCE, "Performance message should not be logged by mail slot Logger 1");
	/* Destroying filter */
    ma_log_filter_release(filter);


	TEST_ASSERT_MESSAGE(ma_generic_log_filter_create("Logger_Test_Facility.Debug" , &filter) == MA_OK,
                        "Filter could not be created");
	TEST_ASSERT_MESSAGE(ma_logger_add_filter((ma_logger_t *)mail_slot, filter) == MA_OK, \
                        "Filter could not be added for File Logger");
	MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_CRITICAL, "Critical Error logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_ERROR, "Error should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_WARNING, "Warning should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_INFO, "Information message should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_NOTICE, "Notice should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_DEBUG, "Debug message should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_TRACE, "Trace message should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_PERFORMANCE, "Performance message should not be logged by mail slot Logger 1");
	/* Destroying filter */
    ma_log_filter_release(filter);

	
	TEST_ASSERT_MESSAGE(ma_generic_log_filter_create("Logger_Test_Facility.Trace" , &filter) == MA_OK,
                        "Filter could not be created");
	TEST_ASSERT_MESSAGE(ma_logger_add_filter((ma_logger_t *)mail_slot, filter) == MA_OK, \
                        "Filter could not be added for File Logger");
	MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_CRITICAL, "Critical Error logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_ERROR, "Error should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_WARNING, "Warning should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_INFO, "Information message should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_NOTICE, "Notice should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_DEBUG, "Debug message should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_TRACE, "Trace message should not be logged by mail slot Logger 1");
    MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_PERFORMANCE, "Performance message should not be logged by mail slot Logger 1");
	/* Destroying filter */
    ma_log_filter_release(filter);

    TEST_ASSERT_MESSAGE(ma_mail_slot_logger_release(mail_slot) == MA_OK, \
                        "mail slot Logger could not be released");
}



/* This test is to check continues writing on AVIVIEW, this will run for 1 to 2 minutes in this
period please open and close AVIVIEW frequently, it should start writing when we open the AVIVIEW.*/

TEST(ma_logger_mailslot_tests, verify_basic_logging_loop)
{
    ma_mail_slot_logger_t *mail_slot = NULL;
    SYSTEMTIME systime; 
	ma_uint16_t	 min=0;
    

    TEST_ASSERT_MESSAGE(ma_mail_slot_logger_create(aviv_file, &mail_slot) == MA_OK, \
                        "mail slot Logger could not be created");
	GetLocalTime(&systime);
	min = systime.wMinute;
	printf("Start time =%d:%d\n",systime.wHour,systime.wMinute);
   	do{
		MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_CRITICAL, "Critical Error logged by mail slot Logger 1");
		MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_ERROR, "Error should not be logged by mail slot Logger 1");
		MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_WARNING, "Warning should not be logged by mail slot Logger 1");
		MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_INFO, "Information message should not be logged by mail slot Logger 1");
		MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_NOTICE, "Notice should not be logged by mail slot Logger 1");
		MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_DEBUG, "Debug message should not be logged by mail slot Logger 1");
		MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_TRACE, "Trace message should not be logged by mail slot Logger 1");
		MA_LOG((ma_logger_t *)mail_slot, MA_LOG_SEV_PERFORMANCE, "Performance message should not be logged by mail slot Logger 1");
		GetLocalTime(&systime);
		Sleep(100);
	}while(systime.wMinute <= min+1 && systime.wMinute>0);
	printf("End time =%d:%d\n",systime.wHour,systime.wMinute);
    TEST_ASSERT_MESSAGE(ma_mail_slot_logger_release(mail_slot) == MA_OK, \
                        "mail slot Logger could not be released");
}