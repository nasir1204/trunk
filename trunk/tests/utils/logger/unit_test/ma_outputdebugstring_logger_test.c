#include "unity.h"
#include "unity_fixture.h"

#include "ma/logger/ma_outputdebugstring_logger.h"
//#include "ma/ma_log.h"

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif
#define LOG_FACILITY_NAME "Logger_Test_Facility"

TEST_GROUP_RUNNER(ma_outputdebugstring_logger_tests_group) 
{
	do {RUN_TEST_CASE(ma_outputdebugstring_logger_test, verify_basic_logging)} while (0);
}

TEST_SETUP(ma_outputdebugstring_logger_test) 
{   

}

TEST_TEAR_DOWN(ma_outputdebugstring_logger_test) {
    
}

TEST(ma_outputdebugstring_logger_test, verify_basic_logging)
{
	ma_logger_t *logger = NULL;

	//FAIL-> TEST_ASSERT(MA_ERROR_INVALIDARG == ma_outputdebugstring_logger_create(NULL));
	TEST_ASSERT(MA_OK == ma_outputdebugstring_logger_create(&logger));
	MA_LOG(logger, MA_LOG_SEV_WARNING, "Test message");
	ma_logger_dec_ref(logger);
}