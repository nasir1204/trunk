/*****************************************************************************
Main Test runner for all Logger unit tests.
******************************************************************************/

#include "unity.h"
#include "unity_fixture.h"
#include "ma/ma_common.h"
#include "ma_application_internal.h"
#include "ma/internal/utils/app_info/ma_application.h"
#include "ma/internal/ma_macros.h"

#ifdef MA_WINDOWS
#include <windows.h>
#endif
#ifdef _MSC_VER
# include <crtdbg.h> /* */
# define MA_TRACKLEAKS _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF)
#else
# define MA_TRACKLEAKS ((void *)0)
#endif

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif

#define LOG_FACILITY_NAME "Logger_Test_Facility"


#ifdef _WIN64
#define MA_AGENT_BIT_AFFINITY 2
#elif _WIN32
#define MA_AGENT_BIT_AFFINITY 1
#else
#endif

static void run_all_tests(void) {
    do {RUN_TEST_GROUP(ma_log_tests_group); } while (0);
    do {RUN_TEST_GROUP(ma_logger_collector_tests_group); } while (0);
    do {RUN_TEST_GROUP(ma_log_utils_tests_group);} while (0); /* do loop to work around unity declaration flaws that are not ANSI C compatible */
    do {RUN_TEST_GROUP(ma_log_rotation_test_group);} while (0); /* do loop to work around unity declaration flaws that are not ANSI C compatible */
    do {RUN_TEST_GROUP(ma_log_msg_test_group);} while (0);
#ifdef MA_THREADING_ENABLED
    do {RUN_TEST_GROUP(ma_log_threading_tests_group);} while (0); /* do loop to work around unity declaration flaws that are not ANSI C compatible */
#endif
/* These test will be added for mail slot logger and they execute on windows after runing AVIView.exe*/
#ifdef MA_WINDOWS
    do {RUN_TEST_GROUP(ma_logger_mailslot_tests_group); } while (0);
#endif
    do {RUN_TEST_GROUP(logger_db_tests_group); } while (0);
	do {RUN_TEST_GROUP(ma_outputdebugstring_logger_tests_group); } while (0);
}


int main(int argc, char *argv[]) {
	ma_int32_t ret = 0;
    MA_TRACKLEAKS;	
	ret = UnityMain(argc, argv, run_all_tests);
	return ret;
}


