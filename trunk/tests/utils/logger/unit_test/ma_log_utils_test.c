/*****************************************************************************
Logger unit tests covering the logger utilities.
******************************************************************************/



#include "unity.h"
#include "unity_fixture.h"

#include <stdlib.h>
#include <string.h>

#include "ma/internal/utils/logger/ma_logger_internal.h"
#include "ma/logger/ma_log_filter.h"

#ifdef LOG_FACILITY_NAME
#undef LOG_FACILITY_NAME
#endif

#define LOG_FACILITY_NAME "Logger_Test_Facility"

TEST_GROUP_RUNNER(ma_log_utils_tests_group) {
    do {RUN_TEST_CASE(ma_log_utils_tests, verify_log_filter)} while (0);
    do {RUN_TEST_CASE(ma_log_utils_tests, verify_log_formatting)} while (0);
}

TEST_SETUP(ma_log_utils_tests) {
    Unity.TestFile = "ma_log_utils_test.c";
}

TEST_TEAR_DOWN(ma_log_utils_tests) {

}

TEST(ma_log_utils_tests, verify_log_filter) {
    ma_log_filter_t *filter = NULL ;
    char *pattern = NULL ;
    ma_log_msg_t msg;
    memset(&msg,0,sizeof(msg));
    msg.time.hour = 12;
    msg.time.minute = 34;
    msg.time.second = 56;

    /*Negative test case */
    TEST_ASSERT_MESSAGE(MA_OK != ma_generic_log_filter_create(pattern , NULL) , "ma_generic_log_filter_create should not return MA_OK in negative case");

    /* No filter */
    TEST_ASSERT_MESSAGE(MA_OK == ma_generic_log_filter_create(NULL , &filter) , "ma_generic_log_filter_create should return MA_OK even when filter is NULL");
    TEST_ASSERT_MESSAGE(MA_TRUE == ma_log_filter_accept(filter , &msg) , "ma_log_filter_accept should not reject any message with NULL being a filter ");
    ma_log_filter_release(filter);

    /* Single rule - Any*/
    TEST_ASSERT_MESSAGE(MA_OK == ma_generic_log_filter_create("*.*" , &filter) , "ma_generic_log_filter_create should return MA_OK");
    msg.facility = "msgbus"  , msg.severity = MA_LOG_SEV_INFO ;
    TEST_ASSERT_MESSAGE(MA_TRUE == ma_log_filter_accept(filter , &msg) , "ma_log_filter_accept should accept \"msgbus.Critical\" message with \"*.*\" being a filter ");
    msg.facility = "datastore"  , msg.severity = MA_LOG_SEV_DEBUG ;
    TEST_ASSERT_MESSAGE(MA_TRUE == ma_log_filter_accept(filter , &msg) , "ma_log_filter_accept should reject \"datastore.Info\" message with \"*.*\" being a filter ");
    ma_log_filter_release(filter);

    /* Single rule - Facility*/
    TEST_ASSERT_MESSAGE(MA_OK == ma_generic_log_filter_create("msgbus.*" , &filter) , "ma_generic_log_filter_create should return MA_OK");
    msg.facility = "msgbus"  , msg.severity = MA_LOG_SEV_CRITICAL ;
    TEST_ASSERT_MESSAGE(MA_TRUE == ma_log_filter_accept(filter , &msg) , "ma_log_filter_accept should accept \"msgbus.Critical\" message with \"msgbus.*\" being a filter ");
    msg.facility = "datastore"  , msg.severity = MA_LOG_SEV_CRITICAL ;
    TEST_ASSERT_MESSAGE(MA_FALSE == ma_log_filter_accept(filter , &msg) , "ma_log_filter_accept should reject \"datastore.Critical\" message with \"msgbus.*\" being a filter ");
    ma_log_filter_release(filter);

    /* Single rule - Severity  */
    TEST_ASSERT_MESSAGE(MA_OK == ma_generic_log_filter_create("*.Info" , &filter) , "ma_generic_log_filter_create should return MA_OK");
    msg.facility = "msgbus"  , msg.severity = MA_LOG_SEV_INFO ;
    TEST_ASSERT_MESSAGE(MA_TRUE == ma_log_filter_accept(filter , &msg) , "ma_log_filter_accept should accept \"msgbus.Info\" message with \"*.Info\" being a filter ");
    msg.facility = "msgbus"  , msg.severity = MA_LOG_SEV_DEBUG ;
    TEST_ASSERT_MESSAGE(MA_FALSE == ma_log_filter_accept(filter , &msg) , "ma_log_filter_accept should reject \"msgbus.Debug\" message with \"*.Info\" being a filter ");
    msg.facility = "msgbus"  , msg.severity = MA_LOG_SEV_CRITICAL ;
    TEST_ASSERT_MESSAGE(MA_TRUE == ma_log_filter_accept(filter , &msg) , "ma_log_filter_accept should accept \"msgbus.Critical\" message with \"*.Info\" being a filter ");
    ma_log_filter_release(filter);

     /* Single rule - Facility.Severity  */
    TEST_ASSERT_MESSAGE(MA_OK == ma_generic_log_filter_create("msgbus.Critical" , &filter) , "ma_generic_log_filter_create should return MA_OK");
    msg.facility = "msgbus"  , msg.severity = MA_LOG_SEV_CRITICAL ;
    TEST_ASSERT_MESSAGE(MA_TRUE == ma_log_filter_accept(filter , &msg) , "ma_log_filter_accept should accept \"msgbus.Critical\" message with \"msgbus.Critical\" being a filter ");
    msg.facility = "datastore"  , msg.severity = MA_LOG_SEV_CRITICAL ;
    TEST_ASSERT_MESSAGE(MA_FALSE == ma_log_filter_accept(filter , &msg) , "ma_log_filter_accept should reject \"datastore.Critical\" message with \"msgbus.Critical\" being a filter ");
    msg.facility = "msgbus"  , msg.severity = MA_LOG_SEV_DEBUG ;
    TEST_ASSERT_MESSAGE(MA_FALSE == ma_log_filter_accept(filter , &msg) , "ma_log_filter_accept should reject \"msgbus.Debug\" message with \"msgbus.Critical\" being a filter ");
    ma_log_filter_release(filter);


    /* Single rule - ~Facility.Severity  */
    TEST_ASSERT_MESSAGE(MA_OK == ma_generic_log_filter_create("~msgbus.Critical" , &filter) , "ma_generic_log_filter_create should return MA_OK");
    msg.facility = "msgbus"  , msg.severity = MA_LOG_SEV_CRITICAL ;
    TEST_ASSERT_MESSAGE(MA_FALSE == ma_log_filter_accept(filter , &msg) , "ma_log_filter_accept should reject \"msgbus.Critical\" message with \"~msgbus.Critical\" being a filter ");
    msg.facility = "datastore"  , msg.severity = MA_LOG_SEV_CRITICAL ;
    TEST_ASSERT_MESSAGE(MA_TRUE== ma_log_filter_accept(filter , &msg) , "ma_log_filter_accept should accept \"datastore.Critical\" message with \"~msgbus.Critical\" being a filter ");
    msg.facility = "datastore"  , msg.severity = MA_LOG_SEV_INFO ;
    TEST_ASSERT_MESSAGE(MA_TRUE == ma_log_filter_accept(filter , &msg) , "ma_log_filter_accept should accept \"datastore.INFO\" message with \"~msgbus.Critical\" being a filter ");
    ma_log_filter_release(filter);

    /* Single rule - Spaces in Facility */
    TEST_ASSERT_MESSAGE(MA_OK == ma_generic_log_filter_create("MA broker.Critical" , &filter) , "ma_generic_log_filter_create should return MA_OK");
    msg.facility = "MA broker"  , msg.severity = MA_LOG_SEV_CRITICAL ;
    TEST_ASSERT_MESSAGE(MA_TRUE == ma_log_filter_accept(filter , &msg) , "ma_log_filter_accept should accept \"MA broker.Critical\" message with \"MA broker.Critical\" being a filter ");
    msg.facility = "MAbroker"  , msg.severity = MA_LOG_SEV_CRITICAL ;
    TEST_ASSERT_MESSAGE(MA_FALSE == ma_log_filter_accept(filter , &msg) , "ma_log_filter_accept should reject \"MA broker.Critical\" message with \"MA broker.Critical\"  being a filter ");
    ma_log_filter_release(filter);

    /* Multiple rules - Facility*/
    TEST_ASSERT_MESSAGE(MA_OK == ma_generic_log_filter_create("msgbus.*|datastore.*" , &filter) , "ma_generic_log_filter_create should return MA_OK");
    msg.facility = "msgbus"  , msg.severity = MA_LOG_SEV_CRITICAL ;
    TEST_ASSERT_MESSAGE(MA_TRUE == ma_log_filter_accept(filter , &msg) , "ma_log_filter_accept should accept \"msgbus.Critical\" message with \"msgbus.*|datastore.*"" being a filter ");
    msg.facility = "datastore"  , msg.severity = MA_LOG_SEV_CRITICAL ;
    TEST_ASSERT_MESSAGE(MA_TRUE == ma_log_filter_accept(filter , &msg) , "ma_log_filter_accept should accept \"datastore.Critical\" message with \"msgbus.*|datastore.*"" being a filter ");
    msg.facility = "network"  , msg.severity = MA_LOG_SEV_CRITICAL ;
    TEST_ASSERT_MESSAGE(MA_FALSE == ma_log_filter_accept(filter , &msg) , "ma_log_filter_accept should reject \"network.Critical\" message with \"msgbus.*|datastore.*"" being a filter ");
    ma_log_filter_release(filter);

    /* Multiple rules - Severity  */
    TEST_ASSERT_MESSAGE(MA_OK == ma_generic_log_filter_create("*.Info|*.Critical" , &filter) , "ma_generic_log_filter_create should return MA_OK");
    msg.facility = "msgbus"  , msg.severity = MA_LOG_SEV_INFO ;
    TEST_ASSERT_MESSAGE(MA_TRUE == ma_log_filter_accept(filter , &msg) , "ma_log_filter_accept should accept \"msgbus.Info\" message with \"*.Info|*.Critical\" being a filter ");
    msg.facility = "msgbus"  , msg.severity = MA_LOG_SEV_CRITICAL ;
    TEST_ASSERT_MESSAGE(MA_TRUE == ma_log_filter_accept(filter , &msg) , "ma_log_filter_accept should accept \"msgbus.Critical\" message with \"*.Info|*.Critical\" being a filter ");
    msg.facility = "msgbus"  , msg.severity = MA_LOG_SEV_DEBUG ;
    TEST_ASSERT_MESSAGE(MA_FALSE == ma_log_filter_accept(filter , &msg) , "ma_log_filter_accept should reject \"msgbus.Debug\" message with \"*.Info|*.Critical\" being a filter ");
    ma_log_filter_release(filter);


     /* Multiple rules - Facility.Severity  */
    TEST_ASSERT_MESSAGE(MA_OK == ma_generic_log_filter_create("msgbus.Info|DataStore.Info|network.Debug" , &filter) , "ma_generic_log_filter_create should return MA_OK");
    msg.facility = "msgbus"  , msg.severity = MA_LOG_SEV_CRITICAL ;
    TEST_ASSERT_MESSAGE(MA_TRUE == ma_log_filter_accept(filter , &msg) , "ma_log_filter_accept should accept \"msgbus.Critical\" message with \"msgbus.Info|DataStore.Info|network.Debug\" being a filter ");
    msg.facility = "msgbus"  , msg.severity = MA_LOG_SEV_DEBUG ;
    TEST_ASSERT_MESSAGE(MA_FALSE == ma_log_filter_accept(filter , &msg) , "ma_log_filter_accept should reject \"msgbus.Debug\" message with \"msgbus.Info|DataStore.Info|network.Debug\" being a filter ");
    msg.facility = "datastore"  , msg.severity = MA_LOG_SEV_DEBUG ;
    TEST_ASSERT_MESSAGE(MA_FALSE == ma_log_filter_accept(filter , &msg) , "ma_log_filter_accept should reject \"datastore.debug\" message with \"msgbus.Info|DataStore.Info|network.Debug\" being a filter ");
    msg.facility = "network"  , msg.severity = MA_LOG_SEV_DEBUG ;
    TEST_ASSERT_MESSAGE(MA_TRUE == ma_log_filter_accept(filter , &msg) , "ma_log_filter_accept should accept \"network.Debug\" message with \"msgbus.Info|DataStore.Info|network.Debug\" being a filter ");
    ma_log_filter_release(filter);


    /* Multiple rules - ~Facility.Severity  */
    TEST_ASSERT_MESSAGE(MA_OK == ma_generic_log_filter_create("~msgbus.Critical|~datastore.Info" , &filter) , "ma_generic_log_filter_create should return MA_OK");
    msg.facility = "msgbus"  , msg.severity = MA_LOG_SEV_CRITICAL ;
    TEST_ASSERT_MESSAGE(MA_FALSE == ma_log_filter_accept(filter , &msg) , "ma_log_filter_accept should reject \"msgbus.Critical\" message with \"~msgbus.Critical|~datastore.Info\" being a filter ");
    msg.facility = "datastore"  , msg.severity = MA_LOG_SEV_CRITICAL ;
    TEST_ASSERT_MESSAGE(MA_FALSE == ma_log_filter_accept(filter , &msg) , "ma_log_filter_accept should reject \"datastore.Critical\" message with \"~msgbus.Critical|~datastore.Info\" being a filter ");
    msg.facility = "newtork"  , msg.severity = MA_LOG_SEV_INFO ;
    TEST_ASSERT_MESSAGE(MA_TRUE == ma_log_filter_accept(filter , &msg) , "ma_log_filter_accept should accept \"network.Info\" message with \"~msgbus.Critical|~datastore.Info\" being a filter ");
    ma_log_filter_release(filter);


    /* Multiple rules - Spaces in Facility */
    TEST_ASSERT_MESSAGE(MA_OK == ma_generic_log_filter_create("MA broker.Critical|Listen Server.Info" , &filter) , "ma_generic_log_filter_create should return MA_OK");
    msg.facility = "MA broker"  , msg.severity = MA_LOG_SEV_CRITICAL ;
    TEST_ASSERT_MESSAGE(MA_TRUE == ma_log_filter_accept(filter , &msg) , "ma_log_filter_accept should accept \"MA broker.Critical\" message with \"MA broker.Critical|Listen Server.Info\" being a filter ");
    msg.facility = "Listen Server"  , msg.severity = MA_LOG_SEV_INFO ;
    TEST_ASSERT_MESSAGE(MA_TRUE == ma_log_filter_accept(filter , &msg) , "ma_log_filter_accept should accept \"Listen Server.Info\" message with \"MA broker.Critical|Listen Server.Info\"  being a filter ");
    msg.facility = "ListenServer"  , msg.severity = MA_LOG_SEV_INFO ;
    TEST_ASSERT_MESSAGE(MA_FALSE == ma_log_filter_accept(filter , &msg) , "ma_log_filter_accept should reject \"MA Listen Server.Info\" message with \"MA broker.Critical|Listen Server.Info\"  being a filter ");
    ma_log_filter_release(filter);
}

TEST(ma_log_utils_tests, verify_log_formatting) {
    char buffer[200];
    ma_log_msg_t msg;

    int characters_copy = 0;

    /* Log message content */
    ma_log_msg_t msg1 =
    {
        MA_LOG_SEV_NOTICE,                         // Severity
        "MA Logger Test Facility",                 // Facility
        0,                                         // Not wide char
        (const void *)"This is McAfee Agent Software Testing",   // Message
        1234,                                      // PID
        5678,                                      // Thread ID
        { 2013, 1, 8, 23, 40, 56, 857 },           // Date and Time
    #ifdef MA_WINDOWS
        "Test Station 10",                         // Windows Station
        9999,                                      // Session ID
    #else
        9999,                                      // UID
    #endif
        "QA Machine",                              // Machine Name
        "Utility Test",                            // File Name
        "Formatting Function",                     // Function Name
        100 };                                     // Line Number

    char *expected_value_in_buffer = "2013-01-08 23:40:56.857 Notice 4d2 5678 9999 MA Logger Test Facility Utility Test Formatting Function 0100 This is McAfee Agent Software Testing";

    memset(&msg,0,sizeof(msg));
    msg.time.hour = 12;
    msg.time.minute = 34;
    msg.time.second = 56;

    /* Test for invalid format string */
    TEST_ASSERT_MESSAGE(1 == ma_log_utils_format_msg(NULL, 0,"a", &msg), "Bytes required to store the message is not calculated correctly");
    TEST_ASSERT_MESSAGE(2 == ma_log_utils_format_msg(NULL, 0,"aa", &msg), "Bytes required to store the message is not calculated correctly");
    TEST_ASSERT_MESSAGE(1 == ma_log_utils_format_msg(NULL, 0,"%%", &msg), "Bytes required to store the message is not calculated correctly");
    TEST_ASSERT_MESSAGE(2 == ma_log_utils_format_msg(NULL, 0,"%%a", &msg), "Bytes required to store the message is not calculated correctly");

    /* Test for calculation of length */
    TEST_ASSERT_MESSAGE(8 == ma_log_utils_format_msg(NULL, 0, "%t", &msg), "Bytes required to store the message is not calculated correctly");
    TEST_ASSERT_MESSAGE(12 == ma_log_utils_format_msg(NULL, 0, "%+t", &msg), "Bytes required to store the message is not calculated correctly");

    /* */
    TEST_ASSERT_MESSAGE(8 == ma_log_utils_format_msg(buffer,9,"%t",&msg), "ma_log_utils_format_msg did not return the expected number of copied chars");
    TEST_ASSERT_MESSAGE(ma_log_utils_format_msg(buffer,8,"%t",&msg) < 0, "ma_log_utils_format_msg should return negative value on slight buffer overflow") ;
    TEST_ASSERT_MESSAGE(ma_log_utils_format_msg(buffer,7,"%t",&msg) < 0, "ma_log_utils_format_msg should return negative value on buffer overflow");

    msg.facility = "More than 7 characters";
    msg.severity = MA_LOG_SEV_WARNING; /* Warning */
    TEST_ASSERT_MESSAGE(7 == ma_log_utils_format_msg(NULL, 0, "%s", &msg), "Bytes required to store the message is not calculated correctly");


    /* Test for formatted string with all contents filled */

    /* Actual value of characters to be copied */
	characters_copy = ma_log_utils_format_msg(buffer, 200, "%+d %+t %s %+P %T %S %f %F %U %l %m", &msg1);

    // FAIL-> (in LINUX only) TEST_ASSERT_MESSAGE(characters_copy == strlen(expected_value_in_buffer), "No. of characters to be copied is not correct");

    printf("%s\n", expected_value_in_buffer);
    printf("%s\n", buffer);

    // FAIL-> (in LINUX only) TEST_ASSERT_EQUAL_MEMORY_MESSAGE(expected_value_in_buffer, buffer, strlen(expected_value_in_buffer), "Log message is not formatted correctly");

	ma_log_utils_format_msg(buffer, 200, "%-F", &msg1);
	ma_log_utils_format_msg(buffer, 200, "%l", &msg1);
	ma_log_utils_format_msg(buffer, 200, "%-s", &msg1);
	ma_log_utils_format_msg(buffer, 200, "%-S", &msg1);
	ma_log_utils_format_msg(buffer, 200, "%b", &msg1);
}


