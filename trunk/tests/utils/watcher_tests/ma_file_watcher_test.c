/******************************************************************************
Unit tests for file watcher.
******************************************************************************/

#include "unity.h"
#include "unity_fixture.h"
#include "unity_internals.h"
#include "uv.h"
#include "ma/internal/utils/watcher/ma_file_watcher.h"
#include <fcntl.h>
#include <string.h>

static int dir_watcher_count = 0;
static int file_watcher_count = 0;

static ma_event_loop_t *loop = NULL;
static ma_watcher_t *watcher = NULL;
static uv_fs_t req;
static uv_file open_request_result = 0;

// Test number of the currently running test. Test number helps in exiting the test.
static int test_number = 0;

#ifdef MA_WINDOWS
    #define DIR_NAME "watcher_test_folder"
    #define FILE_NAME_1 "watcher_test_folder\\test_file_1.txt"
    #define FILE_NAME_2 "watcher_test_folder\\test_file_2.txt"
    #define FILE_NAME_3 "watcher_test_folder\\test_file_3.txt"
    #define FILE_NAME_4 "watcher_test_folder\\test_file_4.txt"
    #define FILE_NAME_5 "watcher_test_folder\\test_file_5.txt"
#else
    #define DIR_NAME "watcher_test_folder"
    #define FILE_NAME_1 "watcher_test_folder/test_file_1.txt"
    #define FILE_NAME_2 "watcher_test_folder/test_file_2.txt"
    #define FILE_NAME_3 "watcher_test_folder/test_file_3.txt"
    #define FILE_NAME_4 "watcher_test_folder/test_file_4.txt"
    #define FILE_NAME_5 "watcher_test_folder/test_file_5.txt"
#endif

TEST_GROUP_RUNNER(ma_file_watcher_test_group)
{
    do {RUN_TEST_CASE(file_watcher_tests, file_creation_in_directory_test)} while (0);
    do {RUN_TEST_CASE(file_watcher_tests, file_modification_in_directory_test)} while (0);
    do {RUN_TEST_CASE(file_watcher_tests, file_removal_in_directory_test)} while (0);
}

// Increment the directory watcher count indicating that the watched directory has
// been modified. Delete watcher when test is complete.
ma_error_t dir_watcher_cb(ma_watcher_t *watcher, const char *name, void *cb_data)
{
    dir_watcher_count++;

    // For test number 1
    if((test_number == 1) && (dir_watcher_count == 5))
    {
        TEST_ASSERT(MA_OK == ma_watcher_delete(watcher, name));
    }

    // For test number 2 & 3
    if(((test_number == 2) || (test_number == 3)) && (dir_watcher_count == 15))
    {
        TEST_ASSERT(MA_OK == ma_watcher_delete(watcher, name));
    }

    return MA_OK;
}

// Increment the file watcher count indicating that the watched file has been modified.
// Delete watcher when test is complete.
ma_error_t file_watcher_cb(ma_watcher_t *watcher, const char *name, void *cb_data)
{
    file_watcher_count++;

    // For test number 2 & 3
    if(((test_number == 2) || (test_number == 3)) && (file_watcher_count == 10))
    {
        TEST_ASSERT(MA_OK == ma_watcher_delete(watcher, FILE_NAME_1));
        TEST_ASSERT(MA_OK == ma_watcher_delete(watcher, FILE_NAME_2));
        TEST_ASSERT(MA_OK == ma_watcher_delete(watcher, FILE_NAME_3));
        TEST_ASSERT(MA_OK == ma_watcher_delete(watcher, FILE_NAME_4));
        TEST_ASSERT(MA_OK == ma_watcher_delete(watcher, FILE_NAME_5));
    }
    return MA_OK;
}

// Create a folder
static void create_test_folder(const char *dir_name)
{
  uv_fs_t req;

  uv_fs_mkdir(ma_event_loop_get_uv_loop(loop), &req, dir_name, O_RDWR | O_CREAT, NULL);
  uv_fs_req_cleanup(&req);
}

// Remove the folder
static void remove_test_folder(const char *dir_name)
{
  uv_fs_t req;
  uv_fs_rmdir(ma_event_loop_get_uv_loop(loop), &req, dir_name, NULL);
  uv_fs_req_cleanup(&req);
}

// Create a file (close the file. The write function should open it again)
static void create_file(const char* file_name)
{
  open_request_result = uv_fs_open(ma_event_loop_get_uv_loop(loop), &req, file_name, O_RDWR | O_CREAT, S_IWRITE | S_IREAD, NULL);
  uv_fs_req_cleanup(&req);

  uv_fs_close(ma_event_loop_get_uv_loop(loop), &req, open_request_result, NULL);
  uv_fs_req_cleanup(&req);
}

// Write into the file (File modification. It should be detected by watcher)
static void write_file(const char* file_name, const char *value)
{
  open_request_result = uv_fs_open(ma_event_loop_get_uv_loop(loop), &req, file_name, O_RDWR | O_CREAT | O_APPEND, S_IWRITE | S_IREAD, NULL);
  uv_fs_req_cleanup(&req);

  uv_fs_write(ma_event_loop_get_uv_loop(loop), &req, open_request_result, (void*)value, strlen(value), -1, NULL);
  uv_fs_req_cleanup(&req);

  uv_fs_close(ma_event_loop_get_uv_loop(loop), &req, open_request_result, NULL);
  uv_fs_req_cleanup(&req);
}

// Delete file
static void remove_file(const char* file_name)
{
  open_request_result = uv_fs_open(ma_event_loop_get_uv_loop(loop), &req, file_name, O_RDWR | O_CREAT, S_IWRITE | S_IREAD, NULL);
  uv_fs_req_cleanup(&req);

  uv_fs_close(ma_event_loop_get_uv_loop(loop), &req, open_request_result, NULL);
  uv_fs_req_cleanup(&req);

  uv_fs_unlink(ma_event_loop_get_uv_loop(loop), &req, file_name, NULL);
  uv_fs_req_cleanup(&req);
}


/*
Test Setup -
1) Create event loop.
2) Create file watcher.
3) Create a test folder.
4) Re-initialize counter for maintaining the directory and file count.
*/
TEST_SETUP(file_watcher_tests)
{
    ma_event_loop_create(&loop);
    ma_file_watcher_create(loop, &watcher);

    create_test_folder(DIR_NAME);

    dir_watcher_count = 0;
    file_watcher_count = 0;
}

/*
Test Cleanup -
1) Remove test folder.
2) Release file watcher.
3) Release event loop.
*/
TEST_TEAR_DOWN(file_watcher_tests)
{
    remove_test_folder(DIR_NAME);

    ma_watcher_release((ma_watcher_t *)watcher);
    ma_event_loop_release(loop);
}


/*
1) Create a folder and add directory watcher on it.
2) Create 5 files in the folder. (5 dir. watcher callbacks)
3) Run event loop.
4) Remove all 5 files.
5) Remove directory.
*/
TEST(file_watcher_tests, file_creation_in_directory_test)
{
    test_number = 1;

    TEST_ASSERT(MA_OK == ma_watcher_add((ma_watcher_t*)watcher, DIR_NAME, dir_watcher_cb, MA_TRUE, NULL));
    create_file(FILE_NAME_1);
    create_file(FILE_NAME_2);
    create_file(FILE_NAME_3);
    create_file(FILE_NAME_4);
    create_file(FILE_NAME_5);

    ma_event_loop_run(loop);

    // Directory watcher count should be 5
    TEST_ASSERT(5 == dir_watcher_count);

    // File watcher count should be 0. No file watcher associated.
    TEST_ASSERT(0 == file_watcher_count);

    remove_file(FILE_NAME_1);
    remove_file(FILE_NAME_2);
    remove_file(FILE_NAME_3);
    remove_file(FILE_NAME_4);
    remove_file(FILE_NAME_5);
}

/*
1) Create a folder and add directory watcher on it.
2) Create 5 files in the folder and add file watcher on those files. (5 dir. watcher callbacks)
3) Write some values in each of these files. (5 dir. watcher callbacks, 5 file watcher callbacks)
4) Write some values in each of these files again. (5 dir. watcher callbacks, 5 file watcher callbacks)
5) Run event loop.
6) Remove all 5 files. (5 dir. watcher callbacks, 5 file watcher callbacks)
7) Remove directory.
*/
TEST(file_watcher_tests, file_modification_in_directory_test)
{
	test_number = 2;

	TEST_ASSERT(MA_OK == ma_watcher_add((ma_watcher_t*)watcher, DIR_NAME, dir_watcher_cb, MA_TRUE, NULL)); 	

	create_file(FILE_NAME_1);
    create_file(FILE_NAME_2);
    create_file(FILE_NAME_3);
    create_file(FILE_NAME_4);
    create_file(FILE_NAME_5);

    TEST_ASSERT(MA_OK == ma_watcher_add((ma_watcher_t*)watcher, FILE_NAME_1, file_watcher_cb, MA_TRUE, NULL));
    TEST_ASSERT(MA_OK == ma_watcher_add((ma_watcher_t*)watcher, FILE_NAME_2, file_watcher_cb, MA_TRUE, NULL));
    TEST_ASSERT(MA_OK == ma_watcher_add((ma_watcher_t*)watcher, FILE_NAME_3, file_watcher_cb, MA_TRUE, NULL));
    TEST_ASSERT(MA_OK == ma_watcher_add((ma_watcher_t*)watcher, FILE_NAME_4, file_watcher_cb, MA_TRUE, NULL));
    TEST_ASSERT(MA_OK == ma_watcher_add((ma_watcher_t*)watcher, FILE_NAME_5, file_watcher_cb, MA_TRUE, NULL));

    write_file(FILE_NAME_1, "foo");
    write_file(FILE_NAME_2, "foo");
    write_file(FILE_NAME_3, "foo");
    write_file(FILE_NAME_4, "foo");
    write_file(FILE_NAME_5, "foo");

    write_file(FILE_NAME_1, " bar");
    write_file(FILE_NAME_2, " bar");
    write_file(FILE_NAME_3, " bar");
    write_file(FILE_NAME_4, " bar");
    write_file(FILE_NAME_5, " bar");

    ma_event_loop_run(loop);

    // Directory watcher count should be 15 (5 for file creation + 10 for file modification)
    TEST_ASSERT(15 == dir_watcher_count);

    // File watcher count should be 10.
    TEST_ASSERT(10 == file_watcher_count);

    remove_file(FILE_NAME_1);
    remove_file(FILE_NAME_2);
    remove_file(FILE_NAME_3);
    remove_file(FILE_NAME_4);
    remove_file(FILE_NAME_5);
}

/*
1) Create a folder and add directory watcher on it.
2) Create 5 files in the folder and add file watcher on those files. (5 dir. watcher callbacks)
3) Write some values in each of these files. (5 dir. watcher callbacks, 5 file watcher callbacks)
4) Remove all 5 files. (5 dir. watcher callbacks, 5 file watcher callbacks)
5) Run event loop.
6) Remove directory.
*/
TEST(file_watcher_tests, file_removal_in_directory_test)
{
    test_number = 3;

    TEST_ASSERT(MA_OK == ma_watcher_add((ma_watcher_t*)watcher, DIR_NAME, dir_watcher_cb, MA_TRUE, NULL));
    create_file(FILE_NAME_1);
    create_file(FILE_NAME_2);
    create_file(FILE_NAME_3);
    create_file(FILE_NAME_4);
    create_file(FILE_NAME_5);

    TEST_ASSERT(MA_OK == ma_watcher_add((ma_watcher_t*)watcher, FILE_NAME_1, file_watcher_cb, MA_TRUE, NULL));
    TEST_ASSERT(MA_OK == ma_watcher_add((ma_watcher_t*)watcher, FILE_NAME_2, file_watcher_cb, MA_TRUE, NULL));
    TEST_ASSERT(MA_OK == ma_watcher_add((ma_watcher_t*)watcher, FILE_NAME_3, file_watcher_cb, MA_TRUE, NULL));
    TEST_ASSERT(MA_OK == ma_watcher_add((ma_watcher_t*)watcher, FILE_NAME_4, file_watcher_cb, MA_TRUE, NULL));
    TEST_ASSERT(MA_OK == ma_watcher_add((ma_watcher_t*)watcher, FILE_NAME_5, file_watcher_cb, MA_TRUE, NULL));

    write_file(FILE_NAME_1, "foo");
    write_file(FILE_NAME_2, "foo");
    write_file(FILE_NAME_3, "foo");
    write_file(FILE_NAME_4, "foo");
    write_file(FILE_NAME_5, "foo");

    remove_file(FILE_NAME_1);
    remove_file(FILE_NAME_2);
    remove_file(FILE_NAME_3);
    remove_file(FILE_NAME_4);
    remove_file(FILE_NAME_5);

    ma_event_loop_run(loop);

    // Directory watcher count should be 15 (5 for file creation + 5 for file modification + 5 for file deletion)
    TEST_ASSERT(15 == dir_watcher_count);

    // File watcher count should be 10.
    TEST_ASSERT(10 == file_watcher_count);
}

