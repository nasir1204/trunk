/******************************************************************************
Unit tests for registry watcher.

Note -
Following are the limitations in the registry watcher code as of now.

1) There is a same entry in both default and alternate registry locations, the
   ma_watcher_delete can only delete the entry for one location only.
******************************************************************************/

#include "unity.h"
#include "unity_fixture.h"
#include "unity_internals.h"
#include "ma/internal/utils/watcher/ma_registry_watcher.h"
#include "ma/datastore/ma_ds_registry.h"
#include <string.h>

TEST_GROUP_RUNNER(ma_registry_watcher_test_group)
{
    do {RUN_TEST_CASE(registry_watcher_tests, reg_default_watcher_default)} while (0);
    do {RUN_TEST_CASE(registry_watcher_tests, reg_default_watcher_alternate)} while (0);
    do {RUN_TEST_CASE(registry_watcher_tests, reg_alternate_watcher_default)} while (0);
    do {RUN_TEST_CASE(registry_watcher_tests, reg_alternate_watcher_alternate)} while (0);
}

ma_bool_t isSystem64Bit()
{
#ifdef MA_WINDOWS

    UINT size = 0;
    HMODULE hm_k32 = (HMODULE) 0;
    FARPROC proc;
    TCHAR tmp[1];
    OSVERSIONINFOW g_osvi = {0};
    ma_bool_t ret = MA_FALSE;

    g_osvi.dwOSVersionInfoSize = sizeof( OSVERSIONINFO );
    GetVersionEx(&g_osvi);
    // GetSystemWow64Directory() requires at least Win XP or Win 2003 Server
    // and 64-bit is potentially only on at least XP.
    if (g_osvi.dwMajorVersion >= 4 || (g_osvi.dwMajorVersion >= 3 && g_osvi.dwMinorVersion >= 1))
    {
        // Since delay loading Kernel32.dll is not supported, we need to
        // check for the presence of GetSystemWow64Directory manually.
        hm_k32 = LoadLibrary(L"Kernel32.dll");
        if (hm_k32)
        {
            proc = GetProcAddress(hm_k32, "GetSystemWow64DirectoryW");
            if (proc)
            {
                // GetSystemWow64Directory always fails on 32 bit operating systems.
                // It returns 0 on failure or the length, so we must give it a chance
                // to copy at least 1 character into the buffer so that we can dif-
                // ferentiate between an error code and a size of 0.
                size = ((UINT (__stdcall *)(LPTSTR, UINT)) proc)(tmp, 1);
                if (size > 0)
                    ret = MA_TRUE;
            }
            FreeLibrary(hm_k32);
        }
    }
    return ret;
#endif
    return MA_FALSE;
}

#define REG_ROOT_KEY "Software\\McAfee"

#define REG_NAME_1 "HKEY_LOCAL_MACHINE\\Software\\McAfee\\Watcher_Test\\process_status"
#define REG_NAME_2 "HKEY_LOCAL_MACHINE\\Software\\McAfee\\Watcher_Test\\file_status"

static ma_watcher_t *watcher = NULL; // Watcher
static ma_event_loop_t *loop = NULL; // Event Loop
static ma_loop_worker_t *loop_worker = NULL; // Loop worker
static uv_timer_t timer_req;         // UV Timer object
static ma_ds_t *g_ds = NULL;         // Datastore handle

// Callback counters
static int watcher_cb_1_count = 0;
static int watcher_cb_2_count = 0;

// Test sequence counter
static int counter = 0;

TEST_SETUP(registry_watcher_tests)
{
    ma_event_loop_create(&loop);
    ma_loop_worker_create(loop, &loop_worker);
}

TEST_TEAR_DOWN(registry_watcher_tests)
{
    ma_loop_worker_release(loop_worker);
    ma_event_loop_release(loop);
}

static ma_error_t watcher_cb_1(ma_watcher_t *watcher, const char *name, void *cb_data)
{
    printf("Registry watcher_cb_1 invoked for registry <%s> changes count is <%d>\n", name, watcher_cb_1_count);

    watcher_cb_1_count++;

    TEST_ASSERT(0 == strcmp(name, REG_NAME_1));

    return MA_OK;
}


static ma_error_t watcher_cb_2(ma_watcher_t *watcher, const char *name, void *cb_data)
{
    printf("Registry watcher_cb_2 invoked for registry <%s> changes count is <%d>\n", name, watcher_cb_2_count);

    watcher_cb_2_count++;
    TEST_ASSERT(0 == strcmp(name, REG_NAME_2));

    return MA_OK;
}

// Dummy timer close handle
static void close_cb(uv_handle_t *handle)
{
}

void loop_stop_handler(ma_event_loop_t *loop, int status, void *cb_data) {
    ma_loop_worker_t *loop_worker = (ma_loop_worker_t *)cb_data;
    
    ma_loop_worker_stop(loop_worker);
}

static void clear_watchers()
{
    TEST_ASSERT(MA_OK == ma_watcher_delete(watcher, REG_NAME_1));
    TEST_ASSERT(MA_OK == ma_watcher_delete(watcher, REG_NAME_2));
    uv_timer_stop(&timer_req);
    uv_close((uv_handle_t *)&timer_req, close_cb);
    ma_event_loop_register_stop_handler(loop, loop_stop_handler, loop_worker);
    ma_event_loop_stop(loop);
}

// Modify the registry settings periodically.
static void timer_callback(uv_timer_t* handle, int status)
{
    if(counter == 0) // Addition (1st callback)
    {
        TEST_ASSERT(MA_OK == ma_ds_set_int(g_ds, "Watcher_Test\\process_status", "num_threads", 10));
        TEST_ASSERT(MA_OK == ma_ds_set_int(g_ds, "Watcher_Test\\file_status", "open_sockets", 15));
    }
    if(counter == 1) // Modification (2nd callback)
    {
        TEST_ASSERT(MA_OK == ma_ds_set_int(g_ds, "Watcher_Test\\process_status", "num_threads", 20));
        TEST_ASSERT(MA_OK == ma_ds_set_int(g_ds, "Watcher_Test\\file_status", "open_sockets", 30));
    }
    if(counter == 2) // Removal (3rd callback)
    {
        TEST_ASSERT(MA_OK == ma_ds_rem(g_ds, "Watcher_Test\\process_status", "is_running", 0));
        TEST_ASSERT(MA_OK == ma_ds_rem(g_ds, "Watcher_Test\\file_status", "is_open", 0));
    }
    if(counter == 3) // Modification (4th callback)
    {
        TEST_ASSERT(MA_OK == ma_ds_set_int(g_ds, "Watcher_Test\\process_status", "num_threads", 30));
        TEST_ASSERT(MA_OK == ma_ds_set_int(g_ds, "Watcher_Test\\file_status", "open_sockets", 40));
    }
    if(counter == 4) // Addition (5th callback)
    {
        TEST_ASSERT(MA_OK == ma_ds_set_int(g_ds, "Watcher_Test\\process_status", "num_handles", 2));
        TEST_ASSERT(MA_OK == ma_ds_set_int(g_ds, "Watcher_Test\\file_status", "open_file_handles", 7));
    }
    if(counter == 5) // Recursive Removal (6 callback - 1 each for 'process_status' and 'file_status')
    {
        TEST_ASSERT(MA_OK == ma_ds_set_int(g_ds, "Watcher_Test\\process_status", "num_handles", 24));
        TEST_ASSERT(MA_OK == ma_ds_set_int(g_ds, "Watcher_Test\\file_status", "open_file_handles", 71));
    }

    if(counter == 6)
    {
        clear_watchers();
    }

    counter++;
}

// Registry created in default value and watcher is monitoring the default view
TEST(registry_watcher_tests, reg_default_watcher_default)
{
    ma_ds_registry_t *ds_reg = NULL ;
    TEST_ASSERT(MA_OK == ma_ds_registry_open(HKEY_LOCAL_MACHINE, REG_ROOT_KEY, MA_TRUE, MA_TRUE, KEY_ALL_ACCESS, &ds_reg));
    g_ds = (ma_ds_t*)ds_reg;

    // Clearing all counters at the start of the test
    watcher_cb_1_count = watcher_cb_2_count = counter = 0;
    
    ma_loop_worker_start(loop_worker);

    TEST_ASSERT(MA_OK == ma_registry_watcher_create(loop, loop_worker, &watcher));
	ma_regitry_watcher_set_filter(watcher, REG_NOTIFY_CHANGE_NAME | REG_NOTIFY_CHANGE_LAST_SET);
    TEST_ASSERT(MA_OK == ma_ds_set_str(g_ds, "Watcher_Test\\process_status", "is_running", "true", strlen("true")));
    TEST_ASSERT(MA_OK == ma_ds_set_str(g_ds, "Watcher_Test\\file_status", "is_open", "false", strlen("false")));

    // Adding watchers for monitoring registry
    TEST_ASSERT(MA_OK == ma_watcher_add((ma_watcher_t *)watcher, REG_NAME_1, watcher_cb_1, MA_TRUE, NULL));
    TEST_ASSERT(MA_OK == ma_watcher_add((ma_watcher_t *)watcher, REG_NAME_2, watcher_cb_2, MA_TRUE, NULL));

    // Setting up timer for periodically changing the registry.
    uv_timer_init(ma_event_loop_get_uv_loop(loop), &timer_req);
    uv_timer_start(&timer_req, timer_callback, 2000, 2000);

    // Starting event loop
    ma_event_loop_run(loop);

    TEST_ASSERT(6 == watcher_cb_1_count);
    TEST_ASSERT(6 == watcher_cb_2_count);

    // Cleanup
    ma_watcher_release((ma_watcher_t *)watcher);
    TEST_ASSERT(MA_OK == ma_ds_rem(g_ds, "Watcher_Test", NULL, 1));
    TEST_ASSERT(MA_OK == ma_ds_registry_release((ma_ds_registry_t*)g_ds));    
}

// Registry created in default value and watcher is monitoring the alternate view
TEST(registry_watcher_tests, reg_default_watcher_alternate)
{
    ma_ds_registry_t *ds_reg = NULL ;
    TEST_ASSERT(MA_OK == ma_ds_registry_open(HKEY_LOCAL_MACHINE, REG_ROOT_KEY, MA_TRUE, MA_TRUE, KEY_ALL_ACCESS, &ds_reg));
    g_ds = (ma_ds_t*)ds_reg;

    // Clearing all counters at the start of the test
    watcher_cb_1_count = watcher_cb_2_count = counter = 0;

    ma_loop_worker_start(loop_worker);

    TEST_ASSERT(MA_OK == ma_registry_watcher_create(loop, loop_worker, &watcher));
	ma_regitry_watcher_set_filter(watcher, REG_NOTIFY_CHANGE_NAME | REG_NOTIFY_CHANGE_LAST_SET);
    TEST_ASSERT(MA_OK == ma_ds_set_str(g_ds, "Watcher_Test\\process_status", "is_running", "true", strlen("true")));
    TEST_ASSERT(MA_OK == ma_ds_set_str(g_ds, "Watcher_Test\\file_status", "is_open", "false", strlen("false")));

    // Adding watchers for monitoring registry
    TEST_ASSERT(MA_OK == ma_watcher_add((ma_watcher_t *)watcher, REG_NAME_1, watcher_cb_1, MA_FALSE, NULL));
    TEST_ASSERT(MA_OK == ma_watcher_add((ma_watcher_t *)watcher, REG_NAME_2, watcher_cb_2, MA_FALSE, NULL));

    // Setting up timer for periodically changing the registry.
    uv_timer_init(ma_event_loop_get_uv_loop(loop), &timer_req);
    uv_timer_start(&timer_req, timer_callback, 2000, 2000);

    // Starting event loop
    ma_event_loop_run(loop);

    // Watcher should not be hit for a 64 bit platform. On a 32-bit
    // machine, there are no default or alternate views, therefore,
    // the entry must always be watched.
    if(MA_TRUE == isSystem64Bit())
    {
        TEST_ASSERT(0 == watcher_cb_1_count);
        TEST_ASSERT(0 == watcher_cb_2_count);
    }
    else
    {
        TEST_ASSERT(6 == watcher_cb_1_count);
        TEST_ASSERT(6 == watcher_cb_2_count);
    }

    // Cleanup
    ma_watcher_release((ma_watcher_t *)watcher);
    TEST_ASSERT(MA_OK == ma_ds_rem(g_ds, "Watcher_Test", NULL, 1));
    TEST_ASSERT(MA_OK == ma_ds_registry_release((ma_ds_registry_t*)g_ds));
}


// Registry created in alternate value and watcher is monitoring the default view
TEST(registry_watcher_tests, reg_alternate_watcher_default)
{
    ma_ds_registry_t *ds_reg = NULL ;
    TEST_ASSERT(MA_OK == ma_ds_registry_open(HKEY_LOCAL_MACHINE, REG_ROOT_KEY, MA_FALSE, MA_TRUE, KEY_ALL_ACCESS, &ds_reg));
    g_ds = (ma_ds_t*)ds_reg;

    // Clearing all counters at the start of the test
    watcher_cb_1_count = watcher_cb_2_count = counter = 0;

    ma_loop_worker_start(loop_worker);

    TEST_ASSERT(MA_OK == ma_registry_watcher_create(loop, loop_worker, &watcher));
	ma_regitry_watcher_set_filter(watcher, REG_NOTIFY_CHANGE_NAME | REG_NOTIFY_CHANGE_LAST_SET);
    TEST_ASSERT(MA_OK == ma_ds_set_str(g_ds, "Watcher_Test\\process_status", "is_running", "true", strlen("true")));
    TEST_ASSERT(MA_OK == ma_ds_set_str(g_ds, "Watcher_Test\\file_status", "is_open", "false", strlen("false")));

    // Adding watchers for monitoring registry
    TEST_ASSERT(MA_OK == ma_watcher_add((ma_watcher_t *)watcher, REG_NAME_1, watcher_cb_1, MA_TRUE, NULL));
    TEST_ASSERT(MA_OK == ma_watcher_add((ma_watcher_t *)watcher, REG_NAME_2, watcher_cb_2, MA_TRUE, NULL));

    // Setting up timer for periodically changing the registry.
    uv_timer_init(ma_event_loop_get_uv_loop(loop), &timer_req);
    uv_timer_start(&timer_req, timer_callback, 2000, 2000);

    // Starting event loop
    ma_event_loop_run(loop);

    // Watcher should not be hit for a 64 bit platform. On a 32-bit
    // machine, there are no default or alternate views, therefore,
    // the entry must always be watched.
    if(MA_TRUE == isSystem64Bit())
    {
        TEST_ASSERT(0 == watcher_cb_1_count);
        TEST_ASSERT(0 == watcher_cb_2_count);
    }
    else
    {
        TEST_ASSERT(6 == watcher_cb_1_count);
        TEST_ASSERT(6 == watcher_cb_2_count);
    }

    // Cleanup
    ma_watcher_release((ma_watcher_t *)watcher);
    TEST_ASSERT(MA_OK == ma_ds_rem(g_ds, "Watcher_Test", NULL, 1));
    TEST_ASSERT(MA_OK == ma_ds_registry_release((ma_ds_registry_t*)g_ds));
}

// Registry created in alternate value and watcher is monitoring the alternate view
TEST(registry_watcher_tests, reg_alternate_watcher_alternate)
{
    ma_ds_registry_t *ds_reg = NULL ;
    TEST_ASSERT(MA_OK == ma_ds_registry_open(HKEY_LOCAL_MACHINE, REG_ROOT_KEY, MA_FALSE, MA_TRUE, KEY_ALL_ACCESS, &ds_reg));
    g_ds = (ma_ds_t*)ds_reg;

    // Clearing all counters at the start of the test
    watcher_cb_1_count = watcher_cb_2_count = counter = 0;
    
    ma_loop_worker_start(loop_worker);

    TEST_ASSERT(MA_OK == ma_registry_watcher_create(loop, loop_worker, &watcher));
	ma_regitry_watcher_set_filter(watcher, REG_NOTIFY_CHANGE_NAME | REG_NOTIFY_CHANGE_LAST_SET);
    TEST_ASSERT(MA_OK == ma_ds_set_str(g_ds, "Watcher_Test\\process_status", "is_running", "true", strlen("true")));
    TEST_ASSERT(MA_OK == ma_ds_set_str(g_ds, "Watcher_Test\\file_status", "is_open", "false", strlen("false")));

    // Adding watchers for monitoring registry
    TEST_ASSERT(MA_OK == ma_watcher_add((ma_watcher_t *)watcher, REG_NAME_1, watcher_cb_1, MA_FALSE, NULL));
    TEST_ASSERT(MA_OK == ma_watcher_add((ma_watcher_t *)watcher, REG_NAME_2, watcher_cb_2, MA_FALSE, NULL));

    // Setting up timer for periodically changing the registry.
    uv_timer_init(ma_event_loop_get_uv_loop(loop), &timer_req);
    uv_timer_start(&timer_req, timer_callback, 2000, 2000);

    // Starting event loop
    ma_event_loop_run(loop);

    TEST_ASSERT(6 == watcher_cb_1_count);
    TEST_ASSERT(6 == watcher_cb_2_count);

    // Cleanup
    ma_watcher_release((ma_watcher_t *)watcher);
    TEST_ASSERT(MA_OK == ma_ds_rem(g_ds, "Watcher_Test", NULL, 1));
    TEST_ASSERT(MA_OK == ma_ds_registry_release((ma_ds_registry_t*)g_ds));
}







