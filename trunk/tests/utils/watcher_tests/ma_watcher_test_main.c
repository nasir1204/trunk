/******************************************************************************
Main test runner for the file/registry/process watcher UTs
******************************************************************************/
#include "unity.h"
#include "unity_fixture.h"
#include "ma/ma_common.h"

#ifdef MA_WINDOWS
#include <windows.h>
#endif
#ifdef _MSC_VER
# include <crtdbg.h> /* */
# define MA_TRACKLEAKS _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF)
#else
# define MA_TRACKLEAKS ((void *)0)
#endif

// Registry and File watcher UTs are enabled for Windows environment only. Following
// issues are pending in the watcher -
// 1) File watcher UV based implementation doesn't work for old Linux versions,
//    therefore, it is not enabled.
// 2) Process watcher implementation is idle timer based. Still to discuss about the
//    usefulness and usage of this implementation. Probably, this is not what is
//    needed. Therefore, UTs are not fixed as of now.
static void run_all_tests(void)
{
//    do { RUN_TEST_GROUP(ma_process_watcher_test_group);} while(0);
#ifdef MA_WINDOWS
    do { RUN_TEST_GROUP(ma_file_watcher_test_group);} while(0);
    do { RUN_TEST_GROUP(ma_registry_watcher_test_group);} while(0);
#endif
}

int main(int argc, char *argv[])
{
    MA_TRACKLEAKS;
    return UnityMain(argc, argv, run_all_tests);
}
