/******************************************************************************
Unit tests for process watcher.
******************************************************************************/

#include "unity.h"
#include "unity_fixture.h"
#include "unity_internals.h"
#include "uv.h"
#include "ma/internal/utils/watcher/ma_process_watcher.h"

TEST_GROUP_RUNNER(ma_process_watcher_test_group)
{
    do {RUN_TEST_CASE(process_watcher_tests, proc_test)} while (0);
}

static ma_watcher_t *watcher = NULL; // Watcher handle
static ma_event_loop_t *loop = NULL; // Event loop
static uv_timer_t timer_req;         // UV Timer object

#ifdef MA_WINDOWS
    #define PROC_NAME "calc.exe"
#else
    #define PROC_NAME "ma_broker"
#endif

enum { INITIAL, RUNNING, STOPPED, RUN_WATCHED, STOP_WATCHED, COMPLETE } current_process_status;

TEST_SETUP(process_watcher_tests)
{
    ma_event_loop_create(&loop);
    ma_process_watcher_create(loop, &watcher);
}

TEST_TEAR_DOWN(process_watcher_tests)
{
    ma_watcher_release((ma_watcher_t *)watcher);
    ma_event_loop_release(loop);
}

static ma_error_t proc_watcher_cb(ma_watcher_t *watcher, const char *name, void *cb_data)
{
    static int count = 0;

    printf("Process watcher callback invoked for process <%s> changes  count is <%d>\n", name, count);
    count++;

    if(current_process_status == RUNNING)
    {
        current_process_status = RUN_WATCHED;
    }

    if(current_process_status == STOPPED)
    {
        current_process_status = STOP_WATCHED;
    }

    if(11 == count)
    {
        TEST_ASSERT(MA_OK == ma_watcher_delete(watcher, name));
        current_process_status = COMPLETE;
    }

    return MA_OK;
}

// Run or stop a dummy process on each timer call. The Process watcher should be able
// to detect changes.
static void timer_callback(uv_timer_t* handle, int status)
{
    if(current_process_status == INITIAL || current_process_status == STOP_WATCHED)
    {
        #ifdef MA_WINDOWS
            system("start calc.exe");
        #else
            system("ma_brokersvc start");
        #endif
        current_process_status = RUNNING;
    }

    if(current_process_status == RUN_WATCHED)
    {
        #ifdef MA_WINDOWS
            system("taskkill /IM calc.exe");
        #else
            system("ma_brokersvc stop");
        #endif
        current_process_status = STOPPED;
    }

    if(current_process_status == COMPLETE)
    {
        uv_timer_stop(handle);
    }
}


TEST(process_watcher_tests, proc_test)
{
    TEST_ASSERT( MA_OK == ma_watcher_add((ma_watcher_t*)watcher, PROC_NAME, proc_watcher_cb, MA_TRUE, NULL));

    uv_timer_init(ma_event_loop_get_uv_loop(loop), &timer_req);
    uv_timer_start(&timer_req, timer_callback, 2000, 2000);

    current_process_status = INITIAL;

    ma_event_loop_run(loop);
}

