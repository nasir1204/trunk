/*****************************************************************************
Main Test runner for all macrypto  unit tests.
******************************************************************************/

#include "unity.h"
#include "unity_fixture.h"
#include "ma/ma_common.h"

#include "ma_crypto_tests_setup.h"

#ifdef MA_WINDOWS
#include <windows.h>
#endif
#ifdef _MSC_VER
# include <crtdbg.h> /* */
# define MA_TRACKLEAKS _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF)
#else
# define MA_TRACKLEAKS ((void *)0)
#endif


static void run_all_tests(void) {    
     do { RUN_TEST_GROUP(macrypto_tests_group);} while(0); /* do loop to work around unity declaration flaws that are not ANSI C compatible */ 
    
    /* setup tests for constructing the macrypto object with mode - NON-FIPS, role - officer and agent mode -MANAGED 
       teardown tests for destructing the macrypto object 
    */
    do { RUN_TEST_GROUP(macrypto_tests_setup_group);} while(0);
    do { RUN_TEST_GROUP(macrypto_hash_test_group);} while(0);

#if !defined(MACX)
	do { RUN_TEST_GROUP(macrypto_sign_test_group);} while(0); 
    do { RUN_TEST_GROUP(macrypto_encode_test_group);} while(0);
	do { RUN_TEST_GROUP(macrypto_non_fips_enc_dec_test_group);} while(0); 
    do { RUN_TEST_GROUP(macrypto_tests_teardown_group);} while(0);
	/* FIPS mode macrypto objects and corresponding tests should be executed here as RSA lib can not be loaded again in same process
       once we initialize with FIPS mode */
	do { RUN_TEST_GROUP(macrypto_fips_tests_setup_group);} while(0);  
	do { RUN_TEST_GROUP(macrypto_fips_enc_dec_test_group);} while(0); 
	do { RUN_TEST_GROUP(macrypto_fips_tests_teardown_group);} while(0);
#else
	do { RUN_TEST_GROUP(macrypto_encode_test_group);} while(0);
	do { RUN_TEST_GROUP(macrypto_tests_teardown_group);} while(0);
#endif
}

/************* Non FISP Setup ****************************************/
ma_crypto_t *test_macrypto = NULL ;  

TEST_GROUP_RUNNER(macrypto_tests_setup_group) {
    RUN_TEST_CASE(macrypto_setup_tests, create_macrypto)
}
TEST_SETUP(macrypto_setup_tests) {
}
TEST_TEAR_DOWN(macrypto_setup_tests) {
}
TEST(macrypto_setup_tests, create_macrypto) { 
    TEST_ASSERT_EQUAL_INT(1,create_macrypto_object_with_keys(MA_CRYPTO_MODE_NON_FIPS,MA_CRYPTO_ROLE_OFFICER,MA_CRYPTO_AGENT_MANAGED,&test_macrypto));
}

TEST_GROUP_RUNNER(macrypto_tests_teardown_group) {
    RUN_TEST_CASE(macrypto_teardown_tests, release_macrypto)    
}
TEST_SETUP(macrypto_teardown_tests) {
}
TEST_TEAR_DOWN(macrypto_teardown_tests) { 
}
TEST(macrypto_teardown_tests, release_macrypto) { 
    TEST_ASSERT_EQUAL_INT(1,release_macrypto_object(MA_CRYPTO_MODE_NON_FIPS,MA_CRYPTO_ROLE_OFFICER,MA_CRYPTO_AGENT_MANAGED,test_macrypto));
}

/***************** FIPS setup ********************************************/
ma_crypto_t *fips_test_macrypto = NULL ;  

TEST_GROUP_RUNNER(macrypto_fips_tests_setup_group) {
    RUN_TEST_CASE(macrypto_fips_setup_tests, create_fips_macrypto)
}
TEST_SETUP(macrypto_fips_setup_tests) {
}
TEST_TEAR_DOWN(macrypto_fips_setup_tests) {
}
TEST(macrypto_fips_setup_tests, create_fips_macrypto) { 
    TEST_ASSERT_EQUAL_INT(1,create_macrypto_object_with_keys(MA_CRYPTO_MODE_FIPS,MA_CRYPTO_ROLE_OFFICER,MA_CRYPTO_AGENT_MANAGED,&fips_test_macrypto));
}

TEST_GROUP_RUNNER(macrypto_fips_tests_teardown_group) {
    RUN_TEST_CASE(macrypto_fips_teardown_tests, release_fips_macrypto)    
}
TEST_SETUP(macrypto_fips_teardown_tests) {
}
TEST_TEAR_DOWN(macrypto_fips_teardown_tests) { 
}
TEST(macrypto_fips_teardown_tests, release_fips_macrypto) { 
    TEST_ASSERT_EQUAL_INT(1,release_macrypto_object(MA_CRYPTO_MODE_FIPS,MA_CRYPTO_ROLE_OFFICER,MA_CRYPTO_AGENT_MANAGED,fips_test_macrypto));
}

int main(int argc, char *argv[]) {
    MA_TRACKLEAKS;
    return UnityMain(argc, argv, run_all_tests);
}
