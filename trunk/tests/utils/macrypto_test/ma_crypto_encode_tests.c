#include "unity.h"
#include "unity_fixture.h"

#include "ma/internal/utils/macrypto/ma_crypto.h"

#include <string.h>

TEST_GROUP_RUNNER(macrypto_encode_test_group) {
    RUN_TEST_CASE(macrypto_encode_tests, test_encode_decode)
    do {RUN_TEST_CASE(macrypto_encode_tests, test_encode_decode_skip_conversion);} while (0);     
}

#define DATA "BADCm0gA9nALbG4C1Kcsr035/z30S7RwvEvcR+NtAItcnR+Zy6qrBuVMcn21OFnnftKSPuetlRzf4jXnGYh6rsbzD981q0JaNaLX73bW+Dszc/nvKq+rTDEDFc3SB1bY0+SzJNItSWgLJFEPRjadc1aJuTyuHSxJKXS8VliEa+wCg/3hun4P8w5G7HqXQ1KR/nbjT66kD/2JXaGGKudO6sTIv7hxl2oK64hq1MXQ2LVHJ37t3ECro+75plCffUQQe9RHwRJRfsHEoVZ7+4x3K+kdM+vr1d+/G+VGcq38e6otLX282ul4dUy3cLS8OvpknfuOjj5D1uMosQX1QFBt90Bzvzytn9oxG/la5qxURGP95UMfCoa3XBACiJ8f3qTvuFz47ieRONJW7Y7XFjzYDA1FqCsBy2licrofqWHSxDBMJZa0wWqkyg6FymsPMHkgjsnyOD4VDyCC5D6d0wi1oTB7Fdv+lDaJ/cvD8lmOsjKZP4uUm3C0dkDFtMjzyH7T1cTedKLjbHJ0tlu3SU6UoLhxUqJ52pm3KGA=="
#define DATA_SIZE 549

ma_bytebuffer_t *encoded_data = NULL;
ma_bytebuffer_t *decoded_data = NULL;

TEST_SETUP(macrypto_encode_tests) {
    ma_bytebuffer_create(2*DATA_SIZE, &encoded_data);
    ma_bytebuffer_create(DATA_SIZE, &decoded_data);
}

TEST_TEAR_DOWN(macrypto_encode_tests) { 
    ma_bytebuffer_release(encoded_data);
    ma_bytebuffer_release(decoded_data);
}

TEST(macrypto_encode_tests, test_encode_decode) {     
    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_encode((unsigned char *)DATA, DATA_SIZE, encoded_data, MA_FALSE), "ma_crypto_encode failed");    
    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_decode(ma_bytebuffer_get_bytes(encoded_data), ma_bytebuffer_get_size(encoded_data), decoded_data, MA_FALSE), "ma_crypto_decode failed");
    TEST_ASSERT_EQUAL_INT(0 , memcmp(DATA, (const char*)ma_bytebuffer_get_bytes(decoded_data), ma_bytebuffer_get_size(decoded_data)));
}


#define DATA1 "S8zsfPFJo9tnY6QNOsVexH9psAU8z0HbZ2OkDTrFXsR/abAFPM9B3Q=="              /* vinod123 */
#define DATA1_SIZE  56

TEST(macrypto_encode_tests, test_encode_decode_skip_conversion) {     
    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_encode((unsigned char *)DATA1, DATA1_SIZE, encoded_data, MA_TRUE), "ma_crypto_encode failed");    
    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_decode(ma_bytebuffer_get_bytes(encoded_data), ma_bytebuffer_get_size(encoded_data), decoded_data, MA_TRUE), "ma_crypto_decode failed");
    TEST_ASSERT_EQUAL_INT(0 , memcmp(DATA1, (const char*)ma_bytebuffer_get_bytes(decoded_data), ma_bytebuffer_get_size(decoded_data)));
}

