#include "unity.h"
#include "unity_fixture.h"

#include "ma/internal/utils/macrypto/ma_crypto.h"
#include "ma/internal/utils/datastructures/ma_bytebuffer.h"
#include "ma/internal/ma_macros.h"

#include "ma_crypto_tests_setup.h"


#include <sys/stat.h>

unsigned char g_server_public_key[] = {
    "\x52\x53\x41\x31\x30\x82\x01\x0a\x02\x82\x01\x01\x00\xc9\x01\x5a\xee\xc5\xe9\x14\x0b\xee\x0b\x6e\xac\x59\xe5\xfe\xb9\x38\xf5\xf5\xea\xc0\xdf\x78\xe6\xc2\xf2\x4f\x98\x6a\x63\xb4\x72\xb6\x9e\x84\xf7\x5d\xc7\x67\x9e\x31\xde\xc5\xf2\x57\x49\xf8\x0d\x19\xae\x20\
\x85\x85\xa7\x62\x5c\xee\xf8\xa3\x9e\xb9\x01\xa1\x8c\x3e\x70\x85\x52\x02\x8e\x89\xf7\x28\x86\x95\xb1\x1a\x64\x84\xb7\x3c\x6a\x5c\xf7\xf7\xaa\xfc\x2a\x05\x6c\x46\x5f\xbe\x55\x82\x0d\x11\x4f\xa7\x73\xe0\x69\x6e\x7f\xdf\xe3\xa9\xf1\x8d\x89\x05\x2a\xeb\xfa\x4e\
\x6b\x6a\x9c\x89\x16\x6b\xc5\xd2\xaa\x0c\x39\xde\xea\x20\xf0\x32\xb8\x57\x0a\xe7\x2c\x14\x01\xae\x51\x00\x19\xd7\x63\x70\x41\xd1\xbe\x14\x70\xa6\xea\x74\x23\xec\x5c\x67\x9f\xcb\x2b\xff\xfc\xd8\xd3\xf2\xb4\xdb\xda\xe6\x26\x38\xff\x36\x43\xc1\xdc\xfa\xef\xb8\
\x92\x2f\x02\x30\x7e\xd5\xf8\x6f\x5f\x30\x7c\xbf\x6e\xb1\xae\xd4\x98\x45\xa3\x52\xc5\x6f\xcd\x29\xd7\xbf\x46\x8f\x32\x83\x3d\x84\x0e\x88\x0b\x4a\xde\x79\x6b\x3e\x38\x13\xe3\x9b\x32\xc9\x1a\xba\xd0\x56\x30\x2b\x36\xbe\x58\xa8\x48\xd6\x9a\x92\x92\x1e\xc0\x71\
\x9a\xa6\xd3\xba\x47\xbe\xbd\x33\x9c\xbf\x8b\x83\xf1\x02\x03\x01\x00\x01"
};

unsigned char g_server_sign_key[] = {
"\x52\x53\x41\x31\x30\x82\x04\xa6\x02\x01\x00\x02\x82\x01\x01\x00\xa9\xde\x61\x87\xb7\xf3\xd0\x82\x53\xfd\xb1\xdd\xe9\xa2\x0c\x5f\xae\x0c\xd2\x77\x33\xc1\x76\xe9\x42\x87\x2e\x10\x82\x69\xc9\xee\x04\x59\xeb\x9e\xe6\xa5\x87\x41\xdf\x14\x45\xde\x1a\xcd\xbc\x03\
\x5c\x7b\x5d\xe2\x06\xee\x0f\xf7\xec\x2d\x56\x7d\x59\xdd\xf2\x7e\xbe\xf5\x56\x7a\xd5\x57\x18\xf7\x78\xaa\x24\x94\xa1\xa6\xc0\x42\xa5\x8a\xed\x69\xb8\x84\x1b\x26\x71\x46\x90\x5c\xd6\x95\xac\x45\x87\xf7\x4d\x00\x75\x7a\xaf\x03\xf3\x82\x5d\xcd\xf4\x20\x36\x2d\
\xde\x83\x96\x0c\xd3\x82\x1e\x68\x51\xa2\x80\x19\xa3\x1d\x74\xf1\x5d\xe4\x9f\xab\x0a\x18\x61\x2f\x71\x6f\x26\xc1\x79\x97\xff\xf8\x20\x1d\x5f\xca\x19\x06\x0a\x34\xc8\x7d\xb3\x0b\x0c\xe6\x08\x6e\x14\xee\x79\x78\x95\xd5\xdf\x7c\x64\xec\x57\xc6\x1f\xad\x94\x88\
\x04\x9a\xe7\xa5\x7e\x84\xa2\x56\x12\x41\xa7\x95\x70\x84\xa9\x2f\xe8\x0f\xf0\x49\xe0\x6e\xe2\x94\x1c\x54\x6d\x7f\x38\x0e\xfe\x49\xd6\x1b\xdc\x77\x64\x44\x48\xf4\xd1\xb3\xcb\x71\x62\xdc\x02\xb2\x16\x86\x8c\xde\x6a\x8d\x03\x84\x9d\x8e\x6a\xbf\x96\xfc\x28\x41\
\x96\xe9\x12\xa9\x8f\xc6\x4d\x2e\xa2\x5f\x58\x5d\x6d\x5a\x78\x9d\x02\x03\x01\x00\x01\x02\x82\x01\x01\x00\x9e\x24\x87\x5e\x07\xb6\xb8\x16\xad\x45\x11\x53\x4c\x8f\xed\xbf\x21\xd5\x94\x17\x1f\x9a\xec\x2e\x6b\x1e\x58\x97\x4c\x22\x5a\xb8\xf9\x5a\x11\x8e\xe4\xa1\
\x94\x13\x92\x30\x58\x7f\x57\x2a\xd3\xef\xb9\x28\xad\x15\xa1\x67\xfe\x7b\xcc\x67\xc5\x0e\x7c\x1c\xb2\x06\xa7\x96\xd6\x45\x12\xda\xbc\x46\xa0\x12\x9e\x6d\xb5\x34\xfd\xe6\xce\x21\x51\xb7\xcb\xed\x49\x2d\x77\xe5\xbe\xbe\x7f\x93\xe6\x46\xf1\x37\x1d\x7b\xe0\xe3\
\x2f\x8e\x99\x24\x74\x5a\x29\xfd\xd7\x12\xcb\x06\x6c\x2a\x52\xba\xdf\xbe\x34\xc9\xf9\x42\x64\xe6\x26\x98\x8e\x39\x6d\xd5\xdc\xe6\x01\x21\x07\xf3\x8c\x65\xc8\x3c\xb3\xb9\x65\x65\x0c\xe3\x36\x0f\x74\xf4\x12\x5d\x97\x2d\x8a\xb6\x73\x3e\xef\x33\x50\x72\xbe\x41\
\x5b\x40\xba\xe0\xcb\x4c\x28\x8b\xbf\xf4\xcd\xa8\xca\x5d\xc7\x88\xd9\x25\xa1\x14\x71\x14\x8e\xd2\xba\xfc\xfa\xec\x00\x7a\x18\xc5\x34\x9a\xf5\xc3\x58\xcc\xed\x87\x8c\x19\x09\x57\x80\x5a\xcc\xb3\xb4\x0f\xfe\xd0\x45\x86\xeb\x0b\xdd\x39\x81\xd4\x15\x3c\xf8\xdc\
\x29\x5d\x03\xfe\x9f\xa2\xe5\x7c\x39\x9b\x98\xc0\xb9\x73\x6b\xde\xa2\x40\xe9\x97\x03\xa8\xf0\xad\xec\x61\x02\x81\x81\x00\xd9\x4a\xbd\x67\xb6\xc7\x9c\xc6\xf4\x7f\x55\xd3\x7a\xb5\x86\xe7\x26\x9d\x1e\x5f\xb7\x82\xac\x1e\x87\xce\x14\xc4\xfc\xa3\x09\xf0\x08\x5e\
\x43\x14\xec\x57\x88\x54\x8c\x44\xd7\x7c\x96\x63\x1b\x69\xa2\xe9\xde\x73\x92\x72\x13\x08\x68\xfa\x8e\x6e\xaf\x4d\x01\xb0\x35\xaf\xe0\xbe\xb1\x37\xc4\xc9\x69\xcf\x5f\x60\xe9\xbc\xde\x4d\xb8\xc4\x4e\x1b\x98\x0a\x55\x6c\xa6\x2e\x25\x95\xdf\x7d\xf2\x7c\x38\x8c\
\x49\x9d\xea\x6f\xae\x9a\x0f\x93\xfa\x27\x25\xbf\x5f\x8d\x5b\xbf\xd7\x18\x53\x3f\xbc\x82\xee\x18\xe9\xaa\xbd\x1c\x1d\x05\x02\x81\x81\x00\xc8\x20\xfa\x04\xbe\xed\x2a\xa3\xab\x7a\x89\x7f\x9f\x2a\xdc\x07\x57\x2a\xb1\x6a\xf9\x05\x8c\x15\x64\x25\x77\xe6\xfc\x8d\
\xce\x4e\x4e\x9c\xed\x62\xc8\x3e\x8b\x82\xc4\xf2\xd0\xc3\x6f\x22\x6f\x36\xd6\x24\xec\xaf\xa8\xaf\x54\x6e\x01\x84\x03\x39\x57\xb9\x86\x3a\x24\x97\x05\x2a\xc0\x2e\x0b\x1d\x8f\x26\xde\x7a\x51\xac\xeb\x85\x9e\x50\xb6\xc6\x95\xe4\x7a\xa5\xaf\x19\x0e\xf6\x4f\x3b\
\x22\xd8\xc2\xef\xec\x85\x10\x65\x5b\x38\xc8\xc7\xb5\xaa\x3d\x92\xde\x90\xa6\x22\xf3\x5f\x91\xaf\x15\x6d\xb8\x4b\x87\x7f\xee\x1b\x80\xb9\x02\x81\x81\x00\xc5\x05\x46\xad\xa6\x86\x61\xb9\xe7\xf1\x19\x41\xf2\x8e\x6b\xae\x65\x4d\xab\x3f\xc0\xa5\xe4\x28\x9e\x9c\
\x96\x2a\x72\xa3\xe6\x1b\xd5\x8a\x9a\xaa\x7b\x0d\x77\x18\xaa\x42\xb5\x5c\x4b\x03\xbb\xd4\x51\x9a\x5a\x50\x52\x65\xca\xaa\x22\x7d\xa4\xed\xe9\x57\x1a\x83\xca\x7a\x19\x7e\xee\x33\x11\xe4\x4a\x63\x74\x28\xec\x66\x8a\xee\xcf\xec\x76\xa2\x00\x7e\xc6\x2c\x08\x95\
\xe9\x10\x76\x9f\xbd\x61\x32\xe8\xc8\xcb\x3d\xc9\x5b\xf8\x66\xcd\x96\xd1\x2e\x21\xd2\xa2\xda\xa1\x1c\x87\xfa\xdb\xdc\xb2\x9e\xee\xc9\xd8\x3d\x36\x23\x7d\x02\x81\x81\x00\xc7\x57\x49\x92\x76\x97\x07\xf4\xd5\x36\x8e\x45\x07\xc7\x0a\x39\x69\x92\xc5\xfc\x16\xa5\
\x76\xde\x4b\xcd\xa2\x7b\xfb\x2e\x48\xc3\xe9\x3c\x18\x9c\x5f\x77\x25\x41\x91\x69\x87\x96\x53\x59\x8a\x4b\xa5\xcc\x4b\xd1\xc0\x85\x3b\xe5\x03\x2f\x7a\x82\xa0\xa5\xd4\x54\x8f\xe3\xb9\xaa\xd6\x98\x46\x4c\x20\x2e\x08\x46\xf0\x99\xfc\x53\x30\xd9\x96\xe5\x81\xa2\
\x7e\xa4\x10\x20\x36\xae\x13\x47\x85\x33\x42\x20\xf7\x87\xa7\x48\x3e\x2f\xf6\x96\x2e\x96\xaf\x14\x5f\x0a\xd7\xe1\x52\x67\xec\x55\xe8\x53\x4c\xd2\x36\xda\xaf\xd2\xc7\x49\x02\x81\x81\x00\x9d\x4b\x29\x13\xde\xfa\xfd\xcc\xad\x23\xff\x77\xf1\x85\xd5\x70\x97\x4d\
\x38\x62\xc6\xee\xe5\x08\xe7\xc6\x86\x9d\xb0\x32\x94\x78\xae\x15\xd2\x1f\x54\x47\xfb\xf8\x0b\xfb\xae\xbb\xc9\xf7\xbc\xfd\x57\x3f\x10\x0e\x91\x66\xdd\xcf\x81\x87\xcf\x81\x20\x4a\xed\xed\x6c\x6e\x69\x43\x01\x3c\xae\xbf\x02\xc9\xaa\xda\x7d\x91\xdc\xb7\x4f\x92\
\x3e\x75\xe8\xb5\xb2\xe2\x2c\x53\x25\x49\xc1\x80\xaf\xec\x2e\xd0\xaf\x48\x7a\xed\xbb\x01\x03\x8e\x6c\x9a\x99\x67\x1a\x5b\x4e\x81\x4d\xce\xc5\x3c\xe7\x43\x49\xa0\x05\x38\x1d\x61\x6e\x0f"
};

/*Note:  intentionally we are skipping fips test cases , as going forward there will be no difference 
        as far as key size are concerned and have nothing to do with macrypto logic.
        Mfecryptc tests will take care of FIPS test cases anyways 
*/
         
TEST_GROUP_RUNNER(macrypto_tests_group) {    
    /* negative test cases */
    //TODO
    RUN_TEST_CASE(macrypto_tests, test_macrypto_apis)    

    /*NOTE NOTE: sequence of below two  test cases are important so that it can run faster, and use each other*/
    /*non_fips mode , officer*/
    do {RUN_TEST_CASE(macrypto_tests, test_fresh_agent);} while (0); /* do..while is a Unity workaround */
    /*non_fips to non_fips upgrade, user*/
    do {RUN_TEST_CASE(macrypto_tests, test_upgrade_agent_non_fips_to_non_fips);} while (0); /* do..while is a Unity workaround */
    
    /*unmanaged agent*/
    do {RUN_TEST_CASE(macrypto_tests, test_unmanaged_agent);} while (0); /* do..while is a Unity workaround */        

    /*NOTE : Run this test case as adminstrator/root*/
    ///????NOTE - THIS TEST CASE HAVE TO BE LAST , as we once do in fips mode we can't load RSA again in any other mode */
    /*non_fips_to_fips ,officer */ /*only officer can do that */
   // do {RUN_TEST_CASE(macrypto_tests, test_upgrade_agent_non_fips_to_fips);} while (0); /* do..while is a Unity workaround */
}


TEST_SETUP(macrypto_tests) {
    Unity.TestFile = "ma_crypto_tests.c";
}

TEST_TEAR_DOWN(macrypto_tests) { 
}

TEST(macrypto_tests, test_macrypto_apis) {    
}

TEST(macrypto_tests, test_fresh_agent) { 
    ma_crypto_t *macrypto = NULL ;    
    int i = 0 ;   
    ma_bytebuffer_t *buffer = NULL;
	    
    /* Create the new keystore */
    TEST_ASSERT_EQUAL_INT(1,create_macrypto_object_with_new_keystore(MA_CRYPTO_MODE_NON_FIPS,MA_CRYPTO_ROLE_OFFICER,MA_CRYPTO_AGENT_MANAGED,&macrypto));
    /* import server public key */
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_crypto_import_server_public_key(macrypto, g_server_public_key, sizeof(g_server_public_key) -1));
    /*import server sign key */
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_crypto_import_server_signing_key(macrypto, g_server_sign_key, sizeof(g_server_sign_key) - 1));

    /*Check agent public key hash*/
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_crypto_get_agent_public_key_hash(macrypto,&buffer));
    TEST_ASSERT_NOT_NULL(buffer);    
    TEST_ASSERT_EQUAL_INT(1, 0 != ma_bytebuffer_get_size(buffer));

    /*Check server public key hash*/
    buffer = NULL ;
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_crypto_get_agent_public_key_hash(macrypto,&buffer));
    TEST_ASSERT_NOT_NULL(buffer);    
    TEST_ASSERT_EQUAL_INT(1, 0 != ma_bytebuffer_get_size(buffer));
    
    /*Check the keys */
    for(i = 0 ; i < MAX_KEYSTORE_KEYS ; i++) {
        char file_path[MACRYPTO_TEST_BUFFER_SIZE]= {0};     
        struct stat st = {0};
        MA_MSC_SELECT(_snprintf, snprintf)(file_path, MACRYPTO_TEST_BUFFER_SIZE,"%s%s%s",TEST_KEYSTORE_PATH,MACRYPTO_TEST_PATH_SEPARATOR,g_keystore_keys_name[i]);        
        TEST_ASSERT_EQUAL_INT(0, stat(file_path,&st)); 
    }
    ma_crypto_deinitialize(macrypto);
    ma_crypto_release(macrypto);  
}

TEST(macrypto_tests, test_upgrade_agent_non_fips_to_non_fips) { 
    ma_crypto_t *macrypto = NULL ;    
    int i = 0 ;   
    ma_bytebuffer_t *buffer = NULL;
    /* Create the macrypto object with old keystore creatd in above test case */
    TEST_ASSERT_EQUAL_INT(1,create_macrypto_object_with_old_keystore(MA_CRYPTO_MODE_NON_FIPS,MA_CRYPTO_ROLE_USER,MA_CRYPTO_AGENT_MANAGED,TEST_KEYSTORE_PATH,&macrypto));

    /*Check agent public key hash*/
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_crypto_get_agent_public_key_hash(macrypto,&buffer));
    TEST_ASSERT_NOT_NULL(buffer);    
    TEST_ASSERT_EQUAL_INT(1, 0 != ma_bytebuffer_get_size(buffer));

    /*Check server public key hash*/
    buffer = NULL ;
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_crypto_get_agent_public_key_hash(macrypto,&buffer));
    TEST_ASSERT_NOT_NULL(buffer);    
    TEST_ASSERT_EQUAL_INT(1, 0 != ma_bytebuffer_get_size(buffer));

    /*Check the keys */
    for(i = 0 ; i < MAX_KEYSTORE_KEYS ; i++) {
        char file_path[MACRYPTO_TEST_BUFFER_SIZE]= {0};     
        struct stat st = {0};
        MA_MSC_SELECT(_snprintf, snprintf)(file_path, MACRYPTO_TEST_BUFFER_SIZE,"%s%s%s",TEST_KEYSTORE_PATH,MACRYPTO_TEST_PATH_SEPARATOR,g_keystore_keys_name[i]);        
        TEST_ASSERT_EQUAL_INT(0, stat(file_path,&st)); 
    }
     /* Note: now finally deleting the whole keystore*/
    TEST_ASSERT_EQUAL_INT(1,release_macrypto_object(MA_CRYPTO_MODE_FIPS,MA_CRYPTO_ROLE_OFFICER,MA_CRYPTO_AGENT_MANAGED,macrypto));
}

/* Only in adminstartor or root mode */
TEST(macrypto_tests, test_upgrade_agent_non_fips_to_fips) { 
    ma_crypto_t *macrypto = NULL ;    
    int i = 0 ;   
    ma_bytebuffer_t *buffer = NULL;    

    /* Create the macrypto object with non fips kesystore first */
    TEST_ASSERT_EQUAL_INT(1,create_macrypto_object_with_keys(MA_CRYPTO_MODE_NON_FIPS,MA_CRYPTO_ROLE_OFFICER,MA_CRYPTO_AGENT_MANAGED,&macrypto)); 
    ma_crypto_deinitialize(macrypto);
    ma_crypto_release(macrypto);  

    /* Create the macrypto object in fips mode created above */
    TEST_ASSERT_EQUAL_INT(1,create_macrypto_object_with_old_keystore(MA_CRYPTO_MODE_FIPS,MA_CRYPTO_ROLE_OFFICER,MA_CRYPTO_AGENT_MANAGED,TEST_KEYSTORE_PATH,&macrypto));    
    
     /* import server public key */
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_crypto_import_server_public_key(macrypto, g_server_public_key, sizeof(g_server_public_key) - 1));
    /*import server sign key */
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_crypto_import_server_signing_key(macrypto, g_server_sign_key, sizeof(g_server_sign_key) -1 ));

    /*Check agent public key hash*/
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_crypto_get_agent_public_key_hash(macrypto,&buffer));
    TEST_ASSERT_NOT_NULL(buffer);    
    TEST_ASSERT_EQUAL_INT(1, 0 != ma_bytebuffer_get_size(buffer));

    /*Check server public key hash*/
    buffer = NULL ;
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_crypto_get_server_public_key_hash(macrypto,&buffer));
    TEST_ASSERT_NOT_NULL(buffer);    
    TEST_ASSERT_EQUAL_INT(1, 0 != ma_bytebuffer_get_size(buffer));

    /*Check the keys */
    for(i = 0 ; i < MAX_KEYSTORE_KEYS ; i++) {
        char file_path[MACRYPTO_TEST_BUFFER_SIZE]= {0};     
        struct stat st = {0};
        MA_MSC_SELECT(_snprintf, snprintf)(file_path, MACRYPTO_TEST_BUFFER_SIZE,"%s%s%s",TEST_KEYSTORE_PATH,MACRYPTO_TEST_PATH_SEPARATOR,g_keystore_keys_name[i]);        
        TEST_ASSERT_EQUAL_INT(0, stat(file_path,&st)); 
    }
    TEST_ASSERT_EQUAL_INT(1,release_macrypto_object(MA_CRYPTO_MODE_FIPS,MA_CRYPTO_ROLE_OFFICER,MA_CRYPTO_AGENT_MANAGED,macrypto));   
}

TEST(macrypto_tests, test_unmanaged_agent) {
    ma_crypto_t *macrypto = NULL ;    
    int i = 0 ;   
    ma_bytebuffer_t *buffer = NULL;
    /*Create the keystore in non_fips , agent unmanaged mode */
    TEST_ASSERT_EQUAL_INT(1,create_macrypto_object_with_new_keystore(MA_CRYPTO_MODE_NON_FIPS,MA_CRYPTO_ROLE_USER,MA_CRYPTO_AGENT_UNMANAGED,&macrypto));      

    /* import server public key should fail , as agent is in unmanaged mode*/
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG ,ma_crypto_import_server_public_key(macrypto, g_server_public_key, sizeof(g_server_public_key) -1 ));
    /*import server sign key should fail , as agent is in unmanaged mode*/
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_crypto_import_server_signing_key(macrypto, g_server_sign_key, sizeof(g_server_sign_key) -1 ));
      
    /* get key hash should fail as agent is in unmanaged mode */
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_crypto_get_agent_public_key_hash(macrypto , &buffer));

    /* get key hash should fail as agent is in unmanaged mode */
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_crypto_get_server_public_key_hash(macrypto , &buffer));

    /*Check the keys , there should not be any keys persisting as agent got created in unmanged mode*/
    for(i = 0 ; i < MAX_KEYSTORE_KEYS ; i++) {
        char file_path[MACRYPTO_TEST_BUFFER_SIZE]= {0};     
        struct stat st = {0};
        MA_MSC_SELECT(_snprintf, snprintf)(file_path, MACRYPTO_TEST_BUFFER_SIZE,"%s%s%s",TEST_KEYSTORE_PATH,MACRYPTO_TEST_PATH_SEPARATOR,g_keystore_keys_name[i]);        
        TEST_ASSERT_EQUAL_INT(-1, stat(file_path,&st)); 
    }
    /*Release keystore*/
    TEST_ASSERT_EQUAL_INT(1,release_macrypto_object(MA_CRYPTO_MODE_FIPS,MA_CRYPTO_ROLE_OFFICER,MA_CRYPTO_AGENT_MANAGED,macrypto));    
}


