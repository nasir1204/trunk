#include "unity.h"
#include "unity_fixture.h"

#include "ma/internal/utils/macrypto/ma_crypto.h"
#include <string.h>

#include "ma_crypto_tests_setup.h"

extern ma_crypto_t *test_macrypto;

TEST_GROUP_RUNNER(macrypto_non_fips_enc_dec_test_group) {
    RUN_TEST_CASE(macrypto_enc_dec_tests, test_encrypt_decrypt_3des)    
    do {RUN_TEST_CASE(macrypto_enc_dec_tests, test_encode_decode_3des);} while (0); 
    do {RUN_TEST_CASE(macrypto_enc_dec_tests, test_encrypt_decrypt_aes128);} while (0);
    do {RUN_TEST_CASE(macrypto_enc_dec_tests, test_encode_decode_aes128);} while (0);
    do {RUN_TEST_CASE(macrypto_enc_dec_tests, test_decrypt_policy_sym);} while (0);
    do {RUN_TEST_CASE(macrypto_enc_dec_tests, test_decode_decrypt_password_non_fips);} while (0);
}

TEST_SETUP(macrypto_enc_dec_tests) {
}

TEST_TEAR_DOWN(macrypto_enc_dec_tests) { 
}

#define BYTEBUFFER_SIZE 24  // size to initialize bytebuffer objects

/* data to verify 3des, aes128*/
typedef struct data_s {
    size_t data_len;
    char *data;    
} data_t;

#define MAX_ARRAY    3

data_t data[MAX_ARRAY] =  {
    {5,  "vinod" },
    {24, "Dont try to decrypt this" },
    {32, "!@#$%^&*()_+=-}[{]|\\'\";:,.<>/?~`"},
};

TEST(macrypto_enc_dec_tests, test_encrypt_decrypt_3des) {
    ma_bytebuffer_t *encrypted_data = NULL;
    ma_bytebuffer_t *decrypted_data = NULL;    
    int i =0;

    ma_crypto_encryption_t *enc_obj = NULL;
    ma_crypto_decryption_t *dec_obj = NULL;
    
    for(i = 0; i < MAX_ARRAY; i++) {
        TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_encrypt_create(test_macrypto, MA_CRYPTO_ENCRYPTION_3DES, &enc_obj), "ma_crypto_encrypt_create failed");   
        TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_decrypt_create(test_macrypto, MA_CRYPTO_DECRYPTION_3DES, &dec_obj), "ma_crypto_decrypt_create failed");   
        ma_bytebuffer_create(BYTEBUFFER_SIZE, &encrypted_data);
        ma_bytebuffer_create(BYTEBUFFER_SIZE, &decrypted_data);
        memset(ma_bytebuffer_get_bytes(encrypted_data), 0 , BYTEBUFFER_SIZE);
        memset(ma_bytebuffer_get_bytes(decrypted_data), 0 , BYTEBUFFER_SIZE);
        TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_encrypt(enc_obj, (unsigned char*)data[i].data, data[i].data_len, encrypted_data), "ma_crypto_encrypt failed" );
        TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_decrypt(dec_obj, ma_bytebuffer_get_bytes(encrypted_data), ma_bytebuffer_get_size(encrypted_data), decrypted_data), "ma_crypto_decrypt failed" );
        TEST_ASSERT_EQUAL_INT(0, memcmp(data[i].data, ma_bytebuffer_get_bytes(decrypted_data), ma_bytebuffer_get_size(decrypted_data)));
        ma_bytebuffer_release(decrypted_data); decrypted_data = NULL;
        ma_bytebuffer_release(encrypted_data); encrypted_data = NULL;
        TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_encrypt_release(enc_obj), "ma_crypto_encrypt_release failed");
        TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_decrypt_release(dec_obj), "ma_crypto_decrypt_release failed");
		enc_obj = NULL; dec_obj = NULL;
    }   
}

TEST(macrypto_enc_dec_tests, test_encode_decode_3des) {
    ma_bytebuffer_t *encrypted_data = NULL;
    ma_bytebuffer_t *decrypted_data = NULL;
    int i =0;

    ma_crypto_encryption_t *enc_obj = NULL;
    ma_crypto_decryption_t *dec_obj = NULL;
    
    for(i = 0; i < MAX_ARRAY; i++) {
        TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_encrypt_create(test_macrypto, MA_CRYPTO_ENCRYPTION_3DES, &enc_obj), "ma_crypto_encrypt_create failed");   
        TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_decrypt_create(test_macrypto, MA_CRYPTO_DECRYPTION_3DES, &dec_obj), "ma_crypto_decrypt_create failed"); 
        ma_bytebuffer_create(BYTEBUFFER_SIZE, &encrypted_data);
        ma_bytebuffer_create(BYTEBUFFER_SIZE, &decrypted_data);
        memset(ma_bytebuffer_get_bytes(encrypted_data), 0 , BYTEBUFFER_SIZE);
        memset(ma_bytebuffer_get_bytes(decrypted_data), 0 , BYTEBUFFER_SIZE);
        TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_encrypt_and_encode(enc_obj, (unsigned char*)data[i].data, data[i].data_len, encrypted_data), "ma_crypto_encrypt_and_encode failed" );
        TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_decode_and_decrypt(dec_obj, ma_bytebuffer_get_bytes(encrypted_data), ma_bytebuffer_get_size(encrypted_data), decrypted_data), "ma_crypto_decode_and_decrypt failed" );
        TEST_ASSERT_EQUAL_INT(0, memcmp(data[i].data, ma_bytebuffer_get_bytes(decrypted_data), ma_bytebuffer_get_size(decrypted_data)));
        TEST_ASSERT_EQUAL_INT(0, memcmp(data[i].data, ma_bytebuffer_get_bytes(decrypted_data), data[i].data_len));  // TBD - will compare with decrypt_data size
        ma_bytebuffer_release(decrypted_data); decrypted_data = NULL;
        ma_bytebuffer_release(encrypted_data); encrypted_data = NULL;
        TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_encrypt_release(enc_obj), "ma_crypto_encrypt_release failed");
        TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_decrypt_release(dec_obj), "ma_crypto_decrypt_release failed");
		enc_obj = NULL; dec_obj = NULL;
    }   
}

TEST(macrypto_enc_dec_tests, test_encrypt_decrypt_aes128) {
    ma_bytebuffer_t *encrypted_data = NULL;
    ma_bytebuffer_t *decrypted_data = NULL;
    int i =0;

    ma_crypto_encryption_t *enc_obj = NULL;
    ma_crypto_decryption_t *dec_obj = NULL;   

    for(i = 0; i < MAX_ARRAY; i++) {
        TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_encrypt_create(test_macrypto, MA_CRYPTO_ENCRYPTION_AES128, &enc_obj), "ma_crypto_encrypt_create failed");   
        TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_decrypt_create(test_macrypto, MA_CRYPTO_DECRYPTION_AES128, &dec_obj), "ma_crypto_decrypt_create failed"); 
        ma_bytebuffer_create(BYTEBUFFER_SIZE, &encrypted_data);
        ma_bytebuffer_create(BYTEBUFFER_SIZE, &decrypted_data);
        memset(ma_bytebuffer_get_bytes(encrypted_data), 0 , BYTEBUFFER_SIZE);
        memset(ma_bytebuffer_get_bytes(decrypted_data), 0 , BYTEBUFFER_SIZE);
        TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_encrypt(enc_obj, (unsigned char*)data[i].data, data[i].data_len, encrypted_data), "ma_crypto_encrypt failed" );
        TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_decrypt(dec_obj, ma_bytebuffer_get_bytes(encrypted_data), ma_bytebuffer_get_size(encrypted_data), decrypted_data), "ma_crypto_decrypt failed" );
        TEST_ASSERT_EQUAL_INT(0, memcmp(data[i].data, ma_bytebuffer_get_bytes(decrypted_data), ma_bytebuffer_get_size(decrypted_data)));
        ma_bytebuffer_release(decrypted_data); decrypted_data = NULL;
        ma_bytebuffer_release(encrypted_data); encrypted_data = NULL;
        TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_encrypt_release(enc_obj), "ma_crypto_encrypt_release failed");
        TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_decrypt_release(dec_obj), "ma_crypto_decrypt_release failed");
		enc_obj = NULL; dec_obj = NULL;
    }    
}

TEST(macrypto_enc_dec_tests, test_encode_decode_aes128) {
    ma_bytebuffer_t *encrypted_data = NULL;
    ma_bytebuffer_t *decrypted_data = NULL;    
    int i =0;

    ma_crypto_encryption_t *enc_obj = NULL;
    ma_crypto_decryption_t *dec_obj = NULL;    
  
    for(i = 0; i < MAX_ARRAY; i++) {
        TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_encrypt_create(test_macrypto, MA_CRYPTO_ENCRYPTION_AES128, &enc_obj), "ma_crypto_encrypt_create failed");   
        TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_decrypt_create(test_macrypto, MA_CRYPTO_DECRYPTION_AES128, &dec_obj), "ma_crypto_decrypt_create failed"); 

        ma_bytebuffer_create(BYTEBUFFER_SIZE, &encrypted_data);
        ma_bytebuffer_create(BYTEBUFFER_SIZE, &decrypted_data);
        memset(ma_bytebuffer_get_bytes(encrypted_data), 0 , BYTEBUFFER_SIZE);
        memset(ma_bytebuffer_get_bytes(decrypted_data), 0 , BYTEBUFFER_SIZE);
        TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_encrypt_and_encode(enc_obj, (unsigned char*)data[i].data, data[i].data_len, encrypted_data), "ma_crypto_encrypt_and_encode failed" );
        TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_decode_and_decrypt(dec_obj, ma_bytebuffer_get_bytes(encrypted_data), ma_bytebuffer_get_size(encrypted_data), decrypted_data), "ma_crypto_decode_and_decrypt failed" );
        TEST_ASSERT_EQUAL_INT(0, memcmp(data[i].data, ma_bytebuffer_get_bytes(decrypted_data), ma_bytebuffer_get_size(decrypted_data)));
        TEST_ASSERT_EQUAL_INT(0, memcmp(data[i].data, ma_bytebuffer_get_bytes(decrypted_data), data[i].data_len));  // TBD - will compare with decrypt_data size
        ma_bytebuffer_release(decrypted_data); decrypted_data = NULL;
        ma_bytebuffer_release(encrypted_data); encrypted_data = NULL;
        TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_encrypt_release(enc_obj), "ma_crypto_encrypt_release failed");
        TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_decrypt_release(dec_obj), "ma_crypto_decrypt_release failed");
		enc_obj = NULL; dec_obj = NULL;
    }    
}


/* EPO data to verify the policysym decryption */
typedef struct epo_data_s {
    size_t enc_data_len;
    char *enc_data;
    size_t dec_data_len;
    char *dec_data;
} epo_data_t;

#define MAX_SIZE    4

epo_data_t epo_data[MAX_SIZE] =  {
    { 42, "EPOAES128:152fa60339ac6c0341e79303a4547812",       4,                  "eric"               },
    {138, "EPOAES128:25a7e36dcb5f92050ef7bc5bfef13ac0799895608aa7a1e04855fc7f2a8999ac1bc787dd423a5818fbeec3b99ad6ae09fa7aa87892d344a0b1f94ab908202ef0", 27, "this is a string to encrypt" },
    {170, "EPOAES128:167085f9991079b5402669ff10f174a7637ec0a429f9a1b72d2f81d8bd097cbe54397fa6f5675892b18ff8f87e17adf1c4603eaabb8743a663f1f9442ab9a063b98790b364e4c35de288b10db8cbe51e", 32, "18a41f3e35f69b652c5843aae81d153e" },
    {170, "EPOAES128:7936f516f77f199f8cbf857be1a19bd2d0143f7e8ffa9471e0ee82c7dc63fad80ded307df7ddebe2cfc02e1c3d200a03fccce6ed11da4e8efd11a8327d82edbba8e8f7c4ccb684730ee9ef658edb2f70", 32, "!@#$%^&*()_+=-}[{]|\\'\";:,.<>/?~`" },
};


TEST(macrypto_enc_dec_tests, test_decrypt_policy_sym) {
    ma_bytebuffer_t *decrypted_data = NULL;
    int i =0;

    ma_crypto_decryption_t *dec_obj = NULL;    

    for(i = 0; i < MAX_SIZE; i++) {
        TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_decrypt_create(test_macrypto, MA_CRYPTO_DECRYPTION_POLICY_SYM, &dec_obj), "ma_crypto_decrypt_create failed"); 
        ma_bytebuffer_create(BYTEBUFFER_SIZE, &decrypted_data);        
        memset(ma_bytebuffer_get_bytes(decrypted_data), 0 , BYTEBUFFER_SIZE);
        TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_decrypt(dec_obj, (unsigned char*)epo_data[i].enc_data, epo_data[i].enc_data_len, decrypted_data), "ma_crypto_decrypt failed" );
        TEST_ASSERT_EQUAL_INT(0, memcmp(ma_bytebuffer_get_bytes(decrypted_data), epo_data[i].dec_data, epo_data[i].dec_data_len));
        ma_bytebuffer_release(decrypted_data); decrypted_data = NULL;
        TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_decrypt_release(dec_obj), "ma_crypto_decrypt_release failed");
		dec_obj = NULL;
    }
}


#define ENC_NON_FIPS_PASSWD "S8zsfPFJo9tnY6QNOsVexH9psAU8z0HbZ2OkDTrFXsR/abAFPM9B3Q=="              /* vinod123 */
#define ENC_NON_FIPS_PASSWD_SIZE 56
#define DEC_NON_FIPS_PASSWD "vinod123"
#define DNC_NON_FIPS_PASSWD_SIZE 8

TEST(macrypto_enc_dec_tests, test_decode_decrypt_password_non_fips) {
    ma_bytebuffer_t *decrypted_data = NULL;
    ma_crypto_decryption_t *dec_obj = NULL;     
    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_decrypt_create(test_macrypto, MA_CRYPTO_DECRYPTION_PASSWORD_3DES, &dec_obj), "ma_crypto_decrypt_create failed"); 

    ma_bytebuffer_create(BYTEBUFFER_SIZE, &decrypted_data);        
    memset(ma_bytebuffer_get_bytes(decrypted_data), 0 , BYTEBUFFER_SIZE);
    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_decode_and_decrypt(dec_obj, ENC_NON_FIPS_PASSWD, ENC_NON_FIPS_PASSWD_SIZE, decrypted_data), "ma_crypto_decrypt failed" );
    TEST_ASSERT_EQUAL_INT(0, memcmp(DEC_NON_FIPS_PASSWD, ma_bytebuffer_get_bytes(decrypted_data), ma_bytebuffer_get_size(decrypted_data)));
    TEST_ASSERT_EQUAL_INT(0, memcmp(DEC_NON_FIPS_PASSWD, ma_bytebuffer_get_bytes(decrypted_data), DNC_NON_FIPS_PASSWD_SIZE)); // TBD - will compare with decrypt_data size
    ma_bytebuffer_release(decrypted_data); decrypted_data = NULL;

    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_decrypt_release(dec_obj), "ma_crypto_decrypt_release failed");
	dec_obj = NULL;
}

/*************** FIPS TESTS ******************************/

extern ma_crypto_t *fips_test_macrypto;

TEST_GROUP_RUNNER(macrypto_fips_enc_dec_test_group) {
    RUN_TEST_CASE(macrypto_enc_dec_fips_tests, test_decode_decrypt_password_fips)
}

TEST_SETUP(macrypto_enc_dec_fips_tests) {
}

TEST_TEAR_DOWN(macrypto_enc_dec_fips_tests) { 
}

/* TBD - this data has to be added and enable this tests */
#define ENC_FIPS_PASSWD "UlNBMSxQK0taNXZ2ZCsvVlgrbG9LcmorS1FzZ25vaVJYbmZHanluSi9CWTFPUmY0PYnq5AYJrLlSFOG2k1H17krD7h7/9kYHhBebIc4MECiVWYUfZcPhpY1s2y8QZhRcYOlo+Rkio8A6QPLD631X7JghSLA82NogZ/m8e6wCbgqvikVC+1Gjs7Qp6hDJ1qTnfxfuj4KHZWD2zI66wNP6vABVjueCst1ZL7VjO4xga2Xgu9QhCwxm/AAbT0xzzR9JqPU3qsYfP1dpWF3EGvH3e0YXMuyzMaAHiGXb8NuooQ5HgaIkCFs094scBO1pIVrxFapr4dwuOp7NmTdTuKMR8UghUs9mjZDi8NrR8i36sPFjeoGS/SdtlS8Zw/4NByTKPoTRzLyObAzWljoCvZVK9Rk="
#define ENC_FIPS_PASSWD_SIZE 408
#define DEC_FIPS_PASSWD "pramod123"
#define DNC_FIPS_PASSWD_SIZE 9


TEST(macrypto_enc_dec_fips_tests, test_decode_decrypt_password_fips) {
    ma_bytebuffer_t *decrypted_data = NULL;
    ma_crypto_decryption_t *dec_obj = NULL;     
    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_decrypt_create(fips_test_macrypto, MA_CRYPTO_DECRYPTION_PASSWORD_ASYM, &dec_obj), "ma_crypto_decrypt_create failed");

    ma_bytebuffer_create(BYTEBUFFER_SIZE, &decrypted_data);        
    memset(ma_bytebuffer_get_bytes(decrypted_data), 0 , BYTEBUFFER_SIZE);
    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_decode_and_decrypt(dec_obj, ENC_FIPS_PASSWD, ENC_FIPS_PASSWD_SIZE, decrypted_data), "ma_crypto_decrypt failed" );
    TEST_ASSERT_EQUAL_INT(0, memcmp(ma_bytebuffer_get_bytes(decrypted_data), DEC_FIPS_PASSWD, DNC_FIPS_PASSWD_SIZE));
    ma_bytebuffer_release(decrypted_data); decrypted_data = NULL;

    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_decrypt_release(dec_obj), "ma_crypto_decrypt_release failed");
	dec_obj = NULL;
}

