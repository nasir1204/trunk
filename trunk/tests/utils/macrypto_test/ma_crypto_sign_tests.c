#include "unity.h"
#include "unity_fixture.h"

#include "ma/internal/utils/macrypto/ma_crypto.h"

extern ma_crypto_t *test_macrypto;

unsigned char g_server_non_fips_sign_public_key[] = {
"\x52\x53\x41\x31\x30\x82\x01\x0a\x02\x82\x01\x01\x00\xa9\xde\x61\x87\xb7\xf3\xd0\x82\x53\xfd\xb1\xdd\xe9\xa2\x0c\x5f\xae\x0c\xd2\x77\x33\xc1\x76\xe9\x42\x87\x2e\x10\x82\x69\xc9\xee\x04\x59\xeb\x9e\xe6\xa5\x87\x41\xdf\x14\x45\xde\x1a\xcd\xbc\x03\x5c\x7b\x5d\
\xe2\x06\xee\x0f\xf7\xec\x2d\x56\x7d\x59\xdd\xf2\x7e\xbe\xf5\x56\x7a\xd5\x57\x18\xf7\x78\xaa\x24\x94\xa1\xa6\xc0\x42\xa5\x8a\xed\x69\xb8\x84\x1b\x26\x71\x46\x90\x5c\xd6\x95\xac\x45\x87\xf7\x4d\x00\x75\x7a\xaf\x03\xf3\x82\x5d\xcd\xf4\x20\x36\x2d\xde\x83\x96\
\x0c\xd3\x82\x1e\x68\x51\xa2\x80\x19\xa3\x1d\x74\xf1\x5d\xe4\x9f\xab\x0a\x18\x61\x2f\x71\x6f\x26\xc1\x79\x97\xff\xf8\x20\x1d\x5f\xca\x19\x06\x0a\x34\xc8\x7d\xb3\x0b\x0c\xe6\x08\x6e\x14\xee\x79\x78\x95\xd5\xdf\x7c\x64\xec\x57\xc6\x1f\xad\x94\x88\x04\x9a\xe7\
\xa5\x7e\x84\xa2\x56\x12\x41\xa7\x95\x70\x84\xa9\x2f\xe8\x0f\xf0\x49\xe0\x6e\xe2\x94\x1c\x54\x6d\x7f\x38\x0e\xfe\x49\xd6\x1b\xdc\x77\x64\x44\x48\xf4\xd1\xb3\xcb\x71\x62\xdc\x02\xb2\x16\x86\x8c\xde\x6a\x8d\x03\x84\x9d\x8e\x6a\xbf\x96\xfc\x28\x41\x96\xe9\x12\
\xa9\x8f\xc6\x4d\x2e\xa2\x5f\x58\x5d\x6d\x5a\x78\x9d\x02\x03\x01\x00\x01"
};


TEST_GROUP_RUNNER(macrypto_sign_test_group) {
    /*unsigned char buffer[4096] = {0};
    FILE *fp = fopen("D:\\reqpubkey.bin", "rb");
    fread(buffer,1,4096,fp);
    fclose(fp);*/
    RUN_TEST_CASE(macrypto_sign_tests, test_sign_and_verify_with_agent_key)
    do {RUN_TEST_CASE(macrypto_sign_tests, test_sign_and_verify_update_final_with_agent_key);} while (0);
    do {RUN_TEST_CASE(macrypto_sign_tests, test_sign_and_verify_with_signing_server_key);} while (0); /* do..while is a Unity workaround */
    do {RUN_TEST_CASE(macrypto_sign_tests, test_sign_and_verify_update_final_with_signing_server_key);} while (0); /* do..while is a Unity workaround */
}

/* The below data is signed with agent private key and verifed with agent public key */ 
#define DATA_AGENT "BAD0ihpQx1bXZlTE2qRlOdKXjO18B4bqVG2HedxV1FNrB78y1K1ZE5l+cwrU8prCpRHWvXcZkiRt6NFGb/z3Xm7eG1ADqtJlYlrpaqYp9FJAHdH9rrj27hGz3QuMpp+2taiav72lfw1RgXFGTYw5sA0kVaEBK0t012PGIfsOGBSMewCgzeohMq1rNwa79hQlKvvHM4Q6aXMD+wQ/Kc+p9kku9Jwoj8g0ueoLC+r/9t1ufvWhKcR+qwmWfsBI6rMN4wuq7wIXPlNOl7mi+pLqYV4myueEvAHITJDYh7PLQysa/Y9siiPRHGxL5+B6fsU/st0JSvcM/lnXeLOLm0uYTEgVn9nGYc0Mqo7mnMJFnTYuNOmzlEL0P49gBACs40kIvyOd4LETiQPUOM/6DgNAttDXWrWmTOmDfxLUXn1SaMfIXdi9UKVCljv/4DJbHCY0X+FuMGbnvrMCvtvHUaoTDHz5w6ECdaQvFVQ844gkRnUWJFwz8qljP6G9ogEbBLVDWwvbzpqPljqJ+nllhatv1bs47OyVtAFRfVdu0w=="
#define DATA_AGENT_SIZE  552


/* The below data is signed with signing server key and verifed with signing server public key */ 
#define DATA_SERVER "BAD6LZCm0gA9nALbG4C1Kcsr035/z30S7RwvEvcR+NtAItcnR+Zy6qrBuVMcn21OFnnftKSPuetlRzf4jXnGYh6rsbzD981q0JaNaLX73bW+Dszc/nvKq+rTDEDFc3SB1bY0+SzJNItSWgLJFEPRjadc1aJuTyuHSxJKXS8VliEa+wCg/3hun4P8w5G7HqXQ1KR/nbjT66kD/2JXaGGKudO6sTIv7hxl2oK64hq1MXQ2LVHJ37t3ECro+75plCffUQQe9RHwRJRfsHEoVZ7+4x3K+kdM+vr1d+/G+VGcq38e6otLX282ul4dUy3cLS8OvpknfuOjj5D1uMosQX1QFBt90Bzvzytn9oxG/la5qxURGP95UMfCoa3XBACiJ8f3qTvuFz47ieRONJW7Y7XFjzYDA1FqCsBy2licrofqWHSxDBMJZa0wWqkyg6FymsPMHkgjsnyOD4VDyCC5D6d0wi1oTB7Fdv+lDaJ/cvD8lmOsjKZP4uUm3C0dkDFtMjzyH7T1cTedKLjbHJ0tlu3SU6UoLhxUqJ52pm3KGA=="
#define DATA_SERVER_SIZE  552

#define MA_CRYPTO_ASYM_KEY_LENGTH 2048

TEST_SETUP(macrypto_sign_tests) {
}

TEST_TEAR_DOWN(macrypto_sign_tests) { 
}

TEST(macrypto_sign_tests, test_sign_and_verify_with_agent_key) { 
    ma_bytebuffer_t *signature = NULL;
    ma_bool_t is_sucess = MA_FALSE;

    ma_crypto_sign_t *sign_obj = NULL;
    ma_crypto_verify_t *verify_obj = NULL;

    ma_bytebuffer_create(552, &signature);
       
    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_sign_create(test_macrypto, MA_CRYPTO_SIGN_AGENT_PRIVATE_KEY, &sign_obj), "ma_crypto_sign_create failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_sign_buffer(sign_obj, (unsigned char*)DATA_AGENT, DATA_AGENT_SIZE, signature), "ma_crypto_sign_buffer Failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_sign_release(sign_obj), "ma_crypto_sign_release failed");
        
    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_verify_create(test_macrypto, MA_CRYPTO_VERIFY_AGENT_PUBLIC_KEY, &verify_obj), "ma_crypto_verify_create failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_verify_buffer(verify_obj, (unsigned char*)DATA_AGENT, DATA_AGENT_SIZE, ma_bytebuffer_get_bytes(signature), ma_bytebuffer_get_size(signature), &is_sucess), "ma_crypto_verify_buffer Failed");
    TEST_ASSERT_MESSAGE(MA_TRUE == is_sucess, "ma_crypto_verify_buffer failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_verify_release(verify_obj), "ma_crypto_verify_release failed");

    ma_bytebuffer_release(signature);
}

TEST(macrypto_sign_tests, test_sign_and_verify_update_final_with_agent_key) { 
    ma_error_t rc = MA_OK;
    ma_bytebuffer_t *signature = NULL;
    ma_bool_t is_sucess = MA_FALSE;

    ma_crypto_sign_t *sign_obj = NULL;
    ma_crypto_verify_t *verify_obj = NULL;

    ma_bytebuffer_create(552, &signature);

    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_sign_create(test_macrypto, MA_CRYPTO_SIGN_AGENT_PRIVATE_KEY, &sign_obj), "ma_crypto_sign_create failed");

    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_sign_data_update(sign_obj, (const unsigned char *)"BAD0ihpQx1bXZlTE2qRlOdKXjO18B4bqVG2HedxV1FNrB78y1K1ZE5l+cwrU8prCpRHWvXcZkiRt6NFGb/z3Xm7eG1ADqtJlYlrp", 100), "ma_crypto_sign_data_update Failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_sign_data_update(sign_obj, (const unsigned char *)"aqYp9FJAHdH9rrj27hGz3QuMpp+2taiav72lfw1RgXFGTYw5sA0kVaEBK0t012PGIfsOGBSMewCgzeohMq1rNwa79hQlKvvHM4Q6", 100), "ma_crypto_sign_data_update Failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_sign_data_update(sign_obj, (const unsigned char *)"aXMD+wQ/Kc+p9kku9Jwoj8g0ueoLC+r/9t1ufvWhKcR+qwmWfsBI6rMN4wuq7wIXPlNOl7mi+pLqYV4myueEvAHITJDYh7PLQysa/Y9siiPRHGxL5+B6fsU/st0JSvcM/lnXeLOLm0uYTEgVn9nGYc0Mqo7mnMJFnTYuNOmzlEL0P49gBACs40kIvyOd4LETiQPUOM/6DgNAttDXWrWmTOmDfxLUXn1SaMfIXdi9UKVCljv/4DJbHCY0X+FuMGbnvrMCvtvHUaoTDHz5w6ECdaQvFVQ844gkRnUWJFwz8qljP6G9ogEbBLVDWwvbzpqPljqJ+nllhatv1bs47OyVtAFRfVdu0w==", 352), "ma_crypto_sign_data_update Failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_sign_data_final(sign_obj, signature), "ma_crypto_sign_data_final failed");
    
    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_sign_release(sign_obj), "ma_crypto_sign_release failed");
    

    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_verify_create(test_macrypto, MA_CRYPTO_VERIFY_AGENT_PUBLIC_KEY, &verify_obj), "ma_crypto_verify_create failed");

    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_verify_data_update(verify_obj, (const unsigned char *)"BAD0ihpQx1bXZlTE2qRlOdKXjO18B4bqVG2HedxV1FNrB78y1K1ZE5l+cwrU8prCpRHWvXcZkiRt6NFGb/z3Xm7eG1ADqtJlYlrp", 100), "ma_crypto_verify_data_update Failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_verify_data_update(verify_obj, (const unsigned char *)"aqYp9FJAHdH9rrj27hGz3QuMpp+2taiav72lfw1RgXFGTYw5sA0kVaEBK0t012PGIfsOGBSMewCgzeohMq1rNwa79hQlKvvHM4Q6", 100), "ma_crypto_verify_data_update Failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_verify_data_update(verify_obj, (const unsigned char *)"aXMD+wQ/Kc+p9kku9Jwoj8g0ueoLC+r/9t1ufvWhKcR+qwmWfsBI6rMN4wuq7wIXPlNOl7mi+pLqYV4myueEvAHITJDYh7PLQysa/Y9siiPRHG", 110), "ma_crypto_verify_data_update Failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_verify_data_update(verify_obj, (const unsigned char *)"xL5+B6fsU/st0JSvcM/lnXeLOLm0uYTEgVn9nGYc0Mqo7mnMJFnTYuNOmzlEL0P49gBACs40kIvyOd4LETiQPUOM/6DgNAttDXWrWmTOmDfxLUXn1SaMfIXdi9UKVCljv/4DJbHCY0X+FuMGbnvrMCvtvHUaoTDHz5w6ECdaQvFVQ844gkRnUWJFwz8qljP6G9ogEbBLVDWwvbzpqPljqJ+nllhatv1bs47OyVtAFRfVdu0w==", 242), "ma_crypto_verify_data_update Failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_verify_data_final(verify_obj, ma_bytebuffer_get_bytes(signature), ma_bytebuffer_get_size(signature), &is_sucess), "ma_crypto_verify_data_final Failed" );
    TEST_ASSERT_MESSAGE(MA_TRUE == is_sucess, "ma_crypto_verify_data_final failed");
    
    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_verify_release(verify_obj), "ma_crypto_verify_release failed");

    ma_bytebuffer_release(signature);
}

TEST(macrypto_sign_tests, test_sign_and_verify_with_signing_server_key) {
    ma_bytebuffer_t *signature = NULL;
    ma_bool_t is_sucess = MA_FALSE;

    ma_crypto_sign_t *sign_obj = NULL;
    ma_crypto_verify_t *verify_obj = NULL;

    ma_bytebuffer_create(552, &signature);
    
    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_sign_create(test_macrypto, MA_CRYPTO_SIGN_SERVER_SIGNING_KEY, &sign_obj), "ma_crypto_sign_create failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_sign_buffer(sign_obj, (const unsigned char*)DATA_SERVER, DATA_SERVER_SIZE, signature), "ma_crypto_sign_buffer Failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_sign_release(sign_obj), "ma_crypto_sign_release failed");
    
    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_verify_create_with_key(test_macrypto, g_server_non_fips_sign_public_key, sizeof(g_server_non_fips_sign_public_key)-1, MA_CRYPTO_ASYM_KEY_LENGTH, &verify_obj), "ma_crypto_verify_create_with_key failed");
    TEST_ASSERT_MESSAGE(MA_OK ==  ma_crypto_verify_buffer(verify_obj, (const unsigned char*)DATA_SERVER, DATA_SERVER_SIZE, ma_bytebuffer_get_bytes(signature), ma_bytebuffer_get_size(signature), &is_sucess),"ma_crypto_verify_buffer Failed"); 
    TEST_ASSERT_MESSAGE(MA_TRUE == is_sucess, "ma_crypto_verify_buffer failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_verify_release(verify_obj), "ma_crypto_verify_release failed");    

    ma_bytebuffer_release(signature);
}

TEST(macrypto_sign_tests, test_sign_and_verify_update_final_with_signing_server_key) {
    ma_bytebuffer_t *signature = NULL;
    ma_bool_t is_sucess = MA_FALSE;

    ma_crypto_sign_t *sign_obj = NULL;
    ma_crypto_verify_t *verify_obj = NULL;

    ma_bytebuffer_create(552, &signature);

    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_sign_create(test_macrypto, MA_CRYPTO_SIGN_SERVER_SIGNING_KEY, &sign_obj), "ma_crypto_sign_create failed");

    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_sign_data_update(sign_obj, (const unsigned char *)"BAD6LZCm0gA9nALbG4C1Kcsr035/z30S7RwvEvcR+NtAItcnR+Zy6qrBuVMcn21OFnnftKSPuetlRzf4jXnGYh6rsbzD981q0JaN", 100), "ma_crypto_sign_data_update Failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_sign_data_update(sign_obj, (const unsigned char *)"aLX73bW+Dszc/nvKq+rTDEDFc3SB1bY0+SzJNItSWgLJFEPRjadc1aJuTyuHSxJKXS8VliEa+wCg/3hun4P8w5G7HqXQ1KR/nbjT", 100), "ma_crypto_sign_data_update Failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_sign_data_update(sign_obj, (const unsigned char *)"66kD/2JXaGGKudO6sTIv7hxl2oK64hq1MXQ2LVHJ37t3ECro+75plCffUQQe9RHwRJRfsHEoVZ7+4x3K+kdM+vr1d+/G+VGcq38e6otLX282ul4dUy3cLS8OvpknfuOjj5D1uMosQX1QFBt90Bzvzytn9oxG/la5qxURGP95UMfCoa3XBACiJ8f3qTvuFz47ieRONJW7Y7XFjzYDA1FqCsBy2licrofqWHSxDBMJZa0wWqkyg6FymsPMHkgjsnyOD4VDyCC5D6d0wi1oTB7Fdv+lDaJ/cvD8lmOsjKZP4uUm3C0dkDFtMjzyH7T1cTedKLjbHJ0tlu3SU6UoLhxUqJ52pm3KGA==", 352), "ma_crypto_sign_data_update Failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_sign_data_final(sign_obj, signature), "ma_crypto_sign_data_final failed");
    
    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_sign_release(sign_obj), "ma_crypto_sign_release failed");
    

    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_verify_create_with_key(test_macrypto, g_server_non_fips_sign_public_key, sizeof(g_server_non_fips_sign_public_key)-1, MA_CRYPTO_ASYM_KEY_LENGTH, &verify_obj), "ma_crypto_verify_create_with_key failed");  

    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_verify_data_update(verify_obj, (const unsigned char *)"BAD6LZCm0gA9nALbG4C1Kcsr035/z30S7RwvEvcR+NtAItcnR+Zy6qrBuVMcn21OFnnftKSPuetlRzf4jXnGYh6rsbzD981q0JaN", 100), "ma_crypto_verify_data_update Failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_verify_data_update(verify_obj, (const unsigned char *)"aLX73bW+Dszc/nvKq+rTDEDFc3SB1bY0+SzJNItSWgLJFEPRjadc1aJuTyuHSxJKXS8VliEa+wCg/3hun4P8w5G7HqXQ1KR/nbjT", 100), "ma_crypto_verify_data_update Failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_verify_data_update(verify_obj, (const unsigned char *)"66kD/2JXaGGKudO6sTIv7hxl2oK64hq1MXQ2LVHJ37t3ECro+75plCffUQQe9RHwRJRfsHEoVZ7+4x3K+kdM+vr1d+/G+VGcq38e6otLX282ul", 110), "ma_crypto_verify_data_update Failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_verify_data_update(verify_obj, (const unsigned char *)"4dUy3cLS8OvpknfuOjj5D1uMosQX1QFBt90Bzvzytn9oxG/la5qxURGP95UMfCoa3XBACiJ8f3qTvuFz47ieRONJW7Y7XFjzYDA1FqCsBy2licrofqWHSxDBMJZa0wWqkyg6FymsPMHkgjsnyOD4VDyCC5D6d0wi1oTB7Fdv+lDaJ/cvD8lmOsjKZP4uUm3C0dkDFtMjzyH7T1cTedKLjbHJ0tlu3SU6UoLhxUqJ52pm3KGA==", 242), "ma_crypto_verify_data_update Failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_verify_data_final(verify_obj, ma_bytebuffer_get_bytes(signature), ma_bytebuffer_get_size(signature), &is_sucess), "ma_crypto_verify_data_final Failed" );
    TEST_ASSERT_MESSAGE(MA_TRUE == is_sucess, "ma_crypto_verify_data_final failed");
    
    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_verify_release(verify_obj), "ma_crypto_verify_release failed");

    ma_bytebuffer_release(signature);
}

