#include "unity.h"
#include "unity_fixture.h"

#include <string.h>

#include "ma/internal/utils/macrypto/ma_crypto.h"
#include "ma/internal/utils/datastructures/ma_bytebuffer.h"

extern ma_crypto_t *test_macrypto;

TEST_GROUP_RUNNER(macrypto_hash_test_group) {
    RUN_TEST_CASE(macrypto_hash_tests, test_hash_sha1)
    do {RUN_TEST_CASE(macrypto_hash_tests, test_hash_update_final_sha1);} while (0); /* do..while is a Unity workaround */
    do {RUN_TEST_CASE(macrypto_hash_tests, test_hash_sha256);} while (0); /* do..while is a Unity workaround */
    do {RUN_TEST_CASE(macrypto_hash_tests, test_hash_update_final_sha256);} while (0); /* do..while is a Unity workaround */
    do {RUN_TEST_CASE(macrypto_hash_tests, test_hash_encode_sha1);} while (0); /* do..while is a Unity workaround */
    do {RUN_TEST_CASE(macrypto_hash_tests, test_hash_encode_sha256);} while (0); /* do..while is a Unity workaround */
}

/* Data for has SHA1 and SHA256 verification */
#define DATA "BAD0ihpQx1bXZlTE2qRlOdKXjO18B4bqVG2HedxV1FNrB78y1K1ZE5l+cwrU8prCpRHWvXcZkiRt6NFGb/z3Xm7eG1ADqtJlYlrpaqYp9FJAHdH9rrj27hGz3QuMpp+2taiav72lfw1RgXFGTYw5sA0kVaEBK0t012PGIfsOGBSMewCgzeohMq1rNwa79hQlKvvHM4Q6aXMD+wQ/Kc+p9kku9Jwoj8g0ueoLC+r/9t1ufvWhKcR+qwmWfsBI6rMN4wuq7wIXPlNOl7mi+pLqYV4myueEvAHITJDYh7PLQysa/Y9siiPRHGxL5+B6fsU/st0JSvcM/lnXeLOLm0uYTEgVn9nGYc0Mqo7mnMJFnTYuNOmzlEL0P49gBACs40kIvyOd4LETiQPUOM/6DgNAttDXWrWmTOmDfxLUXn1SaMfIXdi9UKVCljv/4DJbHCY0X+FuMGbnvrMCvtvHUaoTDHz5w6ECdaQvFVQ844gkRnUWJFwz8qljP6G9ogEbBLVDWwvbzpqPljqJ+nllhatv1bs47OyVtAFRfVdu0w=="
#define DATA_SIZE   552
#define HASH_SHA1 "507574bd175b5ee80d07d317a9fe3336261f0acf"
#define HASH_SHA1_HEX_SIZE   40
#define HASH_SHA256 "486f82a31f999d3e627c9899f5ad2aec8b3af213f48ad3f3dac19b8bc3be4e53"
#define HASH_SHA256_HEX_SIZE   64

/* Data for has SHA1 and SHA256 with base64 encode verification */
#define DATA1 "BAD6LZCm0gA9nALbG4C1Kcsr035/z30S7RwvEvcR+NtAItcnR+Zy6qrBuVMcn21OFnnftKSPuetlRzf4jXnGYh6rsbzD981q0JaNaLX73bW+Dszc/nvKq+rTDEDFc3SB1bY0+SzJNItSWgLJFEPRjadc1aJuTyuHSxJKXS8VliEa+wCg/3hun4P8w5G7HqXQ1KR/nbjT66kD/2JXaGGKudO6sTIv7hxl2oK64hq1MXQ2LVHJ37t3ECro+75plCffUQQe9RHwRJRfsHEoVZ7+4x3K+kdM+vr1d+/G+VGcq38e6otLX282ul4dUy3cLS8OvpknfuOjj5D1uMosQX1QFBt90Bzvzytn9oxG/la5qxURGP95UMfCoa3XBACiJ8f3qTvuFz47ieRONJW7Y7XFjzYDA1FqCsBy2licrofqWHSxDBMJZa0wWqkyg6FymsPMHkgjsnyOD4VDyCC5D6d0wi1oTB7Fdv+lDaJ/cvD8lmOsjKZP4uUm3C0dkDFtMjzyH7T1cTedKLjbHJ0tlu3SU6UoLhxUqJ52pm3KGA=="
#define DATA1_SIZE  552
#define HASH_SHA1_ENCODE "JYgSY7zy1g3Nq7TwzKH8gQL5NY8="
#define HASH_SHA1_ENCODE_SIZE 28
#define HASH_SHA256_ENCODE "aVazcyyT60Y4qxxhd3wnF5AsQIaCW2tNxLddVSyTggk="
#define HASH_SHA256_ENCODE_SIZE 44

TEST_SETUP(macrypto_hash_tests) {
       
}

TEST_TEAR_DOWN(macrypto_hash_tests) { 
   
}

static ma_bool_t  hex_encode(const unsigned char *raw,const unsigned int size,unsigned char *hex,unsigned int max_size) {
    if(raw && hex && max_size >= size*2) {
        int i, j;
        const char hex_char[16]={'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'};
        for(i=0,j=0;i<size;i++)
        {
	        char temp=*(raw+i);
	        char first=(temp>>4) & 0x0F;
	        char second=temp & 0x0F;

            *(hex+j)=hex_char[(int)first];j++;
	        *(hex+j)=hex_char[(int)second];j++;
        }
        return MA_TRUE;
    }
    return MA_FALSE;
}


TEST(macrypto_hash_tests, test_hash_sha1) {
    ma_crypto_hash_t *hash_obj = NULL;      
    ma_bytebuffer_t *hash_buffer = NULL; 
    unsigned char hex_hash[HASH_SHA1_HEX_SIZE] = {0};
    ma_bytebuffer_create(HASH_SHA1_HEX_SIZE, &hash_buffer); 

    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_hash_create(test_macrypto, MA_CRYPTO_HASH_SHA1, &hash_obj), "ma_crypto_hash_create with SHA1 failed");

    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_hash_buffer(hash_obj, DATA, DATA_SIZE, hash_buffer), "ma_crypto_hash_buffer with SHA1 failed");
    /* coverting binary format to hex to compare */
    TEST_ASSERT_EQUAL_INT(MA_TRUE, hex_encode(ma_bytebuffer_get_bytes(hash_buffer), ma_bytebuffer_get_size(hash_buffer), hex_hash, HASH_SHA1_HEX_SIZE));

    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_hash_release(hash_obj), "ma_crypto_hash_release with SHA1 failed");

    TEST_ASSERT_EQUAL_INT(0, memcmp(hex_hash, HASH_SHA1, HASH_SHA1_HEX_SIZE));

    ma_bytebuffer_release(hash_buffer);	
}

TEST(macrypto_hash_tests, test_hash_update_final_sha1) {
    ma_crypto_hash_t *hash_obj = NULL;      
    ma_bytebuffer_t *hash_buffer = NULL; 
    unsigned char hex_hash[HASH_SHA1_HEX_SIZE] = {0};
    ma_bytebuffer_create(HASH_SHA1_HEX_SIZE, &hash_buffer); 

    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_hash_create(test_macrypto, MA_CRYPTO_HASH_SHA1, &hash_obj), "ma_crypto_hash_create with SHA1 failed");

    /* DATA divided into four parts */
    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_hash_data_update(hash_obj, (unsigned char *)"BAD0ihpQx1bXZlTE2qRlOdKXjO18B4bqVG2HedxV1FNrB78y1K1ZE5l+cwrU8prCpRHWvXcZkiRt6NFGb/z3Xm7eG1ADqtJlYlrp", 100), "ma_crypto_hash_buffer with SHA1 failed");
    
    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_hash_data_update(hash_obj, (unsigned char *)"aqYp9FJAHdH9rrj27hGz3QuMpp+2taiav72lfw1RgXFGTYw5sA0kVaEBK0t012PGIfsOGBSMewCgzeohMq1rNwa79hQlKvvHM4Q6", 100), "ma_crypto_hash_buffer with SHA1 failed");

    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_hash_data_update(hash_obj, (unsigned char *)"aXMD+wQ/Kc+p9kku9Jwoj8g0ueoLC+r/9t1ufvWhKcR+qwmWfsBI6rMN4wuq7wIXPlNOl7mi+pLqYV4myueEvAHITJDYh7PLQysa/Y9siiPRHG", 110), "ma_crypto_hash_buffer with SHA1 failed");
    
    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_hash_data_update(hash_obj, (unsigned char *)"xL5+B6fsU/st0JSvcM/lnXeLOLm0uYTEgVn9nGYc0Mqo7mnMJFnTYuNOmzlEL0P49gBACs40kIvyOd4LETiQPUOM/6DgNAttDXWrWmTOmDfxLUXn1SaMfIXdi9UKVCljv/4DJbHCY0X+FuMGbnvrMCvtvHUaoTDHz5w6ECdaQvFVQ844gkRnUWJFwz8qljP6G9ogEbBLVDWwvbzpqPljqJ+nllhatv1bs47OyVtAFRfVdu0w==", 242), "ma_crypto_hash_buffer with SHA1 failed");

    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_hash_data_final(hash_obj, hash_buffer), "ma_crypto_hash_data_final Failed");    
    
    /* coverting binary format to hex to compare */

    TEST_ASSERT_EQUAL_INT(MA_TRUE, hex_encode(ma_bytebuffer_get_bytes(hash_buffer), ma_bytebuffer_get_size(hash_buffer), hex_hash, HASH_SHA1_HEX_SIZE));

    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_hash_release(hash_obj), "ma_crypto_hash_release with SHA1 failed");

    TEST_ASSERT_EQUAL_INT(0, memcmp(hex_hash, HASH_SHA1, HASH_SHA1_HEX_SIZE));

    ma_bytebuffer_release(hash_buffer);	
}

TEST(macrypto_hash_tests, test_hash_sha256) {
    ma_crypto_hash_t *hash_obj = NULL;      
    ma_bytebuffer_t *hash_buffer = NULL; 
    unsigned char hex_hash[HASH_SHA256_HEX_SIZE] = {0};
    ma_bytebuffer_create(HASH_SHA256_HEX_SIZE, &hash_buffer); 

    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_hash_create(test_macrypto, MA_CRYPTO_HASH_SHA256, &hash_obj), "ma_crypto_hash_create with SH256 failed");

    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_hash_buffer(hash_obj, (unsigned char *)DATA, DATA_SIZE, hash_buffer), "ma_crypto_hash_buffer with SHA256 failed");
    /* coverting binary format to hex to compare */
    TEST_ASSERT_EQUAL_INT(MA_TRUE, hex_encode(ma_bytebuffer_get_bytes(hash_buffer), ma_bytebuffer_get_size(hash_buffer), hex_hash, HASH_SHA256_HEX_SIZE));

    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_hash_release(hash_obj), "ma_crypto_hash_release with SHA1 failed");

    TEST_ASSERT_EQUAL_INT(0, memcmp(hex_hash, HASH_SHA256, HASH_SHA256_HEX_SIZE));

    ma_bytebuffer_release(hash_buffer);	
}

TEST(macrypto_hash_tests, test_hash_update_final_sha256) {
    ma_crypto_hash_t *hash_obj = NULL;      
    ma_bytebuffer_t *hash_buffer = NULL; 
    unsigned char hex_hash[HASH_SHA256_HEX_SIZE] = {0};
    ma_bytebuffer_create(HASH_SHA256_HEX_SIZE, &hash_buffer); 

    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_hash_create(test_macrypto, MA_CRYPTO_HASH_SHA256, &hash_obj), "ma_crypto_hash_create with SHA1 failed");

    /* DATA divided into four parts */
    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_hash_data_update(hash_obj, (unsigned char *)"BAD0ihpQx1bXZlTE2qRlOdKXjO18B4bqVG2HedxV1FNrB78y1K1ZE5l+cwrU8prCpRHWvXcZkiRt6NFGb/z3Xm7eG1ADqtJlYlrp", 100), "ma_crypto_hash_buffer with SHA1 failed");
    
    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_hash_data_update(hash_obj, (unsigned char *)"aqYp9FJAHdH9rrj27hGz3QuMpp+2taiav72lfw1RgXFGTYw5sA0kVaEBK0t012PGIfsOGBSMewCgzeohMq1rNwa79hQlKvvHM4Q6", 100), "ma_crypto_hash_buffer with SHA1 failed");

    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_hash_data_update(hash_obj, (unsigned char *)"aXMD+wQ/Kc+p9kku9Jwoj8g0ueoLC+r/9t1ufvWhKcR+qwmWfsBI6rMN4wuq7wIXPlNOl7mi+pLqYV4myueEvAHITJDYh7PLQysa/Y9siiPRHG", 110), "ma_crypto_hash_buffer with SHA1 failed");
    
    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_hash_data_update(hash_obj, (unsigned char *)"xL5+B6fsU/st0JSvcM/lnXeLOLm0uYTEgVn9nGYc0Mqo7mnMJFnTYuNOmzlEL0P49gBACs40kIvyOd4LETiQPUOM/6DgNAttDXWrWmTOmDfxLUXn1SaMfIXdi9UKVCljv/4DJbHCY0X+FuMGbnvrMCvtvHUaoTDHz5w6ECdaQvFVQ844gkRnUWJFwz8qljP6G9ogEbBLVDWwvbzpqPljqJ+nllhatv1bs47OyVtAFRfVdu0w==", 242), "ma_crypto_hash_buffer with SHA1 failed");

    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_hash_data_final(hash_obj, hash_buffer), "ma_crypto_hash_data_final Failed");    
    
    /* coverting binary format to hex to compare */

    TEST_ASSERT_EQUAL_INT(MA_TRUE, hex_encode(ma_bytebuffer_get_bytes(hash_buffer), ma_bytebuffer_get_size(hash_buffer), hex_hash, HASH_SHA256_HEX_SIZE));

    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_hash_release(hash_obj), "ma_crypto_hash_release with SHA1 failed");

    TEST_ASSERT_EQUAL_INT(0, memcmp(hex_hash, HASH_SHA256, HASH_SHA256_HEX_SIZE));

    ma_bytebuffer_release(hash_buffer);	
}

TEST(macrypto_hash_tests, test_hash_encode_sha1) {
    ma_crypto_hash_t *hash_obj = NULL;      
    ma_bytebuffer_t *hash_buffer = NULL; 
    ma_bytebuffer_create(HASH_SHA1_ENCODE_SIZE, &hash_buffer); 

    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_hash_create(test_macrypto, MA_CRYPTO_HASH_SHA1, &hash_obj), "ma_crypto_hash_create with SHA1 failed");

    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_hash_and_encode(hash_obj, DATA1, DATA1_SIZE, hash_buffer), "ma_crypto_hash_buffer with SHA1 failed");
  
    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_hash_release(hash_obj), "ma_crypto_hash_release with SHA1 failed");

    TEST_ASSERT_EQUAL_INT(0, memcmp(ma_bytebuffer_get_bytes(hash_buffer), HASH_SHA1_ENCODE, HASH_SHA1_ENCODE_SIZE));

    ma_bytebuffer_release(hash_buffer);	 
}

TEST(macrypto_hash_tests, test_hash_encode_sha256) {

    ma_crypto_hash_t *hash_obj = NULL;      
    ma_bytebuffer_t *hash_buffer = NULL; 
    ma_bytebuffer_create(HASH_SHA256_ENCODE_SIZE, &hash_buffer); 

    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_hash_create(test_macrypto, MA_CRYPTO_HASH_SHA256, &hash_obj), "ma_crypto_hash_create with SHA256 failed");

    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_hash_and_encode(hash_obj, DATA1, DATA1_SIZE, hash_buffer), "ma_crypto_hash_buffer with SHA256 failed");
  
    TEST_ASSERT_MESSAGE(MA_OK == ma_crypto_hash_release(hash_obj), "ma_crypto_hash_release with SHA1 failed");

    TEST_ASSERT_EQUAL_INT(0, memcmp(ma_bytebuffer_get_bytes(hash_buffer), HASH_SHA256_ENCODE, HASH_SHA256_ENCODE_SIZE));

    ma_bytebuffer_release(hash_buffer);	
}

