/*****************************************************************************
Variant library unit tests for the basic data types.
******************************************************************************/
#include "unity.h"
#include "unity_fixture.h"
#include "ma/ma_variant.h"

TEST_GROUP_RUNNER(ma_variant_basic_types_test_group)
{
    do {RUN_TEST_CASE(ma_variant_basic_types_tests, ma_variant_get_version_test)} while (0);
    do {RUN_TEST_CASE(ma_variant_basic_types_tests, ma_variant_create_test)} while (0);
    //do {RUN_TEST_CASE(ma_variant_basic_types_tests, ma_variant_init_deinit_test)} while (0);
    do {RUN_TEST_CASE(ma_variant_basic_types_tests, ma_variant_get_type_test)} while (0);
    do {RUN_TEST_CASE(ma_variant_basic_types_tests, ma_variant_create_and_get_from_uint64_test)} while (0);
    do {RUN_TEST_CASE(ma_variant_basic_types_tests, ma_variant_create_and_get_from_int64_test)} while (0);
    do {RUN_TEST_CASE(ma_variant_basic_types_tests, ma_variant_create_and_get_from_double_test)} while (0);
    do {RUN_TEST_CASE(ma_variant_basic_types_tests, ma_variant_create_and_get_from_uint32_test)} while (0);
    do {RUN_TEST_CASE(ma_variant_basic_types_tests, ma_variant_create_and_get_from_int32_test)} while (0);
    do {RUN_TEST_CASE(ma_variant_basic_types_tests, ma_variant_create_and_get_from_float_test)} while (0);
    do {RUN_TEST_CASE(ma_variant_basic_types_tests, ma_variant_create_and_get_from_uint16_test)} while (0);
    do {RUN_TEST_CASE(ma_variant_basic_types_tests, ma_variant_create_and_get_from_int16_test)} while (0);
    do {RUN_TEST_CASE(ma_variant_basic_types_tests, ma_variant_create_and_get_from_uint8_test)} while (0);
    do {RUN_TEST_CASE(ma_variant_basic_types_tests, ma_variant_create_and_get_from_int8_test)} while (0);

    #ifndef bool
        do {RUN_TEST_CASE(ma_variant_basic_types_tests, ma_variant_create_and_get_from_bool_test_01)} while (0);
    #else
        /* Only execute this test if the compiler and compiler options
           support the boolean data-type */
        do {RUN_TEST_CASE(ma_variant_basic_types_tests, ma_variant_create_and_get_from_bool_test_02)} while (0);
    #endif
    do {RUN_TEST_CASE(ma_variant_basic_types_tests, ma_variant_is_equal_test_01)} while (0);
    do {RUN_TEST_CASE(ma_variant_basic_types_tests, ma_variant_is_equal_test_02)} while (0);
    do {RUN_TEST_CASE(ma_variant_basic_types_tests, ma_variant_is_equal_array_table_test)} while (0);
    do {RUN_TEST_CASE(ma_variant_basic_types_tests, ma_variant_inc_ref_dec_ref_test)} while (0);
}

TEST_SETUP(ma_variant_basic_types_tests)
{
}

TEST_TEAR_DOWN(ma_variant_basic_types_tests)
{

}


/*
Unit tests for ma_variant_get_version macro.
*/
TEST(ma_variant_basic_types_tests, ma_variant_get_version_test)
{
    unsigned int version_info = 0;

    /*Test 1 : Calling ma_variant_get_version macro with NULL address*/
    TEST_ASSERT(ma_variant_get_version(NULL) == MA_ERROR_INVALIDARG);

    /*Test 2 : Calling ma_variant_get_version with valid address for storing the version string*/
    TEST_ASSERT(ma_variant_get_version(&version_info) == MA_OK);

    TEST_ASSERT(0x05020100 == version_info);
}

/*
Unit tests for ma_variant_create and ma_variant_get_type macros.
*/
TEST(ma_variant_basic_types_tests, ma_variant_create_test)
{
    /* 'ma_variant_t' type Pointer for storing the base address of variant object */
    ma_variant_t *variant_ptr = NULL;

    /* This variable should hold MA_VARTYPE_NULL after the ma_variant_get_type
       call. Initial value of this variable is given as MA_VARTYPE_STRING. */
    ma_vartype_t vartype_obj = MA_VARTYPE_STRING;

    /*Test 1 : Calling ma_variant_create macro with NULL address*/

    TEST_ASSERT(ma_variant_create(NULL) == MA_ERROR_INVALIDARG);

    /*Test 2 : Calling ma_variant_create with valid address for storing the variant base address*/

    TEST_ASSERT(ma_variant_create(&variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_type(variant_ptr, &vartype_obj) == MA_OK);
    TEST_ASSERT(vartype_obj == MA_VARTYPE_NULL);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

}

/*
Unit tests for ma_variant_init and ma_variant_deinit macros.

These two functions do not have any meaningful implementation now. Therefore, only
dummy tests are provided here. These tests may be updated later.
*/
TEST(ma_variant_basic_types_tests, ma_variant_init_deinit_test)
{
    /*TEST_ASSERT(ma_variant_init() == MA_OK);
    TEST_ASSERT(ma_variant_deinit() == MA_OK);*/
}

/*
Unit tests for ma_variant_get_type macro.
*/
TEST(ma_variant_basic_types_tests, ma_variant_get_type_test)
{
    /* 'ma_variant_t' type Pointer for storing the base address of variant object */
    ma_variant_t *variant_ptr = NULL;

    /* This variable should hold correct variant type after the ma_variant_get_type
       call. Initial value of this variable is given as MA_VARTYPE_STRING. */
    ma_vartype_t vartype_obj = MA_VARTYPE_STRING;

    /*Test 1 : Calling ma_variant_get_type macro with NULL as address of variant
      variable and a valid address to store the variant type*/

    TEST_ASSERT(ma_variant_get_type(NULL, &vartype_obj) == MA_ERROR_INVALIDARG);

    /*Test 2 : Calling ma_variant_get_type macro with a valid pointer to a
     variant type and a NULL address to store the variant type*/
    TEST_ASSERT(ma_variant_create(&variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_type(variant_ptr,NULL) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /*Test 3 : Calling ma_variant_get_type macro with a valid pointer to a
     variant type and a valid location to store the variant type*/

    /* Testing for NULL Variant type */
    TEST_ASSERT(ma_variant_create(&variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_type(variant_ptr, &vartype_obj) == MA_OK);
    TEST_ASSERT(vartype_obj == MA_VARTYPE_NULL);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* Testing for 64 Bit unsigned integer Variant type */
    TEST_ASSERT(ma_variant_create_from_uint64(0x12345678ABCDEF09ULL, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_type(variant_ptr, &vartype_obj) == MA_OK);
    TEST_ASSERT(vartype_obj == MA_VARTYPE_UINT64);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* Testing for 64 Bit signed integer Variant type */
    TEST_ASSERT(ma_variant_create_from_int64(0x12345678ABCDEF09LL, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_type(variant_ptr, &vartype_obj) == MA_OK);
    TEST_ASSERT(vartype_obj == MA_VARTYPE_INT64);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* Testing for double Variant type */
    TEST_ASSERT(ma_variant_create_from_double(1e+37, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_type(variant_ptr, &vartype_obj) == MA_OK);
    TEST_ASSERT(vartype_obj == MA_VARTYPE_DOUBLE);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* Testing for 32 Bit unsigned integer Variant type */
    TEST_ASSERT(ma_variant_create_from_uint32(0x1234ABCDUL, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_type(variant_ptr, &vartype_obj) == MA_OK);
    TEST_ASSERT(vartype_obj == MA_VARTYPE_UINT32);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* Testing for 32 Bit signed integer Variant type */
    TEST_ASSERT(ma_variant_create_from_int32(0xEFAB0123, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_type(variant_ptr, &vartype_obj) == MA_OK);
    TEST_ASSERT(vartype_obj == MA_VARTYPE_INT32);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);
    /* Testing for float Variant type */
    TEST_ASSERT(ma_variant_create_from_float(123.45f, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_type(variant_ptr, &vartype_obj) == MA_OK);
    TEST_ASSERT(vartype_obj == MA_VARTYPE_FLOAT);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* Testing for 16 Bit unsigned integer Variant type */
    TEST_ASSERT(ma_variant_create_from_uint16(0x1234, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_type(variant_ptr, &vartype_obj) == MA_OK);
    TEST_ASSERT(vartype_obj == MA_VARTYPE_UINT16);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* Testing for 16 Bit signed integer Variant type */
    TEST_ASSERT(ma_variant_create_from_int16(0x7FAB, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_type(variant_ptr, &vartype_obj) == MA_OK);
    TEST_ASSERT(vartype_obj == MA_VARTYPE_INT16);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

     /* Testing for 8 Bit unsigned integer Variant type */
    TEST_ASSERT(ma_variant_create_from_uint8(0x12, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_type(variant_ptr, &vartype_obj) == MA_OK);
    TEST_ASSERT(vartype_obj == MA_VARTYPE_UINT8);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* Testing for 8 Bit signed integer Variant type */
    TEST_ASSERT(ma_variant_create_from_int8(0x7F, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_type(variant_ptr, &vartype_obj) == MA_OK);
    TEST_ASSERT(vartype_obj == MA_VARTYPE_INT8);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* Testing for boolean Variant type */
    TEST_ASSERT(ma_variant_create_from_bool(123, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_type(variant_ptr, &vartype_obj) == MA_OK);
    TEST_ASSERT(vartype_obj == MA_VARTYPE_BOOL);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);
}


/*
Unit tests for ma_variant_create_from_uint64 and ma_variant_get_uint64 macros.
*/
TEST(ma_variant_basic_types_tests, ma_variant_create_and_get_from_uint64_test)
{
    /* 'ma_variant_t' type Pointer for storing the base address of variant object */
    ma_variant_t *variant_ptr = NULL;

    // Loop iterator.
    ma_uint8_t counter = 0;

    /* Variable to hold the 64 bit unsigned integer value from the ma_variant_get_uint64
       macro */
    ma_uint64_t expected_value = 0ULL;

    ma_uint64_t test_vector[] = { 0xFFFFFFFFFFFFFFFFULL, 0x1234567890ABCDEFULL,
                                  0xFEDCBA0987654321ULL, 0x1234567891234567ULL,
                                  0x0101010101010101ULL, 0x1010101010101010ULL,
                                  0x3456789012345678ULL, 0x3453453453453451ULL,
                                  0x0000000000000000ULL, 0x0000000000000001ULL };

    /*Test 1 : Calling ma_variant_create_from_uint64 macro with NULL address*/

    TEST_ASSERT(ma_variant_create_from_uint64(0x12345678ABCDEF09ULL, NULL) == MA_ERROR_INVALIDARG);

    /*Test 2 : Calling ma_variant_create_from_uint64 macro for valid pointer
      and known 64 bit values*/

    for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        TEST_ASSERT(ma_variant_create_from_uint64(test_vector[counter], &variant_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_get_uint64(variant_ptr, &expected_value) == MA_OK);
        TEST_ASSERT(test_vector[counter] == expected_value);

        TEST_ASSERT(ma_variant_get_uint64(NULL, &expected_value) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_variant_get_uint64(variant_ptr, NULL) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);
    }

    /*Test 3 : ma_variant_get_uint64 macro test for any type other than unsigned 64 bit integer*/

    /* ma_variant_get_uint64 called with NULL Variant type */
    TEST_ASSERT(ma_variant_create(&variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_uint64(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_uint64 called with 64 Bit signed integer Variant type */
    TEST_ASSERT(ma_variant_create_from_int64(0x12345678ABCDEF09LL, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_uint64(variant_ptr, &expected_value) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_uint64 called with double Variant type */
    TEST_ASSERT(ma_variant_create_from_double(1e+37, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_uint64(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_uint64 called with 32 Bit unsigned integer Variant type */
    TEST_ASSERT(ma_variant_create_from_uint32(0x1234ABCDUL, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_uint64(variant_ptr, &expected_value) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_uint64 called with 32 Bit signed integer Variant type */
    TEST_ASSERT(ma_variant_create_from_int32(0xEFAB0123, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_uint64(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_uint64 called with float Variant type */
    TEST_ASSERT(ma_variant_create_from_float(123.45f, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_uint64(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_uint64 called with 16 Bit unsigned integer Variant type */
    TEST_ASSERT(ma_variant_create_from_uint16(0x1234, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_uint64(variant_ptr, &expected_value) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /*ma_variant_get_uint64 called with 16 Bit signed integer Variant type */
    TEST_ASSERT(ma_variant_create_from_int16(0x7FAB, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_uint64(variant_ptr, &expected_value) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_uint64 called with 8 Bit unsigned integer Variant type */
    TEST_ASSERT(ma_variant_create_from_uint8(0x12, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_uint64(variant_ptr, &expected_value) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /*ma_variant_get_uint64 called with 8 Bit signed integer Variant type */
    TEST_ASSERT(ma_variant_create_from_int8(0x7F, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_uint64(variant_ptr, &expected_value) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_uint64 called with boolean Variant type */
    TEST_ASSERT(ma_variant_create_from_bool(123, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_uint64(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);
}


/*
Unit tests for ma_variant_create_from_int64 and ma_variant_get_int64 macros.
*/
TEST(ma_variant_basic_types_tests, ma_variant_create_and_get_from_int64_test)
{
    /* 'ma_variant_t' type Pointer for storing the base address of variant object */
    ma_variant_t *variant_ptr = NULL;

    // Loop iterator.
    ma_uint8_t counter = 0;

    /* Variable to hold the 64 bit signed integer value from the ma_variant_get_int64
       macro */
    ma_int64_t expected_value = 0LL;

    ma_int64_t test_vector[] = { 0xFFFFFFFFFFFFFFFFLL, 0x1234567890ABCDEFLL,
                                 0xFEDCBA0987654321LL, 0x1234567891234567LL,
                                 0x0101010101010101LL, 0x1010101010101010LL,
                                 0x3456789012345678LL, 0x3453453453453451LL,
                                 0x0000000000000000LL, 0x0000000000000001LL };

    /*Test 1 : Calling ma_variant_create_from_int64 macro with NULL address*/

    TEST_ASSERT(ma_variant_create_from_int64(0x12345678ABCDEF09LL, NULL) == MA_ERROR_INVALIDARG);

    /*Test 2 : Calling ma_variant_create_from_int64 macro for valid pointer and
      known 64 bit values*/

    for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        TEST_ASSERT(ma_variant_create_from_int64(test_vector[counter], &variant_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_get_int64(variant_ptr, &expected_value) == MA_OK);
        TEST_ASSERT(test_vector[counter] == expected_value);

        TEST_ASSERT(ma_variant_get_int64(NULL, &expected_value) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_variant_get_int64(variant_ptr, NULL) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);
    }

    /*Test 3 : ma_variant_get_int64 macro test for any type other than signed 64 bit integer*/

    /* ma_variant_get_int64 called with NULL Variant type */
    TEST_ASSERT(ma_variant_create(&variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_int64(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_int64 called with 64 Bit unsigned integer Variant type */
    TEST_ASSERT(ma_variant_create_from_uint64(0x12345678ABCDEF09ULL, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_int64(variant_ptr, &expected_value) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_int64 called with double Variant type */
    TEST_ASSERT(ma_variant_create_from_double(1e+37, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_int64(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
     TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_int64 called with 32 Bit unsigned integer Variant type */
    TEST_ASSERT(ma_variant_create_from_uint32(0x1234ABCDUL, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_int64(variant_ptr, &expected_value) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_int64 called with 32 Bit signed integer Variant type */
    TEST_ASSERT(ma_variant_create_from_int32(0xEFAB0123, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_int64(variant_ptr, &expected_value) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_int64 called with float Variant type */
    TEST_ASSERT(ma_variant_create_from_float(123.45f, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_int64(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_int64 called with 16 Bit unsigned integer Variant type */
    TEST_ASSERT(ma_variant_create_from_uint16(0x1234, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_int64(variant_ptr, &expected_value) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_int64 called with 16 Bit signed integer Variant type */
    TEST_ASSERT(ma_variant_create_from_int16(0x7FAB, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_int64(variant_ptr, &expected_value) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_int64 called with 8 Bit unsigned integer Variant type */
    TEST_ASSERT(ma_variant_create_from_uint8(0x12, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_int64(variant_ptr, &expected_value) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_int64 called with 8 Bit signed integer Variant type */
    TEST_ASSERT(ma_variant_create_from_int8(0x7F, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_int64(variant_ptr, &expected_value) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_int64 called with boolean Variant type */
    TEST_ASSERT(ma_variant_create_from_bool(123, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_int64(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);
}


/*
Unit tests for ma_variant_create_from_double and ma_variant_get_double macros.
*/
TEST(ma_variant_basic_types_tests, ma_variant_create_and_get_from_double_test)
{
    /* 'ma_variant_t' type Pointer for storing the base address of variant object */
    ma_variant_t *variant_ptr = NULL;

    // Loop iterator.
    ma_uint8_t counter = 0;

    /* Variable to hold the double value from the ma_variant_get_double macro */
    ma_double_t expected_value = 789.123;

    ma_double_t test_vector[] = { 0.0, 23.456, -123.4567, 1.001e11, -2345.5678, 1e-308, 1e-100, 0.1234, 1e+308 };


    /*Test 1 : Calling ma_variant_create_from_double macro with NULL address*/

    TEST_ASSERT(ma_variant_create_from_double(1.023e-50, NULL) == MA_ERROR_INVALIDARG);

    /*Test 2 : Calling ma_variant_create_from_double macro for valid pointer and
      known double values*/

    for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        TEST_ASSERT(ma_variant_create_from_double(test_vector[counter], &variant_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_get_double(variant_ptr, &expected_value) == MA_OK);
        TEST_ASSERT(ma_variant_get_double(NULL, &expected_value) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_variant_get_double(variant_ptr, NULL) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);
    }

    /*Test 3 : ma_variant_get_double macro test for any type other than double*/

    /* ma_variant_get_double called with NULL Variant type */
    TEST_ASSERT(ma_variant_create(&variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_double(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_double called with 64 Bit unsigned integer Variant type */
    TEST_ASSERT(ma_variant_create_from_uint64(0x12345678ABCDEF09ULL, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_double(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_double called with 64 Bit signed integer Variant type */
    TEST_ASSERT(ma_variant_create_from_int64(0x12345678ABCDEF09LL, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_double(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_double called with 32 Bit unsigned integer Variant type */
    TEST_ASSERT(ma_variant_create_from_uint32(0x1234ABCDUL, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_double(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_double called with 32 Bit signed integer Variant type */
    TEST_ASSERT(ma_variant_create_from_int32(0xEFAB0123, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_double(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_double called with float Variant type */
    TEST_ASSERT(ma_variant_create_from_float(123.45f, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_double(variant_ptr, &expected_value) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_double called with 16 Bit unsigned integer Variant type */
    TEST_ASSERT(ma_variant_create_from_uint16(0x1234, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_double(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_double called with 16 Bit signed integer Variant type */
    TEST_ASSERT(ma_variant_create_from_int16(0x7FAB, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_double(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_double called with 8 Bit unsigned integer Variant type */
    TEST_ASSERT(ma_variant_create_from_uint8(0x12, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_double(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_double called with 8 Bit signed integer Variant type */
    TEST_ASSERT(ma_variant_create_from_int8(0x7F, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_double(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_double called with boolean Variant type */
    TEST_ASSERT(ma_variant_create_from_bool(123, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_double(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);
}


/*
Unit tests for ma_variant_create_from_uint32 and ma_variant_get_uint32 macros.
*/
TEST(ma_variant_basic_types_tests, ma_variant_create_and_get_from_uint32_test)
{
    /* 'ma_variant_t' type Pointer for storing the base address of variant object */
    ma_variant_t *variant_ptr = NULL;

    // Loop iterator.
    ma_uint8_t counter = 0;

    /* Variable to hold the 32 bit unsigned integer value from the ma_variant_get_uint32
       macro */
    ma_uint32_t expected_value = 0ULL;

    ma_uint32_t test_vector[] = { 0xFFFFFFFFUL, 0x12345678UL, 0xFEDCBA09UL, 0x87654321UL,
                                  0x01010101UL, 0x10101010UL, 0x34567890UL, 0x00000000UL,
                                  0x00000001UL  };


    /*Test 1 : Calling ma_variant_create_from_uint32 macro with NULL address*/

    TEST_ASSERT(ma_variant_create_from_uint32(0x12345678UL, NULL) == MA_ERROR_INVALIDARG);

    /*Test 2 : Calling ma_variant_create_from_uint32 macro for valid pointer
      and known 32 bit values*/

    for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        TEST_ASSERT(ma_variant_create_from_uint32(test_vector[counter], &variant_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_get_uint32(variant_ptr, &expected_value) == MA_OK);
        TEST_ASSERT(test_vector[counter] == expected_value);
        TEST_ASSERT(ma_variant_get_uint32(NULL, &expected_value) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_variant_get_uint32(variant_ptr, NULL) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);
    }

    /*Test 3 : ma_variant_get_uint32 macro test for any type other than unsigned 32 bit integer*/

    /* ma_variant_get_uint32 called with NULL Variant type */
    TEST_ASSERT(ma_variant_create(&variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_uint32(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_uint32 called with 64 Bit unsigned integer Variant type */
    TEST_ASSERT(ma_variant_create_from_uint64(0x12345678ABCDEF09ULL, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_uint32(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_uint32 called with 64 Bit signed integer Variant type */
    TEST_ASSERT(ma_variant_create_from_int64(0x12345678ABCDEF09LL, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_uint32(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_uint32 called with double Variant type */
    TEST_ASSERT(ma_variant_create_from_double(1e+37, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_uint32(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_uint32 called with 32 Bit signed integer Variant type */
    TEST_ASSERT(ma_variant_create_from_int32(0xEFAB0123, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_uint32(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_uint32 called with float Variant type */
    TEST_ASSERT(ma_variant_create_from_float(123.45f, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_uint32(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_uint32 called with 16 Bit unsigned integer Variant type */
    TEST_ASSERT(ma_variant_create_from_uint16(0x1234, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_uint32(variant_ptr, &expected_value) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /*ma_variant_get_uint32 called with 16 Bit signed integer Variant type */
    TEST_ASSERT(ma_variant_create_from_int16(0x7FAB, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_uint32(variant_ptr, &expected_value) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_uint32 called with 8 Bit unsigned integer Variant type */
    TEST_ASSERT(ma_variant_create_from_uint8(0x12, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_uint32(variant_ptr, &expected_value) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /*ma_variant_get_uint32 called with 8 Bit signed integer Variant type */
    TEST_ASSERT(ma_variant_create_from_int8(0x7F, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_uint32(variant_ptr, &expected_value) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_uint32 called with boolean Variant type */
    TEST_ASSERT(ma_variant_create_from_bool(123, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_uint32(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);
}


/*
Unit tests for ma_variant_create_from_int32 and ma_variant_get_int32 macros.
*/
TEST(ma_variant_basic_types_tests, ma_variant_create_and_get_from_int32_test)
{
    /* 'ma_variant_t' type Pointer for storing the base address of variant object */
    ma_variant_t *variant_ptr = NULL;

    // Loop iterator.
    ma_uint8_t counter = 0;

    /* Variable to hold the 32 bit signed integer value from the ma_variant_get_int32
       macro */
    ma_int32_t expected_value = 0LL;

    ma_int32_t test_vector[] = { 0xFFFFFFFFL, 0x12345678L, 0xFEDCBA09L, 0x87654321L,
         0x01010101L, 0x10101010L, 0x34567890L, 0x00000000L,
         0x00000001L  };


    /*Test 1 : Calling ma_variant_create_from_int32 macro with NULL address*/

    TEST_ASSERT(ma_variant_create_from_int32(0x12345678L, NULL) == MA_ERROR_INVALIDARG);

    /*Test 2 : Calling ma_variant_create_from_int32 macro for valid pointer
      and known 32 bit values*/

    for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        TEST_ASSERT(ma_variant_create_from_int32(test_vector[counter], &variant_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_get_int32(variant_ptr, &expected_value) == MA_OK);
        TEST_ASSERT(test_vector[counter] == expected_value);

        TEST_ASSERT(ma_variant_get_int32(NULL, &expected_value) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_variant_get_int32(variant_ptr, NULL) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);
    }

    /*Test 3 : ma_variant_get_int32 macro test for any type other than signed 32 bit integer*/

    /* ma_variant_get_int32 called with NULL Variant type */
    TEST_ASSERT(ma_variant_create(&variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_int32(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_int32 called with 64 Bit unsigned integer Variant type */
    TEST_ASSERT(ma_variant_create_from_uint64(0x12345678ABCDEF09ULL, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_int32(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_int32 called with 64 Bit signed integer Variant type */
    TEST_ASSERT(ma_variant_create_from_int64(0x12345678ABCDEF09LL, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_int32(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_int32 called with double Variant type */
    TEST_ASSERT(ma_variant_create_from_double(1e+37, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_int32(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_int32 called with 32 Bit unsigned integer Variant type */
    TEST_ASSERT(ma_variant_create_from_uint32(0xEFAB0123UL, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_int32(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_int32 called with float Variant type */
    TEST_ASSERT(ma_variant_create_from_float(123.45f, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_int32(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_int32 called with 16 Bit unsigned integer Variant type */
    TEST_ASSERT(ma_variant_create_from_uint16(0x1234, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_int32(variant_ptr, &expected_value) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /*ma_variant_get_int32 called with 16 Bit signed integer Variant type */
    TEST_ASSERT(ma_variant_create_from_int16(0x7FAB, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_int32(variant_ptr, &expected_value) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_int32 called with 8 Bit unsigned integer Variant type */
    TEST_ASSERT(ma_variant_create_from_uint8(0x12, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_int32(variant_ptr, &expected_value) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /*ma_variant_get_int32 called with 8 Bit signed integer Variant type */
    TEST_ASSERT(ma_variant_create_from_int8(0x7F, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_int32(variant_ptr, &expected_value) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_int32 called with boolean Variant type */
    TEST_ASSERT(ma_variant_create_from_bool(123, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_int32(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);
}


/*
Unit tests for ma_variant_create_from_float and ma_variant_get_float macros.
*/
TEST(ma_variant_basic_types_tests, ma_variant_create_and_get_from_float_test)
{
    /* 'ma_variant_t' type Pointer for storing the base address of variant object */
    ma_variant_t *variant_ptr = NULL;

    // Loop iterator.
    ma_uint8_t counter = 0;

    /* Variable to hold the float value from the ma_variant_get_float macro */
    ma_float_t expected_value = 12.34f;

    ma_float_t test_vector[] = { 0.0f, 23.456f, -123.45f, 1.001f, 2345.56f, -1e-30f, 1e-10f, 0.12f, 1e+3f };


    /*Test 1 : Calling ma_variant_create_from_float macro with NULL address*/

    TEST_ASSERT(ma_variant_create_from_float(1.023f, NULL) == MA_ERROR_INVALIDARG);

    /*Test 2 : Calling ma_variant_create_from_float macro for valid pointer and
      known float values*/

    for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        TEST_ASSERT(ma_variant_create_from_float(test_vector[counter], &variant_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_get_float(variant_ptr, &expected_value) == MA_OK);
        TEST_ASSERT(test_vector[counter] == expected_value);
        TEST_ASSERT(ma_variant_get_float(NULL, &expected_value) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_variant_get_float(variant_ptr, NULL) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);
    }

    /*Test 3 : ma_variant_get_float macro test for any type other than float*/

    /* ma_variant_get_float called with NULL Variant type */
    TEST_ASSERT(ma_variant_create(&variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_float(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_float called with 64 Bit unsigned integer Variant type */
    TEST_ASSERT(ma_variant_create_from_uint64(0x12345678ABCDEF09ULL, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_float(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_float called with 64 Bit signed integer Variant type */
    TEST_ASSERT(ma_variant_create_from_int64(0x12345678ABCDEF09LL, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_float(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_float called with 32 Bit unsigned integer Variant type */
    TEST_ASSERT(ma_variant_create_from_uint32(0x1234ABCDUL, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_float(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_float called with 32 Bit signed integer Variant type */
    TEST_ASSERT(ma_variant_create_from_int32(0xEFAB0123, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_float(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_float called with double Variant type */
    TEST_ASSERT(ma_variant_create_from_double(1e-37, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_float(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_float called with 16 Bit unsigned integer Variant type */
    TEST_ASSERT(ma_variant_create_from_uint16(0x1234, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_float(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_float called with 16 Bit signed integer Variant type */
    TEST_ASSERT(ma_variant_create_from_int16(0x7FAB, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_float(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_float called with 8 Bit unsigned integer Variant type */
    TEST_ASSERT(ma_variant_create_from_uint8(0x12, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_float(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_float called with 8 Bit signed integer Variant type */
    TEST_ASSERT(ma_variant_create_from_int8(0x7F, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_float(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_float called with boolean Variant type */
    TEST_ASSERT(ma_variant_create_from_bool(123, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_float(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);
}

/*
Unit tests for ma_variant_create_from_uint16 and ma_variant_get_uint16 macros.
*/
TEST(ma_variant_basic_types_tests, ma_variant_create_and_get_from_uint16_test)
{
    /* 'ma_variant_t' type Pointer for storing the base address of variant object */
    ma_variant_t *variant_ptr = NULL;

    // Loop iterator.
    ma_uint8_t counter = 0;

    /* Variable to hold the 16 bit unsigned integer value from the ma_variant_get_uint16
       macro */
    ma_uint16_t expected_value = 0;

    ma_uint16_t test_vector[] = { 0xFFFF, 0x1234, 0xFEDC, 0x8765, 0x0101, 0x1010, 0xCDEF, 0x0000, 0x0001 };


    /*Test 1 : Calling ma_variant_create_from_uint16 macro with NULL address*/

    TEST_ASSERT(ma_variant_create_from_uint16(0x1234, NULL) == MA_ERROR_INVALIDARG);

    /*Test 2 : Calling ma_variant_create_from_uint16 macro for valid pointer and known 16 bit values*/

    for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        TEST_ASSERT(ma_variant_create_from_uint16(test_vector[counter], &variant_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_get_uint16(variant_ptr, &expected_value) == MA_OK);
        TEST_ASSERT(test_vector[counter] == expected_value);

        TEST_ASSERT(ma_variant_get_uint16(NULL, &expected_value) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_variant_get_uint16(variant_ptr, NULL) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);
    }

    /*Test 3 : ma_variant_get_uint16 macro test for any type other than unsigned 16 bit integer*/

    /* ma_variant_get_uint16 called with NULL Variant type */
    TEST_ASSERT(ma_variant_create(&variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_uint16(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_uint16 called with 64 Bit unsigned integer Variant type */
    TEST_ASSERT(ma_variant_create_from_uint64(0x12345678ABCDEF09ULL, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_uint16(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_uint16 called with 64 Bit signed integer Variant type */
    TEST_ASSERT(ma_variant_create_from_int64(0x12345678ABCDEF09LL, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_uint16(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_uint16 called with double Variant type */
    TEST_ASSERT(ma_variant_create_from_double(1e+37, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_uint16(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);
    /* ma_variant_get_uint16 called with 32 Bit unsigned integer Variant type */
    TEST_ASSERT(ma_variant_create_from_uint32(0xEFAB0123UL, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_uint16(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_uint16 called with 32 Bit signed integer Variant type */
    TEST_ASSERT(ma_variant_create_from_int32(0xEFAB0123, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_uint16(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_uint16 called with float Variant type */
    TEST_ASSERT(ma_variant_create_from_float(123.45f, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_uint16(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /*ma_variant_get_uint16 called with 16 Bit signed integer Variant type */
    TEST_ASSERT(ma_variant_create_from_int16(0x7FAB, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_uint16(variant_ptr, &expected_value) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_uint16 called with 8 Bit unsigned integer Variant type */
    TEST_ASSERT(ma_variant_create_from_uint8(0x12, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_uint16(variant_ptr, &expected_value) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /*ma_variant_get_uint16 called with 8 Bit signed integer Variant type */
    TEST_ASSERT(ma_variant_create_from_int8(0x7F, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_uint16(variant_ptr, &expected_value) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_uint16 called with boolean Variant type */
    TEST_ASSERT(ma_variant_create_from_bool(123, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_uint16(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);
}


/*
Unit tests for ma_variant_create_from_int16 and ma_variant_get_int16 macros.
*/
TEST(ma_variant_basic_types_tests, ma_variant_create_and_get_from_int16_test)
{
    /* 'ma_variant_t' type Pointer for storing the base address of variant object */
    ma_variant_t *variant_ptr = NULL;

    // Loop iterator.
    ma_uint8_t counter = 0;

    /* Variable to hold the 16 bit unsigned integer value from the ma_variant_get_int16
       macro */
    ma_int16_t expected_value = 0;

    ma_int16_t test_vector[] = { 0x7FFF, 0x1234, 0x6EDC, 0x1765, 0x0101, 0x1010, 0x7DEF, 0x0000, 0x0001 };


    /*Test 1 : Calling ma_variant_create_from_int16 macro with NULL address*/

    TEST_ASSERT(ma_variant_create_from_int16(0x1234, NULL) == MA_ERROR_INVALIDARG);

    /*Test 2 : Calling ma_variant_create_from_int16 macro for valid pointer and known 16 bit values*/

    for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        TEST_ASSERT(ma_variant_create_from_int16(test_vector[counter], &variant_ptr) == MA_OK);

        TEST_ASSERT(ma_variant_get_int16(variant_ptr, &expected_value) == MA_OK);
        TEST_ASSERT(test_vector[counter] == expected_value);
        TEST_ASSERT(ma_variant_get_int16(NULL, &expected_value) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_variant_get_int16(variant_ptr, NULL) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);
    }

    /*Test 3 : ma_variant_get_int16 macro test for any type other than signed 16 bit integer*/

    /* ma_variant_get_int16 called with NULL Variant type */
    TEST_ASSERT(ma_variant_create(&variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_int16(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_int16 called with 64 Bit unsigned integer Variant type */
    TEST_ASSERT(ma_variant_create_from_uint64(0x12345678ABCDEF09ULL, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_int16(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_int16 called with 64 Bit signed integer Variant type */
    TEST_ASSERT(ma_variant_create_from_int64(0x12345678ABCDEF09LL, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_int16(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_int16 called with double Variant type */
    TEST_ASSERT(ma_variant_create_from_double(1e+37, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_int16(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_int16 called with 32 Bit unsigned integer Variant type */
    TEST_ASSERT(ma_variant_create_from_uint32(0xEFAB0123UL, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_int16(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_int16 called with 32 Bit signed integer Variant type */
    TEST_ASSERT(ma_variant_create_from_int32(0xEFAB0123, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_int16(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_int16 called with float Variant type */
    TEST_ASSERT(ma_variant_create_from_float(123.45f, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_int16(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /*ma_variant_get_int16 called with 16 Bit unsigned integer Variant type */
    TEST_ASSERT(ma_variant_create_from_uint16(0xEFAB, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_int16(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_int16 called with 8 Bit unsigned integer Variant type */
    TEST_ASSERT(ma_variant_create_from_uint8(0x12, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_int16(variant_ptr, &expected_value) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /*ma_variant_get_int16 called with 8 Bit signed integer Variant type */
    TEST_ASSERT(ma_variant_create_from_int8(0x6F, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_int16(variant_ptr, &expected_value) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_int16 called with boolean Variant type */
    TEST_ASSERT(ma_variant_create_from_bool(123, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_int16(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);
}


/*
Unit tests for ma_variant_create_from_uint8 and ma_variant_get_uint8 macros.
*/
TEST(ma_variant_basic_types_tests, ma_variant_create_and_get_from_uint8_test)
{
    /* 'ma_variant_t' type Pointer for storing the base address of variant object */
    ma_variant_t *variant_ptr = NULL;

    // Loop iterator.
    ma_uint8_t counter = 0;

    /* Variable to hold the 8 bit unsigned integer value from the ma_variant_get_uint8
       macro */
    ma_uint8_t expected_value = 0;

    ma_uint8_t test_vector[] = { 0xFFU, 0x12U, 0xFEU, 0x87U, 0x01U, 0x10U, 0xCDU, 0x00U, 0x01U };


    /*Test 1 : Calling ma_variant_create_from_uint8 macro with NULL address*/

    TEST_ASSERT(ma_variant_create_from_uint8(0x12, NULL) == MA_ERROR_INVALIDARG);

    /*Test 2 : Calling ma_variant_create_from_uint8 macro for valid pointer and known 8 bit values*/

    for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        TEST_ASSERT(ma_variant_create_from_uint8(test_vector[counter], &variant_ptr) == MA_OK);

        TEST_ASSERT(ma_variant_get_uint8(variant_ptr, &expected_value) == MA_OK);
        TEST_ASSERT(test_vector[counter] == expected_value);
        TEST_ASSERT(ma_variant_get_uint8(NULL, &expected_value) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_variant_get_uint8(variant_ptr, NULL) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);
    }

    /*Test 3 : ma_variant_get_uint8 macro test for any type other than unsigned 8 bit integer*/

    /* ma_variant_get_uint8 called with NULL Variant type */
    TEST_ASSERT(ma_variant_create(&variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_uint8(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_uint8 called with 64 Bit unsigned integer Variant type */
    TEST_ASSERT(ma_variant_create_from_uint64(0x12345678ABCDEF09ULL, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_uint8(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_uint8 called with 64 Bit signed integer Variant type */
    TEST_ASSERT(ma_variant_create_from_int64(0x12345678ABCDEF09LL, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_uint8(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_uint8 called with double Variant type */
    TEST_ASSERT(ma_variant_create_from_double(1e+37, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_uint8(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_uint8 called with 32 Bit unsigned integer Variant type */
    TEST_ASSERT(ma_variant_create_from_uint32(0xEFAB0123UL, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_uint8(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_uint8 called with 32 Bit signed integer Variant type */
    TEST_ASSERT(ma_variant_create_from_int32(0xEFAB0123, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_uint8(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_uint8 called with float Variant type */
    TEST_ASSERT(ma_variant_create_from_float(123.45f, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_uint8(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /*ma_variant_get_uint8 called with 16 Bit unsigned integer Variant type */
    TEST_ASSERT(ma_variant_create_from_uint16(0xEFAB, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_uint8(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /*ma_variant_get_uint8 called with 16 Bit signed integer Variant type */
    TEST_ASSERT(ma_variant_create_from_int16(0x3FAB, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_uint8(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /*ma_variant_get_uint8 called with 8 Bit signed integer Variant type */
    TEST_ASSERT(ma_variant_create_from_int8(0x7F, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_uint8(variant_ptr, &expected_value) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_uint8 called with boolean Variant type */
    TEST_ASSERT(ma_variant_create_from_bool(123, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_uint8(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);
}


/*
Unit tests for ma_variant_create_from_int8 and ma_variant_get_int8 macros.
*/
TEST(ma_variant_basic_types_tests, ma_variant_create_and_get_from_int8_test)
{
    /* 'ma_variant_t' type Pointer for storing the base address of variant object */
    ma_variant_t *variant_ptr = NULL;

    // Loop iterator.
    ma_uint8_t counter = 0;

    /* Variable to hold the 8 bit signed integer value from the ma_variant_get_int8
       macro */
    ma_int8_t expected_value = 0;

    ma_int8_t test_vector[] = { 0x2F, 0x12, 0x5E, 0x17, 0x01, 0x10, 0x3D, 0x00, 0x01 };


    /*Test 1 : Calling ma_variant_create_from_int8 macro with NULL address*/

    TEST_ASSERT(ma_variant_create_from_int8(0x12, NULL) == MA_ERROR_INVALIDARG);

    /*Test 2 : Calling ma_variant_create_from_int8 macro for valid pointer and known 8 bit values*/

    for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        TEST_ASSERT(ma_variant_create_from_int8(test_vector[counter], &variant_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_get_int8(variant_ptr, &expected_value) == MA_OK);
        TEST_ASSERT(test_vector[counter] == expected_value);
        TEST_ASSERT(ma_variant_get_int8(NULL, &expected_value) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_variant_get_int8(variant_ptr, NULL) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);
    }

    /*Test 3 : ma_variant_get_int8 macro test for any type other than signed 8 bit integer*/

    /* ma_variant_get_int8 called with NULL Variant type */
    TEST_ASSERT(ma_variant_create(&variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_int8(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_int8 called with 64 Bit unsigned integer Variant type */
    TEST_ASSERT(ma_variant_create_from_uint64(0x12345678ABCDEF09ULL, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_int8(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_int8 called with 64 Bit signed integer Variant type */
    TEST_ASSERT(ma_variant_create_from_int64(0x12345678ABCDEF09LL, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_int8(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_int8 called with double Variant type */
    TEST_ASSERT(ma_variant_create_from_double(1e+37, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_int8(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_int8 called with 32 Bit unsigned integer Variant type */
    TEST_ASSERT(ma_variant_create_from_uint32(0xEFAB0123UL, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_int8(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_int8 called with 32 Bit signed integer Variant type */
    TEST_ASSERT(ma_variant_create_from_int32(0xEFAB0123, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_int8(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_int8 called with float Variant type */
    TEST_ASSERT(ma_variant_create_from_float(123.45f, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_int8(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /*ma_variant_get_int8 called with 16 Bit unsigned integer Variant type */
    TEST_ASSERT(ma_variant_create_from_uint16(0xEFAB, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_int8(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /*ma_variant_get_uint8 called with 16 Bit signed integer Variant type */
    TEST_ASSERT(ma_variant_create_from_int16(0x7FAB, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_int8(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_int8 called with 8 Bit unsigned integer Variant type */
    TEST_ASSERT(ma_variant_create_from_uint8(0x12U, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_int8(variant_ptr, &expected_value) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_int8 called with boolean Variant type */
    TEST_ASSERT(ma_variant_create_from_bool(123, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_int8(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);
}


/*
Unit tests for ma_variant_create_from_bool and ma_variant_get_bool macros.

Only execute this test if the 'bool' data-type is not supported, otherwise,
the next test should be executed.
*/
#ifndef bool

TEST(ma_variant_basic_types_tests, ma_variant_create_and_get_from_bool_test_01)
{
    /* 'ma_variant_t' type Pointer for storing the base address of variant object */
    ma_variant_t *variant_ptr = NULL;

    // Loop iterator.
    ma_uint8_t counter = 0;

    /* Variable to hold the 8 bit signed integer value from the ma_variant_get_int8
       macro */
    ma_bool_t expected_value = 0;

    ma_bool_t test_vector[] = { 0xFF, 0x12, 0xFE, 0x87, 0x01, 0x00, 0xCD, 0x00, 0x01 };


    /*Test 1 : Calling ma_variant_create_from_bool macro with NULL address*/

    TEST_ASSERT(ma_variant_create_from_bool(0x12, NULL) == MA_ERROR_INVALIDARG);

    /*Test 2 : Calling ma_variant_create_from_bool macro for valid pointer and known values (both zero and non-zero)*/

    for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        TEST_ASSERT(ma_variant_create_from_bool(test_vector[counter], &variant_ptr) == MA_OK);

        TEST_ASSERT(ma_variant_get_bool(variant_ptr, &expected_value) == MA_OK);

        if(test_vector[counter])
        {
            TEST_ASSERT(1 == expected_value);
        }
        else
        {
            TEST_ASSERT(0 == expected_value);
        }

        TEST_ASSERT(ma_variant_get_bool(NULL, &expected_value) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_variant_get_bool(variant_ptr, NULL) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);
    }

    /*Test 3 : ma_variant_get_bool macro test for any type other than boolean*/

    /* ma_variant_get_bool called with NULL Variant type */
    TEST_ASSERT(ma_variant_create(&variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_bool(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_bool called with 64 Bit unsigned integer Variant type */
    TEST_ASSERT(ma_variant_create_from_uint64(0x12345678ABCDEF09ULL, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_bool(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_bool called with 64 Bit signed integer Variant type */
    TEST_ASSERT(ma_variant_create_from_int64(0x12345678ABCDEF09LL, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_bool(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_bool called with double Variant type */
    TEST_ASSERT(ma_variant_create_from_double(1e+37, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_bool(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_int8 called with 32 Bit unsigned integer Variant type */
    TEST_ASSERT(ma_variant_create_from_uint32(0xEFAB0123UL, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_bool(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_int8 called with 32 Bit signed integer Variant type */
    TEST_ASSERT(ma_variant_create_from_int32(0xEFAB0123, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_bool(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_int8 called with float Variant type */
    TEST_ASSERT(ma_variant_create_from_float(123.45f, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_bool(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /*ma_variant_get_int8 called with 16 Bit unsigned integer Variant type */
    TEST_ASSERT(ma_variant_create_from_uint16(0xEFAB, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_bool(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /*ma_variant_get_uint8 called with 16 Bit signed integer Variant type */
    TEST_ASSERT(ma_variant_create_from_int16(0x7FAB, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_bool(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_int8 called with 8 Bit unsigned integer Variant type */
    TEST_ASSERT(ma_variant_create_from_uint8(0x12U, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_bool(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_int8 called with 8 Bit signed integer Variant type */
    TEST_ASSERT(ma_variant_create_from_int8(0x6B, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_bool(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);
}

#endif

/*
Unit tests for ma_variant_create_from_bool and ma_variant_get_bool macros.

Only execute this test if the 'bool' data-type is supported, otherwise, the
previous test where ma_bool_t is typedeffed to 8 bit integer, will be executed.
*/
#ifdef bool

TEST(ma_variant_basic_types_tests, ma_variant_create_and_get_from_bool_test_02)
{
    /* 'ma_variant_t' type Pointer for storing the base address of variant object */
    ma_variant_t *variant_ptr = NULL;

    // Loop iterator.
    ma_uint8_t counter = 0;

    /* Variable to hold the 8 bit signed integer value from the ma_variant_get_int8
       macro */
    ma_bool_t expected_value = 0;

    ma_bool_t test_vector[] = { MA_TRUE, MA_FALSE, MA_TRUE, MA_FALSE };

    /*Test 1 : Calling ma_variant_create_from_bool macro with NULL address*/

    TEST_ASSERT(ma_variant_create_from_bool(MA_TRUE, NULL) == MA_ERROR_INVALIDARG);

    /*Test 2 : Calling ma_variant_create_from_bool macro for valid pointer and known values (both zero and non-zero)*/

    for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        TEST_ASSERT(ma_variant_create_from_bool(test_vector[counter], &variant_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_get_bool(variant_ptr, &expected_value) == MA_OK);

        if(test_vector[count])
        {
            TEST_ASSERT(1 == expected_value);
        }
        else
        {
            TEST_ASSERT(0 == expected_value);
        }

        TEST_ASSERT(ma_variant_get_bool(NULL, &expected_value) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_variant_get_bool(variant_ptr, NULL) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);
    }

    /*Test 3 : ma_variant_get_bool macro test for any type other than boolean*/

    /* ma_variant_get_bool called with NULL Variant type */
    TEST_ASSERT(ma_variant_create(&variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_bool(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_bool called with 64 Bit unsigned integer Variant type */
    TEST_ASSERT(ma_variant_create_from_uint64(0x12345678ABCDEF09ULL, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_bool(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_bool called with 64 Bit signed integer Variant type */
    TEST_ASSERT(ma_variant_create_from_int64(0x12345678ABCDEF09LL, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_bool(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_bool called with double Variant type */
    TEST_ASSERT(ma_variant_create_from_double(1e+37, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_bool(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_int8 called with 32 Bit unsigned integer Variant type */
    TEST_ASSERT(ma_variant_create_from_uint32(0xEFAB0123UL, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_bool(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_int8 called with 32 Bit signed integer Variant type */
    TEST_ASSERT(ma_variant_create_from_int32(0xEFAB0123, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_bool(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_int8 called with float Variant type */
    TEST_ASSERT(ma_variant_create_from_float(123.45f, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_bool(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /*ma_variant_get_int8 called with 16 Bit unsigned integer Variant type */
    TEST_ASSERT(ma_variant_create_from_uint16(0xEFAB, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_bool(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /*ma_variant_get_uint8 called with 16 Bit signed integer Variant type */
    TEST_ASSERT(ma_variant_create_from_int16(0xEFAB, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_bool(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_int8 called with 8 Bit unsigned integer Variant type */
    TEST_ASSERT(ma_variant_create_from_uint8(0x12U, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_bool(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);

    /* ma_variant_get_int8 called with 8 Bit signed integer Variant type */
    TEST_ASSERT(ma_variant_create_from_int8(0xAB, &variant_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_get_bool(variant_ptr, &expected_value) == MA_ERROR_PRECONDITION);
    TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);
}

#endif


/*
Unit tests for ma_variant_is_equal macro in the following scenarios -

Test 1 : When the NULL address is passed to the ma_variant_is_equal macro as first
variant.
1) Call the ma_variant_is_equal macro passing it the NULL address as first variant,
   a valid second variant and a valid integer point to store the result. Verify
   that the macro returns the MA_ERROR_INVALIDARG.

Test 2 : When the NULL address is passed to the ma_variant_is_equal macro as second
variant.
1) Call the ma_variant_is_equal macro passing it the valid first variant, a NULL
   as second variant and a valid integer point to store the result. Verify that
   the macro returns the MA_ERROR_INVALIDARG.

Test 3 : When the NULL address is passed to the ma_variant_is_equal macro to store
the result of macro.
1) Call the ma_variant_is_equal macro passing it the valid first and second variants
   and a NULL to store the result. Verify that the macro returns the MA_ERROR_INVALIDARG.

Test 4 : Verify that the ma_variant_is_equal macro returns MA_TRUE if the same
variant is passed as first and second variants.
*/

TEST(ma_variant_basic_types_tests, ma_variant_is_equal_test_01)
{
    /* 'ma_variant_t' type Pointers for storing the base address of variant object */
    ma_variant_t *variant_ptr_1 = NULL;
    ma_variant_t *variant_ptr_2 = NULL;

    /* Integer to store the result of equality comparison */
    ma_bool_t result = MA_TRUE;

    /*Test 1 : Verifying functionality of ma_variant_is_equal macro when first variant is NULL*/

    /* ma_variant_is_equal called with NULL as first variant and NULL Variant as second variant*/
    TEST_ASSERT(ma_variant_create(&variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(NULL, variant_ptr_1, &result) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);

    /* ma_variant_is_equal called with NULL as first variant and unsigned 64 bit integer Variant as
       second variant*/
    TEST_ASSERT(ma_variant_create_from_uint64(0x12345678ABCDEF09ULL, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(NULL, variant_ptr_1, &result) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);

    /* ma_variant_is_equal called with NULL as first variant and signed 64 bit integer Variant as
       second variant*/
    TEST_ASSERT(ma_variant_create_from_int64(0x12345678ABCDEF09LL, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(NULL, variant_ptr_1, &result) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);

    /* ma_variant_is_equal called with NULL as first variant and double Variant as second variant*/
    TEST_ASSERT(ma_variant_create_from_double(-1.23e+30, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(NULL, variant_ptr_1, &result) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);

    /* ma_variant_is_equal called with NULL as first variant and unsigned 32 bit integer Variant
       as second variant*/
    TEST_ASSERT(ma_variant_create_from_uint32(0xEFAB0123UL, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(NULL, variant_ptr_1, &result) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);

     /* ma_variant_is_equal called with NULL as first variant and signed 32 bit integer Variant
       as second variant*/
    TEST_ASSERT(ma_variant_create_from_int32(0xEFAB0123, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(NULL, variant_ptr_1, &result) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);

    /* ma_variant_is_equal called with NULL as first variant and float Variant as second variant*/
    TEST_ASSERT(ma_variant_create_from_float(10.23f, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(NULL, variant_ptr_1, &result) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);

    /* ma_variant_is_equal called with NULL as first variant and unsigned 16 bit integer Variant
       as second variant*/
    TEST_ASSERT(ma_variant_create_from_uint16(0xEFAB, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(NULL, variant_ptr_1, &result) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);

    /* ma_variant_is_equal called with NULL as first variant and signed 16 bit integer Variant
       as second variant*/
    TEST_ASSERT(ma_variant_create_from_int16(0x1234, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(NULL, variant_ptr_1, &result) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);

    /* ma_variant_is_equal called with NULL as first variant and unsigned 8 bit integer Variant
       as second variant*/
    TEST_ASSERT(ma_variant_create_from_uint8(0x12U, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(NULL, variant_ptr_1, &result) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);

    /* ma_variant_is_equal called with NULL as first variant and signed 8 bit integer Variant
       as second variant*/
    TEST_ASSERT(ma_variant_create_from_int8(0x3F, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(NULL, variant_ptr_1, &result) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);

    /* ma_variant_is_equal called with NULL as first variant and boolean Variant as second variant*/
    TEST_ASSERT(ma_variant_create_from_bool(123, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(NULL, variant_ptr_1, &result) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);


    /*Test 2 : Verifying functionality of ma_variant_is_equal macro when second variant is NULL*/

    /* ma_variant_is_equal called with NULL Variant as first variant and NULL as second variant*/
    TEST_ASSERT(ma_variant_create(&variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_2, NULL, &result) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* ma_variant_is_equal called with unsigned 64 bit integer Variant as first variant
       and NULL as second variant*/
    TEST_ASSERT(ma_variant_create_from_uint64(0x12345678ABCDEF09ULL, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_2, NULL, &result) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* ma_variant_is_equal called with signed 64 bit integer Variant as first variant
       and NULL as second variant*/
    TEST_ASSERT(ma_variant_create_from_int64(0x12345678ABCDEF09LL, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_2, NULL, &result) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* ma_variant_is_equal called with double Variant as first variant and NULL as second variant*/
    TEST_ASSERT(ma_variant_create_from_double(-1.23e+30, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_2, NULL, &result) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* ma_variant_is_equal called with unsigned 32 bit integer Variant as first variant
       and NULL as second variant*/
    TEST_ASSERT(ma_variant_create_from_uint32(0xEFAB0123UL, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_2, NULL, &result) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

     /* ma_variant_is_equal called with signed 32 bit integer Variant as first variant
       and  NULL as second variant*/
    TEST_ASSERT(ma_variant_create_from_int32(0xEFAB0123, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_2, NULL, &result) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* ma_variant_is_equal called with float Variant as first variant and NULL as second variant*/
    TEST_ASSERT(ma_variant_create_from_float(10.23f, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_2, NULL, &result) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* ma_variant_is_equal called with unsigned 16 bit integer Variant as first variant
       and  NULL as second variant*/
    TEST_ASSERT(ma_variant_create_from_uint16(0xEFAB, &variant_ptr_2) == MA_OK);

    TEST_ASSERT(ma_variant_is_equal(variant_ptr_2, NULL, &result) == MA_ERROR_INVALIDARG);

    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* ma_variant_is_equal called with signed 16 bit integer Variant as first variant
       and NULL as second variant*/
    TEST_ASSERT(ma_variant_create_from_int16(0x1234, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_2, NULL, &result) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* ma_variant_is_equal called with unsigned 8 bit integer Variant as first variant
       and NULL as second variant*/
    TEST_ASSERT(ma_variant_create_from_uint8(0x12U, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_2, NULL, &result) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* ma_variant_is_equal called with signed 8 bit integer Variant as first variant
       and NULL as second variant*/
    TEST_ASSERT(ma_variant_create_from_int8(0x2F, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_2, NULL, &result) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* ma_variant_is_equal called with boolean Variant as first variant and NULL as second variant*/
    TEST_ASSERT(ma_variant_create_from_bool(123, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_2, NULL, &result) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /*Test 3 : Verifying functionality of ma_variant_is_equal macro when first and second variants are valid
      and NULL pointer is passed to store the result*/

    /* ma_variant_is_equal called with NULL Variant as first and second variants and a NULL pointer to
       store result*/
    TEST_ASSERT(ma_variant_create(&variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create(&variant_ptr_2) == MA_OK);

    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, NULL) == MA_ERROR_INVALIDARG);

    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* ma_variant_is_equal called with unsigned 64 bit integer Variants as first and
       second variants and a NULL pointer to store result*/
    TEST_ASSERT(ma_variant_create_from_uint64(0x12345678ABCDEF09ULL, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_uint64(0x12345678ABCDEF09ULL, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, NULL) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* ma_variant_is_equal called with signed 64 bit integer Variants as first and
       second variants and a NULL pointer to store result*/
    TEST_ASSERT(ma_variant_create_from_int64(0x0987654321FEDCBALL, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_int64(0x12345678ABCDEF09LL, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, NULL) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* ma_variant_is_equal called with double Variants as first and second variants and a NULL
       pointer to store result*/
    TEST_ASSERT(ma_variant_create_from_double(1.23e+30, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_double(-1.23e+30, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, NULL) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* ma_variant_is_equal called with unsigned 32 bit integer Variants as first
       and second variants and NULL pointer to store the result*/
    TEST_ASSERT(ma_variant_create_from_uint32(0x12345678UL, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_uint32(0xEFAB0123UL, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, NULL) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* ma_variant_is_equal called with signed 32 bit integer Variants as first
       and second variants and NULL pointer to store the result*/
    TEST_ASSERT(ma_variant_create_from_int32(0xEFAB0123, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_int32(0xEFAB0123, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, NULL) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* ma_variant_is_equal called with float Variants as first and second variants and NULL
       pointer to store the result*/
    TEST_ASSERT(ma_variant_create_from_float(-123.34f, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_float(10.23f, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, NULL) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* ma_variant_is_equal called with unsigned 16 bit integer Variants as first
       and second variants and NULL pointer to store the result*/
    TEST_ASSERT(ma_variant_create_from_uint16(0xEFAC, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_uint16(0xEFAB, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, NULL) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* ma_variant_is_equal called with signed 16 bit integer Variants as first and
       second variants and NULL pointer to store the result*/
    TEST_ASSERT(ma_variant_create_from_int16(0x4321, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_int16(0x1234, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, NULL) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* ma_variant_is_equal called with unsigned 8 bit integer Variants as first
       and second variants and NULL pointer to store the result*/
    TEST_ASSERT(ma_variant_create_from_uint8(0x34U, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_uint8(0x12U, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, NULL) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* ma_variant_is_equal called with signed 8 bit integer Variants as first and
       second variants and NULL pointer to store the result*/
    TEST_ASSERT(ma_variant_create_from_int8(0x1F, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_int8(0x1D, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, NULL) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* ma_variant_is_equal called with boolean Variants as first and second variants
       and NULL pointer to store result*/
    TEST_ASSERT(ma_variant_create_from_bool(100, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_bool(123, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, NULL) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /*Test 4 : Verifying functionality of ma_variant_is_equal macro when same variant is passed as both
      first and second variant argument*/

    /* ma_variant_is_equal called with NULL as first and second variant*/
    TEST_ASSERT(ma_variant_create(&variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_1, &result) == MA_OK);
    TEST_ASSERT(result == MA_TRUE);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);

    /* ma_variant_is_equal called unsigned 64 bit integer Variant as first and second variant*/
    TEST_ASSERT(ma_variant_create_from_uint64(0x12345678ABCDEF09ULL, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_1, &result) == MA_OK);
    TEST_ASSERT(result == MA_TRUE);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);

    /* ma_variant_is_equal called with signed 64 bit integer Variant as first and second variant*/
    TEST_ASSERT(ma_variant_create_from_int64(0x12345678ABCDEF09LL, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_1, &result) == MA_OK);
    TEST_ASSERT(result == MA_TRUE);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);

    /* ma_variant_is_equal called with double Variant as first and second variant*/
    TEST_ASSERT(ma_variant_create_from_double(-1.23e+30, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_1, &result) == MA_OK);
    TEST_ASSERT(result == MA_TRUE);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);

    /* ma_variant_is_equal called with unsigned 32 bit integer Variant as first and second variant*/
    TEST_ASSERT(ma_variant_create_from_uint32(0xEFAB0123UL, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_1, &result) == MA_OK);
    TEST_ASSERT(result == MA_TRUE);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);

    /* ma_variant_is_equal called with signed 32 bit integer Variant as first and second variant*/
    TEST_ASSERT(ma_variant_create_from_int32(0xEFAB0123, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_1, &result) == MA_OK);
    TEST_ASSERT(result == MA_TRUE);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);

    /* ma_variant_is_equal called with float Variant as first and second variant*/
    TEST_ASSERT(ma_variant_create_from_float(10.23f, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_1, &result) == MA_OK);
    TEST_ASSERT(result == MA_TRUE);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);

    /* ma_variant_is_equal called with unsigned 16 bit integer Variant as first and second variant*/
    TEST_ASSERT(ma_variant_create_from_uint16(0xEFAB, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_1, &result) == MA_OK);
    TEST_ASSERT(result == MA_TRUE);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);

    /* ma_variant_is_equal called with signed 16 bit integer Variant as first and second variant*/
    TEST_ASSERT(ma_variant_create_from_int16(0x1234, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_1, &result) == MA_OK);
    TEST_ASSERT(result == MA_TRUE);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);

    /* ma_variant_is_equal called with unsigned 8 bit integer Variant as first and second variant*/
    TEST_ASSERT(ma_variant_create_from_uint8(0x12U, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_1, &result) == MA_OK);
    TEST_ASSERT(result == MA_TRUE);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);

    /* ma_variant_is_equal called signed 8 bit integer Variant as first and second variant*/
    TEST_ASSERT(ma_variant_create_from_int8(0x2F, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_1, &result) == MA_OK);
    TEST_ASSERT(result == MA_TRUE);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);

    /* ma_variant_is_equal called with boolean Variant as first and second variant*/
    TEST_ASSERT(ma_variant_create_from_bool(123, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_1, &result) == MA_OK);
    TEST_ASSERT(result == MA_TRUE);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
}


/*
Unit tests for ma_variant_is_equal macro in the following scenarios -

Scenario Data Type        Value                 Data Type        Value                    Return Value    Comparison
No.      (1st Variant)    (1st Variant)         (2nd Variant)    (2nd Variant)    Result

1         NULL               -                   NULL              -                       MA_OK           MA_TRUE
2         NULL               -                   Bool              1                       MA_OK           MA_FALSE
3         uint8            0xFF                  uint8            0xFF                     MA_OK           MA_TRUE
4         uint8            0xFF                  uint8            0xFE                     MA_OK           MA_FALSE
5         uint8            0xFF                  int8             0xFF                     MA_OK           MA_FALSE
6         uint8            0xFF                  uint16           0x00FF                   MA_OK           MA_FALSE
7         uint8            0x34                  int16            0x1234                   MA_OK           MA_FALSE
8         uint8            0x12                  uint16           0xAB12                   MA_OK           MA_FALSE
9         int8             0x12                  uint8            0x12                     MA_OK           MA_FALSE
10        int8             0x55                  int8             0x55                     MA_OK           MA_TRUE
11        int8             0xFF                  uint16           0x00FF                   MA_OK           MA_FALSE
12        int8             0x77                  int8             0x77                     MA_OK           MA_TRUE
13        int8             0xCD                  int16            0xABCD                   MA_OK           MA_FALSE
14        uint16           0x1234                uint16           0x1234                   MA_OK           MA_TRUE
15        uint16           0x1234                int16            0x1234                   MA_OK           MA_FALSE
16        uint16           0x0000                uint16           0x0001                   MA_OK           MA_FALSE
17        uint16           0x7777                uint32           0x00007777               MA_OK           MA_FALSE
18        uint16           0xABCD                int16            0xABCD                   MA_OK           MA_FALSE
19        int16            0x1234                uint16           0x1234                   MA_OK           MA_FALSE
20        int16            0x7894                int16            0x7894                   MA_OK           MA_TRUE
21        int16            0x0000                uint16           0x8000                   MA_OK           MA_FALSE
22        int32            0x77771234            uint32           0x77771234               MA_OK           MA_FALSE
23        int32            0x77771234            int32            0x77771234               MA_OK           MA_TRUE
24        uint32           0xABCDEF01            uint32           0xABCDEF00               MA_OK           MA_FALSE
25        uint32           0xABCDEF01            uint32           0xABCDEF01               MA_OK           MA_TRUE
26        uint32           0xABCDEF01            int32            0xABCDEF01               MA_OK           MA_FALSE
27        uint64           0xABCDEF0123456789    uint64           0xABCDEF0023456789       MA_OK           MA_FALSE
28        uint64           0xABCDEF0123456789    uint64           0xABCDEF0123456789       MA_OK           MA_TRUE
29        uint64           0xABCDEF0123456789    int64            0xABCDEF0123456789       MA_OK           MA_FALSE
30        int64            0xABCDEF0123456789    int64            0xABCDEF0023456789       MA_OK           MA_FALSE
31        int64            0xABCDEF0123456789    int64            0xABCDEF0123456789       MA_OK           MA_TRUE
32        int64            0xABCDEF0123456789    uint64           0xABCDEF0123456789       MA_OK           MA_FALSE
33        float            1234.23f              float            1234.2f                  MA_OK           MA_FALSE
34        float            10.00f                float            9.99f                    MA_OK           MA_FALSE
35        float            1234.23f              double           1234.23                  MA_OK           MA_FALSE
36        float            1234.23f              float            1234.23f                 MA_OK           MA_TRUE
37        float            100.0f                int8             100                      MA_OK           MA_FALSE
38        double           150.01                int8             150                      MA_OK           MA_FALSE
39        double           1e+37                 double           1e-37                    MA_OK           MA_FALSE
40        double           .001                  double           1e-3                     MA_OK           MA_TRUE

Test 2 : Verify that the ma_variant_is_equal macro returns MA_TRUE if the same
variant is passed as first and second variants.
*/
TEST(ma_variant_basic_types_tests, ma_variant_is_equal_test_02)
{
    /* 'ma_variant_t' type Pointers for storing the base address of variant object */
    ma_variant_t *variant_ptr_1 = NULL;
    ma_variant_t *variant_ptr_2 = NULL;

    /* Integer to store the result of equality comparison */
    ma_bool_t result_val = MA_FALSE;

    /*Test 1 : Verifying functionality of ma_variant_is_equal macro */

    /* 1 */
    TEST_ASSERT(ma_variant_create(&variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create(&variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, &result_val) == MA_OK);
    TEST_ASSERT(MA_TRUE == result_val);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* 2 */
    TEST_ASSERT(ma_variant_create(&variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_bool(1, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, &result_val) == MA_OK);
    TEST_ASSERT(MA_FALSE == result_val);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* 3 */
    TEST_ASSERT(ma_variant_create_from_uint8(0xFFU, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_uint8(0xFFU, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, &result_val) == MA_OK);
    TEST_ASSERT(MA_TRUE == result_val);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* 4 */
    TEST_ASSERT(ma_variant_create_from_uint8(0xFFU, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_uint8(0xFEU, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, &result_val) == MA_OK);
    TEST_ASSERT(MA_FALSE == result_val);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* 5 */
    TEST_ASSERT(ma_variant_create_from_uint8(0xFFU, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_int8(0x6F, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, &result_val) == MA_OK);
    TEST_ASSERT(MA_FALSE == result_val);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* 6 */
    TEST_ASSERT(ma_variant_create_from_uint8(0xFFU, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_uint16(0x00FF, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, &result_val) == MA_OK);
    TEST_ASSERT(MA_TRUE == result_val);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* 7 */
    TEST_ASSERT(ma_variant_create_from_uint8(0x34U, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_int16(0x1234, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, &result_val) == MA_OK);
    TEST_ASSERT(MA_FALSE == result_val);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* 8 */
    TEST_ASSERT(ma_variant_create_from_uint8(0x12U, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_uint16(0xAB12, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, &result_val) == MA_OK);
    TEST_ASSERT(MA_FALSE == result_val);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* 9 */
    TEST_ASSERT(ma_variant_create_from_int8(0x12, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_uint8(0x12U, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, &result_val) == MA_OK);
    TEST_ASSERT(MA_TRUE == result_val);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* 10 */
    TEST_ASSERT(ma_variant_create_from_int8(0x55, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_int8(0x55, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, &result_val) == MA_OK);
    TEST_ASSERT(MA_TRUE == result_val);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* 11 */
    TEST_ASSERT(ma_variant_create_from_int8(0x55, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_uint16(0x00FF, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, &result_val) == MA_OK);
    TEST_ASSERT(MA_FALSE == result_val);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* 12 */
    TEST_ASSERT(ma_variant_create_from_int8(0x77, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_int8(0x77, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, &result_val) == MA_OK);
    TEST_ASSERT(MA_TRUE == result_val);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* 13 */
    TEST_ASSERT(ma_variant_create_from_int8(0x7D, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_int16(0x7B7D, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, &result_val) == MA_OK);
    TEST_ASSERT(MA_FALSE == result_val);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* 14 */
    TEST_ASSERT(ma_variant_create_from_uint16(0x1234, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_uint16(0x1234, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, &result_val) == MA_OK);
    TEST_ASSERT(MA_TRUE == result_val);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* 15 */
    TEST_ASSERT(ma_variant_create_from_uint16(0x1234, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_int16(0x1234, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, &result_val) == MA_OK);
    TEST_ASSERT(MA_TRUE == result_val);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* 16 */
    TEST_ASSERT(ma_variant_create_from_uint16(0x0000, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_uint16(0x0001, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, &result_val) == MA_OK);
    TEST_ASSERT(MA_FALSE == result_val);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* 17 */
    TEST_ASSERT(ma_variant_create_from_uint16(0x7777, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_uint32(0x00007777UL, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, &result_val) == MA_OK);
    TEST_ASSERT(MA_TRUE == result_val);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* 18 */
    TEST_ASSERT(ma_variant_create_from_uint16(0x7B8D, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_int16(0x7B8D, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, &result_val) == MA_OK);
    TEST_ASSERT(MA_TRUE == result_val);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* 19 */
    TEST_ASSERT(ma_variant_create_from_int16(0x1234, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_uint16(0x1234, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, &result_val) == MA_OK);
    TEST_ASSERT(MA_TRUE == result_val);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* 20 */
    TEST_ASSERT(ma_variant_create_from_int16(0x7894, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_int16(0x7894, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, &result_val) == MA_OK);
    TEST_ASSERT(MA_TRUE == result_val);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* 21 */
    TEST_ASSERT(ma_variant_create_from_int16(0x0000, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_uint16(0x8000, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, &result_val) == MA_OK);
    TEST_ASSERT(MA_FALSE == result_val);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* 22 */
    TEST_ASSERT(ma_variant_create_from_int32(0x77771234L, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_uint32(0x77771234UL, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, &result_val) == MA_OK);
    TEST_ASSERT(MA_TRUE == result_val);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* 23 */
    TEST_ASSERT(ma_variant_create_from_int32(0x77771234L, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_int32(0x77771234L, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, &result_val) == MA_OK);
    TEST_ASSERT(MA_TRUE == result_val);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* 24 */
    TEST_ASSERT(ma_variant_create_from_uint32(0xABCDEF01UL, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_uint32(0xABCDEF00UL, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, &result_val) == MA_OK);
    TEST_ASSERT(MA_FALSE == result_val);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* 25 */
    TEST_ASSERT(ma_variant_create_from_uint32(0xABCDEF01UL, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_uint32(0xABCDEF01UL, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, &result_val) == MA_OK);
    TEST_ASSERT(MA_TRUE == result_val);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* 26 */
    TEST_ASSERT(ma_variant_create_from_uint32(0xABCDEF01UL, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_int32(0xABCDEF01L, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, &result_val) == MA_OK);
    TEST_ASSERT(MA_FALSE == result_val);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* 27 */
    TEST_ASSERT(ma_variant_create_from_uint64(0xABCDEF0123456789ULL, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_uint64(0xABCDEF0023456789ULL, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, &result_val) == MA_OK);
    TEST_ASSERT(MA_FALSE == result_val);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* 28 */
    TEST_ASSERT(ma_variant_create_from_uint64(0xABCDEF0123456789ULL, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_uint64(0xABCDEF0123456789ULL, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, &result_val) == MA_OK);
    TEST_ASSERT(MA_TRUE == result_val);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* 29 */
    TEST_ASSERT(ma_variant_create_from_uint64(0xABCDEF0123456789ULL, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_int64(0xABCDEF0123456789LL, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, &result_val) == MA_OK);
    TEST_ASSERT(MA_TRUE == result_val);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* 30 */
    TEST_ASSERT(ma_variant_create_from_int64(0xABCDEF0123456789LL, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_int64(0xABCDEF0023456789LL, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, &result_val) == MA_OK);
    TEST_ASSERT(MA_FALSE == result_val);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* 31 */
    TEST_ASSERT(ma_variant_create_from_int64(0xABCDEF0123456789LL, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_int64(0xABCDEF0123456789LL, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, &result_val) == MA_OK);
    TEST_ASSERT(MA_TRUE == result_val);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* 32 */
    TEST_ASSERT(ma_variant_create_from_int64(0xABCDEF0123456789LL, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_uint64(0xABCDEF0123456789ULL, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, &result_val) == MA_OK);
    TEST_ASSERT(MA_TRUE == result_val);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* 33 */
    TEST_ASSERT(ma_variant_create_from_float(1234.23f, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_float(1234.2f, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, &result_val) == MA_OK);
    TEST_ASSERT(MA_FALSE == result_val);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* 34 */
    TEST_ASSERT(ma_variant_create_from_float(10.00f, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_float(9.99f, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, &result_val) == MA_OK);
    TEST_ASSERT(MA_FALSE == result_val);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* 35 */
    TEST_ASSERT(ma_variant_create_from_float(1234.23f, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_double(1234.23, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, &result_val) == MA_OK);
    TEST_ASSERT(MA_FALSE == result_val);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* 36 */
    TEST_ASSERT(ma_variant_create_from_float(1234.23f, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_float(1234.23f, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, &result_val) == MA_OK);
    TEST_ASSERT(MA_TRUE == result_val);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* 36.1 */
    TEST_ASSERT_EQUAL_FLOAT(0.0f, -0.0f);
    TEST_ASSERT(ma_variant_create_from_float(0.0f, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_float(-0.0f, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, &result_val) == MA_OK);
    TEST_ASSERT(MA_TRUE == result_val);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* 37 */
    TEST_ASSERT(ma_variant_create_from_float(100.0f, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_int8(100, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, &result_val) == MA_OK);
    TEST_ASSERT(MA_FALSE == result_val);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* 38 */
    TEST_ASSERT(ma_variant_create_from_double(127.01, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_int8(127, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, &result_val) == MA_OK);
    TEST_ASSERT(MA_FALSE == result_val);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* 39 */
    TEST_ASSERT(ma_variant_create_from_double(1e+37, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_double(1e-37, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, &result_val) == MA_OK);
    TEST_ASSERT(MA_FALSE == result_val);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);

    /* 40 */
    TEST_ASSERT(ma_variant_create_from_double(0.001, &variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_create_from_double(1e-3, &variant_ptr_2) == MA_OK);
    TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, &result_val) == MA_OK);
    TEST_ASSERT(MA_TRUE == result_val);
    TEST_ASSERT(ma_variant_release(variant_ptr_1) == MA_OK);
    TEST_ASSERT(ma_variant_release(variant_ptr_2) == MA_OK);
}

/*
Variant equality comparison tests for arrays and tables
*/
TEST(ma_variant_basic_types_tests, ma_variant_is_equal_array_table_test)
{
    /* 'ma_variant_t' type Pointers for storing the base address of variant object */
    ma_variant_t *variant_ptr_1 = NULL;
    ma_variant_t *variant_ptr_2 = NULL;

    /* Integer to store the result of equality comparison */
    ma_bool_t result_val = MA_FALSE;

    /*Test 1 : Two similar array variants */
    {
        ma_variant_t *array_variant_ptr_1 = NULL;
        ma_variant_t *array_variant_ptr_2 = NULL;

        ma_array_t *array_1 = NULL;
        ma_array_t *array_2 = NULL;

        TEST_ASSERT(MA_OK == ma_variant_create_from_uint8(0xFFU, &variant_ptr_1));
        TEST_ASSERT(MA_OK == ma_variant_create_from_uint8(0xFFU, &variant_ptr_2));

        TEST_ASSERT(MA_OK == ma_array_create(&array_1));
        TEST_ASSERT(MA_OK == ma_array_create(&array_2));

        TEST_ASSERT(MA_OK == ma_array_push(array_1, variant_ptr_1));
        TEST_ASSERT(MA_OK == ma_array_push(array_2, variant_ptr_2));

        TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr_1));
        TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr_2));

        TEST_ASSERT(MA_OK == ma_variant_create_from_array(array_1, &array_variant_ptr_1));
        TEST_ASSERT(MA_OK == ma_variant_create_from_array(array_2, &array_variant_ptr_2));

        TEST_ASSERT(MA_OK == ma_array_release(array_1));
        TEST_ASSERT(MA_OK == ma_array_release(array_2));

        TEST_ASSERT(MA_OK == ma_variant_is_equal(array_variant_ptr_1, array_variant_ptr_2, &result_val));
        TEST_ASSERT(MA_TRUE == result_val);

        TEST_ASSERT(MA_OK == ma_variant_release(array_variant_ptr_1));
        TEST_ASSERT(MA_OK == ma_variant_release(array_variant_ptr_2));
    }

    /*Test 2 : Two different array variants */
    {
        ma_variant_t *array_variant_ptr_1 = NULL;
        ma_variant_t *array_variant_ptr_2 = NULL;

        ma_array_t *array_1 = NULL;
        ma_array_t *array_2 = NULL;

        TEST_ASSERT(MA_OK == ma_variant_create_from_uint8(0xFFU, &variant_ptr_1));
        TEST_ASSERT(MA_OK == ma_variant_create_from_uint8(0xEFU, &variant_ptr_2));

        TEST_ASSERT(MA_OK == ma_array_create(&array_1));
        TEST_ASSERT(MA_OK == ma_array_create(&array_2));

        TEST_ASSERT(MA_OK == ma_array_push(array_1, variant_ptr_1));
        TEST_ASSERT(MA_OK == ma_array_push(array_2, variant_ptr_2));

        TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr_1));
        TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr_2));

        TEST_ASSERT(MA_OK == ma_variant_create_from_array(array_1, &array_variant_ptr_1));
        TEST_ASSERT(MA_OK == ma_variant_create_from_array(array_2, &array_variant_ptr_2));

        TEST_ASSERT(MA_OK == ma_array_release(array_1));
        TEST_ASSERT(MA_OK == ma_array_release(array_2));

        TEST_ASSERT(MA_OK == ma_variant_is_equal(array_variant_ptr_1, array_variant_ptr_2, &result_val));
        TEST_ASSERT(MA_FALSE == result_val);

        TEST_ASSERT(MA_OK == ma_variant_release(array_variant_ptr_1));
        TEST_ASSERT(MA_OK == ma_variant_release(array_variant_ptr_2));
    }

    /*Test 3 : Two table variants containing 1 element each (same key value pair)*/
    {
        ma_variant_t *table_variant_ptr_1 = NULL;
        ma_variant_t *table_variant_ptr_2 = NULL;

        ma_table_t *table_1 = NULL;
        ma_table_t *table_2 = NULL;

        TEST_ASSERT(MA_OK == ma_variant_create_from_uint8(0xFFU, &variant_ptr_1));
        TEST_ASSERT(MA_OK == ma_variant_create_from_uint8(0xFFU, &variant_ptr_2));

        TEST_ASSERT(MA_OK == ma_table_create(&table_1));
        TEST_ASSERT(MA_OK == ma_table_create(&table_2));

        TEST_ASSERT(MA_OK == ma_table_add_entry(table_1, "same_key", variant_ptr_1));
        TEST_ASSERT(MA_OK == ma_table_add_entry(table_2, "same_key", variant_ptr_2));

        TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr_1));
        TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr_2));

        TEST_ASSERT(MA_OK == ma_variant_create_from_table(table_1, &table_variant_ptr_1));
        TEST_ASSERT(MA_OK == ma_variant_create_from_table(table_2, &table_variant_ptr_2));

        TEST_ASSERT(MA_OK == ma_table_release(table_1));
        TEST_ASSERT(MA_OK == ma_table_release(table_2));

        TEST_ASSERT(MA_OK == ma_variant_is_equal(table_variant_ptr_1, table_variant_ptr_2, &result_val));
        TEST_ASSERT(MA_TRUE == result_val);

        TEST_ASSERT(MA_OK == ma_variant_release(table_variant_ptr_1));
        TEST_ASSERT(MA_OK == ma_variant_release(table_variant_ptr_2));
    }

    /*Test 4 : Two table variants containing 1 element each (same key, different values)*/
    {
        ma_variant_t *table_variant_ptr_1 = NULL;
        ma_variant_t *table_variant_ptr_2 = NULL;

        ma_table_t *table_1 = NULL;
        ma_table_t *table_2 = NULL;

        TEST_ASSERT(MA_OK == ma_variant_create_from_uint8(0xFFU, &variant_ptr_1));
        TEST_ASSERT(MA_OK == ma_variant_create_from_uint8(0xEFU, &variant_ptr_2));

        TEST_ASSERT(MA_OK == ma_table_create(&table_1));
        TEST_ASSERT(MA_OK == ma_table_create(&table_2));

        TEST_ASSERT(MA_OK == ma_table_add_entry(table_1, "same_key", variant_ptr_1));
        TEST_ASSERT(MA_OK == ma_table_add_entry(table_2, "same_key", variant_ptr_2));

        TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr_1));
        TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr_2));

        TEST_ASSERT(MA_OK == ma_variant_create_from_table(table_1, &table_variant_ptr_1));
        TEST_ASSERT(MA_OK == ma_variant_create_from_table(table_2, &table_variant_ptr_2));

        TEST_ASSERT(MA_OK == ma_table_release(table_1));
        TEST_ASSERT(MA_OK == ma_table_release(table_2));

        TEST_ASSERT(MA_OK == ma_variant_is_equal(table_variant_ptr_1, table_variant_ptr_2, &result_val));
        TEST_ASSERT(MA_FALSE == result_val);

        TEST_ASSERT(MA_OK == ma_variant_release(table_variant_ptr_1));
        TEST_ASSERT(MA_OK == ma_variant_release(table_variant_ptr_2));
    }

    /*Test 5 : Two table variants containing 1 element each (different key, same values)*/
    {
        ma_variant_t *table_variant_ptr_1 = NULL;
        ma_variant_t *table_variant_ptr_2 = NULL;

        ma_table_t *table_1 = NULL;
        ma_table_t *table_2 = NULL;

        TEST_ASSERT(MA_OK == ma_variant_create_from_uint8(0xFFU, &variant_ptr_1));
        TEST_ASSERT(MA_OK == ma_variant_create_from_uint8(0xFFU, &variant_ptr_2));

        TEST_ASSERT(MA_OK == ma_table_create(&table_1));
        TEST_ASSERT(MA_OK == ma_table_create(&table_2));

        TEST_ASSERT(MA_OK == ma_table_add_entry(table_1, "same_key", variant_ptr_1));
        TEST_ASSERT(MA_OK == ma_table_add_entry(table_2, "different_key", variant_ptr_2));

        TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr_1));
        TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr_2));

        TEST_ASSERT(MA_OK == ma_variant_create_from_table(table_1, &table_variant_ptr_1));
        TEST_ASSERT(MA_OK == ma_variant_create_from_table(table_2, &table_variant_ptr_2));

        TEST_ASSERT(MA_OK == ma_table_release(table_1));
        TEST_ASSERT(MA_OK == ma_table_release(table_2));

        TEST_ASSERT(MA_OK == ma_variant_is_equal(table_variant_ptr_1, table_variant_ptr_2, &result_val));
        TEST_ASSERT(MA_FALSE == result_val);

        TEST_ASSERT(MA_OK == ma_variant_release(table_variant_ptr_1));
        TEST_ASSERT(MA_OK == ma_variant_release(table_variant_ptr_2));
    }
}

/*
Unit tests for ma_variant_inc_ref and ma_variant_dec_ref macros.
*/
TEST(ma_variant_basic_types_tests, ma_variant_inc_ref_dec_ref_test)
{
    /* 'ma_variant_t' type Pointer for storing the base address of variant object */
    ma_variant_t *variant_ptr = NULL;

    // Loop iterator.
    ma_uint8_t counter = 0;

    /* Variable to hold the variant type */
    ma_vartype_t vartype_obj = MA_VARTYPE_STRING;

    /*Test 1 : Calling ma_variant_inc_ref macro with address of variant as NULL*/

    TEST_ASSERT(ma_variant_add_ref(NULL) == MA_ERROR_INVALIDARG);

    /*Test 2 : Calling ma_variant_dec_ref macro with address of variant as NULL*/

    TEST_ASSERT(ma_variant_release(NULL) == MA_ERROR_INVALIDARG);

    /*Test 3 : Verifying that the ma_variant_inc_ref/ma_variant_dec_ref macro maintains correct reference count*/

    /*Creating a unsigned 8-bit integer variant*/
    TEST_ASSERT(ma_variant_create_from_uint8(0x74U, &variant_ptr) == MA_OK);

    /*Incrementing the variant reference count (10 times) */
    for(counter = 0; counter < 10; counter++)
    {
        TEST_ASSERT(ma_variant_add_ref(variant_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_get_type(variant_ptr, &vartype_obj) == MA_OK);
        TEST_ASSERT(vartype_obj == MA_VARTYPE_UINT8);    }

    /* Decrement the variant reference count (11 times). The variant should be destroyed
       by the end of this for-loop */
    for(counter = 0; counter < 11; counter++)
    {
        TEST_ASSERT(ma_variant_get_type(variant_ptr, &vartype_obj) == MA_OK);
        TEST_ASSERT(vartype_obj == MA_VARTYPE_UINT8);
        TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);
    }
}




