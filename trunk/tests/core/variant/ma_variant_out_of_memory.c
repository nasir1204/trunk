/*****************************************************************************
This file contains the unit tests for covering various robustness and other
leftover scenarios for the Variant APIs.
******************************************************************************/
#include "unity.h"
#include "unity_fixture.h"

#include "ma/ma_variant.h"
#include <stdlib.h>


TEST_GROUP_RUNNER(ma_variant_outofmemory_test_group)
{
    do {RUN_TEST_CASE(ma_variant_outofmemory_tests, ma_variant_out_of_memory_test)} while (0);
}

TEST_SETUP(ma_variant_outofmemory_tests)
{
}

TEST_TEAR_DOWN(ma_variant_outofmemory_tests)
{

}



/*
This support function is used for testing the 'out of memory' scenarios.
This function runs till all the dynamic memory is exhausted. This simulates the
behavior that there is no memory for buffer creation.
*/

int **memory_ptr = NULL;
int counter = 0;

void exhaust_dynamic_memory()
{
    size_t block_size = 2048;

    while(block_size)
    {
        while(malloc(block_size)) {;}
        block_size = block_size >> 1;
    }
}


/*
Unit test for verifying the scenario where no type of variant (basic types, string,
buffer, array or table) could be created because there is no heap memory left to
create these variants.
*/
TEST(ma_variant_outofmemory_tests, ma_variant_out_of_memory_test)
{
    /* 'ma_variant_t' type Pointer for storing the base address of variant object */
    ma_variant_t *variant_ptr = NULL;

    /* 'ma_array_t' array type pointer */
    ma_array_t *array_ptr = NULL;

    /* 'ma_table_t' table type pointer */
    ma_table_t *table_ptr = NULL;

    ma_uint8_t raw_buffer[] = { 0x12, 0x23, 0x34, 0x45, 0x56, 0x67, 0x78, 0x89, 0x90 };

    /* Simulating that all dynamic memory is exhausted */
    exhaust_dynamic_memory();

    /* Trying to create a NULL variant */
    TEST_ASSERT_MESSAGE(ma_variant_create(&variant_ptr) == MA_ERROR_OUTOFMEMORY, \
                        "ma_variant_create doesn't return MA_ERROR_OUTOFMEMORY if it is not able to allocate memory for variant");

    // Trying to create a array of variants */
    TEST_ASSERT_MESSAGE(ma_array_create(&array_ptr) == MA_ERROR_OUTOFMEMORY, \
                        "ma_array_create doesn't returns MA_ERROR_OUTOFMEMORY if it is not able to allocate memory for array variant");

    /*Creating a raw buffer variant*/
    TEST_ASSERT_MESSAGE(ma_variant_create_from_raw(raw_buffer, sizeof(raw_buffer)/sizeof(raw_buffer[0]), &variant_ptr) == MA_ERROR_OUTOFMEMORY, \
                        "ma_variant_create_from_raw doesn't return MA_ERROR_OUTOFMEMORY if it is not able to allocate memory for array variant");

    /*Creating a string variant*/
    TEST_ASSERT_MESSAGE(ma_variant_create_from_string("MA Security Agent", 17, &variant_ptr) == MA_ERROR_OUTOFMEMORY, \
                        "ma_variant_create_from_string doesn't return MA_ERROR_OUTOFMEMORY if it is not able to allocate memory for string variant");

    /*Creating a wide character string variant*/
    TEST_ASSERT_MESSAGE(ma_variant_create_from_wstring(L"MA Security Agent", 17, &variant_ptr) == MA_ERROR_OUTOFMEMORY, \
                        "ma_variant_create_from_wstring doesn't return MA_ERROR_OUTOFMEMORY if it is not able to allocate memory for string variant");

    /*Creating a table variant*/
    TEST_ASSERT_MESSAGE(ma_table_create(&table_ptr) == MA_ERROR_OUTOFMEMORY, \
                        "ma_table_create doesn't return MA_ERROR_OUTOFMEMORY  if it is not able to allocate memory for table variant");
}

