/*****************************************************************************
Variant library unit tests for arrays
******************************************************************************/
#include "unity.h"
#include "unity_fixture.h"
#include "ma/ma_variant.h"
#include <string.h>
#include <wchar.h>

TEST_GROUP_RUNNER(ma_variant_array_test_group)
{
    do { RUN_TEST_CASE(ma_variant_array_tests, ma_variant_create_get_from_array_test)} while (0);
    do { RUN_TEST_CASE(ma_variant_array_tests, ma_variant_foreach_element_from_array_test)} while (0);
    do { RUN_TEST_CASE(ma_variant_array_tests, ma_array_is_equal_test)} while (0);
    do { RUN_TEST_CASE(ma_variant_array_tests, ma_variant_array_self_reference_test)} while (0);
    do { RUN_TEST_CASE(ma_variant_array_tests, ma_variant_inc_ref_dec_ref_array_test)} while (0);
    do { RUN_TEST_CASE(ma_variant_array_tests, ma_variant_char_array_test)} while (0);

    #ifdef MA_HAS_WCHAR_T
        do { RUN_TEST_CASE(ma_variant_array_tests, ma_variant_wide_char_array_test)} while (0);
    #endif
}

TEST_SETUP(ma_variant_array_tests)
{
}

TEST_TEAR_DOWN(ma_variant_array_tests)
{
}


/* Test Vector for foreach API test */
ma_uint32_t test_vector_array[] = { 0xFFFFFFFFUL, 0x12345678UL, 0xFEDCBA09UL, 0x87654321UL,
                                    0x01010101UL, 0x10101010UL, 0x34567890UL, 0x00000000UL,
                                    0x00000001UL  };


/*
callback function for iterating through the array. This callback function reads
the variant at given index and compares it against the set of unsigned 32-bit
integer variants pushed into the array.
*/
void test_array_callback(size_t index, ma_variant_t *value, void *cb_args, ma_bool_t *stop_loop)
{
    ma_uint32_t actual_value = 0UL;

    TEST_ASSERT_MESSAGE(ma_variant_get_uint32(value, &actual_value) == MA_OK, \
                        "ma_variant_get_uint32 could not retrieve 32 bit value from the variant");

    TEST_ASSERT_MESSAGE(test_vector_array[index - 1] == actual_value, "Foreach iterator could not read correct value");
}

/*
Unit tests for following APIs -
1) ma_array_create
2) ma_array_push
3) ma_array_size
4) ma_variant_create_from_array
5) ma_variant_get_array
6) ma_array_get_element_at
7) ma_array_set_element_at
*/
TEST(ma_variant_array_tests, ma_variant_create_get_from_array_test)
{
    /* 'ma_variant_t' type Pointer for storing the base address of variant object */
    ma_variant_t *array_variant_ptr = NULL;

    /* 'ma_variant_t' type Pointer for storing the variant address */
    ma_variant_t *variant_ptr = NULL;

    /* 'ma_array_t' array type pointer */
    ma_array_t *array_ptr = NULL;

    /* Array pointer retrieved from the array variant */
    ma_array_t *array_ptr_1 = NULL;

    /* Variant Pointer retrieved from array */
    ma_variant_t *variant_ptr_1 = NULL;

    /* For storing array size returned by ma_array_size API */
    size_t array_size = 100;

    /* This variable should hold MA_VARTYPE_ARRAY after the ma_variant_get_type
       call. Initial value of this variable is given as MA_VARTYPE_STRING. */
    ma_vartype_t vartype_obj = MA_VARTYPE_STRING;

    /* Variables for storing values retrieved from array */
    ma_uint64_t  unsigned_64bit_val = 0ULL;
    ma_int64_t   signed_64bit_val = 0LL;
    ma_uint32_t  unsigned_32bit_val = 0UL;
    ma_int32_t   signed_32bit_val = 0L;
    ma_uint16_t  unsigned_16bit_val = 0;
    ma_int16_t   signed_16bit_val = 0;
    ma_uint8_t   unsigned_8bit_val = 0U;
    ma_int8_t    signed_8bit_val = 0;
    double       double_val = 0.0;
    float        float_val = 0.0f;
    ma_bool_t    boolean_val = 0;
    ma_buffer_t  *buffer_ptr = NULL;
    ma_wbuffer_t *wbuffer_ptr = NULL;
    const char   *string_val = NULL;
    const wchar_t * wstring_val = NULL;
    size_t       string_size = 0;


    /* Sample Raw Buffer. To be used while creating raw buffer variant */
    ma_uint8_t raw_buffer[] = { 0x12, 0x23, 0x34, 0x45, 0x56, 0x67, 0x78, 0x89, 0x90 };


    /* Creating array of variants */
    TEST_ASSERT_MESSAGE(ma_array_create(NULL) == MA_ERROR_INVALIDARG, \
                        "ma_array_create doesn't return MA_ERROR_INVALIDARG for NULL I/P");

    TEST_ASSERT_MESSAGE(ma_array_create(&array_ptr) == MA_OK, \
                        "ma_array_create doesn't returns MA_OK for valid array pointer address");

    /*
    Creating variants of various types -> pushing them into the array -> Checking that the size of array is
    incremented by 1
    */

    /* ma_array_size returns NULL for invalid arguments */
    TEST_ASSERT_MESSAGE(ma_array_size(NULL, NULL) == MA_ERROR_INVALIDARG, \
                        "ma_array_size doesn't returns MA_ERROR_INVALIDARG for invalid arguments");

    TEST_ASSERT_MESSAGE(ma_array_size(array_ptr, NULL) == MA_ERROR_INVALIDARG, \
                        "ma_array_size doesn't returns MA_ERROR_INVALIDARG for invalid arguments");

    TEST_ASSERT_MESSAGE(ma_array_size(NULL, &array_size) == MA_ERROR_INVALIDARG, \
                        "ma_array_size doesn't returns MA_ERROR_INVALIDARG for invalid arguments");

    /* ma_array_size returns 0 before any variant is added */
    TEST_ASSERT_MESSAGE(ma_array_size(array_ptr, &array_size) == MA_OK, \
                        "ma_array_size doesn't returns MA_OK for valid arguments");

    TEST_ASSERT_MESSAGE(0 == array_size, "ma_array_size doesn't return 0 array size for an empty array");

    /* NULL Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create(&variant_ptr) == MA_OK, "NULL variant could not be created");

    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr, variant_ptr) == MA_OK, "Variant could not be pushed to array");
    ma_variant_release(variant_ptr);

    TEST_ASSERT_MESSAGE(ma_array_size(array_ptr, &array_size) == MA_OK, "ma_array_size doesn't returns MA_OK for valid arguments");

    TEST_ASSERT_MESSAGE(1 == array_size, "ma_array_size doesn't return correct array size");

    /* 64 Bit unsigned integer Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_uint64(0x12345678ABCDEF09ULL, &variant_ptr) == MA_OK, \
                        "64 Bit unsigned variant could not be created");

    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr, variant_ptr) == MA_OK, "Variant could not be pushed to array");
    ma_variant_release(variant_ptr);

    TEST_ASSERT_MESSAGE(ma_array_size(array_ptr, &array_size) == MA_OK, "ma_array_size doesn't returns MA_OK for valid arguments");

    TEST_ASSERT_MESSAGE(2 == array_size, "ma_array_size doesn't return correct array size");

    /* 64 Bit signed integer Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_int64(0x12345678ABCDEF09LL, &variant_ptr) == MA_OK, \
                        "64 Bit unsigned variant could not be created");

    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr, variant_ptr) == MA_OK, "Variant could not be pushed to array");
    ma_variant_release(variant_ptr);

    TEST_ASSERT_MESSAGE(ma_array_size(array_ptr, &array_size) == MA_OK, "ma_array_size doesn't returns MA_OK for valid arguments");

    TEST_ASSERT_MESSAGE(3 == array_size, "ma_array_size doesn't return correct array size");

    /* Double Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_double(1.23e+18, &variant_ptr) == MA_OK, "Double variant could not be created");

    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr, variant_ptr) == MA_OK, "Variant could not be pushed to array");
    ma_variant_release(variant_ptr);

    TEST_ASSERT_MESSAGE(ma_array_size(array_ptr, &array_size) == MA_OK, "ma_array_size doesn't returns MA_OK for valid arguments");

    TEST_ASSERT_MESSAGE(4 == array_size, "ma_array_size doesn't return correct array size");

    /* 32 Bit unsigned integer Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_uint32(0x1234ABCDUL, &variant_ptr) == MA_OK, \
                        "32 Bit unsigned variant could not be created");

    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr, variant_ptr) == MA_OK, "Variant could not be pushed to array");
    ma_variant_release(variant_ptr);

    TEST_ASSERT_MESSAGE(ma_array_size(array_ptr, &array_size) == MA_OK, "ma_array_size doesn't returns MA_OK for valid arguments");

    TEST_ASSERT_MESSAGE(5 == array_size, "ma_array_size doesn't return correct array size");

    /* 32 Bit signed integer Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_int32(0x3FAB0123LL, &variant_ptr) == MA_OK, \
                        "32 Bit signed variant could not be created");

    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr, variant_ptr) == MA_OK, "Variant could not be pushed to array");
    ma_variant_release(variant_ptr);

    TEST_ASSERT_MESSAGE(ma_array_size(array_ptr, &array_size) == MA_OK, "ma_array_size doesn't returns MA_OK for valid arguments");

    TEST_ASSERT_MESSAGE(6 == array_size, "ma_array_size doesn't return correct array size");

    /* Float Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_float(123.45f, &variant_ptr) == MA_OK, \
                        "Float variant could not be created");

    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr, variant_ptr) == MA_OK, "Variant could not be pushed to array");
    ma_variant_release(variant_ptr);

    TEST_ASSERT_MESSAGE(ma_array_size(array_ptr, &array_size) == MA_OK, "ma_array_size doesn't returns MA_OK for valid arguments");

    TEST_ASSERT_MESSAGE(7 == array_size, "ma_array_size doesn't return correct array size");

    /* 16 Bit unsigned integer Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_uint16(0x1234, &variant_ptr) == MA_OK, \
                        "16 Bit unsigned variant could not be created");

    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr, variant_ptr) == MA_OK, "Variant could not be pushed to array");
    ma_variant_release(variant_ptr);

    TEST_ASSERT_MESSAGE(ma_array_size(array_ptr, &array_size) == MA_OK, "ma_array_size doesn't returns MA_OK for valid arguments");

    TEST_ASSERT_MESSAGE(8 == array_size, "ma_array_size doesn't return correct array size");

    /* 16 Bit signed integer Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_int16(0x3FAB, &variant_ptr) == MA_OK, \
                        "16 Bit signed variant could not be created");

    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr, variant_ptr) == MA_OK, "Variant could not be pushed to array");
    ma_variant_release(variant_ptr);

    TEST_ASSERT_MESSAGE(ma_array_size(array_ptr, &array_size) == MA_OK, "ma_array_size doesn't returns MA_OK for valid arguments");

    TEST_ASSERT_MESSAGE(9 == array_size, "ma_array_size doesn't return correct array size");

    /* 8 Bit unsigned integer Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_uint8(0x12, &variant_ptr) == MA_OK, \
                        "8 Bit unsigned variant could not be created");

    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr, variant_ptr) == MA_OK, "Variant could not be pushed to array");
    ma_variant_release(variant_ptr);

    TEST_ASSERT_MESSAGE(ma_array_size(array_ptr, &array_size) == MA_OK, "ma_array_size doesn't returns MA_OK for valid arguments");

    TEST_ASSERT_MESSAGE(10 == array_size, "ma_array_size doesn't return correct array size");

    /* 8 Bit signed integer Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_int8(0x7F, &variant_ptr) == MA_OK, \
                        "8 Bit signed variant could not be created");

    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr, variant_ptr) == MA_OK, "Variant could not be pushed to array");
    ma_variant_release(variant_ptr);

    TEST_ASSERT_MESSAGE(ma_array_size(array_ptr, &array_size) == MA_OK, "ma_array_size doesn't returns MA_OK for valid arguments");

    TEST_ASSERT_MESSAGE(11 == array_size, "ma_array_size doesn't return correct array size");

    /* Boolean Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_bool(123, &variant_ptr) == MA_OK, \
                        "Boolean variant could not be created");

    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr, variant_ptr) == MA_OK, "Variant could not be pushed to array");
    ma_variant_release(variant_ptr);

    TEST_ASSERT_MESSAGE(ma_array_size(array_ptr, &array_size) == MA_OK, "ma_array_size doesn't returns MA_OK for valid arguments");

    TEST_ASSERT_MESSAGE(12 == array_size, "ma_array_size doesn't return correct array size");

    /* String Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_string("MA Security Agent", 17, &variant_ptr) == MA_OK, \
                        "ma_variant_create_from_string doesn't return MK_OK for valid string");

    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr, variant_ptr) == MA_OK, "Variant could not be pushed to array");
    ma_variant_release(variant_ptr);

    TEST_ASSERT_MESSAGE(ma_array_size(array_ptr, &array_size) == MA_OK, "ma_array_size doesn't returns MA_OK for valid arguments");

    TEST_ASSERT_MESSAGE(13 == array_size, "ma_array_size doesn't return correct array size");

    /* Wide Character String Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_wstring(L"MA Security Agent$%$%\x212B", 22, &variant_ptr) == MA_OK, \
                        "ma_variant_create_from_wstring doesn't return MK_OK for valid string");

    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr, variant_ptr) == MA_OK, "Variant could not be pushed to array");
    ma_variant_release(variant_ptr);

    TEST_ASSERT_MESSAGE(ma_array_size(array_ptr, &array_size) == MA_OK, "ma_array_size doesn't returns MA_OK for valid arguments");

    TEST_ASSERT_MESSAGE(14 == array_size, "ma_array_size doesn't return correct array size");

    /* Raw Data Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_raw(raw_buffer, sizeof(raw_buffer)/sizeof(raw_buffer[0]), &variant_ptr) == MA_OK, \
                        "Raw Buffer variant could not be created");

    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr, variant_ptr) == MA_OK, "Variant could not be pushed to array");
    ma_variant_release(variant_ptr);

    TEST_ASSERT_MESSAGE(ma_array_size(array_ptr, &array_size) == MA_OK, "ma_array_size doesn't returns MA_OK for valid arguments");

    TEST_ASSERT_MESSAGE(15 == array_size, "ma_array_size doesn't return correct array size");


    /*----------------- ma_array_set_element_at() verification. Setting a new value at array location 11 -------------*/
    TEST_ASSERT(ma_variant_create_from_int8(0x34, &variant_ptr) == MA_OK);

    TEST_ASSERT(ma_array_set_element_at(array_ptr, 0, variant_ptr) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_array_set_element_at(array_ptr, 16, variant_ptr) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_array_set_element_at(NULL, 11, variant_ptr) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_array_set_element_at(array_ptr, 11, NULL) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_array_set_element_at(array_ptr, 11, variant_ptr) == MA_OK);
    ma_variant_release(variant_ptr);

    // Verifying that the size of array still remains 15. Element is modified not added.
    TEST_ASSERT(ma_array_size(array_ptr, &array_size) == MA_OK);
    TEST_ASSERT(15 == array_size);
    /*----------------------------------------------------------------------------------------------------------------*/

    /* Creating Variant from the array */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_array(NULL, NULL) == MA_ERROR_INVALIDARG, \
                        "ma_variant_create_from_array doesn't returns MA_ERROR_INVALIDARG for invalid arguments");

    TEST_ASSERT_MESSAGE(ma_variant_create_from_array(array_ptr, NULL) == MA_ERROR_INVALIDARG, \
                        "ma_variant_create_from_array doesn't returns MA_ERROR_INVALIDARG for invalid arguments");

    TEST_ASSERT_MESSAGE(ma_variant_create_from_array(NULL, &array_variant_ptr) == MA_ERROR_INVALIDARG, \
                        "ma_variant_create_from_array doesn't returns MA_ERROR_INVALIDARG for invalid arguments");

    TEST_ASSERT_MESSAGE(ma_variant_create_from_array(array_ptr, &array_variant_ptr) == MA_OK, \
                        "ma_variant_create_from_array could not create variant from array");

    /* Checking the type of variant (array type) */
    TEST_ASSERT_MESSAGE(ma_variant_get_type(array_variant_ptr, &vartype_obj) == MA_OK, \
                        "ma_variant_get_type doesn't return MA_OK for Array Variant");

    TEST_ASSERT_MESSAGE(vartype_obj == MA_VARTYPE_ARRAY, \
                        "ma_variant_get_type doesn't return MA_VARTYPE_ARRAY for array variant");

    /* Trying to push array variant into the same array (self reference check should fail) */
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr, array_variant_ptr) == MA_ERROR_INVALIDARG, "Self-Reference check didn't work");

    /* Trying to retrieve array from a variant of NULL type */
    TEST_ASSERT_MESSAGE(ma_variant_create(&variant_ptr) == MA_OK, "Variant could not be created");
    TEST_ASSERT_MESSAGE(ma_variant_get_array(variant_ptr, &array_ptr_1) == MA_ERROR_PRECONDITION, "ma_variant_get_array failed");
    ma_variant_release(variant_ptr);

    /* Retrieving array back from the Variant */
    TEST_ASSERT_MESSAGE(ma_variant_get_array(NULL, NULL) == MA_ERROR_INVALIDARG, \
                        "ma_variant_get_array doesn't returns MA_ERROR_INVALIDARG for invalid arguments");

    TEST_ASSERT_MESSAGE(ma_variant_get_array(array_variant_ptr, NULL) == MA_ERROR_INVALIDARG, \
                        "ma_variant_get_array doesn't returns MA_ERROR_INVALIDARG for invalid arguments");

    TEST_ASSERT_MESSAGE(ma_variant_get_array(NULL, &array_ptr_1) == MA_ERROR_INVALIDARG, \
                        "ma_variant_get_array doesn't returns MA_ERROR_INVALIDARG for invalid arguments");

    TEST_ASSERT_MESSAGE(ma_variant_get_array(array_variant_ptr, &array_ptr_1) == MA_OK, \
                        "ma_variant_get_array could not retrieve array from the array variant");

    /* Reading elements back from the retrieved array */
    TEST_ASSERT_MESSAGE(ma_array_get_element_at(NULL, 0, NULL) == MA_ERROR_INVALIDARG, \
                        "ma_array_get_element_at doesn't returns MA_ERROR_INVALIDARG for invalid arguments");

    TEST_ASSERT_MESSAGE(ma_array_get_element_at(array_ptr_1, 0, NULL) == MA_ERROR_INVALIDARG, \
                        "ma_array_get_element_at doesn't returns MA_ERROR_INVALIDARG for invalid arguments");

    TEST_ASSERT_MESSAGE(ma_array_get_element_at(NULL, 0, &variant_ptr_1) == MA_ERROR_INVALIDARG, \
                        "ma_array_get_element_at doesn't returns MA_ERROR_INVALIDARG for invalid arguments");

    /* 15 variants were pushed into the array. They should occupy index 1 to 15. Therefore, location 0 and
       location 16 are invalid locations. */
    TEST_ASSERT_MESSAGE(ma_array_get_element_at(array_ptr_1, 0, &variant_ptr_1) == MA_ERROR_INVALIDARG, \
                        "ma_array_get_element_at doesn't returns MA_ERROR_INVALIDARG for invalid array index");

    TEST_ASSERT_MESSAGE(ma_array_get_element_at(array_ptr_1, 16, &variant_ptr_1) == MA_ERROR_INVALIDARG, \
                        "ma_array_get_element_at doesn't returns MA_ERROR_INVALIDARG for invalid array index");

    /* Reading variant at index 1 */
    TEST_ASSERT_MESSAGE(ma_array_get_element_at(array_ptr_1, 1, &variant_ptr_1) == MA_OK, \
                        "ma_array_get_element_at couldn't retrieve variant from index 0 of the array");

    TEST_ASSERT_MESSAGE(ma_variant_get_type(variant_ptr_1, &vartype_obj) == MA_OK, \
                        "ma_variant_get_type doesn't return MA_OK for valid Variant");

    TEST_ASSERT_MESSAGE(vartype_obj == MA_VARTYPE_NULL, \
                        "NULL variant should have been stored at index 1, but it is not");
    ma_variant_release(variant_ptr_1);

    /* Reading variant at index 2 */
    TEST_ASSERT_MESSAGE(ma_array_get_element_at(array_ptr_1, 2, &variant_ptr_1) == MA_OK, \
                        "ma_array_get_element_at couldn't retrieve variant from index 2 of the array");

    TEST_ASSERT_MESSAGE(ma_variant_get_uint64(variant_ptr_1, &unsigned_64bit_val) == MA_OK, \
                        "Value could not be retrieved from the variant");

    TEST_ASSERT_MESSAGE(0x12345678ABCDEF09ULL == unsigned_64bit_val, "Correct value is not retrieved from variant");
    ma_variant_release(variant_ptr_1);

    /* Reading variant at index 3 */
    TEST_ASSERT_MESSAGE(ma_array_get_element_at(array_ptr_1, 3, &variant_ptr_1) == MA_OK, \
                        "ma_array_get_element_at couldn't retrieve variant from index 3 of the array");

    TEST_ASSERT_MESSAGE(ma_variant_get_int64(variant_ptr_1, &signed_64bit_val) == MA_OK, \
                        "Value could not be retrieved from the variant");

    TEST_ASSERT_MESSAGE(0x12345678ABCDEF09LL == signed_64bit_val, "Correct value is not retrieved from variant");
    ma_variant_release(variant_ptr_1);

    /* Reading variant at index 4 */
    TEST_ASSERT_MESSAGE(ma_array_get_element_at(array_ptr_1, 4, &variant_ptr_1) == MA_OK, \
                        "ma_array_get_element_at couldn't retrieve variant from index 4 of the array");

    TEST_ASSERT_MESSAGE(ma_variant_get_double(variant_ptr_1, &double_val) == MA_OK, \
                        "Value could not be retrieved from the variant");

    TEST_ASSERT_MESSAGE(1.23e+18 == double_val, "Correct value is not retrieved from variant");
    ma_variant_release(variant_ptr_1);

    /* Reading variant at index 5 */
    TEST_ASSERT_MESSAGE(ma_array_get_element_at(array_ptr_1, 5, &variant_ptr_1) == MA_OK, \
                        "ma_array_get_element_at couldn't retrieve variant from index 5 of the array");

    TEST_ASSERT_MESSAGE(ma_variant_get_uint32(variant_ptr_1, &unsigned_32bit_val) == MA_OK, \
                        "Value could not be retrieved from the variant");

    TEST_ASSERT_MESSAGE(0x1234ABCDUL == unsigned_32bit_val, "Correct value is not retrieved from variant");
    ma_variant_release(variant_ptr_1);

    /* Reading variant at index 6 */
    TEST_ASSERT_MESSAGE(ma_array_get_element_at(array_ptr_1, 6, &variant_ptr_1) == MA_OK, \
                        "ma_array_get_element_at couldn't retrieve variant from index 6 of the array");

    TEST_ASSERT_MESSAGE(ma_variant_get_int32(variant_ptr_1, &signed_32bit_val) == MA_OK, \
                        "Value could not be retrieved from the variant");

    TEST_ASSERT_MESSAGE(0x3FAB0123 == signed_32bit_val, "Correct value is not retrieved from variant");
    ma_variant_release(variant_ptr_1);

    /* Reading variant at index 7 */
    TEST_ASSERT_MESSAGE(ma_array_get_element_at(array_ptr_1, 7, &variant_ptr_1) == MA_OK, \
                        "ma_array_get_element_at couldn't retrieve variant from index 7 of the array");

    TEST_ASSERT_MESSAGE(ma_variant_get_float(variant_ptr_1, &float_val) == MA_OK, \
                        "Value could not be retrieved from the variant");

    TEST_ASSERT_MESSAGE(123.45f == float_val, "Correct value is not retrieved from variant");
    ma_variant_release(variant_ptr_1);

    /* Reading variant at index 8 */
    TEST_ASSERT_MESSAGE(ma_array_get_element_at(array_ptr_1, 8, &variant_ptr_1) == MA_OK, \
                        "ma_array_get_element_at couldn't retrieve variant from index 8 of the array");

    TEST_ASSERT_MESSAGE(ma_variant_get_uint16(variant_ptr_1, &unsigned_16bit_val) == MA_OK, \
                        "Value could not be retrieved from the variant");

    TEST_ASSERT_MESSAGE(0x1234 == unsigned_16bit_val, "Correct value is not retrieved from variant");
    ma_variant_release(variant_ptr_1);

    /* Reading variant at index 9 */
    TEST_ASSERT_MESSAGE(ma_array_get_element_at(array_ptr_1, 9, &variant_ptr_1) == MA_OK, \
                        "ma_array_get_element_at couldn't retrieve variant from index 9 of the array");

    TEST_ASSERT_MESSAGE(ma_variant_get_int16(variant_ptr_1, &signed_16bit_val) == MA_OK, \
                        "Value could not be retrieved from the variant");

    TEST_ASSERT_MESSAGE(0x3FAB == signed_16bit_val, "Correct value is not retrieved from variant");
    ma_variant_release(variant_ptr_1);

    /* Reading variant at index 10 */
    TEST_ASSERT_MESSAGE(ma_array_get_element_at(array_ptr_1, 10, &variant_ptr_1) == MA_OK, \
                        "ma_array_get_element_at couldn't retrieve variant from index 10 of the array");

    TEST_ASSERT_MESSAGE(ma_variant_get_uint8(variant_ptr_1, &unsigned_8bit_val) == MA_OK, \
                        "Value could not be retrieved from the variant");

    TEST_ASSERT_MESSAGE(0x12 == unsigned_8bit_val, "Correct value is not retrieved from variant");
    ma_variant_release(variant_ptr_1);

    /* Reading variant at index 11. This element was modified using ma_array_set_element_at */
    TEST_ASSERT_MESSAGE(ma_array_get_element_at(array_ptr_1, 11, &variant_ptr_1) == MA_OK, \
                        "ma_array_get_element_at couldn't retrieve variant from index 11 of the array");

    TEST_ASSERT_MESSAGE(ma_variant_get_int8(variant_ptr_1, &signed_8bit_val) == MA_OK, \
                        "Value could not be retrieved from the variant");

    TEST_ASSERT_MESSAGE(0x34 == signed_8bit_val, "Correct value is not retrieved from variant");
    ma_variant_release(variant_ptr_1);

    /* Reading variant at index 12 */
    TEST_ASSERT_MESSAGE(ma_array_get_element_at(array_ptr_1, 12, &variant_ptr_1) == MA_OK, \
                        "ma_array_get_element_at couldn't retrieve variant from index 12 of the array");

    TEST_ASSERT_MESSAGE(ma_variant_get_bool(variant_ptr_1, &boolean_val) == MA_OK, \
                        "Value could not be retrieved from the variant");

    TEST_ASSERT_MESSAGE(1 == boolean_val, "Correct value is not retrieved from variant");
    ma_variant_release(variant_ptr_1);

    /* Reading variant at index 13 */
    TEST_ASSERT_MESSAGE(ma_array_get_element_at(array_ptr_1, 13, &variant_ptr_1) == MA_OK, \
                        "ma_array_get_element_at couldn't retrieve variant from index 13 of the array");

    TEST_ASSERT_MESSAGE(ma_variant_get_string_buffer(variant_ptr_1, &buffer_ptr) == MA_OK, \
                        "String Buffer cannot be retrieved from the Variant");

    TEST_ASSERT_MESSAGE(ma_buffer_get_string(buffer_ptr, &string_val, &string_size) == MA_OK, \
                        "ma_buffer_get_string could not retrieve string and its size from the buffer");

    TEST_ASSERT_MESSAGE(17 == string_size, "String size is not read correctly from the buffer");

    TEST_ASSERT_EQUAL_MEMORY_MESSAGE("MA Security Agent", string_val, 17, "String is not read correctly from the buffer");
    ma_buffer_release(buffer_ptr);
    ma_variant_release(variant_ptr_1);

    /* Reading variant at index 14 */
    TEST_ASSERT_MESSAGE(ma_array_get_element_at(array_ptr_1, 14, &variant_ptr_1) == MA_OK, \
                        "ma_array_get_element_at couldn't retrieve variant from index 14 of the array");

    TEST_ASSERT_MESSAGE(ma_variant_get_wstring_buffer(variant_ptr_1, &wbuffer_ptr) == MA_OK, \
                        "String Buffer cannot be retrieved from the Variant");

    TEST_ASSERT_MESSAGE(ma_wbuffer_get_string(wbuffer_ptr, &wstring_val, &string_size) == MA_OK, \
                        "ma_wbuffer_get_string could not retrieve string and its size from the buffer");


    TEST_ASSERT_MESSAGE(22 == string_size, "String size is not read correctly from the buffer");

    TEST_ASSERT_EQUAL_MEMORY_MESSAGE(L"MA Security Agent$%$%\x212B", wstring_val, 23, "String is not read correctly from the buffer");
    ma_wbuffer_release(wbuffer_ptr);
    ma_variant_release(variant_ptr_1);

    /* Reading variant at index 15 */
    TEST_ASSERT_MESSAGE(ma_array_get_element_at(array_ptr_1, 15, &variant_ptr_1) == MA_OK, \
                        "ma_array_get_element_at couldn't retrieve variant from index 15 of the array");

    TEST_ASSERT_MESSAGE(ma_variant_get_raw_buffer(variant_ptr_1, &buffer_ptr) == MA_OK, \
                        "String Buffer cannot be retrieved from the Variant");

    TEST_ASSERT_MESSAGE(ma_buffer_get_raw(buffer_ptr, (const unsigned char**)&string_val, &string_size) == MA_OK, \
                        "ma_buffer_get_string could not retrieve string and its size from the buffer");

    TEST_ASSERT_MESSAGE(sizeof(raw_buffer)/sizeof(raw_buffer[0]) == string_size, "Raw Buffer size is not read correctly");

    TEST_ASSERT_EQUAL_MEMORY_MESSAGE(raw_buffer, string_val, sizeof(raw_buffer)/sizeof(raw_buffer[0]), \
                                     "Raw Buffer is not read correctly");
    ma_buffer_release(buffer_ptr);
    ma_variant_release(variant_ptr_1);

    ma_array_release(array_ptr_1);
    ma_variant_release(array_variant_ptr);
    ma_array_release(array_ptr);

}

/*
Unit tests for following APIs -
1) ma_array_create
2) ma_array_push
3) ma_array_size
4) ma_array_foreach
5) ma_array_inc_ref
6) ma_array_release
*/
TEST(ma_variant_array_tests, ma_variant_foreach_element_from_array_test)
{
    /* 'ma_variant_t' type Pointer for storing the variant address */
    ma_variant_t *variant_ptr = NULL;

    /* 'ma_array_t' array type pointer */
    ma_array_t *array_ptr = NULL;

    /* For storing array size returned by ma_array_size API */
    size_t array_size = 100;

    // Loop iterator.
    ma_uint8_t counter = 0;

    TEST_ASSERT_MESSAGE(ma_array_create(&array_ptr) == MA_OK, \
                        "Array could not be created");

    /* ma_array_size returns 0 before any variant is added */
    TEST_ASSERT_MESSAGE(ma_array_size(array_ptr, &array_size) == MA_OK, \
                        "ma_array_size doesn't returns MA_OK for valid arguments");

    TEST_ASSERT_MESSAGE(0 == array_size, "ma_array_size doesn't return 0 array size for an empty array");

    TEST_ASSERT_MESSAGE(ma_array_push(NULL, NULL) == MA_ERROR_INVALIDARG, \
                        "ma_array_push doesn't return MA_ERROR_INVALIDARG for invalid arguments");

    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr, NULL) == MA_ERROR_INVALIDARG, \
                        "ma_array_push doesn't return MA_ERROR_INVALIDARG for invalid arguments");

    /* Creating a list of 32-bit unsigned integer variants and pushing them into the array */
    for(counter = 0; counter < sizeof(test_vector_array)/sizeof(test_vector_array[0]); counter++)
    {
        TEST_ASSERT_MESSAGE(ma_variant_create_from_uint32(test_vector_array[counter], &variant_ptr) == MA_OK, \
                            "32 Bit unsigned variant could not be created");

        TEST_ASSERT_MESSAGE(ma_array_push(NULL, NULL) == MA_ERROR_INVALIDARG, \
                            "ma_array_push doesn't return MA_ERROR_INVALIDARG for invalid arguments");

        TEST_ASSERT_MESSAGE(ma_array_push(array_ptr, NULL) == MA_ERROR_INVALIDARG, \
                            "ma_array_push doesn't return MA_ERROR_INVALIDARG for invalid arguments");

        TEST_ASSERT_MESSAGE(ma_array_push(NULL, variant_ptr) == MA_ERROR_INVALIDARG, \
                            "ma_array_push doesn't return MA_ERROR_INVALIDARG for invalid arguments");

        TEST_ASSERT_MESSAGE(ma_array_push(array_ptr, variant_ptr) == MA_OK, \
                            "Variant could not be pushed into the array");

        TEST_ASSERT_MESSAGE(ma_array_size(array_ptr, &array_size) == MA_OK, \
                            "ma_array_size doesn't returns MA_OK for valid arguments");

        TEST_ASSERT_MESSAGE(counter + 1 == array_size, \
                                  "Array size is not incremented correctly after 1 variant is pushed");
        ma_variant_release(variant_ptr);
    }

    /* Retrieving elements from array using foreach Array API and verifying the value in callback function */
    TEST_ASSERT_MESSAGE(ma_array_foreach(NULL, NULL, NULL) == MA_ERROR_INVALIDARG,
                        "ma_array_foreach doesn't return MA_ERROR_INVALIDARG for invalid arguments");

    TEST_ASSERT_MESSAGE(ma_array_foreach(NULL, test_array_callback, NULL) == MA_ERROR_INVALIDARG,
                        "ma_array_foreach doesn't return MA_ERROR_INVALIDARG for invalid arguments");

    TEST_ASSERT_MESSAGE(ma_array_foreach(array_ptr, NULL, NULL) == MA_ERROR_PRECONDITION,
                        "ma_array_foreach doesn't return MA_ERROR_PRECONDITION when callback function is NULL");

    TEST_ASSERT_MESSAGE(ma_array_foreach(array_ptr, test_array_callback, NULL) == MA_OK,
                        "ma_array_foreach doesn't return MA_OK when array and callback functions are valid");

    /* Increment and Decrement operations on array of variants */

    /*Calling ma_array_inc_ref macro with address of variant as NULL*/
    TEST_ASSERT_MESSAGE(ma_array_add_ref(NULL) == MA_ERROR_INVALIDARG, \
                        "ma_variant_inc_ref doesn't return MA_ERROR_INVALIDARG when argument passed is NULL");

    /*Calling ma_array_release macro with address of variant as NULL*/
    TEST_ASSERT_MESSAGE(ma_array_release(NULL) == MA_ERROR_INVALIDARG, \
                        "ma_array_dec_ref doesn't return MA_ERROR_INVALIDARG when argument passed is NULL");

    /*Verifying that the ma_array_inc_ref/ma_array_dec_ref macro maintains correct reference count*/

    /*Incrementing the array reference count (10 times) */
    for(counter = 0; counter < 10; counter++)
    {
        TEST_ASSERT_MESSAGE(ma_array_add_ref(array_ptr) == MA_OK, \
                            "ma_array_inc_ref could not increase array reference count");
    }

    /* Decrement the array reference count (11 times). The array should be destroyed
       by the end of this for-loop */
    for(counter = 0; counter < 11; counter++)
    {
        TEST_ASSERT_MESSAGE(ma_array_release(array_ptr) == MA_OK, \
                            "ma_array_dec_ref could not decrease array reference count");
    }
}

/*
Unit tests for ma_array_is_equal API.
*/
TEST(ma_variant_array_tests, ma_array_is_equal_test)
{
    /* 'ma_variant_t' type Pointer for storing the variant address */
    ma_variant_t *variant_ptr_1 = NULL;
    ma_variant_t *variant_ptr_2 = NULL;
    ma_variant_t *variant_ptr_3 = NULL;
    ma_variant_t *variant_ptr_4 = NULL;
    ma_variant_t *variant_ptr_5 = NULL;
    ma_variant_t *variant_ptr_6 = NULL;
    ma_variant_t *variant_ptr_7 = NULL;

    /* 'ma_array_t' array type pointers. Arrays of variants for comparison */
    ma_array_t *array_ptr_lhs = NULL;
    ma_array_t *array_ptr_rhs = NULL;

    /* Comparison Result */
    ma_bool_t comparison_result = MA_FALSE;

    /* Creating variants for pushing into the arrays */
    TEST_ASSERT_MESSAGE(ma_variant_create(&variant_ptr_1) == MA_OK, "NULL variant could not be created");
    TEST_ASSERT_MESSAGE(ma_variant_create_from_uint64(0x12345678ABCDEF09ULL, &variant_ptr_2) == MA_OK, "64 Bit unsigned variant could not be created");
    TEST_ASSERT_MESSAGE(ma_variant_create_from_int64(0x12345678ABCDEF09LL, &variant_ptr_3) == MA_OK, "64 Bit unsigned variant could not be created");
    TEST_ASSERT_MESSAGE(ma_variant_create_from_double(1.23e-18, &variant_ptr_4) == MA_OK, "Double variant could not be created");
    TEST_ASSERT_MESSAGE(ma_variant_create_from_uint32(0x1234ABCDUL, &variant_ptr_5) == MA_OK, "32 Bit unsigned variant could not be created");
    TEST_ASSERT_MESSAGE(ma_variant_create_from_int32(0x7FAB0123LL, &variant_ptr_6) == MA_OK, "32 Bit signed variant could not be created");
    TEST_ASSERT_MESSAGE(ma_variant_create_from_float(123.45f, &variant_ptr_7) == MA_OK, "Float variant could not be created");

    /* Scenario 1 - Creating Arrays -> performing equality comparison on empty arrays -> Destroying arrays */
    TEST_ASSERT_MESSAGE(ma_array_create(&array_ptr_lhs) == MA_OK, "Array could not be created");
    TEST_ASSERT_MESSAGE(ma_array_create(&array_ptr_rhs) == MA_OK, "Array could not be created");

    TEST_ASSERT_MESSAGE(ma_array_is_equal(NULL, NULL, NULL) == MA_ERROR_INVALIDARG, \
                        "ma_array_is_equal doesn't return MA_ERROR_INVALIDARG for invalid arguments");

    TEST_ASSERT_MESSAGE(ma_array_is_equal(array_ptr_lhs, array_ptr_rhs, NULL) == MA_ERROR_INVALIDARG, \
                        "ma_array_is_equal doesn't return MA_ERROR_INVALIDARG for invalid arguments");

    TEST_ASSERT_MESSAGE(ma_array_is_equal(NULL, array_ptr_rhs, &comparison_result) == MA_ERROR_INVALIDARG, \
                        "ma_array_is_equal doesn't return MA_ERROR_INVALIDARG for invalid arguments");

    TEST_ASSERT_MESSAGE(ma_array_is_equal(array_ptr_lhs, NULL, &comparison_result) == MA_ERROR_INVALIDARG, \
                        "ma_array_is_equal doesn't return MA_ERROR_INVALIDARG for invalid arguments");

    TEST_ASSERT_MESSAGE(ma_array_is_equal(array_ptr_lhs, array_ptr_rhs, &comparison_result) == MA_OK, \
                        "ma_array_is_equal doesn't return MA_OK for empty arrays");

    TEST_ASSERT_MESSAGE(MA_TRUE == comparison_result, "Array Comparison result is not correct for empty arrays");

    TEST_ASSERT_MESSAGE(ma_array_release(array_ptr_lhs) == MA_OK, "Array could not be destroyed");
    TEST_ASSERT_MESSAGE(ma_array_release(array_ptr_rhs) == MA_OK, "Array could not be destroyed");

    /* Scenario 2 - Same variants present in the arrays in same sequence. Comparison should be true */
    TEST_ASSERT_MESSAGE(ma_array_create(&array_ptr_lhs) == MA_OK, "Array could not be created");
    TEST_ASSERT_MESSAGE(ma_array_create(&array_ptr_rhs) == MA_OK, "Array could not be created");

    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_lhs, variant_ptr_1) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_lhs, variant_ptr_2) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_lhs, variant_ptr_3) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_lhs, variant_ptr_4) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_lhs, variant_ptr_5) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_lhs, variant_ptr_6) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_lhs, variant_ptr_7) == MA_OK, "Variant could not be pushed into the array");

    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_rhs, variant_ptr_1) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_rhs, variant_ptr_2) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_rhs, variant_ptr_3) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_rhs, variant_ptr_4) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_rhs, variant_ptr_5) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_rhs, variant_ptr_6) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_rhs, variant_ptr_7) == MA_OK, "Variant could not be pushed into the array");

    TEST_ASSERT_MESSAGE(ma_array_is_equal(array_ptr_lhs, array_ptr_rhs, &comparison_result) == MA_OK, \
                        "ma_array_is_equal doesn't return MA_OK while comparing valid arrays");

    TEST_ASSERT_MESSAGE(MA_TRUE == comparison_result, "Array Comparison result is not correct");

    /* Scenario 3 - Comparing array with itself. Comparison should be true */
    TEST_ASSERT_MESSAGE(ma_array_is_equal(array_ptr_lhs, array_ptr_lhs, &comparison_result) == MA_OK, \
                        "ma_array_is_equal doesn't return MA_OK while comparing valid arrays");

    TEST_ASSERT_MESSAGE(MA_TRUE == comparison_result, "Array Comparison result is not correct");

    TEST_ASSERT_MESSAGE(ma_array_is_equal(array_ptr_rhs, array_ptr_rhs, &comparison_result) == MA_OK, \
                        "ma_array_is_equal doesn't return MA_OK while comparing valid arrays");

    TEST_ASSERT_MESSAGE(MA_TRUE == comparison_result, "Array Comparison result is not correct");

    TEST_ASSERT_MESSAGE(ma_array_release(array_ptr_lhs) == MA_OK, "Array could not be destroyed");
    TEST_ASSERT_MESSAGE(ma_array_release(array_ptr_rhs) == MA_OK, "Array could not be destroyed");

    /* Scenario 4 - Same variants present in the arrays in different sequence. Comparison should be false */
    TEST_ASSERT_MESSAGE(ma_array_create(&array_ptr_lhs) == MA_OK, "Array could not be created");
    TEST_ASSERT_MESSAGE(ma_array_create(&array_ptr_rhs) == MA_OK, "Array could not be created");

    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_lhs, variant_ptr_1) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_lhs, variant_ptr_2) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_lhs, variant_ptr_3) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_lhs, variant_ptr_4) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_lhs, variant_ptr_5) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_lhs, variant_ptr_6) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_lhs, variant_ptr_7) == MA_OK, "Variant could not be pushed into the array");

    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_rhs, variant_ptr_2) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_rhs, variant_ptr_1) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_rhs, variant_ptr_3) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_rhs, variant_ptr_4) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_rhs, variant_ptr_5) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_rhs, variant_ptr_6) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_rhs, variant_ptr_7) == MA_OK, "Variant could not be pushed into the array");

    TEST_ASSERT_MESSAGE(ma_array_is_equal(array_ptr_lhs, array_ptr_rhs, &comparison_result) == MA_OK, \
                        "ma_array_is_equal doesn't return MA_OK while comparing valid arrays");

    TEST_ASSERT_MESSAGE(MA_FALSE == comparison_result, "Array Comparison result is not correct");

    TEST_ASSERT_MESSAGE(ma_array_release(array_ptr_lhs) == MA_OK, "Array could not be destroyed");
    TEST_ASSERT_MESSAGE(ma_array_release(array_ptr_rhs) == MA_OK, "Array could not be destroyed");

    /* Scenario 5 - Unequal number of variants in arrays. 1 less element in RHS at the end. Comparison should be false */
    TEST_ASSERT_MESSAGE(ma_array_create(&array_ptr_lhs) == MA_OK, "Array could not be created");
    TEST_ASSERT_MESSAGE(ma_array_create(&array_ptr_rhs) == MA_OK, "Array could not be created");

    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_lhs, variant_ptr_1) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_lhs, variant_ptr_2) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_lhs, variant_ptr_3) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_lhs, variant_ptr_4) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_lhs, variant_ptr_5) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_lhs, variant_ptr_6) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_lhs, variant_ptr_7) == MA_OK, "Variant could not be pushed into the array");

    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_rhs, variant_ptr_1) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_rhs, variant_ptr_2) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_rhs, variant_ptr_3) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_rhs, variant_ptr_4) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_rhs, variant_ptr_5) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_rhs, variant_ptr_6) == MA_OK, "Variant could not be pushed into the array");

    TEST_ASSERT_MESSAGE(ma_array_is_equal(array_ptr_lhs, array_ptr_rhs, &comparison_result) == MA_OK, \
                        "ma_array_is_equal doesn't return MA_OK while comparing valid arrays");

    TEST_ASSERT_MESSAGE(MA_FALSE == comparison_result, "Array Comparison result is not correct");

    TEST_ASSERT_MESSAGE(ma_array_release(array_ptr_lhs) == MA_OK, "Array could not be destroyed");
    TEST_ASSERT_MESSAGE(ma_array_release(array_ptr_rhs) == MA_OK, "Array could not be destroyed");

    /* Scenario 6 - Unequal number of variants in arrays. 1 element in less RHS in the middle. Comparison should be false */
    TEST_ASSERT_MESSAGE(ma_array_create(&array_ptr_lhs) == MA_OK, "Array could not be created");
    TEST_ASSERT_MESSAGE(ma_array_create(&array_ptr_rhs) == MA_OK, "Array could not be created");

    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_lhs, variant_ptr_1) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_lhs, variant_ptr_2) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_lhs, variant_ptr_3) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_lhs, variant_ptr_4) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_lhs, variant_ptr_5) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_lhs, variant_ptr_6) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_lhs, variant_ptr_7) == MA_OK, "Variant could not be pushed into the array");

    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_rhs, variant_ptr_1) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_rhs, variant_ptr_2) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_rhs, variant_ptr_3) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_rhs, variant_ptr_5) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_rhs, variant_ptr_6) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_rhs, variant_ptr_7) == MA_OK, "Variant could not be pushed into the array");

    TEST_ASSERT_MESSAGE(ma_array_is_equal(array_ptr_lhs, array_ptr_rhs, &comparison_result) == MA_OK, \
                        "ma_array_is_equal doesn't return MA_OK while comparing valid arrays");

    TEST_ASSERT_MESSAGE(MA_FALSE == comparison_result, "Array Comparison result is not correct");

    TEST_ASSERT_MESSAGE(ma_array_release(array_ptr_lhs) == MA_OK, "Array could not be destroyed");
    TEST_ASSERT_MESSAGE(ma_array_release(array_ptr_rhs) == MA_OK, "Array could not be destroyed");

    /* Scenario 7 - Equal number of variants in arrays. Sequence of elements is reverse. Comparison should be false */
    TEST_ASSERT_MESSAGE(ma_array_create(&array_ptr_lhs) == MA_OK, "Array could not be created");
    TEST_ASSERT_MESSAGE(ma_array_create(&array_ptr_rhs) == MA_OK, "Array could not be created");

    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_lhs, variant_ptr_1) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_lhs, variant_ptr_2) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_lhs, variant_ptr_3) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_lhs, variant_ptr_5) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_lhs, variant_ptr_6) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_lhs, variant_ptr_7) == MA_OK, "Variant could not be pushed into the array");

    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_rhs, variant_ptr_7) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_rhs, variant_ptr_6) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_rhs, variant_ptr_5) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_rhs, variant_ptr_4) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_rhs, variant_ptr_3) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_rhs, variant_ptr_2) == MA_OK, "Variant could not be pushed into the array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_rhs, variant_ptr_1) == MA_OK, "Variant could not be pushed into the array");

    TEST_ASSERT_MESSAGE(ma_array_is_equal(array_ptr_lhs, array_ptr_rhs, &comparison_result) == MA_OK, \
                        "ma_array_is_equal doesn't return MA_OK while comparing valid arrays");

    TEST_ASSERT_MESSAGE(MA_FALSE == comparison_result, "Array Comparison result is not correct");

    TEST_ASSERT_MESSAGE(ma_array_release(array_ptr_lhs) == MA_OK, "Array could not be destroyed");
    TEST_ASSERT_MESSAGE(ma_array_release(array_ptr_rhs) == MA_OK, "Array could not be destroyed");

    ma_variant_release(variant_ptr_7);
    ma_variant_release(variant_ptr_6);
    ma_variant_release(variant_ptr_5);
    ma_variant_release(variant_ptr_4);
    ma_variant_release(variant_ptr_3);
    ma_variant_release(variant_ptr_2);
    ma_variant_release(variant_ptr_1);
}

/*
Unit tests for self-reference to within a array variant i.e. the scenario which occurs when
following sequence is followed -
1) An array of variants is created.
2) The variant is created from the array created in step 1.
3) The variant created in step 2 is again pushed into the array created in step 1.
*/
TEST(ma_variant_array_tests, ma_variant_array_self_reference_test)
{
    /* 'ma_variant_t' type Pointer for storing the base address of variant object */
    ma_variant_t *array_variant_ptr = NULL;

    /* 'ma_variant_t' type Pointer for storing the variant address */
    ma_variant_t *variant_ptr = NULL;

    /* 'ma_array_t' array type pointer */
    ma_array_t *array_ptr = NULL;

    /* 'ma_array_t' array type pointer */
    ma_array_t *array_ptr_1 = NULL;

     /* This variable should hold MA_VARTYPE_ARRAY after the ma_variant_get_type
       call. Initial value of this variable is given as MA_VARTYPE_STRING. */
    ma_vartype_t vartype_obj = MA_VARTYPE_STRING;

    /* Creating array of variants (Will be used for self reference) */
    TEST_ASSERT_MESSAGE(ma_array_create(&array_ptr) == MA_OK, \
                        "ma_array_create doesn't returns MA_OK for valid array pointer address");

    /* Creating array of variants (Should be able to successfully add a variant created from 'array_ptr' */
    TEST_ASSERT_MESSAGE(ma_array_create(&array_ptr_1) == MA_OK, \
                        "ma_array_create doesn't returns MA_OK for valid array pointer address");


    /* Creating variants of various types and pushing them into the array */

    /* NULL Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create(&variant_ptr) == MA_OK, "NULL variant could not be created");

    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr, variant_ptr) == MA_OK, "Variant could not be pushed to array");
    ma_variant_release(variant_ptr);

    /* 64 Bit unsigned integer Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_uint64(0x12345678ABCDEF09ULL, &variant_ptr) == MA_OK, \
                        "64 Bit unsigned variant could not be created");

    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr, variant_ptr) == MA_OK, "Variant could not be pushed to array");
    ma_variant_release(variant_ptr);

    /* 64 Bit signed integer Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_int64(0x12345678ABCDEF09LL, &variant_ptr) == MA_OK, \
                        "64 Bit unsigned variant could not be created");

    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr, variant_ptr) == MA_OK, "Variant could not be pushed to array");
    ma_variant_release(variant_ptr);

    /* Double Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_double(1e+37, &variant_ptr) == MA_OK, "Double variant could not be created");

    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr, variant_ptr) == MA_OK, "Variant could not be pushed to array");
    ma_variant_release(variant_ptr);

    /* 32 Bit unsigned integer Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_uint32(0x1234ABCDUL, &variant_ptr) == MA_OK, \
                        "32 Bit unsigned variant could not be created");

    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr, variant_ptr) == MA_OK, "Variant could not be pushed to array");
    ma_variant_release(variant_ptr);

    /* 32 Bit signed integer Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_int32(0x7FAB0123LL, &variant_ptr) == MA_OK, \
                        "32 Bit signed variant could not be created");

    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr, variant_ptr) == MA_OK, "Variant could not be pushed to array");
    ma_variant_release(variant_ptr);

    /* Float Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_float(123.45f, &variant_ptr) == MA_OK, \
                        "Float variant could not be created");

    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr, variant_ptr) == MA_OK, "Variant could not be pushed to array");
    ma_variant_release(variant_ptr);

    /* 16 Bit unsigned integer Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_uint16(0x1234, &variant_ptr) == MA_OK, \
                        "16 Bit unsigned variant could not be created");

    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr, variant_ptr) == MA_OK, "Variant could not be pushed to array");
    ma_variant_release(variant_ptr);

    /* 16 Bit signed integer Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_int16(0x7FAB, &variant_ptr) == MA_OK, \
                        "16 Bit signed variant could not be created");

    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr, variant_ptr) == MA_OK, "Variant could not be pushed to array");
    ma_variant_release(variant_ptr);

    /* 8 Bit unsigned integer Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_uint8(0x12, &variant_ptr) == MA_OK, \
                        "8 Bit unsigned variant could not be created");

    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr, variant_ptr) == MA_OK, "Variant could not be pushed to array");
    ma_variant_release(variant_ptr);

    /* 8 Bit signed integer Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_int8(0x7F, &variant_ptr) == MA_OK, \
                        "8 Bit signed variant could not be created");

    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr, variant_ptr) == MA_OK, "Variant could not be pushed to array");
    ma_variant_release(variant_ptr);

    /* Boolean Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_bool(123, &variant_ptr) == MA_OK, \
                        "Boolean variant could not be created");

    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr, variant_ptr) == MA_OK, "Variant could not be pushed to array");
    ma_variant_release(variant_ptr);

    /* Creating variant from the array created previously in this test. */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_array(array_ptr, &array_variant_ptr) == MA_OK, \
                        "ma_variant_create_from_array could not create variant from array");

    /* Checking the type of variant (array type) */
    TEST_ASSERT_MESSAGE(ma_variant_get_type(array_variant_ptr, &vartype_obj) == MA_OK, \
                        "ma_variant_get_type doesn't return MA_OK for Array Variant");

    TEST_ASSERT_MESSAGE(vartype_obj == MA_VARTYPE_ARRAY, \
                        "ma_variant_get_type doesn't return MA_VARTYPE_ARRAY for array variant");

    /* Pushing the variant (created from array) back into the same array...self-reference */
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr, array_variant_ptr) == MA_ERROR_INVALIDARG, \
                        "Variant incorrectly pushed to array (self-reference permitted) ");

    /* Pushing the variant (created from array) into a different array...not a self-reference */
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_1, array_variant_ptr) == MA_OK, \
                        "Variant could not be pushed to array");

    ma_variant_release(array_variant_ptr);
    ma_array_release(array_ptr_1);
    ma_array_release(array_ptr);

}


/*
Unit tests for ma_variant_inc_ref and ma_variant_dec_ref macros for a variant created from an array.
*/
TEST(ma_variant_array_tests, ma_variant_inc_ref_dec_ref_array_test)
{
    /* 'ma_variant_t' type Pointer for storing the base address of variant object */
    ma_variant_t *variant_ptr = NULL;

    /* 'ma_array_t' array type pointer */
    ma_array_t *array_ptr = NULL;

    // Loop iterator.
    ma_uint8_t counter = 0;

    /* Variable to hold the variant type */
    ma_vartype_t vartype_obj = MA_VARTYPE_NULL;

    /*Test 1 : Calling ma_variant_inc_ref macro with address of variant as NULL*/

    TEST_ASSERT_MESSAGE(ma_variant_add_ref(NULL) == MA_ERROR_INVALIDARG, \
                        "ma_variant_inc_ref doesn't return MA_ERROR_INVALIDARG when argument passed is NULL");

    /*Test 2 : Calling ma_variant_dec_ref macro with address of variant as NULL*/

    TEST_ASSERT_MESSAGE(ma_variant_release(NULL) == MA_ERROR_INVALIDARG, \
                        "ma_variant_dec_ref doesn't return MA_ERROR_INVALIDARG when argument passed is NULL");

    /*Test 3 : Verifying that the ma_variant_inc_ref/ma_variant_dec_ref macro maintains correct reference count*/

    /* Creating a array */
    TEST_ASSERT_MESSAGE(ma_array_create(&array_ptr) == MA_OK, \
                        "ma_array_create doesn't returns MA_OK for valid array I/P");

    /* Creating a array variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_array(array_ptr, &variant_ptr) == MA_OK, \
                        "ma_variant_create_from_array doesn't return MA_OK for while creating variant from array");

    /*Incrementing the variant reference count (10 times) */
    for(counter = 0; counter < 10; counter++)
    {
        TEST_ASSERT_MESSAGE(ma_variant_add_ref(variant_ptr) == MA_OK, \
                            "ma_variant_inc_ref could not increase variant reference count");

        TEST_ASSERT_MESSAGE(ma_variant_get_type(variant_ptr, &vartype_obj) == MA_OK, \
                            "The Variant type could not be retrieved");

        TEST_ASSERT_MESSAGE(vartype_obj == MA_VARTYPE_ARRAY, \
                            "ma_variant_get_type doesn't hold correct variant type");
    }

    /* Decrement the variant reference count (11 times). The variant should be destroyed
       by the end of this for-loop */
    for(counter = 0; counter < 11; counter++)
    {
        TEST_ASSERT_MESSAGE(ma_variant_get_type(variant_ptr, &vartype_obj) == MA_OK, \
                            "The Variant type could not be retrieved");

        TEST_ASSERT_MESSAGE(vartype_obj == MA_VARTYPE_ARRAY, \
                            "ma_variant_get_type doesn't hold correct variant type");

        TEST_ASSERT_MESSAGE(ma_variant_release(variant_ptr) == MA_OK, \
                            "ma_variant_dec_ref could not decrease variant reference count");
    }

    ma_array_release(array_ptr);
}

/*
Unit test for array variant of strings
*/
TEST(ma_variant_array_tests, ma_variant_char_array_test)
{
    ma_uint8_t counter = 0;

    // Array variant
    ma_variant_t *array_variant_ptr = NULL;
    ma_array_t *array_ptr = NULL;

    // Values
    const char *test_vector[] =
    {
         "MA Security Agent",
         "McAfee Security 12345654569587Agent",
         "McAfee Security 12345654569587Agent",
         "MCAFEE SECURITY AGENT",
         "MCAFEE SECURITY AGENT 1.0",
         "12345678901234567890",
         "!@@#$%%^^&*&&(*(**(**)()",
         "ABCD1234$%@$%",
         "~!@#$%^&*$^%^%$^#^%&^%&^%",
         "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghi jklmnopqrstuvwxyz1234567890!@#$%^&*()_+-=<>?:\"",
    };

    // Creating array
    TEST_ASSERT(ma_array_create(&array_ptr) == MA_OK);

    // Pushing string variants into the array
    for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        ma_variant_t *variant_ptr = NULL;

        TEST_ASSERT(ma_variant_create_from_string(test_vector[counter], strlen(test_vector[counter]), &variant_ptr) == MA_OK);
        TEST_ASSERT(ma_array_push(array_ptr, variant_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);
    }

    // Converting array to variant and then, releasing array
    TEST_ASSERT(ma_variant_create_from_array(array_ptr, &array_variant_ptr) == MA_OK);
    TEST_ASSERT(ma_array_release(array_ptr) == MA_OK);

    // Retrieving array back from array variant
    TEST_ASSERT(ma_variant_get_array(array_variant_ptr, &array_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_release(array_variant_ptr) == MA_OK);

    for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        ma_variant_t *variant_ptr = NULL;

        ma_buffer_t *buffer_ptr = NULL;
        const char *buffer_val = NULL;
        size_t size = 0;

        TEST_ASSERT(ma_array_get_element_at(array_ptr, counter + 1, &variant_ptr) == MA_OK); // Array index starts at 1
        TEST_ASSERT(ma_variant_get_string_buffer(variant_ptr, &buffer_ptr) == MA_OK);
        TEST_ASSERT(ma_buffer_get_string(buffer_ptr, &buffer_val, &size) == MA_OK);

        TEST_ASSERT(0 == strcmp(buffer_val, test_vector[counter]));

        ma_buffer_release(buffer_ptr);
        ma_variant_release(variant_ptr);
    }

    TEST_ASSERT(ma_array_release(array_ptr) == MA_OK);
}

#ifdef MA_HAS_WCHAR_T

/*
Unit test for array variant of wide character strings
*/
TEST(ma_variant_array_tests, ma_variant_wide_char_array_test)
{
    ma_uint8_t counter = 0;

    // Array variant
    ma_variant_t *array_variant_ptr = NULL;
    ma_array_t *array_ptr = NULL;

    // Values
    const wchar_t *test_vector[] =
    {
        L"MA Security Agent",
        L"McAfee Security 12345654569587Agent",
        L"McAfee Security 12345654569587Agent",
        L"MCAFEE SECURITY AGENT",
        L"MCAFEE SECURITY AGENT 1.0",
        L"12345678901234567890\x211C",
        L"!@@#$%%^^&*&&(*(**(**)(\x0041\x030A",
        L"ABCD1234$%@$%",
        L"~!@#$%^&*$^%^%$^#^%&^%&^%",
        L"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghi jklmnopqrs\x09058 tuvwxyz1234567890!@#$%^&*()_+-=<>?:\"",
        L"\0x9053\0x53EF\0x02FE\0x02B2\0x5E38\0x025D"
    };

    // Creating array
    TEST_ASSERT(ma_array_create(&array_ptr) == MA_OK);

    // Pushing string variants into the array
    for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        ma_variant_t *variant_ptr = NULL;

        TEST_ASSERT(ma_variant_create_from_wstring(test_vector[counter], wcslen(test_vector[counter]), &variant_ptr) == MA_OK);
        TEST_ASSERT(ma_array_push(array_ptr, variant_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);
    }

    // Converting array to variant and then, releasing array
    TEST_ASSERT(ma_variant_create_from_array(array_ptr, &array_variant_ptr) == MA_OK);
    TEST_ASSERT(ma_array_release(array_ptr) == MA_OK);

    // Retrieving array back from array variant
    TEST_ASSERT(ma_variant_get_array(array_variant_ptr, &array_ptr) == MA_OK);
    TEST_ASSERT(ma_variant_release(array_variant_ptr) == MA_OK);

    for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        ma_variant_t *variant_ptr = NULL;

        ma_wbuffer_t *wbuffer_ptr = NULL;
        const wchar_t *wbuffer_val = NULL;
        size_t size = 0;

        TEST_ASSERT(ma_array_get_element_at(array_ptr, counter + 1, &variant_ptr) == MA_OK); // Array index starts at 1
        TEST_ASSERT(ma_variant_get_wstring_buffer(variant_ptr, &wbuffer_ptr) == MA_OK);
        TEST_ASSERT(ma_wbuffer_get_string(wbuffer_ptr, &wbuffer_val, &size) == MA_OK);

        TEST_ASSERT(0 == wcscmp(wbuffer_val, test_vector[counter]));

        ma_wbuffer_release(wbuffer_ptr);
        ma_variant_release(variant_ptr);
    }

    TEST_ASSERT(ma_array_release(array_ptr) == MA_OK);
}

#endif





