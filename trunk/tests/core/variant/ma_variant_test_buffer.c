/*****************************************************************************
This file contains the unit tests for the character and wide character buffer
APIs from the Variant library.
******************************************************************************/
#include "unity.h"
#include "unity_fixture.h"
#include "ma/ma_buffer.h"

TEST_GROUP_RUNNER(ma_variant_buffer_test_group)
{
    do {RUN_TEST_CASE(ma_variant_buffer_tests, ma_buffer_create_set_get_string_test)} while (0);
    do {RUN_TEST_CASE(ma_variant_buffer_tests, ma_buffer_create_set_get_raw_test)} while (0);
    do {RUN_TEST_CASE(ma_variant_buffer_tests, ma_buffer_is_equal_test)} while (0);
    do {RUN_TEST_CASE(ma_variant_buffer_tests, ma_buffer_is_equal_test_raw_stream)} while (0);
    do {RUN_TEST_CASE(ma_variant_buffer_tests, ma_buffer_inc_ref_dec_ref_test)} while (0);

    #ifdef MA_HAS_WCHAR_T
        do {RUN_TEST_CASE(ma_variant_buffer_tests, ma_wbuffer_create_and_set_test)} while (0);
        do {RUN_TEST_CASE(ma_variant_buffer_tests, ma_wbuffer_is_equal_test)} while (0);
        do {RUN_TEST_CASE(ma_variant_buffer_tests, ma_wbuffer_inc_ref_dec_ref_test)} while (0);
    #endif
}

TEST_SETUP(ma_variant_buffer_tests)
{
}

TEST_TEAR_DOWN(ma_variant_buffer_tests)
{
}


/*
Unit tests for following macros -
1) ma_buffer_create
2) ma_buffer_set
3) ma_buffer_get_string
4) ma_buffer_size
*/
TEST(ma_variant_buffer_tests, ma_buffer_create_set_get_string_test)
{
    /* 'ma_buffer_t' type Pointer for storing the base address of buffer */
    ma_buffer_t *buffer_ptr = NULL;

    /* Loop iterator. */
    ma_uint8_t counter = 0;

    /* To hold the string and its size, as returned by the ma_buffer_get_string
       macro */
    const char *buffer_val = "";
    size_t string_size = 0;

    /* To hold the size of the buffer */
    size_t buffer_size = 0;

    struct LookupTable
    {
        size_t test_buffer_size;
        char *test_string;
        size_t test_string_size;
        ma_error_t ret_value;
    };

    const struct LookupTable test_vector[] =
    {
        { 20,             "MA Security Agent",                                                                    17,                  MA_OK               },
        { 40,             "McAfee Security 12345654569587Agent",                                                  35,                  MA_OK               },
        { 20,             "MCAFEE SECURITY AGENT",                                                                21,                  MA_ERROR_INVALIDARG },
        { 26,             "MCAFEE SECURITY AGENT 1.0",                                                            25,                  MA_OK               },
        { 21,             "12345678901234567890",                                                                 20,                  MA_OK               },
        { 25,             "!@@#$%%^^&*&&(*(**(**)()",                                                             24,                  MA_OK               },
        { 15,             "ABCD1234$%@$%",                                                                        13,                  MA_OK               },
        { 30,             "~!@#$%^&*$^%^%$^#^%&^%&^%",                                                            25,                  MA_OK               },
        { 0,              "",                                                                                     0,                   MA_OK               },
        { 100,            "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghi jklmnopqrstuvwxyz1234567890!@#$%^&*()_+-=<>?:\"",  82,                  MA_OK               },
    };


    /*--------------------------------------------------------------------------*/
    /*
    Test 1 : Verify that the ma_buffer_create macro returns MA_ERROR_INVALIDARG
    when the address of ma_buffer_t type pointer passed to the macro is NULL.
    Calling ma_buffer_create macro passing the NULL as the address of the
    ma_buffer_t type pointer and known buffer length
    */
    TEST_ASSERT(ma_buffer_create(NULL, 10) == MA_ERROR_INVALIDARG);

    /*--------------------------------------------------------------------------*/
    /*
    Test 2 : Verify that the ma_buffer_create macro returns MA_ERROR_INVALIDARG
    when the size of the buffer to be created, is 0. Calling ma_buffer_create
    macro to create a buffer of size 0.
    */
    TEST_ASSERT(ma_buffer_create(&buffer_ptr, 0) == MA_OK);
    ma_buffer_release(buffer_ptr);

    /*--------------------------------------------------------------------------*/
    /*
    Test 3 : Verify that the ma_buffer_set macro returns MA_ERROR_INVALIDARG
    when the address of ma_buffer_t type pointer passed to the macro is NULL.
    Calling ma_buffer_set macro with the address of ma_buffer_t type pointer
    as NULL
    */
    TEST_ASSERT(ma_buffer_set(NULL, "MA Security Agent", 22) == MA_ERROR_INVALIDARG);

    /*--------------------------------------------------------------------------*/
    /*
    Test 4 : Verify that the ma_buffer_set macro returns MA_ERROR_INVALIDARG
    when the string to copied to the buffer is NULL.
    */

    /* Creating a buffer of size 30 */
    TEST_ASSERT(ma_buffer_create(&buffer_ptr, 30) == MA_OK);

    /* Calling ma_buffer_size with NULL as address of buffer pointer */
    TEST_ASSERT(ma_buffer_size(NULL, &buffer_size) == MA_ERROR_INVALIDARG);

    /* Calling ma_buffer_size with NULL pointer to hold the size */
    TEST_ASSERT(ma_buffer_size(buffer_ptr, NULL) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_buffer_size(buffer_ptr, &buffer_size) == MA_OK);
    TEST_ASSERT(30 == buffer_size);

    /* Calling ma_buffer_set macro with the string to copied as NULL */
    TEST_ASSERT(ma_buffer_set(buffer_ptr, NULL, 10) == MA_ERROR_INVALIDARG);

    /*--------------------------------------------------------------------------*/
    /*
    Test 5 : Verify that the ma_buffer_set macro returns MA_ERROR_INVALIDARG
    when the string to copied is more than size of the buffer.
    */

    /* Use the same buffer which is created in Test 4 */
    TEST_ASSERT(ma_buffer_set(buffer_ptr, "This is the test for Variant Library", 37) == MA_ERROR_INVALIDARG);

    /*--------------------------------------------------------------------------*/
    /* Test 6 : Calling ma_buffer_set macro when the size of the string to be copied is 0 */

    /* Use the same buffer which is created in Test 4 */
    TEST_ASSERT(ma_buffer_set(buffer_ptr, "MA Security Agent", 0) == MA_OK);

    /*--------------------------------------------------------------------------*/
    /*Test 7 : Verify ma_buffer_get_string macro for various valid and invalid scenarios. */

    /* Use the same buffer which is created in Test 4 */

    TEST_ASSERT(ma_buffer_set(buffer_ptr, "MA Security Agent", 17) == MA_OK);
    TEST_ASSERT(ma_buffer_get_string(buffer_ptr, &buffer_val, &string_size) == MA_OK);
    TEST_ASSERT(17 == string_size);
    TEST_ASSERT_EQUAL_MEMORY("MA Security Agent", buffer_val, 17);
    TEST_ASSERT(ma_buffer_get_string(NULL, (const char**)&buffer_val, &string_size) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_buffer_get_string(buffer_ptr, NULL, &string_size) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_buffer_get_string(buffer_ptr, &buffer_val, NULL) == MA_ERROR_INVALIDARG);

    /* Deallocating buffer which was allocated in Test 4. */
    ma_buffer_release(buffer_ptr);

    /*--------------------------------------------------------------------------*/
    /*Test 8 : Testing the functionality for various buffer sizes and values */

    for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        TEST_ASSERT(ma_buffer_create(&buffer_ptr, test_vector[counter].test_buffer_size) == MA_OK);

        TEST_ASSERT(ma_buffer_size(buffer_ptr, &buffer_size) == MA_OK);

        TEST_ASSERT(test_vector[counter].test_buffer_size == buffer_size);

        TEST_ASSERT(ma_buffer_set(buffer_ptr, test_vector[counter].test_string, test_vector[counter].test_string_size) == test_vector[counter].ret_value);

        if(test_vector[counter].ret_value == MA_OK)
        {
            TEST_ASSERT(ma_buffer_get_string(buffer_ptr, &buffer_val, &string_size) == MA_OK);

            TEST_ASSERT(test_vector[counter].test_string_size == string_size);

            if(string_size) {
                TEST_ASSERT_EQUAL_MEMORY(test_vector[counter].test_string, buffer_val, test_vector[counter].test_string_size);
            }
        }

        /* Deallocating buffer */
        ma_buffer_release(buffer_ptr);
    }

    /*--------------------------------------------------------------------------*/
}


/*
Unit tests for following macros -
1) ma_buffer_create
2) ma_buffer_set
3) ma_buffer_get_raw
4) ma_buffer_size
*/
TEST(ma_variant_buffer_tests, ma_buffer_create_set_get_raw_test)
{
    /* 'ma_buffer_t' type Pointer for storing the base address of buffer */
    ma_buffer_t *buffer_ptr = NULL;

    /* Loop iterator. */
    ma_uint16_t counter = 0;

    /* To hold the raw stream and its size, as returned by the ma_buffer_get_raw
       macro */
    const unsigned char *raw_val = 0;
    size_t raw_size = 0;

    /* To hold the size of the buffer */
    size_t buffer_size = 0;

    /* For storing the result of input and output raw stream comparison. */
    ma_bool_t comp_result = 1;

    /* An instance of this structure acts as the raw buffer stream */
    struct test_struct
    {
        ma_uint8_t int_data;
        char char_data[300];
        ma_float_t float_data;
        ma_bool_t boolean_data;
    };

    struct test_struct raw_buffer = { 0x12,
                                      "This is a Next-Gen McAfee Agent software - Variant Library raw buffer test."
                                      "This is a Next-Gen McAfee Agent software - Variant Library raw buffer test."
                                      "This is a Next-Gen McAfee Agent software - Variant Library raw buffer test."
                                      "!@#$%^&*()_+01234567890123456789",
                                      2.3f,
                                      1};


    /*--------------------------------------------------------------------------*/
    /* Creating buffer for size more than the size of raw stream */
    TEST_ASSERT(ma_buffer_create(&buffer_ptr, sizeof(raw_buffer) + 100) == MA_OK);
    TEST_ASSERT(ma_buffer_size(buffer_ptr, &buffer_size) == MA_OK);
    TEST_ASSERT(sizeof(raw_buffer) + 100 == buffer_size);
    TEST_ASSERT(ma_buffer_set(buffer_ptr, (const char *)&raw_buffer, sizeof(raw_buffer)) == MA_OK);

    /* Invalid condition check */
    TEST_ASSERT(ma_buffer_get_raw(NULL, &raw_val, &raw_size) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_buffer_get_raw(buffer_ptr, NULL, &raw_size) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_buffer_get_raw(buffer_ptr, (const unsigned char**)&raw_val, NULL) == MA_ERROR_INVALIDARG);

    /* Valid condition check */
    TEST_ASSERT(ma_buffer_get_raw(buffer_ptr, &raw_val, &raw_size) == MA_OK);

    /* Size of raw stream should be equal to size of buffer */
    TEST_ASSERT(buffer_size == raw_size);

    for(counter = 0; counter < sizeof(raw_buffer); counter++)
    {
        // Typecasting structure instance to raw stream
        if(((const unsigned char*)&raw_buffer)[counter] != raw_val[counter])
        {
            comp_result = 0;
        }
    }

    TEST_ASSERT(comp_result == 1);

    ma_buffer_release(buffer_ptr);
    /*--------------------------------------------------------------------------*/
}


/*
Unit tests for ma_buffer_is_equal macro (for strings).
*/
TEST(ma_variant_buffer_tests, ma_buffer_is_equal_test)
{
    /* 'ma_buffer_t' type Pointer for storing the base address of buffer */
    ma_buffer_t *buffer_ptr_1 = NULL;
    ma_buffer_t *buffer_ptr_2 = NULL;

    /* Loop iterator. */
    ma_uint8_t counter = 0;

    /* Variable to hold the results */
    ma_bool_t comparison_result = MA_FALSE;

    struct LookupTable
    {
        size_t test_buffer_size_1;
        char *test_string_1;
        size_t test_buffer_size_2;
        char *test_string_2;
        ma_error_t ret_value;
        int exp_result;
    };

    const struct LookupTable test_vector[] =
    {
        { 50, "MA Security Agent",                              40, "MA Security Agent",                     MA_OK,                 MA_FALSE            },
        { 30, "McAfee Security 12345654569587Agent",            30, "McAfee Security 12345654569587",        MA_OK,                 MA_TRUE             },
        { 25, "890!@#$%^&*()_+-=<>?:\"",                        25, "890!@#$%^&*()_+-=<>?:\"",               MA_OK,                 MA_TRUE             },
        { 50, "MCAFEE SECURITY AGENT",                          50, "MCAFEE SECURITY AGENt",                 MA_OK,                 MA_FALSE            },
        { 25, "12345678901234567890",                           25, "12345678901234567890",                  MA_OK,                 MA_TRUE             },
        { 50, "~!@#$%^&*$^%^%$^#^%&^%&^%",                      50, "!@#$%^&*$^%^%$^#^%&^%&^%",              MA_OK,                 MA_FALSE            },
        { 50, "MCAFEE SECURITY AGENT 1.0",                      50, "MCAFEE SECURITY AGENT1.0",              MA_OK,                 MA_FALSE            },
        { 50, "!@@#$%%^^&*&&(*(**(**)()",                       50, "!@@#$%%^^&*&&(*(**(**)()",              MA_OK,                 MA_TRUE             },
        { 50, "ABCD1234$%@$%",                                  50, "ABCD1234$%@$%",                         MA_OK,                 MA_TRUE             },
        { 10, "",                                               10, "",                                      MA_OK,                 MA_TRUE             },
    };

    /*--------------------------------------------------------------------------*/

    for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        TEST_ASSERT(ma_buffer_create(&buffer_ptr_1, test_vector[counter].test_buffer_size_1) == MA_OK);
        TEST_ASSERT(ma_buffer_create(&buffer_ptr_2, test_vector[counter].test_buffer_size_2) == MA_OK);
        TEST_ASSERT(ma_buffer_set(buffer_ptr_1, test_vector[counter].test_string_1, test_vector[counter].test_buffer_size_1) == MA_OK);
        TEST_ASSERT(ma_buffer_set(buffer_ptr_2, test_vector[counter].test_string_2, test_vector[counter].test_buffer_size_2) == MA_OK);
        TEST_ASSERT(test_vector[counter].ret_value == ma_buffer_is_equal(buffer_ptr_1, buffer_ptr_2, &comparison_result));
        TEST_ASSERT(comparison_result == test_vector[counter].exp_result);

        /* Deallocating buffer */
        ma_buffer_release(buffer_ptr_1);
        ma_buffer_release(buffer_ptr_2);
    }
    /*--------------------------------------------------------------------------*/

}

/*
Unit tests for ma_buffer_is_equal macro (for raw stream).
*/
TEST(ma_variant_buffer_tests, ma_buffer_is_equal_test_raw_stream)
{
    /* 'ma_buffer_t' type Pointer for storing the base address of buffer */
    ma_buffer_t *buffer_ptr_1 = NULL;
    ma_buffer_t *buffer_ptr_2 = NULL;
    ma_buffer_t *buffer_ptr_3 = NULL;
    ma_buffer_t *buffer_ptr_4 = NULL;

    /* Variable to hold the results */
    ma_bool_t comparison_result = MA_FALSE;

    struct test_struct
    {
        ma_uint8_t int_data;
        char char_data[300];
        ma_float_t float_data;
        ma_bool_t boolean_data;
    };

    struct test_struct raw_buffer_1 = { 0x12,
                                        "This is a Next-Gen McAfee Agent software - Variant Library raw buffer test."
                                        "This is a Next-Gen McAfee Agent software - Variant Library raw buffer test."
                                        "This is a Next-Gen McAfee Agent software - Variant Library raw buffer test."
                                        "!@#$%^&*()_+01234567890123456789",
                                        2.3f,
                                        1};

    // Same as raw_buffer_1
    struct test_struct raw_buffer_2 =  { 0x12,
                                        "This is a Next-Gen McAfee Agent software - Variant Library raw buffer test."
                                        "This is a Next-Gen McAfee Agent software - Variant Library raw buffer test."
                                        "This is a Next-Gen McAfee Agent software - Variant Library raw buffer test."
                                        "!@#$%^&*()_+01234567890123456789",
                                        2.3f,
                                        1};

    // String member is different from raw_buffer_1
    struct test_struct raw_buffer_3 =  { 0x12,
                                        "This is a Next-Gen McAfee Agent software - Variant Library raw buffer test."
                                        "This is a Next-Gen McAfee Agent software - Variant Library raw buffer test."
                                        "This is a Next-Gen McAfee Agent software - Variant Library raw buffer test."
                                        "!@#$%^&*()_+0123456789012345678",
                                        2.3f,
                                        1};

    // Float member is different from raw_buffer_1
    struct test_struct raw_buffer_4 =  { 0x12,
                                        "This is a Next-Gen McAfee Agent software - Variant Library raw buffer test."
                                        "This is a Next-Gen McAfee Agent software - Variant Library raw buffer test."
                                        "This is a Next-Gen McAfee Agent software - Variant Library raw buffer test."
                                        "!@#$%^&*()_+01234567890123456789",
                                        -2.3f,
                                        1};

    /*--------------------------------------------------------------------------*/

    // Creating buffers of size more than size of the raw stream
    TEST_ASSERT(ma_buffer_create(&buffer_ptr_1, sizeof(raw_buffer_1) + 1) == MA_OK);
    TEST_ASSERT(ma_buffer_create(&buffer_ptr_2, sizeof(raw_buffer_2) + 1) == MA_OK);
    TEST_ASSERT(ma_buffer_create(&buffer_ptr_3, sizeof(raw_buffer_3) + 1) == MA_OK);
    TEST_ASSERT(ma_buffer_create(&buffer_ptr_4, sizeof(raw_buffer_4) + 1) == MA_OK);

    // Setting known raw stream in buffers
    TEST_ASSERT(ma_buffer_set(buffer_ptr_1, (const char *)&raw_buffer_1, sizeof(raw_buffer_1)) == MA_OK);
    TEST_ASSERT(ma_buffer_set(buffer_ptr_2, (const char *)&raw_buffer_2, sizeof(raw_buffer_2)) == MA_OK);
    TEST_ASSERT(ma_buffer_set(buffer_ptr_3, (const char *)&raw_buffer_3, sizeof(raw_buffer_3)) == MA_OK);
    TEST_ASSERT(ma_buffer_set(buffer_ptr_4, (const char *)&raw_buffer_4, sizeof(raw_buffer_4)) == MA_OK);

    /* Checking for invalid arguments */
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_buffer_is_equal(NULL, buffer_ptr_2, &comparison_result));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_buffer_is_equal(buffer_ptr_1, NULL, &comparison_result));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_buffer_is_equal(buffer_ptr_1, buffer_ptr_2, NULL));

    /* Valid equality comparisons */
    TEST_ASSERT(MA_OK == ma_buffer_is_equal(buffer_ptr_1, buffer_ptr_2, &comparison_result));
    TEST_ASSERT(comparison_result == MA_TRUE);

    TEST_ASSERT(MA_OK == ma_buffer_is_equal(buffer_ptr_1, buffer_ptr_3, &comparison_result));
    TEST_ASSERT(comparison_result == MA_FALSE);

    TEST_ASSERT(MA_OK == ma_buffer_is_equal(buffer_ptr_1, buffer_ptr_4, &comparison_result));
    TEST_ASSERT(comparison_result == MA_FALSE);

    /* Deallocating buffer */
    ma_buffer_release(buffer_ptr_1);
    ma_buffer_release(buffer_ptr_2);
    ma_buffer_release(buffer_ptr_3);
    ma_buffer_release(buffer_ptr_4);
    /*--------------------------------------------------------------------------*/
}

/*
Unit tests for ma_buffer_inc_ref and ma_buffer_dec_ref macros.
*/
TEST(ma_variant_buffer_tests, ma_buffer_inc_ref_dec_ref_test)
{
    /* 'ma_buffer_t' type Pointer for storing the base address of buffer */
    ma_buffer_t *buffer_ptr = NULL;

    // Loop iterator.
    ma_uint8_t counter = 0;

    /* Variable to hold the size of the buffer */
    size_t buffer_size = 0;

    /*--------------------------------------------------------------------------*/
    /*Test 1 : Calling ma_buffer_inc_ref macro with address of buffer as NULL*/

    TEST_ASSERT(ma_buffer_add_ref(NULL) == MA_ERROR_INVALIDARG);

    /*--------------------------------------------------------------------------*/
    /*Test 2 : Calling ma_buffer_dec_ref macro with address of buffer as NULL*/

    TEST_ASSERT(ma_buffer_release(NULL) == MA_ERROR_INVALIDARG);

    /*--------------------------------------------------------------------------*/
    /*Test 3 : Verifying that the ma_buffer_inc_ref/ma_buffer_dec_ref macro maintains correct reference count*/

    TEST_ASSERT(ma_buffer_create(&buffer_ptr, 10) == MA_OK);

    /*Incrementing the buffer reference count (10 times) */
    for(counter = 0; counter < 10; counter++)
    {
        TEST_ASSERT(ma_buffer_add_ref(buffer_ptr) == MA_OK);
        TEST_ASSERT(ma_buffer_size(buffer_ptr, &buffer_size) == MA_OK);
        TEST_ASSERT(buffer_size == 10);
    }

    /* Decrementing the buffer reference count (11 times). The buffer should be
       destroyed by the end of this for-loop */
    for(counter = 0; counter < 11; counter++)
    {
        TEST_ASSERT(ma_buffer_size(buffer_ptr, &buffer_size) == MA_OK);
        TEST_ASSERT(buffer_size == 10);
        TEST_ASSERT(ma_buffer_release(buffer_ptr) == MA_OK);
    }

    /*--------------------------------------------------------------------------*/
    /*
    The buffer is destroyed at the end of previous for-loop. Incrementing and decrementing
    an already destroyed buffer, is not valid. Therefore, it is not verified in this test.
    */
}


/*
Unit tests for following macros -
1) ma_wbuffer_create
2) ma_wbuffer_set
3) ma_wbuffer_get_string
4) ma_wbuffer_size
*/

#ifdef MA_HAS_WCHAR_T

TEST(ma_variant_buffer_tests, ma_wbuffer_create_and_set_test)
{
    /* 'ma_wbuffer_t' type Pointer for storing the base address of buffer */
    ma_wbuffer_t *wbuffer_ptr = NULL;

    /* Loop iterator. */
    ma_uint8_t counter = 0;

    /* To hold the string and its size, as returned by the ma_buffer_get_string
       macro */
    const wchar_t *wbuffer_val = L"";
    size_t wstring_size = 0;

    /* To hold the size of the buffer */
    size_t wbuffer_size = 0;

    struct LookupTable
    {
        size_t test_wbuffer_size;
        wchar_t *test_wstring;
        size_t test_wstring_size;
        ma_error_t ret_value;
    };

    const struct LookupTable test_vector[] =
    {
        { 20,             L"MA Security Agent",                                                                    17,                  MA_OK               },
        { 40,             L"McAfee Security 12345654569587Agent",                                                  35,                  MA_OK               },
        { 20,             L"MCAFEE SECURITY AGENT",                                                                21,                  MA_ERROR_INVALIDARG },
        { 26,             L"MCAFEE SECURITY AGENT 1.0",                                                            25,                  MA_OK               },
        { 21,             L"1234567890123456789\x0041",                                                            20,                  MA_OK               },
        { 25,             L"!@@#$%%^^&*&&(*(**(**)()",                                                             24,                  MA_OK               },
        { 15,             L"ABCD1234$%@$%",                                                                        13,                  MA_OK               },
        { 30,             L"~!@#$%^&*$^%^%$^#^%&^%&^%",                                                            25,                  MA_OK               },
        { 1,              L"",                                                                                     0,                   MA_OK },
        { 100,            L"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghi jklmnopqrstuvwxyz1234567890!@#$%^&*()_+-=<>?:\"",  82,                  MA_OK               },
    };

    /*--------------------------------------------------------------------------*/
    /*
    Test 1 : Verify that the ma_wbuffer_create macro returns MA_ERROR_INVALIDARG
    when the address of ma_wbuffer_t type pointer passed to the macro is NULL.
    Calling ma_wbuffer_create macro passing the NULL as the address of the
    ma_wbuffer_t type pointer and known buffer length
    */
    TEST_ASSERT(ma_wbuffer_create(NULL, 10) == MA_ERROR_INVALIDARG);

    /*--------------------------------------------------------------------------*/
    /*
    Test 2 : Verify that the ma_wbuffer_create macro returns MA_ERROR_INVALIDARG
    when the size of the buffer to be created, is 0. Calling ma_wbuffer_create
    macro to create a buffer of size 0.
    */
    TEST_ASSERT(ma_wbuffer_create(&wbuffer_ptr, 0) == MA_OK);
	ma_wbuffer_release(wbuffer_ptr);

    /*--------------------------------------------------------------------------*/
    /*
    Test 3 : Verify that the ma_wbuffer_set macro returns MA_ERROR_INVALIDARG
    when the address of ma_wbuffer_t type pointer passed to the macro is NULL.
    Calling ma_wbuffer_set macro with the address of ma_wbuffer_t type pointer
    as NULL
    */
    TEST_ASSERT(ma_wbuffer_set(NULL, L"MA Security Agent", 22) == MA_ERROR_INVALIDARG);

    /*--------------------------------------------------------------------------*/
    /*
    Test 4 : Verify that the ma_wbuffer_set macro returns MA_ERROR_INVALIDARG
    when the string to copied to the buffer is NULL.
    */

    /* Creating a buffer of size 30 */
    TEST_ASSERT(ma_wbuffer_create(&wbuffer_ptr, 30) == MA_OK);

    /* Calling ma_wbuffer_size with NULL as address of buffer pointer */
    TEST_ASSERT(ma_wbuffer_size(NULL, &wbuffer_size) == MA_ERROR_INVALIDARG);

    /* Calling ma_wbuffer_size with NULL pointer to hold the size */
    TEST_ASSERT(ma_wbuffer_size(wbuffer_ptr, NULL) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_wbuffer_size(wbuffer_ptr, &wbuffer_size) == MA_OK);
    TEST_ASSERT(30 * sizeof(wchar_t) == wbuffer_size);

    /* Calling ma_wbuffer_set macro with the string to copied as NULL */
    TEST_ASSERT(ma_wbuffer_set(wbuffer_ptr, NULL, 10) == MA_ERROR_INVALIDARG);

    /*--------------------------------------------------------------------------*/
    /*
    Test 5 : Verify that the ma_wbuffer_set macro returns MA_ERROR_INVALIDARG
    when the string to copied is more than size of the buffer.
    */

    /* Use the same buffer which is created in Test 4 */

    TEST_ASSERT(ma_wbuffer_set(wbuffer_ptr, L"This is the test for Variant Library", 37) == MA_ERROR_INVALIDARG);

    /*--------------------------------------------------------------------------*/
    /* Test 6 : Calling ma_wbuffer_set macro when the size of the string to be copied is 0 */

    /* Use the same buffer which is created in Test 4 */

    TEST_ASSERT(ma_wbuffer_set(wbuffer_ptr, L"MA Security Agent", 0) == MA_OK);

    /*--------------------------------------------------------------------------*/
    /*Test 7 : Verify ma_wbuffer_get_string macro for various valid and invalid scenarios. */

    /* Use the same buffer which is created in Test 4 */
    TEST_ASSERT(ma_wbuffer_set(wbuffer_ptr, L"MA Security Agent", 17) == MA_OK);
    TEST_ASSERT(ma_wbuffer_get_string(wbuffer_ptr, &wbuffer_val, &wstring_size) == MA_OK);
    TEST_ASSERT(17 == wstring_size);
    TEST_ASSERT_EQUAL_MEMORY(L"MA Security Agent", wbuffer_val, 17 * sizeof(wchar_t));

    TEST_ASSERT(ma_wbuffer_get_string(NULL, &wbuffer_val, &wstring_size) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_wbuffer_get_string(wbuffer_ptr, NULL, &wstring_size) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_wbuffer_get_string(wbuffer_ptr, &wbuffer_val, NULL) == MA_ERROR_INVALIDARG);

    /* Deallocating buffer which was allocated in Test 4 */
    ma_wbuffer_release(wbuffer_ptr);

    /*--------------------------------------------------------------------------*/
    /*Test 8 : Testing the functionality for various buffer sizes and values */

    for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        TEST_ASSERT(ma_wbuffer_create(&wbuffer_ptr, test_vector[counter].test_wbuffer_size) == MA_OK);

        TEST_ASSERT(ma_wbuffer_size(wbuffer_ptr, &wbuffer_size) == MA_OK);

        TEST_ASSERT(test_vector[counter].test_wbuffer_size * sizeof(wchar_t) == wbuffer_size);

        TEST_ASSERT(ma_wbuffer_set(wbuffer_ptr, test_vector[counter].test_wstring, test_vector[counter].test_wstring_size) == test_vector[counter].ret_value);

        if(test_vector[counter].ret_value == MA_OK)
        {
            TEST_ASSERT(ma_wbuffer_get_string(wbuffer_ptr, &wbuffer_val, &wstring_size) == MA_OK);
            TEST_ASSERT(test_vector[counter].test_wstring_size == wstring_size);

            if(wstring_size)
            {
                TEST_ASSERT_EQUAL_MEMORY(test_vector[counter].test_wstring, wbuffer_val, test_vector[counter].test_wstring_size * sizeof(wchar_t));
            }
        }

        /* Deallocating buffer */
        ma_wbuffer_release(wbuffer_ptr);
    }

    /*--------------------------------------------------------------------------*/
}

/*
Unit tests for ma_wbuffer_is_equal macro.
*/

TEST(ma_variant_buffer_tests, ma_wbuffer_is_equal_test)
{
    /* 'ma_wbuffer_t' type Pointer for storing the base address of buffer */
    ma_wbuffer_t *wbuffer_ptr_1 = NULL;
    ma_wbuffer_t *wbuffer_ptr_2 = NULL;

    /* Loop iterator. */
    ma_uint8_t counter = 0;

    /* Variable to hold the results */
    ma_bool_t comparison_result = MA_FALSE;

    struct LookupTable
    {
        size_t test_wbuffer_size_1;
        wchar_t *test_wstring_1;
        size_t test_wbuffer_size_2;
        wchar_t *test_wstring_2;
        ma_error_t ret_value;
        int exp_result;
    };

    const struct LookupTable test_vector[] =
    {
        { 50, L"MA Security Agent",                              40, L"MA Security Agent",                     MA_OK,                 MA_FALSE            },
        { 50, L"McAfee Security 12345654569587Agent",            50, L"McAfee Security 12345654569587",        MA_OK,                 MA_FALSE            },
        { 50, L"MCAFEE SECURITY AGENT",                          50, L"MCAFEE SECURITY AGENt",                 MA_OK,                 MA_FALSE            },
        { 50, L"MCAFEE SECURITY AGENT 1.0",                      50, L"MCAFEE SECURITY AGENT1.0",              MA_OK,                 MA_FALSE            },
        { 25, L"12345678901234567890",                           25, L"12345678901234567890",                  MA_OK,                 MA_TRUE             },
        { 50, L"!@@#$%%^^&*&&(*(**(**)()\x212B",                 50, L"!@@#$%%^^&*&&(*(**(**)()\x212B",        MA_OK,                 MA_TRUE             },
        { 50, L"ABCD1234$%@$%",                                  50, L"ABCD1234$%@$%",                         MA_OK,                 MA_TRUE             },
        { 50, L"~!@#$%^&*$^%^%$^#^%&^%&^%",                      50, L"!@#$%^&*$^%^%$^#^%&^%&^%",              MA_OK,                 MA_FALSE            },
        { 10, L"",                                               10, L"",                                      MA_OK,                 MA_TRUE             },
        { 25, L"890!@#$%^&*()_+-=<>?:\"",                        25, L"890!@#$%^&*()_+-=<>?:\"",               MA_OK,                 MA_TRUE             },
    };

    /*--------------------------------------------------------------------------*/

    for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        TEST_ASSERT(ma_wbuffer_create(&wbuffer_ptr_1, test_vector[counter].test_wbuffer_size_1) == MA_OK);
        TEST_ASSERT(ma_wbuffer_create(&wbuffer_ptr_2, test_vector[counter].test_wbuffer_size_2) == MA_OK);
        TEST_ASSERT(ma_wbuffer_set(wbuffer_ptr_1, test_vector[counter].test_wstring_1, test_vector[counter].test_wbuffer_size_1) == MA_OK);
        TEST_ASSERT(ma_wbuffer_set(wbuffer_ptr_2, test_vector[counter].test_wstring_2, test_vector[counter].test_wbuffer_size_2) == MA_OK);
        TEST_ASSERT(test_vector[counter].ret_value == ma_wbuffer_is_equal(wbuffer_ptr_1, wbuffer_ptr_2, &comparison_result));
        TEST_ASSERT(comparison_result == test_vector[counter].exp_result);

        /* Deallocating buffer which was allocated in Test 4 */
        ma_wbuffer_release(wbuffer_ptr_1);
        ma_wbuffer_release(wbuffer_ptr_2);
    }
    /*--------------------------------------------------------------------------*/
}

/*
Unit tests for ma_wbuffer_inc_ref and ma_wbuffer_dec_ref macros.
*/

TEST(ma_variant_buffer_tests, ma_wbuffer_inc_ref_dec_ref_test)
{
    /* 'ma_wbuffer_t' type Pointer for storing the base address of buffer */
    ma_wbuffer_t *wbuffer_ptr = NULL;

    // Loop iterator.
    ma_uint8_t counter = 0;

    /* Variable to hold the size of the buffer */
    size_t wbuffer_size = 0;

    /*--------------------------------------------------------------------------*/
    /*Test 1 : Calling ma_wbuffer_inc_ref macro with address of buffer as NULL*/

    TEST_ASSERT(ma_wbuffer_add_ref(NULL) == MA_ERROR_INVALIDARG);

    /*--------------------------------------------------------------------------*/
    /*Test 2 : Calling ma_wbuffer_dec_ref macro with address of buffer as NULL*/

    TEST_ASSERT(ma_wbuffer_release(NULL) == MA_ERROR_INVALIDARG);

    /*--------------------------------------------------------------------------*/
    /*Test 3 : Verifying that the ma_wbuffer_inc_ref/ma_wbuffer_dec_ref macro maintains correct reference count*/

    TEST_ASSERT(ma_wbuffer_create(&wbuffer_ptr, 10) == MA_OK);

    /*Incrementing the buffer reference count (10 times) */
    for(counter = 0; counter < 10; counter++)
    {
        TEST_ASSERT(ma_wbuffer_add_ref(wbuffer_ptr) == MA_OK);
        TEST_ASSERT(ma_wbuffer_size(wbuffer_ptr, &wbuffer_size) == MA_OK);
        TEST_ASSERT(wbuffer_size == 10 * sizeof(wchar_t));
    }

    /* Decrementing the buffer reference count (11 times). The buffer should be
       destroyed by the end of this for-loop */
    for(counter = 0; counter < 11; counter++)
    {
        TEST_ASSERT(ma_wbuffer_size(wbuffer_ptr, &wbuffer_size) == MA_OK);
        TEST_ASSERT(wbuffer_size == 10 * sizeof(wchar_t));
        TEST_ASSERT(ma_wbuffer_release(wbuffer_ptr) == MA_OK);
    }

    /*--------------------------------------------------------------------------*/
    /*
    The buffer is destroyed at the end of previous for-loop. Incrementing and decrementing
    an already destroyed buffer, is not valid. Therefore, it is not verified in this test.
    */
}

#endif


