#include "ma/variant.hxx"
#include "gtest/gtest.h"

/* Test Vector for foreach API test */
ma_uint32_t test_vector_array[] = { 0xFFFFFFFFUL, 0x12345678UL, 0xFEDCBA09UL, 0x87654321UL,
                                    0x01010101UL, 0x10101010UL, 0x34567890UL, 0x00000000UL,
                                    0x00000001UL  };

/*
callback function for iterating through the array. This callback function
verifies the variant value against the global list of expected values.
*/
void test_array_callback(mileaccess::ma::variant value)
{
    static ma_uint8_t count = 0;
    ASSERT_EQ(test_vector_array[count], mileaccess::ma::get<ma_uint32_t>(value));
    count++; // for next iteration
}



TEST(ma_variant_array_tests, ma_variant_create_get_from_array_test)
{
   mileaccess::ma::array a;

   
   //Creating variants of various types -> pushing them into the array -> Checking that the size of array is
   //incremented by 1
  

   //size is 0 before any variant is added
   ASSERT_EQ(0, a.size());

   //NULL Variant
   {
       mileaccess::ma::variant v;
       a.push(v);
       ASSERT_EQ(1, a.size());
   }

   //64 Bit unsigned integer Variant
   {
       mileaccess::ma::variant v((ma_uint64_t)0x12345678ABCDEF09ULL);
       a.push(v);
       ASSERT_EQ(2, a.size());
   }

   //64 Bit signed integer Variant
   {
       mileaccess::ma::variant v((ma_int64_t)0x12345678ABCDEF09LL);
       a.push(v);
       ASSERT_EQ(3, a.size());
   }

   //Double Variant
   {
       mileaccess::ma::variant v(1.23e+18);
       a.push(v);
       ASSERT_EQ(4, a.size());
   }

   //32 Bit unsigned integer Variant
   {
       mileaccess::ma::variant v((ma_uint32_t)0x1234ABCDUL);
       a.push(v);
       ASSERT_EQ(5, a.size());
   }

   //32 Bit signed integer Variant
   {
       mileaccess::ma::variant v((ma_int32_t)0xEFAB0123LL);
       a.push(v);
       ASSERT_EQ(6, a.size());
   }

   //Float Variant
   {
       mileaccess::ma::variant v(123.45f);
       a.push(v);
       ASSERT_EQ(7, a.size());
   }

   //16 Bit unsigned integer Variant
   {
       mileaccess::ma::variant v((ma_uint16_t)0x1234);
       a.push(v);
       ASSERT_EQ(8, a.size());
   }

   //16 Bit signed integer Variant
   {
       mileaccess::ma::variant v((ma_int16_t)-23456);
       a.push(v);
       ASSERT_EQ(9, a.size());
   }

   //8 Bit unsigned integer Variant
   {
       mileaccess::ma::variant v((ma_uint8_t)0xAB);
       a.push(v);
       ASSERT_EQ(10, a.size());
   }

   //8 Bit signed integer Variant
   {
       mileaccess::ma::variant v((ma_int8_t)-34);
       a.push(v);
       ASSERT_EQ(11, a.size());
   }

   //String Variant
   {
       mileaccess::ma::variant v("MA Security Agent");
       a.push(v);
       ASSERT_EQ(12, a.size());
   }

   //Wide Character String Variant
   {
       mileaccess::ma::variant v(L"MA Security Agent$%$%\x212B");
       a.push(v);
       ASSERT_EQ(13, a.size());
   }

   //Creating Variant from the array
   mileaccess::ma::variant v_array(a);

   //Checking the type of variant (array type)
   ASSERT_EQ(MA_VARTYPE_ARRAY, v_array.type());

   //Retrieving array back from the Variant
   mileaccess::ma::array new_array = mileaccess::ma::get<mileaccess::ma::array>(v_array);
   ASSERT_EQ(a, new_array);

   //Reading variant at index 1
   {
       mileaccess::ma::variant v = new_array.at(1);
       ASSERT_EQ(MA_VARTYPE_NULL, v.type());
   }

   //Reading variant at index 2
   {
       mileaccess::ma::variant v = new_array.at(2);
       ASSERT_EQ(MA_VARTYPE_UINT64, v.type());
       ASSERT_EQ(0x12345678ABCDEF09ULL, mileaccess::ma::get<ma_uint64_t>(v));
   }

   //Reading variant at index 3
   {
       mileaccess::ma::variant v = new_array.at(3);
       ASSERT_EQ(MA_VARTYPE_INT64, v.type());
       ASSERT_EQ(0x12345678ABCDEF09LL, mileaccess::ma::get<ma_int64_t>(v));
   }

   //Reading variant at index 4
   {
       mileaccess::ma::variant v = new_array.at(4);
       ASSERT_EQ(MA_VARTYPE_DOUBLE, v.type());
       ASSERT_EQ(1.23e+18, mileaccess::ma::get<ma_double_t>(v));
   }

   //Reading variant at index 5
   {
       mileaccess::ma::variant v = new_array.at(5);
       ASSERT_EQ(MA_VARTYPE_UINT32, v.type());
       ASSERT_EQ(0x1234ABCDUL, mileaccess::ma::get<ma_uint32_t>(v));
   }

   //Reading variant at index 6
   {
       mileaccess::ma::variant v = new_array.at(6);
       ASSERT_EQ(MA_VARTYPE_INT32, v.type());
       ASSERT_EQ(0xEFAB0123, mileaccess::ma::get<ma_int32_t>(v));
   }

   //Reading variant at index 7
   {
       mileaccess::ma::variant v = new_array.at(7);
       ASSERT_EQ(MA_VARTYPE_FLOAT, v.type());
       ASSERT_EQ(123.45f, mileaccess::ma::get<ma_float_t>(v));
   }

   //Reading variant at index 8
   {
       mileaccess::ma::variant v = new_array.at(8);
       ASSERT_EQ(MA_VARTYPE_UINT16, v.type());
       ASSERT_EQ(0x1234, mileaccess::ma::get<ma_uint16_t>(v));
   }

 //  Reading variant at index 9
   {
       mileaccess::ma::variant v = new_array[9]; // Using [] operator
       ASSERT_EQ(MA_VARTYPE_INT16, v.type());
       ASSERT_EQ(-23456, mileaccess::ma::get<ma_int16_t>(v));
   }

   //Reading variant at index 10
   {
       mileaccess::ma::variant v = new_array[10]; // Using [] operator
       ASSERT_EQ(MA_VARTYPE_UINT8, v.type());
       ASSERT_EQ(0xAB, mileaccess::ma::get<ma_uint8_t>(v));
   }

   //Reading variant at index 11
   {
       mileaccess::ma::variant v = new_array[11]; // Using [] operator
       ASSERT_EQ(MA_VARTYPE_INT8, v.type());
       ASSERT_EQ(-34, mileaccess::ma::get<ma_int8_t>(v));
   }

   //Reading variant at index 12
   {
       mileaccess::ma::variant v = new_array[12]; // Using [] operator
       ASSERT_EQ(MA_VARTYPE_STRING, v.type());
       ASSERT_STREQ("MA Security Agent", mileaccess::ma::get<std::string>(v).data());
   }

   //Reading variant at index 13
   {
       mileaccess::ma::variant v = new_array[13]; // Using [] operator
       ASSERT_EQ(MA_VARTYPE_STRING, v.type());
       ASSERT_STREQ(L"MA Security Agent$%$%\x212B", mileaccess::ma::get<std::wstring>(v).data());
   }
}

TEST(ma_variant_array_tests, ma_variant_foreach_element_from_array_test)
{
    mileaccess::ma::array a;

    /* size is 0 before any variant is added */
    ASSERT_EQ(0, a.size());

    /* Creating a list of 32-bit unsigned integer variants and pushing them into the array */
    for(ma_uint8_t counter = 0; counter < sizeof(test_vector_array)/sizeof(test_vector_array[0]); counter++)
    {
        mileaccess::ma::variant v(test_vector_array[counter]);
        a.push(v);
        ASSERT_EQ(counter + 1, a.size());
    }

    /* Retrieving elements from array using foreach Array API and verifying the value in
       callback function */
    a.for_each(test_array_callback);
}


/*
Unit tests for equality of arrays.
*/
TEST(ma_variant_array_tests, ma_array_is_equal_test)
{
    /* Creating variants for pushing into the arrays */
    mileaccess::ma::variant v1;
    mileaccess::ma::variant v2((ma_uint64_t)0x12345678ABCDEF09ULL);
    mileaccess::ma::variant v3((ma_int64_t)0x12345678ABCDEF09LL);
    mileaccess::ma::variant v4(1.23e-18);
    mileaccess::ma::variant v5((ma_uint32_t)0x1234ABCDUL);
    mileaccess::ma::variant v6((ma_int32_t)0xEFAB0123LL);
    mileaccess::ma::variant v7(123.45f);

    /* Scenario 1 - performing equality comparison on empty arrays */
    {
        mileaccess::ma::array a_lhs, a_rhs;
        ASSERT_TRUE(a_lhs == a_rhs);
    }

    /* Scenario 2 - Same variants present in the arrays in same sequence. Comparison should be true */
    {
        mileaccess::ma::array a_lhs, a_rhs;

        a_lhs.push(v1); a_lhs.push(v2); a_lhs.push(v3); a_lhs.push(v4); a_lhs.push(v5);
        a_lhs.push(v6); a_lhs.push(v7);

        a_rhs.push(v1); a_rhs.push(v2); a_rhs.push(v3); a_rhs.push(v4); a_rhs.push(v5);
        a_rhs.push(v6); a_rhs.push(v7);

        ASSERT_TRUE(a_lhs == a_rhs);

        ASSERT_TRUE(a_lhs == a_lhs); // Comparing array with itself
        ASSERT_TRUE(a_rhs == a_rhs); // Comparing array with itself
    }

    /* Scenario 3 - Same variants present in the arrays in different sequence. Comparison should be false */
    {
        mileaccess::ma::array a_lhs, a_rhs;

        a_lhs.push(v1); a_lhs.push(v2); a_lhs.push(v3); a_lhs.push(v4); a_lhs.push(v5);
        a_lhs.push(v6); a_lhs.push(v7);

        a_rhs.push(v2); a_rhs.push(v1); a_rhs.push(v3); a_rhs.push(v4); a_rhs.push(v5);
        a_rhs.push(v6); a_rhs.push(v7);

        ASSERT_FALSE(a_lhs == a_rhs);
    }

    /* Scenario 4 - Unequal number of variants in arrays. 1 less element in RHS at the end. Comparison should be false */
    {
        mileaccess::ma::array a_lhs, a_rhs;

        a_lhs.push(v1); a_lhs.push(v2); a_lhs.push(v3); a_lhs.push(v4); a_lhs.push(v5);
        a_lhs.push(v6); a_lhs.push(v7);

        a_rhs.push(v1); a_rhs.push(v2); a_rhs.push(v3); a_rhs.push(v4); a_rhs.push(v5);
        a_rhs.push(v6);

        ASSERT_FALSE(a_lhs == a_rhs);
    }

    /* Scenario 5 - Unequal number of variants in arrays. 1 element in less RHS in the middle. Comparison should be false */
    {
        mileaccess::ma::array a_lhs, a_rhs;

        a_lhs.push(v1); a_lhs.push(v2); a_lhs.push(v3); a_lhs.push(v4); a_lhs.push(v5);
        a_lhs.push(v6); a_lhs.push(v7);

        a_rhs.push(v1); a_rhs.push(v2); a_rhs.push(v3); a_rhs.push(v5); a_rhs.push(v6); a_rhs.push(v7);

        ASSERT_FALSE(a_lhs == a_rhs);
    }

    /* Scenario 6 - Equal number of variants in arrays. Sequence of elements is reverse. Comparison should be false */
    {
        mileaccess::ma::array a_lhs, a_rhs;

        a_lhs.push(v1); a_lhs.push(v2); a_lhs.push(v3); a_lhs.push(v4); a_lhs.push(v5);
        a_lhs.push(v6); a_lhs.push(v7);

        a_rhs.push(v7); a_rhs.push(v6); a_rhs.push(v5); a_rhs.push(v4); a_rhs.push(v3);
        a_rhs.push(v2); a_rhs.push(v1);

        ASSERT_FALSE(a_lhs == a_rhs);
    }
}

TEST(ma_variant_array_tests, ma_variant_char_array_test)
{
    // Values
    const char *test_vector[] =
    {
         "MA Security Agent",
         "McAfee Security 12345654569587Agent",
         "McAfee Security 12345654569587Agent",
         "MCAFEE SECURITY AGENT",
         "MCAFEE SECURITY AGENT 1.0",
         "12345678901234567890",
         "!@@#$%%^^&*&&(*(**(**)()",
         "ABCD1234$%@$%",
         "~!@#$%^&*$^%^%$^#^%&^%&^%",
         "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghi jklmnopqrstuvwxyz1234567890!@#$%^&*()_+-=<>?:\"",
    };

    // Creating array
    mileaccess::ma::array a;

    // Pushing string variants into the array
    for(ma_uint8_t counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        mileaccess::ma::variant v(test_vector[counter]);
        a.push(v);
    }

    // Converting array to variant
    mileaccess::ma::variant v_array(a);

    // Retrieving array back from the Variant
    mileaccess::ma::array new_array = mileaccess::ma::get<mileaccess::ma::array>(v_array);
    ASSERT_EQ(a, new_array);

    for(ma_uint8_t counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        mileaccess::ma::variant v = new_array.at(counter + 1);
        ASSERT_EQ(MA_VARTYPE_STRING, v.type());
        ASSERT_STREQ(test_vector[counter], mileaccess::ma::get<std::string>(v).data());
    }
}


TEST(ma_variant_array_tests, ma_variant_wide_char_array_test)
{
   // Values
   const wchar_t *test_vector[] =
   {
       L"MA Security Agent",
       L"McAfee Security 12345654569587Agent",
       L"McAfee Security 12345654569587Agent",
       L"MCAFEE SECURITY AGENT",
       L"MCAFEE SECURITY AGENT 1.0",
       L"12345678901234567890\x211C",
       L"!@@#$%%^^&*&&(*(**(**)(\x0041\x030A",
       L"ABCD1234$%@$%",
       L"~!@#$%^&*$^%^%$^#^%&^%&^%",
       L"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghi jklmnopqrs\x09058 tuvwxyz1234567890!@#$%^&*()_+-=<>?:\"",
       L"\0x9053\0x53EF\0x02FE\0x02B2\0x5E38\0x025D"
   };

   // Creating array
   mileaccess::ma::array a;

   // Pushing string variants into the array
   for(ma_uint8_t counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
   {
       mileaccess::ma::variant v(test_vector[counter]);
       a.push(v);
   }

   // Converting array to variant
   mileaccess::ma::variant v_array(a);

   // Retrieving array back from the Variant
   mileaccess::ma::array new_array = mileaccess::ma::get<mileaccess::ma::array>(v_array);
   ASSERT_EQ(a, new_array);

   for(ma_uint8_t counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
   {
       mileaccess::ma::variant v = new_array.at(counter + 1);
       ASSERT_EQ(MA_VARTYPE_STRING, v.type());
       ASSERT_STREQ(test_vector[counter], mileaccess::ma::get<std::wstring>(v).data());
   }
}




