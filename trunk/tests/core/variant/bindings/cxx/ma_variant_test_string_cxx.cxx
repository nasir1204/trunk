#include "ma/variant.hxx"
#include "gtest/gtest.h"

TEST(ma_variant_string_tests, ma_variant_empty_string_test)
{
    mileaccess::ma::variant v1("");
    mileaccess::ma::variant v2("     ");
    ASSERT_FALSE(v1 == v2);

    mileaccess::ma::variant v3(L"");
    mileaccess::ma::variant v4(L"     ");
    ASSERT_FALSE(v3 == v4);
}

TEST(ma_variant_string_tests, ma_variant_create_and_get_from_string_test)
{
    const char *test_vector1[] =
    {
        "MA Security Agent",
        "McAfee Security 12345654569587Agent",
        "McAfee Security 12345654569587Agent",
        "MCAFEE SECURITY AGENT",
        "MCAFEE SECURITY AGENT 1.0",
        "12345678901234567890",
        "!@@#$%%^^&*&&(*(**(**)()",
        "ABCD1234$%@$%",
        "~!@#$%^&*$^%^%$^#^%&^%&^%",
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghi jklmnopqrstuvwxyz1234567890!@#$%^&*()_+-=<>?:\"",
    };

    std::string test_vector2[] =
    {
        "MA Security Agent",
        "McAfee Security 12345654569587Agent",
        "McAfee Security 12345654569587Agent",
        "MCAFEE SECURITY AGENT",
        "MCAFEE SECURITY AGENT 1.0",
        "12345678901234567890",
        "!@@#$%%^^&*&&(*(**(**)()",
        "ABCD1234$%@$%",
        "~!@#$%^&*$^%^%$^#^%&^%&^%",
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghi jklmnopqrstuvwxyz1234567890!@#$%^&*()_+-=<>?:\"",
    };

    std::wstring test_vector3[] =
    {
        L"MA Security Agent",
        L"McAfee Security 12345654569587Agent",
        L"McAfee Security 12345654569587Agent",
        L"MCAFEE SECURITY AGENT",
        L"MCAFEE SECURITY AGENT 1.0",
        L"12345678901234567890\x211C",
        L"!@@#$%%^^&*&&(*(**(**)(\x0041\x030A",
        L"ABCD1234$%@$%",
        L"~!@#$%^&*$^%^%$^#^%&^%&^%",
        L"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghi jklmnopqrstuvwxyz1234567890!@#$%^&*()_+-=<>?:\"",
    };

    // Verify for const char* string
    for(ma_uint8_t counter = 0; counter < sizeof(test_vector1)/sizeof(test_vector1[0]); counter++)
    {
        mileaccess::ma::variant v(test_vector1[counter]);
        ASSERT_EQ(MA_VARTYPE_STRING, v.type());
        ASSERT_STREQ(test_vector1[counter], mileaccess::ma::get<std::string>(v).data());
    }

    // Verify for std::string
    for(ma_uint8_t counter = 0; counter < sizeof(test_vector2)/sizeof(test_vector2[0]); counter++)
    {
        mileaccess::ma::variant v(test_vector2[counter]);
        ASSERT_EQ(MA_VARTYPE_STRING, v.type());
        ASSERT_STREQ(test_vector2[counter].data(), mileaccess::ma::get<std::string>(v).data());
    }

    // Verify for partial std::string (7 chars starting at 3)
    for(ma_uint8_t counter = 0; counter < sizeof(test_vector2)/sizeof(test_vector2[0]); counter++)
    {
        mileaccess::ma::variant v(test_vector2[counter], 3, 7);
        ASSERT_EQ(MA_VARTYPE_STRING, v.type());
        ASSERT_STREQ(test_vector2[counter].substr(3, 7).data(), mileaccess::ma::get<std::string>(v).data());
    }

    // Verify for wide char string
    for(ma_uint8_t counter = 0; counter < sizeof(test_vector3)/sizeof(test_vector3[0]); counter++)
    {
        mileaccess::ma::variant v(test_vector3[counter].data());
        ASSERT_EQ(MA_VARTYPE_STRING, v.type());
        ASSERT_STREQ(test_vector3[counter].data(), mileaccess::ma::get<std::wstring>(v).data());
    }

    // Verify for partial wide char string (7 chars starting at 3)
    for(ma_uint8_t counter = 0; counter < sizeof(test_vector3)/sizeof(test_vector3[0]); counter++)
    {
        mileaccess::ma::variant v(test_vector3[counter], 3, 7);
        ASSERT_EQ(MA_VARTYPE_STRING, v.type());
        ASSERT_STREQ(test_vector3[counter].substr(3, 7).data(), mileaccess::ma::get<std::wstring>(v).data());
    }
}

TEST(ma_variant_string_tests, ma_string_variant_is_equal_test)
{
   {
       mileaccess::ma::variant v1("MA Security Agent");
       mileaccess::ma::variant v2("MA Security Agent");
       ASSERT_TRUE(v1 == v2);
   }

   {
       mileaccess::ma::variant v1("MA Security Agent");
       mileaccess::ma::variant v2(L"MA Security Agent");
       ASSERT_TRUE(v1 == v2);
   }

   {
       mileaccess::ma::variant v1("MA Security Agent");
       mileaccess::ma::variant v2("Ma Security Agent");
       ASSERT_FALSE(v1 == v2);
   }

   {
       mileaccess::ma::variant v1("!@@#$%%^^&*&&(*(**(**)()");
       mileaccess::ma::variant v2("!@@#$%%^^&*&&(*(**(**)()");
       ASSERT_TRUE(v1 == v2);
   }

   {
       mileaccess::ma::variant v1("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghi j klmnopqrstuvwxyz1234567890!@#$%^&*()_+-=<>?:\"");
       mileaccess::ma::variant v2(L"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghi j klmnopqrstuvwxyz1234567890!@#$%^&*()_+-=<>?:\"");
       ASSERT_TRUE(v1 == v2);
   }

   {
       mileaccess::ma::variant v1("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghi j klmnopqrstuvwxyz1234567890!@#$%^&*()_+-=<>?:\"");
       mileaccess::ma::variant v2("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghij klmnopqrstuvwxyz1234567890!@#$%^&*()_+-=<>?:\"");
       ASSERT_FALSE(v1 == v2);
   }

   {
       mileaccess::ma::variant v1("MCAFEE SECURITY AGENT 1.0");
       mileaccess::ma::variant v2("MCAFEE SECURITY AGENT 1.0 ");
       ASSERT_FALSE(v1 == v2);
   }

   {
       mileaccess::ma::variant v1(L"ABCD1234$%@$%\x212B");
       mileaccess::ma::variant v2(L"ABCD1234$%@$%\x212B");
       ASSERT_TRUE(v1 == v2);
   }

   {
      unsigned char euro_utf8[] = {0xe2, 0x82, 0xac, 0}; /* U+20AC, aka Euro Sign */

      mileaccess::ma::variant v1((const char *)euro_utf8);
      mileaccess::ma::variant v2(L"\x20AC");
      ASSERT_TRUE(v1 == v2);
   }
}



