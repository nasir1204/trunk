#include "ma/variant.hxx"
#include "gtest/gtest.h"

TEST(ma_variant_table_tests, ma_variant_create_get_from_table_test)
{
   /* Creating table of variants */
   mileaccess::ma::table t;

   /*
   Creating variants of various types -> pushing them into the table -> Checking that the size of
   table is incremented by 1
   */

   /* number of keys in the table are 0 before any variant is added (Getting the keys in an array and
   verifying that the array size is 0)
   */
   {
       mileaccess::ma::array t_size = t.keys();
       ASSERT_EQ(0, t_size.size());
   }

   /* NULL Variant */
   {
       mileaccess::ma::variant v;
       t.set_entry("First Entry", v);

       mileaccess::ma::array t_size = t.keys();
       ASSERT_EQ(1, t_size.size());
   }

   /* 64 Bit unsigned integer Variant */
   {
       mileaccess::ma::variant v((ma_uint64_t)0x12345678ABCDEF09ULL);
       t.set_entry("Second Entry", v);

       mileaccess::ma::array t_size = t.keys();
       ASSERT_EQ(2, t_size.size());
   }

   /* 64 Bit signed integer Variant */
   {
       mileaccess::ma::variant v((ma_int64_t)0x12345678ABCDEF09LL);
       t.set_entry("Third Entry", v);

       mileaccess::ma::array t_size = t.keys();
       ASSERT_EQ(3, t_size.size());
   }

   /* Double Variant */
   {
       mileaccess::ma::variant v(1.23e+18);
       t.set_entry("Fourth Entry", v);

       mileaccess::ma::array t_size = t.keys();
       ASSERT_EQ(4, t_size.size());
   }

   /* 32 Bit unsigned integer Variant */
   {
       mileaccess::ma::variant v((ma_uint32_t)0x1234ABCDUL);
       t.set_entry("Fifth Entry", v);

       mileaccess::ma::array t_size = t.keys();
       ASSERT_EQ(5, t_size.size());
   }

   /* 32 Bit signed integer Variant */
   {
       mileaccess::ma::variant v((ma_int32_t)0x34AB0123);
       t.set_entry("Sixth Entry", v);

       mileaccess::ma::array t_size = t.keys();
       ASSERT_EQ(6, t_size.size());
   }

   /* Float Variant */
   {
       mileaccess::ma::variant v(123.45f);
       t.set_entry("Seventh Entry", v);

       mileaccess::ma::array t_size = t.keys();
       ASSERT_EQ(7, t_size.size());
   }

   /* 16 Bit unsigned integer Variant */
   {
       mileaccess::ma::variant v((ma_uint16_t)0x1234);
       t.set_entry("Eighth Entry", v);

       mileaccess::ma::array t_size = t.keys();
       ASSERT_EQ(8, t_size.size());
   }

   /* 16 Bit signed integer Variant */
   {
       mileaccess::ma::variant v((ma_int16_t)0x2345);
       t.set_entry("Ninth Entry", v);

       mileaccess::ma::array t_size = t.keys();
       ASSERT_EQ(9, t_size.size());
   }

   /* 8 Bit unsigned integer Variant */
   {
       mileaccess::ma::variant v((ma_uint8_t)0xFF);
       t.set_entry("Tenth Entry", v);

       mileaccess::ma::array t_size = t.keys();
       ASSERT_EQ(10, t_size.size());
   }

   /* 8 Bit signed integer Variant */
   {
       mileaccess::ma::variant v((ma_int8_t)0x35);
       t.set_entry("Eleventh Entry", v);

       mileaccess::ma::array t_size = t.keys();
       ASSERT_EQ(11, t_size.size());
   }

   /* String Variant */
   {
       mileaccess::ma::variant v("MA Security Agent");
       t.set_entry(L"Twelveth Entry", v);    //Wide char key

       mileaccess::ma::array t_size = t.keys();
       ASSERT_EQ(12, t_size.size());
   }

   /* Wide Character String Variant */
   {
       mileaccess::ma::variant v(L"MA Security Agent$%$%\x212B");
       t.set_entry(L"\x2345Thirteenth Entry", v);  //Wide char key

       mileaccess::ma::array t_size = t.keys();
       ASSERT_EQ(13, t_size.size());
   }

   /* Creating Variant from the table */
   mileaccess::ma::variant v_table(t);

   /* Checking the type of variant (table type) */
   ASSERT_EQ(MA_VARTYPE_TABLE, v_table.type());

   /* Retrieving table back from the Variant */
   mileaccess::ma::table new_table = mileaccess::ma::get<mileaccess::ma::table>(v_table);

   /* Reading variant for key 'First Entry' */
   {
       mileaccess::ma::variant v = new_table.get_entry("First Entry");
       ASSERT_EQ(MA_VARTYPE_NULL, v.type());
   }

   /* Reading variant for key 'Second Entry' */
   {
       mileaccess::ma::variant v = new_table.get_entry("Second Entry");
       ASSERT_EQ(MA_VARTYPE_UINT64, v.type());
       ASSERT_EQ(0x12345678ABCDEF09ULL, mileaccess::ma::get<ma_uint64_t>(v));
   }

   /* Reading variant for key 'Third Entry' */
   {
       mileaccess::ma::variant v = new_table.get_entry("Third Entry");
       ASSERT_EQ(MA_VARTYPE_INT64, v.type());
       ASSERT_EQ(0x12345678ABCDEF09LL, mileaccess::ma::get<ma_int64_t>(v));
   }

   /* Reading variant for key 'Fourth Entry' */
   {
       mileaccess::ma::variant v = new_table.get_entry("Fourth Entry");
       ASSERT_EQ(MA_VARTYPE_DOUBLE, v.type());
       ASSERT_DOUBLE_EQ(1.23e+18, mileaccess::ma::get<ma_double_t>(v));
   }

   /* Reading variant for key 'Fifth Entry' */
   {
       mileaccess::ma::variant v = new_table.get_entry("Fifth Entry");
       ASSERT_EQ(MA_VARTYPE_UINT32, v.type());
       ASSERT_EQ(0x1234ABCDUL, mileaccess::ma::get<ma_uint32_t>(v));
   }

   /* Reading variant for key 'Sixth Entry' */
   {
       mileaccess::ma::variant v = new_table.get_entry("Sixth Entry");
       ASSERT_EQ(MA_VARTYPE_INT32, v.type());
       ASSERT_EQ(0x34AB0123, mileaccess::ma::get<ma_int32_t>(v));
   }

   /* Reading variant for key 'Seventh Entry' (Key is a variant) */
   {
       mileaccess::ma::variant v_key("Seventh Entry");
       mileaccess::ma::variant v = new_table.get_entry(v_key);
       ASSERT_EQ(MA_VARTYPE_FLOAT, v.type());
       ASSERT_FLOAT_EQ(123.45f, mileaccess::ma::get<ma_float_t>(v));
   }

   /* Reading variant for key 'Eighth Entry' */
   {
       mileaccess::ma::variant v = new_table.get_entry("Eighth Entry");
       ASSERT_EQ(MA_VARTYPE_UINT16, v.type());
       ASSERT_EQ(0x1234, mileaccess::ma::get<ma_uint16_t>(v));
   }

   /* Reading variant for key 'Ninth Entry' */
   {
       mileaccess::ma::variant v = new_table.get_entry("Ninth Entry");
       ASSERT_EQ(MA_VARTYPE_INT16, v.type());
       ASSERT_EQ(0x2345, mileaccess::ma::get<ma_int16_t>(v));
   }

   /* Reading variant for key 'Tenth Entry' */
   {
       mileaccess::ma::variant v = new_table.get_entry("Tenth Entry");
       ASSERT_EQ(MA_VARTYPE_UINT8, v.type());
       ASSERT_EQ(0xFF, mileaccess::ma::get<ma_uint8_t>(v));
   }

   /* Reading variant for key 'Eleventh Entry' */
   {
       mileaccess::ma::variant v = new_table.get_entry("Eleventh Entry");
       ASSERT_EQ(MA_VARTYPE_INT8, v.type());
       ASSERT_EQ(0x35, mileaccess::ma::get<ma_int8_t>(v));
   }

   /* Reading variant for key 'Twelveth Entry' */
   {
       mileaccess::ma::variant v = new_table.get_entry(L"Twelveth Entry");
       ASSERT_EQ(MA_VARTYPE_STRING, v.type());
       ASSERT_STREQ("MA Security Agent", mileaccess::ma::get<std::string>(v).data());
   }

   /* Reading variant for key 'Thirteenth Entry' */
   {
       mileaccess::ma::variant v = new_table.get_entry(L"\x2345Thirteenth Entry");
       ASSERT_EQ(MA_VARTYPE_STRING, v.type());
       ASSERT_STREQ(L"MA Security Agent$%$%\x212B", mileaccess::ma::get<std::wstring>(v).data());
   }
}

/*
Unit tests for equality of tables.
*/
TEST(ma_variant_table_tests, ma_table_is_equal_test)
{
    /* Creating variants for pushing into the tables */
    mileaccess::ma::variant v1;
    mileaccess::ma::variant v2((ma_uint64_t)0x12345678ABCDEF09ULL);
    mileaccess::ma::variant v3((ma_int64_t)0x12345678ABCDEF09LL);
    mileaccess::ma::variant v4(1.23e-18);
    mileaccess::ma::variant v5((ma_uint32_t)0x1234ABCDUL);
    mileaccess::ma::variant v6((ma_int32_t)0xEFAB0123LL);
    mileaccess::ma::variant v7(123.45f);

    /* Scenario 1 - performing equality comparison on empty arrays */
    {
        mileaccess::ma::table t_lhs, t_rhs;
        ASSERT_TRUE(t_lhs == t_rhs);
    }

    /* Scenario 2 - Same variants present in the Tables in same sequence. LHS is a normal string while
       RHS is wide char string */
    {
        mileaccess::ma::table t_lhs, t_rhs;

        t_lhs.set_entry("1", v1); t_lhs.set_entry("2", v2); t_lhs.set_entry("3", v3); t_lhs.set_entry("4", v4);
        t_lhs.set_entry("5", v5); t_lhs.set_entry("6", v6); t_lhs.set_entry("7", v7);

        t_rhs.set_entry(L"1", v1); t_rhs.set_entry(L"2", v2); t_rhs.set_entry(L"3", v3); t_rhs.set_entry(L"4", v4);
        t_rhs.set_entry(L"5", v5); t_rhs.set_entry(L"6", v6); t_rhs.set_entry(L"7", v7);

        ASSERT_TRUE(t_lhs == t_rhs);

        ASSERT_TRUE(t_lhs == t_lhs); // Comparing table with itself
        ASSERT_TRUE(t_rhs == t_rhs); // Comparing table with itself
    }

    /* Scenario 3 - Same variants present in the tables in different sequence. Comparison should be true */
    {
        mileaccess::ma::table t_lhs, t_rhs;

        t_lhs.set_entry("1", v1); t_lhs.set_entry("2", v2); t_lhs.set_entry("3", v3); t_lhs.set_entry("4", v4);
        t_lhs.set_entry("5", v5); t_lhs.set_entry("6", v6); t_lhs.set_entry("7", v7);

        t_rhs.set_entry("1", v1); t_rhs.set_entry("2", v2); t_rhs.set_entry("3", v3); t_rhs.set_entry("4", v4);
        t_rhs.set_entry("5", v5); t_rhs.set_entry("7", v7); t_rhs.set_entry("6", v6);

        ASSERT_TRUE(t_lhs == t_rhs);
    }

    /* Scenario 4 - Unequal number of variants in tables. 1 less element in RHS at the end. Comparison should be false */
    {
        mileaccess::ma::table t_lhs, t_rhs;

        t_lhs.set_entry("1", v1); t_lhs.set_entry("2", v2); t_lhs.set_entry("3", v3); t_lhs.set_entry("4", v4);
        t_lhs.set_entry("5", v5); t_lhs.set_entry("6", v6); t_lhs.set_entry("7", v7);

        t_rhs.set_entry("1", v1); t_rhs.set_entry("2", v2); t_rhs.set_entry("3", v3); t_rhs.set_entry("4", v4);
        t_rhs.set_entry("5", v5); t_rhs.set_entry("6", v6);

        ASSERT_FALSE(t_lhs == t_rhs);
    }

    /* Scenario 6 - Unequal number of variants in tables. 1 element in less RHS in the middle. Comparison should be false */
    {
        mileaccess::ma::table t_lhs, t_rhs;

        t_lhs.set_entry("1", v1); t_lhs.set_entry("2", v2); t_lhs.set_entry("3", v3); t_lhs.set_entry("4", v4);
        t_lhs.set_entry("5", v5); t_lhs.set_entry("6", v6); t_lhs.set_entry("7", v7);

        t_rhs.set_entry("1", v1); t_rhs.set_entry("2", v2); t_rhs.set_entry("3", v3); t_rhs.set_entry("5", v5);
        t_rhs.set_entry("6", v6); t_rhs.set_entry("7", v7);

        ASSERT_FALSE(t_lhs == t_rhs);
    }

    /* Scenario 7 - Equal number of variants in tables. Sequence of elements is reverse. Comparison should be false */
    {
        mileaccess::ma::table t_lhs, t_rhs;

        t_lhs.set_entry("1", v1); t_lhs.set_entry("2", v2); t_lhs.set_entry("3", v3); t_lhs.set_entry("4", v4);
        t_lhs.set_entry("5", v5); t_lhs.set_entry("6", v6); t_lhs.set_entry("7", v7);

        t_rhs.set_entry("7", v7); t_rhs.set_entry("6", v6); t_rhs.set_entry("5", v5); t_rhs.set_entry("4", v4);
        t_rhs.set_entry("3", v3); t_rhs.set_entry("2", v2); t_rhs.set_entry("1", v1);

        ASSERT_TRUE(t_lhs == t_rhs);
    }
}


/******************************************************************************
Utility function for generating random string. Fills the passed string of given
length with any one character from the below character set.
******************************************************************************/
const char charset[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*()+_:<>?";

void generate_random_string(char *str, unsigned int length)
{
    unsigned int counter = 0;
    static int iterator = 1;

    for(counter = 0; counter < length; counter++)
    {
        str[counter] = charset[(iterator * counter) % strlen(charset)];
    }
    iterator++;
}

/*
Unit tests where large number of key-value pairs are added into the table
and then retrieved successfully.
*/
TEST(ma_variant_table_tests, ma_variant_check_for_large_number_of_keys)
{
    /*
    To hold the values which will be entered into the table. Modify the values
    here, if the tests is to be executed for a different number of key-value
    pairs or different key size or value size.
    */
    char key[20];
    char *values[2000] = {NULL};
    unsigned int value_size = 1024;

    mileaccess::ma::table t;

    /*
    Creating variants of strings -> pushing them into the table -> Checking that the size of
    table is incremented by 1
    */
    for(ma_int16_t counter = 0; counter < sizeof(values)/sizeof(values[0]); counter++)
    {
        // Creating a unique string key value
        sprintf(key, "%d", counter);

        /* Generate a random value (Allocating memory for storing the value (1 extra byte for NULL)). */
        values[counter] = (char *)calloc(value_size + 1, 1);
        generate_random_string(values[counter], value_size);

        /* creating a string variant from the value and adding it to table. */
        mileaccess::ma::variant v(values[counter]);
        t.set_entry(key, v);

        mileaccess::ma::array t_size = t.keys();
        ASSERT_EQ(counter + 1, t_size.size());
    }

    /* Creating Variant from the table */
    mileaccess::ma::variant v_table(t);

    /* Checking the type of variant (table type) */
    ASSERT_EQ(MA_VARTYPE_TABLE, v_table.type());

    /* Retrieving table back from the Variant */
    mileaccess::ma::table new_t = mileaccess::ma::get<mileaccess::ma::table>(v_table);

    /* Reading elements back from the retrieved table in the same order that they were added*/
   for(ma_int16_t counter = 0; counter < sizeof(values)/sizeof(values[0]); counter++)
    {
        // Creating a unique string key value
        sprintf(key, "%d", counter);

        mileaccess::ma::variant v = new_t.get_entry(key);
        ASSERT_EQ(MA_VARTYPE_STRING, v.type());
        ASSERT_STREQ(values[counter], mileaccess::ma::get<std::string>(v).data());
    }

    /* Reading elements back from the table in the reverse order that they were added*/
    for(ma_int16_t counter = sizeof(values)/sizeof(values[0]) - 1; counter >= 0; counter--)
    {
        // Creating a unique string key value
        sprintf(key, "%d", counter);

        mileaccess::ma::variant v = new_t.get_entry(key);
        ASSERT_EQ(MA_VARTYPE_STRING, v.type());
        ASSERT_STREQ(values[counter], mileaccess::ma::get<std::string>(v).data());

        // Releasing memory allocated to values
        free(values[counter]);
    }
}




