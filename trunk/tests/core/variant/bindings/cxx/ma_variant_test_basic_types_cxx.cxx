#include "ma/variant.hxx"
#include "gtest/gtest.h"

TEST(ma_variant_basic_types_tests, ma_variant_create_test)
{
    mileaccess::ma::variant v;
    ASSERT_EQ(MA_VARTYPE_NULL, v.type());
}

TEST(ma_variant_basic_types_tests, ma_variant_get_type_test)
{
    {
        mileaccess::ma::variant v;
        ASSERT_EQ(MA_VARTYPE_NULL, v.type());
    }

    {
        mileaccess::ma::variant v((ma_uint64_t)0x12345678ABCDEF09ULL);
        ASSERT_EQ(MA_VARTYPE_UINT64, v.type());
    }

    {
        mileaccess::ma::variant v((ma_int64_t)0x12345678ABCDEF09LL);
        ASSERT_EQ(MA_VARTYPE_INT64, v.type());
    }

    {
        mileaccess::ma::variant v(1e+37);
        ASSERT_EQ(MA_VARTYPE_DOUBLE, v.type());
    }

    {
        mileaccess::ma::variant v((ma_uint32_t)0x1234ABCDUL);
        ASSERT_EQ(MA_VARTYPE_UINT32, v.type());
    }

    {
        mileaccess::ma::variant v((ma_int32_t)0xEFAB0123);
        ASSERT_EQ(MA_VARTYPE_INT32, v.type());
    }

    {
        mileaccess::ma::variant v(123.45f);
        ASSERT_EQ(MA_VARTYPE_FLOAT, v.type());
    }

    {
        mileaccess::ma::variant v((ma_uint16_t)0x1234);
        ASSERT_EQ(MA_VARTYPE_UINT16, v.type());
    }

    {
        mileaccess::ma::variant v((ma_int16_t)0x1234);
        ASSERT_EQ(MA_VARTYPE_INT16, v.type());
    }

    {
        mileaccess::ma::variant v((ma_uint8_t)0x12U);
        ASSERT_EQ(MA_VARTYPE_UINT8, v.type());
    }

    {
        mileaccess::ma::variant v((ma_int8_t)0x34);
        ASSERT_EQ(MA_VARTYPE_INT8, v.type());
    }
}

TEST(ma_variant_basic_types_tests, ma_variant_create_and_get_uint64_test)
{
    ma_uint64_t test_vector[] = { 0xFFFFFFFFFFFFFFFFULL, 0x1234567890ABCDEFULL,
                                  0xFEDCBA0987654321ULL, 0x1234567891234567ULL,
                                  0x0101010101010101ULL, 0x1010101010101010ULL,
                                  0x3456789012345678ULL, 0x3453453453453451ULL,
                                  0x0000000000000000ULL, 0x0000000000000001ULL };

    for(ma_uint8_t counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        mileaccess::ma::variant v(test_vector[counter]);
        ASSERT_EQ(MA_VARTYPE_UINT64, v.type());
        ASSERT_EQ(test_vector[counter], mileaccess::ma::get<ma_uint64_t>(v));
    }
}

TEST(ma_variant_basic_types_tests, ma_variant_create_and_get_int64_test)
{
    ma_int64_t test_vector[] = { 0xFFFFFFFFFFFFFFFFLL, 0x1234567890ABCDEFLL,
                                 0xFEDCBA0987654321LL, 0x1234567891234567LL,
                                 0x0101010101010101LL, 0x1010101010101010LL,
                                 0x3456789012345678LL, 0x3453453453453451LL,
                                 0x0000000000000000LL, 0x0000000000000001LL };

    for(ma_uint8_t counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        mileaccess::ma::variant v(test_vector[counter]);
        ASSERT_EQ(MA_VARTYPE_INT64, v.type());
        ASSERT_EQ(test_vector[counter], mileaccess::ma::get<ma_int64_t>(v));
    }
}

TEST(ma_variant_basic_types_tests, ma_variant_create_and_get_double_test)
{
    ma_double_t test_vector[] = { 0.0, 23.456, -123.4567, 1.001e11, -2345.5678, 1e-308, 1e-100, 0.1234, 1e+308 };

    for(ma_uint8_t counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        mileaccess::ma::variant v(test_vector[counter]);
        ASSERT_EQ(MA_VARTYPE_DOUBLE, v.type());
        ASSERT_DOUBLE_EQ(test_vector[counter], mileaccess::ma::get<ma_double_t>(v));
    }
}

TEST(ma_variant_basic_types_tests, ma_variant_create_and_get_uint32_test)
{
    ma_uint32_t test_vector[] = { 0xFFFFFFFFUL, 0x12345678UL, 0xFEDCBA09UL, 0x87654321UL,
                                  0x01010101UL, 0x10101010UL, 0x34567890UL, 0x00000000UL,
                                  0x00000001UL  };

    for(ma_uint8_t counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        mileaccess::ma::variant v(test_vector[counter]);
        ASSERT_EQ(MA_VARTYPE_UINT32, v.type());
        ASSERT_EQ(test_vector[counter], mileaccess::ma::get<ma_uint32_t>(v));
    }
}

TEST(ma_variant_basic_types_tests, ma_variant_create_and_get_int32_test)
{
    ma_int32_t test_vector[] = { 0x7FFFFFFFL, 0x12345678L, 0x7EDCBA09L, 0x77654321L,
                                 0x01010101L, 0x10101010L, 0x34567890L, 0x00000000L,
                                 0x00000001L };

    for(ma_uint8_t counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        mileaccess::ma::variant v(test_vector[counter]);
        ASSERT_EQ(MA_VARTYPE_INT32, v.type());
        ASSERT_EQ(test_vector[counter], mileaccess::ma::get<ma_int32_t>(v));
    }
}

TEST(ma_variant_basic_types_tests, ma_variant_create_and_get_float_test)
{
    ma_float_t test_vector[] = { 0.0f, 23.456f, -123.45f, 1.001f, 2345.56f, -1e-30f, 1e-10f, 0.12f, 1e+3f };

    for(ma_uint8_t counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        mileaccess::ma::variant v(test_vector[counter]);
        ASSERT_EQ(MA_VARTYPE_FLOAT, v.type());
        ASSERT_FLOAT_EQ(test_vector[counter], mileaccess::ma::get<ma_float_t>(v));
    }
}

TEST(ma_variant_basic_types_tests, ma_variant_create_and_get_uint16_test)
{
    ma_uint16_t test_vector[] = { 0xFFFF, 0x1234, 0xFEDC, 0x8765, 0x0101, 0x1010, 0xCDEF, 0x0000, 0x0001 };

    for(ma_uint8_t counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        mileaccess::ma::variant v(test_vector[counter]);
        ASSERT_EQ(MA_VARTYPE_UINT16, v.type());
        ASSERT_EQ(test_vector[counter], mileaccess::ma::get<ma_uint16_t>(v));
    }
}

TEST(ma_variant_basic_types_tests, ma_variant_create_and_get_int16_test)
{
    ma_int16_t test_vector[] = { 0x7FFF, 0x1234, 0x5EDC, 0x3765, 0x0101, 0x1010, 0x1CEF, 0x0000, 0x0001 };

    for(ma_uint8_t counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        mileaccess::ma::variant v(test_vector[counter]);
        ASSERT_EQ(MA_VARTYPE_INT16, v.type());
        ASSERT_EQ(test_vector[counter], mileaccess::ma::get<ma_int16_t>(v));
    }
}

TEST(ma_variant_basic_types_tests, ma_variant_create_and_get_uint8_test)
{
    ma_uint8_t test_vector[] = { 0xFFU, 0x12U, 0xFEU, 0x87U, 0x01U, 0x10U, 0xCDU, 0x00U, 0x01U };

    for(ma_uint8_t counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        mileaccess::ma::variant v(test_vector[counter]);
        ASSERT_EQ(MA_VARTYPE_UINT8, v.type());
        ASSERT_EQ(test_vector[counter], mileaccess::ma::get<ma_uint8_t>(v));
    }
}

TEST(ma_variant_basic_types_tests, ma_variant_create_and_get_int8_test)
{
    ma_int8_t test_vector[] = { 0x7F, 0x12, 0x3E, 0x7A, 0x01, 0x10, 0x1D, 0x00, 0x01 };

    for(ma_uint8_t counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        mileaccess::ma::variant v(test_vector[counter]);
        ASSERT_EQ(MA_VARTYPE_INT8, v.type());
        ASSERT_EQ(test_vector[counter], mileaccess::ma::get<ma_int8_t>(v));
    }
}


TEST(ma_variant_basic_types_tests, ma_variant_is_equal_test_01)
{
    {
        mileaccess::ma::variant v1;
        mileaccess::ma::variant v2;
        ASSERT_TRUE(v1 == v2);
    }

    {
        mileaccess::ma::variant v1;
        mileaccess::ma::variant v2((ma_uint64_t)0x1234567809ABCDEFULL);
        ASSERT_FALSE(v1 == v2);
    }

    {
        mileaccess::ma::variant v1;
        mileaccess::ma::variant v2(-1.23e+30);
        ASSERT_FALSE(v1 == v2);
    }

    {
        mileaccess::ma::variant v1;
        mileaccess::ma::variant v2((ma_uint32_t)0xEFAB0123UL);
        ASSERT_FALSE(v1 == v2);
    }

    {
        mileaccess::ma::variant v1;
        mileaccess::ma::variant v2((ma_int32_t)0x7FAB0123L);
        ASSERT_FALSE(v1 == v2);
    }

    {
        mileaccess::ma::variant v1;
        mileaccess::ma::variant v2(-123456);
        ASSERT_FALSE(v1 == v2);
    }

    {
        mileaccess::ma::variant v1;
        mileaccess::ma::variant v2(0);
        ASSERT_FALSE(v1 == v2);
    }

    {
        mileaccess::ma::variant v1(0x777);
        mileaccess::ma::variant v2(1911);
        ASSERT_TRUE(v1 == v2);
    }

    {
        mileaccess::ma::variant v1(010);
        mileaccess::ma::variant v2(8);
        ASSERT_TRUE(v1 == v2);
    }
}

TEST(ma_variant_basic_types_tests, ma_variant_is_equal_test_02)
{
    /* 1 */
    {
        mileaccess::ma::variant v1, v2;
        ASSERT_TRUE(v1 == v2);
    }

    /* 2 */
    {
        mileaccess::ma::variant v1;
        mileaccess::ma::variant v2(1.3249);
        ASSERT_FALSE(v1 == v2);
    }

    /* 3 */
    {
        mileaccess::ma::variant v1(0xFFU);
        mileaccess::ma::variant v2(0xFFU);
        ASSERT_TRUE(v1 == v2);
    }

    /* 4 */
    {
        mileaccess::ma::variant v1(0xFFU);
        mileaccess::ma::variant v2(0xFEU);
        ASSERT_FALSE(v1 == v2);
    }

    /* 5 */
    {
        mileaccess::ma::variant v1(0xFFU);
        mileaccess::ma::variant v2(0xFF);
        ASSERT_TRUE(v1 == v2);
    }

    /* 6 */
    {
        mileaccess::ma::variant v1((ma_uint8_t)0xFF);
        mileaccess::ma::variant v2((ma_uint16_t)0x00FF);
        ASSERT_TRUE(v1 == v2);
    }

    /* 7 */
    {
        mileaccess::ma::variant v1(0x34);
        mileaccess::ma::variant v2(0x1234);
        ASSERT_FALSE(v1 == v2);
    }

    /* 8 */
    {
        mileaccess::ma::variant v1(0x12U);
        mileaccess::ma::variant v2(0xAB12);
        ASSERT_FALSE(v1 == v2);
    }

    /* 9 */
    {
        mileaccess::ma::variant v1(0x12);
        mileaccess::ma::variant v2(0x12U);
        ASSERT_TRUE(v1 == v2);
    }

    /* 10 */
    {
        mileaccess::ma::variant v1(0x55);
        mileaccess::ma::variant v2(0x55);
        ASSERT_TRUE(v1 == v2);
    }

    /* 11 */
    {
        mileaccess::ma::variant v1(0x55);
        mileaccess::ma::variant v2(0x00FF);
        ASSERT_FALSE(v1 == v2);
    }

    /* 12 */
    {
        mileaccess::ma::variant v1(0x77);
        mileaccess::ma::variant v2(0x77);
        ASSERT_TRUE(v1 == v2);
    }

    /* 13 */
    {
        mileaccess::ma::variant v1(0xCD);
        mileaccess::ma::variant v2(0xABCD);
        ASSERT_FALSE(v1 == v2);
    }

    /* 14 */
    {
        mileaccess::ma::variant v1(0x1234);
        mileaccess::ma::variant v2(0x1234);
        ASSERT_TRUE(v1 == v2);
    }

    /* 15 */
    {
        mileaccess::ma::variant v1(0x1234);
        mileaccess::ma::variant v2(0x1234U);
        ASSERT_TRUE(v1 == v2);
    }

    /* 16 */
    {
        mileaccess::ma::variant v1(0x0000);
        mileaccess::ma::variant v2(0x0001);
        ASSERT_FALSE(v1 == v2);
    }

    /* 17 */
    {
        mileaccess::ma::variant v1((ma_uint16_t)0x7777);
        mileaccess::ma::variant v2((ma_uint32_t)0x00007777);
        ASSERT_TRUE(v1 == v2);
    }

    /* 18 */
    {
        mileaccess::ma::variant v1(0xABCD);
        mileaccess::ma::variant v2(0xABCD);
        ASSERT_TRUE(v1 == v2);
    }

    /* 19 */
    {
        mileaccess::ma::variant v1(0x1234);
        mileaccess::ma::variant v2(0x1234);
        ASSERT_TRUE(v1 == v2);
    }

    /* 20 */
    {
        mileaccess::ma::variant v1(0x7894);
        mileaccess::ma::variant v2(0x7894);
        ASSERT_TRUE(v1 == v2);
    }

    /* 21 */
    {
        mileaccess::ma::variant v1(0x0000);
        mileaccess::ma::variant v2(0x8000);
        ASSERT_FALSE(v1 == v2);
    }

    /* 22 */
    {
        mileaccess::ma::variant v1((ma_int32_t)0x77771234);
        mileaccess::ma::variant v2((ma_uint32_t)0x77771234UL);
        ASSERT_TRUE(v1 == v2);
    }

    /* 23 */
    {
        mileaccess::ma::variant v1((ma_int32_t)0x77771234L);
        mileaccess::ma::variant v2((ma_int32_t)0x77771234L);
        ASSERT_TRUE(v1 == v2);
    }

    /* 24 */
    {
        mileaccess::ma::variant v1((ma_uint32_t)0xABCDEF00UL);
        mileaccess::ma::variant v2((ma_uint32_t)0xABCDEF01UL);
        ASSERT_FALSE(v1 == v2);
    }

    /* 25 */
    {
        mileaccess::ma::variant v1((ma_uint32_t)0xABCDEF01UL);
        mileaccess::ma::variant v2((ma_uint32_t)0xABCDEF01UL);
        ASSERT_TRUE(v1 == v2);
    }

    /* 26 */
    {
        mileaccess::ma::variant v1((ma_uint32_t)0x7BCDEF01UL);
        mileaccess::ma::variant v2((ma_int32_t)0x7BCDEF01L);
        ASSERT_TRUE(v1 == v2);
    }

    /* 27 */
    {
        mileaccess::ma::variant v1((ma_uint64_t)0xABCDEF0023456789ULL);
        mileaccess::ma::variant v2((ma_uint64_t)0xABCDEF0123456789ULL);
        ASSERT_FALSE(v1 == v2);
    }

    /* 28 */
    {
        mileaccess::ma::variant v1((ma_uint64_t)0xABCDEF0123456789ULL);
        mileaccess::ma::variant v2((ma_uint64_t)0xABCDEF0123456789ULL);
        ASSERT_TRUE(v1 == v2);
    }

    /* 29 */
    {
        mileaccess::ma::variant v1((ma_uint64_t)0x7BCDEF0123456789ULL);
        mileaccess::ma::variant v2((ma_int64_t)0x7BCDEF0123456789LL);
        ASSERT_TRUE(v1 == v2);
    }

    /* 30 */
    {
        mileaccess::ma::variant v1((ma_int64_t)0xABCDEF0023456789LL);
        mileaccess::ma::variant v2((ma_int64_t)0xABCDEF0123456789LL);
        ASSERT_FALSE(v1 == v2);
    }

    /* 31 */
    {
        mileaccess::ma::variant v1((ma_int64_t)0xABCDEF0123456789LL);
        mileaccess::ma::variant v2((ma_int64_t)0xABCDEF0123456789LL);
        ASSERT_TRUE(v1 == v2);
    }

    /* 32 */
    {
        mileaccess::ma::variant v1((ma_int64_t)0x5BCDEF0123456789LL);
        mileaccess::ma::variant v2((ma_uint64_t)0x5BCDEF0123456789ULL);
        ASSERT_TRUE(v1 == v2);
    }

    /* 33 */
    {
        mileaccess::ma::variant v1(1234.23f);
        mileaccess::ma::variant v2(1234.2f);
        ASSERT_FALSE(v1 == v2);
    }

    /* 34 */
    {
        mileaccess::ma::variant v1(10.00f);
        mileaccess::ma::variant v2(9.99f);
        ASSERT_FALSE(v1 == v2);
    }

    /* 35 */
    {
        mileaccess::ma::variant v1(1234.23f);
        mileaccess::ma::variant v2(1234.23);
        ASSERT_FALSE(v1 == v2);
    }

    /* 36 */
    {
        mileaccess::ma::variant v1(1234.23f);
        mileaccess::ma::variant v2(1234.23f);
        ASSERT_TRUE(v1 == v2);
    }

    /* 36.1 */
    {
        ASSERT_FLOAT_EQ(0.0f, -0.0f);
        mileaccess::ma::variant v1(0.0f);
        mileaccess::ma::variant v2(-0.0f);
        ASSERT_TRUE(v1 == v2);
    }

    /* 37 */
    {
        mileaccess::ma::variant v1(100.0f);
        mileaccess::ma::variant v2(100);
        ASSERT_FALSE(v1 == v2);
    }

    /* 38 */
    {
        mileaccess::ma::variant v1(150.01);
        mileaccess::ma::variant v2(150);
        ASSERT_FALSE(v1 == v2);
    }

    /* 39 */
    {
        mileaccess::ma::variant v1(1e+37);
        mileaccess::ma::variant v2(1e-37);
        ASSERT_FALSE(v1 == v2);
    }

    /* 40 */
    {
        mileaccess::ma::variant v1(0.001);
        mileaccess::ma::variant v2(1e-3);
        ASSERT_TRUE(v1 == v2);
    }
}

TEST(ma_variant_basic_types_tests, ma_variant_is_equal_array_table_test)
{
    /*Test 1 : Two similar array variants */
    {
        mileaccess::ma::variant v1(0xFF);
        mileaccess::ma::variant v2(0xFF);

        mileaccess::ma::array a1;
        mileaccess::ma::array a2;

        a1.push(v1);
        a2.push(v2);

        ASSERT_TRUE(a1 == a2); // Two arrays are equal

        mileaccess::ma::variant v3(a1);
        mileaccess::ma::variant v4(a2);

        ASSERT_TRUE(v3 == v4); // Array variants are equal
    }

    /*Test 2 : Two different array variants */
    {
        mileaccess::ma::variant v1(0xFF);
        mileaccess::ma::variant v2(0xFE);

        mileaccess::ma::array a1;
        mileaccess::ma::array a2;

        a1.push(v1);
        a2.push(v2);

        ASSERT_FALSE(a1 == a2); // Two arrays are not equal

        mileaccess::ma::variant v3(a1);
        mileaccess::ma::variant v4(a2);

        ASSERT_FALSE(v3 == v4); // Array variants are not equal
    }

    /*Test 3 : Two table variants containing 1 element each (same key value pair)*/
    {
        mileaccess::ma::variant v1(0xFF);
        mileaccess::ma::variant v2(0xFF);

        mileaccess::ma::table t1;
        mileaccess::ma::table t2;

        t1.set_entry("same key", v1);
        t2.set_entry("same key", v2);

        ASSERT_TRUE(t1 == t2); // two tables are equal

        mileaccess::ma::variant v3(t1);
        mileaccess::ma::variant v4(t2);

        ASSERT_TRUE(v3 == v4); // Table variants are equal
    }

    /*Test 4 : Two table variants containing 1 element each (same key, different values) */
    {
        mileaccess::ma::variant v1(0xFF);
        mileaccess::ma::variant v2(0xFE);

        mileaccess::ma::table t1;
        mileaccess::ma::table t2;

        t1.set_entry("same key", v1);
        t2.set_entry("same key", v2);

        ASSERT_FALSE(t1 == t2); // two tables are not equal

        mileaccess::ma::variant v3(t1);
        mileaccess::ma::variant v4(t2);

        ASSERT_FALSE(v3 == v4); // Table variants are not equal
    }

    /*Test 5 : Two table variants containing 1 element each (different key, same values) */
    {
        mileaccess::ma::variant v1(0xABCD);
        mileaccess::ma::variant v2(0xABCD);

        mileaccess::ma::table t1;
        mileaccess::ma::table t2;

        t1.set_entry("same key", v1);
        t2.set_entry("different key", v2);

        ASSERT_FALSE(t1 == t2); // two tables are not equal

        mileaccess::ma::variant v3(t1);
        mileaccess::ma::variant v4(t2);

        ASSERT_FALSE(v3 == v4); // Table variants are not equal
    }
}






