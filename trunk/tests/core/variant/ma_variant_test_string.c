/*****************************************************************************
This file contains the unit tests for the character and wide character String
APIs from the Variant library.
******************************************************************************/
#include "unity.h"
#include "unity_fixture.h"
#include "ma/ma_variant.h"

#include <string.h>
#include <wchar.h>


// External definition. This API is not exposed in the header
ma_error_t ma_variant_get_stringtype(ma_variant_t* variant, int* string_type){
	return MA_OK;
}


TEST_GROUP_RUNNER(ma_variant_string_test_group)
{
    do {RUN_TEST_CASE(ma_variant_string_tests, ma_variant_empty_string_test)} while(0);
    do {RUN_TEST_CASE(ma_variant_string_tests, ma_variant_create_and_get_from_string_test)} while (0);
    do {RUN_TEST_CASE(ma_variant_string_tests, ma_variant_is_equal_string_test)} while (0);
    do {RUN_TEST_CASE(ma_variant_string_tests, ma_variant_inc_ref_dec_ref_string_test)} while (0);

    #ifdef MA_HAS_WCHAR_T
        do {RUN_TEST_CASE(ma_variant_string_tests, ma_variant_create_and_get_from_wstring_test)} while (0);
        do {RUN_TEST_CASE(ma_variant_string_tests, ma_variant_is_equal_wstring_test)} while (0);
        do {RUN_TEST_CASE(ma_variant_string_tests, ma_variant_inc_ref_dec_ref_wstring_test)} while (0);
        do {RUN_TEST_CASE(ma_variant_string_tests, ma_variant_is_equal_string_wstring_test)} while (0);
        //do {RUN_TEST_CASE(ma_variant_string_tests, ma_variant_get_stringtype_api_test)} while (0);
    #endif
}

TEST_SETUP(ma_variant_string_tests)
{
}

TEST_TEAR_DOWN(ma_variant_string_tests)
{

}

TEST(ma_variant_string_tests, ma_variant_empty_string_test)
{
    ma_variant_t *empty_str_var = NULL;
    ma_buffer_t *empty_buf = NULL;
    ma_variant_t *empty_wstr_var = NULL;
    ma_wbuffer_t *empty_wbuf = NULL;

    // Input test strings
    const char *input_string_1 = "";
    const char *input_string_2 = "    ";

    // Retrieved String and Size
    const char *my_string = NULL;
    size_t my_string_size = 10;

    const wchar_t *my_wstring = NULL;
    size_t my_wstring_size = 20;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_string(input_string_1, strlen(input_string_1), &empty_str_var));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_get_string_buffer(empty_str_var, &empty_buf));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_buffer_get_string(empty_buf, &my_string, &my_string_size));
    TEST_ASSERT_EQUAL_INT(0, strcmp(input_string_1, my_string));
    TEST_ASSERT_EQUAL_INT(my_string_size, strlen(input_string_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_buffer_release(empty_buf));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(empty_str_var));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_string(input_string_2, strlen(input_string_2), &empty_str_var));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_get_string_buffer(empty_str_var, &empty_buf));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_buffer_get_string(empty_buf, &my_string, &my_string_size));
    TEST_ASSERT_EQUAL_INT(0, strcmp(input_string_2, my_string));
    TEST_ASSERT_EQUAL_INT(my_string_size, strlen(input_string_2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_buffer_release(empty_buf));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(empty_str_var));

    #ifdef MA_HAS_WCHAR_T
        TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_wstring(L"", 0, &empty_wstr_var));
        TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_get_wstring_buffer(empty_wstr_var, &empty_wbuf));
        TEST_ASSERT_EQUAL_INT(MA_OK, ma_wbuffer_get_string(empty_wbuf, &my_wstring, &my_wstring_size));
        TEST_ASSERT_EQUAL_INT(0, wcscmp(my_wstring, L""));
        TEST_ASSERT_EQUAL_INT(my_wstring_size, wcslen(L""));
        TEST_ASSERT_EQUAL_INT(MA_OK, ma_wbuffer_release(empty_wbuf));
        TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(empty_wstr_var));
    #endif
}
/*
Unit tests for following macros for string variants -
1) ma_variant_create_from_string
2) ma_variant_get_string_buffer
3) ma_variant_get_type

Note - This test uses the ma_buffer_get_string macro from the ma_buffer.h/.c for
verifying the functionality of above mentioned macros. This ma_buffer_get_string
macro will be fully verified (with all its robustness) in the ma_buffer unit and
white box tests.
*/
TEST(ma_variant_string_tests, ma_variant_create_and_get_from_string_test)
{
    /* 'ma_variant_t' type Pointer for storing the base address of created variant */
    ma_variant_t *variant_ptr = NULL;

    /* 'ma_buffer_t' type Pointer for storing the base address of the buffer */
    ma_buffer_t *buffer_ptr = NULL;

    /* Loop iterator. */
    ma_uint8_t counter = 0;

    /* To hold the string and its size, as returned by the ma_buffer_get_string
       macro */
    const char *buffer_val = "";
    size_t string_size = 0;

    /* To hold the size of the buffer */
    size_t buffer_size = 0;

    /* This variable should hold MA_VARTYPE_STRING after the ma_variant_get_type
       call. Initial value of this variable is given as MA_VARTYPE_NULL. */
    ma_vartype_t vartype_obj = MA_VARTYPE_NULL;

    struct LookupTable
    {
        const char *test_string;    // Test String passed to the ma_variant_create_from_string macro.
        size_t test_length;         // Test String length passed to the ma_variant_create_from_string macro.
        const char *return_string;  // Expected String stored and returned by the ma_buffer_get_string macro.
        size_t return_string_size;  // Expected String length stored and returned by the ma_buffer_get_string macro.
    };

    const struct LookupTable test_vector[] =
    {
        { "MA Security Agent",                                                                    17,   "MA Security Agent",                                                                    17 },
        { "McAfee Security 12345654569587Agent",                                                  35,   "McAfee Security 12345654569587Agent",                                                  35 },
        { "McAfee Security 12345654569587Agent",                                                 35,   "McAfee Security 12345654569587Agent",                                                  35 },
        { "MCAFEE SECURITY AGENT",                                                                 6,   "MCAFEE",                                                                                6 },
        { "MCAFEE SECURITY AGENT 1.0",                                                           100,   "MCAFEE SECURITY AGENT 1.0",                                                            25 },
        { "12345678901234567890",                                                                 30,   "12345678901234567890",                                                                 20 },
        { "!@@#$%%^^&*&&(*(**(**)()",                                                             24,   "!@@#$%%^^&*&&(*(**(**)()",                                                             24 },
        { "ABCD1234$%@$%",                                                                        12,   "ABCD1234$%@$",                                                                         12 },
        { "~!@#$%^&*$^%^%$^#^%&^%&^%",                                                            25,   "~!@#$%^&*$^%^%$^#^%&^%&^%",                                                            25 },
        { "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghi jklmnopqrstuvwxyz1234567890!@#$%^&*()_+-=<>?:\"",  82,   "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghi jklmnopqrstuvwxyz1234567890!@#$%^&*()_+-=<>?:\"",  82 },
    };


    /*--------------------------------------------------------------------------*/
    /*
    Test 1 : Verify that the ma_variant_create_from_string macro returns
    MA_ERROR_INVALIDARG    when the string passed to the macro is NULL.
    */
    TEST_ASSERT(ma_variant_create_from_string(NULL, 10, &variant_ptr) == MA_ERROR_INVALIDARG);

    /*--------------------------------------------------------------------------*/
    /*
    Test 2 : Verify that the ma_variant_create_from_string macro returns
    MA_ERROR_PRECONDITION when the length of the string passed to the macro is 0.
    */
    TEST_ASSERT(ma_variant_create_from_string("MA Security Agent", 0, &variant_ptr) == MA_OK);
    ma_variant_release(variant_ptr);

    /*--------------------------------------------------------------------------*/
    /*
    Test 3 : Verify that the ma_variant_create_from_string macro returns
    MA_ERROR_INVALIDARG    when the address of variant pointer passed for storing
    created variant is NULL.
    */
    TEST_ASSERT(ma_variant_create_from_string("MA Security Agent", 17, NULL) == MA_ERROR_INVALIDARG);

    /*--------------------------------------------------------------------------*/
    /*
    Test 4 : Verify that the ma_variant_create_from_string macro returns MA_OK
    and creates the variant correctly using the passed string.
    */
    for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        TEST_ASSERT(ma_variant_create_from_string(test_vector[counter].test_string, test_vector[counter].test_length, &variant_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_get_type(variant_ptr, &vartype_obj) == MA_OK);
        TEST_ASSERT(vartype_obj == MA_VARTYPE_STRING);
        TEST_ASSERT(ma_variant_get_string_buffer(variant_ptr, &buffer_ptr) == MA_OK);
        TEST_ASSERT(ma_buffer_get_string(buffer_ptr, &buffer_val, &string_size) == MA_OK);
        TEST_ASSERT(ma_buffer_size(buffer_ptr, &buffer_size) == MA_OK);
        TEST_ASSERT(test_vector[counter].return_string_size == string_size);
        TEST_ASSERT_EQUAL_MEMORY(test_vector[counter].return_string, buffer_val, test_vector[counter].return_string_size);

        /* ma_variant_get_string_buffer returns MA_ERROR_INVALIDARG for NULL variant */
        TEST_ASSERT(ma_variant_get_string_buffer(NULL, &buffer_ptr) == MA_ERROR_INVALIDARG);

        /* ma_variant_get_string_buffer returns MA_ERROR_INVALIDARG for NULL buffer pointer */
        TEST_ASSERT(ma_variant_get_string_buffer(variant_ptr, NULL) == MA_ERROR_INVALIDARG);

        /* Deallocating buffer */
        ma_buffer_release(buffer_ptr);
        ma_variant_release(variant_ptr), variant_ptr=NULL;
    }

    /*--------------------------------------------------------------------------*/
}

/*
Unit tests for ma_variant_is_equal macro for string variants.
*/
TEST(ma_variant_string_tests, ma_variant_is_equal_string_test)
{
    /* 'ma_variant_t' type Pointer for storing the base address of created variant */
    ma_variant_t *variant_ptr_1 = NULL;
    ma_variant_t *variant_ptr_2 = NULL;

    /* Loop iterator. */
    ma_uint8_t counter = 0;

    /* This variable should hold MA_VARTYPE_STRING after the ma_variant_get_type
       call. Initial value of this variable is given as MA_VARTYPE_NULL. */
    ma_vartype_t vartype_obj = MA_VARTYPE_NULL;

    /* Variable to hold the results */
    ma_bool_t comparison_result = MA_FALSE;

    struct LookupTable
    {
        const char *test_string_1;
        size_t test_string_length_1;
        const char *test_string_2;
        ma_uint32_t test_string_length_2;
        int exp_result;
    };

    const struct LookupTable test_vector[] =
    {
        { "MA Security Agent",                                                                     17,   "MA Security Agent",                                                                    17,   MA_TRUE  },
        { "McAfee Security 12345654569587Agent",                                                   35,   "McAfee Security 12345654569587Agent",                                                  35,   MA_TRUE  },
        { "McAfee Security 12345654569587Agent",                                                   35,   "McAfee Security 12345654569587Agent",                                                  35,   MA_TRUE  },
        { "MCAFEE SECURITY AGENT",                                                                 22,   "MCAFEE SECURITY AGENT",                                                                21,   MA_TRUE },
        { "MCAFEE SECURITY AGENT 1.0",                                                            100,   "MCAFEE SECURITY AGENT 1.0",                                                            25,   MA_TRUE  },
        { "MCAFEE SECURITY AGENT 1.0",                                                             26,   "MCAFEE SECURITY AGENT 1.0 ",                                                           26,   MA_FALSE },
        { "12345678901234567890",                                                                  19,   "12345678901234567890",                                                                 20,   MA_FALSE },
        { "!@@#$%%^^&*&&(*(**(**)()",                                                              24,   "!@@#$%%^^&*&&(*(**(**)()",                                                             24,   MA_TRUE  },
        { "ABCD1234$%@$%",                                                                         13,   "ABCD1234$%@$",                                                                         13,   MA_FALSE },
        { "~!@#$%^&*$^%^%$^#^%&^%&^%",                                                             25,   "~!@#$%^&*$^%^%$^#^%&^%&^%",                                                            25,   MA_TRUE  },
        { "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghi jklmnopqrstuvwxyz1234567890!@#$%^&*()_+-=<>?:\"",   82,   "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghi jklmnopqrstuvwxyz1234567890!@#$%^&*()_+-=<>?:\"",  82,   MA_TRUE  },
        { "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghi j klmnopqrstuvwxyz1234567890!@#$%^&*()_+-=<>?:\"",  83,   "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghi jklmnopqrstuvwxyz1234567890!@#$%^&*()_+-=<>?:\"",  82,   MA_FALSE },
    };

    /*--------------------------------------------------------------------------*/

    for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        /* Creating string variant 1 and checking its type */
        TEST_ASSERT(ma_variant_create_from_string(test_vector[counter].test_string_1, test_vector[counter].test_string_length_1, &variant_ptr_1) == MA_OK);
        TEST_ASSERT(ma_variant_get_type(variant_ptr_1, &vartype_obj) == MA_OK);
        TEST_ASSERT(vartype_obj == MA_VARTYPE_STRING);

        /* Creating string variant 2 and checking its type */
        TEST_ASSERT(ma_variant_create_from_string(test_vector[counter].test_string_2, test_vector[counter].test_string_length_2, &variant_ptr_2) == MA_OK);
        TEST_ASSERT(ma_variant_get_type(variant_ptr_2, &vartype_obj) == MA_OK);
        TEST_ASSERT(vartype_obj == MA_VARTYPE_STRING);

        /* Equality Checks */

        /* Comparing variant 1 with NULL results in MA_ERROR_INVALIDARG */
        TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, NULL, &comparison_result) == MA_ERROR_INVALIDARG);

        /* Comparing variant 2 with NULL results in MA_ERROR_INVALIDARG */
        TEST_ASSERT(ma_variant_is_equal(NULL, variant_ptr_2, &comparison_result) == MA_ERROR_INVALIDARG);

        /* MA_ERROR_INVALIDARG when location to store comparison result is NULL */
        TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, NULL) == MA_ERROR_INVALIDARG);

        /* Comparing String Variant 1 with itself */
        TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_1, &comparison_result) == MA_OK);
        TEST_ASSERT(comparison_result == MA_TRUE);

        /* Comparing String Variant 2 with itself */
        TEST_ASSERT(ma_variant_is_equal(variant_ptr_2, variant_ptr_2, &comparison_result) == MA_OK);
        TEST_ASSERT(comparison_result == MA_TRUE);

        /* Comparing Valid string Variants (Variant 1 and Variant 2 */
        TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, &comparison_result) == MA_OK);
        TEST_ASSERT(comparison_result == test_vector[counter].exp_result);

        ma_variant_release(variant_ptr_1);
        ma_variant_release(variant_ptr_2);
    }
    /*--------------------------------------------------------------------------*/
}

/*
Unit tests for ma_variant_inc_ref and ma_variant_dec_ref macros for string variant.
*/
TEST(ma_variant_string_tests, ma_variant_inc_ref_dec_ref_string_test)
{
    /* 'ma_variant_t' type Pointer for storing the base address of variant object */
    ma_variant_t *variant_ptr = NULL;

    // Loop iterator.
    ma_uint8_t counter = 0;

    /* Variable to hold the variant type */
    ma_vartype_t vartype_obj = MA_VARTYPE_NULL;

    /*Test 1 : Calling ma_variant_inc_ref macro with address of variant as NULL*/
    TEST_ASSERT(ma_variant_add_ref(NULL) == MA_ERROR_INVALIDARG);

    /*Test 2 : Calling ma_variant_dec_ref macro with address of variant as NULL*/
    TEST_ASSERT(ma_variant_release(NULL) == MA_ERROR_INVALIDARG);

    /*Test 3 : Verifying that the ma_variant_inc_ref/ma_variant_dec_ref macro maintains correct reference count*/

    /*Creating a string variant*/
    TEST_ASSERT(ma_variant_create_from_string("MA Security Agent", 17, &variant_ptr) == MA_OK);

    /*Incrementing the variant reference count (10 times) */
    for(counter = 0; counter < 10; counter++)
    {
        TEST_ASSERT(ma_variant_add_ref(variant_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_get_type(variant_ptr, &vartype_obj) == MA_OK);
        TEST_ASSERT(vartype_obj == MA_VARTYPE_STRING);
    }

    /* Decrementing the variant reference count (11 times). The variant should be destroyed
       by the end of this for-loop */
    for(counter = 0; counter < 11; counter++)
    {
        TEST_ASSERT(ma_variant_get_type(variant_ptr, &vartype_obj) == MA_OK);
        TEST_ASSERT(vartype_obj == MA_VARTYPE_STRING);
        TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);
    }
}


#ifdef MA_HAS_WCHAR_T

/*
Unit tests for following macros for wide character string variants -
1) ma_variant_create_from_wstring
2) ma_variant_get_wstring_buffer
3) ma_variant_get_type

Note - This test uses the ma_buffer_get_wstring macro from the ma_buffer.h/.c for
verifying the functionality of above mentioned macros. This ma_buffer_get_wstring
macro will be fully verified (with all its robustness) in the ma_buffer unit and
white box tests.
*/
TEST(ma_variant_string_tests, ma_variant_create_and_get_from_wstring_test)
{
    /* 'ma_variant_t' type Pointer for storing the base address of created variant */
    ma_variant_t *variant_ptr = NULL;

    /* 'ma_buffer_t' type Pointer for storing the base address of the buffer */
    ma_wbuffer_t *wbuffer_ptr = NULL;

    /* Loop iterator. */
    ma_uint8_t counter = 0;

    /* To hold the string and its size, as returned by the ma_buffer_get_wstring
       macro */
    const wchar_t *wbuffer_val = NULL;
    size_t string_size = 0;

    /* To hold the size of the buffer */
    size_t buffer_size = 0;

    /* This variable should hold MA_VARTYPE_STRING after the ma_variant_get_type
       call. Initial value of this variable is given as MA_VARTYPE_NULL. */
    ma_vartype_t vartype_obj = MA_VARTYPE_NULL;


    struct LookupTable
    {
        const wchar_t *test_string;    // Test String passed to the ma_variant_create_from_string macro.
        size_t test_length;            // Test String length passed to the ma_variant_create_from_string macro.
        const wchar_t *return_string;  // Expected String stored and returned by the ma_buffer_get_string macro.
        size_t return_string_size;     // Expected String length stored and returned by the ma_buffer_get_string macro.
    };

    const struct LookupTable test_vector[] =
    {
        { L"MA Security Agent",                                                                    17,   L"MA Security Agent",                                                                    17 },
        { L"McAfee Security 12345654569587Agent",                                                  35,   L"McAfee Security 12345654569587Agent",                                                  35 },
        { L"McAfee Security 12345654569587Agent",                                                 35,   L"McAfee Security 12345654569587Agent",                                                  35 },
        { L"MCAFEE SECURITY AGENT",                                                                 6,   L"MCAFEE",                                                                                6 },
        { L"MCAFEE SECURITY AGENT 1.0",                                                           100,   L"MCAFEE SECURITY AGENT 1.0",                                                            25 },
        { L"12345678901234567890\x211C",                                                           30,   L"12345678901234567890\x211C",                                                           21 },
        { L"!@@#$%%^^&*&&(*(**(**)(\x0041\x030A",                                                  24,   L"!@@#$%%^^&*&&(*(**(**)(\x212B",                                                        24 },
        { L"ABCD1234$%@$%",                                                                        12,   L"ABCD1234$%@$",                                                                         12 },
        { L"~!@#$%^&*$^%^%$^#^%&^%&^%",                                                            25,   L"~!@#$%^&*$^%^%$^#^%&^%&^%",                                                            25 },
        { L"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghi jklmnopqrstuvwxyz1234567890!@#$%^&*()_+-=<>?:\"",  82,   L"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghi jklmnopqrstuvwxyz1234567890!@#$%^&*()_+-=<>?:\"",  82 },
    };

    /*--------------------------------------------------------------------------*/
    /*
    Test 1 : Verify that the ma_variant_create_from_wstring macro returns
    MA_ERROR_INVALIDARG    when the string passed to the macro is NULL.
    */
    TEST_ASSERT(ma_variant_create_from_wstring(NULL, 10, &variant_ptr) == MA_ERROR_INVALIDARG);

    /*--------------------------------------------------------------------------*/
    /*
    Test 2 : Verify that the ma_variant_create_from_wstring macro returns
    MA_ERROR_PRECONDITION when the length of the string passed to the macro is 0.
    */
    TEST_ASSERT(ma_variant_create_from_wstring(L"MA Security Agent'U+2212'", 0, &variant_ptr) == MA_OK);
    ma_variant_release(variant_ptr);

    /*--------------------------------------------------------------------------*/
    /*
    Test 3 : Verify that the ma_variant_create_from_string macro returns
    MA_ERROR_INVALIDARG    when the address of variant pointer passed for storing
    created variant is NULL.
    */
    TEST_ASSERT(ma_variant_create_from_string("MA Security Agent", 17, NULL) == MA_ERROR_INVALIDARG);

    /*--------------------------------------------------------------------------*/
    /*
    Test 4 : Verify that the ma_variant_create_from_string macro returns MA_OK
    and creates the variant correctly using the passed string.
    */
    for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        TEST_ASSERT(ma_variant_create_from_wstring(test_vector[counter].test_string, test_vector[counter].test_length, &variant_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_get_type(variant_ptr, &vartype_obj) == MA_OK);
        TEST_ASSERT(vartype_obj == MA_VARTYPE_STRING);

        TEST_ASSERT(ma_variant_get_wstring_buffer(variant_ptr, &wbuffer_ptr) == MA_OK);
        TEST_ASSERT(ma_wbuffer_get_string(wbuffer_ptr, &wbuffer_val, &string_size) == MA_OK);

        TEST_ASSERT(ma_wbuffer_size(wbuffer_ptr, &buffer_size) == MA_OK);
        TEST_ASSERT(test_vector[counter].return_string_size == string_size);
        TEST_ASSERT_EQUAL_MEMORY(test_vector[counter].return_string, wbuffer_val, test_vector[counter].return_string_size);

        /* ma_variant_get_wstring_buffer returns MA_ERROR_INVALIDARG for NULL variant */
        TEST_ASSERT(ma_variant_get_wstring_buffer(NULL, &wbuffer_ptr) == MA_ERROR_INVALIDARG);

        /* ma_variant_get_wstring_buffer returns MA_ERROR_INVALIDARG for NULL buffer pointer */
        TEST_ASSERT(ma_variant_get_wstring_buffer(variant_ptr, NULL) == MA_ERROR_INVALIDARG);

        /* Deallocating buffer */
        ma_wbuffer_release(wbuffer_ptr);
        ma_variant_release(variant_ptr);
    }

    /*--------------------------------------------------------------------------*/
}

/*
Unit tests for ma_variant_is_equal macro for wide character string variants.
*/
TEST(ma_variant_string_tests, ma_variant_is_equal_wstring_test)
{
    /* 'ma_variant_t' type Pointer for storing the base address of created variant */
    ma_variant_t *variant_ptr_1 = NULL;
    ma_variant_t *variant_ptr_2 = NULL;

    /* Loop iterator. */
    ma_uint8_t counter = 0;

    /* This variable should hold MA_VARTYPE_STRING after the ma_variant_get_type
       call. Initial value of this variable is given as MA_VARTYPE_NULL. */
    ma_vartype_t vartype_obj = MA_VARTYPE_NULL;

    /* Variable to hold the results */
    ma_bool_t comparison_result = MA_FALSE;

    struct LookupTable
    {
        const wchar_t *test_string_1;
        size_t test_string_length_1;
        const wchar_t *test_string_2;
        ma_uint32_t test_string_length_2;
        int exp_result;
    };

    const struct LookupTable test_vector[] =
    {
        { L"MA Security Agent",                                                                     17,   L"MA Security Agent",                                                                    17,   MA_TRUE  },
        { L"MCAFEE SECURITY AGENT",                                                                 22,   L"MCAFEE SECURITY AGENT",                                                                21,   MA_TRUE },
        { L"MCAFEE SECURITY AGENT 1.0",                                                            100,   L"MCAFEE SECURITY AGENT 1.0",                                                            25,   MA_TRUE  },
        { L"MCAFEE SECURITY AGENT 1.0",                                                             26,   L"MCAFEE SECURITY AGENT 1.0 ",                                                           26,   MA_FALSE },
        { L"ABCD1234$%@$%\x212B",                                                                   14,   L"ABCD1234$%@$%\x212B",                                                                  14,   MA_TRUE  },
        { L"12345678901234567890",                                                                  19,   L"12345678901234567890",                                                                 20,   MA_FALSE },
        { L"!@@#$%%^^&*&&(*(**(**)()",                                                              24,   L"!@@#$%%^^&*&&(*(**(**)()",                                                             24,   MA_TRUE  },
        { L"~!@#$%^&*$^%^%$^#^%&^%&^%",                                                             25,   L"~!@#$%^&*$^%^%$^#^%&^%&^%",                                                            25,   MA_TRUE  },
        { L"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghi jklmnopqrstuvwxyz1234567890!@#$%^&*()_+-=<>?:\"",   82,   L"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghi jklmnopqrstuvwxyz1234567890!@#$%^&*()_+-=<>?:\"",  82,   MA_TRUE  },
        { L"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghi j klmnopqrstuvwxyz1234567890!@#$%^&*()_+-=<>?:\"",  83,   L"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghi jklmnopqrstuvwxyz1234567890!@#$%^&*()_+-=<>?:\"",  82,   MA_FALSE },
    };

    /*--------------------------------------------------------------------------*/

    for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        /* Creating string variant 1 and checking its type */
        TEST_ASSERT(ma_variant_create_from_wstring(test_vector[counter].test_string_1, test_vector[counter].test_string_length_1, &variant_ptr_1) == MA_OK);
        TEST_ASSERT(ma_variant_get_type(variant_ptr_1, &vartype_obj) == MA_OK);
        TEST_ASSERT(vartype_obj == MA_VARTYPE_STRING);

        /* Creating string variant 2 and checking its type */
        TEST_ASSERT(ma_variant_create_from_wstring(test_vector[counter].test_string_2, test_vector[counter].test_string_length_2, &variant_ptr_2) == MA_OK);
        TEST_ASSERT(ma_variant_get_type(variant_ptr_2, &vartype_obj) == MA_OK);
        TEST_ASSERT(vartype_obj == MA_VARTYPE_STRING);

        /* Equality Checks */

        /* Comparing variant 1 with NULL results in MA_ERROR_INVALIDARG */
        TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, NULL, &comparison_result) == MA_ERROR_INVALIDARG);

        /* Comparing variant 2 with NULL results in MA_ERROR_INVALIDARG */
        TEST_ASSERT(ma_variant_is_equal(NULL, variant_ptr_2, &comparison_result) == MA_ERROR_INVALIDARG);

        /* MA_ERROR_INVALIDARG when location to store comparison result is NULL */
        TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, NULL) == MA_ERROR_INVALIDARG);

        /* Comparing String Variant 1 with itself */
        TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_1, &comparison_result) == MA_OK);
        TEST_ASSERT(comparison_result == MA_TRUE);

        /* Comparing String Variant 2 with itself */
        TEST_ASSERT(ma_variant_is_equal(variant_ptr_2, variant_ptr_2, &comparison_result) == MA_OK);
        TEST_ASSERT(comparison_result == MA_TRUE);
        ma_variant_is_equal(variant_ptr_1, variant_ptr_2, &comparison_result);

        /* Comparing Valid string Variants (Variant 1 and Variant 2 */
        TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, &comparison_result) == MA_OK);
        TEST_ASSERT(comparison_result == test_vector[counter].exp_result);

        ma_variant_release(variant_ptr_1);
        ma_variant_release(variant_ptr_2);
        TEST_ASSERT(comparison_result == test_vector[counter].exp_result);
    }
    /*--------------------------------------------------------------------------*/
}

/*
Unit tests for ma_variant_inc_ref and ma_variant_dec_ref macros for wide character string variant.
*/
TEST(ma_variant_string_tests, ma_variant_inc_ref_dec_ref_wstring_test)
{
    /* 'ma_variant_t' type Pointer for storing the base address of variant object */
    ma_variant_t *variant_ptr = NULL;

    // Loop iterator.
    ma_uint8_t counter = 0;

    /* Variable to hold the variant type */
    ma_vartype_t vartype_obj = MA_VARTYPE_NULL;

    /*Test 1 : Calling ma_variant_inc_ref macro with address of variant as NULL*/

    TEST_ASSERT(ma_variant_add_ref(NULL) == MA_ERROR_INVALIDARG);

    /*Test 2 : Calling ma_variant_dec_ref macro with address of variant as NULL*/

    TEST_ASSERT(ma_variant_release(NULL) == MA_ERROR_INVALIDARG);

    /*Test 3 : Verifying that the ma_variant_inc_ref/ma_variant_dec_ref macro maintains correct reference count*/

    /*Creating a string variant*/
    TEST_ASSERT(ma_variant_create_from_wstring(L"MA Security Agent\x4e00", 18, &variant_ptr) == MA_OK);

    /*Incrementing the variant reference count (10 times) */
    for(counter = 0; counter < 10; counter++)
    {
        TEST_ASSERT(ma_variant_add_ref(variant_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_get_type(variant_ptr, &vartype_obj) == MA_OK);
        TEST_ASSERT(vartype_obj == MA_VARTYPE_STRING);
    }

    /* Decrementing the variant reference count (11 times). The variant should be destroyed
       by the end of this for-loop */
    for(counter = 0; counter < 11; counter++)
    {
        TEST_ASSERT(ma_variant_get_type(variant_ptr, &vartype_obj) == MA_OK);
        TEST_ASSERT(vartype_obj == MA_VARTYPE_STRING);
        TEST_ASSERT(ma_variant_release(variant_ptr) == MA_OK);
    }
}

/*
Unit tests for ma_variant_is_equal macro comaprison b/w character and wide character string variants.
*/
TEST(ma_variant_string_tests, ma_variant_is_equal_string_wstring_test)
{
    /* 'ma_variant_t' type Pointer for storing the base address of created variant */
    ma_variant_t *variant_ptr_1 = NULL;
    ma_variant_t *variant_ptr_2 = NULL;

    /* Loop iterator. */
    ma_uint8_t counter = 0;

    /* This variable should hold MA_VARTYPE_STRING after the ma_variant_get_type
       call. Initial value of this variable is given as MA_VARTYPE_NULL. */
    ma_vartype_t vartype_obj = MA_VARTYPE_NULL;

    /* Variable to hold the results */
    ma_bool_t comparison_result = MA_FALSE;

    const static char euro_utf8[] = {0xe2, 0x82, 0xac, 0}; /* U+20AC, aka Euro Sign */

    struct LookupTable
    {
        const char *test_string_1;
        size_t test_string_length_1;
        const wchar_t *test_string_2;
        ma_uint32_t test_string_length_2;
        int exp_result;
        int err_ret;
    };

    const struct LookupTable test_vector[] =
    {
    #ifdef MA_WINDOWS  // Only supported if C99 or C++ is being used, therefore, enabled only for Windows
        { euro_utf8,                                                                                3,   L"\u20AC",                                                                                1,   MA_TRUE,  MA_OK  },
    #endif
        { "MA Security Agent",                                                                     17,   L"MA Security Agent",                                                                    17,   MA_TRUE,  MA_OK  },
        { "McAfee Security 12345654569587Agent",                                                   30,   L"McAfee Security 12345654569587Agent",                                                  35,   MA_FALSE, MA_OK  },
        { "McAfee Security 12345654569587Agent",                                                   30,   L"McAfee Security 12345654569587Agent",                                                  38,   MA_FALSE, MA_OK  },
        { "MCAFEE SECURITY AGENT",                                                                 22,   L"MCAFEE SECURITY AGENT",                                                                21,   MA_TRUE, MA_OK },
        { "MCAFEE SECURITY AGENT 1.0",                                                            100,   L"MCAFEE SECURITY AGENT 1.0",                                                            25,   MA_TRUE, MA_OK  },
        { "12345678901234567890",                                                                  20,   L"12345678901234567890",                                                                 20,   MA_TRUE },
        { "!@@#$%%^^&*&&(*(**(**)()",                                                              24,   L"!@@#$%%^^&*&&(*(**(**)()",                                                             24,   MA_TRUE  },
        { "ABCD1234$%@$%",                                                                         13,   L"ABCD1234$%@$",                                                                         13,   MA_FALSE, MA_OK },
        { "~!@#$%^&*$^%^%$^#^%&^%&^%",                                                             25,   L"~!@#$%^&*$^%^%$^#^%&^%&^%",                                                            25,   MA_TRUE,  MA_OK  },
        { "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghi jklmnopqrstuvwxyz1234567890!@#$%^&*()_+-=<>?:\"",   82,   L"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghi jklmnopqrstuvwxyz1234567890!@#$%^&*()_+-=<>?:\"",  82,   MA_TRUE, MA_OK  },
        { "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghi j klmnopqrstuvwxyz1234567890!@#$%^&*()_+-=<>?:\"",  83,   L"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghi jklmnopqrstuvwxyz1234567890!@#$%^&*()_+-=<>?:\"",  82,   MA_FALSE, MA_OK },
    };

    /*--------------------------------------------------------------------------*/

    for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        /* Creating string variant 1 (Standard Character width) and checking its type */
        TEST_ASSERT(ma_variant_create_from_string(test_vector[counter].test_string_1, test_vector[counter].test_string_length_1, &variant_ptr_1) == MA_OK);
        TEST_ASSERT(ma_variant_get_type(variant_ptr_1, &vartype_obj) == MA_OK);
        TEST_ASSERT(vartype_obj == MA_VARTYPE_STRING);

        /* Creating string variant 2 (Wide Char) and checking its type */
        TEST_ASSERT(ma_variant_create_from_wstring(test_vector[counter].test_string_2, test_vector[counter].test_string_length_2, &variant_ptr_2) == MA_OK);
        TEST_ASSERT(ma_variant_get_type(variant_ptr_2, &vartype_obj) == MA_OK);
        TEST_ASSERT(vartype_obj == MA_VARTYPE_STRING);

        /* Equality Checks */
        /* Comparing Valid string Variants (Variant 1 = Standard Width String.... Variant 2 = Wide Character String) */
        TEST_ASSERT(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, &comparison_result) == test_vector[counter].err_ret);
        TEST_ASSERT(comparison_result == test_vector[counter].exp_result);

        ma_variant_release(variant_ptr_2);
        ma_variant_release(variant_ptr_1);
    }
    /*--------------------------------------------------------------------------*/
}

TEST(ma_variant_string_tests, ma_variant_get_stringtype_api_test)
{
    int counter = 0;

    struct LookupTable
    {
        const char *test_string_1;
        const wchar_t *test_string_2;
    };

    const struct LookupTable test_vector[] =
    {
        { "MA Security Agent",                                                                     L"MA Security Agent"  },
        { "McAfee Security 12345654569587Agent",                                                   L"McAfee Security 12345654569587Agent"  },
        { "McAfee Security 12345654569587Agent",                                                   L"McAfee Security 12345654569587Agent"  },
        { "MCAFEE SECURITY AGENT",                                                                 L"MCAFEE SECURITY AGENT" },
        { "MCAFEE SECURITY AGENT 1.0",                                                             L"MCAFEE SECURITY AGENT 1.0" },
        { "ABCD1234$%@$%",                                                                         L"ABCD1234$%@$" },
        { "~!@#$%^&*$^%^%$^#^%&^%&^%",                                                             L"~!@#$%^&*$^%^%$^#^%&^%&^%" },
        { "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghi jklmnopqrstuvwxyz1234567890!@#$%^&*()_+-=<>?:\"",   L"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghi jklmnopqrstuvwxyz1234567890!@#$%^&*()_+-=<>?:\"" },
        { "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghi j klmnopqrstuvwxyz1234567890!@#$%^&*()_+-=<>?:\"",  L"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghi jklmnopqrstuvwxyz1234567890!@#$%^&*()_+-=<>?:\"" },
    };

    for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
    {
        int stringtype = -1;
        ma_variant_t *variant_ptr_1 = NULL;
        ma_variant_t *variant_ptr_2 = NULL;

        /* Creating string variant 1 (Standard Character width) */
        TEST_ASSERT(ma_variant_create_from_string(test_vector[counter].test_string_1, strlen(test_vector[counter].test_string_1), &variant_ptr_1) == MA_OK);

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_variant_get_stringtype(NULL, &stringtype));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_variant_get_stringtype(variant_ptr_1, NULL));
        TEST_ASSERT(MA_OK == ma_variant_get_stringtype(variant_ptr_1, &stringtype));
        TEST_ASSERT(0 == stringtype);

        ma_variant_release(variant_ptr_1);

        /* Creating string variant 2 (Wide Character width) */
        TEST_ASSERT(ma_variant_create_from_wstring(test_vector[counter].test_string_2, wcslen(test_vector[counter].test_string_2), &variant_ptr_2) == MA_OK);

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_variant_get_stringtype(NULL, &stringtype));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_variant_get_stringtype(variant_ptr_2, NULL));
        TEST_ASSERT(MA_OK == ma_variant_get_stringtype(variant_ptr_2, &stringtype));
        TEST_ASSERT(1 == stringtype);

        ma_variant_release(variant_ptr_2);
    }

    // Invalid type (not string type scenario)
    {
        int stringtype = -1;
        ma_variant_t *variant_ptr = NULL;

        /* Creating integer variant */
        TEST_ASSERT(ma_variant_create_from_uint64(0x12345678ABCDEF09ULL, &variant_ptr) == MA_OK);
        TEST_ASSERT(MA_ERROR_PRECONDITION == ma_variant_get_stringtype(variant_ptr, &stringtype));
        ma_variant_release(variant_ptr);
    }
}

#endif

