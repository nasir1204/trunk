/*****************************************************************************
Variant library unit tests for the Tables (Dictionary).
******************************************************************************/
#define UNITY_FIXTURE_MALLOC_OVERRIDES_H_ 1

#include "unity.h"
#include "unity_fixture.h"
#include "unity_internals.h"
#include "ma/ma_variant.h"
#include <string.h>

/*
Unity Fixture file overrides the standard definition of calloc and free. Therefore,
include stdlib.h, so that calloc can be resolved.
*/
#include <stdlib.h>

#ifndef MA_WINDOWS
#include <wchar.h>
#endif

TEST_GROUP_RUNNER(ma_variant_table_test_group)
{
    do {RUN_TEST_CASE(ma_variant_table_tests, ma_variant_create_get_from_table_test)} while (0);
    do {RUN_TEST_CASE(ma_variant_table_tests, ma_variant_foreach_element_from_table_test)} while (0);
    do {RUN_TEST_CASE(ma_variant_table_tests, ma_table_is_equal_test)} while (0);
    do {RUN_TEST_CASE(ma_variant_table_tests, ma_variant_table_self_reference_test)} while (0);
    do {RUN_TEST_CASE(ma_variant_table_tests, ma_table_keys_with_same_prefix)} while (0);

    #ifdef MA_HAS_WCHAR_T
        do {RUN_TEST_CASE(ma_variant_table_tests, ma_variant_create_get_from_wtable_test)} while (0);
        do {RUN_TEST_CASE(ma_variant_table_tests, ma_variant_foreach_element_from_wtable_test)} while (0);
        do {RUN_TEST_CASE(ma_variant_table_tests, ma_table_wwkeys_with_same_prefix)} while (0);
        do {RUN_TEST_CASE(ma_variant_table_tests, ma_table_cwkeys_with_same_prefix)} while (0);
        do {RUN_TEST_CASE(ma_variant_table_tests, ma_table_wckeys_with_same_prefix)} while (0);
    #endif

    do {RUN_TEST_CASE(ma_variant_table_tests, ma_variant_check_empty_table_returns_ok)} while (0);
    do {RUN_TEST_CASE(ma_variant_table_tests, ma_variant_check_for_large_number_of_keys)} while (0);
    do {RUN_TEST_CASE(ma_variant_table_tests, ma_variant_remove_entries_test)} while(0);
}

TEST_SETUP(ma_variant_table_tests)
{
}

TEST_TEAR_DOWN(ma_variant_table_tests)
{

}


/* Test Vector for foreach API test */
struct LookupTable
{
    const char    *key;
    const wchar_t *wkey;
    ma_uint32_t   value;
};

struct LookupTable test_vector_table[] = {
                                             { "1", L"1", 0xFFFFFFFFUL },
                                             { "2", L"2", 0x12345678UL },
                                             { "3", L"3", 0xFEDCBA09UL },
                                             { "4", L"4", 0x87654321UL },
                                             { "5", L"5", 0x01010101UL },
                                             { "6", L"6", 0x10101010UL },
                                             { "7", L"7", 0x34567890UL },
                                             { "8", L"8", 0x00000000UL },
                                             { "9", L"9", 0x00000001UL },
                                         };


ma_uint8_t cb_counter = 0;
ma_uint8_t wcb_counter = 0;
/*
callback function for iterating through the table. This callback function
should be called 9 times by the foreach iterator.
*/
void test_table_callback(char const *table_key,
                        ma_variant_t *value,
                        void *cb_args,
                        ma_bool_t *stop_loop)
{
    cb_counter++;
}

/*
callback function for iterating through the table. This callback function
should be called 9 times by the foreach iterator.
*/
void test_wtable_callback(wchar_t const *table_key,
                        ma_variant_t *value,
                        void *cb_args,
                        ma_bool_t *stop_loop)
{
    wcb_counter++;
}


/*
Unit tests for following APIs -
1) ma_table_create
2) ma_table_add_entry
3) ma_table_size
4) ma_variant_create_from_table
5) ma_variant_get_table
6) ma_table_get_keys
7) ma_table_get_value
*/
TEST(ma_variant_table_tests, ma_variant_create_get_from_table_test)
{
    /* 'ma_variant_t' type Pointer for storing the base address of variant object */
    ma_variant_t *table_variant_ptr = NULL;

    /* 'ma_variant_t' type Pointer for storing the variant address */
    ma_variant_t *variant_ptr = NULL;

    /* 'ma_table_t' table type pointer */
    ma_table_t *table_ptr = NULL;

    /* Table pointer retrieved from the table variant */
    ma_table_t *table_ptr_1 = NULL;

    /* Variant Pointer retrieved from array */
    ma_variant_t *variant_ptr_1 = NULL;
    ma_variant_t *variant_ptr_2 = NULL;

    /* To hold the keys returned by ma_table_get_keys API */
    ma_array_t *keys = NULL;

    /* For storing array size returned by ma_table_size API */
    size_t table_size = 100;

    /* This variable should hold MA_VARTYPE_TABLE after the ma_variant_get_type
       call. Initial value of this variable is given as MA_VARTYPE_STRING. */
    ma_vartype_t vartype_obj = MA_VARTYPE_STRING;

    /* Variables for storing values retrieved from table */
    ma_uint64_t  unsigned_64bit_val = 0ULL;
    ma_int64_t   signed_64bit_val = 0LL;
    ma_uint32_t  unsigned_32bit_val = 0UL;
    ma_int32_t   signed_32bit_val = 0L;
    ma_uint16_t  unsigned_16bit_val = 0;
    ma_int16_t   signed_16bit_val = 0;
    ma_uint8_t   unsigned_8bit_val = 0U;
    ma_int8_t    signed_8bit_val = 0;
    double       double_val = 0.0;
    float        float_val = 0.0f;
    ma_bool_t    boolean_val = 0;
    ma_buffer_t  *buffer_ptr = NULL;
    ma_wbuffer_t *wbuffer_ptr = NULL;
    const char         *string_val = NULL;
    const wchar_t      *wstring_val = NULL;
    size_t       string_size = 0;
    const unsigned char *raw_val = 0;
    size_t       raw_size = 0;

    /* Sample Raw Buffer. To be used while creating raw buffer variant */
    ma_uint8_t raw_buffer[] = { 0x12, 0x23, 0x34, 0x45, 0x56, 0x67, 0x78, 0x89, 0x90 };

    /* Creating table of variants */
    TEST_ASSERT_MESSAGE(ma_table_create(NULL) == MA_ERROR_INVALIDARG, \
                        "ma_table_create doesn't return MA_ERROR_INVALIDARG for NULL I/P");

    TEST_ASSERT_MESSAGE(ma_table_create(&table_ptr) == MA_OK, \
                        "ma_table_create doesn't returns MA_OK for valid table pointer address");

    /*
    Creating variants of various types -> pushing them into the table -> Checking that the size of
    table is incremented by 1
    */

    /* ma_table_size returns NULL for invalid arguments */
    TEST_ASSERT_MESSAGE(ma_table_size(NULL, NULL) == MA_ERROR_INVALIDARG, \
                        "ma_table_size doesn't returns MA_ERROR_INVALIDARG for invalid arguments");

    TEST_ASSERT_MESSAGE(ma_table_size(table_ptr, NULL) == MA_ERROR_INVALIDARG, \
                        "ma_table_size doesn't returns MA_ERROR_INVALIDARG for invalid arguments");

    TEST_ASSERT_MESSAGE(ma_table_size(NULL, &table_size) == MA_ERROR_INVALIDARG, \
                        "ma_table_size doesn't returns MA_ERROR_INVALIDARG for invalid arguments");

    /* ma_table_size returns 0 before any variant is added */
    TEST_ASSERT_MESSAGE(ma_table_size(table_ptr, &table_size) == MA_OK, \
                        "ma_table_size doesn't returns MA_OK for valid arguments");

    TEST_ASSERT_MESSAGE(table_size == 0, "ma_table_size doesn't return 0 array size for an empty array");

    /* NULL Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create(&variant_ptr) == MA_OK, "NULL variant could not be created");

    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr, "First Entry", variant_ptr) == MA_OK, "Variant could not be pushed to table");
    ma_variant_release(variant_ptr), variant_ptr = 0;

    TEST_ASSERT_MESSAGE(ma_table_size(table_ptr, &table_size) == MA_OK, "ma_table_size doesn't returns MA_OK for valid arguments");

    TEST_ASSERT_MESSAGE(table_size == 1, "ma_table_size doesn't return correct table size");

    /* 64 Bit unsigned integer Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_uint64(0x12345678ABCDEF09ULL, &variant_ptr) == MA_OK, \
                        "64 Bit unsigned variant could not be created");

    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr, "Second Entry", variant_ptr) == MA_OK, "Variant could not be pushed to table");
    ma_variant_release(variant_ptr), variant_ptr = 0;

    TEST_ASSERT_MESSAGE(ma_table_size(table_ptr, &table_size) == MA_OK, "ma_table_size doesn't returns MA_OK for valid arguments");

    TEST_ASSERT_MESSAGE(table_size == 2, "ma_table_size doesn't return correct table size");

    /* 64 Bit signed integer Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_int64(0x12345678ABCDEF09LL, &variant_ptr) == MA_OK, \
                        "64 Bit unsigned variant could not be created");

    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr, "Third Entry", variant_ptr) == MA_OK, "Variant could not be pushed to table");
    ma_variant_release(variant_ptr), variant_ptr = 0;

    TEST_ASSERT_MESSAGE(ma_table_size(table_ptr, &table_size) == MA_OK, "ma_table_size doesn't returns MA_OK for valid arguments");

    TEST_ASSERT_MESSAGE(table_size == 3, "ma_table_size doesn't return correct table size");

    /* Double Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_double(1.23e+18, &variant_ptr) == MA_OK, "Double variant could not be created");

    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr, "Fourth Entry", variant_ptr) == MA_OK, "Variant could not be pushed to table");
    ma_variant_release(variant_ptr), variant_ptr = 0;

    TEST_ASSERT_MESSAGE(ma_table_size(table_ptr, &table_size) == MA_OK, "ma_table_size doesn't returns MA_OK for valid arguments");

    TEST_ASSERT_MESSAGE(table_size == 4, "ma_table_size doesn't return correct table size");

    /* 32 Bit unsigned integer Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_uint32(0x1234ABCDUL, &variant_ptr) == MA_OK, \
                        "32 Bit unsigned variant could not be created");

    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr, "Fifth Entry", variant_ptr) == MA_OK, "Variant could not be pushed to table");
    ma_variant_release(variant_ptr), variant_ptr = 0;

    TEST_ASSERT_MESSAGE(ma_table_size(table_ptr, &table_size) == MA_OK, "ma_table_size doesn't returns MA_OK for valid arguments");

    TEST_ASSERT_MESSAGE(table_size == 5, "ma_table_size doesn't return correct table size");

    /* 32 Bit signed integer Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_int32(0xEFAB0123, &variant_ptr) == MA_OK, \
                        "32 Bit signed variant could not be created");

    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr, "Sixth Entry", variant_ptr) == MA_OK, "Variant could not be pushed to table");
    ma_variant_release(variant_ptr), variant_ptr = 0;

    TEST_ASSERT_MESSAGE(ma_table_size(table_ptr, &table_size) == MA_OK, "ma_table_size doesn't returns MA_OK for valid arguments");

    TEST_ASSERT_MESSAGE(table_size == 6, "ma_table_size doesn't return correct table size");

    /* Float Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_float(123.45f, &variant_ptr) == MA_OK, \
                        "Float variant could not be created");

    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr, "Seventh Entry", variant_ptr) == MA_OK, "Variant could not be pushed to table");
    ma_variant_release(variant_ptr), variant_ptr = 0;

    TEST_ASSERT_MESSAGE(ma_table_size(table_ptr, &table_size) == MA_OK, "ma_table_size doesn't returns MA_OK for valid arguments");

    TEST_ASSERT_MESSAGE(table_size == 7, "ma_table_size doesn't return correct table size");

    /* 16 Bit unsigned integer Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_uint16(0x1234, &variant_ptr) == MA_OK, \
                        "16 Bit unsigned variant could not be created");

    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr, "Eighth Entry", variant_ptr) == MA_OK, "Variant could not be pushed to table");
    ma_variant_release(variant_ptr), variant_ptr = 0;

    TEST_ASSERT_MESSAGE(ma_table_size(table_ptr, &table_size) == MA_OK, "ma_table_size doesn't returns MA_OK for valid arguments");

    TEST_ASSERT_MESSAGE(table_size == 8, "ma_table_size doesn't return correct table size");

    /* 16 Bit signed integer Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_int16(0x7FAB, &variant_ptr) == MA_OK, \
                        "16 Bit signed variant could not be created");

    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr, "Ninth Entry", variant_ptr) == MA_OK, "Variant could not be pushed to table");
    ma_variant_release(variant_ptr), variant_ptr = 0;

    TEST_ASSERT_MESSAGE(ma_table_size(table_ptr, &table_size) == MA_OK, "ma_table_size doesn't returns MA_OK for valid arguments");

    TEST_ASSERT_MESSAGE(table_size == 9, "ma_table_size doesn't return correct table size");

    /* 8 Bit unsigned integer Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_uint8(0x12, &variant_ptr) == MA_OK, \
                        "8 Bit unsigned variant could not be created");

    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr, "Tenth Entry", variant_ptr) == MA_OK, "Variant could not be pushed to table");
    ma_variant_release(variant_ptr), variant_ptr = 0;

    TEST_ASSERT_MESSAGE(ma_table_size(table_ptr, &table_size) == MA_OK, "ma_table_size doesn't returns MA_OK for valid arguments");

    TEST_ASSERT_MESSAGE(table_size == 10, "ma_table_size doesn't return correct table size");

    /* 8 Bit signed integer Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_int8(0x7F, &variant_ptr) == MA_OK, \
                        "8 Bit signed variant could not be created");

    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr, "Eleventh Entry", variant_ptr) == MA_OK, "Variant could not be pushed to table");
    ma_variant_release(variant_ptr), variant_ptr = 0;

    TEST_ASSERT_MESSAGE(ma_table_size(table_ptr, &table_size) == MA_OK, "ma_table_size doesn't returns MA_OK for valid arguments");

    TEST_ASSERT_MESSAGE(table_size == 11, "ma_table_size doesn't return correct table size");

    /* Boolean Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_bool(123, &variant_ptr) == MA_OK, \
                        "Boolean variant could not be created");

    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr, "Twelfth Entry", variant_ptr) == MA_OK, "Variant could not be pushed to table");
    ma_variant_release(variant_ptr), variant_ptr = 0;

    TEST_ASSERT_MESSAGE(ma_table_size(table_ptr, &table_size) == MA_OK, "ma_table_size doesn't returns MA_OK for valid arguments");

    TEST_ASSERT_MESSAGE(table_size == 12, "ma_table_size doesn't return correct table size");

    /* String Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_string("MA Security Agent", 17, &variant_ptr) == MA_OK, \
                        "ma_variant_create_from_string doesn't return MK_OK for valid string");

    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr, "Thirteenth Entry", variant_ptr) == MA_OK, "Variant could not be pushed to table");
    ma_variant_release(variant_ptr), variant_ptr = 0;

    TEST_ASSERT_MESSAGE(ma_table_size(table_ptr, &table_size) == MA_OK, "ma_table_size doesn't returns MA_OK for valid arguments");

    TEST_ASSERT_MESSAGE(table_size == 13, "ma_table_size doesn't return correct table size");

    /* Wide Character String Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_wstring(L"MA Security Agent$%$%\x212B", 22, &variant_ptr) == MA_OK, \
                        "ma_variant_create_from_wstring doesn't return MK_OK for valid string");

    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr, "Fourteenth Entry", variant_ptr) == MA_OK, "Variant could not be pushed to table");
    ma_variant_release(variant_ptr), variant_ptr = 0;

    TEST_ASSERT_MESSAGE(ma_table_size(table_ptr, &table_size) == MA_OK, "ma_table_size doesn't returns MA_OK for valid arguments");

    TEST_ASSERT_MESSAGE(table_size == 14, "ma_table_size doesn't return correct table size");

    /* Raw Data Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_raw(raw_buffer, sizeof(raw_buffer)/sizeof(raw_buffer[0]), &variant_ptr) == MA_OK, \
                        "Raw Buffer variant could not be created");

    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr, "Fifteenth Entry", variant_ptr) == MA_OK, "Variant could not be pushed to table");
    ma_variant_release(variant_ptr), variant_ptr = 0;

    TEST_ASSERT_MESSAGE(ma_table_size(table_ptr, &table_size) == MA_OK, "ma_table_size doesn't returns MA_OK for valid arguments");

    TEST_ASSERT_MESSAGE(table_size == 15, "ma_table_size doesn't return correct table size");

    /* Creating Variant from the table */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_table(NULL, NULL) == MA_ERROR_INVALIDARG, \
                        "ma_variant_create_from_table doesn't returns MA_ERROR_INVALIDARG for invalid arguments");

    TEST_ASSERT_MESSAGE(ma_variant_create_from_table(table_ptr, NULL) == MA_ERROR_INVALIDARG, \
                        "ma_variant_create_from_table doesn't returns MA_ERROR_INVALIDARG for invalid arguments");

    TEST_ASSERT_MESSAGE(ma_variant_create_from_table(NULL, &table_variant_ptr) == MA_ERROR_INVALIDARG, \
                        "ma_variant_create_from_table doesn't returns MA_ERROR_INVALIDARG for invalid arguments");

    TEST_ASSERT_MESSAGE(ma_variant_create_from_table(table_ptr, &table_variant_ptr) == MA_OK, \
                        "ma_variant_create_from_table could not create variant from table");

    /* Checking the type of variant (table type) */
    TEST_ASSERT_MESSAGE(ma_variant_get_type(table_variant_ptr, &vartype_obj) == MA_OK, \
                        "ma_variant_get_type doesn't return MA_OK for Table Variant");

    TEST_ASSERT_MESSAGE(vartype_obj == MA_VARTYPE_TABLE, \
                        "ma_variant_get_type doesn't return MA_VARTYPE_ARRAY for array variant");

    /* Trying to retrieve table from a variant which is not of table type */
    TEST_ASSERT_MESSAGE(ma_variant_create(&variant_ptr) == MA_OK, "NULL variant could not be created");

    TEST_ASSERT_MESSAGE(ma_variant_get_table(variant_ptr, &table_ptr_1) == MA_ERROR_PRECONDITION, \
                        "ma_variant_get_table failed");
    ma_variant_release(variant_ptr), variant_ptr = 0;

    /* Retrieving table back from the Variant */
    TEST_ASSERT_MESSAGE(ma_variant_get_table(NULL, NULL) == MA_ERROR_INVALIDARG, \
                        "ma_variant_get_table doesn't return MA_ERROR_INVALIDARG for invalid arguments");

    TEST_ASSERT_MESSAGE(ma_variant_get_table(table_variant_ptr, NULL) == MA_ERROR_INVALIDARG, \
                        "ma_variant_get_table doesn't return MA_ERROR_INVALIDARG for invalid arguments");

    TEST_ASSERT_MESSAGE(ma_variant_get_table(NULL, &table_ptr_1) == MA_ERROR_INVALIDARG, \
                        "ma_variant_get_table doesn't returns MA_ERROR_INVALIDARG for invalid arguments");

    TEST_ASSERT_MESSAGE(ma_variant_get_table(table_variant_ptr, &table_ptr_1) == MA_OK, \
                        "ma_variant_get_table could not retrieve table from the table variant");

    ma_variant_release(table_variant_ptr);
    ma_table_release(table_ptr);

    /* Reading elements back from the retrieved table */
    TEST_ASSERT_MESSAGE(ma_table_get_value(NULL, NULL, NULL) == MA_ERROR_INVALIDARG, \
                        "ma_table_get_value doesn't returns MA_ERROR_INVALIDARG for invalid arguments");

    TEST_ASSERT_MESSAGE(ma_table_get_value(table_ptr_1, "First Entry", NULL) == MA_ERROR_INVALIDARG, \
                        "ma_table_get_value doesn't returns MA_ERROR_INVALIDARG for invalid arguments");

    TEST_ASSERT_MESSAGE(ma_table_get_value(NULL, "First Entry", &variant_ptr_1) == MA_ERROR_INVALIDARG, \
                        "ma_table_get_value doesn't return MA_ERROR_INVALIDARG for invalid arguments");

    /* 15 variants were pushed into the table. The ma_table_get_value should return MA_ERROR_OBJECT_NOTFOUND if passed key
       is not present in the table and passed variant should remain set to NULL */
    TEST_ASSERT_MESSAGE(ma_table_get_value(table_ptr_1, "Zeroth Entry", &variant_ptr_1) == MA_ERROR_OBJECTNOTFOUND, \
                        "ma_table_get_value doesn't returns MA_OK for key is not present in table");

    TEST_ASSERT_NULL_MESSAGE(variant_ptr_1, "Passed variant pointer doesn't remain set to NULL if key is not present in table");

    TEST_ASSERT_MESSAGE(ma_table_get_value(table_ptr_1, "Sixteenth Entry", &variant_ptr_1) == MA_ERROR_OBJECTNOTFOUND, \
                        "ma_table_get_value doesn't returns MA_OK for key is not present in table");

    TEST_ASSERT_NULL_MESSAGE(variant_ptr_1, "Passed variant pointer doesn't remain set to NULL if key is not present in table");

    /* Reading variant for key 'First Entry' */
    TEST_ASSERT_MESSAGE(ma_table_get_value(table_ptr_1, "First Entry", &variant_ptr_1) == MA_OK, \
                        "ma_table_get_value couldn't retrieve variant from table");

    TEST_ASSERT_MESSAGE(ma_variant_get_type(variant_ptr_1, &vartype_obj) == MA_OK, \
                        "ma_variant_get_type doesn't return MA_OK for valid Variant");

    TEST_ASSERT_MESSAGE(vartype_obj == MA_VARTYPE_NULL, \
                        "NULL variant should have been present in table, but it is not");
    ma_variant_release(variant_ptr_1);

    /* Reading variant for key 'Second Entry' */
    TEST_ASSERT_MESSAGE(ma_table_get_value(table_ptr_1, "Second Entry", &variant_ptr_1) == MA_OK, \
                        "ma_table_get_value couldn't retrieve variant from table");

    TEST_ASSERT_MESSAGE(ma_variant_get_uint64(variant_ptr_1, &unsigned_64bit_val) == MA_OK, \
                        "Value could not be retrieved from the variant");

    TEST_ASSERT_MESSAGE(0x12345678ABCDEF09ULL == unsigned_64bit_val, "Correct value is not retrieved from variant");
    ma_variant_release(variant_ptr_1);

    /* Reading variant for key 'Third Entry' */
    TEST_ASSERT_MESSAGE(ma_table_get_value(table_ptr_1, "Third Entry", &variant_ptr_1) == MA_OK, \
                        "ma_table_get_value couldn't retrieve variant from table");

    TEST_ASSERT_MESSAGE(ma_variant_get_int64(variant_ptr_1, &signed_64bit_val) == MA_OK, \
                        "Value could not be retrieved from the variant");

    TEST_ASSERT_MESSAGE(0x12345678ABCDEF09LL == signed_64bit_val, "Correct value is not retrieved from variant");
    ma_variant_release(variant_ptr_1);

    /* Reading variant for key 'Fourth Entry' */
    TEST_ASSERT_MESSAGE(ma_table_get_value(table_ptr_1, "Fourth Entry", &variant_ptr_1) == MA_OK, \
                        "ma_table_get_value couldn't retrieve variant from table");

    TEST_ASSERT_MESSAGE(ma_variant_get_double(variant_ptr_1, &double_val) == MA_OK, \
                        "Value could not be retrieved from the variant");

    TEST_ASSERT_MESSAGE(1.23e+18 == double_val, "Correct value is not retrieved from variant");
    ma_variant_release(variant_ptr_1);

    /* Reading variant for key 'Fifth Entry' */
    TEST_ASSERT_MESSAGE(ma_table_get_value(table_ptr_1, "Fifth Entry", &variant_ptr_1) == MA_OK, \
                        "ma_table_get_value couldn't retrieve variant from table");

    TEST_ASSERT_MESSAGE(ma_variant_get_uint32(variant_ptr_1, &unsigned_32bit_val) == MA_OK, \
                        "Value could not be retrieved from the variant");

    TEST_ASSERT_MESSAGE(0x1234ABCDUL == unsigned_32bit_val, "Correct value is not retrieved from variant");
    ma_variant_release(variant_ptr_1);

    /* Reading variant for key 'Sixth Entry' */
    TEST_ASSERT_MESSAGE(ma_table_get_value(table_ptr_1, "Sixth Entry", &variant_ptr_1) == MA_OK, \
                        "ma_table_get_value couldn't retrieve variant from table");

    TEST_ASSERT_MESSAGE(ma_variant_get_int32(variant_ptr_1, &signed_32bit_val) == MA_OK, \
                        "Value could not be retrieved from the variant");

    TEST_ASSERT_MESSAGE(0xEFAB0123 == signed_32bit_val, "Correct value is not retrieved from variant");
    ma_variant_release(variant_ptr_1);

    /* Reading variant for key 'Seventh Entry' */
    TEST_ASSERT_MESSAGE(ma_table_get_value(table_ptr_1, "Seventh Entry", &variant_ptr_1) == MA_OK, \
                        "ma_table_get_value couldn't retrieve variant from table");

    TEST_ASSERT_MESSAGE(ma_variant_get_float(variant_ptr_1, &float_val) == MA_OK, \
                        "Value could not be retrieved from the variant");

    TEST_ASSERT_MESSAGE(123.45f == float_val, "Correct value is not retrieved from variant");
    ma_variant_release(variant_ptr_1);

    /* Reading variant for key 'Eighth Entry' */
    TEST_ASSERT_MESSAGE(ma_table_get_value(table_ptr_1, "Eighth Entry", &variant_ptr_1) == MA_OK, \
                        "ma_table_get_value couldn't retrieve variant from table");

    TEST_ASSERT_MESSAGE(ma_variant_get_uint16(variant_ptr_1, &unsigned_16bit_val) == MA_OK, \
                        "Value could not be retrieved from the variant");

    TEST_ASSERT_MESSAGE(0x1234 == unsigned_16bit_val, "Correct value is not retrieved from variant");
    ma_variant_release(variant_ptr_1);

    /* Reading variant for key 'Ninth Entry' */
    TEST_ASSERT_MESSAGE(ma_table_get_value(table_ptr_1, "Ninth Entry", &variant_ptr_1) == MA_OK, \
                        "ma_table_get_value couldn't retrieve variant from table");

    TEST_ASSERT_MESSAGE(ma_variant_get_int16(variant_ptr_1, &signed_16bit_val) == MA_OK, \
                        "Value could not be retrieved from the variant");

    TEST_ASSERT_MESSAGE(0x7FAB == signed_16bit_val, "Correct value is not retrieved from variant");
    ma_variant_release(variant_ptr_1);

    /* Reading variant for key 'Tenth Entry' */
    TEST_ASSERT_MESSAGE(ma_table_get_value(table_ptr_1, "Tenth Entry", &variant_ptr_1) == MA_OK, \
                        "ma_table_get_value couldn't retrieve variant from table");

    TEST_ASSERT_MESSAGE(ma_variant_get_uint8(variant_ptr_1, &unsigned_8bit_val) == MA_OK, \
                        "Value could not be retrieved from the variant");

    TEST_ASSERT_MESSAGE(0x12 == unsigned_8bit_val, "Correct value is not retrieved from variant");
    ma_variant_release(variant_ptr_1);

    /* Reading variant for key 'Eleventh Entry' */
    TEST_ASSERT_MESSAGE(ma_table_get_value(table_ptr_1, "Eleventh Entry", &variant_ptr_1) == MA_OK, \
                        "ma_table_get_value couldn't retrieve variant from table");

    TEST_ASSERT_MESSAGE(ma_variant_get_int8(variant_ptr_1, &signed_8bit_val) == MA_OK, \
                        "Value could not be retrieved from the variant");

    TEST_ASSERT_MESSAGE(0x7F == signed_8bit_val, "Correct value is not retrieved from variant");
    ma_variant_release(variant_ptr_1);

    /* Reading variant for key 'Twelfth Entry' */
    TEST_ASSERT_MESSAGE(ma_table_get_value(table_ptr_1, "Twelfth Entry", &variant_ptr_1) == MA_OK, \
                        "ma_table_get_value couldn't retrieve variant from table");

    TEST_ASSERT_MESSAGE(ma_variant_get_bool(variant_ptr_1, &boolean_val) == MA_OK, \
                        "Value could not be retrieved from the variant");

    TEST_ASSERT_MESSAGE(1 == boolean_val, "Correct value is not retrieved from variant");
    ma_variant_release(variant_ptr_1);

    /* Reading variant for key 'Thirteenth Entry' */
    TEST_ASSERT_MESSAGE(ma_table_get_value(table_ptr_1, "Thirteenth Entry", &variant_ptr_1) == MA_OK, \
                        "ma_table_get_value couldn't retrieve variant from table");

    TEST_ASSERT_MESSAGE(ma_variant_get_string_buffer(variant_ptr_1, &buffer_ptr) == MA_OK, \
                        "String Buffer cannot be retrieved from the Variant");

    TEST_ASSERT_MESSAGE(ma_buffer_get_string(buffer_ptr, &string_val, &string_size) == MA_OK, \
                        "ma_buffer_get_string could not retrieve string and its size from the buffer");

    TEST_ASSERT_MESSAGE(17 == string_size, "String size is not read correctly from the buffer");

    TEST_ASSERT_EQUAL_MEMORY_MESSAGE("MA Security Agent", string_val, 17, "String is not read correctly from the buffer");
    ma_buffer_release(buffer_ptr);
    ma_variant_release(variant_ptr_1);

    /* Reading variant for key 'Fourteenth Entry' */
    TEST_ASSERT_MESSAGE(ma_table_get_value(table_ptr_1, "Fourteenth Entry", &variant_ptr_1) == MA_OK, \
                        "ma_table_get_value couldn't retrieve variant from table");

    TEST_ASSERT_MESSAGE(ma_variant_get_wstring_buffer(variant_ptr_1, &wbuffer_ptr) == MA_OK, \
                        "String Buffer cannot be retrieved from the Variant");

    TEST_ASSERT_MESSAGE(ma_wbuffer_get_string(wbuffer_ptr, &wstring_val, &string_size) == MA_OK, \
                        "ma_wbuffer_get_string could not retrieve string and its size from the buffer");

    TEST_ASSERT_MESSAGE(22 == string_size, "String size is not read correctly from the buffer");

    TEST_ASSERT_EQUAL_MEMORY_MESSAGE(L"MA Security Agent$%$%\x212B", wstring_val, 22, "String is not read correctly from the buffer");
    ma_wbuffer_release(wbuffer_ptr);
    ma_variant_release(variant_ptr_1);

    /* Reading variant for key 'Fifteenth Entry' */
    TEST_ASSERT_MESSAGE(ma_table_get_value(table_ptr_1, "Fifteenth Entry", &variant_ptr_1) == MA_OK, \
                        "ma_table_get_value couldn't retrieve variant from table");

    TEST_ASSERT_MESSAGE(ma_variant_get_raw_buffer(variant_ptr_1, &buffer_ptr) == MA_OK, \
                        "String Buffer cannot be retrieved from the Variant");

    TEST_ASSERT_MESSAGE(ma_buffer_get_raw(buffer_ptr, &raw_val, &raw_size) == MA_OK, \
                        "ma_buffer_get_raw could not retrieve raw buffer from buffer variant");

    TEST_ASSERT_MESSAGE(sizeof(raw_buffer)/sizeof(raw_buffer[0]) == raw_size, "Raw Buffer size is not read correctly");

    TEST_ASSERT_EQUAL_MEMORY_MESSAGE(raw_buffer, raw_val, sizeof(raw_buffer)/sizeof(raw_buffer[0]), \
                                     "Raw Buffer is not read correctly");
    ma_buffer_release(buffer_ptr);
    ma_variant_release(variant_ptr_1);

    /* ma_table_get_keys API Test */
    TEST_ASSERT_MESSAGE(ma_table_get_keys(NULL, &keys)== MA_ERROR_INVALIDARG, "ma_table_get_keys failed");
    TEST_ASSERT_MESSAGE(ma_table_get_keys(table_ptr, NULL)== MA_ERROR_INVALIDARG, "ma_table_get_keys failed");
    TEST_ASSERT_MESSAGE(ma_table_get_keys(table_ptr, &keys)== MA_OK, "ma_table_get_keys failed");

    TEST_ASSERT_MESSAGE(ma_array_get_element_at(keys, 1, &variant_ptr_2) == MA_OK, "Couldn't read element at location 1");

    TEST_ASSERT_MESSAGE(ma_variant_get_string_buffer(variant_ptr_2, &buffer_ptr) == MA_OK, \
                        "String Buffer cannot be retrieved from the Variant");

    TEST_ASSERT_MESSAGE(ma_buffer_get_string(buffer_ptr, &string_val, &string_size) == MA_OK, \
                        "ma_buffer_get_string could not retrieve string and its size from the buffer");

    ma_buffer_release(buffer_ptr);
    ma_variant_release(variant_ptr_2);


    TEST_ASSERT_MESSAGE(ma_array_get_element_at(keys, 2, &variant_ptr_2) == MA_OK, "Couldn't read element at location 2");

    TEST_ASSERT_MESSAGE(ma_variant_get_string_buffer(variant_ptr_2, &buffer_ptr) == MA_OK, \
                        "String Buffer cannot be retrieved from the Variant");

    TEST_ASSERT_MESSAGE(ma_buffer_get_string(buffer_ptr, &string_val, &string_size) == MA_OK, \
                        "ma_buffer_get_string could not retrieve string and its size from the buffer");

    ma_buffer_release(buffer_ptr);
    ma_variant_release(variant_ptr_2);

    TEST_ASSERT_MESSAGE(ma_array_get_element_at(keys, 3, &variant_ptr_2) == MA_OK, "Couldn't read element at location 3");

    TEST_ASSERT_MESSAGE(ma_variant_get_string_buffer(variant_ptr_2, &buffer_ptr) == MA_OK, \
                        "String Buffer cannot be retrieved from the Variant");

    TEST_ASSERT_MESSAGE(ma_buffer_get_string(buffer_ptr, &string_val, &string_size) == MA_OK, \
                        "ma_buffer_get_string could not retrieve string and its size from the buffer");

    ma_buffer_release(buffer_ptr);
    ma_variant_release(variant_ptr_2);

    TEST_ASSERT_MESSAGE(ma_array_get_element_at(keys, 4, &variant_ptr_2) == MA_OK, "Couldn't read element at location 4");

    TEST_ASSERT_MESSAGE(ma_variant_get_string_buffer(variant_ptr_2, &buffer_ptr) == MA_OK, \
                        "String Buffer cannot be retrieved from the Variant");

    TEST_ASSERT_MESSAGE(ma_buffer_get_string(buffer_ptr, &string_val, &string_size) == MA_OK, \
                        "ma_buffer_get_string could not retrieve string and its size from the buffer");

    ma_buffer_release(buffer_ptr);
    ma_variant_release(variant_ptr_2);

    TEST_ASSERT_MESSAGE(ma_array_get_element_at(keys, 5, &variant_ptr_2) == MA_OK, "Couldn't read element at location 5");

    TEST_ASSERT_MESSAGE(ma_variant_get_string_buffer(variant_ptr_2, &buffer_ptr) == MA_OK, \
                        "String Buffer cannot be retrieved from the Variant");

    TEST_ASSERT_MESSAGE(ma_buffer_get_raw(buffer_ptr, &raw_val, &raw_size) == MA_OK, \
                        "ma_buffer_get_raw could not retrieve raw buffer and its size from the buffer variant");

    ma_buffer_release(buffer_ptr);
    ma_variant_release(variant_ptr_2);

    TEST_ASSERT_MESSAGE(ma_array_get_element_at(keys, 6, &variant_ptr_2) == MA_OK, "Couldn't read element at location 6");

    TEST_ASSERT_MESSAGE(ma_variant_get_string_buffer(variant_ptr_2, &buffer_ptr) == MA_OK, \
                        "String Buffer cannot be retrieved from the Variant");

    TEST_ASSERT_MESSAGE(ma_buffer_get_string(buffer_ptr, &string_val, &string_size) == MA_OK, \
                        "ma_buffer_get_string could not retrieve string and its size from the buffer");

    ma_buffer_release(buffer_ptr);
    ma_variant_release(variant_ptr_2);

    TEST_ASSERT_MESSAGE(ma_array_get_element_at(keys, 7, &variant_ptr_2) == MA_OK, "Couldn't read element at location 7");

    TEST_ASSERT_MESSAGE(ma_variant_get_string_buffer(variant_ptr_2, &buffer_ptr) == MA_OK, \
                        "String Buffer cannot be retrieved from the Variant");

    TEST_ASSERT_MESSAGE(ma_buffer_get_string(buffer_ptr, &string_val, &string_size) == MA_OK, \
                        "ma_buffer_get_string could not retrieve string and its size from the buffer");

    ma_buffer_release(buffer_ptr);
    ma_variant_release(variant_ptr_2);

    TEST_ASSERT_MESSAGE(ma_array_get_element_at(keys, 8, &variant_ptr_2) == MA_OK, "Couldn't read element at location 8");

    TEST_ASSERT_MESSAGE(ma_variant_get_string_buffer(variant_ptr_2, &buffer_ptr) == MA_OK, \
                        "String Buffer cannot be retrieved from the Variant");

    TEST_ASSERT_MESSAGE(ma_buffer_get_string(buffer_ptr, &string_val, &string_size) == MA_OK, \
                        "ma_buffer_get_string could not retrieve string and its size from the buffer");

    ma_buffer_release(buffer_ptr);
    ma_variant_release(variant_ptr_2);

    TEST_ASSERT_MESSAGE(ma_array_get_element_at(keys, 9, &variant_ptr_2) == MA_OK, "Couldn't read element at location 9");

    TEST_ASSERT_MESSAGE(ma_variant_get_string_buffer(variant_ptr_2, &buffer_ptr) == MA_OK, \
                        "String Buffer cannot be retrieved from the Variant");

    TEST_ASSERT_MESSAGE(ma_buffer_get_string(buffer_ptr, &string_val, &string_size) == MA_OK, \
                        "ma_buffer_get_string could not retrieve string and its size from the buffer");

    ma_buffer_release(buffer_ptr);
    ma_variant_release(variant_ptr_2);

    TEST_ASSERT_MESSAGE(ma_array_get_element_at(keys, 10, &variant_ptr_2) == MA_OK, "Couldn't read element at location 10");

    TEST_ASSERT_MESSAGE(ma_variant_get_string_buffer(variant_ptr_2, &buffer_ptr) == MA_OK, \
                        "String Buffer cannot be retrieved from the Variant");

    TEST_ASSERT_MESSAGE(ma_buffer_get_string(buffer_ptr, &string_val, &string_size) == MA_OK, \
                        "ma_buffer_get_string could not retrieve string and its size from the buffer");

    ma_buffer_release(buffer_ptr);
    ma_variant_release(variant_ptr_2);

    TEST_ASSERT_MESSAGE(ma_array_get_element_at(keys, 11, &variant_ptr_2) == MA_OK, "Couldn't read element at location 11");

    TEST_ASSERT_MESSAGE(ma_variant_get_string_buffer(variant_ptr_2, &buffer_ptr) == MA_OK, \
                        "String Buffer cannot be retrieved from the Variant");

    TEST_ASSERT_MESSAGE(ma_buffer_get_string(buffer_ptr, &string_val, &string_size) == MA_OK, \
                        "ma_buffer_get_string could not retrieve string and its size from the buffer");

    ma_buffer_release(buffer_ptr);
    ma_variant_release(variant_ptr_2);

    TEST_ASSERT_MESSAGE(ma_array_get_element_at(keys, 12, &variant_ptr_2) == MA_OK, "Couldn't read element at location 12");

    TEST_ASSERT_MESSAGE(ma_variant_get_string_buffer(variant_ptr_2, &buffer_ptr) == MA_OK, \
                        "String Buffer cannot be retrieved from the Variant");

    TEST_ASSERT_MESSAGE(ma_buffer_get_string(buffer_ptr, &string_val, &string_size) == MA_OK, \
                        "ma_buffer_get_string could not retrieve string and its size from the buffer");

    ma_buffer_release(buffer_ptr);
    ma_variant_release(variant_ptr_2);

    TEST_ASSERT_MESSAGE(ma_array_get_element_at(keys, 13, &variant_ptr_2) == MA_OK, "Couldn't read element at location 13");

    TEST_ASSERT_MESSAGE(ma_variant_get_string_buffer(variant_ptr_2, &buffer_ptr) == MA_OK, \
                        "String Buffer cannot be retrieved from the Variant");

    TEST_ASSERT_MESSAGE(ma_buffer_get_string(buffer_ptr, &string_val, &string_size) == MA_OK, \
                        "ma_buffer_get_string could not retrieve string and its size from the buffer");

    ma_buffer_release(buffer_ptr);
    ma_variant_release(variant_ptr_2);

    TEST_ASSERT_MESSAGE(ma_array_get_element_at(keys, 14, &variant_ptr_2) == MA_OK, "Couldn't read element at location 14");

    TEST_ASSERT_MESSAGE(ma_variant_get_string_buffer(variant_ptr_2, &buffer_ptr) == MA_OK, \
                        "String Buffer cannot be retrieved from the Variant");

    TEST_ASSERT_MESSAGE(ma_buffer_get_string(buffer_ptr, &string_val, &string_size) == MA_OK, \
                        "ma_buffer_get_string could not retrieve string and its size from the buffer");

    ma_buffer_release(buffer_ptr);
    ma_variant_release(variant_ptr_2);

    TEST_ASSERT_MESSAGE(ma_array_get_element_at(keys, 15, &variant_ptr_2) == MA_OK, "Couldn't read element at location 15");

    TEST_ASSERT_MESSAGE(ma_variant_get_string_buffer(variant_ptr_2, &buffer_ptr) == MA_OK, \
                        "String Buffer cannot be retrieved from the Variant");

    TEST_ASSERT_MESSAGE(ma_buffer_get_string(buffer_ptr, &string_val, &string_size) == MA_OK, \
                        "ma_buffer_get_string could not retrieve string and its size from the buffer");

    ma_buffer_release(buffer_ptr);
    ma_variant_release(variant_ptr_2);

    ma_array_release(keys);
    ma_table_release(table_ptr_1);
}


/*
Unit tests for following APIs -
1) ma_table_create
2) ma_table_add_entry
3) ma_table_size
4) ma_table_foreach
5) ma_table_inc_ref
6) ma_table_dec_ref
*/
TEST(ma_variant_table_tests, ma_variant_foreach_element_from_table_test)
{
    /* 'ma_variant_t' type Pointer for storing the variant address */
    ma_variant_t *variant_ptr = NULL;

    /* 'ma_table_t' table type pointer */
    ma_table_t *table_ptr = NULL;

    /* For storing table size returned by ma_table_size API */
    size_t table_size = 100;

    // Loop iterator.
    ma_uint8_t counter = 0;


    TEST_ASSERT_MESSAGE(ma_table_create(&table_ptr) == MA_OK, \
                        "Table could not be created");

    /* ma_table_size returns 0 before any variant is added */
    TEST_ASSERT_MESSAGE(ma_table_size(table_ptr, &table_size) == MA_OK, \
                        "ma_table_size doesn't returns MA_OK for valid arguments");

    TEST_ASSERT_MESSAGE(0 == table_size, "ma_table_size doesn't return 0 table size for an empty table");

    /* Creating a list of 32-bit unsigned integer variants and pushing them into the table */
    for(counter = 0; counter < sizeof(test_vector_table)/sizeof(test_vector_table[0]); counter++)
    {
        TEST_ASSERT_MESSAGE(ma_variant_create_from_uint32(test_vector_table[counter].value, &variant_ptr) == MA_OK, \
                            "32 Bit unsigned variant could not be created");

        TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr, NULL, NULL) == MA_ERROR_INVALIDARG, \
                            "ma_table_add_entry doesn't return MA_ERROR_INVALIDARG for invalid arguments");

        TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr, test_vector_table[counter].key, NULL) == MA_ERROR_INVALIDARG, \
                            "ma_table_add_entry doesn't return MA_ERROR_INVALIDARG for invalid arguments");

        TEST_ASSERT_MESSAGE(ma_table_add_entry(NULL, test_vector_table[counter].key, variant_ptr) == MA_ERROR_INVALIDARG, \
                            "ma_table_add_entry doesn't return MA_ERROR_INVALIDARG for invalid arguments");

        TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr, test_vector_table[counter].key, variant_ptr) == MA_OK, \
                            "Variant could not be pushed into the Table");

        ma_variant_release(variant_ptr);

        TEST_ASSERT_MESSAGE(ma_table_size(table_ptr, &table_size) == MA_OK, \
                            "ma_table_size doesn't returns MA_OK for valid arguments");

        TEST_ASSERT_MESSAGE((counter + 1) == table_size, \
                            "Table size is not incremented correctly after 1 variant is pushed");
    }

    /* Retrieving elements from table using foreach table API and verifying the value in callback function */
    TEST_ASSERT_MESSAGE(ma_table_foreach(NULL, NULL, NULL) == MA_ERROR_INVALIDARG,
                        "ma_table_foreach doesn't return MA_ERROR_INVALIDARG for invalid arguments");

    TEST_ASSERT_MESSAGE(ma_table_foreach(NULL, test_table_callback, NULL) == MA_ERROR_INVALIDARG,
                        "ma_table_foreach doesn't return MA_ERROR_INVALIDARG for invalid arguments");

    TEST_ASSERT_MESSAGE(ma_table_foreach(table_ptr, NULL, NULL) == MA_ERROR_PRECONDITION,
                        "ma_table_foreach doesn't return MA_ERROR_PRECONDITION when callback function is NULL");

    TEST_ASSERT_MESSAGE(ma_table_foreach(table_ptr, test_table_callback, NULL) == MA_OK,
                        "ma_table_foreach doesn't return MA_OK when table and callback functions are valid");

    TEST_ASSERT_MESSAGE(cb_counter == 9, "Call back function is not called in each iteration of foreach iterator");

    /* Increment and Decrement operations on Table of variants */

    /*Calling ma_table_inc_ref macro with address of variant as NULL*/
    TEST_ASSERT_MESSAGE(ma_table_add_ref(NULL) == MA_ERROR_INVALIDARG, \
                        "ma_table_inc_ref doesn't return MA_ERROR_INVALIDARG when argument passed is NULL");

    /*Calling ma_table_dec_ref macro with address of variant as NULL*/
    TEST_ASSERT_MESSAGE(ma_table_release(NULL) == MA_ERROR_INVALIDARG, \
                        "ma_table_dec_ref doesn't return MA_ERROR_INVALIDARG when argument passed is NULL");

    /*Verifying that the ma_table_inc_ref/ma_table_dec_ref macro maintains correct reference count*/

    /*Incrementing the table reference count (10 times) */
    for(counter = 0; counter < 10; counter++)
    {
        TEST_ASSERT_MESSAGE(ma_table_add_ref(table_ptr) == MA_OK, \
                            "ma_table_inc_ref could not increase table reference count");
    }

    /* Decrement the table reference count (11 times). The array should be destroyed
       by the end of this for-loop */
    for(counter = 0; counter < 11; counter++)
    {
        TEST_ASSERT_MESSAGE(ma_table_release(table_ptr) == MA_OK, \
                            "ma_table_dec_ref could not decrease table reference count");
    }
}


/*
Unit tests for ma_table_is_equal API.
*/
TEST(ma_variant_table_tests, ma_table_is_equal_test)
{
    /* 'ma_variant_t' type Pointer for storing the variant address */
    ma_variant_t *variant_ptr_1 = NULL;
    ma_variant_t *variant_ptr_2 = NULL;
    ma_variant_t *variant_ptr_3 = NULL;
    ma_variant_t *variant_ptr_4 = NULL;
    ma_variant_t *variant_ptr_5 = NULL;
    ma_variant_t *variant_ptr_6 = NULL;
    ma_variant_t *variant_ptr_7 = NULL;

    /* 'ma_table_t' table type pointers. Table of variants for comparison */
    ma_table_t *table_ptr_lhs = NULL;
    ma_table_t *table_ptr_rhs = NULL;

    /* Comparison Result */
    ma_bool_t comparison_result = MA_FALSE;


    /* Creating variants for pushing into the arrays */
    TEST_ASSERT_MESSAGE(ma_variant_create(&variant_ptr_1) == MA_OK, "NULL variant could not be created");
    TEST_ASSERT_MESSAGE(ma_variant_create_from_uint64(0x12345678ABCDEF09ULL, &variant_ptr_2) == MA_OK, "64 Bit unsigned variant could not be created");
    TEST_ASSERT_MESSAGE(ma_variant_create_from_int64(0x12345678ABCDEF09LL, &variant_ptr_3) == MA_OK, "64 Bit unsigned variant could not be created");
    TEST_ASSERT_MESSAGE(ma_variant_create_from_double(1.23e-18, &variant_ptr_4) == MA_OK, "Double variant could not be created");
    TEST_ASSERT_MESSAGE(ma_variant_create_from_uint32(0x1234ABCDUL, &variant_ptr_5) == MA_OK, "32 Bit unsigned variant could not be created");
    TEST_ASSERT_MESSAGE(ma_variant_create_from_int32(0x7FAB0123LL, &variant_ptr_6) == MA_OK, "32 Bit signed variant could not be created");
    TEST_ASSERT_MESSAGE(ma_variant_create_from_float(123.45f, &variant_ptr_7) == MA_OK, "Float variant could not be created");

    /* Scenario 1 - Creating Tables -> performing equality comparison on empty Tables -> Destroying Tables */
    TEST_ASSERT_MESSAGE(ma_table_create(&table_ptr_lhs) == MA_OK, "Table could not be created");
    TEST_ASSERT_MESSAGE(ma_table_create(&table_ptr_rhs) == MA_OK, "Table could not be created");

    TEST_ASSERT_MESSAGE(ma_table_is_equal(NULL, NULL, NULL) == MA_ERROR_INVALIDARG, \
                        "ma_table_is_equal doesn't return MA_ERROR_INVALIDARG for invalid arguments");

    TEST_ASSERT_MESSAGE(ma_table_is_equal(table_ptr_lhs, table_ptr_rhs, NULL) == MA_ERROR_INVALIDARG, \
                        "ma_table_is_equal doesn't return MA_ERROR_INVALIDARG for invalid arguments");

    TEST_ASSERT_MESSAGE(ma_table_is_equal(NULL, table_ptr_rhs, &comparison_result) == MA_ERROR_INVALIDARG, \
                        "ma_table_is_equal doesn't return MA_ERROR_INVALIDARG for invalid arguments");

    TEST_ASSERT_MESSAGE(ma_table_is_equal(table_ptr_lhs, NULL, &comparison_result) == MA_ERROR_INVALIDARG, \
                        "ma_table_is_equal doesn't return MA_ERROR_INVALIDARG for invalid arguments");

    TEST_ASSERT_MESSAGE(ma_table_is_equal(table_ptr_lhs, table_ptr_rhs, &comparison_result) == MA_OK, \
                        "ma_table_is_equal doesn't return MA_OK for empty tables");

    TEST_ASSERT_MESSAGE(MA_TRUE == comparison_result, "Table Comparison result is not correct for empty tables");

    TEST_ASSERT_MESSAGE(ma_table_release(table_ptr_lhs) == MA_OK, "Table could not be destroyed");
    TEST_ASSERT_MESSAGE(ma_table_release(table_ptr_rhs) == MA_OK, "Table could not be destroyed");

    /* Scenario 2 - Same variants present in the Tables in same sequence. LHS is a normal string while
       RHS is wide char string */
    TEST_ASSERT_MESSAGE(ma_table_create(&table_ptr_lhs) == MA_OK, "Table could not be created");
    TEST_ASSERT_MESSAGE(ma_table_create(&table_ptr_rhs) == MA_OK, "Table could not be created");

    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_lhs, "1", variant_ptr_1) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_lhs, "2", variant_ptr_2) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_lhs, "3", variant_ptr_3) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_lhs, "4", variant_ptr_4) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_lhs, "5", variant_ptr_5) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_lhs, "6", variant_ptr_6) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_lhs, "7", variant_ptr_7) == MA_OK, "Variant could not be added to table");

    TEST_ASSERT_MESSAGE(ma_table_add_wentry(table_ptr_rhs, L"1", variant_ptr_1) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_wentry(table_ptr_rhs, L"2", variant_ptr_2) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_wentry(table_ptr_rhs, L"3", variant_ptr_3) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_wentry(table_ptr_rhs, L"4", variant_ptr_4) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_wentry(table_ptr_rhs, L"5", variant_ptr_5) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_wentry(table_ptr_rhs, L"6", variant_ptr_6) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_wentry(table_ptr_rhs, L"7", variant_ptr_7) == MA_OK, "Variant could not be added to table");

    TEST_ASSERT_MESSAGE(ma_table_is_equal(table_ptr_lhs, table_ptr_rhs, &comparison_result) == MA_OK, \
                        "ma_table_is_equal doesn't return MA_OK while comparing valid tables");

    TEST_ASSERT_MESSAGE(MA_TRUE == comparison_result, "Table Comparison result is not correct");

    /* Scenario 3 - Comparing table with itself. Comparison should be true */
    TEST_ASSERT_MESSAGE(ma_table_is_equal(table_ptr_lhs, table_ptr_lhs, &comparison_result) == MA_OK, \
                        "ma_table_is_equal doesn't return MA_OK while comparing valid tables");

    TEST_ASSERT_MESSAGE(MA_TRUE == comparison_result, "Table Comparison result is not correct");

    TEST_ASSERT_MESSAGE(ma_table_is_equal(table_ptr_rhs, table_ptr_rhs, &comparison_result) == MA_OK, \
                        "ma_table_is_equal doesn't return MA_OK while comparing valid tables");

    TEST_ASSERT_MESSAGE(MA_TRUE == comparison_result, "Table Comparison result is not correct");

    TEST_ASSERT_MESSAGE(ma_table_release(table_ptr_lhs) == MA_OK, "Table could not be destroyed");
    TEST_ASSERT_MESSAGE(ma_table_release(table_ptr_rhs) == MA_OK, "Table could not be destroyed");

    /* Scenario 4 - Same variants present in the tables in different sequence. Comparison should be true */
    TEST_ASSERT_MESSAGE(ma_table_create(&table_ptr_lhs) == MA_OK, "Table could not be created");
    TEST_ASSERT_MESSAGE(ma_table_create(&table_ptr_rhs) == MA_OK, "Table could not be created");

    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_lhs, "1", variant_ptr_1) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_lhs, "2", variant_ptr_2) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_lhs, "3", variant_ptr_3) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_lhs, "4", variant_ptr_4) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_lhs, "5", variant_ptr_5) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_lhs, "6", variant_ptr_6) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_lhs, "7", variant_ptr_7) == MA_OK, "Variant could not be added to table");

    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_rhs, "1", variant_ptr_1) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_rhs, "2", variant_ptr_2) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_rhs, "3", variant_ptr_3) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_rhs, "4", variant_ptr_4) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_rhs, "5", variant_ptr_5) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_rhs, "7", variant_ptr_7) == MA_OK, "Variant could not be added to table");
	TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_rhs, "6", variant_ptr_6) == MA_OK, "Variant could not be added to table");
	
    TEST_ASSERT_MESSAGE(ma_table_is_equal(table_ptr_lhs, table_ptr_rhs, &comparison_result) == MA_OK, \
                        "ma_table_is_equal doesn't return MA_OK while comparing valid tables");

    TEST_ASSERT_MESSAGE(MA_TRUE == comparison_result, "Table Comparison result is not correct");

    TEST_ASSERT_MESSAGE(ma_table_release(table_ptr_lhs) == MA_OK, "Table could not be destroyed");
    TEST_ASSERT_MESSAGE(ma_table_release(table_ptr_rhs) == MA_OK, "Table could not be destroyed");

    /* Scenario 5 - Unequal number of variants in tables. 1 less element in RHS at the end. Comparison should be false */
    TEST_ASSERT_MESSAGE(ma_table_create(&table_ptr_lhs) == MA_OK, "Table could not be created");
    TEST_ASSERT_MESSAGE(ma_table_create(&table_ptr_rhs) == MA_OK, "Table could not be created");

    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_lhs, "1", variant_ptr_1) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_lhs, "2", variant_ptr_2) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_lhs, "3", variant_ptr_3) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_lhs, "4", variant_ptr_4) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_lhs, "5", variant_ptr_5) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_lhs, "6", variant_ptr_6) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_lhs, "7", variant_ptr_7) == MA_OK, "Variant could not be added to table");

    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_rhs, "1", variant_ptr_1) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_rhs, "2", variant_ptr_2) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_rhs, "3", variant_ptr_3) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_rhs, "4", variant_ptr_4) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_rhs, "5", variant_ptr_5) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_rhs, "6", variant_ptr_6) == MA_OK, "Variant could not be added to table");

    TEST_ASSERT_MESSAGE(ma_table_is_equal(table_ptr_lhs, table_ptr_rhs, &comparison_result) == MA_OK, \
                        "ma_table_is_equal doesn't return MA_OK while comparing valid tables");

    TEST_ASSERT_MESSAGE(MA_FALSE == comparison_result, "Table Comparison result is not correct");

    TEST_ASSERT_MESSAGE(ma_table_release(table_ptr_lhs) == MA_OK, "Table could not be destroyed");
    TEST_ASSERT_MESSAGE(ma_table_release(table_ptr_rhs) == MA_OK, "Table could not be destroyed");

    /* Scenario 6 - Unequal number of variants in tables. 1 element in less RHS in the middle. Comparison should be false */
    TEST_ASSERT_MESSAGE(ma_table_create(&table_ptr_lhs) == MA_OK, "Table could not be created");
    TEST_ASSERT_MESSAGE(ma_table_create(&table_ptr_rhs) == MA_OK, "Table could not be created");

    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_lhs, "1", variant_ptr_1) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_lhs, "2", variant_ptr_2) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_lhs, "3", variant_ptr_3) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_lhs, "4", variant_ptr_4) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_lhs, "5", variant_ptr_5) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_lhs, "6", variant_ptr_6) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_lhs, "7", variant_ptr_7) == MA_OK, "Variant could not be added to table");

    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_rhs, "1", variant_ptr_1) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_rhs, "2", variant_ptr_2) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_rhs, "3", variant_ptr_3) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_rhs, "5", variant_ptr_5) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_rhs, "6", variant_ptr_6) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_rhs, "7", variant_ptr_7) == MA_OK, "Variant could not be added to table");

    TEST_ASSERT_MESSAGE(ma_table_is_equal(table_ptr_lhs, table_ptr_rhs, &comparison_result) == MA_OK, \
                        "ma_table_is_equal doesn't return MA_OK while comparing valid tables");

    TEST_ASSERT_MESSAGE(MA_FALSE == comparison_result, "Table Comparison result is not correct");

    TEST_ASSERT_MESSAGE(ma_table_release(table_ptr_lhs) == MA_OK, "Table could not be destroyed");
    TEST_ASSERT_MESSAGE(ma_table_release(table_ptr_rhs) == MA_OK, "Table could not be destroyed");

    /* Scenario 7 - Equal number of variants in tables. Sequence of elements is reverse. Comparison should be true */
    TEST_ASSERT_MESSAGE(ma_table_create(&table_ptr_lhs) == MA_OK, "Table could not be created");
    TEST_ASSERT_MESSAGE(ma_table_create(&table_ptr_rhs) == MA_OK, "Table could not be created");

    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_lhs, "1", variant_ptr_1) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_lhs, "2", variant_ptr_2) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_lhs, "3", variant_ptr_3) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_lhs, "4", variant_ptr_4) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_lhs, "5", variant_ptr_5) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_lhs, "6", variant_ptr_6) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_lhs, "7", variant_ptr_7) == MA_OK, "Variant could not be added to table");

    TEST_ASSERT_MESSAGE(ma_table_add_wentry(table_ptr_rhs, L"1", variant_ptr_1) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_wentry(table_ptr_rhs, L"2", variant_ptr_2) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_wentry(table_ptr_rhs, L"3", variant_ptr_3) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_wentry(table_ptr_rhs, L"4", variant_ptr_4) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_wentry(table_ptr_rhs, L"5", variant_ptr_5) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_wentry(table_ptr_rhs, L"6", variant_ptr_6) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_wentry(table_ptr_rhs, L"7", variant_ptr_7) == MA_OK, "Variant could not be added to table");

    TEST_ASSERT_MESSAGE(ma_table_is_equal(table_ptr_lhs, table_ptr_rhs, &comparison_result) == MA_OK, \
                        "ma_table_is_equal doesn't return MA_OK while comparing valid tables");

    TEST_ASSERT_MESSAGE(MA_TRUE == comparison_result, "Table Comparison result is not correct");

    TEST_ASSERT_MESSAGE(ma_table_release(table_ptr_lhs) == MA_OK, "Table could not be destroyed");
    TEST_ASSERT_MESSAGE(ma_table_release(table_ptr_rhs) == MA_OK, "Table could not be destroyed");

    ma_variant_release(variant_ptr_1);
    ma_variant_release(variant_ptr_2);
    ma_variant_release(variant_ptr_3);
    ma_variant_release(variant_ptr_4);
    ma_variant_release(variant_ptr_5);
    ma_variant_release(variant_ptr_6);
    ma_variant_release(variant_ptr_7);

}

/*
Unit tests for self-reference to within a table variant i.e. the scenario which occurs
when following sequence is followed -
1) A table of variants is created.
2) The variant is created from the table created in step 1.
3) The variant created in step 2 is again pushed into the table created in step 1.
*/
TEST(ma_variant_table_tests, ma_variant_table_self_reference_test)
{
    /* 'ma_variant_t' type Pointer for storing the base address of variant object */
    ma_variant_t *table_variant_ptr = NULL;

    /* 'ma_variant_t' type Pointer for storing the variant address */
    ma_variant_t *variant_ptr = NULL;

    /* 'ma_table_t' table type pointer */
    ma_table_t *table_ptr = NULL;

    /* 'ma_table_t' table type pointer */
    ma_table_t *table_ptr_1 = NULL;

     /* This variable should hold MA_VARTYPE_TABLE after the ma_variant_get_type
       call. Initial value of this variable is given as MA_VARTYPE_STRING. */
    ma_vartype_t vartype_obj = MA_VARTYPE_STRING;

    /* Creating table of variants (Will be used for self reference) */
    TEST_ASSERT_MESSAGE(ma_table_create(&table_ptr) == MA_OK, \
                        "ma_table_create doesn't returns MA_OK for valid table pointer address");

    /* Creating table of variants (Should be able to successfully add a variant created from 'table_ptr' */
    TEST_ASSERT_MESSAGE(ma_table_create(&table_ptr_1) == MA_OK, \
                        "ma_table_create doesn't returns MA_OK for valid table pointer address");

    /* Creating variants of various types and pushing them into the table */

    /* NULL Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create(&variant_ptr) == MA_OK, "NULL variant could not be created");

    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr, "1", variant_ptr) == MA_OK, "Variant could not be pushed to table");
    ma_variant_release(variant_ptr);

    /* 64 Bit unsigned integer Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_uint64(0x12345678ABCDEF09ULL, &variant_ptr) == MA_OK, \
                        "64 Bit unsigned variant could not be created");

    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr, "2", variant_ptr) == MA_OK, "Variant could not be pushed to table");
    ma_variant_release(variant_ptr);

    /* 64 Bit signed integer Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_int64(0x12345678ABCDEF09LL, &variant_ptr) == MA_OK, \
                        "64 Bit unsigned variant could not be created");

    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr, "3", variant_ptr) == MA_OK, "Variant could not be pushed to table");
    ma_variant_release(variant_ptr);

    /* Double Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_double(1e+37, &variant_ptr) == MA_OK, "Double variant could not be created");

    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr, "4", variant_ptr) == MA_OK, "Variant could not be pushed to table");
    ma_variant_release(variant_ptr);

    /* 32 Bit unsigned integer Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_uint32(0x1234ABCDUL, &variant_ptr) == MA_OK, \
                        "32 Bit unsigned variant could not be created");

    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr, "5", variant_ptr) == MA_OK, "Variant could not be pushed to table");
    ma_variant_release(variant_ptr);

    /* 32 Bit signed integer Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_int32(0x7FAB0123LL, &variant_ptr) == MA_OK, \
                        "32 Bit signed variant could not be created");

    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr, "6", variant_ptr) == MA_OK, "Variant could not be pushed to table");
    ma_variant_release(variant_ptr);

    /* Float Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_float(123.45f, &variant_ptr) == MA_OK, \
                        "Float variant could not be created");

    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr, "7", variant_ptr) == MA_OK, "Variant could not be pushed to table");
    ma_variant_release(variant_ptr);

    /* 16 Bit unsigned integer Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_uint16(0x1234, &variant_ptr) == MA_OK, \
                        "16 Bit unsigned variant could not be created");

    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr, "8", variant_ptr) == MA_OK, "Variant could not be pushed to table");
    ma_variant_release(variant_ptr);

    /* 16 Bit signed integer Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_int16(0x7FAB, &variant_ptr) == MA_OK, \
                        "16 Bit signed variant could not be created");

    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr, "9", variant_ptr) == MA_OK, "Variant could not be pushed to table");
    ma_variant_release(variant_ptr);

    /* 8 Bit unsigned integer Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_uint8(0x12, &variant_ptr) == MA_OK, \
                        "8 Bit unsigned variant could not be created");

    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr, "a", variant_ptr) == MA_OK, "Variant could not be pushed to table");
    ma_variant_release(variant_ptr);

    /* 8 Bit signed integer Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_int8(0x7F, &variant_ptr) == MA_OK, \
                        "8 Bit signed variant could not be created");

    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr, "b", variant_ptr) == MA_OK, "Variant could not be pushed to table");
    ma_variant_release(variant_ptr);

    /* Boolean Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_bool(123, &variant_ptr) == MA_OK, \
                        "Boolean variant could not be created");

    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr, "c", variant_ptr) == MA_OK, "Variant could not be pushed to table");
    ma_variant_release(variant_ptr);

    /* Creating variant from the table created previously in this test. */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_table(table_ptr, &table_variant_ptr) == MA_OK, \
                        "ma_variant_create_from_table could not create variant from table");

    /* Checking the type of variant (table type) */
    TEST_ASSERT_MESSAGE(ma_variant_get_type(table_variant_ptr, &vartype_obj) == MA_OK, \
                        "ma_variant_get_type doesn't return MA_OK for Table Variant");

    TEST_ASSERT_MESSAGE(vartype_obj == MA_VARTYPE_TABLE, \
                        "ma_variant_get_type doesn't return MA_VARTYPE_TABLE for table variant");

    /* Pushing the variant (created from table) back into the same table...self-reference */
    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr, "13", table_variant_ptr) == MA_ERROR_INVALIDARG, \
                        "Variant incorrectly pushed to table (self-reference permitted) ");

    /* Pushing the variant (created from table) into a different array...not a self-reference */
    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_1, "1", table_variant_ptr) == MA_OK, \
                        "Variant could not be pushed to table");

    ma_variant_release(table_variant_ptr);

    ma_table_release(table_ptr_1);
    ma_table_release(table_ptr);

}

/*
Unit tests verifying that two keys with same initial characters can be successfully added and
retrieved into the table.
*/
TEST(ma_variant_table_tests, ma_table_keys_with_same_prefix) {
    ma_table_t *table_ptr;
    ma_variant_t *variant_ptr;
    size_t table_size;
    TEST_ASSERT_EQUAL_MESSAGE(MA_OK, ma_table_create(&table_ptr), "ma_table_create failed");
    TEST_ASSERT_EQUAL_MESSAGE(MA_OK, ma_variant_create(&variant_ptr), "ma_variant_create failed");

    TEST_ASSERT_EQUAL_MESSAGE(MA_OK, ma_table_add_entry(table_ptr, "ABC", variant_ptr), "failure adding entry to table");
    TEST_ASSERT_EQUAL_MESSAGE(MA_OK, ma_table_add_entry(table_ptr, "ABC123", variant_ptr), "failure adding entry to table");

    TEST_ASSERT_EQUAL_MESSAGE(MA_OK, ma_table_size(table_ptr, &table_size), "Unable to obtain table size");

    TEST_ASSERT_EQUAL_MESSAGE(2, table_size, "Table size is not 2");
    ma_variant_release(variant_ptr);

    TEST_ASSERT_EQUAL_MESSAGE(MA_OK, ma_table_get_value(table_ptr, "ABC", &variant_ptr), "unable to get 'ABC' value");
    ma_variant_release(variant_ptr);

    TEST_ASSERT_EQUAL_MESSAGE(MA_OK, ma_table_get_value(table_ptr, "ABC123", &variant_ptr), "unable to get 'ABC123' value");
    ma_variant_release(variant_ptr);

    ma_table_release(table_ptr);
}

#ifdef MA_HAS_WCHAR_T

/*
Unit tests verifying that two wide character keys with same initial characters can be successfully added and
retrieved into the table.
*/
TEST(ma_variant_table_tests, ma_table_wwkeys_with_same_prefix) {
    ma_table_t *table_ptr;
    ma_variant_t *variant_ptr;
    size_t table_size;
    TEST_ASSERT_EQUAL_MESSAGE(MA_OK, ma_table_create(&table_ptr),"ma_table_create failed");
    TEST_ASSERT_EQUAL_MESSAGE(MA_OK, ma_variant_create(&variant_ptr),"ma_variant_create failed");

    TEST_ASSERT_EQUAL_MESSAGE(MA_OK, ma_table_add_wentry(table_ptr, L"ABC", variant_ptr), "failure adding entry to table");
    TEST_ASSERT_EQUAL_MESSAGE(MA_OK, ma_table_add_wentry(table_ptr, L"ABC123", variant_ptr), "failure adding entry to table");

    TEST_ASSERT_EQUAL_MESSAGE(MA_OK, ma_table_size(table_ptr, &table_size), "Unable to obtain table size");

    TEST_ASSERT_EQUAL_MESSAGE(2, table_size, "Table size is not 2");
    ma_variant_release(variant_ptr);

    TEST_ASSERT_EQUAL_MESSAGE(MA_OK, ma_table_get_wvalue(table_ptr, L"ABC", &variant_ptr), "unable to get 'ABC' value");
    ma_variant_release(variant_ptr);

    TEST_ASSERT_EQUAL_MESSAGE(MA_OK, ma_table_get_wvalue(table_ptr, L"ABC123",&variant_ptr), "unable to get 'ABC123' value");
    ma_variant_release(variant_ptr);

    ma_table_release(table_ptr);
}

/*
Unit tests verifying that a normal character set key and a wide character set key with same initial characters
can be successfully added and retrieved into the table.
*/
TEST(ma_variant_table_tests, ma_table_cwkeys_with_same_prefix) {
    ma_table_t *table_ptr;
    ma_variant_t *variant_ptr;
    size_t table_size;
    TEST_ASSERT_EQUAL_MESSAGE(MA_OK, ma_table_create(&table_ptr),"ma_table_create failed");
    TEST_ASSERT_EQUAL_MESSAGE(MA_OK, ma_variant_create(&variant_ptr),"ma_variant_create failed");

    TEST_ASSERT_EQUAL_MESSAGE(MA_OK, ma_table_add_entry(table_ptr, "ABC", variant_ptr), "failure adding entry to table");
    TEST_ASSERT_EQUAL_MESSAGE(MA_OK, ma_table_add_wentry(table_ptr, L"ABC123", variant_ptr), "failure adding entry to table");

    TEST_ASSERT_EQUAL_MESSAGE(MA_OK, ma_table_size(table_ptr, &table_size), "Unable to obtain table size");

    TEST_ASSERT_EQUAL_MESSAGE(2, table_size, "Table size is not 2");
    ma_variant_release(variant_ptr);

    TEST_ASSERT_EQUAL_MESSAGE(MA_OK, ma_table_get_wvalue(table_ptr, L"ABC", &variant_ptr), "unable to get 'ABC' value");
    ma_variant_release(variant_ptr);

    TEST_ASSERT_EQUAL_MESSAGE(MA_OK, ma_table_get_wvalue(table_ptr, L"ABC123",&variant_ptr), "unable to get 'ABC123' value");
    ma_variant_release(variant_ptr);

    ma_table_release(table_ptr);
}

/*
Unit tests verifying that a normal character set key and a wide character set key with same initial characters
can be successfully added and retrieved into the table.
*/
TEST(ma_variant_table_tests, ma_table_wckeys_with_same_prefix) {
    ma_table_t *table_ptr;
    ma_variant_t *variant_ptr;
    size_t table_size;
    TEST_ASSERT_EQUAL_MESSAGE(MA_OK, ma_table_create(&table_ptr),"ma_table_create failed");
    TEST_ASSERT_EQUAL_MESSAGE(MA_OK, ma_variant_create(&variant_ptr),"ma_variant_create failed");

    TEST_ASSERT_EQUAL_MESSAGE(MA_OK, ma_table_add_wentry(table_ptr, L"ABC", variant_ptr), "failure adding entry to table");
    TEST_ASSERT_EQUAL_MESSAGE(MA_OK, ma_table_add_entry(table_ptr, "ABC123", variant_ptr), "failure adding entry to table");

    TEST_ASSERT_EQUAL_MESSAGE(MA_OK, ma_table_size(table_ptr, &table_size), "Unable to obtain table size");

    TEST_ASSERT_EQUAL_MESSAGE(2, table_size, "Table size is not 2");
    ma_variant_release(variant_ptr);

    TEST_ASSERT_EQUAL_MESSAGE(MA_OK, ma_table_get_wvalue(table_ptr, L"ABC", &variant_ptr), "unable to get 'ABC' value");
    ma_variant_release(variant_ptr);

    TEST_ASSERT_EQUAL_MESSAGE(MA_OK, ma_table_get_wvalue(table_ptr, L"ABC123",&variant_ptr), "unable to get 'ABC123' value");
    ma_variant_release(variant_ptr);

    ma_table_release(table_ptr);
}




/*
Unit tests for following APIs -
1) ma_table_add_wentry
2) ma_table_get_wvalue
*/
TEST(ma_variant_table_tests, ma_variant_create_get_from_wtable_test)
{
    /* 'ma_variant_t' type Pointer for storing the variant address */
    ma_variant_t *variant_ptr = NULL;

    /* 'ma_table_t' table type pointer */
    ma_table_t *table_ptr = NULL;

    /* This variable should hold MA_VARTYPE_TABLE after the ma_variant_get_type
       call. Initial value of this variable is given as MA_VARTYPE_STRING. */
    ma_vartype_t vartype_obj = MA_VARTYPE_STRING;

    /* Variables for storing values retrieved from table */
    ma_uint64_t  unsigned_64bit_val = 0ULL;
    ma_int64_t   signed_64bit_val = 0LL;
    ma_uint32_t  unsigned_32bit_val = 0UL;
    ma_int32_t   signed_32bit_val = 0L;

    TEST_ASSERT_MESSAGE(ma_table_create(&table_ptr) == MA_OK, \
                        "ma_table_create doesn't returns MA_OK for valid table pointer address");

    /* NULL Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create(&variant_ptr) == MA_OK, "NULL variant could not be created");

    TEST_ASSERT_MESSAGE(ma_table_add_wentry(table_ptr, L"First Entry", variant_ptr) == MA_OK, "Variant could not be pushed to table");
    ma_variant_release(variant_ptr);

    /* 64 Bit unsigned integer Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_uint64(0x12345678ABCDEF09ULL, &variant_ptr) == MA_OK, \
                        "64 Bit unsigned variant could not be created");

    TEST_ASSERT_MESSAGE(ma_table_add_wentry(table_ptr, L"Second Entry", variant_ptr) == MA_OK, "Variant could not be pushed to table");
    ma_variant_release(variant_ptr);

    /* 64 Bit signed integer Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_int64(0x12345678ABCDEF09LL, &variant_ptr) == MA_OK, \
                        "64 Bit unsigned variant could not be created");

    TEST_ASSERT_MESSAGE(ma_table_add_wentry(table_ptr, L"Third Entry", variant_ptr) == MA_OK, "Variant could not be pushed to table");
    ma_variant_release(variant_ptr);

    /* 32 Bit unsigned integer Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_uint32(0x1234ABCDUL, &variant_ptr) == MA_OK, \
                        "32 Bit unsigned variant could not be created");

    TEST_ASSERT_MESSAGE(ma_table_add_wentry(table_ptr, L"Fourth Entry", variant_ptr) == MA_OK, "Variant could not be pushed to table");
    ma_variant_release(variant_ptr);

    /* 32 Bit signed integer Variant */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_int32(0x7FAB0123LL, &variant_ptr) == MA_OK, \
                        "32 Bit signed variant could not be created");

    TEST_ASSERT_MESSAGE(ma_table_add_wentry(table_ptr, L"Fifth Entry", variant_ptr) == MA_OK, "Variant could not be pushed to table");
    ma_variant_release(variant_ptr);

     /* 5 variants were pushed into the table. Reading them back */

    /* Reading variant for key 'First Entry' */
    TEST_ASSERT_MESSAGE(ma_table_get_wvalue(table_ptr, L"First Entry", &variant_ptr) == MA_OK, \
                        "ma_table_get_wvalue couldn't retrieve variant from table");

    TEST_ASSERT_MESSAGE(ma_variant_get_type(variant_ptr, &vartype_obj) == MA_OK, \
                        "ma_variant_get_type doesn't return MA_OK for valid Variant");

    TEST_ASSERT_MESSAGE(vartype_obj == MA_VARTYPE_NULL, \
                        "NULL variant should have been present in table, but it is not");
    ma_variant_release(variant_ptr);

    /* Reading variant for key 'Second Entry' */
    TEST_ASSERT_MESSAGE(ma_table_get_wvalue(table_ptr, L"Second Entry", &variant_ptr) == MA_OK, \
                        "ma_table_get_wvalue couldn't retrieve variant from table");

    TEST_ASSERT_MESSAGE(ma_variant_get_uint64(variant_ptr, &unsigned_64bit_val) == MA_OK, \
                        "Value could not be retrieved from the variant");

    TEST_ASSERT_MESSAGE(0x12345678ABCDEF09ULL == unsigned_64bit_val, "Correct value is not retrieved from variant");
    ma_variant_release(variant_ptr);

    /* Reading variant for key 'Third Entry' */
    TEST_ASSERT_MESSAGE(ma_table_get_wvalue(table_ptr, L"Third Entry", &variant_ptr) == MA_OK, \
                        "ma_table_get_wvalue couldn't retrieve variant from table");

    TEST_ASSERT_MESSAGE(ma_variant_get_int64(variant_ptr, &signed_64bit_val) == MA_OK, \
                        "Value could not be retrieved from the variant");

    TEST_ASSERT_MESSAGE(0x12345678ABCDEF09LL == signed_64bit_val, "Correct value is not retrieved from variant");
    ma_variant_release(variant_ptr);

    /* Reading variant for key 'Fourth Entry' */
    TEST_ASSERT_MESSAGE(ma_table_get_wvalue(table_ptr, L"Fourth Entry", &variant_ptr) == MA_OK, \
                        "ma_table_get_wvalue couldn't retrieve variant from table");

    TEST_ASSERT_MESSAGE(ma_variant_get_uint32(variant_ptr, &unsigned_32bit_val) == MA_OK, \
                        "Value could not be retrieved from the variant");

    TEST_ASSERT_MESSAGE(0x1234ABCDUL == unsigned_32bit_val, "Correct value is not retrieved from variant");
    ma_variant_release(variant_ptr);

    /* Reading variant for key 'Fifth Entry' */
    TEST_ASSERT_MESSAGE(ma_table_get_wvalue(table_ptr, L"Fifth Entry", &variant_ptr) == MA_OK, \
                        "ma_table_get_wvalue couldn't retrieve variant from table");

    TEST_ASSERT_MESSAGE(ma_variant_get_int32(variant_ptr, &signed_32bit_val) == MA_OK, \
                        "Value could not be retrieved from the variant");

    TEST_ASSERT_MESSAGE(0x7FAB0123LL == signed_32bit_val, "Correct value is not retrieved from variant");
    ma_variant_release(variant_ptr);

    ma_table_release(table_ptr);

}


/*
Unit tests for following APIs -
1) ma_table_create
2) ma_table_add_wentry
3) ma_table_size
4) ma_table_wforeach
*/
TEST(ma_variant_table_tests, ma_variant_foreach_element_from_wtable_test)
{
    /* 'ma_table_t' table type pointer */
    ma_table_t *table_ptr = NULL;

    /* For storing table size returned by ma_table_size API */
    size_t table_size = 100;

    // Loop iterator.
    ma_uint8_t counter = 0;

    TEST_ASSERT_MESSAGE(ma_table_create(&table_ptr) == MA_OK, \
                        "Table could not be created");

    /* ma_table_size returns 0 before any variant is added */
    TEST_ASSERT_MESSAGE(ma_table_size(table_ptr, &table_size) == MA_OK, \
                        "ma_table_size doesn't returns MA_OK for valid arguments");

    TEST_ASSERT_MESSAGE(0 == table_size, "ma_table_size doesn't return 0 table size for an empty table");

    /* Creating a list of 32-bit unsigned integer variants and pushing them into the table */
    for(counter = 0; counter < sizeof(test_vector_table)/sizeof(test_vector_table[0]); counter++)
    {
        /* 'ma_variant_t' type Pointer for storing the variant address */
        ma_variant_t *variant_ptr = NULL;
        TEST_ASSERT_MESSAGE(ma_variant_create_from_uint32(test_vector_table[counter].value, &variant_ptr) == MA_OK, \
                            "32 Bit unsigned variant could not be created");

        TEST_ASSERT_MESSAGE(ma_table_add_wentry(table_ptr, NULL, NULL) == MA_ERROR_INVALIDARG, \
                            "ma_table_add_wentry doesn't return MA_ERROR_INVALIDARG for invalid arguments");

        TEST_ASSERT_MESSAGE(ma_table_add_wentry(table_ptr, test_vector_table[counter].wkey, NULL) == MA_ERROR_INVALIDARG, \
                            "ma_table_add_entry doesn't return MA_ERROR_INVALIDARG for invalid arguments");

        TEST_ASSERT_MESSAGE(ma_table_add_wentry(NULL, test_vector_table[counter].wkey, variant_ptr) == MA_ERROR_INVALIDARG, \
                            "ma_table_add_entry doesn't return MA_ERROR_INVALIDARG for invalid arguments");

        TEST_ASSERT_MESSAGE(ma_table_add_wentry(table_ptr, test_vector_table[counter].wkey, variant_ptr) == MA_OK, \
                            "Variant could not be pushed into the Table");

        TEST_ASSERT_MESSAGE(ma_table_size(table_ptr, &table_size) == MA_OK, \
                            "ma_table_size doesn't returns MA_OK for valid arguments");

        TEST_ASSERT_MESSAGE((counter + 1) == table_size, \
                            "Table size is not incremented correctly after 1 variant is pushed");
        ma_variant_release(variant_ptr);
    }

    /* Retrieving elements from table using foreach Table API and verifying the value in callback function */
    TEST_ASSERT_MESSAGE(ma_table_wforeach(NULL, NULL, NULL) == MA_ERROR_INVALIDARG,
                        "ma_table_wforeach doesn't return MA_ERROR_INVALIDARG for invalid arguments");

    TEST_ASSERT_MESSAGE(ma_table_wforeach(NULL, test_wtable_callback, NULL) == MA_ERROR_INVALIDARG,
                        "ma_table_wforeach doesn't return MA_ERROR_INVALIDARG for invalid arguments");

    TEST_ASSERT_MESSAGE(ma_table_wforeach(table_ptr, NULL, NULL) == MA_ERROR_PRECONDITION,
                        "ma_table_wforeach doesn't return MA_ERROR_PRECONDITION when callback function is NULL");

    TEST_ASSERT_MESSAGE(ma_table_wforeach(table_ptr, test_wtable_callback, NULL) == MA_OK,
                        "ma_table_wforeach doesn't return MA_OK when table and callback functions are valid");

    TEST_ASSERT_MESSAGE(wcb_counter == 9, "Call back function is not called in each iteration of foreach iterator");

    ma_table_release(table_ptr);
}

#endif

/*
Unit tests verifying that a ma_table_get_value call on a empty table
with a random key returns MA_OK.
*/
TEST(ma_variant_table_tests, ma_variant_check_empty_table_returns_ok)
{
    ma_variant_t *value;
    ma_table_t *table;
    TEST_ASSERT_EQUAL(MA_OK, ma_table_create(&table));

    TEST_ASSERT_EQUAL(MA_ERROR_OBJECTNOTFOUND, ma_table_get_value(table, "not there", &value));

    ma_table_release(table);
}


/******************************************************************************
Utility function for generating random string. Fills the passed string of given
length with any one character from the below character set.
******************************************************************************/
const char charset[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*()+_:<>?";

void generate_random_string(char *str, unsigned int length)
{
    unsigned int counter = 0;
    static int iterator = 1;

    for(counter = 0; counter < length; counter++)
    {
        str[counter] = charset[(iterator * counter) % strlen(charset)];
    }
    iterator++;
}

/*
Unit tests where large number of key-value pairs are added into the table
and then retrieved successfully.
*/
TEST(ma_variant_table_tests, ma_variant_check_for_large_number_of_keys)
{
    /* 'ma_variant_t' type Pointer for storing the base address of variant object */
    ma_variant_t *table_variant_ptr = NULL;

    /* 'ma_variant_t' type Pointer for storing the variant address */
    ma_variant_t *variant_ptr = NULL;

    /* 'ma_table_t' table type pointer */
    ma_table_t *table_ptr = NULL;

    /* Table pointer retrieved from the table variant */
    ma_table_t *table_ptr_1 = NULL;

    /* Variant Pointer retrieved from array */
    ma_variant_t *variant_ptr_1 = NULL;

    /*
    To hold the values which will be entered into the table. Modify the values
    here, if the tests is to be executed for a different number of key-value
    pairs or different key size or value size.
    */
    char key[20];
    char *values[5000] = {NULL};
    unsigned int value_size = 1024;

    /* Loop variable */
    int counter = 0;

    /* For storing array size returned by ma_table_size API */
    size_t table_size = 100;

    /* This variable should hold MA_VARTYPE_TABLE after the ma_variant_get_type
       call. Initial value of this variable is given as MA_VARTYPE_STRING. */
    ma_vartype_t vartype_obj = MA_VARTYPE_STRING;

    /* Variables for storing values retrieved from table */
    ma_buffer_t  *buffer_ptr = NULL;
    const char   *string_val = NULL;
    size_t       string_size = 0;

    TEST_ASSERT_MESSAGE(ma_table_create(&table_ptr) == MA_OK, \
                        "ma_table_create doesn't returns MA_OK for valid table pointer address");

    /*
    Creating variants of strings -> pushing them into the table -> Checking that the size of
    table is incremented by 1
    */
    for(counter = 0; counter < sizeof(values)/sizeof(values[0]); counter++)
    {
         // Creating a unique string key value
         sprintf(key, "%d", counter);

         /* Generate a random value (Allocating memory for storing the value (1 extra byte for NULL)). */
         values[counter] = (char *)calloc(value_size + 1, 1);
         generate_random_string(values[counter], value_size);

         /* creating a string variant from the value and adding it to table. */
         TEST_ASSERT_MESSAGE(ma_variant_create_from_string(values[counter], value_size, &variant_ptr) == MA_OK, \
                            "variant could not be created from the string.");

         TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr, key, variant_ptr) == MA_OK, "Variant could not be pushed to table");

         ma_variant_release(variant_ptr);
         variant_ptr = NULL;

         TEST_ASSERT_MESSAGE(ma_table_size(table_ptr, &table_size) == MA_OK, "ma_table_size doesn't returns MA_OK for valid arguments");

         TEST_ASSERT_MESSAGE(table_size == (counter + 1), "ma_table_size doesn't return correct table size");
    }

    /* Creating Variant from the table */
    TEST_ASSERT_MESSAGE(ma_variant_create_from_table(table_ptr, &table_variant_ptr) == MA_OK, \
                        "ma_variant_create_from_table could not create variant from table");

    ma_table_release(table_ptr);

    /* Checking the type of variant (table type) */
    TEST_ASSERT_MESSAGE(ma_variant_get_type(table_variant_ptr, &vartype_obj) == MA_OK, \
                        "ma_variant_get_type doesn't return MA_OK for Table Variant");

    TEST_ASSERT_MESSAGE(vartype_obj == MA_VARTYPE_TABLE, \
                        "ma_variant_get_type doesn't return MA_VARTYPE_ARRAY for array variant");

    /* Retrieving table back from the Variant */
    TEST_ASSERT_MESSAGE(ma_variant_get_table(table_variant_ptr, &table_ptr_1) == MA_OK, \
                        "ma_variant_get_table could not retrieve table from the table variant");

    ma_variant_release(table_variant_ptr);


    /* Reading elements back from the retrieved table in the same order that they were added*/
   for(counter = 0; counter < sizeof(values)/sizeof(values[0]); counter++)
    {
        // Creating a unique string key value
        sprintf(key, "%d", counter);

        TEST_ASSERT_MESSAGE(ma_table_get_value(table_ptr_1, key, &variant_ptr_1) == MA_OK, \
                            "ma_table_get_value couldn't retrieve variant from table");

        TEST_ASSERT_MESSAGE(ma_variant_get_string_buffer(variant_ptr_1, &buffer_ptr) == MA_OK, \
                            "String Buffer cannot be retrieved from the Variant");

        TEST_ASSERT_MESSAGE(ma_buffer_get_string(buffer_ptr, &string_val, &string_size) == MA_OK, \
                            "ma_buffer_get_string could not retrieve string and its size from the buffer");

        TEST_ASSERT_MESSAGE(value_size == string_size, "String size is not read correctly from the buffer");

        TEST_ASSERT_EQUAL_MEMORY_MESSAGE(values[counter], string_val, value_size, "String is not read correctly from the buffer");

        ma_buffer_release(buffer_ptr);
        ma_variant_release(variant_ptr_1);
    }

    /* Reading elements back from the table in the reverse order that they were added*/
    for(counter = sizeof(values)/sizeof(values[0]) - 1; counter >= 0; counter--)
    {
        sprintf(key, "%d", counter);

        TEST_ASSERT_MESSAGE(ma_table_get_value(table_ptr_1, key, &variant_ptr_1) == MA_OK, \
                            "ma_table_get_value couldn't retrieve variant from table");

        TEST_ASSERT_MESSAGE(ma_variant_get_string_buffer(variant_ptr_1, &buffer_ptr) == MA_OK, \
                            "String Buffer cannot be retrieved from the Variant");

        ma_variant_release(variant_ptr_1);

        TEST_ASSERT_MESSAGE(ma_buffer_get_string(buffer_ptr, &string_val, &string_size) == MA_OK, \
                            "ma_buffer_get_string could not retrieve string and its size from the buffer");

        TEST_ASSERT_MESSAGE(value_size == string_size, "String size is not read correctly from the buffer");

        TEST_ASSERT_EQUAL_MEMORY_MESSAGE(values[counter], string_val, value_size, "String is not read correctly from the buffer");

        ma_buffer_release(buffer_ptr);

        // Releasing memory allocated to values
        free(values[counter]);
    }


    ma_table_release(table_ptr_1);
}


TEST(ma_variant_table_tests, ma_variant_remove_entries_test)
{
    ma_table_t *table = NULL;
    const char *char_key = "char_key";
    const wchar_t *wchar_key = L"wchar_key";
    size_t table_size = 0;

    TEST_ASSERT_MESSAGE(MA_OK == ma_table_create(&table), "table creation failed");
    {
        ma_variant_t *char_variant = NULL;
        TEST_ASSERT_MESSAGE(MA_OK == ma_variant_create(&char_variant), "variant creation failed");
        TEST_ASSERT_MESSAGE(MA_OK == ma_table_add_entry(table, char_key, char_variant), "adding char variant");
        TEST_ASSERT_MESSAGE(MA_OK == ma_variant_release(char_variant), "variant release failed");
    }
    {
        ma_variant_t *wchar_variant = NULL;
        TEST_ASSERT_MESSAGE(MA_OK == ma_variant_create(&wchar_variant), "variant creation failed");
        TEST_ASSERT_MESSAGE(MA_OK == ma_table_add_wentry(table, wchar_key, wchar_variant), "adding wide char variant");
        TEST_ASSERT_MESSAGE(MA_OK == ma_variant_release(wchar_variant), "variant release failed");
    }

    TEST_ASSERT_MESSAGE(MA_OK == ma_table_size(table, &table_size), "table size extraction failed");
    TEST_ASSERT_MESSAGE(2 == table_size, "table size different");

    TEST_ASSERT_MESSAGE(MA_OK == ma_table_remove_entry(table, char_key), "table entry remove failed");

    TEST_ASSERT_MESSAGE(MA_OK == ma_table_size(table, &table_size), "table size extraction failed");
    TEST_ASSERT_MESSAGE(1 == table_size, "table size different");

    TEST_ASSERT_MESSAGE(MA_OK == ma_table_remove_wentry(table, wchar_key), "table entry remove failed");

    TEST_ASSERT_MESSAGE(MA_OK == ma_table_size(table, &table_size), "table size extraction failed");
    TEST_ASSERT_MESSAGE(0 == table_size, "table size different");

    {
        ma_variant_t *char_variant = NULL;
        TEST_ASSERT_MESSAGE(MA_OK == ma_variant_create(&char_variant), "variant creation failed");
        TEST_ASSERT_MESSAGE(MA_OK == ma_table_add_entry(table, char_key, char_variant), "adding char variant");
        TEST_ASSERT_MESSAGE(MA_OK == ma_variant_release(char_variant), "variant release failed");
    }

    {
        ma_variant_t *wchar_variant = NULL;
        TEST_ASSERT_MESSAGE(MA_OK == ma_variant_create(&wchar_variant), "variant creation failed");
        TEST_ASSERT_MESSAGE(MA_OK == ma_table_add_wentry(table, wchar_key, wchar_variant), "adding wide char variant");
        TEST_ASSERT_MESSAGE(MA_OK == ma_variant_release(wchar_variant), "variant release failed");
    }

    TEST_ASSERT_MESSAGE(MA_OK == ma_table_remove_entry(table, "wchar_key"), "table entry remove failed");

    TEST_ASSERT_MESSAGE(MA_OK == ma_table_size(table, &table_size), "table size extraction failed");
    TEST_ASSERT_MESSAGE(1 == table_size, "table size different");

    TEST_ASSERT_MESSAGE(MA_OK == ma_table_remove_wentry(table, L"char_key"), "table entry remove failed");

    TEST_ASSERT_MESSAGE(MA_OK == ma_table_size(table, &table_size), "table size extraction failed");
    TEST_ASSERT_MESSAGE(0 == table_size, "table size different");

    TEST_ASSERT_MESSAGE(MA_OK == ma_table_release(table), "table release failed");
}

