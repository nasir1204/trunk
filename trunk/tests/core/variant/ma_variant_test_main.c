/*****************************************************************************
Main Test runner for all Variant library unit tests.
******************************************************************************/

#include "unity.h"
#include "unity_fixture.h"

#ifdef MA_WINDOWS
#include <windows.h>
#endif
#ifdef _MSC_VER
# include <crtdbg.h> /* */
# define MA_TRACKLEAKS _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF)
#else
# define MA_TRACKLEAKS ((void *)0)
#endif


static void run_all_tests(void) {
    do {RUN_TEST_GROUP(ma_variant_basic_types_test_group); } while (0);
    do {RUN_TEST_GROUP(ma_variant_buffer_test_group); } while (0);
    do {RUN_TEST_GROUP(ma_variant_raw_test_group);} while (0);
    do {RUN_TEST_GROUP(ma_variant_string_test_group);} while (0);
    do {RUN_TEST_GROUP(ma_variant_array_test_group);} while (0);
    do {RUN_TEST_GROUP(ma_variant_table_test_group);} while (0);
    //do {RUN_TEST_GROUP(ma_variant_outofmemory_test_group);} while (0);
}

int main(int argc, char *argv[]) {
    MA_TRACKLEAKS;
    return UnityMain(argc, argv, run_all_tests);
}
