/******************************************************************************
Unit tests for Service Manager Client.
******************************************************************************/

#include "unity.h"
#include "unity_fixture.h"
#include "unity_internals.h"

#include "ma/ma_service_manager.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_message.h"
#include "ma/logger/ma_console_logger.h"
#include <uv.h>
#include <string.h>

#define UT_PRODUCT_ID "MA_SRVC_MGR_CLIENT"
#define DUMMY_TOPIC_FILTER "abc.xyz.pqr.*"
#define DUMMY_SERVICE_FILTER "pqr.xyz.abc.*"

TEST_GROUP_RUNNER(ma_service_mgr_client_test_group)
{
    do {RUN_TEST_CASE(ma_service_mgr_client_tests, subscriber_notification_test_single_topic)} while (0);
    do {RUN_TEST_CASE(ma_service_mgr_client_tests, subscriber_notification_test_multiple_topics)} while (0);
    do {RUN_TEST_CASE(ma_service_mgr_client_tests, subscriber_list_test)} while (0);
    do {RUN_TEST_CASE(ma_service_mgr_client_tests, subscriber_list_async_test)} while (0);
    do {RUN_TEST_CASE(ma_service_mgr_client_tests, services_list_test)} while (0);
}

/******************************************************************************
Global variable declarations
******************************************************************************/
// Service manager client message bus instance
static ma_msgbus_t *service_mgr_msgbus_obj = NULL;

// Service Manager client
static ma_service_manager_t *service_manager_obj = NULL;

// Dummy service manager service object
static ma_msgbus_server_t *server = NULL;

// Logger object
static ma_console_logger_t *console_logger = NULL;

// Mutex variables
static ma_uint8_t done = 0;
static uv_cond_t cond;
static uv_mutex_t mux;

// These variables store the "topic" and registration/un-registration sent to the notification
// callback function. These values are used for verification within the tests.
static const char *topic_registered_1 = NULL, *product_id_1 = NULL;
static ma_bool_t notification_type_registered_1 = MA_FALSE;
static const char *topic_registered_2 = NULL, *product_id_2 = NULL;
static ma_bool_t notification_type_registered_2 = MA_FALSE;
static const char *topic_registered_3 = NULL, *product_id_3 = NULL;
static ma_bool_t notification_type_registered_3 = MA_FALSE;

// Test scenario selector
ma_uint8_t test_scenario = 0;

/* Subscriber List and Service List */
struct LookupTable
{
    const char *topic_or_service_name;
    const char *product_id;
};

struct LookupTable test_vector_subscribers[] = {
                                                   { "topic1.sub-topic1.*",          "Product 8" },
                                                   { "topic1.*",                     "Product 0" },
                                                   { "topic2.sub-topic1.sub-topic1", "Product 5" },
                                                   { "topic1.sub-topic1",            "Product 7" },
                                                   { "topic1.sub-topic1.sub-topic2", "Product 3" },
                                                   { "topic2.*",                     "Product 9" },
                                                   { "topic1",                       "Product 4" },
                                                   { "topic.*",                      "Product 2" },
                                                   { "Topic.*",                      "Product 1" },
                                                   { "*.*",                          "Product 6" },
                                               };

struct LookupTable test_vector_services[] = {
                                                { "services.event.*",        "Product 8" },
                                                { "services.property.*",     "Product 0" },
                                                { "services.event.my_event", "Product 5" },
                                                { "services.event1",         "Product 7" },
                                                { "services.event2",         "Product 3" },
                                                { "services.event.event1",   "Product 9" },
                                                { "services.*",              "Product 4" },
                                                { "services.event.*",        "Product 2" },
                                                { "services.scheduler",      "Product 1" },
                                                { "*.*",                     "Product 6" },
                                            };

/******************************************************************************
Callback functions and support functions.
******************************************************************************/
static void wait_until_done()
{
    uv_mutex_lock(&mux);
    while (done)
    {
        uv_cond_wait(&cond,&mux);
    }

    uv_mutex_unlock(&mux);
}

static void set_done()
{
    uv_mutex_lock(&mux);
    done--;
    uv_cond_signal(&cond);
    uv_mutex_unlock(&mux);
}

static ma_error_t server_cb(ma_msgbus_server_t *server, ma_message_t *request_message, void *cb_data, ma_msgbus_client_request_t *c_request);

// 1) Creating and starting message bus for the service manager client.
// 2) Creating a dummy service manager service to respond to requests from service manager client.
// 3) Create service manager client.
// 4) Set logger for service manager client.
TEST_SETUP(ma_service_mgr_client_tests)
{
    uv_mutex_init(&mux);
    uv_cond_init(&cond);

    ma_msgbus_create(UT_PRODUCT_ID, &service_mgr_msgbus_obj);
    ma_msgbus_start(service_mgr_msgbus_obj);

    ma_msgbus_server_create(service_mgr_msgbus_obj, "ma.msgbus.services_manager_service.service", &server);
    ma_msgbus_server_start(server, server_cb, NULL);

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_service_manager_create(NULL, &service_manager_obj));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_service_manager_create(service_mgr_msgbus_obj, NULL));
    TEST_ASSERT(MA_OK == ma_service_manager_create(service_mgr_msgbus_obj, &service_manager_obj));

    TEST_ASSERT(MA_OK == ma_console_logger_create(&console_logger));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_service_manager_set_logger(NULL, (ma_logger_t *)console_logger));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_service_manager_set_logger(service_manager_obj, NULL));
    TEST_ASSERT(MA_OK == ma_service_manager_set_logger(service_manager_obj, (ma_logger_t *)console_logger));
}

// 1) Release service manager client instance.
// 2) Stopping and releasing dummy service manager service.
// 3) Stopping and releasing message-bus for the service manager client
TEST_TEAR_DOWN(ma_service_mgr_client_tests)
{
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_service_manager_release(NULL));
    TEST_ASSERT(MA_OK == ma_service_manager_release(service_manager_obj));

    TEST_ASSERT(MA_OK == ma_logger_release((ma_logger_t *)console_logger));

    ma_msgbus_server_stop(server);
    ma_msgbus_server_release(server);

    ma_msgbus_stop(service_mgr_msgbus_obj, MA_TRUE);
    ma_msgbus_release(service_mgr_msgbus_obj);

    uv_cond_destroy(&cond);
    uv_mutex_destroy(&mux);
}

/*
Call back function to be executed when the Service Manager service receives a
request for subscriber list from the service manager client.

This dummy service manager service callback function is designed to perform
following tasks -

TEST SCENARIO - 1 (Subscriber List Verification)
1) Verify the Request Type and the Topic filter values are correctly sent to
   the service.
2) Irrespective of the topic filter, this function sends a list of 10 known
   subscribers to the client.

TEST SCENARIO - 2 (Services List Verification)
1) Verify the Request Type and the Service Name filter values are correctly
   sent to the service.
2) Irrespective of the service name filter, this function sends a list of 10
   known services to the client.
*/
static ma_error_t server_cb(ma_msgbus_server_t *server, ma_message_t *payload, void *cb_data, ma_msgbus_client_request_t *c_request)
{
    ma_variant_t *response_payload = NULL;
    ma_message_t *response_message = NULL;

    // Verifying that the "request type" property sent in the payload is correct

    if(test_scenario == 1)
    {
        const char *request_type = NULL;

        TEST_ASSERT(MA_OK == ma_message_get_property(payload, "prop.key.service_manager_request_type", &request_type));
        TEST_ASSERT(0 == strcmp("prop.value.get_subscribers_list_request", request_type));
    }
    else if(test_scenario == 2)
    {
        const char *request_type = NULL;

        TEST_ASSERT(MA_OK == ma_message_get_property(payload, "prop.key.service_manager_request_type", &request_type));
        TEST_ASSERT(0 == strcmp("prop.value.get_services_list_request", request_type));
    }
    else
    {
        TEST_FAIL();
    }


    // Verifying that the "service name filter" or "topic filter" property sent in the payload is correct
    if(test_scenario == 1)
    {
        const char *topic_filter = NULL;

        TEST_ASSERT(MA_OK == ma_message_get_property(payload, "prop.key.subscriber_topic_filter", &topic_filter));
        TEST_ASSERT(0 == strcmp(DUMMY_TOPIC_FILTER, topic_filter));
    }
    else if(test_scenario == 2)
    {
        const char *service_name_filter = NULL;

        TEST_ASSERT(MA_OK == ma_message_get_property(payload, "prop.key.service_name_filter", &service_name_filter));
        TEST_ASSERT(0 == strcmp(DUMMY_SERVICE_FILTER, service_name_filter));
    }
    else
    {
        TEST_FAIL();
    }

    // Creating the subscriber list to be sent in the response
    if(test_scenario == 1)
    {
        ma_array_t *sub_list = NULL;

        ma_uint8_t counter = 0;

        TEST_ASSERT(MA_OK == ma_array_create(&sub_list));

        for(counter = 0; counter < sizeof(test_vector_subscribers)/sizeof(test_vector_subscribers[0]); counter++)
        {
            ma_table_t *table_ptr = NULL;
            ma_variant_t *table_variant = NULL;

            ma_variant_t *variant_ptr = NULL;

            TEST_ASSERT(MA_OK == ma_table_create(&table_ptr));

            // Adding "Topic Name" into the table
            TEST_ASSERT(MA_OK == ma_variant_create_from_string(test_vector_subscribers[counter].topic_or_service_name,
                                                               strlen(test_vector_subscribers[counter].topic_or_service_name),
                                                               &variant_ptr));
            TEST_ASSERT(MA_OK == ma_table_add_entry(table_ptr, "subscriber.topic_name", variant_ptr));
            TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));
            variant_ptr = NULL;

            // Adding "Product ID" into the table
            TEST_ASSERT(MA_OK == ma_variant_create_from_string(test_vector_subscribers[counter].product_id,
                                                               strlen(test_vector_subscribers[counter].product_id),
                                                               &variant_ptr));
            TEST_ASSERT(MA_OK == ma_table_add_entry(table_ptr, "subscriber.product_id", variant_ptr));
            TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));
            variant_ptr = NULL;

            // Converting table to variant and putting it in subscriber list array
            TEST_ASSERT(MA_OK == ma_variant_create_from_table(table_ptr, &table_variant));
            TEST_ASSERT(MA_OK == ma_table_release(table_ptr));
            table_ptr = NULL;

            TEST_ASSERT(MA_OK == ma_array_push(sub_list, table_variant));
            TEST_ASSERT(MA_OK == ma_variant_release(table_variant));
            table_variant = NULL;
        }

        // Converting array to variant
        TEST_ASSERT(MA_OK == ma_variant_create_from_array(sub_list, &response_payload));
        TEST_ASSERT(MA_OK == ma_array_release(sub_list));
    }

    // Creating the service list to be sent in the response
    if(test_scenario == 2)
    {
        ma_array_t *service_list = NULL;

        ma_uint8_t counter = 0;

        TEST_ASSERT(MA_OK == ma_array_create(&service_list));

        for(counter = 0; counter < sizeof(test_vector_services)/sizeof(test_vector_services[0]); counter++)
        {
            ma_table_t *table_ptr = NULL;
            ma_variant_t *table_variant = NULL;

            ma_variant_t *variant_ptr = NULL;

            TEST_ASSERT(MA_OK == ma_table_create(&table_ptr));

            // Adding "Topic Name" into the table
            TEST_ASSERT(MA_OK == ma_variant_create_from_string(test_vector_services[counter].topic_or_service_name,
                                                               strlen(test_vector_services[counter].topic_or_service_name),
                                                               &variant_ptr));
            TEST_ASSERT(MA_OK == ma_table_add_entry(table_ptr, "service.name", variant_ptr));
            TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));
            variant_ptr = NULL;

            // Adding "Product ID" into the table
            TEST_ASSERT(MA_OK == ma_variant_create_from_string(test_vector_services[counter].product_id,
                                                               strlen(test_vector_services[counter].product_id),
                                                               &variant_ptr));
            TEST_ASSERT(MA_OK == ma_table_add_entry(table_ptr, "service.product_id", variant_ptr));
            TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));
            variant_ptr = NULL;

            // Converting table to variant and putting it in subscriber list array
            TEST_ASSERT(MA_OK == ma_variant_create_from_table(table_ptr, &table_variant));
            TEST_ASSERT(MA_OK == ma_table_release(table_ptr));
            table_ptr = NULL;

            TEST_ASSERT(MA_OK == ma_array_push(service_list, table_variant));
            TEST_ASSERT(MA_OK == ma_variant_release(table_variant));
            table_variant = NULL;
        }

        // Converting array to variant
        TEST_ASSERT(MA_OK == ma_variant_create_from_array(service_list, &response_payload));
        TEST_ASSERT(MA_OK == ma_array_release(service_list));
    }

    // Creating and posting MA_OK response message (subscriber list in payload).
    TEST_ASSERT(MA_OK == ma_message_create(&response_message));
    TEST_ASSERT(MA_OK == ma_message_set_payload(response_message, response_payload));
    TEST_ASSERT(MA_OK == ma_msgbus_server_client_request_post_result(c_request, MA_OK, response_message));
    TEST_ASSERT(MA_OK == ma_variant_release(response_payload));
    TEST_ASSERT(MA_OK == ma_message_release(response_message));
    response_payload = NULL;
    response_message = NULL;

    return MA_OK;
}

/*
Callback functions to be executed for any change in subscription.
*/
static ma_error_t on_subscribe_cb_1(ma_service_manager_t *service_manager, ma_bool_t status, const char *topic, const char *product_id, void *cb_data)
{
    topic_registered_1 = strdup(topic);
    product_id_1 = strdup(product_id);
    notification_type_registered_1 = status;
    set_done();
    return MA_OK;
}

static ma_error_t on_subscribe_cb_2(ma_service_manager_t *service_manager, ma_bool_t status, const char *topic, const char *product_id, void *cb_data)
{
    topic_registered_2 = strdup(topic);
    product_id_2 = (product_id) ? strdup(product_id) : NULL;
    notification_type_registered_2 = status;
    set_done();
    return MA_OK;
}

static ma_error_t on_subscribe_cb_3(ma_service_manager_t *service_manager, ma_bool_t status, const char *topic, const char *product_id, void *cb_data)
{
    topic_registered_3 = strdup(topic);
    product_id_3 = strdup(product_id) ? strdup(product_id) : NULL;
    notification_type_registered_3 = status;

    set_done();
    return MA_OK;
}


/*
Callback functions to be when callback is received with the subscriber list (async function).
*/
ma_error_t get_subscriber_callback(ma_error_t status, ma_service_manager_t *service_manager, const char *topic_filter, const ma_service_manager_subscriber_list_t *subscribers_list, size_t count, void *cb_data)
{
    ma_uint8_t counter = 0;

    TEST_ASSERT(sizeof(test_vector_subscribers)/sizeof(test_vector_subscribers[0]) == count);

    // Verifying that the topics is retrieved correctly from the subscriber list
    for(counter = 0; counter < sizeof(test_vector_subscribers)/sizeof(test_vector_subscribers[0]); counter++)
    {
        const char *topic = NULL;
        const char *product_id = NULL;

        TEST_ASSERT(MA_OK == ma_service_manager_subscriber_list_get_topic_name((ma_service_manager_subscriber_list_t *)subscribers_list, counter, &topic));
        TEST_ASSERT(0 == strcmp(test_vector_subscribers[counter].topic_or_service_name, topic));

        TEST_ASSERT(MA_OK == ma_service_manager_subscriber_list_get_product_id((ma_service_manager_subscriber_list_t *)subscribers_list, counter, &product_id));
        TEST_ASSERT(0 == strcmp(test_vector_subscribers[counter].product_id, product_id));
    }

    set_done();

    return MA_OK;
}

/******************************************************************************
Subscriber notification test for single topic
******************************************************************************/
TEST(ma_service_mgr_client_tests, subscriber_notification_test_single_topic)
{
    // Subscriber Notification Registration (invalid and valid scenarios)
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_service_manager_register_subscriber_notification(NULL, "Topic_1", on_subscribe_cb_1, NULL));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_service_manager_register_subscriber_notification(service_manager_obj, NULL, on_subscribe_cb_1, NULL));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_service_manager_register_subscriber_notification(service_manager_obj, "Topic_1", NULL, NULL));

    TEST_ASSERT(MA_OK == ma_service_manager_register_subscriber_notification(service_manager_obj, "topic1.*", on_subscribe_cb_1, NULL));

    {
        ma_message_t *sub_message = NULL;

        // Reset
        topic_registered_1 = NULL, product_id_1 = NULL;
        notification_type_registered_1 = MA_FALSE;
        topic_registered_2 = NULL, product_id_2 = NULL;
        notification_type_registered_2 = MA_FALSE;
        topic_registered_3 = NULL, product_id_3 = NULL;
        notification_type_registered_3 = MA_FALSE;

        done = 1;

		TEST_ASSERT(MA_OK == ma_message_create(&sub_message));
        TEST_ASSERT(MA_OK == ma_message_set_property(sub_message, "prop.key.subscriber_change_notification", "prop.value.notification_subscriber_registered"));
        TEST_ASSERT(MA_OK == ma_message_set_property(sub_message, "prop.key.subscriber_topic_name", "topic1.event"));
        TEST_ASSERT(MA_OK == ma_message_set_property(sub_message, "prop.key.subscriber_product_id", "product1"));
        TEST_ASSERT(MA_OK == ma_msgbus_publish(service_mgr_msgbus_obj, "ma.msgbus.subscriber_change_notification", MSGBUS_CONSUMER_REACH_INPROC, sub_message));

        // Wait for callback to complete
        wait_until_done();

        // Verify the information is correctly communicated to correct notification callback function. Other notification callbacks are not triggered.
        TEST_ASSERT(0 == strcmp("topic1.event", topic_registered_1));
        TEST_ASSERT(0 == strcmp("product1", product_id_1));
        TEST_ASSERT(MA_TRUE == notification_type_registered_1); // Topic registered
        TEST_ASSERT_NULL(topic_registered_2);
        TEST_ASSERT_NULL(topic_registered_3);

        TEST_ASSERT(MA_OK == ma_message_release(sub_message));
    }

    {
        ma_message_t *sub_message = NULL;

        // Reset
        topic_registered_1 = NULL, product_id_1 = NULL;
        notification_type_registered_1 = MA_FALSE;
        topic_registered_2 = NULL, product_id_2 = NULL;
        notification_type_registered_2 = MA_FALSE;
        topic_registered_3 = NULL, product_id_3 = NULL;
        notification_type_registered_3 = MA_FALSE;

        done = 1;

        TEST_ASSERT(MA_OK == ma_message_create(&sub_message));
        TEST_ASSERT(MA_OK == ma_message_set_property(sub_message, "prop.key.subscriber_change_notification", "prop.value.notification_subscriber_registered"));
        TEST_ASSERT(MA_OK == ma_message_set_property(sub_message, "prop.key.subscriber_topic_name", "topic1.event.my_event"));
        TEST_ASSERT(MA_OK == ma_message_set_property(sub_message, "prop.key.subscriber_product_id", "product2"));
        TEST_ASSERT(MA_OK == ma_msgbus_publish(service_mgr_msgbus_obj, "ma.msgbus.subscriber_change_notification", MSGBUS_CONSUMER_REACH_INPROC, sub_message));

        // Wait for callback to complete
        wait_until_done();

        // Verify the information is correctly communicated to correct notification callback function. Other notification callbacks are not triggered.
        TEST_ASSERT(0 == strcmp("topic1.event.my_event", topic_registered_1));
        TEST_ASSERT(0 == strcmp("product2", product_id_1));
        TEST_ASSERT(MA_TRUE == notification_type_registered_1); // Topic registered
        TEST_ASSERT_NULL(topic_registered_2);
        TEST_ASSERT_NULL(topic_registered_3);

        TEST_ASSERT(MA_OK == ma_message_release(sub_message));
    }

    {
        ma_message_t *sub_message = NULL;

        // Reset
        topic_registered_1 = NULL, product_id_1 = NULL;
        notification_type_registered_1 = MA_FALSE;
        topic_registered_2 = NULL, product_id_2 = NULL;
        notification_type_registered_2 = MA_FALSE;
        topic_registered_3 = NULL, product_id_3 = NULL;
        notification_type_registered_3 = MA_FALSE;

        done = 1;

        TEST_ASSERT(MA_OK == ma_message_create(&sub_message));
        TEST_ASSERT(MA_OK == ma_message_set_property(sub_message, "prop.key.subscriber_change_notification", "prop.value.notification_subscriber_unregistered"));
        TEST_ASSERT(MA_OK == ma_message_set_property(sub_message, "prop.key.subscriber_topic_name", "topic1.event.my_event"));
        TEST_ASSERT(MA_OK == ma_message_set_property(sub_message, "prop.key.subscriber_product_id", "product3"));
        TEST_ASSERT(MA_OK == ma_msgbus_publish(service_mgr_msgbus_obj, "ma.msgbus.subscriber_change_notification", MSGBUS_CONSUMER_REACH_INPROC, sub_message));

        // Wait for callback to complete
        wait_until_done();

        // Verify the information is correctly communicated to correct notification callback function. Other notification callbacks are not triggered.
        TEST_ASSERT(0 == strcmp("topic1.event.my_event", topic_registered_1));
        TEST_ASSERT(0 == strcmp("product3", product_id_1));
        TEST_ASSERT(MA_FALSE == notification_type_registered_1); // Topic unregistered
        TEST_ASSERT_NULL(topic_registered_2);
        TEST_ASSERT_NULL(topic_registered_3);

        TEST_ASSERT(MA_OK == ma_message_release(sub_message));
    }

    {
        ma_message_t *sub_message = NULL;

        // Resetting to NULL
        topic_registered_1 = NULL, product_id_1 = NULL;
        notification_type_registered_1 = MA_FALSE;
        topic_registered_2 = NULL, product_id_2 = NULL;
        notification_type_registered_2 = MA_FALSE;
        topic_registered_3 = NULL, product_id_3 = NULL;
        notification_type_registered_3 = MA_FALSE;

        done = 1;

        TEST_ASSERT(MA_OK == ma_message_create(&sub_message));
        TEST_ASSERT(MA_OK == ma_message_set_property(sub_message, "prop.key.subscriber_change_notification", "prop.value.notification_subscriber_registered"));
        TEST_ASSERT(MA_OK == ma_message_set_property(sub_message, "prop.key.subscriber_topic_name", "topic1.event.my_event.my_event_2"));
        TEST_ASSERT(MA_OK == ma_message_set_property(sub_message, "prop.key.subscriber_product_id", "product4"));
        TEST_ASSERT(MA_OK == ma_msgbus_publish(service_mgr_msgbus_obj, "ma.msgbus.subscriber_change_notification", MSGBUS_CONSUMER_REACH_INPROC, sub_message));

        // Wait for callback to complete
        wait_until_done();

        // Verify the information is correctly communicated to correct notification callback function. Other notification callbacks are not triggered.
        TEST_ASSERT(0 == strcmp("topic1.event.my_event.my_event_2", topic_registered_1));
        TEST_ASSERT(0 == strcmp("product4", product_id_1));
        TEST_ASSERT(MA_TRUE == notification_type_registered_1); // Topic registered
        TEST_ASSERT_NULL(topic_registered_2);
        TEST_ASSERT_NULL(topic_registered_3);

        TEST_ASSERT(MA_OK == ma_message_release(sub_message));
    }

    {
        ma_message_t *sub_message = NULL;

        // Resetting to NULL
        topic_registered_1 = NULL;
        notification_type_registered_1 = MA_FALSE;
        topic_registered_2 = NULL;
        notification_type_registered_2 = MA_FALSE;
        topic_registered_3 = NULL;
        notification_type_registered_3 = MA_FALSE;

        TEST_ASSERT(MA_OK == ma_message_create(&sub_message));
        TEST_ASSERT(MA_OK == ma_message_set_property(sub_message, "prop.key.subscriber_change_notification", "prop.value.notification_subscriber_registered"));
        TEST_ASSERT(MA_OK == ma_message_set_property(sub_message, "prop.key.subscriber_topic_name", "topic2"));
        TEST_ASSERT(MA_OK == ma_message_set_property(sub_message, "prop.key.subscriber_product_id", "product5"));
        TEST_ASSERT(MA_OK == ma_msgbus_publish(service_mgr_msgbus_obj, "ma.msgbus.subscriber_change_notification", MSGBUS_CONSUMER_REACH_INPROC, sub_message));

        // Do not wait for notification callback. It will not happen because topic is not registered for notification.
        TEST_ASSERT_NULL(topic_registered_1);
        TEST_ASSERT_NULL(topic_registered_2);
        TEST_ASSERT_NULL(topic_registered_3);

        TEST_ASSERT(MA_OK == ma_message_release(sub_message));
    }

    {
        ma_message_t *sub_message = NULL;

        // Resetting to NULL
        topic_registered_1 = NULL;
        notification_type_registered_1 = MA_FALSE;
        topic_registered_2 = NULL;
        notification_type_registered_2 = MA_FALSE;
        topic_registered_3 = NULL;
        notification_type_registered_3 = MA_FALSE;

        TEST_ASSERT(MA_OK == ma_message_create(&sub_message));
        TEST_ASSERT(MA_OK == ma_message_set_property(sub_message, "prop.key.subscriber_change_notification", "prop.value.notification_subscriber_registered"));
        TEST_ASSERT(MA_OK == ma_message_set_property(sub_message, "prop.key.subscriber_topic_name", "topic1"));
        TEST_ASSERT(MA_OK == ma_msgbus_publish(service_mgr_msgbus_obj, "ma.msgbus.subscriber_change_notification", MSGBUS_CONSUMER_REACH_INPROC, sub_message));

        // Do not wait for notification callback. It will not happen because sub-topics ("topic1.*") are registered for notification but not the main topic ("topic1").
        TEST_ASSERT_NULL(topic_registered_1);
        TEST_ASSERT_NULL(topic_registered_2);
        TEST_ASSERT_NULL(topic_registered_3);

        TEST_ASSERT(MA_OK == ma_message_release(sub_message));
    }

    // Subscriber notification un-register
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_service_manager_unregister_subscriber_notification(NULL, "topic1.*"));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_service_manager_unregister_subscriber_notification(service_manager_obj, NULL));
    TEST_ASSERT(MA_OK == ma_service_manager_unregister_subscriber_notification(service_manager_obj, "topic1.*"));

    // Again publishing notification. The callback should not be triggered because the subscriber is unregistered.
    {
        ma_message_t *sub_message = NULL;

        // Reset
        topic_registered_1 = NULL, product_id_1 = NULL;
        notification_type_registered_1 = MA_FALSE;
        topic_registered_2 = NULL, product_id_2 = NULL;
        notification_type_registered_2 = MA_FALSE;
        topic_registered_3 = NULL, product_id_3 = NULL;
        notification_type_registered_3 = MA_FALSE;

        TEST_ASSERT(MA_OK == ma_message_create(&sub_message));
        TEST_ASSERT(MA_OK == ma_message_set_property(sub_message, "prop.key.subscriber_change_notification", "prop.value.notification_subscriber_unregistered"));
        TEST_ASSERT(MA_OK == ma_message_set_property(sub_message, "prop.key.subscriber_topic_name", "topic1.event.my_event"));
        TEST_ASSERT(MA_OK == ma_message_set_property(sub_message, "prop.key.subscriber_product_id", "product3"));
        TEST_ASSERT(MA_OK == ma_msgbus_publish(service_mgr_msgbus_obj, "ma.msgbus.subscriber_change_notification", MSGBUS_CONSUMER_REACH_INPROC, sub_message));

        // Do not wait for notification callback. It will not happen because subscriber notification is unregistered.
        TEST_ASSERT_NULL(topic_registered_1);
        TEST_ASSERT_NULL(topic_registered_2);
        TEST_ASSERT_NULL(topic_registered_3);

        TEST_ASSERT(MA_OK == ma_message_release(sub_message));
    }
}


/******************************************************************************
Subscriber notification test for multiple topics
******************************************************************************/
TEST(ma_service_mgr_client_tests, subscriber_notification_test_multiple_topics)
{
    // Subscriber Notification Registration
    TEST_ASSERT(MA_OK == ma_service_manager_register_subscriber_notification(service_manager_obj, "service.property.*", on_subscribe_cb_1, NULL));
    TEST_ASSERT(MA_OK == ma_service_manager_register_subscriber_notification(service_manager_obj, "service.*", on_subscribe_cb_2, NULL));
    TEST_ASSERT(MA_OK == ma_service_manager_register_subscriber_notification(service_manager_obj, "service.event.*", on_subscribe_cb_3, NULL));

    // Topic - "service.property". Notification for "service.*" should be triggered.
    {
        ma_message_t *sub_message = NULL;

        topic_registered_1 = NULL, product_id_1 = NULL;
        notification_type_registered_1 = MA_FALSE;
        topic_registered_2 = NULL, product_id_2 = NULL;
        notification_type_registered_2 = MA_FALSE;
        topic_registered_3 = NULL, product_id_3 = NULL;
        notification_type_registered_3 = MA_FALSE;

        done = 1;

        TEST_ASSERT(MA_OK == ma_message_create(&sub_message));
        TEST_ASSERT(MA_OK == ma_message_set_property(sub_message, "prop.key.subscriber_change_notification", "prop.value.notification_subscriber_registered"));
        TEST_ASSERT(MA_OK == ma_message_set_property(sub_message, "prop.key.subscriber_topic_name", "service.property"));
        TEST_ASSERT(MA_OK == ma_message_set_property(sub_message, "prop.key.subscriber_product_id", "product1"));
        TEST_ASSERT(MA_OK == ma_msgbus_publish(service_mgr_msgbus_obj, "ma.msgbus.subscriber_change_notification", MSGBUS_CONSUMER_REACH_INPROC, sub_message));

        // Wait for callback to complete
        wait_until_done();

        // Verify that only the intended callbacks are triggered.
        TEST_ASSERT_NULL(topic_registered_1);
        TEST_ASSERT(0 == strcmp("service.property", topic_registered_2));
        TEST_ASSERT(0 == strcmp("product1", product_id_2));
        TEST_ASSERT(MA_TRUE == notification_type_registered_2); // Topic registered
        TEST_ASSERT_NULL(topic_registered_3);
        TEST_ASSERT(MA_OK == ma_message_release(sub_message));
    }

    // Topic - "service.policy". Notification for "service.*" should be triggered.
    {
        ma_message_t *sub_message = NULL;

        topic_registered_1 = NULL;
        notification_type_registered_1 = MA_FALSE;
        topic_registered_2 = NULL;
        notification_type_registered_2 = MA_FALSE;
        topic_registered_3 = NULL;
        notification_type_registered_3 = MA_FALSE;

        done = 1;

        TEST_ASSERT(MA_OK == ma_message_create(&sub_message));
        TEST_ASSERT(MA_OK == ma_message_set_property(sub_message, "prop.key.subscriber_change_notification", "prop.value.notification_subscriber_registered"));
        TEST_ASSERT(MA_OK == ma_message_set_property(sub_message, "prop.key.subscriber_topic_name", "service.policy"));
        TEST_ASSERT(MA_OK == ma_message_set_property(sub_message, "prop.key.subscriber_product_id", "product2"));
        TEST_ASSERT(MA_OK == ma_msgbus_publish(service_mgr_msgbus_obj, "ma.msgbus.subscriber_change_notification", MSGBUS_CONSUMER_REACH_INPROC, sub_message));

        // Wait for callback to complete
        wait_until_done();

        // Verify that only the intended callbacks are triggered.
        TEST_ASSERT_NULL(topic_registered_1);
        TEST_ASSERT(0 == strcmp("service.policy", topic_registered_2));
        TEST_ASSERT(0 == strcmp("product2", product_id_2));
        TEST_ASSERT(MA_TRUE == notification_type_registered_2); // Topic registered
        TEST_ASSERT_NULL(topic_registered_3);
        TEST_ASSERT(MA_OK == ma_message_release(sub_message));
    }

    // Topic - "service.property.get_property". Notification for "service.*" and "service.property.*" should be triggered.
    {
        ma_message_t *sub_message = NULL;

        topic_registered_1 = NULL;
        notification_type_registered_1 = MA_FALSE;
        topic_registered_2 = NULL;
        notification_type_registered_2 = MA_FALSE;
        topic_registered_3 = NULL;
        notification_type_registered_3 = MA_FALSE;

        done = 2;

        TEST_ASSERT(MA_OK == ma_message_create(&sub_message));
        TEST_ASSERT(MA_OK == ma_message_set_property(sub_message, "prop.key.subscriber_change_notification", "prop.value.notification_subscriber_registered"));
        TEST_ASSERT(MA_OK == ma_message_set_property(sub_message, "prop.key.subscriber_topic_name", "service.property.get_property"));
        TEST_ASSERT(MA_OK == ma_message_set_property(sub_message, "prop.key.subscriber_product_id", "product3"));
        TEST_ASSERT(MA_OK == ma_msgbus_publish(service_mgr_msgbus_obj, "ma.msgbus.subscriber_change_notification", MSGBUS_CONSUMER_REACH_INPROC, sub_message));

        // Wait for 2 callbacks to complete
        wait_until_done();

        // Verify that only the intended callbacks are triggered.
        TEST_ASSERT(0 == strcmp("service.property.get_property", topic_registered_1));
        TEST_ASSERT(0 == strcmp("product3", product_id_1));
        TEST_ASSERT(MA_TRUE == notification_type_registered_1); // Topic registered
        TEST_ASSERT(0 == strcmp("service.property.get_property", topic_registered_2));
        TEST_ASSERT(0 == strcmp("product3", product_id_2));
        TEST_ASSERT(MA_TRUE == notification_type_registered_2); // Topic registered
        TEST_ASSERT_NULL(topic_registered_3);
        TEST_ASSERT(MA_OK == ma_message_release(sub_message));
    }

    // Topic - "service.event.push_events". Notification for "service.*" and "service.event.*" should be triggered.
    {
        ma_message_t *sub_message = NULL;

        topic_registered_1 = NULL;
        notification_type_registered_1 = MA_TRUE;
        topic_registered_2 = NULL;
        notification_type_registered_2 = MA_TRUE;
        topic_registered_3 = NULL;
        notification_type_registered_3 = MA_TRUE;

        done = 2;

        TEST_ASSERT(MA_OK == ma_message_create(&sub_message));
        TEST_ASSERT(MA_OK == ma_message_set_property(sub_message, "prop.key.subscriber_change_notification", "prop.value.notification_subscriber_unregistered"));
        TEST_ASSERT(MA_OK == ma_message_set_property(sub_message, "prop.key.subscriber_topic_name", "service.event.push_events"));
        TEST_ASSERT(MA_OK == ma_message_set_property(sub_message, "prop.key.subscriber_product_id", "product4"));
        TEST_ASSERT(MA_OK == ma_msgbus_publish(service_mgr_msgbus_obj, "ma.msgbus.subscriber_change_notification", MSGBUS_CONSUMER_REACH_INPROC, sub_message));

        // Wait for 2 callbacks to complete
        wait_until_done();

        // Verify that only the intended callbacks are triggered.
        TEST_ASSERT_NULL(topic_registered_1);
        TEST_ASSERT(0 == strcmp("service.event.push_events", topic_registered_2));
        TEST_ASSERT(0 == strcmp("product4", product_id_2));
        TEST_ASSERT(MA_FALSE == notification_type_registered_2); // Topic unregistered
        TEST_ASSERT(0 == strcmp("service.event.push_events", topic_registered_3));
        TEST_ASSERT(0 == strcmp("product4", product_id_3));
        TEST_ASSERT(MA_FALSE == notification_type_registered_3); // Topic unregistered
        TEST_ASSERT(MA_OK == ma_message_release(sub_message));
    }

    // Topic - "service.property.get_property.my_property.your_property". Notification for "service.*" and "service.property.*" should be triggered.
    {
        ma_message_t *sub_message = NULL;

        topic_registered_1 = NULL;
        notification_type_registered_1 = MA_FALSE;
        topic_registered_2 = NULL;
        notification_type_registered_2 = MA_FALSE;
        topic_registered_3 = NULL;
        notification_type_registered_3 = MA_FALSE;

        done = 2;

        TEST_ASSERT(MA_OK == ma_message_create(&sub_message));
        TEST_ASSERT(MA_OK == ma_message_set_property(sub_message, "prop.key.subscriber_change_notification", "prop.value.notification_subscriber_registered"));
        TEST_ASSERT(MA_OK == ma_message_set_property(sub_message, "prop.key.subscriber_topic_name", "service.property.get_property.my_property.your_property"));
        TEST_ASSERT(MA_OK == ma_message_set_property(sub_message, "prop.key.subscriber_product_id", "product5"));
        TEST_ASSERT(MA_OK == ma_msgbus_publish(service_mgr_msgbus_obj, "ma.msgbus.subscriber_change_notification", MSGBUS_CONSUMER_REACH_INPROC, sub_message));

        // Wait for 2 callbacks to complete
        wait_until_done();

        // Verify that only the intended callbacks are triggered.
        TEST_ASSERT(0 == strcmp("service.property.get_property.my_property.your_property", topic_registered_1));
        TEST_ASSERT(0 == strcmp("product5", product_id_1));
        TEST_ASSERT(MA_TRUE == notification_type_registered_1); // Topic registered
        TEST_ASSERT(0 == strcmp("service.property.get_property.my_property.your_property", topic_registered_2));
        TEST_ASSERT(0 == strcmp("product5", product_id_2));
        TEST_ASSERT(MA_TRUE == notification_type_registered_2); // Topic registered
        TEST_ASSERT_NULL(topic_registered_3);
        TEST_ASSERT(MA_OK == ma_message_release(sub_message));
    }

    // Topic - "services.event". No notification should be triggered.
    {
        ma_message_t *sub_message = NULL;

        topic_registered_1 = NULL;
        notification_type_registered_1 = MA_FALSE;
        topic_registered_2 = NULL;
        notification_type_registered_2 = MA_FALSE;
        topic_registered_3 = NULL;
        notification_type_registered_3 = MA_FALSE;

        TEST_ASSERT(MA_OK == ma_message_create(&sub_message));
        TEST_ASSERT(MA_OK == ma_message_set_property(sub_message, "prop.key.subscriber_change_notification", "prop.value.notification_subscriber_registered"));
        TEST_ASSERT(MA_OK == ma_message_set_property(sub_message, "prop.key.subscriber_topic_name", "services.event"));
        TEST_ASSERT(MA_OK == ma_msgbus_publish(service_mgr_msgbus_obj, "ma.msgbus.subscriber_change_notification", MSGBUS_CONSUMER_REACH_INPROC, sub_message));

        // Do not wait for notification callback. It will not happen because topic is not registered for notification.

        // Verify that only the intended callbacks are triggered.
        TEST_ASSERT_NULL(topic_registered_1);
        TEST_ASSERT_NULL(topic_registered_2);
        TEST_ASSERT_NULL(topic_registered_3);
        TEST_ASSERT(MA_OK == ma_message_release(sub_message));
    }

    // Un-registering notification which was never registered
    TEST_ASSERT(MA_OK == ma_service_manager_unregister_subscriber_notification(service_manager_obj, "service.event.my_event"));

    // Subscriber notification un-register
    TEST_ASSERT(MA_OK == ma_service_manager_unregister_subscriber_notification(service_manager_obj, "service.property.*"));
    TEST_ASSERT(MA_OK == ma_service_manager_unregister_subscriber_notification(service_manager_obj, "service.*"));
    TEST_ASSERT(MA_OK == ma_service_manager_unregister_subscriber_notification(service_manager_obj, "service.event.*"));
}


/******************************************************************************
Test for subscriber list APIs
******************************************************************************/
TEST(ma_service_mgr_client_tests, subscriber_list_test)
{
    ma_uint8_t count = 0;
    ma_service_manager_subscriber_list_t *my_sub_list = NULL;
    size_t sub_count = 100;

    const char *topic = NULL;
    const char *product_id = NULL;

    test_scenario = 1;

    // Getting the subscriber list for DUMMY_TOPIC_FILTER. The dummy service manager service is designed
    // such that it returns all 10 records when the ma_service_manager_get_subscribers API is called.
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_service_manager_get_subscribers(NULL, DUMMY_TOPIC_FILTER, &my_sub_list, &sub_count));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_service_manager_get_subscribers(service_manager_obj, NULL, &my_sub_list, &sub_count));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_service_manager_get_subscribers(service_manager_obj, DUMMY_TOPIC_FILTER, NULL, &sub_count));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_service_manager_get_subscribers(service_manager_obj, DUMMY_TOPIC_FILTER, &my_sub_list, NULL));
    TEST_ASSERT(MA_OK == ma_service_manager_get_subscribers(service_manager_obj, DUMMY_TOPIC_FILTER, &my_sub_list, &sub_count));
    TEST_ASSERT(sizeof(test_vector_subscribers)/sizeof(test_vector_subscribers[0]) == sub_count);

    // Verifying that the topics is retrieved correctly from the subscriber list
    for(count = 0; count < sizeof(test_vector_subscribers)/sizeof(test_vector_subscribers[0]); count++)
    {
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_service_manager_subscriber_list_get_topic_name(NULL, count, &topic));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_service_manager_subscriber_list_get_topic_name(my_sub_list, count, NULL));
        TEST_ASSERT(MA_OK == ma_service_manager_subscriber_list_get_topic_name(my_sub_list, count, &topic));
        TEST_ASSERT(0 == strcmp(test_vector_subscribers[count].topic_or_service_name, topic));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_service_manager_subscriber_list_get_product_id(NULL, count, &product_id));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_service_manager_subscriber_list_get_product_id(my_sub_list, count, NULL));
        TEST_ASSERT(MA_OK == ma_service_manager_subscriber_list_get_product_id(my_sub_list, count, &product_id));
        TEST_ASSERT(0 == strcmp(test_vector_subscribers[count].product_id, product_id));
    }

    // Trying to retrieve the topic name or product ID from location which is more than the count of services registered
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_service_manager_subscriber_list_get_topic_name(my_sub_list, (sub_count + 1), &topic));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_service_manager_subscriber_list_get_product_id(my_sub_list, (sub_count + 1), &product_id));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_service_manager_subscriber_list_release(NULL));
    TEST_ASSERT(MA_OK == ma_service_manager_subscriber_list_release(my_sub_list));
}


/******************************************************************************
Test for subscriber list (Async) APIs
******************************************************************************/
TEST(ma_service_mgr_client_tests, subscriber_list_async_test)
{
    test_scenario = 1;

    done = 1;

    // Getting the subscriber list for DUMMY_TOPIC_FILTER. The dummy service
    // manager service is designed such that it returns all 10 records when
    // the ma_service_manager_get_subscribers API is called.
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_service_manager_get_subscribers_async(NULL, DUMMY_TOPIC_FILTER, get_subscriber_callback, NULL));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_service_manager_get_subscribers_async(service_manager_obj, NULL, get_subscriber_callback, NULL));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_service_manager_get_subscribers_async(service_manager_obj, DUMMY_TOPIC_FILTER, NULL, NULL));
    TEST_ASSERT(MA_OK == ma_service_manager_get_subscribers_async(service_manager_obj, DUMMY_TOPIC_FILTER, get_subscriber_callback, NULL));

    wait_until_done();
}

/******************************************************************************
Test for services list APIs
******************************************************************************/
TEST(ma_service_mgr_client_tests, services_list_test)
{
    ma_uint8_t count = 0;
    ma_service_manager_service_list_t *my_service_list = NULL;
    size_t sub_count = 100;

    const char *service_name = NULL;
    const char *product_id = NULL;

    test_scenario = 2;

    // Getting the subscriber list for DUMMY_SERVICE_FILTER. The dummy service manager service is designed
    // such that it returns all 10 records when the ma_service_manager_get_services API is called.
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_service_manager_get_services(NULL, DUMMY_SERVICE_FILTER, &my_service_list, &sub_count));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_service_manager_get_services(service_manager_obj, NULL, &my_service_list, &sub_count));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_service_manager_get_services(service_manager_obj, DUMMY_SERVICE_FILTER, NULL, &sub_count));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_service_manager_get_services(service_manager_obj, DUMMY_SERVICE_FILTER, &my_service_list, NULL));
    TEST_ASSERT(MA_OK == ma_service_manager_get_services(service_manager_obj, DUMMY_SERVICE_FILTER, &my_service_list, &sub_count));
    TEST_ASSERT(sizeof(test_vector_services)/sizeof(test_vector_services[0]) == sub_count);

    // Verifying that the topics is retrieved correctly from the subscriber list
    for(count = 0; count < sizeof(test_vector_services)/sizeof(test_vector_services[0]); count++)
    {
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_service_manager_service_list_get_service_name(NULL, count, &service_name));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_service_manager_service_list_get_service_name(my_service_list, count, NULL));
        TEST_ASSERT(MA_OK == ma_service_manager_service_list_get_service_name(my_service_list, count, &service_name));
        TEST_ASSERT(0 == strcmp(test_vector_services[count].topic_or_service_name, service_name));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_service_manager_service_list_get_product_id(NULL, count, &product_id));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_service_manager_service_list_get_product_id(my_service_list, count, NULL));
        TEST_ASSERT(MA_OK == ma_service_manager_service_list_get_product_id(my_service_list, count, &product_id));
        TEST_ASSERT(0 == strcmp(test_vector_services[count].product_id, product_id));
    }

    // Trying to retrieve the service name or product ID from location which is more than the count of services registered
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_service_manager_service_list_get_service_name(my_service_list, (sub_count + 1), &service_name));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_service_manager_service_list_get_product_id(my_service_list, (sub_count + 1), &product_id));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_service_manager_service_list_release(NULL));
    TEST_ASSERT(MA_OK == ma_service_manager_service_list_release(my_service_list));
}


