#include "unity.h"
#include "unity_fixture.h"

#include "ma/ma_msgbus.h"
#include "ma/ma_message.h"
#include "ma/ma_log.h"
#include "ma/logger/ma_file_logger.h"

#include <uv.h>

#ifndef MA_WINDOWS
    #include <unistd.h>
#endif

TEST_GROUP_RUNNER(msgbus_client_test_group) {
//    getc(stdin);
    do {RUN_TEST_CASE(msgbus_client_tests, client_async_send)} while (0);
	do {RUN_TEST_CASE(msgbus_client_tests, client_sync_send)} while (0);
	do {RUN_TEST_CASE(msgbus_client_tests, client_oneway_send)} while (0);
  //  getc(stdin);
}

static const char service_name[] = "ma.msgbus.testclient";
static ma_msgbus_server_t *server = NULL;

static ma_msgbus_t *msgbus = NULL;

static const char log_filename[] = "msgbus_client_test";
ma_file_logger_t *file_logger = NULL;

static ma_bool_t done = 0;
static uv_cond_t cond;
static uv_mutex_t mux;

static ma_bool_t signal_from_server = MA_FALSE;

typedef enum client_test_type_e {
	ASYNC_TEST = 0,

	SYNC_TEST,

	ONEWAY_TEST, 

} client_test_type_t;

static void wait_until_done() {

    uv_mutex_lock(&mux);

    while (!done) {uv_cond_wait(&cond,&mux);}

    uv_mutex_unlock(&mux);
}

static void set_done() {
    uv_mutex_lock(&mux);
    done = MA_TRUE;
    uv_cond_signal(&cond);
    uv_mutex_unlock(&mux);
}

static ma_error_t server_cb(ma_msgbus_server_t *server, ma_message_t *request_message, void *cb_data, ma_msgbus_client_request_t *c_request) {

    ma_message_t *result = NULL;

    const char *call_type; 
    TEST_ASSERT_EQUAL(MA_OK, ma_message_get_property(request_message,"call_type",&call_type));

    ma_message_create(&result);
    
    ma_msgbus_server_client_request_post_result(c_request, MA_OK, result);
    ma_message_release(result);

    signal_from_server = MA_TRUE;

    return MA_OK;
}

TEST_SETUP(msgbus_client_tests) {
    uv_mutex_init(&mux), uv_cond_init(&cond);

    ma_msgbus_create("MSGBUS_CLIENT_TESTS", &msgbus);
	ma_msgbus_start(msgbus);

    /*if(MA_OK == ma_file_logger_create(NULL, log_filename, ".log", &file_logger))
        ma_msgbus_set_logger((ma_logger_t*)file_logger);   */    

	ma_msgbus_server_create(msgbus, service_name, &server);

	ma_msgbus_server_start(server, &server_cb, NULL);

    signal_from_server = MA_FALSE;
}

TEST_TEAR_DOWN(msgbus_client_tests) {
	ma_msgbus_server_stop(server);

    ma_msgbus_server_release(server);

	ma_msgbus_stop(msgbus, MA_TRUE);
    ma_msgbus_release(msgbus);

   /* if(file_logger) {
        ma_file_logger_release(file_logger);
        file_logger = NULL;
    }*/

    /* comment below line to see the log file for this tests */
    unlink("msgbus_client_test.log");

    uv_cond_destroy(&cond), uv_mutex_destroy(&mux);
}

static ma_error_t client_cb(ma_error_t status, ma_message_t *response, void *cb_data, ma_msgbus_request_t *request){
	ma_msgbus_endpoint_t *endpoint = NULL;
    //TEST_ASSERT_EQUAL(MA_OK, ma_message_get_property(request_message, "call_type", value));

//    TEST_ASSERT_EQUAL_STRING("async",value);

	if(response) ma_message_release(response);

	if(request) {
		ma_msgbus_request_release(request);
		ma_msgbus_request_get_endpoint(request, &endpoint);
		if(endpoint) ma_msgbus_endpoint_release(endpoint);
	}


    set_done();

    return MA_OK;
}

void perform_client_test(client_test_type_t type) {
    ma_msgbus_endpoint_t *endpoint = NULL;
	ma_msgbus_request_t *msg_req = NULL;
	ma_message_t *request = NULL, *response = NULL;
	ma_error_t rc = MA_OK;

	ma_message_create(&request);
	ma_msgbus_endpoint_create(msgbus, service_name, NULL, &endpoint);

	switch(type) {
	case ASYNC_TEST: {
		ma_message_set_property(request, "call_type","async");
		rc = ma_msgbus_async_send(endpoint, request, &client_cb, NULL, &msg_req);

        wait_until_done();

		break;
        }
	case SYNC_TEST:
		ma_message_set_property(request, "call_type","sync");	
		rc = ma_msgbus_send(endpoint, request, &response);
		ma_message_release(response);
		ma_msgbus_endpoint_release(endpoint);
		break;
	case ONEWAY_TEST:
		ma_message_set_property(request, "call_type","oneway");	
		rc = ma_msgbus_send_and_forget(endpoint, request);
		ma_msgbus_endpoint_release(endpoint);
		break;
	}


    ma_message_release(request);    

    while(!signal_from_server) {
    #ifdef WIN32
        Sleep(1000);
    #else
        sleep(1);
    #endif
    }    
}

TEST(msgbus_client_tests, client_async_send) {
    perform_client_test(ASYNC_TEST);
}

TEST(msgbus_client_tests, client_sync_send) {
    perform_client_test(SYNC_TEST);
}

TEST(msgbus_client_tests, client_oneway_send) {
    perform_client_test(ONEWAY_TEST);
}
