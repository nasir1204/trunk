#include "unity.h"
#include "unity_fixture.h"

#include "ma/internal/core/msgbus/ma_message_router.h"
#include "ma/internal/utils/event_loop/ma_loop_worker.h"

static ma_event_loop_t  *ma_loop;
static uv_loop_t        *uv_loop; 
static ma_loop_worker_t *sut;

static int test_value = 0;

const static int MA_TEST_CHECK_VALUE = 125;

TEST_GROUP_RUNNER(loop_worker_test_group) {
    RUN_TEST_CASE(loop_worker_tests, test_submit_async_work)
    do {RUN_TEST_CASE(loop_worker_tests, test_submit_sync_work);} while (0); /* do..while is a Unity workaround */
}

TEST_SETUP(loop_worker_tests) {
    test_value = 0;
    ma_event_loop_create(&ma_loop);
    uv_loop = ma_event_loop_get_uv_loop(ma_loop);
    ma_loop_worker_create(ma_loop,&sut);
}

TEST_TEAR_DOWN(loop_worker_tests) {
    ma_loop_worker_release(sut);
    uv_loop = NULL;
    ma_event_loop_release(ma_loop);
}


/* this function executes on i/o thread */
static void worker(void *data) {
    TEST_ASSERT_TRUE(ma_event_loop_is_loop_thread(ma_loop));
    test_value = MA_TEST_CHECK_VALUE;
    ma_loop_worker_stop(sut);
}

static void async_thread_fun(uv_work_t *work) {
    ma_loop_worker_submit_work(sut, &worker, NULL, MA_FALSE);
}

/* executed on i/o thread */
static void after_async_thread_fun(uv_work_t *work, int status) {
    
}

static uv_work_t uv_work;

TEST(loop_worker_tests, test_submit_async_work) {

    ma_event_loop_start(ma_loop);
    ma_loop_worker_start(sut);

    if (uv_queue_work(uv_loop, &uv_work, &async_thread_fun, &after_async_thread_fun)) {
        fprintf(stderr, "queue work error %s\n", uv_err_name(uv_last_error(uv_loop)));
    }

    ma_event_loop_run(ma_loop);

    TEST_ASSERT_EQUAL_INT(MA_TEST_CHECK_VALUE,test_value);

}

/* this function executes on i/o thread */
static void sync_worker(void *data) {
    TEST_ASSERT_TRUE(ma_event_loop_is_loop_thread(ma_loop));
    test_value =  * (int *) data;
}


static void sync_thread_fun(uv_work_t *work) {
    int desired_value = 954;

    TEST_ASSERT_NOT_EQUAL(desired_value,test_value);
    ma_loop_worker_submit_work(sut, &sync_worker, &desired_value, MA_TRUE);
    TEST_ASSERT_EQUAL(desired_value,test_value);
}

/* executed on i/o thread */
static void after_sync_thread_fun(uv_work_t *work) {
    ma_loop_worker_stop(sut);
}


TEST(loop_worker_tests, test_submit_sync_work) {

    ma_event_loop_start(ma_loop);
    ma_loop_worker_start(sut);

    if (uv_queue_work(uv_loop, &uv_work, &sync_thread_fun, &after_sync_thread_fun)) {
        fprintf(stderr, "queue work error %s\n", uv_err_name(uv_last_error(uv_loop)));
    }

    ma_event_loop_run(ma_loop);
}

