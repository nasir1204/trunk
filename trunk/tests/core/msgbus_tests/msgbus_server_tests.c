#include "unity.h"
#include "unity_fixture.h"

#include "ma/ma_msgbus.h"

TEST_GROUP_RUNNER(msgbus_server_test_group) {
    RUN_TEST_CASE(msgbus_server_tests, test_server_create)
    //do {RUN_TEST_CASE(msgbus_server_tests, );} while (0); /* do..while is a Unity workaround */
}

TEST_SETUP(msgbus_server_tests) {
    
}

TEST_TEAR_DOWN(msgbus_server_tests) {
   
}

TEST(msgbus_server_tests, test_server_create) {
	ma_msgbus_server_t *server = NULL;
	long priority;

	ma_msgbus_server_create("xyz", &server);

	ma_msgbus_server_setopt(server, MSGBUSOPT_PRIORITY, 1);

	ma_msgbus_server_getinfo(server, MSGBUSOPT_PRIORITY, &priority);
	TEST_ASSERT_EQUAL_INT(1, priority);

	ma_msgbus_server_release(server);
}