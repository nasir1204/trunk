//#define UNITY_FIXTURE_MALLOC_OVERRIDES_H_ 1
#include "unity.h"
#include "unity_fixture.h"

#include "ma/ma_message.h"
#include "ma/internal/core/msgbus/ma_message_header.h"
#include "ma/internal/utils/serialization/ma_serialize.h"
#include "ma/internal/utils/serialization/ma_deserialize.h"

#include "ma/ma_variant.h"
#include <string.h>
#include <stdlib.h>



TEST_GROUP_RUNNER(message_test_group) {
	RUN_TEST_CASE(message_tests, test_ma_message_as_variant)   ;
    do { RUN_TEST_CASE(message_tests, test_message_clone) } while (0);
}

TEST_SETUP(message_tests) {
}

TEST_TEAR_DOWN(message_tests) {
}

void print_headers(ma_message_t *msg){
	//Check header
	printf(" version = %d \n", GET_MESSAGE_VERSION(msg));
	printf(" type = %d \n", GET_MESSAGE_TYPE(msg));
	printf(" correlation id = %d \n", GET_MESSAGE_CORRELATION_ID(msg));
	printf(" time = %u \n", GET_MESSAGE_TIME(msg));
	printf(" status = %u \n", GET_MESSAGE_STATUS(msg));

	printf(" source name = %s, len = %d \n", GET_MESSAGE_SOURCE_NAME(msg), GET_MESSAGE_SOURCE_NAME_LEN(msg)-1);
	printf(" source host = %s, len = %d \n", GET_MESSAGE_SOURCE_HOST_NAME(msg), GET_MESSAGE_SOURCE_HOST_NAME_LEN(msg)-1);
	printf(" source pid = %d \n", GET_MESSAGE_SOURCE_PID(msg));
	printf(" destination name = %s, len = %d \n", GET_MESSAGE_DESTINATION_NAME(msg), GET_MESSAGE_DESTINATION_NAME_LEN(msg)-1);
	printf(" destination host = %s, len = %d \n", GET_MESSAGE_DESTINATION_HOST_NAME(msg), GET_MESSAGE_DESTINATION_HOST_NAME_LEN(msg)-1);
	printf(" destination pid = %d \n", GET_MESSAGE_DESTINATION_PID(msg));
}


void simulate_serialize_and_deserialize(ma_variant_t *serialized_variant , ma_variant_t **deserialized_variant) {
	ma_serializer_t *serializer = NULL ;
	ma_deserializer_t *deserializer = NULL ;
	char *data = NULL ;
	size_t len = 0 ;


	ma_serializer_create(&serializer);
	ma_serializer_add_variant(serializer, serialized_variant) ;
	ma_serializer_get(serializer, &data, &len) ;

	ma_deserializer_create(&deserializer);
	ma_deserializer_put(deserializer, data, len);
	ma_deserializer_get_next(deserializer, deserialized_variant) ;

	ma_serializer_destroy(serializer);
	ma_deserializer_destroy(deserializer);
}
TEST(message_tests, test_ma_message_as_variant) {
	ma_message_t *msg = NULL ;
	ma_variant_t *value_1 = NULL , *value_2 = NULL , *value_3 = NULL ;
	ma_variant_t *msg_variant = NULL ;
	char *data = NULL;

	/* Variable to hold the results */
	ma_bool_t comparison_result = MA_FALSE;

	struct addr {
		char *name;
		long pid;
		char *host;
	};
	struct addr src = { "ma.managebility.policy", 4321, "172.16.220.12" };
	struct addr dest = { "ma.broker", 1111, "172.16.220.100" };

	TEST_ASSERT_MESSAGE(MA_OK  == ma_message_create(&msg), "ma_message_create failed to create the object");

	//Set header
	SET_MESSAGE_VERSION(msg , 5);
	SET_MESSAGE_TYPE(msg , MA_MESSAGE_TYPE_REQUEST);
	SET_MESSAGE_SUB_TYPE(msg , MA_MESSAGE_SUB_TYPE_ONEWAY);

	SET_MESSAGE_CORRELATION_ID(msg, 1234);
	SET_MESSAGE_TIME(msg, 10102010L);
	SET_MESSAGE_STATUS(msg, MA_OK);

	// set source in header
	SET_MESSAGE_SOURCE_NAME(msg, src.name);
	SET_MESSAGE_SOURCE_PID(msg, src.pid);
	SET_MESSAGE_SOURCE_HOST_NAME(msg, src.host);

	// set destination in header
	SET_MESSAGE_DESTINATION_NAME(msg, dest.name);
	SET_MESSAGE_DESTINATION_PID(msg, dest.pid);
	SET_MESSAGE_DESTINATION_HOST_NAME(msg, dest.host);

	//Set property
	ma_message_set_property(msg , "Location" , "Beaverton");

	//Set payload
	TEST_ASSERT_MESSAGE(MA_OK  == ma_variant_create_from_string("This is my test message" , strlen("This is my test message") , &value_1) , "ma_variant_create_from_string failed to create the object");
	ma_message_set_payload(msg , value_1);

	ma_message_as_variant(msg , &msg_variant) ;
	simulate_serialize_and_deserialize(msg_variant , &value_3);
	ma_variant_release(msg_variant); ma_message_release(msg);
	msg = NULL ;

	ma_message_from_variant(value_3 , &msg);
	ma_variant_release(value_3);

	//Check header
	TEST_ASSERT_EQUAL_INT(5 , GET_MESSAGE_VERSION(msg));
	//Enable it when we are ready
	TEST_ASSERT_EQUAL_INT(MA_MESSAGE_TYPE_REQUEST , GET_MESSAGE_TYPE(msg));
	TEST_ASSERT_EQUAL_INT(MA_MESSAGE_SUB_TYPE_ONEWAY , GET_MESSAGE_SUB_TYPE(msg));

	TEST_ASSERT_EQUAL_INT(strlen(src.name) + 1 , GET_MESSAGE_SOURCE_NAME_LEN(msg));
	TEST_ASSERT_EQUAL_INT(strlen(src.host) + 1 , GET_MESSAGE_SOURCE_HOST_NAME_LEN(msg));
	TEST_ASSERT_EQUAL_INT(strlen(dest.name) + 1 , GET_MESSAGE_DESTINATION_NAME_LEN(msg));
	TEST_ASSERT_EQUAL_INT(strlen(dest.host) + 1 , GET_MESSAGE_DESTINATION_HOST_NAME_LEN(msg));

	TEST_ASSERT_EQUAL_INT(1234 , GET_MESSAGE_CORRELATION_ID(msg));
	TEST_ASSERT_EQUAL_INT32(10102010L , GET_MESSAGE_TIME(msg));
	TEST_ASSERT_EQUAL_INT(MA_OK , GET_MESSAGE_STATUS(msg));

	TEST_ASSERT_EQUAL_INT(0 , strcmp( (char*)GET_MESSAGE_SOURCE_NAME(msg) , src.name));
	TEST_ASSERT_EQUAL_INT(0 , strcmp( (char*)GET_MESSAGE_SOURCE_HOST_NAME(msg) , src.host));
	TEST_ASSERT_EQUAL_INT(src.pid , GET_MESSAGE_SOURCE_PID(msg));

	TEST_ASSERT_EQUAL_INT(0 , strcmp( (char*)GET_MESSAGE_DESTINATION_NAME(msg) , dest.name));
	TEST_ASSERT_EQUAL_INT(0 , strcmp( (char*)GET_MESSAGE_DESTINATION_HOST_NAME(msg) , dest.host));
	TEST_ASSERT_EQUAL_INT(dest.pid , GET_MESSAGE_DESTINATION_PID(msg));

	//print_headers(msg);

	//Check property
	ma_message_get_property(msg ,  "Location"  , &data) ;
	TEST_ASSERT_MESSAGE(0 == strcmp("Beaverton" , data) , "Failed to get the property ");

	//Chek payload
	ma_message_get_payload(msg , &value_2);
	TEST_ASSERT_MESSAGE(ma_variant_is_equal(value_1, value_2, &comparison_result) == MA_OK, "payload could not be compared");
	TEST_ASSERT_MESSAGE(comparison_result == MA_TRUE, "payload comparision failed");

	ma_variant_release(value_1); ma_variant_release(value_2);
	ma_message_release(msg);

}


TEST(message_tests, test_message_clone) {
    ma_message_t *msg = NULL, *new_msg = NULL ;
	ma_variant_t *value_1 = NULL , *value_2 = NULL ;
	char *data = NULL;

	/* Variable to hold the results */
	ma_bool_t comparison_result = MA_FALSE;

	struct addr {
		char *name;
		long pid;
		char *host;
	};
	struct addr src = { "ma.managebility.policy", 4321, "172.16.220.12" };
	struct addr dest = { "ma.broker", 1111, "172.16.220.100" };

	TEST_ASSERT_MESSAGE(MA_OK  == ma_message_create(&msg), "ma_message_create failed to create the object");

	//Set header
	SET_MESSAGE_VERSION(msg , 5);
	SET_MESSAGE_TYPE(msg , MA_MESSAGE_TYPE_REQUEST);
	SET_MESSAGE_SUB_TYPE(msg , MA_MESSAGE_SUB_TYPE_ONEWAY);

	SET_MESSAGE_CORRELATION_ID(msg, 1234);
	SET_MESSAGE_TIME(msg, 10102010L);
	SET_MESSAGE_STATUS(msg, MA_OK);

	// set source in header
	SET_MESSAGE_SOURCE_NAME(msg, src.name);
	SET_MESSAGE_SOURCE_PID(msg, src.pid);
	SET_MESSAGE_SOURCE_HOST_NAME(msg, src.host);

	// set destination in header
	SET_MESSAGE_DESTINATION_NAME(msg, dest.name);
	SET_MESSAGE_DESTINATION_PID(msg, dest.pid);
	SET_MESSAGE_DESTINATION_HOST_NAME(msg, dest.host);

	//Set property
	ma_message_set_property(msg , "Location" , "Beaverton");

	//Set payload
	TEST_ASSERT_MESSAGE(MA_OK  == ma_variant_create_from_string("This is my test message" , strlen("This is my test message") , &value_1) , "ma_variant_create_from_string failed to create the object");
	ma_message_set_payload(msg , value_1);

    TEST_ASSERT(MA_OK == ma_message_clone(msg, &new_msg));
	ma_message_release(msg);

	//Check header
	TEST_ASSERT_EQUAL_INT(0 , GET_MESSAGE_VERSION(new_msg));
	//Enable it when we are ready
	TEST_ASSERT_EQUAL_INT(MA_MESSAGE_TYPE_UNSPECIFIED , GET_MESSAGE_TYPE(new_msg));
	TEST_ASSERT_EQUAL_INT(0 , GET_MESSAGE_SUB_TYPE(new_msg));

	TEST_ASSERT_EQUAL_INT(0 , GET_MESSAGE_SOURCE_NAME_LEN(new_msg));
	TEST_ASSERT_EQUAL_INT(0 , GET_MESSAGE_SOURCE_HOST_NAME_LEN(new_msg));
	TEST_ASSERT_EQUAL_INT(0 , GET_MESSAGE_DESTINATION_NAME_LEN(new_msg));
	TEST_ASSERT_EQUAL_INT(0 , GET_MESSAGE_DESTINATION_HOST_NAME_LEN(new_msg));

	TEST_ASSERT_EQUAL_INT(0 , GET_MESSAGE_CORRELATION_ID(new_msg));
	TEST_ASSERT_EQUAL_INT32(0L , GET_MESSAGE_TIME(new_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK , GET_MESSAGE_STATUS(new_msg));

	TEST_ASSERT_EQUAL_INT(1 , GET_MESSAGE_SOURCE_NAME(new_msg) == NULL);
	TEST_ASSERT_EQUAL_INT(1 , GET_MESSAGE_SOURCE_HOST_NAME(new_msg) == NULL);
	TEST_ASSERT_EQUAL_INT(0 , GET_MESSAGE_SOURCE_PID(new_msg));

	TEST_ASSERT_EQUAL_INT(1 , GET_MESSAGE_DESTINATION_NAME(new_msg) == NULL);
	TEST_ASSERT_EQUAL_INT(1 , GET_MESSAGE_DESTINATION_HOST_NAME(new_msg) == NULL);
	TEST_ASSERT_EQUAL_INT(0 , GET_MESSAGE_DESTINATION_PID(new_msg));

	//print_headers(msg);

	//Check property
	ma_message_get_property(new_msg ,  "Location"  , &data) ;
	TEST_ASSERT_MESSAGE(0 == strcmp("Beaverton" , data) , "Failed to get the property ");

	//Chek payload
	ma_message_get_payload(new_msg , &value_2);
	TEST_ASSERT_MESSAGE(ma_variant_is_equal(value_1, value_2, &comparison_result) == MA_OK, "payload could not be compared");
	TEST_ASSERT_MESSAGE(comparison_result == MA_TRUE, "payload comparision failed");

	ma_variant_release(value_1); ma_variant_release(value_2);
	ma_message_release(new_msg);

}

