#include "unity.h"
#include "unity_fixture.h"

#include "ma/ma_common.h"

#ifdef MA_WINDOWS
# include <crtdbg.h> /* */
# define MA_TRACKLEAKS _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF)
#else
# define MA_TRACKLEAKS ((void *)0)
#endif


static void run_all_tests(void) {
    RUN_TEST_GROUP(router_test_group);	
    do {RUN_TEST_GROUP(message_test_group);} while (0); /* do loop to work around unity declaration flaws that are not ANSI C compatible */	    
    do {RUN_TEST_GROUP(named_pipe_test_group);} while (0); /* do loop to work around unity declaration flaws that are not ANSI C compatible */
    do {RUN_TEST_GROUP(loop_worker_test_group);} while (0); /* do loop to work around unity declaration flaws that are not ANSI C compatible */
    do {RUN_TEST_GROUP(msgbus_impl_test_group);} while (0); /* do loop to work around unity declaration flaws that are not ANSI C compatible */
    do {RUN_TEST_GROUP(msgbus_client_test_group);} while (0); /* do loop to work around unity declaration flaws that are not ANSI C compatible */
    do {RUN_TEST_GROUP(msgbus_pub_sub_test_group);} while (0); /* do loop to work around unity declaration flaws that are not ANSI C compatible */	
	// It is hanging. Need to look at this later
    //do {RUN_TEST_GROUP(msgbus_out_proc_test_group);} while (0); /* do loop to work around unity declaration flaws that are not ANSI C compatible */	
} 

int main(int argc, char *argv[]) {
    MA_TRACKLEAKS;
    return UnityMain(argc, argv, run_all_tests);
}


