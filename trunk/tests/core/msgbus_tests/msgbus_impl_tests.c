#include "unity.h"
#include "unity_fixture.h"

#include "ma/ma_msgbus.h"
#include "ma/ma_message.h"
#include "ma/logger/ma_outputdebugstring_logger.h"
#include <uv.h>

static ma_msgbus_t *msgbus = NULL;
static ma_logger_t *logger = 0;

TEST_GROUP_RUNNER(msgbus_impl_test_group) {
    do {RUN_TEST_CASE(msgbus_impl_tests_noinit, init_deinit)} while (0);

    do {RUN_TEST_CASE(msgbus_impl_tests, server_create_destroy)} while (0);
    do {RUN_TEST_CASE(msgbus_impl_tests, server_start_send_stop)} while (0);
}

TEST_SETUP(msgbus_impl_tests_noinit) {

	ma_console_logger_create(&logger);
    //ma_outputdebugstring_logger_create(&logger);

}

TEST_TEAR_DOWN(msgbus_impl_tests_noinit) {

    ma_logger_release(logger), logger = 0;


}

TEST(msgbus_impl_tests_noinit, init_deinit) {
    int cnt = 12;

    TEST_ASSERT_EQUAL(MA_OK, ma_msgbus_create("MSGBUS_TESTS", &msgbus));

    ma_msgbus_set_logger(msgbus, logger);
	
    while (cnt--) {
        TEST_ASSERT_EQUAL(MA_OK, ma_msgbus_start(msgbus));
        TEST_ASSERT_EQUAL(MA_OK, ma_msgbus_stop(msgbus, MA_TRUE));
    };
    
    TEST_ASSERT_EQUAL(MA_OK, ma_msgbus_release(msgbus));
}

/***********************************************/

static const char service_name[] = "ma.msgbus.testservice";

static ma_msgbus_server_t *server;

static ma_bool_t done = 0;
static uv_cond_t cond;
static uv_mutex_t mux;

TEST_SETUP(msgbus_impl_tests) {
    TEST_ASSERT_EQUAL(MA_OK, ma_msgbus_create("MSGBUS_TESTS", &msgbus));
	TEST_ASSERT_EQUAL(MA_OK, ma_msgbus_start(msgbus));
}

TEST_TEAR_DOWN(msgbus_impl_tests) {
	TEST_ASSERT_EQUAL(MA_OK, ma_msgbus_stop(msgbus, MA_TRUE));
    TEST_ASSERT_EQUAL(MA_OK, ma_msgbus_release(msgbus));
}

TEST(msgbus_impl_tests, server_create_destroy) {
    ma_msgbus_server_create(msgbus, service_name, &server);
    ma_msgbus_server_release(server), server=0;
}

static ma_error_t server_cb(ma_msgbus_server_t *server, ma_message_t *request_payload, void *cb_data, ma_msgbus_client_request_t *c_request) {

    ma_message_t *result;
    ma_message_create(&result);
    
    ma_msgbus_server_client_request_post_result(c_request, MA_OK, result);
    ma_message_release(result);

    return MA_OK;
}

void client_thread(void *arg) {

    /* use client api to connect to server */
    ma_msgbus_endpoint_t *endpoint;
    ma_message_t *request = NULL, *response = NULL;

    ma_message_create(&request);
    ma_message_set_property(request, "command","stop");

    ma_msgbus_endpoint_create(msgbus, service_name, NULL, &endpoint);

    ma_msgbus_send(endpoint, request, &response);

    ma_message_release(response);
    ma_message_release(request);


    ma_msgbus_endpoint_release(endpoint);

    uv_mutex_lock(&mux);
    done = MA_TRUE;
    uv_mutex_unlock(&mux);
    uv_cond_signal(&cond);

}

TEST(msgbus_impl_tests, server_start_send_stop) {
    uv_thread_t tid;
    ma_msgbus_server_create(msgbus, service_name, &server);

    ma_msgbus_server_start(server, &server_cb, NULL);

    uv_mutex_init(&mux), uv_cond_init(&cond);

    uv_thread_create(&tid, &client_thread, 0);

    uv_mutex_lock(&mux);
    while (!done) {uv_cond_wait(&cond,&mux);}
    uv_mutex_unlock(&mux);

	ma_msgbus_server_stop(server);

    ma_msgbus_server_release(server), server=0;

    uv_thread_join(&tid);

    uv_cond_destroy(&cond), uv_mutex_destroy(&mux);

}