#include "unity.h"
#include "unity_fixture.h"

#include "uv.h"
#include "ma/ma_msgbus.h"

#ifdef MA_WINDOWS
#define CLIENT_OUT_FILE "C:\\msgbus_out_proc_client_result.out" /* Until OUT_DIR macro is set while building msgbus_out_proc_{client, server}.c */
#define SERVER_OUT_FILE "C:\\msgbus_out_proc_server_result.out" /* Until OUT_DIR macro is set while building msgbus_out_proc_{client, server}.c */
#else
#include <unistd.h>
#include <sys/wait.h>
#include <errno.h>
#define CLIENT_OUT_FILE "/tmp/msgbus_out_proc_client_result.out"
#define SERVER_OUT_FILE "/tmp/msgbus_out_proc_server_result.out"
#endif


const char *BROKER          = OUT_DIR "ma_broker";
const char *OUT_PROC_SERVER = OUT_DIR "msgbus_out_proc_server";
const char *OUT_PROC_CLIENT = OUT_DIR "msgbus_out_proc_client";

TEST_GROUP_RUNNER(msgbus_out_proc_test_group) {
    RUN_TEST_CASE(msgbus_out_proc_tests, run_broker)
        do {RUN_TEST_CASE(msgbus_out_proc_tests, start_client_server);} while (0); /* do..while is a Unity workaround */
        do {RUN_TEST_CASE(msgbus_out_proc_tests, verify_client_log);} while (0); /* do..while is a Unity workaround */
}

static void stop_broker();

TEST_SETUP(msgbus_out_proc_tests) {

}

TEST_TEAR_DOWN(msgbus_out_proc_tests) {

}

TEST(msgbus_out_proc_tests, run_broker) {
#ifdef MA_WINDOWS
    /* install */
    char parameters[128] = " install";

    STARTUPINFO startup_info;
    PROCESS_INFORMATION process_information;

    memset(&startup_info, 0, sizeof(startup_info));
    startup_info.cb = sizeof(startup_info);

    memset(&process_information, 0, sizeof(process_information));

    TEST_ASSERT_NOT_EQUAL(0, CreateProcessA(BROKER, parameters, NULL, NULL, FALSE, 0, NULL, NULL, &startup_info, &process_information));

    WaitForSingleObject(process_information.hProcess, INFINITE);
    CloseHandle(process_information.hProcess);
    CloseHandle(process_information.hThread);

    /* start */
    strcpy(parameters, " start");
    memset(&startup_info, 0, sizeof(startup_info));
    startup_info.cb = sizeof(startup_info);

    memset(&process_information, 0, sizeof(process_information));

    TEST_ASSERT_NOT_EQUAL(0, CreateProcessA(BROKER, parameters, NULL, NULL, FALSE, 0, NULL, NULL, &startup_info, &process_information));

    WaitForSingleObject(process_information.hProcess, INFINITE);
    CloseHandle(process_information.hProcess);
    CloseHandle(process_information.hThread);
#else
    char* argv[3] = {0};
    argv[0] = BROKER;
    argv[1] = "start";
    argv[2] = NULL;
    pid_t child_pid;
    int child_status;

    printf("Starting broker...\n");
    child_pid = fork();
    TEST_ASSERT_NOT_EQUAL(-1, child_pid);

    if(0 == child_pid) {
	printf(" broker exe with full path %s\n", BROKER);
        /* This is done by the child process. */
        int ret = execvp(argv[0], argv);

        /* If execvp returns, it must have failed. */
        printf("Failed to start broker. Error: %d\n", errno);
        TEST_ASSERT_NOT_EQUAL(-1, ret);
    }
    else {
        while (wait(&child_status) != child_pid) {}
    }
    sleep(2);

#endif //MA_WINDOWS
}

static void stop_broker() {
    #ifdef MA_WINDOWS
    /* stop */
    char parameters[128] = " stop";
    STARTUPINFO startup_info;
    PROCESS_INFORMATION process_information;

    memset(&startup_info, 0, sizeof(startup_info));
    startup_info.cb = sizeof(startup_info);

    memset(&process_information, 0, sizeof(process_information));

    TEST_ASSERT_NOT_EQUAL(0, CreateProcessA(BROKER, parameters, NULL, NULL, FALSE, 0, NULL, NULL, &startup_info, &process_information));

    WaitForSingleObject(process_information.hProcess, INFINITE);
    CloseHandle(process_information.hProcess);
    CloseHandle(process_information.hThread);

    /* uninstall */
    strcpy(parameters, " uninstall");
    memset(&startup_info, 0, sizeof(startup_info));
    startup_info.cb = sizeof(startup_info);

    memset(&process_information, 0, sizeof(process_information));

    TEST_ASSERT_NOT_EQUAL(0, CreateProcessA(BROKER, parameters, NULL, NULL, FALSE, 0, NULL, NULL, &startup_info, &process_information));

    WaitForSingleObject(process_information.hProcess, INFINITE);
    CloseHandle(process_information.hProcess);
    CloseHandle(process_information.hThread);
#else
    char* argv[3] = {0};
    argv[0] = BROKER;
    argv[1] = "stop";
    argv[2] = NULL;
    pid_t child_pid;
    int child_status;

    printf("Stopping broker...\n");
    child_pid = fork();
    TEST_ASSERT_NOT_EQUAL(-1, child_pid);

    if(0 == child_pid) {
        /* This is done by the child process. */
        int ret = execvp(argv[0], argv);

        /* If execvp returns, it must have failed. */
        printf("Failed to stop broker. Error: %d\n", errno);
        TEST_ASSERT_NOT_EQUAL(-1, ret);
    }
    else {
        while (wait(&child_status) != child_pid) {}
    }

#endif //MA_WINDOWS
}

TEST(msgbus_out_proc_tests, start_client_server) {
    #ifdef MA_WINDOWS
    typedef enum handle_to_e {
        SERVER = 0,
        CLIENT
    } handle_to_s;

    HANDLE client_server_handles[2] = {0};
    char server_parameters[128] = {0};
    STARTUPINFO server_startup_info;
    PROCESS_INFORMATION server_process_information;

    char client_parameters[128] = {0};
    STARTUPINFO client_startup_info;
    PROCESS_INFORMATION client_process_information;

    /* Start Server */
    memset(&server_startup_info, 0, sizeof(server_startup_info));
    server_startup_info.cb = sizeof(server_startup_info);
    memset(&server_process_information, 0, sizeof(server_process_information));

    TEST_ASSERT_NOT_EQUAL(0, CreateProcessA(OUT_PROC_SERVER, server_parameters, NULL, NULL, FALSE, 0, NULL, NULL, &server_startup_info, &server_process_information));
    client_server_handles[SERVER] = server_process_information.hProcess;
    Sleep(2 *1000); /* Give sometime for the server to start and register with the broker */

    /* Start Client */
    memset(&client_startup_info, 0, sizeof(client_startup_info));
    client_startup_info.cb = sizeof(client_startup_info);
    memset(&client_process_information, 0, sizeof(client_process_information));

    TEST_ASSERT_NOT_EQUAL(0, CreateProcessA(OUT_PROC_CLIENT, client_parameters, NULL, NULL, FALSE, 0, NULL, NULL, &client_startup_info, &client_process_information));
    client_server_handles[CLIENT] = client_process_information.hProcess;

    /* Cleanup */
    WaitForMultipleObjects(2, client_server_handles, TRUE, 90 * 1000); /* Bump the timeout if you want the client to fire requests for more than a minute */

    CloseHandle(server_process_information.hProcess);
    CloseHandle(server_process_information.hThread);

    CloseHandle(client_process_information.hProcess);
    CloseHandle(client_process_information.hThread);

    /* Sometimes the client and server processes are just hung. Forcibly killing them so that the next run will be not be affected */
    TerminateProcess(server_process_information.hProcess, 1);
    WaitForSingleObject(server_process_information.hProcess, 90 * 1000);
    TerminateProcess(client_process_information.hProcess, 1);
    WaitForSingleObject(client_process_information.hProcess, 90 * 1000);
#else
    char* server_argv[2] = {0};
    char *client_argv[2] = {0};
    pid_t server_pid, client_pid, w;
    int child_status;

    /* Start Server */
    server_argv[0] = OUT_PROC_SERVER;
    server_argv[1] = NULL;

    server_pid = fork();
    TEST_ASSERT_NOT_EQUAL(-1, server_pid);

    if(0 == server_pid) {
        /* This is done by the child process. */
        int ret = execvp(server_argv[0], server_argv);

        /* If execvp returns, it must have failed. */
        printf("Failed to start server. Error: %d\n", errno);
        TEST_ASSERT_NOT_EQUAL(-1, ret);
    }

    sleep(2); /* Give sometime for the server to start and register with the broker */

    /* Start Client */
    client_argv[0] = OUT_PROC_CLIENT;
    client_argv[1] = NULL;

    client_pid = fork();
    TEST_ASSERT_NOT_EQUAL(-1, client_pid);

    if(0 == client_pid) {
        /* This is done by the child process. */
        int ret = execvp(client_argv[0], client_argv);

        /* If execvp returns, it must have failed. */
        printf("Failed to start client. Error: %d\n", errno);
        TEST_ASSERT_NOT_EQUAL(-1, ret);
    }

    sleep(90); /* Bump the timeout if you want the client to fire requests for more than a minute */
    w = waitpid(server_pid, &child_status, 0);
    if (server_pid != w) kill(server_pid, SIGKILL);

    w = waitpid(client_pid, &child_status, 0);
    if (client_pid != w) kill(client_pid, SIGKILL);


#endif //MA_WINDOWS
    stop_broker();
}

TEST(msgbus_out_proc_tests, verify_client_log) {
    FILE *client_log_fp = NULL;
    FILE *server_log_fp = NULL;
    int c;

	client_log_fp = fopen(CLIENT_OUT_FILE, "r");
    TEST_ASSERT_NOT_NULL(client_log_fp);
    printf("Printing client 'out' file content: %s\n", CLIENT_OUT_FILE);
    while ((c = fgetc(client_log_fp)) != EOF) {
        printf("%c", c);
    }

	server_log_fp = fopen(SERVER_OUT_FILE, "r");
	TEST_ASSERT_NOT_NULL(server_log_fp);
    printf("Printing server 'out' file content: %s\n", SERVER_OUT_FILE);
    while ((c = fgetc(server_log_fp)) != EOF) {
        printf("%c", c);
    }

#ifdef MA_WINDOWS
    TEST_ASSERT_EQUAL_INT(0, fseek(client_log_fp, -3, SEEK_END)); /* Trying to read last 3 characters. Success(1\nEOF) or Failure(0\nEOF) */
#else
    TEST_ASSERT_EQUAL_INT(0, fseek(client_log_fp, -2, SEEK_END)); /* Success(1\n) or Failure(0\n) */
#endif

    c = fgetc(client_log_fp);
    TEST_ASSERT_EQUAL_INT(1, c);

#ifdef MA_WINDOWS
    TEST_ASSERT_EQUAL_INT(0, fseek(server_log_fp, -3, SEEK_END)); /* Trying to match the one way message */
#else
    TEST_ASSERT_EQUAL_INT(0, fseek(server_log_fp, -2, SEEK_END)); /* Success(1\n) or Failure(0\n) */
#endif

    c = fgetc(server_log_fp);
    TEST_ASSERT_TRUE('X' == c);

    fclose(client_log_fp);
    fclose(server_log_fp);
}

