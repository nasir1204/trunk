#include "ma/ma_msgbus.h"
#include "ma/ma_buffer.h"
#include "ma/ma_variant.h"
#include "ma/ma_message.h"
#include "ma/logger/ma_file_logger.h"

//#define MA_THREADING_ENABLED /* Need to enable it later...CHECK!!!*/
#include "ma/internal/utils/threading/ma_atomic.h"

#include "uv.h"
#include <stdio.h>
#include <stdlib.h>

#define MAX_SIZE      10

#define LOG_FACILITY_NAME           "client"
#ifdef MA_WINDOWS
#define LOG_BASE_DIR                "C:\\" // TODO. Get the base dir from installer or something equivalent
#else
#define LOG_BASE_DIR                "/tmp/"
#endif

#define CLIENT_LOG_FILE_NAME        "msgbus_out_proc_client"
#define CLIENT_LOG_FILE_EXT         ".log"

#define CLIENT_OUT_FILE_NAME        "msgbus_out_proc_client_result"
#define CLIENT_OUT_FILE_EXT         ".out"

static ma_msgbus_t *msgbus = NULL;

static ma_file_logger_t *logger = NULL;
ma_logger_t *ma_broker_get_logger();

ma_logger_t *ma_broker_get_logger() {
	return (ma_logger_t *)logger;
}

#define MA_CLIENT_LOGGER ma_broker_get_logger()


FILE *client_fp = NULL;
uv_cond_t          cond;
uv_mutex_t         mux;
unsigned short done = 0;

size_t send_until_zero = 0x1 << 12;
ma_atomic_counter_t recieve_until_zero = 0x1 << 12;
ma_atomic_counter_t n_matched_replies = 0;

typedef struct msgbus_client_handles_s
{
	ma_msgbus_server_h p_service;
	ma_msgbus_endpoint_h p_client_endpoint;
}msgbus_client_handles_t;

msgbus_client_handles_t _handles = {NULL,NULL};

typedef struct data_s
{
	size_t req_no;
	size_t size;
	short block[MAX_SIZE];
} data_t;

static void send_and_forget_test() {

	ma_message_t *client_request = NULL;
	ma_msgbus_request_h mb_request = NULL;
	ma_variant_h variant = NULL;
	int i = 0;
	ma_error_t rc = MA_OK;
	char test_data[] = "X";


	printf("[client] send and forget...\n");
	fprintf(client_fp, "send and forget...\n");

	if(MA_OK == rc) rc = ma_variant_create_from_string(test_data, strlen(test_data) + 1, &variant);
	if(MA_OK == rc) rc = ma_message_create(&client_request);

	if(MA_OK == rc) rc = ma_message_set_property(client_request, "ONEWAY", "TEST");
	if(MA_OK == rc) rc = ma_message_set_payload(client_request, variant);

	if(MA_OK == rc) rc = ma_msgbus_endpoint_create(msgbus, "ep.flip.service", NULL, & _handles.p_client_endpoint);
	if(MA_OK == rc) rc = ma_msgbus_endpoint_setopt(_handles.p_client_endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
	if(MA_OK == rc) rc = ma_msgbus_endpoint_setopt(_handles.p_client_endpoint, MSGBUSOPT_TIMEOUT, 2 * 1000);

	if(MA_OK == rc) rc = ma_msgbus_send_and_forget(_handles.p_client_endpoint, client_request);
}

static ma_error_t async_send_callback(ma_error_t status, ma_message_t *response, void *cb_data, ma_msgbus_request_t *mb_request) {

	ma_error_t rc = MA_OK;

	ma_variant_h variant= NULL;
	ma_buffer_h p_buffer= NULL;
	unsigned char *raw_data = NULL;
	size_t size = 0;
	data_t *flipped_blocks = {0};

	if (MA_OK == status && response) {
		int i = 0;

		rc = ma_message_get_payload(response, &variant);
		if(MA_OK == rc) rc = ma_variant_get_raw_buffer(variant, &p_buffer);
		if(MA_OK == rc) rc = ma_buffer_get_raw(p_buffer, &raw_data, &size);
		if(MA_OK == rc) {
			flipped_blocks = (data_t*) raw_data;

			printf("[client] Got reply for Req no = %d\n", flipped_blocks->req_no);
			fprintf(client_fp, "Got reply for Req no = %d\n", flipped_blocks->req_no);

			for(i = 0; i < flipped_blocks->size && flipped_blocks->block[i] == ~0x0; ++i) {/* Empty */}

			if (MAX_SIZE == i) {
				printf("[client] Reply matched\n");
				fprintf(client_fp, "Reply matched\n");
				MA_ATOMIC_INCREMENT(n_matched_replies);
				// any cleanup??
				rc = MA_OK;
			}
			else {
				printf("[client] Reply NOT matched\n");
				fprintf(client_fp, "Reply NOT matched\n");
				rc = MA_ERROR_UNEXPECTED;
			}
		}
	}
	else {
		printf("[client] Bad status. %d\n", status);
		fprintf(client_fp, "Bad status: %d\n", status);
		rc = MA_ERROR_UNEXPECTED;
	}
	/* Trying for a lock-free programming. Make sure the below one gets executed only at the end */
	if (0 == MA_ATOMIC_DECREMENT(recieve_until_zero)) { /* Last reply recieved */
		printf("[client] Stopping client...\n");
		fprintf(client_fp, "Stopping client...\n");
		uv_mutex_lock(&mux);
		done = 1;
		uv_cond_signal(&cond);
		uv_mutex_unlock(&mux);
	}

	return rc;
}

static void send_request(size_t req_no) {
	ma_message_t *client_request = NULL;
	ma_msgbus_request_h mb_request = NULL;
	ma_variant_h variant = NULL;
	int i = 0;
	ma_error_t rc = MA_OK;
	data_t *client_data = NULL;

	client_data = (data_t *) calloc(1, sizeof(data_t));
	if(client_data) {
		int i = 0;
		printf("[client] Flip the block values and send it back to me. Req no = %d\n", req_no);
		fprintf(client_fp, "Flip the block values and send it back to me. Req no = %d\n", req_no);
		client_data->req_no = req_no;
		client_data->size = MAX_SIZE;
		for(i = 0; i < MAX_SIZE; ++i) {
			client_data->block[i] = 0x0;
		}

		rc = ma_variant_create_from_raw((void*)client_data, sizeof(data_t), &variant);
		if(MA_OK == rc) rc = ma_message_create(&client_request);
		free(client_data);

		/* Stop the server after the last request */
		if (0 == req_no) {
			if(MA_OK == rc) rc = ma_message_set_property(client_request, "STOP_AFTER_THIS", "I AM DONE");
		}
		if(MA_OK == rc) rc = ma_message_set_property(client_request, "FLIP", "ALL");
		if(MA_OK == rc) rc = ma_message_set_payload(client_request, variant);

		if(MA_OK == rc) rc = ma_msgbus_endpoint_create(msgbus, "ep.flip.service", NULL, & _handles.p_client_endpoint);
		if(MA_OK == rc) rc = ma_msgbus_endpoint_setopt(_handles.p_client_endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);
		if(MA_OK == rc) rc = ma_msgbus_endpoint_setopt(_handles.p_client_endpoint, MSGBUSOPT_TIMEOUT, 2 * 1000);

		if(MA_OK == rc) rc = ma_msgbus_async_send(_handles.p_client_endpoint, client_request, async_send_callback, NULL, &mb_request);

		// any cleanup??
	}
	else {
		fprintf(client_fp, "Not enough memory to create client data\n");
		fclose(client_fp);
		exit(1);
	}
}


#define GLUE_IT(dest, X, Y, Z) \
	strcpy(dest, X); \
	strcat(dest, Y); \
	strcat(dest, Z);

int main(int argc, char* argv[])
{
	ma_error_t rc = MA_OK;
	size_t req_no = send_until_zero;

	char file_name[1024] = {0};
	GLUE_IT(file_name, LOG_BASE_DIR, CLIENT_OUT_FILE_NAME, CLIENT_OUT_FILE_EXT);

	client_fp = fopen(file_name, "w");
	if (! client_fp)
		exit(1);

	if (MA_OK != (rc = ma_file_logger_create(LOG_BASE_DIR, CLIENT_LOG_FILE_NAME, CLIENT_LOG_FILE_EXT, &logger))) {
		fprintf(stderr, "[client] Error creating <file logger> instance. Error code = %d\n", rc);
	}

    MA_LOG(MA_CLIENT_LOGGER, MA_LOG_SEV_INFO, "initializing message bus");
    if (MA_OK != (rc = ma_msgbus_create("MSGBUS_OUTPROC_TEST", &msgbus))) {
        MA_LOG(MA_CLIENT_LOGGER, MA_LOG_SEV_ERROR, "message bus initialization failed. Error code = %d", rc);
        fclose(client_fp);
        return EXIT_FAILURE;
    }

    if (MA_OK != (rc = ma_msgbus_set_logger(msgbus, MA_CLIENT_LOGGER))) {
        MA_LOG(MA_CLIENT_LOGGER, MA_LOG_SEV_ERROR, "failed to set message bus logger. Error code = %d", rc);
    }

	if (MA_OK != (rc = ma_msgbus_start(msgbus))) {
        MA_LOG(MA_CLIENT_LOGGER, MA_LOG_SEV_ERROR, "failed to start msgbus. Error code = %d", rc);
    }

    /* Testing send and forget */
    send_and_forget_test();

	/* Testing async send */
	uv_cond_init(&cond);
	uv_mutex_init(&mux);
	while(req_no) {        
		//#ifdef MA_WINDOWS
		//        Sleep(1*1000); /* ATTENTION!!! Sending the messages without time gap are never reaching the server. */
		//#else
		//        sleep(1);
		//#endif
		send_request(--req_no);
	}

	printf("[client] Waiting on condition...\n");
	fprintf(client_fp, "[client] Waiting on condition...\n");
	uv_mutex_lock(&mux);
	while(0 == done) {
		fprintf(client_fp, "[client] COND_WAIT\n");
		uv_cond_wait(&cond, &mux); 
	}
	printf("[client] Out of condition\n");
	fprintf(client_fp, "[client] Out of condition\n");

	uv_mutex_unlock(&mux);
	uv_mutex_destroy(&mux);
	uv_cond_destroy(&cond);
    ma_msgbus_stop(msgbus, MA_TRUE); /* ATTENTION!!! This is blocking */
	ma_msgbus_release(msgbus);

	fprintf(client_fp, "%d\n", n_matched_replies == send_until_zero); /* Print Success or Failure at the end of file. This will be asserted in msgbus_out_proc_tests.c*/
	fclose(client_fp);
	return EXIT_SUCCESS;
}


