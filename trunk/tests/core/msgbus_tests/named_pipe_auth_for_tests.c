#include "ma/internal/core/msgbus/ma_named_pipe_auth.h"
#include "ma/internal/core/msgbus/ma_msgbus_defs.h"
#include "ma/internal/core/msgbus/ma_message_auth_info_private.h"
#include "ma/internal/core/msgbus/ma_msgbus_crypto_provider.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>

struct ma_named_pipe_auth_s {

    char 										signer[128];

	char 										subject[128] ;

    long 										ref_count;

	ma_named_pipe_on_authenticate_cb            authenticate_cb;

    void                                        *authenticate_cb_data;

    uv_loop_t                                   *uv_loop;
    ma_message_auth_info_t			*auth_info;

    uv_timer_t                                  uv_timer;


};


ma_error_t ma_named_pipe_auth_create(uv_loop_t *uv_loop, struct ma_msgbus_crypto_provider_s *crypto_provider, uv_pipe_t *pipe, ma_named_pipe_auth_t **pipe_auth) {
    if(uv_loop && pipe_auth) {
        ma_named_pipe_auth_t *self = (ma_named_pipe_auth_t*)calloc(1, sizeof(ma_named_pipe_auth_t));
        if (self) {
		ma_error_t rc = MA_OK;
		if(MA_OK == (rc = ma_message_auth_info_create(&self->auth_info))) {
			self->ref_count = 1;
			self->uv_loop = uv_loop;
			strcpy(self->signer, "testsigner");
			strcpy(self->subject, "testsubject");
			*pipe_auth = self;
            		return MA_OK;
		}
		return rc;
        }
        return MA_ERROR_OUTOFMEMORY;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_named_pipe_auth_add_ref(ma_named_pipe_auth_t *self) {
    if (self && (0 <= self->ref_count)) {
        ++self->ref_count;
    }
    return MA_OK;
}
ma_error_t ma_named_pipe_auth_release(ma_named_pipe_auth_t *self) {
    if (self && (0 <= self->ref_count)) {
        if (0 == (--self->ref_count)) {
		ma_message_auth_info_release(self->auth_info);
			free(self);
        }
    }
    return MA_OK;
}

ma_error_t ma_named_pipe_auth_set_logger(ma_named_pipe_auth_t *self, ma_logger_t *logger) {
    if(self) {
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_named_pipe_auth_stop(ma_named_pipe_auth_t *self) {
    if (self) {
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}
ma_error_t ma_named_pipe_auth_set_passphrase_handler(ma_named_pipe_auth_t *self, struct ma_msgbus_passphrase_handler_s *passphrase_handler) {
	if(self) {
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}

static void close_cb(uv_handle_t *handle) {
    ma_named_pipe_auth_t *self = (ma_named_pipe_auth_t *)handle->data;
    ma_message_auth_info_add_ref(self->auth_info);
    self->authenticate_cb(self, MA_OK, self->authenticate_cb_data, self->auth_info);    
}

static void timer_cb(uv_timer_t *handle, int status) {
    uv_close((uv_handle_t *)handle, close_cb); 
}

ma_error_t ma_named_pipe_auth_start(ma_named_pipe_auth_t *self, long timeout_in_milliseconds, ma_named_pipe_on_authenticate_cb cb, void *cb_data) {
    if(self) {
        self->authenticate_cb = cb;
        self->authenticate_cb_data = cb_data;
        uv_timer_init(self->uv_loop, &self->uv_timer);
        self->uv_timer.data = self;
        uv_timer_start(&self->uv_timer, timer_cb, 0, 0);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_named_pipe_auth_set_server(ma_named_pipe_auth_t *self) { 
	return MA_OK; 
}
