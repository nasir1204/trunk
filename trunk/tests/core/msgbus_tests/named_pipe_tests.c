#include "unity.h"
#include "unity_fixture.h"


#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/core/msgbus/ma_named_pipe_server.h"
#include "ma/internal/core/msgbus/ma_named_pipe_connection.h"
#include "ma/internal/core/msgbus/ma_message_header.h"
#include "ma/logger/ma_console_logger.h"

#include "tests/utils/macrypto_test/ma_crypto_tests_setup.h"

#include "ma/internal/ma_macros.h"

#include <fcntl.h>

#ifndef MA_WINDOWS
    #include <unistd.h>
#endif

#define MAX_PIPENAME_LEN            256

static const char pipe_name_template[] = "ma_msgbus_tests_99407f44-16b5-4ca1-bfbc-d32de8f23b86.%lu";

typedef struct msgbus_tests_s {

    ma_event_loop_t *ma_loop;

    ma_crypto_t     *crypto;

    ma_named_pipe_server_t *pipe_server;

    ma_named_pipe_connection_t *c_connection, *s_connection;

    ma_message_consumer_t s_consumer_impl;    

    ma_logger_t		*logger;

} msgbus_tests_t;

static msgbus_tests_t sut;

TEST_GROUP_RUNNER(named_pipe_test_group) {
    RUN_TEST_CASE(named_pipe_tests, simple_send_receive)
}

/* BEGIN apparatus for server to receive messages */

static void close_all(void) {

    ma_named_pipe_connection_close(sut.s_connection, 0);

    ma_named_pipe_connection_close(sut.c_connection, 0);

    ma_named_pipe_server_stop(sut.pipe_server);
}

static ma_error_t on_message(ma_message_consumer_t *base, ma_message_t *m) {
    /* TODO Check the message */

    /* Shut down everything so we can terminate */
    ma_named_pipe_connection_set_consumer(sut.s_connection, NULL);

    close_all();


    return MA_OK;
};

const static ma_message_consumer_methods_t s_methods = {
    &on_message,
    NULL,
};
/* END apparatus */


static void s_accept_cb(ma_named_pipe_connection_t *connection, void *cb_data) {
    /* a client has connected, here is the connection ...*/
    sut.s_connection = connection;
    /* TODO Configure sink */
    ma_message_consumer_init(&sut.s_consumer_impl, &s_methods, -1, &sut);

    ma_named_pipe_connection_set_consumer(connection, &sut.s_consumer_impl);

    ma_named_pipe_connection_start_receiving(connection);
}

static void c_connect_cb(ma_named_pipe_connection_t *connection, ma_error_t status, void *cb_data) {
    /* client connected,  */
    ma_message_consumer_t *consumer = (ma_message_consumer_t *) connection;
    ma_message_t *msg;

    ma_named_pipe_connection_start_receiving(connection);

    //close_all();
    // send a message to server
    ma_message_create(&msg);
	SET_MESSAGE_DESTINATION_PID(msg , getpid());
    ma_message_consumer_send(consumer, msg);
    ma_message_release(msg);
}

// Create a folder
static void create_test_folder(const char *dir_name)
{
  uv_fs_t req;
  uv_fs_mkdir(ma_event_loop_get_uv_loop(sut.ma_loop), &req, dir_name, O_RDWR | O_CREAT, NULL);
  uv_fs_req_cleanup(&req);

  uv_fs_chmod(ma_event_loop_get_uv_loop(sut.ma_loop), &req, dir_name, 0777, NULL);
  uv_fs_req_cleanup(&req);
}

// Remove the folder
static void remove_test_folder(const char *dir_name)
{
  uv_fs_t req;
  uv_fs_rmdir(ma_event_loop_get_uv_loop(sut.ma_loop), &req, dir_name, NULL);
  uv_fs_req_cleanup(&req);
}

TEST_SETUP(named_pipe_tests) {
    const char *server_name;
    char pipe_name[MAX_PIPENAME_LEN];

    pipe_name[MAX_PIPENAME_LEN-1] = 0;
    ma_event_loop_create(&sut.ma_loop);

#ifndef MA_WINDOWS
	create_test_folder("/var/tmp/.msgbus");
#endif
    MA_MSC_CONCAT(_,snprintf)(pipe_name, MAX_PIPENAME_LEN-1, pipe_name_template, (unsigned long) getpid());

	ma_console_logger_create((ma_console_logger_t **)&sut.logger);


    /* start server */
    ma_named_pipe_server_create(sut.ma_loop, &sut.pipe_server);   
    ma_named_pipe_server_set_logger(sut.pipe_server, sut.logger);
    
    ma_named_pipe_server_start(sut.pipe_server, sut.crypto, pipe_name, s_accept_cb, &sut);
    ma_named_pipe_server_get_pipe_name(sut.pipe_server,&server_name);

    /* start client */
    ma_named_pipe_connection_create(sut.ma_loop, &sut.c_connection);
	ma_named_pipe_connection_set_logger(sut.c_connection, sut.logger);
    ma_named_pipe_connection_connect(sut.c_connection, sut.crypto, server_name, c_connect_cb, &sut);
}

TEST_TEAR_DOWN(named_pipe_tests) {

    ma_named_pipe_connection_release(sut.c_connection), sut.c_connection = NULL;

    ma_named_pipe_connection_release(sut.s_connection), sut.s_connection = NULL;	

    ma_named_pipe_server_release(sut.pipe_server);

#ifndef MA_WINDOWS	
	// The call to remove the folder is crashing as the uv_loop is being deleted inside 
	// the ma_event_loop_run call. 
	// Proper fix would be to move the uv_loop_delete call from ma_event_loop_run to ma_event_loop_release. 
	// Commenting out the remove folder call for the time being. 
	//remove_test_folder("/var/tmp/.msgbus");
#endif

	ma_event_loop_release(sut.ma_loop);
}

TEST(named_pipe_tests, simple_send_receive) {
    ma_event_loop_run(sut.ma_loop);
}

