#include "unity.h"
#include "unity_fixture.h"

#include "ma/internal/core/msgbus/ma_message_router.h"

typedef struct ma_message_router_sut_s {
    ma_message_router_t *router;
} ma_message_router_sut_t;

typedef struct test_consumer_s {
    /* 'derive' from  */
    ma_message_consumer_t consumer_impl;

    int test_count;

} test_consumer_t;


static ma_message_router_sut_t sut;


static ma_error_t on_message(ma_message_consumer_t *base, ma_message_t *msg) {
    test_consumer_t *self = base->data;

    ++self->test_count;

    return MA_OK;
}

static void destroy(ma_message_consumer_t *base) {
    test_consumer_t *self = base->data;
}


static const ma_message_consumer_methods_t methods = {
    &on_message,
    &destroy
};



static void init_consumer(test_consumer_t *c) {
    c->consumer_impl.methods = &methods;
    c->consumer_impl.data = c;
    c->test_count = 0;
}

static test_consumer_t test_consumer;


TEST_GROUP_RUNNER(router_test_group) {
    do {RUN_TEST_CASE(router_tests, test_add_subscriber);} while (0);
    do {RUN_TEST_CASE(router_tests, test_add_remove_subscriber);} while (0); /* do..while is a Unity workaround */
    do {RUN_TEST_CASE(router_tests, test_add_add_remove_subscriber);} while (0); /* do..while is a Unity workaround */

}

TEST_SETUP(router_tests) {
    init_consumer(&test_consumer);
    ma_message_router_create(&sut.router);
}

TEST_TEAR_DOWN(router_tests) {
    ma_message_router_release(sut.router), sut.router = NULL;
}





TEST(router_tests, test_add_subscriber) {

    TEST_ASSERT_EQUAL_INT(0,test_consumer.test_count);
    ma_message_consumer_send((ma_message_consumer_t*)sut.router, NULL);
    TEST_ASSERT_EQUAL_INT(0,test_consumer.test_count);

    ma_message_router_add_consumer(sut.router, &test_consumer.consumer_impl, NULL, MA_MSGBUS_CLIENT);

    ma_message_consumer_send((ma_message_consumer_t*)sut.router, NULL);
    TEST_ASSERT_EQUAL_INT(1,test_consumer.test_count);

    ma_message_consumer_send((ma_message_consumer_t*)sut.router, NULL);
    TEST_ASSERT_EQUAL_INT(2,test_consumer.test_count);

}


TEST(router_tests, test_add_remove_subscriber) {

    ma_message_router_add_consumer(sut.router, &test_consumer.consumer_impl, NULL, MA_MSGBUS_CLIENT);

    ma_message_consumer_send((ma_message_consumer_t*)sut.router, NULL);
    TEST_ASSERT_EQUAL_INT(1,test_consumer.test_count);

    ma_message_router_remove_consumer(sut.router, &test_consumer.consumer_impl);
    ma_message_consumer_send((ma_message_consumer_t*)sut.router, NULL);
    TEST_ASSERT_EQUAL_INT(1,test_consumer.test_count);

}


TEST(router_tests, test_add_add_remove_subscriber) {

    ma_message_router_add_consumer(sut.router, &test_consumer.consumer_impl, NULL, MA_MSGBUS_CLIENT);
    ma_message_router_add_consumer(sut.router, &test_consumer.consumer_impl, NULL, MA_MSGBUS_CLIENT);

    ma_message_consumer_send((ma_message_consumer_t*)sut.router, NULL);
    TEST_ASSERT_EQUAL_INT(2,test_consumer.test_count);

    /*  */
    ma_message_router_remove_consumer(sut.router, &test_consumer.consumer_impl);
    ma_message_consumer_send((ma_message_consumer_t*)sut.router, NULL);
    TEST_ASSERT_EQUAL_INT(3,test_consumer.test_count);

    ma_message_router_remove_consumer(sut.router, &test_consumer.consumer_impl);
    ma_message_consumer_send((ma_message_consumer_t*)sut.router, NULL);
    TEST_ASSERT_EQUAL_INT(3,test_consumer.test_count);

}
