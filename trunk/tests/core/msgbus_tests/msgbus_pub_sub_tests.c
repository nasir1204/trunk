#include "unity.h"
#include "unity_fixture.h"

#include "ma/ma_msgbus.h"
#include "ma/ma_message.h"
#include "ma/internal/core/msgbus/ma_message_header.h"
#include <string.h>
#include <uv.h>

TEST_GROUP_RUNNER(msgbus_pub_sub_test_group) {
	RUN_TEST_CASE(msgbus_pub_sub_tests, subscribe_message);
	
}

#define PUBLISHER_TOPIC		"ma.managebaility.policy.enforcement.timeout"
#define SUBSCRIBER_TOPIC_FILTER	 "ma.managebaility.*"

ma_msgbus_subscriber_t *sub = NULL ;

static ma_bool_t done = 0;
static uv_cond_t cond;
static uv_mutex_t mux;

static ma_msgbus_t *msgbus = NULL;


static void wait_until_done() {

    uv_mutex_lock(&mux);

    while (!done) {uv_cond_wait(&cond,&mux);}

    uv_mutex_unlock(&mux);
}



TEST_SETUP(msgbus_pub_sub_tests) {
    uv_mutex_init(&mux), uv_cond_init(&cond);
    ma_msgbus_create("PUB_SUB_TEST", &msgbus);
	ma_msgbus_start(msgbus);
}

TEST_TEAR_DOWN(msgbus_pub_sub_tests) {	

	ma_msgbus_stop(msgbus, MA_TRUE);
    ma_msgbus_release(msgbus);

    uv_cond_destroy(&cond), uv_mutex_destroy(&mux);
}

static ma_error_t subscriber_cb(const char *topic, ma_message_t *payload, void *cb_data) {
	char *value  = NULL ;
	
	TEST_ASSERT_EQUAL_INT(MA_MESSAGE_TYPE_PUBLISH , GET_MESSAGE_TYPE(payload));

	ma_message_get_property(payload , MA_MSG_PUBLISH_TOPIC  , &value);

	TEST_ASSERT_EQUAL_INT(0  , strcmp(PUBLISHER_TOPIC ,value));

	ma_msgbus_subscriber_unregister(sub);
	ma_msgbus_subscriber_release(sub);
	

	done = MA_TRUE;
    uv_cond_signal(&cond);
	return MA_OK ;
}

void sub_test_thread(void *arg) {
	ma_message_t *msg = NULL ;
	ma_msgbus_subscriber_create( msgbus, &sub );
	ma_msgbus_subscriber_register(sub , SUBSCRIBER_TOPIC_FILTER , subscriber_cb , sub);		
	ma_message_create(&msg);
	ma_msgbus_publish(msgbus, PUBLISHER_TOPIC, MSGBUS_CONSUMER_REACH_INPROC , msg)  ;
	ma_message_release(msg);
}

TEST(msgbus_pub_sub_tests, subscribe_message) {
    uv_thread_t tid;	

    uv_thread_create(&tid, &sub_test_thread , NULL );

	wait_until_done();
	uv_thread_join(&tid);
}

