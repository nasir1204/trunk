#include "ma/ma_msgbus.h"
#include "ma/ma_buffer.h"
#include "ma/ma_variant.h"
#include "ma/ma_message.h"
#include "ma/logger/ma_file_logger.h"

#include "uv.h"
#include <stdio.h>
#include <stdlib.h>

#define MAX_SIZE      10

#define LOG_FACILITY_NAME           "server"
#ifdef MA_WINDOWS
#define LOG_BASE_DIR                "C:\\" // TODO. Get the base dir from installer or something equivalent
#else
#define LOG_BASE_DIR                "/tmp/"
#endif

#define SERVER_LOG_FILE_NAME        "msgbus_out_proc_server"
#define SERVER_LOG_FILE_EXT         ".log"

#define SERVER_OUT_FILE_NAME        "msgbus_out_proc_server_result"
#define SERVER_OUT_FILE_EXT         ".out"

static ma_msgbus_t *msgbus = NULL;

static ma_file_logger_t *logger = NULL;
ma_logger_t *ma_broker_get_logger();

ma_logger_t *ma_broker_get_logger() {
    return (ma_logger_t *)logger;
}

#define MA_SERVER_LOGGER ma_broker_get_logger()

FILE               *server_fp = NULL;
uv_cond_t          cond;
uv_mutex_t         mux;

typedef struct msgbus_server_handles_s
{
    ma_msgbus_server_h p_service;
}msgbus_server_handles_t;

msgbus_server_handles_t _handles = {NULL};

typedef struct data_s
{
    size_t req_no;
    size_t size;
    short block[MAX_SIZE];
}data_t;

char *one_way_msg = NULL;


static ma_error_t flip_service_handler(ma_msgbus_server_t *server, ma_message_t *request_payload, void *cb_data, ma_msgbus_client_request_t *c_request) {
    ma_variant_h variant= NULL;
    ma_buffer_h p_buffer= NULL;
    unsigned char *raw_data = NULL;
    size_t size = 0;
    data_t *flip_blocks = {0};
    ma_message_t *reply = NULL;
    char *value = NULL;

    /* Message Filter */
    if (server && request_payload) {
        if (MA_OK == ma_message_get_property(request_payload, "FLIP", &value) && !strcmp("ALL", value)) {
            int i = 0;

            ma_message_get_payload(request_payload, &variant);
            ma_variant_get_raw_buffer(variant, &p_buffer);
            ma_buffer_get_raw(p_buffer, &raw_data, &size);

            flip_blocks = (data_t*) raw_data;
            printf("[server] Flipping the blocks of Req no = %d\n", flip_blocks->req_no);
            fprintf(server_fp, "Flipping the blocks of Req no = %d\n", flip_blocks->req_no);

            for(i = 0; i < flip_blocks->size; ++i) {
                flip_blocks->block[i] = ~flip_blocks->block[i];
            }
            fprintf(server_fp, "Result post flipping\n");
            for(i = 0; i < flip_blocks->size; ++i) { 
                fprintf(server_fp, "\t flip_blocks->block[%d]: %d\n", i, flip_blocks->block[i]);
            }

            ma_variant_create_from_raw((void*)flip_blocks, sizeof(data_t), &variant);
            ma_message_create(&reply);
            ma_message_set_payload(reply, variant);
            ma_msgbus_server_client_request_post_result(c_request, MA_OK, reply);

            if (MA_OK == ma_message_get_property(request_payload, "STOP_AFTER_THIS", &value)) {
                uv_mutex_lock(&mux);
                uv_cond_signal(&cond);
                uv_mutex_unlock(&mux);
            }

        }
        else if (MA_OK == ma_message_get_property(request_payload, "ONEWAY", &value) && !strcmp("TEST", value)) {

            ma_buffer_t *buffer = NULL;
            size_t size = 0;
            char data[] = "GOT ONEWAY MSG"; // this will never reach the client.

            printf("[server] got one way message...\n");
            fprintf(server_fp, "\ngot one way message...\n");

            ma_message_get_payload(request_payload, &variant);
            ma_variant_get_string_buffer(variant, &buffer);
            ma_buffer_get_string(buffer, &one_way_msg, &size);
            printf("[server] one way message: %s\n", one_way_msg);

            ma_variant_create_from_string(data, strlen(data) + 1, &variant);
            ma_message_create(&reply);
            ma_message_set_payload(reply, variant);
            ma_msgbus_server_client_request_post_result(c_request, MA_OK, reply); // this will never reach the client.
        }
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

#define GLUE_IT(dest, X, Y, Z) \
    strcpy(dest, X); \
    strcat(dest, Y); \
    strcat(dest, Z);

int main(int argc, char* argv[])
{
    ma_error_t rc = MA_OK;
    int ret = 0;

    char file_name[1024] = {0};
    GLUE_IT(file_name, LOG_BASE_DIR, SERVER_OUT_FILE_NAME, SERVER_OUT_FILE_EXT);

    server_fp = fopen(file_name, "w");
    if(! server_fp)
        exit(1);

    if (MA_OK != (rc = ma_file_logger_create(LOG_BASE_DIR, SERVER_LOG_FILE_NAME, SERVER_LOG_FILE_EXT, &logger))) {
        fprintf(stderr, "[server] Error creating <file logger> instance. Error code = %d\n", rc);
    }

    if (MA_OK != (rc = ma_msgbus_set_logger(msgbus, MA_SERVER_LOGGER))) {
        MA_LOG(MA_SERVER_LOGGER, MA_LOG_SEV_ERROR, "failed to set message bus logger. Error code = %d", rc);
    }

    MA_LOG(MA_SERVER_LOGGER, MA_LOG_SEV_INFO, "initializing message bus");
    if (MA_OK != (rc = ma_msgbus_create("MSGBUS_OUTPROC_SERVER", &msgbus))) {
        MA_LOG(MA_SERVER_LOGGER, MA_LOG_SEV_ERROR, "message bus create failed. Error code = %d", rc);
        fclose(server_fp);
        return EXIT_FAILURE;
    }

	if (MA_OK != (rc = ma_msgbus_start(msgbus))) {
        MA_LOG(MA_SERVER_LOGGER, MA_LOG_SEV_ERROR, "message bus start failed. Error code = %d", rc);
        fclose(server_fp);
        return EXIT_FAILURE;
    }

    rc = ma_msgbus_server_create(msgbus, "ep.flip.service", &_handles.p_service);
    rc = ma_msgbus_server_setopt(_handles.p_service, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC);

    printf("[server] Starting Server...\n");
    fprintf(server_fp, "Starting Server...\n");

    uv_cond_init(&cond);
    uv_mutex_init(&mux);
    rc = ma_msgbus_server_start(_handles.p_service, flip_service_handler, NULL);

    printf("[server] Waiting on condition...\n");
    fprintf(server_fp, "[server] Waiting on condition...\n");

    uv_mutex_lock(&mux);

    //fprintf(server_fp, "[server] COND_WAIT: %d\n",uv_cond_timedwait(&cond, &mux, (uint64_t)(60 * 1e9))); /* Bump the timeout if the client fires requests for more than a minute */
    fprintf(server_fp, "[server] COND_WAIT\n"),
    uv_cond_wait(&cond, &mux);

    uv_mutex_unlock(&mux);
    uv_mutex_destroy(&mux);
    uv_cond_destroy(&cond);

    printf("[server] Out of condition\n");
    fprintf(server_fp, "[server] Out of condition\n");

#ifdef MA_WINDOWS
    Sleep(2 *1000); // Wait for any pending replies to go out...
#else
    sleep(2);
#endif

    ma_msgbus_server_stop(_handles.p_service);
    ma_msgbus_server_release(_handles.p_service);
    ma_msgbus_stop(msgbus, MA_TRUE); // This is blocking. Need to look at this
	ma_msgbus_release(msgbus); 

    printf("[server] Server going away\n");
    fprintf(server_fp, "\nServer going away\n");
    fprintf(server_fp, "test string:\n%s\n", one_way_msg);
    fclose(server_fp);
    return EXIT_SUCCESS;
}


