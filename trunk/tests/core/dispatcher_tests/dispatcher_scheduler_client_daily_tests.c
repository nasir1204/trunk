//#if 0
#include "unity.h"
#include "unity_fixture.h"

#include "ma/ma_common.h"
#include "ma/internal/ma_macros.h"
#include "ma/scheduler/ma_scheduler.h"
#include "ma/scheduler/ma_triggers.h"
#include "ma/scheduler/ma_task.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"
#include "ma/internal/defs/ma_scheduler_defs.h"
#include "ma/internal/services/scheduler/ma_scheduler_service.h"
#include "ma/internal/services/ma_service.h"
#include "ma/ma_dispatcher.h"
#include "ma/ma_msgbus.h"
//#include "ma_ut_setup.h"
#include "ma_client_ut_setup.h"
#include <uv.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


#ifdef MA_WINDOWS
# include <crtdbg.h>
#include <Windows.h>
# define MA_TRACKLEAKS _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF)
# define MA_INPUT_FOLDER_PATH ".\\infile.xml"
# define MA_OUTPUT_FOLDER_PATH ".\\outfile.xml"
#else
# define MA_TRACKLEAKS ((void *)0)
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
# define MA_INPUT_FOLDER_PATH "./infile.xml"
# define MA_OUTPUT_FOLDER_PATH "./outfile.xml"
#endif

#include "dispatcher_tests_utils.h"

#define DISPATCHER_PROUDUCT_ID "ma.dispatcher.test"

#ifdef MA_WINDOWS
# include <crtdbg.h>
#include <Windows.h>
# define MA_TRACKLEAKS _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF)
#define SOFTWARE_ID_PATH "SOFTWARE\\McAfee\\Agent\\Applications\\EPOAGENT3000"
#define SOFTWARE_ID_PATH1 "SOFTWARE\\McAfee\\Agent\\Applications\\EPOAGENT3000"
#define MA_DISPATCHER_PRODUCT_INSTAL_PATH "install_path"
#define MA_DISPATCHER_RSDK_PATH "RuntimeSdkPath"
#else
# define MA_TRACKLEAKS ((void *)0)
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#define SOFTWARE_ID_PATH "/etc/ma.d"
#endif


typedef struct{
    size_t on_task_execute_ctr;
    size_t on_task_stop_ctr;
    ma_bool_t schd_flag;
    ma_bool_t modify_task;
} ma_test_params_t;
static ma_test_params_t test_params;

static ma_service_t *g_scheduler_service = NULL;

static ma_client_t *g_ma_client;
static ma_ut_client_setup_t *g_ut_setup;

TEST_GROUP_RUNNER(scheduler_client_daily_test_group) {
    do {RUN_TEST_CASE(scheduler_client_daily_test, start_service); }while(0);
 //  do {RUN_TEST_CASE(scheduler_client_daily_test, task_set_get_settings); }while(0);
 //   do {RUN_TEST_CASE(scheduler_client_daily_test, trigger_multiple_trigger_combination_test); }while(0);
 //   do {RUN_TEST_CASE(scheduler_client_daily_test, add_task_single_trigger_retry_test); }while(0);
 //   do {RUN_TEST_CASE(scheduler_client_daily_test, multiple_triggers_intervals_test); }while(0);
 //   do {RUN_TEST_CASE(scheduler_client_daily_test, get_task_parameters_test); }while(0);
    //do {RUN_TEST_CASE(scheduler_client_daily_test, remove_task_multiple_task_single_trigger_test); }while(0);
    do {RUN_TEST_CASE(scheduler_client_daily_test, simple_add_trigger_test);}while(0);
    do {RUN_TEST_CASE(scheduler_client_daily_test, modify_task_single_task_multiple_trigger_test); }while(0);
    do {RUN_TEST_CASE(scheduler_client_daily_test, stop_service); }while(0); // don't forget to stop and release the service
}

static void scheduler_client_daily_test_reset_counters() {
    test_params.on_task_execute_ctr = 0;
    test_params.on_task_stop_ctr = 0;
    test_params.schd_flag = MA_FALSE;
    test_params.modify_task = MA_FALSE;
}

static ma_dispatcher_t *dispatcher = NULL;

TEST_SETUP(scheduler_client_daily_test_group) {
	#ifdef MA_WINDOWS
    ma_dispatcher_test_create_registry(SOFTWARE_ID_PATH);
#else
    ma_dispatcher_test_create_registry("EPOAGENT3000");
#endif
    ma_dispatcher_initialize(NULL, &dispatcher);
}

TEST_TEAR_DOWN(scheduler_client_daily_test_group) {
    ma_dispatcher_deinitialize( dispatcher);
	#ifdef MA_WINDOWS
   ma_dispatcher_tests_delete_registry(SOFTWARE_ID_PATH);
   ma_dispatcher_tests_delete_registry(SOFTWARE_ID_PATH1);
#else
    ma_dispatcher_tests_delete_registry("ma");
    ma_dispatcher_tests_delete_registry("ma1");
#endif
}


TEST(scheduler_client_daily_test_group, start_service) {
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ut_setup_create("scheduler_client_test", "scheduler.client.test", &g_ut_setup));
    g_ma_client = ma_ut_setup_get_client(g_ut_setup);
    // start the scheduler service
    TEST_ASSERT_MESSAGE(MA_OK == ma_scheduler_service_create(MA_SCHEDULER_SERVICE_NAME_STR, &g_scheduler_service), "scheduler service creation failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_service_configure(g_scheduler_service, ma_ut_setup_get_context(g_ut_setup), MA_SERVICE_CONFIG_NEW), "scheduler service configuration failed");
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_service_start(g_scheduler_service));
    //TBD let's enable the thread pool for testing. Else it will be tough to test the APIs that are internally using "send and forget" 
    
}


TEST(scheduler_client_daily_test_group, stop_service) {
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_service_stop(g_scheduler_service));
    ma_service_release(g_scheduler_service);
    ma_ut_setup_release(g_ut_setup);
}

static ma_error_t on_task_execute_get(ma_client_t *ma_client, const char *product_id, const char *task_type, const ma_task_t *task, void *cb_data) {
    char *task_id = NULL;
    ma_task_time_t curr_time;
    ma_task_time_t last_run_time;
    ma_task_time_t next_run_time;
    ma_task_t *reply_task = NULL;
    ma_trigger_t *trigger_1 = NULL;
    char *task_name = NULL;
    char *task_type_ = NULL;
    ma_trigger_t *reply_trigger = NULL;
    ma_trigger_type_t trigger_type;
    ma_trigger_state_t trigger_state;
    char *trigger_id = NULL;
    ma_task_time_t trigger_begin_date;
    ma_task_time_t trigger_end_date;
    ma_uint16_t max_run_time_limit = 0;
    ma_task_retry_policy_t retry_policy;
    ma_task_repeat_policy_t repeat_policy;
    ma_trigger_run_once_policy_t reply_run_once_policy;
    ma_trigger_daily_policy_t reply_daily_policy;
    printf("\n..................");
    printf("\nTask Type = %s", task_type);

    get_localtime(&curr_time);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id((ma_task_t *)task, &task_id));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_name((ma_task_t *)task, &task_name));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_type((ma_task_t *)task, &task_type_));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_get_task(g_ma_client, task_id, &reply_task));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_retry_policy(reply_task, &retry_policy));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_repeat_policy(reply_task, &repeat_policy));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id(reply_task, &task_id));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_name(reply_task, &task_name));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_type(reply_task, &task_type_));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_max_run_time_limit(reply_task, &max_run_time_limit));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_last_run_time(reply_task, &last_run_time));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_next_run_time(reply_task, &next_run_time));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_find_trigger(reply_task, "0", &trigger_1));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_find_trigger(reply_task, "0", &reply_trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_get_type(reply_trigger, &trigger_type));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_get_state(reply_trigger, &trigger_state));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_get_id(reply_trigger, &trigger_id));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_get_begin_date(reply_trigger, &trigger_begin_date));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_get_end_date(reply_trigger, &trigger_end_date));
    if(MA_TRIGGER_ONCE == trigger_type) {
        TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_get_policy(reply_trigger, &reply_run_once_policy));
        printf("\nrun_once_policy = %d", reply_run_once_policy.once);
    }else if(MA_TRIGGER_DAILY == trigger_type) {
        TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_get_policy(reply_trigger, &reply_daily_policy));
        printf("\ndaily_policy = %d", reply_daily_policy.days_interval);
    }

    printf("\nRetry Count = %d", retry_policy.retry_count);
    printf("\nRetry retry_interval = %d", retry_policy.retry_interval.minute);
    printf("\nRepeat interval - hour = %d", repeat_policy.repeat_interval.hour);
    printf("\nRepeat interval - min = %d", repeat_policy.repeat_interval.minute);
    printf("\nRepeat until - min = %d", repeat_policy.repeat_until.hour);
    printf("\nRepeat until - min = %d", repeat_policy.repeat_until.minute);
    printf("\nTask Id = %s", task_id);
    printf("\nTask Name = %s", task_name);
    printf("\nTask Type = %s", task_type_);
    printf("\nTask Max RunTime Limit = %d", max_run_time_limit);
    printf("\nTask Last Run Time = %d/%d/%d:%d:%d:%d", last_run_time.day, last_run_time.month, last_run_time.year, last_run_time.hour, last_run_time.minute, last_run_time.secs);
    printf("\nTask Next Run Time = %d/%d/%d:%d:%d:%d", next_run_time.day, next_run_time.month, next_run_time.year, next_run_time.hour, next_run_time.minute, next_run_time.secs);
    printf("\ntrigger_type = %d", trigger_type);
    printf("\ntrigger_state = %d", trigger_state);
    printf("\ntrigger_id = %s", trigger_id);
    printf("\ntrigger_begin_date = %d/%d/%d:%d:%d:%d", trigger_begin_date.day, trigger_begin_date.month, trigger_begin_date.year, trigger_begin_date.hour, trigger_begin_date.minute, trigger_begin_date.secs);
    printf("\ntrigger_end_date = %d/%d/%d:%d:%d:%d", trigger_end_date.day, trigger_end_date.month, trigger_end_date.year, trigger_end_date.hour, trigger_end_date.minute, trigger_end_date.secs);
    test_params.on_task_execute_ctr ++;
    return MA_OK;
}

static ma_error_t on_task_execute(ma_client_t *ma_client, const char *product_id, const char *task_type, const ma_task_t *task, void *cb_data) {
    const char *task_id = NULL;
    ma_task_time_t last_run_time;
    ma_task_time_t next_run_time;
    ma_task_time_t curr_time;
    ma_task_t *reply_task = NULL;
    const char *task_name = NULL;
    const char *task_type_ = NULL;
    const char *type = NULL;

    get_localtime(&curr_time);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id((ma_task_t *)task, &task_id));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_name((ma_task_t *)task, &task_name));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_type((ma_task_t *)task, &task_type_));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_get_task(g_ma_client, task_id, &reply_task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id(reply_task, &task_id));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_name(reply_task, &task_name));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_type(reply_task, &task_type_));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_last_run_time(reply_task, &last_run_time));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_next_run_time(reply_task, &next_run_time));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_type(reply_task, &type));

    printf("\n.................. <%d-%d-%d:%d:%d:%d>", curr_time.day, curr_time.month, curr_time.year, curr_time.hour, curr_time.minute, curr_time.secs);
    printf("\nExecuting Task id %s...\n", task_id);
    printf("\nLast Run Time - (%d/%d/%d:%d:%d:%d)", last_run_time.day, last_run_time.month, last_run_time.year,
                                            last_run_time.hour, last_run_time.minute, last_run_time.secs);
    printf("\nNext Run Time - (%d/%d/%d:%d:%d:%d)", next_run_time.day, next_run_time.month, next_run_time.year,
                                            next_run_time.hour, next_run_time.minute, next_run_time.secs);
    printf("\nTask Type = %s", type);
    if(MA_TRUE == test_params.schd_flag) {
        printf("\nForcefully Failing the Task now !!");
        TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_update_task_status(g_ma_client, task_id, MA_TASK_EXEC_FAILED));
    }else TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_update_task_status(g_ma_client, task_id, MA_TASK_EXEC_SUCCESS));

    if(MA_TRUE == test_params.modify_task && !test_params.on_task_execute_ctr) {
        ma_trigger_t *trigger_3 = NULL;
        ma_trigger_run_once_policy_t policy_once = {0};
        ma_task_time_t start_date;
        policy_once.once = MA_TRUE;
        advance_current_date_by_seconds(120, &start_date);
        TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy_once, &trigger_3));
        TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_3, &start_date));
        TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(reply_task, trigger_3));
        TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_alter_task(g_ma_client, reply_task));
    }
    test_params.on_task_execute_ctr ++;
    return MA_OK;
}

static ma_error_t on_task_stop(ma_client_t *ma_client, const char *product_id, const char *task_type, const ma_task_t *task, void *cb_data) {
    const char *task_id = NULL;
    const char *task_name = NULL;
    const char *task_type_ = NULL;
    ma_task_time_t last_run_time;
    ma_task_time_t next_run_time;
    ma_task_t *reply_task = NULL;
    ma_task_time_t curr_time;
    
    get_localtime(&curr_time);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id((ma_task_t *)task, &task_id));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_name((ma_task_t *)task, &task_name));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_type((ma_task_t *)task, &task_type_));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_get_task(g_ma_client, task_id, &reply_task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id(reply_task, &task_id));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_name(reply_task, &task_name));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_type(reply_task, &task_type_));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_last_run_time(reply_task, &last_run_time));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_next_run_time(reply_task, &next_run_time));
    printf("\n.................. <%d/%d/%d:%d:%d:%d>", curr_time.day, curr_time.month, curr_time.year, curr_time.hour, curr_time.minute, curr_time.secs);
    printf("\nStopping Task id %s...\n", task_id);
    printf("\nLast Run Time - (%d/%d/%d:%d:%d:%d)", last_run_time.day, last_run_time.month, last_run_time.year,
                                            last_run_time.hour, last_run_time.minute, last_run_time.secs);
    printf("\nNext Run Time - (%d/%d/%d:%d:%d:%d)", next_run_time.day, next_run_time.month, next_run_time.year,
                                            next_run_time.hour, next_run_time.minute, next_run_time.secs);
    test_params.on_task_stop_ctr ++;
    return MA_OK;
}

static ma_error_t on_task_remove(ma_client_t *ma_client, const char *product_id, const char *task_type, const ma_task_t *task, void *cb_data) {
    const char *task_id = NULL;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id((ma_task_t *)task, &task_id));
    printf("\n..................................");
    printf("\nRemoving Task id %s...\n", task_id);
    return MA_OK;
}

TEST_SETUP(scheduler_client_daily_test) {
	#ifdef MA_WINDOWS
    ma_dispatcher_test_create_registry(SOFTWARE_ID_PATH);
#else
    ma_dispatcher_test_create_registry("EPOAGENT3000");
#endif
	ma_dispatcher_initialize(NULL, &dispatcher);
}

TEST_TEAR_DOWN(scheduler_client_daily_test) {
	ma_dispatcher_deinitialize( dispatcher);
	#ifdef MA_WINDOWS
   ma_dispatcher_tests_delete_registry(SOFTWARE_ID_PATH);
   ma_dispatcher_tests_delete_registry(SOFTWARE_ID_PATH1);
#else
    ma_dispatcher_tests_delete_registry("ma");
    ma_dispatcher_tests_delete_registry("ma1");
#endif
}

TEST(scheduler_client_daily_test, start_service) {
    // let's enable the thread pool for testing. Else it will be tough to test the APIs that are internally using "send and forget" 
	ma_dispatcher_initialize(NULL, &dispatcher);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ut_setup_create("scheduler_client_test", "scheduler.client.test", &g_ut_setup));
    g_ma_client = ma_ut_setup_get_client(g_ut_setup);
    // start the scheduler service
    TEST_ASSERT_MESSAGE(MA_OK == ma_scheduler_service_create(MA_SCHEDULER_SERVICE_NAME_STR, &g_scheduler_service), "scheduler service creation failed");
    //TEST_ASSERT_MESSAGE(MA_OK == ma_service_configure(g_scheduler_service, ma_ut_setup_get_context(g_ut_setup), MA_SERVICE_CONFIG_NEW), "scheduler service configuration failed");
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_service_start(g_scheduler_service));
    //TBD let's enable the thread pool for testing. Else it will be tough to test the APIs that are internally using "send and forget" 
	ma_dispatcher_deinitialize( dispatcher);
}

TEST(scheduler_client_daily_test, get_task_parameters_test) {
    ma_task_t *task_1 = NULL;
    ma_task_t *task_2 = NULL;
    ma_trigger_t *trigger_1 = NULL;
    ma_trigger_t *trigger_2 = NULL;
    ma_trigger_run_once_policy_t policy_once = {0};
    ma_trigger_daily_policy_t policy_daily = {0};
    ma_task_time_t start_date = {0};
    ma_variant_t *app_payload = NULL;
    ma_task_retry_policy_t retry_policy_1 = { 0, 0 };
    ma_task_retry_policy_t retry_policy_2 = { 0, 0 };
    ma_task_repeat_policy_t repeat_policy_1 = { 0, 0 };
    ma_task_repeat_policy_t repeat_policy_2 = { 0, 0 };

	ma_scheduler_task_callbacks_t cb = {&on_task_execute_get, &on_task_stop, &on_task_remove};
    printf("\nStarting Test Suite - <%s>", __FUNCTION__);
    
    policy_once.once = MA_TRUE;
    policy_daily.days_interval = 2;
    retry_policy_1.retry_count = 2;
    retry_policy_1.retry_interval.minute = 1;
    retry_policy_2.retry_count = 3;
    retry_policy_2.retry_interval.minute = 2;

    repeat_policy_1.repeat_interval.hour = 1;
    repeat_policy_1.repeat_interval.minute = 2;
    repeat_policy_1.repeat_until.hour = 10;
    repeat_policy_1.repeat_until.minute = 30;

    repeat_policy_2.repeat_interval.hour = 1;
    repeat_policy_2.repeat_interval.minute = 15;
    repeat_policy_2.repeat_until.hour = 12;
    repeat_policy_2.repeat_until.minute = 20;

    scheduler_client_daily_test_reset_counters();

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy_once, &trigger_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&policy_daily, &trigger_2));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_create("ma.runonce.1001", &task_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_name(task_1, "Task 1"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_type(task_1, "Run Once"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_create("ma.daily.2001", &task_2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_name(task_2, "Task 2"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_type(task_2, "Daily"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_1, trigger_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_2, trigger_2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_string(__FUNCTION__, strlen(__FUNCTION__), &app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_app_payload(task_1, app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_app_payload(task_2, app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_creator_id(task_1, __FUNCTION__));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_creator_id(task_2, __FUNCTION__));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time_limit(task_1, 1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time_limit(task_2, 2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_retry_policy(task_1, retry_policy_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_retry_policy(task_2, retry_policy_2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_repeat_policy(task_1, repeat_policy_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_repeat_policy(task_2, repeat_policy_2));

    advance_current_date_by_seconds(30, &start_date);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_2, &start_date));
    advance_current_date_by_seconds(120, &start_date);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_1, &start_date));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_register_task_callbacks(g_ma_client, "ZZZ", "Add", &cb, NULL));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_add_task(g_ma_client, task_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_add_task(g_ma_client, task_2));
    
    // wait for the published messages
    while(test_params.on_task_execute_ctr < 2);
         
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_unregister_task_callbacks(g_ma_client, "ZZZ", "Add"));
    printf("\nStopping Test Suite - <%s>", __FUNCTION__);
}

TEST(scheduler_client_daily_test, trigger_multiple_trigger_combination_test) {
    ma_task_t *task_1 = NULL;
    ma_trigger_t *trigger_1 = NULL;
    ma_trigger_t *trigger_2 = NULL;
    ma_trigger_t *trigger_3 = NULL;
    ma_trigger_run_once_policy_t policy_once = {0};
    ma_trigger_daily_policy_t policy_daily = {0};
    ma_trigger_weekly_policy_t policy_weekly = {0};
    ma_task_time_t start_date = {0};
    ma_variant_t *app_payload = NULL;
    ma_scheduler_task_callbacks_t cb = {&on_task_execute, &on_task_stop, &on_task_remove};
    printf("\nStarting Test Suite - <%s>", __FUNCTION__);
    
    policy_once.once = MA_TRUE;
    policy_daily.days_interval = 4;
    policy_weekly.days_of_the_week = WEDNESDAY;
    policy_weekly.weeks_interval = 2;
    scheduler_client_daily_test_reset_counters();

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy_once, &trigger_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&policy_daily, &trigger_2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_weekly(&policy_weekly, &trigger_3));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_create("ma.daily.1001", &task_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_name(task_1, "Task 1"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_type(task_1, "Add"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_1, trigger_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_1, trigger_2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_1, trigger_3));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_string(__FUNCTION__, strlen(__FUNCTION__), &app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_app_payload(task_1, app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_creator_id(task_1, __FUNCTION__));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time_limit(task_1, 1));

    advance_current_date_by_seconds(30, &start_date);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_1, &start_date));
    advance_current_date_by_seconds(120, &start_date);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_2, &start_date));
    advance_current_date_by_seconds(200, &start_date);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_3, &start_date));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_register_task_callbacks(g_ma_client, "ZZZ", "Add", &cb, NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_add_task(g_ma_client, task_1));
    
    // wait for the published messages
    while(test_params.on_task_execute_ctr < 3);
         
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_unregister_task_callbacks(g_ma_client, "ZZZ", "Add"));
    printf("\nStopping Test Suite - <%s>", __FUNCTION__);
}

static ma_error_t simple_task_execute(ma_client_t *ma_client, const char *product_id, const char *task_type, const ma_task_t *task, void *cb_data) {
    const char *task_id = NULL;

    (void)ma_task_get_id((ma_task_t*)task, &task_id);
    printf("task id(%s)..on execute\n", task_id);test_params.on_task_execute_ctr = 1;
    return MA_OK;
}

static ma_error_t simple_task_remove(ma_client_t *ma_client, const char *product_id, const char *task_type, ma_task_t *task, void *cb_data) {
    const char *task_id = NULL;

    (void)ma_task_get_id((ma_task_t*)task, &task_id);
    printf("task id(%s)..on remove\n", task_id);test_params.on_task_execute_ctr = 4;
    return MA_OK;
}

static ma_error_t simple_task_stop(ma_client_t *ma_client, const char *product_id, const char *task_type, const ma_task_t *task, void *cb_data) {
    const char *task_id = NULL;

    (void)ma_task_get_id((ma_task_t*)task, &task_id);
    printf("task id(%s)..on stop\n", task_id); test_params.on_task_execute_ctr = 2;
    return MA_OK;
}

static ma_error_t simple_task_update_status(ma_client_t *ma_client, const char *product_id, const char *task_type, ma_task_t *task, void *cb_data) {
    const char *task_id = NULL;

    (void)ma_task_get_id((ma_task_t*)task, &task_id);
    printf("task id(%s)..on update\n", task_id);test_params.on_task_execute_ctr = 3;
    if(MA_OK == ma_scheduler_update_task_status(g_ma_client, task_id, MA_TASK_EXEC_SUCCESS)) {
        printf("task id(%s) update successfully\n", task_id);
    }
    return MA_OK;
}

TEST(scheduler_client_daily_test, simple_add_trigger_test) {
    ma_task_t *task_1 = NULL;
    ma_trigger_t *run_now = NULL;
    ma_trigger_run_now_policy_t policy = {0};
    const char *product = "simple_task";
    ma_task_randomize_policy_t random_policy = {{0}};
    ma_scheduler_task_callbacks_t cb = {&simple_task_execute, &simple_task_stop, &simple_task_remove, &simple_task_update_status};

    srand((unsigned int)time(NULL));
    random_policy.random_interval.minute = 7;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_now(&policy, &run_now));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_create("ma.runnow.1001", &task_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_name(task_1, "Task 1"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_type(task_1, "Add"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_software_id(task_1, product));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_1, run_now));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time_limit(task_1, 1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_creator_id(task_1, product));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_randomize_policy(task_1, random_policy));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_register_task_callbacks(g_ma_client, product, "Add", &cb, NULL));
	//dispatcher_tests comment
    //TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_add_task(g_ma_client, task_1));
    
    // wait for the published messages
    while(test_params.on_task_execute_ctr < 4);
    
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_unregister_task_callbacks(g_ma_client, product, "Add"));
    printf("\nStopping simple task test Suite - <%s>", __FUNCTION__);

}

TEST(scheduler_client_daily_test, modify_task_single_task_multiple_trigger_test) {
    ma_task_t *task_1 = NULL;
    ma_trigger_t *trigger_1 = NULL;
    ma_trigger_t *trigger_2 = NULL;
    ma_trigger_daily_policy_t policy = {0};
    ma_task_time_t start_date = {0};
    ma_variant_t *app_payload = NULL;
    const char *creator_id = NULL;
	ma_scheduler_task_callbacks_t cb = {&on_task_execute, &on_task_stop, &on_task_remove};

    printf("\nStarting Test Suite - <%s>", __FUNCTION__);
    
    policy.days_interval = 4;
    scheduler_client_daily_test_reset_counters();
    test_params.modify_task = MA_TRUE;

    advance_current_date_by_seconds(10, &start_date);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&policy, &trigger_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&policy, &trigger_2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_1, &start_date));
    advance_current_date_by_seconds(240, &start_date);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_2, &start_date));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_create("ma.daily.1001", &task_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_name(task_1, "Task 1"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_type(task_1, "Add"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_1, trigger_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_1, trigger_2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_string(__FUNCTION__, strlen(__FUNCTION__), &app_payload));
	//dispatcher_tests comment
    //TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_app_payload(task_1, app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_creator_id(task_1, __FUNCTION__));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_creator_id(task_1, &creator_id));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time_limit(task_1, 1));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_register_task_callbacks(g_ma_client, "ZZZ", "Add", &cb, NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_add_task(g_ma_client, task_1));
    
    // wait for the published messages
    while(test_params.on_task_execute_ctr < 3);
         
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_unregister_task_callbacks(g_ma_client, "ZZZ", "Add"));
    printf("\nStopping Test Suite - <%s>", __FUNCTION__);
}

TEST(scheduler_client_daily_test, remove_task_multiple_task_single_trigger_test) {
    ma_task_t *task_1 = NULL;
    ma_task_t *task_2 = NULL;
    ma_task_t *task_3 = NULL;
    ma_trigger_t *trigger_1 = NULL;
    ma_trigger_t *trigger_2 = NULL;
    ma_trigger_t *trigger_3 = NULL;
    ma_trigger_daily_policy_t policy = {0};
    ma_task_time_t start_date = {0};
    ma_variant_t *app_payload = NULL;

    ma_scheduler_task_callbacks_t cb = {&on_task_execute, &on_task_stop, &on_task_remove};
    printf("\nStarting Test Suite - <%s>", __FUNCTION__);
    
    policy.days_interval = 4;

    scheduler_client_daily_test_reset_counters();

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&policy, &trigger_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&policy, &trigger_2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&policy, &trigger_3));
    advance_current_date_by_seconds(20, &start_date);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_1, &start_date));
    advance_current_date_by_seconds(30, &start_date);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_2, &start_date));
    advance_current_date_by_seconds(40, &start_date);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_3, &start_date));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_create("ma.daily.1001", &task_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_name(task_1, "Task 1"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_type(task_1, "Add"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_create("ma.daily.1002", &task_2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_name(task_2, "Task 2"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_type(task_2, "Add"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_create("ma.daily.1003", &task_3));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_name(task_3, "Task 3"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_type(task_3, "Add"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_1, trigger_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_2, trigger_2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_3, trigger_3));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_string(__FUNCTION__, strlen(__FUNCTION__), &app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_app_payload(task_1, app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_app_payload(task_2, app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_app_payload(task_3, app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_creator_id(task_1, __FUNCTION__));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_creator_id(task_2, __FUNCTION__));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_creator_id(task_3, __FUNCTION__));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time_limit(task_1, 1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time_limit(task_2, 1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time_limit(task_3, 1));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_register_task_callbacks(g_ma_client, "ZZZ", "Add", &cb, NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_add_task(g_ma_client, task_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_add_task(g_ma_client, task_2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_add_task(g_ma_client, task_3));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_remove_task(g_ma_client, "ma.daily.1002"));
    
    // wait for the published messages
    while(test_params.on_task_execute_ctr < 2) {
    }
         
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_unregister_task_callbacks(g_ma_client, "ZZZ", "Add"));
    printf("\nStopping Test Suite - <%s>", __FUNCTION__);
}

TEST(scheduler_client_daily_test, add_task_single_trigger_retry_test) {
    ma_task_t *task = NULL;
    ma_trigger_daily_policy_t policy = {0};
    ma_task_retry_policy_t retry_policy = {0};
    ma_trigger_t *trigger = NULL;
    ma_task_time_t start_date = {0};
    ma_variant_t *app_payload = NULL;
    ma_scheduler_task_callbacks_t cb = {&on_task_execute, &on_task_stop, &on_task_remove};
    printf("\nStarting Test Suite - <%s>", __FUNCTION__);
    
    policy.days_interval = 2;

    retry_policy.retry_count = 2;
    retry_policy.retry_interval.minute = 1;
    scheduler_client_daily_test_reset_counters();
    test_params.schd_flag = MA_TRUE;

    advance_current_date_by_seconds(30, &start_date);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&policy, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger, &start_date));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_create("ma.daily.1001", &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_name(task, "Task 1"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_type(task, "Add"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_string(__FUNCTION__, strlen(__FUNCTION__), &app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_app_payload(task, app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_creator_id(task, __FUNCTION__));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_retry_policy(task, retry_policy));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time_limit(task, 1));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_register_task_callbacks(g_ma_client, "ZZZ", "Add", &cb, NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_add_task(g_ma_client, task));
    
    // wait for the published messages
    while(test_params.on_task_execute_ctr < 3);
         
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_unregister_task_callbacks(g_ma_client, "ZZZ", "Add"));
    printf("\nStopping Test Suite - <%s>", __FUNCTION__);
}

TEST(scheduler_client_daily_test, multiple_triggers_intervals_test) {
    ma_task_t *task_1 = NULL;
    ma_task_t *task_2 = NULL;
    ma_trigger_daily_policy_t policy = {0};
    ma_task_repeat_policy_t repeat_policy;
    ma_trigger_t *trigger_1 = NULL;
    ma_trigger_t *trigger_2 = NULL;
    ma_task_time_t start_date = {0};
    ma_variant_t *app_payload = NULL;
    ma_scheduler_task_callbacks_t cb = {&on_task_execute, &on_task_stop, &on_task_remove};
    printf("\nStarting Test Suite - <%s>", __FUNCTION__);
    
    policy.days_interval = 2;
    repeat_policy.repeat_interval.hour = 0;
    repeat_policy.repeat_interval.minute = 1;
    repeat_policy.repeat_until.hour = 0;
    repeat_policy.repeat_until.minute = 6;

    scheduler_client_daily_test_reset_counters();

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&policy, &trigger_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&policy, &trigger_2));
    advance_current_date_by_seconds(30, &start_date);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_1, &start_date));
    advance_current_date_by_seconds(120, &start_date);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_2, &start_date));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_create("ma.daily.1001", &task_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_name(task_1, "Task 1"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_type(task_1, "Add"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_create("ma.daily.2001", &task_2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_name(task_2, "Task 1"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_type(task_2, "Add"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_1, trigger_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_2, trigger_2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_string(__FUNCTION__, strlen(__FUNCTION__), &app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_app_payload(task_1, app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_app_payload(task_2, app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_creator_id(task_1, __FUNCTION__));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_creator_id(task_2, __FUNCTION__));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time_limit(task_1, 1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time_limit(task_2, 1));
    ma_task_set_repeat_policy(task_2, repeat_policy);

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_register_task_callbacks(g_ma_client, "ZZZ", "Add", &cb, NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_add_task(g_ma_client, task_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_add_task(g_ma_client, task_2));
    
    // wait for the published messages
    while(test_params.on_task_execute_ctr < 5);
         
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_unregister_task_callbacks(g_ma_client, "ZZZ", "Add"));
    printf("\nStopping Test Suite - <%s>", __FUNCTION__);
}

TEST(scheduler_client_daily_test, stop_service) {
   //TEST_ASSERT_EQUAL_INT(MA_OK, ma_service_stop(g_scheduler_service));
    ma_service_release(g_scheduler_service);
    ma_ut_setup_release(g_ut_setup);
}

TEST(scheduler_client_daily_test, task_set_get_settings) {
    ma_task_t *task = NULL;
    ma_variant_t *value = NULL;
    ma_variant_t *ret_value = NULL;
    ma_vartype_t ret_value_type = MA_VARTYPE_FLOAT;

    TEST_ASSERT_EQUAL_INT(MA_OK,ma_variant_create_from_string("1", strlen("1"), &value));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_create("1", &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_name(task, "Task 1"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_type(task, "test"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_setting(task, "section", "key", value));
    ma_variant_release(value);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_setting(task, "section", "key", &ret_value));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_get_type(ret_value, &ret_value_type));
    TEST_ASSERT_EQUAL_INT(MA_VARTYPE_STRING, ret_value_type);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(ret_value));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_release(task));
}


//#endif
