#include "unity.h"
#include "unity_fixture.h"

#include <uv.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "ma/ma_common.h"
#include "ma/ma_message.h"
//#include "ma/ma_msgbus.h"
#include "ma/ma_variant.h"
#include "ma/property/ma_property.h"
#include "ma/property/ma_property_bag.h"
#include "ma/ma_dispatcher.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/core/msgbus/ma_message_header.h"
#include "ma/logger/ma_file_logger.h"
#include "ma/ma_errors.h"

#include "ma/event/ma_event_client.h"
#include "ma/internal/utils/xml/ma_xml.h"
#include "ma_client_ut_setup.h"



#include "dispatcher_tests_utils.h"

#define DISPATCHER_PROUDUCT_ID "ma.dispatcher.test"

#ifdef MA_WINDOWS
# include <crtdbg.h>
#include <Windows.h>
# define MA_TRACKLEAKS _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF)
#define SOFTWARE_ID_PATH "SOFTWARE\\McAfee\\Agent\\Applications\\EPOAGENT3000"
#define SOFTWARE_ID_PATH1 "SOFTWARE\\McAfee\\Agent\\Applications\\EPOAGENT3000"
#define MA_DISPATCHER_PRODUCT_INSTAL_PATH "install_path"
#define MA_DISPATCHER_RSDK_PATH "RuntimeSdkPath"
#else
# define MA_TRACKLEAKS ((void *)0)
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#define SOFTWARE_ID_PATH "/etc/ma.d"
#endif

// Global objects
static ma_client_t *g_ma_ev_client = NULL;
static ma_msgbus_t *g_ev_msgbus = NULL;
static ma_ut_client_setup_t *g_ev_ut_setup = NULL;

static ma_msgbus_server_t *ev_server = NULL;
static ma_dispatcher_t *ev_dispatcher = NULL;
static const char service_name[] = "ma.service.event";
static const char product_id[] = "ma.event.client.test";


// Mutex variables
static ma_uint8_t done = 0;
static uv_cond_t cond;
static uv_mutex_t mux;

// Server callback function
static ma_error_t respond_to_event_client(ma_msgbus_server_t *server, ma_message_t *request_payload, void *cb_data, ma_msgbus_client_request_t *c_request);

// Notification callback function
static ma_error_t receive_published_custom_event(ma_client_t *ma_client, const char *event_product_id, ma_custom_event_t *event, void *cb_data);

// Callbacks bundle for registration
static struct ma_event_callbacks_s callbacks_bundle = { NULL, receive_published_custom_event };

TEST_GROUP_RUNNER(dispatcher_event_test_group)
{
    do {RUN_TEST_CASE(ma_event_client_custom_event_tests, ma_custom_event_create_node_by_node_test)} while (0);
   // do {RUN_TEST_CASE(ma_event_client_custom_event_tests, ma_custom_event_create_from_buffer_test)} while (0);
}



TEST_SETUP(ma_event_client_custom_event_tests) {
#ifdef MA_WINDOWS
    ma_dispatcher_test_create_registry(SOFTWARE_ID_PATH);
#else
    ma_dispatcher_test_create_registry("EPOAGENT3000");
#endif
	ma_dispatcher_initialize(NULL,&ev_dispatcher);
	uv_mutex_init(&mux);
    uv_cond_init(&cond);

	//if(g_ut_setup == NULL)
		ma_ut_setup_create("event_client.event_bag.tests", product_id, &g_ev_ut_setup);
	
	g_ev_msgbus = ma_ut_setup_get_msgbus(g_ev_ut_setup);
	g_ma_ev_client = ma_ut_setup_get_client(g_ev_ut_setup);

    ma_msgbus_server_create(g_ev_msgbus, service_name, &ev_server);
    ma_msgbus_server_start(ev_server, respond_to_event_client, NULL);
}

TEST_TEAR_DOWN(ma_event_client_custom_event_tests) {

	 ma_msgbus_server_stop(ev_server);
    ma_msgbus_server_release(ev_server);
    ma_ut_setup_release(g_ev_ut_setup);

    uv_cond_destroy(&cond);
    uv_mutex_destroy(&mux);
	ma_dispatcher_deinitialize(ev_dispatcher);
	#ifdef MA_WINDOWS
   ma_dispatcher_tests_delete_registry(SOFTWARE_ID_PATH);
   ma_dispatcher_tests_delete_registry(SOFTWARE_ID_PATH1);
#else
    ma_dispatcher_tests_delete_registry("ma");
    ma_dispatcher_tests_delete_registry("ma1");
#endif

	 
}

/******************************************************************************
Callback functions and support functions.
******************************************************************************/
static void wait_until_done()
{
    uv_mutex_lock(&mux);
    while (done)
    {
        uv_cond_wait(&cond,&mux);
    }

    uv_mutex_unlock(&mux);
}

static void set_done()
{
    uv_mutex_lock(&mux);
    done--;
    uv_cond_signal(&cond);
    uv_mutex_unlock(&mux);
}


/**************************************************************************
1) Creating the unit test context.
2) Retrieving underlying message bus instance.
3) Creating and starting message bus service (Event Service) using the
   message bus instance.
**************************************************************************/


/*
Unit test performs following functionality -
1) Creates and sends an Event Message consisting of the below mentioned content on the
   message bus.
2) Creates a server to accept the message and the verifies that all fields are successfully
   parsed from the message.
3) Note that the server is configured for the OUTPROC communication, but this test, creates
   client and server within the same process (INRPROC) to verify the event client functionality.

<BehaviourBlockEvent>
    <MachineInfo>
        <MachineName>AMTSHARE</MachineName>
        <AgentGUID>{4b896b5c-8e4f-11e3-05dc-001ec92d86bc}</AgentGUID>
        <IPAddress>10.213.254.82</IPAddress>
        <OSName>Windows 2008</OSName>
        <UserName>NT AUTHORITY\SYSTEM</UserName>
        <TimeZoneBias>-330</TimeZoneBias>
        <RawMACAddress>001ec92d86bc</RawMACAddress>
    </MachineInfo>
    <ScannerSoftware ProductName="VirusScan Enterprise" ProductVersion="8.8" ProductFamily="TVD">
        <EngineVersion>0</EngineVersion>
        <DATVersion>123</DATVersion>
        <ScannerType>OAS</ScannerType>
        <TaskName>OAS</TaskName>
        <ProductFamily>TVD</ProductFamily>
        <ProductName>VirusScan Enterprise</ProductName>
        <ProductVersion>8.8</ProductVersion>
        <BlockedBehaviourInfo1>
            <EventID>1092</EventID>
            <Severity>2</Severity>
            <GMTTime>2014-02-11T11:56:47</GMTTime>
            <UTCTime>2014-02-11T06:26:47</UTCTime>
            <RuleName>
                Common Standard Protection:Prevent modification of McAfee Common Management Agent files and settings
            </RuleName>
            <ProcessName>C:\Program Files\McAfee\Agent\macompatsvc.exe</ProcessName>
            <FileName>\REGISTRY\MACHINE\SOFTWARE\McAfee\Agent\lpc\lpc_throb</FileName>
            <Source></Source>
            <ActionsBlocked>3</ActionsBlocked>
            <szActionsBlocked>Create</szActionsBlocked>
        </BlockedBehaviourInfo1>
        <BlockedBehaviourInfo2>
            <EventID>2345</EventID>
            <Severity>1</Severity>
            <GMTTime>2014-02-14T11:54:12</GMTTime>
            <UTCTime>2014-02-14T06:23:34</UTCTime>
            <RuleName>
                Special Protection:Prevent modification of any program files
            </RuleName>
            <ProcessName>C:\Program Files\McAfee\Agent\macompatsvc.exe</ProcessName>
            <Source>No Source</Source>
            <szActionsBlocked>Modify</szActionsBlocked>
        </BlockedBehaviourInfo2>
    </ScannerSoftware>
</BehaviourBlockEvent>
*/

/******************************************************************************
Call back function to be executed when the Event Service receives a custom
event from event client. This function verifies that all parameters are correctly
put into the custom event buffer.
******************************************************************************/
static ma_error_t respond_to_event_client(ma_msgbus_server_t *server,
                                          ma_message_t *request_payload,
                                          void *cb_data,
                                          ma_msgbus_client_request_t *c_request)
{
    ma_variant_t *custom_event_variant_obj = NULL;

    // Handling the message payload
    if(request_payload == NULL)
    {
        return MA_ERROR_UNEXPECTED;
    }

    /**************************************************************************
    Verifying request type
    **************************************************************************/
    {
        const char *string_val = NULL;
        TEST_ASSERT(ma_message_get_property(request_payload, "prop.key.msg_type", &string_val) == MA_OK);
        TEST_ASSERT(0 == strcmp("prop.value.req.post_custom_event", string_val));
    }

    /**************************************************************************
    Verifying product ID
    **************************************************************************/
    {
        const char *string_val = NULL;
        TEST_ASSERT(ma_message_get_property(request_payload, "prop.key.software_id", &string_val) == MA_OK);
        TEST_ASSERT(0 == strcmp(product_id, string_val));
    }

    /**************************************************************************
    Retrieving buffer -> Reading the buffer as XML -> Verifying all the elements
    **************************************************************************/
    {
        const char *string_val = NULL;
        ma_xml_t *my_xml = NULL;
        ma_xml_node_t *root_node = NULL;
        ma_xml_node_t *first_node = NULL;

        // Retrieving XML buffer from the the message payload
        {
            ma_variant_t *my_variant_ptr = NULL;
            ma_buffer_t *my_buffer = NULL;
            size_t string_size = 0; // size is not used further in the test, therefore, it is okay to declare it local

            TEST_ASSERT(MA_OK == ma_message_get_payload(request_payload, &my_variant_ptr));
            TEST_ASSERT(MA_OK == ma_variant_get_string_buffer(my_variant_ptr, &my_buffer));
            TEST_ASSERT(MA_OK == ma_buffer_get_string(my_buffer, &string_val, &string_size));
            TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
            TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));
        }

        // Creating XML and loading buffer
        TEST_ASSERT(MA_OK == ma_xml_create(&my_xml));
        TEST_ASSERT(MA_OK == ma_xml_load_from_buffer(my_xml, string_val, strlen(string_val)));
        TEST_ASSERT_NOT_NULL(root_node = ma_xml_get_node(my_xml));

        // Verifying Node fields and attributes
        TEST_ASSERT_NOT_NULL(first_node = ma_xml_node_search_by_path(root_node, "BehaviourBlockEvent/ScannerSoftware"));
        TEST_ASSERT(0 == strcmp("VirusScan Enterprise", ma_xml_node_attribute_get(first_node, "ProductName")));

        TEST_ASSERT_NOT_NULL(first_node = ma_xml_node_search_by_path(root_node, "BehaviourBlockEvent/ScannerSoftware"));
        TEST_ASSERT(0 == strcmp("8.8", ma_xml_node_attribute_get(first_node, "ProductVersion")));

        TEST_ASSERT_NOT_NULL(first_node = ma_xml_node_search_by_path(root_node, "BehaviourBlockEvent/ScannerSoftware"));
        TEST_ASSERT(0 == strcmp("TVD", ma_xml_node_attribute_get(first_node, "ProductFamily")));

        TEST_ASSERT_NOT_NULL(first_node = ma_xml_node_search_by_path(root_node, "BehaviourBlockEvent/ScannerSoftware/EngineVersion"));
        TEST_ASSERT(0 == strcmp("0", ma_xml_node_get_data(first_node)));

        TEST_ASSERT_NOT_NULL(first_node = ma_xml_node_search_by_path(root_node, "BehaviourBlockEvent/ScannerSoftware/DATVersion"));
        TEST_ASSERT(0 == strcmp("123", ma_xml_node_get_data(first_node)));

        TEST_ASSERT_NOT_NULL(first_node = ma_xml_node_search_by_path(root_node, "BehaviourBlockEvent/ScannerSoftware/ScannerType"));
        TEST_ASSERT(0 == strcmp("OAS", ma_xml_node_get_data(first_node)));

        TEST_ASSERT_NOT_NULL(first_node = ma_xml_node_search_by_path(root_node, "BehaviourBlockEvent/ScannerSoftware/TaskName"));
        TEST_ASSERT(0 == strcmp("OAS", ma_xml_node_get_data(first_node)));

        TEST_ASSERT_NOT_NULL(first_node = ma_xml_node_search_by_path(root_node, "BehaviourBlockEvent/ScannerSoftware/ProductFamily"));
        TEST_ASSERT(0 == strcmp("TVD", ma_xml_node_get_data(first_node)));

        TEST_ASSERT_NOT_NULL(first_node = ma_xml_node_search_by_path(root_node, "BehaviourBlockEvent/ScannerSoftware/ProductName"));
        TEST_ASSERT(0 == strcmp("VirusScan Enterprise", ma_xml_node_get_data(first_node)));

        TEST_ASSERT_NOT_NULL(first_node = ma_xml_node_search_by_path(root_node, "BehaviourBlockEvent/ScannerSoftware/ProductVersion"));
        TEST_ASSERT(0 == strcmp("8.8", ma_xml_node_get_data(first_node)));

        // Verifying Event 1 fields (Exact Values of GMTTime and UTCTime cannot be verified, therefore, just verify that it is not NULL)
        TEST_ASSERT_NOT_NULL(first_node = ma_xml_node_search_by_path(root_node, "BehaviourBlockEvent/ScannerSoftware/BlockedBehaviourInfo1/EventID"));
        TEST_ASSERT(0 == strcmp("1092", ma_xml_node_get_data(first_node)));

        TEST_ASSERT_NOT_NULL(first_node = ma_xml_node_search_by_path(root_node, "BehaviourBlockEvent/ScannerSoftware/BlockedBehaviourInfo1/Severity"));
        TEST_ASSERT(0 == strcmp("2", ma_xml_node_get_data(first_node)));

        TEST_ASSERT_NOT_NULL(ma_xml_node_search_by_path(root_node, "BehaviourBlockEvent/ScannerSoftware/BlockedBehaviourInfo1/GMTTime"));

        TEST_ASSERT_NOT_NULL(ma_xml_node_search_by_path(root_node, "BehaviourBlockEvent/ScannerSoftware/BlockedBehaviourInfo1/UTCTime"));

        TEST_ASSERT_NOT_NULL(first_node = ma_xml_node_search_by_path(root_node, "BehaviourBlockEvent/ScannerSoftware/BlockedBehaviourInfo1/RuleName"));
        TEST_ASSERT(0 == strcmp("Common Standard Protection:Prevent modification of McAfee Common Management Agent files and settings", ma_xml_node_get_data(first_node)));

        TEST_ASSERT_NOT_NULL(first_node = ma_xml_node_search_by_path(root_node, "BehaviourBlockEvent/ScannerSoftware/BlockedBehaviourInfo1/ProcessName"));
        TEST_ASSERT(0 == strcmp("C:\\Program Files\\McAfee\\Agent\\macompatsvc.exe", ma_xml_node_get_data(first_node)));

        TEST_ASSERT_NOT_NULL(first_node = ma_xml_node_search_by_path(root_node, "BehaviourBlockEvent/ScannerSoftware/BlockedBehaviourInfo1/FileName"));
        TEST_ASSERT(0 == strcmp("\\REGISTRY\\MACHINE\\SOFTWARE\\McAfee\\Agent\\lpc\\lpc_throb", ma_xml_node_get_data(first_node)));

        TEST_ASSERT_NOT_NULL(first_node = ma_xml_node_search_by_path(root_node, "BehaviourBlockEvent/ScannerSoftware/BlockedBehaviourInfo1/Source"));
        TEST_ASSERT(0 == strcmp("", ma_xml_node_get_data(first_node)));

        TEST_ASSERT_NOT_NULL(first_node = ma_xml_node_search_by_path(root_node, "BehaviourBlockEvent/ScannerSoftware/BlockedBehaviourInfo1/ActionsBlocked"));
        TEST_ASSERT(0 == strcmp("3", ma_xml_node_get_data(first_node)));

        TEST_ASSERT_NOT_NULL(first_node = ma_xml_node_search_by_path(root_node, "BehaviourBlockEvent/ScannerSoftware/BlockedBehaviourInfo1/szActionsBlocked"));
        TEST_ASSERT(0 == strcmp("Create", ma_xml_node_get_data(first_node)));

        // Verifying Event 2 fields (Exact Values of GMTTime and UTCTime cannot be verified, therefore, just verify that it is not NULL)
        TEST_ASSERT_NOT_NULL(first_node = ma_xml_node_search_by_path(root_node, "BehaviourBlockEvent/ScannerSoftware/BlockedBehaviourInfo2/EventID"));
        TEST_ASSERT(0 == strcmp("2345", ma_xml_node_get_data(first_node)));

        TEST_ASSERT_NOT_NULL(first_node = ma_xml_node_search_by_path(root_node, "BehaviourBlockEvent/ScannerSoftware/BlockedBehaviourInfo2/Severity"));
        TEST_ASSERT(0 == strcmp("1", ma_xml_node_get_data(first_node)));

        TEST_ASSERT_NOT_NULL(ma_xml_node_search_by_path(root_node, "BehaviourBlockEvent/ScannerSoftware/BlockedBehaviourInfo2/GMTTime"));

        TEST_ASSERT_NOT_NULL(ma_xml_node_search_by_path(root_node, "BehaviourBlockEvent/ScannerSoftware/BlockedBehaviourInfo2/UTCTime"));

        TEST_ASSERT_NOT_NULL(first_node = ma_xml_node_search_by_path(root_node, "BehaviourBlockEvent/ScannerSoftware/BlockedBehaviourInfo2/RuleName"));
        TEST_ASSERT(0 == strcmp("Special Protection:Prevent modification of any program files", ma_xml_node_get_data(first_node)));

        TEST_ASSERT_NOT_NULL(first_node = ma_xml_node_search_by_path(root_node, "BehaviourBlockEvent/ScannerSoftware/BlockedBehaviourInfo2/ProcessName"));
        TEST_ASSERT(0 == strcmp("C:\\Program Files\\McAfee\\Agent\\macompatsvc.exe", ma_xml_node_get_data(first_node)));

        // Field doesn't exist
        TEST_ASSERT_NULL(ma_xml_node_search_by_path(root_node, "BehaviourBlockEvent/ScannerSoftware/BlockedBehaviourInfo2/FileName"));

        TEST_ASSERT_NOT_NULL(first_node = ma_xml_node_search_by_path(root_node, "BehaviourBlockEvent/ScannerSoftware/BlockedBehaviourInfo2/Source"));
        TEST_ASSERT(0 == strcmp("No Source", ma_xml_node_get_data(first_node)));

        // Field doesn't exist
        TEST_ASSERT_NULL(ma_xml_node_search_by_path(root_node, "BehaviourBlockEvent/ScannerSoftware/BlockedBehaviourInfo2/ActionsBlocked"));

        TEST_ASSERT_NOT_NULL(first_node = ma_xml_node_search_by_path(root_node, "BehaviourBlockEvent/ScannerSoftware/BlockedBehaviourInfo2/szActionsBlocked"));
        TEST_ASSERT(0 == strcmp("Modify", ma_xml_node_get_data(first_node)));

		TEST_ASSERT(MA_OK == ma_xml_release(my_xml));
    }

    set_done();

    // In Thread_IO mode, the event client sends a 'send-and-forget' type request to
    // the event service, therefore no response is required.
    return MA_OK;
}


/******************************************************************************
Call back function to be executed when the Event Service receives a custom
event from event client. This function verifies that all parameters are correctly
put into the custom event buffer.
******************************************************************************/
static ma_error_t receive_published_custom_event(ma_client_t *ma_client, const char *event_product_id, ma_custom_event_t *event, void *cb_data)
{
    ma_variant_t *custom_event_variant_obj = NULL;

    // Event Bag cannot be NULL
    TEST_ASSERT_NOT_NULL(event);

    // Verifying Product ID
    TEST_ASSERT(0 == strcmp(product_id, event_product_id));

    /**************************************************************************
    Retrieving buffer -> Reading the buffer as XML -> Verifying all the elements
    **************************************************************************/
    {
        ma_xml_node_t *root_node = NULL;
        ma_xml_node_t *first_node = NULL;
        ma_xml_node_t *second_node = NULL;
        ma_xml_node_t *third_node = NULL;
        const char *string_val = NULL;

        // Retrieving root node and verifying that it is not NULL
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_custom_event_get_root_node(NULL, &root_node));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_custom_event_get_root_node(event, NULL));
        TEST_ASSERT(MA_OK == ma_custom_event_get_root_node(event, &root_node));
        TEST_ASSERT_NOT_NULL(root_node);

        //---------------------------------------------------------------------------

        // Verifying Root Node (Product) attributes
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_custom_event_node_search(NULL, "ScannerSoftware", &first_node));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_custom_event_node_search(root_node, NULL, &first_node));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_custom_event_node_search(root_node, "ScannerSoftware", NULL));
        TEST_ASSERT(MA_OK == ma_custom_event_node_search(root_node, "ScannerSoftware", &first_node));

        string_val = NULL;
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_custom_event_node_attribute_get(NULL, "ProductName", &string_val));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_custom_event_node_attribute_get(first_node, NULL, &string_val));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_custom_event_node_attribute_get(first_node, "ProductName", NULL));
        TEST_ASSERT(MA_OK == ma_custom_event_node_attribute_get(first_node, "ProductName", &string_val));
        TEST_ASSERT(0 == strcmp("VirusScan Enterprise", string_val));

        string_val = NULL;
        TEST_ASSERT(MA_OK == ma_custom_event_node_attribute_get(first_node, "ProductVersion", &string_val));
        TEST_ASSERT(0 == strcmp("8.8", string_val));

        string_val = NULL;
        TEST_ASSERT(MA_OK == ma_custom_event_node_attribute_get(first_node, "ProductFamily", &string_val));
        TEST_ASSERT(0 == strcmp("TVD", string_val));

        //---------------------------------------------------------------------------

        // Identifying and verifying the Product Fields (2nd Level)
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_custom_event_get_child_node(NULL, &second_node));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_custom_event_get_child_node(first_node, NULL));
        TEST_ASSERT(MA_OK == ma_custom_event_get_child_node(first_node, &second_node));

        string_val = NULL;
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_custom_event_node_name_get(NULL, &string_val));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_custom_event_node_name_get(second_node, NULL));
        TEST_ASSERT(MA_OK == ma_custom_event_node_name_get(second_node, &string_val));
        TEST_ASSERT(0 == strcmp("EngineVersion", string_val));

        string_val = NULL;
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_custom_event_node_data_get(NULL, &string_val));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_custom_event_node_data_get(second_node, NULL));
        TEST_ASSERT(MA_OK == ma_custom_event_node_data_get(second_node, &string_val));
        TEST_ASSERT(0 == strcmp("0", string_val));

        // No further children
        TEST_ASSERT(MA_ERROR_OBJECTNOTFOUND == ma_custom_event_get_child_node(second_node, &third_node));

        string_val = NULL;
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_custom_event_get_next_node(NULL, &third_node));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_custom_event_get_next_node(second_node, NULL));
        TEST_ASSERT(MA_OK == ma_custom_event_get_next_node(second_node, &third_node));
        TEST_ASSERT(MA_OK == ma_custom_event_node_name_get(third_node, &string_val));
        TEST_ASSERT(0 == strcmp("DATVersion", string_val));
        TEST_ASSERT(MA_OK == ma_custom_event_node_data_get(third_node, &string_val));
        TEST_ASSERT(0 == strcmp("123", string_val));
        second_node = third_node; // Moving pointer ahead to retrieve next element

        string_val = NULL;
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_custom_event_get_next_sibling(NULL, &third_node));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_custom_event_get_next_sibling(second_node, NULL));
        TEST_ASSERT(MA_OK == ma_custom_event_get_next_sibling(second_node, &third_node));
        TEST_ASSERT(MA_OK == ma_custom_event_node_name_get(third_node, &string_val));
        TEST_ASSERT(0 == strcmp("ScannerType", string_val));
        TEST_ASSERT(MA_OK == ma_custom_event_node_data_get(third_node, &string_val));
        TEST_ASSERT(0 == strcmp("OAS", string_val));
        second_node = third_node; // Moving pointer ahead to retrieve next element

        string_val = NULL;
        TEST_ASSERT(MA_OK == ma_custom_event_get_next_sibling(second_node, &third_node));
        TEST_ASSERT(MA_OK == ma_custom_event_node_name_get(third_node, &string_val));
        TEST_ASSERT(0 == strcmp("TaskName", string_val));
        TEST_ASSERT(MA_OK == ma_custom_event_node_data_get(third_node, &string_val));
        TEST_ASSERT(0 == strcmp("OAS", string_val));
        second_node = third_node; // Moving pointer ahead to retrieve next element

        string_val = NULL;
        TEST_ASSERT(MA_OK == ma_custom_event_get_next_sibling(second_node, &third_node));
        TEST_ASSERT(MA_OK == ma_custom_event_node_name_get(third_node, &string_val));
        TEST_ASSERT(0 == strcmp("ProductFamily", string_val));
        TEST_ASSERT(MA_OK == ma_custom_event_node_data_get(third_node, &string_val));
        TEST_ASSERT(0 == strcmp("TVD", string_val));
        second_node = third_node; // Moving pointer ahead to retrieve next element

        string_val = NULL;
        TEST_ASSERT(MA_OK == ma_custom_event_get_next_sibling(second_node, &third_node));
        TEST_ASSERT(MA_OK == ma_custom_event_node_name_get(third_node, &string_val));
        TEST_ASSERT(0 == strcmp("ProductName", string_val));
        TEST_ASSERT(MA_OK == ma_custom_event_node_data_get(third_node, &string_val));
        TEST_ASSERT(0 == strcmp("VirusScan Enterprise", string_val));
        second_node = third_node; // Moving pointer ahead to retrieve next element

        string_val = NULL;
        TEST_ASSERT(MA_OK == ma_custom_event_get_next_sibling(second_node, &third_node));
        TEST_ASSERT(MA_OK == ma_custom_event_node_name_get(third_node, &string_val));
        TEST_ASSERT(0 == strcmp("ProductVersion", string_val));
        TEST_ASSERT(MA_OK == ma_custom_event_node_data_get(third_node, &string_val));
        TEST_ASSERT(0 == strcmp("8.8", string_val));
        second_node = third_node; // Moving pointer ahead to retrieve next element

        string_val = NULL;
        TEST_ASSERT(MA_OK == ma_custom_event_get_next_sibling(second_node, &third_node));
        TEST_ASSERT(MA_OK == ma_custom_event_node_name_get(third_node, &string_val));
        TEST_ASSERT(0 == strcmp("BlockedBehaviourInfo1", string_val));
        TEST_ASSERT(MA_OK == ma_custom_event_node_data_get(third_node, &string_val));
        TEST_ASSERT(0 == strcmp("", string_val));
        second_node = third_node; // Moving pointer ahead to retrieve next element

        string_val = NULL;
        TEST_ASSERT(MA_OK == ma_custom_event_get_next_sibling(second_node, &third_node));
        TEST_ASSERT(MA_OK == ma_custom_event_node_name_get(third_node, &string_val));
        TEST_ASSERT(0 == strcmp("BlockedBehaviourInfo2", string_val));
        TEST_ASSERT(MA_OK == ma_custom_event_node_data_get(third_node, &string_val));
        TEST_ASSERT(0 == strcmp("", string_val));
        second_node = third_node; // Moving pointer ahead to retrieve next element

        string_val = NULL;

        // No more elements at this level, therefore, ma_custom_event_get_next_sibling
        // should return MA_ERROR_OBJECTNOTFOUND
        TEST_ASSERT(MA_ERROR_OBJECTNOTFOUND == ma_custom_event_get_next_sibling(second_node, &third_node));

        // ---------------------------------------------------------------------------------

        // Verifying Custom Event 1 (search for "BlockedBehaviourInfo1" event)
        TEST_ASSERT(MA_OK == ma_custom_event_node_search(root_node, "BlockedBehaviourInfo1", &first_node));

        // No attributes
        string_val = NULL;
        TEST_ASSERT(MA_ERROR_OBJECTNOTFOUND == ma_custom_event_node_attribute_get(first_node, "ProductName", &string_val));
        TEST_ASSERT_NULL(string_val);

        // Identifying event fields and verifying (2nd Level)
        TEST_ASSERT(MA_OK == ma_custom_event_get_child_node(first_node, &second_node));

        string_val = NULL;
        TEST_ASSERT(MA_OK == ma_custom_event_node_name_get(second_node, &string_val));
        TEST_ASSERT(0 == strcmp("EventID", string_val));

        string_val = NULL;
        TEST_ASSERT(MA_OK == ma_custom_event_node_data_get(second_node, &string_val));
        TEST_ASSERT(0 == strcmp("1092", string_val));

        TEST_ASSERT(MA_ERROR_OBJECTNOTFOUND == ma_custom_event_get_child_node(second_node, &third_node));

        string_val = NULL;
        TEST_ASSERT(MA_OK == ma_custom_event_get_next_node(second_node, &third_node));
        TEST_ASSERT(MA_OK == ma_custom_event_node_name_get(third_node, &string_val));
        TEST_ASSERT(0 == strcmp("Severity", string_val));
        TEST_ASSERT(MA_OK == ma_custom_event_node_data_get(third_node, &string_val));
        TEST_ASSERT(0 == strcmp("2", string_val));
        second_node = third_node; // Moving pointer ahead to retrieve next element

        // Exact Values of GMTTime and UTCTime cannot be verified, therefore, just verify that it is not NULL
        string_val = NULL;
        TEST_ASSERT(MA_OK == ma_custom_event_get_next_sibling(second_node, &third_node));
        TEST_ASSERT(MA_OK == ma_custom_event_node_name_get(third_node, &string_val));
        TEST_ASSERT(0 == strcmp("GMTTime", string_val));
        TEST_ASSERT(MA_OK == ma_custom_event_node_data_get(third_node, &string_val));
        TEST_ASSERT_NOT_NULL(string_val);
        second_node = third_node; // Moving pointer ahead to retrieve next element

        string_val = NULL;
        TEST_ASSERT(MA_OK == ma_custom_event_get_next_sibling(second_node, &third_node));
        TEST_ASSERT(MA_OK == ma_custom_event_node_name_get(third_node, &string_val));
        TEST_ASSERT(0 == strcmp("UTCTime", string_val));
        TEST_ASSERT(MA_OK == ma_custom_event_node_data_get(third_node, &string_val));
        TEST_ASSERT_NOT_NULL(string_val);
        second_node = third_node; // Moving pointer ahead to retrieve next element

        string_val = NULL;
        TEST_ASSERT(MA_OK == ma_custom_event_get_next_sibling(second_node, &third_node));
        TEST_ASSERT(MA_OK == ma_custom_event_node_name_get(third_node, &string_val));
        TEST_ASSERT(0 == strcmp("RuleName", string_val));
        TEST_ASSERT(MA_OK == ma_custom_event_node_data_get(third_node, &string_val));
        TEST_ASSERT(0 == strcmp("Common Standard Protection:Prevent modification of McAfee Common Management Agent files and settings", string_val));
        second_node = third_node; // Moving pointer ahead to retrieve next element

        string_val = NULL;
        TEST_ASSERT(MA_OK == ma_custom_event_get_next_sibling(second_node, &third_node));
        TEST_ASSERT(MA_OK == ma_custom_event_node_name_get(third_node, &string_val));
        TEST_ASSERT(0 == strcmp("ProcessName", string_val));
        TEST_ASSERT(MA_OK == ma_custom_event_node_data_get(third_node, &string_val));
        TEST_ASSERT(0 == strcmp("C:\\Program Files\\McAfee\\Agent\\macompatsvc.exe", string_val));
        second_node = third_node; // Moving pointer ahead to retrieve next element

        string_val = NULL;
        TEST_ASSERT(MA_OK == ma_custom_event_get_next_sibling(second_node, &third_node));
        TEST_ASSERT(MA_OK == ma_custom_event_node_name_get(third_node, &string_val));
        TEST_ASSERT(0 == strcmp("FileName", string_val));
        TEST_ASSERT(MA_OK == ma_custom_event_node_data_get(third_node, &string_val));
        TEST_ASSERT(0 == strcmp("\\REGISTRY\\MACHINE\\SOFTWARE\\McAfee\\Agent\\lpc\\lpc_throb", string_val));
        second_node = third_node; // Moving pointer ahead to retrieve next element

        string_val = NULL;
        TEST_ASSERT(MA_OK == ma_custom_event_get_next_sibling(second_node, &third_node));
        TEST_ASSERT(MA_OK == ma_custom_event_node_name_get(third_node, &string_val));
        TEST_ASSERT(0 == strcmp("Source", string_val));
        TEST_ASSERT(MA_OK == ma_custom_event_node_data_get(third_node, &string_val));
        TEST_ASSERT(0 == strcmp("", string_val));
        second_node = third_node; // Moving pointer ahead to retrieve next element

        string_val = NULL;
        TEST_ASSERT(MA_OK == ma_custom_event_get_next_sibling(second_node, &third_node));
        TEST_ASSERT(MA_OK == ma_custom_event_node_name_get(third_node, &string_val));
        TEST_ASSERT(0 == strcmp("ActionsBlocked", string_val));
        TEST_ASSERT(MA_OK == ma_custom_event_node_data_get(third_node, &string_val));
        TEST_ASSERT(0 == strcmp("3", string_val));
        second_node = third_node; // Moving pointer ahead to retrieve next element

        string_val = NULL;
        TEST_ASSERT(MA_OK == ma_custom_event_get_next_sibling(second_node, &third_node));
        TEST_ASSERT(MA_OK == ma_custom_event_node_name_get(third_node, &string_val));
        TEST_ASSERT(0 == strcmp("szActionsBlocked", string_val));
        TEST_ASSERT(MA_OK == ma_custom_event_node_data_get(third_node, &string_val));
        TEST_ASSERT(0 == strcmp("Create", string_val));
        second_node = third_node; // Moving pointer ahead to retrieve next element

        // ---------------------------------------------------------------------------------

        // Verifying Custom Event 2 (search for "BlockedBehaviourInfo2" event)
        TEST_ASSERT(MA_OK == ma_custom_event_node_search(root_node, "BlockedBehaviourInfo2", &first_node));

        // Identifying event fields and verifying (2nd Level)
        TEST_ASSERT(MA_OK == ma_custom_event_get_child_node(first_node, &second_node));

        string_val = NULL;
        TEST_ASSERT(MA_OK == ma_custom_event_node_name_get(second_node, &string_val));
        TEST_ASSERT(0 == strcmp("EventID", string_val));

        string_val = NULL;
        TEST_ASSERT(MA_OK == ma_custom_event_node_data_get(second_node, &string_val));
        TEST_ASSERT(0 == strcmp("2345", string_val));

        TEST_ASSERT(MA_ERROR_OBJECTNOTFOUND == ma_custom_event_get_child_node(second_node, &third_node));

        string_val = NULL;
        TEST_ASSERT(MA_OK == ma_custom_event_get_next_node(second_node, &third_node));
        TEST_ASSERT(MA_OK == ma_custom_event_node_name_get(third_node, &string_val));
        TEST_ASSERT(0 == strcmp("Severity", string_val));
        TEST_ASSERT(MA_OK == ma_custom_event_node_data_get(third_node, &string_val));
        TEST_ASSERT(0 == strcmp("1", string_val));
        second_node = third_node; // Moving pointer ahead to retrieve next element

        // Exact Values of GMTTime and UTCTime cannot be verified, therefore, just verify that it is not NULL
        string_val = NULL;
        TEST_ASSERT(MA_OK == ma_custom_event_get_next_sibling(second_node, &third_node));
        TEST_ASSERT(MA_OK == ma_custom_event_node_name_get(third_node, &string_val));
        TEST_ASSERT(0 == strcmp("GMTTime", string_val));
        TEST_ASSERT(MA_OK == ma_custom_event_node_data_get(third_node, &string_val));
        TEST_ASSERT_NOT_NULL(string_val);
        second_node = third_node; // Moving pointer ahead to retrieve next element

        string_val = NULL;
        TEST_ASSERT(MA_OK == ma_custom_event_get_next_sibling(second_node, &third_node));
        TEST_ASSERT(MA_OK == ma_custom_event_node_name_get(third_node, &string_val));
        TEST_ASSERT(0 == strcmp("UTCTime", string_val));
        TEST_ASSERT(MA_OK == ma_custom_event_node_data_get(third_node, &string_val));
        TEST_ASSERT_NOT_NULL(string_val);
        second_node = third_node; // Moving pointer ahead to retrieve next element

        string_val = NULL;
        TEST_ASSERT(MA_OK == ma_custom_event_get_next_sibling(second_node, &third_node));
        TEST_ASSERT(MA_OK == ma_custom_event_node_name_get(third_node, &string_val));
        TEST_ASSERT(0 == strcmp("RuleName", string_val));
        TEST_ASSERT(MA_OK == ma_custom_event_node_data_get(third_node, &string_val));
        TEST_ASSERT(0 == strcmp("Special Protection:Prevent modification of any program files", string_val));
        second_node = third_node; // Moving pointer ahead to retrieve next element

        string_val = NULL;
        TEST_ASSERT(MA_OK == ma_custom_event_get_next_sibling(second_node, &third_node));
        TEST_ASSERT(MA_OK == ma_custom_event_node_name_get(third_node, &string_val));
        TEST_ASSERT(0 == strcmp("ProcessName", string_val));
        TEST_ASSERT(MA_OK == ma_custom_event_node_data_get(third_node, &string_val));
        TEST_ASSERT(0 == strcmp("C:\\Program Files\\McAfee\\Agent\\macompatsvc.exe", string_val));
        second_node = third_node; // Moving pointer ahead to retrieve next element

        string_val = NULL;
        TEST_ASSERT(MA_OK == ma_custom_event_get_next_sibling(second_node, &third_node));
        TEST_ASSERT(MA_OK == ma_custom_event_node_name_get(third_node, &string_val));
        TEST_ASSERT(0 == strcmp("Source", string_val));
        TEST_ASSERT(MA_OK == ma_custom_event_node_data_get(third_node, &string_val));
        TEST_ASSERT(0 == strcmp("No Source", string_val));
        second_node = third_node; // Moving pointer ahead to retrieve next element

        string_val = NULL;
        TEST_ASSERT(MA_OK == ma_custom_event_get_next_sibling(second_node, &third_node));
        TEST_ASSERT(MA_OK == ma_custom_event_node_name_get(third_node, &string_val));
        TEST_ASSERT(0 == strcmp("szActionsBlocked", string_val));
        TEST_ASSERT(MA_OK == ma_custom_event_node_data_get(third_node, &string_val));
        TEST_ASSERT(0 == strcmp("Modify", string_val));
        second_node = third_node; // Moving pointer ahead to retrieve next element

        // No more elements in the XML, therefore, ma_custom_event_get_next_node
        // should return MA_ERROR_OBJECTNOTFOUND
        TEST_ASSERT(MA_ERROR_OBJECTNOTFOUND == ma_custom_event_get_next_node(second_node, &third_node));
    }

    set_done();

    // In Thread_IO mode, the event client sends a 'send-and-forget' type request to
    // the event service, therefore no response is required.
    return MA_OK;
}



/*
Custom Event Test - creating node by node
*/
TEST(ma_event_client_custom_event_tests, ma_custom_event_create_node_by_node_test)
{
    // Object pointers
    ma_custom_event_t *event_ptr = NULL;
    ma_xml_node_t *root_node = NULL;
    ma_xml_node_t *first_node = NULL;
		ma_dispatcher_initialize(NULL,&ev_dispatcher);

    /**************************************************************************
    Creating Custom Event
    **************************************************************************/
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_custom_event_create(NULL, "BehaviourBlockEvent", &event_ptr, &root_node));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_custom_event_create(g_ma_ev_client, NULL, &event_ptr, &root_node));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_custom_event_create(g_ma_ev_client, "BehaviourBlockEvent", NULL, &root_node));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_custom_event_create(g_ma_ev_client, "BehaviourBlockEvent", &event_ptr, NULL));
    TEST_ASSERT(MA_OK == ma_custom_event_create(g_ma_ev_client, "BehaviourBlockEvent", &event_ptr, &root_node));

    /**************************************************************************
    Creating Node, setting attributes and data fields
    **************************************************************************/
    {
        ma_xml_node_t *second_node = NULL;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_custom_event_node_create(NULL, "ScannerSoftware", &first_node));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_custom_event_node_create(root_node, NULL, &first_node));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_custom_event_node_create(root_node, "ScannerSoftware", NULL));
        TEST_ASSERT(MA_OK == ma_custom_event_node_create(root_node, "ScannerSoftware", &first_node));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_custom_event_node_attribute_set(NULL, "ProductName", "VirusScan Enterprise"));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_custom_event_node_attribute_set(first_node, NULL, "VirusScan Enterprise"));
        TEST_ASSERT(MA_OK == ma_custom_event_node_attribute_set(first_node, "ProductName", "VirusScan Enterprise"));
        TEST_ASSERT(MA_OK == ma_custom_event_node_attribute_set(first_node, "ProductVersion", "8.8"));
        TEST_ASSERT(MA_OK == ma_custom_event_node_attribute_set(first_node, "ProductFamily", "TVD"));

        // Setting sub-fields and data
        TEST_ASSERT(MA_OK == ma_custom_event_node_create(first_node, "EngineVersion", &second_node));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_custom_event_node_set_data(NULL, "0"));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_custom_event_node_set_data(second_node, NULL));
        TEST_ASSERT(MA_OK == ma_custom_event_node_set_data(second_node, "0"));

        TEST_ASSERT(MA_OK == ma_custom_event_node_create(first_node, "DATVersion", &second_node));
        TEST_ASSERT(MA_OK == ma_custom_event_node_set_data(second_node, "123"));

        TEST_ASSERT(MA_OK == ma_custom_event_node_create(first_node, "ScannerType", &second_node));
        TEST_ASSERT(MA_OK == ma_custom_event_node_set_data(second_node, "OAS"));

        TEST_ASSERT(MA_OK == ma_custom_event_node_create(first_node, "TaskName", &second_node));
        TEST_ASSERT(MA_OK == ma_custom_event_node_set_data(second_node, "OAS"));

        TEST_ASSERT(MA_OK == ma_custom_event_node_create(first_node, "ProductFamily", &second_node));
        TEST_ASSERT(MA_OK == ma_custom_event_node_set_data(second_node, "TVD"));

        TEST_ASSERT(MA_OK == ma_custom_event_node_create(first_node, "ProductName", &second_node));
        TEST_ASSERT(MA_OK == ma_custom_event_node_set_data(second_node, "VirusScan Enterprise"));

        TEST_ASSERT(MA_OK == ma_custom_event_node_create(first_node, "ProductVersion", &second_node));
        TEST_ASSERT(MA_OK == ma_custom_event_node_set_data(second_node, "8.8"));
    }

    /**************************************************************************
    Creating Event Node 1 and setting data fields
    **************************************************************************/
    {
        ma_xml_node_t *second_node = NULL;
        ma_xml_node_t *third_node = NULL;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_custom_event_create_event_node(NULL, first_node, "BlockedBehaviourInfo1", 1092, MA_EVENT_SEVERITY_MINOR, &second_node));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_custom_event_create_event_node(event_ptr, NULL, "BlockedBehaviourInfo1", 1092, MA_EVENT_SEVERITY_MINOR, &second_node));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_custom_event_create_event_node(event_ptr, first_node, NULL, 1092, MA_EVENT_SEVERITY_MINOR, &second_node));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_custom_event_create_event_node(event_ptr, first_node, "BlockedBehaviourInfo1", 1092, MA_EVENT_SEVERITY_MINOR, NULL));
        TEST_ASSERT(MA_OK == ma_custom_event_create_event_node(event_ptr, first_node, "BlockedBehaviourInfo1", 1092, MA_EVENT_SEVERITY_MINOR, &second_node));

        TEST_ASSERT(MA_OK == ma_custom_event_node_create(second_node, "RuleName", &third_node));
        TEST_ASSERT(MA_OK == ma_custom_event_node_set_data(third_node, "Common Standard Protection:Prevent modification of McAfee Common Management Agent files and settings"));

        TEST_ASSERT(MA_OK == ma_custom_event_node_create(second_node, "ProcessName", &third_node));
        TEST_ASSERT(MA_OK == ma_custom_event_node_set_data(third_node, "C:\\Program Files\\McAfee\\Agent\\macompatsvc.exe"));

        TEST_ASSERT(MA_OK == ma_custom_event_node_create(second_node, "FileName", &third_node));
        TEST_ASSERT(MA_OK == ma_custom_event_node_set_data(third_node, "\\REGISTRY\\MACHINE\\SOFTWARE\\McAfee\\Agent\\lpc\\lpc_throb"));

        TEST_ASSERT(MA_OK == ma_custom_event_node_create(second_node, "Source", &third_node));
        TEST_ASSERT(MA_OK == ma_custom_event_node_set_data(third_node, ""));

        TEST_ASSERT(MA_OK == ma_custom_event_node_create(second_node, "ActionsBlocked", &third_node));
        TEST_ASSERT(MA_OK == ma_custom_event_node_set_data(third_node, "3"));

        TEST_ASSERT(MA_OK == ma_custom_event_node_create(second_node, "szActionsBlocked", &third_node));
        TEST_ASSERT(MA_OK == ma_custom_event_node_set_data(third_node, "Create"));
    }

    /**************************************************************************
    Creating Event Node 2 and setting data fields
    **************************************************************************/
    {
        ma_xml_node_t *second_node = NULL;
        ma_xml_node_t *third_node = NULL;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_custom_event_create_event_node(NULL, first_node, "BlockedBehaviourInfo2", 2345, MA_EVENT_SEVERITY_WARNING, &second_node));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_custom_event_create_event_node(event_ptr, NULL, "BlockedBehaviourInfo2", 2345, MA_EVENT_SEVERITY_WARNING, &second_node));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_custom_event_create_event_node(event_ptr, first_node, NULL, 2345, MA_EVENT_SEVERITY_WARNING, &second_node));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_custom_event_create_event_node(event_ptr, first_node, "BlockedBehaviourInfo2", 2345, MA_EVENT_SEVERITY_WARNING, NULL));
        TEST_ASSERT(MA_OK == ma_custom_event_create_event_node(event_ptr, first_node, "BlockedBehaviourInfo2", 2345, MA_EVENT_SEVERITY_WARNING, &second_node));

        TEST_ASSERT(MA_OK == ma_custom_event_node_create(second_node, "RuleName", &third_node));
        TEST_ASSERT(MA_OK == ma_custom_event_node_set_data(third_node, "Special Protection:Prevent modification of any program files"));

        TEST_ASSERT(MA_OK == ma_custom_event_node_create(second_node, "ProcessName", &third_node));
        TEST_ASSERT(MA_OK == ma_custom_event_node_set_data(third_node, "C:\\Program Files\\McAfee\\Agent\\macompatsvc.exe"));

        TEST_ASSERT(MA_OK == ma_custom_event_node_create(second_node, "Source", &third_node));
        TEST_ASSERT(MA_OK == ma_custom_event_node_set_data(third_node, "No Source"));

        TEST_ASSERT(MA_OK == ma_custom_event_node_create(second_node, "szActionsBlocked", &third_node));
        TEST_ASSERT(MA_OK == ma_custom_event_node_set_data(third_node, "Modify"));
    }

    /**************************************************************************
     Registering callback
    **************************************************************************/
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_register_callbacks(NULL, &callbacks_bundle, NULL));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_register_callbacks(g_ma_ev_client, NULL, NULL));
    TEST_ASSERT(MA_OK == ma_event_register_callbacks(g_ma_ev_client, &callbacks_bundle, NULL));

    /**************************************************************************
     Sending Custom Event
    **************************************************************************/
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_custom_event_send(NULL));
    TEST_ASSERT(MA_OK == ma_custom_event_send(event_ptr));

    // Wait for callback to complete
    done = 2;
    wait_until_done();

    /**************************************************************************
     Un-registering callback
    **************************************************************************/
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_unregister_callbacks(NULL));
    TEST_ASSERT(MA_OK == ma_event_unregister_callbacks(g_ma_ev_client));

    /**************************************************************************
     Releasing Custom Event
    **************************************************************************/
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_custom_event_release(NULL));
    TEST_ASSERT(MA_OK == ma_custom_event_release(event_ptr));

	ma_dispatcher_deinitialize(ev_dispatcher);
}

/*
Custom Event Test - Loading from buffer
*/

TEST(ma_event_client_custom_event_tests, ma_custom_event_create_from_buffer_test)
{
    // Object pointers
    ma_custom_event_t *event_ptr = NULL;
    ma_xml_node_t *root_node = NULL;
    ma_xml_node_t *first_node = NULL;

    const char valid_buffer[] = "\x3c\x42\x65\x68\x61\x76\x69\x6f\x75\x72\x42\x6c\x6f\x63\x6b\x45\x76\x65\x6e\x74\x3e\x3c\x53\x63\x61\x6e\x6e\x65\x72\x53\x6f\x66\x74\x77\x61\x72\x65\x20\x50\x72\x6f\x64\x75\x63\x74\x4e\x61\x6d\x65\x3d\x22\x56\x69\x72\x75\x73\x53\x63\x61\x6e\x20\x45\x6e\x74\x65\x72\x70\x72\x69\x73\x65\x22\x20\x50\x72\x6f\x64\x75\x63\x74\x56\x65\x72\x73\x69\x6f\x6e\x3d\x22\x38\x2e\x38\x22\x20\x50\x72\x6f\x64\x75\x63\x74\x46\x61\x6d\x69\x6c\x79\x3d\x22\x54\x56\x44\x22\x3e\x3c\x45\x6e\x67\x69\x6e\x65\x56\x65\x72\x73\x69\x6f\x6e\x3e\x30\x3c\x2f\x45\x6e\x67\x69\x6e\x65\x56\x65\x72\x73\x69\x6f\x6e\x3e\x3c\x44\x41\x54\x56\x65\x72\x73\x69\x6f\x6e\x3e\x31\x32\x33\x3c\x2f\x44\x41\x54\x56\x65\x72\x73\x69\x6f\x6e\x3e\x3c\x53\x63\x61\x6e\x6e\x65\x72\x54\x79\x70\x65\x3e\x4f\x41\x53\x3c\x2f\x53\x63\x61\x6e\x6e\x65\x72\x54\x79\x70\x65\x3e\x3c\x54\x61\x73\x6b\x4e\x61\x6d\x65\x3e\x4f\x41\x53\x3c\x2f\x54\x61\x73\x6b\x4e\x61\x6d\x65\x3e\x3c\x50\x72\x6f\x64\x75\x63\x74\x46\x61\x6d\x69\x6c\x79\x3e\x54\x56\x44\x3c\x2f\x50\x72\x6f\x64\x75\x63\x74\x46\x61\x6d\x69\x6c\x79\x3e\x3c\x50\x72\x6f\x64\x75\x63\x74\x4e\x61\x6d\x65\x3e\x56\x69\x72\x75\x73\x53\x63\x61\x6e\x20\x45\x6e\x74\x65\x72\x70\x72\x69\x73\x65\x3c\x2f\x50\x72\x6f\x64\x75\x63\x74\x4e\x61\x6d\x65\x3e\x3c\x50\x72\x6f\x64\x75\x63\x74\x56\x65\x72\x73\x69\x6f\x6e\x3e\x38\x2e\x38\x3c\x2f\x50\x72\x6f\x64\x75\x63\x74\x56\x65\x72\x73\x69\x6f\x6e\x3e\x3c\x42\x6c\x6f\x63\x6b\x65\x64\x42\x65\x68\x61\x76\x69\x6f\x75\x72\x49\x6e\x66\x6f\x31\x3e\x3c\x45\x76\x65\x6e\x74\x49\x44\x3e\x31\x30\x39\x32\x3c\x2f\x45\x76\x65\x6e\x74\x49\x44\x3e\x3c\x53\x65\x76\x65\x72\x69\x74\x79\x3e\x32\x3c\x2f\x53\x65\x76\x65\x72\x69\x74\x79\x3e\x3c\x47\x4d\x54\x54\x69\x6d\x65\x3e\x32\x30\x31\x34\x2d\x30\x32\x2d\x31\x31\x54\x31\x31\x3a\x35\x36\x3a\x34\x37\x3c\x2f\x47\x4d\x54\x54\x69\x6d\x65\x3e\x3c\x55\x54\x43\x54\x69\x6d\x65\x3e\x32\x30\x31\x34\x2d\x30\x32\x2d\x31\x31\x54\x30\x36\x3a\x32\x36\x3a\x34\x37\x3c\x2f\x55\x54\x43\x54\x69\x6d\x65\x3e\x3c\x52\x75\x6c\x65\x4e\x61\x6d\x65\x3e\x43\x6f\x6d\x6d\x6f\x6e\x20\x53\x74\x61\x6e\x64\x61\x72\x64\x20\x50\x72\x6f\x74\x65\x63\x74\x69\x6f\x6e\x3a\x50\x72\x65\x76\x65\x6e\x74\x20\x6d\x6f\x64\x69\x66\x69\x63\x61\x74\x69\x6f\x6e\x20\x6f\x66\x20\x4d\x63\x41\x66\x65\x65\x20\x43\x6f\x6d\x6d\x6f\x6e\x20\x4d\x61\x6e\x61\x67\x65\x6d\x65\x6e\x74\x20\x41\x67\x65\x6e\x74\x20\x66\x69\x6c\x65\x73\x20\x61\x6e\x64\x20\x73\x65\x74\x74\x69\x6e\x67\x73\x3c\x2f\x52\x75\x6c\x65\x4e\x61\x6d\x65\x3e\x3c\x50\x72\x6f\x63\x65\x73\x73\x4e\x61\x6d\x65\x3e\x43\x3a\x5c\x50\x72\x6f\x67\x72\x61\x6d\x20\x46\x69\x6c\x65\x73\x5c\x4d\x63\x41\x66\x65\x65\x5c\x41\x67\x65\x6e\x74\x5c\x6d\x61\x63\x6f\x6d\x70\x61\x74\x73\x76\x63\x2e\x65\x78\x65\x3c\x2f\x50\x72\x6f\x63\x65\x73\x73\x4e\x61\x6d\x65\x3e\x3c\x46\x69\x6c\x65\x4e\x61\x6d\x65\x3e\x5c\x52\x45\x47\x49\x53\x54\x52\x59\x5c\x4d\x41\x43\x48\x49\x4e\x45\x5c\x53\x4f\x46\x54\x57\x41\x52\x45\x5c\x4d\x63\x41\x66\x65\x65\x5c\x41\x67\x65\x6e\x74\x5c\x6c\x70\x63\x5c\x6c\x70\x63\x5f\x74\x68\x72\x6f\x62\x3c\x2f\x46\x69\x6c\x65\x4e\x61\x6d\x65\x3e\x3c\x53\x6f\x75\x72\x63\x65\x3e\x3c\x2f\x53\x6f\x75\x72\x63\x65\x3e\x3c\x41\x63\x74\x69\x6f\x6e\x73\x42\x6c\x6f\x63\x6b\x65\x64\x3e\x33\x3c\x2f\x41\x63\x74\x69\x6f\x6e\x73\x42\x6c\x6f\x63\x6b\x65\x64\x3e\x3c\x73\x7a\x41\x63\x74\x69\x6f\x6e\x73\x42\x6c\x6f\x63\x6b\x65\x64\x3e\x43\x72\x65\x61\x74\x65\x3c\x2f\x73\x7a\x41\x63\x74\x69\x6f\x6e\x73\x42\x6c\x6f\x63\x6b\x65\x64\x3e\x3c\x2f\x42\x6c\x6f\x63\x6b\x65\x64\x42\x65\x68\x61\x76\x69\x6f\x75\x72\x49\x6e\x66\x6f\x31\x3e\x3c\x42\x6c\x6f\x63\x6b\x65\x64\x42\x65\x68\x61\x76\x69\x6f\x75\x72\x49\x6e\x66\x6f\x32\x3e\x3c\x45\x76\x65\x6e\x74\x49\x44\x3e\x32\x33\x34\x35\x3c\x2f\x45\x76\x65\x6e\x74\x49\x44\x3e\x3c\x53\x65\x76\x65\x72\x69\x74\x79\x3e\x31\x3c\x2f\x53\x65\x76\x65\x72\x69\x74\x79\x3e\x3c\x47\x4d\x54\x54\x69\x6d\x65\x3e\x32\x30\x31\x34\x2d\x30\x32\x2d\x31\x34\x54\x31\x31\x3a\x35\x34\x3a\x31\x32\x3c\x2f\x47\x4d\x54\x54\x69\x6d\x65\x3e\x3c\x55\x54\x43\x54\x69\x6d\x65\x3e\x32\x30\x31\x34\x2d\x30\x32\x2d\x31\x34\x54\x30\x36\x3a\x32\x33\x3a\x33\x34\x3c\x2f\x55\x54\x43\x54\x69\x6d\x65\x3e\x3c\x52\x75\x6c\x65\x4e\x61\x6d\x65\x3e\x53\x70\x65\x63\x69\x61\x6c\x20\x50\x72\x6f\x74\x65\x63\x74\x69\x6f\x6e\x3a\x50\x72\x65\x76\x65\x6e\x74\x20\x6d\x6f\x64\x69\x66\x69\x63\x61\x74\x69\x6f\x6e\x20\x6f\x66\x20\x61\x6e\x79\x20\x70\x72\x6f\x67\x72\x61\x6d\x20\x66\x69\x6c\x65\x73\x3c\x2f\x52\x75\x6c\x65\x4e\x61\x6d\x65\x3e\x3c\x50\x72\x6f\x63\x65\x73\x73\x4e\x61\x6d\x65\x3e\x43\x3a\x5c\x50\x72\x6f\x67\x72\x61\x6d\x20\x46\x69\x6c\x65\x73\x5c\x4d\x63\x41\x66\x65\x65\x5c\x41\x67\x65\x6e\x74\x5c\x6d\x61\x63\x6f\x6d\x70\x61\x74\x73\x76\x63\x2e\x65\x78\x65\x3c\x2f\x50\x72\x6f\x63\x65\x73\x73\x4e\x61\x6d\x65\x3e\x3c\x53\x6f\x75\x72\x63\x65\x3e\x4e\x6f\x20\x53\x6f\x75\x72\x63\x65\x3c\x2f\x53\x6f\x75\x72\x63\x65\x3e\x3c\x73\x7a\x41\x63\x74\x69\x6f\x6e\x73\x42\x6c\x6f\x63\x6b\x65\x64\x3e\x4d\x6f\x64\x69\x66\x79\x3c\x2f\x73\x7a\x41\x63\x74\x69\x6f\x6e\x73\x42\x6c\x6f\x63\x6b\x65\x64\x3e\x3c\x2f\x42\x6c\x6f\x63\x6b\x65\x64\x42\x65\x68\x61\x76\x69\x6f\x75\x72\x49\x6e\x66\x6f\x32\x3e\x3c\x2f\x53\x63\x61\x6e\x6e\x65\x72\x53\x6f\x66\x74\x77\x61\x72\x65\x3e\x3c\x2f\x42\x65\x68\x61\x76\x69\x6f\x75\x72\x42\x6c\x6f\x63\x6b\x45\x76\x65\x6e\x74\x3e";
    const char invalid_buffer[] = "\x42\x65\x68\x61\x76\x69\x6f\x75\x72\x42\x6c\x6f\x63\x6b\x45\x76\x65\x6e\x74\x3e\x3c\x53\x63\x61\x6e\x6e\x65\x72\x53\x6f\x66\x74\x77\x61\x72\x65\x20\x50\x72\x6f\x64\x75\x63\x74\x4e\x61\x6d\x65\x3d\x22\x56\x69\x72\x75\x73\x53\x63\x61\x6e\x20\x45\x6e\x74\x65\x72\x70\x72\x69\x73\x65\x22\x20\x50\x72\x6f\x64\x75\x63\x74\x56\x65\x72\x73\x69\x6f\x6e\x3d\x22\x38\x2e\x38\x22\x20\x50\x72\x6f\x64\x75\x63\x74\x46\x61\x6d\x69\x6c\x79\x3d\x22\x54\x56\x44\x22\x3e\x3c\x45\x6e\x67\x69\x6e\x65\x56\x65\x72\x73\x69\x6f\x6e\x3e\x30\x3c\x2f\x45\x6e\x67\x69\x6e\x65\x56\x65\x72\x73\x69\x6f\x6e\x3e\x3c\x44\x41\x54\x56\x65\x72\x73\x69\x6f\x6e\x3e\x31\x32\x33\x3c\x2f\x44\x41\x54\x56\x65\x72\x73\x69\x6f\x6e\x3e\x3c\x53\x63\x61\x6e\x6e\x65\x72\x54\x79\x70\x65\x3e\x4f\x41\x53\x3c\x2f\x53\x63\x61\x6e\x6e\x65\x72\x54\x79\x70\x65\x3e\x3c\x54\x61\x73\x6b\x4e\x61\x6d\x65\x3e\x4f\x41\x53\x3c\x2f\x54\x61\x73\x6b\x4e\x61\x6d\x65\x3e\x3c\x50\x72\x6f\x64\x75\x63\x74\x46\x61\x6d\x69\x6c\x79\x3e\x54\x56\x44\x3c\x2f\x50\x72\x6f\x64\x75\x63\x74\x46\x61\x6d\x69\x6c\x79\x3e\x3c\x50\x72\x6f\x64\x75\x63\x74\x4e\x61\x6d\x65\x3e\x56\x69\x72\x75\x73\x53\x63\x61\x6e\x20\x45\x6e\x74\x65\x72\x70\x72\x69\x73\x65\x3c\x2f\x50\x72\x6f\x64\x75\x63\x74\x4e\x61\x6d\x65\x3e\x3c\x50\x72\x6f\x64\x75\x63\x74\x56\x65\x72\x73\x69\x6f\x6e\x3e\x38\x2e\x38\x3c\x2f\x50\x72\x6f\x64\x75\x63\x74\x56\x65\x72\x73\x69\x6f\x6e\x3e\x3c\x42\x6c\x6f\x63\x6b\x65\x64\x42\x65\x68\x61\x76\x69\x6f\x75\x72\x49\x6e\x66\x6f\x31\x3e\x3c\x45\x76\x65\x6e\x74\x49\x44\x3e\x31\x30\x39\x32\x3c\x2f\x45\x76\x65\x6e\x74\x49\x44\x3e\x3c\x53\x65\x76\x65\x72\x69\x74\x79\x3e\x32\x3c\x2f\x53\x65\x76\x65\x72\x69\x74\x79\x3e\x3c\x47\x4d\x54\x54\x69\x6d\x65\x3e\x32\x30\x31\x34\x2d\x30\x32\x2d\x31\x31\x54\x31\x31\x3a\x35\x36\x3a\x34\x37\x3c\x2f\x47\x4d\x54\x54\x69\x6d\x65\x3e\x3c\x55\x54\x43\x54\x69\x6d\x65\x3e\x32\x30\x31\x34\x2d\x30\x32\x2d\x31\x31\x54\x30\x36\x3a\x32\x36\x3a\x34\x37\x3c\x2f\x55\x54\x43\x54\x69\x6d\x65\x3e\x3c\x52\x75\x6c\x65\x4e\x61\x6d\x65\x3e\x43\x6f\x6d\x6d\x6f\x6e\x20\x53\x74\x61\x6e\x64\x61\x72\x64\x20\x50\x72\x6f\x74\x65\x63\x74\x69\x6f\x6e\x3a\x50\x72\x65\x76\x65\x6e\x74\x20\x6d\x6f\x64\x69\x66\x69\x63\x61\x74\x69\x6f\x6e\x20\x6f\x66\x20\x4d\x63\x41\x66\x65\x65\x20\x43\x6f\x6d\x6d\x6f\x6e\x20\x4d\x61\x6e\x61\x67\x65\x6d\x65\x6e\x74\x20\x41\x67\x65\x6e\x74\x20\x66\x69\x6c\x65\x73\x20\x61\x6e\x64\x20\x73\x65\x74\x74\x69\x6e\x67\x73\x3c\x2f\x52\x75\x6c\x65\x4e\x61\x6d\x65\x3e\x3c\x50\x72\x6f\x63\x65\x73\x73\x4e\x61\x6d\x65\x3e\x43\x3a\x5c\x50\x72\x6f\x67\x72\x61\x6d\x20\x46\x69\x6c\x65\x73\x5c\x4d\x63\x41\x66\x65\x65\x5c\x41\x67\x65\x6e\x74\x5c\x6d\x61\x63\x6f\x6d\x70\x61\x74\x73\x76\x63\x2e\x65\x78\x65\x3c\x2f\x50\x72\x6f\x63\x65\x73\x73\x4e\x61\x6d\x65\x3e\x3c\x46\x69\x6c\x65\x4e\x61\x6d\x65\x3e\x5c\x52\x45\x47\x49\x53\x54\x52\x59\x5c\x4d\x41\x43\x48\x49\x4e\x45\x5c\x53\x4f\x46\x54\x57\x41\x52\x45\x5c\x4d\x63\x41\x66\x65\x65\x5c\x41\x67\x65\x6e\x74\x5c\x6c\x70\x63\x5c\x6c\x70\x63\x5f\x74\x68\x72\x6f\x62\x3c\x2f\x46\x69\x6c\x65\x4e\x61\x6d\x65\x3e\x3c\x53\x6f\x75\x72\x63\x65\x3e\x3c\x2f\x53\x6f\x75\x72\x63\x65\x3e\x3c\x41\x63\x74\x69\x6f\x6e\x73\x42\x6c\x6f\x63\x6b\x65\x64\x3e\x33\x3c\x2f\x41\x63\x74\x69\x6f\x6e\x73\x42\x6c\x6f\x63\x6b\x65\x64\x3e\x3c\x73\x7a\x41\x63\x74\x69\x6f\x6e\x73\x42\x6c\x6f\x63\x6b\x65\x64\x3e\x43\x72\x65\x61\x74\x65\x3c\x2f\x73\x7a\x41\x63\x74\x69\x6f\x6e\x73\x42\x6c\x6f\x63\x6b\x65\x64\x3e\x3c\x2f\x42\x6c\x6f\x63\x6b\x65\x64\x42\x65\x68\x61\x76\x69\x6f\x75\x72\x49\x6e\x66\x6f\x31\x3e\x3c\x42\x6c\x6f\x63\x6b\x65\x64\x42\x65\x68\x61\x76\x69\x6f\x75\x72\x49\x6e\x66\x6f\x32\x3e\x3c\x45\x76\x65\x6e\x74\x49\x44\x3e\x32\x33\x34\x35\x3c\x2f\x45\x76\x65\x6e\x74\x49\x44\x3e\x3c\x53\x65\x76\x65\x72\x69\x74\x79\x3e\x31\x3c\x2f\x53\x65\x76\x65\x72\x69\x74\x79\x3e\x3c\x47\x4d\x54\x54\x69\x6d\x65\x3e\x32\x30\x31\x34\x2d\x30\x32\x2d\x31\x34\x54\x31\x31\x3a\x35\x34\x3a\x31\x32\x3c\x2f\x47\x4d\x54\x54\x69\x6d\x65\x3e\x3c\x55\x54\x43\x54\x69\x6d\x65\x3e\x32\x30\x31\x34\x2d\x30\x32\x2d\x31\x34\x54\x30\x36\x3a\x32\x33\x3a\x33\x34\x3c\x2f\x55\x54\x43\x54\x69\x6d\x65\x3e\x3c\x52\x75\x6c\x65\x4e\x61\x6d\x65\x3e\x53\x70\x65\x63\x69\x61\x6c\x20\x50\x72\x6f\x74\x65\x63\x74\x69\x6f\x6e\x3a\x50\x72\x65\x76\x65\x6e\x74\x20\x6d\x6f\x64\x69\x66\x69\x63\x61\x74\x69\x6f\x6e\x20\x6f\x66\x20\x61\x6e\x79\x20\x70\x72\x6f\x67\x72\x61\x6d\x20\x66\x69\x6c\x65\x73\x3c\x2f\x52\x75\x6c\x65\x4e\x61\x6d\x65\x3e\x3c\x50\x72\x6f\x63\x65\x73\x73\x4e\x61\x6d\x65\x3e\x43\x3a\x5c\x50\x72\x6f\x67\x72\x61\x6d\x20\x46\x69\x6c\x65\x73\x5c\x4d\x63\x41\x66\x65\x65\x5c\x41\x67\x65\x6e\x74\x5c\x6d\x61\x63\x6f\x6d\x70\x61\x74\x73\x76\x63\x2e\x65\x78\x65\x3c\x2f\x50\x72\x6f\x63\x65\x73\x73\x4e\x61\x6d\x65\x3e\x3c\x53\x6f\x75\x72\x63\x65\x3e\x4e\x6f\x20\x53\x6f\x75\x72\x63\x65\x3c\x2f\x53\x6f\x75\x72\x63\x65\x3e\x3c\x73\x7a\x41\x63\x74\x69\x6f\x6e\x73\x42\x6c\x6f\x63\x6b\x65\x64\x3e\x4d\x6f\x64\x69\x66\x79\x3c\x2f\x73\x7a\x41\x63\x74\x69\x6f\x6e\x73\x42\x6c\x6f\x63\x6b\x65\x64\x3e\x3c\x2f\x42\x6c\x6f\x63\x6b\x65\x64\x42\x65\x68\x61\x76\x69\x6f\x75\x72\x49\x6e\x66\x6f\x32\x3e\x3c\x2f\x53\x63\x61\x6e\x6e\x65\x72\x53\x6f\x66\x74\x77\x61\x72\x65\x3e\x3c\x2f\x42\x65\x68\x61\x76\x69\x6f\x75\x72\x42\x6c\x6f\x63\x6b\x45\x76\x65\x6e\x74\x3e";
	ma_dispatcher_initialize(NULL,&ev_dispatcher);
    /**************************************************************************
    TEST 1 - Creating Custom Event from Buffer. Buffer contains valid XML
    **************************************************************************/
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_custom_event_create_from_buffer(NULL, valid_buffer, &event_ptr));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_custom_event_create_from_buffer(g_ma_ev_client, NULL, &event_ptr));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_custom_event_create_from_buffer(g_ma_ev_client, valid_buffer, NULL));
    TEST_ASSERT(MA_OK == ma_custom_event_create_from_buffer(g_ma_ev_client, valid_buffer, &event_ptr));

    /**************************************************************************
     Registering callback
    **************************************************************************/
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_register_callbacks(NULL, &callbacks_bundle, NULL));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_register_callbacks(g_ma_ev_client, NULL, NULL));
    TEST_ASSERT(MA_OK == ma_event_register_callbacks(g_ma_ev_client, &callbacks_bundle, NULL));

    /**************************************************************************
    Sending the Event message to dummy event service.
    **************************************************************************/
    // The server side callback function and the registered client callbacks have
    // to verify the received custom event contents. Wait for them to finish.
    done = 2;

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_custom_event_send(NULL));
    TEST_ASSERT(MA_OK == ma_custom_event_send(event_ptr));

    wait_until_done();

    /**************************************************************************
     Un-registering callback
    **************************************************************************/
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_unregister_callbacks(NULL));
    TEST_ASSERT(MA_OK == ma_event_unregister_callbacks(g_ma_ev_client));

    /**************************************************************************
     Releasing custom event
    **************************************************************************/
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_custom_event_release(NULL));
    TEST_ASSERT(MA_OK == ma_custom_event_release(event_ptr));

    /**************************************************************************
    TEST 2 - Creating Custom Event from Buffer. Buffer contains invalid XML
    **************************************************************************/
    TEST_ASSERT(MA_ERROR_XML_LOAD_FAILED == ma_custom_event_create_from_buffer(g_ma_ev_client, invalid_buffer, &event_ptr));

	ma_dispatcher_deinitialize(ev_dispatcher);
}





