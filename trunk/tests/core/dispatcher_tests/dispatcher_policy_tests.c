#include "unity.h"
#include "unity_fixture.h"

#include <uv.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "ma/policy/ma_policy.h"

#include "ma/ma_common.h"
#include "ma/ma_message.h"
//#include "ma/ma_msgbus.h"
#include "ma/ma_variant.h"
#include "ma/ma_dispatcher.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/core/msgbus/ma_message_header.h"
#include "ma/logger/ma_file_logger.h"
#include "ma/ma_errors.h"



#include "dispatcher_tests_utils.h"

#define DISPATCHER_PROUDUCT_ID "ma.dispatcher.test"

#ifdef MA_WINDOWS
# include <crtdbg.h>
#include <Windows.h>
# define MA_TRACKLEAKS _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF)
#define SOFTWARE_ID_PATH "SOFTWARE\\McAfee\\Agent\\Applications\\EPOAGENT3000"
#define SOFTWARE_ID_PATH1 "SOFTWARE\\McAfee\\Agent\\Applications\\EPOAGENT3000"
#define MA_DISPATCHER_PRODUCT_INSTAL_PATH "install_path"
#define MA_DISPATCHER_RSDK_PATH "RuntimeSdkPath"
#else
# define MA_TRACKLEAKS ((void *)0)
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#define SOFTWARE_ID_PATH "/etc/ma.d"
#endif



ma_dispatcher_t *dispatcher = NULL;


TEST_GROUP_RUNNER(dispatcher_policy_bag_tests_group)
{
    do {RUN_TEST_CASE(policy_bag_tests, policy_bag_get_set_test)} while (0);
}

// URIs
ma_policy_uri_t *policy_uri_1 = NULL;
ma_policy_uri_t *policy_uri_2 = NULL;

TEST_SETUP(policy_bag_tests)
{
	#ifdef MA_WINDOWS
    ma_dispatcher_test_create_registry(SOFTWARE_ID_PATH);
#else
    ma_dispatcher_test_create_registry("EPOAGENT3000");
#endif

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_initialize(NULL, &dispatcher));

    // Creating Policy URI 1
    TEST_ASSERT(MA_OK == ma_policy_uri_create(&policy_uri_1));
    TEST_ASSERT(MA_OK == ma_policy_uri_set_feature(policy_uri_1, "My Policy Features 1"));
    TEST_ASSERT(MA_OK == ma_policy_uri_set_category(policy_uri_1, "Special Category 1"));
    TEST_ASSERT(MA_OK == ma_policy_uri_set_type(policy_uri_1, "No Identification 1"));
    TEST_ASSERT(MA_OK == ma_policy_uri_set_name(policy_uri_1, "First Policy URI 1"));

    // Creating Policy URI 2
    TEST_ASSERT(MA_OK == ma_policy_uri_create(&policy_uri_2));
    TEST_ASSERT(MA_OK == ma_policy_uri_set_feature(policy_uri_2, "My Policy Features 2"));
    TEST_ASSERT(MA_OK == ma_policy_uri_set_category(policy_uri_2, "Special Category 2"));
    TEST_ASSERT(MA_OK == ma_policy_uri_set_type(policy_uri_2, "No Identification 2"));
    TEST_ASSERT(MA_OK == ma_policy_uri_set_name(policy_uri_2, "First Policy URI 2"));
}

TEST_TEAR_DOWN(policy_bag_tests)
{
	#ifdef MA_WINDOWS
   ma_dispatcher_tests_delete_registry(SOFTWARE_ID_PATH);
   ma_dispatcher_tests_delete_registry(SOFTWARE_ID_PATH1);
#else
    ma_dispatcher_tests_delete_registry("ma");
    ma_dispatcher_tests_delete_registry("ma1");
#endif
    // Releasing URIs
    TEST_ASSERT(MA_OK == ma_policy_uri_release(policy_uri_1));
    TEST_ASSERT(MA_OK == ma_policy_uri_release(policy_uri_2));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_deinitialize(dispatcher));
}

/*
Test Approach -
-------------
1. Create a new policy bag.
2. Fill it with PSO properties and Section Information using set API
3. Copy the policy bag into a new bag using copy API. Release the first policy bag.
4. Retrieve the information from the copied bag (new) using get API and verify that the
   information is retrieved correctly.
5. Release the new policy bag also.
*/
TEST(policy_bag_tests, policy_bag_get_set_test)
{
    // Policy bags
    ma_policy_bag_t *my_policy_bag = NULL;  // Policy bag created
    ma_policy_bag_t *new_policy_bag = NULL; // Policy bag to be copied to

    ma_variant_t * my_variant_ptr = NULL;
	ma_dispatcher_t *dispatcher_handle = NULL;
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_initialize(NULL, &dispatcher_handle));

    // Creating Policy Bags (current and new)
    TEST_ASSERT(MA_ERROR_INVALIDARG ==  ma_policy_bag_create(NULL));
    TEST_ASSERT(MA_OK ==  ma_policy_bag_create(&my_policy_bag));
    TEST_ASSERT(MA_OK ==  ma_policy_bag_create(&new_policy_bag));

    // Setting values in Policy bag
    TEST_ASSERT(MA_OK == ma_variant_create_from_string("VALUE 1", strlen("VALUE 1"), &my_variant_ptr));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_bag_set_value(NULL, policy_uri_1, "SECTION 1", "KEY 1", my_variant_ptr));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_bag_set_value(my_policy_bag, NULL, "SECTION 1", "KEY 1", my_variant_ptr));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_bag_set_value(my_policy_bag, policy_uri_1, NULL, "KEY 1", my_variant_ptr));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_bag_set_value(my_policy_bag, policy_uri_1, "SECTION 1", NULL, my_variant_ptr));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_bag_set_value(my_policy_bag, policy_uri_1, "SECTION 1", "KEY 1", NULL));
    TEST_ASSERT(MA_OK == ma_policy_bag_set_value(my_policy_bag, policy_uri_1, "SECTION 1", "KEY 1", my_variant_ptr));
    TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));

    TEST_ASSERT(MA_OK == ma_variant_create_from_string("VALUE 2", strlen("VALUE 2"), &my_variant_ptr));
    TEST_ASSERT(MA_OK == ma_policy_bag_set_value(my_policy_bag, policy_uri_1, "SECTION 2", "KEY 2", my_variant_ptr));
    TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));

    TEST_ASSERT(MA_OK == ma_variant_create_from_string("VALUE 3", strlen("VALUE 3"), &my_variant_ptr));
    TEST_ASSERT(MA_OK == ma_policy_bag_set_value(my_policy_bag, policy_uri_2, "SECTION 2", "KEY 3", my_variant_ptr));
    TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));

    TEST_ASSERT(MA_OK == ma_variant_create_from_string("VALUE 4", strlen("VALUE 4"), &my_variant_ptr));
    TEST_ASSERT(MA_OK == ma_policy_bag_set_value(my_policy_bag, policy_uri_1, "SECTION 1", "KEY 4", my_variant_ptr));
    TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));

    TEST_ASSERT(MA_OK == ma_variant_create_from_string("VALUE 5", strlen("VALUE 5"), &my_variant_ptr));
    TEST_ASSERT(MA_OK == ma_policy_bag_set_value(my_policy_bag, policy_uri_2, "SECTION 2", "KEY 5", my_variant_ptr));
    TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));

    TEST_ASSERT(MA_OK == ma_variant_create_from_string("VALUE 6", strlen("VALUE 6"), &my_variant_ptr));
    TEST_ASSERT(MA_OK == ma_policy_bag_set_value(my_policy_bag, policy_uri_1, "SECTION 3", "KEY 6", my_variant_ptr));
    TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));

    TEST_ASSERT(MA_OK == ma_variant_create_from_string("VALUE 7", strlen("VALUE 7"), &my_variant_ptr));
    TEST_ASSERT(MA_OK == ma_policy_bag_set_value(my_policy_bag, policy_uri_2, "SECTION 3", "KEY 7", my_variant_ptr));
    TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));

    TEST_ASSERT(MA_OK == ma_variant_create_from_string("VALUE 8", strlen("VALUE 8"), &my_variant_ptr));
    TEST_ASSERT(MA_OK == ma_policy_bag_set_value(my_policy_bag, policy_uri_1, "SECTION 3", "KEY 8", my_variant_ptr));
    TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));

    TEST_ASSERT(MA_OK == ma_variant_create_from_string("VALUE 9", strlen("VALUE 9"), &my_variant_ptr));
    TEST_ASSERT(MA_OK == ma_policy_bag_set_value(my_policy_bag, policy_uri_2, "SECTION 4", "KEY 9", my_variant_ptr));
    TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));

    TEST_ASSERT(MA_OK == ma_variant_create_from_string("VALUE 10", strlen("VALUE 10"), &my_variant_ptr));
    TEST_ASSERT(MA_OK == ma_policy_bag_set_value(my_policy_bag, policy_uri_1, "SECTION 3", "KEY 10", my_variant_ptr));
    TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));

  
    // Retrieving values from the Policy bag for verification
    //{
    //    ma_buffer_t *my_buffer = NULL;
    //    const char *my_string = NULL;
    //    size_t my_string_size = 0;

    //    // Verifying invalid scenarios
    //    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_bag_get_value(NULL, policy_uri_1, "SECTION 1", "KEY 1", &my_variant_ptr));
    //    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_bag_get_value(my_policy_bag, policy_uri_1, NULL, "KEY 1", &my_variant_ptr));
    //    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_bag_get_value(my_policy_bag, policy_uri_1, "SECTION 1", "KEY 1", NULL));

    //    // Searching for non-existent section
    //    TEST_ASSERT(MA_ERROR_OBJECTNOTFOUND == ma_policy_bag_get_value(my_policy_bag, policy_uri_1, "SECTION 4", "KEY 1", &my_variant_ptr));

    //    // Searching for non-existent key
    //    TEST_ASSERT(MA_ERROR_OBJECTNOTFOUND == ma_policy_bag_get_value(my_policy_bag, policy_uri_1, "SECTION 1", "KEY 11", &my_variant_ptr));

    //    // Valid scenarios (URI 1)
    //    TEST_ASSERT(MA_OK == ma_policy_bag_get_value(my_policy_bag, policy_uri_1, "SECTION 1", "KEY 1", &my_variant_ptr));
    //    ma_variant_get_string_buffer(my_variant_ptr, &my_buffer);
    //    ma_buffer_get_string(my_buffer, &my_string, &my_string_size);
    //    TEST_ASSERT(0 == strcmp(my_string, "VALUE 1"));
    //    TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
    //    TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));

    //    TEST_ASSERT(MA_OK == ma_policy_bag_get_value(my_policy_bag, policy_uri_1, "SECTION 2", "KEY 2", &my_variant_ptr));
    //    ma_variant_get_string_buffer(my_variant_ptr, &my_buffer);
    //    ma_buffer_get_string(my_buffer, &my_string, &my_string_size);
    //    TEST_ASSERT(0 == strcmp(my_string, "VALUE 2"));
    //    TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
    //    TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));

    //    TEST_ASSERT(MA_OK == ma_policy_bag_get_value(my_policy_bag, policy_uri_1, "SECTION 1", "KEY 4", &my_variant_ptr));
    //    ma_variant_get_string_buffer(my_variant_ptr, &my_buffer);
    //    ma_buffer_get_string(my_buffer, &my_string, &my_string_size);
    //    TEST_ASSERT(0 == strcmp(my_string, "VALUE 4"));
    //    TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
    //    TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));

    //    TEST_ASSERT(MA_OK == ma_policy_bag_get_value(my_policy_bag, policy_uri_1, "SECTION 3", "KEY 6", &my_variant_ptr));
    //    ma_variant_get_string_buffer(my_variant_ptr, &my_buffer);
    //    ma_buffer_get_string(my_buffer, &my_string, &my_string_size);
    //    TEST_ASSERT(0 == strcmp(my_string, "VALUE 6"));
    //    TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
    //    TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));

    //    TEST_ASSERT(MA_OK == ma_policy_bag_get_value(my_policy_bag, policy_uri_1, "SECTION 3", "KEY 8", &my_variant_ptr));
    //    ma_variant_get_string_buffer(my_variant_ptr, &my_buffer);
    //    ma_buffer_get_string(my_buffer, &my_string, &my_string_size);
    //    TEST_ASSERT(0 == strcmp(my_string, "VALUE 8"));
    //    TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
    //    TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));

    //    TEST_ASSERT(MA_OK == ma_policy_bag_get_value(my_policy_bag, policy_uri_1, "SECTION 3", "KEY 10", &my_variant_ptr));
    //    ma_variant_get_string_buffer(my_variant_ptr, &my_buffer);
    //    ma_buffer_get_string(my_buffer, &my_string, &my_string_size);
    //    TEST_ASSERT(0 == strcmp(my_string, "VALUE 10"));
    //    TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
    //    TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));

    //    // Valid scenarios (URI 2)
    //    TEST_ASSERT(MA_OK == ma_policy_bag_get_value(my_policy_bag, policy_uri_2, "SECTION 2", "KEY 3", &my_variant_ptr));
    //    ma_variant_get_string_buffer(my_variant_ptr, &my_buffer);
    //    ma_buffer_get_string(my_buffer, &my_string, &my_string_size);
    //    TEST_ASSERT(0 == strcmp(my_string, "VALUE 3"));
    //    TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
    //    TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));

    //    TEST_ASSERT(MA_OK == ma_policy_bag_get_value(my_policy_bag, policy_uri_2, "SECTION 2", "KEY 5", &my_variant_ptr));
    //    ma_variant_get_string_buffer(my_variant_ptr, &my_buffer);
    //    ma_buffer_get_string(my_buffer, &my_string, &my_string_size);
    //    TEST_ASSERT(0 == strcmp(my_string, "VALUE 5"));
    //    TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
    //    TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));

    //    TEST_ASSERT(MA_OK == ma_policy_bag_get_value(my_policy_bag, policy_uri_2, "SECTION 3", "KEY 7", &my_variant_ptr));
    //    ma_variant_get_string_buffer(my_variant_ptr, &my_buffer);
    //    ma_buffer_get_string(my_buffer, &my_string, &my_string_size);
    //    TEST_ASSERT(0 == strcmp(my_string, "VALUE 7"));
    //    TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
    //    TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));

    //    TEST_ASSERT(MA_OK == ma_policy_bag_get_value(my_policy_bag, policy_uri_2, "SECTION 4", "KEY 9", &my_variant_ptr));
    //    ma_variant_get_string_buffer(my_variant_ptr, &my_buffer);
    //    ma_buffer_get_string(my_buffer, &my_string, &my_string_size);
    //    TEST_ASSERT(0 == strcmp(my_string, "VALUE 9"));
    //    TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
    //    TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));
    //}

    // Retrieving values from the Policy bag for verification (through iterator)
    //{
    //    ma_policy_uri_t *policy_uri = NULL;
    //    const char *section = NULL;
    //    const char *key = NULL;

    //    ma_policy_bag_iterator_t *iterator = NULL;

    //    // Creating iterator
    //    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_bag_get_iterator(NULL, &iterator));
    //    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_bag_get_iterator(my_policy_bag, NULL));
    //    TEST_ASSERT(MA_OK == ma_policy_bag_get_iterator(my_policy_bag, &iterator));

    //    // Iterating through elements
    //    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_bag_iterator_get_next(NULL, &policy_uri, &section, &key, &my_variant_ptr));
    //    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_bag_iterator_get_next(iterator, NULL, &section, &key, &my_variant_ptr));
    //    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_bag_iterator_get_next(iterator, &policy_uri, NULL, &key, &my_variant_ptr));
    //    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_bag_iterator_get_next(iterator, &policy_uri, &section, NULL, &my_variant_ptr));
    //    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_bag_iterator_get_next(iterator, &policy_uri, &section, &key, NULL));

    //    // Retrieving Element 1
    //    TEST_ASSERT(MA_OK == ma_policy_bag_iterator_get_next(iterator, &policy_uri, &section, &key, &my_variant_ptr));
    //    {
    //        ma_buffer_t *my_buffer = NULL;
    //        const char *my_string = NULL;
    //        size_t my_string_size = 0;

    //        char *value = NULL;

    //        TEST_ASSERT(MA_OK == ma_policy_uri_get_feature(policy_uri, &value));
    //        TEST_ASSERT(0 == strcmp(value, "My Policy Features 1"));

    //        TEST_ASSERT(MA_OK == ma_policy_uri_get_category(policy_uri, &value));
    //        TEST_ASSERT(0 == strcmp(value, "Special Category 1"));

    //        TEST_ASSERT(MA_OK  == ma_policy_uri_get_type(policy_uri, &value));
    //        TEST_ASSERT(0 == strcmp(value, "No Identification 1"));

    //        TEST_ASSERT(MA_OK == ma_policy_uri_get_name(policy_uri, &value));
    //        TEST_ASSERT(0 == strcmp(value, "First Policy URI 1"));

    //        TEST_ASSERT(0 == strcmp(section, "SECTION 1"));
    //        TEST_ASSERT(0 == strcmp(key, "KEY 1"));

    //        ma_variant_get_string_buffer(my_variant_ptr, &my_buffer);
    //        ma_buffer_get_string(my_buffer, &my_string, &my_string_size);
    //        TEST_ASSERT(0 == strcmp(my_string, "VALUE 1"));
    //        TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
    //    }

    //    // Retrieving Element 2
    //    TEST_ASSERT(MA_OK == ma_policy_bag_iterator_get_next(iterator, &policy_uri, &section, &key, &my_variant_ptr));
    //    {
    //        ma_buffer_t *my_buffer = NULL;
    //        const char *my_string = NULL;
    //        size_t my_string_size = 0;

    //        char *value = NULL;

    //        TEST_ASSERT(MA_OK == ma_policy_uri_get_feature(policy_uri, &value));
    //        TEST_ASSERT(0 == strcmp(value, "My Policy Features 1"));

    //        TEST_ASSERT(MA_OK == ma_policy_uri_get_category(policy_uri, &value));
    //        TEST_ASSERT(0 == strcmp(value, "Special Category 1"));

    //        TEST_ASSERT(MA_OK  == ma_policy_uri_get_type(policy_uri, &value));
    //        TEST_ASSERT(0 == strcmp(value, "No Identification 1"));

    //        TEST_ASSERT(MA_OK == ma_policy_uri_get_name(policy_uri, &value));
    //        TEST_ASSERT(0 == strcmp(value, "First Policy URI 1"));

    //        TEST_ASSERT(0 == strcmp(section, "SECTION 1"));
    //        TEST_ASSERT(0 == strcmp(key, "KEY 4"));

    //        ma_variant_get_string_buffer(my_variant_ptr, &my_buffer);
    //        ma_buffer_get_string(my_buffer, &my_string, &my_string_size);
    //        TEST_ASSERT(0 == strcmp(my_string, "VALUE 4"));
    //        TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
    //    }

    //    // Retrieving Element 3
    //    TEST_ASSERT(MA_OK == ma_policy_bag_iterator_get_next(iterator, &policy_uri, &section, &key, &my_variant_ptr));
    //    {
    //        ma_buffer_t *my_buffer = NULL;
    //        const char *my_string = NULL;
    //        size_t my_string_size = 0;

    //        char *value = NULL;

    //        TEST_ASSERT(MA_OK == ma_policy_uri_get_feature(policy_uri, &value));
    //        TEST_ASSERT(0 == strcmp(value, "My Policy Features 1"));

    //        TEST_ASSERT(MA_OK == ma_policy_uri_get_category(policy_uri, &value));
    //        TEST_ASSERT(0 == strcmp(value, "Special Category 1"));

    //        TEST_ASSERT(MA_OK  == ma_policy_uri_get_type(policy_uri, &value));
    //        TEST_ASSERT(0 == strcmp(value, "No Identification 1"));

    //        TEST_ASSERT(MA_OK == ma_policy_uri_get_name(policy_uri, &value));
    //        TEST_ASSERT(0 == strcmp(value, "First Policy URI 1"));

    //        TEST_ASSERT(0 == strcmp(section, "SECTION 2"));
    //        TEST_ASSERT(0 == strcmp(key, "KEY 2"));

    //        ma_variant_get_string_buffer(my_variant_ptr, &my_buffer);
    //        ma_buffer_get_string(my_buffer, &my_string, &my_string_size);
    //        TEST_ASSERT(0 == strcmp(my_string, "VALUE 2"));
    //        TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
    //    }

    //    // Retrieving Element 4
    //    TEST_ASSERT(MA_OK == ma_policy_bag_iterator_get_next(iterator, &policy_uri, &section, &key, &my_variant_ptr));
    //    {
    //        ma_buffer_t *my_buffer = NULL;
    //        const char *my_string = NULL;
    //        size_t my_string_size = 0;

    //        char *value = NULL;

    //        TEST_ASSERT(MA_OK == ma_policy_uri_get_feature(policy_uri, &value));
    //        TEST_ASSERT(0 == strcmp(value, "My Policy Features 1"));

    //        TEST_ASSERT(MA_OK == ma_policy_uri_get_category(policy_uri, &value));
    //        TEST_ASSERT(0 == strcmp(value, "Special Category 1"));

    //        TEST_ASSERT(MA_OK  == ma_policy_uri_get_type(policy_uri, &value));
    //        TEST_ASSERT(0 == strcmp(value, "No Identification 1"));

    //        TEST_ASSERT(MA_OK == ma_policy_uri_get_name(policy_uri, &value));
    //        TEST_ASSERT(0 == strcmp(value, "First Policy URI 1"));

    //        TEST_ASSERT(0 == strcmp(section, "SECTION 3"));
    //        TEST_ASSERT(0 == strcmp(key, "KEY 10"));

    //        ma_variant_get_string_buffer(my_variant_ptr, &my_buffer);
    //        ma_buffer_get_string(my_buffer, &my_string, &my_string_size);

    //        TEST_ASSERT(0 == strcmp(my_string, "VALUE 10"));
    //        TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
    //    }

    //    // Retrieving Element 5
    //    TEST_ASSERT(MA_OK == ma_policy_bag_iterator_get_next(iterator, &policy_uri, &section, &key, &my_variant_ptr));
    //    {
    //        ma_buffer_t *my_buffer = NULL;
    //        const char *my_string = NULL;
    //        size_t my_string_size = 0;

    //        char *value = NULL;

    //        TEST_ASSERT(MA_OK == ma_policy_uri_get_feature(policy_uri, &value));
    //        TEST_ASSERT(0 == strcmp(value, "My Policy Features 1"));

    //        TEST_ASSERT(MA_OK == ma_policy_uri_get_category(policy_uri, &value));
    //        TEST_ASSERT(0 == strcmp(value, "Special Category 1"));

    //        TEST_ASSERT(MA_OK  == ma_policy_uri_get_type(policy_uri, &value));
    //        TEST_ASSERT(0 == strcmp(value, "No Identification 1"));

    //        TEST_ASSERT(MA_OK == ma_policy_uri_get_name(policy_uri, &value));
    //        TEST_ASSERT(0 == strcmp(value, "First Policy URI 1"));

    //        TEST_ASSERT(0 == strcmp(section, "SECTION 3"));
    //        TEST_ASSERT(0 == strcmp(key, "KEY 6"));

    //        ma_variant_get_string_buffer(my_variant_ptr, &my_buffer);
    //        ma_buffer_get_string(my_buffer, &my_string, &my_string_size);
    //        TEST_ASSERT(0 == strcmp(my_string, "VALUE 6"));
    //        TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
    //    }

    //    // Retrieving Element 6
    //    TEST_ASSERT(MA_OK == ma_policy_bag_iterator_get_next(iterator, &policy_uri, &section, &key, &my_variant_ptr));
    //    {
    //        ma_buffer_t *my_buffer = NULL;
    //        const char *my_string = NULL;
    //        size_t my_string_size = 0;

    //        char *value = NULL;

    //        TEST_ASSERT(MA_OK == ma_policy_uri_get_feature(policy_uri, &value));
    //        TEST_ASSERT(0 == strcmp(value, "My Policy Features 1"));

    //        TEST_ASSERT(MA_OK == ma_policy_uri_get_category(policy_uri, &value));
    //        TEST_ASSERT(0 == strcmp(value, "Special Category 1"));

    //        TEST_ASSERT(MA_OK  == ma_policy_uri_get_type(policy_uri, &value));
    //        TEST_ASSERT(0 == strcmp(value, "No Identification 1"));

    //        TEST_ASSERT(MA_OK == ma_policy_uri_get_name(policy_uri, &value));
    //        TEST_ASSERT(0 == strcmp(value, "First Policy URI 1"));

    //        TEST_ASSERT(0 == strcmp(section, "SECTION 3"));
    //        TEST_ASSERT(0 == strcmp(key, "KEY 8"));

    //        ma_variant_get_string_buffer(my_variant_ptr, &my_buffer);
    //        ma_buffer_get_string(my_buffer, &my_string, &my_string_size);
    //        TEST_ASSERT(0 == strcmp(my_string, "VALUE 8"));
    //        TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
    //    }

    //    // Retrieving Element 7
    //    TEST_ASSERT(MA_OK == ma_policy_bag_iterator_get_next(iterator, &policy_uri, &section, &key, &my_variant_ptr));
    //    {
    //        ma_buffer_t *my_buffer = NULL;
    //        const char *my_string = NULL;
    //        size_t my_string_size = 0;

    //        char *value = NULL;

    //        TEST_ASSERT(MA_OK == ma_policy_uri_get_feature(policy_uri, &value));
    //        TEST_ASSERT(0 == strcmp(value, "My Policy Features 2"));

    //        TEST_ASSERT(MA_OK == ma_policy_uri_get_category(policy_uri, &value));
    //        TEST_ASSERT(0 == strcmp(value, "Special Category 2"));

    //        TEST_ASSERT(MA_OK  == ma_policy_uri_get_type(policy_uri, &value));
    //        TEST_ASSERT(0 == strcmp(value, "No Identification 2"));

    //        TEST_ASSERT(MA_OK == ma_policy_uri_get_name(policy_uri, &value));
    //        TEST_ASSERT(0 == strcmp(value, "First Policy URI 2"));

    //        TEST_ASSERT(0 == strcmp(section, "SECTION 2"));
    //        TEST_ASSERT(0 == strcmp(key, "KEY 3"));

    //        ma_variant_get_string_buffer(my_variant_ptr, &my_buffer);
    //        ma_buffer_get_string(my_buffer, &my_string, &my_string_size);
    //        TEST_ASSERT(0 == strcmp(my_string, "VALUE 3"));
    //        TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
    //    }

    //    // Retrieving Element 8
    //    TEST_ASSERT(MA_OK == ma_policy_bag_iterator_get_next(iterator, &policy_uri, &section, &key, &my_variant_ptr));
    //    {
    //        ma_buffer_t *my_buffer = NULL;
    //        const char *my_string = NULL;
    //        size_t my_string_size = 0;

    //        char *value = NULL;

    //        TEST_ASSERT(MA_OK == ma_policy_uri_get_feature(policy_uri, &value));
    //        TEST_ASSERT(0 == strcmp(value, "My Policy Features 2"));

    //        TEST_ASSERT(MA_OK == ma_policy_uri_get_category(policy_uri, &value));
    //        TEST_ASSERT(0 == strcmp(value, "Special Category 2"));

    //        TEST_ASSERT(MA_OK  == ma_policy_uri_get_type(policy_uri, &value));
    //        TEST_ASSERT(0 == strcmp(value, "No Identification 2"));

    //        TEST_ASSERT(MA_OK == ma_policy_uri_get_name(policy_uri, &value));
    //        TEST_ASSERT(0 == strcmp(value, "First Policy URI 2"));

    //        TEST_ASSERT(0 == strcmp(section, "SECTION 2"));
    //        TEST_ASSERT(0 == strcmp(key, "KEY 5"));

    //        ma_variant_get_string_buffer(my_variant_ptr, &my_buffer);
    //        ma_buffer_get_string(my_buffer, &my_string, &my_string_size);
    //        TEST_ASSERT(0 == strcmp(my_string, "VALUE 5"));
    //        TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
    //    }

    //    // Retrieving Element 9
    //    TEST_ASSERT(MA_OK == ma_policy_bag_iterator_get_next(iterator, &policy_uri, &section, &key, &my_variant_ptr));
    //    {
    //        ma_buffer_t *my_buffer = NULL;
    //        const char *my_string = NULL;
    //        size_t my_string_size = 0;

    //        char *value = NULL;

    //        TEST_ASSERT(MA_OK == ma_policy_uri_get_feature(policy_uri, &value));
    //        TEST_ASSERT(0 == strcmp(value, "My Policy Features 2"));

    //        TEST_ASSERT(MA_OK == ma_policy_uri_get_category(policy_uri, &value));
    //        TEST_ASSERT(0 == strcmp(value, "Special Category 2"));

    //        TEST_ASSERT(MA_OK  == ma_policy_uri_get_type(policy_uri, &value));
    //        TEST_ASSERT(0 == strcmp(value, "No Identification 2"));

    //        TEST_ASSERT(MA_OK == ma_policy_uri_get_name(policy_uri, &value));
    //        TEST_ASSERT(0 == strcmp(value, "First Policy URI 2"));

    //        TEST_ASSERT(0 == strcmp(section, "SECTION 3"));
    //        TEST_ASSERT(0 == strcmp(key, "KEY 7"));

    //        ma_variant_get_string_buffer(my_variant_ptr, &my_buffer);
    //        ma_buffer_get_string(my_buffer, &my_string, &my_string_size);
    //        TEST_ASSERT(0 == strcmp(my_string, "VALUE 7"));
    //        TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
    //    }

    //    // Retrieving Element 10
    //    TEST_ASSERT(MA_OK == ma_policy_bag_iterator_get_next(iterator, &policy_uri, &section, &key, &my_variant_ptr));
    //    {
    //        ma_buffer_t *my_buffer = NULL;
    //        const char *my_string = NULL;
    //        size_t my_string_size = 0;

    //        char *value = NULL;

    //        TEST_ASSERT(MA_OK == ma_policy_uri_get_feature(policy_uri, &value));
    //        TEST_ASSERT(0 == strcmp(value, "My Policy Features 2"));

    //        TEST_ASSERT(MA_OK == ma_policy_uri_get_category(policy_uri, &value));
    //        TEST_ASSERT(0 == strcmp(value, "Special Category 2"));

    //        TEST_ASSERT(MA_OK  == ma_policy_uri_get_type(policy_uri, &value));
    //        TEST_ASSERT(0 == strcmp(value, "No Identification 2"));

    //        TEST_ASSERT(MA_OK == ma_policy_uri_get_name(policy_uri, &value));
    //        TEST_ASSERT(0 == strcmp(value, "First Policy URI 2"));

    //        TEST_ASSERT(0 == strcmp(section, "SECTION 4"));
    //        TEST_ASSERT(0 == strcmp(key, "KEY 9"));

    //        ma_variant_get_string_buffer(my_variant_ptr, &my_buffer);
    //        ma_buffer_get_string(my_buffer, &my_string, &my_string_size);
    //        TEST_ASSERT(0 == strcmp(my_string, "VALUE 9"));
    //        TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
    //    }

    //    // No more items in the list
    //    TEST_ASSERT(MA_ERROR_NO_MORE_ITEMS == ma_policy_bag_iterator_get_next(iterator, &policy_uri, &section, &key, &my_variant_ptr));

    //    // Releasing iterator
    //    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_bag_iterator_release(NULL));
    //    TEST_ASSERT(MA_OK == ma_policy_bag_iterator_release(iterator));
    //}

    TEST_ASSERT(MA_OK == ma_policy_bag_release(my_policy_bag));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_deinitialize(dispatcher_handle));
}

