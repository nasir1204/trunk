#include "ma/ma_common.h"
#include "ma/internal/ma_macros.h"
#include "dispatcher_tests_utils.h"

#include <string.h>


#define DISPATCHER_PROUDUCT_ID "ma.dispatcher.test"

#ifdef MA_WINDOWS
# include <crtdbg.h>
#include <Windows.h>
# define MA_TRACKLEAKS _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF)
#define SOFTWARE_ID_PATH "SOFTWARE\\McAfee\\Agent\\Applications\\EPOAGENT3000"
#define SOFTWARE_ID_PATH1 "SOFTWARE\\McAfee\\Agent\\Applications\\EPOAGENT3000"
#define MA_DISPATCHER_PRODUCT_INSTAL_PATH "install_path"
#define MA_DISPATCHER_RSDK_PATH "RuntimeSdkPath"
#else
# define MA_TRACKLEAKS ((void *)0)
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#define SOFTWARE_ID_PATH "/etc/ma.d"
#endif

ma_error_t ma_dispatcher_test_create_registry_path(const char *registry_key)
{
    ma_error_t rc = MA_OK;
#ifdef MA_WINDOWS
    HKEY hKey = NULL;
    DWORD dwReturn;
    //char szBuffer[MAX_PATH] = "C:\\McAfee\\ma\\trunk\\build\\msvc\\Release";
	char szBuffer[MAX_PATH];
    dwReturn = GetCurrentDirectoryA(MAX_PATH,szBuffer);
	
    if (ERROR_SUCCESS == (rc = RegCreateKeyExA(HKEY_LOCAL_MACHINE, registry_key, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hKey, NULL)))
    {
        RegCloseKey(hKey);
	}

	
	
        /* Write the broker reg keys into Alternate Registry View. This will enalbe 32 or 64 bit service read 32 or 64 bit broker reg keys */
	
    if (ERROR_SUCCESS == rc) {
#if defined(WIN64)
        if (ERROR_SUCCESS == (rc = RegCreateKeyExA(HKEY_LOCAL_MACHINE, registry_key, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS | KEY_WOW64_32KEY, NULL, &hKey, NULL))) {
            RegCloseKey(hKey);
            if (ERROR_SUCCESS == (rc = RegOpenKeyExA(HKEY_LOCAL_MACHINE, registry_key, 0, KEY_READ|KEY_WRITE|KEY_WOW64_32KEY, &hKey))) {

#else
        if (ERROR_SUCCESS == (rc = RegCreateKeyExA(HKEY_LOCAL_MACHINE, registry_key, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS | KEY_WOW64_64KEY, NULL, &hKey, NULL))) {
            RegCloseKey(hKey);
            if (ERROR_SUCCESS == (rc = RegOpenKeyExA(HKEY_LOCAL_MACHINE, registry_key, 0, KEY_READ|KEY_WRITE|KEY_WOW64_64KEY, &hKey))) {
#endif
               
            }
        }
    }
#else
    char product_dir[MA_MAX_LEN] = {0};
    int status = 0;
    struct stat st;
    const static char *config_file = "config.xml";
    FILE *fp = NULL;

    sprintf(product_dir, "%s%c%s", SOFTWARE_ID_PATH,MA_PATH_SEPARATOR,registry_key);
    status = stat(product_dir, &st);
    if(-1 == status) {
        char command[MA_MAX_LEN] = {0};
        //create directory
        sprintf(command, "mkdir -p %s", product_dir);
        system(command);
    }

#endif
	return rc;
}



ma_error_t ma_dispatcher_test_create_registry(const char *registry_key)
{
    ma_error_t rc = MA_OK;
#ifdef MA_WINDOWS
    HKEY hKey = NULL;
    DWORD dwReturn;
    //char szBuffer[MAX_PATH] = "C:\\McAfee\\ma\\trunk\\build\\msvc\\Release";
	char szBuffer[MAX_PATH];
    dwReturn = GetCurrentDirectoryA(MAX_PATH,szBuffer);
	
    if (ERROR_SUCCESS == (rc = RegCreateKeyExA(HKEY_LOCAL_MACHINE, registry_key, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hKey, NULL)))
    {
        RegCloseKey(hKey);
		if (ERROR_SUCCESS == (rc = RegOpenKeyExA(HKEY_LOCAL_MACHINE, registry_key, 0, KEY_READ|KEY_WRITE, &hKey)))
        {                
			rc = RegSetValueExA(hKey, MA_DISPATCHER_PRODUCT_INSTAL_PATH, 0, REG_SZ, (const BYTE *)szBuffer, (DWORD)(strlen(szBuffer) + 1));
			rc = RegSetValueExA(hKey, MA_DISPATCHER_RSDK_PATH, 0, REG_SZ, (const BYTE *)szBuffer, (DWORD)(strlen(szBuffer) + 1));

			RegCloseKey(hKey);
        }
    }

	
	
        /* Write the broker reg keys into Alternate Registry View. This will enalbe 32 or 64 bit service read 32 or 64 bit broker reg keys */
	
    if (ERROR_SUCCESS == rc) {
#if defined(WIN64)
        if (ERROR_SUCCESS == (rc = RegCreateKeyExA(HKEY_LOCAL_MACHINE, registry_key, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS | KEY_WOW64_32KEY, NULL, &hKey, NULL))) {
            RegCloseKey(hKey);
            if (ERROR_SUCCESS == (rc = RegOpenKeyExA(HKEY_LOCAL_MACHINE, registry_key, 0, KEY_READ|KEY_WRITE|KEY_WOW64_32KEY, &hKey))) {

#else
        if (ERROR_SUCCESS == (rc = RegCreateKeyExA(HKEY_LOCAL_MACHINE, registry_key, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS | KEY_WOW64_64KEY, NULL, &hKey, NULL))) {
            RegCloseKey(hKey);
            if (ERROR_SUCCESS == (rc = RegOpenKeyExA(HKEY_LOCAL_MACHINE, registry_key, 0, KEY_READ|KEY_WRITE|KEY_WOW64_64KEY, &hKey))) {
#endif
                rc = RegSetValueExA(hKey, MA_DISPATCHER_PRODUCT_INSTAL_PATH, 0, REG_SZ, (const BYTE *)szBuffer, (DWORD)(strlen(szBuffer) + 1));
			    rc = RegSetValueExA(hKey, MA_DISPATCHER_RSDK_PATH, 0, REG_SZ, (const BYTE *)szBuffer, (DWORD)(strlen(szBuffer) + 1));

                RegCloseKey(hKey);
            }
        }
    }
	
#else
    char product_dir[MA_MAX_LEN] = {0};
    int status = 0;
    struct stat st;
    const static char *config_file = "config.xml";
    FILE *fp = NULL;

    sprintf(product_dir, "%s%c%s", SOFTWARE_ID_PATH,MA_PATH_SEPARATOR,registry_key);
    status = stat(product_dir, &st);
    if(-1 == status) {
        char command[MA_MAX_LEN] = {0};
        //create directory
        sprintf(command, "mkdir -p %s", product_dir);
        system(command);
    }
    sprintf(product_dir, "%s%c%s", product_dir, MA_PATH_SEPARATOR, config_file);
    fp = fopen(product_dir, "r");
    if(!fp) {
        fp = fopen(product_dir, "w");
        if(!fp)return MA_ERROR_INVALIDARG;
        else {
            char config_start_tag[MA_MAX_LEN]= {0};
            char config_end_tag[MA_MAX_LEN]= {0};
            char current_dir[MA_MAX_LEN] = {0};

            strcpy(config_start_tag, "<?xml version=\"1.0\" encoding=\"UTF-8\"?> <SharedLibLocation>");
            strcpy(config_end_tag, "</SharedLibLocation>");
            fprintf(fp, "%s%s%s", config_start_tag, getcwd(current_dir, MA_MAX_LEN), config_end_tag);
            fflush(fp);

            strcpy(config_start_tag, "<?xml version=\"1.0\" encoding=\"UTF-8\"?> <RuntimeSdkPath>");
            strcpy(config_end_tag, "</RuntimeSdkPath>");
            fprintf(fp, "%s%s%s", config_start_tag, getcwd(current_dir, MA_MAX_LEN), config_end_tag);
            fflush(fp);

            strcpy(config_start_tag, "<?xml version=\"1.0\" encoding=\"UTF-8\"?> <install_path>");
            strcpy(config_end_tag, "</install_path>");
            fprintf(fp, "%s%s%s", config_start_tag, getcwd(current_dir, MA_MAX_LEN), config_end_tag);
            fflush(fp);

            fclose(fp);
            return rc;
        }
    }
    fclose(fp);
#endif
    return rc;
}

ma_error_t ma_dispatcher_tests_delete_registry(const char *key) {
#ifdef MA_WINDOWS
    RegDeleteKeyA(HKEY_LOCAL_MACHINE, key);
	return MA_OK;
#else
    int status = 0;
    char dir_path[MA_MAX_LEN] = {0};
    struct stat st;
    char command[MA_MAX_LEN] = {0};

    strncpy(dir_path, SOFTWARE_ID_PATH, strlen(SOFTWARE_ID_PATH));
    dir_path[strlen(SOFTWARE_ID_PATH)] = MA_PATH_SEPARATOR;
    strncat(dir_path, key, strlen(key));
    if(-1 == stat(dir_path, &st)) return MA_OK;
    sprintf(command, "%s %s", "rm -rf", dir_path);
    status = system(command);
    if(0 != status) {
        perror("system");
        return MA_ERROR_APIFAILED;
    }
	return MA_OK;
#endif
}