#include "unity.h"
#include "unity_fixture.h"

#include <uv.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "ma/ma_common.h"
#include "ma/ma_message.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_variant.h"
#include "ma/ma_dispatcher.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/core/msgbus/ma_message_header.h"
#include "ma/logger/ma_file_logger.h"
#include "ma/ma_errors.h"


#include "dispatcher_tests_utils.h"

#define DISPATCHER_PROUDUCT_ID "ma.dispatcher.test"

#ifdef MA_WINDOWS
# include <crtdbg.h>
#include <Windows.h>
# define MA_TRACKLEAKS _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF)
#define SOFTWARE_ID_PATH "SOFTWARE\\McAfee\\Agent\\Applications\\EPOAGENT3000"
#define SOFTWARE_ID_PATH1 "SOFTWARE\\McAfee\\Agent\\Applications\\EPOAGENT3000"
#define MA_DISPATCHER_PRODUCT_INSTAL_PATH "install_path"
#define MA_DISPATCHER_RSDK_PATH "RuntimeSdkPath"
#else
# define MA_TRACKLEAKS ((void *)0)
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#define SOFTWARE_ID_PATH "/etc/ma.d"
#endif


static ma_uint8_t cb_counter = 0;
static ma_uint8_t wcb_counter = 0;
static ma_uint8_t ma_array_cb_counter = 0;

TEST_GROUP_RUNNER(dispatcher_test_group) {
    
	do {RUN_TEST_CASE(dispatcher_tests, variant_init_deinit); }while(0);
    do {RUN_TEST_CASE(dispatcher_tests, variant_msgbus_init_deinit); }while(0);
    do {RUN_TEST_CASE(dispatcher_tests, variant_msgbus_init_deinit_mix); }while(0);
    do {RUN_TEST_CASE(dispatcher_tests, variant_msgbus_init_dispatcher_deinit); }while(0);
    do {RUN_TEST_CASE(dispatcher_tests, variant_no_init_but_deinit); }while(0);
    do {RUN_TEST_CASE(dispatcher_tests, dispatcher_init_deinit); }while(0);
    do {RUN_TEST_CASE(dispatcher_tests, get_function_address); }while(0);
    do {RUN_TEST_CASE(dispatcher_tests, variant_msgbus_different_product_id); }while(0);
    do {RUN_TEST_CASE(dispatcher_tests, msgbus_var_arg); }while(0);
    // Variant Library tests 
    do {RUN_TEST_CASE(dispatcher_tests, variant_create); }while(0);
    //do {RUN_TEST_CASE(dispatcher_tests, variant_msgbus_endpoint_create); }while(0);
    do {RUN_TEST_CASE(dispatcher_tests, variant_basic_types_tests); }while(0);
    do {RUN_TEST_CASE(dispatcher_tests, variant_array_tests); }while(0);
    do {RUN_TEST_CASE(dispatcher_tests, variant_table_tests); }while(0);
    do {RUN_TEST_CASE(dispatcher_tests, variant_create_get_from_raw_buffer); }while(0);
    do {RUN_TEST_CASE(dispatcher_tests, variant_buffer_tests); }while(0);
	
    ///*Message Bus library tests */
   // do {RUN_TEST_CASE(dispatcher_tests, msgbus_noinit_but_deinit); } while(0);
    //do {RUN_TEST_CASE(dispatcher_tests, msgbus_endpoint_setopt); }while(0);
    //do {RUN_TEST_CASE(dispatcher_tests, msgbus_endpoint_getopt); }while(0);
    //do {RUN_TEST_CASE(dispatcher_tests, msgbus_send_and_forget); }while(0);
    //do {RUN_TEST_CASE(dispatcher_tests, msgbus_sync_send); }while(0);
    //do {RUN_TEST_CASE(dispatcher_tests, msgbus_version); }while(0);
    //do {RUN_TEST_CASE(dispatcher_tests, message_variant_payload); }while(0);
    //do {RUN_TEST_CASE(dispatcher_tests, msgbus_pub_sub); }while(0);
    //do {RUN_TEST_CASE(dispatcher_tests, msgbus_async_send); }while(0);
    ///*Multithreading tests*/
    //crash in Team city, ma_tools need to be upgraded
    //do {RUN_TEST_CASE(dispatcher_tests, dispatcher_multithreading); }while(0);
}

/*
callback function for iterating through the table. This callback function
should be called 'N' times by the foreach iterator.
*/
static void test_table_callback(char const *table_key,
                        ma_variant_t *value,
                        void *cb_args,
						ma_bool_t *stop_loop)
{
    cb_counter++;
}

/*
callback function for iterating through the table. This callback function
should be called 'N' times by the foreach iterator.
*/
static void test_wtable_callback(wchar_t const *table_key,
                        ma_variant_t *value,
                        void *cb_args,
						ma_bool_t *stop_loop)
{
    wcb_counter++;
}

/*
callback function for iterating through the array. This callback function
should be called 'N' times by the foreach iterator.
*/
static void test_array_callback(size_t index,
                        ma_variant_t *value,
                        void *cb_args,
						ma_bool_t *stop_loop)
{
    ma_array_cb_counter++;
}


TEST_SETUP(dispatcher_tests) {
#ifdef MA_WINDOWS
    ma_dispatcher_test_create_registry(SOFTWARE_ID_PATH);
#else
    ma_dispatcher_test_create_registry("EPOAGENT3000");
#endif
}

TEST_TEAR_DOWN(dispatcher_tests) {
#ifdef MA_WINDOWS
   ma_dispatcher_tests_delete_registry(SOFTWARE_ID_PATH);
   ma_dispatcher_tests_delete_registry(SOFTWARE_ID_PATH1);
#else
    ma_dispatcher_tests_delete_registry("ma");
    ma_dispatcher_tests_delete_registry("ma1");
#endif
}

TEST(dispatcher_tests, get_function_address)
{
    ma_dispatcher_t *dispatcher = NULL;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_initialize(NULL, &dispatcher));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_deinitialize(dispatcher));
}

TEST(dispatcher_tests, dispatcher_init_deinit) {
    ma_dispatcher_t *dispatcher = NULL;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_initialize(NULL, &dispatcher));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_deinitialize(dispatcher));
}

TEST(dispatcher_tests, var_init_deinit) {
	ma_dispatcher_t *dispatcher = NULL;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_initialize(NULL, &dispatcher));
	/*ma_variant_init();
	ma_variant_deinit();*/
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_deinitialize(dispatcher));
}

TEST(dispatcher_tests, variant_init_deinit) {
    ma_dispatcher_t *handle = NULL;
    ma_variant_t    *v1 = NULL;
    ma_variant_t    *v3 = NULL;
    ma_variant_t    *v2 = NULL;
    ma_variant_t    *variant_ptr = NULL;
    ma_bool_t       result = MA_FALSE;
    ma_buffer_t *buffer_ptr = NULL;
    ma_wbuffer_t *wbuffer_ptr = NULL;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_initialize(NULL,&handle));
    //TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_init());
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create(&v1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create(&v3));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(v1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(v3));

    //string test
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_string("hello", 5, &v1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_string("hello", 5, &v2));

    TEST_ASSERT_MESSAGE(ma_variant_get_string_buffer(v1, &buffer_ptr) == MA_OK, \
                        "char String Buffer cannot be retrieved from the Variant");
    ma_buffer_release(buffer_ptr);

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_is_equal(v1, v2, &result));
    TEST_ASSERT_MESSAGE(MA_TRUE  == result, "strings variant are different");
    TEST_ASSERT_MESSAGE(ma_variant_release(v1) == MA_OK, "The variant could not be destroyed");
    TEST_ASSERT_MESSAGE(ma_variant_release(v2) == MA_OK, "The variant could not be destroyed");
    
    TEST_ASSERT_MESSAGE(ma_variant_create_from_wstring(L"MA Security Agent", 17, &variant_ptr) == MA_OK, \
                        "String variant could not be created");

    TEST_ASSERT_MESSAGE(ma_variant_get_wstring_buffer(variant_ptr, &wbuffer_ptr) == MA_OK, \
                        "Wide char String Buffer cannot be retrieved from the Variant");
    ma_wbuffer_release(wbuffer_ptr);
    TEST_ASSERT_MESSAGE(ma_variant_release(variant_ptr) == MA_OK, "The variant could not be destroyed");

    //TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_deinit());
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_deinitialize(handle));
}


TEST(dispatcher_tests, variant_no_init_but_deinit) {
    ma_dispatcher_t *handle = NULL;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_initialize(NULL,&handle));
    //TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_deinit());
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_deinitialize(handle));
}

TEST(dispatcher_tests, variant_msgbus_init_deinit) {
    ma_dispatcher_t *handle = NULL;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_initialize(NULL,&handle));
    //TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_init());
    
    //TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_deinit());
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_deinitialize(handle));
}

TEST(dispatcher_tests, variant_msgbus_init_deinit_mix) {
    ma_dispatcher_t *handle = NULL;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_initialize(NULL,&handle));
    //TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_init());
    
    //TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_deinit());
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_deinitialize(handle));
}

TEST(dispatcher_tests, variant_msgbus_init_dispatcher_deinit) {
    ma_dispatcher_t *handle = NULL;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_initialize(NULL,&handle));
    //TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_init());
    
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_deinitialize(handle));
}

TEST(dispatcher_tests, variant_msgbus_different_product_id) {
    ma_dispatcher_t *handle = NULL;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_initialize(NULL,&handle));
    //TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_init());
    
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_deinitialize(handle));
}

TEST(dispatcher_tests, msgbus_var_arg) {
    ma_dispatcher_t *handle = NULL;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_initialize(NULL,&handle));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_deinitialize(handle));
}


TEST(dispatcher_tests, variant_create) {
    ma_dispatcher_t *handle = NULL;
    ma_variant_t *variant_ptr = NULL;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_initialize(NULL,&handle));
    //ma_variant_init();
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create(&variant_ptr));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(variant_ptr));
    //TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_deinit());
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_deinitialize(handle));
}	

TEST(dispatcher_tests, variant_msgbus_endpoint_create) {
    ma_dispatcher_t *handle = NULL;
    ma_msgbus_endpoint_t *endpoint;
    const char service_name[] = "ma.msgbus.testservice";
    ma_msgbus_t *msgbus = NULL;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_initialize(NULL,&handle));
    //TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_init());
    
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_create(DISPATCHER_PROUDUCT_ID, &msgbus));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_start(msgbus));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_msgbus_endpoint_create(msgbus, service_name, NULL, &endpoint));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_endpoint_release(endpoint));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_stop(msgbus, MA_TRUE));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_release(msgbus));
    //TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_deinit());
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_deinitialize(handle));
}

TEST(dispatcher_tests,variant_basic_types_tests) {
    ma_dispatcher_t *handle = NULL;
    ma_variant_t *variant_ptr = NULL;
    ma_variant_t *variant_ptr_1 = NULL;
    ma_variant_t *variant_ptr_2 = NULL;
    ma_bool_t result_val = MA_FALSE;
    ma_vartype_t vartype_obj = MA_VARTYPE_STRING;

    ma_uint64_t expected_uint64_value = 0;
    ma_int64_t expected_int64_value = 0;
    ma_uint32_t expected_uint32_value = 0;
    ma_int32_t expected_int32_value = 0;
    ma_uint16_t expected_uint16_value = 0;
    ma_int16_t expected_int16_value = 0;
    ma_uint8_t expected_uint8_value = 0;
    ma_int8_t expected_int8_value = 0;
    ma_float_t expected_float_value = 0;
    ma_double_t expected_double_value = 0;
    ma_bool_t expected_bool_value = MA_FALSE;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_initialize(NULL,&handle));
    //TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_init());

    TEST_ASSERT_EQUAL_INT(MA_OK,ma_variant_create(&variant_ptr));
    TEST_ASSERT_MESSAGE(ma_variant_release(variant_ptr) == MA_OK, "The variant could not be destroyed");

    TEST_ASSERT_MESSAGE(ma_variant_create(&variant_ptr_1) == MA_OK, "NULL variant could not be created");
    TEST_ASSERT_MESSAGE(ma_variant_create(&variant_ptr_2) == MA_OK, "NULL variant could not be created");

    TEST_ASSERT_MESSAGE(ma_variant_is_equal(variant_ptr_1, variant_ptr_2, &result_val) == MA_OK,
                        "ma_variant_is_equal doesn't return MA_OK for valid variants");

    TEST_ASSERT_MESSAGE(ma_variant_release(variant_ptr_1) == MA_OK, "The variant could not be destroyed");
    TEST_ASSERT_MESSAGE(ma_variant_release(variant_ptr_2) == MA_OK, "The variant could not be destroyed");

    TEST_ASSERT_MESSAGE(ma_variant_create_from_uint64(0x123456789ULL, &variant_ptr) == MA_OK, \
                                            "64 Bit unsigned variant could not be created");
    TEST_ASSERT_MESSAGE(ma_variant_get_uint64(variant_ptr, &expected_uint64_value) == MA_OK, \
                            "ma_variant_get_uint64 could not retrieve 64 bit value from the variant");
    TEST_ASSERT_MESSAGE(0x123456789ULL == expected_uint64_value, "values does not match");

    TEST_ASSERT_MESSAGE(ma_variant_get_type(variant_ptr, &vartype_obj) == MA_OK, \
                        "ma_variant_get_type doesn't return MA_OK for unsigned 64-Bit integer Variant");

    TEST_ASSERT_MESSAGE(vartype_obj == MA_VARTYPE_UINT64, \
                        "ma_variant_get_type doesn't return MA_VARTYPE_UINT64 for unsigned 64-Bit integer Variant");

    TEST_ASSERT_MESSAGE(ma_variant_release(variant_ptr) == MA_OK, "The variant could not be destroyed");

    TEST_ASSERT_MESSAGE(ma_variant_create_from_int64(0x123456789LL, &variant_ptr) == MA_OK, \
                                            "64 Bit signed variant could not be created");
    TEST_ASSERT_MESSAGE(ma_variant_get_int64(variant_ptr, &expected_int64_value) == MA_OK, \
                            "ma_variant_get_int64 could not retrieve 64 bit value from the variant");
    TEST_ASSERT_MESSAGE(0x123456789LL == expected_int64_value, "values does not match");
    TEST_ASSERT_MESSAGE(ma_variant_release(variant_ptr) == MA_OK, "The variant could not be destroyed");

    TEST_ASSERT_MESSAGE(ma_variant_create_from_uint32(0xEFAB0123ULL, &variant_ptr) == MA_OK, \
                                            "32 Bit unsigned variant could not be created");
    TEST_ASSERT_MESSAGE(ma_variant_get_uint32(variant_ptr, &expected_uint32_value) == MA_OK, \
                            "ma_variant_get_uint32 could not retrieve 32 bit value from the variant");
    TEST_ASSERT_MESSAGE(0xEFAB0123ULL == expected_uint32_value, "values does not match");
    TEST_ASSERT_MESSAGE(ma_variant_release(variant_ptr) == MA_OK, "The variant could not be destroyed");

    TEST_ASSERT_MESSAGE(ma_variant_create_from_int32(0x12345678L, &variant_ptr) == MA_OK, \
                                            "32 Bit signed variant could not be created");
    TEST_ASSERT_MESSAGE(ma_variant_get_int32(variant_ptr, &expected_int32_value) == MA_OK, \
                            "ma_variant_get_int32 could not retrieve 32 bit value from the variant");
    TEST_ASSERT_MESSAGE(0x12345678L == expected_int32_value, "values does not match");
    TEST_ASSERT_MESSAGE(ma_variant_release(variant_ptr) == MA_OK, "The variant could not be destroyed");

    TEST_ASSERT_MESSAGE(ma_variant_create_from_uint16(0x1234, &variant_ptr) == MA_OK, \
                                            "16 Bit unsigned variant could not be created");
    TEST_ASSERT_MESSAGE(ma_variant_get_uint16(variant_ptr, &expected_uint16_value) == MA_OK, \
                            "ma_variant_get_uint16 could not retrieve 16 bit value from the variant");
    TEST_ASSERT_MESSAGE(0x1234 == expected_uint16_value, "values does not match");
    TEST_ASSERT_MESSAGE(ma_variant_release(variant_ptr) == MA_OK, "The variant could not be destroyed");

    TEST_ASSERT_MESSAGE(ma_variant_create_from_int16(0x0101, &variant_ptr) == MA_OK, \
                                            "16 Bit signed variant could not be created");
    TEST_ASSERT_MESSAGE(ma_variant_get_int16(variant_ptr, &expected_int16_value) == MA_OK, \
                            "ma_variant_get_int16 could not retrieve 16 bit value from the variant");
    TEST_ASSERT_MESSAGE(0x0101 == expected_int16_value, "values does not match");
    TEST_ASSERT_MESSAGE(ma_variant_release(variant_ptr) == MA_OK, "The variant could not be destroyed");

    TEST_ASSERT_MESSAGE(ma_variant_create_from_uint8(0x12U, &variant_ptr) == MA_OK, \
                                            "8 Bit unsigned variant could not be created");
    TEST_ASSERT_MESSAGE(ma_variant_get_uint8(variant_ptr, &expected_uint8_value) == MA_OK, \
                            "ma_variant_get_uint8 could not retrieve 8 bit value from the variant");
    TEST_ASSERT_MESSAGE(0x12U == expected_uint8_value, "values does not match");
    TEST_ASSERT_MESSAGE(ma_variant_release(variant_ptr) == MA_OK, "The variant could not be destroyed");

    TEST_ASSERT_MESSAGE(ma_variant_create_from_int8(0x0E, &variant_ptr) == MA_OK, \
                                            "8 Bit signed variant could not be created");
    TEST_ASSERT_MESSAGE(ma_variant_get_int8(variant_ptr, &expected_int8_value) == MA_OK, \
                            "ma_variant_get_int8 could not retrieve 8 bit value from the variant");
    TEST_ASSERT_MESSAGE(0x0E == expected_int8_value, "values does not match");
    TEST_ASSERT_MESSAGE(ma_variant_release(variant_ptr) == MA_OK, "The variant could not be destroyed");

    TEST_ASSERT_MESSAGE(ma_variant_create_from_bool(MA_TRUE, &variant_ptr) == MA_OK, \
                        "Boolean variant could not be created");
    TEST_ASSERT_MESSAGE(ma_variant_get_bool(variant_ptr, &expected_bool_value) == MA_OK, \
                        "ma_variant_get_bool could not retrieve boolean value from the variant");
    TEST_ASSERT_MESSAGE(MA_TRUE == expected_bool_value, "values does not match");
    TEST_ASSERT_MESSAGE(ma_variant_release(variant_ptr) == MA_OK, "The variant could not be destroyed");

    TEST_ASSERT_MESSAGE(ma_variant_create_from_float(123.45f, &variant_ptr) == MA_OK, \
                        "Float variant could not be created");
    TEST_ASSERT_MESSAGE(ma_variant_get_float(variant_ptr, &expected_float_value) == MA_OK, \
                        "ma_variant_get_float could not retrieve float value from the variant");
    TEST_ASSERT_MESSAGE(123.45f == expected_float_value, \
                        "ma_variant_get_float could not retrieve float value from the variant");
    TEST_ASSERT_MESSAGE(ma_variant_release(variant_ptr) == MA_OK, "The variant could not be destroyed");

    TEST_ASSERT_MESSAGE(ma_variant_create_from_double(1.001e11, &variant_ptr) == MA_OK, \
                        "Double variant could not be created");
    TEST_ASSERT_MESSAGE(ma_variant_get_double(variant_ptr, &expected_double_value) == MA_OK, \
                        "ma_variant_get_double could not retrieve double value from the variant");
    TEST_ASSERT_MESSAGE(1.001e11 == expected_double_value, \
                        "ma_variant_get_double could not retrieve double value from the variant");
    TEST_ASSERT_MESSAGE(ma_variant_release(variant_ptr) == MA_OK, "The variant could not be destroyed");

    //TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_deinit());
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_deinitialize(handle));
}

TEST(dispatcher_tests,variant_array_tests) {
    ma_dispatcher_t *handle = NULL;
    ma_variant_t *variant_ptr = NULL;
    ma_variant_t *variant_array_ptr = NULL;
    ma_array_t *array_ptr_1 = NULL;
    ma_array_t *array_ptr_2 = NULL;
    ma_array_t *array_variant_ptr = NULL;
    size_t array_size = 0;
    ma_bool_t comparison_result = 0;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_initialize(NULL,&handle));
    //TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_init());

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_array_create(&array_ptr_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_array_create(&array_ptr_2));

    TEST_ASSERT_MESSAGE(ma_variant_create(&variant_ptr) == MA_OK, "NULL variant could not be created");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_1, variant_ptr) == MA_OK, "Variant could not be pushed to array");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr_2, variant_ptr) == MA_OK, "Variant could not be pushed to array");
    TEST_ASSERT_MESSAGE(ma_array_size(array_ptr_1, &array_size) == MA_OK, "ma_array_size doesn't returns MA_OK for valid arguments");
    TEST_ASSERT_MESSAGE(1 == array_size, "ma_array_size doesn't return correct array size");
    TEST_ASSERT_MESSAGE(ma_array_is_equal(array_ptr_1, array_ptr_2, &comparison_result) == MA_OK, \
                        "ma_array_is_equal doesn't return MA_OK for empty arrays");
    TEST_ASSERT_MESSAGE(MA_TRUE == comparison_result, "Array Comparison result is not correct for empty arrays");
    TEST_ASSERT_MESSAGE(ma_variant_release(variant_ptr) == MA_OK, "The variant could not be destroyed");
    TEST_ASSERT_MESSAGE(ma_variant_release(variant_ptr) == MA_OK, "The variant could not be destroyed");

    TEST_ASSERT_MESSAGE(ma_array_add_ref(NULL) == MA_ERROR_INVALIDARG, \
                        "ma_variant_inc_ref doesn't return MA_ERROR_INVALIDARG when argument passed is NULL");

    TEST_ASSERT_MESSAGE(ma_array_get_element_at(array_ptr_1, 1, &variant_ptr) == MA_OK, \
                        "ma_array_get_element_at couldn't retrieve variant from index 0 of the array");
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(variant_ptr));

    TEST_ASSERT_MESSAGE(ma_array_foreach(array_ptr_1, test_array_callback, NULL) == MA_OK,
                        "ma_array_foreach doesn't return MA_OK when array and callback functions are valid");

    TEST_ASSERT_MESSAGE(ma_array_cb_counter == 1, "Call back function is not called in each iteration of foreach iterator");

    ma_variant_release(variant_ptr);

    
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_array(array_ptr_1, &variant_array_ptr));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_get_array(variant_array_ptr, &array_variant_ptr));
    TEST_ASSERT_MESSAGE(array_variant_ptr == array_ptr_1, "getting array from variant failed");

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_array_release(array_variant_ptr));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_array_release(array_ptr_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_array_release(array_ptr_2));

    //TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_deinit());
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_deinitialize(handle));
}

TEST(dispatcher_tests,variant_table_tests) {
    ma_dispatcher_t *handle = NULL;
    ma_table_t *table_ptr_1 = NULL;
    ma_table_t *table_ptr_2 = NULL;
    ma_table_t *table_variant_ptr = NULL;
    ma_variant_t *variant_ptr = NULL;
    ma_variant_t *variant_table_ptr = NULL;
    ma_variant_t *variant_ptr_1 = NULL;
    ma_variant_t *variant_ptr_2 = NULL;
    ma_array_t *keys = NULL;
    ma_vartype_t vartype_obj = MA_VARTYPE_NULL;
    size_t table_size = 0;
    ma_bool_t comparison_result = 0;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_initialize(NULL,&handle));
    //ma_variant_init();
    TEST_ASSERT_MESSAGE(ma_table_create(NULL) == MA_ERROR_INVALIDARG,"NULL is passed so invalid argument for ma_table_create()");
    TEST_ASSERT_MESSAGE(ma_table_create(&table_ptr_1) == MA_OK,"ma_table_create() doesnt return MA_OK if a valid handle is passed to it");
    TEST_ASSERT_MESSAGE(ma_table_create(&table_ptr_2) == MA_OK,"ma_table_create() doesnt return MA_OK if a valid handle is passed to it");

    TEST_ASSERT_MESSAGE(ma_variant_create_from_int32(0xEFAB0123LL, &variant_ptr_1) == MA_OK, "32 Bit signed variant could not be created");
    TEST_ASSERT_MESSAGE(ma_variant_create_from_int32(0xEFAB0123LL, &variant_ptr_2) == MA_OK, "32 Bit signed variant could not be created");
    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_1, "1", variant_ptr_1) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_entry(table_ptr_2, "1", variant_ptr_2) == MA_OK, "Variant could not be added to table");

    TEST_ASSERT_MESSAGE(ma_table_add_wentry(table_ptr_1, L"2", variant_ptr_1) == MA_OK, "Variant could not be added to table");
    TEST_ASSERT_MESSAGE(ma_table_add_wentry(table_ptr_2, L"2", variant_ptr_2) == MA_OK, "Variant could not be added to table");

    TEST_ASSERT_MESSAGE(ma_table_size(table_ptr_1, &table_size) == MA_OK, "ma_table_size doesn't returns MA_OK for valid arguments");
    TEST_ASSERT_MESSAGE(table_size == 2, "table size is unequal");

    TEST_ASSERT_MESSAGE(ma_table_is_equal(table_ptr_1, table_ptr_2, &comparison_result) == MA_OK, "ma_table_is_equal doesn't return MA_OK for tables");
    TEST_ASSERT_MESSAGE(MA_TRUE == comparison_result, "Table Comparison result is not correct for tables");

    TEST_ASSERT_MESSAGE(ma_table_add_ref(NULL) == MA_ERROR_INVALIDARG, \
                        "ma_table_inc_ref doesn't return MA_ERROR_INVALIDARG when argument passed is NULL");

    TEST_ASSERT_MESSAGE(ma_table_get_keys(table_ptr_1, &keys)== MA_OK, "ma_table_get_keys doesn't return MA_OK for valid arguments");
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_array_release(keys));

    TEST_ASSERT_MESSAGE(ma_table_get_value(table_ptr_1, "1", &variant_ptr) == MA_OK, \
                        "ma_table_get_value couldn't retrieve variant from table");

    TEST_ASSERT_MESSAGE(ma_variant_get_type(variant_ptr, &vartype_obj) == MA_OK, \
                        "ma_variant_get_type doesn't return MA_OK for valid Variant");

    TEST_ASSERT_MESSAGE(vartype_obj == MA_VARTYPE_INT32, \
                        "int32 variant should have been present in table, but it is not");
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(variant_ptr));

    TEST_ASSERT_MESSAGE(ma_table_get_wvalue(table_ptr_1, L"2", &variant_ptr) == MA_OK, \
                        "ma_table_get_value couldn't retrieve variant from table");

    TEST_ASSERT_MESSAGE(ma_variant_get_type(variant_ptr, &vartype_obj) == MA_OK, \
                        "ma_variant_get_type doesn't return MA_OK for valid Variant");

    TEST_ASSERT_MESSAGE(vartype_obj == MA_VARTYPE_INT32, \
                        "int32 variant should have been present in table, but it is not");

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(variant_ptr));

    cb_counter = 0;
    TEST_ASSERT_MESSAGE(ma_table_foreach(table_ptr_1, test_table_callback, NULL) == MA_OK,
                        "ma_table_foreach doesn't return MA_OK when table and callback functions are valid");
    TEST_ASSERT_MESSAGE(cb_counter == 2, "Call back function is not called in each iteration of foreach iterator");

    wcb_counter = 0;
    TEST_ASSERT_MESSAGE(ma_table_wforeach(table_ptr_1, test_wtable_callback, NULL) == MA_OK,
                        "ma_table_foreach doesn't return MA_OK when table and callback functions are valid");
    TEST_ASSERT_MESSAGE(wcb_counter == 2, "Call back function is not called in each iteration of foreach iterator");

    ma_variant_release(variant_ptr_1);
    ma_variant_release(variant_ptr_2);
    
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_table(table_ptr_1, &variant_table_ptr));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_get_table(variant_table_ptr, &table_variant_ptr));
    TEST_ASSERT_MESSAGE(table_variant_ptr == table_ptr_1, "getting array from variant failed");

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(variant_table_ptr));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_table_release(table_ptr_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_table_release(table_ptr_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_table_release(table_ptr_2));

    //TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_deinit());
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_deinitialize(handle));
}

TEST(dispatcher_tests, variant_create_get_from_raw_buffer) {
    ma_dispatcher_t *handle = NULL;
    ma_variant_t *variant_ptr = NULL;
    ma_buffer_t *buffer_ptr = NULL;
    size_t buffer_size = 0;
    size_t raw_size = 0;
    unsigned char*raw_val = 0;
    ma_vartype_t vartype_obj = MA_VARTYPE_NULL;

    struct test_struct
    {
        ma_uint8_t int_data;
        char char_data[300];
        ma_float_t float_data;
        ma_bool_t boolean_data;
    };

    struct test_struct raw_buffer = { 0x12,
                                      "Variant raw buffer"
                                      "!@@!#@#$#%#$^$%^%$&^%&*^&*^&(&*(&*(^&^$%@#$#!@#!@$%%$&%^*&%^*^&(*&(*&(%^^$"
                                      "!@#$%^&*()_+01234567890123456789",
                                      2.3f,
                                      1};

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_initialize(NULL,&handle));
   // TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_init());

    TEST_ASSERT_MESSAGE(ma_variant_create_from_raw((const void*)&raw_buffer, sizeof(raw_buffer), &variant_ptr) == MA_OK, \
                        "Raw Buffer variant could not be created");

    TEST_ASSERT_MESSAGE(ma_variant_get_type(variant_ptr, &vartype_obj) == MA_OK, \
                        "ma_variant_get_type doesn't return MA_OK for Raw Buffer Variant");

    TEST_ASSERT_MESSAGE(vartype_obj == MA_VARTYPE_RAW, "ma_variant_get_type doesn't return MA_VARTYPE_RAW for RAW Buffer Variant");

    TEST_ASSERT_MESSAGE(ma_variant_get_raw_buffer(variant_ptr, &buffer_ptr) == MA_OK, \
                        "Raw Buffer cannot be retrieved from the Variant");

    TEST_ASSERT_MESSAGE(ma_buffer_size(buffer_ptr, &buffer_size) == MA_OK, \
                        "ma_buffer_size doesn't return MA_OK for valid buffer");

    TEST_ASSERT_MESSAGE(sizeof(raw_buffer) == buffer_size, \
                        "ma_buffer_size doesn't return correct buffer size");

    TEST_ASSERT_MESSAGE(ma_buffer_get_raw(buffer_ptr, &raw_val, &raw_size) == MA_OK, \
                        "ma_buffer_get_raw could not retrieve raw buffer and its size from the raw buffer variant");
    /* Deallocating buffer */
    ma_variant_release(variant_ptr);
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_buffer_release(buffer_ptr));

    //TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_deinit());
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_deinitialize(handle));

}

TEST(dispatcher_tests, variant_buffer_tests) {
    ma_dispatcher_t *handle = NULL;
    ma_variant_t *variant_ptr = NULL;
    ma_buffer_t *buffer_ptr = NULL;
    ma_wbuffer_t *wbuffer_ptr = NULL;
    size_t buffer_size = 0, wbuffer_size = 0;
    char *buffer_val = "";
    wchar_t *wbuffer_val = L"";
    size_t string_size = 0, wstring_size = 0;
    ma_bool_t result = MA_FALSE;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_initialize(NULL,&handle));
   // TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_init());

    TEST_ASSERT_MESSAGE(ma_buffer_add_ref(NULL) == MA_ERROR_INVALIDARG, \
                        "ma_buffer_inc_ref doesn't return MA_ERROR_INVALIDARG when buffer address is NULL");

    TEST_ASSERT_MESSAGE(ma_buffer_create(&buffer_ptr, 30) == MA_OK,
                        "ma_buffer_create could not create buffer of size 30");

    TEST_ASSERT_MESSAGE(ma_buffer_size(buffer_ptr, &buffer_size) == MA_OK, \
                        "ma_buffer_size doesn't return MA_OK for valid buffer");

    TEST_ASSERT_MESSAGE(30 == buffer_size, "ma_buffer_size doesn't return correct buffer size");

    TEST_ASSERT_MESSAGE(ma_buffer_set(buffer_ptr, "MA Security Agent", 17) == MA_OK, \
                        "ma_buffer_set doesn't return MA_OK when a valid string is copied into buffer");

    TEST_ASSERT_MESSAGE(ma_buffer_get_string(buffer_ptr, &buffer_val, &string_size) == MA_OK, \
                        "ma_buffer_get_string could not retrieve string and its size from the buffer");

    TEST_ASSERT_MESSAGE(17 == string_size, "String size is not read correctly from the buffer");
    TEST_ASSERT_MESSAGE(ma_buffer_is_equal(buffer_ptr, buffer_ptr, &result) == MA_OK, "ma_buffer_is_equal failed");
    TEST_ASSERT_MESSAGE(result == MA_TRUE, "ma_buffer_is_equal returns wrong");

    TEST_ASSERT_MESSAGE(ma_buffer_release(buffer_ptr) == MA_OK, "ma_buffer_release failed");



    TEST_ASSERT_MESSAGE(ma_wbuffer_add_ref(NULL) == MA_ERROR_INVALIDARG, \
                        "ma_wbuffer_inc_ref doesn't return MA_ERROR_INVALIDARG when wbuffer address is NULL");

    TEST_ASSERT_MESSAGE(ma_wbuffer_create(&wbuffer_ptr, 30) == MA_OK,
                        "ma_wbuffer_create could not create wbuffer of size 30");

    TEST_ASSERT_MESSAGE(ma_wbuffer_size(wbuffer_ptr, &wbuffer_size) == MA_OK, \
                        "ma_wbuffer_size doesn't return MA_OK for valid buffer");

    TEST_ASSERT_MESSAGE(ma_variant_create_from_wstring(L"MA Security Agent'U+2212'", 0, &variant_ptr) == MA_OK, \
                        "ma_variant_create_from_string doesn't return MA_OK when length of string is 0");

    TEST_ASSERT_MESSAGE(30 * sizeof(wchar_t) == wbuffer_size, "ma_buffer_size doesn't return correct wbuffer size");

    TEST_ASSERT_MESSAGE(ma_wbuffer_set(wbuffer_ptr, L"MA Security Agent", 17) == MA_OK, \
                        "ma_wbuffer_set doesn't return MA_OK when a valid wstring is copied into wbuffer");

    TEST_ASSERT_MESSAGE(ma_wbuffer_get_string(wbuffer_ptr, &wbuffer_val, &wstring_size) == MA_OK, \
                        "ma_wbuffer_get_string could not retrieve wstring and its size from the wbuffer");

    TEST_ASSERT_MESSAGE(17 == string_size, "wString size is not read correctly from the wbuffer");
    TEST_ASSERT_MESSAGE(ma_wbuffer_is_equal(wbuffer_ptr, wbuffer_ptr, &result) == MA_OK, "ma_wbuffer_is_equal failed");
    TEST_ASSERT_MESSAGE(result == MA_TRUE, "ma_wbuffer_is_equal returns wrong");

    TEST_ASSERT_MESSAGE(ma_wbuffer_release(wbuffer_ptr) == MA_OK, "ma_wbuffer_release failed");

    //TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_deinit());
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_deinitialize(handle));
}


void dispatcher_variant_create_thread_1(void *param)
{
    ma_dispatcher_t *handle = NULL;
    ma_variant_t *variant_ptr = NULL;
    ma_vartype_t vartype_obj = MA_VARTYPE_NULL;
    size_t counter = 0;
    ma_buffer_t *buffer_ptr = NULL;
    size_t buffer_size = 0;
    char *buffer_val = "";
    size_t string_size = 0;
    ma_bool_t result = MA_FALSE;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_initialize(NULL,&handle));

    /*Creating a unsigned 8-bit integer variant*/
    TEST_ASSERT_MESSAGE(ma_variant_create_from_uint8(0x74U, &variant_ptr) == MA_OK, \
                        "8 Bit unsigned variant could not be created");
    TEST_ASSERT_MESSAGE(ma_variant_release(variant_ptr) == MA_OK, \
                            "ma_variant_dec_ref could not decrease variant reference count");

    TEST_ASSERT_MESSAGE(ma_buffer_create(&buffer_ptr, 30) == MA_OK,
                        "ma_buffer_create could not create buffer of size 30");
    TEST_ASSERT_MESSAGE(ma_buffer_set(buffer_ptr, "MA Security Agent", 17) == MA_OK, \
                        "ma_buffer_set doesn't return MA_OK when a valid string is copied into buffer");

    TEST_ASSERT_MESSAGE(ma_buffer_release(buffer_ptr) == MA_OK, "ma_buffer_release failed");

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_deinitialize(handle));
}

void dispatcher_variant_create_thread_2(void *param)
{
    ma_dispatcher_t *handle = NULL;
    ma_variant_t *variant_ptr = NULL;
    ma_variant_t *variant_ptr_1 = NULL;
    ma_variant_t *variant_ptr_2 = NULL;
    ma_array_t *array_ptr = NULL;
    ma_array_t *array_ptr_1 = NULL;
    ma_variant_t *array_variant_ptr = NULL;
    ma_vartype_t vartype_obj = MA_VARTYPE_NULL;
    ma_bool_t result_val = MA_FALSE;
    size_t array_size = 100;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_initialize(NULL,&handle));

    TEST_ASSERT_MESSAGE(ma_array_create(&array_ptr) == MA_OK, \
                        "ma_array_create doesn't returns MA_OK for valid array pointer address");

    TEST_ASSERT_MESSAGE(ma_variant_create(&variant_ptr) == MA_OK, "NULL variant could not be created");
    TEST_ASSERT_MESSAGE(ma_array_push(array_ptr, variant_ptr) == MA_OK, "Variant could not be pushed to array");
    ma_variant_release(variant_ptr);

    TEST_ASSERT_MESSAGE(ma_variant_create_from_array(array_ptr, &array_variant_ptr) == MA_OK, \
                        "ma_variant_create_from_array could not create variant from array");
    TEST_ASSERT_MESSAGE(ma_variant_get_array(array_variant_ptr, &array_ptr_1) == MA_OK, \
                        "ma_variant_get_array could not retrieve array from the array variant");

    ma_array_release(array_ptr_1);
    ma_variant_release(array_variant_ptr);
    ma_array_release(array_ptr);

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_deinitialize(handle));
}

void dispatcher_variant_create_thread_3(void *param)
{
    ma_dispatcher_t *handle = NULL;
    ma_variant_t *variant_ptr = NULL;
    ma_vartype_t vartype_obj = MA_VARTYPE_NULL;
    ma_uint8_t counter = 0;
    ma_bool_t expected_value = 0;
    ma_int8_t int_expected_value = 0;
    ma_table_t *table_ptr = NULL;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_initialize(NULL,&handle));

    TEST_ASSERT_MESSAGE(ma_table_create(&table_ptr) == MA_OK, \
                        "ma_table_create doesn't returns MA_OK for valid table pointer address");
    TEST_ASSERT_MESSAGE(ma_variant_create(&variant_ptr) == MA_OK, "NULL variant could not be created");
    TEST_ASSERT_MESSAGE(ma_table_add_wentry(table_ptr, L"First Entry", variant_ptr) == MA_OK, "Variant could not be pushed to table");
    ma_variant_release(variant_ptr);

    ma_table_release(table_ptr);

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_deinitialize(handle));
}


TEST(dispatcher_tests, dispatcher_multithreading) {
    uv_thread_t tid_1 , tid_2 , tid_3 ;

    // Creating thread 1 
    TEST_ASSERT_MESSAGE( 0 == uv_thread_create(&tid_1 , dispatcher_variant_create_thread_1 , NULL) , "Thread could not be created");

    // Creating thread 2 
    TEST_ASSERT_MESSAGE( 0 == uv_thread_create(&tid_2 , dispatcher_variant_create_thread_2 , NULL) , "Thread could not be created");

    // Creating thread 3 
   TEST_ASSERT_MESSAGE( 0 == uv_thread_create(&tid_3 , dispatcher_variant_create_thread_3 , NULL) , "Thread could not be created");

    // Wait until all threads have terminated.
    uv_thread_join(&tid_1);
    uv_thread_join(&tid_2);
    uv_thread_join(&tid_3);
}

//Message Bus tests

//Publish subscribe test macros
#define PUBLISHER_TOPIC		"ma.managebaility.policy.enforcement.timeout"
#define SUBSCRIBER_TOPIC_FILTER	 "ma.managebaility.*"
ma_msgbus_subscriber_t *sub = NULL ;
ma_msgbus_consumer_reachability_t reachability;

//async_send
static ma_msgbus_endpoint_h p_async_endpoint, p_async_endpoint1;

static ma_bool_t done = 0,async_done = 0;
static uv_cond_t cond, async_cond;
static uv_mutex_t mux, async_mux;

TEST(dispatcher_tests, msgbus_noinit_but_deinit) {
    ma_dispatcher_t *handle = NULL;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_initialize(NULL,&handle));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_deinitialize(handle));
}

TEST(dispatcher_tests, msgbus_endpoint_setopt) {

    ma_dispatcher_t *handle = NULL;
    ma_msgbus_endpoint_h p_endpoint;
	long timeout_val;
    ma_msgbus_t *msgbus = NULL;

    const char service_name[] = "ma.msgbus.testservice";
	timeout_val = 2*1000;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_initialize(NULL,&handle));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_create(DISPATCHER_PROUDUCT_ID, &msgbus));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_start(msgbus));	
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_endpoint_create(msgbus, service_name,"localhost", &p_endpoint));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_endpoint_setopt(p_endpoint, MSGBUSOPT_REACH, MSGBUS_CONSUMER_REACH_OUTPROC));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_endpoint_setopt(p_endpoint, MSGBUSOPT_TIMEOUT, timeout_val));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_endpoint_release(p_endpoint));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_stop(msgbus, MA_TRUE));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_release(msgbus));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_deinitialize(handle));


}


TEST(dispatcher_tests, msgbus_send_and_forget) {
    ma_dispatcher_t *handle = NULL;
    ma_msgbus_endpoint_h p_endpoint;
    const char service_name[] = "ma.msgbus.testservice";
    ma_message_t *request = NULL;
    ma_msgbus_t *msgbus = NULL;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_initialize(NULL,&handle));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_create(DISPATCHER_PROUDUCT_ID, &msgbus));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_start(msgbus));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_msgbus_endpoint_create(msgbus, service_name,"localhost", &p_endpoint));
	
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_message_create(&request));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_message_set_property(request, "call_type","sync"));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_msgbus_send_and_forget(p_endpoint, request));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_message_release(request));
	//This is an issue where we should not call endpt rel when the io thread is still using it
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_endpoint_release(p_endpoint));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_stop(msgbus, MA_TRUE));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_release(msgbus));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_deinitialize(handle));


}

static ma_error_t server_cb(ma_msgbus_server_t *server, ma_message_t *request_message, void *cb_data, ma_msgbus_client_request_t *c_request) {

    ma_message_t *result = NULL;

    const char *call_type;
    TEST_ASSERT_EQUAL(MA_OK, ma_message_get_property(request_message,"call_type",&call_type));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_message_create(&result));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_client_request_add_ref(c_request));
    
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_server_client_request_post_result(c_request, MA_OK, result));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_client_request_release(c_request));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_message_release(result));

    return MA_OK;
}

TEST(dispatcher_tests, msgbus_sync_send) {
    ma_dispatcher_t *handle = NULL;
    ma_msgbus_endpoint_h p_endpoint;
    const char service_name[] = "ma.msgbus.testservice";
    ma_msgbus_server_t *server = NULL;
    ma_message_t *request = NULL, *response = NULL;
    ma_msgbus_t *msgbus = NULL;
	
    //Msgbus server start
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_initialize(NULL,&handle));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_create(DISPATCHER_PROUDUCT_ID, &msgbus));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_start(msgbus));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_server_create(msgbus, service_name,&server));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_server_start(server, &server_cb, NULL));

    TEST_ASSERT_EQUAL_INT(MA_OK,ma_msgbus_endpoint_create(msgbus, service_name,"localhost", &p_endpoint));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_message_create(&request));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_message_set_property(request, "call_type","sync"));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_msgbus_send(p_endpoint, request, &response));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_message_release(response));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_message_release(request));
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_endpoint_release(p_endpoint));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_server_stop(server));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_server_release(server));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_stop(msgbus, MA_TRUE));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_release(msgbus));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_deinitialize(handle));
	
}

TEST(dispatcher_tests, message_variant_payload) {
    ma_message_t *msg = NULL, *msg1 = NULL, *msg2 = NULL;
    ma_dispatcher_t *handle = NULL;
    ma_variant_t *value = NULL, *value1 = NULL, *value2 = NULL;
    ma_variant_t *msg_variant = NULL, *msg_variant1 = NULL;
    char *data = NULL;
	ma_bool_t       result = MA_FALSE;
    ma_message_type_t msgtype;
                                                                                                                                                                                    
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_initialize(NULL,&handle));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_message_create(&msg));
    SET_MESSAGE_VERSION(msg , 5);
    SET_MESSAGE_TYPE(msg , MA_MESSAGE_TYPE_REQUEST);
    SET_MESSAGE_SUB_TYPE(msg , MA_MESSAGE_SUB_TYPE_ONEWAY);

    SET_MESSAGE_CORRELATION_ID(msg, 1234);
    SET_MESSAGE_TIME(msg, 10102010L);
    SET_MESSAGE_STATUS(msg, MA_OK);

    //Set property
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_message_set_property(msg , "Type" , "Request"));

    //Set payload
    TEST_ASSERT_MESSAGE(MA_OK  == ma_variant_create_from_string("This is my test message" , strlen("This is my test message") , &value) , "ma_variant_create_from_string failed to create the object");
    //TEST_ASSERT_EQUAL_INT(MA_OK, ma_message_set_payload(msg , value));
    
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_message_as_variant(msg , &msg_variant));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_message_from_variant(msg_variant, &msg2));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_message_as_variant(msg2, &msg_variant1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_is_equal(msg_variant, msg_variant1, &result));
    TEST_ASSERT_MESSAGE(MA_TRUE  == result, "Message variants are different");    
    TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == ma_message_from_variant(value1 , &msg1), "msg variant invalid");
    
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_message_get_property(msg ,  "Type"  , &data));
    TEST_ASSERT_MESSAGE(0 == strncmp("Request" , data, strlen(data)) , "Failed to get the property ");
    //Get message type
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_message_get_message_type(msg,&msgtype));
    //Get payload
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_message_get_payload(msg , &value2));
    
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(msg_variant));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(msg_variant1));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_variant_release(value));
    //TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_variant_release(value2));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_message_release(msg));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_message_release(msg2));
    //TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_deinit());
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_deinitialize(handle));
}

static void wait_until_done() {

    uv_mutex_lock(&mux);
     while (!done) {uv_cond_wait(&cond,&mux);}
    uv_mutex_unlock(&mux);
}

static void wait_until_done_async() {

    uv_mutex_lock(&async_mux);
     while (!async_done) {uv_cond_wait(&async_cond,&async_mux);}
    uv_mutex_unlock(&async_mux);
}

static ma_error_t subscriber_cb(const char *topic, ma_message_t *payload, void *cb_data) {
    char *value  = NULL ;
    
    TEST_ASSERT_MESSAGE(MA_MESSAGE_TYPE_PUBLISH == GET_MESSAGE_TYPE(payload), "publish message invalid");

    ma_message_get_property(payload , MA_MSG_PUBLISH_TOPIC  , &value);

    TEST_ASSERT_EQUAL_INT(0  , strncmp(PUBLISHER_TOPIC ,value, strlen(value)));

    ma_msgbus_subscriber_unregister(sub);
    ma_msgbus_subscriber_release(sub);
    

    done = MA_TRUE;
    uv_cond_signal(&cond);
    return MA_OK ;
}

void sub_test_thread(void *arg) {
    ma_message_t *msg = NULL ;
	long *opt_reachability = NULL;
    ma_msgbus_t *msgbus = NULL;
	reachability = MSGBUS_CONSUMER_REACH_INPROC;
    ma_msgbus_create(DISPATCHER_PROUDUCT_ID, &msgbus);
	ma_msgbus_start(msgbus);
    ma_msgbus_subscriber_create( msgbus, &sub );
	ma_msgbus_subscriber_setopt(sub, MSGBUSOPT_REACH, reachability);
    ma_msgbus_subscriber_register(sub , SUBSCRIBER_TOPIC_FILTER , subscriber_cb , sub);		
	ma_message_create(&msg);
    ma_msgbus_publish(msgbus, PUBLISHER_TOPIC, MSGBUS_CONSUMER_REACH_INPROC , msg)  ;
    ma_message_release(msg);
	ma_msgbus_stop(msgbus, MA_TRUE);
    ma_msgbus_release(msgbus);
}

TEST(dispatcher_tests, msgbus_pub_sub) {
    ma_dispatcher_t *handle = NULL;
    uv_thread_t tid;

    uv_mutex_init(&mux), uv_cond_init(&cond);
    
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_initialize(NULL,&handle));
    
    
    uv_thread_create(&tid, &sub_test_thread , NULL );

    wait_until_done();
    uv_thread_join(&tid);

    uv_cond_destroy(&cond), uv_mutex_destroy(&mux);
    ma_dispatcher_deinitialize(handle);
}

static void set_done_async() {
    uv_mutex_lock(&async_mux);
    async_done = MA_TRUE;
    uv_cond_signal(&async_cond);
    uv_mutex_unlock(&async_mux);
}


static ma_error_t client_cb(ma_error_t status, ma_message_t *request_message, void *cb_data, ma_msgbus_request_t *request){
    
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_request_get_endpoint(request, &p_async_endpoint1));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_endpoint_release(p_async_endpoint));
	
	set_done_async();

    return MA_OK;
}

TEST(dispatcher_tests, msgbus_async_send) {
    ma_dispatcher_t *handle = NULL;
    const char service_name[] = "ma.msgbus.testservice";
    ma_msgbus_server_t *server = NULL;
    ma_message_t *request = NULL, *response = NULL;
    ma_msgbus_request_t *msg_req = NULL;
    ma_msgbus_t *msgbus = NULL;
    long priority_num;

	uv_mutex_init(&async_mux);
	uv_cond_init(&async_cond);

	//Msgbus server start
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_initialize(NULL,&handle));
    
    
	priority_num = 1;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_create(DISPATCHER_PROUDUCT_ID, &msgbus));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_start(msgbus));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_server_create(msgbus, service_name,&server));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_server_setopt(server, MSGBUSOPT_PRIORITY, priority_num));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_server_start(server, &server_cb, NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_endpoint_create(msgbus, service_name,"localhost", &p_async_endpoint));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_message_create(&request));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_message_set_property(request, "call_type","async"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_async_send(p_async_endpoint, request, &client_cb, NULL, &msg_req));
    
	wait_until_done_async();

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_request_cancel(msg_req,MA_MSGBUS_NO_NOTIFY));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_request_release(msg_req));
		

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_message_release(request));
    
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_server_stop(server));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_server_release(server));
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_stop(msgbus, MA_TRUE));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_release(msgbus));
	
	uv_cond_destroy(&async_cond);
	uv_mutex_destroy(&async_mux);

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_deinitialize(handle));
}

TEST(dispatcher_tests, msgbus_version) {
    ma_dispatcher_t *handle = NULL;
    ma_msgbus_client_request_t *c_request = NULL;
    
    unsigned int version;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_initialize(NULL,&handle));
    
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_get_version(&version));
 
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_deinitialize(handle));


}


static void run_all_tests(void) {	
	
	do {RUN_TEST_GROUP(dispatcher_test_group_without_rsdk_path); } while (0);
	do {RUN_TEST_GROUP(dispatcher_test_group); } while (0);
	//do {RUN_TEST_GROUP(ma_dispatcher_property_client_test_group); } while (0);
	do {RUN_TEST_GROUP(dispatcher_policy_bag_tests_group); } while (0);
	//do {RUN_TEST_GROUP(dispatcher_event_test_group); } while (0);
	//do {RUN_TEST_GROUP(scheduler_client_daily_test_group); } while (0);
	//do {RUN_TEST_GROUP(scheduler_client_run_once_test_group); } while (0);
	do {RUN_TEST_GROUP(scheduler_client_api_test_group); } while (0);
	//do {RUN_TEST_GROUP(ma_repository_client_test_group); } while (0);
	//do {RUN_TEST_GROUP(ma_repository_mirror_test_group); } while (0);
}

int main(int argc, char *argv[]) {
#ifdef MA_WINDOWS
    MA_TRACKLEAKS;
#endif
	//Sleep(20000);
    return UnityMain(argc, argv, run_all_tests);
}
