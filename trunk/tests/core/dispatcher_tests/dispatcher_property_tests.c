#include "unity.h"
#include "unity_fixture.h"

#include <uv.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "ma/ma_common.h"
#include "ma/ma_message.h"
//#include "ma/ma_msgbus.h"
#include "ma/ma_variant.h"
#include "ma/property/ma_property.h"
#include "ma/property/ma_property_bag.h"
#include "ma/ma_dispatcher.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/core/msgbus/ma_message_header.h"
#include "ma/logger/ma_file_logger.h"
#include "ma/ma_errors.h"

#include "ma_client_ut_setup.h"


#include "dispatcher_tests_utils.h"

#define DISPATCHER_PROUDUCT_ID "ma.dispatcher.test"

#ifdef MA_WINDOWS
# include <crtdbg.h>
#include <Windows.h>
# define MA_TRACKLEAKS _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF)
#define SOFTWARE_ID_PATH "SOFTWARE\\McAfee\\Agent\\Applications\\EPOAGENT3000"
#define SOFTWARE_ID_PATH1 "SOFTWARE\\McAfee\\Agent\\Applications\\EPOAGENT3000"
#define MA_DISPATCHER_PRODUCT_INSTAL_PATH "install_path"
#define MA_DISPATCHER_RSDK_PATH "RuntimeSdkPath"
#else
# define MA_TRACKLEAKS ((void *)0)
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#define SOFTWARE_ID_PATH "/etc/ma.d"
#endif

ma_uint8_t test_scenario = 0;

TEST_GROUP_RUNNER(dispatcher_property_test_group) {
    
	do {RUN_TEST_CASE(dispatcher_property_tests, property_bag_test_data); }while(0);
    
}

TEST_SETUP(dispatcher_property_tests) {
#ifdef MA_WINDOWS
    ma_dispatcher_test_create_registry(SOFTWARE_ID_PATH);
#else
    ma_dispatcher_test_create_registry("EPOAGENT3000");
#endif
}

TEST_TEAR_DOWN(dispatcher_property_tests) {
#ifdef MA_WINDOWS
   ma_dispatcher_tests_delete_registry(SOFTWARE_ID_PATH);
   ma_dispatcher_tests_delete_registry(SOFTWARE_ID_PATH1);
#else
    ma_dispatcher_tests_delete_registry("ma");
    ma_dispatcher_tests_delete_registry("ma1");
#endif
}


TEST(dispatcher_property_tests, property_bag_test_data) {
	ma_property_bag_t *bag = NULL ;
    ma_variant_t *variant_ptr = NULL;
    ma_buffer_t *buffer = NULL;
    char *string_val = NULL;
    size_t string_size = 0 ;
	ma_dispatcher_t *dispatcher = NULL;
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_initialize(NULL, &dispatcher));

	

    TEST_ASSERT(MA_OK == ma_property_bag_create(&bag));

    // Adding properties
    TEST_ASSERT(MA_OK == ma_variant_create_from_string("Windows 7 Desktop", strlen("Windows 7 Desktop"), &variant_ptr));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_property_bag_add_property(NULL, "ComputerProperties1", "PlatformID", variant_ptr));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_property_bag_add_property(bag, NULL, "PlatformID", variant_ptr));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_property_bag_add_property(bag, "ComputerProperties1", NULL, variant_ptr));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_property_bag_add_property(bag, "ComputerProperties1", "PlatformID", NULL));
    TEST_ASSERT(MA_OK == ma_property_bag_add_property(bag, "ComputerProperties1", "PlatformID", variant_ptr));
    TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));
    variant_ptr = NULL;

    TEST_ASSERT(MA_OK == ma_variant_create_from_string("user.beaeng.mfeeng.org", strlen("user.beaeng.mfeeng.org"), &variant_ptr));
    TEST_ASSERT(MA_OK == ma_property_bag_add_property(bag, "ComputerProperties2" , "IPHostName", variant_ptr));
    TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));
    variant_ptr = NULL;

    TEST_ASSERT(MA_OK == ma_variant_create_from_string("X's Laptop", strlen("X's Laptop"), &variant_ptr));
    TEST_ASSERT(MA_OK == ma_property_bag_add_property(bag, "ComputerProperties1", "ComputerName", variant_ptr));
    TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));
    variant_ptr = NULL;

    // Retrieving properties in reverse order
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_property_bag_get_property(NULL, "ComputerProperties1", "ComputerName", &variant_ptr));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_property_bag_get_property(bag, NULL, "ComputerName", &variant_ptr));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_property_bag_get_property(bag, "ComputerProperties1", NULL, &variant_ptr));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_property_bag_get_property(bag, "ComputerProperties1", "ComputerName", NULL));
    TEST_ASSERT(MA_OK == ma_property_bag_get_property(bag, "ComputerProperties1", "ComputerName", &variant_ptr));
    TEST_ASSERT(MA_OK == ma_variant_get_string_buffer(variant_ptr, &buffer));
    TEST_ASSERT(MA_OK == ma_buffer_get_string(buffer, &string_val, &string_size));
    TEST_ASSERT(strlen("X's Laptop") == string_size);
    TEST_ASSERT(0 == strcmp("X's Laptop", string_val));
    TEST_ASSERT(MA_OK == ma_buffer_release(buffer));
    TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));
    variant_ptr = NULL;

    TEST_ASSERT(MA_OK == ma_property_bag_get_property(bag, "ComputerProperties2", "IPHostName", &variant_ptr));
    TEST_ASSERT(MA_OK == ma_variant_get_string_buffer(variant_ptr, &buffer));
    TEST_ASSERT(MA_OK == ma_buffer_get_string(buffer, &string_val, &string_size));
    TEST_ASSERT(strlen("user.beaeng.mfeeng.org") == string_size);
    TEST_ASSERT(0 ==strcmp("user.beaeng.mfeeng.org", string_val));
    TEST_ASSERT(MA_OK == ma_buffer_release(buffer));
    TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));
    variant_ptr = NULL;

    TEST_ASSERT(MA_OK == ma_property_bag_get_property(bag, "ComputerProperties1", "PlatformID", &variant_ptr));
    TEST_ASSERT(MA_OK == ma_variant_get_string_buffer(variant_ptr, &buffer));
    TEST_ASSERT(MA_OK == ma_buffer_get_string(buffer, &string_val, &string_size));
    TEST_ASSERT(strlen("Windows 7 Desktop") == string_size);
    TEST_ASSERT(0 == strcmp("Windows 7 Desktop", string_val));
    TEST_ASSERT(MA_OK == ma_buffer_release(buffer));
    TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));
    variant_ptr = NULL;

    // Retrieving from a non-existent path
    TEST_ASSERT(MA_ERROR_PROPERTY_BAG_PATH_NOT_FOUND == ma_property_bag_get_property(bag, "ComputerProperties3", "PlatformID", &variant_ptr));
    TEST_ASSERT_NULL(variant_ptr);

    // Retrieving a non-existent property from a valid path
    TEST_ASSERT(MA_ERROR_OBJECTNOTFOUND == ma_property_bag_get_property(bag, "ComputerProperties1", "IP Address", &variant_ptr));
    TEST_ASSERT_NULL(variant_ptr);

    TEST_ASSERT(MA_OK == ma_property_bag_release(bag));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_deinitialize(dispatcher));
}




/******************************************************************************
Test Scenario 1 - Collect and Send Properties test when all properties are requested.
Test Scenario 2 - Collect and Send Properties test when minimal properties are
                  requested.
******************************************************************************/
TEST_GROUP_RUNNER(ma_dispatcher_property_client_test_group)
{
    test_scenario = 1;
    do {RUN_TEST_CASE(property_provider_tests, property_provider_send_properties_test)} while (0);

    test_scenario = 2;
//    do {RUN_TEST_CASE(property_provider_tests, property_provider_send_properties_test)} while (0);
}

/* Global variable declaration */
static ma_client_t *g_ma_client = NULL;
static ma_msgbus_t *g_msgbus = NULL;
static ma_ut_client_setup_t *g_ut_setup = NULL;

#define SERVICE_NAME "ma.service.property"
#define PRODUCT_ID "ma.property.client.test"

static ma_msgbus_server_t *server = NULL;

static ma_bool_t done = 0;
static uv_cond_t cond;
static uv_mutex_t mux;

static void wait_until_done()
{
    uv_mutex_lock(&mux);
    while (!done)
    {
        uv_cond_wait(&cond,&mux);
    }

    uv_mutex_unlock(&mux);
}

static void set_done()
{
    uv_mutex_lock(&mux);
    done = MA_TRUE;
    uv_cond_signal(&cond);
    uv_mutex_unlock(&mux);
}

static ma_error_t server_cb(ma_msgbus_server_t *server, ma_message_t *request_message, void *cb_data, ma_msgbus_client_request_t *c_request);


/**************************************************************************
1) Creating the unit test context.
2) Retrieving underlying message bus instance.
3) Creating and starting message bus service (Property Service) using the
   message bus instance.
**************************************************************************/
static ma_dispatcher_t *dispatcher = NULL;
TEST_SETUP(property_provider_tests)
{
	#ifdef MA_WINDOWS
    ma_dispatcher_test_create_registry(SOFTWARE_ID_PATH);
#else
    ma_dispatcher_test_create_registry("EPOAGENT3000");
#endif
	ma_dispatcher_initialize(NULL,&dispatcher);
    uv_mutex_init(&mux);
    uv_cond_init(&cond);

	//if(g_ut_setup == NULL) {
		ma_ut_setup_create("property_client.property_provider.tests", PRODUCT_ID, &g_ut_setup);
	
	g_msgbus = ma_ut_setup_get_msgbus(g_ut_setup);
	g_ma_client = ma_ut_setup_get_client(g_ut_setup);
	
    ma_msgbus_server_create(g_msgbus, SERVICE_NAME, &server);
    ma_msgbus_server_start(server, server_cb, NULL);
}

/**************************************************************************
1) Stopping and releasing the message bus service (Property service).
2) Releasing the context.
**************************************************************************/
TEST_TEAR_DOWN(property_provider_tests)
{
    ma_msgbus_server_stop(server);
    ma_msgbus_server_release(server);
    ma_ut_setup_release(g_ut_setup);
    uv_cond_destroy(&cond);
    uv_mutex_destroy(&mux);
	ma_dispatcher_deinitialize(dispatcher);
	#ifdef MA_WINDOWS
   ma_dispatcher_tests_delete_registry(SOFTWARE_ID_PATH);
   ma_dispatcher_tests_delete_registry(SOFTWARE_ID_PATH1);
#else
    ma_dispatcher_tests_delete_registry("ma");
    ma_dispatcher_tests_delete_registry("ma1");
#endif
}

/**************************************************************************
Structure consisting of test values which will be used as the properties
in this test.
**************************************************************************/
struct LookupTable
{
    char *property_path;
    char *property_name;
    char *property_value;
};

struct LookupTable const test_vector_minimal[] =
{
    { "General",             "Processor",        "Intel"      },
    { "SystemInfo",          "Number of Cores",  "8"          },
    { "ComputerProperties",  "Processor Speed",  "2.4GHz"     },
    { "SystemInfo",          "OperatingSystem",  "Win8"       },
    { "General",             "ComputerName",     "My Desktop" },
    { "General",             "Username",         "Admin"      },
};

struct LookupTable const test_vector_other[] =
{
    { "Path 1", "Name 1_1", "Value 1_1"},
    { "Path 2", "Name 2_1", "Value 2_1"},
    { "Path 1", "Name 1_2", "Value 1_2"},
    { "Path 3", "Name 3_1", "Value 3_1"},
    { "Path 2", "Name 2_2", "Value 2_2"},
    { "Path 1", "Name 1_3", "Value 1_3"},
    { "Path 1", "Name 1_4", "Value 1_4"},
    { "Path 2", "Name 2_3", "Value 2_3"},
    { "Path 3", "Name 3_2", "Value 3_2"},
    { "Path 1", "Name 1_5", "Value 1_5"},
};

/**************************************************************************
Property Provider callback function for adding the properties to Property
Bag.
This function tries to fill the property bag with both Minimal Properties
(i.e. "General", "SystemInfo" and "ComputerProperties" section) as well as
other properties (Any Other Section).
**************************************************************************/
ma_error_t my_property_provider_callback(ma_client_t *ma_client, const char *product_id, ma_property_bag_t *property_bag, void *cb_data)
{
    ma_uint8_t counter = 0;

    // Verifying Product ID
    TEST_ASSERT(0 == strcmp(PRODUCT_ID, product_id));

    if(property_bag == NULL)
    {
        printf("Invalid arguments passed to callback function.\n");
        return MA_ERROR_INVALIDARG;
    }

    // Filling Minimal Properties
    for(counter = 0; counter < sizeof(test_vector_minimal)/sizeof(test_vector_minimal[0]); counter++)
    {
        ma_variant_t *variant_ptr = NULL;

        TEST_ASSERT(ma_variant_create_from_string(test_vector_minimal[counter].property_value, strlen(test_vector_minimal[counter].property_value), &variant_ptr) == MA_OK);

        // Verifying invalid scenarios
        TEST_ASSERT(ma_property_bag_add_property(NULL, test_vector_minimal[counter].property_path, test_vector_minimal[counter].property_name, variant_ptr) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_property_bag_add_property(property_bag, NULL, test_vector_minimal[counter].property_name, variant_ptr) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_property_bag_add_property(property_bag, test_vector_minimal[counter].property_path, NULL, variant_ptr) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_property_bag_add_property(property_bag, test_vector_minimal[counter].property_path, test_vector_minimal[counter].property_name, NULL) == MA_ERROR_INVALIDARG);

        // Valid scenario
        TEST_ASSERT(ma_property_bag_add_property(property_bag, test_vector_minimal[counter].property_path, test_vector_minimal[counter].property_name, variant_ptr) == MA_OK);

        ma_variant_release(variant_ptr);
        variant_ptr = NULL;
    }

    // Filling Other Properties
    for(counter = 0; counter < sizeof(test_vector_other)/sizeof(test_vector_other[0]); counter++)
    {
        ma_variant_t *variant_ptr = NULL;

        TEST_ASSERT(ma_variant_create_from_string(test_vector_other[counter].property_value, strlen(test_vector_other[counter].property_value), &variant_ptr) == MA_OK);

        // Verifying invalid scenarios
        TEST_ASSERT(ma_property_bag_add_property(NULL, test_vector_other[counter].property_path, test_vector_other[counter].property_name, variant_ptr) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_property_bag_add_property(property_bag, NULL, test_vector_other[counter].property_name, variant_ptr) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_property_bag_add_property(property_bag, test_vector_other[counter].property_path, NULL, variant_ptr) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_property_bag_add_property(property_bag, test_vector_other[counter].property_path, test_vector_other[counter].property_name, NULL) == MA_ERROR_INVALIDARG);

        // Valid scenario
        TEST_ASSERT(ma_property_bag_add_property(property_bag, test_vector_other[counter].property_path, test_vector_other[counter].property_name, variant_ptr) == MA_OK);

        ma_variant_release(variant_ptr);
        variant_ptr = NULL;
    }

    return MA_OK;
}

/******************************************************************************
Call back function to be executed when the Property Service receives the
Properties from the property client. Properties received from client will be
verified in this callback function.

1) Creating fresh response message and post it as first task in the callback.
2) Process the payload received from Properties client.
   a) Verify that the Message Type, Provider ID and Session ID is set correctly
      in message properties.
   b) Extracting Property sections from the table and verifying them against
      the expected test values (If minimal properties are requested, only
      "General", "SystemInfo" and "ComputerProperties" section should be
      there in the Properties Bag.
******************************************************************************/
static ma_error_t server_cb(ma_msgbus_server_t *server, ma_message_t *property_payload, void *cb_data, ma_msgbus_client_request_t *c_request) {
    ma_table_t *properties_bag_variant_ptr = NULL;
    ma_vartype_t vartype_obj = MA_VARTYPE_NULL;

    ma_variant_t *table_variant_obj = NULL;
    ma_table_t *table_variant_ptr = NULL;
    ma_variant_t *variant_obj = NULL;
    ma_buffer_t *buffer = NULL;
    char *string_val = NULL;
    size_t string_size = 0;

    int counter = 0;

    // Property client has requested in send and forget mode, therefore, no response need to be sent, but,
    // request type many be changed later. Considering that, posting an MA_OK response for the properties
    // package received from property client.
   {
        ma_message_t* response_payload = NULL;
        ma_variant_t *properties_bag_variant_obj = NULL;

        TEST_ASSERT(MA_OK == ma_message_create(&response_payload));
        TEST_ASSERT(MA_OK == ma_variant_create_from_string("Properties Received", strlen("Properties Received"), &properties_bag_variant_obj));
        TEST_ASSERT(MA_OK == ma_message_set_payload(response_payload, properties_bag_variant_obj));
        TEST_ASSERT(MA_OK == ma_msgbus_server_client_request_post_result(c_request, MA_OK, response_payload));
        TEST_ASSERT(MA_OK == ma_variant_release(properties_bag_variant_obj));
        TEST_ASSERT(MA_OK == ma_message_release(response_payload));
        properties_bag_variant_obj = NULL;
        response_payload = NULL;
    }

    // Verifying Message Type property
    {
        const char *property_value = NULL;

        TEST_ASSERT(ma_message_get_property(property_payload, "prop.key.msg_type", &property_value) == MA_OK);
        TEST_ASSERT(0 == strcmp("prop.key.property_collected", property_value));
    }

    // Verifying Session ID property
    {
        const char *property_value = NULL;

        TEST_ASSERT(ma_message_get_property(property_payload, "ma.property.session_id", &property_value) == MA_OK);
        TEST_ASSERT(0 == strcmp("12345", property_value));
    }

    // Verifying Provider ID property
    {
        const char *property_value = NULL;

        TEST_ASSERT(ma_message_get_property(property_payload, "ma.property.provider_id", &property_value) == MA_OK);
        TEST_ASSERT(0 == strcmp(PRODUCT_ID, property_value));
    }

    // Extracting top-level variant from message
    {
        ma_variant_t *properties_bag_variant_obj = NULL;

        TEST_ASSERT(ma_message_get_payload(property_payload, &properties_bag_variant_obj) == MA_OK);
        TEST_ASSERT(ma_variant_get_type(properties_bag_variant_obj, &vartype_obj) == MA_OK);
        TEST_ASSERT(vartype_obj == MA_VARTYPE_TABLE);
        TEST_ASSERT(ma_variant_get_table(properties_bag_variant_obj, &properties_bag_variant_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_release(properties_bag_variant_obj) == MA_OK);
        properties_bag_variant_obj = NULL;
    }

    // Trying to extract a section which is not part of property bag
    TEST_ASSERT(ma_table_get_value(properties_bag_variant_ptr, "Specific", &table_variant_obj) == MA_ERROR_OBJECTNOTFOUND);

    // Extracting "General" Section
    {
        TEST_ASSERT(ma_table_get_value(properties_bag_variant_ptr, "General", &table_variant_obj) == MA_OK);

        TEST_ASSERT(ma_variant_get_type(table_variant_obj, &vartype_obj) == MA_OK);
        TEST_ASSERT(vartype_obj == MA_VARTYPE_TABLE);
        TEST_ASSERT(ma_variant_get_table(table_variant_obj, &table_variant_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_release(table_variant_obj) == MA_OK);
        table_variant_obj = NULL;

        for(counter = 0; counter < sizeof(test_vector_minimal)/sizeof(test_vector_minimal[0]); counter++)
        {
            // Trying to extract non-existent property from a particular section in property bag
            TEST_ASSERT(ma_table_get_value(table_variant_ptr, "IP Address", &variant_obj) == MA_ERROR_OBJECTNOTFOUND);

            if(strcmp(test_vector_minimal[counter].property_path, "General") == 0)
            {
                TEST_ASSERT(ma_table_get_value(table_variant_ptr, test_vector_minimal[counter].property_name, &variant_obj) == MA_OK);
                TEST_ASSERT(ma_variant_get_string_buffer(variant_obj, &buffer) == MA_OK);
                TEST_ASSERT(ma_buffer_get_string(buffer, &string_val, &string_size) == MA_OK);

                TEST_ASSERT(strlen(test_vector_minimal[counter].property_value) == string_size);
                TEST_ASSERT(0 == strcmp(test_vector_minimal[counter].property_value, string_val));

                TEST_ASSERT(ma_buffer_release(buffer) == MA_OK);
                TEST_ASSERT(ma_variant_release(variant_obj) == MA_OK);
                buffer = NULL;
                variant_obj = NULL;
            }
        }

        TEST_ASSERT(ma_table_release(table_variant_ptr) == MA_OK);
        table_variant_ptr = NULL;
    }

    // Extracting "SystemInfo" Section
    {
        TEST_ASSERT(ma_table_get_value(properties_bag_variant_ptr, "SystemInfo", &table_variant_obj) == MA_OK);

        TEST_ASSERT(ma_variant_get_type(table_variant_obj, &vartype_obj) == MA_OK);
        TEST_ASSERT(vartype_obj == MA_VARTYPE_TABLE);
        TEST_ASSERT(ma_variant_get_table(table_variant_obj, &table_variant_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_release(table_variant_obj) == MA_OK);
        table_variant_obj = NULL;

        for(counter = 0; counter < sizeof(test_vector_minimal)/sizeof(test_vector_minimal[0]); counter++)
        {
            if(strcmp(test_vector_minimal[counter].property_path, "SystemInfo") == 0)
            {
                TEST_ASSERT(ma_table_get_value(table_variant_ptr, test_vector_minimal[counter].property_name, &variant_obj) == MA_OK);
                TEST_ASSERT(ma_variant_get_string_buffer(variant_obj, &buffer) == MA_OK);
                TEST_ASSERT(ma_buffer_get_string(buffer, &string_val, &string_size) == MA_OK);

                TEST_ASSERT(strlen(test_vector_minimal[counter].property_value) == string_size);
                TEST_ASSERT(0 == strcmp(test_vector_minimal[counter].property_value, string_val));

                TEST_ASSERT(ma_buffer_release(buffer) == MA_OK);
                TEST_ASSERT(ma_variant_release(variant_obj) == MA_OK);
                buffer = NULL;
                variant_obj = NULL;
            }
        }

        TEST_ASSERT(ma_table_release(table_variant_ptr) == MA_OK);
        table_variant_ptr = NULL;
    }

    // Extracting "ComputerProperties" Section
    {
        TEST_ASSERT(ma_table_get_value(properties_bag_variant_ptr, "ComputerProperties", &table_variant_obj) == MA_OK);

        TEST_ASSERT(ma_variant_get_type(table_variant_obj, &vartype_obj) == MA_OK);
        TEST_ASSERT(vartype_obj == MA_VARTYPE_TABLE);
        TEST_ASSERT(ma_variant_get_table(table_variant_obj, &table_variant_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_release(table_variant_obj) == MA_OK);
        table_variant_obj = NULL;

        for(counter = 0; counter < sizeof(test_vector_minimal)/sizeof(test_vector_minimal[0]); counter++)
        {
            if(strcmp(test_vector_minimal[counter].property_path, "ComputerProperties") == 0)
            {
                TEST_ASSERT(ma_table_get_value(table_variant_ptr, test_vector_minimal[counter].property_name, &variant_obj) == MA_OK);
                TEST_ASSERT(ma_variant_get_string_buffer(variant_obj, &buffer) == MA_OK);
                TEST_ASSERT(ma_buffer_get_string(buffer, &string_val, &string_size) == MA_OK);

                TEST_ASSERT(strlen(test_vector_minimal[counter].property_value) == string_size);
                TEST_ASSERT(0 == strcmp(test_vector_minimal[counter].property_value, string_val));

                TEST_ASSERT(ma_buffer_release(buffer) == MA_OK);
                TEST_ASSERT(ma_variant_release(variant_obj) == MA_OK);
                buffer = NULL;
                variant_obj = NULL;
            }
        }

        TEST_ASSERT(ma_table_release(table_variant_ptr) == MA_OK);
        table_variant_ptr = NULL;
    }

    // Extracting "Path 1" Section
    if(test_scenario == 1)
    {
        TEST_ASSERT(ma_table_get_value(properties_bag_variant_ptr, "Path 1", &table_variant_obj) == MA_OK);

        TEST_ASSERT(ma_variant_get_type(table_variant_obj, &vartype_obj) == MA_OK);
        TEST_ASSERT(vartype_obj == MA_VARTYPE_TABLE);
        TEST_ASSERT(ma_variant_get_table(table_variant_obj, &table_variant_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_release(table_variant_obj) == MA_OK);
        table_variant_obj = NULL;

        for(counter = 0; counter < sizeof(test_vector_other)/sizeof(test_vector_other[0]); counter++)
        {
            if(strcmp(test_vector_other[counter].property_path, "Path 1") == 0)
            {
                TEST_ASSERT(ma_table_get_value(table_variant_ptr, test_vector_other[counter].property_name, &variant_obj) == MA_OK);
                TEST_ASSERT(ma_variant_get_string_buffer(variant_obj, &buffer) == MA_OK);
                TEST_ASSERT(ma_buffer_get_string(buffer, &string_val, &string_size) == MA_OK);

                TEST_ASSERT(strlen(test_vector_other[counter].property_value) == string_size);
                TEST_ASSERT(0 == strcmp(test_vector_other[counter].property_value, string_val));

                TEST_ASSERT(ma_buffer_release(buffer) == MA_OK);
                TEST_ASSERT(ma_variant_release(variant_obj) == MA_OK);
                buffer = NULL;
                variant_obj = NULL;
            }
        }

        TEST_ASSERT(ma_table_release(table_variant_ptr) == MA_OK);
        table_variant_ptr = NULL;
    }


    // Extracting "Path 2" Section
    if(test_scenario == 1)
    {
        TEST_ASSERT(ma_table_get_value(properties_bag_variant_ptr, "Path 2", &table_variant_obj) == MA_OK);

        TEST_ASSERT(ma_variant_get_type(table_variant_obj, &vartype_obj) == MA_OK);
        TEST_ASSERT(vartype_obj == MA_VARTYPE_TABLE);
        TEST_ASSERT(ma_variant_get_table(table_variant_obj, &table_variant_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_release(table_variant_obj) == MA_OK);
        table_variant_obj = NULL;

        for(counter = 0; counter < sizeof(test_vector_other)/sizeof(test_vector_other[0]); counter++)
        {
            if(strcmp(test_vector_other[counter].property_path, "Path 2") == 0)
            {
                TEST_ASSERT(ma_table_get_value(table_variant_ptr, test_vector_other[counter].property_name, &variant_obj) == MA_OK);
                TEST_ASSERT(ma_variant_get_string_buffer(variant_obj, &buffer) == MA_OK);
                TEST_ASSERT(ma_buffer_get_string(buffer, &string_val, &string_size) == MA_OK);

                TEST_ASSERT(strlen(test_vector_other[counter].property_value) == string_size);
                TEST_ASSERT(0 == strcmp(test_vector_other[counter].property_value, string_val));

                TEST_ASSERT(ma_buffer_release(buffer) == MA_OK);
                TEST_ASSERT(ma_variant_release(variant_obj) == MA_OK);
                buffer = NULL;
                variant_obj = NULL;
            }
        }

        TEST_ASSERT(ma_table_release(table_variant_ptr) == MA_OK);
        table_variant_ptr = NULL;
    }

    // Extracting "Path 3" Section
    if(test_scenario == 1)
    {
        TEST_ASSERT(ma_table_get_value(properties_bag_variant_ptr, "Path 3", &table_variant_obj) == MA_OK);

        TEST_ASSERT(ma_variant_get_type(table_variant_obj, &vartype_obj) == MA_OK);
        TEST_ASSERT(vartype_obj == MA_VARTYPE_TABLE);
        TEST_ASSERT(ma_variant_get_table(table_variant_obj, &table_variant_ptr) == MA_OK);
        TEST_ASSERT(ma_variant_release(table_variant_obj) == MA_OK);
        table_variant_obj = NULL;

        for(counter = 0; counter < sizeof(test_vector_other)/sizeof(test_vector_other[0]); counter++)
        {
            if(strcmp(test_vector_other[counter].property_path, "Path 3") == 0)
            {
                TEST_ASSERT(ma_table_get_value(table_variant_ptr, test_vector_other[counter].property_name, &variant_obj) == MA_OK);
                TEST_ASSERT(ma_variant_get_string_buffer(variant_obj, &buffer) == MA_OK);
                TEST_ASSERT(ma_buffer_get_string(buffer, &string_val, &string_size) == MA_OK);

                TEST_ASSERT(strlen(test_vector_other[counter].property_value) == string_size);
                TEST_ASSERT(0 == strcmp(test_vector_other[counter].property_value, string_val));

                TEST_ASSERT(ma_buffer_release(buffer) == MA_OK);
                TEST_ASSERT(ma_variant_release(variant_obj) == MA_OK);
                buffer = NULL;
                variant_obj = NULL;
            }
        }

        TEST_ASSERT(ma_table_release(table_variant_ptr) == MA_OK);
        table_variant_ptr = NULL;
    }

    // In test_Scenario = 2, only minimal properties are requested, therefore, these sections should
    // not be there in the properties bag.
    if(test_scenario == 2)
    {
        TEST_ASSERT(ma_table_get_value(properties_bag_variant_ptr, "Path 1", &table_variant_obj) == MA_ERROR_OBJECTNOTFOUND);
        TEST_ASSERT(ma_table_get_value(properties_bag_variant_ptr, "Path 2", &table_variant_obj) == MA_ERROR_OBJECTNOTFOUND);
        TEST_ASSERT(ma_table_get_value(properties_bag_variant_ptr, "Path 3", &table_variant_obj) == MA_ERROR_OBJECTNOTFOUND);
    }

    TEST_ASSERT(ma_table_release(properties_bag_variant_ptr) == MA_OK);
    properties_bag_variant_ptr = NULL;

    // Control flow is complete, therefore, unlock the mutex now, so that control can exit.
    set_done();

    return MA_OK;
}

/**************************************************************************
Property Provider Sending property Tests.
**************************************************************************/
TEST(property_provider_tests, property_provider_send_properties_test)
{
    ma_message_t* publisher_payload = NULL;
	ma_dispatcher_t *dispatcher = NULL;
	ma_dispatcher_initialize(NULL,&dispatcher);
    /**************************************************************************
    Creating Property Client and registering callback
    **************************************************************************/
    TEST_ASSERT(ma_property_register_provider_callback(NULL, PRODUCT_ID, my_property_provider_callback, NULL) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_property_register_provider_callback(g_ma_client, PRODUCT_ID, NULL, NULL) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_property_register_provider_callback(g_ma_client, PRODUCT_ID, my_property_provider_callback, NULL) == MA_OK);

    // Trying to register a callback, which is already registered once
    TEST_ASSERT(ma_property_register_provider_callback(g_ma_client, PRODUCT_ID, my_property_provider_callback, NULL) == MA_ERROR_PROPERTY_PROVIDER_ALREADY_REGISTERED);

    /**************************************************************************
    Publishing (broadcasting) a message on the message bus, that the properties
    are requested.
    Message Type - Publish
    Properties -
        Test Scenario 1 ---> Minimal Props = "0" i.e. collect Full Properties
        Test Scenario 2 ---> Minimal Props = "1" i.e. collect Minimal Properties
        SESSION_ID
    Topic - ma.property.collect
    No Payload
    **************************************************************************/
    TEST_ASSERT(ma_message_create(&publisher_payload) == MA_OK);

    if(test_scenario == 1) TEST_ASSERT(ma_message_set_property(publisher_payload, "ma.property.minimal_props", "0") == MA_OK);
    if(test_scenario == 2) TEST_ASSERT(ma_message_set_property(publisher_payload, "ma.property.minimal_props", "1") == MA_OK);

    TEST_ASSERT(ma_message_set_property(publisher_payload, "ma.property.session_id", "12345") == MA_OK);
    TEST_ASSERT(ma_msgbus_publish(g_msgbus,"ma.property.collect", MSGBUS_CONSUMER_REACH_INPROC, publisher_payload) == MA_OK);

    done = 0;
    wait_until_done();

    /***************************************************************************
    Cleanup code
    ***************************************************************************/
    TEST_ASSERT(ma_message_release(publisher_payload) == MA_OK);
    publisher_payload = NULL;

    TEST_ASSERT(ma_property_unregister_provider_callback(NULL, PRODUCT_ID) == MA_ERROR_INVALIDARG);
    TEST_ASSERT(ma_property_unregister_provider_callback(g_ma_client, PRODUCT_ID) == MA_OK);

    // Trying to un-register callback, which is already unregistered once.
    TEST_ASSERT(ma_property_unregister_provider_callback(g_ma_client, PRODUCT_ID) == MA_ERROR_PROPERTY_PROVIDER_NOT_REGISTERED);
	ma_dispatcher_deinitialize(dispatcher);
}
