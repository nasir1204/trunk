#include "unity.h"
#include "unity_fixture.h"

#include <uv.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "ma/ma_common.h"
#include "ma/ma_message.h"
#include "ma/ma_msgbus.h"
#include "ma/ma_variant.h"
#include "ma/ma_dispatcher.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/internal/core/msgbus/ma_message_header.h"
#include "ma/logger/ma_file_logger.h"
#include "ma/ma_errors.h"

#include "dispatcher_tests_utils.h"

#define DISPATCHER_PROUDUCT_ID "ma.dispatcher.test"

#ifdef MA_WINDOWS
# include <crtdbg.h>
#include <Windows.h>
# define MA_TRACKLEAKS _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF)
#define SOFTWARE_ID_PATH "SOFTWARE\\McAfee\\Agent\\Applications\\EPOAGENT3000"
#define SOFTWARE_ID_PATH1 "SOFTWARE\\McAfee\\Agent\\Applications\\EPOAGENT3000"
#define MA_DISPATCHER_PRODUCT_INSTAL_PATH ""
#define MA_DISPATCHER_RSDK_PATH ""
#else
# define MA_TRACKLEAKS ((void *)0)
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#define SOFTWARE_ID_PATH "/etc/ma.d"
#endif

TEST_GROUP_RUNNER(dispatcher_test_group_without_rsdk_path) {
    do {RUN_TEST_CASE(dispatcher_tests_, variant_init_deinit_); }while(0);
    do {RUN_TEST_CASE(dispatcher_tests_, variant_no_init_but_deinit_); }while(0);    
}


TEST_SETUP(dispatcher_tests_) {
	#ifdef MA_WINDOWS
	ma_dispatcher_test_create_registry_path(SOFTWARE_ID_PATH);
    ma_dispatcher_test_create_registry(SOFTWARE_ID_PATH);
#else
    ma_dispatcher_test_create_registry("EPOAGENT3000");
#endif
}


TEST_TEAR_DOWN(dispatcher_tests_) {
	#ifdef MA_WINDOWS
   ma_dispatcher_tests_delete_registry(SOFTWARE_ID_PATH);
   ma_dispatcher_tests_delete_registry(SOFTWARE_ID_PATH1);
#else
    ma_dispatcher_tests_delete_registry("ma");
    ma_dispatcher_tests_delete_registry("ma1");
#endif
}

TEST(dispatcher_tests_, variant_init_deinit_) {
    ma_dispatcher_t *handle = NULL;
    ma_variant_t    *v1 = NULL;
    ma_variant_t    *v3 = NULL;
    ma_variant_t    *v2 = NULL;
    ma_variant_t    *variant_ptr = NULL;
    ma_bool_t       result = MA_FALSE;
    ma_buffer_t *buffer_ptr = NULL;
    ma_wbuffer_t *wbuffer_ptr = NULL;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_initialize(NULL,&handle));
    //TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_init());
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create(&v1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create(&v3));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(v1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(v3));

    //string test
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_string("hello", 5, &v1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_string("hello", 5, &v2));

    TEST_ASSERT_MESSAGE(ma_variant_get_string_buffer(v1, &buffer_ptr) == MA_OK, \
                        "char String Buffer cannot be retrieved from the Variant");
    ma_buffer_release(buffer_ptr);

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_is_equal(v1, v2, &result));
    TEST_ASSERT_MESSAGE(MA_TRUE  == result, "strings variant are different");
    TEST_ASSERT_MESSAGE(ma_variant_release(v1) == MA_OK, "The variant could not be destroyed");
    TEST_ASSERT_MESSAGE(ma_variant_release(v2) == MA_OK, "The variant could not be destroyed");
    
    TEST_ASSERT_MESSAGE(ma_variant_create_from_wstring(L"MA Security Agent", 17, &variant_ptr) == MA_OK, \
                        "String variant could not be created");

    TEST_ASSERT_MESSAGE(ma_variant_get_wstring_buffer(variant_ptr, &wbuffer_ptr) == MA_OK, \
                        "Wide char String Buffer cannot be retrieved from the Variant");
    ma_wbuffer_release(wbuffer_ptr);
    TEST_ASSERT_MESSAGE(ma_variant_release(variant_ptr) == MA_OK, "The variant could not be destroyed");

    //TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_deinit());
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_deinitialize(handle));
}


TEST(dispatcher_tests_, variant_no_init_but_deinit_) {
    ma_dispatcher_t *handle = NULL;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_initialize(NULL,&handle));
    //TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_deinit());
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_dispatcher_deinitialize(handle));
}

