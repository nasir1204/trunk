#include "ma/ma_common.h"

ma_error_t ma_dispatcher_test_create_registry(const char *registry_key);

ma_error_t ma_dispatcher_test_create_registry_path(const char *registry_key);

ma_error_t ma_dispatcher_tests_delete_registry(const char *key);