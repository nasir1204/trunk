#include "ma_ut_setup.h"

#include "ma/internal/services/ma_service.h"
#include "ma/internal/clients/ma/ma_client_internal.h"
#include "ma/logger/ma_file_logger.h"

#include "ma/internal/ma_macros.h"

#include "ma/internal/services/ma_policy_settings_bag.h"
#include "ma/internal/utils/configurator/ma_configurator.h"

#include "ma/internal/utils/macrypto/ma_crypto.h"
#include "ma/datastore/ma_ds_database.h"
#include "ma/internal/ma_strdef.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#ifndef MA_WINDOWS
    #include <unistd.h>
#endif

#define MA_UT_DB_SCHEDULER_FILENAME_STR         "ma.ut.db.scheduler"
#define MA_UT_DB_POLICY_FILENAME_STR            "ma.ut.db.policy"
#define MA_UT_DB_TASK_FILENAME_STR              "ma.ut.db.task"
#define MA_UT_DB_COMMON_SVC_FILENAME_STR        "ma.ut.db.cmnsvc"
#define MA_UT_DB_DEFAULT_FILENAME_STR           "ma.ut.db.default"

struct ma_ut_setup_s
{
    ma_client_t                                     *client;
    ma_logger_t                                     *logger;
    char                                            log_file_name[MA_MAX_LEN];
    ma_configurator_t                               *configurator;
    ma_policy_settings_bag_t                        *policy_settings_bag;
    ma_context_t                                    *context;
    ma_ut_policy_table_provider_callback_t          table_provider_cb;
    void                                            *table_provider_cb_data;
    ma_ut_policy_variant_provider_callback_t        variant_provider_cb;
    void                                            *variant_provider_cb_data;
    ma_ut_policy_buffer_provider_callback_t         buffer_provider_cb;
    void                                            *buffer_provider_cb_data;
};

static ma_error_t ma_ut_configurator_create(ma_configurator_t **configurator);
static ma_error_t ma_ut_agent_policy_settings_bag_create(ma_ut_setup_t *ut_setup, ma_policy_settings_bag_t **policy_settings_bag);

ma_error_t ma_ut_setup_create(const char *ut_name, const char *product_id, ma_ut_setup_t **setup) {
    if(product_id && setup) {
        ma_error_t rc = MA_OK;
        ma_ut_setup_t *self = (ma_ut_setup_t *)calloc(1,sizeof(ma_ut_setup_t));
        if(!self)       return MA_ERROR_OUTOFMEMORY;

        MA_MSC_SELECT(_snprintf, snprintf)(self->log_file_name, MA_MAX_LEN,"%s.log", ut_name);
        unlink(self->log_file_name);
        if(MA_OK == (rc = ma_file_logger_create(NULL, ut_name, NULL, (ma_file_logger_t **)&self->logger))) {
            if(MA_OK == (rc = ma_ut_configurator_create(&self->configurator))) {
                if(MA_OK == (rc = ma_ut_agent_policy_settings_bag_create(self, &self->policy_settings_bag))) {
                    if(MA_OK == (rc = ma_client_create(product_id, &self->client))) {
                        ma_client_set_logger(self->client, self->logger);
                        ma_client_set_thread_option(self->client, MA_MSGBUS_CALLBACK_THREAD_IO);
                        ma_client_set_consumer_reachability(self->client, MSGBUS_CONSUMER_REACH_INPROC);
                        if(MA_OK == (rc = ma_client_start(self->client))) {
                            ma_client_get_context(self->client,&self->context);
                            ma_context_add_object_info(self->context, MA_OBJECT_POLICY_SETTINGS_BAG_STR, &self->policy_settings_bag);
                            ma_context_add_object_info(self->context, MA_OBJECT_MA_CONFIGURATOR_NAME_STR, &self->configurator);
                            ma_context_add_object_info(self->context, MA_OBJECT_LOGGER_NAME_STR, &self->logger);
                            *setup = self;
                            return MA_OK;
                        }
                        ma_client_release(self->client);
                    }
                    ma_policy_settings_bag_release(self->policy_settings_bag);
                }
                ma_configurator_release(self->configurator);
            }
            ma_logger_release(self->logger);
            unlink(self->log_file_name);
        }
        free(self);
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

void ma_ut_setup_release(ma_ut_setup_t *self) {
    if(self) {
        ma_client_stop(self->client);
        ma_client_release(self->client);
        ma_policy_settings_bag_release(self->policy_settings_bag);
        ma_configurator_release(self->configurator);
        ma_logger_release(self->logger);
        unlink(self->log_file_name);
        free(self);
    }
}

ma_context_t *ma_ut_setup_get_context(ma_ut_setup_t *self) {
    return self->context;
}

ma_msgbus_t *ma_ut_setup_get_msgbus(ma_ut_setup_t *self) {
    ma_msgbus_t *bus = NULL;
    ma_client_get_msgbus(self->client, &bus);
    return bus;
}

ma_client_t *ma_ut_setup_get_client(ma_ut_setup_t *self) {
    return self->client;
}

ma_logger_t *ma_ut_setup_get_logger(ma_ut_setup_t *self) {
    return self->logger;
}


ma_ds_t *ma_ut_setup_get_datastore(ma_ut_setup_t *self) {
    return ma_configurator_get_datastore(self->configurator);
}

ma_db_t *ma_ut_setup_get_database(ma_ut_setup_t *self) {
    return ma_configurator_get_database(self->configurator);
}

void ma_ut_setup_set_policy_buffer_provider_callback(ma_ut_setup_t *self, ma_ut_policy_buffer_provider_callback_t cb, void *cb_data) {
    if(self) {
        self->buffer_provider_cb = cb;
        self->buffer_provider_cb_data = cb_data;
    }
}

void ma_ut_setup_set_policy_variant_provider_callback(ma_ut_setup_t *self, ma_ut_policy_variant_provider_callback_t cb, void *cb_data) {
    if(self) {
        self->variant_provider_cb = cb;
        self->variant_provider_cb_data = cb_data;
    }
}

void ma_ut_setup_set_policy_table_provider_callback(ma_ut_setup_t *self, ma_ut_policy_table_provider_callback_t cb, void *cb_data) {
    if(self) {
        self->table_provider_cb = cb;
        self->table_provider_cb_data = cb_data;
    }
}


/* configurator setup */

typedef struct ma_ut_configurator_s {
    ma_configurator_t                   configurator;
    ma_db_t                             *db_default;
    ma_db_t                             *db_policy;
    ma_db_t                             *db_task;
    ma_db_t                             *db_scheduler;
    ma_db_t                             *db_msgbus;
    ma_ds_t                             *ds_default;
    ma_ds_t                             *ds_scheduler;
}ma_ut_configurator_t;

static ma_error_t ut_configurator_get_ds_info(ma_configurator_t *configurator, int ds_type, struct ma_datastore_info_s *ds_info);
static ma_error_t ut_configurator_populate_agent_configuration(ma_configurator_t *configurator, struct ma_configuration_s *configuration);
static void ut_configurator_destroy(ma_configurator_t *configurator);

static ma_configurator_methods_t methods = {
    &ut_configurator_get_ds_info,
    &ut_configurator_populate_agent_configuration,
    &ut_configurator_destroy
};

static ma_error_t ma_ut_configurator_create(ma_configurator_t **configurator)
{
    if(configurator) {
        ma_ut_configurator_t *self = NULL;

        self = (ma_ut_configurator_t *)calloc(1, sizeof(ma_ut_configurator_t));
        if(!self)   return MA_ERROR_OUTOFMEMORY;

        /* clean up from old one if any */
        unlink(MA_UT_DB_POLICY_FILENAME_STR);
        unlink(MA_UT_DB_TASK_FILENAME_STR);
        unlink(MA_UT_DB_COMMON_SVC_FILENAME_STR);
        unlink(MA_UT_DB_SCHEDULER_FILENAME_STR);
        unlink(MA_UT_DB_DEFAULT_FILENAME_STR);

        ma_configurator_init((ma_configurator_t*)self, &methods, self);
        *configurator = (ma_configurator_t *)self;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


static ma_error_t ut_configurator_get_ds_info(ma_configurator_t *configurator, int ds_type, struct ma_datastore_info_s *ds_info) {
    if(configurator && ds_info) {
        ma_error_t rc = MA_OK;
        ma_ut_configurator_t *self = (ma_ut_configurator_t *)configurator;
        switch(ds_type) {
            case MA_DS_INFO_DEFAULT: {
                if(!self->ds_default)
                {
                    if(self->ds_default) ma_ds_release(self->ds_default); self->ds_default = NULL;
                    if(self->db_default) ma_db_close(self->db_default); self->db_default = NULL;
                    if(MA_OK == (rc = ma_db_open(MA_UT_DB_DEFAULT_FILENAME_STR, MA_DB_OPEN_READWRITE, &self->db_default))) {
                        if(MA_OK == (rc = ma_ds_database_create(self->db_default, MA_DS_DEFAULT_INSTANCE_NAME, (ma_ds_database_t **)&self->ds_default))){
                            ds_info->ds = self->ds_default;
                            ds_info->db = self->db_default;
                            return MA_OK;
                        }
                        ma_db_close(self->db_default);
                    }
                }
                else
                {
                    ds_info->ds = self->ds_default;
                    ds_info->db = self->db_default;
                }
            }
                break;
            case MA_DS_INFO_POLICY: {
                if(self->db_policy) ma_db_close(self->db_policy); self->db_policy = NULL;
                if(MA_OK == (rc = ma_db_open(MA_UT_DB_POLICY_FILENAME_STR, MA_DB_OPEN_READWRITE, &self->db_policy))) {
                    ds_info->ds = NULL;
                    ds_info->db = self->db_policy;
                    return MA_OK;
                }
            }
                break;
            case MA_DS_INFO_TASK: {
                if(self->db_task) ma_db_close(self->db_task); self->db_task = NULL;
                if(MA_OK == (rc = ma_db_open(MA_UT_DB_TASK_FILENAME_STR, MA_DB_OPEN_READWRITE, &self->db_task))) {
                    ds_info->ds = NULL;
                    ds_info->db = self->db_task;
                    return MA_OK;
                }
            }
                break;
            case MA_DS_INFO_SCHEDULER: {
                if(self->ds_scheduler) ma_ds_release(self->ds_scheduler); self->ds_scheduler = NULL;
                if(self->db_scheduler) ma_db_close(self->db_scheduler); self->db_scheduler = NULL;
                if(MA_OK == (rc = ma_db_open(MA_UT_DB_SCHEDULER_FILENAME_STR,MA_DB_OPEN_READWRITE, &self->db_scheduler))) {
                    if(MA_OK == (rc = ma_ds_database_create(self->db_default, MA_DS_DEFAULT_INSTANCE_NAME, (ma_ds_database_t **)&self->db_scheduler))){
                        ds_info->ds = self->ds_scheduler;
                        ds_info->db = NULL;
                        return MA_OK;
                    }
                    ma_db_close(self->db_scheduler);
                }
            }
                break;
            case MA_DS_INFO_MSGBUS: {
                if(self->db_msgbus) ma_db_close(self->db_msgbus); self->db_msgbus = NULL;
                if(MA_OK == (rc = ma_db_open(MA_UT_DB_COMMON_SVC_FILENAME_STR,MA_DB_OPEN_READWRITE, &self->db_msgbus))) {
                    ds_info->ds = NULL;
                    ds_info->db = self->db_msgbus;
                    return MA_OK;
                }
            }
            default:
                return MA_ERROR_INVALID_OPTION;
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}


static ma_error_t ut_configurator_populate_agent_configuration(ma_configurator_t *configurator, struct ma_configuration_s *configuration) {
    if(configurator && configuration) {
        configuration->agent_data_path = ".\\AGENT\\";
        configuration->agent_install_path = ".\\AGENT\\";
        configuration->agent_lib_path = ".\\AGENT\\";
        configuration->agent_tools_lib_path = ".\\AGENT\\";
        configuration->agent_configuration = (ma_agent_configuration_t*)calloc(1, sizeof(ma_agent_configuration_t));
        configuration->agent_configuration->agent_mode = MA_AGENT_MODE_MANAGED_INT;
        configuration->agent_configuration->agent_crypto_mode = MA_CRYPTO_MODE_NON_FIPS;
        configuration->agent_configuration->agent_crypto_role = MA_CRYPTO_ROLE_USER;
        configuration->agent_configuration->vdi_mode = 0;
        memset(configuration->agent_configuration->agent_guid , 1, UUID_LEN_TXT - 1);
        configuration->agent_configuration->agent_guid[UUID_LEN_TXT - 1] = '\0';
        memset(configuration->agent_configuration->tenant_id , 1, UUID_LEN_TXT - 1);
        configuration->agent_configuration->tenant_id[UUID_LEN_TXT - 1] = '\0';
        configuration->agent_configuration->mode_changed = MA_FALSE;

        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static void ut_configurator_destroy(ma_configurator_t *configurator) {
    if(configurator) {
        ma_ut_configurator_t *self = (ma_ut_configurator_t *)configurator;

        if(self->ds_scheduler){
            ma_ds_release(self->ds_scheduler); self->ds_scheduler = NULL;
        }

        if(self->db_scheduler) {
                ma_db_close(self->db_scheduler); self->db_scheduler = NULL;
                unlink(MA_DB_SCHEDULER_FILENAME_STR);
        }

        if(self->db_policy) {
            ma_db_close(self->db_policy); self->db_policy = NULL;
            unlink(MA_DB_POLICY_FILENAME_STR);
        }

        if(self->db_task) {
            ma_db_close(self->db_task); self->db_task = NULL;
            unlink(MA_DB_TASK_FILENAME_STR);
        }

        if(self->db_msgbus) {
            ma_db_close(self->db_msgbus);
            unlink(MA_UT_DB_COMMON_SVC_FILENAME_STR);
        }

        if(self->ds_default) {
            ma_ds_release(self->ds_default); self->ds_default = NULL;
        }
        if(self->db_default){
            ma_db_close(self->db_default); self->db_default = NULL;
            unlink(MA_DB_DEFAULT_FILENAME_STR);
        }

        unlink(MA_UT_DB_POLICY_FILENAME_STR);
        unlink(MA_UT_DB_TASK_FILENAME_STR);
        unlink(MA_UT_DB_COMMON_SVC_FILENAME_STR);
        unlink(MA_UT_DB_SCHEDULER_FILENAME_STR);
        unlink(MA_UT_DB_DEFAULT_FILENAME_STR);

        free(self);
    }
}


/*policy settings bag*/


typedef struct ma_ut_agent_policy_settings_bag_s {
    ma_policy_settings_bag_t    base;
    ma_ut_setup_t               *ut_setup;

}ma_ut_agent_policy_settings_bag_t;

static ma_error_t get_bool(ma_policy_settings_bag_t *bag, const char *section, const char *key, ma_bool_t *value);
static ma_error_t get_int(ma_policy_settings_bag_t *bag, const char *section, const char *key, ma_int32_t *value);
static ma_error_t get_str(ma_policy_settings_bag_t *bag, const char *section, const char *key, const char **value);
static ma_error_t get_buffer(ma_policy_settings_bag_t *bag, const char *section, const char *key, ma_buffer_t **value);
static ma_error_t get_variant(ma_policy_settings_bag_t *self, const char *section, const char *key, ma_variant_t **value);
static ma_error_t get_table(ma_policy_settings_bag_t *bag, const char *section, ma_table_t **value);
static void destroy(ma_policy_settings_bag_t *bag); /*< virtual destructor */


static const ma_policy_settings_bag_methods_t policy_methods = {
    &get_bool,
    &get_int,
    &get_str,
    &get_buffer,
    &get_variant,
    &destroy
};

static ma_error_t ma_ut_agent_policy_settings_bag_create(ma_ut_setup_t *ut_setup, ma_policy_settings_bag_t **bag) {
    if(bag ) {
        ma_ut_agent_policy_settings_bag_t *self = (ma_ut_agent_policy_settings_bag_t *)calloc(1, sizeof(ma_ut_agent_policy_settings_bag_t));
        if(!self) return MA_ERROR_OUTOFMEMORY;

        ma_policy_settings_bag_init((ma_policy_settings_bag_t *)self, &policy_methods, self);
        self->ut_setup = ut_setup;
        *bag = (ma_policy_settings_bag_t *)self;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG ;
}

static void destroy(ma_policy_settings_bag_t *bag) {
    if(bag) {
        ma_ut_agent_policy_settings_bag_t *self = (ma_ut_agent_policy_settings_bag_t *)bag;
        free(self);
    }
}

static ma_error_t get_table(ma_policy_settings_bag_t *bag, const char *section, ma_table_t **value) {
    if(bag) {
        ma_ut_agent_policy_settings_bag_t *self = (ma_ut_agent_policy_settings_bag_t *)bag;
        if(self->ut_setup->table_provider_cb)
            return self->ut_setup->table_provider_cb(section, value, self->ut_setup->table_provider_cb_data);
    }

    return MA_ERROR_INVALIDARG;
}

static ma_error_t get_variant(ma_policy_settings_bag_t *bag, const char *section, const char *key, ma_variant_t **value) {
    if(bag) {
        ma_ut_agent_policy_settings_bag_t *self = (ma_ut_agent_policy_settings_bag_t *)bag;
        if(self->ut_setup->variant_provider_cb)
            return self->ut_setup->variant_provider_cb(section, key, value, self->ut_setup->variant_provider_cb_data);
    }

    return MA_ERROR_INVALIDARG;
}

static ma_error_t get_buffer(ma_policy_settings_bag_t *bag, const char *section, const char *key, ma_buffer_t **value) {
    if(bag) {
        ma_ut_agent_policy_settings_bag_t *self = (ma_ut_agent_policy_settings_bag_t *)bag;
        if(self->ut_setup->buffer_provider_cb)
            return self->ut_setup->buffer_provider_cb(section, key, value, self->ut_setup->buffer_provider_cb_data);
    }

    return MA_ERROR_INVALIDARG;
}

static ma_error_t get_str(ma_policy_settings_bag_t *bag, const char *section, const char *key, const char **value ) {
    ma_error_t rc = MA_OK;
    ma_buffer_t *dummy_buffer = NULL;
    if((MA_OK == (rc = get_buffer(bag, section, key, &dummy_buffer)) && dummy_buffer)) {
        const char *new_value = NULL;
        size_t size = 0;
        if((MA_OK == (rc = ma_buffer_get_string(dummy_buffer, &new_value, &size))) && new_value)
            *value = strdup(new_value);
        (void)ma_buffer_release(dummy_buffer);
    }
    return rc;
}

static ma_error_t get_int(ma_policy_settings_bag_t *bag, const char *section, const char *key, ma_int32_t *value) {
    ma_error_t rc = MA_OK;
    ma_buffer_t *buffer = NULL;
    if(MA_OK == (rc = get_buffer(bag, section, key, &buffer))) {
        const char *new_value = NULL;
        size_t size = 0;
        if((MA_OK == (rc = ma_buffer_get_string(buffer, &new_value, &size))) && new_value)
            *value = atoi(new_value);
        (void) ma_buffer_release(buffer);
    }
    return rc;
}

static ma_error_t get_bool(ma_policy_settings_bag_t *bag, const char *section, const char *key, ma_bool_t *value)  {
    ma_error_t rc = MA_OK;
    ma_int32_t dummy_value = 0;

    if(MA_OK == (rc = get_int(bag, section, key, &dummy_value)))
        *value = dummy_value ? MA_TRUE :MA_FALSE;

    return rc;
}

