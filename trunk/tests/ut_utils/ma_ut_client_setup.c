#include "ma_ut_setup.h"
#include "ma/internal/services/ma_service.h"
#include "ma/internal/clients/ma/ma_client_internal.h"
#include "ma/logger/ma_file_logger.h"

#include "ma/internal/ma_macros.h"

#include "ma/datastore/ma_ds_database.h"
#include "ma/internal/ma_strdef.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#ifndef MA_WINDOWS
    #include <unistd.h>
#endif

struct ma_ut_setup_s {
    ma_client_t                                     *client;
    ma_logger_t                                     *logger;
    char                                            log_file_name[MA_MAX_LEN];
    ma_context_t                                    *context;
};


ma_error_t ma_ut_setup_create(const char *ut_name, const char *product_id, ma_ut_setup_t **setup) {
    if(product_id && setup) {
        ma_error_t rc = MA_OK;
        ma_ut_setup_t *self = (ma_ut_setup_t *)calloc(1,sizeof(ma_ut_setup_t));
        if(!self)       return MA_ERROR_OUTOFMEMORY;

        MA_MSC_SELECT(_snprintf, snprintf)(self->log_file_name, MA_MAX_LEN,"%s.log", ut_name);
        if(MA_OK == (rc = ma_file_logger_create(NULL, ut_name, NULL, (ma_file_logger_t **)&self->logger))) {
            if(MA_OK == (rc = ma_client_create(product_id, &self->client))) {
                ma_client_set_logger(self->client, self->logger);
                ma_client_set_thread_option(self->client, MA_MSGBUS_CALLBACK_THREAD_IO);
                ma_client_set_consumer_reachability(self->client, MSGBUS_CONSUMER_REACH_INPROC);
                if(MA_OK == (rc = ma_client_start(self->client))) {
                    ma_client_get_context(self->client,&self->context);
                    ma_context_add_object_info(self->context, MA_OBJECT_LOGGER_NAME_STR, &self->logger);
                    *setup = self;
                    return MA_OK;
                }
                ma_client_release(self->client);
            }
        }
        ma_logger_release(self->logger);
        unlink(self->log_file_name);
        free(self);
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

void ma_ut_setup_release(ma_ut_setup_t *self) {
    if(self) {
        ma_client_stop(self->client);
        ma_client_release(self->client);
        ma_logger_release(self->logger);
        //unlink(self->log_file_name);
        free(self);
    }
}

ma_context_t *ma_ut_setup_get_context(ma_ut_setup_t *self) {
    return self->context;
}

ma_msgbus_t *ma_ut_setup_get_msgbus(ma_ut_setup_t *self) {
    ma_msgbus_t *bus = NULL;
    ma_client_get_msgbus(self->client, &bus);
    return bus;
}

ma_client_t *ma_ut_setup_get_client(ma_ut_setup_t *self) {
    return self->client;
}

ma_logger_t *ma_ut_setup_get_logger(ma_ut_setup_t *self) {
    return self->logger;
}
