#include "ma/ma_client.h"
#include "ma/internal/clients/ma/ma_client_internal.h"
#include "ma/ma_dispatcher.h"
/*
This file is a copy of ma_client.c which is a part of ma.lib. The dispatcher initialization and
release is removed from here because it conflicts with the test setup for the client UTs.

This ma_client.c and ma_ut_client_setup.c should be build as a part of the test project and ma.lib
is not required to be linked.
*/


#include "ma/logger/ma_logger.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/utils/context/ma_context.h"
#include "ma/internal/clients/ma/client/ma_client_handler.h"

#include <stdlib.h>
#include <string.h>


ma_error_t ma_client_create(const char *product_id,  ma_client_t **client) {
    if(product_id && client) {
        ma_error_t rc = MA_OK;
        ma_client_t *self = (ma_client_t *)calloc(1, sizeof(ma_client_t));
        self->dispatcher = NULL; // Dispatcher is removed in this client for UT purposes.
        if(self) {
                if(MA_OK == (rc = ma_client_handler_create(product_id, &self->client_handler))) {
                    *client = self;
                    return MA_OK;
                }
            free(self);
            return rc;
        }
        return MA_ERROR_OUTOFMEMORY;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_client_start(ma_client_t *self) {
    ma_client_handler_set_logger(self->client_handler, self->logger);
    return ma_client_handler_start(self->client_handler);
}

ma_error_t ma_client_stop(ma_client_t *self) {
    return ma_client_handler_stop(self->client_handler);
}

ma_error_t ma_client_release(ma_client_t *self) {
    if(self) {
        if(self->client_handler) ma_client_handler_release(self->client_handler);
        free(self);
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_client_get_msgbus(ma_client_t *self, ma_msgbus_t **msgbus) {
    return ma_client_handler_get_msgbus(self->client_handler, msgbus);
}

ma_error_t ma_client_set_msgbus(ma_client_t *self, ma_msgbus_t *msgbus) {
    return ma_client_handler_set_msgbus(self->client_handler, msgbus);
}

ma_error_t ma_client_set_thread_option(ma_client_t *self, ma_msgbus_callback_thread_options_t thread_option) {
    return ma_client_handler_set_thread_option(self->client_handler, thread_option);
}

ma_error_t ma_client_get_thread_option(ma_client_t *self, ma_msgbus_callback_thread_options_t *thread_option) {
    return ma_client_handler_get_thread_option(self->client_handler, thread_option);
}

