#include "ma/internal/services/udp_server/ma_udp_msg_handler.h"
#include "ma/internal/services/udp_server/ma_udp_connection.h"

#include "ma/internal/services/p2p/ma_p2p_discovery_hanlder.h"
#include "ma/internal/services/p2p/ma_p2p_db_manager.h"

#include "ma/internal/services/ma_agent_policy_settings_bag.h" 
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/defs/ma_http_server_defs.h"


#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include "ma/internal/utils/platform/ma_system_property.h"

#include "ma/ma_log.h"

#include "p2p_header.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define LOG_FACILITY_NAME "udp"

static ma_bool_t on_match(ma_udp_msg_handler_t *handler, ma_udp_msg_t *c_msg) ;
static ma_error_t on_msg(ma_udp_msg_handler_t *handler, struct ma_udp_connection_s *c_request, ma_udp_msg_t *c_msg) ;
static ma_error_t add_ref(ma_udp_msg_handler_t *handler) ;
static ma_error_t on_release(ma_udp_msg_handler_t *handler) ;

static const ma_udp_msg_handler_methods_t local_p2p_discovery_handler_methods = {
	&on_match,
	&on_msg,
	&add_ref,
	&on_release
} ;



ma_error_t local_p2p_discovery_handler_create(struct ma_context_s *context, ma_udp_msg_handler_t **discovery_handler) {
    if(discovery_handler) {
        ma_p2p_discovery_handler_t *tmp = (ma_p2p_discovery_handler_t *)calloc(1, sizeof(ma_p2p_discovery_handler_t)) ;

        if(!tmp)    return MA_ERROR_OUTOFMEMORY ;

		ma_udp_msg_handler_init(&tmp->base, &local_p2p_discovery_handler_methods, (void *)tmp) ;
		MA_ATOMIC_INCREMENT(tmp->ref_count) ;
		tmp->context = context ;
		*discovery_handler = (ma_udp_msg_handler_t*)tmp ;

        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t local_p2p_discovery_handler_release(ma_p2p_discovery_handler_t *self) {
    if(self) {

		if(0 == MA_ATOMIC_DECREMENT(self->ref_count)) {
			self->context = NULL ;
			self->data = NULL ;

			free(self) ;
		}
	}
    return MA_OK  ;
}

/* The p2p handler on_match function is for p2p content discovery messages */
static ma_bool_t on_match(ma_udp_msg_handler_t *handler, ma_udp_msg_t *c_msg) {
	if(handler && c_msg) {
		ma_error_t rc = MA_OK ;
		ma_bool_t rc1 = MA_FALSE ;
		ma_message_t *msg = NULL ;
		ma_p2p_discovery_handler_t *self = (ma_p2p_discovery_handler_t *)handler ;

		if(MA_OK == (rc = ma_udp_msg_get_ma_message(c_msg, &msg))) {
			char *msg_type = NULL ;
			/* TODO : need to revisit as per the udp message specs */
			ma_message_get_property(msg, MA_P2P_MSG_KEY_TYPE_STR, &msg_type) ;

			if(msg_type && 0 == strcmp(msg_type, MA_P2P_MSG_PROP_VAL_RQST_CONTENT_DISCOVERY_STR)) {
				rc1 = MA_TRUE ;
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "udp message matched for p2p content discovery") ;
			}

			ma_message_release(msg) ; msg = NULL ;
		}
		return rc1 ;
	}
	return MA_FALSE ;
}

/* The local p2p on_msg() function replays the recieved message back to the client with the http_port property set
in the message. Refer to ma_ut_setup.c file in this test project to see how the http_port is set.
*/
static ma_error_t on_msg(ma_udp_msg_handler_t *handler, ma_udp_connection_t *c_request, ma_udp_msg_t *c_msg) {
	if(handler && c_request && c_msg) {
		ma_error_t rc = MA_OK ;
		ma_p2p_discovery_handler_t *self = (ma_p2p_discovery_handler_t *)handler ;
		ma_message_t *msg = NULL ;
		char *content_hash = NULL ;
		ma_bool_t is_exists = MA_FALSE ;
		
		
		ma_udp_msg_get_ma_message(c_msg, &msg) ;
		ma_message_get_property(msg, MA_P2P_MSG_KEY_CONTENT_HASH_STR, &content_hash) ;

		MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "processing p2p content  discovery for hash(%s).", content_hash) ;

		if(content_hash) {
			(void)ma_p2p_db_is_content_exists(ma_configurator_get_msgbus_database(MA_CONTEXT_GET_CONFIGURATOR(self->context)), content_hash, &is_exists) ;

			is_exists=MA_TRUE;

			if(is_exists)  {
				ma_temp_buffer_t temp_url = MA_TEMP_BUFFER_INIT ;
				char *p = NULL ;
				ma_int32_t http_port = 0 ;	
				ma_policy_settings_bag_t *pso_bag = MA_CONTEXT_GET_POLICY_SETTINGS_BAG(self->context) ;
				//const ma_network_system_property_t *net = ma_system_property_network_get(MA_CONTEXT_GET_SYSTEM_PROPERTY(self->context)) ;
				ma_udp_msg_t *resp_udp_msg = NULL ;
				ma_message_t *resp_msg = NULL ;

				ma_policy_settings_bag_get_int(pso_bag, MA_HTTP_SERVER_SERVICE_SECTION_NAME_STR, MA_HTTP_SERVER_KEY_PORT_INT, MA_TRUE,&http_port,0) ;

				/*ma_temp_buffer_reserve(&temp_url, strlen(MA_P2P_CONTENT_URL_FORMAT_STR) + strlen(net->ip_address_formated) + sizeof(ma_uint32_t) + strlen(content_hash) + 1) ;
				MA_MSC_SELECT(_snprintf, snprintf)(p = (char *)ma_temp_buffer_get(&temp_url), ma_temp_buffer_capacity(&temp_url) ,MA_P2P_CONTENT_URL_FORMAT_STR, net->ip_address_formated, http_port, content_hash) ;*/
				//http_port = 1234;

				ma_temp_buffer_reserve(&temp_url, sizeof(ma_uint32_t)+ 1) ;
				MA_MSC_SELECT(_snprintf, snprintf)(p = (char *)ma_temp_buffer_get(&temp_url), ma_temp_buffer_capacity(&temp_url) , "%d", http_port) ;

				//MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_DEBUG, "p2p content found for hash(%s), url(%s) formed.", content_hash, p = (char *)ma_temp_buffer_get(&temp_url)) ;

				if(MA_OK == (rc = ma_udp_msg_create(&resp_udp_msg)))
				{
					if(MA_OK == (rc = ma_message_create(&resp_msg))) {
						ma_message_set_property(resp_msg, MA_P2P_MSG_KEY_TYPE_STR, MA_P2P_MSG_PROP_VAL_RPLY_CONTENT_DISCOVERY_STR) ;
						ma_message_set_property(resp_msg, MA_P2P_MSG_KEY_CONTENT_HASH_STR, content_hash) ;
						ma_message_set_property(resp_msg, MA_P2P_MSG_KEY_CONTENT_URL_PORT_STR, p = (char *)ma_temp_buffer_get(&temp_url)) ;

						ma_udp_msg_set_ma_message(resp_udp_msg, resp_msg) ;

						rc = ma_udp_connection_post_response(c_request, resp_udp_msg) ;
					
						(void)ma_message_release(resp_msg) ;	resp_msg = NULL ;
					}
					(void)ma_udp_msg_release(resp_udp_msg) ;	resp_udp_msg = NULL ;
				}
				if(MA_OK != rc)
					MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "Falied(%d) to send response for p2p content discovery for hash(%s).", rc , content_hash) ;
				ma_temp_buffer_uninit(&temp_url) ;
			}
			else {
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "No p2p content for hash(%s).",content_hash) ;
			}
		}
		else {
				MA_LOG(MA_CONTEXT_GET_LOGGER(self->context), MA_LOG_SEV_ERROR, "No hash found in message.",content_hash) ;
		}

		ma_message_release(msg) ;	msg = NULL ;
		return MA_OK ;
	}
	return MA_ERROR_INVALIDARG ;
}

static ma_error_t add_ref(ma_udp_msg_handler_t *handler) {
	if(handler) {
		ma_p2p_discovery_handler_t *self = (ma_p2p_discovery_handler_t *)handler ;
		MA_ATOMIC_INCREMENT(self->ref_count) ;
	}
	return MA_ERROR_INVALIDARG ;
}

static ma_error_t on_release(ma_udp_msg_handler_t *handler) {
	if(handler) {
		ma_p2p_discovery_handler_t *self = (ma_p2p_discovery_handler_t *)handler ;
		(void)local_p2p_discovery_handler_release(self) ;
	}
	return MA_OK  ;
}
