/*****************************************************************************
Main Test runner for all discovery client and server tests.
******************************************************************************/

#include "unity.h"
#include "unity_fixture.h"
#include "ma/ma_common.h"

#ifdef MA_WINDOWS
#include <windows.h>
#endif
#ifdef _MSC_VER
# include <crtdbg.h> /* */
# define MA_TRACKLEAKS _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF)
#else
# define MA_TRACKLEAKS ((void *)0)
#endif


static void run_all_tests(void)
{  
	do {RUN_TEST_GROUP(ma_udp_server_test_group); } while(0);
	do {RUN_TEST_GROUP(ma_udp_server_test_group_relay_enabled_diff_port); } while(0);
	do {RUN_TEST_GROUP(ma_udp_server_test_group_p2p_enabled); } while(0);
	do {RUN_TEST_GROUP(ma_udp_server_test_group_relay_p2p_disabled); } while(0);
	do {RUN_TEST_GROUP(ma_udp_server_relay_client_test_group); } while(0);
}

int main(int argc, char *argv[])
{
    MA_TRACKLEAKS;
    return UnityMain(argc, argv, run_all_tests);
}
