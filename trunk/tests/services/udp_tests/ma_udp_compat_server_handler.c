#include "ma/internal/services/udp_server/ma_udp_connection.h"
#include "ma/internal/services/udp_server/ma_udp_msg_handler.h"
#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/utils/threading/ma_atomic.h"

#include "ma/internal/ma_macros.h"

#include <stdlib.h>
#include <stdio.h>

static ma_bool_t udp_handler_on_match(ma_udp_msg_handler_t *self, ma_udp_msg_t *c_msg);
static ma_error_t udp_handler_on_msg(ma_udp_msg_handler_t *self, struct ma_udp_connection_s *c_request, ma_udp_msg_t *c_msg);
static ma_error_t udp_handler_add_ref(ma_udp_msg_handler_t *self);
static ma_error_t udp_handler_release(ma_udp_msg_handler_t *self);

static ma_udp_msg_handler_methods_t methods = {
	&udp_handler_on_match,
	&udp_handler_on_msg,
	&udp_handler_add_ref,
	&udp_handler_release
};

typedef struct ma_udp_handler_s ma_udp_handler_t;
struct ma_udp_handler_s{
	ma_udp_msg_handler_t		base;

	ma_logger_t					*logger;

	ma_atomic_counter_t			ref_count;
};

ma_error_t ma_udp_compat_handler_create(ma_udp_msg_handler_t **self, ma_logger_t *logger){
	ma_udp_handler_t *handler = (ma_udp_handler_t*) calloc(1, sizeof(ma_udp_handler_t));
	if(handler && logger){
		MA_ATOMIC_INCREMENT(handler->ref_count);
		handler->logger = logger;
		ma_udp_msg_handler_init((ma_udp_msg_handler_t*)handler, &methods, handler);
		*self = (ma_udp_msg_handler_t*)handler;
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
}


static ma_bool_t udp_handler_on_match(ma_udp_msg_handler_t *self, ma_udp_msg_t *c_msg){
	ma_message_t *msg = NULL;
	ma_bool_t match = MA_FALSE;
	ma_buffer_t *buffer = NULL;
	if(MA_OK == ma_udp_msg_get_raw_message(c_msg, &buffer)){
		const char *type = NULL;
		size_t size = 0;

		ma_buffer_get_string(buffer, &type, &size);
		if(type && 0 == strcmp(type, "compat_relay")){
			printf("\n\n\n\n compat relay received \n\n\n\n");		
			match = MA_TRUE;
		}
		(void)ma_buffer_release(buffer);
	}
	return match;
}


static ma_error_t udp_handler_on_msg(ma_udp_msg_handler_t *self, struct ma_udp_connection_s *c_request, ma_udp_msg_t *c_msg){
	ma_udp_msg_t *response = NULL;
	ma_udp_handler_t *handler = (ma_udp_handler_t*)(self);

	if(MA_OK == ma_udp_msg_create(&response)){
		ma_message_t *msg = NULL;
		if(MA_OK == ma_message_create(&msg)){
			char *port = "8083";
			/*form message*/
			(void)ma_message_set_property(msg, "type", "relay1");				
			(void)ma_message_set_property(msg, "port", port);
			(void)ma_udp_msg_set_ma_message(response, msg);
			(void)ma_message_release(msg);

			/*udp connection response*/
			(void)ma_udp_connection_set_delayed_response(c_request, MA_TRUE);
			(void)ma_udp_connection_set_response_delay_timeout(c_request, 500);
			(void)ma_udp_connection_post_response(c_request, response);
		}			
		ma_udp_msg_release(response);
	}	
	return MA_OK;
}

static ma_error_t udp_handler_add_ref(ma_udp_msg_handler_t *self){
	ma_udp_handler_t *handler = (ma_udp_handler_t*)(self);
	MA_ATOMIC_INCREMENT(handler->ref_count);
	return MA_OK;
}

static ma_error_t udp_handler_release(ma_udp_msg_handler_t *self){
	ma_udp_handler_t *handler = (ma_udp_handler_t*)(self);
	if(0 == MA_ATOMIC_DECREMENT(handler->ref_count))
		free(handler);	
	return MA_OK;
}