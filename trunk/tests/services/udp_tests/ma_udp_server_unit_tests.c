#include "unity.h"
#include "unity_fixture.h"
#include "ma/logger/ma_console_logger.h"

#include "ma/internal/ma_macros.h"

#include "ma/internal/clients/udp/ma_udp_request.h"
#include "ma/internal/services/udp_server/ma_udp_server.h"
#include "ma/internal/defs/ma_relay_defs.h"

#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/utils/event_loop/ma_loop_worker.h"

#include "ma_ut_setup.h"
#include "p2p_header.h"

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "udp_test"


#define PORT			8089
#define ADDRESS			"ff08::9"
/*
Test Approach: 

Use the same test project for UDP server and UDP client. The approach involves intializing the server and client in
the test setup. And use a timer call back function to start the server and send the udp request to the server
in udp_async_send _call(). Use only async_send() in this test project. The verification  is by means of checking the 
repsonse message from the server in the client response callbacks that has beeen written.

Server Initialization:
Server is initialized with the multicast address and port and listens ona socket. Then the server is registered with
the handlers to handle
the different types of messages it receives. A handler is for handling a specific type of message and each handlers
has a on-match and on-msg function implemented. So if you want to support a new message, write a handler for it and
register with the server. The different handlers written in this test are relay, compat, p2p and generic udp 
message handler.

Client Initialization:
The client is initialized with a multicast address and port. The client is used to send the udp_request message to 
the server.

Request Initialization:
The udp request is such that one udp request can contain a max of 2 messages. We create a message(MA message or 
raw message) and the message is associated with the request and the request is sent to the udp_server in async_send()
fashion. The udp_request is associated with a send and receive callbacks, where in the callbacks we implement the 
functionality to verify the received responses from the udp_server.
*/

static ma_loop_worker_t		*ma_loop_worker = NULL;
static ma_event_loop_t		*ma_ev_loop = NULL;
static ma_logger_t			*logger = NULL;
static ma_udp_msg_handler_t *handler = NULL;
static ma_udp_msg_handler_t *relay_handler=NULL;
static ma_udp_msg_handler_t *p2p_handler=NULL;
static ma_udp_server_t		*server = NULL;

static ma_udp_client_t		*udp_client = NULL;
/* UDP request array for basic MA, relay and P2P */
static ma_udp_request_t		*udp_req[3] = {NULL,NULL,NULL};

ma_context_t *g_ma_context = NULL;
ma_ut_setup_t *g_ut_setup = NULL;


static int what = 0;
static int send_come = 0;
static int response_come = 0;
static int final_come = 0;

extern ma_error_t local_p2p_discovery_handler_create(struct ma_context_s *context, ma_udp_msg_handler_t **discovery_handler);

//Send a raw message
struct raw_t {
	int x;
	float y;
	char rawstring[20];
};

static struct raw_t raw_data = {
1,
2.5,
"helloworld"
};

extern ma_error_t ma_udp_handler_create(ma_udp_msg_handler_t **self, ma_logger_t *logger);
extern ma_error_t ma_relay_discovery_handler_create(ma_udp_msg_handler_t **self);
extern ma_error_t ma_relay_discovery_handler_configure(ma_udp_msg_handler_t *self, ma_context_t *context);

extern ma_error_t ma_service_ut_context_create(const char *product_id, ma_bool_t thread_pool_enabled, ma_context_t **context);
/* The ut_set_up_create() call is used to enable the different tests like relay enable, p2p enable, same port/different port */
extern ma_error_t ma_ut_setup_create(const char *ut_name, const char *product_id, ma_bool_t relay, ma_bool_t same_port, ma_bool_t p2p, ma_ut_setup_t **ut_setup);
extern ma_context_t *ma_ut_setup_get_context(ma_ut_setup_t *ut_setup);

TEST_GROUP_RUNNER(ma_udp_server_test_group)
{
    do {RUN_TEST_CASE(ma_udp_server_test_ut, udp_server_test);} while(0);
}

ma_error_t udp_request_send_cb(ma_udp_request_t *request, void *user_data, ma_bool_t *stop){
	MA_LOG(logger, MA_LOG_SEV_DEBUG, "udp_request_send_cb");
	send_come = 1;
	return MA_OK;
}

/* Response callback for the udp request. It checks the udp response and retrieves the message and prints the 
payload if present. As of now the same repsonse handler handles p2p , relay, raw messages.

It also sets the response_come variable which is later checked in the timer callback.
*/
ma_error_t udp_response_cb(ma_udp_request_t *request, ma_udp_response_t *response, void *user_data, ma_bool_t *stop){
	MA_LOG(logger, MA_LOG_SEV_DEBUG, "udp_response_cb");
	MA_LOG(logger,MA_LOG_SEV_DEBUG, "Response: %s",(char*) user_data);
	//Need to print the response message payload if it is present
	if(response)
	{
		ma_message_t *mamsg = NULL;
		ma_variant_t *payload = NULL;
		// Buffer to hold string extracted from variant
	    ma_buffer_t *buffer = NULL;
		char *response_string = NULL;
		
		size_t size;

		ma_udp_msg_t *udpmsg = response->response;
		if (MA_OK == ma_udp_msg_get_ma_message(udpmsg, &mamsg)) {

			if(mamsg) {
				char *response_http_port = NULL;
				char *relay_port = NULL;

				ma_message_get_property(mamsg,"RelayServerPort",&relay_port);
				if(relay_port)
				MA_LOG(logger,MA_LOG_SEV_DEBUG, "Relay Port: %s",relay_port);
			
				//Check for P2P property being set
				ma_message_get_property(mamsg,"prop.key.content_url.port",&response_http_port);
				if(response_http_port)
				MA_LOG(logger,MA_LOG_SEV_DEBUG, "P2P Http Port: %s",response_http_port);

				ma_message_get_payload(mamsg, &payload);
				if(payload) 
				{
					ma_variant_get_string_buffer(payload, &buffer);
					ma_buffer_get_string(buffer, &response_string, &size);
					ma_buffer_release(buffer); buffer = NULL;
			
					MA_LOG(logger,MA_LOG_SEV_DEBUG, "Response Payload: %s",response_string);
				}

			}
		} else if( MA_OK == ma_udp_msg_get_raw_message(udpmsg,&buffer) )
		{
			ma_buffer_get_string(buffer, &response_string, &size);
			MA_LOG(logger,MA_LOG_SEV_DEBUG, " Raw Response Payload: %s",response_string);
			ma_buffer_release(buffer); buffer = NULL;
		}
		
	}
	response_come = 1;
	return MA_OK;
}

void udp_request_finalize_cb(ma_error_t rc, ma_udp_request_t *request, void *user_data){
	MA_LOG(logger, MA_LOG_SEV_DEBUG, "udp_request_finalize_cb");
	final_come = 1;
}

TEST_SETUP(ma_udp_server_test_ut)
{   
	static ma_udp_server_policy_t policy;
	policy.is_enabled = MA_TRUE;
	policy.port = PORT;
	policy.multicast_addr = ADDRESS;

    TEST_ASSERT(MA_OK == ma_event_loop_create(&ma_ev_loop));
	TEST_ASSERT(MA_OK == ma_loop_worker_create(ma_ev_loop, &ma_loop_worker));
	TEST_ASSERT(MA_OK == ma_loop_worker_start(ma_loop_worker));
	TEST_ASSERT(MA_OK == ma_console_logger_create((ma_console_logger_t**)&logger));

	/*udp server*/
	TEST_ASSERT(MA_OK == ma_udp_server_create(&server));
	TEST_ASSERT(MA_OK == ma_udp_server_set_logger(server, logger));
	TEST_ASSERT(MA_OK == ma_udp_server_set_event_loop(server, ma_ev_loop));

	/*handler*/
	TEST_ASSERT(MA_OK == ma_udp_handler_create(&handler, logger));
	TEST_ASSERT(MA_OK == ma_udp_server_register_msg_handler(server, handler));
	TEST_ASSERT(MA_OK == ma_udp_server_set_policy(server, &policy));

	/* Set up ut context*/
	ma_ut_setup_create("udp_service.tests", "ma.service.udp.test",MA_TRUE,MA_TRUE, MA_TRUE, &g_ut_setup);
    g_ma_context = ma_ut_setup_get_context(g_ut_setup);

	/*ma relay handler*/
	TEST_ASSERT(MA_OK == ma_relay_discovery_handler_create(&relay_handler));
	TEST_ASSERT(MA_OK == ma_relay_discovery_handler_configure(relay_handler, g_ma_context));
	TEST_ASSERT(MA_OK == ma_udp_server_register_msg_handler(server, relay_handler));					
	
	/*ma_p2p_handler*/
	TEST_ASSERT(MA_OK == local_p2p_discovery_handler_create(g_ma_context,&p2p_handler));
	TEST_ASSERT(MA_OK == ma_udp_server_register_msg_handler(server,p2p_handler));

	/*client*/
	TEST_ASSERT(MA_OK == ma_udp_client_create(&udp_client));
	TEST_ASSERT(MA_OK == ma_udp_client_set_logger(udp_client, logger));
	TEST_ASSERT(MA_OK == ma_udp_client_set_loop_worker(udp_client, ma_loop_worker));
	TEST_ASSERT(MA_OK == ma_udp_client_set_event_loop(udp_client, ma_ev_loop));

	/*udp request*/
	TEST_ASSERT(MA_OK == ma_udp_request_create(udp_client, &udp_req[0]));
	TEST_ASSERT(MA_OK == ma_udp_request_set_port(udp_req[0], PORT));
	TEST_ASSERT(MA_OK == ma_udp_request_set_multicast_addr(udp_req[0], ADDRESS));
	TEST_ASSERT(MA_OK == ma_udp_request_set_timeout(udp_req[0], 5000));
	TEST_ASSERT(MA_OK == ma_udp_request_set_callbacks(udp_req[0], udp_request_send_cb, udp_response_cb, "udp req 1"));

	/*relay udp request*/
	TEST_ASSERT(MA_OK == ma_udp_request_create(udp_client, &udp_req[1]));
	TEST_ASSERT(MA_OK == ma_udp_request_set_port(udp_req[1], PORT));
	TEST_ASSERT(MA_OK == ma_udp_request_set_multicast_addr(udp_req[1], ADDRESS));
	TEST_ASSERT(MA_OK == ma_udp_request_set_timeout(udp_req[1], 5000));
	TEST_ASSERT(MA_OK == ma_udp_request_set_callbacks(udp_req[1], udp_request_send_cb, udp_response_cb, "udp req 2"));

	/*p2p udp request*/
	TEST_ASSERT(MA_OK == ma_udp_request_create(udp_client, &udp_req[2]));
	TEST_ASSERT(MA_OK == ma_udp_request_set_port(udp_req[2], PORT));
	TEST_ASSERT(MA_OK == ma_udp_request_set_multicast_addr(udp_req[2], ADDRESS));
	TEST_ASSERT(MA_OK == ma_udp_request_set_timeout(udp_req[2], 5000));
	TEST_ASSERT(MA_OK == ma_udp_request_set_callbacks(udp_req[2], udp_request_send_cb, udp_response_cb, "udp req 3"));

}

TEST_TEAR_DOWN(ma_udp_server_test_ut)
{
	TEST_ASSERT(MA_OK == ma_logger_release(logger));
	TEST_ASSERT(MA_OK == ma_loop_worker_release(ma_loop_worker));
	TEST_ASSERT(MA_OK == ma_udp_msg_handler_release(handler));
	TEST_ASSERT(MA_OK == ma_udp_msg_handler_release(relay_handler));
	TEST_ASSERT(MA_OK == ma_udp_msg_handler_release(p2p_handler));
	TEST_ASSERT(MA_OK == ma_udp_server_release(server));
	TEST_ASSERT(MA_OK == ma_udp_request_release(udp_req[0]));
	TEST_ASSERT(MA_OK == ma_udp_request_release(udp_req[1]));
	TEST_ASSERT(MA_OK == ma_udp_request_release(udp_req[2]));
	TEST_ASSERT(MA_OK == ma_udp_client_release(udp_client));
	/*TODO:
	At this place the event_loop_release()call there is an issue in Linux alone. If you comment
	the event_loop_release() call, the tests are successful. Needs to be investigated if its an issue
	here or somewhere else
	*/
	TEST_ASSERT(MA_OK == ma_event_loop_release(ma_ev_loop));
}

static uv_buf_t alloc_cb(uv_handle_t* handle, size_t suggested_size) {
	if(handle) {
		return uv_buf_init((char*)calloc(suggested_size, 1), suggested_size);
	}
	return uv_buf_init(NULL, 0);
}

static void recv_cb(uv_udp_t* handle, ssize_t nread, uv_buf_t buf, struct sockaddr* addr, unsigned flags) {	
}

static void udp_handle_close_cb(uv_handle_t* handle){
	MA_LOG(logger, MA_LOG_SEV_DEBUG, "**************");
}

static void timer_exit(uv_timer_t *handle, int status)
{
	static uv_udp_t test;
	if(what == 0){
		ma_udp_msg_t *request[2];
		ma_udp_msg_t *rel_request[1];
		ma_udp_msg_t *p2p_request[1];
		ma_message_t *message = NULL;
		ma_message_t *rel_message = NULL;
		ma_message_t *p2p_message = NULL;
		ma_variant_t *vart=NULL;
		ma_buffer_t *rawbuf_msg = NULL;
		char formatted_msg[64] = {0};
		
		uv_loop_t *loop = NULL;

		loop = ma_event_loop_get_uv_loop(ma_ev_loop);
		//start the exit timer
		uv_udp_init(loop, &test);    
		//uv_udp_bind(&test, sa, 0);
		//uv_udp_recv_start(&test, alloc_cb, recv_cb);

		uv_timer_stop(handle); 
		what++;
		/*send*/

		TEST_ASSERT(MA_OK == ma_udp_msg_create(&request[0]));
		TEST_ASSERT(MA_OK == ma_message_create(&message));
		TEST_ASSERT(MA_OK == ma_message_set_property(message, "type", "relay"));
		//Set payload to the message
		TEST_ASSERT(MA_OK == ma_variant_create_from_string("hello2hi",strlen("hello2hi"),&vart));
		TEST_ASSERT(MA_OK == ma_message_set_payload(message, vart));
		TEST_ASSERT(MA_OK == ma_udp_msg_set_ma_message(request[0], message));

		
		//Send a raw message
		TEST_ASSERT(MA_OK == ma_udp_msg_create(&request[1]));
		//create raw buffer message
		//MA_MSC_SELECT(_snprintf, snprintf)(formatted_msg, 64, "%d%f%s",raw_data.x,raw_data.y,raw_data.rawstring);
		MA_MSC_SELECT(_snprintf, snprintf)(formatted_msg, 64, "%s","hithisisrawmessage");
				
		if(MA_OK == ma_buffer_create(&rawbuf_msg, strlen(formatted_msg))){
			(void)ma_buffer_set(rawbuf_msg, formatted_msg, strlen(formatted_msg));
			(void)ma_udp_msg_set_raw_message(request[1], rawbuf_msg);
			(void)ma_buffer_release(rawbuf_msg);
		}
	
		TEST_ASSERT(MA_OK == ma_udp_request_set_message(udp_req[0],&request[0], 2));
		TEST_ASSERT(MA_OK == ma_udp_msg_release(request[0]));
		TEST_ASSERT(MA_OK == ma_udp_msg_release(request[1]));
		TEST_ASSERT(MA_OK == ma_message_release(message));

		TEST_ASSERT(MA_OK == ma_udp_server_start(server));
		TEST_ASSERT(MA_OK == ma_udp_request_async_send(udp_req[0], udp_request_finalize_cb));

		//Send a relay message
		TEST_ASSERT(MA_OK == ma_udp_msg_create(&rel_request[0]));
		TEST_ASSERT(MA_OK == ma_message_create(&rel_message));
		TEST_ASSERT(MA_OK == ma_message_set_property(rel_message,MA_RELAY_MSG_PROP_KEY_TYPE_STR , MA_RELAY_MSG_PROP_VAL_REQUEST_RELAY_DISCOVERY_STR));
		//Set payload to the message
		TEST_ASSERT(MA_OK == ma_variant_create_from_string("hello2hi",strlen("hello2hi_relay"),&vart));
		TEST_ASSERT(MA_OK == ma_message_set_payload(rel_message, vart));
		TEST_ASSERT(MA_OK == ma_udp_msg_set_ma_message(rel_request[0], rel_message));

		TEST_ASSERT(MA_OK == ma_udp_request_set_message(udp_req[1], &rel_request[0], 1));
		TEST_ASSERT(MA_OK == ma_udp_msg_release(rel_request[0]));
		TEST_ASSERT(MA_OK == ma_message_release(rel_message));

	//	TEST_ASSERT(MA_OK == ma_udp_request_async_send(udp_req[1], udp_request_finalize_cb));

		//Send a p2p discovery content message
		TEST_ASSERT(MA_OK == ma_udp_msg_create(&p2p_request[0]));
		TEST_ASSERT(MA_OK == ma_message_create(&p2p_message));
		TEST_ASSERT(MA_OK == ma_message_set_property(p2p_message,"prop.key.msg_type","prop.val.rqst.p2p.content_discovery"));
		TEST_ASSERT(MA_OK == ma_message_set_property(p2p_message, MA_P2P_MSG_KEY_CONTENT_HASH_STR,"123"));
		
		TEST_ASSERT(MA_OK == ma_udp_msg_set_ma_message(p2p_request[0],p2p_message));
		TEST_ASSERT(MA_OK == ma_udp_request_set_message(udp_req[2],p2p_request,1));

		TEST_ASSERT(MA_OK == ma_udp_msg_release(p2p_request[0]));
		TEST_ASSERT(MA_OK == ma_message_release(p2p_message));
		
//		TEST_ASSERT(MA_OK == ma_udp_request_async_send(udp_req[2], udp_request_finalize_cb));

		uv_timer_start(handle, timer_exit, 50000, 0);
	}
	else if(what == 1){
		uv_timer_stop(handle); 
		what++;
		//Sleep(20000);
		/*check response*/
		TEST_ASSERT(send_come == 1);
		TEST_ASSERT(response_come == 1);
		TEST_ASSERT(final_come == 1);

		/*relase server and client*/
		ma_udp_server_stop(server);
		ma_udp_server_unregister_msg_handler(server, handler);			
		ma_udp_server_unregister_msg_handler(server, relay_handler);
		ma_udp_server_unregister_msg_handler(server, p2p_handler);
		ma_loop_worker_stop(ma_loop_worker);
		uv_close((uv_handle_t*) &test, udp_handle_close_cb);
	}
}

static uv_timer_t timer;	

TEST(ma_udp_server_test_ut, udp_server_test)
{
	
    uv_loop_t *loop = NULL;
	
	loop = ma_event_loop_get_uv_loop(ma_ev_loop);
    //start the exit timer
    uv_timer_init(loop, &timer);
    uv_timer_start(&timer, timer_exit, 2000, 0);
	ma_event_loop_run(ma_ev_loop);    	
}