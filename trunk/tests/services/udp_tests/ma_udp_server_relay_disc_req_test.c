#include "unity.h"
#include "unity_fixture.h"
#include "ma/logger/ma_console_logger.h"

#include "ma/internal/ma_macros.h"

#include "ma/internal/clients/udp/ma_udp_request.h"
#include "ma/internal/services/udp_server/ma_udp_server.h"
#include "ma/internal/defs/ma_relay_defs.h"
#include "ma/internal/clients/relay/ma_relay_discovery_request.h"

#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/utils/event_loop/ma_loop_worker.h"

#include "ma_ut_setup.h"


#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "udp_test"


#define PORT			8089
#define ADDRESS			"ff08::9"

static ma_loop_worker_t		*ma_loop_worker = NULL;
static ma_event_loop_t		*ma_ev_loop = NULL;
static ma_logger_t			*logger = NULL;
static ma_udp_msg_handler_t *relay_handler=NULL;
static ma_udp_server_t		*server = NULL;

static ma_udp_client_t		*udp_client = NULL;
static ma_relay_discovery_request_t *relay_discovery_req = NULL;

ma_context_t *g_ma_context_relay_client = NULL;
ma_ut_setup_t *g_ut_setup_relay_client = NULL;

static int what = 0;
static int send_come = 0;
static int response_come = 0;
static int final_come = 0;

extern ma_error_t local_p2p_discovery_handler_create(struct ma_context_s *context, ma_udp_msg_handler_t **discovery_handler);

//Send a raw message
struct raw_t {
	int x;
	float y;
	char rawstring[20];
};

static struct raw_t raw_data = {
1,
2.5,
"helloworld"
};

extern ma_error_t ma_udp_handler_create(ma_udp_msg_handler_t **self, ma_logger_t *logger);
extern ma_error_t ma_relay_discovery_handler_create(ma_udp_msg_handler_t **self);
extern ma_error_t ma_relay_discovery_handler_configure(ma_udp_msg_handler_t *self, ma_context_t *context);

extern ma_error_t ma_service_ut_context_create(const char *product_id, ma_bool_t thread_pool_enabled, ma_context_t **context);
extern ma_error_t ma_ut_setup_create(const char *ut_name, const char *product_id, ma_bool_t relay, ma_bool_t same_port, ma_bool_t p2p, ma_ut_setup_t **ut_setup);
extern ma_context_t *ma_ut_setup_get_context(ma_ut_setup_t *ut_setup);

/*
static void relay_discovery_cb(ma_relay_discovery_request_t *req, ma_relay_list_t *relay_list, void *user_data){
	MA_LOG(logger, MA_LOG_SEV_DEBUG, "relay_discovery_cb");
}

static void relay_discovery_final_cb(ma_relay_discovery_request_t *req, void *user_data){
	MA_LOG(logger, MA_LOG_SEV_DEBUG, "relay_discovery_final_cb");
}
*/

TEST_GROUP_RUNNER(ma_udp_server_relay_client_test_group)
{
    do {RUN_TEST_CASE(ma_udp_server_test_relay_client_ut, udp_server_relay_client_test);} while(0);
}

TEST_SETUP(ma_udp_server_test_relay_client_ut)
{   
	
	static ma_udp_server_policy_t policy;
	//ma_relay_discovery_callbacks_t cbs;
	
	policy.is_enabled = MA_TRUE;
	policy.port = PORT;
	policy.multicast_addr = ADDRESS;

	/* Set up ut context*/
	ma_ut_setup_create("udp_service.tests", "ma.service.udp.test",MA_TRUE,MA_FALSE, MA_TRUE, &g_ut_setup_relay_client);
    g_ma_context_relay_client = ma_ut_setup_get_context(g_ut_setup_relay_client);

	TEST_ASSERT(MA_OK == ma_console_logger_create((ma_console_logger_t**)&logger));
	TEST_ASSERT(MA_OK == ma_event_loop_create(&ma_ev_loop));
	TEST_ASSERT(MA_OK == ma_loop_worker_create(ma_ev_loop, &ma_loop_worker));
	TEST_ASSERT(MA_OK == ma_loop_worker_start(ma_loop_worker));

	/*udp server*/
	TEST_ASSERT(MA_OK == ma_udp_server_create(&server));
	TEST_ASSERT(MA_OK == ma_udp_server_set_logger(server, logger));
	TEST_ASSERT(MA_OK == ma_udp_server_set_event_loop(server, ma_ev_loop));
	TEST_ASSERT(MA_OK == ma_udp_server_set_policy(server, &policy));

	
	/*ma relay handler*/
	TEST_ASSERT(MA_OK == ma_relay_discovery_handler_create(&relay_handler));
	TEST_ASSERT(MA_OK == ma_relay_discovery_handler_configure(relay_handler, g_ma_context_relay_client));
	TEST_ASSERT(MA_OK == ma_udp_server_register_msg_handler(server, relay_handler));					
	
	/* UDP Client */
	TEST_ASSERT(MA_OK == ma_udp_client_create(&udp_client));
	TEST_ASSERT(MA_OK == ma_udp_client_set_logger(udp_client, logger));
	TEST_ASSERT(MA_OK == ma_udp_client_set_event_loop(udp_client,ma_ev_loop));			
	TEST_ASSERT(MA_OK == ma_udp_client_set_loop_worker(udp_client,ma_loop_worker));

	TEST_ASSERT(MA_OK == ma_relay_discovery_request_create(udp_client, &relay_discovery_req));
	//cbs.relay_discovery_cb = relay_discovery_cb;
	//cbs.relay_discovery_final_cb = relay_discovery_final_cb;				
	//TEST_ASSERT(MA_OK == ma_relay_discovery_request_set_logger(relay_discovery_req, logger));
	//TEST_ASSERT(MA_OK == ma_relay_discovery_request_set_callbacks(relay_discovery_req, &cbs, NULL));
		
}

TEST_TEAR_DOWN(ma_udp_server_test_relay_client_ut)
{
	TEST_ASSERT(MA_OK == ma_logger_release(logger));
	TEST_ASSERT(MA_OK == ma_loop_worker_release(ma_loop_worker));
	TEST_ASSERT(MA_OK == ma_udp_msg_handler_release(relay_handler));
	TEST_ASSERT(MA_OK == ma_udp_server_release(server));
	TEST_ASSERT(MA_OK == ma_relay_discovery_request_release(relay_discovery_req));
	TEST_ASSERT(MA_OK == ma_udp_client_release(udp_client));
	TEST_ASSERT(MA_OK == ma_event_loop_release(ma_ev_loop));
}

static uv_buf_t alloc_cb(uv_handle_t* handle, size_t suggested_size) {
	if(handle) {
		return uv_buf_init((char*)calloc(suggested_size, 1), suggested_size);
	}
	return uv_buf_init(NULL, 0);
}

static void recv_cb(uv_udp_t* handle, ssize_t nread, uv_buf_t buf, struct sockaddr* addr, unsigned flags) {	
}

static void udp_handle_close_cb(uv_handle_t* handle){
	MA_LOG(logger, MA_LOG_SEV_DEBUG, "**************");
}

static void timer_exit_relay_client(uv_timer_t *handle, int status)
{
	static uv_udp_t test;
	//ma_relay_discovery_settings_t settings;
	ma_relay_discovery_policy_t rel_policy;	
	if(what == 0){
		ma_error_t err=MA_OK;
		
		uv_loop_t *loop = NULL;

		loop = ma_event_loop_get_uv_loop(ma_ev_loop);
		//start the exit timer
		uv_udp_init(loop, &test);    
		//uv_udp_bind(&test, sa, 0);
		//uv_udp_recv_start(&test, alloc_cb, recv_cb);

		uv_timer_stop(handle); 
		what++;
		TEST_ASSERT(MA_OK == ma_udp_server_start(server));
		rel_policy.discovery_port = PORT;
		rel_policy.discovery_multicast_addr = ADDRESS;
		rel_policy.discovery_timeout_ms = 2*1000;
		rel_policy.max_no_of_relays_to_discover = 5;
		rel_policy.sync_discovery = MA_FALSE;

		TEST_ASSERT(MA_OK == (ma_relay_discovery_request_set_policy(relay_discovery_req, &rel_policy)));
				
		//settings.max_no_of_relays_to_discover = 5;
		//settings.discovery_timeout_ms = 2 * 1000;
		//settings.sync_discovery = MA_FALSE;
		//ma_relay_discovery_request_reset(relay_discovery_req);
		//ma_relay_discovery_request_perform(relay_discovery_req, &settings);
		ma_relay_discovery_request_send(relay_discovery_req);
				
		uv_timer_start(handle, timer_exit_relay_client, 50000, 0);
	}
	else if(what == 1){
		const ma_proxy_list_t *relay_list = NULL;
		ma_error_t rc;
		uv_timer_stop(handle); 
		what++;
		//Sleep(20000);
		
		rc = ma_relay_discovery_request_get_relays(relay_discovery_req, &relay_list);
		TEST_ASSERT(rc == MA_OK || rc == MA_ERROR_RELAY_NONE);
		/*relase server and client*/
		ma_udp_server_stop(server);
		ma_udp_server_unregister_msg_handler(server, relay_handler);
		ma_loop_worker_stop(ma_loop_worker);
		uv_close((uv_handle_t*) &test, udp_handle_close_cb);
	}
}

static uv_timer_t timer;

TEST(ma_udp_server_test_relay_client_ut, udp_server_relay_client_test)
{
	//uv_timer_t timer;	
    uv_loop_t *loop = NULL;
	
	loop = ma_event_loop_get_uv_loop(ma_ev_loop);
    //start the exit timer
    uv_timer_init(loop, &timer);
    uv_timer_start(&timer, timer_exit_relay_client, 2000, 0);
	ma_event_loop_run(ma_ev_loop);    	
}