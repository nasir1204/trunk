#include "ma/internal/utils/threading/ma_atomic.h"
#include "ma/internal/defs/ma_p2p_defs.h"

typedef struct ma_p2p_discovery_handler_s ma_p2p_discovery_handler_t;
struct ma_p2p_discovery_handler_s {
	ma_udp_msg_handler_t	base ;
	ma_atomic_counter_t		ref_count ;
	ma_context_t			*context ;
    void					*data ;
} ;
