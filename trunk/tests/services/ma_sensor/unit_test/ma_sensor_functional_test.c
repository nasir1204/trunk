
#include "unity.h"
#include "unity_fixture.h"

#include "ma/ma_common.h"
#include "ma/internal/ma_macros.h"
#include "ma/ma_variant.h"


#include "ma/sensor/ma_sensor.h"
#include "ma/sensor/ma_sensor_tray.h"
#include "ma/sensor/ma_user_sensor.h"
#include "ma/sensor/ma_system_sensor.h"
#include "ma/sensor/ma_power_sensor.h"
#include "ma/sensor/ma_idle_sensor.h"
#include "ma/sensor/ma_network_sensor.h"
#include "ma/sensor/ma_sensor_msg.h"
#include "ma/sensor/ma_sensor_utils.h"



//#include <uv.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
//#include <time.h>
#include "ma/logger/ma_file_logger.h"
#include "ma/logger/ma_console_logger.h"


#ifdef MA_WINDOWS
# include <crtdbg.h>
#include <Windows.h>
# define MA_TRACKLEAKS _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF)
# define MA_INPUT_FOLDER_PATH ".\\infile.xml"
# define MA_OUTPUT_FOLDER_PATH ".\\outfile.xml"
#else
# define MA_TRACKLEAKS ((void *)0)
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
# define MA_INPUT_FOLDER_PATH "./infile.xml"
# define MA_OUTPUT_FOLDER_PATH "./outfile.xml"
#endif




static ma_msgbus_t *g_msgbus = NULL;
ma_file_logger_t *file_logger = NULL;
ma_logger_t *sensor_logger;

TEST_GROUP_RUNNER(sensor_functional_test_group) {
#ifdef MA_WINDOWS
CoInitializeEx(NULL,COINIT_MULTITHREADED);
#endif

do {RUN_TEST_CASE(sensor_functional_test, ma_sensor_set_logger); }while(0); 

do {RUN_TEST_CASE(sensor_functional_test, ma_sensor_start_user_valid); }while(0);
do {RUN_TEST_CASE(sensor_functional_test, ma_sensor_start_system_valid); }while(0);
do {RUN_TEST_CASE(sensor_functional_test, ma_sensor_start_network_valid); }while(0);
do {RUN_TEST_CASE(sensor_functional_test, ma_sensor_start_power_valid); }while(0);
do {RUN_TEST_CASE(sensor_functional_test, ma_sensor_start_idle_valid); }while(0);

do {RUN_TEST_CASE(sensor_functional_test, ma_sensor_start_all_valid); }while(0);
do {RUN_TEST_CASE(sensor_functional_test, ma_sensor_start_stop_invalid); }while(0);

do {RUN_TEST_CASE(sensor_functional_test, ma_sensor_get_id_valid); }while(0);
do {RUN_TEST_CASE(sensor_functional_test, ma_sensor_get_id_invalid); }while(0);

do {RUN_TEST_CASE(sensor_functional_test, ma_sensor_get_type_valid); }while(0);

do {RUN_TEST_CASE(sensor_functional_test, ma_sensor_on_msg_valid); }while(0);
do {RUN_TEST_CASE(sensor_functional_test, ma_sensor_on_msg_valid1); }while(0);

do {RUN_TEST_CASE(sensor_functional_test, ma_sensor_on_msg_network_valid); }while(0);
do {RUN_TEST_CASE(sensor_functional_test, ma_sensor_on_msg_power_valid); }while(0);
do {RUN_TEST_CASE(sensor_functional_test, ma_sensor_on_msg_idle_valid); }while(0);
do {RUN_TEST_CASE(sensor_functional_test, ma_sensor_on_msg_system_valid); }while(0);

do {RUN_TEST_CASE(sensor_functional_test, ma_sensor_release_logger); }while(0); 

#ifdef MA_WINDOWS
CoUninitialize();
#endif
}

//extern ma_error_t sensor_tray_start(ma_sensor_t *);

TEST_SETUP(sensor_functional_test) {
	char *product_id = "Sensor_test";
	

	//TEST_ASSERT_MESSAGE(MA_OK == ma_msgbus_create(product_id, &g_msgbus),"ma_msgbus_create failed");
	//TEST_ASSERT_MESSAGE(MA_OK == ma_msgbus_start(g_msgbus),"ma_msgbus_start failed"); 
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_create(product_id, &g_msgbus));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_start(g_msgbus));
	   
   
}

TEST_TEAR_DOWN(sensor_functional_test) {
   
	//TEST_ASSERT_MESSAGE(MA_OK == ma_msgbus_stop(g_msgbus,MA_TRUE),"ma_msgbus_stop failed");
	//TEST_ASSERT_MESSAGE(MA_OK == ma_msgbus_release(g_msgbus),"ma_msgbus_release failed");    
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_stop(g_msgbus,MA_TRUE));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_release(g_msgbus));
}

TEST(sensor_functional_test,ma_sensor_set_logger ) {

	/*Clean up the old one's , if by chance they are there */
    unlink("test_file_logger.log");
	 /* Valid File Logger creation. */
    TEST_ASSERT_MESSAGE(ma_file_logger_create(NULL, "test_file_logger", ".log", &file_logger) == MA_OK, \
                        "File Logger could not be created");
	sensor_logger = (ma_logger_t*)file_logger;
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_set_logger(sensor_logger));
}

TEST(sensor_functional_test,ma_sensor_release_logger ) {

	TEST_ASSERT_MESSAGE(MA_OK == ma_logger_dec_ref((ma_logger_t *)file_logger),"ma_logger_dec_ref failed");
}

TEST(sensor_functional_test,ma_sensor_start_user_valid ) {
    ma_sensor_tray_t *tray = NULL;
	ma_user_sensor_t *user = NULL;
	
	
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_create(g_msgbus, &tray));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_create(g_msgbus, &user));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_add(tray,(ma_sensor_t *)user));
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_start((ma_sensor_t *)tray));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_stop((ma_sensor_t *)tray));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_release(tray));
	//Release all sensors after tray is released
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_release(user));
	

}

TEST(sensor_functional_test,ma_sensor_start_system_valid ) {
    ma_sensor_tray_t *tray = NULL;
	ma_system_sensor_t *system = NULL;
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_create(g_msgbus, &tray));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_system_sensor_create(g_msgbus, &system));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_add(tray,(ma_sensor_t *)system));
		
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_start((ma_sensor_t *)tray));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_stop((ma_sensor_t *)tray));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_release(tray));
	//Release all sensors after tray is released
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_system_sensor_release(system));

}

TEST(sensor_functional_test,ma_sensor_start_network_valid ) {
    ma_sensor_tray_t *tray = NULL;
	ma_network_sensor_t *network = NULL;
	
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_create(g_msgbus, &tray));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_create(g_msgbus, &network));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_add(tray,(ma_sensor_t *)network));
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_start((ma_sensor_t *)tray));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_stop((ma_sensor_t *)tray));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_release(tray));
	//Release all sensors after tray is released
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_release(network));
	
}

TEST(sensor_functional_test,ma_sensor_start_power_valid ) {
    ma_sensor_tray_t *tray = NULL;
	ma_power_sensor_t *power = NULL;
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_create(g_msgbus, &tray));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_create(g_msgbus, &power));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_add(tray,(ma_sensor_t *)power));
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_start((ma_sensor_t *)tray));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_stop((ma_sensor_t *)tray));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_release(tray));
	//Release all sensors after tray is released
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_release(power));
	

}

TEST(sensor_functional_test,ma_sensor_start_idle_valid ) {
    ma_sensor_tray_t *tray = NULL;
	ma_idle_sensor_t *idle = NULL;
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_create(g_msgbus, &tray));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_idle_sensor_create(g_msgbus, &idle));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_add(tray,(ma_sensor_t *)idle));
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_start((ma_sensor_t *)tray));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_stop((ma_sensor_t *)tray));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_release(tray));
	//Release all sensors after tray is released
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_idle_sensor_release(idle));
	

}

TEST(sensor_functional_test,ma_sensor_start_all_valid ) {
     ma_sensor_tray_t *tray = NULL;
	ma_network_sensor_t *network = NULL;
	ma_user_sensor_t *user = NULL;
	ma_system_sensor_t *system = NULL;
	ma_idle_sensor_t *idle = NULL;
	ma_power_sensor_t *power = NULL;

	
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_create(g_msgbus, &tray));
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_create(g_msgbus, &network));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_create(g_msgbus, &user));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_system_sensor_create(g_msgbus, &system));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_idle_sensor_create(g_msgbus, &idle));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_create(g_msgbus, &power));

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)network));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)user));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)system));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)idle));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)power));

	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_start((ma_sensor_t *)tray));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_stop((ma_sensor_t *)tray));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_release(tray));

	//Release all sensors after tray is released
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_release(network));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_release(user));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_system_sensor_release(system));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_idle_sensor_release(idle));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_release(power));

}

TEST(sensor_functional_test,ma_sensor_start_stop_invalid ) {
   
		
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_sensor_start(NULL));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_sensor_stop(NULL));

	

}

TEST(sensor_functional_test,ma_sensor_get_id_valid ) {
     ma_sensor_tray_t *tray = NULL;
	ma_network_sensor_t *network = NULL;
	ma_user_sensor_t *user = NULL;
	ma_system_sensor_t *system = NULL;
	ma_idle_sensor_t *idle = NULL;
	ma_power_sensor_t *power = NULL;
	char *str_id = NULL;

	
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_create(g_msgbus, &tray));
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_create(g_msgbus, &network));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_create(g_msgbus, &user));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_system_sensor_create(g_msgbus, &system));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_idle_sensor_create(g_msgbus, &idle));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_create(g_msgbus, &power));

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)network));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)user));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)system));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)idle));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)power));

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_get_id((ma_sensor_t *)tray,&str_id));
	TEST_ASSERT_EQUAL_INT(MA_OK,strcmp(str_id,"ma.sensor.tray"));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_get_id((ma_sensor_t *)network,&str_id));
	TEST_ASSERT_EQUAL_INT(MA_OK,strcmp(str_id,"ma.sensor.network"));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_get_id((ma_sensor_t *)user,&str_id));
	TEST_ASSERT_EQUAL_INT(MA_OK,strcmp(str_id,"ma.sensor.user"));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_get_id((ma_sensor_t *)system,&str_id));
	TEST_ASSERT_EQUAL_INT(MA_OK,strcmp(str_id,"ma.sensor.system"));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_get_id((ma_sensor_t *)idle,&str_id));
	TEST_ASSERT_EQUAL_INT(MA_OK,strcmp(str_id,"ma.sensor.idle"));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_get_id((ma_sensor_t *)power,&str_id));
	TEST_ASSERT_EQUAL_INT(MA_OK,strcmp(str_id,"ma.sensor.power"));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_release(tray));

	//Release all sensors after tray is released
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_release(network));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_release(user));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_system_sensor_release(system));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_idle_sensor_release(idle));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_release(power));

}

TEST(sensor_functional_test,ma_sensor_get_id_invalid ) {
    
	ma_network_sensor_t *network = NULL;
	char *str_id = NULL;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_create(g_msgbus, &network));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_sensor_get_id(NULL,&str_id));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_sensor_get_id((ma_sensor_t *)network,NULL));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_release(network));
}

TEST(sensor_functional_test,ma_sensor_get_type_valid ) {
     ma_sensor_tray_t *tray = NULL;
	ma_network_sensor_t *network = NULL;
	ma_user_sensor_t *user = NULL;
	ma_system_sensor_t *system = NULL;
	ma_idle_sensor_t *idle = NULL;
	ma_power_sensor_t *power = NULL;
	char *str_id = NULL;
	ma_sensor_type_t type;
	
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_create(g_msgbus, &tray));
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_create(g_msgbus, &network));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_create(g_msgbus, &user));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_system_sensor_create(g_msgbus, &system));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_idle_sensor_create(g_msgbus, &idle));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_create(g_msgbus, &power));

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)network));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)user));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)system));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)idle));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)power));

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_get_type((ma_sensor_t *)tray,&type));
	TEST_ASSERT_EQUAL_INT(MA_SENSOR_TYPE_TRAY,type);
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_get_type((ma_sensor_t *)network,&type));
	TEST_ASSERT_EQUAL_INT(MA_SENSOR_TYPE_NETWORK,type);
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_get_type((ma_sensor_t *)system,&type));
	TEST_ASSERT_EQUAL_INT(MA_SENSOR_TYPE_SYSTEM,type);
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_get_type((ma_sensor_t *)idle,&type));
	TEST_ASSERT_EQUAL_INT(MA_SENSOR_TYPE_IDLE,type);
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_get_type((ma_sensor_t *)user,&type));
	TEST_ASSERT_EQUAL_INT(MA_SENSOR_TYPE_USER,type);
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_get_type((ma_sensor_t *)power,&type));
	TEST_ASSERT_EQUAL_INT(MA_SENSOR_TYPE_POWER,type);

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_release(tray));

	//Release all sensors after tray is released
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_release(network));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_release(user));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_system_sensor_release(system));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_idle_sensor_release(idle));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_release(power));

}

TEST(sensor_functional_test, ma_sensor_on_msg_valid) {
    ma_sensor_msg_t *sensor_msg;
	ma_sensor_msg_t *sensor_msg1;
	ma_user_sensor_msg_t *user_sensor_msg;
	ma_user_sensor_msg_t *user_sensor_msg1;
	char *domain1 = "nai.corp.org";
	char *user1 = "sheetal";
	ma_uint32_t s_id1= 1001;
	ma_user_sensor_t *user = NULL;
	ma_sensor_event_type_t type = MA_USER_SENSOR_LOG_ON_EVENT;
	ma_user_logged_state_t logged_status = MA_USER_LOGGED_STATE_ON;
	
	//User Loggs in
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_create(g_msgbus, &user));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_create(&user_sensor_msg,1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_set_domain(user_sensor_msg,0,domain1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_set_user(user_sensor_msg,0,user1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_set_session_id(user_sensor_msg,0,s_id1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_set_logon_state(user_sensor_msg,logged_status));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_set_event_type(user_sensor_msg,type));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_user_sensor_msg(user_sensor_msg,&sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_start((ma_sensor_t *)user));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_on_msg((ma_sensor_t *)user,sensor_msg));
	

	//Now User Loggs off
	type = MA_USER_SENSOR_LOG_OFF_EVENT;
	logged_status = MA_USER_LOGGED_STATE_OFF;
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_create(&user_sensor_msg1,1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_set_domain(user_sensor_msg1,0,domain1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_set_user(user_sensor_msg1,0,user1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_set_session_id(user_sensor_msg1,0,s_id1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_set_logon_state(user_sensor_msg1,logged_status));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_set_event_type(user_sensor_msg1,type));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_user_sensor_msg(user_sensor_msg1,&sensor_msg1));
	//There is a memory leak in ma_sensor_on_msg from ma_buffer.c need to investigate
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_on_msg((ma_sensor_t *)user,sensor_msg1));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_stop((ma_sensor_t *)user));

    TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_release(user_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_release(user_sensor_msg1));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg1));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_release(user));	
	
		
}

TEST(sensor_functional_test, ma_sensor_on_msg_valid1) {
    ma_sensor_msg_t *sensor_msg;
	ma_user_sensor_msg_t *user_sensor_msg;
	char *domain1 = "nai.corp.org";
	char *domain2 = "epo2003";
	char *user1 = "sheetal";
	char *user2 = "hulloli";
	ma_uint32_t s_id1= 1001;
	ma_uint32_t s_id2 = 2002;
	ma_user_sensor_t *user = NULL;
	 ma_sensor_event_type_t type = MA_USER_SENSOR_LOG_ON_EVENT;

	ma_user_logged_state_t logged_status = MA_USER_LOGGED_STATE_ON;
	ma_user_logged_state_t t_logged_status = MA_USER_LOGGED_STATE_OFF;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_create(g_msgbus, &user));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_create(&user_sensor_msg,2));
	

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_set_domain(user_sensor_msg,0,domain1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_set_domain(user_sensor_msg,1,domain2));
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_set_user(user_sensor_msg,0,user1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_set_user(user_sensor_msg,1,user2));
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_set_session_id(user_sensor_msg,0,s_id1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_set_session_id(user_sensor_msg,1,s_id2));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_set_logon_state(user_sensor_msg,logged_status));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_set_event_type(user_sensor_msg,type));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_user_sensor_msg(user_sensor_msg,&sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_start((ma_sensor_t *)user));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_on_msg((ma_sensor_t *)user,sensor_msg));

	type = MA_USER_SENSOR_LOG_OFF_EVENT;
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_set_event_type(user_sensor_msg,type));
	logged_status = MA_USER_LOGGED_STATE_OFF;
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_set_logon_state(user_sensor_msg,logged_status));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_user_sensor_msg(user_sensor_msg,&sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_on_msg((ma_sensor_t *)user,sensor_msg));
	

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_stop((ma_sensor_t *)user));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_release(user_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_release(user));	
	
		
}

TEST(sensor_functional_test, ma_sensor_on_msg_network_valid) {
    ma_sensor_msg_t *sensor_msg;
	ma_sensor_msg_t *sensor_msg1;
	ma_network_sensor_msg_t *network_sensor_msg;
	ma_network_sensor_msg_t *network_sensor_msg1;
	ma_network_sensor_t *network = NULL;
	ma_sensor_event_type_t type = MA_NETWORK_SENSOR_CONNECTIVITY_STATUS_CHANGE_EVENT;
	ma_network_connectivity_status_t status = MA_NETWORK_CONNECTIVITY_STATUS_CONNECTED;

	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_create(g_msgbus, &network));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_msg_create(&network_sensor_msg,1));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_network_sensor_msg_set_connectivity_status(network_sensor_msg,status));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_network_sensor_msg_set_event_type(network_sensor_msg,type));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_network_sensor_msg(network_sensor_msg,&sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_start((ma_sensor_t *)network));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_on_msg((ma_sensor_t *)network,sensor_msg));
	

	type = MA_NETWORK_SENSOR_CONNECTIVITY_STATUS_CHANGE_EVENT;
	status = MA_NETWORK_CONNECTIVITY_STATUS_DISCONNECTED;
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_msg_create(&network_sensor_msg1,1));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_network_sensor_msg_set_connectivity_status(network_sensor_msg1,status));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_network_sensor_msg_set_event_type(network_sensor_msg1,type));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_network_sensor_msg(network_sensor_msg1,&sensor_msg1));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_on_msg((ma_sensor_t *)network,sensor_msg1));
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_stop((ma_sensor_t *)network));

    TEST_ASSERT_EQUAL_INT(MA_OK,ma_network_sensor_msg_release(network_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_network_sensor_msg_release(network_sensor_msg1));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg1));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_release(network));	
		
}

TEST(sensor_functional_test, ma_sensor_on_msg_power_valid) {
    ma_sensor_msg_t *sensor_msg;
	ma_sensor_msg_t *sensor_msg1;
	ma_power_sensor_msg_t *power_sensor_msg;
	ma_power_sensor_msg_t *power_sensor_msg1;
	ma_power_sensor_t *power = NULL;
	ma_sensor_event_type_t type = MA_POWER_SENSOR_STATUS_CHANGE_EVENT;
	ma_power_status_t power_status = MA_POWER_STATUS_ONLINE;
	ma_battery_charge_status_t battery_status = MA_BATTERY_CHARGE_STATUS_HIGH;
	int  battery_life = 10*60*60;
	int  battery_percent = 80;
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_create(g_msgbus, &power));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_msg_create(&power_sensor_msg,1));
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_set_battery_lifetime(power_sensor_msg,0,battery_life));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_set_event_type(power_sensor_msg,type));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_set_power_status(power_sensor_msg,0,power_status));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_set_battery_charge_status(power_sensor_msg,0,battery_status));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_set_battery_percent(power_sensor_msg,0,battery_percent));
		
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_power_sensor_msg(power_sensor_msg,&sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_start((ma_sensor_t *)power));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_on_msg((ma_sensor_t *)power,sensor_msg));
	

	type = MA_POWER_SENSOR_BATTERY_LOW_EVENT;
	power_status = MA_POWER_STATUS_ONLINE;
	battery_status = MA_BATTERY_CHARGE_STATUS_LOW;
	battery_life = 5*60*60;
	battery_percent = 60;
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_msg_create(&power_sensor_msg1,1));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_set_event_type(power_sensor_msg1,type));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_set_power_status(power_sensor_msg1,0,power_status));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_set_battery_charge_status(power_sensor_msg1,0,battery_status));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_set_battery_percent(power_sensor_msg1,0,battery_percent));
	
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_power_sensor_msg(power_sensor_msg1,&sensor_msg1));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_on_msg((ma_sensor_t *)power,sensor_msg1));
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_stop((ma_sensor_t *)power));

    TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_release(power_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_release(power_sensor_msg1));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg1));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_release(power));	
		
}

TEST(sensor_functional_test, ma_sensor_on_msg_idle_valid) {
    ma_sensor_msg_t *sensor_msg;
	ma_sensor_msg_t *sensor_msg1;
	ma_idle_sensor_msg_t *idle_sensor_msg;
	ma_idle_sensor_msg_t *idle_sensor_msg1;
	ma_idle_sensor_t *idle = NULL;
	ma_sensor_event_type_t type = MA_NETWORK_SENSOR_CONNECTIVITY_STATUS_CHANGE_EVENT;
		
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_idle_sensor_create(g_msgbus, &idle));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_idle_sensor_msg_create(&idle_sensor_msg,1));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_idle_sensor_msg_set_event_type(idle_sensor_msg,type));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_idle_sensor_msg(idle_sensor_msg,&sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_start((ma_sensor_t *)idle));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_on_msg((ma_sensor_t *)idle,sensor_msg));
	

	type = MA_NETWORK_SENSOR_CONNECTIVITY_STATUS_CHANGE_EVENT;
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_idle_sensor_msg_create(&idle_sensor_msg1,1));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_idle_sensor_msg_set_event_type(idle_sensor_msg1,type));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_idle_sensor_msg(idle_sensor_msg1,&sensor_msg1));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_on_msg((ma_sensor_t *)idle,sensor_msg1));
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_stop((ma_sensor_t *)idle));

    TEST_ASSERT_EQUAL_INT(MA_OK,ma_idle_sensor_msg_release(idle_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_idle_sensor_msg_release(idle_sensor_msg1));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg1));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_idle_sensor_release(idle));	
		
}

TEST(sensor_functional_test, ma_sensor_on_msg_system_valid) {
    ma_sensor_msg_t *sensor_msg;
	ma_sensor_msg_t *sensor_msg1;
	ma_system_sensor_msg_t *system_sensor_msg;
	ma_system_sensor_msg_t *system_sensor_msg1;
	ma_system_sensor_t *system = NULL;
	ma_sensor_event_type_t type = MA_SYSTEM_SENSOR_EVENT_STOP;
		
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_system_sensor_create(g_msgbus, &system));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_system_sensor_msg_create(&system_sensor_msg,1));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_system_sensor_msg_set_event_type(system_sensor_msg,type));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_system_sensor_msg(system_sensor_msg,&sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_start((ma_sensor_t *)system));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_on_msg((ma_sensor_t *)system,sensor_msg));
	

	type = MA_SYSTEM_SENSOR_EVENT_SHUTDOWN;
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_system_sensor_msg_create(&system_sensor_msg1,1));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_system_sensor_msg_set_event_type(system_sensor_msg1,type));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_system_sensor_msg(system_sensor_msg1,&sensor_msg1));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_on_msg((ma_sensor_t *)system,sensor_msg1));
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_stop((ma_sensor_t *)system));

    TEST_ASSERT_EQUAL_INT(MA_OK,ma_system_sensor_msg_release(system_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_system_sensor_msg_release(system_sensor_msg1));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg1));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_system_sensor_release(system));	
		
}
