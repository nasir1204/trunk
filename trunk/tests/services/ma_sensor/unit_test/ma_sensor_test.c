
#include "unity.h"
#include "unity_fixture.h"

#include "ma/ma_common.h"
#include "ma/internal/ma_macros.h"
#include "ma/ma_variant.h"
#include "ma/internal/utils/xml/ma_xml.h"
#include "ma/internal/utils/xml/ma_xml_node.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include "src/utils/app_info/ma_application_internal.h"
#include "ma/internal/utils/app_info/ma_application.h"
#include "ma/ma_datastore.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma/datastore/ma_ds_registry.h"

#include "ma/sensor/ma_sensor.h"
#include "ma/sensor/ma_sensor_tray.h"
#include "ma/sensor/ma_user_sensor.h"
#include "ma/sensor/ma_system_sensor.h"
#include "ma/sensor/ma_power_sensor.h"
#include "ma/sensor/ma_idle_sensor.h"
#include "ma/sensor/ma_network_sensor.h"
#include "ma/sensor/ma_sensor_msg.h"
#include "ma/sensor/ma_sensor_utils.h"



//#include <uv.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
//#include <time.h>

#ifdef MA_WINDOWS
# include <crtdbg.h>
#include <Windows.h>
# define MA_TRACKLEAKS _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF)
# define MA_INPUT_FOLDER_PATH ".\\infile.xml"
# define MA_OUTPUT_FOLDER_PATH ".\\outfile.xml"
#else
# define MA_TRACKLEAKS ((void *)0)
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
# define MA_INPUT_FOLDER_PATH "./infile.xml"
# define MA_OUTPUT_FOLDER_PATH "./outfile.xml"
#endif

#ifdef MA_WINDOWS
    #define TEST_AGENT_ROOT    ".\\AGENT\\"
    #define TEST_AGENT_DATA_PATH ".\\AGENT\\"
    #define TEST_AGENT_SETUP_DATA_PATH ".\\AGENT\\db"
    #define TEST_AGENT_INSTALL_PATH ".\\AGENT\\"
#else
    #define TEST_AGENT_ROOT    "./AGENT/"
    #define TEST_AGENT_DATA_PATH "./AGENT/"
    #define TEST_AGENT_SETUP_DATA_PATH "./AGENT/db"
    #define TEST_AGENT_INSTALL_PATH "./AGENT/"
#endif

#ifdef _WIN64
#define MA_AGENT_BIT_AFFINITY 2
#elif _WIN32
#define MA_AGENT_BIT_AFFINITY 1
#else
#define MA_AGENT_BIT_AFFINITY 0
#endif

ma_msgbus_t *g_msgbus = NULL;

ma_bool_t isSystem64Bit() {
#ifdef MA_WINDOWS

    UINT size = 0;
    HMODULE hm_k32 = (HMODULE) 0;
    FARPROC proc;
    TCHAR tmp[1];
    OSVERSIONINFOW g_osvi = {0};
    ma_bool_t ret = MA_FALSE;

    g_osvi.dwOSVersionInfoSize = sizeof( OSVERSIONINFO );
    GetVersionEx(&g_osvi);
    // GetSystemWow64Directory() requires at least Win XP or Win 2003 Server
    // and 64-bit is potentially only on at least XP.
    if (g_osvi.dwMajorVersion >= 4 || (g_osvi.dwMajorVersion >= 3 && g_osvi.dwMinorVersion >= 1))
    {
        // Since delay loading Kernel32.dll is not supported, we need to
        // check for the presence of GetSystemWow64Directory manually.
        hm_k32 = LoadLibrary(L"Kernel32.dll");
        if (hm_k32)
        {
            proc = GetProcAddress(hm_k32, "GetSystemWow64DirectoryW");
            if (proc)
            {
                // GetSystemWow64Directory always fails on 32 bit operating systems.
                // It returns 0 on failure or the length, so we must give it a chance
                // to copy at least 1 character into the buffer so that we can dif-
                // ferentiate between an error code and a size of 0.
                size = ((UINT (__stdcall *)(LPTSTR, UINT)) proc)(tmp, 1);
                if (size > 0)
                    ret = MA_TRUE;
            }
            FreeLibrary(hm_k32);
        }
    }
    return ret;
#endif
    return MA_FALSE;
}

static void add_agent_configuration(const char *scan_path1) {
    char command[1024] = {0};
    ma_xml_t *config_xml = NULL;
    ma_xml_node_t *config_root = NULL;

    ma_temp_buffer_t buffer;
    ma_temp_buffer_init(&buffer);
#ifdef MA_WINDOWS
    form_path(&buffer, TEST_AGENT_ROOT, "EPOAGENT3000", MA_FALSE);
#else
    form_path(&buffer, MA_REGISTRY_STORE_SCAN_PATH, "EPOAGENT3000", MA_FALSE);
#endif
    MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s %s", "mkdir" , (char*)ma_temp_buffer_get(&buffer));
    system(command);

    if(MA_OK == ma_xml_create(&config_xml)) {
        if(MA_OK == ma_xml_construct(config_xml)) {
            if(NULL != (config_root = ma_xml_get_node(config_xml))) {
                ma_xml_node_t *node = NULL;
                if(MA_OK == ma_xml_node_create(config_root, MA_REGISTRY_STORE_XML_PATH, &node)) {
                    ma_xml_node_t *config_node  = NULL;
                    ma_error_t error = MA_ERROR_APIFAILED;
                    if(MA_OK == (error = ma_xml_node_create(node, MA_APPLICATION_INFO_DATA_DIR, &config_node))) {
                        ma_xml_node_set_data(config_node, TEST_AGENT_DATA_PATH);
                    }
                    if(MA_OK == error  && MA_OK == (error = ma_xml_node_create(node, MA_APPLICATION_INFO_INSTALL_DIR, &config_node))) {
                        ma_xml_node_set_data(config_node, TEST_AGENT_INSTALL_PATH);
                    }
                    if(MA_OK == error  && MA_OK == (error = ma_xml_node_create(node, MA_APPLICATION_INTEGRATION_INFO_SOFTWARE_ID, &config_node))) {
                        ma_xml_node_set_data(config_node, "EPOAGENT3000");
                    }
                    if(MA_OK == error  && MA_OK == (error = ma_xml_node_create(node, MA_APPLICATION_INTEGRATION_INFO_DATACHANNEL_CONSUMER, &config_node))) {
                        ma_xml_node_set_data(config_node, "1");
                    }
                    if(MA_OK == error  && MA_OK == (error = ma_xml_node_create(node, MA_APPLICATION_INTEGRATION_INFO_EVENT_GENERATOR, &config_node))) {
                        ma_xml_node_set_data(config_node, "1");
                    }
                    if(MA_OK == error  && MA_OK == (error = ma_xml_node_create(node, MA_APPLICATION_INTEGRATION_INFO_POLICY_CONSUMER, &config_node))) {
                        ma_xml_node_set_data(config_node, "1");
                    }
                    if(MA_OK == error  && MA_OK == (error = ma_xml_node_create(node, MA_APPLICATION_INTEGRATION_INFO_PROPERTY_PROVIDER, &config_node))) {
                        ma_xml_node_set_data(config_node, "1");
                    }
                    if(MA_OK == error  && MA_OK == (error = ma_xml_node_create(node, MA_APPLICATION_INTEGRATION_INFO_SCHEDULER_CONSUMER, &config_node))) {
                        ma_xml_node_set_data(config_node, "1");
                    }
                    if(MA_OK == error  && MA_OK == (error = ma_xml_node_create(node, MA_APPLICATION_INTEGRATION_INFO_UPDATER_CONSUMER, &config_node))) {
                        ma_xml_node_set_data(config_node, "1");
                    }
                    if(MA_OK == error ){
                        form_path(&buffer, (char *)ma_temp_buffer_get(&buffer), "config.xml",MA_FALSE);
                        ma_xml_save_to_file(config_xml, (char *)ma_temp_buffer_get(&buffer));
                    }
                }
            }
        }
        ma_xml_release(config_xml);
    }

    ma_temp_buffer_uninit(&buffer);
}

static void add_software_to_configuration(const char *scan_path1, const char *software_id, ma_int32_t registered_services){
    char command[1024] = {0};
    ma_xml_t *config_xml = NULL;
    ma_xml_node_t *config_root = NULL;

    ma_temp_buffer_t buffer;
    ma_temp_buffer_init(&buffer);
    form_path(&buffer, MA_REGISTRY_STORE_SCAN_PATH, software_id, MA_FALSE);
#ifdef MA_WINDOWS
    form_path(&buffer, TEST_AGENT_ROOT, software_id, MA_FALSE);
#else
    form_path(&buffer, MA_REGISTRY_STORE_SCAN_PATH, software_id, MA_FALSE);
#endif
    MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s %s", "mkdir" , (char*)ma_temp_buffer_get(&buffer));
    system(command);

    if(MA_OK == ma_xml_create(&config_xml)) {
        if(MA_OK == ma_xml_construct(config_xml)) {
            if(NULL != (config_root = ma_xml_get_node(config_xml))) {
                ma_xml_node_t *node = NULL;
                if(MA_OK == ma_xml_node_create(config_root, MA_REGISTRY_STORE_XML_PATH, &node)) {
                    ma_xml_node_t *config_node  = NULL;
                    ma_error_t error = MA_ERROR_APIFAILED;

                    if(MA_OK == (error = ma_xml_node_create(node, MA_APPLICATION_INTEGRATION_INFO_SOFTWARE_ID, &config_node))) {
                        ma_xml_node_set_data(config_node, software_id);
                    }
                    if(MA_OK == error  && (MA_REGISTERED_DATACHANNEL_SERVICE == (registered_services & MA_REGISTERED_DATACHANNEL_SERVICE)) && MA_OK == (error = ma_xml_node_create(node, MA_APPLICATION_INTEGRATION_INFO_DATACHANNEL_CONSUMER, &config_node))) {
                        ma_xml_node_set_data(config_node, "1");
                    }
                    if(MA_OK == error  && (MA_REGISTERED_EVENT_SERVICE == (registered_services & MA_REGISTERED_EVENT_SERVICE)) && MA_OK == (error = ma_xml_node_create(node, MA_APPLICATION_INTEGRATION_INFO_EVENT_GENERATOR, &config_node))) {
                        ma_xml_node_set_data(config_node, "1");
                    }
                    if(MA_OK == error  && (MA_REGISTERED_POLICY_SERVICE == (registered_services & MA_REGISTERED_POLICY_SERVICE)) && MA_OK == (error = ma_xml_node_create(node, MA_APPLICATION_INTEGRATION_INFO_POLICY_CONSUMER, &config_node))) {
                        ma_xml_node_set_data(config_node, "1");
                    }
                    if(MA_OK == error && (MA_REGISTERED_PROPERTY_SERVICE == (registered_services & MA_REGISTERED_PROPERTY_SERVICE))   && MA_OK == (error = ma_xml_node_create(node, MA_APPLICATION_INTEGRATION_INFO_PROPERTY_PROVIDER, &config_node))) {
                        ma_xml_node_set_data(config_node, "1");
                    }
                    if(MA_OK == error && (MA_REGISTERED_SCHEDULER_SERVICE == (registered_services & MA_REGISTERED_SCHEDULER_SERVICE))  && MA_OK == (error = ma_xml_node_create(node, MA_APPLICATION_INTEGRATION_INFO_SCHEDULER_CONSUMER, &config_node))) {
                        ma_xml_node_set_data(config_node, "1");
                    }
                    if(MA_OK == error && (MA_REGISTERED_UPDATER_SERVICE == (registered_services & MA_REGISTERED_UPDATER_SERVICE))  && MA_OK == (error = ma_xml_node_create(node, MA_APPLICATION_INTEGRATION_INFO_UPDATER_CONSUMER, &config_node))) {
                        ma_xml_node_set_data(config_node, "1");
                    }
                    if(MA_OK == error ){
                        form_path(&buffer, (char *)ma_temp_buffer_get(&buffer), "config.xml",MA_FALSE);
                        ma_xml_save_to_file(config_xml, (char *)ma_temp_buffer_get(&buffer));
                    }
                }
            }
        }
        ma_xml_release(config_xml);
    }

    ma_temp_buffer_uninit(&buffer);
}


static void fill_agent_information(ma_ds_t *ds) {
    ma_ds_set_str(ds, MA_REGISTRY_STORE_AGENT_SCAN_PATH, MA_APPLICATION_INFO_DATA_DIR, TEST_AGENT_DATA_PATH, strlen(TEST_AGENT_DATA_PATH));
    ma_ds_set_str(ds, MA_REGISTRY_STORE_AGENT_SCAN_PATH, MA_APPLICATION_INFO_INSTALL_DIR, TEST_AGENT_INSTALL_PATH, strlen(TEST_AGENT_INSTALL_PATH));
}



static void create_application_list_in_x64_view(ma_ds_t *ds) {
    /*Created First APP*/
    ma_temp_buffer_t buffer;
    ma_temp_buffer_init(&buffer);

    form_path(&buffer, MA_REGISTRY_STORE_AGENT_SCAN_PATH, "Applications", MA_TRUE);
    form_path(&buffer, (char *) ma_temp_buffer_get(&buffer), "xApplication_1", MA_TRUE);
    ma_ds_set_str(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SOFTWARE_ID, "xApplication_1", strlen("xApplication_1"));
    ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_DATACHANNEL_CONSUMER, 1);
    ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_EVENT_GENERATOR, 1);

    form_path(&buffer, MA_REGISTRY_STORE_AGENT_SCAN_PATH, "Applications", MA_TRUE);
    form_path(&buffer, (char *) ma_temp_buffer_get(&buffer), "xApplication_2", MA_TRUE);
    ma_ds_set_str(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SOFTWARE_ID, "xApplication_2", strlen("xApplication_2"));
    ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_POLICY_CONSUMER, 1);
    ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_PROPERTY_PROVIDER, 1);

    form_path(&buffer, MA_REGISTRY_STORE_AGENT_SCAN_PATH, "Applications", MA_TRUE);
    form_path(&buffer, (char *) ma_temp_buffer_get(&buffer), "xApplication_3", MA_TRUE);
    ma_ds_set_str(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SOFTWARE_ID, "xApplication_3", strlen("xApplication_3"));
    ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SCHEDULER_CONSUMER, 1);
    ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_UPDATER_CONSUMER, 1);

    form_path(&buffer, MA_REGISTRY_STORE_AGENT_SCAN_PATH, "Applications", MA_TRUE);
    form_path(&buffer, (char *) ma_temp_buffer_get(&buffer), "xApplication_4", MA_TRUE);
    ma_ds_set_str(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SOFTWARE_ID, "xApplication_4", strlen("xApplication_4"));
    ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_DATACHANNEL_CONSUMER, 1);
    ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_EVENT_GENERATOR, 1);
    ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_POLICY_CONSUMER, 1);
    ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_PROPERTY_PROVIDER, 1);
    ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SCHEDULER_CONSUMER, 1);
    ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_UPDATER_CONSUMER, 1);

    ma_temp_buffer_uninit(&buffer);
}

static void create_application_list_in_x86_view(ma_ds_t *ds) {
    /*Created First APP*/
    ma_temp_buffer_t buffer;
    ma_temp_buffer_init(&buffer);

    form_path(&buffer, MA_REGISTRY_STORE_AGENT_SCAN_PATH, "Applications", MA_TRUE);
    form_path(&buffer, (char *) ma_temp_buffer_get(&buffer), "Application_1", MA_TRUE);
    ma_ds_set_str(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SOFTWARE_ID, "Application_1", strlen("Application_1"));
    ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_DATACHANNEL_CONSUMER, 1);
    ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_EVENT_GENERATOR, 1);

    form_path(&buffer, MA_REGISTRY_STORE_AGENT_SCAN_PATH, "Applications", MA_TRUE);
    form_path(&buffer, (char *) ma_temp_buffer_get(&buffer), "Application_2", MA_TRUE);
    ma_ds_set_str(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SOFTWARE_ID, "Application_2", strlen("Application_2"));
    ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_POLICY_CONSUMER, 1);
    ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_PROPERTY_PROVIDER, 1);

    form_path(&buffer, MA_REGISTRY_STORE_AGENT_SCAN_PATH, "Applications", MA_TRUE);
    form_path(&buffer, (char *) ma_temp_buffer_get(&buffer), "Application_3", MA_TRUE);
    ma_ds_set_str(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SOFTWARE_ID, "Application_3", strlen("Application_3"));
    ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SCHEDULER_CONSUMER, 1);
    ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_UPDATER_CONSUMER, 1);

    form_path(&buffer, MA_REGISTRY_STORE_AGENT_SCAN_PATH, "Applications", MA_TRUE);
    form_path(&buffer, (char *) ma_temp_buffer_get(&buffer), "Application_4", MA_TRUE);
    ma_ds_set_str(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SOFTWARE_ID, "Application_4", strlen("Application_4"));
    ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_DATACHANNEL_CONSUMER, 1);
    ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_EVENT_GENERATOR, 1);
    ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_POLICY_CONSUMER, 1);
    ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_PROPERTY_PROVIDER, 1);
    ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SCHEDULER_CONSUMER, 1);
    ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_UPDATER_CONSUMER, 1);

    ma_temp_buffer_uninit(&buffer);
}


static void populate_applications(ma_ds_t *ds, ma_bool_t is_64_bit_os, ma_bool_t default_view){
    /*
     * First create the Agent information DATAPATH and INSTALLPATH
     * In x86 Os, create directly
     * In x64 Os, create in default view if Agent Affinity is x64
     * In x64 OS, create in x86 view if Agent Affinity is x86
     */
    if(default_view) {
        fill_agent_information(ds);
    }
    switch(MA_AGENT_BIT_AFFINITY) {
        case 1: // x86
            {
                if(is_64_bit_os && !default_view) {
                    create_application_list_in_x64_view(ds);
                }
                else if(is_64_bit_os && default_view) {
                    create_application_list_in_x86_view(ds);
                }
            }
            break;
        case 2: //x64
            {
                if(is_64_bit_os && default_view) {
                    create_application_list_in_x64_view(ds);
                }
                else if(is_64_bit_os && !default_view) {
                    create_application_list_in_x86_view(ds);
                }
            }
            break;
        default:
            break;
    }
}


static void do_registry_setup() {
    char command[1024];
#ifdef MA_WINDOWS
    ma_ds_registry_t *g_registry_default = NULL;
    #ifdef MA_WINDOWS
        MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s %s", "rmdir /S /Q" , TEST_AGENT_ROOT);
    #else
        MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s %s", "rm -rf " , TEST_AGENT_ROOT);
    #endif
    system(command);
    if(MA_OK == ma_ds_registry_open(HKEY_LOCAL_MACHINE,MA_REGISTRY_STORE_SCAN_PATH, MA_TRUE, MA_TRUE, KEY_ALL_ACCESS, &g_registry_default)) {
        populate_applications((ma_ds_t *) g_registry_default, isSystem64Bit(), MA_TRUE);
        ma_ds_registry_release(g_registry_default);
    }
    if(isSystem64Bit() == MA_TRUE) {
        if(MA_OK == ma_ds_registry_open(HKEY_LOCAL_MACHINE,MA_REGISTRY_STORE_SCAN_PATH, MA_FALSE, MA_TRUE, KEY_ALL_ACCESS, &g_registry_default)) {
            populate_applications((ma_ds_t *) g_registry_default, isSystem64Bit(), MA_FALSE);
            ma_ds_registry_release(g_registry_default);
        }
    }
#endif

    MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s %s", "mkdir" , TEST_AGENT_ROOT);
    system(command);
    MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s %s", "mkdir" , TEST_AGENT_SETUP_DATA_PATH);
    system(command);
    MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s %s", "mkdir" , TEST_AGENT_INSTALL_PATH);
    system(command);

#ifdef MA_WINDOWS
    MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s", "CD");
#else
    MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s", "pwd");
#endif
    system(command);

#ifndef MA_WINDOWS
    add_agent_configuration(MA_REGISTRY_STORE_SCAN_PATH);
    add_software_to_configuration(MA_REGISTRY_STORE_SCAN_PATH, "Application_1", MA_REGISTERED_DATACHANNEL_SERVICE | MA_REGISTERED_EVENT_SERVICE);
    add_software_to_configuration(MA_REGISTRY_STORE_SCAN_PATH, "Application_2", MA_REGISTERED_POLICY_SERVICE | MA_REGISTERED_PROPERTY_SERVICE);
    add_software_to_configuration(MA_REGISTRY_STORE_SCAN_PATH, "Application_3", MA_REGISTERED_SCHEDULER_SERVICE| MA_REGISTERED_UPDATER_SERVICE);
    add_software_to_configuration(MA_REGISTRY_STORE_SCAN_PATH, "Application_4", MA_REGISTERED_ALL);
    add_software_to_configuration(MA_REGISTRY_STORE_SCAN_PATH, "xApplication_1", MA_REGISTERED_DATACHANNEL_SERVICE | MA_REGISTERED_EVENT_SERVICE);
    add_software_to_configuration(MA_REGISTRY_STORE_SCAN_PATH, "xApplication_2", MA_REGISTERED_POLICY_SERVICE | MA_REGISTERED_PROPERTY_SERVICE);
    add_software_to_configuration(MA_REGISTRY_STORE_SCAN_PATH, "xApplication_3", MA_REGISTERED_SCHEDULER_SERVICE| MA_REGISTERED_UPDATER_SERVICE);
    add_software_to_configuration(MA_REGISTRY_STORE_SCAN_PATH, "xApplication_4", MA_REGISTERED_ALL);
#endif
}

static void do_registry_cleanup() {
    char command[1024]={0};
#ifdef MA_WINDOWS
    ma_ds_registry_t *g_registry_default = NULL;
    if(MA_OK == ma_ds_registry_open(HKEY_LOCAL_MACHINE, MA_REGISTRY_STORE_SCAN_PATH, MA_TRUE, MA_TRUE, KEY_ALL_ACCESS, &g_registry_default)) {
        ma_ds_rem((ma_ds_t*)g_registry_default, "McAfee", NULL, MA_TRUE);
        ma_ds_registry_release(g_registry_default);
    }
    if(isSystem64Bit() == MA_TRUE) {
		if(MA_OK == ma_ds_registry_open(HKEY_LOCAL_MACHINE,MA_REGISTRY_STORE_SCAN_PATH, MA_FALSE, MA_TRUE, KEY_ALL_ACCESS, &g_registry_default)) {
            ma_ds_rem((ma_ds_t *)g_registry_default, "McAfee", NULL, MA_TRUE);
            ma_ds_registry_release(g_registry_default);
        }
    }
    MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s %s", "rmdir /S /Q" , TEST_AGENT_ROOT);
#else
    MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s %s", "rm -rf " , TEST_AGENT_ROOT);
#endif
    system(command);
}

TEST_GROUP_RUNNER(sensor_test_group) {
#if 1
    //Sensor Test Cases
    do {RUN_TEST_CASE(sensor_test, ma_sensor_tray_create_valid); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_sensor_tray_create_invalid); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_user_sensor_create_valid); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_user_sensor_create_valid_invalid); }while(0);

	do {RUN_TEST_CASE(sensor_test, ma_system_sensor_create_valid); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_system_sensor_create_valid_invalid); }while(0);

	do {RUN_TEST_CASE(sensor_test, ma_power_sensor_create_valid); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_power_sensor_create_valid_invalid); }while(0);

	do {RUN_TEST_CASE(sensor_test, ma_idle_sensor_create_valid); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_idle_sensor_create_valid_invalid); }while(0);

	do {RUN_TEST_CASE(sensor_test, ma_network_sensor_create_valid); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_network_sensor_create_valid_invalid); }while(0);

	do {RUN_TEST_CASE(sensor_test, ma_sensor_tray_add_valid); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_sensor_tray_add_valid1); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_sensor_tray_add_valid2); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_sensor_tray_add_invalid); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_sensor_tray_add_invalid1); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_sensor_tray_add_invalid2); }while(0);
	
	do {RUN_TEST_CASE(sensor_test, ma_sensor_tray_release_invalid); }while(0);

	do {RUN_TEST_CASE(sensor_test, ma_sensor_tray_remove); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_sensor_tray_remove1); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_sensor_tray_remove2); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_sensor_tray_remove3); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_sensor_tray_remove4); }while(0);
 
	do {RUN_TEST_CASE(sensor_test, ma_sensor_tray_remove_invalid); }while(0);

	do {RUN_TEST_CASE(sensor_test, ma_sensor_tray_find_valid); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_sensor_tray_find_invalid); }while(0);

	do {RUN_TEST_CASE(sensor_test, ma_sensor_msg_create_from_user_sensor_msg_valid); }while(0);

	do {RUN_TEST_CASE(sensor_test, ma_sensor_msg_create_from_user_sensor_msg_invalid); }while(0);

	do {RUN_TEST_CASE(sensor_test, ma_sensor_msg_create_from_system_sensor_msg_valid); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_sensor_msg_create_from_system_sensor_msg_invalid); }while(0);

	do {RUN_TEST_CASE(sensor_test, ma_sensor_msg_create_from_network_sensor_msg_valid); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_sensor_msg_create_from_network_sensor_msg_invalid); }while(0);

	do {RUN_TEST_CASE(sensor_test, ma_sensor_msg_create_from_idle_sensor_msg_valid); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_sensor_msg_create_from_idle_sensor_msg_invalid); }while(0);
	
	do {RUN_TEST_CASE(sensor_test, ma_sensor_msg_create_from_power_sensor_msg_valid); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_sensor_msg_create_from_power_sensor_msg_invalid); }while(0);
	
	do {RUN_TEST_CASE(sensor_test, ma_sensor_msg_add_ref_valid_invalid); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_sensor_msg_get_set_type_valid); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_sensor_msg_get_set_type_invalid); }while(0);

	do {RUN_TEST_CASE(sensor_test, ma_user_sensor_msg_set_get_user_valid); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_user_sensor_msg_set_get_user_invalid); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_user_sensor_msg_get_set_size_valid); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_user_sensor_msg_get_set_size_invalid); }while(0);
			
	do {RUN_TEST_CASE(sensor_test, ma_user_sensor_msg_set_get_event_type_valid); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_user_sensor_msg_set_get_event_type_invalid); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_user_sensor_msg_set_get_domain_valid); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_user_sensor_msg_set_get_domain_invalid); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_user_sensor_msg_set_get_session_id_valid); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_user_sensor_msg_set_get_session_id_invalid); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_user_sensor_msg_set_get_logon_state_valid); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_user_sensor_msg_set_get_logon_state_invalid); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_user_sensor_msg_is_user_domain_wide_char_valid); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_user_sensor_msg_is_user_domain_wide_char_invalid); }while(0);
	
	
	do {RUN_TEST_CASE(sensor_test, ma_system_sensor_msg_get_set_size_valid); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_system_sensor_msg_get_set_size_invalid); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_system_sensor_msg_set_get_event_type_valid); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_system_sensor_msg_set_get_event_type_invalid); }while(0);

	do {RUN_TEST_CASE(sensor_test, ma_network_sensor_msg_get_set_size_valid); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_network_sensor_msg_get_set_size_invalid); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_network_sensor_msg_set_get_event_type_valid); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_network_sensor_msg_set_get_event_type_invalid); }while(0);
	
	do {RUN_TEST_CASE(sensor_test, ma_idle_sensor_msg_get_set_size_valid); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_idle_sensor_msg_get_set_size_invalid); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_idle_sensor_msg_set_get_event_type_valid); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_idle_sensor_msg_set_get_event_type_invalid); }while(0);
	
	do {RUN_TEST_CASE(sensor_test, ma_power_sensor_msg_get_set_size_valid); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_power_sensor_msg_get_set_size_invalid); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_power_sensor_msg_set_get_event_type_valid); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_power_sensor_msg_set_get_event_type_invalid); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_power_sensor_msg_set_get_power_status_valid); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_power_sensor_msg_set_get_power_status_invalid); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_power_sensor_msg_set_battery_charge_status_valid); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_power_sensor_msg_set_battery_charge_status_invalid); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_power_sensor_msg_set_get_battery_lifetime_valid); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_power_sensor_msg_set_get_battery_lifetime_invalid); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_power_sensor_msg_set_get_battery_percent_valid); }while(0);
	do {RUN_TEST_CASE(sensor_test, ma_power_sensor_msg_set_get_battery_percent_invalid); }while(0); 

	do {RUN_TEST_CASE(sensor_test, convert_user_sensor_msg_to_variant_valid); }while(0); 
	do {RUN_TEST_CASE(sensor_test, convert_user_sensor_msg_to_variant_valid1); }while(0); 
	do {RUN_TEST_CASE(sensor_test, convert_power_sensor_msg_to_variant_valid); }while(0); 
	do {RUN_TEST_CASE(sensor_test, convert_network_sensor_msg_to_variant_valid); }while(0);
	#endif
	
}

TEST_SETUP(sensor_test) {
	char *product_id = "Sensor_test";

	//TEST_ASSERT_MESSAGE(MA_OK == ma_msgbus_create(product_id, &g_msgbus),"ma_msgbus_create failed");
	//TEST_ASSERT_MESSAGE(MA_OK == ma_msgbus_start(g_msgbus),"ma_msgbus_start failed");      
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_create(product_id, &g_msgbus));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_start(g_msgbus));
}

TEST_TEAR_DOWN(sensor_test) {
   
	//TEST_ASSERT_MESSAGE(MA_OK == ma_msgbus_stop(g_msgbus,MA_TRUE),"ma_msgbus_stop failed");
	//TEST_ASSERT_MESSAGE(MA_OK == ma_msgbus_release(g_msgbus),"ma_msgbus_release failed");   
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_stop(g_msgbus,MA_TRUE));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_msgbus_release(g_msgbus));
}


TEST(sensor_test, ma_sensor_tray_create_valid) {
    ma_sensor_tray_t *tray = NULL;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_create(g_msgbus, &tray));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_release(tray));
    
}

TEST(sensor_test, ma_sensor_tray_create_invalid) {
    ma_sensor_tray_t *tray = NULL;

    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_sensor_tray_create(g_msgbus, NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_sensor_tray_create(NULL, &tray)); //issue NULL test not done
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_sensor_tray_release(NULL));

}

TEST(sensor_test, ma_user_sensor_create_valid) {
     ma_user_sensor_t *user = NULL;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_create(g_msgbus, &user));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_release(user));
    
}

TEST(sensor_test, ma_user_sensor_create_valid_invalid) {
     ma_user_sensor_t *user = NULL;

    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_user_sensor_create(g_msgbus, NULL));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_user_sensor_create(NULL, &user));//issue NULL test not done
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_user_sensor_release(NULL));

	//Use release twice
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_create(g_msgbus, &user));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_release(user));
	/*As descussed MA_OK is expected in this case*/
	//Issue not handled release twice, need to return error form API ma_sensor_dec_ref in code
	//TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_release(user)); 
    
}

TEST(sensor_test, ma_system_sensor_create_valid) {
     ma_system_sensor_t *system = NULL;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_system_sensor_create(g_msgbus, &system));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_system_sensor_release(system));
    
}

TEST(sensor_test, ma_system_sensor_create_valid_invalid) {
     ma_system_sensor_t *system = NULL;

    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_system_sensor_create(g_msgbus, NULL));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_system_sensor_create(NULL, &system));//issue NULL test not done
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_system_sensor_release(NULL));

	//Use release twice
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_system_sensor_create(g_msgbus, &system));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_system_sensor_release(system));
	/*As descussed MA_OK is expected in this case*/
	//Issue not handled release twice, need to return error form API ma_sensor_dec_ref in code
	//TEST_ASSERT_EQUAL_INT(MA_OK, ma_system_sensor_release(system)); 
    
}

TEST(sensor_test, ma_power_sensor_create_valid) {
     ma_power_sensor_t *power = NULL;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_create(g_msgbus, &power));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_release(power));
    
}

TEST(sensor_test, ma_power_sensor_create_valid_invalid) {
     ma_power_sensor_t *power = NULL;

    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_power_sensor_create(g_msgbus, NULL));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_power_sensor_create(NULL, &power));//issue NULL test not done
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_power_sensor_release(NULL));

	//Use release twice
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_create(g_msgbus, &power));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_release(power));
	/*As descussed MA_OK is expected in this case*/
	//Issue not handled release twice, need to return error form API ma_sensor_dec_ref in code
	//TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_release(power)); 
    
}

TEST(sensor_test, ma_idle_sensor_create_valid) {
     ma_idle_sensor_t *idle = NULL;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_idle_sensor_create(g_msgbus, &idle));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_idle_sensor_release(idle));
    
}

TEST(sensor_test, ma_idle_sensor_create_valid_invalid) {
     ma_idle_sensor_t *idle = NULL;

    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_idle_sensor_create(g_msgbus, NULL));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_idle_sensor_create(NULL, &idle));//issue NULL test not done
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_idle_sensor_release(NULL));

	//Use release twice
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_idle_sensor_create(g_msgbus, &idle));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_idle_sensor_release(idle));
	/*As descussed MA_OK is expected in this case*/
	//Issue not handled release twice, need to return error form API ma_sensor_dec_ref in code
	//TEST_ASSERT_EQUAL_INT(MA_OK, ma_idle_sensor_release(idle)); 
    
}

TEST(sensor_test, ma_network_sensor_create_valid) {
     ma_network_sensor_t *network = NULL;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_create(g_msgbus, &network));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_release(network));
    
}

TEST(sensor_test, ma_network_sensor_create_valid_invalid) {
     ma_network_sensor_t *network = NULL;

    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_network_sensor_create(g_msgbus, NULL));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_network_sensor_create(NULL, &network));//issue NULL test not done
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_network_sensor_release(NULL));

	//Use release twice
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_create(g_msgbus, &network));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_release(network));
	/*As descussed MA_OK is expected in this case*/
	//Issue not handled release twice, need to return error form API ma_sensor_dec_ref in code
	//TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_release(network)); 
    
}


TEST(sensor_test, ma_sensor_tray_add_valid) {
    ma_sensor_tray_t *tray = NULL;
	ma_network_sensor_t *network = NULL;
	ma_user_sensor_t *user = NULL;
	ma_system_sensor_t *system = NULL;
	ma_idle_sensor_t *idle = NULL;
	ma_power_sensor_t *power = NULL;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_create(g_msgbus, &tray));
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_create(g_msgbus, &network));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_create(g_msgbus, &user));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_system_sensor_create(g_msgbus, &system));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_idle_sensor_create(g_msgbus, &idle));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_create(g_msgbus, &power));

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)network));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)user));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)system));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)idle));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)power));


	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_release(tray));

	//Release all sensors after tray is released
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_release(network));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_release(user));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_system_sensor_release(system));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_idle_sensor_release(idle));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_release(power));
    
}

TEST(sensor_test, ma_sensor_tray_add_valid1) {
    ma_sensor_tray_t *tray = NULL;
	ma_network_sensor_t *network = NULL;
	ma_user_sensor_t *user = NULL;
	ma_system_sensor_t *system = NULL;
	ma_idle_sensor_t *idle = NULL;
	ma_power_sensor_t *power = NULL;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_create(g_msgbus, &tray));
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_create(g_msgbus, &network));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_create(g_msgbus, &user));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_system_sensor_create(g_msgbus, &system));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_idle_sensor_create(g_msgbus, &idle));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_create(g_msgbus, &power));

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)network));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)user));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)system));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)idle));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)power));

	//Release all sensors before tray is released
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_release(network));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_release(user));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_system_sensor_release(system));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_idle_sensor_release(idle));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_release(power));


	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_release(tray));

	
    
}

TEST(sensor_test, ma_sensor_tray_add_valid2) {
    ma_sensor_tray_t *tray = NULL;
	ma_network_sensor_t *network = NULL;
	ma_user_sensor_t *user = NULL;
	ma_system_sensor_t *system = NULL;
	ma_idle_sensor_t *idle = NULL;
	ma_power_sensor_t *power = NULL;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_create(g_msgbus, &tray));
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_create(g_msgbus, &network));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_create(g_msgbus, &user));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_system_sensor_create(g_msgbus, &system));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_idle_sensor_create(g_msgbus, &idle));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_create(g_msgbus, &power));

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)network));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)user));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)system));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)idle));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)power));

	//Release few sensors before tray is released
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_release(network));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_release(user));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_system_sensor_release(system));
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_release(tray));

	//Release few sensors after tray is released
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_idle_sensor_release(idle));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_release(power));
	
    
}

TEST(sensor_test, ma_sensor_tray_add_invalid) {
    ma_sensor_tray_t *tray = NULL;
	ma_network_sensor_t *network = NULL;
	ma_user_sensor_t *user = NULL;
	ma_system_sensor_t *system = NULL;
	ma_idle_sensor_t *idle = NULL;
	ma_power_sensor_t *power = NULL;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_create(g_msgbus, &tray));
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_create(g_msgbus, &network));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_create(g_msgbus, &user));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_system_sensor_create(g_msgbus, &system));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_idle_sensor_create(g_msgbus, &idle));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_create(g_msgbus, &power));

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)network));
	TEST_ASSERT_NOT_EQUAL(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)network)); 
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)system));
	TEST_ASSERT_NOT_EQUAL(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)system));//Should return some error to show its already exist
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)idle));
	TEST_ASSERT_NOT_EQUAL(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)idle));//Should return some error to show its already exist
		
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)power));
	TEST_ASSERT_NOT_EQUAL(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)power));//Should return some error to show its already exist
		
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_release(network));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_release(user));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_system_sensor_release(system));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_idle_sensor_release(idle));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_release(power));
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_release(tray));

	  
}

TEST(sensor_test, ma_sensor_tray_add_invalid1) {
    ma_sensor_tray_t *tray = NULL;
	ma_network_sensor_t *network = NULL;
	

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_create(g_msgbus, &tray));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_create(g_msgbus, &network));
	
	//Adding a same tray in a tray
	TEST_ASSERT_NOT_EQUAL(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)tray));//Need to retun error if same tray we are adding in the tray.

	//Passing normal sensor as sensor tray 
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_sensor_tray_add((ma_sensor_tray_t *)network,(ma_sensor_t *)tray));//Need to retun error,currently it crash

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_release(network));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_release(tray));    
}

TEST(sensor_test, ma_sensor_tray_add_invalid2) {
    ma_sensor_tray_t *tray = NULL;
	ma_sensor_tray_t *tray1 = NULL;
	ma_network_sensor_t *network = NULL;
	

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_create(g_msgbus, &tray));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_create(g_msgbus, &tray1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_create(g_msgbus, &network));
	
		

	//Adding a diferent tray in a tray
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)tray1));//Need to check is it possile to add one tray in naother tray.
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_release(network));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_release(tray));    
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_release(tray1));    
}


TEST(sensor_test, ma_sensor_tray_release_invalid) {
    ma_sensor_tray_t *tray = NULL;
	ma_network_sensor_t *network = NULL;
	

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_create(g_msgbus, &tray));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_create(g_msgbus, &network));
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)network));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_release(network));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_sensor_tray_release(NULL));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_release(tray));
	//TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_release(tray));
}


TEST(sensor_test, ma_sensor_tray_remove) {
    ma_sensor_tray_t *tray = NULL;
	ma_network_sensor_t *network = NULL;
	ma_user_sensor_t *user = NULL;
	ma_system_sensor_t *system = NULL;
	ma_idle_sensor_t *idle = NULL;
	ma_power_sensor_t *power = NULL;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_create(g_msgbus, &tray));
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_create(g_msgbus, &network));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_create(g_msgbus, &user));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_system_sensor_create(g_msgbus, &system));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_idle_sensor_create(g_msgbus, &idle));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_create(g_msgbus, &power));

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)network));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)user));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)system));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)idle));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)power));

	//Release All sensors before they removed from tray.
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_release(network));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_release(user));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_system_sensor_release(system));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_idle_sensor_release(idle));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_release(power));

		TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_remove(tray,(ma_sensor_t *)system));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_remove(tray,(ma_sensor_t *)idle));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_remove(tray,(ma_sensor_t *)power));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_remove(tray,(ma_sensor_t *)network));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_remove(tray,(ma_sensor_t *)user));
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_release(tray));
    
}

TEST(sensor_test, ma_sensor_tray_remove1) {
    ma_sensor_tray_t *tray = NULL;
	ma_network_sensor_t *network = NULL;
	ma_user_sensor_t *user = NULL;
	ma_system_sensor_t *system = NULL;
	ma_idle_sensor_t *idle = NULL;
	ma_power_sensor_t *power = NULL;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_create(g_msgbus, &tray));
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_create(g_msgbus, &network));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_create(g_msgbus, &user));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_system_sensor_create(g_msgbus, &system));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_idle_sensor_create(g_msgbus, &idle));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_create(g_msgbus, &power));

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)network));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)user));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)system));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)idle));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)power));

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_remove(tray,(ma_sensor_t *)system));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_remove(tray,(ma_sensor_t *)idle));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_remove(tray,(ma_sensor_t *)power));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_remove(tray,(ma_sensor_t *)network));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_remove(tray,(ma_sensor_t *)user));
	
	//Release All sensors before they removed from tray.
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_release(network));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_release(user));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_system_sensor_release(system));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_idle_sensor_release(idle));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_release(power));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_release(tray));

}

TEST(sensor_test, ma_sensor_tray_remove2) {
    ma_sensor_tray_t *tray = NULL;
	ma_network_sensor_t *network = NULL;
	ma_user_sensor_t *user = NULL;
	ma_system_sensor_t *system = NULL;
	ma_idle_sensor_t *idle = NULL;
	ma_power_sensor_t *power = NULL;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_create(g_msgbus, &tray));
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_create(g_msgbus, &network));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_create(g_msgbus, &user));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_system_sensor_create(g_msgbus, &system));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_idle_sensor_create(g_msgbus, &idle));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_create(g_msgbus, &power));

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)network));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)user));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)system));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)idle));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)power));

	//Release few sensors before they removed from tray.
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_release(network));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_release(user));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_system_sensor_release(system));

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_remove(tray,(ma_sensor_t *)system));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_remove(tray,(ma_sensor_t *)idle));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_remove(tray,(ma_sensor_t *)power));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_remove(tray,(ma_sensor_t *)network));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_remove(tray,(ma_sensor_t *)user));
	
	//Release few sensors after they removed from tray.
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_idle_sensor_release(idle));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_release(power));
	
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_release(tray));

	   
}

TEST(sensor_test, ma_sensor_tray_remove3) {
    ma_sensor_tray_t *tray = NULL;
	ma_sensor_tray_t *tray1 = NULL;
	ma_network_sensor_t *network = NULL;
	ma_user_sensor_t *user = NULL;
	ma_system_sensor_t *system = NULL;
	ma_idle_sensor_t *idle = NULL;
	ma_power_sensor_t *power = NULL;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_create(g_msgbus, &tray));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_create(g_msgbus, &tray1));
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_create(g_msgbus, &network));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_create(g_msgbus, &user));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_system_sensor_create(g_msgbus, &system));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_idle_sensor_create(g_msgbus, &idle));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_create(g_msgbus, &power));

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)network));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)user));

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray1,(ma_sensor_t *)system));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray1,(ma_sensor_t *)idle));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray1,(ma_sensor_t *)power));

	//Add tray1 in tray
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)tray1));

	//Release few sensors before they removed from tray.
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_release(network));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_release(user));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_system_sensor_release(system));

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_remove(tray,(ma_sensor_t *)tray1));

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_remove(tray1,(ma_sensor_t *)system));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_remove(tray1,(ma_sensor_t *)idle));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_remove(tray1,(ma_sensor_t *)power));

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_remove(tray,(ma_sensor_t *)network));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_remove(tray,(ma_sensor_t *)user));
	
	//Release few sensors after they removed from tray.
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_idle_sensor_release(idle));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_release(power));
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_release(tray1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_release(tray));

	   
}


TEST(sensor_test, ma_sensor_tray_remove4) {
    ma_sensor_tray_t *tray = NULL;
	ma_network_sensor_t *network = NULL;
	ma_user_sensor_t *user = NULL;
	ma_system_sensor_t *system = NULL;
	ma_idle_sensor_t *idle = NULL;
	ma_power_sensor_t *power = NULL;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_create(g_msgbus, &tray));
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_create(g_msgbus, &network));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_create(g_msgbus, &user));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_system_sensor_create(g_msgbus, &system));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_idle_sensor_create(g_msgbus, &idle));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_create(g_msgbus, &power));

	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)power));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)system));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)idle));
		
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_remove(tray,(ma_sensor_t *)system));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_remove(tray,(ma_sensor_t *)idle));

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)network));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)user));
	
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_remove(tray,(ma_sensor_t *)power));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_remove(tray,(ma_sensor_t *)network));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_remove(tray,(ma_sensor_t *)user));
	
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_idle_sensor_release(idle));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_release(power));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_release(network));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_release(user));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_system_sensor_release(system));

	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_release(tray));

	   
}

TEST(sensor_test, ma_sensor_tray_remove_invalid) {
    ma_sensor_tray_t *tray = NULL;
	ma_network_sensor_t *network = NULL;
	

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_create(g_msgbus, &tray));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_create(g_msgbus, &network));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)network));
	
	
	
	//NULL test
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_sensor_tray_remove(tray,NULL));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_sensor_tray_remove(NULL,(ma_sensor_t *)network));
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_remove(tray,(ma_sensor_t *)network));
	
	//Removing sensor which doesnt exist in tray
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_remove(tray,(ma_sensor_t *)network));//Will not return error when sensor doesn't exist in tray
	
	//Removing tray fom tray
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_remove(tray,(ma_sensor_t *)tray));//Will not return error if we try to remove tray from same tray

	//Release few sensors before they removed from tray.
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_release(network));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_release(tray));

	   
}

TEST(sensor_test, ma_sensor_tray_find_valid) {
    ma_sensor_tray_t *tray = NULL;
	ma_sensor_tray_t *tray1 = NULL;
	ma_network_sensor_t *network = NULL;
	ma_user_sensor_t *user = NULL;
	ma_system_sensor_t *system = NULL;
	ma_idle_sensor_t *idle = NULL;
	ma_power_sensor_t *power = NULL;
	ma_bool_t exist = MA_FALSE;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_create(g_msgbus, &tray));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_create(g_msgbus, &tray1));
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_create(g_msgbus, &network));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_create(g_msgbus, &user));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_system_sensor_create(g_msgbus, &system));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_idle_sensor_create(g_msgbus, &idle));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_create(g_msgbus, &power));

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)network));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)user));

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray1,(ma_sensor_t *)system));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray1,(ma_sensor_t *)idle));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray1,(ma_sensor_t *)power));

	//Add tray1 in tray
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)tray1));


	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_find(tray,(ma_sensor_t *)network,&exist));
	TEST_ASSERT_EQUAL_INT(MA_TRUE,exist);
	
	exist = MA_FALSE;
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_find(tray,(ma_sensor_t *)user,&exist));
	TEST_ASSERT_EQUAL_INT(MA_TRUE,exist);

	exist = MA_FALSE;
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_find(tray,(ma_sensor_t *)tray1,&exist));
	TEST_ASSERT_EQUAL_INT(MA_TRUE,exist);

	exist = MA_FALSE;
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_find(tray1,(ma_sensor_t *)system,&exist));
	TEST_ASSERT_EQUAL_INT(MA_TRUE,exist);

	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_release(network));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_release(user));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_system_sensor_release(system));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_idle_sensor_release(idle));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_release(power));
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_release(tray1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_release(tray));

	   
}

TEST(sensor_test, ma_sensor_tray_find_invalid) {
    ma_sensor_tray_t *tray = NULL;
	ma_sensor_tray_t *tray1 = NULL;
	ma_network_sensor_t *network = NULL;
	ma_user_sensor_t *user = NULL;
	ma_system_sensor_t *system = NULL;
	ma_idle_sensor_t *idle = NULL;
	ma_power_sensor_t *power = NULL;
	ma_bool_t exist = MA_TRUE;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_create(g_msgbus, &tray));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_create(g_msgbus, &tray1));
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_create(g_msgbus, &network));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_create(g_msgbus, &user));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_system_sensor_create(g_msgbus, &system));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_idle_sensor_create(g_msgbus, &idle));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_create(g_msgbus, &power));

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)network));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)user));

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray1,(ma_sensor_t *)system));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray1,(ma_sensor_t *)idle));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray1,(ma_sensor_t *)power));

	//Add tray1 in tray
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_add(tray,(ma_sensor_t *)tray1));


	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_find(tray1,(ma_sensor_t *)network,&exist));
	TEST_ASSERT_EQUAL_INT(MA_FALSE,exist);
	
	exist = MA_TRUE;
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_find(tray1,(ma_sensor_t *)user,&exist));
	TEST_ASSERT_EQUAL_INT(MA_FALSE,exist);

	exist = MA_TRUE;
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_find(tray1,(ma_sensor_t *)tray1,&exist));
	TEST_ASSERT_EQUAL_INT(MA_FALSE,exist);

	exist = MA_TRUE;
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_tray_find(tray,(ma_sensor_t *)system,&exist));
	TEST_ASSERT_EQUAL_INT(MA_FALSE,exist);

	
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_sensor_tray_find(NULL,(ma_sensor_t *)system,&exist));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_sensor_tray_find(tray,NULL,&exist));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_sensor_tray_find(tray,(ma_sensor_t *)system,NULL));
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_release(network));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_release(user));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_system_sensor_release(system));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_idle_sensor_release(idle));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_release(power));
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_release(tray1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_tray_release(tray));
	   
}

TEST(sensor_test, ma_sensor_msg_create_from_user_sensor_msg_valid) {
    ma_sensor_msg_t *sensor_msg;
	ma_user_sensor_msg_t *user_sensor_msg;
	ma_user_sensor_msg_t *user_sensor_msg_test;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_create(&user_sensor_msg,1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_user_sensor_msg(user_sensor_msg,&sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_get_user_sensor_msg(sensor_msg,&user_sensor_msg_test));
	TEST_ASSERT_EQUAL_INT(user_sensor_msg_test,user_sensor_msg);

    TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_release(user_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_release(user_sensor_msg_test));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));

}

TEST(sensor_test, ma_sensor_msg_create_from_user_sensor_msg_invalid) {
    ma_sensor_msg_t *sensor_msg;
	ma_user_sensor_msg_t *user_sensor_msg;
	ma_user_sensor_msg_t *user_sensor_msg_test;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_create(&user_sensor_msg,1));
	
	//NULL test
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_user_sensor_msg_create(NULL,1));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_sensor_msg_create_from_user_sensor_msg(user_sensor_msg,NULL));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_sensor_msg_create_from_user_sensor_msg(NULL,&sensor_msg));
	
	//NULL test
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_sensor_msg_get_user_sensor_msg(NULL,&user_sensor_msg_test));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_sensor_msg_get_user_sensor_msg(sensor_msg,NULL));

    TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_release(user_sensor_msg));
	//NULL test
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_user_sensor_msg_release(NULL));
	

}

TEST(sensor_test, ma_sensor_msg_create_from_system_sensor_msg_valid) {
    ma_sensor_msg_t *sensor_msg;
	ma_system_sensor_msg_t *system_sensor_msg;
	ma_system_sensor_msg_t *system_sensor_msg_test;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_system_sensor_msg_create(&system_sensor_msg,1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_system_sensor_msg(system_sensor_msg,&sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_get_system_sensor_msg(sensor_msg,&system_sensor_msg_test));
	TEST_ASSERT_EQUAL_INT(system_sensor_msg_test,system_sensor_msg);

    TEST_ASSERT_EQUAL_INT(MA_OK,ma_system_sensor_msg_release(system_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_system_sensor_msg_release(system_sensor_msg_test));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));

}

TEST(sensor_test, ma_sensor_msg_create_from_system_sensor_msg_invalid) {
    ma_sensor_msg_t *sensor_msg;
	ma_system_sensor_msg_t *system_sensor_msg;
	ma_system_sensor_msg_t *system_sensor_msg_test;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_system_sensor_msg_create(&system_sensor_msg,1));
	
	//NULL test
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_sensor_msg_create_from_system_sensor_msg(system_sensor_msg,NULL));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_sensor_msg_create_from_system_sensor_msg(NULL,&sensor_msg));
	//NULL test
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_sensor_msg_get_system_sensor_msg(NULL,&system_sensor_msg_test));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_sensor_msg_get_system_sensor_msg(sensor_msg,NULL));


    TEST_ASSERT_EQUAL_INT(MA_OK,ma_system_sensor_msg_release(system_sensor_msg));
	//NULL test
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_system_sensor_msg_release(NULL));

}


TEST(sensor_test, ma_sensor_msg_create_from_network_sensor_msg_valid) {
    ma_sensor_msg_t *sensor_msg;
	ma_network_sensor_msg_t *network_sensor_msg;
	ma_network_sensor_msg_t *network_sensor_msg_test;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_msg_create(&network_sensor_msg,1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_network_sensor_msg(network_sensor_msg,&sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_get_network_sensor_msg(sensor_msg,&network_sensor_msg_test));
	TEST_ASSERT_EQUAL_INT(network_sensor_msg_test,network_sensor_msg);

    TEST_ASSERT_EQUAL_INT(MA_OK,ma_network_sensor_msg_release(network_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_network_sensor_msg_release(network_sensor_msg_test));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));

}

TEST(sensor_test, ma_sensor_msg_create_from_network_sensor_msg_invalid) {
    ma_sensor_msg_t *sensor_msg;
	ma_network_sensor_msg_t *network_sensor_msg;
	ma_network_sensor_msg_t *network_sensor_msg_test;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_msg_create(&network_sensor_msg,1));
	
	//NULL test
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_sensor_msg_create_from_network_sensor_msg(network_sensor_msg,NULL));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_sensor_msg_create_from_network_sensor_msg(NULL,&sensor_msg));
	//NULL test
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_sensor_msg_get_network_sensor_msg(NULL,&network_sensor_msg_test));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_sensor_msg_get_network_sensor_msg(sensor_msg,NULL));


    TEST_ASSERT_EQUAL_INT(MA_OK,ma_network_sensor_msg_release(network_sensor_msg));
	//NULL test
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_network_sensor_msg_release(NULL));

}

TEST(sensor_test, ma_sensor_msg_create_from_idle_sensor_msg_valid) {
    ma_sensor_msg_t *sensor_msg;
	ma_idle_sensor_msg_t *idle_sensor_msg;
	ma_idle_sensor_msg_t *idle_sensor_msg_test;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_idle_sensor_msg_create(&idle_sensor_msg,1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_idle_sensor_msg(idle_sensor_msg,&sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_get_idle_sensor_msg(sensor_msg,&idle_sensor_msg_test));
	TEST_ASSERT_EQUAL_INT(idle_sensor_msg_test,idle_sensor_msg);

    TEST_ASSERT_EQUAL_INT(MA_OK,ma_idle_sensor_msg_release(idle_sensor_msg));
	 TEST_ASSERT_EQUAL_INT(MA_OK,ma_idle_sensor_msg_release(idle_sensor_msg_test));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));

}

TEST(sensor_test, ma_sensor_msg_create_from_idle_sensor_msg_invalid) {
    ma_sensor_msg_t *sensor_msg;
	ma_idle_sensor_msg_t *idle_sensor_msg;
	ma_idle_sensor_msg_t *idle_sensor_msg_test;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_idle_sensor_msg_create(&idle_sensor_msg,1));
	
	//NULL test
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_sensor_msg_create_from_idle_sensor_msg(idle_sensor_msg,NULL));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_sensor_msg_create_from_idle_sensor_msg(NULL,&sensor_msg));
	//NULL test
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_sensor_msg_get_idle_sensor_msg(NULL,&idle_sensor_msg_test));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_sensor_msg_get_idle_sensor_msg(sensor_msg,NULL));


    TEST_ASSERT_EQUAL_INT(MA_OK,ma_idle_sensor_msg_release(idle_sensor_msg));
	//NULL test
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_idle_sensor_msg_release(NULL));

}

TEST(sensor_test, ma_sensor_msg_create_from_power_sensor_msg_valid) {
    ma_sensor_msg_t *sensor_msg;
	ma_power_sensor_msg_t *power_sensor_msg;
	ma_power_sensor_msg_t *power_sensor_msg_test;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_msg_create(&power_sensor_msg,1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_power_sensor_msg(power_sensor_msg,&sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_get_power_sensor_msg(sensor_msg,&power_sensor_msg_test));
	TEST_ASSERT_EQUAL_INT(power_sensor_msg_test,power_sensor_msg);

    TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_release(power_sensor_msg));
	 TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_release(power_sensor_msg_test));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));

}

TEST(sensor_test, ma_sensor_msg_create_from_power_sensor_msg_invalid) {
    ma_sensor_msg_t *sensor_msg;
	ma_power_sensor_msg_t *power_sensor_msg;
	ma_power_sensor_msg_t *power_sensor_msg_test;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_msg_create(&power_sensor_msg,1));
	
	//NULL test
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_sensor_msg_create_from_power_sensor_msg(power_sensor_msg,NULL));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_sensor_msg_create_from_power_sensor_msg(NULL,&sensor_msg));
	//NULL test
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_sensor_msg_get_power_sensor_msg(NULL,&power_sensor_msg_test));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_sensor_msg_get_power_sensor_msg(sensor_msg,NULL));


    TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_release(power_sensor_msg));
	//NULL test
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_power_sensor_msg_release(NULL));

}

TEST(sensor_test, ma_sensor_msg_add_ref_valid_invalid) {
    ma_sensor_msg_t *sensor_msg;
	ma_user_sensor_msg_t *user_sensor_msg;
	ma_user_sensor_msg_t *user_sensor_msg_test;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_create(&user_sensor_msg,1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_user_sensor_msg(user_sensor_msg,&sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_get_user_sensor_msg(sensor_msg,&user_sensor_msg_test));
	TEST_ASSERT_EQUAL_INT(user_sensor_msg_test,user_sensor_msg);

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_add_ref(sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));

	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_sensor_msg_add_ref(NULL));

    TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_release(user_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_release(user_sensor_msg_test));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));

}

TEST(sensor_test, ma_sensor_msg_get_set_type_valid) {
    ma_sensor_msg_t *sensor_msg;
	ma_user_sensor_msg_t *user_sensor_msg;
	ma_sensor_type_t type;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_create(&user_sensor_msg,1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_user_sensor_msg(user_sensor_msg,&sensor_msg));
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_get_type(sensor_msg,&type));
	TEST_ASSERT_EQUAL_INT(MA_SENSOR_TYPE_USER,type);

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_set_type(sensor_msg,MA_SENSOR_TYPE_IDLE));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_get_type(sensor_msg,&type));
	TEST_ASSERT_EQUAL_INT(MA_SENSOR_TYPE_IDLE,type);

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_set_type(sensor_msg,MA_SENSOR_TYPE_USER));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_release(user_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));

}

TEST(sensor_test, ma_sensor_msg_get_set_type_invalid) {
    ma_sensor_msg_t *sensor_msg;
	ma_user_sensor_msg_t *user_sensor_msg;
	ma_sensor_type_t type;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_create(&user_sensor_msg,1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_user_sensor_msg(user_sensor_msg,&sensor_msg));
	
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_sensor_msg_get_type(sensor_msg,NULL));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_sensor_msg_get_type(NULL,&type));
	
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_sensor_msg_set_type(NULL,MA_SENSOR_TYPE_IDLE));
	
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_release(user_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));

}

TEST(sensor_test, ma_user_sensor_msg_set_get_user_valid) {
    ma_sensor_msg_t *sensor_msg;
	ma_user_sensor_msg_t *user_sensor_msg;
	char *user1 = "Sheetal";
	char *user2 = "Sheetal1";
	ma_buffer_t *user1_buff=NULL;
	ma_buffer_t *user2_buff=NULL;
	char *user1_test = NULL;
	char *user2_test = NULL;
	size_t len =0;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_create(&user_sensor_msg,2));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_user_sensor_msg(user_sensor_msg,&sensor_msg));
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_set_user(user_sensor_msg,0,user1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_set_user(user_sensor_msg,1,user2));

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_get_user(user_sensor_msg,0,&user1_buff));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_get_user(user_sensor_msg,1,&user2_buff));

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_buffer_get_string(user1_buff,&user1_test,&len));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_buffer_get_string(user2_buff,&user2_test,&len));

	TEST_ASSERT_EQUAL_INT(MA_OK,strcmp(user1_test,user1));
	TEST_ASSERT_EQUAL_INT(MA_OK,strcmp(user2_test,user2));

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_buffer_release(user1_buff));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_buffer_release(user2_buff));

    TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_release(user_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));

}


TEST(sensor_test, ma_user_sensor_msg_set_get_user_invalid) {
    ma_sensor_msg_t *sensor_msg;
	ma_user_sensor_msg_t *user_sensor_msg;
	char *user1 = "Sheetal";
	char *user2 = "Sheetal188888888888888888888888888888888888888888888888888888888888888888888888888888888866666666666\
				  Sheetal188888888888888888888888888888888888888888888888888888888888888888888888888888888866666666666\
				  98888888888888888888888888888888888888888888";
	ma_buffer_t *user1_test=NULL;
	ma_buffer_t *user2_test=NULL;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_create(&user_sensor_msg,2));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_user_sensor_msg(user_sensor_msg,&sensor_msg));
	
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_user_sensor_msg_set_user(NULL,0,user1));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_user_sensor_msg_set_user(user_sensor_msg,1,NULL));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_user_sensor_msg_set_user(user_sensor_msg,0,user2));


	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_user_sensor_msg_get_user(NULL,0,&user1_test));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_user_sensor_msg_get_user(user_sensor_msg,1,NULL));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_user_sensor_msg_get_user(user_sensor_msg,1,&user2_test));

    TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_release(user_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));

}

//Test cash at the time of ma_sensor_msg_release
TEST(sensor_test, ma_user_sensor_msg_get_set_size_valid) {
    ma_sensor_msg_t *sensor_msg;
	ma_user_sensor_msg_t *user_sensor_msg;
	size_t users = 100;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_create(&user_sensor_msg,200));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_user_sensor_msg(user_sensor_msg,&sensor_msg));
	
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_get_size(user_sensor_msg,&users));
	TEST_ASSERT_EQUAL_INT(users,200);
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_set_size(user_sensor_msg,1));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_get_size(user_sensor_msg,&users));
	TEST_ASSERT_EQUAL_INT(users,1);
	
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_release(user_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));

}

TEST(sensor_test, ma_user_sensor_msg_get_set_size_invalid) {
    ma_sensor_msg_t *sensor_msg;
	ma_user_sensor_msg_t *user_sensor_msg;
	size_t users = 100;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_create(&user_sensor_msg,0));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_user_sensor_msg(user_sensor_msg,&sensor_msg));
	
	
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_user_sensor_msg_get_size(NULL,&users));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_user_sensor_msg_get_size(user_sensor_msg,NULL));
	
	
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_user_sensor_msg_set_size(NULL,1));
		

    TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_release(user_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));

}

TEST(sensor_test, ma_user_sensor_msg_set_get_event_type_valid) {
    ma_sensor_msg_t *sensor_msg;
	ma_user_sensor_msg_t *user_sensor_msg;
	 ma_sensor_event_type_t type = MA_USER_SENSOR_LOG_ON_EVENT;
	  ma_sensor_event_type_t type_test;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_create(&user_sensor_msg,1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_user_sensor_msg(user_sensor_msg,&sensor_msg));
	
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_get_event_type(user_sensor_msg,&type_test));
	TEST_ASSERT_EQUAL_INT(type_test,MA_SYSTEM_SENSOR_EVENT_STOP);

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_set_event_type(user_sensor_msg,type));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_get_event_type(user_sensor_msg,&type_test));
	TEST_ASSERT_EQUAL_INT(type_test,MA_USER_SENSOR_LOG_ON_EVENT);

	type = MA_USER_SENSOR_LOG_OFF_EVENT;
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_set_event_type(user_sensor_msg,type));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_get_event_type(user_sensor_msg,&type_test));
	TEST_ASSERT_EQUAL_INT(type_test,MA_USER_SENSOR_LOG_OFF_EVENT);
		
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_release(user_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));

}

TEST(sensor_test, ma_user_sensor_msg_set_get_event_type_invalid) {
    ma_sensor_msg_t *sensor_msg;
	ma_user_sensor_msg_t *user_sensor_msg;
	 ma_sensor_event_type_t type = MA_USER_SENSOR_LOG_ON_EVENT;
	  ma_sensor_event_type_t type_test;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_create(&user_sensor_msg,1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_user_sensor_msg(user_sensor_msg,&sensor_msg));
	
	
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_user_sensor_msg_get_event_type(user_sensor_msg,NULL));

	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_user_sensor_msg_set_event_type(NULL,type));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_user_sensor_msg_get_event_type(NULL,&type_test));
	
		
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_release(user_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));

}

TEST(sensor_test, ma_user_sensor_msg_set_get_domain_valid) {
    ma_sensor_msg_t *sensor_msg;
	ma_user_sensor_msg_t *user_sensor_msg;
	char *domain1 = "nai.corp.org";
	char *domain2 = "epo2003";
	ma_buffer_t *domain1_buff=NULL;
	ma_buffer_t *domain2_buff=NULL;
	char *domain1_test = NULL;
	char *domain2_test = NULL;
	size_t len =0;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_create(&user_sensor_msg,2));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_user_sensor_msg(user_sensor_msg,&sensor_msg));
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_set_domain(user_sensor_msg,0,domain1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_set_domain(user_sensor_msg,1,domain2));

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_get_domain(user_sensor_msg,0,&domain1_buff));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_get_domain(user_sensor_msg,1,&domain2_buff));

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_buffer_get_string(domain1_buff,&domain1_test,&len));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_buffer_get_string(domain2_buff,&domain2_test,&len));

	TEST_ASSERT_EQUAL_INT(MA_OK,strcmp(domain1_test,domain1));
	TEST_ASSERT_EQUAL_INT(MA_OK,strcmp(domain2_test,domain2));

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_buffer_release(domain1_buff));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_buffer_release(domain2_buff));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_release(user_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));
	
}

TEST(sensor_test, ma_user_sensor_msg_set_get_domain_invalid) {
    ma_sensor_msg_t *sensor_msg;
	ma_user_sensor_msg_t *user_sensor_msg;
	char *domain1 = "nai.corp.org";
	char *domain2 = "Sheetal188888888888888888888888888888888888888888888888888888888888888888888888888888888866666666666\
				  Sheetal188888888888888888888888888888888888888888888888888888888888888888888888888888888866666666666\
				  98888888888888888888888888888888888888888888";
	ma_buffer_t *domain1_test=NULL;
	ma_buffer_t *domain2_test=NULL;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_create(&user_sensor_msg,2));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_user_sensor_msg(user_sensor_msg,&sensor_msg));
	
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_user_sensor_msg_set_domain(NULL,0,domain1));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_user_sensor_msg_set_domain(user_sensor_msg,1,NULL));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_user_sensor_msg_set_domain(user_sensor_msg,0,domain2));


	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_user_sensor_msg_get_domain(NULL,0,&domain1_test));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_user_sensor_msg_get_domain(user_sensor_msg,1,NULL));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_user_sensor_msg_get_domain(user_sensor_msg,1,&domain2_test));

    TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_release(user_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));

}

TEST(sensor_test, ma_user_sensor_msg_set_get_session_id_valid) {
    ma_sensor_msg_t *sensor_msg;
	ma_user_sensor_msg_t *user_sensor_msg;
	ma_uint32_t session_id1 = 1;
	ma_uint32_t session_id2 = 2;
	ma_uint32_t session_id1_test=0;
	ma_uint32_t session_id2_test=0;
	

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_create(&user_sensor_msg,2));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_user_sensor_msg(user_sensor_msg,&sensor_msg));
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_set_session_id(user_sensor_msg,0,session_id1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_set_session_id(user_sensor_msg,1,session_id2));

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_get_session_id(user_sensor_msg,0,&session_id1_test));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_get_session_id(user_sensor_msg,1,&session_id2_test));

	
	TEST_ASSERT_EQUAL_INT(session_id1,session_id1_test);
	TEST_ASSERT_EQUAL_INT(session_id2,session_id2_test);

    TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_release(user_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));
	
}


TEST(sensor_test, ma_user_sensor_msg_set_get_session_id_invalid) {
    ma_sensor_msg_t *sensor_msg;
	ma_user_sensor_msg_t *user_sensor_msg;
	ma_uint32_t session_id1 = 1;
	ma_uint32_t session_id2 = 2;
	ma_uint32_t session_id1_test=0;
	ma_uint32_t session_id2_test=0;
	

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_create(&user_sensor_msg,2));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_user_sensor_msg(user_sensor_msg,&sensor_msg));
	
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_user_sensor_msg_set_session_id(NULL,1,session_id2));

	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_user_sensor_msg_get_session_id(NULL,0,&session_id1_test));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_user_sensor_msg_get_session_id(user_sensor_msg,1,NULL));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_OUTOFBOUNDS,ma_user_sensor_msg_get_session_id(user_sensor_msg,3,&session_id2_test));//test for index not exist

		
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_release(user_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));
	
}

TEST(sensor_test, ma_user_sensor_msg_set_get_logon_state_valid) {
    ma_sensor_msg_t *sensor_msg;
	ma_user_sensor_msg_t *user_sensor_msg;
	ma_user_logged_state_t state = MA_USER_LOGGED_STATE_ON;
	ma_user_logged_state_t state1_test;//= MA_USER_LOGGED_STATE_ON;
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_create(&user_sensor_msg,2));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_user_sensor_msg(user_sensor_msg,&sensor_msg));
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_set_logon_state(user_sensor_msg,state));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_get_logon_state(user_sensor_msg,&state1_test));
	TEST_ASSERT_EQUAL_INT(state,state1_test);

	state = MA_USER_LOGGED_STATE_OFF;
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_set_logon_state(user_sensor_msg,state));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_get_logon_state(user_sensor_msg,&state1_test));
	TEST_ASSERT_EQUAL_INT(state,state1_test);


    TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_release(user_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));
	
}

TEST(sensor_test, ma_user_sensor_msg_set_get_logon_state_invalid) {
    ma_sensor_msg_t *sensor_msg;
	ma_user_sensor_msg_t *user_sensor_msg;
	ma_user_logged_state_t state = MA_USER_LOGGED_STATE_ON;
	ma_user_logged_state_t state1_test;//= MA_USER_LOGGED_STATE_ON;
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_create(&user_sensor_msg,2));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_user_sensor_msg(user_sensor_msg,&sensor_msg));
	
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_user_sensor_msg_set_logon_state(NULL,state));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_user_sensor_msg_get_logon_state(NULL,&state1_test));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_user_sensor_msg_get_logon_state(user_sensor_msg,NULL));
	
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_release(user_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));
	
}

TEST(sensor_test, ma_user_sensor_msg_is_user_domain_wide_char_valid) {
    ma_sensor_msg_t *sensor_msg;
	ma_user_sensor_msg_t *user_sensor_msg;
	char *user1 = "Sheetal";
	wchar_t *user2 = L"Sheetal1";
	ma_uint32_t is_wchar;
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_create(&user_sensor_msg,2));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_user_sensor_msg(user_sensor_msg,&sensor_msg));
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_set_user(user_sensor_msg,0,user1));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_set_domain(user_sensor_msg,0,"nai.corp.org"));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_set_wuser(user_sensor_msg,1,user2));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_set_wdomain(user_sensor_msg,1,L"epo2003"));

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_is_user_wide_char(user_sensor_msg,0,&is_wchar));
	TEST_ASSERT_EQUAL_INT(is_wchar,MA_ASCII_STRING);
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_is_user_wide_char(user_sensor_msg,1,&is_wchar));
	TEST_ASSERT_EQUAL_INT(is_wchar,MA_WCHAR_STRING);

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_is_domain_wide_char(user_sensor_msg,0,&is_wchar));
	TEST_ASSERT_EQUAL_INT(is_wchar,MA_ASCII_STRING);
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_is_domain_wide_char(user_sensor_msg,1,&is_wchar));
	TEST_ASSERT_EQUAL_INT(is_wchar,MA_WCHAR_STRING);

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_release(user_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));

}

TEST(sensor_test, ma_user_sensor_msg_is_user_domain_wide_char_invalid) {
    ma_sensor_msg_t *sensor_msg;
	ma_user_sensor_msg_t *user_sensor_msg;
	char *user1 = "Sheetal";
	wchar_t *user2 = L"Sheetal1";
	ma_uint32_t is_wchar;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_create(&user_sensor_msg,2));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_user_sensor_msg(user_sensor_msg,&sensor_msg));
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_set_user(user_sensor_msg,0,user1));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_set_domain(user_sensor_msg,0,"nai.corp.org"));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_set_wuser(user_sensor_msg,1,user2));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_set_wdomain(user_sensor_msg,1,L"epo2003"));

	
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_user_sensor_msg_is_user_wide_char(NULL,0,&is_wchar));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_user_sensor_msg_is_user_wide_char(user_sensor_msg,1,NULL));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_OUTOFBOUNDS,ma_user_sensor_msg_is_user_wide_char(user_sensor_msg,2,&is_wchar));

	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_user_sensor_msg_is_domain_wide_char(NULL,0,&is_wchar));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_user_sensor_msg_is_domain_wide_char(user_sensor_msg,1,NULL));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_OUTOFBOUNDS,ma_user_sensor_msg_is_domain_wide_char(user_sensor_msg,5,&is_wchar));

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_release(user_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));

}

TEST(sensor_test, ma_system_sensor_msg_get_set_size_valid) {
    ma_sensor_msg_t *sensor_msg;
	ma_system_sensor_msg_t *system_sensor_msg;
	size_t no_of_message = 100;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_system_sensor_msg_create(&system_sensor_msg,0));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_system_sensor_msg(system_sensor_msg,&sensor_msg));
	
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_system_sensor_msg_get_size(system_sensor_msg,&no_of_message));
	TEST_ASSERT_EQUAL_INT(no_of_message,1);
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_system_sensor_msg_set_size(system_sensor_msg,1));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_system_sensor_msg_get_size(system_sensor_msg,&no_of_message));
	TEST_ASSERT_EQUAL_INT(no_of_message,1);
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_system_sensor_msg_set_size(system_sensor_msg,200));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_system_sensor_msg_get_size(system_sensor_msg,&no_of_message));
	TEST_ASSERT_EQUAL_INT(no_of_message,200);


    TEST_ASSERT_EQUAL_INT(MA_OK,ma_system_sensor_msg_release(system_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));

}

TEST(sensor_test, ma_system_sensor_msg_get_set_size_invalid) {
    ma_sensor_msg_t *sensor_msg;
	ma_system_sensor_msg_t *system_sensor_msg;
	size_t no_of_message = 100;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_system_sensor_msg_create(&system_sensor_msg,0));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_system_sensor_msg(system_sensor_msg,&sensor_msg));
	
	
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_system_sensor_msg_get_size(NULL,&no_of_message));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_system_sensor_msg_set_size(NULL,1));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_system_sensor_msg_get_size(system_sensor_msg,NULL));

    TEST_ASSERT_EQUAL_INT(MA_OK,ma_system_sensor_msg_release(system_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));

}

TEST(sensor_test, ma_system_sensor_msg_set_get_event_type_valid) {
    ma_sensor_msg_t *sensor_msg;
	ma_system_sensor_msg_t *system_sensor_msg;
	ma_sensor_event_type_t type = MA_USER_SENSOR_LOG_ON_EVENT;
	ma_sensor_event_type_t type_test;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_system_sensor_msg_create(&system_sensor_msg,1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_system_sensor_msg(system_sensor_msg,&sensor_msg));
	
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_system_sensor_msg_get_event_type(system_sensor_msg,&type_test));
	TEST_ASSERT_EQUAL_INT(type_test,MA_SYSTEM_SENSOR_EVENT_STOP);

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_system_sensor_msg_set_event_type(system_sensor_msg,type));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_system_sensor_msg_get_event_type(system_sensor_msg,&type_test));
	TEST_ASSERT_EQUAL_INT(type_test,MA_USER_SENSOR_LOG_ON_EVENT);

	type = MA_SYSTEM_SENSOR_EVENT_SHUTDOWN;
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_system_sensor_msg_set_event_type(system_sensor_msg,type));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_system_sensor_msg_get_event_type(system_sensor_msg,&type_test));
	TEST_ASSERT_EQUAL_INT(type_test,MA_SYSTEM_SENSOR_EVENT_SHUTDOWN);
		
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_system_sensor_msg_release(system_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));

}

TEST(sensor_test, ma_system_sensor_msg_set_get_event_type_invalid) {
    ma_sensor_msg_t *sensor_msg;
	ma_system_sensor_msg_t *system_sensor_msg;
	ma_sensor_event_type_t type = MA_USER_SENSOR_LOG_ON_EVENT;
	ma_sensor_event_type_t type_test;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_system_sensor_msg_create(&system_sensor_msg,1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_system_sensor_msg(system_sensor_msg,&sensor_msg));
		
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_system_sensor_msg_get_event_type(system_sensor_msg,NULL));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_system_sensor_msg_set_event_type(NULL,type));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_system_sensor_msg_get_event_type(NULL,&type_test));
			
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_system_sensor_msg_release(system_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));

}


TEST(sensor_test, ma_network_sensor_msg_get_set_size_valid) {
    ma_sensor_msg_t *sensor_msg;
	ma_network_sensor_msg_t *network_sensor_msg;
	size_t no_of_message = 100;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_msg_create(&network_sensor_msg,0));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_network_sensor_msg(network_sensor_msg,&sensor_msg));
	
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_network_sensor_msg_get_size(network_sensor_msg,&no_of_message));
	TEST_ASSERT_EQUAL_INT(no_of_message,1);
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_network_sensor_msg_set_size(network_sensor_msg,1));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_network_sensor_msg_get_size(network_sensor_msg,&no_of_message));
	TEST_ASSERT_EQUAL_INT(no_of_message,1);
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_network_sensor_msg_set_size(network_sensor_msg,200));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_network_sensor_msg_get_size(network_sensor_msg,&no_of_message));
	TEST_ASSERT_EQUAL_INT(no_of_message,200);


    TEST_ASSERT_EQUAL_INT(MA_OK,ma_network_sensor_msg_release(network_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));

}

TEST(sensor_test, ma_network_sensor_msg_get_set_size_invalid) {
    ma_sensor_msg_t *sensor_msg;
	ma_network_sensor_msg_t *network_sensor_msg;
	size_t no_of_message = 100;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_msg_create(&network_sensor_msg,0));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_network_sensor_msg(network_sensor_msg,&sensor_msg));
	
	
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_network_sensor_msg_get_size(NULL,&no_of_message));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_network_sensor_msg_set_size(NULL,1));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_network_sensor_msg_get_size(network_sensor_msg,NULL));

    TEST_ASSERT_EQUAL_INT(MA_OK,ma_network_sensor_msg_release(network_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));

}


TEST(sensor_test, ma_network_sensor_msg_set_get_event_type_valid) {
    ma_sensor_msg_t *sensor_msg;
	ma_network_sensor_msg_t *network_sensor_msg;
	ma_sensor_event_type_t type = MA_USER_SENSOR_LOG_ON_EVENT;
	ma_sensor_event_type_t type_test;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_msg_create(&network_sensor_msg,1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_network_sensor_msg(network_sensor_msg,&sensor_msg));
	
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_network_sensor_msg_get_event_type(network_sensor_msg,&type_test));
	TEST_ASSERT_EQUAL_INT(type_test,MA_SYSTEM_SENSOR_EVENT_STOP);

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_network_sensor_msg_set_event_type(network_sensor_msg,type));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_network_sensor_msg_get_event_type(network_sensor_msg,&type_test));
	TEST_ASSERT_EQUAL_INT(type_test,MA_USER_SENSOR_LOG_ON_EVENT);

	type = MA_SYSTEM_SENSOR_EVENT_SHUTDOWN;
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_network_sensor_msg_set_event_type(network_sensor_msg,type));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_network_sensor_msg_get_event_type(network_sensor_msg,&type_test));
	TEST_ASSERT_EQUAL_INT(type_test,MA_SYSTEM_SENSOR_EVENT_SHUTDOWN);
		
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_network_sensor_msg_release(network_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));

}

TEST(sensor_test, ma_network_sensor_msg_set_get_event_type_invalid) {
    ma_sensor_msg_t *sensor_msg;
	ma_network_sensor_msg_t *network_sensor_msg;
	ma_sensor_event_type_t type = MA_USER_SENSOR_LOG_ON_EVENT;
	ma_sensor_event_type_t type_test;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_msg_create(&network_sensor_msg,1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_network_sensor_msg(network_sensor_msg,&sensor_msg));
		
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_network_sensor_msg_get_event_type(network_sensor_msg,NULL));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_network_sensor_msg_set_event_type(NULL,type));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_network_sensor_msg_get_event_type(NULL,&type_test));
			
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_network_sensor_msg_release(network_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));

}


TEST(sensor_test, ma_idle_sensor_msg_get_set_size_valid) {
    ma_sensor_msg_t *sensor_msg;
	ma_idle_sensor_msg_t *idle_sensor_msg;
	size_t no_of_message = 100;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_idle_sensor_msg_create(&idle_sensor_msg,0));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_idle_sensor_msg(idle_sensor_msg,&sensor_msg));
	
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_idle_sensor_msg_get_size(idle_sensor_msg,&no_of_message));
	TEST_ASSERT_EQUAL_INT(no_of_message,1);
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_idle_sensor_msg_set_size(idle_sensor_msg,1));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_idle_sensor_msg_get_size(idle_sensor_msg,&no_of_message));
	TEST_ASSERT_EQUAL_INT(no_of_message,1);
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_idle_sensor_msg_set_size(idle_sensor_msg,200));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_idle_sensor_msg_get_size(idle_sensor_msg,&no_of_message));
	TEST_ASSERT_EQUAL_INT(no_of_message,200);


    TEST_ASSERT_EQUAL_INT(MA_OK,ma_idle_sensor_msg_release(idle_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));

}

TEST(sensor_test, ma_idle_sensor_msg_get_set_size_invalid) {
    ma_sensor_msg_t *sensor_msg;
	ma_idle_sensor_msg_t *idle_sensor_msg;
	size_t no_of_message = 100;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_idle_sensor_msg_create(&idle_sensor_msg,0));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_idle_sensor_msg(idle_sensor_msg,&sensor_msg));
	
	
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_idle_sensor_msg_get_size(NULL,&no_of_message));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_idle_sensor_msg_set_size(NULL,1));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_idle_sensor_msg_get_size(idle_sensor_msg,NULL));

    TEST_ASSERT_EQUAL_INT(MA_OK,ma_idle_sensor_msg_release(idle_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));

}


TEST(sensor_test, ma_idle_sensor_msg_set_get_event_type_valid) {
    ma_sensor_msg_t *sensor_msg;
	ma_idle_sensor_msg_t *idle_sensor_msg;
	ma_sensor_event_type_t type = MA_USER_SENSOR_LOG_ON_EVENT;
	ma_sensor_event_type_t type_test;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_idle_sensor_msg_create(&idle_sensor_msg,1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_idle_sensor_msg(idle_sensor_msg,&sensor_msg));
	
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_idle_sensor_msg_get_event_type(idle_sensor_msg,&type_test));
	TEST_ASSERT_EQUAL_INT(type_test,MA_SYSTEM_SENSOR_EVENT_STOP);

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_idle_sensor_msg_set_event_type(idle_sensor_msg,type));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_idle_sensor_msg_get_event_type(idle_sensor_msg,&type_test));
	TEST_ASSERT_EQUAL_INT(type_test,MA_USER_SENSOR_LOG_ON_EVENT);

	type = MA_SYSTEM_SENSOR_EVENT_SHUTDOWN;
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_idle_sensor_msg_set_event_type(idle_sensor_msg,type));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_idle_sensor_msg_get_event_type(idle_sensor_msg,&type_test));
	TEST_ASSERT_EQUAL_INT(type_test,MA_SYSTEM_SENSOR_EVENT_SHUTDOWN);
		
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_idle_sensor_msg_release(idle_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));

}

TEST(sensor_test, ma_idle_sensor_msg_set_get_event_type_invalid) {
    ma_sensor_msg_t *sensor_msg;
	ma_idle_sensor_msg_t *idle_sensor_msg;
	ma_sensor_event_type_t type = MA_USER_SENSOR_LOG_ON_EVENT;
	ma_sensor_event_type_t type_test;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_idle_sensor_msg_create(&idle_sensor_msg,1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_idle_sensor_msg(idle_sensor_msg,&sensor_msg));
		
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_idle_sensor_msg_get_event_type(idle_sensor_msg,NULL));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_idle_sensor_msg_set_event_type(NULL,type));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_idle_sensor_msg_get_event_type(NULL,&type_test));
			
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_idle_sensor_msg_release(idle_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));

}


TEST(sensor_test, ma_power_sensor_msg_get_set_size_valid) {
    ma_sensor_msg_t *sensor_msg;
	ma_power_sensor_msg_t *power_sensor_msg;
	size_t no_of_message = 100;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_msg_create(&power_sensor_msg,0));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_power_sensor_msg(power_sensor_msg,&sensor_msg));
	
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_get_size(power_sensor_msg,&no_of_message));
	TEST_ASSERT_EQUAL_INT(no_of_message,1);
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_set_size(power_sensor_msg,1));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_get_size(power_sensor_msg,&no_of_message));
	TEST_ASSERT_EQUAL_INT(no_of_message,1);
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_set_size(power_sensor_msg,200));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_get_size(power_sensor_msg,&no_of_message));
	TEST_ASSERT_EQUAL_INT(no_of_message,200);


    TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_release(power_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));

}

TEST(sensor_test, ma_power_sensor_msg_get_set_size_invalid) {
    ma_sensor_msg_t *sensor_msg;
	ma_power_sensor_msg_t *power_sensor_msg;
	size_t no_of_message = 100;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_msg_create(&power_sensor_msg,0));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_power_sensor_msg(power_sensor_msg,&sensor_msg));
	
	
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_power_sensor_msg_get_size(NULL,&no_of_message));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_power_sensor_msg_set_size(NULL,1));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_power_sensor_msg_get_size(power_sensor_msg,NULL));

    TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_release(power_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));

}


TEST(sensor_test, ma_power_sensor_msg_set_get_event_type_valid) {
    ma_sensor_msg_t *sensor_msg;
	ma_power_sensor_msg_t *power_sensor_msg;
	ma_sensor_event_type_t type = MA_POWER_SENSOR_STATUS_CHANGE_EVENT;
	ma_sensor_event_type_t type_test;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_msg_create(&power_sensor_msg,1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_power_sensor_msg(power_sensor_msg,&sensor_msg));
	
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_get_event_type(power_sensor_msg,&type_test));
	TEST_ASSERT_EQUAL_INT(type_test,MA_SYSTEM_SENSOR_EVENT_STOP);

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_set_event_type(power_sensor_msg,type));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_get_event_type(power_sensor_msg,&type_test));
	TEST_ASSERT_EQUAL_INT(type_test,MA_POWER_SENSOR_STATUS_CHANGE_EVENT);

	type = MA_POWER_SENSOR_BATTERY_LOW_EVENT;
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_set_event_type(power_sensor_msg,type));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_get_event_type(power_sensor_msg,&type_test));
	TEST_ASSERT_EQUAL_INT(type_test,MA_POWER_SENSOR_BATTERY_LOW_EVENT);
		
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_release(power_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));

}

TEST(sensor_test, ma_power_sensor_msg_set_get_event_type_invalid) {
    ma_sensor_msg_t *sensor_msg;
	ma_power_sensor_msg_t *power_sensor_msg;
	ma_sensor_event_type_t type = MA_USER_SENSOR_LOG_ON_EVENT;
	ma_sensor_event_type_t type_test;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_msg_create(&power_sensor_msg,1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_power_sensor_msg(power_sensor_msg,&sensor_msg));
		
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_power_sensor_msg_get_event_type(power_sensor_msg,NULL));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_power_sensor_msg_set_event_type(NULL,type));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_power_sensor_msg_get_event_type(NULL,&type_test));
			
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_release(power_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));

}

TEST(sensor_test, ma_power_sensor_msg_set_get_power_status_valid) {
    ma_sensor_msg_t *sensor_msg;
	ma_power_sensor_msg_t *power_sensor_msg;
	ma_power_status_t power_status = MA_POWER_STATUS_OFFLINE;
	ma_power_status_t power_status_test;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_msg_create(&power_sensor_msg,2));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_power_sensor_msg(power_sensor_msg,&sensor_msg));
	
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_set_power_status(power_sensor_msg,0,power_status));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_get_power_status(power_sensor_msg,0,&power_status_test));
	TEST_ASSERT_EQUAL_INT(power_status,power_status_test);

	power_status = MA_POWER_STATUS_ONLINE;
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_set_power_status(power_sensor_msg,0,power_status));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_get_power_status(power_sensor_msg,0,&power_status_test));
	TEST_ASSERT_EQUAL_INT(power_status,power_status_test);

	power_status = MA_POWER_STATUS_ONLINE;
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_set_power_status(power_sensor_msg,1,power_status));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_get_power_status(power_sensor_msg,1,&power_status_test));
	TEST_ASSERT_EQUAL_INT(power_status,power_status_test);

    TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_release(power_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));

}

TEST(sensor_test, ma_power_sensor_msg_set_get_power_status_invalid) {
    
	ma_sensor_msg_t *sensor_msg;
	ma_power_sensor_msg_t *power_sensor_msg;
	ma_power_status_t power_status = MA_POWER_STATUS_OFFLINE;
	ma_power_status_t power_status_test;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_msg_create(&power_sensor_msg,2));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_power_sensor_msg(power_sensor_msg,&sensor_msg));
	
	
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_power_sensor_msg_set_power_status(NULL,0,power_status));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_OUTOFBOUNDS,ma_power_sensor_msg_set_power_status(power_sensor_msg,3,power_status));

	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_power_sensor_msg_get_power_status(NULL,0,&power_status_test));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_power_sensor_msg_get_power_status(power_sensor_msg,0,NULL));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_OUTOFBOUNDS,ma_power_sensor_msg_get_power_status(power_sensor_msg,2,&power_status_test));
		
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_release(power_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));

}

TEST(sensor_test, ma_power_sensor_msg_set_battery_charge_status_valid) {
    ma_sensor_msg_t *sensor_msg;
	ma_power_sensor_msg_t *power_sensor_msg;
	ma_battery_charge_status_t battery_status = MA_BATTERY_CHARGE_STATUS_HIGH;
	ma_battery_charge_status_t battery_status_test;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_msg_create(&power_sensor_msg,2));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_power_sensor_msg(power_sensor_msg,&sensor_msg));
	
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_set_battery_charge_status(power_sensor_msg,0,battery_status));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_get_batter_charge_status(power_sensor_msg,0,&battery_status_test));
	TEST_ASSERT_EQUAL_INT(battery_status,battery_status_test);

	battery_status = MA_BATTERY_CHARGE_STATUS_LOW;
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_set_battery_charge_status(power_sensor_msg,0,battery_status));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_get_batter_charge_status(power_sensor_msg,0,&battery_status_test));
	TEST_ASSERT_EQUAL_INT(battery_status,battery_status_test);

	battery_status = MA_BATTERY_CHARGE_STATUS_CRITICAL;
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_set_battery_charge_status(power_sensor_msg,0,battery_status));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_get_batter_charge_status(power_sensor_msg,0,&battery_status_test));
	TEST_ASSERT_EQUAL_INT(battery_status,battery_status_test);

	battery_status = MA_BATTERY_CHARGE_STATUS_CHARGING;
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_set_battery_charge_status(power_sensor_msg,0,battery_status));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_get_batter_charge_status(power_sensor_msg,0,&battery_status_test));
	TEST_ASSERT_EQUAL_INT(battery_status,battery_status_test);

	battery_status = MA_BATTERY_CHARGE_STATUS_NO_BATTERY;
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_set_battery_charge_status(power_sensor_msg,0,battery_status));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_get_batter_charge_status(power_sensor_msg,0,&battery_status_test));
	TEST_ASSERT_EQUAL_INT(battery_status,battery_status_test);
	
	battery_status = MA_BATTERY_CHARGE_STATUS_UNKNOWN;
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_set_battery_charge_status(power_sensor_msg,0,battery_status));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_get_batter_charge_status(power_sensor_msg,0,&battery_status_test));
	TEST_ASSERT_EQUAL_INT(battery_status,battery_status_test);

    TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_release(power_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));

}

TEST(sensor_test, ma_power_sensor_msg_set_battery_charge_status_invalid) {
    ma_sensor_msg_t *sensor_msg;
	ma_power_sensor_msg_t *power_sensor_msg;
	ma_battery_charge_status_t battery_status = MA_BATTERY_CHARGE_STATUS_HIGH;
	ma_battery_charge_status_t battery_status_test;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_msg_create(&power_sensor_msg,2));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_power_sensor_msg(power_sensor_msg,&sensor_msg));
	
	
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_power_sensor_msg_set_battery_charge_status(NULL,0,battery_status));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_OUTOFBOUNDS,ma_power_sensor_msg_set_battery_charge_status(power_sensor_msg,2,battery_status));


	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_power_sensor_msg_get_batter_charge_status(NULL,0,&battery_status_test));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_power_sensor_msg_get_batter_charge_status(power_sensor_msg,0,NULL));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_OUTOFBOUNDS,ma_power_sensor_msg_get_batter_charge_status(power_sensor_msg,2,&battery_status_test));

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_release(power_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));

}


TEST(sensor_test, ma_power_sensor_msg_set_get_battery_lifetime_valid) {
    ma_sensor_msg_t *sensor_msg;
	ma_power_sensor_msg_t *power_sensor_msg;
	int  battery_life = 10*60*60;
	int battery_life_test;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_msg_create(&power_sensor_msg,2));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_power_sensor_msg(power_sensor_msg,&sensor_msg));
	
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_set_battery_lifetime(power_sensor_msg,0,battery_life));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_get_battery_lifetime(power_sensor_msg,0,&battery_life_test));
	TEST_ASSERT_EQUAL_INT(battery_life,battery_life_test);

	battery_life = 20*60*60;
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_set_battery_lifetime(power_sensor_msg,1,battery_life));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_get_battery_lifetime(power_sensor_msg,1,&battery_life_test));
	TEST_ASSERT_EQUAL_INT(battery_life,battery_life_test);

	
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_release(power_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));

}

TEST(sensor_test, ma_power_sensor_msg_set_get_battery_lifetime_invalid) {
    ma_sensor_msg_t *sensor_msg;
	ma_power_sensor_msg_t *power_sensor_msg;
	int  battery_life = 10*60*60;
	int battery_life_test;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_msg_create(&power_sensor_msg,2));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_power_sensor_msg(power_sensor_msg,&sensor_msg));
	
	
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_power_sensor_msg_set_battery_lifetime(NULL,0,battery_life));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_OUTOFBOUNDS,ma_power_sensor_msg_set_battery_lifetime(power_sensor_msg,3,battery_life));

	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_power_sensor_msg_get_battery_lifetime(power_sensor_msg,1,NULL));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_power_sensor_msg_get_battery_lifetime(NULL,1,&battery_life_test));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_OUTOFBOUNDS,ma_power_sensor_msg_get_battery_lifetime(power_sensor_msg,2,&battery_life_test));
		
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_release(power_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));

}

TEST(sensor_test, ma_power_sensor_msg_set_get_battery_percent_valid) {
    ma_sensor_msg_t *sensor_msg;
	ma_power_sensor_msg_t *power_sensor_msg;
	int  battery_percent = 20;
	int battery_percent_test;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_msg_create(&power_sensor_msg,2));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_power_sensor_msg(power_sensor_msg,&sensor_msg));
	
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_set_battery_percent(power_sensor_msg,0,battery_percent));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_get_battery_percent(power_sensor_msg,0,&battery_percent_test));
	TEST_ASSERT_EQUAL_INT(battery_percent,battery_percent_test);

	battery_percent = 90;
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_set_battery_percent(power_sensor_msg,1,battery_percent));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_get_battery_percent(power_sensor_msg,1,&battery_percent_test));
	TEST_ASSERT_EQUAL_INT(battery_percent,battery_percent_test);

	
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_release(power_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));

}

TEST(sensor_test, ma_power_sensor_msg_set_get_battery_percent_invalid) {
    ma_sensor_msg_t *sensor_msg;
	ma_power_sensor_msg_t *power_sensor_msg;
	int  battery_percent = 10*60*60;
	int battery_percent_test;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_msg_create(&power_sensor_msg,2));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_sensor_msg_create_from_power_sensor_msg(power_sensor_msg,&sensor_msg));
	
	
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_power_sensor_msg_set_battery_percent(NULL,0,battery_percent));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_OUTOFBOUNDS,ma_power_sensor_msg_set_battery_percent(power_sensor_msg,3,battery_percent));

	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_power_sensor_msg_get_battery_percent(power_sensor_msg,1,NULL));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_power_sensor_msg_get_battery_percent(NULL,1,&battery_percent_test));
	TEST_ASSERT_EQUAL_INT(MA_ERROR_OUTOFBOUNDS,ma_power_sensor_msg_get_battery_percent(power_sensor_msg,2,&battery_percent_test));
		
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_release(power_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_sensor_msg_release(sensor_msg));

}

TEST(sensor_test, convert_user_sensor_msg_to_variant_valid) {
	ma_user_sensor_msg_t *user_sensor_msg;
	ma_user_sensor_msg_t *user_sensor_msg_test=NULL;
	ma_variant_t *var_test=NULL;
	char *domain1 = "nai.corp.org";
	char *domain2 = "epo2003";
	char *user1 = "sheetal";
	char *user2 = "hulloli";

	ma_buffer_t *domain1_buff=NULL;
	ma_buffer_t *domain2_buff=NULL;
	char *domain1_test = NULL;
	char *domain2_test = NULL;
	size_t len = 0;
	ma_buffer_t *user1_buff=NULL;
	ma_buffer_t *user2_buff=NULL;
	char *user1_test = NULL;
	char *user2_test = NULL;

	ma_uint32_t s_id1= 1001;
	ma_uint32_t s_id2 = 2002;
	ma_uint32_t ts_id1= 0;
	ma_uint32_t ts_id2 = 0;

	ma_user_logged_state_t logged_status = MA_USER_LOGGED_STATE_ON;
	ma_user_logged_state_t t_logged_status = MA_USER_LOGGED_STATE_OFF;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_create(&user_sensor_msg,2));
		
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_set_domain(user_sensor_msg,0,domain1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_set_domain(user_sensor_msg,1,domain2));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_set_user(user_sensor_msg,0,user1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_set_user(user_sensor_msg,1,user2));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_set_session_id(user_sensor_msg,0,s_id1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_set_session_id(user_sensor_msg,1,s_id2));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_set_logon_state(user_sensor_msg,logged_status));
	
	TEST_ASSERT_EQUAL_INT(MA_OK,convert_user_sensor_msg_to_variant(user_sensor_msg,&var_test));
	TEST_ASSERT_EQUAL_INT(MA_OK,convert_variant_to_user_sensor_msg(var_test,&user_sensor_msg_test));

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_get_domain(user_sensor_msg_test,0,&domain1_buff));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_get_domain(user_sensor_msg_test,1,&domain2_buff));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_buffer_get_string(domain1_buff,&domain1_test,&len));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_buffer_get_string(domain2_buff,&domain2_test,&len));
	TEST_ASSERT_EQUAL_INT(MA_OK,strcmp(domain1_test,domain1));
	TEST_ASSERT_EQUAL_INT(MA_OK,strcmp(domain2_test,domain2));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_buffer_release(domain1_buff));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_buffer_release(domain2_buff));

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_get_user(user_sensor_msg_test,0,&user1_buff));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_get_user(user_sensor_msg_test,1,&user2_buff));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_buffer_get_string(user1_buff,&user1_test,&len));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_buffer_get_string(user2_buff,&user2_test,&len));
	TEST_ASSERT_EQUAL_INT(MA_OK,strcmp(user1_test,user1));
	TEST_ASSERT_EQUAL_INT(MA_OK,strcmp(user2_test,user2));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_buffer_release(user1_buff));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_buffer_release(user2_buff));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_get_session_id(user_sensor_msg_test,0,&ts_id1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_get_session_id(user_sensor_msg_test,1,&ts_id2));
	TEST_ASSERT_EQUAL_INT(ts_id1,s_id1);
	TEST_ASSERT_EQUAL_INT(ts_id2,s_id2);

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_get_logon_state(user_sensor_msg_test,&t_logged_status));
	TEST_ASSERT_EQUAL_INT(t_logged_status,t_logged_status);

    TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_release(user_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_release(user_sensor_msg_test));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_variant_release(var_test));
		
}



TEST(sensor_test, convert_user_sensor_msg_to_variant_valid1) {
	ma_user_sensor_msg_t *user_sensor_msg;
	ma_user_sensor_msg_t *user_sensor_msg_test=NULL;
	ma_variant_t *var_test=NULL;
	wchar_t *domain1 = L"nai.corp.org";
	wchar_t *domain2 = L"epo2003";
	wchar_t *user1 = L"sheetal";
	wchar_t *user2 = L"hulloli";

	ma_wbuffer_t *domain1_buff=NULL;
	ma_wbuffer_t *domain2_buff=NULL;
	wchar_t *domain1_test = NULL;
	wchar_t *domain2_test = NULL;
	size_t len = 0;
	ma_wbuffer_t *user1_buff=NULL;
	ma_wbuffer_t *user2_buff=NULL;
	wchar_t *user1_test = NULL;
	wchar_t *user2_test = NULL;

	ma_uint32_t s_id1= 1001;
	ma_uint32_t s_id2 = 2002;
	ma_uint32_t ts_id1= 0;
	ma_uint32_t ts_id2 = 0;

	ma_user_logged_state_t logged_status = MA_USER_LOGGED_STATE_ON;
	ma_user_logged_state_t t_logged_status = MA_USER_LOGGED_STATE_OFF;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_create(&user_sensor_msg,2));
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_set_wdomain(user_sensor_msg,0,domain1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_set_wdomain(user_sensor_msg,1,domain2));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_set_wuser(user_sensor_msg,0,user1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_set_wuser(user_sensor_msg,1,user2));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_set_session_id(user_sensor_msg,0,s_id1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_set_session_id(user_sensor_msg,1,s_id2));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_set_logon_state(user_sensor_msg,logged_status));
	
	TEST_ASSERT_EQUAL_INT(MA_OK,convert_user_sensor_msg_to_variant(user_sensor_msg,&var_test));
	TEST_ASSERT_EQUAL_INT(MA_OK,convert_variant_to_user_sensor_msg(var_test,&user_sensor_msg_test));

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_get_wdomain(user_sensor_msg_test,0,&domain1_buff));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_get_wdomain(user_sensor_msg_test,1,&domain2_buff));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_wbuffer_get_string(domain1_buff,&domain1_test,&len));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_wbuffer_get_string(domain2_buff,&domain2_test,&len));
	TEST_ASSERT_EQUAL_INT(MA_OK,wcscmp(domain1_test,domain1));
	TEST_ASSERT_EQUAL_INT(MA_OK,wcscmp(domain2_test,domain2));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_wbuffer_release(domain1_buff));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_wbuffer_release(domain2_buff));

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_get_wuser(user_sensor_msg_test,0,&user1_buff));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_get_wuser(user_sensor_msg_test,1,&user2_buff));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_wbuffer_get_string(user1_buff,&user1_test,&len));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_wbuffer_get_string(user2_buff,&user2_test,&len));
	TEST_ASSERT_EQUAL_INT(MA_OK,wcscmp(user1_test,user1));
	TEST_ASSERT_EQUAL_INT(MA_OK,wcscmp(user2_test,user2));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_wbuffer_release(user1_buff));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_wbuffer_release(user2_buff));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_get_session_id(user_sensor_msg_test,0,&ts_id1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_get_session_id(user_sensor_msg_test,1,&ts_id2));
	TEST_ASSERT_EQUAL_INT(ts_id1,s_id1);
	TEST_ASSERT_EQUAL_INT(ts_id2,s_id2);

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_user_sensor_msg_get_logon_state(user_sensor_msg_test,&t_logged_status));
	TEST_ASSERT_EQUAL_INT(t_logged_status,t_logged_status);

    TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_release(user_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_user_sensor_msg_release(user_sensor_msg_test));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_variant_release(var_test));
	
}

TEST(sensor_test, convert_power_sensor_msg_to_variant_valid) {
	ma_power_sensor_msg_t *power_sensor_msg;
	ma_power_sensor_msg_t *power_sensor_msg_test=NULL;
	ma_variant_t *var_test=NULL;
	int  battery_life = 10*60*60;
	int  battery_life2 = 20*60*60;
	int battery_life_test;
	int battery_life2_test;
	
	ma_sensor_event_type_t type = MA_POWER_SENSOR_STATUS_CHANGE_EVENT;
	ma_sensor_event_type_t type_test;
		
	ma_power_status_t power_status = MA_POWER_STATUS_ONLINE;
	ma_power_status_t power_status_test;
	ma_power_status_t power_status2 = MA_POWER_STATUS_OFFLINE;
	ma_power_status_t power_status2_test;
	
	ma_battery_charge_status_t battery_status = MA_BATTERY_CHARGE_STATUS_HIGH;
	ma_battery_charge_status_t battery_status_test;
	ma_battery_charge_status_t battery_status2 = MA_BATTERY_CHARGE_STATUS_HIGH;
	ma_battery_charge_status_t battery_status2_test;
	
	
	int  battery_percent = 20;
	int battery_percent_test;
	int  battery_percent2 = 40;
	int battery_percent2_test;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_power_sensor_msg_create(&power_sensor_msg,2));
		
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_set_battery_lifetime(power_sensor_msg,0,battery_life));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_set_battery_lifetime(power_sensor_msg,1,battery_life2));
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_set_event_type(power_sensor_msg,type));
	
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_set_power_status(power_sensor_msg,0,power_status));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_set_power_status(power_sensor_msg,1,power_status2));
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_set_battery_charge_status(power_sensor_msg,0,battery_status));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_set_battery_charge_status(power_sensor_msg,1,battery_status2));
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_set_battery_percent(power_sensor_msg,0,battery_percent));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_set_battery_percent(power_sensor_msg,1,battery_percent2));
	
	TEST_ASSERT_EQUAL_INT(MA_OK,convert_power_sensor_msg_to_variant(power_sensor_msg,&var_test));
	TEST_ASSERT_EQUAL_INT(MA_OK,convert_variant_to_power_sensor_msg(var_test,&power_sensor_msg_test));
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_get_battery_lifetime(power_sensor_msg_test,0,&battery_life_test));
	TEST_ASSERT_EQUAL_INT(battery_life,battery_life_test);
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_get_battery_lifetime(power_sensor_msg_test,1,&battery_life2_test));
	TEST_ASSERT_EQUAL_INT(battery_life2,battery_life2_test);
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_get_event_type(power_sensor_msg_test,&type_test));
	TEST_ASSERT_EQUAL_INT(type_test,type);
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_get_power_status(power_sensor_msg_test,0,&power_status_test));
	TEST_ASSERT_EQUAL_INT(power_status,power_status_test);
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_get_power_status(power_sensor_msg_test,1,&power_status2_test));
	TEST_ASSERT_EQUAL_INT(power_status2,power_status2_test);

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_get_batter_charge_status(power_sensor_msg_test,0,&battery_status_test));
	TEST_ASSERT_EQUAL_INT(battery_status,battery_status_test);
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_get_batter_charge_status(power_sensor_msg_test,1,&battery_status2_test));
	TEST_ASSERT_EQUAL_INT(battery_status2,battery_status2_test);
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_get_battery_percent(power_sensor_msg_test,0,&battery_percent_test));
	TEST_ASSERT_EQUAL_INT(battery_percent,battery_percent_test);
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_get_battery_percent(power_sensor_msg_test,1,&battery_percent2_test));
	TEST_ASSERT_EQUAL_INT(battery_percent2,battery_percent2_test);
	
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_release(power_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_power_sensor_msg_release(power_sensor_msg_test));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_variant_release(var_test));
	
		
}

TEST(sensor_test, convert_network_sensor_msg_to_variant_valid) {
	ma_network_sensor_msg_t *network_sensor_msg;
	ma_network_sensor_msg_t *network_sensor_msg_test=NULL;
	ma_sensor_event_type_t type = MA_NETWORK_SENSOR_CONNECTIVITY_STATUS_CHANGE_EVENT;
	ma_sensor_event_type_t type_test;
	ma_network_connectivity_status_t status = MA_NETWORK_CONNECTIVITY_STATUS_CONNECTED;
	ma_network_connectivity_status_t status_test;
	ma_variant_t *var_test=NULL;
		

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_network_sensor_msg_create(&network_sensor_msg,2));
		
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_network_sensor_msg_set_event_type(network_sensor_msg,type));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_network_sensor_msg_set_connectivity_status(network_sensor_msg,status));
	
	TEST_ASSERT_EQUAL_INT(MA_OK,convert_network_sensor_msg_to_variant(network_sensor_msg,&var_test));
	TEST_ASSERT_EQUAL_INT(MA_OK,convert_variant_to_network_sensor_msg(var_test,&network_sensor_msg_test));
	
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_network_sensor_msg_get_event_type(network_sensor_msg,&type_test));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_network_sensor_msg_get_connectivity_status(network_sensor_msg,&status_test));
	
	TEST_ASSERT_EQUAL_INT(type_test,type);
	TEST_ASSERT_EQUAL_INT(status_test,status);
	
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_network_sensor_msg_release(network_sensor_msg));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_network_sensor_msg_release(network_sensor_msg_test));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_variant_release(var_test));
	
}

static void run_all_unit_tests(void) {
do {RUN_TEST_GROUP(sensor_test_group); } while (0);
do {RUN_TEST_GROUP(sensor_functional_test_group); } while (0);
}


static void run_all_function_tests(){
    
}

int main(int argc, char *argv[]) {
    int i =	0;
    int group = 0;
	int ret = -1;
#ifdef MA_WINDOWS
    MA_TRACKLEAKS;
#endif
	do_registry_setup();
    if(argc == 1)
        ret =  UnityMain(argc, argv, run_all_unit_tests);
    else {
        for(i =0;i<argc;i++)
            if(!strcmp("-g",argv[i])){
                group = 1;
                break;
            }
            if(group)
                ret = UnityMain(argc, argv, run_all_function_tests);
            else
                ret = UnityMain(argc, argv, run_all_unit_tests);
    }
	do_registry_cleanup();
	return ret;
}
