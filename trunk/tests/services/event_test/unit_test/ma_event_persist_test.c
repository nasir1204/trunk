/*****************************************************************************
Event Service - Event Persist Unit Tests
******************************************************************************/
#define UNITY_FIXTURE_MALLOC_OVERRIDES_H_ 1

#include "unity.h"
#include "unity_fixture.h"
#include "src/services/event/ma_event_persist.h"
#include "src/services/event/ma_event_xml_list.h"
#include "ma_ut_setup.h"
#include <string.h>

TEST_GROUP_RUNNER(ma_event_persist_test_group)
{
    do {RUN_TEST_CASE(ma_event_persist_tests, test_for_event_persistence)} while (0);
}


/******************************************************************************
Test Setup -
Create an ut setup
******************************************************************************/
static ma_ut_setup_t *g_ut_setup = NULL;
static ma_context_t *g_ma_context = NULL;
static ma_ds_t *g_ds = NULL;

TEST_SETUP(ma_event_persist_tests)
{
    ma_ut_setup_create("event_service.event_persist.tests", "ma.service.events.test", &g_ut_setup);
    g_ma_context = ma_ut_setup_get_context(g_ut_setup);
    g_ds = ma_ut_setup_get_datastore(g_ut_setup);
}


/******************************************************************************
Test Cleanup -
1) Release ut setup.
******************************************************************************/
TEST_TEAR_DOWN(ma_event_persist_tests)
{
    ma_ut_setup_release(g_ut_setup);
}

/*
<BeaconEvents>
<MachineInfo>
<MachineName>VARSHNEY-PC</MachineName>
<AgentGUID>{a2652f84-a361-11e3-1761-d4bed910ead6}</AgentGUID>
<IPAddress>10.213.254.1</IPAddress>
<OSName>Windows 7</OSName>
<UserName>SYSTEM</UserName>
<TimeZoneBias>-330</TimeZoneBias>
<RawMACAddress>d4bed910ead6</RawMACAddress>
</MachineInfo>
<SampleSoftware ProductName="Sample Point Product" ProductVersion="1.0.0" ProductFamily="Secure" AdditionalSoftwareValue="test">
<SampleEvent SpecialValue="test">
<EventID>100088</EventID>
<Severity>0</Severity>
<GMTTime>2014-03-04T09:15:24</GMTTime>
<Text>test</Text>
<MoreEventData>st��ff</MoreEventData>
</SampleEvent>
<AdditionalSoftwareNode>
example
<AddedSubnodeOfNode>a value</AddedSubnodeOfNode>
</AdditionalSoftwareNode>
</SampleSoftware>
</BeaconEvents>
*/

const char my_event_1[] = "\x3c\x42\x65\x61\x63\x6f\x6e\x45\x76\x65\x6e\x74\x73\x3e\x0d\x0a\x3c\x4d\x61\x63\x68\x69\x6e\x65\x49\x6e\x66\x6f\x3e\x0d\x0a\x3c\x4d\x61\x63\x68\x69\x6e\x65\x4e\x61\x6d\x65\x3e\x56\x41\x52\x53\x48\x4e\x45\x59\x2d\x50\x43\x3c\x2f\x4d\x61\x63\x68\x69\x6e\x65\x4e\x61\x6d\x65\x3e\x0d\x0a\x3c\x41\x67\x65\x6e\x74\x47\x55\x49\x44\x3e\x7b\x61\x32\x36\x35\x32\x66\x38\x34\x2d\x61\x33\x36\x31\x2d\x31\x31\x65\x33\x2d\x31\x37\x36\x31\x2d\x64\x34\x62\x65\x64\x39\x31\x30\x65\x61\x64\x36\x7d\x3c\x2f\x41\x67\x65\x6e\x74\x47\x55\x49\x44\x3e\x0d\x0a\x3c\x49\x50\x41\x64\x64\x72\x65\x73\x73\x3e\x31\x30\x2e\x32\x31\x33\x2e\x32\x35\x34\x2e\x31\x3c\x2f\x49\x50\x41\x64\x64\x72\x65\x73\x73\x3e\x0d\x0a\x3c\x4f\x53\x4e\x61\x6d\x65\x3e\x57\x69\x6e\x64\x6f\x77\x73\x20\x37\x3c\x2f\x4f\x53\x4e\x61\x6d\x65\x3e\x0d\x0a\x3c\x55\x73\x65\x72\x4e\x61\x6d\x65\x3e\x53\x59\x53\x54\x45\x4d\x3c\x2f\x55\x73\x65\x72\x4e\x61\x6d\x65\x3e\x0d\x0a\x3c\x54\x69\x6d\x65\x5a\x6f\x6e\x65\x42\x69\x61\x73\x3e\x2d\x33\x33\x30\x3c\x2f\x54\x69\x6d\x65\x5a\x6f\x6e\x65\x42\x69\x61\x73\x3e\x0d\x0a\x3c\x52\x61\x77\x4d\x41\x43\x41\x64\x64\x72\x65\x73\x73\x3e\x64\x34\x62\x65\x64\x39\x31\x30\x65\x61\x64\x36\x3c\x2f\x52\x61\x77\x4d\x41\x43\x41\x64\x64\x72\x65\x73\x73\x3e\x0d\x0a\x3c\x2f\x4d\x61\x63\x68\x69\x6e\x65\x49\x6e\x66\x6f\x3e\x0d\x0a\x3c\x53\x61\x6d\x70\x6c\x65\x53\x6f\x66\x74\x77\x61\x72\x65\x20\x50\x72\x6f\x64\x75\x63\x74\x4e\x61\x6d\x65\x3d\x22\x53\x61\x6d\x70\x6c\x65\x20\x50\x6f\x69\x6e\x74\x20\x50\x72\x6f\x64\x75\x63\x74\x22\x20\x50\x72\x6f\x64\x75\x63\x74\x56\x65\x72\x73\x69\x6f\x6e\x3d\x22\x31\x2e\x30\x2e\x30\x22\x20\x50\x72\x6f\x64\x75\x63\x74\x46\x61\x6d\x69\x6c\x79\x3d\x22\x53\x65\x63\x75\x72\x65\x22\x20\x41\x64\x64\x69\x74\x69\x6f\x6e\x61\x6c\x53\x6f\x66\x74\x77\x61\x72\x65\x56\x61\x6c\x75\x65\x3d\x22\x74\x65\x73\x74\x22\x3e\x0d\x0a\x3c\x53\x61\x6d\x70\x6c\x65\x45\x76\x65\x6e\x74\x20\x53\x70\x65\x63\x69\x61\x6c\x56\x61\x6c\x75\x65\x3d\x22\x74\x65\x73\x74\x22\x3e\x0d\x0a\x3c\x45\x76\x65\x6e\x74\x49\x44\x3e\x31\x30\x30\x30\x38\x38\x3c\x2f\x45\x76\x65\x6e\x74\x49\x44\x3e\x0d\x0a\x3c\x53\x65\x76\x65\x72\x69\x74\x79\x3e\x30\x3c\x2f\x53\x65\x76\x65\x72\x69\x74\x79\x3e\x0d\x0a\x3c\x47\x4d\x54\x54\x69\x6d\x65\x3e\x32\x30\x31\x34\x2d\x30\x33\x2d\x30\x34\x54\x30\x39\x3a\x31\x35\x3a\x32\x34\x3c\x2f\x47\x4d\x54\x54\x69\x6d\x65\x3e\x0d\x0a\x3c\x54\x65\x78\x74\x3e\x74\x65\x73\x74\x3c\x2f\x54\x65\x78\x74\x3e\x0d\x0a\x3c\x4d\x6f\x72\x65\x45\x76\x65\x6e\x74\x44\x61\x74\x61\x3e\x73\x74\xc3\xbc\xc3\xab\x66\x66\x3c\x2f\x4d\x6f\x72\x65\x45\x76\x65\x6e\x74\x44\x61\x74\x61\x3e\x0d\x0a\x3c\x2f\x53\x61\x6d\x70\x6c\x65\x45\x76\x65\x6e\x74\x3e\x0d\x0a\x3c\x41\x64\x64\x69\x74\x69\x6f\x6e\x61\x6c\x53\x6f\x66\x74\x77\x61\x72\x65\x4e\x6f\x64\x65\x3e\x0d\x0a\x65\x78\x61\x6d\x70\x6c\x65\x0d\x0a\x3c\x41\x64\x64\x65\x64\x53\x75\x62\x6e\x6f\x64\x65\x4f\x66\x4e\x6f\x64\x65\x3e\x61\x20\x76\x61\x6c\x75\x65\x3c\x2f\x41\x64\x64\x65\x64\x53\x75\x62\x6e\x6f\x64\x65\x4f\x66\x4e\x6f\x64\x65\x3e\x0d\x0a\x3c\x2f\x41\x64\x64\x69\x74\x69\x6f\x6e\x61\x6c\x53\x6f\x66\x74\x77\x61\x72\x65\x4e\x6f\x64\x65\x3e\x0d\x0a\x3c\x2f\x53\x61\x6d\x70\x6c\x65\x53\x6f\x66\x74\x77\x61\x72\x65\x3e\x0d\x0a\x3c\x2f\x42\x65\x61\x63\x6f\x6e\x45\x76\x65\x6e\x74\x73\x3e";

/*
<BeaconEvents>
<MachineInfo>
<MachineName>ANURAG-PC</MachineName>
<AgentGUID>{a2652f84-a361-11e3-2345-d4bed91sddd6}</AgentGUID>
<IPAddress>10.213.254.3</IPAddress>
<OSName>Windows 8</OSName>
<UserName>ADMIN</UserName>
<TimeZoneBias>-330</TimeZoneBias>
<RawMACAddress>d4bed910ead6</RawMACAddress>
</MachineInfo>
<SampleSoftware ProductName="Sample Point Product" ProductVersion="2.0.0" ProductFamily="Antivirus" AdditionalSoftwareValue="test">
<SampleEvent SpecialValue="test">
<EventID>100090</EventID>
<Severity>1</Severity>
<GMTTime>2014-05-04T09:15:24</GMTTime>
<Text>test</Text>
<MoreEventData>st��ff</MoreEventData>
</SampleEvent>
<AdditionalSoftwareNode>
example
<AddedSubnodeOfNode>a value</AddedSubnodeOfNode>
</AdditionalSoftwareNode>
</SampleSoftware>
</BeaconEvents>
*/
const char my_event_2[] = "\x3c\x42\x65\x61\x63\x6f\x6e\x45\x76\x65\x6e\x74\x73\x3e\x0d\x0a\x3c\x4d\x61\x63\x68\x69\x6e\x65\x49\x6e\x66\x6f\x3e\x0d\x0a\x3c\x4d\x61\x63\x68\x69\x6e\x65\x4e\x61\x6d\x65\x3e\x41\x4e\x55\x52\x41\x47\x2d\x50\x43\x3c\x2f\x4d\x61\x63\x68\x69\x6e\x65\x4e\x61\x6d\x65\x3e\x0d\x0a\x3c\x41\x67\x65\x6e\x74\x47\x55\x49\x44\x3e\x7b\x61\x32\x36\x35\x32\x66\x38\x34\x2d\x61\x33\x36\x31\x2d\x31\x31\x65\x33\x2d\x32\x33\x34\x35\x2d\x64\x34\x62\x65\x64\x39\x31\x73\x64\x64\x64\x36\x7d\x3c\x2f\x41\x67\x65\x6e\x74\x47\x55\x49\x44\x3e\x0d\x0a\x3c\x49\x50\x41\x64\x64\x72\x65\x73\x73\x3e\x31\x30\x2e\x32\x31\x33\x2e\x32\x35\x34\x2e\x33\x3c\x2f\x49\x50\x41\x64\x64\x72\x65\x73\x73\x3e\x0d\x0a\x3c\x4f\x53\x4e\x61\x6d\x65\x3e\x57\x69\x6e\x64\x6f\x77\x73\x20\x38\x3c\x2f\x4f\x53\x4e\x61\x6d\x65\x3e\x0d\x0a\x3c\x55\x73\x65\x72\x4e\x61\x6d\x65\x3e\x41\x44\x4d\x49\x4e\x3c\x2f\x55\x73\x65\x72\x4e\x61\x6d\x65\x3e\x0d\x0a\x3c\x54\x69\x6d\x65\x5a\x6f\x6e\x65\x42\x69\x61\x73\x3e\x2d\x33\x33\x30\x3c\x2f\x54\x69\x6d\x65\x5a\x6f\x6e\x65\x42\x69\x61\x73\x3e\x0d\x0a\x3c\x52\x61\x77\x4d\x41\x43\x41\x64\x64\x72\x65\x73\x73\x3e\x64\x34\x62\x65\x64\x39\x31\x30\x65\x61\x64\x36\x3c\x2f\x52\x61\x77\x4d\x41\x43\x41\x64\x64\x72\x65\x73\x73\x3e\x0d\x0a\x3c\x2f\x4d\x61\x63\x68\x69\x6e\x65\x49\x6e\x66\x6f\x3e\x0d\x0a\x3c\x53\x61\x6d\x70\x6c\x65\x53\x6f\x66\x74\x77\x61\x72\x65\x20\x50\x72\x6f\x64\x75\x63\x74\x4e\x61\x6d\x65\x3d\x22\x53\x61\x6d\x70\x6c\x65\x20\x50\x6f\x69\x6e\x74\x20\x50\x72\x6f\x64\x75\x63\x74\x22\x20\x50\x72\x6f\x64\x75\x63\x74\x56\x65\x72\x73\x69\x6f\x6e\x3d\x22\x32\x2e\x30\x2e\x30\x22\x20\x50\x72\x6f\x64\x75\x63\x74\x46\x61\x6d\x69\x6c\x79\x3d\x22\x41\x6e\x74\x69\x76\x69\x72\x75\x73\x22\x20\x41\x64\x64\x69\x74\x69\x6f\x6e\x61\x6c\x53\x6f\x66\x74\x77\x61\x72\x65\x56\x61\x6c\x75\x65\x3d\x22\x74\x65\x73\x74\x22\x3e\x0d\x0a\x3c\x53\x61\x6d\x70\x6c\x65\x45\x76\x65\x6e\x74\x20\x53\x70\x65\x63\x69\x61\x6c\x56\x61\x6c\x75\x65\x3d\x22\x74\x65\x73\x74\x22\x3e\x0d\x0a\x3c\x45\x76\x65\x6e\x74\x49\x44\x3e\x31\x30\x30\x30\x39\x30\x3c\x2f\x45\x76\x65\x6e\x74\x49\x44\x3e\x0d\x0a\x3c\x53\x65\x76\x65\x72\x69\x74\x79\x3e\x31\x3c\x2f\x53\x65\x76\x65\x72\x69\x74\x79\x3e\x0d\x0a\x3c\x47\x4d\x54\x54\x69\x6d\x65\x3e\x32\x30\x31\x34\x2d\x30\x35\x2d\x30\x34\x54\x30\x39\x3a\x31\x35\x3a\x32\x34\x3c\x2f\x47\x4d\x54\x54\x69\x6d\x65\x3e\x0d\x0a\x3c\x54\x65\x78\x74\x3e\x74\x65\x73\x74\x3c\x2f\x54\x65\x78\x74\x3e\x0d\x0a\x3c\x4d\x6f\x72\x65\x45\x76\x65\x6e\x74\x44\x61\x74\x61\x3e\x73\x74\xc3\xbc\xc3\xab\x66\x66\x3c\x2f\x4d\x6f\x72\x65\x45\x76\x65\x6e\x74\x44\x61\x74\x61\x3e\x0d\x0a\x3c\x2f\x53\x61\x6d\x70\x6c\x65\x45\x76\x65\x6e\x74\x3e\x0d\x0a\x3c\x41\x64\x64\x69\x74\x69\x6f\x6e\x61\x6c\x53\x6f\x66\x74\x77\x61\x72\x65\x4e\x6f\x64\x65\x3e\x0d\x0a\x65\x78\x61\x6d\x70\x6c\x65\x0d\x0a\x3c\x41\x64\x64\x65\x64\x53\x75\x62\x6e\x6f\x64\x65\x4f\x66\x4e\x6f\x64\x65\x3e\x61\x20\x76\x61\x6c\x75\x65\x3c\x2f\x41\x64\x64\x65\x64\x53\x75\x62\x6e\x6f\x64\x65\x4f\x66\x4e\x6f\x64\x65\x3e\x0d\x0a\x3c\x2f\x41\x64\x64\x69\x74\x69\x6f\x6e\x61\x6c\x53\x6f\x66\x74\x77\x61\x72\x65\x4e\x6f\x64\x65\x3e\x0d\x0a\x3c\x2f\x53\x61\x6d\x70\x6c\x65\x53\x6f\x66\x74\x77\x61\x72\x65\x3e\x0d\x0a\x3c\x2f\x42\x65\x61\x63\x6f\x6e\x45\x76\x65\x6e\x74\x73\x3e";

TEST(ma_event_persist_tests, test_for_event_persistence)
{
    ma_event_persist_t *event_ds = NULL;
    ma_event_xml_list_t *event_xml_list = NULL;

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_persist_create(NULL, &event_ds));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_persist_create(g_ma_context, NULL));
    TEST_ASSERT(MA_OK == ma_event_persist_create(g_ma_context, &event_ds));

    /**************************************************************************
    TEST 1 - Setting and retrieving Disabled Event List
    **************************************************************************/
    {
        ma_variant_t *my_event_list = NULL;
        ma_variant_t *my_event = NULL;
        ma_array_t *my_array = NULL;
        size_t size = 0;
        ma_uint32_t value = 0;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_persist_set_disabled_event_list(NULL, "1024; 1056; 1; 2345; 56789;"));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_persist_set_disabled_event_list(event_ds, NULL));
        TEST_ASSERT(MA_OK == ma_event_persist_set_disabled_event_list(event_ds, "1024; 1056; 1; 2345; 56789;"));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_persist_get_disabled_event_list(NULL, &my_event_list));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_persist_get_disabled_event_list(event_ds, NULL));
        TEST_ASSERT(MA_OK == ma_event_persist_get_disabled_event_list(event_ds, &my_event_list));

        TEST_ASSERT(MA_OK == ma_variant_get_array(my_event_list, &my_array));
        TEST_ASSERT(MA_OK == ma_variant_release(my_event_list));

        TEST_ASSERT(MA_OK == ma_array_size(my_array, &size));
        TEST_ASSERT(5 == size);

        TEST_ASSERT(MA_OK == ma_array_get_element_at(my_array, 1, &my_event));
        TEST_ASSERT(MA_OK == ma_variant_get_uint32(my_event, &value));
        TEST_ASSERT(value == 1024);
        TEST_ASSERT(MA_OK == ma_variant_release(my_event));

        TEST_ASSERT(MA_OK == ma_array_get_element_at(my_array, 2, &my_event));
        TEST_ASSERT(MA_OK == ma_variant_get_uint32(my_event, &value));
        TEST_ASSERT(value == 1056);
        TEST_ASSERT(MA_OK == ma_variant_release(my_event));

        TEST_ASSERT(MA_OK == ma_array_get_element_at(my_array, 3, &my_event));
        TEST_ASSERT(MA_OK == ma_variant_get_uint32(my_event, &value));
        TEST_ASSERT(value == 1);
        TEST_ASSERT(MA_OK == ma_variant_release(my_event));

        TEST_ASSERT(MA_OK == ma_array_get_element_at(my_array, 4, &my_event));
        TEST_ASSERT(MA_OK == ma_variant_get_uint32(my_event, &value));
        TEST_ASSERT(value == 2345);
        TEST_ASSERT(MA_OK == ma_variant_release(my_event));

        TEST_ASSERT(MA_OK == ma_array_get_element_at(my_array, 5, &my_event));
        TEST_ASSERT(MA_OK == ma_variant_get_uint32(my_event, &value));
        TEST_ASSERT(value == 56789);
        TEST_ASSERT(MA_OK == ma_variant_release(my_event));

        TEST_ASSERT(MA_OK == ma_array_release(my_array));
    }

    /**************************************************************************
    TEST 2 - Set/Refresh/Retrieve Event Filter Version
    **************************************************************************/
    {
        char *my_filter_version = NULL;
        size_t size = 0;

        // Calling refresh before setting any Event Filter Version in Datastore
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_persist_refresh_event_filter_version(NULL));
        TEST_ASSERT(MA_ERROR_DS_KEY_NOT_FOUND == ma_event_persist_refresh_event_filter_version(event_ds));

        // Calling Get before setting any Event Filter Version in Datastore
        TEST_ASSERT(MA_ERROR_DS_KEY_NOT_FOUND == ma_event_persist_get_event_filter_version(event_ds, &my_filter_version, &size));

        // Setting Event Filter Version
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_persist_set_event_filter_version(NULL, "5.0.1.234"));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_persist_set_event_filter_version(event_ds, NULL));
        TEST_ASSERT(MA_OK == ma_event_persist_set_event_filter_version(event_ds, "5.0.1.234"));

        // Retrieving and Verifying Event Filter Version
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_persist_get_event_filter_version(NULL, &my_filter_version, &size));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_persist_get_event_filter_version(event_ds, NULL, &size));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_persist_get_event_filter_version(event_ds, &my_filter_version, NULL));
        TEST_ASSERT(MA_OK == ma_event_persist_get_event_filter_version(event_ds, &my_filter_version, &size));
        TEST_ASSERT(0 == strcmp("5.0.1.234", my_filter_version));

        // Setting New Event Filter Version
        TEST_ASSERT(MA_OK == ma_event_persist_set_event_filter_version(event_ds, "5.1.1.1128"));

        // Retrieving and Verifying Event Filter Version
        TEST_ASSERT(MA_OK == ma_event_persist_get_event_filter_version(event_ds, &my_filter_version, &size));
        TEST_ASSERT(0 == strcmp("5.1.1.1128", my_filter_version));

        // Refreshing and Verifying Event Filter Version Again
        TEST_ASSERT(MA_OK == ma_event_persist_refresh_event_filter_version(event_ds));
        TEST_ASSERT(MA_OK == ma_event_persist_get_event_filter_version(event_ds, &my_filter_version, &size));
        TEST_ASSERT(0 == strcmp("5.1.1.1128", my_filter_version));
    }

    /**************************************************************************
    TEST 3 - "Is Event Exist" test
    **************************************************************************/

    // "Is Event Exist" before adding any Event
    {
        ma_bool_t exist_or_not = MA_TRUE;

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_persist_is_event_exist(NULL, MA_EVENT_PRIORITY_NORMAL, &exist_or_not));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_persist_is_event_exist(event_ds, MA_EVENT_PRIORITY_NORMAL, NULL));

        // Path will only be created, when, first element is added
        TEST_ASSERT(MA_ERROR_DS_PATH_NOT_FOUND == ma_event_persist_is_event_exist(event_ds, MA_EVENT_PRIORITY_NORMAL, &exist_or_not));
        TEST_ASSERT(MA_FALSE == exist_or_not);

        TEST_ASSERT(MA_ERROR_DS_PATH_NOT_FOUND == ma_event_persist_is_event_exist(event_ds, MA_EVENT_PRIORITY_UNDECIDED, &exist_or_not));
        TEST_ASSERT(MA_FALSE == exist_or_not);

        TEST_ASSERT(MA_ERROR_DS_PATH_NOT_FOUND == ma_event_persist_is_event_exist(event_ds, MA_EVENT_PRIORITY_IMMEDIATE, &exist_or_not));
        TEST_ASSERT(MA_FALSE == exist_or_not);
    }

    // Add a "Normal" priority event
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_persist_add_event(NULL, MA_EVENT_PRIORITY_NORMAL, (const unsigned char *)my_event_1, strlen(my_event_1)));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_persist_add_event(event_ds, MA_EVENT_PRIORITY_NORMAL, NULL, strlen(my_event_1)));
    TEST_ASSERT(MA_OK == ma_event_persist_add_event(event_ds, MA_EVENT_PRIORITY_NORMAL, (const unsigned char *)my_event_1, strlen(my_event_1)));

    // "Is Event Exist" after adding one event
    {
        ma_bool_t exist_or_not = MA_TRUE;

        // Path will only be created, when, first element is added. Normal and undecided event are stored at same path
        TEST_ASSERT(MA_OK == ma_event_persist_is_event_exist(event_ds, MA_EVENT_PRIORITY_NORMAL, &exist_or_not));
        TEST_ASSERT(MA_TRUE == exist_or_not);

        TEST_ASSERT(MA_OK == ma_event_persist_is_event_exist(event_ds, MA_EVENT_PRIORITY_UNDECIDED, &exist_or_not));
        TEST_ASSERT(MA_TRUE == exist_or_not);

        TEST_ASSERT(MA_ERROR_DS_PATH_NOT_FOUND == ma_event_persist_is_event_exist(event_ds, MA_EVENT_PRIORITY_IMMEDIATE, &exist_or_not));
        TEST_ASSERT(MA_FALSE == exist_or_not);
    }

    // Add a "Immediate" priority event
    TEST_ASSERT(MA_OK == ma_event_persist_add_event(event_ds, MA_EVENT_PRIORITY_IMMEDIATE, (const unsigned char *)my_event_2, strlen(my_event_2)));

    // "Is Event Exist" after adding the event
    {
        ma_bool_t exist_or_not = MA_FALSE;

        // Path will only be created, when, first element is added. Normal and undecided event are stored at same path
        TEST_ASSERT(MA_OK == ma_event_persist_is_event_exist(event_ds, MA_EVENT_PRIORITY_NORMAL, &exist_or_not));
        TEST_ASSERT(MA_TRUE == exist_or_not);

        TEST_ASSERT(MA_OK == ma_event_persist_is_event_exist(event_ds, MA_EVENT_PRIORITY_UNDECIDED, &exist_or_not));
        TEST_ASSERT(MA_TRUE == exist_or_not);

        TEST_ASSERT(MA_OK == ma_event_persist_is_event_exist(event_ds, MA_EVENT_PRIORITY_IMMEDIATE, &exist_or_not));
        TEST_ASSERT(MA_TRUE == exist_or_not);
    }

    /**************************************************************************
    TEST 4 - Retrieve Event XML List
    **************************************************************************/
    // Creating an Event XML List
    TEST_ASSERT_NOT_NULL(event_xml_list = ma_event_xml_list_create());

    // Retrieve XML List (should retrieve both normal and immediate priority events)
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_persist_get_event_xml_list(NULL, MA_EVENT_PRIORITY_UNDECIDED, 3, event_xml_list));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_persist_get_event_xml_list(event_ds, MA_EVENT_PRIORITY_UNDECIDED, 3, NULL));
    TEST_ASSERT(MA_OK == ma_event_persist_get_event_xml_list(event_ds, MA_EVENT_PRIORITY_UNDECIDED, 3, event_xml_list));

    // Remove XML List
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_persist_remove_event_xml(NULL, event_xml_list));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_persist_remove_event_xml(event_ds, NULL));
    TEST_ASSERT(MA_OK == ma_event_persist_remove_event_xml(event_ds, event_xml_list));

    // After removing events, the "Is Event Exist" should again return False
    {
        ma_bool_t exist_or_not = MA_FALSE;

        // Path will only be created, when, first element is added. Normal and undecided event are stored at same path
        TEST_ASSERT(MA_OK == ma_event_persist_is_event_exist(event_ds, MA_EVENT_PRIORITY_NORMAL, &exist_or_not));
        TEST_ASSERT(MA_FALSE == exist_or_not);

        TEST_ASSERT(MA_OK == ma_event_persist_is_event_exist(event_ds, MA_EVENT_PRIORITY_UNDECIDED, &exist_or_not));
        TEST_ASSERT(MA_FALSE == exist_or_not);

        TEST_ASSERT(MA_OK == ma_event_persist_is_event_exist(event_ds, MA_EVENT_PRIORITY_IMMEDIATE, &exist_or_not));
        TEST_ASSERT(MA_FALSE == exist_or_not);
    }

    // event_xml_list release
    ma_event_xml_list_release(event_xml_list);

    /**************************************************************************
    TEST 5 - Adding Event by Name and removing Event
    **************************************************************************/

    // Add a "Normal" priority event
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_persist_add_event_by_name(NULL, "Event_1", MA_EVENT_PRIORITY_NORMAL, (const unsigned char *)my_event_1, strlen(my_event_1)));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_persist_add_event_by_name(event_ds, NULL, MA_EVENT_PRIORITY_NORMAL, (const unsigned char *)my_event_1, strlen(my_event_1)));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_persist_add_event_by_name(event_ds, "Event_1", MA_EVENT_PRIORITY_NORMAL, NULL, strlen(my_event_1)));
    TEST_ASSERT(MA_OK == ma_event_persist_add_event_by_name(event_ds, "Event_1", MA_EVENT_PRIORITY_NORMAL, (const unsigned char *)my_event_1, strlen(my_event_1)));

    // Add a "Immediate" priority event
    TEST_ASSERT(MA_OK == ma_event_persist_add_event_by_name(event_ds, "Event_2", MA_EVENT_PRIORITY_IMMEDIATE, (const unsigned char *)my_event_2, strlen(my_event_2)));

    // Remove Event
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_persist_remove_event(NULL, MA_EVENT_PRIORITY_IMMEDIATE, "Event_1"));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_persist_remove_event(event_ds, MA_EVENT_PRIORITY_IMMEDIATE, NULL));

    // Trying to remove an Event, where no event exist with given name and priority
    TEST_ASSERT(MA_OK == ma_event_persist_remove_event(event_ds, MA_EVENT_PRIORITY_IMMEDIATE, "Event_1"));

    // Verifying "Event_1" and "Event_2" still exist
    {
        ma_bool_t exist_or_not = MA_FALSE;

        TEST_ASSERT(MA_OK == ma_event_persist_is_event_exist(event_ds, MA_EVENT_PRIORITY_NORMAL, &exist_or_not));
        TEST_ASSERT(MA_TRUE == exist_or_not);

        TEST_ASSERT(MA_OK == ma_event_persist_is_event_exist(event_ds, MA_EVENT_PRIORITY_IMMEDIATE, &exist_or_not));
        TEST_ASSERT(MA_TRUE == exist_or_not);
    }

    // Trying to remove "Event_1", normal priority event
    TEST_ASSERT(MA_OK == ma_event_persist_remove_event(event_ds, MA_EVENT_PRIORITY_NORMAL, "Event_1"));

    // Verifying "Event_1" doesn't exist but "Event_2" still exists
    {
        ma_bool_t exist_or_not = MA_FALSE;

        TEST_ASSERT(MA_OK == ma_event_persist_is_event_exist(event_ds, MA_EVENT_PRIORITY_NORMAL, &exist_or_not));
        TEST_ASSERT(MA_FALSE == exist_or_not);

        TEST_ASSERT(MA_OK == ma_event_persist_is_event_exist(event_ds, MA_EVENT_PRIORITY_IMMEDIATE, &exist_or_not));
        TEST_ASSERT(MA_TRUE == exist_or_not);
    }

    // Trying to remove "Event_2", immediate priority event
    TEST_ASSERT(MA_OK == ma_event_persist_remove_event(event_ds, MA_EVENT_PRIORITY_IMMEDIATE, "Event_2"));

    // Verifying "Event_1" and "Event_2 doesn't exist now
    {
        ma_bool_t exist_or_not = MA_TRUE;

        TEST_ASSERT(MA_OK == ma_event_persist_is_event_exist(event_ds, MA_EVENT_PRIORITY_NORMAL, &exist_or_not));
        TEST_ASSERT(MA_FALSE == exist_or_not);

        TEST_ASSERT(MA_OK == ma_event_persist_is_event_exist(event_ds, MA_EVENT_PRIORITY_IMMEDIATE, &exist_or_not));
        TEST_ASSERT(MA_FALSE == exist_or_not);
    }

    /**************************************************************************
    TEST 6 - Event persist object release
    **************************************************************************/
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_event_persist_release(NULL));
    TEST_ASSERT(MA_OK == ma_event_persist_release(event_ds));
}






