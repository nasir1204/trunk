/*****************************************************************************
Event Service Unit Tests
******************************************************************************/
#include "unity.h"
#include "unity_fixture.h"
#include "ma/internal/defs/ma_event_defs.h"
#include "ma/internal/services/ahclient/ma_ah_client_service.h"
#include "ma/internal/services/event/ma_event_service.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/event/ma_event_bag.h"
#include "ma/ma_msgbus.h"
#include "ma/datastore/ma_ds_database.h"
#include "ma/logger/ma_console_logger.h"
#include "ma_ut_setup.h"
#include <string.h>
#include <uv.h>
void timer_event(uv_timer_t *handle, int status);

TEST_GROUP_RUNNER(ma_event_service_test_group)
{
    do {RUN_TEST_CASE(ma_event_service_tests, test_for_event_service)} while (0);
}


/******************************************************************************
Test Setup -
Create an ut setup
******************************************************************************/
extern ma_error_t ma_service_ut_context_create(const char *product_id, ma_bool_t thread_pool_enabled, ma_context_t **context);

ma_context_t *g_ma_context = NULL;
ma_client_t *g_ma_client = NULL;
ma_ut_setup_t *g_ut_setup = NULL;
ma_ds_t *g_ds = NULL;

TEST_SETUP(ma_event_service_tests)
{   
    ma_ut_setup_create("event_client.event_service.tests", "ma.service.events.test", &g_ut_setup);
    g_ma_context = ma_ut_setup_get_context(g_ut_setup);
    g_ds = ma_ut_setup_get_datastore(g_ut_setup);
    g_ma_client = ma_ut_setup_get_client(g_ut_setup);
    
}


/******************************************************************************
Test Cleanup -
1) Release ut setup.
******************************************************************************/
TEST_TEAR_DOWN(ma_event_service_tests)
{
 ma_ut_setup_release(g_ut_setup);
}

/******************************************************************************
Unit test performs following functionality -
1) Creates and sends an Event Message consisting of the content in the following
   format on the message bus.
2) Creates a server to accept the message and the verifies that all fields are
   successfully parsed from the message.
3) Note that the server is configured for the OUTPROC communication, but this
   test, creates client and server within the same process (INRPROC) to verify
   the event client functionality.

<?xml version="1.0" encoding="UTF-8"?>
<EPOEvent>
    <MachineInfo />
    </MachineInfo>
    <SoftwareInfo ProductName="VirusScan Enterprise" ProductVersion="8.7.0" ProductFamily="TVD">
        <CommonFields>
            <Analyzer>VIRUSCAN8700</Analyzer>
            <AnalyzerName>VirusScan Enterprise</AnalyzerName>
            <AnalyzerVersion>8.7</AnalyzerVersion>
            <AnalyzerHostName>TEST_BOX</AnalyzerHostName>
            <AnalyzerDATVersion>5917.0000</AnalyzerDATVersion>
            <AnalyzerEngineVersion>5400.1158</AnalyzerEngineVersion>
        </CommonFields>
        <CustomFields target="MyProductCustomFields">
            <ProductFieldOne>Product_Field_Value_1</ProductFieldOne>
        </CustomFieldsFields>
        <Event>
            <EventID>1027</EventID>
            <Severity>4</Severity>
            <GMTTime>2010-05-14T16:35:04</GMTTime>
            <CommonFields>
                <AnalyzerDetectionMethod>OAS</AnalyzerDetectionMethod>
                <TargetFileName>C:\WINDOWS\SYSTEM32\NTOSKRNL.EXE<    argetFileName>
                <ThreatCategory>av.detect<    hreatCategory>
                <ThreatName>Generic!atr<    hreatName>
                <ThreatType>trojan<    hreatType>
                <ThreatActionTaken>deleted<    hreatActionTaken>
                <ThreatHandled>1<    hreatHandled>
            </CommonFields>
            <CustomFields target="MyProductEvents">
                <EventFieldFive>Value5</EventFieldFive>
                <EventFieldSix>Value6</EventFieldSix>
                <EventFieldSeven>Value7</EventFieldSeven>
                <EventFieldEight>Value8</EventFieldEight>
            </CustomFieldsFields>
        </Event>
        <Event>
            <EventID>2034</EventID>
            <Severity>3</Severity>
            <GMTTime>2013-05-15T16:09:07</GMTTime>
            <CommonFields>
                <AnalyzerDetectionMethod>XYZ</AnalyzerDetectionMethod>
                <TargetFileName>C:\WINDOWS\SYSTEM32\ABCDLL.EXE<    argetFileName>
                <ThreatCategory>av.detect<    hreatCategory>
                <ThreatName>Custom!atr<    hreatName>
                <ThreatType>virus<    hreatType>
                <ThreatActionTaken>quarantine<    hreatActionTaken>
                <ThreatHandled>0<    hreatHandled>
            </CommonFields>
            <CustomFields target="MyProductEvents">
                <EventFieldOne>Value1</EventFieldOne>
                <EventFieldTwo>Value2</EventFieldTwo>
                <EventFieldThree>Value3</EventFieldThree>
                <EventFieldFour>Value4</EventFieldFour>
            </CustomFieldsFields>
        </Event>
    </SoftwareInfo>
</EPOEvent>
******************************************************************************/

// Common and Custom fields in the above mentioned Event Message XML are represented
// in the structure below -
struct event_info
{
    char *field_name;
    char *field_value;
};

const struct event_info event_bag_common_fields[] =
      {
          { "Analyzer",              "VIRUSCAN8700"         },
          { "AnalyzerName",          "VirusScan Enterprise" },
          { "AnalyzerVersion",       "8.7"                  },
          { "AnalyzerHostName",      "TEST_BOX"             },
          { "AnalyzerDATVersion",    "5917.0000"            },
          { "AnalyzerEngineVersion", "5400.1158"            },
      };

const struct event_info event_bag_custom_fields[] =
      {
          { "ProductFieldOne",     "Product_Field_Value_1"  },
      };

const struct event_info event_1_common_fields[] =
      {
          { "AnalyzerDetectionMethod", "OAS" },
          { "TargetFileName",          "C:\\WINDOWS\\SYSTEM32\\NTOSKRNL.EXE" },
          { "ThreatCategory",          "av.detect" },
          { "ThreatName",              "Generic!atr" },
          { "ThreatType",              "trojan" },
          { "ThreatActionTaken",       "deleted" },
          { "ThreatHandled",           "1" },
      };

const struct event_info event_1_custom_fields[] =
      {
          { "EventFieldFive",  "Value5" },
          { "EventFieldSix",   "Value6" },
          { "EventFieldSeven", "Value7" },
          { "EventFieldEight", "Value8" },
      };

const struct event_info event_2_common_fields[] =
      {
          { "AnalyzerDetectionMethod", "XYZ" },
          { "TargetFileName",          "C:\\WINDOWS\\SYSTEM32\\ABCDLL.EXE" },
          { "ThreatCategory",          "av.detect" },
          { "ThreatName",              "Custom!atr" },
          { "ThreatType",              "virus" },
          { "ThreatActionTaken",       "quarantine" },
          { "ThreatHandled",           "0" },
      };

const struct event_info event_2_custom_fields[] =
      {
          { "EventFieldOne",   "Value1" },
          { "EventFieldTwo",   "Value2" },
          { "EventFieldThree", "Value3" },
          { "EventFieldFour",  "Value4" },
      };


/**************************************************************************
This is an event client implementation. When called, this will create an
event message and send it to event service.
**************************************************************************/
void event_client(ma_event_severity_t severity,
                  const char *product_name,
                  const char *product_version,
                  const char *product_family)
{
    // Object pointers
    ma_event_bag_t *event_bag_obj = NULL;
    ma_variant_t *variant_obj = NULL;
    ma_event_t *event_obj = NULL;

    // Loop iterator
    ma_uint8_t counter = 0;

    /**************************************************************************
    Creating event message
    **************************************************************************/
    ma_event_bag_create(g_ma_client, product_name, &event_bag_obj);

    /**************************************************************************
    Setting product version
    **************************************************************************/
    ma_event_bag_set_product_version(event_bag_obj, product_version);

    /**************************************************************************
    Setting product family
    **************************************************************************/
    ma_event_bag_set_product_family(event_bag_obj, product_family);

    /**************************************************************************
    Setting event message priority
    **************************************************************************/
    //ma_event_bag_set_priority(event_bag_obj, event_bag_priority);

    /**************************************************************************
    Adding Event Message Common Fields
    **************************************************************************/
    for(counter = 0; counter < sizeof(event_bag_common_fields)/sizeof(event_bag_common_fields[0]); counter++)
    {
        ma_variant_create_from_string(event_bag_common_fields[counter].field_value,
                                      strlen(event_bag_common_fields[counter].field_value),
                                      &variant_obj);

        ma_event_bag_add_common_field(event_bag_obj, event_bag_common_fields[counter].field_name, variant_obj);

       (void) ma_variant_release(variant_obj);
    }

    /**************************************************************************
    Adding Event Message Custom Fields Table
    **************************************************************************/
    ma_event_bag_set_custom_fields_table(event_bag_obj, "MyProductCustomFields");

    /**************************************************************************
    Adding Event Message Custom Fields
    **************************************************************************/
    for(counter = 0; counter < sizeof(event_bag_custom_fields)/sizeof(event_bag_custom_fields[0]); counter++)
    {
        ma_variant_create_from_string(event_bag_custom_fields[counter].field_value,
                                      strlen(event_bag_custom_fields[counter].field_value),
                                      &variant_obj);

        ma_event_bag_add_custom_field(event_bag_obj, event_bag_custom_fields[counter].field_name, variant_obj);

        (void)ma_variant_release(variant_obj);
    }

    /**************************************************************************
    Creating Event 1
    **************************************************************************/
    ma_event_create(g_ma_client, 1027, severity, &event_obj);

    /**************************************************************************
    Adding common fields for Event 1
    **************************************************************************/
    for(counter = 0; counter < sizeof(event_1_common_fields)/sizeof(event_1_common_fields[0]); counter++)
    {
        ma_variant_create_from_string(event_1_common_fields[counter].field_value,
                                      strlen(event_1_common_fields[counter].field_value),
                                      &variant_obj);

        ma_event_add_common_field(event_obj, event_1_common_fields[counter].field_name, variant_obj);

        (void)ma_variant_release(variant_obj);
    }

    /**************************************************************************
    Adding Event 1 - Custom Fields Table
    **************************************************************************/
    ma_event_set_custom_fields_table(event_obj, "MyProductEvents");

    /**************************************************************************
    Adding Event 1 - Custom Fields
    **************************************************************************/
    for(counter = 0; counter < sizeof(event_1_custom_fields)/sizeof(event_1_custom_fields[0]); counter++)
    {
        ma_variant_create_from_string(event_1_custom_fields[counter].field_value,
                                      strlen(event_1_custom_fields[counter].field_value),
                                      &variant_obj);

        ma_event_add_custom_field(event_obj, event_1_custom_fields[counter].field_name, variant_obj);

        (void)ma_variant_release(variant_obj);
    }

    /**************************************************************************
    Adding Event 1 to message
    **************************************************************************/
    ma_event_bag_add_event(event_bag_obj, event_obj);

    /**************************************************************************
    Releasing Event 1 object after adding it to event message
    **************************************************************************/
    ma_event_release(event_obj);

    /**************************************************************************
    Creating Event 2
    **************************************************************************/
    ma_event_create(g_ma_client, 2034, severity, &event_obj);

    /**************************************************************************
    Adding common fields for Event 2
    **************************************************************************/
    for(counter = 0; counter < sizeof(event_2_common_fields)/sizeof(event_2_common_fields[0]); counter++)
    {
        ma_variant_create_from_string(event_2_common_fields[counter].field_value,
                                      strlen(event_2_common_fields[counter].field_value),
                                      &variant_obj);

        ma_event_add_common_field(event_obj, event_2_common_fields[counter].field_name, variant_obj);

        (void)ma_variant_release(variant_obj);
    }

    /**************************************************************************
    Adding Event 2 - Custom Fields Table
    **************************************************************************/
    ma_event_set_custom_fields_table(event_obj, "MyProductEvents");

    /**************************************************************************
    Adding Event 2 - Custom Fields
    **************************************************************************/
    for(counter = 0; counter < sizeof(event_2_custom_fields)/sizeof(event_2_custom_fields[0]); counter++)
    {
        ma_variant_create_from_string(event_2_custom_fields[counter].field_value,
                                      strlen(event_2_custom_fields[counter].field_value),
                                      &variant_obj);

        ma_event_add_custom_field(event_obj, event_2_custom_fields[counter].field_name, variant_obj);

        (void)ma_variant_release(variant_obj);
    }

    /**************************************************************************
    Adding Event 2 to message
    **************************************************************************/
    ma_event_bag_add_event(event_bag_obj, event_obj);

    /**************************************************************************
    Releasing Event 2 object after adding it to event message
    **************************************************************************/
    ma_event_release(event_obj);

    /**************************************************************************
    Sending the Event message to event service.
    **************************************************************************/
    TEST_ASSERT(ma_event_bag_send(event_bag_obj) == MA_OK);

    /**************************************************************************
    Releasing event message
    **************************************************************************/
    ma_event_bag_release(event_bag_obj);
}

typedef struct ut_event_forwarding_policy_s {
    const char *is_enabled;
	const char *event_batch_size;
	const char *trigger_interval;
	const char *event_trigger_threshold;
}ut_event_forwarding_policy_t;

static ma_error_t ma_set_event_forwarding_policy_callback(const char *section, const char *key, ma_buffer_t **value, void *cb_data) {
    ut_event_forwarding_policy_t *policy = (ut_event_forwarding_policy_t *)cb_data;
    ma_error_t rc = MA_OK;
    if(!strcmp(MA_EVENT_POLICIES_PATH_NAME_STR, section)) {
        ma_buffer_create(value, 10);
        if(!strcmp(MA_EVENT_KEY_IS_ENABLED_PRIORITY_FORWARD_INT, key)) 
            ma_buffer_set(*value, policy->is_enabled, strlen(policy->is_enabled));
        else if(!strcmp(MA_EVENT_KEY_UPLOAD_TIMEOUT_INT,key))
            ma_buffer_set(*value, policy->trigger_interval, strlen(policy->trigger_interval));
        else if(!strcmp(MA_EVENT_KEY_UPLOAD_THRESHOLD_INT,key))
            ma_buffer_set(*value, policy->event_batch_size, strlen(policy->event_batch_size));
        else if(!strcmp(MA_EVENT_KEY_PRIORITY_LEVEL_INT,key))
            ma_buffer_set(*value, policy->event_trigger_threshold, strlen(policy->event_trigger_threshold));
        else
            rc = MA_ERROR_OBJECTNOTFOUND;
    }
    return rc;
}

void timer_event(uv_timer_t *handle, int status)
{
    static int counter = 0;
    static  ma_service_t *event_service_obj = NULL;
    
    /**************************************************************************
    Creating Event Service.

    Note - All objects required for creating the event service are retrieved
    from the context.
    **************************************************************************/
    if(counter == 0)
    {
        TEST_ASSERT(ma_event_service_create(NULL, &event_service_obj) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_event_service_create(MA_EVENT_SERVICE_NAME_STR, NULL) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_event_service_create(MA_EVENT_SERVICE_NAME_STR, &event_service_obj) == MA_OK);
        TEST_ASSERT(ma_service_configure(event_service_obj, g_ma_context, MA_SERVICE_CONFIG_NEW) == MA_OK);
    
        /**************************************************************************
        Adding machine information in the MA DB, so that the event service can read
        this information while generating the Events.xml
        **************************************************************************/
        TEST_ASSERT(MA_OK == ma_ds_set_str(g_ds,
                                          "AGENT\\CONFIGURATION",
                                          "AGENT_GUID",
                                          "3klvhdfnvjcosfhnkdlscnsdilrhensklf",
                                          strlen("3klvhdfnvjcosfhnkdlscnsdilrhensklf")));

        TEST_ASSERT(MA_OK == ma_ds_set_str(g_ds,
                                          "AGENT\\CONFIGURATION",
                                          "AGENT_VERSION",
                                          "5.0.0.1",
                                          strlen("5.0.0.1")));

        /**************************************************************************
        Starting Event Service.
        **************************************************************************/
        TEST_ASSERT(ma_service_start(NULL) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_service_start(event_service_obj) == MA_OK);

        counter = 1;
    }

    // Creating an event client to send the events (in the form of variant) to this
    // Event Service.
    else if(counter == 1)
    {
        ut_event_forwarding_policy_t policy = {0};
        
        policy.is_enabled = "0";//MA_FALSE

        ma_ut_setup_set_policy_buffer_provider_callback(g_ut_setup, ma_set_event_forwarding_policy_callback, &policy);

        event_client(MA_EVENT_SEVERITY_INFORMATIONAL, "Virus Scan", "8.7.3", "McAfee"); // No forwarding policy, this event is treatee as "Normal Event"

        policy.is_enabled = "1";//MA_TRUE
        policy.event_batch_size = "5";
        policy.event_trigger_threshold = "3";//MA_EVENT_SEVERITY_MAJOR;
        policy.trigger_interval = "3";

        ma_ut_setup_set_policy_buffer_provider_callback(g_ut_setup, ma_set_event_forwarding_policy_callback, &policy);

        event_client(MA_EVENT_SEVERITY_CRITICAL,"Solidcore", "1.0.9.0", "Intel"); // Major event forwarding policy, this event is treated as "Immediate Event"
        event_client(MA_EVENT_SEVERITY_MINOR,"Updater", "1.0", "McAfee"); // Major event forwarding policy, this event is treated as "Normal Event"

        policy.is_enabled = "1";//MA_TRUE
        policy.event_batch_size = "5";
        policy.event_trigger_threshold = "2";//MA_EVENT_SEVERITY_MINOR
        policy.trigger_interval = "3";

        ma_ut_setup_set_policy_buffer_provider_callback(g_ut_setup, ma_set_event_forwarding_policy_callback, &policy);
        event_client(MA_EVENT_SEVERITY_MINOR,"My test service", "2.0", "McAfee"); // Major event forwarding policy, this event is treated as "Immediate Event"

        counter = 2;
    }

    // Verification of events -
    // The table is iterated and the Events XML are read as BLOB. The return value
    // should be MA_OK. The Event Message content has to be manually verified.
    else if(counter == 2)
    {
        ma_buffer_t *key_buffer = NULL;
        ma_buffer_t *value_buffer = NULL;
        char *key  = NULL;
        char *value  = NULL;
        size_t size = 0;
        int count = 0;
        ma_ds_iterator_t *ds_iterator = NULL;

        // Verifying Normal priority Events. Only two normal priority events should be there.
        (void)ma_ds_iterator_create(g_ds, 0, "Events", &ds_iterator);

        for(count = 0; count < 2; count++)
        {
            ma_ds_iterator_get_next(ds_iterator, &key_buffer);
            ma_buffer_get_string(key_buffer, &key, &size);

            printf("Key - %s \t Key Size - %d\n", key, size);

            TEST_ASSERT(ma_ds_get_blob(g_ds, "Events", key, &value_buffer) == MA_OK);

            (void)ma_buffer_get_raw(value_buffer, &value, &size);

            printf("Value - %s \t Value Size - %d\n", value, size);

            (void)ma_buffer_release(key_buffer);
            (void)ma_buffer_release(value_buffer);
            key_buffer = NULL;
            value_buffer = NULL;
        }

        TEST_ASSERT(MA_ERROR_NO_MORE_ITEMS == ma_ds_iterator_get_next(ds_iterator, &key_buffer));
        ma_ds_iterator_release(ds_iterator);

        // Verifying Immediate priority Events. Only two immediate priority events should be there.
        ma_ds_iterator_create(g_ds, 0, "Events\\Immediate", &ds_iterator);

        for(count = 0; count < 2; count++)
        {
            ma_ds_iterator_get_next(ds_iterator, &key_buffer);
            ma_buffer_get_string(key_buffer, &key, &size);

            printf("Key - %s \t Key Size - %d\n", key, size);

            TEST_ASSERT(ma_ds_get_blob(g_ds, "Events\\Immediate", key, &value_buffer) == MA_OK);

            (void)ma_buffer_get_raw(value_buffer, &value, &size);

            printf("Value - %s \t Value Size - %d\n", value, size);

            (void)ma_buffer_release(key_buffer);
            (void)ma_buffer_release(value_buffer);
            key_buffer = NULL;
            value_buffer = NULL;
        }

        TEST_ASSERT(MA_ERROR_NO_MORE_ITEMS == ma_ds_iterator_get_next(ds_iterator, &key_buffer));
        ma_ds_iterator_release(ds_iterator);

        counter = 3;

    }

    // Cleanup code
    else if(counter == 3)
    {

        
        /**************************************************************************
        Stopping Event Service.
        **************************************************************************/
        TEST_ASSERT(ma_service_stop(NULL) == MA_ERROR_INVALIDARG);
        TEST_ASSERT(ma_service_stop(event_service_obj) == MA_OK);

        /**************************************************************************
         Releasing Event Service
        **************************************************************************/
        ma_service_release(event_service_obj);

        counter = 7;
    }
}

struct control
{
    uv_timer_t exit_trigger;
    uv_timer_t timer;
    ma_event_loop_t *client_loop;
};

void timer_exit(uv_timer_t *handle, int status)
{
    struct control *ctl = (struct control*)handle->data;

    uv_timer_stop(&ctl->timer);
    uv_timer_stop(&ctl->exit_trigger);
    //ma_event_loop_stop(ctl->client_loop);
}

TEST(ma_event_service_tests, test_for_event_service)
{
    struct control  ctl;
    uv_loop_t *loop = NULL;

    ma_event_loop_create(&ctl.client_loop);
    TEST_ASSERT_NOT_NULL(ctl.client_loop);

    ma_event_loop_get_loop_impl(ctl.client_loop, &loop);

    //start the exit timer
    uv_timer_init(loop, &ctl.exit_trigger);
    ctl.exit_trigger.data = &ctl;
    uv_timer_start(&ctl.exit_trigger, timer_exit, 80000, 0);

    //start the eventing
    uv_timer_init(loop, &ctl.timer);
    uv_timer_start(&ctl.timer, timer_event, 5000, 10000);

    ma_event_loop_run(ctl.client_loop);
    ma_event_loop_release(ctl.client_loop);
}





