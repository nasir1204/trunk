#include "unity.h"
#include "unity_fixture.h"
#include "ma/ma_variant.h"
#include "ma/datastore/ma_ds.h"
#include "ma/datastore/ma_ds_database.h"
#include "ma/internal/services/property/property_xml_generator.h"
#include "xml_generator_utilities.h"
#include "ma/logger/ma_logger.h"

#include "ma_property_xml_generator_test_helper.h"

#include <string.h>
#include <stdlib.h>
extern ma_logger_t *g_console_logger;


#ifdef MA_WINDOWS
#else
#include <unistd.h>
#endif

#define LOCAL_DATABASE_NAME_FOR_UT    "l_datastoreut.db" 

#define GLOBAL_DATABASE_NAME_FOR_UT    "g_datastoreut.db" 


static ma_db_t *g_database_handle = NULL ;
static ma_ds_t *g_ds = NULL ;

extern ma_error_t ma_property_bag_get_table(ma_property_bag_t *bag, ma_table_t **table);

TEST_GROUP_RUNNER(property_xml_generator_test_group)
{
   do {RUN_TEST_CASE(property_xml_generator_test, test_scenario_prod_running_new_section_new_setting)} while (0);
   do {RUN_TEST_CASE(property_xml_generator_test, test_scenario_prod_running_same_section_new_setting)} while (0);
   do {RUN_TEST_CASE(property_xml_generator_test, test_scenario_prod_running_same_section_same_setting)} while (0);
   do {RUN_TEST_CASE(property_xml_generator_test, test_scenario_prod_running_same_section_diff_setting)} while (0);
   do {RUN_TEST_CASE(property_xml_generator_test, test_scenario_prod_running_same_section_del_setting)} while (0);
   do {RUN_TEST_CASE(property_xml_generator_test, test_scenario_prod_running_deleted_section_del_setting)} while (0);
   do {RUN_TEST_CASE(property_xml_generator_test, test_scenario_prod_not_running)} while (0);
   do {RUN_TEST_CASE(property_xml_generator_test, test_scenario_prod_uninstalled)} while (0);
   do {RUN_TEST_CASE(property_xml_generator_test, test_scenario_prod_installed)} while (0);
}


TEST_SETUP(property_xml_generator_test) {
    ma_ds_database_t *ds_database = NULL;
    unlink(GLOBAL_DATABASE_NAME_FOR_UT);
    
    TEST_ASSERT_MESSAGE(MA_OK ==  ma_db_open( GLOBAL_DATABASE_NAME_FOR_UT, MA_DB_OPEN_READWRITE,  &g_database_handle), "ma_db_open failed to open global database datatbase");
    TEST_ASSERT_MESSAGE(MA_OK ==  ma_ds_database_create(g_database_handle , "Properties" , &ds_database), "ma_ds_database_create failed to create the instance");
	g_ds = (ma_ds_t *)ds_database;
}

TEST_TEAR_DOWN(property_xml_generator_test) {
	TEST_ASSERT_MESSAGE(MA_OK ==  ma_ds_database_release((ma_ds_database_t*)g_ds), "ma_ds_database failed to release the instance");
    TEST_ASSERT_MESSAGE(MA_OK ==  ma_db_close(g_database_handle), "ma_db_close failed to close the global datatbase");
    
    unlink(GLOBAL_DATABASE_NAME_FOR_UT);
}

TEST(property_xml_generator_test,test_scenario_prod_running_new_section_new_setting) {
	size_t size = 0;
	ma_property_bag_t *prop_bag= NULL;
	ma_table_t *table;
	ma_property_xml_generator_t *xml_generator = NULL;
	unsigned char *inc_buffer = NULL;
	unsigned char *full_buffer = NULL;

	property_xml_verification_data_t *incr_data = NULL;
	property_xml_verification_data_t *full_data = NULL;
	/*Save PCR_____1000 in database, (assuming this was the last information that was sent to the server*/
	add_to_datastore(PCR_____1000, g_ds, DS_PROP_PATH);
	/*Create the xml generator object*/
	TEST_ASSERT(MA_OK == ma_property_xml_generator_create(&xml_generator, g_ds, DS_PROP_PATH, MACHINE_NAME, MACHINE_GUID, NULL, PROPS_VERSION));
	/*Create a prop bag and add the predefined setting*/
	TEST_ASSERT(MA_OK == ma_property_bag_create(& prop_bag));
	/*Create the verification data manifest*/
	TEST_ASSERT(MA_OK == initialize_property_xml_verification(&incr_data,MA_FALSE));
	TEST_ASSERT(MA_OK == initialize_property_xml_verification(&full_data,MA_TRUE));
	add_to_prop_bag(SYSPROPS1000, prop_bag);
	
	TEST_ASSERT(MA_OK == ma_property_bag_get_table(prop_bag, &table));

	/*add the table*/
	TEST_ASSERT(MA_OK == ma_property_xml_generator_append_prop(xml_generator, get_prod_id(SYSPROPS1000), table));
	TEST_ASSERT(MA_OK == ma_table_release(table));

	TEST_ASSERT(MA_OK == ma_property_bag_release(prop_bag));
	
	TEST_ASSERT(MA_OK == ma_property_bag_create(& prop_bag));
	add_to_prop_bag(PCR_____1000, prop_bag);
	/*add a new section with a new setting*/
	{
		add_setting_to_prop_bag(prop_bag, "Updater","Engine", "MueScript");
		add_setting_to_prop_bag(prop_bag, "Services","Property Service", "on");
		add_setting_to_prop_bag(prop_bag, "Services","Policy Service", "off");
	}
	{
		add_to_verifier(SYSPROPS1000, incr_data);

		add_product_to_xml_verifier(incr_data, get_prod_id(PCR_____1000),MA_FALSE);
		add_section_to_product(incr_data, get_prod_id(PCR_____1000), "Updater", MA_FALSE);
		add_setting_to_section(incr_data, get_prod_id(PCR_____1000), "Updater", "Engine", "MueScript",MA_FALSE,MA_FALSE);
		
		add_section_to_product(incr_data, get_prod_id(PCR_____1000), "Services", MA_FALSE);
		add_setting_to_section(incr_data, get_prod_id(PCR_____1000), "Services", "Property Service", "on",MA_FALSE,MA_FALSE);
		add_setting_to_section(incr_data, get_prod_id(PCR_____1000), "Services", "Policy Service", "off",MA_FALSE,MA_FALSE);
	}
	{
		add_to_verifier(SYSPROPS1000, full_data);
		add_to_verifier(PCR_____1000, full_data);
		add_section_to_product(full_data, get_prod_id(PCR_____1000), "Updater", MA_FALSE);
		add_setting_to_section(full_data, get_prod_id(PCR_____1000), "Updater", "Engine", "MueScript",MA_FALSE,MA_FALSE);
		
		add_section_to_product(full_data, get_prod_id(PCR_____1000), "Services", MA_FALSE);
		add_setting_to_section(full_data, get_prod_id(PCR_____1000), "Services", "Property Service", "on",MA_FALSE,MA_FALSE);
		add_setting_to_section(full_data, get_prod_id(PCR_____1000), "Services", "Policy Service", "off",MA_FALSE,MA_FALSE);
	}
	TEST_ASSERT(MA_OK == ma_property_bag_get_table(prop_bag, &table));

	/*add the table*/
	TEST_ASSERT(MA_OK == ma_property_xml_generator_append_prop(xml_generator, get_prod_id(PCR_____1000), table));
	TEST_ASSERT(MA_OK == ma_table_release(table));

	TEST_ASSERT(MA_OK == ma_property_xml_generator_get_incremental_props(xml_generator, &inc_buffer, &size));
	/*Incremental XML will have only 2 sections, the General Section will not be included because nothing has changed*/
	MA_LOG(g_console_logger, MA_LOG_SEV_INFO, (char*)inc_buffer);
	TEST_ASSERT(MA_OK == verify_xml_buffer(incr_data, inc_buffer));	
	TEST_ASSERT(MA_OK == ma_property_xml_generator_get_full_props(xml_generator, &full_buffer, &size));
	MA_LOG(g_console_logger, MA_LOG_SEV_INFO, (char*)full_buffer);
	TEST_ASSERT(MA_OK == verify_xml_buffer(full_data, full_buffer));	

	/*Verification done. Continue with next*/
	TEST_ASSERT(MA_OK == ma_property_xml_generator_release(xml_generator));
	release_property_xml_verification(incr_data);
	release_property_xml_verification(full_data);
	free_data(inc_buffer);
	free_data(full_buffer);
	TEST_ASSERT(MA_OK == ma_property_bag_release(prop_bag));
}

TEST(property_xml_generator_test,test_scenario_prod_running_same_section_new_setting) {
	/**/
	size_t size = 0;
	ma_property_bag_t *prop_bag= NULL;
	ma_table_t *table;
	ma_property_xml_generator_t *xml_generator = NULL;
	unsigned char *inc_buffer = NULL;
	unsigned char *full_buffer = NULL;

	property_xml_verification_data_t *incr_data = NULL;
	property_xml_verification_data_t *full_data = NULL;
	
	TEST_ASSERT(MA_OK == ma_property_xml_generator_create(&xml_generator, g_ds, DS_PROP_PATH, MACHINE_NAME, MACHINE_GUID, NULL, PROPS_VERSION));
	/*Create the verification data manifest*/
	TEST_ASSERT(MA_OK == initialize_property_xml_verification(&incr_data,MA_FALSE));
	TEST_ASSERT(MA_OK == initialize_property_xml_verification(&full_data,MA_TRUE));
	
	/*Create a prop bag and add the predefined setting*/
	/*save the system property to DB assuming it was stored in a previous generation add system property*/
	add_to_datastore(SYSPROPS1000, g_ds, DS_PROP_PATH);
	TEST_ASSERT(MA_OK == ma_property_bag_create(& prop_bag));
	add_to_prop_bag(SYSPROPS1000, prop_bag);
	add_setting_to_prop_bag(prop_bag,MA_STR_SYSTEM_PROPERTY, "OSBuild", "6600");
	TEST_ASSERT(MA_OK == ma_property_bag_get_table(prop_bag, &table));
	TEST_ASSERT(MA_OK == ma_property_xml_generator_append_prop(xml_generator, MA_SYSTEM_PROP_PROVIDER_ID, table));
	TEST_ASSERT(MA_OK == ma_table_release(table));
	TEST_ASSERT(MA_OK == ma_property_bag_release(prop_bag));
	{
		add_product_to_xml_verifier(incr_data,MA_SYSTEM_PROP_PROVIDER_ID, MA_FALSE);
		add_section_to_product(incr_data, MA_SYSTEM_PROP_PROVIDER_ID, MA_STR_SYSTEM_PROPERTY, MA_FALSE);
		add_setting_to_section(incr_data, MA_SYSTEM_PROP_PROVIDER_ID, MA_STR_SYSTEM_PROPERTY, "OSBuild","6600", MA_FALSE, MA_FALSE);
	}
	{
		add_to_verifier(SYSPROPS1000, full_data);
		add_setting_to_section(incr_data, MA_SYSTEM_PROP_PROVIDER_ID, MA_STR_SYSTEM_PROPERTY, "OSBuild","6600", MA_FALSE, MA_FALSE);
	}
	add_to_datastore(PCR_____1000, g_ds, DS_PROP_PATH);

	TEST_ASSERT(MA_OK == ma_property_bag_create(& prop_bag));
	add_to_prop_bag(PCR_____1000,prop_bag);
	add_setting_to_prop_bag(prop_bag,"General", "Coverage","Good");
	TEST_ASSERT(MA_OK == ma_property_bag_get_table(prop_bag, &table));
	TEST_ASSERT(MA_OK == ma_property_xml_generator_append_prop(xml_generator, get_prod_id(PCR_____1000), table));
	TEST_ASSERT(MA_OK == ma_table_release(table));
	TEST_ASSERT(MA_OK == ma_property_bag_release(prop_bag));
	{
		add_product_to_xml_verifier(incr_data, get_prod_id(PCR_____1000), MA_FALSE);
		add_section_to_product(incr_data, get_prod_id(PCR_____1000), "General", MA_FALSE);
		add_setting_to_section(incr_data, get_prod_id(PCR_____1000), "General", "Coverage", "Good", MA_FALSE, MA_FALSE);
	}
	{
		add_to_verifier(PCR_____1000, full_data);
		add_setting_to_section(full_data, get_prod_id(PCR_____1000), "General", "Coverage", "Good", MA_FALSE, MA_FALSE);
	}
	add_to_datastore(RSD_____4700, g_ds, DS_PROP_PATH);

	TEST_ASSERT(MA_OK == ma_property_bag_create(& prop_bag));
	add_to_prop_bag(RSD_____4700, prop_bag);
	add_setting_to_prop_bag(prop_bag, "General", "useLPC","1");
	add_setting_to_prop_bag(prop_bag, "LocalElect", "InactiveSensor","2500");
	add_setting_to_prop_bag(prop_bag, "Sensor", "VoteTimeout","3000");
	TEST_ASSERT(MA_OK == ma_property_bag_get_table(prop_bag, &table));
	TEST_ASSERT(MA_OK == ma_property_xml_generator_append_prop(xml_generator, get_prod_id(RSD_____4700), table));
	TEST_ASSERT(MA_OK == ma_table_release(table));
	TEST_ASSERT(MA_OK == ma_property_bag_release(prop_bag));

	{
		add_product_to_xml_verifier(incr_data, get_prod_id(RSD_____4700), MA_FALSE);
		add_section_to_product(incr_data, get_prod_id(RSD_____4700), "General", MA_FALSE);
		add_setting_to_section(incr_data, get_prod_id(RSD_____4700), "General", "useLPC", "1", MA_FALSE, MA_FALSE);
		add_section_to_product(incr_data, get_prod_id(RSD_____4700), "LocalElect", MA_FALSE);
		add_setting_to_section(incr_data, get_prod_id(RSD_____4700), "LocalElect", "InactiveSensor", "2500", MA_FALSE, MA_FALSE);
		add_section_to_product(incr_data, get_prod_id(RSD_____4700), "Sensor", MA_FALSE);
		add_setting_to_section(incr_data, get_prod_id(RSD_____4700), "Sensor", "VoteTimeout", "3000", MA_FALSE, MA_FALSE);
	}
	{
		add_to_verifier(RSD_____4700, full_data);
		add_setting_to_section(full_data, get_prod_id(RSD_____4700), "General", "useLPC", "1", MA_FALSE, MA_FALSE);
		add_setting_to_section(full_data, get_prod_id(RSD_____4700), "LocalElect", "InactiveSensor", "2500", MA_FALSE, MA_FALSE);
		add_setting_to_section(full_data, get_prod_id(RSD_____4700), "Sensor", "VoteTimeout", "3000", MA_FALSE, MA_FALSE);
	}

	TEST_ASSERT(MA_OK == ma_property_xml_generator_get_incremental_props(xml_generator, &inc_buffer, &size));
	MA_LOG(g_console_logger, MA_LOG_SEV_INFO, "Incr props : %s\n\n\n", inc_buffer);
	TEST_ASSERT(MA_OK == verify_xml_buffer(incr_data,inc_buffer));
	TEST_ASSERT(MA_OK == ma_property_xml_generator_get_full_props(xml_generator, &full_buffer, &size));
	MA_LOG(g_console_logger, MA_LOG_SEV_INFO, "%s", full_buffer);
	TEST_ASSERT(MA_OK == verify_xml_buffer(full_data,full_buffer));
	release_property_xml_verification(incr_data);
	release_property_xml_verification(full_data);
	free_data(inc_buffer);
	free_data(full_buffer);
	TEST_ASSERT(MA_OK == ma_property_xml_generator_release(xml_generator));
}

TEST(property_xml_generator_test,test_scenario_prod_running_same_section_same_setting) {
	
	/**/
	size_t size = 0;
	ma_property_bag_t *prop_bag= NULL;
	ma_table_t *table;
	ma_property_xml_generator_t *xml_generator = NULL;

	unsigned char *inc_buffer = NULL;
	unsigned char *full_buffer = NULL;

	property_xml_verification_data_t *incr_data = NULL;
	property_xml_verification_data_t *full_data = NULL;
	
	TEST_ASSERT(MA_OK == ma_property_xml_generator_create(&xml_generator, g_ds, DS_PROP_PATH, MACHINE_NAME, MACHINE_GUID, NULL, PROPS_VERSION));
	/*Create the verification data manifest*/
	TEST_ASSERT(MA_OK == initialize_property_xml_verification(&incr_data,MA_FALSE));
	TEST_ASSERT(MA_OK == initialize_property_xml_verification(&full_data,MA_TRUE));
	
	/*Create a prop bag and add the predefined setting*/
	/*save the system property to DB assuming it was stored in a previous generation add system property*/
	add_to_datastore(SYSPROPS1000, g_ds, DS_PROP_PATH);
	TEST_ASSERT(MA_OK == ma_property_bag_create(& prop_bag));
	add_to_prop_bag(SYSPROPS1000, prop_bag);
	TEST_ASSERT(MA_OK == ma_property_bag_get_table(prop_bag, &table));
	TEST_ASSERT(MA_OK == ma_property_xml_generator_append_prop(xml_generator, MA_SYSTEM_PROP_PROVIDER_ID, table));
	TEST_ASSERT(MA_OK == ma_table_release(table));
	TEST_ASSERT(MA_OK == ma_property_bag_release(prop_bag));
	{
	}
	{
		add_to_verifier(SYSPROPS1000, full_data);
	}
	add_to_datastore(PCR_____1000, g_ds, DS_PROP_PATH);

	TEST_ASSERT(MA_OK == ma_property_bag_create(& prop_bag));
	add_to_prop_bag(PCR_____1000,prop_bag);
	TEST_ASSERT(MA_OK == ma_property_bag_get_table(prop_bag, &table));
	TEST_ASSERT(MA_OK == ma_property_xml_generator_append_prop(xml_generator, get_prod_id(PCR_____1000), table));
	TEST_ASSERT(MA_OK == ma_table_release(table));
	TEST_ASSERT(MA_OK == ma_property_bag_release(prop_bag));
	{
		add_product_to_xml_verifier(incr_data, get_prod_id(PCR_____1000), MA_FALSE);
	}
	{
		add_to_verifier(PCR_____1000, full_data);
	}
	add_to_datastore(RSD_____4700, g_ds, DS_PROP_PATH);

	TEST_ASSERT(MA_OK == ma_property_bag_create(& prop_bag));
	add_to_prop_bag(RSD_____4700, prop_bag);
	TEST_ASSERT(MA_OK == ma_property_bag_get_table(prop_bag, &table));
	TEST_ASSERT(MA_OK == ma_property_xml_generator_append_prop(xml_generator, get_prod_id(RSD_____4700), table));
	TEST_ASSERT(MA_OK == ma_table_release(table));
	TEST_ASSERT(MA_OK == ma_property_bag_release(prop_bag));

	{
		add_product_to_xml_verifier(incr_data, get_prod_id(RSD_____4700), MA_FALSE);
	}
	{
		add_to_verifier(RSD_____4700, full_data);
	}

	TEST_ASSERT(MA_OK == ma_property_xml_generator_get_incremental_props(xml_generator, &inc_buffer, &size));
	MA_LOG(g_console_logger, MA_LOG_SEV_INFO, "Incr props : %s\n\n\n", inc_buffer);
	TEST_ASSERT(MA_OK == verify_xml_buffer(incr_data,inc_buffer));
	TEST_ASSERT(MA_OK == ma_property_xml_generator_get_full_props(xml_generator, &full_buffer, &size));
	MA_LOG(g_console_logger, MA_LOG_SEV_INFO, "Full Props: %s", full_buffer);
	TEST_ASSERT(MA_OK == verify_xml_buffer(full_data,full_buffer));
	release_property_xml_verification(incr_data);
	release_property_xml_verification(full_data);
	free_data(inc_buffer);
	free_data(full_buffer);
	TEST_ASSERT(MA_OK == ma_property_xml_generator_release(xml_generator));
}

TEST(property_xml_generator_test,test_scenario_prod_running_same_section_diff_setting) {
	
	/**/
	size_t size = 0;
	ma_property_bag_t *prop_bag= NULL;
	ma_table_t *table;
	ma_property_xml_generator_t *xml_generator = NULL;

	unsigned char *inc_buffer = NULL;
	unsigned char *full_buffer = NULL;

	property_xml_verification_data_t *incr_data = NULL;
	property_xml_verification_data_t *full_data = NULL;
	
	TEST_ASSERT(MA_OK == ma_property_xml_generator_create(&xml_generator, g_ds, DS_PROP_PATH, MACHINE_NAME, MACHINE_GUID, NULL, PROPS_VERSION));
	/*Create the verification data manifest*/
	TEST_ASSERT(MA_OK == initialize_property_xml_verification(&incr_data,MA_FALSE));
	TEST_ASSERT(MA_OK == initialize_property_xml_verification(&full_data,MA_TRUE));
	
	/*Create a prop bag and add the predefined setting*/
	/*save the system property to DB assuming it was stored in a previous generation add system property*/
	add_to_datastore(SYSPROPS1000, g_ds, DS_PROP_PATH);
	TEST_ASSERT(MA_OK == ma_property_bag_create(& prop_bag));
	
	add_setting_to_prop_bag(prop_bag, MA_STR_SYSTEM_PROPERTY, "DomainName", "WORKGROUP");/*changed*/
	add_setting_to_prop_bag(prop_bag, MA_STR_SYSTEM_PROPERTY, "OSType", "Windows 8"); /*changed*/
	add_setting_to_prop_bag(prop_bag, MA_STR_SYSTEM_PROPERTY, "OSOEMId", "00392-918-5000002-85916");
	add_setting_to_prop_bag(prop_bag, MA_STR_SYSTEM_PROPERTY, "CPUSerialNumber", "N/A");

	TEST_ASSERT(MA_OK == ma_property_bag_get_table(prop_bag, &table));
	TEST_ASSERT(MA_OK == ma_property_xml_generator_append_prop(xml_generator, MA_SYSTEM_PROP_PROVIDER_ID, table));
	TEST_ASSERT(MA_OK == ma_table_release(table));
	TEST_ASSERT(MA_OK == ma_property_bag_release(prop_bag));
	{
		add_product_to_xml_verifier(incr_data,get_prod_id(SYSPROPS1000), MA_FALSE);
		add_section_to_product(incr_data,get_prod_id(SYSPROPS1000),MA_STR_SYSTEM_PROPERTY, MA_FALSE);
		add_setting_to_section(incr_data,get_prod_id(SYSPROPS1000),MA_STR_SYSTEM_PROPERTY, "DomainName", "WORKGROUP",MA_FALSE,MA_FALSE);/*changed*/
		add_setting_to_section(incr_data,get_prod_id(SYSPROPS1000),MA_STR_SYSTEM_PROPERTY, "OSType", "Windows 8",MA_FALSE,MA_FALSE); /*changed*/
		add_setting_to_section(incr_data,get_prod_id(SYSPROPS1000),MA_STR_SYSTEM_PROPERTY, "OSOEMId", "00392-918-5000002-85916",MA_FALSE,MA_FALSE);
		add_setting_to_section(incr_data,get_prod_id(SYSPROPS1000),MA_STR_SYSTEM_PROPERTY, "CPUSerialNumber", "N/A",MA_FALSE,MA_FALSE);
	}
	{
		add_product_to_xml_verifier(full_data,get_prod_id(SYSPROPS1000), MA_FALSE);
		add_section_to_product(full_data,get_prod_id(SYSPROPS1000),MA_STR_SYSTEM_PROPERTY, MA_FALSE);
		add_setting_to_section(full_data,get_prod_id(SYSPROPS1000),MA_STR_SYSTEM_PROPERTY, "DomainName", "WORKGROUP",MA_FALSE,MA_FALSE);/*changed*/
		add_setting_to_section(full_data,get_prod_id(SYSPROPS1000),MA_STR_SYSTEM_PROPERTY, "OSType", "Windows 8",MA_FALSE,MA_FALSE); /*changed*/
		add_setting_to_section(full_data,get_prod_id(SYSPROPS1000),MA_STR_SYSTEM_PROPERTY, "OSOEMId", "00392-918-5000002-85916",MA_FALSE,MA_FALSE);
		add_setting_to_section(full_data,get_prod_id(SYSPROPS1000),MA_STR_SYSTEM_PROPERTY, "CPUSerialNumber", "N/A",MA_FALSE,MA_FALSE);
	}
	add_to_datastore(PCR_____1000, g_ds, DS_PROP_PATH);

	TEST_ASSERT(MA_OK == ma_property_bag_create(& prop_bag));
	//TODO:
	add_setting_to_prop_bag(prop_bag,"General","Language","0409");
	add_setting_to_prop_bag(prop_bag,"General","PluginVersion","4.8.0.747");
	add_setting_to_prop_bag(prop_bag,"General","szInstallDir","C:\\Program Files (x86)\\McAfee\\Common Framework");
	TEST_ASSERT(MA_OK == ma_property_bag_get_table(prop_bag, &table));
	TEST_ASSERT(MA_OK == ma_property_xml_generator_append_prop(xml_generator, get_prod_id(PCR_____1000), table));
	TEST_ASSERT(MA_OK == ma_table_release(table));
	TEST_ASSERT(MA_OK == ma_property_bag_release(prop_bag));
	{
		add_product_to_xml_verifier(incr_data, get_prod_id(PCR_____1000), MA_FALSE);
		add_section_to_product(incr_data,get_prod_id(PCR_____1000), "General",MA_FALSE);
		add_setting_to_section(incr_data,get_prod_id(PCR_____1000),"General","Language","0409",MA_FALSE,MA_FALSE);

	}
	{
		add_product_to_xml_verifier(full_data, get_prod_id(PCR_____1000), MA_FALSE);
		add_section_to_product(full_data,get_prod_id(PCR_____1000), "General",MA_FALSE);
		add_setting_to_section(full_data,get_prod_id(PCR_____1000),"General","Language","0409",MA_FALSE,MA_FALSE);
		add_setting_to_section(full_data,get_prod_id(PCR_____1000),"General","PluginVersion","4.8.0.747",MA_FALSE,MA_FALSE);
		add_setting_to_section(full_data,get_prod_id(PCR_____1000),"General","szInstallDir","C:\\Program Files (x86)\\McAfee\\Common Framework",MA_FALSE,MA_FALSE);

	}
	add_to_datastore(RSD_____4700, g_ds, DS_PROP_PATH);

	TEST_ASSERT(MA_OK == ma_property_bag_create(& prop_bag));
	add_setting_to_prop_bag(prop_bag,"General","szInstallDir","C:\\Program Files (x86)\\McAfee\\Common Framework");
	add_setting_to_prop_bag(prop_bag,"General","szProductVersion","4.7.1.140");

	add_setting_to_prop_bag(prop_bag,"LocalElect","ActiveSensorsPerSubnet","2");
	add_setting_to_prop_bag(prop_bag,"LocalElect","AllSensorsActivePerSubnet","6");
	add_setting_to_prop_bag(prop_bag,"LocalElect","FailoverSleepTime","500");

	add_setting_to_prop_bag(prop_bag,"Sensor","HostFilterTimeout","30000");
	add_setting_to_prop_bag(prop_bag,"Sensor","OSFingerprintingDelay","1000");
	add_setting_to_prop_bag(prop_bag,"Sensor","OSFingerprintingInterval","3500");
	add_setting_to_prop_bag(prop_bag,"Sensor","SensorThrottle","4000");
	add_setting_to_prop_bag(prop_bag,"Sensor","VoteInterval","3000");
	TEST_ASSERT(MA_OK == ma_property_bag_get_table(prop_bag, &table));
	TEST_ASSERT(MA_OK == ma_property_xml_generator_append_prop(xml_generator, get_prod_id(RSD_____4700), table));
	TEST_ASSERT(MA_OK == ma_table_release(table));
	TEST_ASSERT(MA_OK == ma_property_bag_release(prop_bag));

	{
		add_product_to_xml_verifier(incr_data, get_prod_id(RSD_____4700), MA_FALSE);
		add_section_to_product(incr_data, get_prod_id(RSD_____4700), "General", MA_FALSE);
		add_setting_to_section(incr_data, get_prod_id(RSD_____4700), "General", "szProductVersion","4.7.1.140",MA_FALSE,MA_FALSE);
		add_section_to_product(incr_data, get_prod_id(RSD_____4700), "LocalElect", MA_FALSE);
		add_setting_to_section(incr_data, get_prod_id(RSD_____4700), "LocalElect", "AllSensorsActivePerSubnet","6",MA_FALSE,MA_FALSE);
		add_section_to_product(incr_data, get_prod_id(RSD_____4700), "Sensor", MA_FALSE);
		add_setting_to_section(incr_data, get_prod_id(RSD_____4700), "Sensor", "OSFingerprintingInterval","3500",MA_FALSE,MA_FALSE);
		add_setting_to_section(incr_data, get_prod_id(RSD_____4700), "Sensor", "SensorThrottle","4000",MA_FALSE,MA_FALSE);
	}
	{
		add_product_to_xml_verifier(full_data, "RSD_____4700",  MA_FALSE);
		add_section_to_product(full_data, "RSD_____4700", "General", MA_FALSE);
		add_setting_to_section(full_data, "RSD_____4700","General","szInstallDir","C:\\Program Files (x86)\\McAfee\\Common Framework",MA_TRUE,MA_FALSE);
		add_setting_to_section(full_data, "RSD_____4700","General","szProductVersion","4.7.1.140",MA_TRUE,MA_FALSE);
	
		add_section_to_product(full_data, "RSD_____4700", "LocalElect", MA_FALSE);
		add_setting_to_section(full_data, "RSD_____4700","LocalElect","ActiveSensorsPerSubnet","2",MA_TRUE,MA_FALSE);
		add_setting_to_section(full_data, "RSD_____4700","LocalElect","AllSensorsActivePerSubnet","6",MA_TRUE,MA_FALSE);
		add_setting_to_section(full_data, "RSD_____4700","LocalElect","FailoverSleepTime","500",MA_TRUE,MA_FALSE);
	
		add_section_to_product(full_data, "RSD_____4700", "Sensor", MA_FALSE);
		add_setting_to_section(full_data, "RSD_____4700","Sensor","HostFilterTimeout","30000",MA_TRUE,MA_FALSE);
		add_setting_to_section(full_data, "RSD_____4700","Sensor","OSFingerprintingDelay","1000",MA_TRUE,MA_FALSE);
		add_setting_to_section(full_data, "RSD_____4700","Sensor","OSFingerprintingInterval","3500",MA_TRUE,MA_FALSE);
		add_setting_to_section(full_data, "RSD_____4700","Sensor","SensorThrottle","4000",MA_TRUE,MA_FALSE);
		add_setting_to_section(full_data, "RSD_____4700","Sensor","VoteInterval","3000",MA_TRUE,MA_FALSE);
	}

	TEST_ASSERT(MA_OK == ma_property_xml_generator_get_incremental_props(xml_generator, &inc_buffer, &size));
	MA_LOG(g_console_logger, MA_LOG_SEV_INFO, "Incr props : %s\n\n\n", inc_buffer);
	TEST_ASSERT(MA_OK == verify_xml_buffer(incr_data,inc_buffer));
	TEST_ASSERT(MA_OK == ma_property_xml_generator_get_full_props(xml_generator, &full_buffer, &size));
	MA_LOG(g_console_logger, MA_LOG_SEV_INFO, "Full Props: %s", full_buffer);
	TEST_ASSERT(MA_OK == verify_xml_buffer(full_data,full_buffer));
	release_property_xml_verification(incr_data);
	release_property_xml_verification(full_data);
	free_data(inc_buffer);
	free_data(full_buffer);
	TEST_ASSERT(MA_OK == ma_property_xml_generator_release(xml_generator));
}

TEST(property_xml_generator_test,test_scenario_prod_running_same_section_del_setting) {
	
	/**/
	size_t size = 0;
	ma_property_bag_t *prop_bag= NULL;
	ma_table_t *table;
	ma_property_xml_generator_t *xml_generator = NULL;

	unsigned char *inc_buffer = NULL;
	unsigned char *full_buffer = NULL;

	property_xml_verification_data_t *incr_data = NULL;
	property_xml_verification_data_t *full_data = NULL;
	
	TEST_ASSERT(MA_OK == ma_property_xml_generator_create(&xml_generator, g_ds, DS_PROP_PATH, MACHINE_NAME, MACHINE_GUID, NULL, PROPS_VERSION));
	/*Create the verification data manifest*/
	TEST_ASSERT(MA_OK == initialize_property_xml_verification(&incr_data,MA_FALSE));
	TEST_ASSERT(MA_OK == initialize_property_xml_verification(&full_data,MA_TRUE));
	
	/*Create a prop bag and add the predefined setting*/
	/*save the system property to DB assuming it was stored in a previous generation add system property*/
	add_to_datastore(SYSPROPS1000, g_ds, DS_PROP_PATH);
	TEST_ASSERT(MA_OK == ma_property_bag_create(& prop_bag));
	
	add_setting_to_prop_bag(prop_bag, MA_STR_SYSTEM_PROPERTY, "DomainName", "WORKGROUP");/*changed*/
	add_setting_to_prop_bag(prop_bag, MA_STR_SYSTEM_PROPERTY, "OSType", "Windows 8"); /*changed*/
	add_setting_to_prop_bag(prop_bag, MA_STR_SYSTEM_PROPERTY, "OSOEMId", "00392-918-5000002-85916");
	//add_setting_to_prop_bag(prop_bag, MA_STR_SYSTEM_PROPERTY, "CPUSerialNumber", "N/A");

	TEST_ASSERT(MA_OK == ma_property_bag_get_table(prop_bag, &table));
	TEST_ASSERT(MA_OK == ma_property_xml_generator_append_prop(xml_generator, MA_SYSTEM_PROP_PROVIDER_ID, table));
	TEST_ASSERT(MA_OK == ma_table_release(table));
	TEST_ASSERT(MA_OK == ma_property_bag_release(prop_bag));
	{
		add_product_to_xml_verifier(incr_data,get_prod_id(SYSPROPS1000), MA_FALSE);
		add_section_to_product(incr_data,get_prod_id(SYSPROPS1000),MA_STR_SYSTEM_PROPERTY, MA_FALSE);
		add_setting_to_section(incr_data,get_prod_id(SYSPROPS1000),MA_STR_SYSTEM_PROPERTY, "DomainName", "WORKGROUP",MA_FALSE,MA_FALSE);/*changed*/
		add_setting_to_section(incr_data,get_prod_id(SYSPROPS1000),MA_STR_SYSTEM_PROPERTY, "OSType", "Windows 8",MA_FALSE,MA_FALSE); /*changed*/
		add_setting_to_section(incr_data,get_prod_id(SYSPROPS1000),MA_STR_SYSTEM_PROPERTY, "OSOEMId", "00392-918-5000002-85916",MA_FALSE,MA_FALSE);
		add_setting_to_section(incr_data,get_prod_id(SYSPROPS1000),MA_STR_SYSTEM_PROPERTY, "CPUSerialNumber", "N/A",MA_FALSE,MA_TRUE);
	}
	{
		add_product_to_xml_verifier(full_data,get_prod_id(SYSPROPS1000), MA_FALSE);
		add_section_to_product(full_data,get_prod_id(SYSPROPS1000),MA_STR_SYSTEM_PROPERTY, MA_FALSE);
		add_setting_to_section(full_data,get_prod_id(SYSPROPS1000),MA_STR_SYSTEM_PROPERTY, "DomainName", "WORKGROUP",MA_FALSE,MA_FALSE);/*changed*/
		add_setting_to_section(full_data,get_prod_id(SYSPROPS1000),MA_STR_SYSTEM_PROPERTY, "OSType", "Windows 8",MA_FALSE,MA_FALSE); /*changed*/
		add_setting_to_section(full_data,get_prod_id(SYSPROPS1000),MA_STR_SYSTEM_PROPERTY, "OSOEMId", "00392-918-5000002-85916",MA_FALSE,MA_FALSE);
		add_setting_to_section(full_data,get_prod_id(SYSPROPS1000),MA_STR_SYSTEM_PROPERTY, "CPUSerialNumber", "N/A",MA_FALSE,MA_TRUE);
	}
	add_to_datastore(PCR_____1000, g_ds, DS_PROP_PATH);

	TEST_ASSERT(MA_OK == ma_property_bag_create(& prop_bag));
	//TODO:
	//add_setting_to_prop_bag(prop_bag,"General","Language","0409");
	add_setting_to_prop_bag(prop_bag,"General","PluginVersion","4.8.0.747");
	add_setting_to_prop_bag(prop_bag,"General","szInstallDir","C:\\Program Files (x86)\\McAfee\\Common Framework");
	TEST_ASSERT(MA_OK == ma_property_bag_get_table(prop_bag, &table));
	TEST_ASSERT(MA_OK == ma_property_xml_generator_append_prop(xml_generator, get_prod_id(PCR_____1000), table));
	TEST_ASSERT(MA_OK == ma_table_release(table));
	TEST_ASSERT(MA_OK == ma_property_bag_release(prop_bag));
	{
		add_product_to_xml_verifier(incr_data, get_prod_id(PCR_____1000), MA_FALSE);
		add_section_to_product(incr_data,get_prod_id(PCR_____1000), "General",MA_FALSE);
		add_setting_to_section(incr_data,get_prod_id(PCR_____1000),"General","Language","0409",MA_FALSE,MA_TRUE);

	}
	{
		add_product_to_xml_verifier(full_data, get_prod_id(PCR_____1000), MA_FALSE);
		add_section_to_product(full_data,get_prod_id(PCR_____1000), "General",MA_FALSE);
		add_setting_to_section(full_data,get_prod_id(PCR_____1000),"General","Language","0409",MA_FALSE,MA_TRUE);
		add_setting_to_section(full_data,get_prod_id(PCR_____1000),"General","PluginVersion","4.8.0.747",MA_FALSE,MA_FALSE);
		add_setting_to_section(full_data,get_prod_id(PCR_____1000),"General","szInstallDir","C:\\Program Files (x86)\\McAfee\\Common Framework",MA_FALSE,MA_FALSE);

	}
	add_to_datastore(RSD_____4700, g_ds, DS_PROP_PATH);

	TEST_ASSERT(MA_OK == ma_property_bag_create(& prop_bag));
	add_setting_to_prop_bag(prop_bag,"General","szInstallDir","C:\\Program Files (x86)\\McAfee\\Common Framework");

	add_setting_to_prop_bag(prop_bag,"LocalElect","ActiveSensorsPerSubnet","2");
	add_setting_to_prop_bag(prop_bag,"LocalElect","FailoverSleepTime","500");

	add_setting_to_prop_bag(prop_bag,"Sensor","HostFilterTimeout","30000");
	add_setting_to_prop_bag(prop_bag,"Sensor","OSFingerprintingDelay","1000");

	add_setting_to_prop_bag(prop_bag,"Sensor","VoteInterval","3000");
	TEST_ASSERT(MA_OK == ma_property_bag_get_table(prop_bag, &table));
	TEST_ASSERT(MA_OK == ma_property_xml_generator_append_prop(xml_generator, get_prod_id(RSD_____4700), table));
	TEST_ASSERT(MA_OK == ma_table_release(table));
	TEST_ASSERT(MA_OK == ma_property_bag_release(prop_bag));

	{
		add_product_to_xml_verifier(incr_data, get_prod_id(RSD_____4700), MA_FALSE);
		add_section_to_product(incr_data, get_prod_id(RSD_____4700), "General", MA_FALSE);
		add_setting_to_section(incr_data, get_prod_id(RSD_____4700), "General", "szProductVersion","4.7.1.140",MA_FALSE,MA_TRUE);
		add_section_to_product(incr_data, get_prod_id(RSD_____4700), "LocalElect", MA_FALSE);
		add_setting_to_section(incr_data, get_prod_id(RSD_____4700), "LocalElect", "AllSensorsActivePerSubnet","6",MA_FALSE,MA_TRUE);
		add_section_to_product(incr_data, get_prod_id(RSD_____4700), "Sensor", MA_FALSE);
		add_setting_to_section(incr_data, get_prod_id(RSD_____4700), "Sensor", "OSFingerprintingInterval","3500",MA_FALSE,MA_TRUE);
		add_setting_to_section(incr_data, get_prod_id(RSD_____4700), "Sensor", "SensorThrottle","4000",MA_FALSE,MA_TRUE);
	}
	{
		add_product_to_xml_verifier(full_data, "RSD_____4700",  MA_FALSE);
		add_section_to_product(full_data, "RSD_____4700", "General", MA_FALSE);
		add_setting_to_section(full_data, "RSD_____4700","General","szInstallDir","C:\\Program Files (x86)\\McAfee\\Common Framework",MA_TRUE,MA_FALSE);
		add_setting_to_section(full_data, "RSD_____4700","General","szProductVersion","4.7.1.140",MA_TRUE,MA_FALSE);
	
		add_section_to_product(full_data, "RSD_____4700", "LocalElect", MA_FALSE);
		add_setting_to_section(full_data, "RSD_____4700","LocalElect","ActiveSensorsPerSubnet","2",MA_TRUE,MA_FALSE);
		add_setting_to_section(full_data, "RSD_____4700","LocalElect","AllSensorsActivePerSubnet","6",MA_TRUE,MA_TRUE);
		add_setting_to_section(full_data, "RSD_____4700","LocalElect","FailoverSleepTime","500",MA_TRUE,MA_FALSE);
	
		add_section_to_product(full_data, "RSD_____4700", "Sensor", MA_FALSE);
		add_setting_to_section(full_data, "RSD_____4700","Sensor","HostFilterTimeout","30000",MA_TRUE,MA_FALSE);
		add_setting_to_section(full_data, "RSD_____4700","Sensor","OSFingerprintingDelay","1000",MA_TRUE,MA_FALSE);
		add_setting_to_section(full_data, "RSD_____4700","Sensor","OSFingerprintingInterval","3500",MA_TRUE,MA_TRUE);
		add_setting_to_section(full_data, "RSD_____4700","Sensor","SensorThrottle","4000",MA_TRUE,MA_TRUE);
		add_setting_to_section(full_data, "RSD_____4700","Sensor","VoteInterval","3000",MA_TRUE,MA_FALSE);
	}

	TEST_ASSERT(MA_OK == ma_property_xml_generator_get_incremental_props(xml_generator, &inc_buffer, &size));
	MA_LOG(g_console_logger, MA_LOG_SEV_INFO, "Incr props : %s\n\n\n", inc_buffer);
	TEST_ASSERT(MA_OK == verify_xml_buffer(incr_data,inc_buffer));
	TEST_ASSERT(MA_OK == ma_property_xml_generator_get_full_props(xml_generator, &full_buffer, &size));
	MA_LOG(g_console_logger, MA_LOG_SEV_INFO, "Full Props: %s", full_buffer);
	TEST_ASSERT(MA_OK == verify_xml_buffer(full_data,full_buffer));
	release_property_xml_verification(incr_data);
	release_property_xml_verification(full_data);
	free_data(inc_buffer);
	free_data(full_buffer);
	TEST_ASSERT(MA_OK == ma_property_xml_generator_release(xml_generator));
}

TEST(property_xml_generator_test,test_scenario_prod_running_deleted_section_del_setting) {
	
	/**/
	size_t size = 0;
	ma_property_bag_t *prop_bag= NULL;
	ma_table_t *table;
	ma_property_xml_generator_t *xml_generator = NULL;

	unsigned char *inc_buffer = NULL;
	unsigned char *full_buffer = NULL;

	property_xml_verification_data_t *incr_data = NULL;
	property_xml_verification_data_t *full_data = NULL;
	
	TEST_ASSERT(MA_OK == ma_property_xml_generator_create(&xml_generator, g_ds, DS_PROP_PATH, MACHINE_NAME, MACHINE_GUID, NULL, PROPS_VERSION));
	/*Create the verification data manifest*/
	TEST_ASSERT(MA_OK == initialize_property_xml_verification(&incr_data,MA_FALSE));
	TEST_ASSERT(MA_OK == initialize_property_xml_verification(&full_data,MA_TRUE));
	
	/*Create a prop bag and add the predefined setting*/
	/*save the system property to DB assuming it was stored in a previous generation add system property*/
	add_to_datastore(SYSPROPS1000, g_ds, DS_PROP_PATH);
	TEST_ASSERT(MA_OK == ma_property_bag_create(& prop_bag));
	
	ma_table_create(&table);
	ma_property_xml_generator_append_prop(xml_generator, get_prod_id(SYSPROPS1000), table);
	ma_table_release(table);
	TEST_ASSERT(MA_OK == ma_property_bag_release(prop_bag));
	{
		add_product_to_xml_verifier(incr_data,get_prod_id(SYSPROPS1000), MA_FALSE);
		add_section_to_product(incr_data,get_prod_id(SYSPROPS1000),MA_STR_SYSTEM_PROPERTY, MA_FALSE);
		add_setting_to_section(incr_data,get_prod_id(SYSPROPS1000),MA_STR_SYSTEM_PROPERTY, "DomainName", "WORKGROUP",MA_FALSE,MA_TRUE);
		add_setting_to_section(incr_data,get_prod_id(SYSPROPS1000),MA_STR_SYSTEM_PROPERTY, "OSType", "Windows 8",MA_FALSE,MA_TRUE); 
		add_setting_to_section(incr_data,get_prod_id(SYSPROPS1000),MA_STR_SYSTEM_PROPERTY, "OSOEMId", "00392-918-5000002-85916",MA_FALSE,MA_TRUE);
		add_setting_to_section(incr_data,get_prod_id(SYSPROPS1000),MA_STR_SYSTEM_PROPERTY, "CPUSerialNumber", "N/A",MA_FALSE,MA_TRUE);
	}
	{
		add_product_to_xml_verifier(full_data,get_prod_id(SYSPROPS1000), MA_FALSE);
		add_section_to_product(full_data,get_prod_id(SYSPROPS1000),MA_STR_SYSTEM_PROPERTY, MA_FALSE);
		add_setting_to_section(full_data,get_prod_id(SYSPROPS1000),MA_STR_SYSTEM_PROPERTY, "DomainName", "WORKGROUP",MA_FALSE,MA_TRUE);/*changed*/
		add_setting_to_section(full_data,get_prod_id(SYSPROPS1000),MA_STR_SYSTEM_PROPERTY, "OSType", "Windows 8",MA_FALSE,MA_TRUE); /*changed*/
		add_setting_to_section(full_data,get_prod_id(SYSPROPS1000),MA_STR_SYSTEM_PROPERTY, "OSOEMId", "00392-918-5000002-85916",MA_FALSE,MA_TRUE);
		add_setting_to_section(full_data,get_prod_id(SYSPROPS1000),MA_STR_SYSTEM_PROPERTY, "CPUSerialNumber", "N/A",MA_FALSE,MA_TRUE);
	}
	add_to_datastore(PCR_____1000, g_ds, DS_PROP_PATH);

	TEST_ASSERT(MA_OK == ma_property_bag_create(& prop_bag));
	ma_table_create(&table);
	ma_property_xml_generator_append_prop(xml_generator, get_prod_id(PCR_____1000), table);
	ma_table_release(table);
	TEST_ASSERT(MA_OK == ma_property_bag_release(prop_bag));
	{
		add_product_to_xml_verifier(incr_data, get_prod_id(PCR_____1000), MA_FALSE);
		add_section_to_product(incr_data,get_prod_id(PCR_____1000), "General",MA_FALSE);
		add_setting_to_section(incr_data,get_prod_id(PCR_____1000),"General","Language","0409",MA_FALSE,MA_TRUE);
		add_setting_to_section(incr_data,get_prod_id(PCR_____1000),"General","PluginVersion","4.8.0.747",MA_FALSE,MA_TRUE);
		add_setting_to_section(incr_data,get_prod_id(PCR_____1000),"General","szInstallDir","C:\\Program Files (x86)\\McAfee\\Common Framework",MA_FALSE,MA_TRUE);

	}
	{
		add_product_to_xml_verifier(full_data, get_prod_id(PCR_____1000), MA_FALSE);
		add_section_to_product(full_data,get_prod_id(PCR_____1000), "General",MA_FALSE);
		add_setting_to_section(full_data,get_prod_id(PCR_____1000),"General","Language","0409",MA_FALSE,MA_TRUE);
		add_setting_to_section(full_data,get_prod_id(PCR_____1000),"General","PluginVersion","4.8.0.747",MA_FALSE,MA_TRUE);
		add_setting_to_section(full_data,get_prod_id(PCR_____1000),"General","szInstallDir","C:\\Program Files (x86)\\McAfee\\Common Framework",MA_FALSE,MA_TRUE);

	}
	add_to_datastore(RSD_____4700, g_ds, DS_PROP_PATH);

	TEST_ASSERT(MA_OK == ma_property_bag_create(& prop_bag));
	
	ma_table_create(&table);
	ma_property_xml_generator_append_prop(xml_generator, get_prod_id(RSD_____4700), table);
	ma_table_release(table);
	TEST_ASSERT(MA_OK == ma_property_bag_release(prop_bag));

	
	{
		add_product_to_xml_verifier(incr_data, "RSD_____4700",  MA_FALSE);
		add_section_to_product(incr_data, "RSD_____4700", "General", MA_FALSE);
		add_setting_to_section(incr_data, "RSD_____4700","General","szInstallDir","C:\\Program Files (x86)\\McAfee\\Common Framework",MA_TRUE,MA_TRUE);
		add_setting_to_section(incr_data, "RSD_____4700","General","szProductVersion","4.7.1.140",MA_TRUE,MA_TRUE);
	
		add_section_to_product(incr_data, "RSD_____4700", "LocalElect", MA_FALSE);
		add_setting_to_section(incr_data, "RSD_____4700","LocalElect","ActiveSensorsPerSubnet","2",MA_TRUE,MA_TRUE);
		add_setting_to_section(incr_data, "RSD_____4700","LocalElect","AllSensorsActivePerSubnet","6",MA_TRUE,MA_TRUE);
		add_setting_to_section(incr_data, "RSD_____4700","LocalElect","FailoverSleepTime","500",MA_TRUE,MA_TRUE);
	
		add_section_to_product(incr_data, "RSD_____4700", "Sensor", MA_FALSE);
		add_setting_to_section(incr_data, "RSD_____4700","Sensor","HostFilterTimeout","30000",MA_TRUE,MA_TRUE);
		add_setting_to_section(incr_data, "RSD_____4700","Sensor","OSFingerprintingDelay","1000",MA_TRUE,MA_TRUE);
		add_setting_to_section(incr_data, "RSD_____4700","Sensor","OSFingerprintingInterval","3500",MA_TRUE,MA_TRUE);
		add_setting_to_section(incr_data, "RSD_____4700","Sensor","SensorThrottle","4000",MA_TRUE,MA_TRUE);
		add_setting_to_section(incr_data, "RSD_____4700","Sensor","VoteInterval","3000",MA_TRUE,MA_TRUE);
	}
	{
		add_product_to_xml_verifier(full_data, "RSD_____4700",  MA_FALSE);
		add_section_to_product(full_data, "RSD_____4700", "General", MA_FALSE);
		add_setting_to_section(full_data, "RSD_____4700","General","szInstallDir","C:\\Program Files (x86)\\McAfee\\Common Framework",MA_TRUE,MA_TRUE);
		add_setting_to_section(full_data, "RSD_____4700","General","szProductVersion","4.7.1.140",MA_TRUE,MA_TRUE);
	
		add_section_to_product(full_data, "RSD_____4700", "LocalElect", MA_FALSE);
		add_setting_to_section(full_data, "RSD_____4700","LocalElect","ActiveSensorsPerSubnet","2",MA_TRUE,MA_TRUE);
		add_setting_to_section(full_data, "RSD_____4700","LocalElect","AllSensorsActivePerSubnet","6",MA_TRUE,MA_TRUE);
		add_setting_to_section(full_data, "RSD_____4700","LocalElect","FailoverSleepTime","500",MA_TRUE,MA_TRUE);
	
		add_section_to_product(full_data, "RSD_____4700", "Sensor", MA_FALSE);
		add_setting_to_section(full_data, "RSD_____4700","Sensor","HostFilterTimeout","30000",MA_TRUE,MA_TRUE);
		add_setting_to_section(full_data, "RSD_____4700","Sensor","OSFingerprintingDelay","1000",MA_TRUE,MA_TRUE);
		add_setting_to_section(full_data, "RSD_____4700","Sensor","OSFingerprintingInterval","3500",MA_TRUE,MA_TRUE);
		add_setting_to_section(full_data, "RSD_____4700","Sensor","SensorThrottle","4000",MA_TRUE,MA_TRUE);
		add_setting_to_section(full_data, "RSD_____4700","Sensor","VoteInterval","3000",MA_TRUE,MA_TRUE);
	}

	TEST_ASSERT(MA_OK == ma_property_xml_generator_get_incremental_props(xml_generator, &inc_buffer, &size));
	MA_LOG(g_console_logger, MA_LOG_SEV_INFO, "Incr props : %s\n\n\n", inc_buffer);
	TEST_ASSERT(MA_OK == verify_xml_buffer(incr_data,inc_buffer));
	TEST_ASSERT(MA_OK == ma_property_xml_generator_get_full_props(xml_generator, &full_buffer, &size));
	MA_LOG(g_console_logger, MA_LOG_SEV_INFO, "Full Props: %s", full_buffer);
	TEST_ASSERT(MA_OK == verify_xml_buffer(full_data,full_buffer));
	release_property_xml_verification(incr_data);
	release_property_xml_verification(full_data);
	free_data(inc_buffer);
	free_data(full_buffer);
	TEST_ASSERT(MA_OK == ma_property_xml_generator_release(xml_generator));
}

TEST(property_xml_generator_test,test_scenario_prod_not_running) {
	/**/
	size_t size = 0;
	ma_property_bag_t *prop_bag= NULL;
	ma_table_t *table;
	ma_property_xml_generator_t *xml_generator = NULL;

	unsigned char *inc_buffer = NULL;
	unsigned char *full_buffer = NULL;

	property_xml_verification_data_t *incr_data = NULL;
	property_xml_verification_data_t *full_data = NULL;

	TEST_ASSERT(MA_OK == ma_property_xml_generator_create(&xml_generator, g_ds, DS_PROP_PATH, MACHINE_NAME, MACHINE_GUID, NULL, PROPS_VERSION));
	/*Create the verification data manifest*/
	TEST_ASSERT(MA_OK == initialize_property_xml_verification(&incr_data,MA_FALSE));
	TEST_ASSERT(MA_OK == initialize_property_xml_verification(&full_data,MA_TRUE));
	
	add_to_datastore(SYSPROPS1000, g_ds, DS_PROP_PATH);
	/*Create a prop bag and add the predefined setting*/
	TEST_ASSERT(MA_OK == ma_property_bag_create(&prop_bag));
	add_to_prop_bag(SYSPROPS1000,prop_bag);
	TEST_ASSERT(MA_OK == ma_property_bag_get_table(prop_bag, &table));
	TEST_ASSERT(MA_OK == ma_property_xml_generator_append_prop(xml_generator, get_prod_id(SYSPROPS1000), table));
	TEST_ASSERT(MA_OK == ma_table_release(table));
	TEST_ASSERT(MA_OK == ma_property_bag_release(prop_bag));
	{

	}
	{
		add_to_verifier(SYSPROPS1000, full_data);
	}
	add_to_datastore(PCR_____1000, g_ds, DS_PROP_PATH);
	ma_property_xml_generator_set_product_not_running(xml_generator, get_prod_id(PCR_____1000));
	{
		add_product_to_xml_verifier(incr_data, get_prod_id(PCR_____1000), MA_FALSE);
	}
	{
		add_product_to_xml_verifier(full_data, get_prod_id(PCR_____1000), MA_FALSE);
	}
	add_to_datastore(RSD_____4700, g_ds, DS_PROP_PATH);
	ma_property_xml_generator_set_product_not_running(xml_generator, get_prod_id(RSD_____4700));
	{
		add_product_to_xml_verifier(incr_data, get_prod_id(RSD_____4700), MA_FALSE);
	}
	{
		add_product_to_xml_verifier(full_data, get_prod_id(RSD_____4700), MA_FALSE);
	}

	TEST_ASSERT(MA_OK == ma_property_xml_generator_get_incremental_props(xml_generator, &inc_buffer, &size));
	MA_LOG(g_console_logger, MA_LOG_SEV_INFO, "Incr props : %s\n\n\n", inc_buffer);
	TEST_ASSERT(MA_OK == verify_xml_buffer(incr_data,inc_buffer));
	TEST_ASSERT(MA_OK == ma_property_xml_generator_get_full_props(xml_generator, &full_buffer, &size));
	MA_LOG(g_console_logger, MA_LOG_SEV_INFO, "Full Props: %s", full_buffer);
	TEST_ASSERT(MA_OK == verify_xml_buffer(full_data,full_buffer));

	release_property_xml_verification(incr_data);
	release_property_xml_verification(full_data);
	free_data(inc_buffer);
	free_data(full_buffer);
	TEST_ASSERT(MA_OK == ma_property_xml_generator_release(xml_generator));
}

TEST(property_xml_generator_test,test_scenario_prod_installed) {
	/**/
	size_t size = 0;
	ma_property_bag_t *prop_bag= NULL;
	ma_table_t *table;
	ma_property_xml_generator_t *xml_generator = NULL;

	unsigned char *inc_buffer = NULL;
	unsigned char *full_buffer = NULL;

	property_xml_verification_data_t *incr_data = NULL;
	property_xml_verification_data_t *full_data = NULL;
	TEST_ASSERT(MA_OK == ma_property_xml_generator_create(&xml_generator, g_ds, DS_PROP_PATH, MACHINE_NAME, MACHINE_GUID, NULL, PROPS_VERSION));
	
	/*Create the verification data manifest*/
	TEST_ASSERT(MA_OK == initialize_property_xml_verification(&incr_data,MA_FALSE));
	TEST_ASSERT(MA_OK == initialize_property_xml_verification(&full_data,MA_TRUE));
	
	add_to_datastore(SYSPROPS1000, g_ds, DS_PROP_PATH);
	/*Create a prop bag and add the predefined setting*/
	TEST_ASSERT(MA_OK == ma_property_bag_create(& prop_bag));
	add_to_prop_bag(SYSPROPS1000, prop_bag);
	TEST_ASSERT(MA_OK == ma_property_bag_get_table(prop_bag, &table));
	TEST_ASSERT(MA_OK == ma_property_xml_generator_append_prop(xml_generator, get_prod_id(SYSPROPS1000), table));
	TEST_ASSERT(MA_OK == ma_table_release(table));
	TEST_ASSERT(MA_OK == ma_property_bag_release(prop_bag));
	{

	}
	{
		add_to_verifier(SYSPROPS1000, full_data);
	}
	TEST_ASSERT(MA_OK == ma_property_bag_create(& prop_bag));
	add_to_prop_bag(PCR_____1000, prop_bag);
	TEST_ASSERT(MA_OK == ma_property_bag_get_table(prop_bag, &table));
	TEST_ASSERT(MA_OK == ma_property_xml_generator_append_prop(xml_generator, get_prod_id(PCR_____1000), table));
	TEST_ASSERT(MA_OK == ma_table_release(table));
	TEST_ASSERT(MA_OK == ma_property_bag_release(prop_bag));
	{
		add_to_verifier(PCR_____1000, incr_data);
	}
	{
		add_to_verifier(PCR_____1000, full_data);
	}
	TEST_ASSERT(MA_OK == ma_property_bag_create(& prop_bag));
	add_to_prop_bag(RSD_____4700, prop_bag);
	TEST_ASSERT(MA_OK == ma_property_bag_get_table(prop_bag, &table));
	TEST_ASSERT(MA_OK == ma_property_xml_generator_append_prop(xml_generator, get_prod_id(RSD_____4700), table));
	TEST_ASSERT(MA_OK == ma_table_release(table));
	TEST_ASSERT(MA_OK == ma_property_bag_release(prop_bag));
	{
		add_to_verifier(RSD_____4700, incr_data);
	}
	{
		add_to_verifier(RSD_____4700, full_data);
	}

	
	
	TEST_ASSERT(MA_OK == ma_property_xml_generator_get_incremental_props(xml_generator, &inc_buffer, &size));
	MA_LOG(g_console_logger, MA_LOG_SEV_INFO, "Incr props : %s\n\n\n", inc_buffer);
	TEST_ASSERT(MA_OK == verify_xml_buffer(incr_data,inc_buffer));
	TEST_ASSERT(MA_OK == ma_property_xml_generator_get_full_props(xml_generator, &full_buffer, &size));
	MA_LOG(g_console_logger, MA_LOG_SEV_INFO, "Full Props: %s", full_buffer);
	TEST_ASSERT(MA_OK == verify_xml_buffer(full_data,full_buffer));

	release_property_xml_verification(incr_data);
	release_property_xml_verification(full_data);
	free_data(inc_buffer);
	free_data(full_buffer);
	TEST_ASSERT(MA_OK == ma_property_xml_generator_release(xml_generator));
}

TEST(property_xml_generator_test,test_scenario_prod_uninstalled) {
	/**/
	size_t size = 0;
	ma_property_bag_t *prop_bag= NULL;
	ma_table_t *table;
	ma_property_xml_generator_t *xml_generator = NULL;

	unsigned char *inc_buffer = NULL;
	unsigned char *full_buffer = NULL;

	property_xml_verification_data_t *incr_data = NULL;
	property_xml_verification_data_t *full_data = NULL;
	TEST_ASSERT(MA_OK == ma_property_xml_generator_create(&xml_generator, g_ds, DS_PROP_PATH, MACHINE_NAME, MACHINE_GUID, NULL, PROPS_VERSION));
	
	/*Create the verification data manifest*/
	TEST_ASSERT(MA_OK == initialize_property_xml_verification(&incr_data,MA_FALSE));
	TEST_ASSERT(MA_OK == initialize_property_xml_verification(&full_data,MA_TRUE));
	
	add_to_datastore(SYSPROPS1000, g_ds, DS_PROP_PATH);
	/*Create a prop bag and add the predefined setting*/
	TEST_ASSERT(MA_OK == ma_property_bag_create(& prop_bag));
	add_to_prop_bag(SYSPROPS1000, prop_bag);
	TEST_ASSERT(MA_OK == ma_property_bag_get_table(prop_bag, &table));
	TEST_ASSERT(MA_OK == ma_property_xml_generator_append_prop(xml_generator, get_prod_id(SYSPROPS1000), table));
	TEST_ASSERT(MA_OK == ma_table_release(table));
	TEST_ASSERT(MA_OK == ma_property_bag_release(prop_bag));
	{

	}
	{
		add_to_verifier(SYSPROPS1000, full_data);
	}
	add_to_datastore(PCR_____1000, g_ds, DS_PROP_PATH);
	{
		add_product_to_xml_verifier(incr_data, get_prod_id(PCR_____1000), MA_TRUE);
	}
	{
		add_product_to_xml_verifier(full_data, get_prod_id(PCR_____1000), MA_TRUE);
	}
	add_to_datastore(RSD_____4700, g_ds, DS_PROP_PATH);
	{
		add_product_to_xml_verifier(incr_data, get_prod_id(RSD_____4700), MA_TRUE);
	}
	{
		add_product_to_xml_verifier(full_data, get_prod_id(RSD_____4700), MA_TRUE);
	}

	
	
	TEST_ASSERT(MA_OK == ma_property_xml_generator_get_incremental_props(xml_generator, &inc_buffer, &size));
	MA_LOG(g_console_logger, MA_LOG_SEV_INFO, "Incr props : %s\n\n\n", inc_buffer);
	TEST_ASSERT(MA_OK == verify_xml_buffer(incr_data,inc_buffer));
	TEST_ASSERT(MA_OK == ma_property_xml_generator_get_full_props(xml_generator, &full_buffer, &size));
	MA_LOG(g_console_logger, MA_LOG_SEV_INFO, "Full Props: %s", full_buffer);
	TEST_ASSERT(MA_OK == verify_xml_buffer(full_data,full_buffer));

	release_property_xml_verification(incr_data);
	release_property_xml_verification(full_data);
	free_data(inc_buffer);
	free_data(full_buffer);
	TEST_ASSERT(MA_OK == ma_property_xml_generator_release(xml_generator));
}
