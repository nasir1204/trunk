/*****************************************************************************
Main Test runner for all macrypto  unit tests.
******************************************************************************/

#include "unity.h"
#include "unity_fixture.h"
#include "ma/ma_common.h"


#ifdef MA_WINDOWS
#include <windows.h>
#endif
#ifdef _MSC_VER
# include <crtdbg.h> /* */
# define MA_TRACKLEAKS _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF)
#else
# define MA_TRACKLEAKS ((void *)0)
#endif

#include "ma/logger/ma_logger.h"
#include "ma/logger/ma_console_logger.h"

ma_logger_t *g_console_logger = NULL;

static void run_all_tests(void)
{    
    do { RUN_TEST_GROUP(property_xml_generator_utility_test_group);} while(0);
    do { RUN_TEST_GROUP(property_xml_generator_test_group);} while(0);
}

int main(int argc, char *argv[]) {
	int ret = 0;
	ma_console_logger_t *clog = NULL;
    MA_TRACKLEAKS;
	if(MA_OK == ma_console_logger_create(&clog)) {
		g_console_logger = (ma_logger_t *)clog;
		ret = UnityMain(argc, argv, run_all_tests);
		ma_console_logger_release(clog);
		clog = NULL;
		g_console_logger = NULL;
	}
    return ret;
}
