#include "unity.h"
#include "unity_fixture.h"
#include "ma/internal/services/property/property_xml_generator.h"
#include "xml_generator_utilities.h"
#include "ma_property_xml_generator_test_helper.h"
#include "ma/ma_variant.h"
#include <string.h>
#include <stdlib.h>

#include "ma/datastore/ma_ds_database.h"

#ifdef MA_WINDOWS
#else
#include <unistd.h>
#endif



#define LOCAL_DATABASE_NAME_FOR_UT    "l_datastoreut.db" 

#define GLOBAL_DATABASE_NAME_FOR_UT    "g_datastoreut.db" 

#define POLICIES_INSTANCE_NAME          "Policies"

#define TEMP_INSTANCE_NAME              "TempData"

static ma_db_t *g_database_handle = NULL ;
static ma_ds_t *g_ds = NULL ;
static ma_buffer_t *buffer = NULL;

TEST_GROUP_RUNNER(property_xml_generator_utility_test_group)
{
	do {RUN_TEST_CASE(property_xml_generator_utility_tests, validate_variant_type_test)} while (0);
	
	do {RUN_TEST_CASE(property_xml_generator_utility_tests, find_product_in_table_test)} while (0);
	do {RUN_TEST_CASE(property_xml_generator_utility_tests, find_section_in_table_test)} while (0);
	do {RUN_TEST_CASE(property_xml_generator_utility_tests, find_setting_in_table_test)} while (0);
	
	do {RUN_TEST_CASE(property_xml_generator_utility_tests, create_new_table_with_id_test)} while (0);
	do {RUN_TEST_CASE(property_xml_generator_utility_tests, add_string_to_table_test)} while (0);
	
	do {RUN_TEST_CASE(property_xml_generator_utility_tests, merge_property_test)} while (0);
	
	do {RUN_TEST_CASE(property_xml_generator_utility_tests, remove_path_from_db_test)} while (0);
	
	do {RUN_TEST_CASE(property_xml_generator_utility_tests, find_property_xml_setting_node_test)} while (0);
	do {RUN_TEST_CASE(property_xml_generator_utility_tests, create_property_xml_setting_node_test)} while (0);
	do {RUN_TEST_CASE(property_xml_generator_utility_tests, find_or_create_property_xml_setting_node_test)} while (0);
	
	do {RUN_TEST_CASE(property_xml_generator_utility_tests, find_xml_product_section_test)} while (0);
	do {RUN_TEST_CASE(property_xml_generator_utility_tests, create_xml_product_section_test)} while (0);
	do {RUN_TEST_CASE(property_xml_generator_utility_tests, find_or_create_xml_product_section_test)} while (0);
	
	do {RUN_TEST_CASE(property_xml_generator_utility_tests, find_xml_product_test)} while (0);
	do {RUN_TEST_CASE(property_xml_generator_utility_tests, create_xml_product_test)} while (0);
	do {RUN_TEST_CASE(property_xml_generator_utility_tests, find_or_create_xml_product_test)} while (0);
	
	do {RUN_TEST_CASE(property_xml_generator_utility_tests, find_xml_property_root_test)} while (0);
	do {RUN_TEST_CASE(property_xml_generator_utility_tests, create_xml_property_root_test)} while (0);
	do {RUN_TEST_CASE(property_xml_generator_utility_tests, find_or_create_xml_property_root_test)} while (0);

	do {RUN_TEST_CASE(property_xml_generator_utility_tests, mark_deleted_products_test)} while (0);
	do {RUN_TEST_CASE(property_xml_generator_utility_tests, mark_deleted_product_settings_test)} while (0);
	do {RUN_TEST_CASE(property_xml_generator_utility_tests, form_path_test)} while (0);
	do {RUN_TEST_CASE(property_xml_generator_utility_tests, is_product_test_test)} while (0);
}

TEST_SETUP(property_xml_generator_utility_tests) {
    ma_ds_database_t *ds_database = NULL;
    unlink(GLOBAL_DATABASE_NAME_FOR_UT);
    TEST_ASSERT_MESSAGE(MA_OK ==  ma_db_open( GLOBAL_DATABASE_NAME_FOR_UT,MA_DB_OPEN_READWRITE,  &g_database_handle), "ma_db_open failed to open global database datatbase");
    TEST_ASSERT_MESSAGE(MA_OK ==  ma_ds_database_create(g_database_handle , "Properties" , &ds_database), "ma_ds_database_open failed to create the instance");
	g_ds = (ma_ds_t *)ds_database;
}

TEST_TEAR_DOWN(property_xml_generator_utility_tests) {
	TEST_ASSERT_MESSAGE(MA_OK ==  ma_db_close(g_database_handle), "ma_db_close failed to close the global datatbase");
    TEST_ASSERT_MESSAGE(MA_OK ==  ma_ds_database_release((ma_ds_database_t*)g_ds), "ma_ds_database failed to release the instance");
    
	if(buffer){
		(void)ma_buffer_release(buffer);
	}
    unlink(GLOBAL_DATABASE_NAME_FOR_UT);
}
#define DS_PROP_PATH "Properties\\EPO\\"
#define MACHINE_NAME "teamcity"
#define MACHINE_GUID "{1234-5555-666677778888-9999}"
#define PROPS_VERSION "20130521101520"

 


TEST(property_xml_generator_utility_tests, validate_variant_type_test) {
	ma_variant_t *variant = NULL;
	TEST_ASSERT(MA_OK == ma_variant_create(&variant));
	TEST_ASSERT(MA_TRUE == validate_variant_type(variant, MA_VARTYPE_NULL));
	TEST_ASSERT(MA_OK == ma_variant_release(variant));
	TEST_ASSERT(MA_OK == ma_variant_create_from_bool(MA_TRUE,&variant));
	TEST_ASSERT(MA_TRUE == validate_variant_type(variant, MA_VARTYPE_BOOL));
	TEST_ASSERT(MA_OK == ma_variant_release(variant));
	TEST_ASSERT(MA_OK == ma_variant_create_from_int16(12,&variant));
	TEST_ASSERT(MA_TRUE == validate_variant_type(variant, MA_VARTYPE_INT16));
	TEST_ASSERT(MA_OK == ma_variant_release(variant));
	TEST_ASSERT(MA_OK == ma_variant_create_from_int32(12,&variant));
	TEST_ASSERT(MA_TRUE == validate_variant_type(variant, MA_VARTYPE_INT32));
	TEST_ASSERT(MA_OK == ma_variant_release(variant));
	TEST_ASSERT(MA_OK == ma_variant_create_from_int64(12,&variant));
	TEST_ASSERT(MA_TRUE == validate_variant_type(variant, MA_VARTYPE_INT64));
	TEST_ASSERT(MA_OK == ma_variant_release(variant));
	TEST_ASSERT(MA_OK == ma_variant_create_from_float(12.0f,&variant));
	TEST_ASSERT(MA_TRUE == validate_variant_type(variant, MA_VARTYPE_FLOAT));
	TEST_ASSERT(MA_OK == ma_variant_release(variant));
	TEST_ASSERT(MA_OK == ma_variant_create_from_string(MA_STR_PROD_PROPERTY, strlen(MA_STR_PROD_PROPERTY),&variant));
	TEST_ASSERT(MA_TRUE == validate_variant_type(variant, MA_VARTYPE_STRING));
	TEST_ASSERT(MA_OK == ma_variant_release(variant));

	TEST_ASSERT(MA_FALSE == validate_variant_type(NULL, MA_VARTYPE_NULL));
}

TEST(property_xml_generator_utility_tests, find_product_in_table_test) {
	ma_table_t *table = NULL;
	ma_table_t *full_prop = NULL;
	ma_table_t *temp = NULL;
	ma_variant_t *variant = NULL;

	TEST_ASSERT(MA_FALSE == find_product_in_table(NULL, MA_SYSTEM_PROP_PROVIDER_ID, &table));
	TEST_ASSERT(MA_OK == ma_table_create(&full_prop));
	TEST_ASSERT(MA_FALSE == find_product_in_table(full_prop, NULL, &table));
	TEST_ASSERT(MA_FALSE == find_product_in_table(full_prop, MA_SYSTEM_PROP_PROVIDER_ID, NULL));
	TEST_ASSERT(MA_OK == ma_variant_create_from_string(MA_SYSTEM_PROP_PROVIDER_ID, strlen(MA_SYSTEM_PROP_PROVIDER_ID), &variant));
	TEST_ASSERT(MA_OK == ma_table_add_entry(full_prop, MA_SYSTEM_PROP_PROVIDER_ID, variant));
	TEST_ASSERT(MA_FALSE == find_product_in_table(full_prop, MA_SYSTEM_PROP_PROVIDER_ID, &table));
	TEST_ASSERT(MA_OK == ma_variant_release(variant));
	TEST_ASSERT(MA_OK == ma_table_release(full_prop));
	
	TEST_ASSERT(MA_OK == ma_table_create(&full_prop));

	TEST_ASSERT(MA_OK == ma_table_create(&temp));
	TEST_ASSERT(MA_OK == ma_variant_create_from_table(temp, &variant));
	TEST_ASSERT(MA_OK == ma_table_release(temp));
	TEST_ASSERT(MA_OK == ma_table_add_entry(full_prop, MA_SYSTEM_PROP_PROVIDER_ID, variant));
	TEST_ASSERT(MA_OK == ma_variant_release(variant));

	TEST_ASSERT(MA_TRUE == find_product_in_table(full_prop, MA_SYSTEM_PROP_PROVIDER_ID, &table) && table != NULL);
	TEST_ASSERT(MA_OK == ma_table_release(table));

	
	TEST_ASSERT(MA_OK == ma_table_create(&temp));
	TEST_ASSERT(MA_OK == ma_variant_create_from_table(temp, &variant));
	TEST_ASSERT(MA_OK == ma_table_release(temp));
	TEST_ASSERT(MA_OK == ma_table_add_entry(full_prop, "RSD_____4700", variant));
	TEST_ASSERT(MA_OK == ma_variant_release(variant));
	TEST_ASSERT(MA_TRUE == find_product_in_table(full_prop, "RSD_____4700", &table) && table != NULL);
	TEST_ASSERT(MA_OK == ma_table_release(table));
	TEST_ASSERT(MA_OK == ma_table_release(full_prop));
}

TEST(property_xml_generator_utility_tests, find_section_in_table_test) {
	ma_table_t *table = NULL;
	ma_table_t *full_prop = NULL;
	ma_table_t *temp = NULL;
	ma_variant_t *variant = NULL;

	TEST_ASSERT(MA_FALSE == find_product_in_table(NULL, MA_SYSTEM_PROP_PROVIDER_ID, &table));
	TEST_ASSERT(MA_OK == ma_table_create(&full_prop));
	TEST_ASSERT(MA_FALSE == find_product_in_table(full_prop, NULL, &table));
	TEST_ASSERT(MA_FALSE == find_product_in_table(full_prop, MA_SYSTEM_PROP_PROVIDER_ID, NULL));
	TEST_ASSERT(MA_OK == ma_variant_create_from_string(MA_SYSTEM_PROP_PROVIDER_ID, strlen(MA_SYSTEM_PROP_PROVIDER_ID), &variant));
	TEST_ASSERT(MA_OK == ma_table_add_entry(full_prop, MA_SYSTEM_PROP_PROVIDER_ID, variant));
	TEST_ASSERT(MA_FALSE == find_product_in_table(full_prop, MA_SYSTEM_PROP_PROVIDER_ID, &table));
	TEST_ASSERT(MA_OK == ma_variant_release(variant));
	TEST_ASSERT(MA_OK == ma_table_release(full_prop));
	
	TEST_ASSERT(MA_OK == ma_table_create(&full_prop));

	TEST_ASSERT(MA_OK == ma_table_create(&temp));
	TEST_ASSERT(MA_OK == ma_variant_create_from_table(temp, &variant));
	TEST_ASSERT(MA_OK == ma_table_release(temp));
	TEST_ASSERT(MA_OK == ma_table_add_entry(full_prop, "Non-General", variant));
	TEST_ASSERT(MA_OK == ma_variant_release(variant));

	TEST_ASSERT(MA_TRUE == find_product_in_table(full_prop, "Non-General", &table) && table != NULL);
	TEST_ASSERT(MA_OK == ma_table_release(table));

	
	TEST_ASSERT(MA_OK == ma_table_create(&temp));
	TEST_ASSERT(MA_OK == ma_variant_create_from_table(temp, &variant));
	TEST_ASSERT(MA_OK == ma_table_release(temp));
	TEST_ASSERT(MA_OK == ma_table_add_entry(full_prop, "General", variant));
	TEST_ASSERT(MA_OK == ma_variant_release(variant));
	TEST_ASSERT(MA_TRUE == find_product_in_table(full_prop, "General", &table) && table != NULL);
	TEST_ASSERT(MA_OK == ma_table_release(table));
	TEST_ASSERT(MA_OK == ma_table_release(full_prop));
}
TEST(property_xml_generator_utility_tests, find_setting_in_table_test) {
	ma_table_t *setting_table = NULL;
	char *value = NULL;
	ma_variant_t *variant = NULL;

	TEST_ASSERT(MA_FALSE == find_setting_in_table(NULL, "AVProtection", &value));
	TEST_ASSERT(MA_OK == ma_table_create(&setting_table));
	TEST_ASSERT(MA_FALSE == find_setting_in_table(setting_table, NULL, &value));
	TEST_ASSERT(MA_FALSE == find_setting_in_table(setting_table, "AVProtection", NULL));
	TEST_ASSERT(MA_OK == ma_variant_create_from_string("AVProtection", strlen("AVProtection"), &variant));
	TEST_ASSERT(MA_OK == ma_table_add_entry(setting_table,"AVProtection", variant));
	TEST_ASSERT(MA_OK == ma_variant_release(variant));
	TEST_ASSERT(MA_OK == ma_table_release(setting_table));

	TEST_ASSERT(MA_OK == ma_table_create(&setting_table));
	TEST_ASSERT(MA_OK == ma_variant_create_from_string("1", strlen("1"), &variant));
	TEST_ASSERT(MA_OK == ma_table_add_entry(setting_table,"AVProtection", variant));
	TEST_ASSERT(MA_OK == ma_variant_release(variant));
	TEST_ASSERT(MA_TRUE == find_setting_in_table(setting_table, "AVProtection", &value));
	TEST_ASSERT(0 == strcmp(value, "1"));
	free_data(value);
	
	TEST_ASSERT(MA_FALSE == find_setting_in_table(setting_table, "APProtection", &value));

	
	TEST_ASSERT(MA_OK == ma_variant_create_from_string("2", strlen("2"), &variant));
	TEST_ASSERT(MA_OK == ma_table_add_entry(setting_table,"APProtection", variant));
	TEST_ASSERT(MA_OK == ma_variant_release(variant));
	TEST_ASSERT(MA_TRUE == find_setting_in_table(setting_table, "APProtection", &value));
	TEST_ASSERT(0 == strcmp(value, "2"));
	free_data(value);
	TEST_ASSERT(MA_OK == ma_table_release(setting_table));
}
	
TEST(property_xml_generator_utility_tests, create_new_table_with_id_test) {
	ma_table_t *table = NULL;
	ma_variant_t *variant = NULL;
	size_t size = 0;
	ma_table_t *new_table = NULL;
	TEST_ASSERT(MA_ERROR_INVALIDARG == create_new_table_with_id(NULL, "Product", &new_table));
	TEST_ASSERT(MA_OK == ma_table_create(&table));
	TEST_ASSERT(MA_ERROR_INVALIDARG == create_new_table_with_id(table, NULL, &new_table));
	TEST_ASSERT(MA_ERROR_INVALIDARG == create_new_table_with_id(table, "Product", NULL));
	TEST_ASSERT(MA_OK == ma_table_release(table));
	
	TEST_ASSERT(MA_OK == ma_table_create(&table));
	TEST_ASSERT(MA_ERROR_OBJECTNOTFOUND == ma_table_get_value(table, "Product", &variant));
	TEST_ASSERT(MA_OK == create_new_table_with_id(table, "Product", &new_table));
	TEST_ASSERT(MA_OK == ma_table_release(new_table));
	TEST_ASSERT(MA_OK == ma_table_get_value(table, "Product", &variant));
	TEST_ASSERT(MA_TRUE == validate_variant_type(variant, MA_VARTYPE_TABLE));
	TEST_ASSERT(MA_OK == ma_table_size(table, &size) && size == 1);
	TEST_ASSERT(MA_OK == ma_variant_get_table(variant, &new_table));
	TEST_ASSERT(MA_OK == ma_table_release(new_table));
	TEST_ASSERT(MA_OK == ma_variant_release(variant));
	TEST_ASSERT(MA_OK == ma_table_release(table));
}
TEST(property_xml_generator_utility_tests, add_string_to_table_test){
	ma_table_t *table = NULL;
	ma_variant_t *variant = NULL;
	ma_buffer_t *buffer = NULL;
	const char *value = NULL;
	size_t size = 0;
	TEST_ASSERT(MA_ERROR_INVALIDARG == add_string_to_table(table, "AVProtection", "off", strlen("off")));
	TEST_ASSERT(MA_OK == ma_table_create(&table));
	TEST_ASSERT(MA_ERROR_INVALIDARG == add_string_to_table(table, NULL, "off", strlen("off")));
	TEST_ASSERT(MA_ERROR_INVALIDARG == add_string_to_table(table, "AVProtection", NULL, strlen("off")));
	TEST_ASSERT(MA_OK == ma_table_release(table));

	
	TEST_ASSERT(MA_OK == ma_table_create(&table));
	TEST_ASSERT(MA_ERROR_OBJECTNOTFOUND == ma_table_get_value(table, "AVProtection", &variant));
	TEST_ASSERT(MA_OK == add_string_to_table(table, "AVProtection", "off", strlen("off")));
	TEST_ASSERT(MA_OK == ma_table_get_value(table, "AVProtection", &variant));
	TEST_ASSERT(MA_OK == ma_variant_get_string_buffer(variant, &buffer));
	TEST_ASSERT(MA_OK == ma_buffer_get_string(buffer, &value, &size));
	TEST_ASSERT(0 == strcmp(value,"off") && size == 3);
	TEST_ASSERT(MA_OK == ma_buffer_release(buffer));
	TEST_ASSERT(MA_OK == ma_variant_release(variant));
	TEST_ASSERT(MA_OK == ma_table_release(table));
}
	
TEST(property_xml_generator_utility_tests, merge_property_test){
	ma_table_t *table = NULL;
	ma_table_t *setting_table = NULL;
	ma_table_t *section_table = NULL;

	char *value = NULL;
	TEST_ASSERT(MA_ERROR_INVALIDARG == merge_property_to_full_props(table,"VIRUSCAN8800","General","AVProtection", "1"));
	TEST_ASSERT(MA_OK == ma_table_create(&table));
	TEST_ASSERT(MA_ERROR_INVALIDARG == merge_property_to_full_props(table,NULL,"General","AVProtection", "1"));
	TEST_ASSERT(MA_ERROR_INVALIDARG == merge_property_to_full_props(table,"VIRUSCAN8800","General",NULL, "1"));
	TEST_ASSERT(MA_ERROR_INVALIDARG == merge_property_to_full_props(table,"VIRUSCAN8800","General","AVProtection", NULL));
	TEST_ASSERT(MA_OK == ma_table_release(table));

	TEST_ASSERT(MA_OK == ma_table_create(&table));
	TEST_ASSERT(MA_OK == merge_property_to_full_props(table,"VIRUSCAN8800","General","AVProtection", "1"));
	TEST_ASSERT(MA_TRUE == find_product_in_table(table,"VIRUSCAN8800",&section_table));
	TEST_ASSERT(MA_TRUE == find_section_in_table(section_table,"General",&setting_table));
	TEST_ASSERT(MA_TRUE == find_setting_in_table(setting_table, "AVProtection", &value));
	TEST_ASSERT(0 == strcmp(value,"1"));
	free_data(value);
	TEST_ASSERT(MA_OK == ma_table_release(setting_table))
	TEST_ASSERT(MA_OK == ma_table_release(section_table));

	TEST_ASSERT(MA_OK == merge_property_to_full_props(table,"VIRUSCAN8800","General","ApProtection", "2"));
	TEST_ASSERT(MA_TRUE == find_product_in_table(table,"VIRUSCAN8800",&section_table));
	TEST_ASSERT(MA_TRUE == find_section_in_table(section_table,"General",&setting_table));
	TEST_ASSERT(MA_TRUE == find_setting_in_table(setting_table, "AVProtection", &value));
	TEST_ASSERT(0 == strcmp(value,"1"));
	free_data(value);
	TEST_ASSERT(MA_TRUE == find_setting_in_table(setting_table, "ApProtection", &value));
	TEST_ASSERT(0 == strcmp(value,"2"));
	free_data(value);
	TEST_ASSERT(MA_OK == ma_table_release(setting_table))
	TEST_ASSERT(MA_OK == ma_table_release(section_table));
	TEST_ASSERT(MA_OK == ma_table_release(table));
}
	
TEST(property_xml_generator_utility_tests, remove_path_from_db_test){
	ma_temp_buffer_t rsd_product_path;
	ma_temp_buffer_t rsd_section_path1;
	ma_temp_buffer_t rsd_section_path2;

	ma_temp_buffer_t vscan_product_path;
	ma_temp_buffer_t vscan_section_path1;
	ma_temp_buffer_t vscan_section_path2;

	ma_temp_buffer_init(&rsd_product_path);
	ma_temp_buffer_init(&rsd_section_path1);
	ma_temp_buffer_init(&rsd_section_path2);
	ma_temp_buffer_init(&vscan_product_path);
	ma_temp_buffer_init(&vscan_section_path1);
	ma_temp_buffer_init(&vscan_section_path2);
	add_to_datastore(RSD_____4700, g_ds, DS_PROP_PATH);
	add_to_datastore(VIRUSCAN8800, g_ds, DS_PROP_PATH);
	
	form_path(&rsd_product_path,DS_PROP_PATH, get_prod_id(RSD_____4700),MA_TRUE);
	form_path(&rsd_section_path1, (char*)ma_temp_buffer_get(&rsd_product_path), "General",MA_TRUE);
	form_path(&rsd_section_path2, (char*)ma_temp_buffer_get(&rsd_product_path), "LocalElect",MA_TRUE);

	form_path(&vscan_product_path,DS_PROP_PATH, get_prod_id(VIRUSCAN8800),MA_TRUE);
	form_path(&vscan_section_path1, (char*)ma_temp_buffer_get(&vscan_product_path), "Access Protection",MA_TRUE);
	form_path(&vscan_section_path2, (char*)ma_temp_buffer_get(&vscan_product_path), "Access Protection Reports",MA_TRUE);
	
	TEST_ASSERT(MA_OK == ma_ds_get_str(g_ds, (char*)ma_temp_buffer_get(&vscan_section_path1), "APEnabled", &buffer));
	TEST_ASSERT(MA_OK == ma_buffer_release(buffer));
	TEST_ASSERT(MA_OK == remove_path_from_db(g_ds, (char*)ma_temp_buffer_get(&vscan_section_path1)));
	
	TEST_ASSERT(MA_OK != ma_ds_get_str(g_ds, (char*)ma_temp_buffer_get(&vscan_section_path1), "APEnabled", &buffer));
	TEST_ASSERT(MA_OK == ma_ds_get_str(g_ds, (char*)ma_temp_buffer_get(&vscan_section_path2), "LogFileFormat", &buffer));
	TEST_ASSERT(MA_OK == ma_buffer_release(buffer));

	
	TEST_ASSERT(MA_OK != ma_ds_get_str(g_ds, (char*)ma_temp_buffer_get(&vscan_section_path1), "APEnabled", &buffer));
	TEST_ASSERT(MA_OK == ma_ds_get_str(g_ds, (char*)ma_temp_buffer_get(&vscan_section_path2), "LogFileFormat", &buffer));
	TEST_ASSERT(MA_OK == ma_buffer_release(buffer));

	
	TEST_ASSERT(MA_OK == ma_ds_get_str(g_ds, (char*)ma_temp_buffer_get(&rsd_section_path1), "szInstallDir", &buffer));
	TEST_ASSERT(MA_OK == ma_buffer_release(buffer));

	remove_path_from_db(g_ds, (char*)ma_temp_buffer_get(&vscan_product_path));

	TEST_ASSERT(MA_OK == ma_ds_get_str(g_ds, (char*)ma_temp_buffer_get(&rsd_section_path1), "szInstallDir", &buffer));
	TEST_ASSERT(MA_OK == ma_buffer_release(buffer));
	TEST_ASSERT(MA_OK != ma_ds_get_str(g_ds, (char*)ma_temp_buffer_get(&vscan_section_path1), "APEnabled", &buffer));
	TEST_ASSERT(MA_OK != ma_ds_get_str(g_ds, (char*)ma_temp_buffer_get(&vscan_section_path2), "LogFileFormat", &buffer));
	

	TEST_ASSERT(MA_ERROR_INVALIDARG == remove_path_from_db(NULL,DS_PROP_PATH));
	TEST_ASSERT(MA_ERROR_INVALIDARG == remove_path_from_db(g_ds, NULL));
	ma_temp_buffer_uninit(&rsd_product_path);
	ma_temp_buffer_uninit(&rsd_section_path1);
	ma_temp_buffer_uninit(&rsd_section_path2);
	ma_temp_buffer_uninit(&vscan_product_path);
	ma_temp_buffer_uninit(&vscan_section_path1);
	ma_temp_buffer_uninit(&vscan_section_path2);
}
	
TEST(property_xml_generator_utility_tests, find_property_xml_setting_node_test){
}
TEST(property_xml_generator_utility_tests, create_property_xml_setting_node_test){
}
TEST(property_xml_generator_utility_tests, find_or_create_property_xml_setting_node_test){
}
	
TEST(property_xml_generator_utility_tests, find_xml_product_section_test){
}
TEST(property_xml_generator_utility_tests, create_xml_product_section_test){
}
TEST(property_xml_generator_utility_tests, find_or_create_xml_product_section_test){
}
	
TEST(property_xml_generator_utility_tests, find_xml_product_test){
}
TEST(property_xml_generator_utility_tests, create_xml_product_test){
}
TEST(property_xml_generator_utility_tests, find_or_create_xml_product_test){
}
	
TEST(property_xml_generator_utility_tests, find_xml_property_root_test){
}
TEST(property_xml_generator_utility_tests, create_xml_property_root_test){
}
TEST(property_xml_generator_utility_tests, find_or_create_xml_property_root_test){
}

TEST(property_xml_generator_utility_tests, mark_deleted_products_test) {
}
TEST(property_xml_generator_utility_tests, mark_deleted_product_settings_test){
}
TEST(property_xml_generator_utility_tests, form_path_test){
	ma_temp_buffer_t path;
	ma_temp_buffer_init(&path);

	TEST_ASSERT(MA_ERROR_INVALIDARG == form_path(&path, "", "",MA_TRUE));
	TEST_ASSERT(MA_ERROR_INVALIDARG == form_path(NULL, "", "",MA_TRUE));
	TEST_ASSERT(MA_ERROR_INVALIDARG == form_path(&path, NULL, "",MA_TRUE));
	TEST_ASSERT(MA_ERROR_INVALIDARG == form_path(&path, "", NULL,MA_TRUE));
	
	TEST_ASSERT(MA_OK == form_path(&path, "", "BB",MA_TRUE));
	TEST_ASSERT(0 == strcmp((char*)ma_temp_buffer_get(&path), "BB"));
	
	TEST_ASSERT(MA_OK == form_path(&path, "AA", "",MA_TRUE));
	TEST_ASSERT(0 == strcmp((char*)ma_temp_buffer_get(&path), "AA"));
	
	TEST_ASSERT(MA_OK == form_path(&path, "AA", "BB",MA_TRUE));
	TEST_ASSERT(0 == strcmp((char*)ma_temp_buffer_get(&path), "AA\\BB"));

	TEST_ASSERT(MA_OK == form_path(&path, "AA\\", "BB",MA_TRUE));
	TEST_ASSERT(0 == strcmp((char*)ma_temp_buffer_get(&path), "AA\\BB"));

	TEST_ASSERT(MA_OK == form_path(&path, "AA", "\\BB",MA_TRUE));
	TEST_ASSERT(0 == strcmp((char*)ma_temp_buffer_get(&path), "AA\\BB"));

	TEST_ASSERT(MA_OK == form_path(&path, "AA\\", "\\BB",MA_TRUE));
	TEST_ASSERT(0 == strcmp((char*)ma_temp_buffer_get(&path), "AA\\BB"));

	
	TEST_ASSERT(MA_ERROR_INVALIDARG == form_path(&path, "", "",MA_FALSE));
	TEST_ASSERT(MA_ERROR_INVALIDARG == form_path(NULL, "", "",MA_FALSE));
	TEST_ASSERT(MA_ERROR_INVALIDARG == form_path(&path, NULL, "",MA_FALSE));
	TEST_ASSERT(MA_ERROR_INVALIDARG == form_path(&path, "", NULL,MA_FALSE));
	
	TEST_ASSERT(MA_OK == form_path(&path, "", "BB",MA_FALSE));
	TEST_ASSERT(0 == strcmp((char*)ma_temp_buffer_get(&path), "BB"));
	
	TEST_ASSERT(MA_OK == form_path(&path, "AA", "",MA_FALSE));
	TEST_ASSERT(0 == strcmp((char*)ma_temp_buffer_get(&path), "AA"));
	
	TEST_ASSERT(MA_OK == form_path(&path, "AA", "BB",MA_FALSE));
#ifdef MA_WINDOWS
	TEST_ASSERT(0 == strcmp((char*)ma_temp_buffer_get(&path), "AA\\BB"));
#else
	TEST_ASSERT(0 == strcmp((char*)ma_temp_buffer_get(&path), "AA/BB"));
#endif
	
	TEST_ASSERT(MA_OK == form_path(&path, "AA\\", "BB",MA_FALSE));
#ifdef MA_WINDOWS
	TEST_ASSERT(0 == strcmp((char*)ma_temp_buffer_get(&path), "AA\\BB"));
#else
	TEST_ASSERT(0 == strcmp((char*)ma_temp_buffer_get(&path), "AA\\/BB"));
#endif

	TEST_ASSERT(MA_OK == form_path(&path, "AA/", "BB",MA_FALSE));
#ifdef MA_WINDOWS
	TEST_ASSERT(0 == strcmp((char*)ma_temp_buffer_get(&path), "AA/\\BB"));
#else
	TEST_ASSERT(0 == strcmp((char*)ma_temp_buffer_get(&path), "AA/BB"));
#endif
	
	TEST_ASSERT(MA_OK == form_path(&path, "AA", "\\BB",MA_FALSE));
#ifdef MA_WINDOWS
	TEST_ASSERT(0 == strcmp((char*)ma_temp_buffer_get(&path), "AA\\BB"));
#else
	TEST_ASSERT(0 == strcmp((char*)ma_temp_buffer_get(&path), "AA/\\BB"));
#endif

	TEST_ASSERT(MA_OK == form_path(&path, "AA", "/BB",MA_FALSE));
#ifdef MA_WINDOWS
	TEST_ASSERT(0 == strcmp((char*)ma_temp_buffer_get(&path), "AA\\/BB"));
#else
	TEST_ASSERT(0 == strcmp((char*)ma_temp_buffer_get(&path), "AA/BB"));
#endif
	
	TEST_ASSERT(MA_OK == form_path(&path, "AA\\", "\\BB",MA_FALSE));
#ifdef MA_WINDOWS
	TEST_ASSERT(0 == strcmp((char*)ma_temp_buffer_get(&path), "AA\\BB"));
#else
	TEST_ASSERT(0 == strcmp((char*)ma_temp_buffer_get(&path), "AA\\/\\BB"));
#endif

	TEST_ASSERT(MA_OK == form_path(&path, "AA/", "/BB",MA_FALSE));
#ifdef MA_WINDOWS
	TEST_ASSERT(0 == strcmp((char*)ma_temp_buffer_get(&path), "AA/\\/BB"));
#else
	TEST_ASSERT(0 == strcmp((char*)ma_temp_buffer_get(&path), "AA/BB"));
#endif
	ma_temp_buffer_uninit(&path);
}

TEST(property_xml_generator_utility_tests, is_product_test_test){
}


	