#ifndef MA_PROPERTY_XML_GENERATOR_TEST_HELPER_H_
#define MA_PROPERTY_XML_GENERATOR_TEST_HELPER_H_

#include "ma/ma_common.h"
#include "ma/property/ma_property_bag.h"
#include "ma/internal/utils/xml/ma_xml.h"
#include "ma/internal/utils/datastructures/ma_slist.h"
#define LOG_FACILITY_NAME "property_xml_generator_tests"
MA_CPP(extern "C" { )

#define DS_PROP_PATH "Properties\\EPO\\"
#define MACHINE_NAME "teamcity"
#define MACHINE_GUID "{1234-5555-666677778888-9999}"
#define PROPS_VERSION "20130521101520"

typedef enum prop_providers_e{ 
	SYSPROPS1000,
	PCR_____1000,
	RSD_____4700,
	VIRUSCAN8800,
}prop_providers_t;



void add_setting_to_database( ma_ds_t *db, const char *db_path, const char *product_id, const char *section_name, const char *setting, const char *value) ;
void add_setting_to_prop_bag( ma_property_bag_t *bag, const char *section_name, const char *setting_name,const char *value);

void add_to_prop_bag(prop_providers_t product_id, ma_property_bag_t *bag);
void add_to_prop_bag_sysprops1000(ma_property_bag_t *bag);
void add_to_prop_bag_pcr_____1000(ma_property_bag_t *bag);
void add_to_prop_bag_rsd_____4700(ma_property_bag_t *bag);
void add_to_prop_bag_viruscan8800(ma_property_bag_t *bag);

void add_to_datastore(prop_providers_t product_id, ma_ds_t *db, const char *root_path);
void add_to_datastore_sysprops1000(ma_ds_t *db, const char *root_path);
void add_to_datastore_pcr_____1000(ma_ds_t *db, const char *root_path);
void add_to_datastore_rsd_____4700(ma_ds_t *db, const char *root_path);
void add_to_datastore_viruscan8800(ma_ds_t *db, const char *root_path);


ma_error_t create_xml_for_verification(const unsigned char *buffer, ma_xml_t **xml)  ;

const char *get_prod_id (prop_providers_t id);
void free_data(void *data);

typedef struct setting_verification_data_s {
	char *setting_name;
	char *expected_value;
	ma_bool_t will_be_missing;
	ma_bool_t will_be_deleted;
	MA_SLIST_NODE_DEFINE(struct setting_verification_data_s);
}setting_verification_data_t;

typedef struct section_verification_data_s {
	char *section_name;
	ma_bool_t is_product_id;
	ma_bool_t will_be_missing;
	MA_SLIST_DEFINE(settings,setting_verification_data_t);
	MA_SLIST_NODE_DEFINE(struct section_verification_data_s);
}section_verification_data_t;

typedef struct product_verification_data_s{
	char		*id;
	ma_bool_t	deleted;
	ma_bool_t	is_product;
	MA_SLIST_DEFINE(sections,section_verification_data_t);
	MA_SLIST_NODE_DEFINE(struct product_verification_data_s);
}product_verification_data_t;

typedef struct property_xml_verification_data_s {
	 ma_bool_t full_xml;
	MA_SLIST_DEFINE(products,product_verification_data_t);
}property_xml_verification_data_t;

ma_error_t initialize_property_xml_verification(property_xml_verification_data_t **data, ma_bool_t full_buffer);

ma_error_t release_property_xml_verification(property_xml_verification_data_t *data);

ma_error_t add_product_to_xml_verifier(property_xml_verification_data_t *data, const char *product_id, ma_bool_t will_be_deleted);

ma_error_t add_section_to_product(property_xml_verification_data_t *data, const char *product_id, const char *section_name, ma_bool_t will_be_missing);

ma_error_t add_setting_to_section(property_xml_verification_data_t *data, const char *product_id, const char *section_name, const char *setting_name, const char *setting_value, ma_bool_t will_be_missing, ma_bool_t will_be_deleted);

ma_error_t add_to_verifier(prop_providers_t product_id, property_xml_verification_data_t *data);
void add_to_verifier_sysprops1000(property_xml_verification_data_t *data);
void add_to_verifier_pcr_____1000(property_xml_verification_data_t *data);
void add_to_verifier_rsd_____4700(property_xml_verification_data_t *data);
void add_to_verifier_viruscan8800(property_xml_verification_data_t *data);
ma_error_t verify_xml_buffer(property_xml_verification_data_t *data, unsigned char *buffer);
MA_CPP(})

#endif 
