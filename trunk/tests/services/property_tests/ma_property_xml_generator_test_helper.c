#include "ma/logger/ma_logger.h"
#include "ma/datastore/ma_ds_database.h"
#include "ma/internal/services/property/property_xml_generator.h"
#include "xml_generator_utilities.h"
#include "ma_property_xml_generator_test_helper.h"
#include "ma/logger/ma_logger.h"
#include <string.h>
#include <stdlib.h>

extern ma_logger_t *g_console_logger;

void add_setting_to_database( ma_ds_t *db, const char *db_path, const char *product_id, const char *section_name, const char *setting, const char *value) {
	if(db && db_path && product_id && section_name && setting && value) {
		ma_temp_buffer_t datastore_path;
		ma_temp_buffer_init(&datastore_path);		
		if(MA_OK == form_path(&datastore_path, db_path, product_id, MA_TRUE)) {
			if(MA_OK == form_path(&datastore_path, (char*)ma_temp_buffer_get(&datastore_path), section_name,MA_TRUE)) {
				ma_ds_set_str(db, (char*)ma_temp_buffer_get(&datastore_path), setting, value, strlen(value));
			}
		}
		ma_temp_buffer_uninit(&datastore_path);
	}
}

void add_setting_to_prop_bag( ma_property_bag_t *bag, const char *section_name, const char *setting_name,const char *value) {
	if(bag && section_name && value) {
		ma_variant_t *variant = NULL;
		if(MA_OK == ma_variant_create_from_string(value,strlen(value),&variant)){
			ma_property_bag_add_property(bag, section_name, setting_name, variant);
			ma_variant_release(variant);
		}
	}
}

void add_to_prop_bag(prop_providers_t product_id, ma_property_bag_t *bag) {
	switch(product_id) {
		case SYSPROPS1000:
			add_to_prop_bag_sysprops1000(bag);
			break;
		case PCR_____1000:
			add_to_prop_bag_pcr_____1000(bag);
			break;
		case RSD_____4700:
			add_to_prop_bag_rsd_____4700(bag);
			break;
		case VIRUSCAN8800:
			add_to_prop_bag_viruscan8800(bag);
			break;
		default:
			MA_LOG(g_console_logger, MA_LOG_SEV_ERROR, "An invalid parameter was passed");
	}
}

void add_to_prop_bag_sysprops1000(ma_property_bag_t *bag) {
	add_setting_to_prop_bag(bag, MA_STR_SYSTEM_PROPERTY, "CPUSerialNumber", "N/A");
	add_setting_to_prop_bag(bag, MA_STR_SYSTEM_PROPERTY, "CPUType", "Intel(R) Core(TM) i7-3520M CPU @ 2.90GHz");
	add_setting_to_prop_bag(bag, MA_STR_SYSTEM_PROPERTY, "DomainName", "WORKGROUP");
	add_setting_to_prop_bag(bag, MA_STR_SYSTEM_PROPERTY, "OSOEMId", "00392-918-5000002-85916");
	add_setting_to_prop_bag(bag, MA_STR_SYSTEM_PROPERTY, "OSType", "Windows 7");
}

void add_to_prop_bag_pcr_____1000(ma_property_bag_t *bag) {
	add_setting_to_prop_bag(bag,"General","Language","0000");
	add_setting_to_prop_bag(bag,"General","PluginVersion","4.8.0.747");
	add_setting_to_prop_bag(bag,"General","szInstallDir","C:\\Program Files (x86)\\McAfee\\Common Framework");
}

void add_to_prop_bag_rsd_____4700(ma_property_bag_t *bag) {
	add_setting_to_prop_bag(bag,"General","szInstallDir","C:\\Program Files (x86)\\McAfee\\Common Framework");
	add_setting_to_prop_bag(bag,"General","szProductVersion","4.7.1.120");

	add_setting_to_prop_bag(bag,"LocalElect","ActiveSensorsPerSubnet","2");
	add_setting_to_prop_bag(bag,"LocalElect","AllSensorsActivePerSubnet","5");
	add_setting_to_prop_bag(bag,"LocalElect","FailoverSleepTime","500");

	add_setting_to_prop_bag(bag,"Sensor","HostFilterTimeout","30000");
	add_setting_to_prop_bag(bag,"Sensor","OSFingerprintingDelay","1000");
	add_setting_to_prop_bag(bag,"Sensor","OSFingerprintingInterval","2500");
	add_setting_to_prop_bag(bag,"Sensor","SensorThrottle","3000");
	add_setting_to_prop_bag(bag,"Sensor","VoteInterval","3000");
}

void add_to_prop_bag_viruscan8800(ma_property_bag_t *bag) {	
	add_setting_to_prop_bag(bag,NULL,"APEnabled","1");
	add_setting_to_prop_bag(bag,"Access Protection","APEnabled","1");
	add_setting_to_prop_bag(bag,"Access Protection","bPVSPTEnabled","1");

	add_setting_to_prop_bag(bag,"Access Protection Reports","LogFileFormat","Unicode (UTF8)");
	add_setting_to_prop_bag(bag,"Access Protection Reports","bLimitSize","1");
	add_setting_to_prop_bag(bag,"Access Protection Reports","bLogToFile","1");
	add_setting_to_prop_bag(bag,"Access Protection Reports","dwMaxLogSizeMB","1");

	add_setting_to_prop_bag(bag,"Additional Alerting Options","SuppressAlertsBelow","1");
	add_setting_to_prop_bag(bag,"Additional Alerting Options","bLocalEventLog","1");
	add_setting_to_prop_bag(bag,"Additional Alerting Options","bSendSNMP","0");

	add_setting_to_prop_bag(bag,"Alert Manager Alerts","AlertManagerServerPath","1");
	add_setting_to_prop_bag(bag,"Alert Manager Alerts","AlertType","Alert Manager alerting enabled.");
}

void add_to_datastore(prop_providers_t product_id, ma_ds_t *db, const char *root_path) {
	switch(product_id) {
		case SYSPROPS1000:
			add_to_datastore_sysprops1000(db,root_path);
			break;
		case PCR_____1000:
			add_to_datastore_pcr_____1000(db,root_path);
			break;
		case RSD_____4700:
			add_to_datastore_rsd_____4700(db,root_path);
			break;
		case VIRUSCAN8800:
			add_to_datastore_viruscan8800(db,root_path);
			break;
		default:
			MA_LOG(g_console_logger, MA_LOG_SEV_ERROR, "An invalid parameter was passed");
	}
}

void add_to_datastore_sysprops1000(ma_ds_t *db, const char *root_path) {
	add_setting_to_database(db, root_path, MA_SYSTEM_PROP_PROVIDER_ID, MA_STR_SYSTEM_PROPERTY, "CPUSerialNumber", "N/A");
	add_setting_to_database(db, root_path, MA_SYSTEM_PROP_PROVIDER_ID, MA_STR_SYSTEM_PROPERTY, "CPUType", "Intel(R) Core(TM) i7-3520M CPU @ 2.90GHz");
	add_setting_to_database(db, root_path, MA_SYSTEM_PROP_PROVIDER_ID, MA_STR_SYSTEM_PROPERTY, "DomainName", "WORKGROUP");
	add_setting_to_database(db, root_path, MA_SYSTEM_PROP_PROVIDER_ID, MA_STR_SYSTEM_PROPERTY, "OSOEMId", "00392-918-5000002-85916");
	add_setting_to_database(db, root_path, MA_SYSTEM_PROP_PROVIDER_ID, MA_STR_SYSTEM_PROPERTY, "OSType", "Windows 7");
}

void add_to_datastore_pcr_____1000(ma_ds_t *db, const char *root_path) {
	add_setting_to_database(db,root_path,"PCR_____1000","General","Language","0000");
	add_setting_to_database(db,root_path,"PCR_____1000","General","PluginVersion","4.8.0.747");
	add_setting_to_database(db,root_path,"PCR_____1000","General","szInstallDir","C:\\Program Files (x86)\\McAfee\\Common Framework");
}

void add_to_datastore_rsd_____4700(ma_ds_t *db, const char *root_path) {
	add_setting_to_database(db,root_path,"RSD_____4700","General","szInstallDir","C:\\Program Files (x86)\\McAfee\\Common Framework");
	add_setting_to_database(db,root_path,"RSD_____4700","General","szProductVersion","4.7.1.120");

	add_setting_to_database(db,root_path,"RSD_____4700","LocalElect","ActiveSensorsPerSubnet","2");
	add_setting_to_database(db,root_path,"RSD_____4700","LocalElect","AllSensorsActivePerSubnet","5");
	add_setting_to_database(db,root_path,"RSD_____4700","LocalElect","FailoverSleepTime","500");

	add_setting_to_database(db,root_path,"RSD_____4700","Sensor","HostFilterTimeout","30000");
	add_setting_to_database(db,root_path,"RSD_____4700","Sensor","OSFingerprintingDelay","1000");
	add_setting_to_database(db,root_path,"RSD_____4700","Sensor","OSFingerprintingInterval","2500");
	add_setting_to_database(db,root_path,"RSD_____4700","Sensor","SensorThrottle","3000");
	add_setting_to_database(db,root_path,"RSD_____4700","Sensor","VoteInterval","3000");
}

void add_to_datastore_viruscan8800(ma_ds_t *db, const char *root_path) {
	add_setting_to_database(db,root_path,"VIRUSCAN8800","Access Protection","APEnabled","1");
	add_setting_to_database(db,root_path,"VIRUSCAN8800","Access Protection","APEnabled","1");
	add_setting_to_database(db,root_path,"VIRUSCAN8800","Access Protection","bPVSPTEnabled","1");

	add_setting_to_database(db,root_path,"VIRUSCAN8800","Access Protection Reports","LogFileFormat","Unicode (UTF8)");
	add_setting_to_database(db,root_path,"VIRUSCAN8800","Access Protection Reports","bLimitSize","1");
	add_setting_to_database(db,root_path,"VIRUSCAN8800","Access Protection Reports","bLogToFile","1");
	add_setting_to_database(db,root_path,"VIRUSCAN8800","Access Protection Reports","dwMaxLogSizeMB","1");

	add_setting_to_database(db,root_path,"VIRUSCAN8800","Additional Alerting Options","SuppressAlertsBelow","1");
	add_setting_to_database(db,root_path,"VIRUSCAN8800","Additional Alerting Options","bLocalEventLog","1");
	add_setting_to_database(db,root_path,"VIRUSCAN8800","Additional Alerting Options","bSendSNMP","0");

	add_setting_to_database(db,root_path,"VIRUSCAN8800","Alert Manager Alerts","AlertManagerServerPath","1");
	add_setting_to_database(db,root_path,"VIRUSCAN8800","Alert Manager Alerts","AlertType","Alert Manager alerting enabled.");
}

ma_error_t create_xml_for_verification(const unsigned char *buffer, ma_xml_t **xml) {
	if(xml && buffer) {
		if(MA_OK == ma_xml_create(xml)) {
			if(MA_OK == ma_xml_load_from_buffer(*xml, (char*)buffer, strlen((char*)buffer))) {
				return MA_OK;
			}
		}
	}
	return MA_ERROR_APIFAILED;
}

#define STR_PCR_____1000 "PCR_____1000"
#define STR_RSD_____4700 "RSD_____4700"
#define STR_VIRUSCAN8800 "VIRUSCAN8800"

const char *get_prod_id (prop_providers_t id) {
	switch(id) {
		case SYSPROPS1000:
			return MA_SYSTEM_PROP_PROVIDER_ID;
			break;
		case PCR_____1000:
			return STR_PCR_____1000;
			break;
		case RSD_____4700:
			return STR_RSD_____4700;
			break;
		case VIRUSCAN8800:
			return STR_VIRUSCAN8800;
			break ;
		default:
			MA_LOG(g_console_logger, MA_LOG_SEV_ERROR, "An invalid parameter was passed");
			return NULL;
	}
}

void free_data(void *data) {
	free(data);
}


ma_error_t initialize_property_xml_verification(property_xml_verification_data_t **data, ma_bool_t full_buffer) {
	if(data){
		*data = (property_xml_verification_data_t *) calloc(1, sizeof(property_xml_verification_data_t));
		if(*data) {
			(*data)->full_xml = full_buffer;
			MA_SLIST_INIT(*data,products);
			return MA_OK;
		}
		return MA_ERROR_OUTOFMEMORY;
	}
	return MA_ERROR_INVALIDARG;
}
static product_verification_data_t* get_product_from_verification_data(property_xml_verification_data_t *data, const char *product_id) {
		product_verification_data_t *product = NULL;

		MA_SLIST_FOREACH(data,products, product){
			if(0 == strcmp(product->id, product_id)) {
				return product;
			}
		}
		return NULL;
}
static section_verification_data_t* get_section_from_verification_data(property_xml_verification_data_t *data, const char *product_id, const char *section_name) {
		product_verification_data_t *product = NULL;
		product = get_product_from_verification_data(data, product_id);
		if(product) {
			section_verification_data_t *section = NULL;
			MA_SLIST_FOREACH(product,sections, section){
				if(0 == strcmp(section->section_name, section_name)) {
					return section;
				}
			}
		}
		return NULL;
}
static setting_verification_data_t* get_setting_from_verification_data(property_xml_verification_data_t *data, const char *product_id, const char *section_name, const char *setting_name) {
		product_verification_data_t *product = NULL;
		product = get_product_from_verification_data(data, product_id);
		if(product) {
			section_verification_data_t *section = get_section_from_verification_data(data, product_id, section_name);
			if(section){
				setting_verification_data_t *setting = NULL;
				MA_SLIST_FOREACH(section,settings, setting){
					if(0 == strcmp(setting->setting_name, setting_name)) {
						return setting;
					}
				}
			}
		}
		return NULL;
}
ma_error_t add_product_to_xml_verifier(property_xml_verification_data_t *data, const char *product_id, ma_bool_t will_be_deleted) {
	if(data) {
		product_verification_data_t *product = NULL;

		MA_SLIST_FOREACH(data,products, product){
			if(0 == strcmp(product->id, product_id))
				break;
		}
		if(!product){
			product = (product_verification_data_t*) calloc(1, sizeof(product_verification_data_t));
			if(product){
				product->id = (char*)product_id;
				product->is_product = is_product(product_id);
				product->deleted = will_be_deleted;
				MA_SLIST_INIT(product, sections);
				MA_SLIST_NODE_INIT(product);
			}
			MA_SLIST_PUSH_BACK(data, products, product);
			return MA_OK;
		}
		else
			return MA_ERROR_ALREADY_REGISTERED;
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t add_section_to_product(property_xml_verification_data_t *data, const char *product_id, const char *section_name, ma_bool_t will_be_missing) {
	section_verification_data_t *section = NULL;
	section = get_section_from_verification_data(data,product_id, section_name);
	if(!section){
		section = (section_verification_data_t *) calloc(1, sizeof(section_verification_data_t));
		if(section) {
			product_verification_data_t* product = get_product_from_verification_data(data, product_id);
			section->section_name = (char *) section_name;
			section->is_product_id = is_product(product_id);
			section->will_be_missing = will_be_missing;
			MA_SLIST_INIT(section,settings);
			MA_SLIST_NODE_INIT(section);

			MA_SLIST_PUSH_BACK(product, sections, section);
			return MA_OK;
		}
	}
	return MA_ERROR_ALREADY_REGISTERED;
}

ma_error_t add_setting_to_section(property_xml_verification_data_t *data, const char *product_id, const char *section_name, const char *setting_name, const char *setting_value, ma_bool_t will_be_missing, ma_bool_t will_be_deleted) {
	setting_verification_data_t *setting = NULL;
	setting = get_setting_from_verification_data(data,product_id, section_name,setting_name);
	if(!setting){
		setting = (setting_verification_data_t *) calloc(1,sizeof(setting_verification_data_t));
		if(setting){
			section_verification_data_t *section = get_section_from_verification_data(data, product_id, section_name);
			setting->setting_name = (char*)setting_name;
			setting->expected_value = (char*)setting_value;
			setting->will_be_deleted = will_be_deleted;
			setting->will_be_missing = will_be_missing;
			MA_SLIST_NODE_INIT(setting);
			MA_SLIST_PUSH_BACK(section, settings, setting);
		}
	}
	return MA_ERROR_ALREADY_REGISTERED;
}
ma_error_t add_to_verifier(prop_providers_t product_id, property_xml_verification_data_t *data) {
	switch(product_id) {
		case SYSPROPS1000:
			add_to_verifier_sysprops1000(data);
			break;
		case PCR_____1000:
			add_to_verifier_pcr_____1000(data);
			break;
		case RSD_____4700:
			add_to_verifier_rsd_____4700(data);
			break;
		case VIRUSCAN8800:
			add_to_verifier_viruscan8800(data);	
			break;
		default: {
			MA_LOG(g_console_logger, MA_LOG_SEV_ERROR, "An invalid parameter was passed");
			return MA_ERROR_APIFAILED;
				 }
	}
	return MA_OK;
}
void add_to_verifier_sysprops1000(property_xml_verification_data_t *data) {
	add_product_to_xml_verifier(data, MA_SYSTEM_PROP_PROVIDER_ID,  MA_FALSE);
	add_section_to_product(data, MA_SYSTEM_PROP_PROVIDER_ID, MA_STR_SYSTEM_PROPERTY, MA_FALSE);
	add_setting_to_section(data, MA_SYSTEM_PROP_PROVIDER_ID, MA_STR_SYSTEM_PROPERTY, "CPUSerialNumber", "N/A" , MA_TRUE, MA_FALSE);
	add_setting_to_section(data, MA_SYSTEM_PROP_PROVIDER_ID, MA_STR_SYSTEM_PROPERTY, "CPUType", "Intel(R) Core(TM) i7-3520M CPU @ 2.90GHz", MA_TRUE, MA_FALSE);
	add_setting_to_section(data, MA_SYSTEM_PROP_PROVIDER_ID, MA_STR_SYSTEM_PROPERTY, "DomainName", "WORKGROUP", MA_TRUE, MA_FALSE);
	add_setting_to_section(data, MA_SYSTEM_PROP_PROVIDER_ID, MA_STR_SYSTEM_PROPERTY, "OSOEMId", "00392-918-5000002-85916", MA_TRUE, MA_FALSE);
	add_setting_to_section(data, MA_SYSTEM_PROP_PROVIDER_ID, MA_STR_SYSTEM_PROPERTY, "OSType", "Windows 7", MA_TRUE, MA_FALSE);
}

void add_to_verifier_pcr_____1000(property_xml_verification_data_t *data) {
	add_product_to_xml_verifier(data, "PCR_____1000",  MA_FALSE);
	add_section_to_product(data, "PCR_____1000", "General", MA_FALSE);
	add_setting_to_section(data, "PCR_____1000","General","Language","0000",MA_TRUE,MA_FALSE);
	add_setting_to_section(data, "PCR_____1000","General","PluginVersion","4.8.0.747",MA_TRUE,MA_FALSE);
	add_setting_to_section(data, "PCR_____1000","General","szInstallDir","C:\\Program Files (x86)\\McAfee\\Common Framework",MA_TRUE,MA_FALSE);
}

void add_to_verifier_rsd_____4700(property_xml_verification_data_t *data) {
	add_product_to_xml_verifier(data, "RSD_____4700",  MA_FALSE);
	add_section_to_product(data, "RSD_____4700", "General", MA_FALSE);
	add_setting_to_section(data, "RSD_____4700","General","szInstallDir","C:\\Program Files (x86)\\McAfee\\Common Framework",MA_TRUE,MA_FALSE);
	add_setting_to_section(data, "RSD_____4700","General","szProductVersion","4.7.1.120",MA_TRUE,MA_FALSE);
	
	add_section_to_product(data, "RSD_____4700", "LocalElect", MA_FALSE);
	add_setting_to_section(data, "RSD_____4700","LocalElect","ActiveSensorsPerSubnet","2",MA_TRUE,MA_FALSE);
	add_setting_to_section(data, "RSD_____4700","LocalElect","AllSensorsActivePerSubnet","5",MA_TRUE,MA_FALSE);
	add_setting_to_section(data, "RSD_____4700","LocalElect","FailoverSleepTime","500",MA_TRUE,MA_FALSE);
	
	add_section_to_product(data, "RSD_____4700", "Sensor", MA_FALSE);
	add_setting_to_section(data, "RSD_____4700","Sensor","HostFilterTimeout","30000",MA_TRUE,MA_FALSE);
	add_setting_to_section(data, "RSD_____4700","Sensor","OSFingerprintingDelay","1000",MA_TRUE,MA_FALSE);
	add_setting_to_section(data, "RSD_____4700","Sensor","OSFingerprintingInterval","2500",MA_TRUE,MA_FALSE);
	add_setting_to_section(data, "RSD_____4700","Sensor","SensorThrottle","3000",MA_TRUE,MA_FALSE);
	add_setting_to_section(data, "RSD_____4700","Sensor","VoteInterval","3000",MA_TRUE,MA_FALSE);
}
void add_to_verifier_viruscan8800(property_xml_verification_data_t *data) {
	add_product_to_xml_verifier(data, "VIRUSCAN8800",  MA_FALSE);
	add_section_to_product(data, "VIRUSCAN8800", "Access Protection", MA_FALSE);
	add_setting_to_section(data, "VIRUSCAN8800","Access Protection", "APEnabled", "1", MA_TRUE, MA_FALSE);
	add_setting_to_section(data, "VIRUSCAN8800","Access Protection", "APEnabled", "1", MA_TRUE, MA_FALSE);
	add_setting_to_section(data, "VIRUSCAN8800","Access Protection", "bPVSPTEnabled", "1", MA_TRUE, MA_FALSE);
	
	add_section_to_product(data, "VIRUSCAN8800", "Access Protection Reports", MA_FALSE);
	add_setting_to_section(data, "VIRUSCAN8800", "Access Protection Reports","LogFileFormat","Unicode (UTF8)",MA_TRUE,MA_FALSE);
	add_setting_to_section(data, "VIRUSCAN8800", "Access Protection Reports","bLimitSize","1",MA_TRUE,MA_FALSE);
	add_setting_to_section(data, "VIRUSCAN8800", "Access Protection Reports","bLogToFile","1",MA_TRUE,MA_FALSE);
	add_setting_to_section(data, "VIRUSCAN8800", "Access Protection Reports","dwMaxLogSizeMB","1",MA_TRUE,MA_FALSE);
	
	add_section_to_product(data, "VIRUSCAN8800", "Additional Alerting Options", MA_FALSE);
	add_setting_to_section(data, "VIRUSCAN8800", "Additional Alerting Options","SuppressAlertsBelow","1",MA_TRUE, MA_FALSE);
	add_setting_to_section(data, "VIRUSCAN8800", "Additional Alerting Options","bLocalEventLog","1",MA_TRUE, MA_FALSE);
	add_setting_to_section(data, "VIRUSCAN8800", "Additional Alerting Options","bSendSNMP","0", MA_TRUE, MA_FALSE);
	
	add_section_to_product(data, "VIRUSCAN8800", "Alert Manager Alerts", MA_FALSE);
	add_setting_to_section(data, "VIRUSCAN8800","Alert Manager Alerts","AlertManagerServerPath","1", MA_TRUE, MA_FALSE);
	add_setting_to_section(data, "VIRUSCAN8800","Alert Manager Alerts","AlertType","Alert Manager alerting enabled.", MA_TRUE, MA_FALSE);
}

ma_error_t verify_settings(setting_verification_data_t *setting, ma_xml_node_t *node,ma_bool_t b_product){
	if(setting && node){
		if(b_product){
			const char *del_attr = ma_xml_node_attribute_get(node, MA_STR_PROPERTY_ATTR_DELETE);
			if(del_attr && setting->will_be_deleted) {
				if(0 == strcmp( MA_STR_TRUE, del_attr)){
					return MA_OK;
				}
			}
			else if((!del_attr && setting->will_be_deleted) ||  (del_attr && !setting->will_be_deleted)){
				return MA_ERROR_APIFAILED;
			}
			return MA_ERROR_APIFAILED;
		}
		else {
			const char *p = ma_xml_node_get_name(node);
			if(p && 0 == strcmp(p, setting->setting_name)){
				const char *value = ma_xml_node_get_data(node);
				if(!setting->will_be_deleted) {
					if(value && 0 == strcmp(value, setting->expected_value)) {
						return MA_OK;
					}
				}
				else {
					const char *del_attr = ma_xml_node_attribute_get(node, MA_STR_PROPERTY_ATTR_DELETE);
					if(0 == strcmp(del_attr, MA_STR_TRUE)){
						return MA_OK;
					}
				}
			}
			return MA_ERROR_APIFAILED;
		}
	}
	return MA_ERROR_INVALIDARG;
}
ma_error_t verify_section(section_verification_data_t *section, ma_xml_node_t *node,ma_bool_t b_product){
	if(section && node){
		ma_error_t error =MA_ERROR_APIFAILED;
		ma_bool_t b_valid = MA_FALSE;
		if(b_product){
			const char *name = ma_xml_node_attribute_get(node, MA_STR_PROD_PROPERTY_ATTR_NAME);
			if(0 == strcmp(section->section_name, name )){
				int count =  ma_xml_node_get_child_count(node);
				if(section->settings_count == count )
					b_valid = MA_TRUE;
			}
		}

		if(b_valid){
			ma_xml_node_t *setting_node = NULL;
			setting_verification_data_t* setting = NULL;
			MA_SLIST_FOREACH(section, settings, setting) {
				if(MA_OK == find_property_xml_setting_node(node, setting->setting_name, b_product, MA_FALSE, &setting_node)) {
					error = verify_settings(setting, setting_node, b_product);
					if(error != MA_OK) {
						MA_LOG(g_console_logger, MA_LOG_SEV_ERROR, "Section verification failed: %s setting of %s section",setting->setting_name, section->section_name);
						return error;
					}
				}
				error = MA_OK;
			}
		}
		return error;
	}
	return MA_ERROR_INVALIDARG;
}
ma_error_t verify_product(product_verification_data_t *product, ma_xml_node_t *node) {
	if(product && node) {
		ma_error_t error = MA_ERROR_APIFAILED;
		ma_bool_t b_sys_prod = !is_product(product->id);
		ma_bool_t b_valid = MA_FALSE;
		if(b_sys_prod){
			if(0 == strcmp(MA_STR_SYSTEM_PROPERTY, ma_xml_node_get_name(node))) 
				b_valid = MA_TRUE;
		}
		else {
			if(0 == strcmp(MA_STR_PROD_PROPERTY, ma_xml_node_get_name(node))) {
				if(0 == strcmp(product->id, ma_xml_node_attribute_get(node, MA_STR_PROD_PROPERTY_ATTR_SOFTWARE_ID))) {
					const char *del_attr = ma_xml_node_attribute_get(node, MA_STR_PROPERTY_ATTR_DELETE);
					if((product->deleted && 0 == strcmp(MA_STR_TRUE, del_attr)) || (!product->deleted && 0 == strcmp(MA_STR_FALSE, del_attr))){
						b_valid = MA_TRUE;
					}
					else{
						b_valid = MA_FALSE;
					}
				}
			}
		}
		if(b_valid){
			int count = ma_xml_node_get_child_count(node);
			if(b_sys_prod) {
				section_verification_data_t *section = product->sections_head;
				setting_verification_data_t *setting = NULL;
				ma_xml_node_t *setting_node = NULL;
				MA_SLIST_FOREACH(section, settings, setting){
					if(MA_TRUE == find_property_xml_setting_node(node, setting->setting_name, MA_FALSE, MA_FALSE, &setting_node)) {
						error = verify_settings(setting, setting_node, MA_FALSE);
						if(error != MA_OK) {
							MA_LOG(g_console_logger, MA_LOG_SEV_ERROR, "Setting verification failed : %s, %s", setting->setting_name, product->id);
							return error;
						}
					}
				}
				error = MA_OK;
				return error;
			}
			else {
				if(product->sections_count ==(count )){
					ma_xml_node_t *section_node = NULL;
					section_verification_data_t *section = NULL; 
					MA_SLIST_FOREACH(product,sections, section){
						if(MA_TRUE == find_xml_product_section(node, section->section_name, &section_node)){
							error = verify_section(section, section_node,!b_sys_prod);
							if(error !=MA_OK) {
								MA_LOG(g_console_logger, MA_LOG_SEV_ERROR, "Section verification failed: %s section of %s product",section->section_name, product->id);
								return error;
							}
						}
					}

					error = MA_OK;
					return MA_OK;
				}
				else{
					MA_LOG(g_console_logger, MA_LOG_SEV_ERROR, "Product section count mismatch :%s found %d section expected %d sections", product->id, count, product->sections_count);
				}
			}
		}
	}
	return MA_ERROR_INVALIDARG;
}

ma_error_t verify_xml_buffer(property_xml_verification_data_t *data, unsigned char *buffer) {
	if(data && buffer) {
		ma_xml_t *xml = NULL;
		ma_error_t error = MA_ERROR_APIFAILED;
		if(MA_OK == create_xml_for_verification(buffer, &xml)) {
			ma_xml_node_t *prop_root = NULL;
			if(MA_TRUE == find_xml_property_root(xml, &prop_root)){
				int count = ma_xml_node_get_child_count(prop_root);
				if(data->products_count == count){
					product_verification_data_t *product;
					ma_xml_node_t *product_node = NULL;
					MA_SLIST_FOREACH(data,products,product){
						if(MA_TRUE == find_xml_product(prop_root, is_product(product->id), product->id, &product_node)) {
							error = verify_product(product, product_node);
							if(error != MA_OK) {
								MA_LOG(g_console_logger, MA_LOG_SEV_ERROR, "Product verification failed: %s",product->id);
								break;
							}
						}
					}
				}
			}
			ma_xml_release(xml);
		}
		return error;
	}
	return MA_ERROR_INVALIDARG;
}

static ma_error_t setting_xml_verifier_release(setting_verification_data_t *setting){
	free(setting);
	return MA_OK;
}
static ma_error_t section_xml_verifier_release(section_verification_data_t *section){
	MA_SLIST_CLEAR(section,settings,setting_verification_data_t, setting_xml_verifier_release);
	free(section);
	return MA_OK;
}
static ma_error_t product_xml_verifier_release(product_verification_data_t *product){
	MA_SLIST_CLEAR(product,sections,section_verification_data_t, section_xml_verifier_release);
	free(product);
	return MA_OK;
}

ma_error_t release_property_xml_verification(property_xml_verification_data_t *data) {
	MA_SLIST_CLEAR(data,products,product_verification_data_t, product_xml_verifier_release);
	free(data);
	return MA_OK;
}
