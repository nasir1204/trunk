#include "ma_ut_setup.h"

#include "ma/internal/services/ma_service.h"
#include "ma/internal/clients/ma/ma_client_internal.h"
#include "ma/logger/ma_file_logger.h"

#include "ma/internal/ma_macros.h"

#include "ma/policy/ma_policy_bag.h"
#include "ma/internal/services/ma_policy_settings_bag.h"
#include "ma/internal/utils/configurator/ma_configurator.h"

#include "ma/internal/utils/macrypto/ma_crypto.h"
#include "ma/datastore/ma_ds_database.h"
#include "ma/internal/ma_strdef.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

struct ma_ut_setup_s {
    ma_client_t                                     *client;

    ma_logger_t                                     *logger;

    char                                            log_file_name[MA_MAX_LEN];

    ma_policy_settings_bag_t                        *policy_settings_bag;

    ma_context_t                                    *context;
    
};

static ma_error_t ma_ut_agent_policy_settings_bag_create(ma_ut_setup_t *ut_setup, ma_policy_settings_bag_t **policy_settings_bag);

ma_error_t ma_ut_setup_create(const char *ut_name, const char *product_id, ma_ut_setup_t **setup) {
    if(product_id && setup) {
        ma_error_t rc = MA_OK;
        ma_ut_setup_t *self = (ma_ut_setup_t *)calloc(1,sizeof(ma_ut_setup_t));
        if(!self)       return MA_ERROR_OUTOFMEMORY;

        MA_MSC_SELECT(_snprintf, snprintf)(self->log_file_name, MA_MAX_LEN,"%s.log", ut_name);
        unlink(self->log_file_name);
        if(MA_OK == (rc = ma_file_logger_create(NULL, ut_name, NULL, (ma_file_logger_t **)&self->logger))) {
                if(MA_OK == (rc = ma_ut_agent_policy_settings_bag_create(self, &self->policy_settings_bag))) {
                    if(MA_OK == (rc = ma_client_create(product_id, &self->client))) {
                        ma_client_set_logger(self->client, self->logger);
                        ma_client_set_thread_option(self->client, MA_MSGBUS_CALLBACK_THREAD_IO);
                        ma_client_set_consumer_reachability(self->client, MSGBUS_CONSUMER_REACH_INPROC);
                        if(MA_OK == (rc = ma_client_start(self->client))) {
                            ma_client_get_context(self->client,&self->context);
                            ma_context_add_object_info(self->context, MA_OBJECT_POLICY_SETTINGS_BAG_STR, &self->policy_settings_bag);
                            ma_context_add_object_info(self->context, MA_OBJECT_LOGGER_NAME_STR, &self->logger);
                            *setup = self;
                            return MA_OK;
                        }
                        ma_client_release(self->client);
                    }
                    ma_policy_settings_bag_release(self->policy_settings_bag);
            }
            ma_logger_release(self->logger);
            unlink(self->log_file_name);
        }
        free(self);
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

void ma_ut_setup_release(ma_ut_setup_t *self) {
    if(self) {
        ma_client_stop(self->client);
        ma_client_release(self->client);
        ma_policy_settings_bag_release(self->policy_settings_bag);        
        ma_logger_release(self->logger);
        unlink(self->log_file_name);
        free(self);
    }
}

ma_context_t *ma_ut_setup_get_context(ma_ut_setup_t *self) {
    return self->context;
}

ma_msgbus_t *ma_ut_setup_get_msgbus(ma_ut_setup_t *self) {
    ma_msgbus_t *bus = NULL;
    ma_client_get_msgbus(self->client, &bus);
    return bus;
}

ma_client_t *ma_ut_setup_get_client(ma_ut_setup_t *self) {
    return self->client;
}

ma_logger_t *ma_ut_setup_get_logger(ma_ut_setup_t *self) {
    return self->logger;
}



/*policy settinsg bag*/


typedef struct ma_ut_agent_policy_settings_bag_s {
    ma_policy_settings_bag_t    base;

	ma_policy_bag_t				*bag;

    ma_ut_setup_t               *ut_setup;

}ma_ut_agent_policy_settings_bag_t;

static ma_error_t get_bool(ma_policy_settings_bag_t *bag, const char *section, const char *key, ma_bool_t *value);
static ma_error_t get_int(ma_policy_settings_bag_t *bag, const char *section, const char *key, ma_int32_t *value);
static ma_error_t get_str(ma_policy_settings_bag_t *bag, const char *section, const char *key, const char **value);
static ma_error_t get_buffer(ma_policy_settings_bag_t *bag, const char *section, const char *key, ma_buffer_t **value);
static ma_error_t get_variant(ma_policy_settings_bag_t *bag, const char *section, const char *key, ma_variant_t **value);
static ma_error_t get_table(ma_policy_settings_bag_t *bag, const char *section, const char *key, ma_table_t **value);
static void destroy(ma_policy_settings_bag_t *bag); /*< virtual destructor */


static const ma_policy_settings_bag_methods_t policy_methods = {
    &get_bool,
    &get_int,
    &get_str,
    &get_buffer,
	&get_variant,
    &get_table,
    &destroy
};

static ma_error_t ma_ut_agent_policy_settings_bag_create(ma_ut_setup_t *ut_setup, ma_policy_settings_bag_t **bag) {
    if(bag ) {
        ma_ut_agent_policy_settings_bag_t *self = (ma_ut_agent_policy_settings_bag_t *)calloc(1, sizeof(ma_ut_agent_policy_settings_bag_t));
        if(!self) return MA_ERROR_OUTOFMEMORY;

        ma_policy_settings_bag_init((ma_policy_settings_bag_t *)self, &policy_methods, self);
		ma_policy_bag_create(&self->bag);

        self->ut_setup = ut_setup;
        *bag = (ma_policy_settings_bag_t *)self;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG ;
}

static void destroy(ma_policy_settings_bag_t *bag) {
    if(bag) {
        ma_ut_agent_policy_settings_bag_t *self = (ma_ut_agent_policy_settings_bag_t *)bag;
		ma_policy_bag_release(self->bag);
        free(self);
    }
}

static ma_error_t get_variant(ma_policy_settings_bag_t *bag, const char *section, const char *key, ma_variant_t **value){
	return MA_ERROR_INVALIDARG;
}
static ma_error_t get_table(ma_policy_settings_bag_t *bag, const char *section, ma_table_t **value) {
    return MA_ERROR_INVALIDARG;
}

static ma_error_t get_buffer(ma_policy_settings_bag_t *bag, const char *section, const char *key, ma_buffer_t **value) {
    if(bag) {
        ma_ut_agent_policy_settings_bag_t *self = (ma_ut_agent_policy_settings_bag_t *)bag;
		ma_error_t rc = MA_ERROR_OBJECTNOTFOUND;
		if(self->bag){
			ma_variant_t *var = NULL;
			ma_policy_bag_get_value(self->bag, NULL, section, key, &var);
			if(var){
				rc = ma_variant_get_string_buffer(var, value);
				ma_variant_release(var);
			}		
		}    
		return rc;
    }
	return MA_ERROR_INVALIDARG;
}

static ma_error_t get_str(ma_policy_settings_bag_t *bag, const char *section, const char *key, const char **value ) {
    ma_variant_t *var_value = NULL;
    ma_error_t rc = MA_OK;
    ma_buffer_t *dummy_buffer = NULL;
    if(MA_OK == (rc = get_buffer(bag, section, key, &dummy_buffer))) {
        char *new_value = NULL;
        size_t size = 0;
        if((MA_OK == (rc = ma_buffer_get_string(dummy_buffer, &new_value, &size))) && new_value)
            *value = new_value;
        (void)ma_buffer_release(dummy_buffer);
    }
    return rc;
}

static ma_error_t get_int(ma_policy_settings_bag_t *bag, const char *section, const char *key, ma_int32_t *value) {
    ma_variant_t *var_value = NULL;
    ma_error_t rc = MA_OK;
    ma_buffer_t *buffer = NULL;
    if(MA_OK == (rc = get_buffer(bag, section, key, &buffer))) {
        char *new_value = NULL;
        size_t size = 0;
        if((MA_OK == (rc = ma_buffer_get_string(buffer, &new_value, &size))) && new_value)
            *value = atoi(new_value);
        (void) ma_buffer_release(buffer);
    }
    return rc;
}

static ma_error_t get_bool(ma_policy_settings_bag_t *bag, const char *section, const char *key, ma_bool_t *value)  {
    ma_variant_t *var_value = NULL;
    ma_error_t rc = MA_OK;
    ma_int32_t dummy_value = 0;

    if(MA_OK == (rc = get_int(bag, section, key, &dummy_value)))
        *value = dummy_value ? MA_TRUE :MA_FALSE;

    return rc;
}

#define MA_POLICY_FEATURE_ID            "EPOAGENTMETA"
#define MA_POLICY_CATEGORY_ID           "General"
#define MA_POLICY_TYPE_ID               "General"
#define MA_POLICY_NAME_ID               "My Default"



static ma_error_t add_p2p_service_policies(ma_policy_uri_t *general_uri, ma_policy_bag_t *policy_bag, ma_bool_t enabled) {
    ma_error_t rc = MA_OK ;
    const char *is_installed = "1", *is_enabled = "0", *serving_enabled = "1",  *client_enabled = "1", *disk_quota = "1000", *purge_interval = "1";    
    ma_variant_t *value = NULL ;

	if(enabled) 
		is_enabled = "1";

    if(MA_OK == (rc = ma_variant_create_from_string(is_installed, strlen(is_installed), &value))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_P2P_SERVICE_SECTION_NAME_STR, MA_P2P_KEY_IS_INSTALLED_INT, value) ; 
        (void) ma_variant_release(value); value = NULL ;
    }

    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(is_enabled, strlen(is_enabled), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_P2P_SERVICE_SECTION_NAME_STR, MA_P2P_KEY_IS_ENABLED_INT, value) ; 
        (void) ma_variant_release(value); value = NULL ;
    }

	if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(serving_enabled, strlen(serving_enabled), &value)))) {
		rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_P2P_SERVICE_SECTION_NAME_STR, MA_P2P_KEY_ENABLE_SERVING_INT, value) ; 
        (void) ma_variant_release(value); value = NULL ;
    }

	if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(client_enabled, strlen(client_enabled), &value)))) {
		rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_P2P_SERVICE_SECTION_NAME_STR, MA_P2P_KEY_ENABLE_CLIENT_INT, value) ; 
        (void) ma_variant_release(value); value = NULL ;
    }

	if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(disk_quota, strlen(disk_quota), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_P2P_SERVICE_SECTION_NAME_STR, MA_P2P_KEY_DISK_QUOTA_INT, value) ; 
        (void) ma_variant_release(value); value = NULL ;
    }

	if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(purge_interval, strlen(purge_interval), &value)))) {
		rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_P2P_SERVICE_SECTION_NAME_STR, MA_P2P_KEY_LONGEVITY_INT, value) ; 
        (void) ma_variant_release(value); value = NULL ;
    }

    return rc ;
}

static ma_error_t add_udp_service_policies(ma_policy_uri_t *general_uri, ma_policy_bag_t *policy_bag) {
    ma_error_t rc = MA_OK ;
    const char *is_installed = "1", *is_enabled = "1", *udp_port = "8082", *is_broadcast_ping_enabled = "0", *multicast_address = "ff08::2" ;  
    ma_variant_t *value = NULL ;

    if(MA_OK == (rc = ma_variant_create_from_string(is_installed, strlen(is_installed), &value))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_UDP_SERVICE_SECTION_NAME_STR, MA_UDP_KEY_IS_INSTALLED_INT, value) ; 
        (void) ma_variant_release(value); value = NULL ;
    }

    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(is_enabled, strlen(is_enabled), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_UDP_SERVICE_SECTION_NAME_STR, MA_UDP_KEY_IS_ENABLED_INT, value) ; 
        (void) ma_variant_release(value); value = NULL ;
    }

	if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(multicast_address, strlen(multicast_address), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_UDP_SERVICE_SECTION_NAME_STR, MA_UDP_KEY_MULTICAST_ADDRESS_STR, value); 
        (void) ma_variant_release(value); value = NULL;
    }

	if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(udp_port, strlen(udp_port), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_UDP_SERVICE_SECTION_NAME_STR, MA_UDP_KEY_BROADCAST_PORT_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }
      
    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(is_broadcast_ping_enabled, strlen(is_broadcast_ping_enabled), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_UDP_SERVICE_SECTION_NAME_STR, MA_UDP_KEY_ENABLE_BROADCAST_PING_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }

    return rc ;
}

static ma_error_t add_relay_service_policies(ma_policy_uri_t *general_uri, ma_policy_bag_t *policy_bag, ma_bool_t enabled, ma_bool_t same_port) {
    ma_error_t rc = MA_OK ;
    const char *is_installed = "1", *is_enabled = "0", *relay_discovery_port = "8083", *multicast_address = "ff08::2" ;    
    ma_variant_t *value = NULL ;

	if(enabled) 
		is_enabled = "1";

	if(same_port) 
		relay_discovery_port = "8082";

    if(MA_OK == (rc = ma_variant_create_from_string(is_installed, strlen(is_installed), &value))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_RELAY_SERVICE_SECTION_NAME_STR, MA_RELAY_KEY_IS_INSTALLED_INT, value) ; 
        (void) ma_variant_release(value); value = NULL ;
    }

    if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(is_enabled, strlen(is_enabled), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_RELAY_SERVICE_SECTION_NAME_STR, MA_RELAY_KEY_IS_ENABLED_INT, value) ; 
        (void) ma_variant_release(value); value = NULL ;
    }
	
	if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(multicast_address, strlen(multicast_address), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_RELAY_SERVICE_SECTION_NAME_STR, MA_RELAY_KEY_MULTICAST_ADDRESS_STR, value); 
        (void) ma_variant_release(value); value = NULL;
    }

	if((MA_OK == rc) && (MA_OK == (rc = ma_variant_create_from_string(relay_discovery_port, strlen(relay_discovery_port), &value)))) {
        rc = ma_policy_bag_set_value(policy_bag, general_uri, MA_RELAY_SERVICE_SECTION_NAME_STR, MA_RELAY_KEY_SERVER_PORT_INT, value); 
        (void) ma_variant_release(value); value = NULL;
    }

    return rc ;
}

void refresh_policy(ma_policy_settings_bag_t *bag, ma_bool_t relay, ma_bool_t same_port, ma_bool_t p2p){
	ma_policy_uri_t *uri = NULL;
	ma_policy_uri_create(&uri);
		
	ma_ut_agent_policy_settings_bag_t *self = (ma_ut_agent_policy_settings_bag_t *)bag;

	if(self->bag) ma_policy_bag_release(self->bag);
	self->bag = NULL;
	ma_policy_bag_create(&self->bag);

    /* For adding policies in General PSO */
    ma_policy_uri_set_feature(uri, MA_POLICY_FEATURE_ID); 
    ma_policy_uri_set_category(uri, MA_POLICY_CATEGORY_ID);    
    ma_policy_uri_set_type(uri, MA_POLICY_TYPE_ID);   
    ma_policy_uri_set_name(uri, MA_POLICY_NAME_ID);      

	add_udp_service_policies(uri, self->bag);	
	add_relay_service_policies(uri, self->bag, relay, same_port);	
	add_p2p_service_policies(uri, self->bag, p2p);	
}