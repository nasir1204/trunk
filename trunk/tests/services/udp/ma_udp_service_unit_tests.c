#include "unity.h"
#include "unity_fixture.h"
#include "ma/logger/ma_console_logger.h"

#include "ma/internal/ma_macros.h"

#include "ma/internal/clients/udp/ma_udp_request.h"
#include "ma/internal/services/udp_server/ma_udp_server_service.h"
#include "ma/internal/defs/ma_relay_defs.h"

#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/utils/event_loop/ma_loop_worker.h"

#include "ma_ut_setup.h"
#include "ma/internal/services/ma_policy_settings_bag.h"

#undef LOG_FACILITY_NAME
#define LOG_FACILITY_NAME "udp_test"


#define PORT				8089
#define ADDRESS				"ff08::9"

static ma_udp_client_t		*udp_client = NULL;

ma_ut_setup_t *g_ut_setup  = NULL;
extern ma_error_t ma_ut_setup_create(const char *ut_name, const char *product_id, ma_ut_setup_t **ut_setup);
extern ma_context_t *ma_ut_setup_get_context(ma_ut_setup_t *ut_setup);

TEST_GROUP_RUNNER(ma_udp_service_test_group)
{
	/* Set up ut context*/
	ma_ut_setup_create("udp_service.tests", "ma.service.udp.test", &g_ut_setup);
    
    do {RUN_TEST_CASE(ma_udp_service_test_ut, udp_service_test);} while(0);

	ma_ut_setup_release(g_ut_setup);
}

ma_error_t udp_request_send_cb(ma_udp_request_t *request, void *user_data, ma_bool_t *stop){
}

ma_error_t udp_response_cb(ma_udp_request_t *request, ma_udp_response_t *response, void *user_data, ma_bool_t *stop){

}

void udp_request_finalize_cb(ma_error_t rc, ma_udp_request_t *request, void *user_data){
}


TEST_SETUP(ma_udp_service_test_ut)
{   
	
		
}

TEST_TEAR_DOWN(ma_udp_service_test_ut)
{
	
}

static uv_buf_t alloc_cb(uv_handle_t* handle, size_t suggested_size) {
	if(handle) {
		return uv_buf_init((char*)calloc(suggested_size, 1), suggested_size);
	}
	return uv_buf_init(NULL, 0);
}


TEST(ma_udp_service_test_ut, udp_service_test)
{
	
}