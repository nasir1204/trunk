
#include "unity.h"
#include "unity_fixture.h"

#include "ma/ma_common.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/services/scheduler/ma_scheduler_core.h"
#include "ma/scheduler/ma_triggers.h"
#include "ma/scheduler/ma_task.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"
#include "ma/internal/services/scheduler/ma_triggers_internal.h"

#include <uv.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


#ifdef MA_WINDOWS
# include <crtdbg.h>
#include <Windows.h>
#else
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#endif

#define PRINT_TMSTAMP() \
    {\
        ma_task_time_t ttime;\
        get_localtime(&ttime);\
        printf("\n(%d-%d-%d:%d:%d:%d)", ttime.day, ttime.month,ttime.year, ttime.hour, ttime.minute, ttime.secs);\
    }

extern uv_loop_t *g_loop;
extern ma_ds_t *g_ds;
extern ma_scheduler_t *g_scheduler;
extern void set_os_time(size_t day, size_t month, size_t yr, size_t hr, size_t min, size_t sec);

static struct{
    size_t delay_time;
    size_t run_count;
}g_ma_scheduler_daily_test_globals;

TEST_GROUP_RUNNER(scheduler_daily_test_group) {
    do {RUN_TEST_CASE(scheduler_daily_tests, daily_test_single_task); }while(0);
    do {RUN_TEST_CASE(scheduler_daily_tests, daily_test_single_task_set_retry); }while(0);
    do {RUN_TEST_CASE(scheduler_daily_tests, daily_test_single_task_set_retry_with_end_date); }while(0);
    do {RUN_TEST_CASE(scheduler_daily_tests, daily_test_multiple_trigger); }while(0);
    do {RUN_TEST_CASE(scheduler_daily_tests, daily_test_trigger_boundary_intervals); }while(0);
    do {RUN_TEST_CASE(scheduler_daily_tests, daily_test_trigger_misfire_intervals_delayed); }while(0);
    do {RUN_TEST_CASE(scheduler_daily_tests, daily_test_trigger_misfire_all_intervals); }while(0);
    do {RUN_TEST_CASE(scheduler_daily_tests, daily_test_trigger_misfire_all_intervals_no_next_trigger); }while(0);
    do {RUN_TEST_CASE(scheduler_daily_tests, daily_test_trigger_multiple_triggers_delayed); }while(0);
    do {RUN_TEST_CASE(scheduler_daily_tests, daily_test_trigger_multiple_task_single_trigger); }while(0);
    do {RUN_TEST_CASE(scheduler_daily_tests, daily_test_trigger_multiple_triggers_modify_add); }while(0);
    do {RUN_TEST_CASE(scheduler_daily_tests, daily_test_trigger_multiple_triggers_modify_remove); }while(0);
}


TEST_SETUP(scheduler_daily_tests) {
}

TEST_TEAR_DOWN(scheduler_daily_tests) {
}

static void ma_sheduler_print_last_next_trigger_dates(ma_task_t *task) {
    ma_task_time_t last_run_time = {0};
    ma_task_time_t next_run_time = {0};
    ma_task_get_last_run_time(task, &last_run_time);
    ma_task_get_next_run_time(task, &next_run_time);
    printf("\nGet Last Run Time = <%d-%d-%d:%d:%d:%d>", last_run_time.day, last_run_time.month, last_run_time.year,
                                        last_run_time.hour, last_run_time.minute, last_run_time.secs);
    printf("\nGet Next Run Time = <%d-%d-%d:%d:%d:%d>", next_run_time.day, next_run_time.month, next_run_time.year,
                                        next_run_time.hour, next_run_time.minute, next_run_time.secs);
}

static void uv_thread_daily_test_single_task(void *param) {
    ma_scheduler_t *scheduler = NULL;
    ma_task_t **task = NULL;
    size_t no_of_tasks = 0;
    size_t run_count = 0;
    ma_task_state_t task_state = MA_TASK_STATE_NOT_SCHEDULED;
    ma_task_exec_status_t task_exec_status = MA_TASK_EXEC_NOT_STARTED;
    printf("\nStarting Test Case %s", __FUNCTION__);

    scheduler = (ma_scheduler_t*)param;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_get_task_set(scheduler, &task, &no_of_tasks));
    if(task) {
        do{
            ma_task_get_state(task[0], &task_state);
            ma_task_get_execution_status(task[0], &task_exec_status);
            if((MA_TASK_STATE_RUNNING == task_state)){
                run_count++;
                printf("\nUTC executed (%d) times", run_count);
                printf("\nTask in Running state - Start");
                while(MA_TASK_STATE_RUNNING == task_state) {
                    ma_task_get_state(task[0], &task_state);
                }
                printf("\nTask in Running state - End");
                if(1 == run_count) {
                    printf("\nUpdating OS time to (13-6-2013)");
                    set_os_time(13, 6, 2013, 10, 29, 0);
                } 
                else if(2 == run_count) {
                    printf("\nUpdating OS time to (14-6-2013)");
                    set_os_time(14, 6, 2013, 10, 29, 0);
                }
                if(2 == run_count) break; //Need to update it as 3
            }
        }while(1);
        printf("\nExiting Test Case %s", __FUNCTION__);
    }
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release_task_set(task));
}

static void uv_thread_daily_test_trigger_boundary_intervals(void *param) {
    ma_scheduler_t *scheduler = NULL;
    ma_task_t **task = NULL;
    size_t no_of_tasks = 0;
    size_t run_count = 0;
    ma_task_state_t task_state = MA_TASK_STATE_NOT_SCHEDULED;
    ma_task_exec_status_t task_exec_status = MA_TASK_EXEC_NOT_STARTED;
    printf("\nStarting Test Case %s", __FUNCTION__);

    scheduler = (ma_scheduler_t*)param;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_get_task_set(scheduler, &task, &no_of_tasks));
    if(task) {
        do{
            ma_task_get_state(task[0], &task_state);
            ma_task_get_execution_status(task[0], &task_exec_status);
            if((MA_TASK_STATE_RUNNING == task_state)){
                run_count++;
                printf("\nTask Running - <Count: %d>", run_count);
                if(run_count > 3) {
                    printf("\nInterval window over for current day. Trigger for next week...");
                    break;
                }
                do{
                    ma_task_get_state(task[0], &task_state);
                }while(MA_TASK_STATE_RUNNING == task_state);
                printf("\nTask Running End");
            }
        }while(1);
    }
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release_task_set(task));
    printf("\nExiting Test Case %s", __FUNCTION__);
}

static void uv_thread_daily_test_trigger_misfire_intervals_delayed(void *param) {
    ma_scheduler_t *scheduler = NULL;
    ma_task_t **task = NULL;
    size_t no_of_tasks = 0;
    size_t run_count = 0;
    const char *trigger_id = NULL;
    ma_trigger_t *ret_trigger = NULL;
    ma_task_state_t task_state = MA_TASK_STATE_NOT_SCHEDULED;
    ma_task_exec_status_t task_exec_status = MA_TASK_EXEC_NOT_STARTED;
    printf("\nStarting Test Case %s", __FUNCTION__);

    scheduler = (ma_scheduler_t*)param;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_get_task_set(scheduler, &task, &no_of_tasks));
    if(task) {
        do{
            ma_task_get_state(task[0], &task_state);
            ma_task_get_execution_status(task[0], &task_exec_status);
            if((1 == run_count) && (MA_TASK_STATE_NOT_SCHEDULED == task_state)) {
                ma_task_find_trigger(task[0], trigger_id, &ret_trigger);
                ma_trigger_set_state(ret_trigger, MA_TRIGGER_STATE_BLOCKED);
#if MA_WINDOWS
                Sleep(g_ma_scheduler_daily_test_globals.delay_time * 60 * 1000);
#else
                sleep(g_ma_scheduler_daily_test_globals.delay_time * 60);
#endif
                run_count++;
            }
            if((MA_TASK_STATE_RUNNING == task_state)){
                run_count++;
                printf("\nTask Running - <Count: %d>", run_count);
                if(run_count > g_ma_scheduler_daily_test_globals.run_count) {
                    printf("\nInterval window over for current day. Trigger for next day...");
                    break;
                }
                do{
                    ma_task_get_state(task[0], &task_state);
                }while(MA_TASK_STATE_RUNNING == task_state);
                printf("\nTask Running End");
            }
        }while(1);
    }
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release_task_set(task));
    printf("\nExiting Test Case %s", __FUNCTION__);
}

static void uv_thread_daily_test_single_task_set_retry(void *param) {
    ma_scheduler_t *scheduler = NULL;
    ma_task_t **task = NULL;
    size_t no_of_tasks = 0;
    size_t run_count = 0;
    ma_task_state_t task_state = MA_TASK_STATE_NOT_SCHEDULED;
    ma_task_exec_status_t task_exec_status = MA_TASK_EXEC_NOT_STARTED;
    printf("\nStarting Test Case %s", __FUNCTION__);

    scheduler = (ma_scheduler_t*)param;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_get_task_set(scheduler, &task, &no_of_tasks));
    if(task) {
        do{
            ma_task_get_state(task[0], &task_state);
            ma_task_get_execution_status(task[0], &task_exec_status);
            if((MA_TASK_STATE_RUNNING == task_state)){
                run_count++;
                if(MA_TASK_EXEC_RUNNING == task_exec_status) ma_task_set_execution_status(task[0], MA_TASK_EXEC_FAILED);
                printf("\nTask Running - <Count: %d>", run_count);
                if((run_count > 3) || (MA_TASK_STATE_NO_MORE_RUN == task_state)) break;
                do{
                    ma_task_get_state(task[0], &task_state);
                }while(MA_TASK_STATE_RUNNING == task_state);
                printf("\nTask Running End");
            }
        }while(1);
    }
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release_task_set(task));
    printf("\nExiting Test Case %s", __FUNCTION__);
}

static void uv_thread_daily_test_multiple_trigger(void *param) {
    ma_scheduler_t *scheduler = NULL;
    ma_task_t **task = NULL;
    size_t no_of_tasks = 0;
    size_t run_count = 0;
    ma_task_state_t task1_state = MA_TASK_STATE_NOT_SCHEDULED;
    ma_task_exec_status_t task1_exec_status = MA_TASK_EXEC_NOT_STARTED;
    printf("\nStarting Test Case %s", __FUNCTION__);

    scheduler = (ma_scheduler_t*)param;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_get_task_set(scheduler, &task, &no_of_tasks));
    TEST_ASSERT_EQUAL_INT(1, no_of_tasks);
    if(task) {
        ma_sheduler_print_last_next_trigger_dates(task[0]);
        do{
            ma_task_get_state(task[0], &task1_state);
            ma_task_get_execution_status(task[0], &task1_exec_status);
            if((MA_TASK_STATE_RUNNING == task1_state)){
                run_count++;
                printf("\nTask Running - <Count: %d>", run_count);
                do{
                    ma_task_get_execution_status(task[0], &task1_exec_status);
                }while(MA_TASK_EXEC_RUNNING == task1_exec_status);
                printf("\nTask execution Running End");
                ma_sheduler_print_last_next_trigger_dates(task[0]);
            }
            if(MA_TASK_STATE_NO_MORE_RUN == task1_state || run_count == 2) break;
        }while(1);
    }
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release_task_set(task));
    printf("\nExiting Test Case %s", __FUNCTION__);
}

static void uv_thread_daily_test_trigger_multiple_triggers_delayed(void *param) {
    ma_scheduler_t *scheduler = NULL;
    ma_task_t **task = NULL;
    size_t no_of_tasks = 0;
    size_t run_count = 0;
    ma_task_state_t task1_state = MA_TASK_STATE_NOT_SCHEDULED;
    ma_task_exec_status_t task1_exec_status = MA_TASK_EXEC_NOT_STARTED;
    printf("\nStarting Test Case %s", __FUNCTION__);

    scheduler = (ma_scheduler_t*)param;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_get_task_set(scheduler, &task, &no_of_tasks));
    TEST_ASSERT_EQUAL_INT(1, no_of_tasks);

    if(task) {
        ma_sheduler_print_last_next_trigger_dates(task[0]);
        do{
            ma_task_get_state(task[0], &task1_state);
            ma_task_get_execution_status(task[0], &task1_exec_status);
            if((MA_TASK_STATE_RUNNING == task1_state)){
                run_count++;
                printf("\nTask Running - <Count: %d>", run_count);
                do{
                    ma_task_get_execution_status(task[0], &task1_exec_status);
                }while(MA_TASK_EXEC_RUNNING == task1_exec_status);
                printf("\nTask execution Running End");
                ma_sheduler_print_last_next_trigger_dates(task[0]);
            }
            if(MA_TASK_STATE_NO_MORE_RUN == task1_state || run_count == g_ma_scheduler_daily_test_globals.run_count) break;
        }while(1);
    }
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release_task_set(task));
    printf("\nExiting Test Case %s", __FUNCTION__);
}

static void uv_thread_daily_test_trigger_multiple_triggers_modify_add(void *param) {
    ma_scheduler_t *scheduler = NULL;
    ma_task_t **task = NULL;
    ma_task_time_t now = {0};
    ma_trigger_t *new_trigger = NULL;
    size_t no_of_tasks = 0;
    size_t run_count = 0;
    ma_trigger_daily_policy_t run_daily_policy = {0};
    ma_task_state_t task1_state = MA_TASK_STATE_NOT_SCHEDULED;
    ma_task_exec_status_t task1_exec_status = MA_TASK_EXEC_NOT_STARTED;
    printf("\nStarting Test Case %s", __FUNCTION__);

    scheduler = (ma_scheduler_t*)param;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_get_task_set(scheduler, &task, &no_of_tasks));
    TEST_ASSERT_EQUAL_INT(1, no_of_tasks);
    run_daily_policy.days_interval = 1;

    if(task) {
        ma_sheduler_print_last_next_trigger_dates(task[0]);
        do{
            ma_task_get_state(task[0], &task1_state);
            ma_task_get_execution_status(task[0], &task1_exec_status);
            if((MA_TASK_STATE_RUNNING == task1_state)){
                run_count++;
                printf("\nTask Running - <Count: %d>", run_count);
                if(1 == run_count) {
                    ma_task_repeat_policy_t repeat_policy = {0};
                    repeat_policy.repeat_interval.hour = 0;
                    repeat_policy.repeat_interval.minute = 2;
                    repeat_policy.repeat_until.hour = 0;
                    repeat_policy.repeat_until.minute = 10;
                    advance_current_date_by_seconds(80, &now);
                    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&run_daily_policy, &new_trigger));
                    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(new_trigger, &now));
                    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task[0], new_trigger));
                    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_repeat_policy(task[0], repeat_policy));
                    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_modify_task(g_scheduler, task[0]));
                }
                do{
                    ma_task_get_execution_status(task[0], &task1_exec_status);
                }while(MA_TASK_EXEC_RUNNING == task1_exec_status);
                printf("\nTask execution Running End");
                ma_sheduler_print_last_next_trigger_dates(task[0]);
            }
            if(MA_TASK_STATE_NO_MORE_RUN == task1_state || run_count == g_ma_scheduler_daily_test_globals.run_count) break;
        }while(1);
    }
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release_task_set(task));
    printf("\nExiting Test Case %s", __FUNCTION__);
}

static void uv_thread_daily_test_trigger_multiple_triggers_modify_remove(void *param) {
    ma_scheduler_t *scheduler = NULL;
    ma_task_t **task = NULL;
    ma_task_time_t now = {0};
    size_t no_of_tasks = 0;
    size_t run_count = 0;
    ma_trigger_daily_policy_t run_daily_policy = {0};
    ma_task_state_t task1_state = MA_TASK_STATE_NOT_SCHEDULED;
    ma_task_exec_status_t task1_exec_status = MA_TASK_EXEC_NOT_STARTED;
    printf("\nStarting Test Case %s", __FUNCTION__);

    scheduler = (ma_scheduler_t*)param;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_get_task_set(scheduler, &task, &no_of_tasks));
    TEST_ASSERT_EQUAL_INT(1, no_of_tasks);
    run_daily_policy.days_interval = 1;

    if(task) {
        ma_sheduler_print_last_next_trigger_dates(task[0]);
        do{
            ma_task_get_state(task[0], &task1_state);
            ma_task_get_execution_status(task[0], &task1_exec_status);
            if((MA_TASK_STATE_RUNNING == task1_state)){
                run_count++;
                get_localtime(&now);
                printf("\nTask Running - <Count: %d>", run_count);
                printf("  <%d-%d-%d:%d:%d:%d>", now.day, now.month, now.year,
                                        now.hour, now.minute, now.secs);
                if(1 == run_count) {
                    char trigger_id[4] = {0};
                    sprintf(trigger_id, "%d", run_count);
                    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_remove_trigger(task[0], trigger_id));
                    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_modify_task(g_scheduler, task[0]));
                }
                do{
                    ma_task_get_execution_status(task[0], &task1_exec_status);
                }while(MA_TASK_EXEC_RUNNING == task1_exec_status);
                printf("\nTask execution Running End");
                ma_sheduler_print_last_next_trigger_dates(task[0]);
            }
            if(MA_TASK_STATE_NO_MORE_RUN == task1_state || run_count == g_ma_scheduler_daily_test_globals.run_count) break;
        }while(1);
    }
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release_task_set(task));
    printf("\nExiting Test Case %s", __FUNCTION__);
}

static void uv_thread_daily_test_trigger_multiple_task_single_trigger(void *param) {
    ma_scheduler_t *scheduler = NULL;
    ma_task_t **task = NULL;
    ma_task_t *task_1 = NULL;
    ma_task_t *task_2 = NULL;
    size_t no_of_tasks = 0;
    size_t run_count = 0;
    ma_task_state_t task_state_1 = MA_TASK_STATE_NOT_SCHEDULED;
    ma_task_exec_status_t task_exec_status_1 = MA_TASK_EXEC_NOT_STARTED;
    ma_task_state_t task_state_2 = MA_TASK_STATE_NOT_SCHEDULED;
    ma_task_exec_status_t task_exec_status_2 = MA_TASK_EXEC_NOT_STARTED;
    printf("\nStarting Test Case %s", __FUNCTION__);

    scheduler = (ma_scheduler_t*)param;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_get_task_set(scheduler, &task, &no_of_tasks));
    TEST_ASSERT_EQUAL_INT(2, no_of_tasks);
    task_1 = task[0];
    task_2 = task[1];
    if(task_1 && task_2) {
        printf("\nTASK-1");
        ma_sheduler_print_last_next_trigger_dates(task_1);
        printf("\nTASK-2");
        ma_sheduler_print_last_next_trigger_dates(task_2);
        do{
            ma_task_get_state(task_1, &task_state_1);
            ma_task_get_execution_status(task_1, &task_exec_status_1);
            ma_task_get_state(task_2, &task_state_2);
            ma_task_get_execution_status(task_2, &task_exec_status_2);
            if((MA_TASK_STATE_RUNNING == task_state_1)){
                run_count++;
                printf("\nUTC executed (%d) times", run_count);
                printf("\nTASK-1");
                ma_sheduler_print_last_next_trigger_dates(task_1);
                printf("\nTASK-2");
                ma_sheduler_print_last_next_trigger_dates(task_2);
                while(MA_TASK_STATE_RUNNING == task_state_1) {
                    ma_task_get_state(task_1, &task_state_1);
                    ma_task_get_state(task_2, &task_state_2);
                    if(MA_TASK_STATE_RUNNING == task_state_2) {
                        printf("\nTASK-2 Started Running Now.");
                        break;
                    }
                }
                continue;
            }
            if((MA_TASK_STATE_RUNNING == task_state_2)){
                run_count++;
                printf("\nUTC executed (%d) times", run_count);
                printf("\nTASK-1");
                ma_sheduler_print_last_next_trigger_dates(task_1);
                printf("\nTASK-2");
                ma_sheduler_print_last_next_trigger_dates(task_2);
                while(MA_TASK_STATE_RUNNING == task_state_2) {
                    ma_task_get_state(task_2, &task_state_2);
                    ma_task_get_state(task_1, &task_state_1);
                    if(MA_TASK_STATE_RUNNING == task_state_1) {
                        printf("\nTASK-1 Started Running Now.");
                        break;
                    }
                }
                continue;
            }
            if(2 == run_count) {
                printf("\nTASK-1");
                ma_sheduler_print_last_next_trigger_dates(task_1);
                printf("\nTASK-2");
                ma_sheduler_print_last_next_trigger_dates(task_2);
                break;
            }
        }while(1);
        printf("\nExiting Test Case %s", __FUNCTION__);
    }
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release_task_set(task));
}

TEST(scheduler_daily_tests, daily_test_single_task) {
    ma_task_t *task_1 = NULL;
    ma_trigger_t *trigger_1 = NULL;
    ma_trigger_daily_policy_t run_daily_policy = {0};
    ma_task_time_t now = {0};
    uv_thread_t tid_1;

    run_daily_policy.days_interval = 1;

    set_os_time(12, 6, 2013, 10, 30, 0);
    get_localtime(&now);
    now.minute += 1;
    now.secs += 2;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(g_scheduler, "Daily task_1", "1001", __FUNCTION__, __FUNCTION__, &task_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&run_daily_policy, &trigger_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_1, trigger_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_1, &now));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time(task_1, 1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time_limit(task_1, 1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_start(g_scheduler));
    TEST_ASSERT_MESSAGE( 0 == uv_thread_create(&tid_1 , uv_thread_daily_test_single_task , (ma_scheduler_t*)g_scheduler) , "Thread could not be created");

    uv_thread_join(&tid_1);
    printf("\nTest Suite %s finished", __FUNCTION__);
}

TEST(scheduler_daily_tests, daily_test_single_task_set_retry) {
    //The basic test case for a daily trigger
    ma_task_t *task_1 = NULL;
    ma_trigger_t *trigger_1 = NULL;
    ma_trigger_daily_policy_t run_daily_policy = {0};
    ma_task_retry_policy_t retry_policy = {0};
    ma_task_time_t now = {0};
    uv_thread_t tid_1;

    retry_policy.retry_count = 2;
    retry_policy.retry_interval.minute = 1;
    run_daily_policy.days_interval = 1;

    set_os_time(12, 6, 2013, 10, 30, 0);
    PRINT_TMSTAMP()
    get_localtime(&now);
    now.minute += 1;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(g_scheduler, "Daily task Set Retry", "1003", __FUNCTION__, __FUNCTION__, &task_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&run_daily_policy, &trigger_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_interval(trigger_1, 20));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_1, trigger_1));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_1, &now));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_retry_policy(task_1, retry_policy));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time(task_1, 1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time_limit(task_1, 1));
    //TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_loop(task_1, g_loop));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_start(g_scheduler));
    TEST_ASSERT_MESSAGE( 0 == uv_thread_create(&tid_1 , uv_thread_daily_test_single_task_set_retry , (ma_scheduler_t*)g_scheduler) , "Thread could not be created");

    uv_thread_join(&tid_1);
    printf("\nTest Suite %s finished", __FUNCTION__);
}

TEST(scheduler_daily_tests, daily_test_single_task_set_retry_with_end_date) {
    //This test case with start and end date same.
    //The trigger expected to fire only once. Then the trigger should be expired/removed.
    ma_task_t *task_1 = NULL;
    ma_trigger_t *trigger_1 = NULL;
    ma_trigger_daily_policy_t run_daily_policy = {0};
    ma_task_retry_policy_t retry_policy = {0};
    ma_task_time_t now = {0};
    ma_task_time_t end_date = {0};
    uv_thread_t tid_1;

    retry_policy.retry_count = 2;
    retry_policy.retry_interval.minute = 1;
    run_daily_policy.days_interval = 1;

    set_os_time(12, 6, 2013, 10, 30, 0);
    PRINT_TMSTAMP()
    get_localtime(&now);
    now.minute += 1;
    end_date = now;
    end_date.hour = 12;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(g_scheduler, "Daily task Set Retry", "1004", __FUNCTION__, __FUNCTION__, &task_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&run_daily_policy, &trigger_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_interval(trigger_1, 20));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_1, trigger_1));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_1, &now));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_end_date(trigger_1, &now));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_retry_policy(task_1, retry_policy));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time(task_1, 1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time_limit(task_1, 1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_start(g_scheduler));
    TEST_ASSERT_MESSAGE( 0 == uv_thread_create(&tid_1 , uv_thread_daily_test_single_task_set_retry , (ma_scheduler_t*)g_scheduler) , "Thread could not be created");

    uv_thread_join(&tid_1);
    printf("\nTest Suite %s finished", __FUNCTION__);
}

TEST(scheduler_daily_tests, daily_test_multiple_trigger) {
    //Multiple triggers are created in one Task.
    //Expected to trigger each trigger within an interval. Then advance to next day.
    ma_task_t *task_1 = NULL;
    ma_trigger_t *trigger_1 = NULL;
    ma_trigger_t *trigger_2 = NULL;
    ma_trigger_daily_policy_t run_daily_policy = {0};
    ma_task_time_t start_time = {0};
    ma_task_time_t end_time = {0};
    uv_thread_t tid_1;

    run_daily_policy.days_interval = 1;

    set_os_time(12, 6, 2013, 10, 30, 0);
    PRINT_TMSTAMP()
    get_localtime(&start_time);
    end_time = start_time;
    start_time.minute += 1;
    //start_time.secs += 15;
    //start_time.hour -= 2; //This is to test for valid end date & invalid start date

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(g_scheduler, "Daily task_1", "1005", __FUNCTION__, __FUNCTION__, &task_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&run_daily_policy, &trigger_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&run_daily_policy, &trigger_2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_1, trigger_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_1, trigger_2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_1, &start_time));
    start_time.minute += 4;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_2, &start_time));
    end_time.day += 4; //This is to test for valid end date & invalid start date
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_end_date(trigger_1, &end_time));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_end_date(trigger_2, &end_time));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time(task_1, 1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time_limit(task_1, 1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_start(g_scheduler));
    TEST_ASSERT_MESSAGE( 0 == uv_thread_create(&tid_1 , uv_thread_daily_test_multiple_trigger , (ma_scheduler_t*)g_scheduler) , "Thread could not be created");

    uv_thread_join(&tid_1);
    printf("\nTest Suite %s finished", __FUNCTION__);
}

TEST(scheduler_daily_tests, daily_test_trigger_boundary_intervals) {
    //This is a general pass condition test case for interval.
    //The trigger should fire at the actual trigger start date.
    //Then proceed firing at the intervals and then advance to next day
    ma_task_t *task_1 = NULL;
    ma_trigger_t *trigger_1 = NULL;
    ma_trigger_daily_policy_t run_daily_policy = {0};
    ma_task_time_t now = {0};
    ma_task_repeat_policy_t repeat_policy;
    uv_thread_t tid_1;

    run_daily_policy.days_interval = 1;

    set_os_time(12, 6, 2013, 10, 30, 0);
    PRINT_TMSTAMP()
    get_localtime(&now);
    now.minute += 1;

    repeat_policy.repeat_interval.hour = 0;
    repeat_policy.repeat_interval.minute = 1;
    repeat_policy.repeat_until.hour = 0;
    repeat_policy.repeat_until.minute = 3;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(g_scheduler, "Daily task Set Interval", "1002", __FUNCTION__, __FUNCTION__, &task_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&run_daily_policy, &trigger_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_1, trigger_1));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_1, &now));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time(task_1, 1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time_limit(task_1, 1));
    ma_task_set_repeat_policy(task_1, repeat_policy);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_start(g_scheduler));
    TEST_ASSERT_MESSAGE( 0 == uv_thread_create(&tid_1 , uv_thread_daily_test_trigger_boundary_intervals , (ma_scheduler_t*)g_scheduler) , "Thread could not be created");

    uv_thread_join(&tid_1);
    printf("\nTest Suite %s finished", __FUNCTION__);
}

TEST(scheduler_daily_tests, daily_test_trigger_misfire_intervals_delayed) {
    //This test case is to test the misfired intervals
    //It will fire for the actual start-trigger time on a day.
    //Then delays for some time to miss the first interval. The trigger is expected to fire
    //the remaining intervals and then proceed to next day trigger
    ma_task_t *task_1 = NULL;
    ma_trigger_t *trigger_1 = NULL;
    ma_trigger_daily_policy_t run_daily_policy = {0};
    ma_task_time_t now = {0};
    ma_task_repeat_policy_t repeat_policy;
    uv_thread_t tid_1;

    run_daily_policy.days_interval = 1;

    set_os_time(12, 6, 2013, 10, 30, 0);
    PRINT_TMSTAMP()
    get_localtime(&now);
    now.minute += 1;

    repeat_policy.repeat_interval.hour = 0;
    repeat_policy.repeat_interval.minute = 1;
    repeat_policy.repeat_until.hour = 0;
    repeat_policy.repeat_until.minute = 3;
    g_ma_scheduler_daily_test_globals.delay_time = 2;
    g_ma_scheduler_daily_test_globals.run_count = 4;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(g_scheduler, "Daily task Set Interval", "1002", __FUNCTION__, __FUNCTION__, &task_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&run_daily_policy, &trigger_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_1, trigger_1));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_1, &now));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time(task_1, 1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time_limit(task_1, 1));
    ma_task_set_repeat_policy(task_1, repeat_policy);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_start(g_scheduler));
    TEST_ASSERT_MESSAGE( 0 == uv_thread_create(&tid_1 , uv_thread_daily_test_trigger_misfire_intervals_delayed , (ma_scheduler_t*)g_scheduler) , "Thread could not be created");

    uv_thread_join(&tid_1);
    printf("\nTest Suite %s finished", __FUNCTION__);
}

TEST(scheduler_daily_tests, daily_test_trigger_misfire_all_intervals) {
    //This test case is to test the misfired intervals
    //It will fire for the actual start-trigger time on a day.
    //Then delays for some time to miss the first interval. The trigger is expected to fire
    //the remaining intervals and then proceed to next day trigger
    ma_task_t *task_1 = NULL;
    ma_trigger_t *trigger_1 = NULL;
    ma_trigger_daily_policy_t run_daily_policy = {0};
    ma_task_time_t now = {0};
    ma_task_repeat_policy_t repeat_policy;
    uv_thread_t tid_1;

    run_daily_policy.days_interval = 1;

    set_os_time(12, 6, 2013, 10, 30, 0);
    PRINT_TMSTAMP()
    get_localtime(&now);
    now.minute += 1;

    repeat_policy.repeat_interval.hour = 0;
    repeat_policy.repeat_interval.minute = 1;
    repeat_policy.repeat_until.hour = 0;
    repeat_policy.repeat_until.minute = 3;
    g_ma_scheduler_daily_test_globals.delay_time = 8;
    g_ma_scheduler_daily_test_globals.run_count = 1;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(g_scheduler, "Daily task Set Interval", "1002", __FUNCTION__, __FUNCTION__, &task_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&run_daily_policy, &trigger_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_1, trigger_1));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_1, &now));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time(task_1, 1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time_limit(task_1, 1));
    ma_task_set_repeat_policy(task_1, repeat_policy);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_start(g_scheduler));
    TEST_ASSERT_MESSAGE( 0 == uv_thread_create(&tid_1 , uv_thread_daily_test_trigger_misfire_intervals_delayed , (ma_scheduler_t*)g_scheduler) , "Thread could not be created");

    uv_thread_join(&tid_1);
    printf("\nTest Suite %s finished", __FUNCTION__);
}

TEST(scheduler_daily_tests, daily_test_trigger_misfire_all_intervals_no_next_trigger) {
    //This test case is to test the misfired intervals
    //It will fire for the actual start-trigger time on a day.
    //Then delays for some time to miss the first interval. The trigger is expected to fire
    //the remaining intervals and then proceed to next day trigger
    ma_task_t *task_1 = NULL;
    ma_trigger_t *trigger_1 = NULL;
    ma_trigger_daily_policy_t run_daily_policy = {0};
    ma_task_time_t now = {0};
    ma_task_time_t end_date = {0};
    ma_task_repeat_policy_t repeat_policy;
    uv_thread_t tid_1;

    run_daily_policy.days_interval = 1;
    g_ma_scheduler_daily_test_globals.run_count = 1;

    set_os_time(12, 6, 2013, 10, 30, 0);
    PRINT_TMSTAMP()
    get_localtime(&now);
    now.minute += 1;
    end_date = now;

    repeat_policy.repeat_interval.hour = 0;
    repeat_policy.repeat_interval.minute = 1;
    repeat_policy.repeat_until.hour = 0;
    repeat_policy.repeat_until.minute = 3;
    g_ma_scheduler_daily_test_globals.delay_time = 8;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(g_scheduler, "Daily task Set Interval", "1002", __FUNCTION__, __FUNCTION__, &task_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&run_daily_policy, &trigger_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_1, trigger_1));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_1, &now));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_end_date(trigger_1, &end_date));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time(task_1, 1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time_limit(task_1, 1));
    ma_task_set_repeat_policy(task_1, repeat_policy);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_start(g_scheduler));
    TEST_ASSERT_MESSAGE( 0 == uv_thread_create(&tid_1 , uv_thread_daily_test_trigger_misfire_intervals_delayed , (ma_scheduler_t*)g_scheduler) , "Thread could not be created");

    uv_thread_join(&tid_1);
    printf("\nTest Suite %s finished", __FUNCTION__);
}

TEST(scheduler_daily_tests, daily_test_trigger_multiple_triggers_delayed) {
    //This test case is to test multiple triggers.
    //Four triggers started with a time interval of 2 minutes and max run time limit of 1 min.
    //Then the task is started after 4 minutes, missing first two triggers.
    //Then check whether the remaining 2 triggers fires at correct timings and then advance to next day.
    ma_task_t *task_1 = NULL;
    ma_trigger_t *trigger_1 = NULL;
    ma_trigger_t *trigger_2 = NULL;
    ma_trigger_t *trigger_3 = NULL;
    ma_trigger_t *trigger_4 = NULL;
    ma_trigger_daily_policy_t run_daily_policy = {0};
    ma_task_time_t now = {0};
    ma_task_time_t end_date = {0};
    uv_thread_t tid_1;

    run_daily_policy.days_interval = 1;

    set_os_time(12, 6, 2013, 10, 30, 0);
    PRINT_TMSTAMP()
    get_localtime(&now);
    now.minute += 1;
    end_date = now;

    g_ma_scheduler_daily_test_globals.delay_time = 4;
    g_ma_scheduler_daily_test_globals.run_count = 2;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(g_scheduler, "Daily task Set Interval", "1002", __FUNCTION__, __FUNCTION__, &task_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&run_daily_policy, &trigger_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&run_daily_policy, &trigger_2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&run_daily_policy, &trigger_3));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&run_daily_policy, &trigger_4));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_1, trigger_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_1, trigger_2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_1, trigger_3));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_1, trigger_4));
    //TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_stop(task_1));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_1, &now));
    now.minute += 2;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_2, &now));
    now.minute += 2;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_3, &now));
    now.minute += 2;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_4, &now));
    //TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_end_date(trigger_1, &end_date));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time(task_1, 1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time_limit(task_1, 1));

#if MA_WINDOWS
                Sleep(g_ma_scheduler_daily_test_globals.delay_time * 60 * 1000);
#else
                sleep(g_ma_scheduler_daily_test_globals.delay_time * 60);
#endif

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_start(g_scheduler));
    TEST_ASSERT_MESSAGE( 0 == uv_thread_create(&tid_1 , uv_thread_daily_test_trigger_multiple_triggers_delayed , (ma_scheduler_t*)g_scheduler) , "Thread could not be created");

    uv_thread_join(&tid_1);
    printf("\nTest Suite %s finished", __FUNCTION__);
}

TEST(scheduler_daily_tests, daily_test_trigger_multiple_triggers_modify_add) {
    //This test case is to test modify task with the same handle.
    ma_task_t *task_1 = NULL;
    ma_trigger_t *trigger_1 = NULL;
    ma_trigger_daily_policy_t run_daily_policy = {0};
    ma_task_time_t now = {0};
    ma_task_time_t end_date = {0};
    uv_thread_t tid_1;
    ma_task_repeat_policy_t repeat_policy;

    run_daily_policy.days_interval = 1;

    PRINT_TMSTAMP()
    advance_current_date_by_seconds(10, &now);
    end_date = now;

    g_ma_scheduler_daily_test_globals.delay_time = 4;
    g_ma_scheduler_daily_test_globals.run_count = 5;
    repeat_policy.repeat_interval.hour = 0;
    repeat_policy.repeat_interval.minute = 1;
    repeat_policy.repeat_until.hour = 0;
    repeat_policy.repeat_until.minute = 3;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(g_scheduler, "Daily task Set Interval", "1002", __FUNCTION__, __FUNCTION__, &task_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&run_daily_policy, &trigger_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_1, trigger_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_1, &now));
    ma_task_set_repeat_policy(task_1, repeat_policy);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time_limit(task_1, 1));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_start(g_scheduler));
    TEST_ASSERT_MESSAGE( 0 == uv_thread_create(&tid_1 , uv_thread_daily_test_trigger_multiple_triggers_modify_add , (ma_scheduler_t*)g_scheduler) , "Thread could not be created");

    uv_thread_join(&tid_1);
    printf("\nTest Suite %s finished", __FUNCTION__);
}

TEST(scheduler_daily_tests, daily_test_trigger_multiple_triggers_modify_remove) {
    //This test case is to test modify task with the same handle.
    ma_task_t *task_1 = NULL;
    ma_trigger_t *trigger_1 = NULL;
    ma_trigger_t *trigger_2 = NULL;
    ma_trigger_t *trigger_3 = NULL;
    ma_trigger_daily_policy_t run_daily_policy = {0};
    ma_trigger_weekly_policy_t run_weekly_policy = {0};
    ma_trigger_monthly_date_policy_t run_monthly_date_policy = {0};
    ma_task_time_t now = {0};
    uv_thread_t tid_1;
    ma_task_repeat_policy_t repeat_policy;

    run_daily_policy.days_interval = 1;
    run_weekly_policy.days_of_the_week = WEDNESDAY;
    run_weekly_policy.weeks_interval = 2;
    run_monthly_date_policy.date = 4;
    run_monthly_date_policy.months = AUGUST;

    PRINT_TMSTAMP()

    g_ma_scheduler_daily_test_globals.delay_time = 4;
    g_ma_scheduler_daily_test_globals.run_count = 5;
    repeat_policy.repeat_interval.hour = 0;
    repeat_policy.repeat_interval.minute = 1;
    repeat_policy.repeat_until.hour = 0;
    repeat_policy.repeat_until.minute = 3;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(g_scheduler, "Daily task Set Interval", "2001", __FUNCTION__, __FUNCTION__, &task_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&run_daily_policy, &trigger_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_weekly(&run_weekly_policy, &trigger_2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_monthly_date(&run_monthly_date_policy, &trigger_3));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_1, trigger_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_1, trigger_2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_1, trigger_3));
    advance_current_date_by_seconds(20, &now);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_1, &now));
    advance_current_date_by_seconds(150, &now);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_2, &now));
    advance_current_date_by_seconds(250, &now);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_3, &now));
    //ma_task_set_repeat_policy(task_1, repeat_policy);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time_limit(task_1, 1));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_start(g_scheduler));
    TEST_ASSERT_MESSAGE( 0 == uv_thread_create(&tid_1 , uv_thread_daily_test_trigger_multiple_triggers_modify_remove , (ma_scheduler_t*)g_scheduler) , "Thread could not be created");

    uv_thread_join(&tid_1);
    printf("\nTest Suite %s finished", __FUNCTION__);
}

TEST(scheduler_daily_tests, daily_test_trigger_multiple_task_single_trigger) {
    //This test case is to test multiple tasks.
    //Two tasks started mapped to each associated trigger.
    ma_task_t *task_1 = NULL;
    ma_task_t *task_2 = NULL;
    ma_trigger_t *trigger_1 = NULL;
    ma_trigger_t *trigger_2 = NULL;
    ma_trigger_daily_policy_t run_daily_policy = {0};
    ma_task_time_t now = {0};
    ma_task_time_t end_date = {0};
    uv_thread_t tid_1;

    run_daily_policy.days_interval = 1;

    set_os_time(12, 6, 2013, 10, 30, 0);
    PRINT_TMSTAMP()
    get_localtime(&now);
    now.minute += 1;
    end_date = now;

    g_ma_scheduler_daily_test_globals.delay_time = 4;
    g_ma_scheduler_daily_test_globals.run_count = 2;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(g_scheduler, "Daily task 1", "1001", __FUNCTION__, __FUNCTION__, &task_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(g_scheduler, "Daily task 2", "1002", __FUNCTION__, __FUNCTION__, &task_2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&run_daily_policy, &trigger_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&run_daily_policy, &trigger_2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_1, trigger_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_2, trigger_2));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_1, &now));
    now.minute += 2;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_2, &now));
    //TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_end_date(trigger_1, &end_date));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time(task_1, 1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time_limit(task_1, 1));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_start(g_scheduler));
    TEST_ASSERT_MESSAGE( 0 == uv_thread_create(&tid_1 , uv_thread_daily_test_trigger_multiple_task_single_trigger , (ma_scheduler_t*)g_scheduler) , "Thread could not be created");

    uv_thread_join(&tid_1);
    printf("\nTest Suite %s finished", __FUNCTION__);
}
