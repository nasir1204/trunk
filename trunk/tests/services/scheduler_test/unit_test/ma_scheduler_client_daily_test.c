//#if 0
#include "unity.h"
#include "unity_fixture.h"

#include "ma/ma_common.h"
#include "ma/internal/ma_macros.h"
#include "ma/scheduler/ma_scheduler.h"
#include "ma/scheduler/ma_triggers.h"
#include "ma/scheduler/ma_task.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"
#include "ma/scheduler/ma_scheduler_defs.h"
#include "ma/internal/services/scheduler/ma_scheduler_service.h"
#include "ma/internal/services/ma_service.h"
#include "ma/internal/core/msgbus/ma_msgbus_impl.h"

#include <uv.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


#ifdef MA_WINDOWS
# include <crtdbg.h>
#include <Windows.h>
# define MA_TRACKLEAKS _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF)
# define MA_INPUT_FOLDER_PATH ".\\infile.xml"
# define MA_OUTPUT_FOLDER_PATH ".\\outfile.xml"
#else
# define MA_TRACKLEAKS ((void *)0)
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
# define MA_INPUT_FOLDER_PATH "./infile.xml"
# define MA_OUTPUT_FOLDER_PATH "./outfile.xml"
#endif

#define GLOBAL_DATABASE_NAME_FOR_UT    "g_datastore_scheduler_client_test.db"

static ma_db_t *g_database_handle = NULL;
static ma_msgbus_t *msgbus_instance = NULL;
static uv_timer_t timer;
static uv_loop_t *loop = NULL;
static ma_ds_t *g_ds = NULL;
static ma_event_loop_t *event_loop = NULL;
static ma_scheduler_service_t *service = NULL;
static size_t on_task_execute_ctr = 0;
static size_t on_task_stop_ctr = 0;

extern ma_context_t *g_ma_context;
extern ma_msgbus_t *g_msgbus;

TEST_GROUP_RUNNER(scheduler_client_daily_test_group) {
    do {RUN_TEST_CASE(scheduler_client_daily_test, start_service); }while(0); // it should always executed first to ensure scheduler service is running before client requests
    do {RUN_TEST_CASE(scheduler_client_daily_test, add_task_single_trigger_retry_test); }while(0);
    do {RUN_TEST_CASE(scheduler_client_daily_test, remove_task_multiple_task_single_trigger_test); }while(0);
    do {RUN_TEST_CASE(scheduler_client_daily_test, modify_task_single_task_multiple_trigger_test); }while(0);
    do {RUN_TEST_CASE(scheduler_client_daily_test, multiple_triggers_intervals_test); }while(0);
    do {RUN_TEST_CASE(scheduler_client_daily_test, stop_service); }while(0); // don't forget to stop and release the service
}

TEST_SETUP(scheduler_client_daily_test) {
    
}

TEST_TEAR_DOWN(scheduler_client_daily_test) {
    
}

static void scheduler_client_daily_test_reset_counters() {
    on_task_execute_ctr = 0;
    on_task_stop_ctr = 0;
}

static void timer_evt(uv_timer_t *handle, int status) {
}

static ma_error_t modify_single_task_multiple_trigger_task_execute(ma_context_t *context, const char *task_type, const ma_task_t *task, void *cb_data) {
    ma_task_time_t ttime = {0};
    char *task_id = NULL;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id((ma_task_t *)task, &task_id));
    get_localtime(&ttime);
    printf("\nExecuting Task <%s> - ", task_id);
    printf("<%d-%d-%d:%d:%d:%d>", ttime.day, ttime.month,ttime.year, ttime.hour, ttime.minute, ttime.secs);
    on_task_execute_ctr++;
    return MA_OK;
}

static ma_error_t modify_single_task_multiple_trigger_task_stop(ma_context_t *context, const char *task_type, const ma_task_t *task, void *cb_data) {
    ma_task_time_t ttime = {0};
    char *task_id = NULL;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id((ma_task_t *)task, &task_id));
    get_localtime(&ttime);
    printf("\nStopping Task <%s> - ", task_id);
    printf("<%d-%d-%d:%d:%d:%d>", ttime.day, ttime.month,ttime.year, ttime.hour, ttime.minute, ttime.secs);
    on_task_stop_ctr++;
    return MA_OK;
}

static ma_error_t modify_single_task_multiple_trigger_task_remove(ma_context_t *context, const char *task_type, const ma_task_t *task, void *cb_data) {
    return MA_OK;
}

static ma_error_t add_task_single_trigger_retry_test_task_execute(ma_context_t *context, const char *task_type, const ma_task_t *task, void *cb_data) {
    ma_task_time_t ttime = {0};
    char *task_id = NULL;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id((ma_task_t *)task, &task_id));
    get_localtime(&ttime);
    printf("\nExecuting Task <%s> - ", task_id);
    printf("<%d-%d-%d:%d:%d:%d>", ttime.day, ttime.month,ttime.year, ttime.hour, ttime.minute, ttime.secs);
    on_task_execute_ctr++;
    ma_scheduler_update_task_status(context, task_id, MA_TASK_STATUS_FAILURE);
    return MA_OK;
}

static ma_error_t add_task_single_trigger_retry_test_task_stop(ma_context_t *context, const char *task_type, const ma_task_t *task, void *cb_data) {
    ma_task_time_t ttime = {0};
    char *task_id = NULL;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id((ma_task_t *)task, &task_id));
    get_localtime(&ttime);
    printf("\nStopping Task <%s> - ", task_id);
    printf("<%d-%d-%d:%d:%d:%d>", ttime.day, ttime.month,ttime.year, ttime.hour, ttime.minute, ttime.secs);
    on_task_stop_ctr++;
    return MA_OK;
}

static ma_error_t add_task_single_trigger_retry_test_task_remove(ma_context_t *context, const char *task_type, const ma_task_t *task, void *cb_data) {
    return MA_OK;
}

static ma_error_t remove_task_multiple_task_single_trigger_test_execute(ma_context_t *context, const char *task_type, const ma_task_t *task, void *cb_data) {
    ma_task_time_t ttime = {0};
    char *task_id = NULL;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id((ma_task_t *)task, &task_id));
    get_localtime(&ttime);
    printf("\nExecuting Task <%s> - ", task_id);
    printf("<%d-%d-%d:%d:%d:%d>", ttime.day, ttime.month,ttime.year, ttime.hour, ttime.minute, ttime.secs);
    ma_scheduler_update_task_status(context, task_id, MA_TASK_STATUS_SUCCESS);
    if(!strcmp(task_id, "ma.daily.1002")) {
        printf("\nTest Case Failed. Triggering for Task - \"ma.daily.1002\"");
    }
    on_task_execute_ctr++;
    return MA_OK;
}

static ma_error_t remove_task_multiple_task_single_trigger_test_stop(ma_context_t *context, const char *task_type, const ma_task_t *task, void *cb_data) {
    ma_task_time_t ttime = {0};
    char *task_id = NULL;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id((ma_task_t *)task, &task_id));
    get_localtime(&ttime);
    printf("\nStopping Task <%s> - ", task_id);
    printf("<%d-%d-%d:%d:%d:%d>", ttime.day, ttime.month,ttime.year, ttime.hour, ttime.minute, ttime.secs);
    on_task_stop_ctr++;
    return MA_OK;
}

static ma_error_t remove_task_multiple_task_single_trigger_test_remove(ma_context_t *context, const char *task_type, const ma_task_t *task, void *cb_data) {
    return MA_OK;
}

static ma_error_t multiple_triggers_intervals_test_task_execute(ma_context_t *context, const char *task_type, const ma_task_t *task, void *cb_data) {
    ma_task_time_t ttime = {0};
    char *task_id = NULL;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id((ma_task_t *)task, &task_id));
    get_localtime(&ttime);
    printf("\nExecuting Task <%s> - ", task_id);
    printf("<%d-%d-%d:%d:%d:%d>", ttime.day, ttime.month,ttime.year, ttime.hour, ttime.minute, ttime.secs);
    ma_scheduler_update_task_status(context, task_id, MA_TASK_STATUS_SUCCESS);
    on_task_execute_ctr++;
    return MA_OK;
}

static ma_error_t multiple_triggers_intervals_test_task_stop(ma_context_t *context, const char *task_type, const ma_task_t *task, void *cb_data) {
    ma_task_time_t ttime = {0};
    char *task_id = NULL;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id((ma_task_t *)task, &task_id));
    get_localtime(&ttime);
    printf("\nStopping Task <%s> - ", task_id);
    printf("<%d-%d-%d:%d:%d:%d>", ttime.day, ttime.month,ttime.year, ttime.hour, ttime.minute, ttime.secs);
    on_task_stop_ctr++;
    return MA_OK;
}

static ma_error_t multiple_triggers_intervals_test_task_remove(ma_context_t *context, const char *task_type, const ma_task_t *task, void *cb_data) {
    return MA_OK;
}


TEST(scheduler_client_daily_test, start_service) {
    ma_ds_database_t *ds_database = NULL;
    Unity.TestFile = "ma_scheduler_client_daily_test.c";

    /* 
    * initialize the message bus
    * get its event loop
    * add a dummy timer event to keep the event loop non-empty.
    */
    //ma_msgbus_initialize();
    //msgbus_instance = ma_msgbus_get_instance();
    msgbus_instance = g_msgbus;
    event_loop = ma_msgbus_get_event_loop(msgbus_instance);
    loop = ma_event_loop_get_uv_loop(event_loop);
    uv_timer_init(loop, &timer);
    uv_timer_start(&timer, timer_evt, 1 * 1000, 1 * 1000);

    // create database data store
    unlink(GLOBAL_DATABASE_NAME_FOR_UT);
    TEST_ASSERT_MESSAGE(MA_OK == ma_db_intialize() , "ma_db_initialize failed");
    TEST_ASSERT_MESSAGE(MA_OK ==  ma_db_open( GLOBAL_DATABASE_NAME_FOR_UT,  &g_database_handle), "ma_db_open failed to open global database datatbase");
    TEST_ASSERT_MESSAGE(MA_OK ==  ma_ds_database_create(g_database_handle , "Policies" , &ds_database), "ma_ds_database_create failed to create the instance");
    g_ds = (ma_ds_t *)ds_database;


    // start the scheduler service
    TEST_ASSERT_MESSAGE(MA_OK == ma_scheduler_service_create(g_msgbus, NULL, g_ds, &service), "scheduler service creation failed");
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_service_start((ma_service_t *)service));
}

TEST(scheduler_client_daily_test, modify_task_single_task_multiple_trigger_test) {
    ma_task_t *task_1 = NULL;
    ma_trigger_t *trigger_1 = NULL;
    ma_trigger_t *trigger_2 = NULL;
    ma_trigger_t *trigger_3 = NULL;
    ma_trigger_daily_policy_t policy = {0};
    ma_task_time_t start_date = {0};
    ma_variant_t *app_payload = NULL;
    const char *trigger_id = NULL;
    //ma_bool_t trigger_removed = MA_FALSE;
    ma_scheduler_task_callbacks_t cb = {&modify_single_task_multiple_trigger_task_execute,
        &modify_single_task_multiple_trigger_task_stop,\
        &modify_single_task_multiple_trigger_task_remove};
    printf("\nStarting Test Suite - <%s>", __FUNCTION__);
    
    policy.days_interval = 4;

    scheduler_client_daily_test_reset_counters();

    advance_current_date_by_seconds(40, &start_date);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&policy, &trigger_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&policy, &trigger_2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&policy, &trigger_3));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_1, &start_date));
    start_date.minute += 1;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_2, &start_date));
    start_date.minute += 1;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_3, &start_date));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_create("ma.daily.1001", __FUNCTION__, __FUNCTION__, &task_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_1, trigger_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_1, trigger_2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_1, trigger_3));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_string(__FUNCTION__, strlen(__FUNCTION__), &app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_app_payload(task_1, app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_creator_id(task_1, __FUNCTION__));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time_limit(task_1, 1));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_register_task_callbacks(g_ma_context, "Modify", &cb, NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_add_task(g_ma_context, task_1));
    
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_get_id(trigger_2, &trigger_id));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_remove_trigger(task_1, trigger_id));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_modify_task(g_ma_context, task_1));
    
    // wait for the published messages
    while(on_task_execute_ctr < 2) {
    }
         
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_release(task_1));
    printf("\nStopping Test Suite - <%s>", __FUNCTION__);
}

TEST(scheduler_client_daily_test, remove_task_multiple_task_single_trigger_test) {
    ma_task_t *task_1 = NULL;
    ma_task_t *task_2 = NULL;
    ma_task_t *task_3 = NULL;
    ma_trigger_t *trigger_1 = NULL;
    ma_trigger_t *trigger_2 = NULL;
    ma_trigger_t *trigger_3 = NULL;
    ma_trigger_daily_policy_t policy = {0};
    ma_task_time_t start_date = {0};
    ma_task_time_t next_run_time = {0};
    ma_variant_t *app_payload = NULL;
    ma_bool_t task_removed = MA_FALSE;
    ma_scheduler_task_callbacks_t cb = {&remove_task_multiple_task_single_trigger_test_execute,
        &remove_task_multiple_task_single_trigger_test_stop,\
        &remove_task_multiple_task_single_trigger_test_remove};
    printf("\nStarting Test Suite - <%s>", __FUNCTION__);
    
    policy.days_interval = 4;

    scheduler_client_daily_test_reset_counters();

    advance_current_date_by_seconds(20, &start_date);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&policy, &trigger_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&policy, &trigger_2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&policy, &trigger_3));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_1, &start_date));
    start_date.minute += 1;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_2, &start_date));
    start_date.minute += 1;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_3, &start_date));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_create("ma.daily.1001", __FUNCTION__, __FUNCTION__, &task_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_create("ma.daily.1002", __FUNCTION__, __FUNCTION__, &task_2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_create("ma.daily.1003", __FUNCTION__, __FUNCTION__, &task_3));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_1, trigger_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_2, trigger_2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_3, trigger_3));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_string(__FUNCTION__, strlen(__FUNCTION__), &app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_app_payload(task_1, app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_app_payload(task_2, app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_app_payload(task_3, app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_creator_id(task_1, __FUNCTION__));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_creator_id(task_2, __FUNCTION__));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_creator_id(task_3, __FUNCTION__));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time_limit(task_1, 1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time_limit(task_2, 1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time_limit(task_3, 1));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_register_task_callbacks(g_ma_context, "Remove", &cb, NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_add_task(g_ma_context, task_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_add_task(g_ma_context, task_2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_add_task(g_ma_context, task_3));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_remove_task(g_ma_context, "ma.daily.1002"));
    
    // wait for the published messages
    while(on_task_execute_ctr < 3) {
        //if((1 == on_task_execute_ctr) && (MA_FALSE == task_removed)) {
        //    task_removed = MA_TRUE;
        //    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_consumer_remove_task(scheduler_consumer, "ma.daily.1002"));
        //}
    }
         
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_release(task_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger_2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_release(task_2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger_3));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_release(task_3));
    printf("\nStopping Test Suite - <%s>", __FUNCTION__);
}

TEST(scheduler_client_daily_test, add_task_single_trigger_retry_test) {
    ma_task_t *task = NULL;
    ma_trigger_daily_policy_t policy = {0};
    ma_task_retry_policy_t retry_policy = {0};
    ma_trigger_t *trigger = NULL;
    ma_task_time_t start_date = {0};
    ma_task_time_t next_run_time = {0};
    ma_variant_t *app_payload = NULL;
    ma_scheduler_task_callbacks_t cb = {&add_task_single_trigger_retry_test_task_execute,
        &add_task_single_trigger_retry_test_task_stop,\
        &add_task_single_trigger_retry_test_task_remove};
    printf("\nStarting Test Suite - <%s>", __FUNCTION__);
    
    policy.days_interval = 2;

    retry_policy.retry_count = 2;
    retry_policy.retry_interval.minute = 1;
    scheduler_client_daily_test_reset_counters();

    advance_current_date_by_seconds(20, &start_date);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&policy, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger, &start_date));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_create("ma.daily.1001", __FUNCTION__, __FUNCTION__, &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_string(__FUNCTION__, strlen(__FUNCTION__), &app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_app_payload(task, app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_creator_id(task, __FUNCTION__));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_retry_policy(task, retry_policy));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time(task, 1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time_limit(task, 1));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_register_task_callbacks(g_ma_context, "Add", &cb, NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_add_task(g_ma_context, task));
    
    // wait for the published messages
    while(on_task_execute_ctr < 3);
         
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_release(task));
    printf("\nStopping Test Suite - <%s>", __FUNCTION__);
}

TEST(scheduler_client_daily_test, multiple_triggers_intervals_test) {
    ma_task_t *task_1 = NULL;
    ma_task_t *task_2 = NULL;
    ma_trigger_daily_policy_t policy = {0};
    ma_task_repeat_policy_t repeat_policy;
    ma_trigger_t *trigger_1 = NULL;
    ma_trigger_t *trigger_2 = NULL;
    ma_task_time_t start_date = {0};
    ma_task_time_t next_run_time = {0};
    ma_variant_t *app_payload = NULL;
    ma_scheduler_task_callbacks_t cb = {&multiple_triggers_intervals_test_task_execute,
        &multiple_triggers_intervals_test_task_stop,\
        &multiple_triggers_intervals_test_task_remove};
    printf("\nStarting Test Suite - <%s>", __FUNCTION__);
    
    policy.days_interval = 2;
    repeat_policy.repeat_interval.hour = 0;
    repeat_policy.repeat_interval.minute = 1;
    repeat_policy.repeat_until.hour = 0;
    repeat_policy.repeat_until.minute = 4;

    scheduler_client_daily_test_reset_counters();

    advance_current_date_by_seconds(10, &start_date);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&policy, &trigger_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&policy, &trigger_2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_1, &start_date));
    start_date.minute += 6;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_2, &start_date));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_end_date(trigger_1, &start_date));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_end_date(trigger_2, &start_date));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_create("ma.daily.1001", __FUNCTION__, __FUNCTION__, &task_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_create("ma.daily.2001", __FUNCTION__, __FUNCTION__, &task_2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_1, trigger_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_2, trigger_2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_string(__FUNCTION__, strlen(__FUNCTION__), &app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_app_payload(task_1, app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_app_payload(task_2, app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_creator_id(task_1, __FUNCTION__));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_creator_id(task_2, __FUNCTION__));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time_limit(task_1, 1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time_limit(task_2, 1));
    ma_task_set_repeat_policy(task_1, repeat_policy);

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_register_task_callbacks(g_ma_context, "Add", &cb, NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_add_task(g_ma_context, task_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_add_task(g_ma_context, task_2));
    
    // wait for the published messages
    while(on_task_execute_ctr < 4);
         
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_release(task_1));
    printf("\nStopping Test Suite - <%s>", __FUNCTION__);
}

TEST(scheduler_client_daily_test, stop_service) {
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_service_stop((ma_service_t *)service));
    TEST_ASSERT_MESSAGE(MA_OK == ma_scheduler_service_release(service), "scheduler service release failed");
    TEST_ASSERT_MESSAGE(MA_OK ==  ma_db_close(g_database_handle), "ma_db_close failed to close the global datatbase");
    TEST_ASSERT_MESSAGE(MA_OK ==  ma_ds_database_release((ma_ds_database_t*)g_ds), "ma_ds_database failed to release the instance");
    TEST_ASSERT_MESSAGE(MA_OK == ma_db_deintialize() , "ma_db_deintialize failed");   
    unlink(GLOBAL_DATABASE_NAME_FOR_UT);
    uv_timer_stop(&timer);
    //ma_msgbus_deinitialize();
}

//#endif
