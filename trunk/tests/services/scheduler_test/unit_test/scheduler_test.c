
#include "unity.h"
#include "unity_fixture.h"

#include "ma/ma_common.h"
#include "ma/internal/ma_macros.h"
#include "ma/ma_variant.h"
#include "ma/internal/services/scheduler/ma_scheduler_core.h"
#include "ma/scheduler/ma_triggers.h"
#include "ma/scheduler/ma_task.h"
#include "ma/scheduler/ma_triggerlist.h"
#include "ma/internal/core/msgbus/ma_msgbus_private.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"
#include "ma/internal/services/scheduler/ma_runningtask_collector.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"
#include "ma/internal/services/scheduler/ma_triggers_internal.h"
#include "ma/internal/services/scheduler/ma_task_internal.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/logger/ma_file_logger.h"
#include "ma_ut_setup.h"

#include <uv.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#ifdef MA_WINDOWS
# include <crtdbg.h>
#include <Windows.h>
# define MA_TRACKLEAKS _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF)
# define MA_INPUT_FOLDER_PATH ".\\infile.xml"
# define MA_OUTPUT_FOLDER_PATH ".\\outfile.xml"
#else
# define MA_TRACKLEAKS ((void *)0)
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
# define MA_INPUT_FOLDER_PATH "./infile.xml"
# define MA_OUTPUT_FOLDER_PATH "./outfile.xml"
#endif

static ma_bool_t shut_down = MA_FALSE;
static ma_db_t *g_database_handle = NULL ;
ma_ds_t *g_ds = NULL ;
uv_loop_t *g_loop = NULL;
ma_scheduler_t *g_scheduler = NULL;


#define GLOBAL_DATABASE_NAME_FOR_UT    "g_datastoreut.db"

ma_msgbus_t *g_msgbus = NULL;
ma_ut_setup_t *g_ut_setup = NULL;

void set_os_time(size_t day, size_t month, size_t yr, size_t hr, size_t min, size_t sec) {
    ma_task_time_t tdate;
    ma_int64_t timeout = 0;
#ifdef MA_WINDOWS
    SYSTEMTIME os_time = {0};
#else
	struct timeval now = {0};
    time_t ttime = 0;
    struct tm tm_date = {0};
#endif
    tdate.day = day;
    tdate.month = month;
    tdate.year = yr;
    tdate.hour = hr;
    tdate.minute = min;
    tdate.secs = sec;
    compute_timeout(&tdate, &timeout);
#ifdef MA_WINDOWS
    os_time.wDay = day;
    os_time.wMonth = month;
    os_time.wYear = yr;
    os_time.wHour = hr;
    os_time.wMinute = min;
    os_time.wSecond = sec;
    if(0 == SetLocalTime(&os_time))
        printf("\nFailed  %s with error code %ld \n", __FUNCTION__, GetLastError());
#else
    tm_date.tm_hour = hr;
    tm_date.tm_min = min;
    tm_date.tm_sec = 0;
    tm_date.tm_mday = day;
    tm_date.tm_mon = month;
    tm_date.tm_year = yr;
    ttime = mktime(&tm_date);

    now.tv_sec = ttime;
    settimeofday(&now, NULL);
#endif
    g_loop->time = timeout * 1000;
    uv_update_time(g_loop);
}

static void timer_close_cb(uv_handle_t* handle) {
    printf("\nClosing the loop...");
}

static void loop_timer_cb(uv_timer_t *loop_timer, int status) {
    if(MA_TRUE == shut_down) {
        uv_timer_stop(loop_timer);
    }
    uv_timer_set_repeat(loop_timer, 60 * 1000);
    uv_timer_again(loop_timer);
}

#if (9 < UV_VERSION_MINOR)
#define UV10_ONLY_ARGS ,UV_RUN_DEFAULT    
#else
#define UV10_ONLY_ARGS
#endif


static void ma_scheduler_create_loop_thread(void *param) {
    uv_timer_t loop_timer;
    g_loop =  ma_event_loop_get_uv_loop(ma_msgbus_get_event_loop(g_msgbus));
    //g_loop = uv_default_loop();
    uv_timer_init(g_loop, &loop_timer);
    uv_timer_start(&loop_timer, &loop_timer_cb, (60 * 1000), 0);
    uv_run(g_loop UV10_ONLY_ARGS);
    uv_close((uv_handle_t*)&loop_timer, &timer_close_cb);
}


TEST_GROUP_RUNNER(scheduler_test_group) {
    //scheduler Test Cases
    do {RUN_TEST_CASE(scheduler_tests, ma_scheduler_create); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_scheduler_create_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_scheduler_start_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_scheduler_get_state_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_calculate_week_day); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, compute_until_time); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_calculate_day_of_week); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_calculate_date_of_month); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_scheduler_stop); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_scheduler_stop_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_scheduler_release); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_scheduler_release_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_scheduler_get_runnning_task_set_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_scheduler_get_task_set_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_scheduler_release_runnning_task_set_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_scheduler_release_task_set_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_scheduler_create_task_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_scheduler_get_task_handle_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_scheduler_release_task_invalid); }while(0);
    //Task Collector Test Cases
    do {RUN_TEST_CASE(scheduler_tests, ma_running_task_collector_create_release_valid_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_running_task_collector_add_valid_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_running_task_collector_find_valid_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_running_task_collector_remove_size_valid_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_running_task_collector_start_stop_valid_invalid); }while(0);
    //Scheduler Utils Test Cases
    do {RUN_TEST_CASE(scheduler_tests, convert_sec_to_hr_and_min_get_loctm_valid_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, get_next_date_compute_timeout_valid_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, get_week_day_valid_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, get_next_week_date_after_valid_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, get_next_date_after_valid_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, is_today_last_day_valid_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, get_next_month_date_after_valid_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, get_next_month_dow_date_after_valid_invalid); }while(0);
    //Trigger Test Cases
    do {RUN_TEST_CASE(scheduler_tests, ma_trigger_create_run_once_valid_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_trigger_create_daily_valid_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_trigger_create_weekly_valid_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_trigger_create_monthly_date_valid_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_trigger_create_monthly_date_valid_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_trigger_duration_valid_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_trigger_interval_valid_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_trigger_random_interval_valid_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_trigger_disable_valid_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_trigger_loop_valid_invalid); }while(0);
    //Task test
    do {RUN_TEST_CASE(scheduler_tests, ma_scheduler_create_task_release_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_task_set_retry_policy); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_task_set_retry_policy_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_task_set_repeat_policy); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_task_set_repeat_policy_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_task_set_max_run_time); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_task_set_max_run_time_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_task_set_max_run_time_limit); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_task_set_max_run_time_limit_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_task_set_randomize_policy); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_task_set_randomize_policy_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_task_set_priority); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_task_set_priority_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_task_set_loop); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_task_set_loop_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_task_set_conditions); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_task_set_conditions_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_task_set_delay_policy); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_task_set_delay_policy_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_task_get_state); }while(0);//some issue when executed in group
    do {RUN_TEST_CASE(scheduler_tests, ma_task_get_state_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_task_get_next_runtime_after_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_task_get_last_run_time_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_task_get_next_run_time_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_task_get_priority); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_task_get_priority_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_task_get_conditions); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_task_get_conditions_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_task_get_id); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_task_get_id_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_task_get_retry_policy); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_task_get_max_run_time); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_task_get_max_run_time_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_task_get_max_run_time_limit); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_task_get_max_run_time_limit_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_task_get_loop); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_task_get_loop_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_task_get_execution_status); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_task_get_execution_status_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_task_add_trigger_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_task_remove_trigger); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_task_remove_trigger_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_task_find_trigger); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_task_find_trigger_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_task_stop_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, date_validator); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_task_set_execution_status); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_task_set_execution_status_invalid); }while(0);
#if 0	
    //Trigger
    do {RUN_TEST_CASE(scheduler_tests, ma_trigger_begin_end_date_valid_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_trigger_get_triggertype_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_trigger_get_id_valid_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_scheduler_create_release_test); }while(0);
	
	//Triggerlist test
    do {RUN_TEST_CASE(scheduler_tests, ma_scheduler_task_copy); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_trigger_get_state); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_task_get_next_run_time); }while(0);// releasing scheduler has issue
    do {RUN_TEST_CASE(scheduler_tests, ma_task_get_last_run_time); }while(0);//releasing scheduler has issue
    do {RUN_TEST_CASE(scheduler_tests, ma_task_get_retry_policy_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_scheduler_release1); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_scheduler_release2); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_scheduler_start); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_scheduler_get_state_tst); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, triggerlist_add_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_trigger_list_find_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_trigger_list_find); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, triggerlist_add_remove_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, triggerlist_create_release_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, ma_scheduler_get_runnning_task_set); }while(0); //Not added need to check
    do {RUN_TEST_CASE(scheduler_tests, logon_trigger_create_delete); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, system_start_trigger_create_delete); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, scheduler_add_valid_invalid); }while(0);
    do {RUN_TEST_CASE(scheduler_tests, scheduler_remove_valid_invalid); }while(0);
#endif

}

ma_error_t ma_ut_create(const char *product_id) {
    Unity.TestFile = "scheduler_test.c";
    /* Remove if any from previous run */
    unlink(GLOBAL_DATABASE_NAME_FOR_UT);
   	if(product_id) {
        ma_error_t rc = MA_OK;
              
		TEST_ASSERT_MESSAGE(MA_OK ==  ma_db_open( GLOBAL_DATABASE_NAME_FOR_UT,  MA_DB_OPEN_READWRITE, &g_database_handle), "ma_db_open failed to open global database datatbase");
		TEST_ASSERT_MESSAGE(MA_OK ==  ma_ds_database_create(g_database_handle , "Task", (ma_ds_database_t **)&g_ds), "ma_ds_database_create failed to create the instance");
        if(MA_OK != (rc = ma_msgbus_create(product_id, &g_msgbus))) {   
            return rc;
        }

		if(MA_OK != (rc = ma_msgbus_start(g_msgbus))) {
			ma_msgbus_release(g_msgbus);
            return rc;
        }
		return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

ma_error_t ma_ut_release() {
    if(g_msgbus) {
		ma_error_t rc = MA_OK;

		TEST_ASSERT_MESSAGE(MA_OK ==  ma_db_close(g_database_handle), "ma_db_close failed to close the global datatbase");
		TEST_ASSERT_MESSAGE(MA_OK ==  ma_ds_database_release((ma_ds_database_t*)g_ds), "ma_ds_database failed to release the instance");
		unlink(GLOBAL_DATABASE_NAME_FOR_UT);
		if(MA_OK != (rc = ma_msgbus_stop(g_msgbus, MA_TRUE))) {   
			return rc;
		}

		if(MA_OK != (rc = ma_msgbus_release(g_msgbus))) {
			return rc;
		}
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
    
}

TEST_SETUP(scheduler_tests) {
   TEST_ASSERT_MESSAGE(MA_OK == ma_ut_create("scheduler_test"), "ma_ut_create failed to create the instance");
}

TEST_TEAR_DOWN(scheduler_tests) {
  TEST_ASSERT_MESSAGE(MA_OK == ma_ut_release(), "ma_ut_release failed to release");
}

TEST(scheduler_tests, ma_scheduler_create) {
    ma_scheduler_t *scheduler = NULL;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_scheduler_create_invalid) {
    ma_scheduler_t *scheduler = NULL;

    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_scheduler_create(NULL, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_scheduler_create(g_msgbus, NULL, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_scheduler_create(g_msgbus, g_ds, NULL, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_scheduler_create(NULL, NULL, NULL, NULL));
}

TEST(scheduler_tests, ma_scheduler_start) {
    ma_scheduler_t *scheduler = NULL;
    ma_trigger_t *trigger = NULL;
    ma_task_t *task = NULL;
    ma_task_time_t now = {0};
    ma_trigger_monthly_dow_policy_t run_monthly_dow_policy = {0};

    run_monthly_dow_policy.day_of_the_week = FRIDAY;
    run_monthly_dow_policy.months = JULY;
    run_monthly_dow_policy.which_week = FIRST_WEEK;

    get_localtime(&now);
    now.minute += 1;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "Test Task", "1001", __FUNCTION__, __FUNCTION__, &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_monthly_dow(&run_monthly_dow_policy, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger, &now));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_start(scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_stop(scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_scheduler_start_invalid) {
    ma_scheduler_t *scheduler = NULL;
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_scheduler_start(NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_scheduler_start(scheduler));
}

TEST(scheduler_tests, ma_scheduler_get_state_tst) {

    char *task_id = NULL;
    ma_scheduler_t *scheduler = NULL;
    ma_trigger_t *trigger = NULL;
    ma_task_t *task = NULL;
    ma_task_time_t now = {0};
    ma_trigger_monthly_dow_policy_t run_monthly_dow_policy;
    ma_scheduler_state_t scheduler_state = MA_SCHEDULER_STATE_NOTSTARTED;
    size_t i = 5;
	char *task_id1= NULL;
    ma_task_t **task_set = 0;
    size_t no_of_tasks = 0;
    char buff[100] = {0};
    time_t tim;
	
    run_monthly_dow_policy.day_of_the_week = FRIDAY;
    run_monthly_dow_policy.months = MARCH;
    run_monthly_dow_policy.which_week = FIRST_WEEK;

    get_localtime(&now);
    now.minute += 1;

	task_id1 = (char *)calloc(1,20);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, "scheduler", &scheduler));
    while(i--) {
		sprintf(task_id1,"Test_test%d",i);
        TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "Test Task", task_id1, __FUNCTION__, __FUNCTION__, &task));
        //ma_task_get_id(task, (const char **)&task_id);
        TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_monthly_dow(&run_monthly_dow_policy, &trigger));
        TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger, &now));
        TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger));
        TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));
    }
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_get_state(scheduler, &scheduler_state));
    TEST_ASSERT_EQUAL_INT(MA_SCHEDULER_STATE_NOTSTARTED, scheduler_state);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_start(scheduler));

    tim = time(0);
    strftime (buff, 100, "%Y-%m-%d %H:%M:%S.000", localtime (&tim));
    printf ("begin ma_scheduler_release = %s\n", buff);
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_stop(scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));

    tim = time(0);
	scheduler = NULL;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, "scheduler", &scheduler));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_start(scheduler));
    tim = time(0);

    ma_scheduler_get_task_set(scheduler, &task_set, &no_of_tasks);
    while(no_of_tasks) {
        ma_task_get_id(task_set[--no_of_tasks], (const char **)&task_id);
    }
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_stop(scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release_task_set(task_set));
	free(task_id1);
}

TEST(scheduler_tests, ma_scheduler_get_state_invalid) {
    ma_scheduler_t *scheduler = NULL;
    ma_scheduler_state_t scheduler_state = MA_SCHEDULER_STATE_NOTSTARTED;

    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_scheduler_get_state(NULL, &scheduler_state));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_scheduler_get_state(scheduler, NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_scheduler_get_state(NULL, NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_scheduler_get_state(scheduler, &scheduler_state));
}

TEST(scheduler_tests, ma_scheduler_stop) {
    ma_scheduler_t *scheduler = NULL;
    ma_task_t *task = NULL;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "Test Task", "1001", __FUNCTION__, __FUNCTION__, &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_start(scheduler));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_stop(scheduler));
//    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_start(scheduler));
  //  TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_stop(scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_scheduler_stop_invalid) {
    ma_scheduler_t *scheduler = NULL;

    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_scheduler_stop(NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_scheduler_stop(scheduler));
}

TEST(scheduler_tests, ma_scheduler_release) {
    ma_scheduler_t *scheduler = NULL;
    ma_trigger_t *trigger = NULL;
    ma_task_t *task = NULL;
    ma_trigger_run_once_policy_t policy = {0};
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "Test Task", "1001", __FUNCTION__, __FUNCTION__, &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));
	//TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_start(scheduler));
	//TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_stop(scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}


TEST(scheduler_tests, ma_scheduler_release1) {
    ma_scheduler_t *scheduler = NULL;
    ma_trigger_t *trigger = NULL;
    ma_task_t *task = NULL;
	ma_task_time_t now;
    ma_trigger_run_once_policy_t policy = {0};
	
	get_localtime(&now);
    now.minute += 1;

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "Test Task", "1001", __FUNCTION__, __FUNCTION__, &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger));
	ma_trigger_set_begin_date(trigger,&now);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_start(scheduler));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_stop(scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_scheduler_release2) {
    ma_scheduler_t *scheduler = NULL;
    ma_task_t *task = NULL;
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "Test Task", "1001", __FUNCTION__, __FUNCTION__, &task));
   	TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_start(scheduler));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_stop(scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}
TEST(scheduler_tests, ma_scheduler_release_invalid) {
    ma_scheduler_t *scheduler = NULL;

    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_scheduler_release(NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_scheduler_get_runnning_task_set) {
    ma_scheduler_t *scheduler = NULL;
    ma_trigger_t *trigger = NULL;
    ma_task_t *task = NULL;
    ma_trigger_run_once_policy_t runonce_policy = {0};
    size_t no_of_tasks = 0;
    ma_task_time_t now = {0};
    ma_task_t **running_task_set_1 = NULL;
    ma_task_t **task_set = NULL;

    get_localtime(&now);
    now.minute += 1;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "Test Task", "1001", __FUNCTION__, __FUNCTION__, &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&runonce_policy, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger, &now));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time_limit(task, 1));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_get_runnning_task_set(scheduler, &running_task_set_1, &no_of_tasks));
    TEST_ASSERT_EQUAL_INT(0, no_of_tasks);

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_start(scheduler));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_get_runnning_task_set(scheduler, &running_task_set_1, &no_of_tasks));
    TEST_ASSERT_EQUAL_INT(0, no_of_tasks);

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_get_task_set(scheduler, &task_set, &no_of_tasks));
    TEST_ASSERT_EQUAL_INT(1, no_of_tasks);

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_stop(scheduler));
    //TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release_runnning_task_set(running_task_set_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release_task_set(task_set));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_scheduler_get_runnning_task_set_invalid) {
    ma_scheduler_t *scheduler = NULL;
    ma_task_t **task_set = NULL;
    size_t no_of_tasks = 0;

    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_scheduler_get_runnning_task_set(NULL, &task_set, &no_of_tasks));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_scheduler_get_runnning_task_set(scheduler, NULL, &no_of_tasks));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_scheduler_get_runnning_task_set(scheduler, &task_set, NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_scheduler_get_runnning_task_set(NULL, NULL, NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_scheduler_get_runnning_task_set(scheduler, &task_set, &no_of_tasks));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_get_runnning_task_set(scheduler, &task_set, &no_of_tasks));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_scheduler_release_runnning_task_set(task_set));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_scheduler_get_task_set_invalid) {
    ma_scheduler_t *scheduler = NULL;
    size_t no_of_tasks = 0;
    ma_task_t **task_set = NULL;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_scheduler_get_task_set(NULL, &task_set, &no_of_tasks));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_scheduler_get_task_set(scheduler, NULL, &no_of_tasks));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_scheduler_get_task_set(scheduler, &task_set, NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_scheduler_get_task_set(NULL, NULL, NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_scheduler_release_runnning_task_set_invalid) {
    ma_task_t **task_set = NULL;

    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_scheduler_release_runnning_task_set(NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_scheduler_release_runnning_task_set(task_set));
}

TEST(scheduler_tests, ma_scheduler_release_task_set_invalid) {
    ma_task_t **task_set = NULL;

    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_scheduler_release_task_set(NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_scheduler_release_task_set(task_set));
}

TEST(scheduler_tests, ma_scheduler_create_task_invalid) {
    ma_scheduler_t *scheduler = NULL;
    ma_task_t *task = NULL;

    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_scheduler_create_task(NULL, "Test Task", "1001", __FUNCTION__, __FUNCTION__, &task));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_scheduler_create_task(scheduler, NULL, "1001", __FUNCTION__, __FUNCTION__, &task));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_scheduler_create_task(scheduler, "Test Task", "1001", __FUNCTION__, __FUNCTION__, NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_scheduler_create_task(NULL, NULL, NULL, NULL, NULL, NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "Test Task", "1001", __FUNCTION__, __FUNCTION__, &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_scheduler_get_task_handle) {
    ma_scheduler_t *scheduler = NULL;
    ma_trigger_t *trigger = NULL;
    ma_task_t *task = NULL;
    ma_trigger_run_once_policy_t runonce_policy = {0};
    ma_task_time_t now = {0};
    ma_task_t *task_handle = NULL;
    const char *task_id = NULL;

    get_localtime(&now);
    now.minute += 1;
    runonce_policy.once = MA_TRUE;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "Test Task", "1001", __FUNCTION__, __FUNCTION__, &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&runonce_policy, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger, &now));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id(task, &task_id));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_get_task_handle(scheduler, task_id, &task_handle));
    TEST_ASSERT_EQUAL_PTR(task, task_handle);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_start(scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_stop(scheduler));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release_task(scheduler, task));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release_task(scheduler, task_handle));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_scheduler_get_task_handle_invalid) {
    ma_scheduler_t *scheduler = NULL;
    ma_task_t *task = NULL;
    ma_task_t *task_ret = NULL;
    const char *task_id = "Test Task";

    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_scheduler_get_task_handle(scheduler, task_id, &task));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_scheduler_get_task_handle(scheduler, NULL, &task));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_scheduler_get_task_handle(scheduler, task_id, NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_scheduler_get_task_handle(NULL, NULL, NULL));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "Test Task", "1001", __FUNCTION__, __FUNCTION__, &task));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_SCHEDULER_INVALID_TASK_ID, ma_scheduler_get_task_handle(scheduler, "99998888", &task_ret));
    TEST_ASSERT_EQUAL_PTR(NULL, task_ret);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_scheduler_release_task_invalid) {
    ma_scheduler_t *scheduler = NULL;
    ma_task_t *task_set = NULL;

    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_scheduler_release_task(NULL,task_set));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_scheduler_release_task(NULL,NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_scheduler_release_task(scheduler,NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_running_task_collector_create_release_valid_invalid) {
    ma_running_task_collector_t *collector = NULL;
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_running_task_collector_create(NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_running_task_collector_create(&collector));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_running_task_collector_release(NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_running_task_collector_release(collector));
}

TEST(scheduler_tests, ma_running_task_collector_add_valid_invalid) {
    ma_scheduler_t *scheduler = NULL;
    ma_running_task_collector_t *collector = NULL;
    ma_trigger_t *trigger = NULL;
    ma_task_t *task = NULL;
    ma_trigger_run_once_policy_t runonce_policy = {0};
    runonce_policy.once = MA_TRUE;

    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_running_task_collector_add(collector, task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_running_task_collector_create(&collector));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_running_task_collector_add(collector, task));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_running_task_collector_add(NULL, task));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "Test Task", "1001", __FUNCTION__, __FUNCTION__, &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&runonce_policy, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_trigger_release(trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_running_task_collector_add(collector, task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_running_task_collector_release(collector));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_running_task_collector_find_valid_invalid) {
    ma_scheduler_t *scheduler = NULL;
    ma_running_task_collector_t *collector = NULL;
    ma_trigger_t *trigger_1 = NULL;
    ma_trigger_t *trigger_2 = NULL;
    ma_task_t *valid_task = NULL;
    ma_task_t *invalid_task = NULL;
    ma_trigger_run_once_policy_t runonce_policy = {0};
    ma_bool_t found = MA_FALSE;
    runonce_policy.once = MA_TRUE;

    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_running_task_collector_find(collector, invalid_task, NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_running_task_collector_find(collector, invalid_task, &found));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_running_task_collector_create(&collector));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_running_task_collector_find(collector, invalid_task, &found));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "Valid Test Task", "1001", __FUNCTION__, __FUNCTION__, &valid_task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "Invalid Test Task", "1002", __FUNCTION__, __FUNCTION__, &invalid_task));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_running_task_collector_find(NULL, invalid_task, &found));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&runonce_policy, &trigger_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&runonce_policy, &trigger_2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(valid_task, trigger_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(invalid_task, trigger_2));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_trigger_release(trigger_1));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_trigger_release(trigger_2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_running_task_collector_add(collector, valid_task));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_running_task_collector_find(collector, valid_task, &found));
    TEST_ASSERT_EQUAL_INT(MA_TRUE, found);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_running_task_collector_find(collector, invalid_task, &found));
    TEST_ASSERT_EQUAL_INT(MA_FALSE, found);

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_running_task_collector_release(collector));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_running_task_collector_remove_size_valid_invalid) {
    ma_scheduler_t *scheduler = NULL;
    ma_running_task_collector_t *collector = NULL;
    ma_trigger_t *trigger_1 = NULL;
    ma_trigger_t *trigger_2 = NULL;
    ma_task_t *task_1 = NULL;
    ma_task_t *task_2 = NULL;
    ma_trigger_run_once_policy_t runonce_policy = {0};
    ma_bool_t found = MA_FALSE;
    size_t task_count = 0;
    runonce_policy.once = MA_TRUE;

    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_running_task_collector_remove(collector, task_1));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_running_task_collector_size(collector, NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_running_task_collector_size(collector, &task_count));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "Valid Test Task", "1001", __FUNCTION__, __FUNCTION__, &task_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "Invalid Test Task", "1002", __FUNCTION__, __FUNCTION__, &task_2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&runonce_policy, &trigger_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&runonce_policy, &trigger_2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_1, trigger_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_2, trigger_2));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_trigger_release(trigger_1));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_trigger_release(trigger_2));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_running_task_collector_remove(collector, task_2));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_running_task_collector_create(&collector));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_running_task_collector_size(collector, NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_running_task_collector_remove(collector, NULL));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_running_task_collector_add(collector, task_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_running_task_collector_size(collector, &task_count));
    TEST_ASSERT_EQUAL_INT(1, task_count);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_running_task_collector_find(collector, task_1, &found));
    TEST_ASSERT_EQUAL_INT(MA_TRUE, found);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_running_task_collector_find(collector, task_2, &found));
    TEST_ASSERT_EQUAL_INT(MA_FALSE, found);

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_running_task_collector_add(collector, task_2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_running_task_collector_size(collector, &task_count));
    TEST_ASSERT_EQUAL_INT(2, task_count);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_running_task_collector_find(collector, task_1, &found));
    TEST_ASSERT_EQUAL_INT(MA_TRUE, found);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_running_task_collector_find(collector, task_2, &found));
    TEST_ASSERT_EQUAL_INT(MA_TRUE, found);

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_running_task_collector_remove(collector, task_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_running_task_collector_find(collector, task_1, &found));
    TEST_ASSERT_EQUAL_INT(MA_FALSE, found);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_running_task_collector_find(collector, task_2, &found));
    TEST_ASSERT_EQUAL_INT(MA_TRUE, found);

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_running_task_collector_remove(collector, task_2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_running_task_collector_find(collector, task_1, &found));
    TEST_ASSERT_EQUAL_INT(MA_FALSE, found);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_running_task_collector_find(collector, task_2, &found));
    TEST_ASSERT_EQUAL_INT(MA_FALSE, found);

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_running_task_collector_release(collector));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_running_task_collector_start_stop_valid_invalid) {
    ma_running_task_collector_t *collector = NULL;
    const char *task_id = NULL;
    ma_scheduler_t *scheduler = NULL;
    ma_task_t *task = NULL;

    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_running_task_collector_stop(collector, task_id));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "Test Task", "1001", __FUNCTION__, __FUNCTION__, &task));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_running_task_collector_create(&collector));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_running_task_collector_add(collector, task));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_running_task_collector_stop(collector, task_id));

    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_running_task_collector_stop(NULL, task_id));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id(task, &task_id));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_running_task_collector_stop(collector, task_id));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_running_task_collector_release(collector));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, convert_sec_to_hr_and_min_get_loctm_valid_invalid) {
    ma_uint16_t hour = 0;
    ma_uint16_t minutes = 0;
    ma_task_time_t time = {0};
    get_localtime(NULL);

    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, convert_seconds_to_hour_and_minutes(0, NULL, NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, convert_seconds_to_hour_and_minutes(60, NULL, &minutes));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, convert_seconds_to_hour_and_minutes(60, &hour, NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, convert_seconds_to_hour_and_minutes(86460, &hour, &minutes));
    TEST_ASSERT_EQUAL_INT(hour, 24);
    TEST_ASSERT_EQUAL_INT(minutes, 1);
    get_localtime(&time);
}

TEST(scheduler_tests, get_next_date_compute_timeout_valid_invalid) {
    ma_task_time_t next_day = {0};
    ma_int64_t timeout = 0;
    time_t now = {0};
    struct tm task_time = {0};
    double seconds = 0.0;

    time(&now);
    get_next_date(NULL);
    get_next_date(&next_day);
    compute_timeout(NULL, NULL);
    compute_timeout(&next_day, NULL);
    compute_timeout(NULL, &timeout);
    task_time.tm_hour = next_day.hour;
    task_time.tm_year = next_day.year-1900;
    task_time.tm_mon = next_day.month-1;
    task_time.tm_mday = next_day.day;
    task_time.tm_min = next_day.minute;
    task_time.tm_sec = next_day.secs;
    seconds = difftime(mktime(&task_time), now);
    compute_timeout(&next_day, &timeout);
}

TEST(scheduler_tests, get_week_day_valid_invalid) {
    ma_task_time_t tdate = {0};
    ma_day_of_week_t week_day = SUNDAY;

    get_week_day(NULL, &week_day);
    get_week_day(&tdate, NULL);
    get_week_day(NULL, NULL);
    tdate.hour = 11;
    tdate.minute = 45;
    tdate.secs = 30;
    tdate.day = 15;
    tdate.month = 5;
    tdate.year = 2013;
    get_week_day(&tdate, &week_day);
    TEST_ASSERT_EQUAL_INT(week_day, WEDNESDAY);
}

TEST(scheduler_tests, get_next_week_date_after_valid_invalid) {
    ma_task_time_t start_date = {0};
    ma_uint32_t days_of_week = 0;
    ma_uint16_t weeks_interval = 0;
    ma_task_time_t new_date = {0};

    get_next_week_date_after(start_date, days_of_week, weeks_interval, NULL);
    start_date.hour = 11;
    start_date.minute = 45;
    start_date.secs = 30;
    start_date.day = 1;
    start_date.month = 5;
    start_date.year = 2013;
    days_of_week = THURSDAY;
    weeks_interval = 3;
    get_next_week_date_after(start_date, days_of_week, weeks_interval, &new_date);
    TEST_ASSERT_EQUAL_INT(16, new_date.day);
    TEST_ASSERT_EQUAL_INT(5, new_date.month);
    TEST_ASSERT_EQUAL_INT(2013, new_date.year);
}

TEST(scheduler_tests, get_next_date_after_valid_invalid) {
    ma_task_time_t start_date = {0};
    ma_task_time_t new_date = {0};
    int no_of_days = 0;
    get_next_date_after(start_date, NULL, no_of_days);
    start_date.hour = 10;
    start_date.minute = 30;
    start_date.secs = 15;
    start_date.day = 29;
    start_date.month = 5;
    start_date.year = 2013;
    no_of_days = 4;
    get_next_date_after(start_date, &new_date, no_of_days);
    TEST_ASSERT_EQUAL_INT(2, new_date.day);
    TEST_ASSERT_EQUAL_INT(6, new_date.month);
    TEST_ASSERT_EQUAL_INT(2013, new_date.year);
}

TEST(scheduler_tests, is_today_last_day_valid_invalid) {
    ma_task_time_t end_date = {0};
    ma_task_time_t now = {0};

    end_date.hour = 10;
    end_date.minute = 30;
    end_date.secs = 15;
    end_date.day = 29;
    end_date.month = 5;
    end_date.year = 2020;
    get_localtime(&now);
    TEST_ASSERT_EQUAL_INT(MA_FALSE, check_today_last_day(now, end_date));
    get_next_date(&end_date);
    TEST_ASSERT_EQUAL_INT(MA_FALSE, check_today_last_day(now, end_date));
    get_localtime(&end_date);
    TEST_ASSERT_EQUAL_INT(MA_TRUE, check_today_last_day(end_date, end_date));
}

TEST(scheduler_tests, is_last_day_later_valid_invalid) {
    ma_task_time_t end_date = {0};
    ma_task_time_t next_date = {0};
    TEST_ASSERT_EQUAL_INT(MA_TRUE, is_last_day_later(end_date, next_date));
    end_date.hour = 10;
    end_date.minute = 30;
    end_date.secs = 15;
    end_date.day = 29;
    end_date.month = 5;
    end_date.year = 2015;

    next_date.hour = 10;
    next_date.minute = 30;
    next_date.secs = 20;
    next_date.day = 30;
    next_date.month = 4;
    next_date.year = 2014;
    TEST_ASSERT_EQUAL_INT(MA_TRUE, is_last_day_later(end_date, next_date));
    end_date.hour = 10;
    end_date.minute = 30;
    end_date.secs = 15;
    end_date.day = 29;
    end_date.month = 4;
    end_date.year = 2014;

    next_date.hour = 10;
    next_date.minute = 30;
    next_date.secs = 20;
    next_date.day = 30;
    next_date.month = 4;
    next_date.year = 2014;
    TEST_ASSERT_EQUAL_INT(MA_FALSE, is_last_day_later(end_date, next_date));
}

TEST(scheduler_tests, get_next_month_date_after_valid_invalid) {
    ma_task_time_t start_date = {0};
    ma_uint32_t months = 0;
    ma_uint16_t day = 0;
    ma_task_time_t new_date = {0};

    get_next_month_date_after(start_date, months, day, NULL);
    start_date.hour = 10;
    start_date.minute = 30;
    start_date.secs = 20;
    start_date.day = 30;
    start_date.month = 4;
    start_date.year = 2012;
    months = JUNE;
    day = 7;
    get_next_month_date_after(start_date, months, day, &new_date);
    TEST_ASSERT_EQUAL_INT(7, new_date.day);
    TEST_ASSERT_EQUAL_INT(6, new_date.month);
    TEST_ASSERT_EQUAL_INT(2012, new_date.year);

    start_date.hour = 10;
    start_date.minute = 30;
    start_date.secs = 20;
    start_date.day = 30;
    start_date.month = 7;
    start_date.year = 2012;
    months = JUNE;
    day = 7;
    get_next_month_date_after(start_date, months, day, &new_date);
    TEST_ASSERT_EQUAL_INT(7, new_date.day);
    TEST_ASSERT_EQUAL_INT(6, new_date.month);
    TEST_ASSERT_EQUAL_INT(2013, new_date.year);
}

TEST(scheduler_tests, get_next_month_dow_date_after_valid_invalid) {
    ma_task_time_t start_date = {0};
    ma_uint32_t months = 0;
    ma_uint16_t which_week = FIRST_WEEK;
    ma_uint32_t days_of_week = WEDNESDAY;
    ma_task_time_t new_date = {0};
    ma_task_time_t new_date_nxt_yr = {0};
    get_next_month_dow_date_after(start_date, months, which_week, days_of_week, NULL);

    start_date.hour = 10;
    start_date.minute = 30;
    start_date.secs = 20;
    start_date.day = 30;
    start_date.month = 7;
    start_date.year = 2012;
    months = SEPTEMBER;
    which_week = SECOND_WEEK;
    days_of_week = THURSDAY;
    get_next_month_dow_date_after(start_date, months, which_week, days_of_week, &new_date);
    TEST_ASSERT_EQUAL_INT(13, new_date.day);
    TEST_ASSERT_EQUAL_INT(9, new_date.month);
    TEST_ASSERT_EQUAL_INT(2012, new_date.year);

    start_date.month = 10;
    get_next_month_dow_date_after(start_date, months, which_week, days_of_week, &new_date_nxt_yr);
    TEST_ASSERT_EQUAL_INT(12, new_date_nxt_yr.day);
    TEST_ASSERT_EQUAL_INT(9, new_date_nxt_yr.month);
    TEST_ASSERT_EQUAL_INT(2013, new_date_nxt_yr.year);
}

TEST(scheduler_tests, ma_trigger_create_run_once_valid_invalid) {
    ma_trigger_run_once_policy_t policy = {0};
    ma_trigger_t *trigger = NULL;
    ma_trigger_type_t type = MA_TRIGGER_ONCE;
    policy.once = MA_TRUE;
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_create_run_once(NULL, NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_create_run_once(NULL, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));
    policy.once = MA_FALSE;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_TRIGGER_ONCE, type);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));

}

TEST(scheduler_tests, ma_trigger_create_daily_valid_invalid) {
    ma_trigger_daily_policy_t policy = {0};
    ma_trigger_t *trigger = NULL;
    ma_trigger_type_t type = MA_TRIGGER_DAILY;
    policy.days_interval = -2;
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_create_daily(NULL, NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_create_daily(NULL, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&policy, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_TRIGGER_DAILY, type);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));
}

TEST(scheduler_tests, ma_trigger_create_weekly_valid_invalid) {
    ma_trigger_weekly_policy_t policy = {0};
    ma_trigger_t *trigger = NULL;
    ma_trigger_type_t type = MA_TRIGGER_WEEKLY;
    policy.days_of_the_week = WEDNESDAY;
    policy.weeks_interval = 3;
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_create_weekly(NULL, NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_create_weekly(NULL, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_weekly(&policy, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_TRIGGER_WEEKLY, type);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));
}

TEST(scheduler_tests, ma_trigger_create_monthly_date_valid_invalid) {
    ma_trigger_monthly_date_policy_t policy = {0};
    ma_trigger_t *trigger = NULL;
    ma_trigger_type_t type = MA_TRIGGER_MONTHLY_DATE;
    policy.date = 29;
    policy.months = FEBRUARY;
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_create_monthly_date(NULL, NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_create_monthly_date(NULL, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_monthly_date(&policy, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_TRIGGER_MONTHLY_DATE, type);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));
}

TEST(scheduler_tests, ma_trigger_create_monthly_dow) {
    ma_trigger_monthly_dow_policy_t policy = {0};
    ma_trigger_t *trigger = NULL;
    ma_trigger_type_t type = MA_TRIGGER_MONTHLY_DOW;
    policy.day_of_the_week = FRIDAY;
    policy.months = DECEMBER;
    policy.which_week = SECOND_WEEK;
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_create_monthly_dow(NULL, NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_create_monthly_dow(NULL, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_monthly_dow(&policy, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_TRIGGER_MONTHLY_DOW, type);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));
}


TEST(scheduler_tests, ma_trigger_duration_valid_invalid) {
    ma_trigger_run_once_policy_t policy = {0};
    ma_trigger_t *trigger = NULL;
    ma_uint32_t duration = 0;
    policy.once = MA_TRUE;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_set_duration(NULL, 20));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_duration(trigger, 10));

    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_get_duration(NULL, &duration));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_get_duration(NULL, NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_get_duration(trigger, NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_get_duration(trigger, &duration));
    TEST_ASSERT_EQUAL_INT(10, duration);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));
}

TEST(scheduler_tests, ma_trigger_interval_valid_invalid) {
    ma_trigger_run_once_policy_t policy = {0};
    ma_trigger_t *trigger = NULL;
    ma_uint32_t interval = 0;
    policy.once = MA_TRUE;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_set_interval(NULL, 10));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_interval(trigger, 20));

    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_get_interval(NULL, &interval));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_get_interval(NULL, NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_get_interval(trigger, NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_get_interval(trigger, &interval));
    TEST_ASSERT_EQUAL_INT(20, interval);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));
}

TEST(scheduler_tests, ma_trigger_random_interval_valid_invalid) {
    ma_trigger_run_once_policy_t policy = {0};
    ma_trigger_t *trigger = NULL;
    ma_uint16_t random_interval = 0;
    policy.once = MA_TRUE;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_set_random_interval(NULL, 10));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_random_interval(trigger, 5));

    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_get_random_interval(NULL, &random_interval));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_get_random_interval(NULL, NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_get_random_interval(trigger, NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_get_random_interval(trigger, &random_interval));
    TEST_ASSERT_EQUAL_INT(5, random_interval);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));
}

TEST(scheduler_tests, ma_trigger_disable_valid_invalid) {
    ma_trigger_run_once_policy_t policy = {0};
    ma_trigger_t *trigger = NULL;
    ma_trigger_state_t state;
    policy.once = MA_TRUE;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger));

    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_set_disable(NULL, MA_FALSE));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_get_state(trigger, NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_get_state(NULL, &state));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_get_state(NULL, NULL));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_disable(trigger, MA_TRUE));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_get_state(trigger, &state));
    TEST_ASSERT_EQUAL_INT(MA_TRIGGER_STATE_BLOCKED, state);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_disable(trigger, MA_FALSE));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_get_state(trigger, &state));
    TEST_ASSERT_EQUAL_INT(MA_TRIGGER_STATE_NOT_STARTED, state);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));
}

TEST(scheduler_tests, ma_trigger_loop_valid_invalid) {
    ma_trigger_run_once_policy_t policy = {0};
    ma_trigger_t *trigger = NULL;
    uv_loop_t *loop = uv_default_loop();
    uv_loop_t *get_loop = NULL;
    policy.once = MA_TRUE;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_set_loop(trigger, NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_set_loop(NULL, loop));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_set_loop(NULL, NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_loop(trigger, loop));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_get_loop(trigger, NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_get_loop(NULL, &get_loop));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_get_loop(NULL, NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_get_loop(trigger, &get_loop));
    TEST_ASSERT_EQUAL_PTR(loop, get_loop);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));
}

///Start
TEST(scheduler_tests, triggerlist_create_release_invalid) {
   // TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_list_create(NULL));
   // TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_list_release(NULL));
}

TEST(scheduler_tests, ma_trigger_list_find_invalid) {

    ma_trigger_list_t *tl = NULL;
    ma_trigger_t *ret_trigger = NULL;
    ma_trigger_t *trigger1 = NULL;
    ma_trigger_run_once_policy_t runonce = {0};
    const char *trigger2_id = NULL;
	ma_task_t *task = NULL;
	const char *task_id ="Test-task";

   	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_create(task_id,&task));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_trigger_list(task,&tl));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&runonce, &trigger1));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_list_find(tl, NULL, &ret_trigger));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_list_find(NULL, trigger2_id, &ret_trigger));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_list_find(tl, trigger2_id, NULL));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger1));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_release(task));
}

TEST(scheduler_tests, triggerlist_add_invalid) {
    ma_trigger_list_t *tl = NULL;
    ma_trigger_t *trigger1 = NULL;
    ma_trigger_t *trigger2 = NULL;
    ma_trigger_t *trigger3 = NULL;
    ma_trigger_run_once_policy_t runonce = {0};
    const char *trigger2_id = NULL;
	ma_task_t *task = NULL;
	const char *task_id ="Test-task";

   	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_create(task_id,&task));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_trigger_list(task,&tl));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&runonce, &trigger1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&runonce, &trigger2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&runonce, &trigger3));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_list_add(tl, trigger1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_list_add(tl, trigger1)); //Ned to check behavior when same trigger added in list.currently it returns MA_OK
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_get_id(trigger2, &trigger2_id));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_list_add(NULL, trigger1));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_list_add(tl, NULL));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger2));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger3));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_release(task));
}

TEST(scheduler_tests, triggerlist_add_remove) {
    ma_trigger_list_t *tl = NULL;
    ma_trigger_t *trigger1 = NULL;
    ma_trigger_t *trigger2 = NULL;
    ma_trigger_t *trigger3 = NULL;
    ma_trigger_t *ret_trigger = NULL;
    ma_trigger_run_once_policy_t runonce = {0};
    const char *trigger1_id = NULL;
    const char *trigger3_id = NULL;
	ma_task_t *task = NULL;
	const char *task_id ="Test-task";

   	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_create(task_id,&task));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_trigger_list(task,&tl));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&runonce, &trigger1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&runonce, &trigger2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&runonce, &trigger3));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_list_add(tl, trigger1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_list_add(tl, trigger2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_list_add(tl, trigger3));
	ma_trigger_release(trigger1);
	ma_trigger_release(trigger2);
	ma_trigger_release(trigger3);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_get_id(trigger1, &trigger1_id));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_list_find(tl, trigger1_id, &ret_trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_get_id(trigger3, &trigger3_id));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_list_remove(tl, trigger3_id));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_release(task));
}

TEST(scheduler_tests, triggerlist_add_remove_invalid) {
    ma_trigger_list_t *tl = NULL;
    ma_trigger_t *trigger1 = NULL;
    ma_trigger_t *trigger2 = NULL;
    ma_trigger_t *trigger3 = NULL;
    ma_trigger_t *ret_trigger = NULL;
    ma_trigger_run_once_policy_t runonce = {0};
    const char *trigger3_id = NULL;
	ma_task_t *task = NULL;
	const char *task_id ="Test-task";

   	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_create(task_id,&task));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_trigger_list(task,&tl));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&runonce, &trigger1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&runonce, &trigger2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&runonce, &trigger3));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_list_add(tl, trigger1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_list_add(tl, trigger2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_list_add(tl, trigger3));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_get_id(trigger3, &trigger3_id));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_list_remove(tl, trigger3_id));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_SCHEDULER_INVALID_TRIGGER_ID, ma_trigger_list_find(tl, trigger3_id, &ret_trigger));
    //TEST_ASSERT_EQUAL_PTR(trigger3,ret_trigger);//Curently this is kept as expected because remove is not remove the trigger it just
    //stops it hence still find will return the removed triger.

    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_list_remove(tl, NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_list_remove(NULL,trigger3_id));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger2));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger3));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_release(task));
}


TEST(scheduler_tests, ma_trigger_list_find) {
    ma_trigger_list_t *tl = NULL;
    ma_trigger_t *trigger1 = NULL;
    ma_trigger_t *trigger2 = NULL;
    ma_trigger_t *trigger3 = NULL;
    ma_trigger_t *ret_trigger = NULL;
    ma_trigger_run_once_policy_t runonce = {0};
    const char *trigger1_id = NULL;
    const char *trigger2_id = NULL;
	ma_task_t *task = NULL;
	const char *task_id ="Test-task";

   	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_create(task_id,&task));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_trigger_list(task,&tl));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&runonce, &trigger1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&runonce, &trigger2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&runonce, &trigger3));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_list_add(tl, trigger1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_list_add(tl, trigger2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_list_add(tl, trigger3));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_get_id(trigger1, &trigger1_id));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_list_find(tl, trigger1_id, &ret_trigger));
    TEST_ASSERT_EQUAL_PTR(trigger1,ret_trigger);
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(ret_trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_get_id(trigger2, &trigger2_id));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_list_find(tl, trigger2_id, &ret_trigger));
    TEST_ASSERT_EQUAL_PTR(trigger2,ret_trigger);
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(ret_trigger));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger2));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger3));
	
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_release(task));

}


TEST(scheduler_tests, ma_scheduler_create_task_release_invalid) {
    ma_scheduler_t *scheduler = NULL;
    ma_task_t *task = NULL;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_scheduler_create_task(scheduler, NULL,"100", __FUNCTION__, __FUNCTION__, &task));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_scheduler_create_task(scheduler, "Test","100", __FUNCTION__, __FUNCTION__, NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_scheduler_create_task(NULL, "Test", "100", __FUNCTION__, __FUNCTION__,&task));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_scheduler_create_task(NULL, NULL,NULL, NULL, NULL, NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_scheduler_create_task(scheduler, "Test", NULL, __FUNCTION__, __FUNCTION__, &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_scheduler_release(NULL));
}

TEST(scheduler_tests, ma_task_set_retry_policy) {
    ma_scheduler_t *scheduler = NULL;
    ma_trigger_t *trigger = NULL;
    ma_task_t *task = NULL;
    ma_trigger_run_once_policy_t policy = {0};
    ma_task_retry_policy_t retry_policy ={0};
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__, &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_trigger_release(trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_retry_policy(task,retry_policy));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_task_set_retry_policy_invalid) {
    ma_scheduler_t *scheduler = NULL;
    ma_trigger_t *trigger = NULL;
    ma_task_t *task = NULL;
    ma_trigger_run_once_policy_t policy = {0};
    ma_task_retry_policy_t retry_policy ={0};
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__,  &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_trigger_release(trigger));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_task_set_retry_policy(NULL,retry_policy));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_task_set_repeat_policy) {
    ma_scheduler_t *scheduler = NULL;
    ma_trigger_t *trigger = NULL;
    ma_task_t *task = NULL;
    ma_trigger_run_once_policy_t policy = {0};
    ma_task_repeat_policy_t policy_repeat;
    policy_repeat.repeat_interval.hour = 2;
    policy_repeat.repeat_interval.minute = 10;
    policy_repeat.repeat_until.hour = 10;
    policy_repeat.repeat_until.minute = 50;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__,  &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_repeat_policy(task,policy_repeat));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_task_set_repeat_policy_invalid) {
    ma_scheduler_t *scheduler = NULL;
    ma_trigger_t *trigger = NULL;
    ma_task_t *task = NULL;
    ma_task_t *task1 = NULL;
    ma_trigger_run_once_policy_t policy = {0};
    ma_task_repeat_policy_t policy_repeat;
    policy_repeat.repeat_interval.hour = 0;
    policy_repeat.repeat_interval.minute = 0;
    policy_repeat.repeat_until.hour = 0;
    policy_repeat.repeat_until.minute = 0;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__,  &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_task_set_repeat_policy(task1,policy_repeat)); //NULL test fro task

    policy_repeat.repeat_interval.hour = 3;
    policy_repeat.repeat_interval.minute = 10;
    policy_repeat.repeat_until.hour = 2;
    policy_repeat.repeat_until.minute = 10;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_repeat_policy(task,policy_repeat)); //repeat interval more than,until data.
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_task_set_max_run_time) {
    ma_scheduler_t *scheduler = NULL;
    ma_trigger_t *trigger = NULL;
    ma_task_t *task = NULL;
    ma_trigger_run_once_policy_t policy = {0};
    ma_uint16_t max_run_time = 10;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__,  &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time(task,max_run_time));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));

}

TEST(scheduler_tests, ma_task_set_max_run_time_invalid) {
    ma_scheduler_t *scheduler = NULL;
    ma_trigger_t *trigger = NULL;
    ma_task_t *task = NULL;
    ma_task_t *task1 = NULL;
    ma_trigger_run_once_policy_t policy = {0};
    ma_uint16_t max_run_time = 10;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__,  &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_task_set_max_run_time(task1,max_run_time)); //NULL test for task
    max_run_time = 0;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time(task,max_run_time)); //zero test for max_run_time will return MA_OK now and will set default max run time.
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));

}

TEST(scheduler_tests, ma_task_set_max_run_time_limit) {
    ma_scheduler_t *scheduler = NULL;
    ma_trigger_t *trigger = NULL;
    ma_task_t *task = NULL;
    ma_trigger_run_once_policy_t policy = {0};
    ma_uint16_t max_run_time = 10;
    ma_uint16_t max_run_time_limit = 100;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__,  &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time(task,max_run_time));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time_limit(task,max_run_time_limit));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));

}

TEST(scheduler_tests, ma_task_set_max_run_time_limit_invalid) {
    ma_scheduler_t *scheduler = NULL;
    ma_trigger_t *trigger = NULL;
    ma_task_t *task = NULL;
    ma_task_t *task1 = NULL;
    ma_trigger_run_once_policy_t policy = {0};
    ma_uint16_t max_run_time = 10;
    ma_uint16_t max_run_time_limit = 100;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__,  &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time(task,max_run_time));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_task_set_max_run_time_limit(task1,max_run_time_limit)); //NULL test for task
    max_run_time_limit = 0;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time_limit(task,max_run_time_limit)); //zero test for max_run_time will return MA_OK now and will set default max run limit.
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));

}

TEST(scheduler_tests, ma_task_set_randomize_policy) {
    ma_scheduler_t *scheduler = NULL;
    ma_trigger_t *trigger = NULL;
    ma_task_t *task = NULL;
    ma_trigger_run_once_policy_t policy = {0};
    ma_task_randomize_policy_t policy_randomize;
    policy_randomize.random_interval.hour = 2;
    policy_randomize.random_interval.minute = 0;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__,  &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_randomize_policy(task,policy_randomize));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_task_set_randomize_policy_invalid) {
    ma_scheduler_t *scheduler = NULL;
    ma_trigger_t *trigger = NULL;
    ma_task_t *task = NULL;
    ma_task_t *task1 = NULL;
    ma_trigger_run_once_policy_t policy = {0};
    ma_task_randomize_policy_t policy_randomize;
    policy_randomize.random_interval.hour = 0;
    policy_randomize.random_interval.minute = 0;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__,  &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_task_set_randomize_policy(task1,policy_randomize)); //NULL test fro task
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_randomize_policy(task,policy_randomize)); //Dumy policy repeater.
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_task_set_priority) {
    ma_scheduler_t *scheduler = NULL;
    ma_task_t *task = NULL;
    ma_task_priority_t priority = MA_TASK_PRIORITY_LOW;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__,  &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_priority(task,priority));
    priority = MA_TASK_PRIORITY_NORMAL;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_priority(task,priority));
    priority = MA_TASK_PRIORITY_HIGH;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_priority(task,priority));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_task_set_priority_invalid) {
    ma_scheduler_t *scheduler = NULL;
    ma_task_t *task = NULL;
    ma_task_t *task1 = NULL;
    ma_task_priority_t priority = MA_TASK_PRIORITY_NORMAL;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__,  &task));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_task_set_priority(task1,priority));//NULL test for task
    priority = 0;
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_task_set_priority(task,priority));// NULL set to priority

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_task_set_loop) {
    ma_scheduler_t *scheduler = NULL;
    ma_task_t *task = NULL;
    uv_loop_t *loop = uv_default_loop();

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__,  &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_loop(task,loop));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_task_set_loop_invalid) {
    ma_scheduler_t *scheduler = NULL;
    ma_task_t *task = NULL;
    ma_task_t *task1 = NULL;
    uv_loop_t *loop = uv_default_loop();
    uv_loop_t *loop1 = NULL;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__,  &task));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_task_set_loop(task1,loop)); // NULL test for task
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_task_set_loop(task,loop1)); // NULL test for loop
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_task_set_conditions) {
    ma_scheduler_t *scheduler = NULL;
    ma_task_t *task = NULL;
    ma_task_cond_t cond = MA_TASK_COND_INTERACTIVE;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__,  &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_conditions(task,cond));
    cond = MA_TASK_COND_DELETE_WHEN_DONE;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_conditions(task,cond));
    cond = MA_TASK_COND_DISABLED;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_conditions(task,cond));
    cond = MA_TASK_COND_START_ONLY_IF_IDLE;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_conditions(task,cond));
    cond = MA_TASK_COND_KILL_ON_IDLE_END;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_conditions(task,cond));
    cond = MA_TASK_COND_DONT_START_IF_ON_BATTERIES;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_conditions(task,cond));
    cond = MA_TASK_COND_STOP_IF_GOING_ON_BATTERIES;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_conditions(task,cond));
    cond = MA_TASK_COND_RUN_ONLY_IF_DOCKED;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_conditions(task,cond));
    cond = MA_TASK_COND_HIDDEN;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_conditions(task,cond));
    cond = MA_TASK_COND_RUN_IF_CONNECTED_TO_INTERNET;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_conditions(task,cond));
    cond = MA_TASK_COND_RESTART_ON_IDLE_RESUME;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_conditions(task,cond));
    //cond = MA_TASK_COND_SYSTEM_REQUIRED;
  //  TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_conditions(task,cond));
    //cond = MA_TASK_COND_RUN_ONLY_IF_LOGGED_ON;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_task_set_conditions_invalid) {
    ma_scheduler_t *scheduler = NULL;
    ma_task_t *task = NULL;
    ma_task_t *task1 = NULL;
    ma_task_cond_t cond = MA_TASK_COND_INTERACTIVE;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__, &task));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_task_set_conditions(task1,cond));
    cond = 0;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_conditions(task,cond));///Expected error if condition is zero
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_task_set_execution_status) {
    ma_scheduler_t *scheduler = NULL;
    ma_task_t *task = NULL;
    ma_task_exec_status_t status = MA_TASK_EXEC_NOT_STARTED;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__, &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_execution_status(task,status));
    status = MA_TASK_EXEC_RUNNING;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_execution_status(task,status));
    status = MA_TASK_EXEC_FAILED;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_execution_status(task,status));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_task_set_execution_status_invalid) {
    ma_scheduler_t *scheduler = NULL;
    ma_task_t *task = NULL;
    ma_task_t *task1 = NULL;
    ma_task_exec_status_t status = MA_TASK_EXEC_SUCCESS;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__, &task));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_task_set_execution_status(task1,status)); // NULL test for task
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_execution_status(task,status)); // NULL test for status
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_task_set_delay_policy) {
    ma_scheduler_t *scheduler = NULL;
    ma_task_t *task = NULL;
    ma_task_delay_policy_t policy;
    policy.delay_time.hour = 1;
    policy.delay_time.minute=10;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__, &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_delay_policy(task,policy));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_task_set_delay_policy_invalid) {
    ma_scheduler_t *scheduler = NULL;
    ma_task_t *task = NULL;
    ma_task_t *task1 = NULL;
    ma_task_delay_policy_t policy;
    policy.delay_time.hour = 1;
    policy.delay_time.minute=10;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__, &task));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_task_set_delay_policy(task1,policy));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_task_get_state) {
    ma_scheduler_t *scheduler = NULL;
    ma_task_t *task = NULL;

    ma_trigger_run_once_policy_t policy = {0};
    ma_trigger_t *trigger = NULL;

    ma_task_state_t state;
    ma_task_time_t now = {0};

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__, &task));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_set_max_run_time_limit(task,1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_state(task,&state));
    TEST_ASSERT_EQUAL_INT(MA_TASK_STATE_NO_TRIGGER,state);


    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_state(task,&state));
    TEST_ASSERT_EQUAL_INT(MA_TASK_STATE_NOT_SCHEDULED,state);
    get_localtime(&now);
    now.minute += 1;
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_trigger_set_begin_date(trigger,&now));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));

}

TEST(scheduler_tests, ma_task_get_state_invalid) {
    ma_scheduler_t *scheduler = NULL;
    ma_task_t *task = NULL;
    ma_task_t *task1 = NULL;
    ma_task_state_t state;
    ma_task_state_t *state1=NULL;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__, &task));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_task_get_state(task1,&state));//NULL test for task
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_task_get_state(task,state1));//NULL test for state
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));

}

TEST(scheduler_tests, ma_task_get_next_runtime_after) {
    ma_scheduler_t *scheduler = NULL;
    ma_task_t *task = NULL;

    ma_trigger_daily_policy_t policy = {2};
    ma_trigger_t *trigger = NULL;

    ma_task_state_t state;
    ma_task_time_t now = {0};
    ma_task_time_t next_run_time = {0};

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__, &task));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_set_max_run_time_limit(task,1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_state(task,&state));

    TEST_ASSERT_EQUAL_INT(MA_OK,ma_trigger_create_daily(&policy,&trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_state(task,&state));
    get_localtime(&now);
    now.minute += 1;
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_trigger_set_begin_date(trigger,&now));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_start(task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_state(task,&state));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_state(task,&state));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_task_get_next_runtime_after_invalid) {
    ma_scheduler_t *scheduler = NULL;
    ma_task_t *task = NULL;
    ma_task_t *task1 = NULL;
    ma_task_time_t next_run_time = {0};

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__, &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_task_get_last_run_time) {
    ma_scheduler_t *scheduler = NULL;
    ma_task_t *task = NULL;
    ma_trigger_daily_policy_t policy = {2};
    ma_trigger_t *trigger = NULL;

    //uv_loop_t *loop = uv_default_loop();
    ma_task_time_t now = {0};
    ma_task_time_t last_run_time;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__, &task));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_trigger_create_daily(&policy,&trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));
    get_localtime(&now);
    now.minute += 1;
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_trigger_set_begin_date(trigger,&now));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_scheduler_start(scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_last_run_time(task,&last_run_time));
    printf("Last time=%d hour %d miniute\n",last_run_time.hour,last_run_time.minute);
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_scheduler_stop(scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));

}

TEST(scheduler_tests, ma_task_get_last_run_time_invalid) {
    ma_scheduler_t *scheduler = NULL;
    ma_task_t *task = NULL;
    ma_task_t *task1 = NULL;

    ma_task_time_t next_run_time;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__, &task));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_task_get_last_run_time(task1,&next_run_time));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_task_get_last_run_time(task,NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));

}

TEST(scheduler_tests, ma_task_get_next_run_time) {
    ma_scheduler_t *scheduler = NULL;
    ma_task_t *task = NULL;

    ma_trigger_daily_policy_t policy = {2};
    ma_trigger_t *trigger = NULL;

    ma_task_time_t now = {0};
    ma_task_time_t next_run_time;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__, &task));

    TEST_ASSERT_EQUAL_INT(MA_OK,ma_trigger_create_daily(&policy,&trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));
    get_localtime(&now);
    now.minute += 1;
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_trigger_set_begin_date(trigger,&now));
   	TEST_ASSERT_EQUAL_INT(MA_OK,ma_scheduler_start(scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_next_run_time(task,&next_run_time));
	TEST_ASSERT_EQUAL_INT(MA_OK,ma_scheduler_stop(scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));

}

TEST(scheduler_tests, ma_task_get_next_run_time_invalid) {
    ma_scheduler_t *scheduler = NULL;
    ma_task_t *task = NULL;
    ma_task_t *task1 = NULL;
    ma_task_time_t next_run_time;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__, &task));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_task_get_next_run_time(task1,&next_run_time));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_task_get_last_run_time(task,NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));

}

TEST(scheduler_tests, ma_task_get_priority) {
    ma_scheduler_t *scheduler = NULL;
    ma_task_t *task = NULL;
    ma_task_priority_t priority = MA_TASK_PRIORITY_LOW;
    ma_task_priority_t priority_out;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__, &task));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_priority(task,priority));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_priority(task,&priority_out));
    TEST_ASSERT_EQUAL_INT(priority,priority_out);

    priority = MA_TASK_PRIORITY_HIGH;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_priority(task,priority));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_priority(task,&priority_out));
    TEST_ASSERT_EQUAL_INT(priority,priority_out);

    priority = MA_TASK_PRIORITY_NORMAL;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_priority(task,priority));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_priority(task,&priority_out));
    TEST_ASSERT_EQUAL_INT(priority,priority_out);

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_task_get_priority_invalid) {
    ma_scheduler_t *scheduler = NULL;
    ma_task_t *task = NULL;
    ma_task_t *task1 = NULL;
    ma_task_priority_t priority_out;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__, &task));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_task_get_priority(task1,&priority_out));

    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_task_get_priority(task,NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_task_get_conditions) {
    ma_scheduler_t *scheduler = NULL;
    ma_task_t *task = NULL;

    ma_task_cond_t cond = MA_TASK_COND_INTERACTIVE;
    uint32_t cond_check = MA_TASK_COND_INTERACTIVE|MA_TASK_COND_DELETE_WHEN_DONE;
    uint32_t cond_out;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__, &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_condition(task,cond));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_conditions(task,&cond_out));

    cond = MA_TASK_COND_DELETE_WHEN_DONE;
    cond_check |= cond;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_condition(task,cond));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_conditions(task,&cond_out));
    TEST_ASSERT_EQUAL_INT(cond_check,cond_out);

    cond = MA_TASK_COND_DISABLED;
    cond_check |= cond;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_condition(task,cond));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_conditions(task,&cond_out));
    TEST_ASSERT_EQUAL_INT(cond_check,cond_out);

    cond = MA_TASK_COND_START_ONLY_IF_IDLE;
    cond_check |= cond;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_condition(task,cond));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_conditions(task,&cond_out));
    TEST_ASSERT_EQUAL_INT(cond_check,cond_out);

    cond = MA_TASK_COND_KILL_ON_IDLE_END;
    cond_check |= cond;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_condition(task,cond));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_conditions(task,&cond_out));
    TEST_ASSERT_EQUAL_INT(cond_check,cond_out);

    cond = MA_TASK_COND_DONT_START_IF_ON_BATTERIES;
    cond_check |= cond;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_condition(task,cond));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_conditions(task,&cond_out));
    TEST_ASSERT_EQUAL_INT(cond_check,cond_out);

    cond = MA_TASK_COND_STOP_IF_GOING_ON_BATTERIES;
    cond_check |= cond;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_condition(task,cond));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_conditions(task,&cond_out));
    TEST_ASSERT_EQUAL_INT(cond_check,cond_out);

    cond = MA_TASK_COND_RUN_ONLY_IF_DOCKED;
    cond_check |= cond;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_condition(task,cond));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_conditions(task,&cond_out));
    TEST_ASSERT_EQUAL_INT(cond_check,cond_out);

    cond = MA_TASK_COND_HIDDEN;
    cond_check |= cond;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_condition(task,cond));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_conditions(task,&cond_out));
    TEST_ASSERT_EQUAL_INT(cond_check,cond_out);

    cond = MA_TASK_COND_RUN_IF_CONNECTED_TO_INTERNET;
    cond_check |= cond;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_condition(task,cond));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_conditions(task,&cond_out));
    TEST_ASSERT_EQUAL_INT(cond_check,cond_out);

    cond = MA_TASK_COND_RESTART_ON_IDLE_RESUME;
    cond_check |= cond;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_condition(task,cond));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_conditions(task,&cond_out));
    TEST_ASSERT_EQUAL_INT(cond_check,cond_out);

//    cond = MA_TASK_COND_SYSTEM_REQUIRED;
   // cond_check |= cond;
   //// TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_conditions(task,cond));
    //TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_conditions(task,&cond_out));
    //TEST_ASSERT_EQUAL_INT(cond_check,cond_out);

    //cond = MA_TASK_COND_RUN_ONLY_IF_LOGGED_ON;
    //cond_check |= cond;
    //TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_condition(task,cond));
    //TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_conditions(task,&cond_out));
    //TEST_ASSERT_EQUAL_INT(cond_check,cond_out);

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}


TEST(scheduler_tests, ma_task_get_conditions_invalid) {
    ma_scheduler_t *scheduler = NULL;
    ma_task_t *task = NULL;
    ma_task_t *task1 = NULL;
    ma_uint32_t cond;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__, &task));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_task_get_conditions(task1,&cond));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_task_get_conditions(task,NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_task_get_id) {
    ma_scheduler_t *scheduler = NULL;
    ma_task_t *task = NULL;
    ma_task_state_t state;
    const char *id = NULL;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__, &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_state(task,&state));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_id(task, &id));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_task_get_id_invalid) {
    ma_scheduler_t *scheduler = NULL;
    ma_task_t *task = NULL;
    ma_task_t *task1 = NULL;
    const char *id = NULL;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__, &task));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_task_get_id(task1, &id));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_task_get_id(task,NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_task_get_retry_policy) {
    ma_scheduler_t *scheduler = NULL;
    ma_trigger_t *trigger = NULL;
    ma_task_t *task = NULL;
    ma_trigger_run_once_policy_t policy = {0};
    ma_task_retry_policy_t retry_policy ={0};
    ma_task_retry_policy_t retry_policy_out = {0};
    retry_policy.retry_count = 3;
    retry_policy.retry_interval.hour = 1;
    retry_policy.retry_interval.minute = 10;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__, &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_retry_policy(task,retry_policy));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_retry_policy(task,&retry_policy_out));
    TEST_ASSERT_EQUAL_INT(retry_policy.retry_count,retry_policy_out.retry_count);
    TEST_ASSERT_EQUAL_INT(retry_policy.retry_interval.hour,retry_policy_out.retry_interval.hour);
    TEST_ASSERT_EQUAL_INT(retry_policy.retry_interval.minute,retry_policy_out.retry_interval.minute);

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}


TEST(scheduler_tests, ma_task_get_retry_policy_invalid) {
    ma_scheduler_t *scheduler = NULL;
    ma_trigger_t *trigger = NULL;
    ma_task_t *task = NULL;
    ma_task_t *task1 = NULL;
    ma_trigger_run_once_policy_t policy = {0};
    ma_task_retry_policy_t retry_policy ={0};
    ma_task_retry_policy_t retry_policy_out = {0};
    retry_policy.retry_count = 3;
    retry_policy.retry_interval.hour = 1;
    retry_policy.retry_interval.minute = 10;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__, &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_retry_policy(task,retry_policy));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_task_get_retry_policy(task1,&retry_policy_out));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_task_get_retry_policy(task,NULL));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_task_get_max_run_time) {
    ma_scheduler_t *scheduler = NULL;
    ma_trigger_t *trigger = NULL;
    ma_task_t *task = NULL;
    ma_trigger_run_once_policy_t policy = {0};
    ma_uint16_t max_run_time = 10;
    ma_uint16_t max_run_time_out = 0;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__, &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time(task,max_run_time));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_max_run_time(task,&max_run_time_out));
    TEST_ASSERT_EQUAL_INT(max_run_time_out,max_run_time);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_task_get_max_run_time_invalid) {
    ma_scheduler_t *scheduler = NULL;
    ma_trigger_t *trigger = NULL;
    ma_task_t *task = NULL;
    ma_task_t *task1 = NULL;
    ma_trigger_run_once_policy_t policy = {0};
    ma_uint16_t max_run_time = 10;
    ma_uint16_t max_run_time_out = 0;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__, &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time(task,max_run_time));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_task_get_max_run_time(task1,&max_run_time_out));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_task_get_max_run_time(task,NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_task_get_max_run_time_limit) {
    ma_scheduler_t *scheduler = NULL;
    ma_trigger_t *trigger = NULL;
    ma_task_t *task = NULL;
    ma_trigger_run_once_policy_t policy = {0};
    ma_uint16_t max_run_time = 10;
    ma_uint16_t max_run_time_limit = 100;
    ma_uint16_t max_run_time_limit_out = 0;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__, &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time(task,max_run_time));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time_limit(task,max_run_time_limit));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_max_run_time_limit(task,&max_run_time_limit_out));
    TEST_ASSERT_EQUAL_INT(max_run_time_limit_out,max_run_time);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_task_get_max_run_time_limit_invalid) {
    ma_scheduler_t *scheduler = NULL;
    ma_trigger_t *trigger = NULL;
    ma_task_t *task = NULL;
    ma_task_t *task1 = NULL;

    ma_trigger_run_once_policy_t policy = {0};
    ma_uint16_t max_run_time = 10;
    ma_uint16_t max_run_time_limit = 100;
    ma_uint16_t max_run_time_limit_out = 0;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__, &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time(task,max_run_time));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time_limit(task,max_run_time_limit));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_task_get_max_run_time_limit(task1,&max_run_time_limit_out));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_task_get_max_run_time_limit(task,NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_task_get_loop) {
    ma_scheduler_t *scheduler = NULL;
    ma_task_t *task = NULL;
    uv_loop_t *loop = uv_default_loop();
    uv_loop_t *loop_out = NULL;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__, &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_loop(task,loop));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_loop(task,&loop_out));
    TEST_ASSERT_EQUAL_PTR(loop,loop_out);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_task_get_loop_invalid) {
    ma_scheduler_t *scheduler = NULL;
    ma_task_t *task = NULL;
    ma_task_t *task1 = NULL;
    uv_loop_t *loop_out = NULL;
    uv_loop_t *loop = uv_default_loop();

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__, &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_loop(task,loop));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_task_get_loop(task1,&loop_out));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_task_get_loop(task,NULL));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_task_get_execution_status) {
    ma_scheduler_t *scheduler = NULL;
    ma_task_t *task = NULL;

    ma_task_exec_status_t status = MA_TASK_EXEC_NOT_STARTED;
    ma_task_exec_status_t status_out;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__, &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_execution_status(task,status));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_execution_status(task,&status_out));
    TEST_ASSERT_EQUAL_INT(status,status_out);

    //status = MA_TASK_EXEC_RUNNING;
    //TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_execution_status(task,status));
    //TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_execution_status(task,&status_out));
    //TEST_ASSERT_EQUAL_INT(status,status_out);

    //status = MA_TASK_EXEC_FAILED;
    //TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_execution_status(task,status));
    //TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_execution_status(task,&status_out));
    //TEST_ASSERT_EQUAL_INT(status,status_out);

    //status = MA_TASK_EXEC_SUCCESS;
    //TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_execution_status(task,status));
    //TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_execution_status(task,&status_out));
    //TEST_ASSERT_EQUAL_INT(status,status_out);

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_task_get_execution_status_invalid) {
    ma_scheduler_t *scheduler = NULL;
    ma_task_t *task = NULL;
    ma_task_t *task1 = NULL;

    ma_task_exec_status_t status = MA_TASK_EXEC_NOT_STARTED;
    ma_task_exec_status_t status_out;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__, &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_execution_status(task,status));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_task_get_execution_status(task1,&status_out));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_task_get_execution_status(task,NULL));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_task_add_trigger_invalid) {
    ma_scheduler_t *scheduler = NULL;
    ma_trigger_t *trigger = NULL;

    ma_task_t *task = NULL;
    ma_task_t *task1 = NULL;

    ma_trigger_run_once_policy_t policy = {0};
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__, &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_task_add_trigger(task1, trigger));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_task_add_trigger(task, NULL));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_task_remove_trigger) {
    ma_scheduler_t *scheduler = NULL;
    ma_trigger_t *trigger = NULL;
    ma_trigger_t *trigger1 = NULL;
    ma_trigger_t *trigger2 = NULL;
    ma_task_t *task = NULL;
    char *id = NULL;

    ma_trigger_run_once_policy_t policy = {0};
    ma_trigger_daily_policy_t policy1 = {0};
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__, &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));

    TEST_ASSERT_EQUAL_INT(MA_OK,ma_trigger_create_daily(&policy1,&trigger1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger1));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger2));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger2));

    TEST_ASSERT_EQUAL_INT(MA_OK,ma_trigger_get_id(trigger1,(const char **)&id));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_remove_trigger(task, id));

    TEST_ASSERT_EQUAL_INT(MA_OK,ma_trigger_get_id(trigger,(const char **)&id));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_remove_trigger(task, id));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_task_remove_trigger_invalid) {
    ma_scheduler_t *scheduler = NULL;
    ma_trigger_t *trigger = NULL;
    ma_trigger_t *trigger1 = NULL;
    ma_trigger_t *trigger2 = NULL;
    ma_task_t *task = NULL;
    ma_task_t *task1 = NULL;
    char *id = NULL;
    char *id1 = "100";

	ma_trigger_run_once_policy_t policy = {0};
    ma_trigger_daily_policy_t policy1 = {0};
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__, &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));

    TEST_ASSERT_EQUAL_INT(MA_OK,ma_trigger_create_daily(&policy1,&trigger1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger1));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger2));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger2));

    TEST_ASSERT_EQUAL_INT(MA_OK,ma_trigger_get_id(trigger1,(const char **)&id));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_task_remove_trigger(task1, id));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_task_remove_trigger(task, NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_SCHEDULER_INVALID_TRIGGER_ID, ma_task_remove_trigger(task, id1));//Idealy return invalid ID error condition scheck is done in code.

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_task_find_trigger) {
    ma_scheduler_t *scheduler = NULL;
    ma_trigger_t *trigger = NULL;
    ma_trigger_t *trigger1 = NULL;
    ma_trigger_t *trigger2 = NULL;
    ma_trigger_t *trigger_out = NULL;

    ma_task_t *task = NULL;
    char *id = NULL;
    ma_trigger_run_once_policy_t policy = {0};
    ma_trigger_daily_policy_t policy1 = {0};
    char tid[3] = {0};

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__, &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger));
	

    TEST_ASSERT_EQUAL_INT(MA_OK,ma_trigger_create_daily(&policy1,&trigger1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger1));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger2));
	

    TEST_ASSERT_EQUAL_INT(MA_OK,ma_trigger_get_id(trigger1,(const char **)&id));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_find_trigger(task,id,&trigger_out));
	TEST_ASSERT_EQUAL_PTR(trigger_out,trigger1);
    strcpy(tid, id);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_remove_trigger(task, id));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger_out));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_SCHEDULER_INVALID_TRIGGER_ID,ma_task_find_trigger(task,tid,&trigger_out));
	
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger2));
	
    //TEST_ASSERT_EQUAL_PTR(trigger_out,trigger1);//Finding the trigger after we removed and its expected currently

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}


TEST(scheduler_tests, ma_task_find_trigger_invalid) {
    ma_scheduler_t *scheduler = NULL;
    ma_trigger_t *trigger = NULL;
    ma_trigger_t *trigger1 = NULL;
    ma_trigger_t *trigger2 = NULL;
    ma_trigger_t *trigger_out = NULL;

    ma_task_t *task = NULL;
    ma_task_t *task1 = NULL;
    char *id = NULL;
    ma_trigger_run_once_policy_t policy = {0};
    ma_trigger_daily_policy_t policy1 = {0};
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__, &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));

    TEST_ASSERT_EQUAL_INT(MA_OK,ma_trigger_create_daily(&policy1,&trigger1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger1));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger2));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger2));

    TEST_ASSERT_EQUAL_INT(MA_OK,ma_trigger_get_id(trigger1,(const char **)&id));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_task_find_trigger(task1,id,&trigger_out));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_task_find_trigger(task,id,NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_task_find_trigger(task,NULL,&trigger_out));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_task_find_trigger(NULL,NULL,NULL));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_task_stop) {
    ma_scheduler_t *scheduler = NULL;
    ma_task_t *task = NULL;

    ma_trigger_run_once_policy_t policy = {0};
    ma_trigger_t *trigger = NULL;

    uv_loop_t *loop = uv_default_loop();
    ma_task_state_t state;
    ma_task_time_t now = {0};

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__, &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_state(task,&state));
    printf("State1=%d\n",state);


    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_state(task,&state));
    printf("State2=%d\n",state);
    get_localtime(&now);
    now.minute += 1;
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_trigger_set_begin_date(trigger,&now));

    TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_start(task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_state(task,&state));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_stop(task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_state(task,&state));
    uv_run(loop UV10_ONLY_ARGS);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_state(task,&state));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}


TEST(scheduler_tests, ma_task_stop_invalid) {
    
    ma_task_t *task1 = NULL;
	 
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_task_start(task1));//Start invalid test
	TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG,ma_task_stop(task1)); //Stop invalid test
}

TEST(scheduler_tests, ma_trigger_begin_end_date_valid_invalid) {
    ma_trigger_run_once_policy_t policy = {0};
    ma_trigger_t *trigger = NULL;
    ma_task_time_t start_date = {0};
    ma_task_time_t get_start_date = {0};
    ma_task_time_t end_date = {0};
    ma_task_time_t get_end_date = {0};
    policy.once = MA_TRUE;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger));

    get_localtime(&start_date);
    memcpy(&end_date,&start_date,sizeof(ma_task_time_t));
    end_date.day += 2;

    //valid Tests
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger, NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_get_begin_date(trigger, &get_start_date));
    TEST_ASSERT_EQUAL_INT(get_start_date.day,start_date.day);

    start_date.day += 1;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger, &start_date));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_get_begin_date(trigger, &get_start_date));
    TEST_ASSERT_EQUAL_INT(get_start_date.day,start_date.day);

    TEST_ASSERT_EQUAL_INT(MA_OK,ma_trigger_set_end_date(trigger,&end_date));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_get_end_date(trigger, &get_end_date));
    TEST_ASSERT_EQUAL_INT(get_end_date.day,end_date.day);

    //Invalid Tests
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_set_begin_date(NULL, &start_date));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_set_begin_date(NULL, NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_get_begin_date(trigger, NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_get_begin_date(NULL, &get_start_date));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_get_begin_date(NULL, NULL));

    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_set_end_date(trigger, NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_set_end_date(NULL, &end_date));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger, NULL));
    end_date.day -= 4;
    TEST_ASSERT_EQUAL_INT(MA_ERROR_SCHEDULER_INVALID_DATE, ma_trigger_set_end_date(trigger, &end_date));

    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_get_end_date(trigger, NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_get_end_date(NULL, &get_start_date));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_get_end_date(NULL, NULL));


   TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));
}


TEST(scheduler_tests, ma_trigger_get_triggertype_invalid) {
    ma_trigger_run_once_policy_t policy = {0};
    ma_trigger_t *trigger = NULL;
    policy.once = MA_TRUE;

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));
}

TEST(scheduler_tests, ma_trigger_get_id_valid_invalid) {
    ma_trigger_list_t *tl = NULL;
    ma_trigger_t *trigger1 = NULL;
    ma_trigger_t *trigger2 = NULL;
    ma_trigger_t *trigger3 = NULL;
    ma_trigger_t *ret_trigger = NULL;
    ma_trigger_run_once_policy_t runonce = {0};
    const char *trigger1_id = NULL;
    const char *trigger2_id = NULL;
    const char *trigger3_id = NULL;
	ma_task_t *task = NULL;
	char *task_id ="Test-task";

   	TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_create(task_id,&task));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_get_trigger_list(task,&tl));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&runonce, &trigger1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&runonce, &trigger2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&runonce, &trigger3));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_list_add(tl, trigger1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_list_add(tl, trigger2));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_list_add(tl, trigger3));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_get_id(trigger2, &trigger2_id));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_list_find(tl, trigger2_id, &ret_trigger));
    TEST_ASSERT_EQUAL_PTR(ret_trigger,trigger2);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_get_id(trigger3, &trigger3_id));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger2));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger3));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(ret_trigger));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_get_id(NULL, &trigger1_id));
     TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_trigger_get_id(trigger2, NULL));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_list_remove(tl, trigger3_id));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_release(task));
}


TEST(scheduler_tests, ma_trigger_get_state) {
    ma_scheduler_t *scheduler = NULL;
    ma_task_t *task = NULL;

    ma_trigger_run_once_policy_t policy = {0};
    ma_trigger_t *trigger = NULL;

    ma_trigger_run_once_policy_t policy1 = {0};
    ma_trigger_t *trigger1 = NULL;

    ma_task_state_t state;
    ma_trigger_state_t trigger_state;
    ma_task_time_t now = {0};

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "test","100", __FUNCTION__, __FUNCTION__, &task));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_set_max_run_time_limit(task,1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_state(task,&state));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy, &trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_state(task,&state));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_trigger_get_state(trigger,&trigger_state));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&policy1, &trigger1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger1));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_trigger_get_state(trigger1,&trigger_state));

    get_localtime(&now);
    now.minute += 1;
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_trigger_set_begin_date(trigger,&now));
     now.minute += 2;
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_trigger_set_begin_date(trigger1,&now));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_scheduler_start(scheduler));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_state(task,&state));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_trigger_get_state(trigger,&trigger_state));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_trigger_get_state(trigger1,&trigger_state));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_state(task,&state));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_trigger_get_state(trigger,&trigger_state));
    TEST_ASSERT_EQUAL_INT(MA_OK,ma_trigger_get_state(trigger1,&trigger_state));

	TEST_ASSERT_EQUAL_INT(MA_OK,ma_scheduler_stop(scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, ma_scheduler_create_release_test) {
    ma_scheduler_t *scheduler = NULL;
    
    ma_trigger_t *trigger_A = NULL;
    ma_trigger_t *trigger_B = NULL;
    ma_trigger_t *trigger_A1 = NULL;
    ma_trigger_t *trigger_B1 = NULL;
    
    ma_task_t *task_100 = NULL;
    ma_task_t *task_200 = NULL;
    
    ma_trigger_run_once_policy_t runonce_policy = {0};
    ma_trigger_daily_policy_t daily_policy = {0};
    
    ma_task_time_t now = {0};
    const char *task_id = NULL;
    ma_task_t **task_set = NULL;
    size_t no_of_tasks = 0;
    size_t i = 0;
    size_t count = 0;
    ma_trigger_list_t *trigger_list = NULL;
    size_t no_of_triggers = 0;

    get_localtime(&now);
    now.minute += 10;
    runonce_policy.once = MA_TRUE;

    daily_policy.days_interval = 5;

    // bring up scheduler
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "Test Task", "100", __FUNCTION__, __FUNCTION__, &task_100));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create_task(scheduler, "Test Task", "200", __FUNCTION__, __FUNCTION__, &task_200));
        
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&runonce_policy, &trigger_A));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_A, &now));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_100, trigger_A));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger_A));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&runonce_policy, &trigger_A1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_A1, &now));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_100, trigger_A1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger_A1));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&daily_policy, &trigger_B));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_B, &now));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_200, trigger_B));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger_B));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&daily_policy, &trigger_B1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_B1, &now));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_200, trigger_B1));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger_B1));
    
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_start(scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_stop(scheduler));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
	scheduler= NULL;
    // bring up again
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_start(scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_get_task_set(scheduler, &task_set, &no_of_tasks));
    TEST_ASSERT_EQUAL_INT(2, no_of_tasks);
    for(i = 0; i < no_of_tasks; ++i) {
        TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id(task_set[i], &task_id));
        if(0 == strcmp("100", task_id)) { 
            ++count;
            TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_trigger_list(task_set[i], &trigger_list));
            TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_list_size(trigger_list, &no_of_triggers));
            TEST_ASSERT_EQUAL_INT(2, no_of_triggers);

        }
        else if(0 == strcmp("200", task_id)) {
            ++count;
            TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_trigger_list(task_set[i], &trigger_list));
            TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_list_size(trigger_list, &no_of_triggers));
            TEST_ASSERT_EQUAL_INT(2, no_of_triggers);
        }
        else TEST_ASSERT_EQUAL_STRING_MESSAGE(0, 1, "invalid task id retreived from the scheduler datastore");
    }
    TEST_ASSERT_EQUAL_INT(2, count);
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release_task_set(task_set));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_stop(scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
	
}

TEST(scheduler_tests, ma_scheduler_task_copy) {
    ma_task_t *task_1 = NULL;
    ma_trigger_t *trigger_A = NULL;
    ma_trigger_t *trigger_B = NULL;
    ma_task_time_t now = {0};

    ma_task_t *reply_task_1 = NULL;

    ma_uint16_t reply_max_run_time_limit = 0;
    ma_uint16_t reply_max_run_time = 0;

    ma_trigger_run_once_policy_t runonce_policy = {0};
    ma_task_repeat_policy_t policy_repeat = {0};
    ma_trigger_daily_policy_t daily_policy = {0};
    ma_task_retry_policy_t retry_policy ={0};
    ma_task_missed_policy_t missed_policy = {0};
    ma_task_randomize_policy_t randomize_policy = {0};
    ma_task_exec_status_t status = MA_TASK_EXEC_SUCCESS;
    ma_variant_t *get_app_payload = NULL;

    ma_task_repeat_policy_t reply_policy_repeat = {0, 0};
    ma_task_retry_policy_t reply_retry_policy ={0};
    ma_task_missed_policy_t reply_missed_policy = {0};
    ma_task_exec_status_t reply_status = MA_TASK_EXEC_FAILED;

    const char *reply_task_id = NULL;
    const char *reply_task_name = NULL;
    const char *reply_task_type = NULL;
    const char *reply_creator_id = NULL;
    const char *reply_software_id = NULL;

    runonce_policy.once = MA_TRUE;
    daily_policy.days_interval = 2;
    policy_repeat.repeat_interval.hour = 1;
    policy_repeat.repeat_interval.minute = 30;
    policy_repeat.repeat_until.hour = 10;
    policy_repeat.repeat_until.minute = 45;
    retry_policy.retry_count = 2;
    retry_policy.retry_interval.hour = 2;
    retry_policy.retry_interval.minute = 15;
    missed_policy.missed_duration.hour = 4;
    missed_policy.missed_duration.minute = 30;
    randomize_policy.random_interval.hour = 3;
    randomize_policy.random_interval.minute = 40;

    get_localtime(&now);
	advance_current_date_by_seconds(120, &now);

    ma_variant_create(&get_app_payload);

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_create("ma.task1.1001", &task_1));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_name(task_1, "Task 1"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_type(task_1, "Add"));
        
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_run_once(&runonce_policy, &trigger_A));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_A, &now));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&daily_policy, &trigger_B));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger_B, &now));

    TEST_ASSERT_EQUAL_INT(MA_OK,ma_task_set_max_run_time_limit(task_1, 5));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_retry_policy(task_1, retry_policy));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_repeat_policy(task_1, policy_repeat));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_missed_policy(task_1, missed_policy));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_randomize_policy(task_1, randomize_policy));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_execution_status(task_1, status));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_creator_id(task_1, "CREATOR ID"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_software_id(task_1, "SOFTWARE ID"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_app_payload(task_1, get_app_payload));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_max_run_time(task_1, 1));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_1, trigger_A));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger_A));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task_1, trigger_B));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger_B));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_copy(task_1, &reply_task_1));

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_max_run_time_limit(reply_task_1, &reply_max_run_time_limit));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_retry_policy(reply_task_1, &reply_retry_policy));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_repeat_policy(reply_task_1, &reply_policy_repeat));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_missed_policy(reply_task_1, &reply_missed_policy));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id(reply_task_1, &reply_task_id));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_name(reply_task_1, &reply_task_name));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_type(reply_task_1, &reply_task_type));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_execution_status(reply_task_1, &reply_status));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_creator_id(reply_task_1, &reply_creator_id));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_software_id(reply_task_1, &reply_software_id));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_max_run_time(reply_task_1, &reply_max_run_time));

    TEST_ASSERT_EQUAL_INT(0, strcmp(reply_task_id, "ma.task1.1001"));
    TEST_ASSERT_EQUAL_INT(0, strcmp(reply_task_name,"Task 1"));
    TEST_ASSERT_EQUAL_INT(0, strcmp(reply_task_type,"Add"));
    TEST_ASSERT_EQUAL_INT(0, strcmp(reply_creator_id,"CREATOR ID"));
    TEST_ASSERT_EQUAL_INT(0, strcmp(reply_software_id,"SOFTWARE ID"));
    TEST_ASSERT_EQUAL_INT(1, reply_max_run_time_limit);
    TEST_ASSERT_EQUAL_INT(1, reply_max_run_time);
    //TEST_ASSERT_EQUAL_INT(reply_retry_policy.retry_count, retry_policy.retry_count);
    //TEST_ASSERT_EQUAL_INT(reply_retry_policy.retry_interval.hour, retry_policy.retry_interval.hour);
    //TEST_ASSERT_EQUAL_INT(reply_retry_policy.retry_interval.minute, retry_policy.retry_interval.minute);
    TEST_ASSERT_EQUAL_INT(policy_repeat.repeat_interval.hour, reply_policy_repeat.repeat_interval.hour);
    TEST_ASSERT_EQUAL_INT(policy_repeat.repeat_interval.minute, reply_policy_repeat.repeat_interval.minute);
    TEST_ASSERT_EQUAL_INT(policy_repeat.repeat_until.hour, reply_policy_repeat.repeat_until.hour);
    TEST_ASSERT_EQUAL_INT(policy_repeat.repeat_until.minute, reply_policy_repeat.repeat_until.minute);
    TEST_ASSERT_EQUAL_INT(reply_missed_policy.missed_duration.hour, missed_policy.missed_duration.hour);
    TEST_ASSERT_EQUAL_INT(reply_missed_policy.missed_duration.minute, missed_policy.missed_duration.minute);
	
	ma_task_release(task_1);
	ma_task_release(reply_task_1);
	ma_variant_release(get_app_payload);
}

TEST(scheduler_tests, ma_calculate_week_day) {
    ma_task_time_t start_date = {0};
    ma_task_time_t next_date = {0};
    ma_uint32_t week_days = THURSDAY;
    ma_uint16_t weekly_interval = 2; /* 2013/10/10 9:12:0 */

    get_localtime(&start_date);
    start_date.day = 19;
    start_date.month = 9;
    get_next_week_date_after(start_date, week_days, weekly_interval, &next_date);

}

TEST(scheduler_tests, compute_until_time) {
    ma_task_time_t now = {0};
    ma_task_interval_t until = {0};
    ma_task_interval_t duration = {0};

    get_localtime(&now);
    now.hour = 0; now.minute = 0; now.secs = 0;
    until.hour = 11; until.minute = 59;
    ma_compute_until_duration(now, until, &duration);

}


TEST(scheduler_tests, ma_calculate_day_of_week) {
    ma_task_time_t start_date = {0};
    ma_task_time_t next_date = {0};
    ma_uint32_t days_of_week = SATURDAY;
    ma_uint32_t months = NOVEMBER|DECEMBER|JANUARY|FEBRUARY;
    ma_uint16_t which_week = LAST_WEEK; /* 2013/10/10 9:12:0 */

    get_localtime(&start_date);
    get_next_month_dow_date_after(start_date, months, which_week, days_of_week, &next_date);
}
TEST(scheduler_tests, ma_calculate_date_of_month) {
    ma_task_time_t start_date = {0};
    ma_task_time_t next_date = {0};
    ma_uint16_t day = 30;
    ma_uint32_t months = APRIL;

    get_localtime(&start_date);
    get_next_month_date_after(start_date,  months, day, &next_date);
}

TEST(scheduler_tests, logon_trigger_create_delete) {
    ma_trigger_logon_policy_t policy = {0};
    ma_scheduler_t *scheduler = NULL;
    ma_trigger_t *logon = NULL;
    ma_task_t *task = NULL;

    policy.daily_once = 0;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_create("logon_task", &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_name(task, __FUNCTION__));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_type(task, __FUNCTION__));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_creator_id(task, __FUNCTION__));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_software_id(task, __FUNCTION__));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_logon(&policy, &logon));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, logon));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(logon));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_add(scheduler, task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_release(task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_start(scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_remove(scheduler, "logon_task"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_stop(scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, system_start_trigger_create_delete) {
    ma_trigger_systemstart_policy_t policy = {0};
    ma_scheduler_t *scheduler = NULL;
    ma_trigger_t *logon = NULL;
    ma_task_t *task = NULL;

    policy.daily_once = 0;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_create("system_start", &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_name(task, __FUNCTION__));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_type(task, __FUNCTION__));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_creator_id(task, __FUNCTION__));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_software_id(task, __FUNCTION__));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_system_start(&policy, &logon));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, logon));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(logon));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_add(scheduler, task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_release(task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_start(scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_remove(scheduler, "system_start"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_stop(scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}


TEST(scheduler_tests, scheduler_add_valid_invalid) {
    ma_trigger_systemstart_policy_t policy = {0};
    ma_scheduler_t *scheduler = NULL;
    ma_trigger_t *logon = NULL;
    ma_task_t *task = NULL;

    policy.daily_once = 0;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_create("system_start", &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_name(task, __FUNCTION__));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_type(task, __FUNCTION__));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_creator_id(task, __FUNCTION__));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_software_id(task, __FUNCTION__));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_system_start(&policy, &logon));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, logon));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(logon));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_scheduler_add(NULL, task));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_scheduler_add(scheduler, NULL));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_add(scheduler, task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_release(task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_start(scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_remove(scheduler, "system_start"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_stop(scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}

TEST(scheduler_tests, scheduler_remove_valid_invalid) {
    ma_trigger_systemstart_policy_t policy = {0};
    ma_scheduler_t *scheduler = NULL;
    ma_trigger_t *logon = NULL;
    ma_task_t *task = NULL;

    policy.daily_once = 0;
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_create("system_start", &task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_name(task, __FUNCTION__));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_type(task, __FUNCTION__));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_creator_id(task, __FUNCTION__));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_software_id(task, __FUNCTION__));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_system_start(&policy, &logon));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, logon));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(logon));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_add(scheduler, task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_release(task));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_start(scheduler));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_scheduler_remove(NULL, "system_start"));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_INVALIDARG, ma_scheduler_remove(scheduler, NULL));
    TEST_ASSERT_EQUAL_INT(MA_ERROR_SCHEDULER_INVALID_TASK_ID, ma_scheduler_remove(scheduler, "invalid-task"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_remove(scheduler, "system_start"));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_stop(scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(scheduler));
}



TEST(scheduler_tests, date_validator) {
    TEST_ASSERT_EQUAL_INT(MA_FALSE, ma_date_validator(31, 2, 2014));
    TEST_ASSERT_EQUAL_INT(MA_TRUE, ma_date_validator(1, 12, 2014));
    TEST_ASSERT_EQUAL_INT(MA_FALSE, ma_date_validator(0, 0, 0));
    TEST_ASSERT_EQUAL_INT(MA_FALSE, ma_date_validator(0, 0, 2014));
    TEST_ASSERT_EQUAL_INT(MA_FALSE, ma_date_validator(0, 1, 0));
    TEST_ASSERT_EQUAL_INT(MA_FALSE, ma_date_validator(4, 1, 0));
    TEST_ASSERT_EQUAL_INT(MA_TRUE, ma_date_validator(1, 1, 1));
    TEST_ASSERT_EQUAL_INT(MA_TRUE, ma_date_validator(29, 2, 2000));
}

///End





static void run_all_unit_tests(void) {
    do {RUN_TEST_GROUP(scheduler_test_group); } while (0);
	//do {RUN_TEST_GROUP(scheduler_service_test_group); }while(0);
}

static void ma_scheduler_test_create_scheduler() {
    ma_ds_database_t *ds_database = NULL;
    TEST_ASSERT_MESSAGE(MA_OK == ma_ut_create("scheduler_client_test"), "ma_ut_create failed to create the instance");
#if MA_WINDOWS
    Sleep(4 * 1000);
#else
    sleep(4);
#endif
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_create(g_msgbus, g_ds, MA_OUTPUT_FOLDER_PATH, &g_scheduler));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_start(g_scheduler));
}

static void ma_scheduler_test_release_scheduler() {
   TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_stop(g_scheduler));
   TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_release(g_scheduler));
   TEST_ASSERT_MESSAGE(MA_OK == ma_ut_release(), "ma_ut_release failed to create the instance");
}

static void run_all_function_tests(){
    uv_thread_t tid;
    ma_scheduler_test_create_scheduler();
    TEST_ASSERT_MESSAGE( 0 == uv_thread_create(&tid , ma_scheduler_create_loop_thread , NULL) , "Thread could not be created");
    //Function test will be added
    do {RUN_TEST_GROUP(scheduler_daily_test_group);} while (0);
    
    do {RUN_TEST_GROUP(scheduler_weekly_test_group); }while(0);
    do {RUN_TEST_GROUP(scheduler_monthly_date_test_group); }while(0);
    ma_scheduler_test_release_scheduler();
    shut_down = MA_TRUE;
    uv_thread_join(&tid);
}

int main(int argc, char *argv[]) {
    int i =	0;
    int group = 0;


#ifdef MA_WINDOWS
    MA_TRACKLEAKS;
#endif
    if(argc == 1)
        return UnityMain(argc, argv, run_all_unit_tests);
    else {
        for(i =0;i<argc;i++)
            if(!strcmp("-g",argv[i])){
                group = 1;
                break;
            }
            if(group)
                return UnityMain(argc, argv, run_all_function_tests);
            else
                return UnityMain(argc, argv, run_all_unit_tests);
    }
	
}
