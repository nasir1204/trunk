//#if 0
#include "unity.h"
#include "unity_fixture.h"

#include "ma/ma_common.h"
#include "ma/internal/ma_macros.h"
#include "ma/scheduler/ma_scheduler.h"
#include "ma/scheduler/ma_triggers.h"
#include "ma/scheduler/ma_task.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"
#include "ma/scheduler/ma_scheduler_defs.h"
#include "ma/internal/services/scheduler/ma_scheduler_service.h"
#include "ma/internal/services/ma_service.h"
#include "ma/internal/core/msgbus/ma_msgbus_impl.h"
#include "ma/internal/ma_context_internal.h"

#include <uv.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


#ifdef MA_WINDOWS
# include <crtdbg.h>
#include <Windows.h>
# define MA_TRACKLEAKS _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF)
# define MA_INPUT_FOLDER_PATH ".\\infile.xml"
# define MA_OUTPUT_FOLDER_PATH ".\\outfile.xml"
#else
# define MA_TRACKLEAKS ((void *)0)
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
# define MA_INPUT_FOLDER_PATH "./infile.xml"
# define MA_OUTPUT_FOLDER_PATH "./outfile.xml"
#endif

#define GLOBAL_DATABASE_NAME_FOR_UT    "scheduler_client_test_100.db"

static ma_db_t *database = NULL;
static uv_timer_t timer;
static uv_loop_t *loop = NULL;
static ma_ds_t *datastore = NULL;
static ma_event_loop_t *event_loop = NULL;
static ma_scheduler_service_t *service = NULL;
static unsigned short done = 0;

static ma_context_t *context;
static ma_msgbus_t *msgbus;

TEST_GROUP_RUNNER(scheduler_client_test_group) {
	do {RUN_TEST_CASE(scheduler_client_test, start_service); }while(0); // it should always executed first to ensure scheduler service is running before client requests
	do {RUN_TEST_CASE(scheduler_client_test, add_task_test); }while(0);
	do {RUN_TEST_CASE(scheduler_client_test, stop_service); }while(0); // don't forget to stop and release the service
}

TEST_SETUP(scheduler_client_test) {

}

TEST_TEAR_DOWN(scheduler_client_test) {

}

void timer_event(uv_timer_t *handle, int status) {
}

TEST(scheduler_client_test, start_service) {
	ma_ds_database_t *ds_database = NULL;
	Unity.TestFile = "scheduler_client_test.c";
	/* 
	* add a dummy timer event to keep the event loop non-empty.
	*/
	ma_context_create("ma.scheduler.client.test_100" , &context);
	ma_context_get_msgbus(context, &msgbus);
	event_loop = ma_msgbus_get_event_loop(msgbus);
	loop = ma_event_loop_get_uv_loop(event_loop);
	uv_timer_init(loop, &timer);
	uv_timer_start(&timer, timer_event, 60 * 1000, 60 * 1000);

	// create database data store
	unlink(GLOBAL_DATABASE_NAME_FOR_UT);
	TEST_ASSERT_MESSAGE(MA_OK == ma_db_intialize() , "ma_db_initialize failed");
	TEST_ASSERT_MESSAGE(MA_OK ==  ma_db_open( GLOBAL_DATABASE_NAME_FOR_UT,  &database), "ma_db_open failed to open global database datatbase");
	TEST_ASSERT_MESSAGE(MA_OK ==  ma_ds_database_create(database , "Policies" , &ds_database), "ma_ds_database_create failed to create the instance");
	datastore = (ma_ds_t *)ds_database;

	// start the scheduler service
	TEST_ASSERT_MESSAGE(MA_OK == ma_scheduler_service_create(msgbus, NULL, datastore, &service), "scheduler service creation failed");
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_service_start((ma_service_t *)service));
}

ma_error_t on_task_execute(ma_context_t *context, const char *task_type, const ma_task_t *task, void *cb_data) {
	char *task_id = NULL;
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id((ma_task_t *)task, &task_id));
	printf("Executing Task id %s...\n", task_id);
	ma_scheduler_update_task_status(context, task_id, MA_TASK_STATUS_SUCCESS);
	done = 1;
	return MA_OK;
}

ma_error_t on_task_stop(ma_context_t *context, const char *task_type, const ma_task_t *task, void *cb_data) {
	char *task_id = NULL;
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id((ma_task_t *)task, &task_id));
	printf("Stopping Task id %s...\n", task_id);
	done = 1;
	return MA_OK;
}

ma_error_t on_task_remove(ma_context_t *context, const char *task_type, const ma_task_t *task, void *cb_data) {
	char *task_id = NULL;
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_get_id((ma_task_t *)task, &task_id));
	printf("Removing Task id %s...\n", task_id);
	done = 1;
	return MA_OK;
}

TEST(scheduler_client_test, add_task_test) {
	ma_task_t *task = NULL;
	ma_trigger_daily_policy_t policy = {0};
	ma_trigger_t *trigger = NULL;
	ma_task_time_t start_date = {0};
	ma_variant_t *app_payload = NULL;
	ma_scheduler_task_callbacks_t cb = {&on_task_execute, &on_task_stop, &on_task_remove};

	policy.days_interval = 5;
	advance_current_date_by_seconds(60, &start_date);
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_create_daily(&policy, &trigger));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_set_begin_date(trigger, &start_date));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_create("ma.pp1.1001", "Task 1", "Add", &task));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_add_trigger(task, trigger));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_create_from_string("add task", strlen("add task"), &app_payload));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_app_payload(task, app_payload));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_set_creator_id(task, "scheduler consumer"));

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_register_task_callbacks(context, "Add", &cb, NULL));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_scheduler_add_task(context, task));

	// wait for the published messages
	while(!done) {
#ifdef WIN32
		Sleep(2 * 1000);
#else
		sleep(2);
#endif
	}

	TEST_ASSERT_EQUAL_INT(MA_OK, ma_variant_release(app_payload));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_trigger_release(trigger));
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_task_release(task));
}

TEST(scheduler_client_test, stop_service) {
	TEST_ASSERT_EQUAL_INT(MA_OK, ma_service_stop((ma_service_t *)service));
	TEST_ASSERT_MESSAGE(MA_OK == ma_scheduler_service_release(service), "scheduler service release failed");
	TEST_ASSERT_MESSAGE(MA_OK ==  ma_db_close(database), "ma_db_close failed to close the global datatbase");
	TEST_ASSERT_MESSAGE(MA_OK ==  ma_ds_database_release((ma_ds_database_t*)datastore), "ma_ds_database failed to release the instance");
	TEST_ASSERT_MESSAGE(MA_OK == ma_db_deintialize() , "ma_db_deintialize failed");   
	unlink(GLOBAL_DATABASE_NAME_FOR_UT);
	uv_timer_stop(&timer);
	ma_context_release(context);
}

//#endif
