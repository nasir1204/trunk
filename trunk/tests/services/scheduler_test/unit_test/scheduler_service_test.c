
#include "unity.h"
#include "unity_fixture.h"

#include "ma/ma_common.h"
#include "ma/internal/ma_macros.h"
#include "ma/internal/services/scheduler/ma_scheduler_core.h"
#include "ma/scheduler/ma_scheduler.h"
#include "ma/scheduler/ma_triggers.h"
#include "ma/scheduler/ma_task.h"
#include "ma/internal/services/scheduler/ma_scheduler_utils.h"
#include "ma/internal/services/scheduler/ma_scheduler_service.h"
#include "ma/internal/services/ma_service.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"

#include "ma_ut_setup.h"
#include <uv.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


#ifdef MA_WINDOWS
# include <crtdbg.h>
#include <Windows.h>
# define MA_INPUT_FOLDER_PATH ".\\infile.xml"
# define MA_OUTPUT_FOLDER_PATH ".\\outfile.xml"
#else
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
# define MA_INPUT_FOLDER_PATH "./infile.xml"
# define MA_OUTPUT_FOLDER_PATH "./outfile.xml"
#endif

#if (9 < UV_VERSION_MINOR)
#define UV10_ONLY_ARGS ,UV_RUN_DEFAULT    
#else
#define UV10_ONLY_ARGS
#endif


#define GLOBAL_DATABASE_NAME_FOR_UT    "g_datastoreut.db"

#define PRINT_TIME(X) \
    { \
        time_t t = time(NULL); \
        printf("\n ");\
        printf(X);\
        printf("\n%s - %s", __FUNCTION__, ctime(&t)); \
    }

uv_loop_t *g_loop;

static ma_ut_setup_t *g_ut_setup = NULL;

static ma_ds_t *g_ds = NULL;
static ma_msgbus_t *g_msgbus = NULL;
static ma_db_t *g_database_handle = NULL ;

TEST_GROUP_RUNNER(scheduler_service_test_group) {
    do {RUN_TEST_CASE(scheduler_service_test, conver_task_to_variant_vice_versa); }while(0);
    do {RUN_TEST_CASE(scheduler_service_test, convert_task_to_variant_vice_versa_with_run_once_and_daily_trigger); }while(0);    
    do {RUN_TEST_CASE(scheduler_service_test, convert_task_to_variant_vice_versa_with_missed_policy_set); }while(0);
    do {RUN_TEST_CASE(scheduler_service_test, convert_task_to_variant_vice_versa_with_repeat_policy_set);}while(0);
    do {RUN_TEST_CASE(scheduler_service_test, convert_task_to_variant_vice_versa_with_max_run_time_limit_set);} while(0);
}

static ma_error_t ma_ut_create(const char *product_id) {
    Unity.TestFile = "scheduler_test.c";
    /* Remove if any from previous run */
    unlink(GLOBAL_DATABASE_NAME_FOR_UT);
   	if(product_id) {
        ma_error_t rc = MA_OK;
              
		TEST_ASSERT_MESSAGE(MA_OK ==  ma_db_open( GLOBAL_DATABASE_NAME_FOR_UT, MA_DB_OPEN_READWRITE, &g_database_handle), "ma_db_open failed to open global database datatbase");
		TEST_ASSERT_MESSAGE(MA_OK ==  ma_ds_database_create(g_database_handle , "Task", (ma_ds_database_t **)&g_ds), "ma_ds_database_create failed to create the instance");
        if(MA_OK != (rc = ma_msgbus_create(product_id, &g_msgbus))) {   
            return rc;
        }

		if(MA_OK != (rc = ma_msgbus_start(g_msgbus))) {
			ma_msgbus_release(g_msgbus);
            return rc;
        }
		return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static ma_error_t ma_ut_release() {
    if(g_msgbus) {
		ma_error_t rc = MA_OK;

		TEST_ASSERT_MESSAGE(MA_OK ==  ma_db_close(g_database_handle), "ma_db_close failed to close the global datatbase");
		TEST_ASSERT_MESSAGE(MA_OK ==  ma_ds_database_release((ma_ds_database_t*)g_ds), "ma_ds_database failed to release the instance");
		unlink(GLOBAL_DATABASE_NAME_FOR_UT);
		if(MA_OK != (rc = ma_msgbus_stop(g_msgbus, MA_TRUE))) {   
			return rc;
		}

		if(MA_OK != (rc = ma_msgbus_release(g_msgbus))) {
			return rc;
		}
		return MA_OK;
	}
	return MA_ERROR_INVALIDARG;
    
}

TEST_SETUP(scheduler_service_test) {
   TEST_ASSERT_MESSAGE(MA_OK == ma_ut_create("scheduler_test"), "ma_ut_create failed to create the instance");
}

TEST_TEAR_DOWN(scheduler_service_test) {
  TEST_ASSERT_MESSAGE(MA_OK == ma_ut_release(), "ma_ut_release failed to release");
}



TEST(scheduler_service_test, conver_task_to_variant_vice_versa) {
    ma_task_t *task = NULL;
    ma_trigger_run_once_policy_t policy = {0};
    ma_trigger_t *run_once = NULL;
    ma_variant_t *task_variant = NULL;
    uv_loop_t *loop = uv_default_loop();
    ma_scheduler_t *scheduler = NULL;
    ma_task_time_t now = {0};
    ma_task_t *convert_task = NULL;

    TEST_ASSERT_MESSAGE(MA_OK == ma_scheduler_create(g_msgbus, g_ds, "/scheduler/", &scheduler), "schduler creation failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_scheduler_create_task(scheduler, "test", "1", __FUNCTION__, __FUNCTION__, &task), "task creation failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_trigger_create_run_once(&policy, &run_once), "run once trigger failed");
    get_localtime(&now);
    now.minute +=1;
    TEST_ASSERT_MESSAGE(MA_OK == ma_trigger_set_begin_date(run_once, &now), "setting begin date failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_task_add_trigger(task, run_once), "run once trigger addition failed");
    TEST_ASSERT_MESSAGE(MA_OK == convert_task_to_variant(task, &task_variant), "conversion task to variant failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_scheduler_create_task(scheduler, "test", "2", __FUNCTION__, __FUNCTION__, &convert_task), "task creation failed");
    TEST_ASSERT_MESSAGE(MA_OK == convert_variant_to_task(task_variant, convert_task), "conversion variant to task failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_variant_release(task_variant), "variant release failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_scheduler_start(scheduler), "scheduler start failed");
    uv_run(loop UV10_ONLY_ARGS);
    TEST_ASSERT_MESSAGE(MA_OK == ma_scheduler_release(scheduler), "scheduler release failed");
}

TEST(scheduler_service_test, convert_task_to_variant_vice_versa_with_run_once_and_daily_trigger) {
    ma_task_t *task = NULL;
    ma_trigger_run_once_policy_t policy = {0};
    ma_trigger_daily_policy_t daily_policy = {0};
    ma_trigger_t *run_once = NULL;
    ma_trigger_t *daily = NULL;
    ma_variant_t *task_variant = NULL;
    uv_loop_t *loop = uv_default_loop();
    ma_scheduler_t *scheduler = NULL;
    ma_task_time_t now = {0};
    ma_task_t *convert_task = NULL;

    TEST_ASSERT_MESSAGE(MA_OK == ma_scheduler_create(g_msgbus, g_ds, "/scheduler/", &scheduler), "schduler creation failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_scheduler_create_task(scheduler, "test", "1", __FUNCTION__, __FUNCTION__, &task), "task creation failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_trigger_create_run_once(&policy, &run_once), "run once trigger failed");
    daily_policy.days_interval = 2;
    TEST_ASSERT_MESSAGE(MA_OK == ma_trigger_create_daily(&daily_policy, &daily), "daily trigger failed");
    get_localtime(&now);
    now.minute +=1;
    TEST_ASSERT_MESSAGE(MA_OK == ma_trigger_set_begin_date(run_once, &now), "setting begin date failed");
    
    now.minute +=1;
    TEST_ASSERT_MESSAGE(MA_OK == ma_trigger_set_begin_date(daily, &now), "setting begin date failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_trigger_set_end_date(run_once, &now), "setting end date failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_trigger_set_end_date(daily, &now), "setting end date failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_task_add_trigger(task, run_once), "run once trigger addition failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_task_add_trigger(task, daily), "daily trigger addition failed");
    TEST_ASSERT_MESSAGE(MA_OK == convert_task_to_variant(task, &task_variant), "conversion task to variant failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_scheduler_create_task(scheduler, "test", "2", __FUNCTION__, __FUNCTION__, &convert_task), "task creation failed");
    TEST_ASSERT_MESSAGE(MA_OK == convert_variant_to_task(task_variant, convert_task), "conversion variant to task failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_variant_release(task_variant), "variant release failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_scheduler_start(scheduler), "scheduler start failed");
    uv_run(loop UV10_ONLY_ARGS);
    TEST_ASSERT_MESSAGE(MA_OK == ma_scheduler_release(scheduler), "scheduler release failed");
}


TEST(scheduler_service_test, convert_task_to_variant_vice_versa_with_missed_policy_set) {
    ma_task_t *task = NULL;
    ma_trigger_run_once_policy_t policy = {0};
    ma_trigger_t *run_once = NULL;
    ma_variant_t *task_variant = NULL;
    uv_loop_t *loop = uv_default_loop();
    ma_scheduler_t *scheduler = NULL;
    ma_task_time_t now = {0};
    ma_task_t *convert_task = NULL;
    ma_task_missed_policy_t missed = {{0}};

    TEST_ASSERT_MESSAGE(MA_OK == ma_scheduler_create(g_msgbus, g_ds, "/scheduler/", &scheduler), "schduler creation failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_scheduler_create_task(scheduler, "test", "1", __FUNCTION__, __FUNCTION__, &task), "task creation failed");
    missed.missed_duration.hour = 1;
    missed.missed_duration.minute = 30;
    TEST_ASSERT_MESSAGE(MA_OK == ma_task_set_missed_policy(task, missed), "setting missed policy failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_trigger_create_run_once(&policy, &run_once), "run once trigger failed");
    get_localtime(&now);
    now.minute +=1;
    TEST_ASSERT_MESSAGE(MA_OK == ma_trigger_set_begin_date(run_once, &now), "setting begin date failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_task_add_trigger(task, run_once), "run once trigger addition failed");
    TEST_ASSERT_MESSAGE(MA_OK == convert_task_to_variant(task, &task_variant), "conversion task to variant failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_scheduler_create_task(scheduler, "test", "2", __FUNCTION__, __FUNCTION__, &convert_task), "task creation failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_task_set_missed_policy(convert_task, missed), "setting missed policy failed");
    TEST_ASSERT_MESSAGE(MA_OK == convert_variant_to_task(task_variant, convert_task), "conversion variant to task failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_variant_release(task_variant), "variant release failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_scheduler_start(scheduler), "scheduler start failed");
    uv_run(loop UV10_ONLY_ARGS);
    TEST_ASSERT_MESSAGE(MA_OK == ma_scheduler_release(scheduler), "scheduler release failed");
}


TEST(scheduler_service_test, convert_task_to_variant_vice_versa_with_repeat_policy_set) {
    ma_task_t *task = NULL;
    ma_trigger_run_once_policy_t policy = {0};
    ma_trigger_t *run_once = NULL;
    ma_variant_t *task_variant = NULL;
    uv_loop_t *loop = uv_default_loop();
    ma_scheduler_t *scheduler = NULL;
    ma_task_time_t now = {0};
    ma_task_t *convert_task = NULL;
    ma_task_missed_policy_t missed = {{0}};
    ma_task_repeat_policy_t repeat = {{0}};

    TEST_ASSERT_MESSAGE(MA_OK == ma_scheduler_create(g_msgbus, g_ds, "/scheduler/", &scheduler), "schduler creation failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_scheduler_create_task(scheduler, "test", "1", __FUNCTION__, __FUNCTION__, &task), "task creation failed");
    missed.missed_duration.hour = 1;
    missed.missed_duration.minute = 30;
    repeat.repeat_interval.hour = 0;
    repeat.repeat_interval.minute = 1;
    repeat.repeat_until.hour = 0;
    repeat.repeat_until.minute = 3;
    TEST_ASSERT_MESSAGE(MA_OK == ma_task_set_missed_policy(task, missed), "setting missed policy failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_trigger_create_run_once(&policy, &run_once), "run once trigger failed");
    get_localtime(&now);
    now.minute +=1;
    TEST_ASSERT_MESSAGE(MA_OK == ma_trigger_set_begin_date(run_once, &now), "setting begin date failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_task_add_trigger(task, run_once), "run once trigger addition failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_task_set_repeat_policy(task, repeat), "setting repeat policy failed");
    TEST_ASSERT_MESSAGE(MA_OK == convert_task_to_variant(task, &task_variant), "conversion task to variant failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_scheduler_create_task(scheduler, "test", "2", __FUNCTION__, __FUNCTION__, &convert_task), "task creation failed");
    TEST_ASSERT_MESSAGE(MA_OK == convert_variant_to_task(task_variant, convert_task), "conversion variant to task failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_variant_release(task_variant), "variant release failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_scheduler_start(scheduler), "scheduler start failed");
    uv_run(loop UV10_ONLY_ARGS);
    TEST_ASSERT_MESSAGE(MA_OK == ma_scheduler_release(scheduler), "scheduler release failed");
}


TEST(scheduler_service_test, convert_task_to_variant_vice_versa_with_max_run_time_limit_set) {
    ma_task_t *task = NULL;
    ma_trigger_run_once_policy_t policy = {0};
    ma_trigger_t *run_once = NULL;
    ma_variant_t *task_variant = NULL;
    uv_loop_t *loop = uv_default_loop();
    ma_scheduler_t *scheduler = NULL;
    ma_task_time_t now = {0};
    ma_task_t *convert_task = NULL;
    ma_task_missed_policy_t missed = {{0}};
    ma_task_repeat_policy_t repeat = {{0}};


    TEST_ASSERT_MESSAGE(MA_OK == ma_scheduler_create(g_msgbus, g_ds, "/scheduler/", &scheduler), "schduler creation failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_scheduler_create_task(scheduler, "test", "1", __FUNCTION__, __FUNCTION__, &task), "task creation failed");
    missed.missed_duration.hour = 1;
    missed.missed_duration.minute = 30;
    repeat.repeat_interval.hour = 0;
    repeat.repeat_interval.minute = 1;
    repeat.repeat_until.hour = 0;
    repeat.repeat_until.minute = 3;
    TEST_ASSERT_MESSAGE(MA_OK == ma_task_set_missed_policy(task, missed), "setting missed policy failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_task_set_max_run_time_limit(task, 1), "setting max run time limit failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_trigger_create_run_once(&policy, &run_once), "run once trigger failed");
    get_localtime(&now);
    now.minute +=1;
    TEST_ASSERT_MESSAGE(MA_OK == ma_trigger_set_begin_date(run_once, &now), "setting begin date failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_task_add_trigger(task, run_once), "run once trigger addition failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_task_set_repeat_policy(task, repeat), "setting repeat policy failed");
    TEST_ASSERT_MESSAGE(MA_OK == convert_task_to_variant(task, &task_variant), "conversion task to variant failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_scheduler_create_task(scheduler, "test", "2", __FUNCTION__, __FUNCTION__, &convert_task), "task creation failed");
    TEST_ASSERT_MESSAGE(MA_OK == convert_variant_to_task(task_variant, convert_task), "conversion variant to task failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_variant_release(task_variant), "variant release failed");
    TEST_ASSERT_MESSAGE(MA_OK == ma_scheduler_start(scheduler), "scheduler start failed");
    uv_run(loop UV10_ONLY_ARGS);
    TEST_ASSERT_MESSAGE(MA_OK == ma_scheduler_release(scheduler), "scheduler release failed");
}

