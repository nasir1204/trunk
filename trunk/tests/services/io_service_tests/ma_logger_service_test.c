/*
Unit tests for Task Service APIs
*/
#include "io_service_tests.h"
#include "ma_logger_service.h"
#include "ma/internal/services/ma_service.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/ma_client.h"
#include "ma/internal/utils/configurator/ma_configurator_utils.h"
#include "ma/internal/services/ma_agent_policy_settings_bag.h"
#include "ma/logger/ma_file_logger.h"
#include "ma/internal/utils/platform/ma_system_property.h"
#include "ma/internal/clients/ma/ma_client_internal.h"
#include "ma/internal/services/scheduler/ma_scheduler_core.h"

#include "unity.h"
#include "unity_fixture.h"

#include <stdlib.h>

ma_service_t *logger_service = NULL;
ma_context_t *logger_service_context = NULL;

ma_datastore_configuration_request_t logger_service_datastore_required = {MA_TRUE, MA_TRUE, MA_TRUE, MA_TRUE, MA_TRUE};
ma_agent_configuration_t logger_service_agent_configuration = { 5, 10, 15, TEST_AGENT_GUID };

ma_configurator_t *logger_service_configurator = NULL;

ma_policy_settings_bag_t *logger_service_policy_settings_bag = NULL;
ma_logger_t *logger_service_logger = NULL;
ma_system_property_t *logger_service_system_property = NULL;
ma_msgbus_t *logger_service_msgbus = NULL;
ma_client_t *logger_service_client = NULL;
ma_policy_bag_t** logger_service_policy_bag = NULL;

TEST_GROUP_RUNNER(logger_service_tests_group) {
	do {RUN_TEST_CASE(logger_service_tests, verify_logger_service_start_stop)} while (0);
}

TEST_SETUP(logger_service_tests) {
	
	Unity.TestFile = "ma_logger_service_test.c";
	
	TEST_ASSERT(MA_OK == ma_context_create(&logger_service_context));

	TEST_ASSERT(MA_OK == ma_configurator_create(&logger_service_agent_configuration, &logger_service_configurator));
	TEST_ASSERT(MA_OK == ma_configurator_intialize_datastores(logger_service_configurator, &logger_service_datastore_required));
	TEST_ASSERT(MA_OK == ma_configurator_configure_agent_policies(logger_service_configurator));

	if(logger_service_policy_settings_bag == NULL)
	{
		TEST_ASSERT(MA_OK == ma_agent_policy_settings_bag_create(ma_configurator_get_policies_database(logger_service_configurator), &logger_service_policy_settings_bag));
	}

	TEST_ASSERT(MA_OK == ma_file_logger_create(NULL, "test_file_logger", ".log", (ma_file_logger_t **)&logger_service_logger));
	TEST_ASSERT(MA_OK == ma_system_property_create(&logger_service_system_property, logger_service_logger));
	TEST_ASSERT(MA_OK == ma_msgbus_create(MA_SOFTWAREID_GENERAL_STR, &logger_service_msgbus));
	TEST_ASSERT(MA_OK == ma_msgbus_set_logger(logger_service_msgbus, logger_service_logger));
	TEST_ASSERT(MA_OK == ma_client_create(MA_SOFTWAREID_GENERAL_STR, &logger_service_client));
	TEST_ASSERT(MA_OK == ma_client_set_msgbus(logger_service_client, logger_service_msgbus));
	TEST_ASSERT(MA_OK == ma_client_set_thread_option(logger_service_client, MA_MSGBUS_CALLBACK_THREAD_IO));

	TEST_ASSERT(MA_OK == ma_context_add_object_info(logger_service_context, MA_OBJECT_MA_CONFIGURATOR_NAME_STR, (void*const*)&logger_service_configurator));
	TEST_ASSERT(MA_OK == ma_context_add_object_info(logger_service_context, MA_OBJECT_CLIENT_NAME_STR, (void*const*)&logger_service_client));
	TEST_ASSERT(MA_OK == ma_context_add_object_info(logger_service_context, MA_OBJECT_POLICY_SETTINGS_BAG_STR, (void*const*)&logger_service_policy_settings_bag));
	TEST_ASSERT(MA_OK == ma_context_add_object_info(logger_service_context, MA_OBJECT_LOGGER_NAME_STR, (void*const*)&logger_service_logger));
	TEST_ASSERT(MA_OK == ma_context_add_object_info(logger_service_context, MA_OBJECT_SYSTEM_PROPERTY_STR, (void*const*)&logger_service_system_property));
	TEST_ASSERT(MA_OK == ma_context_add_object_info(logger_service_context, MA_OBJECT_MSGBUS_NAME_STR,(void*const*)&logger_service_msgbus));
	if(logger_service_policy_bag = ma_agent_policy_settings_bag_get_policy_bag(logger_service_policy_settings_bag))
		TEST_ASSERT(MA_OK == ma_context_add_object_info(logger_service_context, MA_OBJECT_POLICY_BAG_STR, (void*const*)logger_service_policy_bag));

	// FAIL -> TEST_ASSERT(MA_ERROR_INVALIDARG == ma_logger_service_create(NULL, &logger_service));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_logger_service_create("ma.service.logger.test", NULL));
	TEST_ASSERT(MA_OK == ma_logger_service_create("ma.service.logger.teust", &logger_service));
}

TEST_TEAR_DOWN(logger_service_tests) {
	if(logger_service_client) {
		ma_client_release(logger_service_client);
		logger_service_client = NULL;
	}
	if(logger_service_msgbus) {
		ma_msgbus_release(logger_service_msgbus);
		logger_service_msgbus = NULL;
	}
	if(logger_service_system_property) {
		ma_system_property_release(logger_service_system_property);
		logger_service_system_property = NULL;
	}
	if(logger_service_policy_settings_bag) {
		//ma_policy_settings_bag_release(logger_service_policy_settings_bag);
		//logger_service_policy_settings_bag = NULL;
	}
	if(logger_service_logger) {
		ma_logger_release(logger_service_logger);
		logger_service_logger = NULL;
	}
	if(logger_service_context) {
		ma_context_release(logger_service_context);
		logger_service_context = NULL;
	}
	if(logger_service) {
		ma_service_release(logger_service);
		logger_service = NULL;
	}
	if(logger_service_configurator) {
		ma_configurator_release(logger_service_configurator);
		logger_service_configurator = NULL;
	}
}

void get_context_without_configurator(ma_context_t **context)
{
	ma_context_t *temp;
	TEST_ASSERT(MA_OK == ma_context_create(&temp));

	TEST_ASSERT(MA_OK == ma_context_add_object_info(temp, MA_OBJECT_CLIENT_NAME_STR, (void*const*)&logger_service_client));
	TEST_ASSERT(MA_OK == ma_context_add_object_info(temp, MA_OBJECT_POLICY_SETTINGS_BAG_STR, (void*const*)&logger_service_policy_settings_bag));
	TEST_ASSERT(MA_OK == ma_context_add_object_info(temp, MA_OBJECT_LOGGER_NAME_STR, (void*const*)&logger_service_logger));
	TEST_ASSERT(MA_OK == ma_context_add_object_info(temp, MA_OBJECT_SYSTEM_PROPERTY_STR, (void*const*)&logger_service_system_property));
	TEST_ASSERT(MA_OK == ma_context_add_object_info(temp, MA_OBJECT_MSGBUS_NAME_STR,(void*const*)&logger_service_msgbus));
	if(logger_service_policy_bag = ma_agent_policy_settings_bag_get_policy_bag(logger_service_policy_settings_bag))
		TEST_ASSERT(MA_OK == ma_context_add_object_info(temp, MA_OBJECT_POLICY_BAG_STR, (void*const*)logger_service_policy_bag));

	*context = temp;
}

void get_context_without_policy_settings_bag(ma_context_t **context)
{
	ma_context_t *temp;
	TEST_ASSERT(MA_OK == ma_context_create(&temp));

	TEST_ASSERT(MA_OK == ma_context_add_object_info(temp, MA_OBJECT_MA_CONFIGURATOR_NAME_STR, (void*const*)&logger_service_configurator));
	TEST_ASSERT(MA_OK == ma_context_add_object_info(temp, MA_OBJECT_CLIENT_NAME_STR, (void*const*)&logger_service_client));
	TEST_ASSERT(MA_OK == ma_context_add_object_info(temp, MA_OBJECT_LOGGER_NAME_STR, (void*const*)&logger_service_logger));
	TEST_ASSERT(MA_OK == ma_context_add_object_info(temp, MA_OBJECT_SYSTEM_PROPERTY_STR, (void*const*)&logger_service_system_property));
	TEST_ASSERT(MA_OK == ma_context_add_object_info(temp, MA_OBJECT_MSGBUS_NAME_STR,(void*const*)&logger_service_msgbus));
	
	*context = temp;
}

void get_context_without_logger(ma_context_t **context)
{
	ma_context_t *temp;
	TEST_ASSERT(MA_OK == ma_context_create(&temp));

	TEST_ASSERT(MA_OK == ma_context_add_object_info(temp, MA_OBJECT_MA_CONFIGURATOR_NAME_STR, (void*const*)&logger_service_configurator));
	TEST_ASSERT(MA_OK == ma_context_add_object_info(temp, MA_OBJECT_CLIENT_NAME_STR, (void*const*)&logger_service_client));
	TEST_ASSERT(MA_OK == ma_context_add_object_info(temp, MA_OBJECT_SYSTEM_PROPERTY_STR, (void*const*)&logger_service_system_property));
	TEST_ASSERT(MA_OK == ma_context_add_object_info(temp, MA_OBJECT_MSGBUS_NAME_STR,(void*const*)&logger_service_msgbus));
	if(logger_service_policy_bag = ma_agent_policy_settings_bag_get_policy_bag(logger_service_policy_settings_bag))
		TEST_ASSERT(MA_OK == ma_context_add_object_info(temp, MA_OBJECT_POLICY_BAG_STR, (void*const*)logger_service_policy_bag));
	
	*context = temp;
}

void get_context_without_system_property(ma_context_t **context)
{
	ma_context_t *temp;
	TEST_ASSERT(MA_OK == ma_context_create(&temp));

	TEST_ASSERT(MA_OK == ma_context_add_object_info(temp, MA_OBJECT_MA_CONFIGURATOR_NAME_STR, (void*const*)&logger_service_configurator));
	TEST_ASSERT(MA_OK == ma_context_add_object_info(temp, MA_OBJECT_CLIENT_NAME_STR, (void*const*)&logger_service_client));
	TEST_ASSERT(MA_OK == ma_context_add_object_info(temp, MA_OBJECT_LOGGER_NAME_STR, (void*const*)&logger_service_logger));
	TEST_ASSERT(MA_OK == ma_context_add_object_info(temp, MA_OBJECT_MSGBUS_NAME_STR,(void*const*)&logger_service_msgbus));
	if(logger_service_policy_bag = ma_agent_policy_settings_bag_get_policy_bag(logger_service_policy_settings_bag))
		TEST_ASSERT(MA_OK == ma_context_add_object_info(temp, MA_OBJECT_POLICY_BAG_STR, (void*const*)logger_service_policy_bag));
	
	*context = temp;
}

TEST(logger_service_tests, verify_logger_service_start_stop){
	ma_context_t *temp_context;
	TEST_ASSERT(MA_ERROR_INVALIDARG == logger_service->methods->configure(NULL, logger_service_context, MA_SERVICE_CONFIG_NEW));
	TEST_ASSERT(MA_ERROR_INVALIDARG == logger_service->methods->configure(NULL, logger_service_context, MA_SERVICE_CONFIG_MODIFIED));

	TEST_ASSERT(MA_ERROR_INVALIDARG == logger_service->methods->configure(logger_service, NULL, MA_SERVICE_CONFIG_NEW));
	TEST_ASSERT(MA_ERROR_INVALIDARG == logger_service->methods->configure(logger_service, NULL, MA_SERVICE_CONFIG_MODIFIED));
	
	get_context_without_configurator(&temp_context);
	// FAIL -> TEST_ASSERT(MA_ERROR_PRECONDITION == logger_service->methods->configure(logger_service, temp_context, MA_SERVICE_CONFIG_NEW));
	// FAIL -> TEST_ASSERT(MA_ERROR_PRECONDITION == logger_service->methods->configure(logger_service, temp_context, MA_SERVICE_CONFIG_MODIFIED));
	ma_context_release(temp_context);

	get_context_without_logger(&temp_context);
	TEST_ASSERT(MA_ERROR_PRECONDITION == logger_service->methods->configure(logger_service, temp_context, MA_SERVICE_CONFIG_NEW));
	TEST_ASSERT(MA_ERROR_PRECONDITION == logger_service->methods->configure(logger_service, temp_context, MA_SERVICE_CONFIG_MODIFIED));
	ma_context_release(temp_context);

	get_context_without_policy_settings_bag(&temp_context);
	TEST_ASSERT(MA_ERROR_PRECONDITION == logger_service->methods->configure(logger_service, temp_context, MA_SERVICE_CONFIG_NEW));
	TEST_ASSERT(MA_ERROR_PRECONDITION == logger_service->methods->configure(logger_service, temp_context, MA_SERVICE_CONFIG_MODIFIED));
	ma_context_release(temp_context);

	get_context_without_system_property(&temp_context);
	TEST_ASSERT(MA_ERROR_PRECONDITION == logger_service->methods->configure(logger_service, temp_context, MA_SERVICE_CONFIG_NEW));
	TEST_ASSERT(MA_ERROR_PRECONDITION == logger_service->methods->configure(logger_service, temp_context, MA_SERVICE_CONFIG_MODIFIED));
	ma_context_release(temp_context);

	TEST_ASSERT(MA_OK == logger_service->methods->configure(logger_service, logger_service_context, MA_SERVICE_CONFIG_NEW));	
	TEST_ASSERT(MA_OK == logger_service->methods->configure(logger_service, logger_service_context, MA_SERVICE_CONFIG_MODIFIED));

	TEST_ASSERT(MA_ERROR_INVALIDARG == logger_service->methods->start(NULL));
	TEST_ASSERT(MA_OK == logger_service->methods->start(logger_service));
	//FAIL-> TEST_ASSERT(MA_ERROR_INVALIDARG == logger_service->methods->stop(NULL));
	TEST_ASSERT(MA_OK == logger_service->methods->stop(logger_service));
}