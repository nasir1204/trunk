/*
Unit tests for Task Service Handler APIs
*/

#include "io_service_tests.h"
#include "ma/internal/services/io/ma_task_service.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/ma_client.h"
#include "ma/internal/utils/configurator/ma_configurator_utils.h"
#include "ma/internal/services/ma_agent_policy_settings_bag.h"
#include "ma/logger/ma_file_logger.h"
#include "ma/internal/utils/platform/ma_system_property.h"
#include "ma/internal/clients/ma/ma_client_internal.h"
#include "ma/internal/services/scheduler/ma_scheduler_core.h"

#include "unity.h"
#include "unity_fixture.h"

#include <stdlib.h>

const char *ma_task_service_handler_test_service_name = "ma.service.task";
const char *ma_task_service_handler_test_task_version = "2014-1-1T10:00:00";

ma_service_t *ma_task_service_handler_test_service = NULL;
ma_context_t *ma_task_service_handler_test_context = NULL;

ma_datastore_configuration_request_t ma_task_service_handler_test_datastore_required = {MA_TRUE, MA_TRUE, MA_TRUE, MA_TRUE, MA_TRUE};
ma_agent_configuration_t ma_task_service_handler_test_agent_configuration = { 5, 10, 15, TEST_AGENT_GUID };

ma_logger_t *ma_task_service_handler_test_logger = NULL;
ma_configurator_t *ma_task_service_handler_test_configurator = NULL;
ma_policy_settings_bag_t *ma_task_service_handler_test_policy_settings_bag = NULL;
ma_msgbus_t *ma_task_service_handler_test_msgbus = NULL;
ma_scheduler_t *ma_task_service_handler_test_scheduler = NULL;

TEST_GROUP_RUNNER(ma_task_service_handler_tests_group) {
    do {RUN_TEST_CASE(ma_task_service_handler_tests, task_service_hanler_epo_task_tests)} while (0);
}

void ma_task_service_handler_test_init_context_object_infos(){
	if(ma_task_service_handler_test_logger == NULL)	{
		TEST_ASSERT(MA_OK == ma_file_logger_create(NULL,"test_file_logger",".log", (ma_file_logger_t **) &ma_task_service_handler_test_logger));	
	}	
	if(ma_task_service_handler_test_configurator == NULL){
		TEST_ASSERT(MA_OK == ma_configurator_create(&ma_task_service_handler_test_agent_configuration, &ma_task_service_handler_test_configurator));
		TEST_ASSERT(MA_OK == ma_configurator_intialize_datastores(ma_task_service_handler_test_configurator, &ma_task_service_handler_test_datastore_required));
		TEST_ASSERT(MA_OK == ma_configurator_configure_agent_policies(ma_task_service_handler_test_configurator));
	}
	if(ma_task_service_handler_test_policy_settings_bag == NULL){
		TEST_ASSERT(MA_OK == ma_agent_policy_settings_bag_create(ma_configurator_get_policies_database(ma_task_service_handler_test_configurator), &ma_task_service_handler_test_policy_settings_bag));
	}
	if(ma_task_service_handler_test_msgbus == NULL){
		TEST_ASSERT(MA_OK == ma_msgbus_create(MA_SOFTWAREID_GENERAL_STR, &ma_task_service_handler_test_msgbus));
		TEST_ASSERT(MA_OK == ma_msgbus_set_logger(ma_task_service_handler_test_msgbus, ma_task_service_handler_test_logger));
	}
	if(ma_task_service_handler_test_scheduler == NULL){
		TEST_ASSERT(MA_OK == ma_scheduler_create(ma_task_service_handler_test_msgbus, ma_configurator_get_scheduler_datastore(ma_task_service_handler_test_configurator), "scheduler", &ma_task_service_handler_test_scheduler));
	}
}

void ma_task_service_handler_test_deinit_context_object_infos()
{	
	if(ma_task_service_handler_test_configurator != NULL){
		ma_task_service_handler_test_configurator->methods->configurator_destroy(ma_task_service_handler_test_configurator);
		ma_task_service_handler_test_configurator = NULL;
	}
	if(ma_task_service_handler_test_policy_settings_bag != NULL){
		ma_task_service_handler_test_policy_settings_bag->methods->destroy(ma_task_service_handler_test_policy_settings_bag);
		ma_task_service_handler_test_policy_settings_bag = NULL;
	}
	if(ma_task_service_handler_test_msgbus != NULL){
		TEST_ASSERT(MA_OK == ma_msgbus_release(ma_task_service_handler_test_msgbus));
		ma_task_service_handler_test_msgbus = NULL;
	}
	if(ma_task_service_handler_test_scheduler != NULL){
		TEST_ASSERT(MA_OK == ma_scheduler_release(ma_task_service_handler_test_scheduler));
		ma_task_service_handler_test_scheduler = NULL;
	}
	if(ma_task_service_handler_test_logger != NULL){
		TEST_ASSERT(MA_OK == ma_file_logger_release((ma_file_logger_t *) ma_task_service_handler_test_logger));	
		ma_task_service_handler_test_logger = NULL;
	}
}

void ma_task_service_handler_test_create_context(){	
	TEST_ASSERT(MA_OK == ma_context_create(&ma_task_service_handler_test_context));

	TEST_ASSERT(MA_OK == ma_context_add_object_info(ma_task_service_handler_test_context, MA_OBJECT_MA_CONFIGURATOR_NAME_STR, (void*const*)&ma_task_service_handler_test_configurator));
    TEST_ASSERT(MA_OK == ma_context_add_object_info(ma_task_service_handler_test_context, MA_OBJECT_POLICY_SETTINGS_BAG_STR, (void*const*)&ma_task_service_handler_test_policy_settings_bag));
    TEST_ASSERT(MA_OK == ma_context_add_object_info(ma_task_service_handler_test_context, MA_OBJECT_LOGGER_NAME_STR, (void*const*)&ma_task_service_handler_test_logger)); 
    TEST_ASSERT(MA_OK == ma_context_add_object_info(ma_task_service_handler_test_context, MA_OBJECT_MSGBUS_NAME_STR, (void*const*)&ma_task_service_handler_test_msgbus));        
	TEST_ASSERT(MA_OK == ma_context_add_object_info(ma_task_service_handler_test_context, MA_OBJECT_SCHEDULER_NAME_STR, (void*const*)&ma_task_service_handler_test_scheduler));
}

void ma_task_service_handler_test_release_context(){
	if(ma_task_service_handler_test_context != NULL){
		ma_context_release(ma_task_service_handler_test_context);
		ma_task_service_handler_test_context = NULL;
	}
}

TEST_SETUP(ma_task_service_handler_tests) {
	Unity.TestFile = "ma_task_service_handler_test.c";
	unlink("test_file_logger.log");	
	ma_task_service_handler_test_init_context_object_infos();
	ma_task_service_handler_test_create_context();
}

TEST_TEAR_DOWN(ma_task_service_handler_tests) {
	ma_task_service_handler_test_release_context();
	ma_task_service_handler_test_deinit_context_object_infos();	
}


TEST(ma_task_service_handler_tests, task_service_hanler_epo_task_tests){	
	TEST_ASSERT(MA_OK == ma_task_service_create(ma_task_service_handler_test_service_name, &ma_task_service_handler_test_service));
	TEST_ASSERT(ma_task_service_handler_test_service != NULL);
	TEST_ASSERT(MA_OK == ma_task_service_handler_test_service->methods->configure(ma_task_service_handler_test_service, ma_task_service_handler_test_context, MA_SERVICE_CONFIG_NEW));

	//FAIL-> TEST_ASSERT(MA_ERROR_INVALIDARG == handle_epo_tasks(NULL, ma_task_service_handler_test_task_version));
	//FAIL-> TEST_ASSERT(MA_ERROR_INVALIDARG == handle_epo_tasks((ma_task_service_t*)ma_task_service_handler_test_service, NULL));
	
	TEST_ASSERT(MA_OK == handle_epo_tasks((ma_task_service_t*)ma_task_service_handler_test_service, ma_task_service_handler_test_task_version));
	ma_task_service_handler_test_service->methods->destroy((ma_task_service_t*)ma_task_service_handler_test_service);
}

