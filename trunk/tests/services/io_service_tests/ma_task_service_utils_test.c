/*
Unit tests for Task Service utils APIs
*/

#include "io_service_tests.h"
#include "ma/internal/services/io/ma_task_service.h"
#include "ma/internal/services/io/ma_task_service_utils.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/services/ma_agent_policy_settings_bag.h"
#include "ma/internal/services/scheduler/ma_scheduler_core.h"
#include "ma/internal/utils/configurator/ma_configurator_utils.h"
#include "ma/logger/ma_file_logger.h"


#include "unity.h"
#include "unity_fixture.h"

const char *ma_task_service_utils_test_service_name = "ma.service.task";

ma_service_t *ma_task_service_utils_test_service = NULL;
ma_context_t *ma_task_service_utils_test_context = NULL;

ma_datastore_configuration_request_t ma_task_service_utils_test_datastore_required = {MA_TRUE, MA_TRUE, MA_TRUE, MA_TRUE, MA_TRUE};
ma_agent_configuration_t ma_task_service_utils_test_agent_configuration = { 5, 10, 15, TEST_AGENT_GUID };

ma_logger_t *ma_task_service_utils_test_logger = NULL;
ma_configurator_t *ma_task_service_utils_test_configurator = NULL;
ma_policy_settings_bag_t *ma_task_service_utils_test_policy_settings_bag = NULL;
ma_msgbus_t * ma_task_service_utils_test_msgbus = NULL;
ma_scheduler_t *ma_task_service_utils_test_scheduler = NULL;

TEST_GROUP_RUNNER(ma_task_service_utils_tests_group) {
	do {RUN_TEST_CASE(ma_task_service_utils_tests, ma_task_service_task_update_tests)} while (0);
	do {RUN_TEST_CASE(ma_task_service_utils_tests, ma_task_service_task_set_status_tests)} while (0);
	do {RUN_TEST_CASE(ma_task_service_utils_tests, ma_task_service_task_start_stop_tests)} while (0);
}

void ma_task_service_utils_test_init_context_object_infos(){
	if(ma_task_service_utils_test_logger == NULL)	{
		TEST_ASSERT(MA_OK == ma_file_logger_create(NULL,"test_file_logger",".log", (ma_file_logger_t **) &ma_task_service_utils_test_logger));	
	}	
	if(ma_task_service_utils_test_configurator == NULL){
		TEST_ASSERT(MA_OK == ma_configurator_create(&ma_task_service_utils_test_agent_configuration, &ma_task_service_utils_test_configurator));
		TEST_ASSERT(MA_OK == ma_configurator_intialize_datastores(ma_task_service_utils_test_configurator, &ma_task_service_utils_test_datastore_required));
		TEST_ASSERT(MA_OK == ma_configurator_configure_agent_policies(ma_task_service_utils_test_configurator));
	}
	if(ma_task_service_utils_test_policy_settings_bag == NULL){
		TEST_ASSERT(MA_OK == ma_agent_policy_settings_bag_create(ma_configurator_get_policies_database(ma_task_service_utils_test_configurator), &ma_task_service_utils_test_policy_settings_bag));
	}
	if(ma_task_service_utils_test_msgbus == NULL){
		TEST_ASSERT(MA_OK == ma_msgbus_create(MA_SOFTWAREID_GENERAL_STR, &ma_task_service_utils_test_msgbus));
		TEST_ASSERT(MA_OK == ma_msgbus_set_logger(ma_task_service_utils_test_msgbus, ma_task_service_utils_test_logger));
	}
	if(ma_task_service_utils_test_scheduler == NULL){
		TEST_ASSERT(MA_OK == ma_scheduler_create(ma_task_service_utils_test_msgbus, ma_configurator_get_scheduler_datastore(ma_task_service_utils_test_configurator), "scheduler", &ma_task_service_utils_test_scheduler));
	}
}

void ma_task_service_utils_test_deinit_context_object_infos()
{	
	if(ma_task_service_utils_test_configurator) {
		//ma_configurator_release(ma_task_service_utils_test_configurator);
		//ma_task_service_utils_test_configurator = NULL;
	}
	if(ma_task_service_utils_test_policy_settings_bag) {
		//ma_policy_settings_bag_release(ma_task_service_utils_test_policy_settings_bag);
		//ma_task_service_utils_test_policy_settings_bag = NULL;
	}
	if(ma_task_service_utils_test_msgbus != NULL){
		//TEST_ASSERT(MA_OK == ma_msgbus_release(ma_task_service_utils_test_msgbus));
		//ma_task_service_utils_test_msgbus = NULL;
	}
	if(ma_task_service_utils_test_scheduler != NULL){
		TEST_ASSERT(MA_OK == ma_scheduler_release(ma_task_service_utils_test_scheduler));
		ma_task_service_utils_test_scheduler = NULL;
	}
	if(ma_task_service_utils_test_logger != NULL){
		TEST_ASSERT(MA_OK == ma_file_logger_release((ma_file_logger_t *) ma_task_service_utils_test_logger));	
		ma_task_service_utils_test_logger = NULL;
	}
}

void ma_task_service_utils_test_create_context(){	
	TEST_ASSERT(MA_OK == ma_context_create(&ma_task_service_utils_test_context));

	TEST_ASSERT(MA_OK == ma_context_add_object_info(ma_task_service_utils_test_context, MA_OBJECT_MA_CONFIGURATOR_NAME_STR, (void*const*)&ma_task_service_utils_test_configurator));
    TEST_ASSERT(MA_OK == ma_context_add_object_info(ma_task_service_utils_test_context, MA_OBJECT_POLICY_SETTINGS_BAG_STR, (void*const*)&ma_task_service_utils_test_policy_settings_bag));
    TEST_ASSERT(MA_OK == ma_context_add_object_info(ma_task_service_utils_test_context, MA_OBJECT_LOGGER_NAME_STR, (void*const*)&ma_task_service_utils_test_logger)); 
    TEST_ASSERT(MA_OK == ma_context_add_object_info(ma_task_service_utils_test_context, MA_OBJECT_MSGBUS_NAME_STR, (void*const*)&ma_task_service_utils_test_msgbus));        
	TEST_ASSERT(MA_OK == ma_context_add_object_info(ma_task_service_utils_test_context, MA_OBJECT_SCHEDULER_NAME_STR, (void*const*)&ma_task_service_utils_test_scheduler));
}

void ma_task_service_utils_test_release_context(){
	if(ma_task_service_utils_test_context != NULL){
		ma_context_release(ma_task_service_utils_test_context);
		ma_task_service_utils_test_context = NULL;
	}
}

TEST_SETUP(ma_task_service_utils_tests) {
	Unity.TestFile = "ma_task_service_utils_test.c";
	ma_task_service_utils_test_init_context_object_infos();
	ma_task_service_utils_test_create_context();
	
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_task_service_task_create(NULL, MA_PROPERTY_COLLECT_TASK_ID_STR, MA_PROPERTY_COLLECT_TASK_NAME_STR, MA_PROPERTY_COLLECT_TASK_TYPE_STR, MA_TASK_SERVICE_CREATOR_NAME_STR, 10, 30));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_task_service_task_create(ma_task_service_utils_test_context, NULL, MA_PROPERTY_COLLECT_TASK_NAME_STR, MA_PROPERTY_COLLECT_TASK_TYPE_STR, MA_TASK_SERVICE_CREATOR_NAME_STR, 10, 30));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_task_service_task_create(ma_task_service_utils_test_context, MA_PROPERTY_COLLECT_TASK_ID_STR, NULL, MA_PROPERTY_COLLECT_TASK_TYPE_STR, MA_TASK_SERVICE_CREATOR_NAME_STR, 10, 30));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_task_service_task_create(ma_task_service_utils_test_context, MA_PROPERTY_COLLECT_TASK_ID_STR, MA_PROPERTY_COLLECT_TASK_NAME_STR, NULL, MA_TASK_SERVICE_CREATOR_NAME_STR, 10, 30));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_task_service_task_create(ma_task_service_utils_test_context, MA_PROPERTY_COLLECT_TASK_ID_STR, MA_PROPERTY_COLLECT_TASK_NAME_STR, MA_PROPERTY_COLLECT_TASK_TYPE_STR, NULL, 10, 30));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_task_service_task_create(ma_task_service_utils_test_context, MA_PROPERTY_COLLECT_TASK_ID_STR, MA_PROPERTY_COLLECT_TASK_NAME_STR, MA_PROPERTY_COLLECT_TASK_TYPE_STR, MA_TASK_SERVICE_CREATOR_NAME_STR, -1, 30));
	// FAIL -> TEST_ASSERT(MA_ERROR_INVALIDARG == ma_task_service_task_create(ma_task_service_utils_test_context, MA_PROPERTY_COLLECT_TASK_ID_STR, MA_PROPERTY_COLLECT_TASK_NAME_STR, MA_PROPERTY_COLLECT_TASK_TYPE_STR, MA_TASK_SERVICE_CREATOR_NAME_STR, 10, -1));

	// FAIL -> TEST_ASSERT(MA_OK == ma_task_service_task_create(ma_task_service_utils_test_context, MA_PROPERTY_COLLECT_TASK_ID_STR, MA_PROPERTY_COLLECT_TASK_NAME_STR, MA_PROPERTY_COLLECT_TASK_TYPE_STR, MA_TASK_SERVICE_CREATOR_NAME_STR, 0, 30));
	// FAIL ->(Crash in debug) TEST_ASSERT(MA_OK == ma_task_service_task_create(ma_task_service_utils_test_context, MA_PROPERTY_COLLECT_TASK_ID_STR, MA_PROPERTY_COLLECT_TASK_NAME_STR, MA_PROPERTY_COLLECT_TASK_TYPE_STR, MA_TASK_SERVICE_CREATOR_NAME_STR, 20, 0));

	TEST_ASSERT(MA_OK == ma_task_service_task_create(ma_task_service_utils_test_context, MA_PROPERTY_COLLECT_TASK_ID_STR, MA_PROPERTY_COLLECT_TASK_NAME_STR, MA_PROPERTY_COLLECT_TASK_TYPE_STR, MA_TASK_SERVICE_CREATOR_NAME_STR, 10, 30));
}

TEST_TEAR_DOWN(ma_task_service_utils_tests) {
	ma_task_service_utils_test_release_context();
}

TEST(ma_task_service_utils_tests, ma_task_service_task_update_tests){

	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_task_service_task_update(NULL, MA_PROPERTY_COLLECT_TASK_ID_STR, MA_PROPERTY_COLLECT_TASK_NAME_STR, MA_PROPERTY_COLLECT_TASK_TYPE_STR, 10));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_task_service_task_update(ma_task_service_utils_test_context, NULL, MA_PROPERTY_COLLECT_TASK_NAME_STR, MA_PROPERTY_COLLECT_TASK_TYPE_STR, 10));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_task_service_task_update(ma_task_service_utils_test_context, MA_PROPERTY_COLLECT_TASK_ID_STR, NULL, MA_PROPERTY_COLLECT_TASK_TYPE_STR, 10));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_task_service_task_update(ma_task_service_utils_test_context, MA_PROPERTY_COLLECT_TASK_ID_STR, MA_PROPERTY_COLLECT_TASK_NAME_STR, NULL, 10));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_task_service_task_update(ma_task_service_utils_test_context, MA_PROPERTY_COLLECT_TASK_ID_STR, MA_PROPERTY_COLLECT_TASK_NAME_STR, MA_PROPERTY_COLLECT_TASK_TYPE_STR, -1));

	// FAIL -> TEST_ASSERT(MA_OK == ma_task_service_task_update(ma_task_service_utils_test_context, MA_PROPERTY_COLLECT_TASK_ID_STR, MA_PROPERTY_COLLECT_TASK_NAME_STR, MA_PROPERTY_COLLECT_TASK_TYPE_STR, 0));
	// FAIL -> TEST_ASSERT(MA_OK == ma_task_service_task_update(ma_task_service_utils_test_context, MA_PROPERTY_COLLECT_TASK_ID_STR, MA_PROPERTY_COLLECT_TASK_NAME_STR, MA_PROPERTY_COLLECT_TASK_TYPE_STR, 10));
}

TEST(ma_task_service_utils_tests, ma_task_service_task_set_status_tests){

	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_task_service_task_set_status(NULL, MA_PROPERTY_COLLECT_TASK_ID_STR, MA_TASK_EXEC_RUNNING));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_task_service_task_set_status(ma_task_service_utils_test_context, NULL, MA_TASK_EXEC_RUNNING));
	TEST_ASSERT(MA_OK == ma_task_service_task_set_status(ma_task_service_utils_test_context, MA_PROPERTY_COLLECT_TASK_ID_STR, MA_TASK_EXEC_RUNNING));
}

TEST(ma_task_service_utils_tests, ma_task_service_task_start_stop_tests){
	
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_task_service_task_start(NULL, MA_PROPERTY_COLLECT_TASK_ID_STR, MA_PROPERTY_COLLECT_TASK_NAME_STR, MA_PROPERTY_COLLECT_TASK_TYPE_STR));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_task_service_task_start(ma_task_service_utils_test_context, NULL, MA_PROPERTY_COLLECT_TASK_NAME_STR, MA_PROPERTY_COLLECT_TASK_TYPE_STR));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_task_service_task_start(ma_task_service_utils_test_context, MA_PROPERTY_COLLECT_TASK_ID_STR, NULL, MA_PROPERTY_COLLECT_TASK_TYPE_STR));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_task_service_task_start(ma_task_service_utils_test_context, MA_PROPERTY_COLLECT_TASK_ID_STR, MA_PROPERTY_COLLECT_TASK_NAME_STR, NULL));
	
	// FAIL-> TEST_ASSERT(MA_OK == ma_task_service_task_start(ma_task_service_utils_test_context, MA_PROPERTY_COLLECT_TASK_ID_STR, MA_PROPERTY_COLLECT_TASK_NAME_STR, MA_PROPERTY_COLLECT_TASK_TYPE_STR));	

	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_task_service_task_stop(NULL, MA_PROPERTY_COLLECT_TASK_ID_STR, MA_PROPERTY_COLLECT_TASK_NAME_STR, MA_PROPERTY_COLLECT_TASK_TYPE_STR));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_task_service_task_stop(ma_task_service_utils_test_context, NULL, MA_PROPERTY_COLLECT_TASK_NAME_STR, MA_PROPERTY_COLLECT_TASK_TYPE_STR));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_task_service_task_stop(ma_task_service_utils_test_context, MA_PROPERTY_COLLECT_TASK_ID_STR, NULL, MA_PROPERTY_COLLECT_TASK_TYPE_STR));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_task_service_task_stop(ma_task_service_utils_test_context, MA_PROPERTY_COLLECT_TASK_ID_STR, MA_PROPERTY_COLLECT_TASK_NAME_STR, NULL));

	// FAIL-> TEST_ASSERT(MA_OK == ma_task_service_task_stop(ma_task_service_utils_test_context, MA_PROPERTY_COLLECT_TASK_ID_STR, MA_PROPERTY_COLLECT_TASK_NAME_STR, MA_PROPERTY_COLLECT_TASK_TYPE_STR));

	ma_task_service_utils_test_deinit_context_object_infos();
}