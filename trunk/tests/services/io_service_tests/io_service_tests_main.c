#include "unity.h"
#include "unity_fixture.h"
#include "ma_application_internal.h"
#include "ma/internal/utils/app_info/ma_application.h"
#include "ma/internal/ma_macros.h"
#include <stdlib.h>
#include <stdio.h>
#include "io_service_tests.h"

#ifdef MA_WINDOWS
	#include "ma/datastore/ma_ds_registry.h"
    #include <windows.h>
#else
#endif

#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include "ma/internal/utils/filesystem/path.h"

#ifdef _MSC_VER
# include <crtdbg.h> /* */
# define MA_TRACKLEAKS _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF)
#else
# define MA_TRACKLEAKS ((void *)0)
#endif

#ifdef _WIN64
#define MA_AGENT_BIT_AFFINITY 2
#elif _WIN32
#define MA_AGENT_BIT_AFFINITY 1
#else
#endif

static void run_all_tests(void) {
    do { RUN_TEST_GROUP(ma_task_service_handler_tests_group); } while (0);
	//do { RUN_TEST_GROUP(ma_task_service_utils_tests_group); } while (0);
	do { RUN_TEST_GROUP(ma_task_service_tests_group); } while (0);
	do { RUN_TEST_GROUP(logger_service_tests_group); } while (0);
}

ma_bool_t isSystem64Bit() {
#ifdef MA_WINDOWS

    UINT size = 0;
    HMODULE hm_k32 = (HMODULE) 0;
    FARPROC proc;
	TCHAR tmp[1];
	OSVERSIONINFOW g_osvi = {0};
	ma_bool_t ret = MA_FALSE;

	g_osvi.dwOSVersionInfoSize = sizeof( OSVERSIONINFO );
    GetVersionEx(&g_osvi);
    // GetSystemWow64Directory() requires at least Win XP or Win 2003 Server
    // and 64-bit is potentially only on at least XP.
    if (g_osvi.dwMajorVersion >= 4 || (g_osvi.dwMajorVersion >= 3 && g_osvi.dwMinorVersion >= 1))
    {
        // Since delay loading Kernel32.dll is not supported, we need to
        // check for the presence of GetSystemWow64Directory manually.
        hm_k32 = LoadLibrary(L"Kernel32.dll");
        if (hm_k32)
        {
            proc = GetProcAddress(hm_k32, "GetSystemWow64DirectoryW");
            if (proc)
            {
                // GetSystemWow64Directory always fails on 32 bit operating systems.
                // It returns 0 on failure or the length, so we must give it a chance
                // to copy at least 1 character into the buffer so that we can dif-
                // ferentiate between an error code and a size of 0.
                size = ((UINT (__stdcall *)(LPTSTR, UINT)) proc)(tmp, 1);
                if (size > 0)
                    ret = MA_TRUE;
            }
            FreeLibrary(hm_k32);
        }
    }
    return ret;
#endif
	return MA_FALSE;
}

static void fill_agent_information(ma_ds_t *ds) {
	ma_ds_set_str(ds, MA_REGISTRY_STORE_AGENT_SCAN_PATH, MA_APPLICATION_INFO_DATA_DIR, TEST_AGENT_DATA_PATH, strlen(TEST_AGENT_DATA_PATH));
	ma_ds_set_str(ds, MA_REGISTRY_STORE_AGENT_SCAN_PATH, MA_APPLICATION_INFO_INSTALL_DIR, TEST_AGENT_INSTALL_PATH, strlen(TEST_AGENT_INSTALL_PATH));
}

static void create_application_list_in_x64_view(ma_ds_t *ds) {
	/*Created First APP*/
	ma_temp_buffer_t buffer;
	ma_temp_buffer_init(&buffer);

	form_path(&buffer, MA_REGISTRY_STORE_AGENT_SCAN_PATH, "Applications", MA_TRUE);
	form_path(&buffer, (char *) ma_temp_buffer_get(&buffer), "xApplication_1", MA_TRUE);
	ma_ds_set_str(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SOFTWARE_ID, "xApplication_1", strlen("xApplication_1"));
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_DATACHANNEL_CONSUMER, 1);
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_EVENT_GENERATOR, 1);

	form_path(&buffer, MA_REGISTRY_STORE_AGENT_SCAN_PATH, "Applications", MA_TRUE);
	form_path(&buffer, (char *) ma_temp_buffer_get(&buffer), "xApplication_2", MA_TRUE);
	ma_ds_set_str(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SOFTWARE_ID, "xApplication_2", strlen("xApplication_2"));
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_POLICY_CONSUMER, 1);
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_PROPERTY_PROVIDER, 1);

	form_path(&buffer, MA_REGISTRY_STORE_AGENT_SCAN_PATH, "Applications", MA_TRUE);
	form_path(&buffer, (char *) ma_temp_buffer_get(&buffer), "xApplication_3", MA_TRUE);
	ma_ds_set_str(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SOFTWARE_ID, "xApplication_3", strlen("xApplication_3"));
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SCHEDULER_CONSUMER, 1);
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_UPDATER_CONSUMER, 1);

	form_path(&buffer, MA_REGISTRY_STORE_AGENT_SCAN_PATH, "Applications", MA_TRUE);
	form_path(&buffer, (char *) ma_temp_buffer_get(&buffer), "xApplication_4", MA_TRUE);
	ma_ds_set_str(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SOFTWARE_ID, "xApplication_4", strlen("xApplication_4"));
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_DATACHANNEL_CONSUMER, 1);
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_EVENT_GENERATOR, 1);
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_POLICY_CONSUMER, 1);
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_PROPERTY_PROVIDER, 1);
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SCHEDULER_CONSUMER, 1);
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_UPDATER_CONSUMER, 1);

	ma_temp_buffer_uninit(&buffer);
}

static void create_application_list_in_x86_view(ma_ds_t *ds) {
	/*Created First APP*/
	ma_temp_buffer_t buffer;
	ma_temp_buffer_init(&buffer);

	form_path(&buffer, MA_REGISTRY_STORE_AGENT_SCAN_PATH, "Applications", MA_TRUE);
	form_path(&buffer, (char *) ma_temp_buffer_get(&buffer), "Application_1", MA_TRUE);
	ma_ds_set_str(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SOFTWARE_ID, "Application_1", strlen("Application_1"));
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_DATACHANNEL_CONSUMER, 1);
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_EVENT_GENERATOR, 1);

	form_path(&buffer, MA_REGISTRY_STORE_AGENT_SCAN_PATH, "Applications", MA_TRUE);
	form_path(&buffer, (char *) ma_temp_buffer_get(&buffer), "Application_2", MA_TRUE);
	ma_ds_set_str(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SOFTWARE_ID, "Application_2", strlen("Application_2"));
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_POLICY_CONSUMER, 1);
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_PROPERTY_PROVIDER, 1);

	form_path(&buffer, MA_REGISTRY_STORE_AGENT_SCAN_PATH, "Applications", MA_TRUE);
	form_path(&buffer, (char *) ma_temp_buffer_get(&buffer), "Application_3", MA_TRUE);
	ma_ds_set_str(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SOFTWARE_ID, "Application_3", strlen("Application_3"));
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SCHEDULER_CONSUMER, 1);
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_UPDATER_CONSUMER, 1);

	form_path(&buffer, MA_REGISTRY_STORE_AGENT_SCAN_PATH, "Applications", MA_TRUE);
	form_path(&buffer, (char *) ma_temp_buffer_get(&buffer), "Application_4", MA_TRUE);
	ma_ds_set_str(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SOFTWARE_ID, "Application_4", strlen("Application_4"));
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_DATACHANNEL_CONSUMER, 1);
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_EVENT_GENERATOR, 1);
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_POLICY_CONSUMER, 1);
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_PROPERTY_PROVIDER, 1);
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SCHEDULER_CONSUMER, 1);
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_UPDATER_CONSUMER, 1);

	ma_temp_buffer_uninit(&buffer);
}

static void populate_applications(ma_ds_t *ds, ma_bool_t is_64_bit_os, ma_bool_t default_view){
	/*
	 * First create the Agent information DATAPATH and INSTALLPATH
	 * In x86 Os, create directly
	 * In x64 Os, create in default view if Agent Affinity is x64
	 * In x64 OS, create in x86 view if Agent Affinity is x86
	 */
	if(default_view) {
		fill_agent_information(ds);
	}
	switch(MA_AGENT_BIT_AFFINITY) {
		case 1: // x86
			{
				if(is_64_bit_os && !default_view) {
					create_application_list_in_x64_view(ds);
				}
				else if(is_64_bit_os && default_view) {
					create_application_list_in_x86_view(ds);
				}
			}
			break;
		case 2: //x64
			{
				if(is_64_bit_os && default_view) {
					create_application_list_in_x64_view(ds);
				}
				else if(is_64_bit_os && !default_view) {
					create_application_list_in_x86_view(ds);
				}
			}
			break;
		default:
			break;
	}
}

static void do_registry_setup() {
	char command[1024];
#ifdef MA_WINDOWS
	ma_ds_registry_t *g_registry_default = NULL;
	#ifdef MA_WINDOWS
		MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s %s", "rmdir /S /Q" , TEST_AGENT_ROOT);
	#else
		MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s %s", "rm -rf " , TEST_AGENT_ROOT);
	#endif
	system(command);
	if(MA_OK == ma_ds_registry_open(HKEY_LOCAL_MACHINE,MA_REGISTRY_STORE_SCAN_PATH, MA_TRUE, MA_TRUE, KEY_ALL_ACCESS, &g_registry_default)) {
		populate_applications((ma_ds_t *) g_registry_default, isSystem64Bit(), MA_TRUE);
		ma_ds_registry_release(g_registry_default);
	}
	if(isSystem64Bit() == MA_TRUE) {
		if(MA_OK == ma_ds_registry_open(HKEY_LOCAL_MACHINE,MA_REGISTRY_STORE_SCAN_PATH, MA_FALSE, MA_TRUE, KEY_ALL_ACCESS, &g_registry_default)) {
			populate_applications((ma_ds_t *) g_registry_default, isSystem64Bit(), MA_FALSE);
			ma_ds_registry_release(g_registry_default);
		}
	}
#endif

    MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s %s", "mkdir" , TEST_AGENT_ROOT);
    system(command);
    MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s %s", "mkdir" , TEST_AGENT_SETUP_DATA_PATH);
    system(command);
    MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s %s", "mkdir" , TEST_AGENT_INSTALL_PATH);
    system(command);

#ifdef MA_WINDOWS
	MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s", "CD");
#else
	MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s", "pwd");
#endif
    system(command);

#ifndef MA_WINDOWS
	add_agent_configuration(MA_REGISTRY_STORE_SCAN_PATH);
	add_software_to_configuration(MA_REGISTRY_STORE_SCAN_PATH, "Application_1", MA_REGISTERED_DATACHANNEL_SERVICE | MA_REGISTERED_EVENT_SERVICE);
	add_software_to_configuration(MA_REGISTRY_STORE_SCAN_PATH, "Application_2", MA_REGISTERED_POLICY_SERVICE | MA_REGISTERED_PROPERTY_SERVICE);
	add_software_to_configuration(MA_REGISTRY_STORE_SCAN_PATH, "Application_3", MA_REGISTERED_SCHEDULER_SERVICE| MA_REGISTERED_UPDATER_SERVICE);
	add_software_to_configuration(MA_REGISTRY_STORE_SCAN_PATH, "Application_4", MA_REGISTERED_ALL);
	add_software_to_configuration(MA_REGISTRY_STORE_SCAN_PATH, "xApplication_1", MA_REGISTERED_DATACHANNEL_SERVICE | MA_REGISTERED_EVENT_SERVICE);
	add_software_to_configuration(MA_REGISTRY_STORE_SCAN_PATH, "xApplication_2", MA_REGISTERED_POLICY_SERVICE | MA_REGISTERED_PROPERTY_SERVICE);
	add_software_to_configuration(MA_REGISTRY_STORE_SCAN_PATH, "xApplication_3", MA_REGISTERED_SCHEDULER_SERVICE| MA_REGISTERED_UPDATER_SERVICE);
	add_software_to_configuration(MA_REGISTRY_STORE_SCAN_PATH, "xApplication_4", MA_REGISTERED_ALL);
#endif
}

static void do_registry_cleanup() {
	char command[1024]={0};
#ifdef MA_WINDOWS
	ma_ds_registry_t *g_registry_default = NULL;
	if(MA_OK == ma_ds_registry_open(HKEY_LOCAL_MACHINE,MA_REGISTRY_STORE_SCAN_PATH, MA_TRUE, MA_TRUE, KEY_ALL_ACCESS, &g_registry_default)) {
		ma_ds_rem((ma_ds_t*)g_registry_default, "McAfee_T1", NULL, MA_TRUE);
		ma_ds_registry_release(g_registry_default);
	}
	if(isSystem64Bit() == MA_TRUE) {
		if(MA_OK == ma_ds_registry_open(HKEY_LOCAL_MACHINE,MA_REGISTRY_STORE_SCAN_PATH, MA_FALSE, MA_TRUE, KEY_ALL_ACCESS, &g_registry_default)) {
			ma_ds_rem((ma_ds_t *)g_registry_default, "McAfee_T1", NULL, MA_TRUE);
			ma_ds_registry_release(g_registry_default);
		}
	}
	MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s %s", "rmdir /S /Q" , TEST_AGENT_ROOT);
#else
	MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s %s", "rm -rf " , TEST_AGENT_ROOT);
#endif
	system(command);
}

int main(int argc, char *argv[]) {
	ma_int32_t ret = 0;
    MA_TRACKLEAKS;
	do_registry_setup();
	ret = UnityMain(argc, argv, run_all_tests);
	do_registry_cleanup();
}




