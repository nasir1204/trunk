/*
Unit tests for Task Service APIs
*/

#include "io_service_tests.h"
#include "ma/internal/services/io/ma_task_service.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/ma_client.h"
#include "ma/internal/utils/configurator/ma_configurator_utils.h"
#include "ma/internal/services/ma_agent_policy_settings_bag.h"
#include "ma/logger/ma_file_logger.h"
#include "ma/internal/utils/platform/ma_system_property.h"
#include "ma/internal/clients/ma/ma_client_internal.h"
#include "ma/internal/services/scheduler/ma_scheduler_core.h"

#include "unity.h"
#include "unity_fixture.h"

#include <stdlib.h>

static const char *ma_task_service_tests_service_name = "ma.task_service_tests.task";

ma_service_t *ma_task_service_tests_service = NULL;
ma_context_t *ma_task_service_tests_context = NULL;

ma_datastore_configuration_request_t task_service_datastore_required = {MA_TRUE, MA_TRUE, MA_TRUE, MA_TRUE, MA_TRUE};
ma_agent_configuration_t task_service_agent_configuration = { 5, 10, 15, TEST_AGENT_GUID };

ma_logger_t *ma_task_service_tests_logger = NULL;
ma_configurator_t *ma_task_service_tests_configurator = NULL;
ma_policy_settings_bag_t *ma_task_service_tests_policy_settings_bag = NULL;
ma_msgbus_t * ma_task_service_tests_msgbus = NULL;
ma_scheduler_t *ma_task_service_tests_scheduler;

ma_datastore_configuration_request_t ma_task_service_tests_datastore_required = {MA_TRUE, MA_TRUE, MA_TRUE, MA_TRUE, MA_FALSE};

TEST_GROUP_RUNNER(ma_task_service_tests_group) {
    do {RUN_TEST_CASE(ma_task_service_tests, task_service_create_tests)} while (0);
}

void ma_task_service_tests_init_context_object_infos(){
	if(ma_task_service_tests_logger == NULL)	{
		TEST_ASSERT(MA_OK == ma_file_logger_create(NULL,"test_file_logger",".log", (ma_file_logger_t **) &ma_task_service_tests_logger));	
	}	
	if(ma_task_service_tests_configurator == NULL){
		TEST_ASSERT(MA_OK == ma_configurator_create(&task_service_agent_configuration, &ma_task_service_tests_configurator));
		TEST_ASSERT(MA_OK == ma_configurator_intialize_datastores(ma_task_service_tests_configurator, &task_service_datastore_required));
		TEST_ASSERT(MA_OK == ma_configurator_configure_agent_policies(ma_task_service_tests_configurator));
	}
	if(ma_task_service_tests_policy_settings_bag == NULL){
		TEST_ASSERT(MA_OK == ma_agent_policy_settings_bag_create(ma_configurator_get_policies_database(ma_task_service_tests_configurator), &ma_task_service_tests_policy_settings_bag));
	}
	if(ma_task_service_tests_msgbus == NULL){
		TEST_ASSERT(MA_OK == ma_msgbus_create(MA_SOFTWAREID_GENERAL_STR, &ma_task_service_tests_msgbus));
		TEST_ASSERT(MA_OK == ma_msgbus_set_logger(ma_task_service_tests_msgbus, ma_task_service_tests_logger));
	}
	if(ma_task_service_tests_scheduler == NULL){
		TEST_ASSERT(MA_OK == ma_scheduler_create(ma_task_service_tests_msgbus, ma_configurator_get_scheduler_datastore(ma_task_service_tests_configurator), "ma_task_service_tests_scheduler", &ma_task_service_tests_scheduler));
	}
}

void ma_task_service_tests_deinit_context_object_infos()
{	
	if(ma_task_service_tests_configurator != NULL){
		ma_task_service_tests_configurator->methods->configurator_destroy(ma_task_service_tests_configurator);
	}
	if(ma_task_service_tests_policy_settings_bag != NULL){
		ma_task_service_tests_policy_settings_bag->methods->destroy(ma_task_service_tests_policy_settings_bag);
	}
	if(ma_task_service_tests_msgbus != NULL){
		TEST_ASSERT(MA_OK == ma_msgbus_release(ma_task_service_tests_msgbus));
	}
	if(ma_task_service_tests_scheduler != NULL){
		TEST_ASSERT(MA_OK == ma_scheduler_release(ma_task_service_tests_scheduler));
	}
	if(ma_task_service_tests_logger != NULL){
		TEST_ASSERT(MA_OK == ma_file_logger_release((ma_file_logger_t *) ma_task_service_tests_logger));	
	}
}

TEST_SETUP(ma_task_service_tests) {
	Unity.TestFile = "ma_task_service_test.c";
	
	// Verifies Task Service creation API
	//FAIL-> TEST_ASSERT(MA_ERROR_INVALIDARG == ma_task_service_create(NULL, &ma_task_service_tests_service));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_task_service_create(ma_task_service_tests_service_name, NULL));
	TEST_ASSERT(MA_OK == ma_task_service_create(ma_task_service_tests_service_name, &ma_task_service_tests_service));
	TEST_ASSERT(ma_task_service_tests_service != NULL);

	ma_task_service_tests_init_context_object_infos();
}

TEST_TEAR_DOWN(ma_task_service_tests) {
	
	// Verifies Task Service release API
	if(ma_task_service_tests_service != NULL){
		//FAIL-> ma_task_service_tests_service->methods->destroy(NULL);
		ma_task_service_tests_service->methods->destroy(ma_task_service_tests_service);
	}

	if(ma_task_service_tests_context != NULL){
		ma_context_release(ma_task_service_tests_context);
		ma_task_service_tests_context = NULL;
	}
	ma_task_service_tests_deinit_context_object_infos();
}

ma_context_t * get_context(){
	if(ma_task_service_tests_context != NULL){
		ma_context_release(ma_task_service_tests_context);
		ma_task_service_tests_context = NULL;
	}
	TEST_ASSERT(MA_OK == ma_context_create(&ma_task_service_tests_context));

	TEST_ASSERT(MA_OK == ma_context_add_object_info(ma_task_service_tests_context, MA_OBJECT_MA_CONFIGURATOR_NAME_STR, (void*const*)&ma_task_service_tests_configurator));
    TEST_ASSERT(MA_OK == ma_context_add_object_info(ma_task_service_tests_context, MA_OBJECT_POLICY_SETTINGS_BAG_STR, (void*const*)&ma_task_service_tests_policy_settings_bag));
    TEST_ASSERT(MA_OK == ma_context_add_object_info(ma_task_service_tests_context, MA_OBJECT_LOGGER_NAME_STR, (void*const*)&ma_task_service_tests_logger)); 
    TEST_ASSERT(MA_OK == ma_context_add_object_info(ma_task_service_tests_context, MA_OBJECT_MSGBUS_NAME_STR, (void*const*)&ma_task_service_tests_msgbus));        
	TEST_ASSERT(MA_OK == ma_context_add_object_info(ma_task_service_tests_context, MA_OBJECT_SCHEDULER_NAME_STR, (void*const*)&ma_task_service_tests_scheduler));
	
	return ma_task_service_tests_context;
}

ma_context_t * get_context_without_configurator_object(){
	if(ma_task_service_tests_context != NULL){
		ma_context_release(ma_task_service_tests_context);
		ma_task_service_tests_context = NULL;
	}
	TEST_ASSERT(MA_OK == ma_context_create(&ma_task_service_tests_context));
		
    TEST_ASSERT(MA_OK == ma_context_add_object_info(ma_task_service_tests_context, MA_OBJECT_POLICY_SETTINGS_BAG_STR, (void*const*)&ma_task_service_tests_policy_settings_bag));
    TEST_ASSERT(MA_OK == ma_context_add_object_info(ma_task_service_tests_context, MA_OBJECT_LOGGER_NAME_STR, (void*const*)&ma_task_service_tests_logger)); 
    TEST_ASSERT(MA_OK == ma_context_add_object_info(ma_task_service_tests_context, MA_OBJECT_MSGBUS_NAME_STR, (void*const*)&ma_task_service_tests_msgbus));        
	TEST_ASSERT(MA_OK == ma_context_add_object_info(ma_task_service_tests_context, MA_OBJECT_SCHEDULER_NAME_STR, (void*const*)&ma_task_service_tests_scheduler));
	
	return ma_task_service_tests_context;
}

ma_context_t * get_context_without_policy_settings_bag_object(){
	if(ma_task_service_tests_context != NULL){
		ma_context_release(ma_task_service_tests_context);
		ma_task_service_tests_context = NULL;
	}
	TEST_ASSERT(MA_OK == ma_context_create(&ma_task_service_tests_context));

	TEST_ASSERT(MA_OK == ma_context_add_object_info(ma_task_service_tests_context, MA_OBJECT_MA_CONFIGURATOR_NAME_STR, (void*const*)&ma_task_service_tests_configurator));
    TEST_ASSERT(MA_OK == ma_context_add_object_info(ma_task_service_tests_context, MA_OBJECT_LOGGER_NAME_STR, (void*const*)&ma_task_service_tests_logger)); 
    TEST_ASSERT(MA_OK == ma_context_add_object_info(ma_task_service_tests_context, MA_OBJECT_MSGBUS_NAME_STR, (void*const*)&ma_task_service_tests_msgbus));        
	TEST_ASSERT(MA_OK == ma_context_add_object_info(ma_task_service_tests_context, MA_OBJECT_SCHEDULER_NAME_STR, (void*const*)&ma_task_service_tests_scheduler));
	
	return ma_task_service_tests_context;
}

ma_context_t * get_context_without_logger_object(){
	if(ma_task_service_tests_context != NULL){
		ma_context_release(ma_task_service_tests_context);
		ma_task_service_tests_context = NULL;
	}
	TEST_ASSERT(MA_OK == ma_context_create(&ma_task_service_tests_context));

	TEST_ASSERT(MA_OK == ma_context_add_object_info(ma_task_service_tests_context, MA_OBJECT_MA_CONFIGURATOR_NAME_STR, (void*const*)&ma_task_service_tests_configurator));
    TEST_ASSERT(MA_OK == ma_context_add_object_info(ma_task_service_tests_context, MA_OBJECT_POLICY_SETTINGS_BAG_STR, (void*const*)&ma_task_service_tests_policy_settings_bag));
    TEST_ASSERT(MA_OK == ma_context_add_object_info(ma_task_service_tests_context, MA_OBJECT_MSGBUS_NAME_STR, (void*const*)&ma_task_service_tests_msgbus));        
	TEST_ASSERT(MA_OK == ma_context_add_object_info(ma_task_service_tests_context, MA_OBJECT_SCHEDULER_NAME_STR, (void*const*)&ma_task_service_tests_scheduler));
	
	return ma_task_service_tests_context;
}

ma_context_t * get_context_without_msgbus_object(){
	if(ma_task_service_tests_context != NULL){
		ma_context_release(ma_task_service_tests_context);
		ma_task_service_tests_context = NULL;
	}
	TEST_ASSERT(MA_OK == ma_context_create(&ma_task_service_tests_context));

	TEST_ASSERT(MA_OK == ma_context_add_object_info(ma_task_service_tests_context, MA_OBJECT_MA_CONFIGURATOR_NAME_STR, (void*const*)&ma_task_service_tests_configurator));
    TEST_ASSERT(MA_OK == ma_context_add_object_info(ma_task_service_tests_context, MA_OBJECT_POLICY_SETTINGS_BAG_STR, (void*const*)&ma_task_service_tests_policy_settings_bag));
    TEST_ASSERT(MA_OK == ma_context_add_object_info(ma_task_service_tests_context, MA_OBJECT_LOGGER_NAME_STR, (void*const*)&ma_task_service_tests_logger));       
	TEST_ASSERT(MA_OK == ma_context_add_object_info(ma_task_service_tests_context, MA_OBJECT_SCHEDULER_NAME_STR, (void*const*)&ma_task_service_tests_scheduler));
	
	return ma_task_service_tests_context;
}

ma_context_t * get_context_without_scheduler_object(){
	if(ma_task_service_tests_context != NULL){
		ma_context_release(ma_task_service_tests_context);
		ma_task_service_tests_context = NULL;
	}
	TEST_ASSERT(MA_OK == ma_context_create(&ma_task_service_tests_context));

	TEST_ASSERT(MA_OK == ma_context_add_object_info(ma_task_service_tests_context, MA_OBJECT_MA_CONFIGURATOR_NAME_STR, (void*const*)&ma_task_service_tests_configurator));
    TEST_ASSERT(MA_OK == ma_context_add_object_info(ma_task_service_tests_context, MA_OBJECT_POLICY_SETTINGS_BAG_STR, (void*const*)&ma_task_service_tests_policy_settings_bag));
    TEST_ASSERT(MA_OK == ma_context_add_object_info(ma_task_service_tests_context, MA_OBJECT_LOGGER_NAME_STR, (void*const*)&ma_task_service_tests_logger)); 
    TEST_ASSERT(MA_OK == ma_context_add_object_info(ma_task_service_tests_context, MA_OBJECT_MSGBUS_NAME_STR, (void*const*)&ma_task_service_tests_msgbus));
	
	return ma_task_service_tests_context;
}

TEST(ma_task_service_tests, task_service_create_tests){
	// Verifies Task Service configure API
	//FAIL-> TEST_ASSERT(MA_ERROR_INVALIDARG == ma_task_service_tests_service->methods->configure(NULL, get_context(), MA_SERVICE_CONFIG_NEW));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_task_service_tests_service->methods->configure(ma_task_service_tests_service, NULL, MA_SERVICE_CONFIG_NEW));
	//FAIL-> TEST_ASSERT(MA_ERROR_INVALIDARG == ma_task_service_tests_service->methods->configure(ma_task_service_tests_service, get_context(), 0x001));

	//FAIL-> TEST_ASSERT(MA_ERROR_INVALIDARG == ma_task_service_tests_service->methods->configure(ma_task_service_tests_service, get_context_without_configurator_object(), MA_SERVICE_CONFIG_NEW));
	//FAIL-> TEST_ASSERT(MA_ERROR_INVALIDARG == ma_task_service_tests_service->methods->configure(ma_task_service_tests_service, get_context_without_policy_settings_bag_object(), MA_SERVICE_CONFIG_NEW));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_task_service_tests_service->methods->configure(ma_task_service_tests_service, get_context_without_msgbus_object(), MA_SERVICE_CONFIG_NEW));
	//TEST_ASSERT(MA_ERROR_INVALIDARG == ma_task_service_tests_service->methods->configure(ma_task_service_tests_service, get_context_without_logger_object(), MA_SERVICE_CONFIG_MODIFIED));
	//TEST_ASSERT(MA_ERROR_INVALIDARG == ma_task_service_tests_service->methods->configure(ma_task_service_tests_service, get_context_without_scheduler_object(), MA_SERVICE_CONFIG_MODIFIED));
	
	TEST_ASSERT(MA_OK == ma_task_service_tests_service->methods->configure(ma_task_service_tests_service, get_context(), MA_SERVICE_CONFIG_NEW));
	//TEST_ASSERT(MA_OK == ma_task_service_tests_service->methods->configure(ma_task_service_tests_service, get_context(), MA_SERVICE_CONFIG_MODIFIED));
	
	// Verifies Task Service start API
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_task_service_tests_service->methods->start(NULL));
	//FAIL-> TEST_ASSERT(MA_OK == ma_task_service_tests_service->methods->start(ma_task_service_tests_service));
	
	// Verifies Task Service stop API
	//FAIL-> TEST_ASSERT(MA_ERROR_INVALIDARG == ma_task_service_tests_service->methods->stop(NULL));
	//FAIL-> TEST_ASSERT(MA_OK == ma_task_service_tests_service->methods->stop(ma_task_service_tests_service));
}

