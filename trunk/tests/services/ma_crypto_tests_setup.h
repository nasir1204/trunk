#ifndef CRYPTO_TESTS_SETUP_H_INCLUDED
#define CRYPTO_TESTS_SETUP_H_INCLUDED

#include "ma/internal/utils/macrypto/ma_crypto.h"

#ifdef MA_WINDOWS
#define MACRYPTO_TEST_PATH_SEPARATOR L"\\"
#else
#define MACRYPTO_TEST_PATH_SEPARATOR L"/"
#endif

#define MACRYPTO_TEST_BUFFER_SIZE 512


#define TEST_SIG_FILE_PATH_OFFICER  L"macrypto_test_officer.sig"
#define TEST_SIG_FILE_PATH_USER  L"macrypto_test_user.sig"

#define TEST_KEYSTORE_PATH_W  L"test_keystore"
#define TEST_KEYSTORE_PATH    "test_keystore"

#define MAX_KEYSTORE_KEYS      4

static const char *g_keystore_keys_name[]= {
    "agentpubkey.bin",
    "serverpubkey.bin",
    "agentprvkey.bin",		
    "serverreqseckey.bin"	
};

unsigned short int create_macrypto_object_with_new_keystore(ma_crypto_mode_t mode, ma_crypto_role_t role, ma_crypto_agent_mode_t agent_mode, ma_crypto_t **macrypto);

unsigned short int create_macrypto_object_with_old_keystore(ma_crypto_mode_t mode, ma_crypto_role_t role, ma_crypto_agent_mode_t agent_mode, const wchar_t *keystore_path , ma_crypto_t **macrypto);

unsigned short int create_macrypto_object_with_keys(ma_crypto_mode_t mode, ma_crypto_role_t role, ma_crypto_agent_mode_t agent_mode, ma_crypto_t **macrypto);

unsigned short int release_macrypto_object(ma_crypto_mode_t mode, ma_crypto_role_t role, ma_crypto_agent_mode_t agent_mode, ma_crypto_t *macrypto);


#endif /*CRYPTO_TESTS_SETUP_H_INCLUDED*/
