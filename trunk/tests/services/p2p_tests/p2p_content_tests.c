/*****************************************************************************
P2P Service - P2P content UTs
******************************************************************************/
#include "unity.h"
#include "unity_fixture.h"
#include "ma/internal/services/p2p/ma_p2p_content.h"
#include <string.h>

TEST_GROUP_RUNNER(p2p_content_tests_group)
{
    do {RUN_TEST_CASE(p2p_content_tests, p2p_content_verification)} while (0);
}

TEST_SETUP(p2p_content_tests)
{
}

TEST_TEAR_DOWN(p2p_content_tests)
{
}

struct LookupTable
{
    const char *hash;
    const char *content_type;
    const char *base_dir;
    const char *urn;
    size_t size;
    const char *contributer;
    ma_bool_t owned;
    ma_p2p_content_format_t format;
    const char *buffer;
    ma_uint32_t hit_count;
    const char *time_last_access;
    const char *time_created;
	ma_p2p_content_add_type_t file_op;
};

struct LookupTable const test_vector[] =
{
    // Hash                                           Content Type      Base Directory                                      URN                                 Content Size   Contributer            Owned      Content Format                Buffer                                          Hit Count    Last Access                   Last Created
    { "oTemZShNsNe3vIyStcUo0ynSLsqHqfjv1QlWBVHPbYk=", "Update content", "c:\\my_p2p_content\\virusscan\\8.0.1.2\\previous", "0000-0000-9E12-0000-O-0000-PQRS-1",          0,   "XYZ-Desktop ",        MA_TRUE,   MA_P2P_CONTENT_FORMAT_BUFFER, "This is the buffer containing P2P content",     2323424,    "06-02-1995 16:06:29.92 IST", "22-05-2014 11:51:12.33 GMT",  MA_P2P_CONTENT_COPY },
    { "oTem@324CFE4Frt4tV0ynSLsqHqfjv1Q$#RWBVHPbYk=", "New content",    "c:\\my_p2p_content\\DLP\\1.1.1\\current",          "0001-0000-9E34-0000-A-0000-0000-2",      12345,   "Reception Desktop",   MA_FALSE,  MA_P2P_CONTENT_FORMAT_FILE,   "This is not the buffer containing P2P content", 102,        "11-12-2000 23:26:39.92 IST", "02-07-2013 12:52:17.23 GMT",  MA_P2P_CONTENT_MOVE },
    { "oTemZShNsNe3vIyStcUo0ynSLsqHqfj213213SDSwdew", "Old content",    "c:\\repository_content\\5.0.1\\testing",           "9999-0000-9E12-0000-B-0000-ABCD-3",    3243602,   "Bunny's Laptop",      MA_TRUE,   MA_P2P_CONTENT_FORMAT_BUFFER, "This is a dirty buffer. Out of date content",   213123213,  "23-09-2002 10:06:49.92 IST", "22-11-2012 13:53:15.37 GMT",  MA_P2P_CONTENT_NONE },
    { "DFEfewcERERdfdcERERf213213@!D#$##@CdfdfdfdD=", "Purge content",  "c:\\my_p2p_content\\virusscan\\8.0.1.2\\previous", "1234-0000-9EAB-0000-C-0000-0000-4",       9999,   "ABC-Desktop Machine", MA_TRUE,   MA_P2P_CONTENT_FORMAT_BUFFER, "Old buffer",                                    0,          "12-07-1999 12:59:59.92 IST", "27-12-2011 14:54:12.12 GMT",  MA_P2P_CONTENT_MOVE },
    { "o3nek3dcm3lejm3ddCCSC2323CWWcdccCSDSDwweweww", "Fix content",    "c:\\",                                             "ABCD-0000-9EFA-0000-D-0000-1234-5",       3428,   "PQR-Desktop Machine", MA_FALSE,  MA_P2P_CONTENT_FORMAT_FILE,   "",                                              102,        "07-01-2013 23:16:19.92 IST", "31-03-2010 15:55:11.54 GMT",  MA_P2P_CONTENT_COPY },
};

TEST(p2p_content_tests, p2p_content_verification)
{
    ma_uint8_t counter = 0;

    /*
    TEST 1 - P2P Content Create and Release test
    */
    {
        ma_p2p_content_t *my_p2p_data = NULL;
        ma_buffer_t *new_buffer = NULL;

        // Creating P2P content object
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_create(NULL));
        TEST_ASSERT(MA_OK == ma_p2p_content_create(&my_p2p_data));

        // Retrieve Hash
        {
            const char *string_val = NULL;

            TEST_ASSERT(MA_OK == ma_p2p_content_get_hash(my_p2p_data, &string_val));
            TEST_ASSERT_NULL(string_val);
        }

        // Retrieve Content Type
        {
            const char *string_val = NULL;

            TEST_ASSERT(MA_OK == ma_p2p_content_get_content_type(my_p2p_data, &string_val));
            TEST_ASSERT_NULL(string_val);
        }

        // Retrieve Base Directory
        {
            const char *string_val = NULL;

            TEST_ASSERT(MA_OK == ma_p2p_content_get_base_dir(my_p2p_data, &string_val));
            TEST_ASSERT_NULL(string_val);
        }

        // Retrieve URN
        {
            const char *string_val = NULL;

            TEST_ASSERT(MA_OK == ma_p2p_content_get_urn(my_p2p_data, &string_val));
            TEST_ASSERT_NULL(string_val);
        }

        // Retrieve Contributor
        {
            const char *string_val = NULL;

            TEST_ASSERT(MA_OK == ma_p2p_content_get_contributor(my_p2p_data, &string_val));
            TEST_ASSERT_NULL(string_val);
        }

        // Retrieve Buffer and verify that the Buffer is NULL
        {
            ma_bool_t comparison_result = MA_FALSE;

            TEST_ASSERT(MA_OK == ma_p2p_content_get_buffer(my_p2p_data, &new_buffer));
            TEST_ASSERT_NULL(new_buffer);
        }

        // Retrieve Content Last Access Time
        {
            const char *string_val = NULL;

            TEST_ASSERT(MA_OK == ma_p2p_content_get_time_last_access(my_p2p_data, &string_val));
            TEST_ASSERT_NULL(string_val);
        }

        // Retrieve Content Creation Time
        {
            const char *string_val = NULL;

            TEST_ASSERT(MA_OK == ma_p2p_content_get_time_created(my_p2p_data, &string_val));
            TEST_ASSERT_NULL(string_val);
        }

        // Releasing P2P content object
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_release(NULL));
        TEST_ASSERT(MA_OK == ma_p2p_content_release(my_p2p_data));
    }

    /*
    TEST 2 - P2P Content Verification
    */
    {
        ma_p2p_content_t *my_p2p_data = NULL;

        // Creating P2P content object
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_create(NULL));
        TEST_ASSERT(MA_OK == ma_p2p_content_create(&my_p2p_data));

        for(counter = 0; counter < sizeof(test_vector)/sizeof(test_vector[0]); counter++)
        {

            ma_buffer_t *my_buffer = NULL;
            ma_buffer_t *new_buffer = NULL;

            // set Hash
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_set_hash(NULL, test_vector[counter].hash));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_set_hash(my_p2p_data, NULL));
            TEST_ASSERT(MA_OK == ma_p2p_content_set_hash(my_p2p_data, test_vector[counter].hash));

            // set Content Type
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_set_content_type(NULL, test_vector[counter].content_type));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_set_content_type(my_p2p_data, NULL));
            TEST_ASSERT(MA_OK == ma_p2p_content_set_content_type(my_p2p_data, test_vector[counter].content_type));

            // set Base Directory
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_set_base_dir(NULL, test_vector[counter].base_dir));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_set_base_dir(my_p2p_data, NULL));
            TEST_ASSERT(MA_OK == ma_p2p_content_set_base_dir(my_p2p_data, test_vector[counter].base_dir));

            // set URN
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_set_urn(NULL, test_vector[counter].urn));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_set_urn(my_p2p_data, NULL));
            TEST_ASSERT(MA_OK == ma_p2p_content_set_urn(my_p2p_data, test_vector[counter].urn));

            // set Content Size
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_set_content_size(NULL, test_vector[counter].size));
            TEST_ASSERT(MA_OK == ma_p2p_content_set_content_size(my_p2p_data, test_vector[counter].size));

            // set Contributer
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_set_contributor(NULL, test_vector[counter].contributer));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_set_contributor(my_p2p_data, NULL));
            TEST_ASSERT(MA_OK == ma_p2p_content_set_contributor(my_p2p_data, test_vector[counter].contributer));

            // set Owner
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_set_owned(NULL, test_vector[counter].owned));
            TEST_ASSERT(MA_OK == ma_p2p_content_set_owned(my_p2p_data, test_vector[counter].owned));

            // set Content Format
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_set_content_format(NULL, test_vector[counter].format));
            TEST_ASSERT(MA_OK == ma_p2p_content_set_content_format(my_p2p_data, test_vector[counter].format));

            // set Buffer
            {
                TEST_ASSERT(MA_OK == ma_buffer_create(&my_buffer, 50));
                TEST_ASSERT(MA_OK == ma_buffer_set(my_buffer, test_vector[counter].buffer, 50));

                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_set_buffer(NULL, my_buffer));
                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_set_buffer(my_p2p_data, NULL));
                TEST_ASSERT(MA_OK == ma_p2p_content_set_buffer(my_p2p_data, my_buffer));
            }

            // set Hit Count
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_set_hit_count(NULL, test_vector[counter].hit_count));
            TEST_ASSERT(MA_OK == ma_p2p_content_set_hit_count(my_p2p_data, test_vector[counter].hit_count));

            // set Content Last Access Time
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_set_time_last_access(NULL, test_vector[counter].time_last_access));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_set_time_last_access(my_p2p_data, NULL));
            TEST_ASSERT(MA_OK == ma_p2p_content_set_time_last_access(my_p2p_data, test_vector[counter].time_last_access));

            // set Content Creation Time
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_set_time_created(NULL, test_vector[counter].time_created));
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_set_time_created(my_p2p_data, NULL));
            TEST_ASSERT(MA_OK == ma_p2p_content_set_time_created(my_p2p_data, test_vector[counter].time_created));

            // set File Operation Type
            TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_set_file_op(NULL, test_vector[counter].file_op));
            TEST_ASSERT(MA_OK == ma_p2p_content_set_file_op(my_p2p_data, test_vector[counter].file_op));

            // Retrieve Hash
            {
                const char *string_val = NULL;

                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_get_hash(NULL, &string_val));
                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_get_hash(my_p2p_data, NULL));
                TEST_ASSERT(MA_OK == ma_p2p_content_get_hash(my_p2p_data, &string_val));
                TEST_ASSERT(0 == strcmp(test_vector[counter].hash, string_val));
            }

            // Retrieve Content Type
            {
                const char *string_val = NULL;

                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_get_content_type(NULL, &string_val));
                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_get_content_type(my_p2p_data, NULL));
                TEST_ASSERT(MA_OK == ma_p2p_content_get_content_type(my_p2p_data, &string_val));
                TEST_ASSERT(0 == strcmp(test_vector[counter].content_type, string_val));
            }

            // Retrieve Base Directory
            {
                const char *string_val = NULL;

                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_get_base_dir(NULL, &string_val));
                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_get_base_dir(my_p2p_data, NULL));
                TEST_ASSERT(MA_OK == ma_p2p_content_get_base_dir(my_p2p_data, &string_val));
                TEST_ASSERT(0 == strcmp(test_vector[counter].base_dir, string_val));
            }

            // Retrieve URN
            {
                const char *string_val = NULL;

                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_get_urn(NULL, &string_val));
                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_get_urn(my_p2p_data, NULL));
                TEST_ASSERT(MA_OK == ma_p2p_content_get_urn(my_p2p_data, &string_val));
                TEST_ASSERT(0 == strcmp(test_vector[counter].urn, string_val));
            }

            // Retrieve Content Size
            {
                size_t int_val = 0;

                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_get_content_size(NULL, &int_val));
                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_get_content_size(my_p2p_data, NULL));
                TEST_ASSERT(MA_OK == ma_p2p_content_get_content_size(my_p2p_data, &int_val));
                TEST_ASSERT(test_vector[counter].size == int_val);
            }

            // Retrieve Contributor
            {
                const char *string_val = NULL;

                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_get_contributor(NULL, &string_val));
                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_get_contributor(my_p2p_data, NULL));
                TEST_ASSERT(MA_OK == ma_p2p_content_get_contributor(my_p2p_data, &string_val));
                TEST_ASSERT(0 == strcmp(test_vector[counter].contributer, string_val));
            }

            // Retrieve Owner
            {
                ma_bool_t owner = MA_FALSE;

                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_get_owned(NULL, &owner));
                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_get_owned(my_p2p_data, NULL));
                TEST_ASSERT(MA_OK == ma_p2p_content_get_owned(my_p2p_data, &owner));
                TEST_ASSERT(test_vector[counter].owned == owner);
            }

            // Retrieve Content Format
            {
                ma_p2p_content_format_t format = MA_P2P_CONTENT_FORMAT_FILE;

                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_get_content_format(NULL, &format));
                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_get_content_format(my_p2p_data, NULL));
                TEST_ASSERT(MA_OK == ma_p2p_content_get_content_format(my_p2p_data, &format));
                TEST_ASSERT(test_vector[counter].format == format);
            }

            // Retrieve Buffer and compare against the buffer set using the ma_p2p_content_set_buffer API
            {
                ma_bool_t comparison_result = MA_FALSE;

                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_get_buffer(NULL, &new_buffer));
                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_get_buffer(my_p2p_data, NULL));
                TEST_ASSERT(MA_OK == ma_p2p_content_get_buffer(my_p2p_data, &new_buffer));
                TEST_ASSERT(MA_OK == ma_buffer_is_equal(new_buffer, my_buffer, &comparison_result));
                TEST_ASSERT(MA_TRUE == comparison_result);
            }

            // Retrieve Hit Count
            {
                ma_uint32_t int_val = 0;

                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_get_hit_count(NULL, &int_val));
                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_get_hit_count(my_p2p_data, NULL));
                TEST_ASSERT(MA_OK == ma_p2p_content_get_hit_count(my_p2p_data, &int_val));
                TEST_ASSERT(test_vector[counter].hit_count == int_val);
            }

            // Retrieve Content Last Access Time
            {
                const char *string_val = NULL;

                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_get_time_last_access(NULL, &string_val));
                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_get_time_last_access(my_p2p_data, NULL));
                TEST_ASSERT(MA_OK == ma_p2p_content_get_time_last_access(my_p2p_data, &string_val));
                TEST_ASSERT(0 == strcmp(test_vector[counter].time_last_access, string_val));
            }

            // Retrieve Content Creation Time
            {
                const char *string_val = NULL;

                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_get_time_created(NULL, &string_val));
                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_get_time_created(my_p2p_data, NULL));
                TEST_ASSERT(MA_OK == ma_p2p_content_get_time_created(my_p2p_data, &string_val));
                TEST_ASSERT(0 == strcmp(test_vector[counter].time_created, string_val));
            }

            // Retrieve File Operation Type
            {
                ma_p2p_content_add_type_t file_op = MA_P2P_CONTENT_NONE;

                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_get_file_op(NULL, &file_op));
                TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_get_file_op(my_p2p_data, NULL));
                TEST_ASSERT(MA_OK == ma_p2p_content_get_file_op(my_p2p_data, &file_op));
                TEST_ASSERT(test_vector[counter].file_op == file_op);
            }

            // Releasing Buffers
            TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
            TEST_ASSERT(MA_OK == ma_buffer_release(new_buffer));
        }

        // Releasing P2P content object
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_p2p_content_release(NULL));
        TEST_ASSERT(MA_OK == ma_p2p_content_release(my_p2p_data));
    }
}

