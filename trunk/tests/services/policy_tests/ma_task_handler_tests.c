#include "unity.h"
#include "unity_fixture.h"
#include "unity_internals.h"

#include <string.h>
#include <stdlib.h>

#include "ma/internal/utils/database/ma_db.h"
#include "ma/internal/services/policy/ma_task_handler.h"
#include "ma/logger/ma_console_logger.h"

TEST_GROUP_RUNNER(task_handler_tests_group) {
    do {RUN_TEST_CASE(task_handler_tests, handler_test)} while (0);    
}

ma_db_t *db = NULL;
ma_net_client_service_t *net_service = NULL;
ma_ah_sitelist_t *sitelist = NULL;
static ma_event_loop_t *event_loop = NULL;

static ma_task_handler_t *task_handle = NULL;

static ma_console_logger_t *console_logger = NULL;

#define POLICY_DB_FILENAME  "POLICY"


#define CA_FILE_NAME        "C:\\ProgramData\\McAfee\\Common Framework\\cabundle.cer"
#define GLOBAL_VERSION      "VlUZlJaB6wuBd75vKrG/LUUa2dLd59wdI0BOR9lUmIs="
#define LOCAL_VERSION       "Mon, 16 Jul 2012 11:52:32 UTC"


TEST_SETUP(task_handler_tests) {
    ma_error_t rc = MA_OK;

    ma_ah_site_t *site = NULL;

    unlink(POLICY_DB_FILENAME) ;
    ma_db_intialize() ;
    TEST_ASSERT_MESSAGE(MA_OK == ma_db_open(POLICY_DB_FILENAME, &db), "DB open failed") ;

    rc = ma_console_logger_create(&console_logger);
	TEST_ASSERT_MESSAGE( MA_OK == rc && NULL != console_logger, "failed to create a console logger");

    rc = ma_event_loop_create(&event_loop);

    rc = ma_net_client_service_create(event_loop, &net_service);
    rc = ma_net_client_service_setlogger(net_service, (ma_logger_t *)console_logger);
    rc = ma_net_client_service_start(net_service); 

    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ah_sitelist_create(&sitelist));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ah_sitelist_set_global_version(sitelist, GLOBAL_VERSION));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ah_sitelist_set_local_version(sitelist, LOCAL_VERSION));
    TEST_ASSERT_EQUAL_INT(MA_OK, ma_ah_sitelist_set_ca_file(sitelist, CA_FILE_NAME));
    

    rc = ma_task_handler_create(db, net_service, sitelist, &task_handle);
}

TEST_TEAR_DOWN(task_handler_tests) {
    ma_net_client_service_stop(net_service);
    ma_net_client_service_release(net_service);
    ma_event_loop_release(event_loop);
    TEST_ASSERT_MESSAGE(MA_OK == ma_db_close(db), "DB open failed") ;
    ma_db_deintialize() ;
    ma_ah_sitelist_release(sitelist);
    ma_console_logger_release(console_logger);
    ma_task_handler_release(task_handle);
}

extern ma_error_t update_tasks_begin(ma_task_handler_t *self, unsigned char *manifest, size_t manifest_size);

TEST(task_handler_tests, handler_test) {
    ma_error_t rc = MA_OK;
    
 //   rc = update_tasks_begin(task_handle, );
}