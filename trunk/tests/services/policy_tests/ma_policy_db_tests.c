/******************************************************************************
Policy Service - Policy DB unit tests.
******************************************************************************/

#include "unity.h"
#include "unity_fixture.h"
#include "unity_internals.h"
#include "ma/internal/utils/database/ma_db.h"
#include "ma/internal/services/policy/ma_policy_db.h"
#include <string.h>

#ifndef WIN32
    #include <unistd.h>
#endif

TEST_GROUP_RUNNER(policy_db_tests_group)
{
    do {RUN_TEST_CASE(policy_db_tests, policy_addition_test)} while (0);
    do {RUN_TEST_CASE(policy_db_tests, policy_settings_test)} while (0);
    do {RUN_TEST_CASE(policy_db_tests, policy_assignment_test)} while (0);
    do {RUN_TEST_CASE(policy_db_tests, get_product_policies_test)} while (0);
    do {RUN_TEST_CASE(policy_db_tests, get_all_products_test)} while (0);
    do {RUN_TEST_CASE(policy_db_tests, get_policy_setting_object_id_test)} while (0);
    do {RUN_TEST_CASE(policy_db_tests, update_policy_setting_object_data_test)} while (0);
    do {RUN_TEST_CASE(policy_db_tests, get_notified_product_list_test)} while (0);
}

static ma_db_t *db = NULL;
#define POLICY_DB_FILENAME  "POLICY"

TEST_SETUP(policy_db_tests)
{
    unlink(POLICY_DB_FILENAME);
    TEST_ASSERT_MESSAGE(MA_OK == ma_db_open(POLICY_DB_FILENAME,MA_DB_OPEN_READWRITE, &db), "DB open failed");

    TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_add_policy_instance(NULL), "db_add_policy_instance failed");
    TEST_ASSERT_MESSAGE(MA_OK == db_add_policy_instance(db), "db_add_policy_instance failed");
}

TEST_TEAR_DOWN(policy_db_tests)
{
    TEST_ASSERT_MESSAGE(MA_OK == ma_db_close(db), "DB close failed") ;
	unlink(POLICY_DB_FILENAME);
}

// Lookup Table consisting of Policy object information
struct LookupTable_1
{
    const char *test_po_id;
    const char *test_po_name;
    const char *test_po_feature;
    const char *test_po_category;
    const char *test_po_type;
    const char *test_po_digest;
    const char *test_po_legacy;
};

static struct LookupTable_1 const policies[] =
{
    { "10", "__EPO_ENFORCE_YES__",  "Feature 4", "Category 1", "Normal",   "8f36f236c1e6382d7bb0b632a52e0e203fc71fac525c838bb31474470bfba853", "true"    },
    { "2",  "Policy 3",             "Feature 1", "Category 3", "Critical", "fklehflfnlefeklcndcndklfhnc,dsncdslkfndsklfnsdlknfdsklfdsklfdsnf", "false"   },
    { "3",  "__EPO_ENFORCE_YES__",  "Feature 1", "Category 2", "Ignore",   "vdldmlsldfo4349u43p34oro34rm34r34nrl4kn3lkn3l5kn34lk5n345l34k5n3", "true"    },
    { "4",  "Policy 4",             "Feature 2", "Category 3", "Normal",   "sdlfudods,fewrfsdlfhsdflkehjfelkcsd,fneoinefsd.fnslfje470bfba853", "Yes"     },
    { "5",  "Policy 5",             "Feature 1", "Category 4", "Default",  "8f36f236c1e6382d7bb0b632a52e0e20sjsdklehfoiefnsdfnhdskffkjsdjs53", "No"      },
    { "6",  "Policy 9",             "Feature 3", "Category 1", "Normal",   "8f36f236c1e6382d7bb0bdwkjldhnlwdfnf,mnbcmsvcsajdvdfhjfdwjhdwjdwj", "TRUE"    },
    { "7",  "Policy 8",             "Feature 1", "Category 2", "Critical", "8f36f236c1e6382d7bb0b632a52e0e203fc71fac525c838bb31474470bfba853", "FALSE"   },
    { "9",  "Policy 7",             "Feature 3", "Category 3", "Major",    "dkwedbwekdwebdkhdwelkdyewhde0e203fc71fac525c838bb3wedewkjjewkjew", "false"   },
    { "11", "__EPO_ENFORCE_YES__",  "Feature 1", "Category 3", "Default",  "wdwedwerwrewrerd7bb0b632a52e0e203fc71facdewwerewrewrwerererereer", "partial" },
    { "12", "Policy 10",            "Feature 2", "Category 1", "Normal",   "8f36f236c1e63823wedwdewdwdwdwedewfc71fac525c838bb31474470bfba853", "New"     },
    { "8",  "Policy 6",             "Feature 2", "Category 5", "Normal",   "8f36f236c1e6382dqwdewedwdwffrwerwedwdwerwerd838bb31474470bfba853", "Legacy"  },
};

// Lookup Table consisting of Policy Settings object information
struct LookupTable_2
{
    const char *test_po_id;
    const char *test_pso_id;
    const char *test_pso_name;
    const char *test_pso_digest;
    const char *test_pso_param_int;
    const char *test_pso_param_str;
    const char *test_time_stamp;
};

// Note - Things to be kept in mind while preparing below test data -
// 1) The policy ID should exist in DB. The code doesn't verify this while assigning policy IDs
//    to Policy Settings IDs.
// 2) 'Method' should be non-NULL, if 'param' is not NULL.
static struct LookupTable_2 const policy_settings[] =
{
    { "10", "100",  "Policy Setting 1",  "8f36f236c1e6382d7bb0b632a52e0e203fc71fac525c838bb31474470bfba853", "1",  "Virus Scan Policy",            "2014-12-22T18:35:01.100" },
    { "9",  "200",  "Policy Setting 2",  "8f459874504957rt7bb0b632a52e0e203fc71fac525c838bb31474470bfba853", "8",  NULL,                           "2012-03-06T18:35:01.000" },
    { "8",  "300",  "Policy Setting 3",  "8f36f236c1e638rotiu98eueln9048ujfnelu489rt434t5bb31474470bfba853", "9",  "My Personal Policy",           "2011-02-01T18:35:01.000" },
    { "7",  "400",  "Policy Setting 4",  "8f36f236c1e6382d7bb0b632a52e0e203fc7hnac525c838bb31474470bfba853", NULL, "Scheduler Policy",             "2014-12-22T18:35:01.100" },
    { "6",  "500",  "Policy Setting 5",  "ejeoiru4oij4n4i5h4nkn4jk4h54bn4hk3l5h43kbn4345j4h534kj8f36f236c1", "6",  NULL,                           "2014-05-26T18:35:11.000" },
    { "5",  "600",  "Policy Setting 6",  "32462389284732yher3hjk3h4i374hn3m4334h3k4n3k43j4h34rn3jk4j3hj853", "5",  "Site Advisor Policy",          "2014-12-22T18:35:01.100" },
    { "4",  "700",  "Policy Setting 7",  "3lhnr3umr3bnkjbn34m3br3b3kjr3br3kj4rb34jkr34brj34krb3rj34krb34rb", NULL, "Critical Error Policy",        "2012-05-16T18:35:11.000" },
    { "3",  "800",  "Policy Setting 8",  "2382892374683924632984632894623948326483274623894632489362498326", "1",  "Scheduled Scan Policy",        "2014-12-22T18:35:01.100" },
    { "2",  "900",  "Policy Setting 9",  "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", "2",  "Weekly Triggered Scan Policy", "2012-04-29T19:35:01.000" },
    { "1",  "1000", "Policy Setting 10", "zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz", NULL, NULL,                           "2011-06-30T18:36:01.000" },
};

// Lookup Table consisting of Product Assignment information
struct LookupTable_3
{
    const char *test_po_id;
    const char *test_product;
    const char *test_method;
    const char *test_param;
};

// Note - Things to be kept in mind while preparing below test data -
// 1) The policy ID should exist in DB. The code doesn't verify this while assigning policy IDs to Product IDs.
// 2) 'Method' should be non-NULL, if 'param'not NULL.
static struct LookupTable_3 const policy_product[] =
{
    { "10", "Product 1", "Method 1",  "No Param" },
    { "10", "Product 2", "Method 2",  "Param 1"  },
    { "10", "Product 1", "Method 4",  "No Param" },
    { "7",  "Product 3", "method 5",   NULL      },
    { "6",  "Product 2", "Method 2",  "Param 1"  },
    { "5",  "Product 1",  NULL,        NULL      },
    { "4",  "Product 2", "Method 7",  "Param 1"  },
    { "1",  "Product 3",  NULL,        NULL      },
    { "3",  "Product 2",  NULL,        NULL      },
    { "5",  "Product 1", "method 11",  NULL      },
    { "7",  "Product 2",  NULL,        NULL      },
    { "3",  "Product 2", "Method 9",  "Param 1"  },
    { "2",  "Product 1", "Method 10", "12345"    },
    { "1",  "Product 3", "Method 2",   NULL      },
    { "10", "Product 1",  NULL,        NULL      },
    { "2",  "Product 1", "Method 7",  "12345"    },
    { "2",  "Product 3",  NULL,        NULL      },
};

/* This function finds out that the policy assignment record retrieved in return for the
   db_get_policy_product_assignments query is one of the expected records.
*/
static ma_int32_t exp_policy_assignment_record_list[sizeof(policy_product)/sizeof(policy_product[0])] = { 0 };

static ma_int32_t do_policy_assignment_matching(const char *actual_po_id,
                                                const char *actual_product,
                                                const char *actual_method,
                                                const char *actual_param)
{
    ma_int32_t counter = 0;

    for(counter = 0; counter < sizeof(policy_product)/sizeof(policy_product[0]); counter++)
    {
        ma_uint8_t matched = 1;

        if(exp_policy_assignment_record_list[counter] == 1)
        {
            if(strcmp(policy_product[counter].test_po_id, actual_po_id)) {  matched = 0;  }

            if(strcmp(policy_product[counter].test_product, actual_product)) {  matched = 0;  }

            // If expected method value is NULL, compare against "0"
            if(!policy_product[counter].test_method)
            {
                if(strcmp("0", actual_method)) {  matched = 0;  }
            }
            else
            {
                if(strcmp(policy_product[counter].test_method, actual_method)) {  matched = 0;  }
            }

            // If expected parameter value is NULL, compare against "0"
            if(!policy_product[counter].test_param)
            {
                if(strcmp("0", actual_param)) {  matched = 0;  }
            }
            else
            {
                if(strcmp(policy_product[counter].test_param, actual_param)) {  matched = 0;  }
            }
        }

        if(exp_policy_assignment_record_list[counter] && matched)
        {
            exp_policy_assignment_record_list[counter] = 0;
            matched = 0;
            return 1;
        }
    }
    return 0;
}

/* This function finds out that the policy record retrieved in return for the db_get_product_policies
   query is one of the expected records.

   Note -
   1) The actual policy data stored in the variant is not verified in this test.
   2) The verification that the PSO name is correct corresponding to the PO, is not performed in this test.
*/
static ma_int32_t exp_product_matching_record_list[sizeof(policies)/sizeof(policies[0])] = { 0 };

static ma_int32_t do_policy_product_matching(const char *actual_po_feature,
                                             const char *actual_po_category,
                                             const char *actual_po_type,
                                             const char *actual_pso_name)
{
    ma_int32_t counter = 0;

    for(counter = 0; counter < sizeof(policies)/sizeof(policies[0]); counter++)
    {
        ma_uint8_t matched = 1;

        if(exp_product_matching_record_list[counter] == 1)
        {
            if(strcmp(policies[counter].test_po_feature, actual_po_feature)) {  matched = 0;  }

            if(strcmp(policies[counter].test_po_category, actual_po_category)) {  matched = 0;  }

            if(strcmp(policies[counter].test_po_type, actual_po_type)) {  matched = 0;  }

            // PSO name is not verified as of now
        }

        if(exp_product_matching_record_list[counter] && matched)
        {
            exp_product_matching_record_list[counter] = 0;
            matched = 0;
            return 1;
        }
    }
    return 0;
}


/******************************************************************************
Test for verifying the Policy addition/removal/retrieval APIs.
******************************************************************************/
TEST(policy_db_tests, policy_addition_test)
{
    ma_table_t *first_level_table = NULL;
    ma_table_t *second_level_table = NULL;
    ma_variant_t *variant_ptr = NULL;
    ma_int32_t counter = 0;

    /* Creating the Policy information. Policy information remains common for all
       Policies. Converting this complete information into a variant (Table of Tables).

    <Section name="Server_Alerting">
        <Setting name="bCleanFiles">1</Setting>
        <Setting name="bDeleteFiles">0</Setting>
        <Setting name="bExcludeCookies">1</Setting>
        <Setting name="szDialogMessage">VirusScan Alert!</Setting>
    </Section>
    <Section name="Server_Blocking">
        <Setting name="VSIDBlock">1</Setting>
        <Setting name="VSIDBlockOnNonVirus">0</Setting>
        <Setting name="VSIDBlockTimeout">10</Setting>
    </Section>
    */
    ma_table_create(&first_level_table);

    /* Section - 1 */
    ma_table_create(&second_level_table);

    ma_variant_create_from_string("1", strlen("1"), &variant_ptr);
    ma_table_add_entry(second_level_table, "bCleanFiles", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_string("0", strlen("0"), &variant_ptr);
    ma_table_add_entry(second_level_table, "bDeleteFiles", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_string("1", strlen("1"), &variant_ptr);
    ma_table_add_entry(second_level_table, "bExcludeCookies", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_string("VirusScan Alert!", strlen("VirusScan Alert!"), &variant_ptr);
    ma_table_add_entry(second_level_table, "szDialogMessage", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_table(second_level_table, &variant_ptr);
    ma_table_add_entry(first_level_table, "Server_Alerting", variant_ptr);
    ma_table_release(second_level_table); second_level_table = NULL;
    ma_variant_release(variant_ptr);

    /* Section - 2 */
    ma_table_create(&second_level_table);

    ma_variant_create_from_string("1", strlen("1"), &variant_ptr);
    ma_table_add_entry(second_level_table, "VSIDBlock", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_string("0", strlen("0"), &variant_ptr);
    ma_table_add_entry(second_level_table, "VSIDBlockOnNonVirus", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_string("10", strlen("10"), &variant_ptr);
    ma_table_add_entry(second_level_table, "VSIDBlockTimeout", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_table(second_level_table, &variant_ptr);
    ma_table_add_entry(first_level_table, "Server_Blocking", variant_ptr);
    ma_table_release(second_level_table);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_table(first_level_table, &variant_ptr);
    ma_table_release(first_level_table);

    // Adding Policies to the DB
    for(counter = 0; counter < sizeof(policies)/sizeof(policies[0]); counter++)
    {
        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_add_policy_object(NULL,
                                                                        policies[counter].test_po_id,
                                                                        policies[counter].test_po_name,
                                                                        policies[counter].test_po_feature,
                                                                        policies[counter].test_po_category,
                                                                        policies[counter].test_po_type,
                                                                        policies[counter].test_po_digest,
                                                                        policies[counter].test_po_legacy), "db_add_policy_object failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_add_policy_object(db,
                                                                        NULL,
                                                                        policies[counter].test_po_name,
                                                                        policies[counter].test_po_feature,
                                                                        policies[counter].test_po_category,
                                                                        policies[counter].test_po_type,
                                                                        policies[counter].test_po_digest,
                                                                        policies[counter].test_po_legacy), "db_add_policy_object failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_add_policy_object(db,
                                                                        policies[counter].test_po_id,
                                                                        NULL,
                                                                        policies[counter].test_po_feature,
                                                                        policies[counter].test_po_category,
                                                                        policies[counter].test_po_type,
                                                                        policies[counter].test_po_digest,
                                                                        policies[counter].test_po_legacy), "db_add_policy_object failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_add_policy_object(db,
                                                                        policies[counter].test_po_id,
                                                                        policies[counter].test_po_name,
                                                                        NULL,
                                                                        policies[counter].test_po_category,
                                                                        policies[counter].test_po_type,
                                                                        policies[counter].test_po_digest,
                                                                        policies[counter].test_po_legacy), "db_add_policy_object failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_add_policy_object(db,
                                                                        policies[counter].test_po_id,
                                                                        policies[counter].test_po_name,
                                                                        policies[counter].test_po_feature,
                                                                        NULL,
                                                                        policies[counter].test_po_type,
                                                                        policies[counter].test_po_digest,
                                                                        policies[counter].test_po_legacy), "db_add_policy_object failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_add_policy_object(db,
                                                                        policies[counter].test_po_id,
                                                                        policies[counter].test_po_name,
                                                                        policies[counter].test_po_feature,
                                                                        policies[counter].test_po_category,
                                                                        NULL,
                                                                        policies[counter].test_po_digest,
                                                                        policies[counter].test_po_legacy), "db_add_policy_object failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_add_policy_object(db,
                                                                        policies[counter].test_po_id,
                                                                        policies[counter].test_po_name,
                                                                        policies[counter].test_po_feature,
                                                                        policies[counter].test_po_category,
                                                                        policies[counter].test_po_type,
                                                                        NULL,
                                                                        policies[counter].test_po_legacy), "db_add_policy_object failed");

       TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_add_policy_object(db,
                                                                        policies[counter].test_po_id,
                                                                        policies[counter].test_po_name,
                                                                        policies[counter].test_po_feature,
                                                                        policies[counter].test_po_category,
                                                                        policies[counter].test_po_type,
                                                                        policies[counter].test_po_digest,
                                                                        NULL), "db_add_policy_object failed");

        TEST_ASSERT_MESSAGE(MA_OK == db_add_policy_object(db,
                                                          policies[counter].test_po_id,
                                                          policies[counter].test_po_name,
                                                          policies[counter].test_po_feature,
                                                          policies[counter].test_po_category,
                                                          policies[counter].test_po_type,
                                                          policies[counter].test_po_digest,
                                                          policies[counter].test_po_legacy), "db_add_policy_object failed");
    }

    // Adding Policy Settings to DB. Note - All policy IDs should exist. This test
    // doesn't verify the scenario in which a policy ID, which doesn't exist, is
    // added to the DB.
    for(counter = 0; counter < sizeof(policy_settings)/sizeof(policy_settings[0]); counter++)
    {
        TEST_ASSERT_MESSAGE(MA_OK == db_add_policy_settings_object(db,
                                                                   policy_settings[counter].test_po_id,
                                                                   policy_settings[counter].test_pso_id,
                                                                   policy_settings[counter].test_pso_name,
                                                                   policy_settings[counter].test_pso_digest,
                                                                   variant_ptr,
                                                                   policy_settings[counter].test_pso_param_int,
                                                                   policy_settings[counter].test_pso_param_str,
                                                                   policy_settings[counter].test_time_stamp), "db_add_policy_settings_object failed");
    }

    // Policy data (stored in the Variant) is added to DB, so variant can be released now.
    ma_variant_release(variant_ptr);

    // Adding Product Assignments to DB. Note - All policy IDs should exist. This test
    // doesn't verify the scenario in which a policy ID, which doesn't exist, is added
    // to the DB.
    for(counter = 0; counter < sizeof(policy_product)/sizeof(policy_product[0]); counter++)
    {
        TEST_ASSERT_MESSAGE(MA_OK == db_add_policy_product_assignment(db,
                                                                      policy_product[counter].test_po_id,
                                                                      policy_product[counter].test_product,
                                                                      policy_product[counter].test_method,
                                                                      policy_product[counter].test_param), "db_add_policy_product_assignment failed");
    }

    // Verifying the Policy entries (in reverse order)
    for(counter = sizeof(policies)/sizeof(policies[0]) - 1; counter >= 0; counter--)
    {
        ma_db_recordset_t *record_set = NULL;
        const char *record_col_values = NULL;

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_get_policy_objects(NULL,
                                                                         policies[counter].test_po_id,
                                                                         &record_set),
                                                                         "db_get_policy_objects failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_get_policy_objects(db,
                                                                         policies[counter].test_po_id,
                                                                         NULL),
                                                                         "db_get_policy_objects failed");

        TEST_ASSERT_MESSAGE(MA_OK == db_get_policy_objects(db,
                                                           policies[counter].test_po_id,
                                                           &record_set),
                                                           "db_get_policy_objects failed");

        while(MA_ERROR_NO_MORE_ITEMS != ma_db_recordset_next(record_set))
        {
            ma_db_recordset_get_string(record_set, 1, &record_col_values);
            TEST_ASSERT_MESSAGE(!strcmp(record_col_values, policies[counter].test_po_id), "Failed to retrieve correct policy ID");

            ma_db_recordset_get_string(record_set, 2, &record_col_values);
            TEST_ASSERT_MESSAGE(!strcmp(record_col_values, policies[counter].test_po_digest), "Failed to retrieve correct policy digest");
        }

        ma_db_recordset_release(record_set);
        record_set = NULL;
    }

    // Scenario - If a Policy is removed from the Policy Table, corresponding
    // entries from the Policy Settings Table and Policy-Product Assignment
    // table are also removed.
    // 1) Removing the policy entry by calling the db_remove_policy_object API.
    //    Then, calling the db_get_policy_objects API and verifying that no
    //    record is returned.
    // 2) Calling the db_get_policy_settings_objects API with the given Policy
    //    ID and verifying that it doesn't return any records.
    // 3) Calling the db_get_policy_product_assignments API with the given Policy
    //    ID and verifying that it doesn't return any records.
    for(counter = 0; counter < sizeof(policies)/sizeof(policies[0]); counter++)
    {
        ma_db_recordset_t *record_set = NULL;

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_remove_policy_object(NULL,
                                                                           policies[counter].test_po_id),
                                                                           "db_remove_policy_object failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_remove_policy_object(db,
                                                                           NULL),
                                                                           "db_remove_policy_object failed");

        TEST_ASSERT_MESSAGE(MA_OK == db_remove_policy_object(db,
                                                             policies[counter].test_po_id),
                                                             "db_remove_policy_object failed");

        TEST_ASSERT_MESSAGE(MA_OK == db_get_policy_objects(db,
                                                           policies[counter].test_po_id,
                                                           &record_set),
                                                           "db_get_policy_objects failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_NO_MORE_ITEMS == ma_db_recordset_next(record_set), "All matching records from Policy Table couldn't be removed");

        ma_db_recordset_release(record_set);
        record_set = NULL;

        TEST_ASSERT_MESSAGE(MA_OK == db_get_policy_settings_objects(db,
                                                                    policies[counter].test_po_id,
                                                                    NULL,
                                                                    &record_set),
                                                                    "db_get_policy_settings_objects failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_NO_MORE_ITEMS == ma_db_recordset_next(record_set), "All matching records from Policy settings couldn't be removed");

        ma_db_recordset_release(record_set);
        record_set = NULL;

        TEST_ASSERT_MESSAGE(MA_OK == db_get_policy_product_assignments(db,
                                                                       policies[counter].test_po_id,
                                                                       NULL,
                                                                       NULL,
                                                                       NULL,
                                                                       &record_set), "db_get_policy_product_assignments failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_NO_MORE_ITEMS == ma_db_recordset_next(record_set), \
                                                                           "Records corresponding to Policy are not removed from Product Assignment table");

        ma_db_recordset_release(record_set);
        record_set = NULL;
    }
}



/******************************************************************************
Test for verifying the Policy Settings addition/removal/retrieval APIs.
******************************************************************************/
TEST(policy_db_tests, policy_settings_test)
{
    ma_table_t *first_level_table = NULL;
    ma_table_t *second_level_table = NULL;
    ma_variant_t *variant_ptr = NULL;
    ma_int32_t counter = 0;

    /* Creating the Policy information. Policy information remains common for all
       Policies. Converting this complete information into a variant (Table of Tables).

    <Section name="Server_Alerting">
        <Setting name="bCleanFiles">1</Setting>
        <Setting name="bDeleteFiles">0</Setting>
        <Setting name="bExcludeCookies">1</Setting>
        <Setting name="szDialogMessage">VirusScan Alert!</Setting>
    </Section>
    <Section name="Server_Blocking">
        <Setting name="VSIDBlock">1</Setting>
        <Setting name="VSIDBlockOnNonVirus">0</Setting>
        <Setting name="VSIDBlockTimeout">10</Setting>
    </Section>
    */
    ma_table_create(&first_level_table);

    /* Section - 1 */
    ma_table_create(&second_level_table);

    ma_variant_create_from_string("1", strlen("1"), &variant_ptr);
    ma_table_add_entry(second_level_table, "bCleanFiles", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_string("0", strlen("0"), &variant_ptr);
    ma_table_add_entry(second_level_table, "bDeleteFiles", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_string("1", strlen("1"), &variant_ptr);
    ma_table_add_entry(second_level_table, "bExcludeCookies", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_string("VirusScan Alert!", strlen("VirusScan Alert!"), &variant_ptr);
    ma_table_add_entry(second_level_table, "szDialogMessage", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_table(second_level_table, &variant_ptr);
    ma_table_add_entry(first_level_table, "Server_Alerting", variant_ptr);
    ma_table_release(second_level_table); second_level_table = NULL;
    ma_variant_release(variant_ptr);

    /* Section - 1 */
    ma_table_create(&second_level_table);

    ma_variant_create_from_string("1", strlen("1"), &variant_ptr);
    ma_table_add_entry(second_level_table, "VSIDBlock", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_string("0", strlen("0"), &variant_ptr);
    ma_table_add_entry(second_level_table, "VSIDBlockOnNonVirus", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_string("10", strlen("10"), &variant_ptr);
    ma_table_add_entry(second_level_table, "VSIDBlockTimeout", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_table(second_level_table, &variant_ptr);
    ma_table_add_entry(first_level_table, "Server_Blocking", variant_ptr);
    ma_table_release(second_level_table);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_table(first_level_table, &variant_ptr);
    ma_table_release(first_level_table);

    // Adding Policy Settings. Note - This test verifies the Policy Settings APIs
    // independently. Therefore, it is not necessary that corresponding policy records
    // should exist in the DB.
    for(counter = 0; counter < sizeof(policy_settings)/sizeof(policy_settings[0]); counter++)
    {
        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_add_policy_settings_object(NULL,
                                                                                 policy_settings[counter].test_po_id,
                                                                                 policy_settings[counter].test_pso_id,
                                                                                 policy_settings[counter].test_pso_name,
                                                                                 policy_settings[counter].test_pso_digest,
                                                                                 variant_ptr,
                                                                                 policy_settings[counter].test_pso_param_int,
                                                                                 policy_settings[counter].test_pso_param_str,
                                                                                 policy_settings[counter].test_time_stamp), "db_add_policy_settings_object failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_add_policy_settings_object(db,
                                                                                 NULL,
                                                                                 policy_settings[counter].test_pso_id,
                                                                                 policy_settings[counter].test_pso_name,
                                                                                 policy_settings[counter].test_pso_digest,
                                                                                 variant_ptr,
                                                                                 policy_settings[counter].test_pso_param_int,
                                                                                 policy_settings[counter].test_pso_param_str,
                                                                                 policy_settings[counter].test_time_stamp), "db_add_policy_settings_object failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_add_policy_settings_object(db,
                                                                                 policy_settings[counter].test_po_id,
                                                                                 NULL,
                                                                                 policy_settings[counter].test_pso_name,
                                                                                 policy_settings[counter].test_pso_digest,
                                                                                 variant_ptr,
                                                                                 policy_settings[counter].test_pso_param_int,
                                                                                 policy_settings[counter].test_pso_param_str,
                                                                                 policy_settings[counter].test_time_stamp), "db_add_policy_settings_object failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_add_policy_settings_object(db,
                                                                                 policy_settings[counter].test_po_id,
                                                                                 policy_settings[counter].test_pso_id,
                                                                                 NULL,
                                                                                 policy_settings[counter].test_pso_digest,
                                                                                 variant_ptr,
                                                                                 policy_settings[counter].test_pso_param_int,
                                                                                 policy_settings[counter].test_pso_param_str,
                                                                                 policy_settings[counter].test_time_stamp), "db_add_policy_settings_object failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_add_policy_settings_object(db,
                                                                                 policy_settings[counter].test_po_id,
                                                                                 policy_settings[counter].test_pso_id,
                                                                                 policy_settings[counter].test_pso_name,
                                                                                 NULL,
                                                                                 variant_ptr,
                                                                                 policy_settings[counter].test_pso_param_int,
                                                                                 policy_settings[counter].test_pso_param_str,
                                                                                 policy_settings[counter].test_time_stamp), "db_add_policy_settings_object failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_add_policy_settings_object(db,
                                                                                 policy_settings[counter].test_po_id,
                                                                                 policy_settings[counter].test_pso_id,
                                                                                 policy_settings[counter].test_pso_name,
                                                                                 policy_settings[counter].test_pso_digest,
                                                                                 NULL,
                                                                                 policy_settings[counter].test_pso_param_int,
                                                                                 policy_settings[counter].test_pso_param_str,
                                                                                 policy_settings[counter].test_time_stamp), "db_add_policy_settings_object failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_add_policy_settings_object(db,
                                                                                 policy_settings[counter].test_po_id,
                                                                                 policy_settings[counter].test_pso_id,
                                                                                 policy_settings[counter].test_pso_name,
                                                                                 policy_settings[counter].test_pso_digest,
                                                                                 variant_ptr,
                                                                                 policy_settings[counter].test_pso_param_int,
                                                                                 policy_settings[counter].test_pso_param_str,
                                                                                 NULL), "db_add_policy_settings_object failed");

        TEST_ASSERT_MESSAGE(MA_OK == db_add_policy_settings_object(db,
                                                                   policy_settings[counter].test_po_id,
                                                                   policy_settings[counter].test_pso_id,
                                                                   policy_settings[counter].test_pso_name,
                                                                   policy_settings[counter].test_pso_digest,
                                                                   variant_ptr,
                                                                   policy_settings[counter].test_pso_param_int,
                                                                   policy_settings[counter].test_pso_param_str,
                                                                   policy_settings[counter].test_time_stamp), "db_add_policy_settings_object failed");
    }

    // Policy data (stored in the Variant) is added to DB, so variant can be released now.
    ma_variant_release(variant_ptr);

    // Verifying the Policy Settings (in reverse order)
    for(counter = sizeof(policy_settings)/sizeof(policy_settings[0]) - 1; counter >= 0; counter--)
    {
        ma_db_recordset_t *record_set = NULL;
        const char *record_col_values = NULL;

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_get_policy_settings_objects(NULL,
                                                                                  policy_settings[counter].test_po_id,
                                                                                  policy_settings[counter].test_pso_id,
                                                                                  &record_set),
                                                                                  "db_get_policy_settings_objects failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_get_policy_settings_objects(db,
                                                                                  policy_settings[counter].test_po_id,
                                                                                  policy_settings[counter].test_pso_id,
                                                                                  NULL),
                                                                                  "db_get_policy_settings_objects failed");

        TEST_ASSERT_MESSAGE(MA_OK == db_get_policy_settings_objects(db,
                                                                    policy_settings[counter].test_po_id,
                                                                    policy_settings[counter].test_pso_id,
                                                                    &record_set),
                                                                    "db_get_policy_settings_objects failed");

        while(MA_ERROR_NO_MORE_ITEMS != ma_db_recordset_next(record_set))
        {
            ma_db_recordset_get_string(record_set, 1, &record_col_values);       /* 1st column is Policy ID */
            TEST_ASSERT_MESSAGE(!strcmp(record_col_values, policy_settings[counter].test_po_id), "Failed to retrieve correct policy id");

            ma_db_recordset_get_string(record_set, 2, &record_col_values);       /* 2nd column is Policy Settings ID */
            TEST_ASSERT_MESSAGE(!strcmp(record_col_values, policy_settings[counter].test_pso_id), "Failed to retrieve correct policy settings id");

            ma_db_recordset_get_string(record_set, 3, &record_col_values);       /* 3rd column is Policy settings digest */
            TEST_ASSERT_MESSAGE(!strcmp(record_col_values, policy_settings[counter].test_pso_digest), "Failed to retrieve correct policy settings digest");
        }

        ma_db_recordset_release(record_set);
        record_set = NULL;
    }

    // Removing elements matching a particular (Policy ID + Policy Settings ID) combination.
    // Then calling the db_get_policy_settings_objects API using same combination. No records
    // should be retrieved.
    for(counter = 0; counter < sizeof(policy_settings)/sizeof(policy_settings[0]); counter++)
    {
        ma_db_recordset_t *record_set = NULL;

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_remove_policy_settings_object(NULL,
                                                                                    policy_settings[counter].test_po_id,
                                                                                    policy_settings[counter].test_pso_id), "db_remove_policy_settings_object failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_remove_policy_settings_object(db,
                                                                                    NULL,
                                                                                    policy_settings[counter].test_pso_id), "db_remove_policy_settings_object failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_remove_policy_settings_object(db,
                                                                                    policy_settings[counter].test_po_id,
                                                                                    NULL), "db_remove_policy_settings_object failed");

        TEST_ASSERT_MESSAGE(MA_OK == db_remove_policy_settings_object(db,
                                                                      policy_settings[counter].test_po_id,
                                                                      policy_settings[counter].test_pso_id), "db_remove_policy_settings_object failed");

        TEST_ASSERT_MESSAGE(MA_OK == db_get_policy_settings_objects(db,
                                                                    policy_settings[counter].test_po_id,
                                                                    policy_settings[counter].test_pso_id,
                                                                    &record_set),
                                                                    "db_get_policy_settings_objects failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_NO_MORE_ITEMS == ma_db_recordset_next(record_set), "All matching records couldn't be removed");

        ma_db_recordset_release(record_set);
        record_set = NULL;
    }
}


/******************************************************************************
Test for verifying the Policy assignment addition/removal/retrieval APIs.
******************************************************************************/
TEST(policy_db_tests, policy_assignment_test)
{
    ma_int32_t counter = 0;

    // Adding Product Assignment. Note - This test verifies the Product Assignment APIs
    // independently. Therefore, it is not necessary that corresponding policy records
    // should exist in the DB.
    for(counter = 0; counter < sizeof(policy_product)/sizeof(policy_product[0]); counter++)
    {
        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_add_policy_product_assignment(NULL,
                                                                                    policy_product[counter].test_po_id,
                                                                                    policy_product[counter].test_product,
                                                                                    policy_product[counter].test_method,
                                                                                    policy_product[counter].test_param), "db_add_policy_product_assignment failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_add_policy_product_assignment(db,
                                                                                    NULL,
                                                                                    policy_product[counter].test_product,
                                                                                    policy_product[counter].test_method,
                                                                                    policy_product[counter].test_param), "db_add_policy_product_assignment failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_add_policy_product_assignment(db,
                                                                                    policy_product[counter].test_po_id,
                                                                                    NULL,
                                                                                    policy_product[counter].test_method,
                                                                                    policy_product[counter].test_param), "db_add_policy_product_assignment failed");

        TEST_ASSERT_MESSAGE(MA_OK == db_add_policy_product_assignment(db,
                                                                      policy_product[counter].test_po_id,
                                                                      policy_product[counter].test_product,
                                                                      policy_product[counter].test_method,
                                                                      policy_product[counter].test_param), "db_add_policy_product_assignment failed");
    }

    // Verifying the Product Assignment (in reverse order)
    for(counter = sizeof(policy_product)/sizeof(policy_product[0]) - 1; counter >= 0; counter--)
    {
        ma_db_recordset_t *record_set = NULL;
        const char *record_col_values = NULL;

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_get_policy_product_assignments(NULL,
                                                                                     policy_product[counter].test_po_id,
                                                                                     policy_product[counter].test_product,
                                                                                     policy_product[counter].test_method,
                                                                                     policy_product[counter].test_param,
                                                                                     &record_set),
                                                                                     "db_get_policy_product_assignments failed");

        TEST_ASSERT_MESSAGE(MA_OK == db_get_policy_product_assignments(db,
                                                                       policy_product[counter].test_po_id,
                                                                       policy_product[counter].test_product,
                                                                       policy_product[counter].test_method,
                                                                       policy_product[counter].test_param,
                                                                       &record_set), "db_get_policy_product_assignments failed");

        while(MA_ERROR_NO_MORE_ITEMS != ma_db_recordset_next(record_set))
        {
            ma_db_recordset_get_string(record_set, 1, &record_col_values);       /* 1st column is Policy ID */
            TEST_ASSERT_MESSAGE(!strcmp(record_col_values, policy_product[counter].test_po_id), "Failed to retrieve correct policy id");

            ma_db_recordset_get_string(record_set, 2, &record_col_values);       /* 2nd column is Product ID */
            TEST_ASSERT_MESSAGE(!strcmp(record_col_values, policy_product[counter].test_product), "Failed to retrieve correct product id");

            ma_db_recordset_get_string(record_set, 3, &record_col_values);       /* 3rd column is method */
            if(policy_product[counter].test_method == NULL)
            {
                TEST_ASSERT_MESSAGE(!strcmp(record_col_values, "0"), "Failed to retrieve correct policy method");
            }
            else
            {
                TEST_ASSERT_MESSAGE(!strcmp(record_col_values, policy_product[counter].test_method), "Failed to retrieve correct policy method");
            }

            ma_db_recordset_get_string(record_set, 4, &record_col_values);       /* 4th column is param */
            if(policy_product[counter].test_param == NULL)
            {
                TEST_ASSERT_MESSAGE(!strcmp(record_col_values, "0"), "Failed to retrieve correct policy parameters");
            }
            else
            {
                TEST_ASSERT_MESSAGE(!strcmp(record_col_values, policy_product[counter].test_param), "Failed to retrieve correct policy parameters");
            }
        }

        ma_db_recordset_release(record_set);
        record_set = NULL;
    }

    // Verifying the Product Assignment (multiple entries). Scenario - Retrieve all elements
    // matching a particular Policy ID (where method and parameter values are NULL).
    for(counter = 0; counter < sizeof(policy_product)/sizeof(policy_product[0]); counter++)
    {
        ma_db_recordset_t *record_set = NULL;
        ma_int32_t counter_1 = 0;

        TEST_ASSERT_MESSAGE(MA_OK == db_get_policy_product_assignments(db,
                                                                       policy_product[counter].test_po_id,
                                                                       NULL,
                                                                       NULL,
                                                                       NULL,
                                                                       &record_set), "db_get_policy_product_assignments failed");

        // Finding items which should match, then pulling one element from the record set DB
        // and verifying, that the element stored in DB is correct.
        for(counter_1 = 0; counter_1 < sizeof(policy_product)/sizeof(policy_product[0]); counter_1++)
        {
            if((strcmp(policy_product[counter].test_po_id, policy_product[counter_1].test_po_id) == 0) &&
               (!policy_product[counter_1].test_method) && (!policy_product[counter_1].test_param))
            {
                exp_policy_assignment_record_list[counter_1] = 1;
            }
        }

        while(MA_ERROR_NO_MORE_ITEMS != ma_db_recordset_next(record_set))
        {
            char *policy_id = NULL;
            char *product = NULL;
            char *method = NULL;
            char *param = NULL;
            ma_db_recordset_get_string(record_set, 1, &policy_id);
            ma_db_recordset_get_string(record_set, 2, &product);
            ma_db_recordset_get_string(record_set, 3, &method);
            ma_db_recordset_get_string(record_set, 4, &param);

            TEST_ASSERT_MESSAGE(do_policy_assignment_matching(policy_id, product, method, param) == 1, \
                                "Unintended record retrieved from DB");
        }

        for(counter_1 = 0; counter_1 < sizeof(exp_policy_assignment_record_list)/sizeof(exp_policy_assignment_record_list[0]); counter_1++)
        {
            TEST_ASSERT_MESSAGE(exp_policy_assignment_record_list[counter_1] == 0, "One expected record not retrieved by query");
        }

        ma_db_recordset_release(record_set);
        record_set = NULL;
    }

    // Verifying the Product Assignment (multiple entries). Scenario - Retrieve all
    // elements matching a particular (Policy ID + Product ID). All records matching
    // the (Policy ID + Product ID) and 'method' and 'param' values as NULL should be
    // retrieved.
    for(counter = 0; counter < sizeof(policy_product)/sizeof(policy_product[0]); counter++)
    {
        ma_db_recordset_t *record_set = NULL;
        ma_int32_t counter_1 = 0;

        TEST_ASSERT_MESSAGE(MA_OK == db_get_policy_product_assignments(db,
                                                                       policy_product[counter].test_po_id,
                                                                       policy_product[counter].test_product,
                                                                       NULL,
                                                                       NULL,
                                                                       &record_set), "db_get_policy_product_assignments failed");

        // Finding items which should match, then pulling one element from the record set DB
        // and verifying, that the element stored in DB is correct.
        for(counter_1 = 0; counter_1 < sizeof(policy_product)/sizeof(policy_product[0]); counter_1++)
        {
            if((strcmp(policy_product[counter].test_po_id, policy_product[counter_1].test_po_id) == 0) &&
               (strcmp(policy_product[counter].test_product, policy_product[counter_1].test_product) == 0) &&
               (policy_product[counter_1].test_method == NULL) &&
               (policy_product[counter_1].test_param == NULL) )
            {
                exp_policy_assignment_record_list[counter_1] = 1;
            }
        }

        while(MA_ERROR_NO_MORE_ITEMS != ma_db_recordset_next(record_set))
        {
            char *policy_id = NULL;
            char *product = NULL;
            char *method = NULL;
            char *param = NULL;
            ma_db_recordset_get_string(record_set, 1, &policy_id);
            ma_db_recordset_get_string(record_set, 2, &product);
            ma_db_recordset_get_string(record_set, 3, &method);
            ma_db_recordset_get_string(record_set, 4, &param);

            TEST_ASSERT_MESSAGE(do_policy_assignment_matching(policy_id, product, method, param) == 1, \
                                "Unintended record retrieved from DB");
        }

        for(counter_1 = 0; counter_1 < sizeof(exp_policy_assignment_record_list)/sizeof(exp_policy_assignment_record_list[0]); counter_1++)
        {
            TEST_ASSERT_MESSAGE(exp_policy_assignment_record_list[counter_1] == 0, "One expected record not retrieved by query");
        }

        ma_db_recordset_release(record_set);
        record_set = NULL;
    }

    // Removing elements matching a particular (Policy ID + Product ID) combination.
    // Then calling the db_get_policy_product_assignments API using same combination.
    // No records should be retrieved.
    for(counter = 0; counter < sizeof(policy_product)/sizeof(policy_product[0]); counter++)
    {
        ma_db_recordset_t *record_set = NULL;

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_remove_policy_product_assignment(NULL,
                                                                                       policy_product[counter].test_po_id,
                                                                                       policy_product[counter].test_product,
                                                                                       NULL,
                                                                                       NULL), "db_remove_policy_product_assignment failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_remove_policy_product_assignment(db,
                                                                                       NULL,
                                                                                       policy_product[counter].test_product,
                                                                                       NULL,
                                                                                       NULL), "db_remove_policy_product_assignment failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_remove_policy_product_assignment(db,
                                                                                       policy_product[counter].test_po_id,
                                                                                       NULL,
                                                                                       NULL,
                                                                                       NULL), "db_remove_policy_product_assignment failed");

        TEST_ASSERT_MESSAGE(MA_OK == db_remove_policy_product_assignment(db,
                                                                         policy_product[counter].test_po_id,
                                                                         policy_product[counter].test_product,
                                                                         NULL,
                                                                         NULL), "db_remove_policy_product_assignment failed");

        TEST_ASSERT_MESSAGE(MA_OK == db_get_policy_product_assignments(db,
                                                                       policy_product[counter].test_po_id,
                                                                       policy_product[counter].test_product,
                                                                       NULL,
                                                                       NULL,
                                                                       &record_set), "db_get_policy_product_assignments failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_NO_MORE_ITEMS == ma_db_recordset_next(record_set), "All matching records couldn't be removed");

        ma_db_recordset_release(record_set);
        record_set = NULL;
    }
}


/******************************************************************************
Test for verifying the db_get_product_policies API.
******************************************************************************/
TEST(policy_db_tests, get_product_policies_test)
{
    ma_table_t *first_level_table = NULL;
    ma_table_t *second_level_table = NULL;
    ma_variant_t *variant_ptr = NULL;
    ma_int32_t counter = 0;

    /* Creating the Policy information. Policy information remains common for all
       Policies. Converting this complete information into a variant (Table of Tables).

    <Section name="Server_Alerting">
        <Setting name="bCleanFiles">1</Setting>
        <Setting name="bDeleteFiles">0</Setting>
        <Setting name="bExcludeCookies">1</Setting>
        <Setting name="szDialogMessage">VirusScan Alert!</Setting>
    </Section>
    <Section name="Server_Blocking">
        <Setting name="VSIDBlock">1</Setting>
        <Setting name="VSIDBlockOnNonVirus">0</Setting>
        <Setting name="VSIDBlockTimeout">10</Setting>
    </Section>
    */
    ma_table_create(&first_level_table);

    /* Section - 1 */
    ma_table_create(&second_level_table);

    ma_variant_create_from_string("1", strlen("1"), &variant_ptr);
    ma_table_add_entry(second_level_table, "bCleanFiles", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_string("0", strlen("0"), &variant_ptr);
    ma_table_add_entry(second_level_table, "bDeleteFiles", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_string("1", strlen("1"), &variant_ptr);
    ma_table_add_entry(second_level_table, "bExcludeCookies", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_string("VirusScan Alert!", strlen("VirusScan Alert!"), &variant_ptr);
    ma_table_add_entry(second_level_table, "szDialogMessage", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_table(second_level_table, &variant_ptr);
    ma_table_add_entry(first_level_table, "Server_Alerting", variant_ptr);
    ma_table_release(second_level_table); second_level_table = NULL;
    ma_variant_release(variant_ptr);

    /* Section - 2 */
    ma_table_create(&second_level_table);

    ma_variant_create_from_string("1", strlen("1"), &variant_ptr);
    ma_table_add_entry(second_level_table, "VSIDBlock", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_string("0", strlen("0"), &variant_ptr);
    ma_table_add_entry(second_level_table, "VSIDBlockOnNonVirus", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_string("10", strlen("10"), &variant_ptr);
    ma_table_add_entry(second_level_table, "VSIDBlockTimeout", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_table(second_level_table, &variant_ptr);
    ma_table_add_entry(first_level_table, "Server_Blocking", variant_ptr);
    ma_table_release(second_level_table);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_table(first_level_table, &variant_ptr);
    ma_table_release(first_level_table);

    // Adding Policies to the DB
    for(counter = 0; counter < sizeof(policies)/sizeof(policies[0]); counter++)
    {
        TEST_ASSERT_MESSAGE(MA_OK == db_add_policy_object(db,
                                                          policies[counter].test_po_id,
                                                          policies[counter].test_po_name,
                                                          policies[counter].test_po_feature,
                                                          policies[counter].test_po_category,
                                                          policies[counter].test_po_type,
                                                          policies[counter].test_po_digest,
                                                          policies[counter].test_po_legacy), "db_add_policy_object failed");
    }

    // Adding Policy Settings. Note - All policy IDs should exist. This test doesn't
    // verify the scenario in which a policy ID, which doesn't exist, is added
    // to the DB.
    for(counter = 0; counter < sizeof(policy_settings)/sizeof(policy_settings[0]); counter++)
    {
        TEST_ASSERT_MESSAGE(MA_OK == db_add_policy_settings_object(db,
                                                                   policy_settings[counter].test_po_id,
                                                                   policy_settings[counter].test_pso_id,
                                                                   policy_settings[counter].test_pso_name,
                                                                   policy_settings[counter].test_pso_digest,
                                                                   variant_ptr,
                                                                   policy_settings[counter].test_pso_param_int,
                                                                   policy_settings[counter].test_pso_param_str,
                                                                   policy_settings[counter].test_time_stamp), "db_add_policy_settings_object failed");
    }

    // Policy data (stored in the Variant) is added to DB, so variant can be released now.
    ma_variant_release(variant_ptr);

    // Adding Product Assignments to DB. Note - All policy IDs should exist. This test
    // doesn't verify the scenario in which a policy ID, which doesn't exist, is added
    // to the DB.
    for(counter = 0; counter < sizeof(policy_product)/sizeof(policy_product[0]); counter++)
    {
        TEST_ASSERT_MESSAGE(MA_OK == db_add_policy_product_assignment(db,
                                                                      policy_product[counter].test_po_id,
                                                                      policy_product[counter].test_product,
                                                                      policy_product[counter].test_method,
                                                                      policy_product[counter].test_param), "db_add_policy_product_assignment failed");
    }

    // Test for retrieving all policy records matching one particular Product ID, method = NULL, 'param' = NULL.
    for(counter = 0; counter < sizeof(policy_product)/sizeof(policy_product[0]); counter++)
    {
        ma_db_recordset_t *record_set = NULL;
        ma_int32_t counter_1 = 0;

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_get_product_policies(NULL,
                                                                           policy_product[counter].test_product,
                                                                           NULL,
                                                                           NULL,
                                                                           NULL,
                                                                           &record_set), "db_get_product_policies failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_get_product_policies(db,
                                                                           NULL,
                                                                           NULL,
                                                                           NULL,
                                                                           NULL,
                                                                           &record_set), "db_get_product_policies failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_get_product_policies(db,
                                                                           policy_product[counter].test_product,
                                                                           NULL,
                                                                           NULL,
                                                                           NULL,
                                                                           NULL), "db_get_product_policies failed");

        TEST_ASSERT_MESSAGE(MA_OK == db_get_product_policies(db,
                                                             policy_product[counter].test_product,
                                                             NULL,
                                                             NULL,
                                                             NULL,
                                                             &record_set), "db_get_product_policies failed");

        // Finding items which should match, then pulling one element from the record set DB
        // and verifying, that the element stored in DB is correct.
        for(counter_1 = 0; counter_1 < sizeof(policy_product)/sizeof(policy_product[0]); counter_1++)
        {
            if((strcmp(policy_product[counter].test_product, policy_product[counter_1].test_product) == 0)
              )
            {
                ma_int32_t counter_2 = 0;

                for(counter_2 = 0; counter_2 < sizeof(policies)/sizeof(policies[0]); counter_2++)
                {
                    if(strcmp(policy_product[counter_1].test_po_id, policies[counter_2].test_po_id) == 0)
                    {
                        exp_product_matching_record_list[counter_2] = 1;
                    }
                }
            }
        }

        while(MA_ERROR_NO_MORE_ITEMS != ma_db_recordset_next(record_set))
        {
            char *po_feature = NULL;
            char *po_category = NULL;
            char *po_type = NULL;
            char *pso_name = NULL;
            ma_db_recordset_get_string(record_set, 1, &po_feature);
            ma_db_recordset_get_string(record_set, 2, &po_category);
            ma_db_recordset_get_string(record_set, 3, &po_type);
            ma_db_recordset_get_string(record_set, 4, &pso_name);

            TEST_ASSERT_MESSAGE(do_policy_product_matching(po_feature, po_category, po_type, pso_name) == 1, \
                                "Unintended record retrieved from DB");
        }

        for(counter_1 = 0; counter_1 < sizeof(exp_product_matching_record_list)/sizeof(exp_product_matching_record_list[0]); counter_1++)
        {
            TEST_ASSERT_MESSAGE(exp_product_matching_record_list[counter_1] == 0, "One expected record not retrieved by query");
        }

        ma_db_recordset_release(record_set);
        record_set = NULL;
    }
}

/******************************************************************************
Test for verifying the db_get_all_products API.

Note - This particular test uses hard-coded test values, therefore, any change
in test data may require the change in this test case too.
******************************************************************************/
TEST(policy_db_tests, get_all_products_test)
{
    // Test for retrieving all Product IDs.
    ma_int32_t counter = 0;
    ma_db_recordset_t *record_set = NULL;
    const char *record_col_values = NULL;

    // Adding Policies to the DB
    for(counter = 0; counter < sizeof(policies)/sizeof(policies[0]); counter++)
    {
        TEST_ASSERT_MESSAGE(MA_OK == db_add_policy_object(db,
                                                          policies[counter].test_po_id,
                                                          policies[counter].test_po_name,
                                                          policies[counter].test_po_feature,
                                                          policies[counter].test_po_category,
                                                          policies[counter].test_po_type,
                                                          policies[counter].test_po_digest,
                                                          policies[counter].test_po_legacy), "db_add_policy_object failed");
    }

    // Policy Settings information is not required for this test.

    // Adding Product Assignments to DB. Note - All policy IDs should exist. This test
    // doesn't verify the scenario in which a policy ID, which doesn't exist, is added
    // to the DB.
    for(counter = 0; counter < sizeof(policy_product)/sizeof(policy_product[0]); counter++)
    {
        TEST_ASSERT_MESSAGE(MA_OK == db_add_policy_product_assignment(db,
                                                                      policy_product[counter].test_po_id,
                                                                      policy_product[counter].test_product,
                                                                      policy_product[counter].test_method,
                                                                      policy_product[counter].test_param), "db_add_policy_product_assignment failed");
    }

    TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_get_all_products(NULL, 1, 
                                                                   &record_set), "db_get_all_products failed");

    TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_get_all_products(db, 1,
                                                                   NULL), "db_get_all_products failed");

    TEST_ASSERT_MESSAGE(MA_OK == db_get_all_products(db, 1, &record_set), "db_get_all_products failed");


    // "Product 1" and "Product 2" match, where, corresponding policy name = "__EPO_ENFORCE_YES__"
    TEST_ASSERT(MA_OK == ma_db_recordset_next(record_set))
    ma_db_recordset_get_string(record_set, 1, &record_col_values);
    TEST_ASSERT(0 == strcmp("Product 1", record_col_values));

    TEST_ASSERT(MA_OK == ma_db_recordset_next(record_set))
    ma_db_recordset_get_string(record_set, 1, &record_col_values);
    TEST_ASSERT(0 == strcmp("Product 2", record_col_values));

    TEST_ASSERT(MA_ERROR_NO_MORE_ITEMS == ma_db_recordset_next(record_set));

    ma_db_recordset_release(record_set);
    record_set = NULL;
}


/******************************************************************************
Test for db_get_policy_setting_object_id API.

Note - This particular test uses hard-coded test values, therefore, nay change
in test data may require the change in this test case too.
******************************************************************************/
TEST(policy_db_tests, get_policy_setting_object_id_test)
{
    ma_table_t *first_level_table = NULL;
    ma_table_t *second_level_table = NULL;
    ma_variant_t *variant_ptr = NULL;
    ma_int32_t counter = 0;

    /* Creating the Policy information. Policy information remains common for all
       Policies. Converting this complete information into a variant (Table of Tables).

    <Section name="Server_Alerting">
        <Setting name="bCleanFiles">1</Setting>
        <Setting name="bDeleteFiles">0</Setting>
        <Setting name="bExcludeCookies">1</Setting>
        <Setting name="szDialogMessage">VirusScan Alert!</Setting>
    </Section>
    <Section name="Server_Blocking">
        <Setting name="VSIDBlock">1</Setting>
        <Setting name="VSIDBlockOnNonVirus">0</Setting>
        <Setting name="VSIDBlockTimeout">10</Setting>
    </Section>
    */
    ma_table_create(&first_level_table);

    /* Section - 1 */
    ma_table_create(&second_level_table);

    ma_variant_create_from_string("1", strlen("1"), &variant_ptr);
    ma_table_add_entry(second_level_table, "bCleanFiles", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_string("0", strlen("0"), &variant_ptr);
    ma_table_add_entry(second_level_table, "bDeleteFiles", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_string("1", strlen("1"), &variant_ptr);
    ma_table_add_entry(second_level_table, "bExcludeCookies", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_string("VirusScan Alert!", strlen("VirusScan Alert!"), &variant_ptr);
    ma_table_add_entry(second_level_table, "szDialogMessage", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_table(second_level_table, &variant_ptr);
    ma_table_add_entry(first_level_table, "Server_Alerting", variant_ptr);
    ma_table_release(second_level_table); second_level_table = NULL;
    ma_variant_release(variant_ptr);

    /* Section - 2 */
    ma_table_create(&second_level_table);

    ma_variant_create_from_string("1", strlen("1"), &variant_ptr);
    ma_table_add_entry(second_level_table, "VSIDBlock", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_string("0", strlen("0"), &variant_ptr);
    ma_table_add_entry(second_level_table, "VSIDBlockOnNonVirus", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_string("10", strlen("10"), &variant_ptr);
    ma_table_add_entry(second_level_table, "VSIDBlockTimeout", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_table(second_level_table, &variant_ptr);
    ma_table_add_entry(first_level_table, "Server_Blocking", variant_ptr);
    ma_table_release(second_level_table);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_table(first_level_table, &variant_ptr);
    ma_table_release(first_level_table);

    // Adding Policies to the DB
    for(counter = 0; counter < sizeof(policies)/sizeof(policies[0]); counter++)
    {
        TEST_ASSERT_MESSAGE(MA_OK == db_add_policy_object(db,
                                                          policies[counter].test_po_id,
                                                          policies[counter].test_po_name,
                                                          policies[counter].test_po_feature,
                                                          policies[counter].test_po_category,
                                                          policies[counter].test_po_type,
                                                          policies[counter].test_po_digest,
                                                          policies[counter].test_po_legacy), "db_add_policy_object failed");
    }

    // Adding Policy Settings to DB. Note - All policy IDs should exist. This test
    // doesn't verify the scenario in which a policy ID, which doesn't exist, is
    // added to the DB.
    for(counter = 0; counter < sizeof(policy_settings)/sizeof(policy_settings[0]); counter++)
    {
        TEST_ASSERT_MESSAGE(MA_OK == db_add_policy_settings_object(db,
                                                                   policy_settings[counter].test_po_id,
                                                                   policy_settings[counter].test_pso_id,
                                                                   policy_settings[counter].test_pso_name,
                                                                   policy_settings[counter].test_pso_digest,
                                                                   variant_ptr,
                                                                   policy_settings[counter].test_pso_param_int,
                                                                   policy_settings[counter].test_pso_param_str,
                                                                   policy_settings[counter].test_time_stamp), "db_add_policy_settings_object failed");
    }

    // Policy data (stored in the Variant) is added to DB, so variant can be released now.
    ma_variant_release(variant_ptr);

    // Adding Product Assignments to DB. Note - All policy IDs should exist. This test
    // doesn't verify the scenario in which a policy ID, which doesn't exist, is added
    // to the DB.
    for(counter = 0; counter < sizeof(policy_product)/sizeof(policy_product[0]); counter++)
    {
        TEST_ASSERT_MESSAGE(MA_OK == db_add_policy_product_assignment(db,
                                                                      policy_product[counter].test_po_id,
                                                                      policy_product[counter].test_product,
                                                                      policy_product[counter].test_method,
                                                                      policy_product[counter].test_param), "db_add_policy_product_assignment failed");
    }

    // Retrieving the policy setting object ID matching a particular product, feature, category and type
    // "Product 1, "Feature 1", "Category 4", "Default"
    {
        ma_db_recordset_t *record_set = NULL;
        const char *record_col_values = NULL;

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_get_policy_setting_object_id(NULL,
                                                                                   "Product 1",
                                                                                   "Feature 1",
                                                                                   "Category 4",
                                                                                   "Default",
																				   NULL,
																				   NULL,
																				   NULL,
																				   NULL,
                                                                                   &record_set),
                                                                                   "db_get_policy_setting_object_id failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_get_policy_setting_object_id(db,
                                                                                   NULL,
                                                                                   "Feature 1",
                                                                                   "Category 4",
                                                                                   "Default",
																				    NULL,
																				   NULL,
																				   NULL,
																				   NULL,
                                                                                   &record_set),
                                                                                   "db_get_policy_setting_object_id failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_get_policy_setting_object_id(db,
                                                                                   "Product 1",
                                                                                   NULL,
                                                                                   "Category 4",
                                                                                   "Default",
																				    NULL,
																				   NULL,
																				   NULL,
																				   NULL,
                                                                                   &record_set),
                                                                                   "db_get_policy_setting_object_id failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_get_policy_setting_object_id(db,
                                                                                   "Product 1",
                                                                                   "Feature 1",
                                                                                   NULL,
                                                                                   "Default",
																				    NULL,
																				   NULL,
																				   NULL,
																				   NULL,
                                                                                   &record_set),
                                                                                   "db_get_policy_setting_object_id failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_get_policy_setting_object_id(db,
                                                                                   "Product 1",
                                                                                   "Feature 1",
                                                                                   "Category 4",
                                                                                   NULL,
																				    NULL,
																				   NULL,
																				   NULL,
																				   NULL,
                                                                                   &record_set),
                                                                                   "db_get_policy_setting_object_id failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_get_policy_setting_object_id(db,
                                                                                   "Product 1",
                                                                                   "Feature 1",
                                                                                   "Category 4",
                                                                                   "Default",
																				    NULL,
																				   NULL,
																				   NULL,
																				   NULL,
                                                                                   NULL),
                                                                                   "db_get_policy_setting_object_id failed");

        TEST_ASSERT_MESSAGE(MA_OK == db_get_policy_setting_object_id(db, "Product 1", "Feature 1", "Category 4", "Default",NULL, NULL, NULL, NULL, &record_set),
                                                                     "db_get_policy_setting_object_id failed");

        // Only one record retrieved
        TEST_ASSERT(MA_OK == ma_db_recordset_next(record_set))
        ma_db_recordset_get_string(record_set, 1, &record_col_values);
        TEST_ASSERT(0 == strcmp("600", record_col_values));

        TEST_ASSERT(MA_ERROR_NO_MORE_ITEMS == ma_db_recordset_next(record_set));

        ma_db_recordset_release(record_set);
        record_set = NULL;
    }

    // Retrieving the policy setting object ID matching a particular product, feature, category and type
    // "Product 1, "Feature 4", "Category 1", "Normal",
    {
        ma_db_recordset_t *record_set = NULL;
        char *record_col_values = NULL;

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_get_policy_setting_object_id(NULL,
                                                                                   "Product 1",
                                                                                   "Feature 4",
                                                                                   "Category 1",
                                                                                   "Normal",
																				    NULL,
																				   NULL,
																				   NULL,
																				   NULL,
                                                                                   &record_set),
                                                                                   "db_get_policy_setting_object_id failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_get_policy_setting_object_id(db,
                                                                                   NULL,
                                                                                   "Feature 4",
                                                                                   "Category 1",
                                                                                   "Normal",
																				    NULL,
																				   NULL,
																				   NULL,
																				   NULL,
                                                                                   &record_set),
                                                                                   "db_get_policy_setting_object_id failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_get_policy_setting_object_id(db,
                                                                                   "Product 1",
                                                                                   NULL,
                                                                                   "Category 1",
                                                                                   "Normal",
																				    NULL,
																				   NULL,
																				   NULL,
																				   NULL,
                                                                                   &record_set),
                                                                                   "db_get_policy_setting_object_id failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_get_policy_setting_object_id(db,
                                                                                   "Product 1",
                                                                                   "Feature 4",
                                                                                   NULL,
                                                                                   "Normal",
																				    NULL,
																				   NULL,
																				   NULL,
																				   NULL,
                                                                                   &record_set),
                                                                                   "db_get_policy_setting_object_id failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_get_policy_setting_object_id(db,
                                                                                   "Product 1",
                                                                                   "Feature 4",
                                                                                   "Category 1",
                                                                                   NULL,
																				    NULL,
																				   NULL,
																				   NULL,
																				   NULL,
                                                                                   &record_set),
                                                                                   "db_get_policy_setting_object_id failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_get_policy_setting_object_id(db,
                                                                                   "Product 1",
                                                                                   "Feature 4",
                                                                                   "Category 1",
                                                                                   "Normal",
																				    NULL,
																				   NULL,
																				   NULL,
																				   NULL,
                                                                                   NULL),
                                                                                   "db_get_policy_setting_object_id failed");

        TEST_ASSERT_MESSAGE(MA_OK == db_get_policy_setting_object_id(db, "Product 1", "Feature 4", "Category 1", "Normal", NULL, NULL, NULL, NULL, &record_set),
                                                                     "db_get_policy_setting_object_id failed");

        // Only one record retrieved
        TEST_ASSERT(MA_OK == ma_db_recordset_next(record_set))
        ma_db_recordset_get_string(record_set, 1, &record_col_values);
        TEST_ASSERT(0 == strcmp("100", record_col_values));

        TEST_ASSERT(MA_ERROR_NO_MORE_ITEMS == ma_db_recordset_next(record_set));

        ma_db_recordset_release(record_set);
        record_set = NULL;
    }
}


/******************************************************************************
Test for db_update_policy_setting_object_data API.

Note - There is no API to retrieve the Policy Setting
******************************************************************************/
TEST(policy_db_tests, update_policy_setting_object_data_test)
{
    ma_table_t *first_level_table = NULL;
    ma_table_t *second_level_table = NULL;
    ma_variant_t *variant_ptr = NULL;
    /* Creating the Policy information. Policy information remains common for all
       Policies. Converting this complete information into a variant (Table of Tables).

    <Section name="Server_Alerting">
        <Setting name="bCleanFiles">1</Setting>
        <Setting name="bDeleteFiles">0</Setting>
        <Setting name="bExcludeCookies">1</Setting>
        <Setting name="szDialogMessage">VirusScan Alert!</Setting>
    </Section>
    <Section name="Server_Blocking">
        <Setting name="VSIDBlock">1</Setting>
        <Setting name="VSIDBlockOnNonVirus">0</Setting>
        <Setting name="VSIDBlockTimeout">10</Setting>
    </Section>
    */
    ma_table_create(&first_level_table);

    /* Section - 1 */
    ma_table_create(&second_level_table);

    ma_variant_create_from_string("1", strlen("1"), &variant_ptr);
    ma_table_add_entry(second_level_table, "bCleanFiles", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_string("0", strlen("0"), &variant_ptr);
    ma_table_add_entry(second_level_table, "bDeleteFiles", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_string("1", strlen("1"), &variant_ptr);
    ma_table_add_entry(second_level_table, "bExcludeCookies", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_string("VirusScan Alert!", strlen("VirusScan Alert!"), &variant_ptr);
    ma_table_add_entry(second_level_table, "szDialogMessage", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_table(second_level_table, &variant_ptr);
    ma_table_add_entry(first_level_table, "Server_Alerting", variant_ptr);
    ma_table_release(second_level_table); second_level_table = NULL;
    ma_variant_release(variant_ptr);

    /* Section - 2 */
    ma_table_create(&second_level_table);

    ma_variant_create_from_string("1", strlen("1"), &variant_ptr);
    ma_table_add_entry(second_level_table, "VSIDBlock", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_string("0", strlen("0"), &variant_ptr);
    ma_table_add_entry(second_level_table, "VSIDBlockOnNonVirus", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_string("10", strlen("10"), &variant_ptr);
    ma_table_add_entry(second_level_table, "VSIDBlockTimeout", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_table(second_level_table, &variant_ptr);
    ma_table_add_entry(first_level_table, "Server_Blocking", variant_ptr);
    ma_table_release(second_level_table);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_table(first_level_table, &variant_ptr);
    ma_table_release(first_level_table);

    // Adding one Policy to DB.
    TEST_ASSERT_MESSAGE(MA_OK == db_add_policy_object(db,
                                                      "10",
                                                      "My Policy",
                                                      "Special Feature",
                                                      "General Category",
                                                      "Critical",
                                                      "8f36f236c1e6382d7bb0b632a52e0e203fc71fac525c838bb31474470bfba853",
                                                      "true"), "db_add_policy_object failed");

    // Adding One Policy Setting to DB.
    TEST_ASSERT_MESSAGE(MA_OK == db_add_policy_settings_object(db,
                                                               "10",
                                                               "100",
                                                               "My Policy Setting",
                                                               "8f36f236c1e6382d7bb0b632a52e0e203fc71fac525c838bb31474470bfba853",
                                                               variant_ptr,
                                                               "1",
                                                               "Virus Scan Policy",
                                                               "2013-06-11T18:35:01.000"), "db_add_policy_settings_object failed");

    // Policy data (stored in the Variant) is added to DB, so variant can be released now.
    ma_variant_release(variant_ptr);

    // Adding Product Assignments to DB which corresponds to the Given PO ID (10).
    TEST_ASSERT_MESSAGE(MA_OK == db_add_policy_product_assignment(db, "10", "Test Product", NULL, NULL), "db_add_policy_product_assignment failed");


    // Creating new variant for storing as Policy Setting Data. (Previous variant is released, therefore, reusing the same object)
    TEST_ASSERT(MA_OK == ma_variant_create_from_string("New Policy Setting Data", strlen("New Policy Setting Data"), &variant_ptr));

    // This test modifies the policy setting data for one policy setting ID (PSO ID = 100), then directly accesses DB
    // to verify that the change is done.
    TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_update_policy_setting_object_data(NULL, variant_ptr, "100"),
                                                                                    "db_update_policy_setting_object_data failed");

    TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_update_policy_setting_object_data(db, NULL, "100"),
                                                                                    "db_update_policy_setting_object_data failed");

    TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_update_policy_setting_object_data(db, variant_ptr, NULL),
                                                                                    "db_update_policy_setting_object_data failed");

    TEST_ASSERT_MESSAGE(MA_OK == db_update_policy_setting_object_data(db, variant_ptr, "100"),
                                                                      "db_update_policy_setting_object_data failed");

    // Policy data (stored in the Variant) is added to DB, so variant can be released now.
    ma_variant_release(variant_ptr);

    // Retrieving all policy records matching Product ID = "Test Product", method = NULL, 'param' = NULL.
    {
        ma_db_recordset_t *record_set = NULL;

        TEST_ASSERT_MESSAGE(MA_OK == db_get_product_policies(db,
                                                             "Test Product",
                                                             NULL,
                                                             NULL,
                                                             NULL,
                                                             &record_set), "db_get_product_policies failed");

        // Verifying that the PO DATA attribute is modified in the retrieved record.
        // Only one record retrieved.
        TEST_ASSERT(MA_OK == ma_db_recordset_next(record_set));
        {
            const char *po_feature = NULL;
            const char *po_category = NULL;
            const char *po_type = NULL;
            const char *po_name = NULL;
			const char *pso_name = NULL;
			const char *pso_param_int = NULL;
			const char *pso_param_str = NULL;

            ma_variant_t *new_variant_ptr = NULL;
            ma_buffer_t *my_buffer = NULL;
            const char *my_string = NULL;
            size_t my_string_size = 0;

            ma_db_recordset_get_string(record_set, 1, &po_feature);
            ma_db_recordset_get_string(record_set, 2, &po_category);
            ma_db_recordset_get_string(record_set, 3, &po_type);
            ma_db_recordset_get_string(record_set, 4, &po_name);
			ma_db_recordset_get_string(record_set, 5, &pso_name);
			ma_db_recordset_get_string(record_set, 6, &pso_param_int);
			ma_db_recordset_get_string(record_set, 7, &pso_param_str);
            ma_db_recordset_get_variant(record_set, 8, MA_VARTYPE_STRING, &new_variant_ptr);

            TEST_ASSERT(0 == strcmp("Special Feature", po_feature));
            TEST_ASSERT(0 == strcmp("General Category", po_category));
            TEST_ASSERT(0 == strcmp("Critical", po_type));
            TEST_ASSERT(0 == strcmp("My Policy", po_name));
            TEST_ASSERT(MA_OK == ma_variant_get_string_buffer(new_variant_ptr, &my_buffer));
            TEST_ASSERT(MA_OK == ma_buffer_get_string(my_buffer, &my_string, &my_string_size));
            TEST_ASSERT(0 == strcmp(my_string, "New Policy Setting Data"));
            TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
            TEST_ASSERT(MA_OK == ma_variant_release(new_variant_ptr));
        }

        TEST_ASSERT(MA_ERROR_NO_MORE_ITEMS == ma_db_recordset_next(record_set));

        ma_db_recordset_release(record_set);
        record_set = NULL;
    }
}


/******************************************************************************
Test for db_get_notified_product_list API.

Note - This particular test uses hard-coded test values, therefore, any change
in test data may require the change in this test case too.
******************************************************************************/
TEST(policy_db_tests, get_notified_product_list_test)
{
    ma_table_t *first_level_table = NULL;
    ma_table_t *second_level_table = NULL;
    ma_variant_t *variant_ptr = NULL;
    ma_int32_t counter = 0;

    /* Creating the Policy information. Policy information remains common for all
       Policies. Converting this complete information into a variant (Table of Tables).

    <Section name="Server_Alerting">
        <Setting name="bCleanFiles">1</Setting>
        <Setting name="bDeleteFiles">0</Setting>
        <Setting name="bExcludeCookies">1</Setting>
        <Setting name="szDialogMessage">VirusScan Alert!</Setting>
    </Section>
    <Section name="Server_Blocking">
        <Setting name="VSIDBlock">1</Setting>
        <Setting name="VSIDBlockOnNonVirus">0</Setting>
        <Setting name="VSIDBlockTimeout">10</Setting>
    </Section>
    */
    ma_table_create(&first_level_table);

    /* Section - 1 */
    ma_table_create(&second_level_table);

    ma_variant_create_from_string("1", strlen("1"), &variant_ptr);
    ma_table_add_entry(second_level_table, "bCleanFiles", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_string("0", strlen("0"), &variant_ptr);
    ma_table_add_entry(second_level_table, "bDeleteFiles", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_string("1", strlen("1"), &variant_ptr);
    ma_table_add_entry(second_level_table, "bExcludeCookies", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_string("VirusScan Alert!", strlen("VirusScan Alert!"), &variant_ptr);
    ma_table_add_entry(second_level_table, "szDialogMessage", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_table(second_level_table, &variant_ptr);
    ma_table_add_entry(first_level_table, "Server_Alerting", variant_ptr);
    ma_table_release(second_level_table); second_level_table = NULL;
    ma_variant_release(variant_ptr);

    /* Section - 2 */
    ma_table_create(&second_level_table);

    ma_variant_create_from_string("1", strlen("1"), &variant_ptr);
    ma_table_add_entry(second_level_table, "VSIDBlock", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_string("0", strlen("0"), &variant_ptr);
    ma_table_add_entry(second_level_table, "VSIDBlockOnNonVirus", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_string("10", strlen("10"), &variant_ptr);
    ma_table_add_entry(second_level_table, "VSIDBlockTimeout", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_table(second_level_table, &variant_ptr);
    ma_table_add_entry(first_level_table, "Server_Blocking", variant_ptr);
    ma_table_release(second_level_table);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_table(first_level_table, &variant_ptr);
    ma_table_release(first_level_table);

    // Adding Policies to the DB
    for(counter = 0; counter < sizeof(policies)/sizeof(policies[0]); counter++)
    {
        TEST_ASSERT_MESSAGE(MA_OK == db_add_policy_object(db,
                                                          policies[counter].test_po_id,
                                                          policies[counter].test_po_name,
                                                          policies[counter].test_po_feature,
                                                          policies[counter].test_po_category,
                                                          policies[counter].test_po_type,
                                                          policies[counter].test_po_digest,
                                                          policies[counter].test_po_legacy), "db_add_policy_object failed");
    }

    // Adding Policy Settings to DB. Note - All policy IDs should exist. This test
    // doesn't verify the scenario in which a policy ID, which doesn't exist, is
    // added to the DB.
    for(counter = 0; counter < sizeof(policy_settings)/sizeof(policy_settings[0]); counter++)
    {
        TEST_ASSERT_MESSAGE(MA_OK == db_add_policy_settings_object(db,
                                                                   policy_settings[counter].test_po_id,
                                                                   policy_settings[counter].test_pso_id,
                                                                   policy_settings[counter].test_pso_name,
                                                                   policy_settings[counter].test_pso_digest,
                                                                   variant_ptr,
                                                                   policy_settings[counter].test_pso_param_int,
                                                                   policy_settings[counter].test_pso_param_str,
                                                                   policy_settings[counter].test_time_stamp), "db_add_policy_settings_object failed");
    }

    // Policy data (stored in the Variant) is added to DB, so variant can be released now.
    ma_variant_release(variant_ptr);

    // Adding Product Assignments to DB. Note - All policy IDs should exist. This test
    // doesn't verify the scenario in which a policy ID, which doesn't exist, is added
    // to the DB.
    for(counter = 0; counter < sizeof(policy_product)/sizeof(policy_product[0]); counter++)
    {
        TEST_ASSERT_MESSAGE(MA_OK == db_add_policy_product_assignment(db,
                                                                      policy_product[counter].test_po_id,
                                                                      policy_product[counter].test_product,
                                                                      policy_product[counter].test_method,
                                                                      policy_product[counter].test_param), "db_add_policy_product_assignment failed");
    }

    // Retrieving the Product list matching the timestamp "2014-12-22T18:35:01.100" and policy name as "__EPO_ENFORCE_YES__".
    {
        ma_db_recordset_t *record_set = NULL;
        const char *record_col_values = NULL;

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_get_notified_product_list(NULL,
                                                                                "2014-12-22T18:35:01.100",
                                                                                &record_set),
                                                                                "db_get_notified_product_list failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_get_notified_product_list(db,
                                                                                NULL,
                                                                                &record_set),
                                                                                "db_get_notified_product_list failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_get_notified_product_list(db,
                                                                                "2014-12-22T18:35:01.100",
                                                                                NULL),
                                                                                "db_get_notified_product_list failed");

        TEST_ASSERT_MESSAGE(MA_OK == db_get_notified_product_list(db,
                                                                  "2014-12-22T18:35:01.100",
                                                                  &record_set),
                                                                  "db_get_notified_product_list failed");

        // "Product 1" and "Product 2" match, where -
        // policy name = "__EPO_ENFORCE_YES__" and
        // timestamp = "2014-12-22T18:35:01.100"
        TEST_ASSERT(MA_OK == ma_db_recordset_next(record_set))
        ma_db_recordset_get_string(record_set, 1, &record_col_values);
        TEST_ASSERT(0 == strcmp("Product 1", record_col_values));

        TEST_ASSERT(MA_OK == ma_db_recordset_next(record_set))
        ma_db_recordset_get_string(record_set, 1, &record_col_values);
        TEST_ASSERT(0 == strcmp("Product 2", record_col_values));

        TEST_ASSERT(MA_ERROR_NO_MORE_ITEMS == ma_db_recordset_next(record_set));

        ma_db_recordset_release(record_set);
        record_set = NULL;
    }
}

