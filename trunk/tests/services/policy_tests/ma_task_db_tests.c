/******************************************************************************
Policy Service - Task DB unit tests.
******************************************************************************/

#include "unity.h"
#include "unity_fixture.h"
#include "unity_internals.h"
#include "ma/internal/utils/database/ma_db.h"
#include "ma/internal/services/policy/ma_task_db.h"
#include <string.h>

#ifndef WIN32
    #include <unistd.h>
#endif

TEST_GROUP_RUNNER(task_db_tests_group)
{
    do {RUN_TEST_CASE(task_db_tests, task_addition_test)} while (0);
    do {RUN_TEST_CASE(task_db_tests, task_assignment_test)} while (0);
    do {RUN_TEST_CASE(task_db_tests, get_all_epo_task_test)} while (0);
}

static ma_db_t *db = NULL;
#define TASK_DB_FILENAME  "TASK"

TEST_SETUP(task_db_tests)
{
    unlink(TASK_DB_FILENAME);
    TEST_ASSERT_MESSAGE(MA_OK == ma_db_open(TASK_DB_FILENAME, MA_DB_OPEN_READWRITE,&db), "DB open failed");

    TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_add_task_instance(NULL), "db_add_task_instance failed");
    TEST_ASSERT_MESSAGE(MA_OK == db_add_task_instance(db), "db_add_task_instance failed");
}

TEST_TEAR_DOWN(task_db_tests)
{
    TEST_ASSERT_MESSAGE(MA_OK == ma_db_close(db), "DB close failed") ;
    
}

// Lookup Table consisting of Task object information
struct LookupTable_1
{
    const char *test_task_object_id;
    const char *test_task_id;
    const char *test_task_name;
    const char *test_task_digest;
    const char *test_task_type;
    const char *test_task_priority;
    const char *test_time_stamp;
};

static struct LookupTable_1 const tasks[] =
{
    { "10", "100", "Test Task 1",  "8f36f236c1e6382d7bb0b632a52e0e203fc71fac525c838bb31474470bfba853", "Virus Scan Task",            "0",  "2013-06-11T18:35:01.000" },
    { "9", "200",  "Test Task 2",  "8f459874504957rt7bb0b632a52e0e203fc71fac525c838bb31474470bfba853", "Auto Updater Task",          "4",  "2012-03-06T18:35:01.000" },
    { "8", "300",  "Test Task 3",  "8f36f236c1e638rotiu98eueln9048ujfnelu489rt434t5bb31474470bfba853", "My Personal Task",           "1",  "2012-05-16T18:35:01.000" },
    { "7", "400",  "Test Task 4",  "8f36f236c1e6382d7bb0b632a52e0e203fc7hnac525c838bb31474470bfba853", "Scheduler Task",             "9",  "2010-04-12T18:35:01.000" },
    { "6", "500",  "Test Task 5",  "ejeoiru4oij4n4i5h4nkn4jk4h54bn4hk3l5h43kbn4345j4h534kj8f36f236c1", "Policy Update Task",         "10", "2014-05-26T18:35:01.000" },
    { "5", "600",  "Test Task 6",  "32462389284732yher3hjk3h4i374hn3m4334h3k4n3k43j4h34rn3jk4j3hj853", "Site Advisor Task",          "23", "2011-06-06T18:35:01.000" },
    { "4", "700",  "Test Task 7",  "3lhnr3umr3bnkjbn34m3br3b3kjr3br3kj4rb34jkr34brj34krb3rj34krb34rb", "Critical Error Task",        "2",  "2012-05-16T18:35:01.000" },
    { "3", "800",  "Test Task 8",  "2382892374683924632984632894623948326483274623894632489362498326", "Scheduled Scan Task",        "12", "2014-05-26T18:35:01.000" },
    { "2", "900",  "Test Task 9",  "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", "Weekly Triggered Scan Task", "17", "2012-04-29T18:35:01.000" },
    { "1", "1000", "Test Task 10", "zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz", "Antivirus DAT update Task",  "7",  "2012-05-16T18:35:01.000" },
};


// Lookup Table consisting of Product Assignment information
struct LookupTable_2
{
    const char *test_task_object_id;
    const char *test_product;
    const char *test_method;
    const char *test_param;
};

// Note - Things to be kept in mind while preparing below test data -
// 1) The task ID should exist in DB. The code doesn't verify this while assigning task IDs to Products.
// 2) 'Method' should be non-NULL, if 'param' is not NULL.
// One task can be assigned to one product only.
static struct LookupTable_2 const task_product[] =
{
    { "10", "Product 1", "Method 1",  "No Param" },
    { "7",  "Product 2", "Method 2",  "Param 1"  },
    { "9",  "Product 1", "Method 4",  "No Param" },
    { "6",  "Product 3", "method 5",   NULL      },
    { "3",  "Product 2", "Method 2",  "Param 1"  },
    { "5",  "Product 1",  NULL,        NULL      },
    { "4",  "Product 2", "Method 7",  "Param 1"  },
    { "1",  "Product 3",  NULL,        NULL      },
    { "2",  "Product 2",  NULL,        NULL      },
    { "8",  "Product 1", "method 11",  NULL      },
};

/* This function finds out that the task assignment record retrieved in return for the
   db_get_epo_task_assignments query is one of the expected records.
*/
static ma_int32_t exp_task_assignment_record_list[sizeof(task_product)/sizeof(task_product[0])] = { 0 };

static ma_int32_t do_task_assignment_matching(const char *actual_task_object_id,
                                              const char *actual_product,
                                              const char *actual_method,
                                              const char *actual_param)
{
    ma_int32_t counter = 0;

    for(counter = 0; counter < sizeof(task_product)/sizeof(task_product[0]); counter++)
    {
        ma_uint8_t matched = 1;

        if(exp_task_assignment_record_list[counter] == 1)
        {
            if(strcmp(task_product[counter].test_task_object_id, actual_task_object_id)) {  matched = 0;  }

            if(strcmp(task_product[counter].test_product, actual_product)) {  matched = 0;  }

            // If either of method values is NULL and other is not-NULL
            if(!task_product[counter].test_method || !actual_method)
            {
                if((!task_product[counter].test_method && actual_method) ||
                   (task_product[counter].test_method && !actual_method)) {  matched = 0;  }
            }
            else
            {
                if(strcmp(task_product[counter].test_method, actual_method)) {  matched = 0;  }
            }

            // If either of param values is NULL and other is not-NULL
            if(!task_product[counter].test_param || !actual_param)
            {
                if((!task_product[counter].test_param && actual_param) ||
                   (task_product[counter].test_param && !actual_param)) {  matched = 0;  }
            }
            else
            {
                if(strcmp(task_product[counter].test_param, actual_param)) {  matched = 0;  }
            }
        }

        if(exp_task_assignment_record_list[counter] && matched)
        {
            exp_task_assignment_record_list[counter] = 0;
            matched = 0;
            return 1;
        }
    }
    return 0;
}


/* This function finds out that the task record retrieved in return for the db_get_all_epo_tasks
   query is one of the expected records.
   Note - The actual task data stored in the variant is not verified in this test.
*/
static ma_int32_t exp_product_matching_record_list[sizeof(tasks)/sizeof(tasks[0])] = { 0 };

static ma_int32_t do_task_product_matching(const char *actual_product,
                                           const char *actual_task_object_id,
                                           const char *actual_task_id,
                                           const char *actual_task_name,
                                           const char *actual_task_type,
                                           const char *actual_task_priority,
                                           const char *actual_task_timestamp)
{
    ma_int32_t counter = 0;

    for(counter = 0; counter < sizeof(tasks)/sizeof(tasks[0]); counter++)
    {
        ma_uint8_t matched = 1;
        ma_int8_t counter_1 = 0;

        if(exp_product_matching_record_list[counter] == 1)
        {
            if(strcmp(tasks[counter].test_task_object_id, actual_task_object_id)) {  matched = 0;  }

            if(strcmp(tasks[counter].test_task_id, actual_task_id)) {  matched = 0;  }

            if(strcmp(tasks[counter].test_task_name, actual_task_name)) {  matched = 0;  }

            if(strcmp(tasks[counter].test_task_type, actual_task_type)) {  matched = 0;  }

            if(strcmp(tasks[counter].test_task_priority, actual_task_priority)) {  matched = 0;  }

            if(strcmp(tasks[counter].test_time_stamp, actual_task_timestamp)) {  matched = 0;  }
        }

        // Matching product ID in the task-product assignment table. Assumption is that one Task
        // will be assigned to at most 1 product
        for(counter_1 = 0; counter_1 < sizeof(task_product)/sizeof(task_product[0]); counter_1++)
        {
            if((strcmp(task_product[counter_1].test_task_object_id, actual_task_object_id) == 0) &&
               (strcmp(task_product[counter_1].test_product, actual_product) != 0))
            {
                matched = 0;
            }
        }

        if(exp_product_matching_record_list[counter] && matched)
        {
            exp_product_matching_record_list[counter] = 0;
            matched = 0;
            return 1;
        }
    }
    return 0;
}


/******************************************************************************
Test for verifying the Task addition/removal/retrieval APIs.
******************************************************************************/
TEST(task_db_tests, task_addition_test)
{
    ma_table_t *first_level_table = NULL;
    ma_table_t *second_level_table = NULL;
    ma_variant_t *variant_ptr = NULL;
    ma_int32_t counter = 0;

    /* Creating the Task information. Task information remains common for all
       tasks. Converting this complete information into a variant (Table of Tables).

    <Section name="Actions">
        <Setting name="uAction">5</Setting>
        <Setting name="uSecAction">4</Setting>
    </Section>
    <Section name="Advanced">
       <Setting name="bDeferScanInFullScreen">0</Setting>
       <Setting name="bDeferScanOnBattery">100</Setting>
       <Setting name="dwHeuristicNetCheckSensitivity">2</Setting>
       <Setting name="dwMacroHeuristicsLevel">1</Setting>
    </Section>
    */
    ma_table_create(&first_level_table);

    /* Section - 1 */
    ma_table_create(&second_level_table);

    ma_variant_create_from_string("5", strlen("5"), &variant_ptr);
    ma_table_add_entry(second_level_table, "uAction", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_string("4", strlen("4"), &variant_ptr);
    ma_table_add_entry(second_level_table, "uSecAction", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_table(second_level_table, &variant_ptr);
    ma_table_add_entry(first_level_table, "Actions", variant_ptr);
    ma_table_release(second_level_table); second_level_table = NULL;
    ma_variant_release(variant_ptr);

    /* Section - 2 */
    ma_table_create(&second_level_table);

    ma_variant_create_from_string("0", strlen("0"), &variant_ptr);
    ma_table_add_entry(second_level_table, "bDeferScanInFullScreen", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_string("100", strlen("100"), &variant_ptr);
    ma_table_add_entry(second_level_table, "bDeferScanOnBattery", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_string("2", strlen("2"), &variant_ptr);
    ma_table_add_entry(second_level_table, "dwHeuristicNetCheckSensitivity", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_string("1", strlen("1"), &variant_ptr);
    ma_table_add_entry(second_level_table, "dwMacroHeuristicsLevel", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_table(second_level_table, &variant_ptr);
    ma_table_add_entry(first_level_table, "Advanced", variant_ptr);
    ma_table_release(second_level_table);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_table(first_level_table, &variant_ptr);
    ma_table_release(first_level_table);

    // Adding tasks to the DB
    for(counter = 0; counter < sizeof(tasks)/sizeof(tasks[0]); counter++)
    {
        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_add_epo_task(NULL,
                                                                   tasks[counter].test_task_object_id,
                                                                   tasks[counter].test_task_id,
                                                                   tasks[counter].test_task_name,
                                                                   tasks[counter].test_task_digest,
                                                                   tasks[counter].test_task_type,
                                                                   tasks[counter].test_task_priority,
                                                                   variant_ptr,
                                                                   tasks[counter].test_time_stamp), "db_add_epo_task failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_add_epo_task(db,
                                                                   NULL,
                                                                   tasks[counter].test_task_id,
                                                                   tasks[counter].test_task_name,
                                                                   tasks[counter].test_task_digest,
                                                                   tasks[counter].test_task_type,
                                                                   tasks[counter].test_task_priority,
                                                                   variant_ptr,
                                                                   tasks[counter].test_time_stamp), "db_add_epo_task failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_add_epo_task(db,
                                                                   tasks[counter].test_task_object_id,
                                                                   NULL,
                                                                   tasks[counter].test_task_name,
                                                                   tasks[counter].test_task_digest,
                                                                   tasks[counter].test_task_type,
                                                                   tasks[counter].test_task_priority,
                                                                   variant_ptr,
                                                                   tasks[counter].test_time_stamp), "db_add_epo_task failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_add_epo_task(db,
                                                                   tasks[counter].test_task_object_id,
                                                                   tasks[counter].test_task_id,
                                                                   NULL,
                                                                   tasks[counter].test_task_digest,
                                                                   tasks[counter].test_task_type,
                                                                   tasks[counter].test_task_priority,
                                                                   variant_ptr,
                                                                   tasks[counter].test_time_stamp), "db_add_epo_task failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_add_epo_task(db,
                                                                   tasks[counter].test_task_object_id,
                                                                   tasks[counter].test_task_id,
                                                                   tasks[counter].test_task_name,
                                                                   NULL,
                                                                   tasks[counter].test_task_type,
                                                                   tasks[counter].test_task_priority,
                                                                   variant_ptr,
                                                                   tasks[counter].test_time_stamp), "db_add_epo_task failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_add_epo_task(db,
                                                                   tasks[counter].test_task_object_id,
                                                                   tasks[counter].test_task_id,
                                                                   tasks[counter].test_task_name,
                                                                   tasks[counter].test_task_digest,
                                                                   NULL,
                                                                   tasks[counter].test_task_priority,
                                                                   variant_ptr,
                                                                   tasks[counter].test_time_stamp), "db_add_epo_task failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_add_epo_task(db,
                                                                   tasks[counter].test_task_object_id,
                                                                   tasks[counter].test_task_id,
                                                                   tasks[counter].test_task_name,
                                                                   tasks[counter].test_task_digest,
                                                                   tasks[counter].test_task_type,
                                                                   NULL,
                                                                   variant_ptr,
                                                                   tasks[counter].test_time_stamp), "db_add_epo_task failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_add_epo_task(db,
                                                                   tasks[counter].test_task_object_id,
                                                                   tasks[counter].test_task_id,
                                                                   tasks[counter].test_task_name,
                                                                   tasks[counter].test_task_digest,
                                                                   tasks[counter].test_task_type,
                                                                   tasks[counter].test_task_priority,
                                                                   NULL,
                                                                   tasks[counter].test_time_stamp), "db_add_epo_task failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_add_epo_task(db,
                                                                   tasks[counter].test_task_object_id,
                                                                   tasks[counter].test_task_id,
                                                                   tasks[counter].test_task_name,
                                                                   tasks[counter].test_task_digest,
                                                                   tasks[counter].test_task_type,
                                                                   tasks[counter].test_task_priority,
                                                                   variant_ptr,
                                                                   NULL), "db_add_epo_task failed");

        TEST_ASSERT_MESSAGE(MA_OK == db_add_epo_task(db,
                                                     tasks[counter].test_task_object_id,
                                                     tasks[counter].test_task_id,
                                                     tasks[counter].test_task_name,
                                                     tasks[counter].test_task_digest,
                                                     tasks[counter].test_task_type,
                                                     tasks[counter].test_task_priority,
                                                     variant_ptr,
                                                     tasks[counter].test_time_stamp), "db_add_epo_task failed");
    }

    // Task data (stored in the Variant) is added to DB, so variant can be released now.
    ma_variant_release(variant_ptr);

    // Adding Product Assignment. Note - All task IDs should exist. This test doesn't
    // verify the scenario in which a task ID, which doesn't exist, is added
    // to the DB. Invalid scenarios for db_add_epo_task_assignment API are verified
    // in the next test.
    for(counter = 0; counter < sizeof(task_product)/sizeof(task_product[0]); counter++)
    {
        TEST_ASSERT_MESSAGE(MA_OK == db_add_epo_task_assignment(db,
                                                                task_product[counter].test_task_object_id,
                                                                task_product[counter].test_product,
                                                                task_product[counter].test_method,
                                                                task_product[counter].test_param), "db_add_epo_task_assignment failed");
    }


    // Verifying the Task entries (in reverse order)
    for(counter = sizeof(tasks)/sizeof(tasks[0]) - 1; counter >= 0; counter--)
    {
        ma_db_recordset_t *record_set = NULL;
        char *record_col_values = NULL;

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_get_epo_task(NULL,
                                                                   tasks[counter].test_task_object_id,
                                                                   &record_set),
                                                                   "db_get_epo_task failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_get_epo_task(db,
                                                                   tasks[counter].test_task_object_id,
                                                                   NULL),
                                                                   "db_get_epo_task failed");

        TEST_ASSERT_MESSAGE(MA_OK == db_get_epo_task(db,
                                                     tasks[counter].test_task_object_id,
                                                     &record_set),
                                                     "db_get_epo_task failed");

        while(MA_ERROR_NO_MORE_ITEMS != ma_db_recordset_next(record_set))
        {
            ma_db_recordset_get_string(record_set, 1, &record_col_values);       /* 1st column is Task Object ID */
            TEST_ASSERT_MESSAGE(!strcmp(record_col_values, tasks[counter].test_task_object_id), "Failed to retrieve correct task data");

            ma_db_recordset_get_string(record_set, 2, &record_col_values);       /* 2nd column is Task Digest */
            TEST_ASSERT_MESSAGE(!strcmp(record_col_values, tasks[counter].test_task_digest), "Failed to retrieve correct task data");
        }

        ma_db_recordset_release(record_set);
        record_set = NULL;
    }

    // Updating the timestamp in the task entries. Note that no API returns the timestamp,
    // therefore, the verification for the timestamp update is performed manually by analyzing
    // the database file. This test only verifies that the API returns MA_OK.
    for(counter = 0; counter < sizeof(tasks)/sizeof(tasks[0]); counter++)
    {
        ma_db_recordset_t *record_set = NULL;
        char *record_col_values = NULL;
        const char *new_timestamp = "2014-06-11T18:12:01.000";

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_update_epo_task(NULL,
                                                                      tasks[counter].test_task_object_id,
                                                                      new_timestamp),
                                                                      "db_update_epo_task failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_update_epo_task(db,
                                                                      NULL,
                                                                      new_timestamp),
                                                                      "db_update_epo_task failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_update_epo_task(db,
                                                                      tasks[counter].test_task_object_id,
                                                                      NULL),
                                                                      "db_update_epo_task failed");

        TEST_ASSERT_MESSAGE(MA_OK == db_update_epo_task(db,
                                                        tasks[counter].test_task_object_id,
                                                        new_timestamp),
                                                        "db_update_epo_task failed");

        TEST_ASSERT_MESSAGE(MA_OK == db_get_epo_task(db,
                                                     tasks[counter].test_task_object_id,
                                                     &record_set),
                                                     "db_get_epo_task failed");

        while(MA_ERROR_NO_MORE_ITEMS != ma_db_recordset_next(record_set))
        {
            ma_db_recordset_get_string(record_set, 1, &record_col_values);       /* 1st column is Task Object ID */

            TEST_ASSERT_MESSAGE(!strcmp(record_col_values, tasks[counter].test_task_object_id), "Failed to retrieve correct task data");

            ma_db_recordset_get_string(record_set, 2, &record_col_values);       /* 2nd column is Task Digest */
            TEST_ASSERT_MESSAGE(!strcmp(record_col_values, tasks[counter].test_task_digest), "Failed to retrieve correct task data");
        }

        ma_db_recordset_release(record_set);
        record_set = NULL;
    }

    // Scenario - If a Task is removed from the Task Table, corresponding
    // entries from the Task-Product Assignment table are also removed.
    // 1) Removing the task entry by calling the db_remove_epo_task API. Then, calling
    //    the db_get_epo_task API and verifying that no record is returned.
    // 2) Also, verifying that the corresponding records for the Task Assignment table
    //    are also removed.
    for(counter = 0; counter < sizeof(tasks)/sizeof(tasks[0]); counter++)
    {
        ma_db_recordset_t *record_set = NULL;

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_remove_epo_task(NULL,
                                                                      tasks[counter].test_task_object_id),
                                                                      "db_remove_epo_task failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_remove_epo_task(db,
                                                                      NULL),
                                                                      "db_remove_epo_task failed");

        TEST_ASSERT_MESSAGE(MA_OK == db_remove_epo_task(db,
                                                        tasks[counter].test_task_object_id),
                                                        "db_get_epo_task failed");

        TEST_ASSERT_MESSAGE(MA_OK == db_get_epo_task(db,
                                                     tasks[counter].test_task_object_id,
                                                     &record_set),
                                                     "db_get_epo_task failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_NO_MORE_ITEMS == ma_db_recordset_next(record_set), "All matching records from Task table could not be removed");

        ma_db_recordset_release(record_set);
        record_set = NULL;

        TEST_ASSERT_MESSAGE(MA_OK == db_get_epo_task_assignments(db,
                                                                 tasks[counter].test_task_object_id,
                                                                 NULL,
                                                                 NULL,
                                                                 NULL,
                                                                 &record_set), "db_get_epo_task_assignments failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_NO_MORE_ITEMS == ma_db_recordset_next(record_set), \
                                                                           "Records corresponding to Task are not removed from Product Assignment table");

        ma_db_recordset_release(record_set);
        record_set = NULL;
    }
}


/******************************************************************************
Test for verifying the Task assignment addition/removal/retrieval APIs.
******************************************************************************/
TEST(task_db_tests, task_assignment_test)
{
    ma_int32_t counter = 0;

    // Adding Product Assignment. Note - This test verifies the Product Assignment APIs
    // independently. Therefore, it is not necessary that corresponding task records
    // should exist in the DB.
    for(counter = 0; counter < sizeof(task_product)/sizeof(task_product[0]); counter++)
    {
        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_add_epo_task_assignment(NULL,
                                                                              task_product[counter].test_task_object_id,
                                                                              task_product[counter].test_product,
                                                                              task_product[counter].test_method,
                                                                              task_product[counter].test_param), "db_add_epo_task_assignment failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_add_epo_task_assignment(db,
                                                                              NULL,
                                                                              task_product[counter].test_product,
                                                                              task_product[counter].test_method,
                                                                              task_product[counter].test_param), "db_add_epo_task_assignment failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_add_epo_task_assignment(db,
                                                                              task_product[counter].test_task_object_id,
                                                                              NULL,
                                                                              task_product[counter].test_method,
                                                                              task_product[counter].test_param), "db_add_epo_task_assignment failed");

        TEST_ASSERT_MESSAGE(MA_OK == db_add_epo_task_assignment(db,
                                                                task_product[counter].test_task_object_id,
                                                                task_product[counter].test_product,
                                                                task_product[counter].test_method,
                                                                task_product[counter].test_param), "db_add_epo_task_assignment failed");
    }

    // Verifying the Product Assignment (in reverse order)
    for(counter = sizeof(task_product)/sizeof(task_product[0]) - 1; counter >= 0; counter--)
    {
        ma_db_recordset_t *record_set = NULL;
        char *record_col_values = NULL;

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_get_epo_task_assignments(NULL,
                                                                               task_product[counter].test_task_object_id,
                                                                               task_product[counter].test_product,
                                                                               task_product[counter].test_method,
                                                                               task_product[counter].test_param,
                                                                               &record_set),
                                                                               "db_get_epo_task_assignments failed");

        TEST_ASSERT_MESSAGE(MA_OK == db_get_epo_task_assignments(db,
                                                                 task_product[counter].test_task_object_id,
                                                                 task_product[counter].test_product,
                                                                 task_product[counter].test_method,
                                                                 task_product[counter].test_param,
                                                                 &record_set), "db_get_epo_task_assignments failed");

        while(MA_ERROR_NO_MORE_ITEMS != ma_db_recordset_next(record_set))
        {
            ma_db_recordset_get_string(record_set, 1, &record_col_values);       /* 1st column is Task Object ID */
            TEST_ASSERT_MESSAGE(!strcmp(record_col_values, task_product[counter].test_task_object_id), "Failed to retrieve correct task object id");

            ma_db_recordset_get_string(record_set, 2, &record_col_values);       /* 2nd column is Product ID */
            TEST_ASSERT_MESSAGE(!strcmp(record_col_values, task_product[counter].test_product), "Failed to retrieve correct task product id");

            ma_db_recordset_get_string(record_set, 3, &record_col_values);       /* 3rd column is method */
            if(task_product[counter].test_method == NULL)
            {
                TEST_ASSERT_NULL_MESSAGE(record_col_values, "Failed to retrieve correct task method");
            }
            else
            {
                TEST_ASSERT_MESSAGE(!strcmp(record_col_values, task_product[counter].test_method), "Failed to retrieve correct task method");
            }

            ma_db_recordset_get_string(record_set, 4, &record_col_values);       /* 4th column is param */
            if(task_product[counter].test_param == NULL)
            {
                TEST_ASSERT_NULL_MESSAGE(record_col_values, "Failed to retrieve correct task parameters");
            }
            else
            {
                TEST_ASSERT_MESSAGE(!strcmp(record_col_values, task_product[counter].test_param), "Failed to retrieve correct task parameters");
            }
        }

        ma_db_recordset_release(record_set);
        record_set = NULL;
    }

    // Verifying the Product Assignment (multiple entries). Scenario - Retrieve all elements
    // matching a particular Task ID.
    for(counter = 0; counter < sizeof(task_product)/sizeof(task_product[0]); counter++)
    {
        ma_db_recordset_t *record_set = NULL;
        ma_int32_t counter_1 = 0;

        TEST_ASSERT_MESSAGE(MA_OK == db_get_epo_task_assignments(db,
                                                                 task_product[counter].test_task_object_id,
                                                                 NULL,
                                                                 NULL,
                                                                 NULL,
                                                                 &record_set), "db_get_epo_task_assignments failed");

        // Finding items which should match, then pulling one element from the record set DB
        // and verifying, that the element stored in DB is correct.
        for(counter_1 = 0; counter_1 < sizeof(task_product)/sizeof(task_product[0]); counter_1++)
        {
            if(strcmp(task_product[counter].test_task_object_id, task_product[counter_1].test_task_object_id) == 0)
            {
                exp_task_assignment_record_list[counter_1] = 1;
            }
        }

        while(MA_ERROR_NO_MORE_ITEMS != ma_db_recordset_next(record_set))
        {
            char *task_obj_id = NULL;
            char *product = NULL;
            char *method = NULL;
            char *param = NULL;
            ma_db_recordset_get_string(record_set, 1, &task_obj_id);
            ma_db_recordset_get_string(record_set, 2, &product);
            ma_db_recordset_get_string(record_set, 3, &method);
            ma_db_recordset_get_string(record_set, 4, &param);

            TEST_ASSERT_MESSAGE(do_task_assignment_matching(task_obj_id, product, method, param) == 1, \
                                "Unintended record retrieved from DB");
        }

        for(counter_1 = 0; counter_1 < sizeof(exp_task_assignment_record_list)/sizeof(exp_task_assignment_record_list[0]); counter_1++)
        {
            TEST_ASSERT_MESSAGE(exp_task_assignment_record_list[counter_1] == 0, "One expected record not retrieved by query");
        }

        ma_db_recordset_release(record_set);
        record_set = NULL;
    }

    // Verifying the Product Assignment (multiple entries). Scenario - Retrieve all
    // elements matching a particular (Task ID + Product ID). All records matching
    // the (Task ID + Product ID) and 'method' and 'param' values as NULL should be
    // retrieved.
    for(counter = 0; counter < sizeof(task_product)/sizeof(task_product[0]); counter++)
    {
        ma_db_recordset_t *record_set = NULL;
        ma_int32_t counter_1 = 0;

        TEST_ASSERT_MESSAGE(MA_OK == db_get_epo_task_assignments(db,
                                                                 task_product[counter].test_task_object_id,
                                                                 task_product[counter].test_product,
                                                                 NULL,
                                                                 NULL,
                                                                 &record_set), "db_get_epo_task_assignments failed");

        // Finding items which should match, then pulling one element from the record set DB
        // and verifying, that the element stored in DB is correct.
        for(counter_1 = 0; counter_1 < sizeof(task_product)/sizeof(task_product[0]); counter_1++)
        {
            if((strcmp(task_product[counter].test_task_object_id, task_product[counter_1].test_task_object_id) == 0) &&
               (strcmp(task_product[counter].test_product, task_product[counter_1].test_product) == 0) &&
               (task_product[counter_1].test_method == NULL) &&
               (task_product[counter_1].test_param == NULL) )
            {
                exp_task_assignment_record_list[counter_1] = 1;
            }
        }

        while(MA_ERROR_NO_MORE_ITEMS != ma_db_recordset_next(record_set))
        {
            char *task_obj_id = NULL;
            char *product = NULL;
            char *method = NULL;
            char *param = NULL;
            ma_db_recordset_get_string(record_set, 1, &task_obj_id);
            ma_db_recordset_get_string(record_set, 2, &product);
            ma_db_recordset_get_string(record_set, 3, &method);
            ma_db_recordset_get_string(record_set, 4, &param);

            TEST_ASSERT_MESSAGE(do_task_assignment_matching(task_obj_id, product, method, param) == 1, \
                                "Unintended record retrieved from DB");
        }

        for(counter_1 = 0; counter_1 < sizeof(exp_task_assignment_record_list)/sizeof(exp_task_assignment_record_list[0]); counter_1++)
        {
            TEST_ASSERT_MESSAGE(exp_task_assignment_record_list[counter_1] == 0, "One expected record not retrieved by query");
        }

        ma_db_recordset_release(record_set);
        record_set = NULL;
    }

    // Removing elements matching a particular (Task ID + Product ID) combination.
    // Then calling the db_get_epo_task_assignments API using same combination. No
    // records should be retrieved.
    for(counter = 0; counter < sizeof(task_product)/sizeof(task_product[0]); counter++)
    {
        ma_db_recordset_t *record_set = NULL;

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_remove_epo_task_assignment(NULL,
                                                                                 task_product[counter].test_task_object_id,
                                                                                 task_product[counter].test_product,
                                                                                 NULL,
                                                                                 NULL), "db_remove_epo_task_assignment failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_remove_epo_task_assignment(db,
                                                                                 NULL,
                                                                                 task_product[counter].test_product,
                                                                                 NULL,
                                                                                 NULL), "db_remove_epo_task_assignment failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_remove_epo_task_assignment(db,
                                                                                 task_product[counter].test_task_object_id,
                                                                                 NULL,
                                                                                 NULL,
                                                                                 NULL), "db_remove_epo_task_assignment failed");

        TEST_ASSERT_MESSAGE(MA_OK == db_remove_epo_task_assignment(db,
                                                                   task_product[counter].test_task_object_id,
                                                                   task_product[counter].test_product,
                                                                   NULL,
                                                                   NULL), "db_remove_epo_task_assignment failed");

        TEST_ASSERT_MESSAGE(MA_OK == db_get_epo_task_assignments(db,
                                                                 task_product[counter].test_task_object_id,
                                                                 task_product[counter].test_product,
                                                                 NULL,
                                                                 NULL,
                                                                 &record_set), "db_get_epo_task_assignments failed");

        TEST_ASSERT_MESSAGE(MA_ERROR_NO_MORE_ITEMS == ma_db_recordset_next(record_set), "All matching records couldn't be removed");

        ma_db_recordset_release(record_set);
        record_set = NULL;
    }
}

/******************************************************************************
Test for verifying the db_get_all_epo_tasks API
******************************************************************************/
TEST(task_db_tests, get_all_epo_task_test)
{
    ma_table_t *first_level_table = NULL;
    ma_table_t *second_level_table = NULL;
    ma_variant_t *variant_ptr = NULL;
    ma_int32_t counter = 0;

    /* Creating the Task information. Task information remains common for all
       tasks. Converting this complete information into a variant (Table of Tables).

    <Section name="Actions">
        <Setting name="uAction">5</Setting>
        <Setting name="uSecAction">4</Setting>
    </Section>
    <Section name="Advanced">
       <Setting name="bDeferScanInFullScreen">0</Setting>
       <Setting name="bDeferScanOnBattery">100</Setting>
       <Setting name="dwHeuristicNetCheckSensitivity">2</Setting>
       <Setting name="dwMacroHeuristicsLevel">1</Setting>
    </Section>
    */
    ma_table_create(&first_level_table);

    /* Section - 1 */
    ma_table_create(&second_level_table);

    ma_variant_create_from_string("5", strlen("5"), &variant_ptr);
    ma_table_add_entry(second_level_table, "uAction", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_string("4", strlen("4"), &variant_ptr);
    ma_table_add_entry(second_level_table, "uSecAction", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_table(second_level_table, &variant_ptr);
    ma_table_add_entry(first_level_table, "Actions", variant_ptr);
    ma_table_release(second_level_table); second_level_table = NULL;
    ma_variant_release(variant_ptr);

    /* Section - 2 */
    ma_table_create(&second_level_table);

    ma_variant_create_from_string("0", strlen("0"), &variant_ptr);
    ma_table_add_entry(second_level_table, "bDeferScanInFullScreen", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_string("100", strlen("100"), &variant_ptr);
    ma_table_add_entry(second_level_table, "bDeferScanOnBattery", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_string("2", strlen("2"), &variant_ptr);
    ma_table_add_entry(second_level_table, "dwHeuristicNetCheckSensitivity", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_string("1", strlen("1"), &variant_ptr);
    ma_table_add_entry(second_level_table, "dwMacroHeuristicsLevel", variant_ptr);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_table(second_level_table, &variant_ptr);
    ma_table_add_entry(first_level_table, "Advanced", variant_ptr);
    ma_table_release(second_level_table);
    ma_variant_release(variant_ptr);

    ma_variant_create_from_table(first_level_table, &variant_ptr);
    ma_table_release(first_level_table);

    // Adding tasks to the DB
    for(counter = 0; counter < sizeof(tasks)/sizeof(tasks[0]); counter++)
    {
        TEST_ASSERT_MESSAGE(MA_OK == db_add_epo_task(db,
                                                     tasks[counter].test_task_object_id,
                                                     tasks[counter].test_task_id,
                                                     tasks[counter].test_task_name,
                                                     tasks[counter].test_task_digest,
                                                     tasks[counter].test_task_type,
                                                     tasks[counter].test_task_priority,
                                                     variant_ptr,
                                                     tasks[counter].test_time_stamp), "db_add_epo_task failed");
    }

    // Task data (stored in the Variant) is added to DB, so variant can be released now.
    ma_variant_release(variant_ptr);

    // Adding Product Assignments to DB
    for(counter = 0; counter < sizeof(task_product)/sizeof(task_product[0]); counter++)
    {
        TEST_ASSERT_MESSAGE(MA_OK == db_add_epo_task_assignment(db,
                                                                task_product[counter].test_task_object_id,
                                                                task_product[counter].test_product,
                                                                task_product[counter].test_method,
                                                                task_product[counter].test_param), "db_add_epo_task_assignment failed");
    }

    // Test for retrieving all task records matching one particular task timestamp
    for(counter = 0; counter < sizeof(tasks)/sizeof(tasks[0]); counter++)
    {
        ma_db_recordset_t *record_set = NULL;
        ma_int32_t counter_1 = 0;

        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_get_all_epo_tasks(NULL,
                                                                        NULL,
                                                                        &record_set), "db_get_all_epo_tasks failed");


        TEST_ASSERT_MESSAGE(MA_ERROR_INVALIDARG == db_get_all_epo_tasks(db,
                                                                        tasks[counter].test_time_stamp,
                                                                        NULL), "db_get_all_epo_tasks failed");

        TEST_ASSERT_MESSAGE(MA_OK == db_get_all_epo_tasks(db,
                                                          tasks[counter].test_time_stamp,
                                                          &record_set), "db_get_all_epo_tasks failed");

        // Finding items which should match, then pulling one element from the record set DB
        // and verifying, that the element stored in DB is correct.
        for(counter_1 = 0; counter_1 < sizeof(tasks)/sizeof(tasks[0]); counter_1++)
        {
            if(strcmp(tasks[counter].test_time_stamp, tasks[counter_1].test_time_stamp) == 0)
            {
                exp_product_matching_record_list[counter_1] = 1;
            }
        }

        while(MA_ERROR_NO_MORE_ITEMS != ma_db_recordset_next(record_set))
        {
            char *product = NULL;
            char *task_obj_id = NULL;
            char *task_id = NULL;
            char *task_name = NULL;
            char *task_type = NULL;
            char *task_priority = NULL;
            char *task_timestamp = NULL;
            ma_db_recordset_get_string(record_set, 1, &product);
            ma_db_recordset_get_string(record_set, 2, &task_obj_id);
            ma_db_recordset_get_string(record_set, 3, &task_id);
            ma_db_recordset_get_string(record_set, 4, &task_name);
            ma_db_recordset_get_string(record_set, 5, &task_type);
            ma_db_recordset_get_string(record_set, 6, &task_priority);
            ma_db_recordset_get_string(record_set, 7, &task_timestamp);

            TEST_ASSERT_MESSAGE(do_task_product_matching(product, task_obj_id, task_id, task_name, task_type, task_priority, task_timestamp) == 1, \
                                "Unintended record retrieved from DB");
        }

        for(counter_1 = 0; counter_1 < sizeof(exp_product_matching_record_list)/sizeof(exp_product_matching_record_list[0]); counter_1++)
        {
            TEST_ASSERT_MESSAGE(exp_product_matching_record_list[counter_1] == 0, "One expected record not retrieved by query");
        }

        ma_db_recordset_release(record_set);
        record_set = NULL;
    }

    // Test for retrieving all ePO task records
    for(counter = 0; counter < sizeof(tasks)/sizeof(tasks[0]); counter++)
    {
        ma_db_recordset_t *record_set = NULL;
        ma_int32_t counter_1 = 0;

        TEST_ASSERT_MESSAGE(MA_OK == db_get_all_epo_tasks(db,
                                                          NULL,
                                                          &record_set), "db_get_all_epo_tasks failed");

        // Setting all records as expected records initially. Once, the task is matched from table
        // and corresponding product ID is matched from the product assignment table, the corresponding
        // record will be marked as 0. The complete list should be 0 by the end of iteration.
        for(counter = 0; counter < sizeof(exp_product_matching_record_list)/sizeof(exp_product_matching_record_list[0]); counter++)
        {
            exp_product_matching_record_list[counter] = 1;
        }

        while(MA_ERROR_NO_MORE_ITEMS != ma_db_recordset_next(record_set))
        {
            char *product = NULL;
            char *task_obj_id = NULL;
            char *task_id = NULL;
            char *task_name = NULL;
            char *task_type = NULL;
            char *task_priority = NULL;
            char *task_timestamp = NULL;
            ma_db_recordset_get_string(record_set, 1, &product);
            ma_db_recordset_get_string(record_set, 2, &task_obj_id);
            ma_db_recordset_get_string(record_set, 3, &task_id);
            ma_db_recordset_get_string(record_set, 4, &task_name);
            ma_db_recordset_get_string(record_set, 5, &task_type);
            ma_db_recordset_get_string(record_set, 6, &task_priority);
            ma_db_recordset_get_string(record_set, 7, &task_timestamp);

            TEST_ASSERT_MESSAGE(do_task_product_matching(product, task_obj_id, task_id, task_name, task_type, task_priority, task_timestamp) == 1, \
                                "Unmatched record retrieved from DB");
        }

        for(counter_1 = 0; counter_1 < sizeof(exp_product_matching_record_list)/sizeof(exp_product_matching_record_list[0]); counter_1++)
        {
            TEST_ASSERT_MESSAGE(exp_product_matching_record_list[counter_1] == 0, "One expected record not retrieved by query");
        }

        ma_db_recordset_release(record_set);
        record_set = NULL;
    }
}


