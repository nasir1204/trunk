/******************************************************************************
Policy Service - Policy Service unit tests.
******************************************************************************/

#include "unity.h"
#include "unity_fixture.h"
#include "unity_internals.h"
#include "ma/internal/services/policy/ma_policy_service_internal.h"
#include "ma/internal/services/policy/ma_policy_db.h"

#include "ma/policy/ma_policy.h"

#include <string.h>
#include <stdlib.h>

#ifndef WIN32
    #include <unistd.h>
#endif

TEST_GROUP_RUNNER(policy_service_tests_group)
{
    do {RUN_TEST_CASE(policy_service_tests, set_get_policy_data_test)} while (0);
}

extern ma_error_t ma_policy_uri_get_table(ma_policy_uri_t *, ma_table_t **);
extern ma_error_t ma_policy_bag_get_variant(ma_policy_bag_t *, ma_variant_t **);
extern ma_error_t ma_policy_bag_create_from_variant(ma_variant_t *, ma_policy_bag_t **);

static ma_db_t *db = NULL;
#define POLICY_DB_FILENAME  "POLICY_SERVICE_UT.db"

TEST_SETUP(policy_service_tests)
{
    unlink(POLICY_DB_FILENAME);
    TEST_ASSERT(MA_OK == ma_db_open(POLICY_DB_FILENAME, MA_DB_OPEN_READWRITE,&db));
    TEST_ASSERT(MA_OK == db_add_policy_instance(db));
}

TEST_TEAR_DOWN(policy_service_tests)
{
    TEST_ASSERT(MA_OK == ma_db_close(db));
}

/******************************************************************************
Verification for ma_policy_service_set_policies and ma_policy_service_get_policies
APIs.
******************************************************************************/
TEST(policy_service_tests, set_get_policy_data_test)
{
    // Policy notification information
    ma_policy_uri_t *uri_ptr = NULL;
    ma_table_t *uri_table = NULL;

    // Policy properties
    ma_policy_uri_t *policy_uri = NULL;

    // Policy Bag
    ma_policy_bag_t *my_policy_bag = NULL;
    ma_variant_t *my_variant_ptr = NULL;
    ma_variant_t *policy_variant_ptr = NULL;
    size_t policy_count = 0;

    ma_policy_bag_t *new_policy_bag = NULL;

    TEST_ASSERT(MA_OK == ma_policy_uri_create(&uri_ptr));
    ma_policy_uri_set_software_id(uri_ptr, "MA Next Generation Software");
    //ma_policy_uri_set_user(uri_ptr, "Current Root User");
    //ma_policy_uri_set_location_id(uri_ptr, "McAfee Bangalore");
    TEST_ASSERT(MA_OK == ma_policy_uri_get_table(uri_ptr, &uri_table));

    // Policy - Properties Table
    TEST_ASSERT(MA_OK == ma_policy_uri_create(&policy_uri));
    TEST_ASSERT(MA_OK == ma_policy_uri_set_feature(policy_uri, "My Policy Features"));
    TEST_ASSERT(MA_OK == ma_policy_uri_set_category(policy_uri, "Special Category"));
    TEST_ASSERT(MA_OK == ma_policy_uri_set_type(policy_uri, "No Identification"));
    TEST_ASSERT(MA_OK == ma_policy_uri_set_name(policy_uri, "First Policy URI"));
	TEST_ASSERT(MA_OK == ma_policy_uri_set_pso_name(policy_uri, "PSO URI"));
	TEST_ASSERT(MA_OK == ma_policy_uri_set_pso_param_int(policy_uri, "1"));
	TEST_ASSERT(MA_OK == ma_policy_uri_set_pso_param_str(policy_uri, ""));

    // Policy  - Section Data Table
    TEST_ASSERT(MA_OK ==  ma_policy_bag_create(&my_policy_bag));

    // Setting values in Policy bag
    TEST_ASSERT(MA_OK == ma_variant_create_from_string("VALUE 1", strlen("VALUE 1"), &my_variant_ptr));
    TEST_ASSERT(MA_OK == ma_policy_bag_set_value(my_policy_bag, policy_uri, "SECTION 1", "KEY 1", my_variant_ptr));
    TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));

    TEST_ASSERT(MA_OK == ma_variant_create_from_string("VALUE 2", strlen("VALUE 2"), &my_variant_ptr));
    TEST_ASSERT(MA_OK == ma_policy_bag_set_value(my_policy_bag, policy_uri, "SECTION 2", "KEY 2", my_variant_ptr));
    TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));

    TEST_ASSERT(MA_OK == ma_variant_create_from_string("VALUE 3", strlen("VALUE 3"), &my_variant_ptr));
    TEST_ASSERT(MA_OK == ma_policy_bag_set_value(my_policy_bag, policy_uri, "SECTION 2", "KEY 3", my_variant_ptr));
    TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));

    TEST_ASSERT(MA_OK == ma_variant_create_from_string("VALUE 4", strlen("VALUE 4"), &my_variant_ptr));
    TEST_ASSERT(MA_OK == ma_policy_bag_set_value(my_policy_bag, policy_uri, "SECTION 1", "KEY 4", my_variant_ptr));
    TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));

    TEST_ASSERT(MA_OK == ma_variant_create_from_string("VALUE 5", strlen("VALUE 5"), &my_variant_ptr));
    TEST_ASSERT(MA_OK == ma_policy_bag_set_value(my_policy_bag, policy_uri, "SECTION 2", "KEY 5", my_variant_ptr));
    TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));

    // Converting Policy bag to variant and then, releasing Policy bag
    TEST_ASSERT(MA_OK == ma_policy_bag_get_variant(my_policy_bag, &policy_variant_ptr));
    TEST_ASSERT(MA_OK == ma_policy_bag_release(my_policy_bag));

    // Setting Policies
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_service_set_policies(NULL, db, policy_variant_ptr));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_service_set_policies(uri_table, NULL, policy_variant_ptr));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_service_set_policies(uri_table, db, NULL));
    TEST_ASSERT(MA_OK == ma_policy_service_set_policies(uri_table, db, policy_variant_ptr));
    TEST_ASSERT(MA_OK == ma_variant_release(policy_variant_ptr)); policy_variant_ptr = NULL;

    // Fetching Policies variant and then, retrieving policy bag from it.
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_service_get_policies(NULL, db, &policy_count, &policy_variant_ptr, MA_TRUE));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_service_get_policies(uri_table, NULL, &policy_count, &policy_variant_ptr, MA_TRUE));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_service_get_policies(uri_table, db, NULL, &policy_variant_ptr, MA_TRUE));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_policy_service_get_policies(uri_table, db, &policy_count, NULL, MA_TRUE));
    TEST_ASSERT(MA_OK == ma_policy_service_get_policies(uri_table, db, &policy_count, &policy_variant_ptr, MA_TRUE));
    TEST_ASSERT(MA_OK == ma_policy_bag_create_from_variant(policy_variant_ptr, &new_policy_bag));
    TEST_ASSERT(MA_OK == ma_variant_release(policy_variant_ptr)); policy_variant_ptr = NULL;

    // Retrieving values from the Policy bag for verification
    {
        ma_buffer_t *my_buffer = NULL;
        const char *my_string = NULL;
        size_t my_string_size = 0;

         // Valid scenarios (URI 1)
        TEST_ASSERT(MA_OK == ma_policy_bag_get_value(new_policy_bag, policy_uri, "SECTION 1", "KEY 1", &my_variant_ptr));
        ma_variant_get_string_buffer(my_variant_ptr, &my_buffer);
        ma_buffer_get_string(my_buffer, &my_string, &my_string_size);
        TEST_ASSERT(0 == strcmp(my_string, "VALUE 1"));
        TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
        TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));

        TEST_ASSERT(MA_OK == ma_policy_bag_get_value(new_policy_bag, policy_uri, "SECTION 2", "KEY 2", &my_variant_ptr));
        ma_variant_get_string_buffer(my_variant_ptr, &my_buffer);
        ma_buffer_get_string(my_buffer, &my_string, &my_string_size);
        TEST_ASSERT(0 == strcmp(my_string, "VALUE 2"));
        TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
        TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));

        TEST_ASSERT(MA_OK == ma_policy_bag_get_value(new_policy_bag, policy_uri, "SECTION 2", "KEY 3", &my_variant_ptr));
        ma_variant_get_string_buffer(my_variant_ptr, &my_buffer);
        ma_buffer_get_string(my_buffer, &my_string, &my_string_size);
        TEST_ASSERT(0 == strcmp(my_string, "VALUE 3"));
        TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
        TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));

        TEST_ASSERT(MA_OK == ma_policy_bag_get_value(new_policy_bag, policy_uri, "SECTION 1", "KEY 4", &my_variant_ptr));
        ma_variant_get_string_buffer(my_variant_ptr, &my_buffer);
        ma_buffer_get_string(my_buffer, &my_string, &my_string_size);
        TEST_ASSERT(0 == strcmp(my_string, "VALUE 4"));
        TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
        TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));

        TEST_ASSERT(MA_OK == ma_policy_bag_get_value(new_policy_bag, policy_uri, "SECTION 2", "KEY 5", &my_variant_ptr));
        ma_variant_get_string_buffer(my_variant_ptr, &my_buffer);
        ma_buffer_get_string(my_buffer, &my_string, &my_string_size);
        TEST_ASSERT(0 == strcmp(my_string, "VALUE 5"));
        TEST_ASSERT(MA_OK == ma_buffer_release(my_buffer));
        TEST_ASSERT(MA_OK == ma_variant_release(my_variant_ptr));
    }

    // Releasing the new policy bag and URI
    TEST_ASSERT(MA_OK == ma_policy_bag_release(new_policy_bag));
    TEST_ASSERT(MA_OK == ma_policy_uri_release(policy_uri));

    // URI notification information release
    TEST_ASSERT(MA_OK == ma_policy_uri_release(uri_ptr));
    TEST_ASSERT(MA_OK == ma_table_release(uri_table));
}
