#include "unity.h"
#include "unity_fixture.h"

#include "ma_spipe_url_request_internal.h"
#include "ma_ah_client_service_test_helper.h"
#include "ma/repository/ma_repository.h"

#include "ma_ut_setup.h"

#define UPSTREAM "up_stream"
#define DOWNSTREAM "down_stream"

static ma_ut_setup_t *g_ut_setup = NULL;
extern ma_ut_setup_t *get_ut_setup();

TEST_GROUP_RUNNER(spipe_url_request_tests_group)
{
	do {RUN_TEST_CASE(spipe_url_request_tests, verify_basics);} while (0);
}

TEST_SETUP(spipe_url_request_tests)
{
	unlink(UPSTREAM);
	unlink(DOWNSTREAM);
	g_ut_setup = get_ut_setup();
}

TEST_TEAR_DOWN(spipe_url_request_tests)
{
	unlink(UPSTREAM);
	unlink(DOWNSTREAM);
}

extern ma_bool_t is_connection_to_next_site_needed(ma_spipe_url_request_t *request, ma_error_t status, int response_code, ma_bool_t *is_current_url_reachable);
extern ma_error_t ma_spipe_url_request_resend(ma_spipe_url_request_t *self);

struct repository_data
{
    const char *repo_name;
    ma_repository_type_t repo_type;
    ma_repository_url_type_t repo_url_type;
    ma_repository_namespace_t repo_namespace;
    ma_proxy_usage_type_t repo_proxy_usage_type;
    ma_repository_auth_t repo_auth;
    ma_bool_t repo_enabled;
    const char *repo_server_fqdn;
    const char *repo_server_name;
    const char *repo_server_ip;
    ma_uint32_t repo_port;
    const char *repo_server_path;
    const char *repo_domain_name;
    const char *repo_user_name;
    const char *repo_password;
    ma_uint32_t repo_pingtime;
    ma_uint32_t repo_subnetdistance;
    ma_uint32_t repo_siteorder;
    ma_repository_state_t repo_state;
    ma_uint32_t ssl_port;
};

static struct repository_data const repo_data_1[] =
{
    // Name            Type                         URL Type                      Namespace                      Proxy Usage Type            Authentication          Enabled          Server FQDN             Server Name       Server IP   Port     Server Path              Domain Name Username  Password Pingtime Subnet Distance Siteorder        State                  ssl port
    {  "EPO_SER3", MA_REPOSITORY_TYPE_MASTER, MA_REPOSITORY_URL_TYPE_SPIPE, MA_REPOSITORY_NAMESPACE_LOCAL, MA_PROXY_USAGE_TYPE_NONE, MA_REPOSITORY_AUTH_NONE,    MA_TRUE,  "lt-rejeesh.corp.nai.org", "lt-rejeesh",      "127.0.0.1",    80, "ftp:\\\\my_ftp_repository", "My_Domain", "",          "",     1000,      4,               3,    MA_REPOSITORY_STATE_ADDED,    443   },
};

static ma_error_t create_repository_db(const struct repository_data *repo_data, ma_uint16_t count)
{
    ma_error_t rc = MA_ERROR_UNEXPECTED;
    ma_repository_t *repository = NULL;
    ma_uint16_t index = 0;

    if(MA_OK == ma_repository_db_remove_all_repositories(ma_configurator_get_database(MA_CONTEXT_GET_CONFIGURATOR(ma_ut_setup_get_context(g_ut_setup)))))
    {
        for(;index < count; index++)
        {
            if(MA_OK == (rc = ma_repository_create(&repository)))
            {
                if((MA_OK == (rc = ma_repository_set_name(repository, repo_data[index].repo_name))) &&
                    (MA_OK == (rc = ma_repository_set_type(repository, repo_data[index].repo_type))) &&
                    (MA_OK == (rc = ma_repository_set_url_type(repository, repo_data[index].repo_url_type))) &&
                    (MA_OK == (rc = ma_repository_set_namespace(repository, repo_data[index].repo_namespace))) &&
                    (MA_OK == (rc = ma_repository_set_proxy_usage_type(repository, repo_data[index].repo_proxy_usage_type))) &&
                    (MA_OK == (rc = ma_repository_set_authentication(repository, repo_data[index].repo_auth))) &&
                    (MA_OK == (rc = ma_repository_set_enabled(repository, repo_data[index].repo_enabled))) &&
                    (MA_OK == (rc = ma_repository_set_server_fqdn(repository, repo_data[index].repo_server_fqdn))) &&
                    (MA_OK == (rc = ma_repository_set_server_name(repository, repo_data[index].repo_server_name))) &&
                    (MA_OK == (rc = ma_repository_set_server_ip(repository, repo_data[index].repo_server_ip))) &&
                    (MA_OK == (rc = ma_repository_set_port(repository, repo_data[index].repo_port))) &&
                    (MA_OK == (rc = ma_repository_set_ssl_port(repository, repo_data[index].ssl_port))) &&
                    (MA_OK == (rc = ma_repository_set_server_path(repository, repo_data[index].repo_server_path))) &&
                    (MA_OK == (rc = ma_repository_set_user_name(repository, repo_data[index].repo_user_name))) &&
                    (MA_OK == (rc = ma_repository_set_password(repository, repo_data[index].repo_password))) &&
                    (MA_OK == (rc = ma_repository_set_pingtime(repository, repo_data[index].repo_pingtime))) &&
                    (MA_OK == (rc = ma_repository_set_subnetdistance(repository, repo_data[index].repo_subnetdistance))) &&
                    (MA_OK == (rc = ma_repository_set_siteorder(repository, repo_data[index].repo_siteorder))) &&
                    (MA_OK == (rc = ma_repository_set_state(repository, repo_data[index].repo_state))))
                {
                    rc = ma_repository_db_add_repository(ma_configurator_get_database(MA_CONTEXT_GET_CONFIGURATOR(ma_ut_setup_get_context(g_ut_setup))), repository);
                }
            }

            if(repository)
            {
                ma_repository_release(repository);
            }
        }
    }
    return rc;
}

void spipe_response_handler(ma_error_t status, int response_code , void *userdata, ma_spipe_url_request_t *spipereq)
{
}

ma_error_t spipe_site_connect_handler(void *userdata, ma_spipe_url_request_t *spipereq)
{
	return MA_OK;
}

TEST(spipe_url_request_tests, verify_basics)
{
	ma_spipe_url_request_t *spipe_url_request = NULL; 
	ma_ah_site_url_provider_t *ah_site_url_provider = NULL;
	ma_stream_t *up_stream = NULL, *down_stream = NULL;
	ma_proxy_list_t *proxy_list = NULL;

	TEST_ASSERT(MA_OK == create_repository_db( repo_data_1, (sizeof(repo_data_1)/sizeof(repo_data_1[0]))));
	TEST_ASSERT(MA_OK == ma_net_client_service_start(MA_CONTEXT_GET_NET_CLIENT(ma_ut_setup_get_context(g_ut_setup))));

	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_spipe_url_request_create(NULL, &spipe_url_request));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_spipe_url_request_create(ma_ut_setup_get_context(g_ut_setup), NULL));
	TEST_ASSERT(MA_OK == ma_spipe_url_request_create(ma_ut_setup_get_context(g_ut_setup), &spipe_url_request));

	TEST_ASSERT(MA_OK == ma_fstream_create(UPSTREAM, MA_STREAM_ACCESS_FLAG_WRITE, NULL, &up_stream));
	TEST_ASSERT(MA_OK == ma_fstream_create(DOWNSTREAM, MA_STREAM_ACCESS_FLAG_WRITE, NULL, &down_stream));

	TEST_ASSERT(NULL == ma_spipe_url_request_get_upstream(NULL));
	TEST_ASSERT(NULL == ma_spipe_url_request_get_downstream(NULL));

	TEST_ASSERT(NULL == ma_spipe_url_request_get_upstream(spipe_url_request));
	TEST_ASSERT(NULL == ma_spipe_url_request_get_downstream(spipe_url_request));

	ma_spipe_url_request_set_upstream(NULL, up_stream);
	ma_spipe_url_request_set_downstream(NULL, down_stream);

	ma_spipe_url_request_set_upstream(spipe_url_request, NULL);
	ma_spipe_url_request_set_downstream(spipe_url_request, NULL);

	ma_spipe_url_request_set_upstream(spipe_url_request, up_stream);
	ma_spipe_url_request_set_downstream(spipe_url_request, down_stream);

	TEST_ASSERT(NULL != ma_spipe_url_request_get_upstream(spipe_url_request));
	TEST_ASSERT(NULL != ma_spipe_url_request_get_downstream(spipe_url_request));
	
	{
		ma_bool_t is_current_url_reachable = MA_FALSE;

		TEST_ASSERT(MA_TRUE == is_connection_to_next_site_needed(spipe_url_request, MA_ERROR_NETWORK_REQUEST_FAILED, 200, &is_current_url_reachable));
		TEST_ASSERT(MA_FALSE ==  is_current_url_reachable);

		TEST_ASSERT(MA_FALSE == is_connection_to_next_site_needed(spipe_url_request, MA_ERROR_NETWORK_REQUEST_SUCCEEDED, 200, &is_current_url_reachable));
		TEST_ASSERT(MA_TRUE ==  is_current_url_reachable);

		TEST_ASSERT(MA_FALSE == is_connection_to_next_site_needed(spipe_url_request, MA_ERROR_NETWORK_REQUEST_SUCCEEDED, 201, &is_current_url_reachable));
		TEST_ASSERT(MA_TRUE ==  is_current_url_reachable);

		TEST_ASSERT(MA_FALSE == is_connection_to_next_site_needed(spipe_url_request, MA_ERROR_NETWORK_REQUEST_SUCCEEDED, 503, &is_current_url_reachable));
		TEST_ASSERT(MA_TRUE ==  is_current_url_reachable);

		TEST_ASSERT(MA_FALSE == is_connection_to_next_site_needed(spipe_url_request, MA_ERROR_NETWORK_REQUEST_SUCCEEDED, 506, &is_current_url_reachable));
		TEST_ASSERT(MA_TRUE ==  is_current_url_reachable);

		TEST_ASSERT(MA_FALSE == is_connection_to_next_site_needed(spipe_url_request, MA_ERROR_NETWORK_REQUEST_SUCCEEDED, 202, &is_current_url_reachable));
		TEST_ASSERT(MA_TRUE ==  is_current_url_reachable);

		TEST_ASSERT(MA_FALSE == is_connection_to_next_site_needed(spipe_url_request, MA_ERROR_NETWORK_REQUEST_SUCCEEDED, 400, &is_current_url_reachable));
		TEST_ASSERT(MA_TRUE ==  is_current_url_reachable);

		TEST_ASSERT(MA_FALSE == is_connection_to_next_site_needed(spipe_url_request, MA_ERROR_NETWORK_REQUEST_SUCCEEDED, 403, &is_current_url_reachable));
		TEST_ASSERT(MA_TRUE ==  is_current_url_reachable);

		TEST_ASSERT(MA_FALSE == is_connection_to_next_site_needed(spipe_url_request, MA_ERROR_NETWORK_REQUEST_SUCCEEDED, 404, &is_current_url_reachable));
		TEST_ASSERT(MA_TRUE ==  is_current_url_reachable);

		TEST_ASSERT(MA_TRUE == is_connection_to_next_site_needed(spipe_url_request, MA_ERROR_NETWORK_REQUEST_SUCCEEDED, 401, &is_current_url_reachable));
		TEST_ASSERT(MA_TRUE ==  is_current_url_reachable);

		TEST_ASSERT(MA_TRUE == is_connection_to_next_site_needed(spipe_url_request, MA_ERROR_NETWORK_REQUEST_SUCCEEDED, 407, &is_current_url_reachable));
		TEST_ASSERT(MA_TRUE ==  is_current_url_reachable);

		TEST_ASSERT(MA_TRUE == is_connection_to_next_site_needed(spipe_url_request, MA_ERROR_NETWORK_REQUEST_SUCCEEDED, 500, &is_current_url_reachable));
		TEST_ASSERT(MA_TRUE ==  is_current_url_reachable);
	}

	TEST_ASSERT(MA_OK == ma_ah_site_url_provider_create(ma_ut_setup_get_context(g_ut_setup), &ah_site_url_provider));
	ma_ah_site_url_provider_load_sitelist(ah_site_url_provider);
	
	ma_spipe_url_request_set_connect_cb(NULL, &spipe_site_connect_handler);
	ma_spipe_url_request_set_connect_cb(spipe_url_request, NULL);
	ma_spipe_url_request_set_connect_cb(spipe_url_request, &spipe_site_connect_handler);

	ma_spipe_url_request_set_final_cb(NULL, &spipe_response_handler);
	ma_spipe_url_request_set_final_cb(spipe_url_request, NULL);
	ma_spipe_url_request_set_final_cb(spipe_url_request, &spipe_response_handler);

	TEST_ASSERT(MA_FALSE == ma_spipe_url_request_is_connected(spipe_url_request));

	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_spipe_url_request_send(spipe_url_request, NULL));
	ma_spipe_url_request_set_url_provider(NULL, ah_site_url_provider);
	ma_spipe_url_request_set_url_provider(spipe_url_request, NULL);
	ma_spipe_url_request_set_url_provider(spipe_url_request, ah_site_url_provider);
	

	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_spipe_url_request_send(NULL, NULL));
	//FAIL -> Memory Leak
	TEST_ASSERT(MA_OK == ma_spipe_url_request_send(spipe_url_request, NULL));
	TEST_ASSERT(MA_TRUE == ma_spipe_url_request_is_connected(spipe_url_request));

	{
	    ma_proxy_t *proxy = NULL;
		
		TEST_ASSERT(MA_OK == ma_proxy_list_create(&proxy_list));
		TEST_ASSERT(MA_OK == ma_proxy_create(&proxy));
		TEST_ASSERT(MA_OK == ma_proxy_set_address(proxy, "127.0.0.1"));
		TEST_ASSERT(MA_OK == ma_proxy_list_add_proxy(proxy_list, proxy));
		TEST_ASSERT(MA_OK == ma_proxy_release(proxy)); proxy = NULL;
		
		ma_spipe_url_request_set_proxy(NULL, proxy_list);
		ma_spipe_url_request_set_proxy(spipe_url_request, NULL);
		ma_spipe_url_request_set_proxy(spipe_url_request, proxy_list);
	}

	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_spipe_url_request_resend(NULL));
	TEST_ASSERT(MA_OK == ma_spipe_url_request_resend(spipe_url_request));
	
	TEST_ASSERT(NULL != ma_spipe_url_request_get_url_request(spipe_url_request));

	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_spipe_url_request_cancel(NULL));
	TEST_ASSERT(MA_OK == ma_spipe_url_request_cancel(spipe_url_request));

	ma_spipe_url_request_cleanup_streams(NULL);
	ma_spipe_url_request_cleanup_streams(spipe_url_request);

	ma_spipe_url_request_deinit(NULL);
	ma_spipe_url_request_deinit(spipe_url_request);

	ma_ah_site_url_provider_release(ah_site_url_provider);
	ma_stream_release(up_stream);
	ma_stream_release(down_stream);
		
	ma_spipe_url_request_release(NULL);
	ma_spipe_url_request_release(spipe_url_request);
	TEST_ASSERT(MA_OK == ma_net_client_service_stop(MA_CONTEXT_GET_NET_CLIENT(ma_ut_setup_get_context(g_ut_setup))));
}