#include <stdlib.h>
#include "ma/internal/services/ahclient/ma_spipe_decorator.h"

typedef struct ma_spipe_mock_decorator_s{
	ma_spipe_decorator_t base;
	int priority;
}ma_spipe_mock_decorator_t;

static ma_error_t ma_spipe_mock_decorator_decorate(ma_spipe_mock_decorator_t *self, ma_spipe_priority_t priority, ma_spipe_package_t *pkg){
	return MA_OK;
}

static ma_error_t ma_spipe_mock_decorator_get_priority(ma_spipe_mock_decorator_t *self, ma_spipe_priority_t *prority){
	*prority = (ma_spipe_priority_t) self->priority;
	switch(self->priority){
	case MA_SPIPE_PRIORITY_NONE:
			self->priority = MA_SPIPE_PRIORITY_NORMAL;
			break;
		case MA_SPIPE_PRIORITY_NORMAL:
			self->priority = MA_SPIPE_PRIORITY_IMMEDIATE;
			break;
		case MA_SPIPE_PRIORITY_IMMEDIATE:
			self->priority = MA_SPIPE_PRIORITY_NORMAL;
			break;
	}

	return MA_OK;
}

static ma_error_t ma_spipe_mock_decorator_destroy(ma_spipe_mock_decorator_t *self){	
	return MA_OK;
}

ma_spipe_decorator_methods_t ma_spipe_mock_decorator_methods={
	&ma_spipe_mock_decorator_decorate,
	&ma_spipe_mock_decorator_get_priority,
	&ma_spipe_mock_decorator_destroy
};
static ma_spipe_decorator_t* ma_spipe_mock_decorator_create(){
	static ma_spipe_mock_decorator_t self;	
	self.priority = MA_SPIPE_PRIORITY_NORMAL;
	ma_spipe_decorator_init( (ma_spipe_decorator_t*)&self, &ma_spipe_mock_decorator_methods, &self);
	return (ma_spipe_decorator_t*)&self;
}
