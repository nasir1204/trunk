#include <stdlib.h>
#include "ma/internal/services/ahclient/ma_spipe_handler.h"

typedef struct ma_spipe_mock_handler_s{
	ma_spipe_handler_t base;		
	ma_ah_client_service_t *ah_service;
}ma_spipe_mock_handler_t;

static ma_error_t ma_spipe_mock_handler_on_spipe_response(ma_spipe_mock_handler_t *self, const ma_spipe_response_t *spipe_response){
	if(self->ah_service){
		ma_ah_client_service_raise_spipe_alert(self->ah_service, "HandlerAlert");
		self->ah_service = NULL;
	}
	return MA_OK;
}

static ma_error_t ma_spipe_mock_handler_destroy(ma_spipe_mock_handler_t *self){	
	return MA_OK;
}
ma_spipe_handler_methods_t ma_spipe_mock_handler_methods={
	&ma_spipe_mock_handler_on_spipe_response,
	&ma_spipe_mock_handler_destroy	
};

static ma_spipe_handler_t* ma_spipe_mock_handler_create(ma_ah_client_service_t *ah_service){
	static ma_spipe_mock_handler_t self;	
	self.ah_service = ah_service;
	ma_spipe_handler_init((ma_spipe_handler_t*)&self, &ma_spipe_mock_handler_methods, &self);
	return (ma_spipe_handler_t*)&self;
}
