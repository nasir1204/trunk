#include "ma_ah_client_service_test_helper.h"
#include "ma/internal/core/msgbus/ma_msgbus_private.h"

ma_error_t ma_ah_client_service_test_helper_create(ma_ah_client_service_test_helper_t ** ma_ah_client_service_test_helper, ma_agent_configuration_t agent_config, ma_datastore_configuration_request_t ds_config_req)
{
	ma_error_t rc = MA_ERROR_UNEXPECTED;

	if(ma_ah_client_service_test_helper)
	{
		*ma_ah_client_service_test_helper = (ma_ah_client_service_test_helper_t *)calloc(1, sizeof(ma_ah_client_service_test_helper_t));

		if((MA_OK == ma_configurator_create(&agent_config, &((*ma_ah_client_service_test_helper)->ma_configurator))) &&
			// FAIL-> Memory leak
			(MA_OK == ma_configurator_intialize_datastores(((*ma_ah_client_service_test_helper)->ma_configurator), &ds_config_req)) &&
			(MA_OK == ma_configurator_configure_agent_policies(((*ma_ah_client_service_test_helper)->ma_configurator))) &&
			(MA_OK == ma_agent_policy_settings_bag_create(ma_configurator_get_policies_database(((*ma_ah_client_service_test_helper)->ma_configurator)), &((*ma_ah_client_service_test_helper)->ma_policy_settings_bag))) &&
			(MA_OK == ma_system_property_create(&((*ma_ah_client_service_test_helper)->ma_system_property), NULL)) &&
			(MA_OK == ma_file_logger_create(NULL, "test_file_logger", ".log",&((*ma_ah_client_service_test_helper)->ma_logger))) &&
			(MA_OK == ma_msgbus_create(MA_SOFTWAREID_GENERAL_STR, &((*ma_ah_client_service_test_helper)->ma_msgbus))) &&
			(MA_OK == ma_net_client_service_create(ma_msgbus_get_event_loop((*ma_ah_client_service_test_helper)->ma_msgbus), &((*ma_ah_client_service_test_helper)->ma_net_service))) &&
			(1 == create_macrypto_object_with_keys(MA_CRYPTO_MODE_NON_FIPS, MA_CRYPTO_ROLE_OFFICER, MA_CRYPTO_AGENT_MANAGED, &((*ma_ah_client_service_test_helper)->ma_crypto)))
			)
		{
			spipe_verify_key = MA_CRYPTO_VERIFY_AGENT_PUBLIC_KEY;
			rc = MA_OK;
		}	
	}
	else
	{
		rc = MA_ERROR_INVALIDARG;
	}
	return rc;
}
ma_error_t ma_ah_client_service_test_helper_initialize_context(ma_ah_client_service_test_helper_t *ma_ah_client_service_test_helper)
{
	ma_error_t rc = MA_ERROR_UNEXPECTED;
	if(ma_ah_client_service_test_helper)
	{
		if((MA_OK == (rc = ma_context_create(&ma_ah_client_service_test_helper->ma_context))) &&
			(MA_OK == (rc = ma_context_add_object_info(ma_ah_client_service_test_helper->ma_context, MA_OBJECT_MA_CONFIGURATOR_NAME_STR, (void*const*)&ma_ah_client_service_test_helper->ma_configurator))) &&
			(MA_OK == (rc = ma_context_add_object_info(ma_ah_client_service_test_helper->ma_context, MA_OBJECT_POLICY_SETTINGS_BAG_STR, (void*const*)&ma_ah_client_service_test_helper->ma_policy_settings_bag))) &&	  
			(MA_OK == (rc = ma_context_add_object_info(ma_ah_client_service_test_helper->ma_context, MA_OBJECT_LOGGER_NAME_STR, (void*const*)&ma_ah_client_service_test_helper->ma_logger))) &&
			(MA_OK == (rc = ma_context_add_object_info(ma_ah_client_service_test_helper->ma_context, MA_OBJECT_SYSTEM_PROPERTY_STR, (void*const*)&ma_ah_client_service_test_helper->ma_system_property))) &&
			(MA_OK == (rc = ma_context_add_object_info(ma_ah_client_service_test_helper->ma_context, MA_OBJECT_MSGBUS_NAME_STR, (void*const*)&ma_ah_client_service_test_helper->ma_msgbus))) &&
			(NULL != (ma_ah_client_service_test_helper->ma_policy_bag = ma_agent_policy_settings_bag_get_policy_bag(ma_ah_client_service_test_helper->ma_policy_settings_bag))) &&
			(MA_OK == (rc = ma_context_add_object_info(ma_ah_client_service_test_helper->ma_context, MA_OBJECT_POLICY_BAG_STR, (void*const*)ma_ah_client_service_test_helper->ma_policy_bag))) &&
			(MA_OK == (rc = ma_context_add_object_info(ma_ah_client_service_test_helper->ma_context, MA_OBJECT_CRYPTO_NAME_STR, (void*const*)&ma_ah_client_service_test_helper->ma_crypto))) &&
			(MA_OK == (rc = ma_context_add_object_info(ma_ah_client_service_test_helper->ma_context, MA_OBJECT_NET_CLIENT_NAME_STR, (void*const*)&ma_ah_client_service_test_helper->ma_net_service)))
			)
		{}
	}
	else
	{
		rc = MA_ERROR_INVALIDARG;
	}
	return rc;
}

ma_error_t ma_ah_client_service_test_helper_deinitialize_context(ma_ah_client_service_test_helper_t *ma_ah_client_service_test_helper)
{
	ma_error_t rc = MA_OK;
	if(ma_ah_client_service_test_helper)
	{
		if(ma_ah_client_service_test_helper->ma_context)
		{
				ma_context_release(ma_ah_client_service_test_helper->ma_context); 
				ma_ah_client_service_test_helper->ma_context = NULL;
		}
	}

	return rc;
}

ma_error_t ma_ah_client_service_test_helper_release(ma_ah_client_service_test_helper_t *ma_ah_client_service_test_helper)
{
	ma_error_t rc = MA_OK;
	
	if(ma_ah_client_service_test_helper != NULL)
	{		
		if(ma_ah_client_service_test_helper->ma_system_property) ma_system_property_release(ma_ah_client_service_test_helper->ma_system_property);
		if(ma_ah_client_service_test_helper->ma_policy_settings_bag) ma_policy_settings_bag_release(ma_ah_client_service_test_helper->ma_policy_settings_bag);
		if(ma_ah_client_service_test_helper->ma_configurator) ma_configurator_release(ma_ah_client_service_test_helper->ma_configurator);
		if(ma_ah_client_service_test_helper->ma_msgbus) ma_msgbus_release(ma_ah_client_service_test_helper->ma_msgbus);
		if(ma_ah_client_service_test_helper->ma_net_service) ma_net_client_service_release(ma_ah_client_service_test_helper->ma_net_service);
		if(ma_ah_client_service_test_helper->ma_logger) ma_file_logger_release(ma_ah_client_service_test_helper->ma_logger);
		if(ma_ah_client_service_test_helper->ma_crypto) release_macrypto_object(MA_CRYPTO_MODE_NON_FIPS, MA_CRYPTO_ROLE_OFFICER, MA_CRYPTO_AGENT_MANAGED, ma_ah_client_service_test_helper->ma_crypto);

		free(ma_ah_client_service_test_helper);
		ma_ah_client_service_test_helper = NULL;
	}

	return rc;
}

ma_crypto_verify_key_type_t MA_SPIPE_VERIFY_KEY()
{
	return spipe_verify_key;
}