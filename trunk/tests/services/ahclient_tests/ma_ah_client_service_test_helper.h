#ifndef MA_AH_CLIENT_SERVICE_TEST_HELPER_H_INCLUDED
#define MA_AH_CLIENT_SERVICE_TEST_HELPER_H_INCLUDED

#include "ma/internal/utils/configurator/ma_configurator_utils.h"
#include "ma/internal/services/ma_agent_policy_settings_bag.h"
#include "ma/internal/utils/platform/ma_system_property.h"
#include "ma/logger/ma_file_logger.h"
#include "ma/internal/clients/ma/ma_client_internal.h"
#include "ma/internal/utils/network/ma_net_client_service.h"
#include "tests/utils/macrypto_test/ma_crypto_tests_setup.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

typedef struct ma_ah_client_service_test_helper_s
{
    ma_context_t                *ma_context;
    ma_configurator_t           *ma_configurator;
    ma_policy_settings_bag_t    *ma_policy_settings_bag;
    ma_system_property_t        *ma_system_property;
    ma_client_t                 *ma_client;
    ma_policy_bag_t             ** ma_policy_bag;
    ma_file_logger_t            *ma_logger;
    ma_msgbus_t                 *ma_msgbus;
    ma_crypto_t                 *ma_crypto;
    ma_net_client_service_t     *ma_net_service;

}ma_ah_client_service_test_helper_t;

ma_crypto_verify_key_type_t spipe_verify_key;

ma_error_t ma_ah_client_service_test_helper_create(ma_ah_client_service_test_helper_t ** context_helper, ma_agent_configuration_t agent_config, ma_datastore_configuration_request_t ds_config_req);
ma_error_t ma_ah_client_service_test_helper_initialize_context(ma_ah_client_service_test_helper_t *context_helper);
ma_error_t ma_ah_client_service_test_helper_deinitialize_context(ma_ah_client_service_test_helper_t *ma_ah_client_service_test_helper);
ma_error_t ma_ah_client_service_test_helper_release(ma_ah_client_service_test_helper_t *context_helper);

#endif //MA_AH_CLIENT_SERVICE_TEST_HELPER_H_INCLUDED

