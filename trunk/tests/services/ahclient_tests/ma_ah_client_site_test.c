#include "unity.h"
#include "unity_fixture.h"
#include <string.h>
#include "ma/internal/services/ahclient/ma_ah_site.h"

ma_ah_site_t *ah_site;

TEST_GROUP_RUNNER(ah_site_tests_group) 
{
    do {RUN_TEST_CASE(ah_site_tests, verify_ah_site);} while (0);
}

TEST_SETUP(ah_site_tests)
{
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_site_create(NULL));
	TEST_ASSERT(MA_OK == ma_ah_site_create(&ah_site));
}

TEST_TEAR_DOWN(ah_site_tests)
{
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_site_release(NULL));
	TEST_ASSERT(MA_OK == ma_ah_site_release(ah_site));
}

typedef struct ah_site_test_data_s
{
    char *name;
    char *ver;
    char *fqdn;
    char *short_name;
    char *ip;
    ma_uint32_t port;
    ma_uint32_t ssl_port;
    ma_uint32_t order;
}ah_site_test_data_t;

static ah_site_test_data_t ah_site_test_data[] = {
	{"EPO_SER3", "5.0.0", "test1.corp.nai.org", "test1", "172.16.220.2", 80, 443, 3},
	{"EPO_SER3", "5.0.0", "test2.corp.nai.org", "test2", "172.16.220.3", 80, 443, 4}, };

extern ma_uint32_t ma_ah_site_get_order(ma_ah_site_t *self);

TEST(ah_site_tests, verify_ah_site)
{
	ma_uint16_t index = 0;

	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_site_set_name(NULL, ah_site_test_data[index].name));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_site_set_name(ah_site, NULL));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_site_set_version(NULL, ah_site_test_data[index].ver));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_site_set_version(ah_site, NULL));
	TEST_ASSERT(MA_ERROR_INVALIDARG  == ma_ah_site_set_fqdn_name(NULL, ah_site_test_data[index].fqdn));
	TEST_ASSERT(MA_ERROR_INVALIDARG  == ma_ah_site_set_fqdn_name(ah_site, NULL));
	TEST_ASSERT(MA_ERROR_INVALIDARG  == ma_ah_site_set_short_name(NULL, ah_site_test_data[index].short_name));
	TEST_ASSERT(MA_ERROR_INVALIDARG  == ma_ah_site_set_short_name(ah_site, NULL));
	TEST_ASSERT(MA_ERROR_INVALIDARG  == ma_ah_site_set_ip_address(NULL, ah_site_test_data[index].ip));
	TEST_ASSERT(MA_ERROR_INVALIDARG  == ma_ah_site_set_ip_address(ah_site, NULL));
	TEST_ASSERT(MA_ERROR_INVALIDARG  == ma_ah_site_set_port(NULL, ah_site_test_data[index].port));	
	TEST_ASSERT(MA_ERROR_INVALIDARG  == ma_ah_site_set_ssl_port(NULL, ah_site_test_data[index].ssl_port));
	TEST_ASSERT(MA_ERROR_INVALIDARG  == ma_ah_site_set_order(NULL, ah_site_test_data[index].order));

	TEST_ASSERT(NULL == ma_ah_site_get_name(NULL));	
	TEST_ASSERT(NULL == ma_ah_site_get_version(NULL));
	TEST_ASSERT(NULL == ma_ah_site_get_fqdn_name(NULL));
	TEST_ASSERT(NULL  == ma_ah_site_get_short_name(NULL));
	TEST_ASSERT(NULL == ma_ah_site_get_ip_address(NULL));
	TEST_ASSERT(NULL  == ma_ah_site_get_ip_address(ah_site));
	TEST_ASSERT(-1 == ma_ah_site_get_port(NULL));	
	TEST_ASSERT(-1  == ma_ah_site_get_ssl_port(NULL));
	TEST_ASSERT(-1  == ma_ah_site_get_order(NULL));

	for(index = 0; index < sizeof(ah_site_test_data)/sizeof(ah_site_test_data[0]); index++)
	{
		TEST_ASSERT(MA_OK == ma_ah_site_set_name(ah_site, ah_site_test_data[index].name));
		TEST_ASSERT(MA_OK == ma_ah_site_set_version(ah_site, ah_site_test_data[index].ver));
		TEST_ASSERT(MA_OK == ma_ah_site_set_fqdn_name(ah_site, ah_site_test_data[index].fqdn));
		TEST_ASSERT(MA_OK == ma_ah_site_set_short_name(ah_site, ah_site_test_data[index].short_name));
		TEST_ASSERT(MA_OK == ma_ah_site_set_ip_address(ah_site, ah_site_test_data[index].ip));
		TEST_ASSERT(MA_OK == ma_ah_site_set_port(ah_site, ah_site_test_data[index].port));
		TEST_ASSERT(MA_OK == ma_ah_site_set_ssl_port(ah_site, ah_site_test_data[index].ssl_port));
		TEST_ASSERT(MA_OK == ma_ah_site_set_order(ah_site, ah_site_test_data[index].order));

		TEST_ASSERT(0 == strcmp(ma_ah_site_get_name(ah_site), ah_site_test_data[index].name));
		TEST_ASSERT(0 == strcmp(ma_ah_site_get_version(ah_site), ah_site_test_data[index].ver));
		TEST_ASSERT(0 == strcmp(ma_ah_site_get_fqdn_name(ah_site), ah_site_test_data[index].fqdn));
		TEST_ASSERT(0 == strcmp(ma_ah_site_get_short_name(ah_site), ah_site_test_data[index].short_name));
		TEST_ASSERT(0 == strcmp(ma_ah_site_get_ip_address(ah_site), ah_site_test_data[index].ip));
		TEST_ASSERT(ma_ah_site_get_port(ah_site) == ah_site_test_data[index].port);
		TEST_ASSERT(ma_ah_site_get_ssl_port(ah_site) == ah_site_test_data[index].ssl_port);
		TEST_ASSERT(ma_ah_site_get_order(ah_site) == ah_site_test_data[index].order);
	}
}