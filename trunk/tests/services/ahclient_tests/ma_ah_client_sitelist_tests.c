#include "unity.h"
#include "unity_fixture.h"

#include "ma/internal/services/ahclient/ma_ah_sitelist.h"

#include <string.h>

#ifdef WIN32
#define CA_FILE_NAME        "C:\\Windows\\temp\\ah_test.cer"
#else
#define CA_FILE_NAME        "/tmp/ah_test.cer"
#endif
#define GLOBAL_VERSION      "VlUZlJaB6wuBd75vKrG/LUUa2dLd59wdI0BOR9lUmIs="
#define LOCAL_VERSION       "Mon, 16 Jul 2012 11:52:32 UTC"
#define STRING_MODIFIED "_Modified"

typedef struct test_data_s
{
    char *name;
    char *ver;
    char *fqdn;
    char *short_name;
    char *ip;
    ma_uint32_t port;
    ma_uint32_t ssl_port;
    ma_uint32_t order;
} test_data_t;

test_data_t *data = NULL;
size_t test_data_size = 0;
/* test data for ordered site list */
test_data_t data1[] = { {"EPO_SER5_1", "5.0.0", "test5_1.corp.nai.org", "test5_1", "172.16.45.56", 80, 443, 5},
                        {"EPO_SER3", "5.0.0", "test3.corp.nai.org", "test3", "172.16.220.45", 80, 443, 3},
                        {"EPO_SER1", "5.0.0", "test1.corp.nai.org", "test1", "172.16.196.56", 80, 443, 1},
                        {"EPO_SER5_2", "5.0.0", "test5_2.corp.nai.org", "test5_2", "172.16.45.56", 80, 443, 5},
                        {"EPO_SER2_0", "5.0.0", "test2_0.corp.nai.org", "test2_0", "172.16.198.45", 80, 443, 2},
                        {"EPO_SER5_0", "5.0.0", "test5_0.corp.nai.org", "test5_0", "172.16.45.56", 80, 443, 5},
                        {"EPO_SER4", "4.8.0", "test4.corp.nai.org", "test4", "172.16.189.45", 80, 443, 4},
                        {"EPO_SER2_1", "5.0.0", "test2_1.corp.nai.org", "test2_1", "172.16.198.45", 80, 443, 2},
                      };
/* test data for sites with equal preferance, i.e, for randomization testing */
test_data_t data2[] = { {"EPO_SER3", "5.0.0", "test1.corp.nai.org", "test1", "172.16.220.45", 80, 443, 1},
                        {"EPO_SER1", "5.0.0", "test2.corp.nai.org", "test2", "172.16.196.56", 80, 443, 1},
                        {"EPO_SER2", "5.0.0", "test4.corp.nai.org", "test4", "172.16.198.45", 80, 443, 1},
                        {"EPO_SER5", "5.0.0", "test7.corp.nai.org", "test7", "172.16.45.56", 80, 443, 1},
                        {"EPO_SER4", "4.8.0", "test9.corp.nai.org", "test9", "172.16.189.45", 80, 443, 1}
                      };

static ma_ah_sitelist_t *sitelist;

TEST_GROUP_RUNNER(ah_client_sitelist_tests_group)
{
    data = data1;
    test_data_size = sizeof(data1)/sizeof(data1[0]);
    do {RUN_TEST_CASE(ah_client_sitelist_tests, verify_sitelist_set_with_different_orders);} while (0);
    do {RUN_TEST_CASE(ah_client_sitelist_tests, verify_sitelist_get_with_different_orders);} while (0);

    data = data2;
    test_data_size = sizeof(data2)/sizeof(data2[0]);
    do {RUN_TEST_CASE(ah_client_sitelist_tests, verify_sitelist_set_with_different_orders);} while (0);
    do {RUN_TEST_CASE(ah_client_sitelist_tests, verify_sitelist_get_with_different_orders);} while (0);
}

TEST_SETUP(ah_client_sitelist_tests)
{
}

TEST_TEAR_DOWN(ah_client_sitelist_tests)
{

}

extern ma_uint32_t ma_ah_site_get_order(ma_ah_site_t *self);

TEST(ah_client_sitelist_tests, verify_sitelist_set_with_different_orders)
{
    ma_uint16_t index = 0;

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_sitelist_create(NULL));
    TEST_ASSERT(MA_OK == ma_ah_sitelist_create(&sitelist));
    TEST_ASSERT(MA_OK == ma_ah_sitelist_release(sitelist));
    TEST_ASSERT(MA_OK == ma_ah_sitelist_create(&sitelist));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_sitelist_set_global_version(NULL, GLOBAL_VERSION));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_sitelist_set_global_version(sitelist, NULL));
    TEST_ASSERT(MA_OK == ma_ah_sitelist_set_global_version(sitelist, GLOBAL_VERSION));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_sitelist_set_local_version(NULL, LOCAL_VERSION));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_sitelist_set_local_version(sitelist, NULL));
    TEST_ASSERT(MA_OK == ma_ah_sitelist_set_local_version(sitelist, LOCAL_VERSION));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_sitelist_set_ca_file(NULL, CA_FILE_NAME));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_sitelist_set_ca_file(sitelist, NULL));
    TEST_ASSERT(MA_OK == ma_ah_sitelist_set_ca_file(sitelist, CA_FILE_NAME));

    for(index = 0; index < test_data_size ; index++)
    {
        ma_ah_site_t *site = NULL;
        TEST_ASSERT(MA_OK == ma_ah_site_create(&site));
        TEST_ASSERT(MA_OK == ma_ah_site_set_name(site, data[index].name));
        TEST_ASSERT(MA_OK == ma_ah_site_set_version(site, data[index].ver));
        TEST_ASSERT(MA_OK == ma_ah_site_set_fqdn_name(site, data[index].fqdn));
        TEST_ASSERT(MA_OK == ma_ah_site_set_short_name(site, data[index].short_name));
        TEST_ASSERT(MA_OK == ma_ah_site_set_ip_address(site, data[index].ip));
        TEST_ASSERT(MA_OK == ma_ah_site_set_port(site, data[index].port));
        TEST_ASSERT(MA_OK == ma_ah_site_set_ssl_port(site, data[index].ssl_port));
        TEST_ASSERT(MA_OK == ma_ah_site_set_order(site, data[index].order));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_sitelist_add_site(NULL, site));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_sitelist_add_site(sitelist, NULL));
        TEST_ASSERT(MA_OK == ma_ah_sitelist_add_site(sitelist, site));
    }
}

static ma_error_t modify_site_data_cb(ma_ah_site_t *ah_site, void *cb_data)
{
    ma_ah_sitelist_t *self = (ma_ah_sitelist_t*)cb_data;

    if(self)
    {       
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


TEST(ah_client_sitelist_tests, verify_sitelist_get_with_different_orders)
{
    ma_uint16_t index = 0;
    ma_ah_sitelist_t *temp_sitelist;

    TEST_ASSERT(NULL == ma_ah_sitelist_get_global_version(NULL));
    TEST_ASSERT(0 == strcmp(ma_ah_sitelist_get_global_version(sitelist), GLOBAL_VERSION));

    TEST_ASSERT(NULL == ma_ah_sitelist_get_local_version(NULL));
    TEST_ASSERT(0 == strcmp(ma_ah_sitelist_get_local_version(sitelist), LOCAL_VERSION));

    TEST_ASSERT(NULL == ma_ah_sitelist_get_ca_file(NULL));
    TEST_ASSERT(0 == strcmp(ma_ah_sitelist_get_ca_file(sitelist), CA_FILE_NAME));

    // FAIL-> TEST_ASSERT(0 == ma_ah_sitelist_get_count(NULL));
    TEST_ASSERT(test_data_size == ma_ah_sitelist_get_count(sitelist));

    TEST_ASSERT(NULL == ma_ah_sitelist_get_site(0, NULL));
    TEST_ASSERT(NULL == ma_ah_sitelist_get_site(-1, sitelist));

    for(index = 0; index < ma_ah_sitelist_get_count(sitelist) ; index++)
    {
        ma_ah_site_t *site = NULL;
        TEST_ASSERT(NULL != (site = ma_ah_sitelist_get_site(index, sitelist)));

        TEST_ASSERT(0 == strcmp(ma_ah_site_get_name(site), data[index].name));
        TEST_ASSERT(0 == strcmp(ma_ah_site_get_version(site), data[index].ver));
        TEST_ASSERT(0 == strcmp(ma_ah_site_get_fqdn_name(site), data[index].fqdn));
        TEST_ASSERT(0 == strcmp(ma_ah_site_get_short_name(site), data[index].short_name));
        TEST_ASSERT(0 == strcmp(ma_ah_site_get_ip_address(site), data[index].ip));
        TEST_ASSERT(ma_ah_site_get_port(site) == data[index].port);
        TEST_ASSERT(ma_ah_site_get_ssl_port(site) == data[index].ssl_port);
        TEST_ASSERT(ma_ah_site_get_order(site) == data[index].order);
    }

    TEST_ASSERT(MA_OK == ma_ah_sitelist_create(&temp_sitelist));
    // FAIL-> TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_sitelist_adjust_site_order(NULL));
    TEST_ASSERT(MA_OK == ma_ah_sitelist_adjust_site_order(temp_sitelist));
    TEST_ASSERT(MA_OK == ma_ah_sitelist_adjust_site_order(sitelist));

    TEST_ASSERT(MA_OK == ma_ah_sitelist_release(temp_sitelist));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_sitelist_for_each(NULL, modify_site_data_cb, sitelist));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_sitelist_for_each(sitelist, NULL, sitelist));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_sitelist_for_each(sitelist, modify_site_data_cb, NULL));
    TEST_ASSERT(MA_OK == ma_ah_sitelist_for_each(sitelist, modify_site_data_cb, sitelist));

    for(index = 0; index < ma_ah_sitelist_get_count(sitelist) ; index++)
    {
        ma_ah_site_t *site = NULL;
        TEST_ASSERT(NULL != (site = ma_ah_sitelist_get_site(index, sitelist)));

        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_sitelist_remove_site(NULL, site));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_sitelist_remove_site(sitelist, NULL));
        TEST_ASSERT(MA_OK == ma_ah_sitelist_remove_site(sitelist, site));
    }

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_sitelist_release(NULL));
    TEST_ASSERT(MA_OK == ma_ah_sitelist_release(sitelist));
}

