#ifndef MA_CONTEXT_CREATE_HELPER_H_INCLUDED
#define MA_CONTEXT_CREATE_HELPER_H_INCLUDED

#include "ma/internal/utils/configurator/ma_configurator_utils.h"
#include "ma/internal/services/ma_agent_policy_settings_bag.h"
#include "ma/internal/utils/platform/ma_system_property.h"
#include "ma/logger/ma_file_logger.h"
#include "ma/internal/core/msgbus/ma_msgbus_private.h"
#include "ma/internal/clients/ma/ma_client_internal.h"
#include "ma/internal/utils/network/ma_net_client_service.h"
#include "tests/utils/macrypto_test/ma_crypto_tests_setup.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

typedef struct ma_context_helper_s 
{
	ma_context_t				*ma_context;
	ma_configurator_t			*ma_configurator;
	ma_policy_settings_bag_t	*ma_policy_settings_bag;
	ma_system_property_t		*ma_system_property;
	ma_client_t					*ma_client;
	ma_policy_bag_t				** ma_policy_bag;
	ma_file_logger_t			*ma_logger;
	ma_msgbus_t					*ma_msgbus;
	ma_crypto_t					*ma_crypto;
	ma_net_client_service_t     *ma_net_service;
}ma_context_helper_t;

ma_error_t ma_context_helper_create(ma_context_helper_t ** context_helper, ma_agent_configuration_t agent_config, ma_datastore_configuration_request_t ds_config_req)
{
	ma_error_t rc = MA_ERROR_UNEXPECTED;

	if(context_helper)
	{
		*context_helper = (ma_context_helper_t *)calloc(1, sizeof(ma_context_helper_t));

		if((MA_OK == ma_configurator_create(&agent_config, &((*context_helper)->ma_configurator))) &&
			(MA_OK == ma_configurator_intialize_datastores(((*context_helper)->ma_configurator), &ds_config_req)) &&
			(MA_OK == ma_configurator_configure_agent_policies(((*context_helper)->ma_configurator))) &&
			(MA_OK == ma_agent_policy_settings_bag_create(ma_configurator_get_policies_database(((*context_helper)->ma_configurator)), &((*context_helper)->ma_policy_settings_bag))) &&
			(MA_OK == ma_system_property_create(&((*context_helper)->ma_system_property), NULL)) &&
			//(MA_OK == ma_client_create(MA_SOFTWAREID_GENERAL_STR, &((*context_helper)->ma_client))) &&
			//(MA_OK == ma_client_set_thread_option(((*context_helper)->ma_client), MA_MSGBUS_CALLBACK_THREAD_IO)) &&
			(MA_OK == ma_file_logger_create(NULL, "test_file_logger", ".log",&((*context_helper)->ma_logger))) &&
			(MA_OK == ma_msgbus_create(MA_SOFTWAREID_GENERAL_STR, &((*context_helper)->ma_msgbus))) &&
			(MA_OK == ma_net_client_service_create(ma_msgbus_get_event_loop((*context_helper)->ma_msgbus), &((*context_helper)->ma_net_service))) /*&&
			(1 == create_macrypto_object_with_keys(MA_CRYPTO_MODE_NON_FIPS, MA_CRYPTO_ROLE_OFFICER, MA_CRYPTO_AGENT_MANAGED, &((*context_helper)->ma_crypto)))*/)
		{
			rc = MA_OK;
		}	
	}
	else
	{
		rc = MA_ERROR_INVALIDARG;
	}
	return rc;
}

ma_error_t ma_context_helper_initialize_context(ma_context_helper_t *context_helper)
{
	ma_error_t rc = MA_ERROR_UNEXPECTED;
	if(context_helper)
	{
		if((MA_OK == (rc = ma_context_create(&context_helper->ma_context))) &&
			(MA_OK == (rc = ma_context_add_object_info(context_helper->ma_context, MA_OBJECT_MA_CONFIGURATOR_NAME_STR, (void*const*)&context_helper->ma_configurator))) &&
			//(MA_OK == (rc = ma_context_add_object_info(context_helper->ma_context, MA_OBJECT_CLIENT_NAME_STR, (void*const*)&context_helper->ma_client))) &&
			(MA_OK == (rc = ma_context_add_object_info(context_helper->ma_context, MA_OBJECT_POLICY_SETTINGS_BAG_STR, (void*const*)&context_helper->ma_policy_settings_bag))) &&	  
			(MA_OK == (rc = ma_context_add_object_info(context_helper->ma_context, MA_OBJECT_LOGGER_NAME_STR, (void*const*)&context_helper->ma_logger))) &&
			(MA_OK == (rc = ma_context_add_object_info(context_helper->ma_context, MA_OBJECT_SYSTEM_PROPERTY_STR, (void*const*)&context_helper->ma_system_property))) &&
			(MA_OK == (rc = ma_context_add_object_info(context_helper->ma_context, MA_OBJECT_MSGBUS_NAME_STR, (void*const*)&context_helper->ma_msgbus))) &&
			(NULL != (context_helper->ma_policy_bag = ma_agent_policy_settings_bag_get_policy_bag(context_helper->ma_policy_settings_bag))) &&
			(MA_OK == (rc = ma_context_add_object_info(context_helper->ma_context, MA_OBJECT_POLICY_BAG_STR, (void*const*)context_helper->ma_policy_bag))) &&
			//(MA_OK == (rc = ma_context_add_object_info(context_helper->ma_context, MA_OBJECT_CRYPTO_NAME_STR, (void*const*)context_helper->ma_crypto))) &&
			(MA_OK == (rc = ma_context_add_object_info(context_helper->ma_context, MA_OBJECT_NET_CLIENT_NAME_STR, (void*const*)context_helper->ma_net_service))))
		{}
	}
	else
	{
		rc = MA_ERROR_INVALIDARG;
	}
	return rc;
}

ma_error_t ma_context_helper_release(ma_context_helper_t *context_helper)
{
	ma_error_t rc = MA_ERROR_UNEXPECTED;
	
	if(context_helper != NULL)
	{		
		//if(context_helper->ma_client) ma_client_release(context_helper->ma_client);
		if(context_helper->ma_system_property) ma_system_property_release(context_helper->ma_system_property);
		if(context_helper->ma_policy_settings_bag) ma_policy_settings_bag_release(context_helper->ma_policy_settings_bag);
		if(context_helper->ma_configurator) ma_configurator_release(context_helper->ma_configurator);
		
		free(context_helper);
		context_helper = NULL;
	}

	return rc;
}

#endif