#include "unity.h"
#include "unity_fixture.h"
#include <string.h>
#include "ma/internal/services/ahclient/ma_ah_sitelist.h"

ma_ah_sitelist_t *ah_sitelist;

TEST_GROUP_RUNNER(ah_site_tests_group) 
{
    do {RUN_TEST_CASE(ah_site_tests, verify_ah_site_list);} while (0);
}

TEST_SETUP(ah_site_tests)
{
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_site_create(NULL));
	TEST_ASSERT(MA_OK == ma_ah_site_create(&ah_site));
}

TEST_TEAR_DOWN(ah_site_tests)
{
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_site_release(NULL));
	TEST_ASSERT(MA_OK == ma_ah_site_release(ah_site));
}

typedef struct ah_site_test_data_s
{
    char *name;
    char *ver;
    char *fqdn;
    char *short_name;
    char *ip;
    ma_uint32_t port;
    ma_uint32_t ssl_port;
    ma_uint32_t order;
}ah_site_test_data_t;

static ah_site_test_data_t ah_site_test_data[] = {
	{"EPO_SER3", "5.0.0", "test1.corp.nai.org", "test1", "172.16.220.2", 80, 443, 3},
	{"EPO_SER3", "5.0.0", "test2.corp.nai.org", "test2", "172.16.220.3", 80, 443, 4}, };

extern ma_uint32_t ma_ah_site_get_order(ma_ah_site_t *self);

TEST(ah_site_tests, verify_ah_site)
{
}