/*****************************************************************************
Main Test runner for ah_client_service unit tests.
******************************************************************************/
#include "unity.h"
#include "unity_fixture.h"
#include "ma_application_internal.h"
#include "ma/internal/utils/app_info/ma_application.h"
#include "ma/internal/ma_macros.h"
#include "ma/datastore/ma_ds.h"
#include "ma/internal/utils/xml/ma_xml.h"
#include "ma/internal/utils/xml/ma_xml_node.h"
#include <stdlib.h>
#include <stdio.h>
#include "ma_ah_client_service_test_helper.h"
#ifdef MA_WINDOWS
    #include "ma/datastore/ma_ds_registry.h"
    #include <windows.h>
#endif

#include "ma/internal/defs/ma_general_defs.h"
#include "ma_ut_setup.h"

#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include "ma/internal/utils/filesystem/path.h"

#ifdef _MSC_VER
    #include <crtdbg.h> /* */
    #define MA_TRACKLEAKS _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF)
#else
    #define MA_TRACKLEAKS ((void *)0)
#endif

static ma_ut_setup_t *g_ut_setup = NULL;

ma_ut_setup_t *get_ut_setup()
{
	return g_ut_setup;
}
static void run_all_tests(void)
{
	ma_ut_setup_create("ahclient", "ahclient.ut", &g_ut_setup);
	do { RUN_TEST_GROUP(ah_client_service_tests_group);} while(0);
	do { RUN_TEST_GROUP(ah_site_tests_group);} while(0);
	do { RUN_TEST_GROUP(ah_client_sitelist_tests_group);} while(0);
	do { RUN_TEST_GROUP(ah_client_site_url_provider_tests_group);} while(0);
	do { RUN_TEST_GROUP(spipe_key_manager_tests_group);} while(0);
	do { RUN_TEST_GROUP(spipe_sitelist_manager_tests_group);} while(0);
	do { RUN_TEST_GROUP(spipe_mux_demux_tests_group);} while(0);
	do { RUN_TEST_GROUP(spipe_url_request_tests_group);} while(0);
	ma_ut_setup_release(g_ut_setup);
}

int main(int argc, char *argv[]) 
{
    ma_int32_t ret = 0;
    MA_TRACKLEAKS;
    return UnityMain(argc, argv, run_all_tests);;
}


