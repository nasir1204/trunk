#include "ma_ut_setup.h"

#include "ma/internal/services/ma_service.h"
#include "ma/internal/clients/ma/ma_client_internal.h"
#include "ma/logger/ma_file_logger.h"

#include "ma/internal/ma_macros.h"

#include "ma/internal/services/ma_policy_settings_bag.h"
#include "ma/internal/utils/configurator/ma_configurator.h"

#include "ma/internal/utils/macrypto/ma_crypto.h"
#include "ma/datastore/ma_ds_database.h"
#include "ma/internal/ma_strdef.h"

#include "ma/internal/utils/network/ma_net_client_service.h"
#include "ma/internal/core/msgbus/ma_msgbus_private.h"
#include "tests/utils/macrypto_test/ma_crypto_tests_setup.h"
#include "ma/internal/utils/repository/ma_repository_db_manager.h"

#include "ma/datastore/ma_ds.h"
#include "ma/internal/utils/xml/ma_xml.h"
#include "ma/internal/utils/xml/ma_xml_node.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include "ma/internal/utils/filesystem/path.h"
#include "ma_application_internal.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#ifdef MA_WINDOWS
    #include "ma/datastore/ma_ds_registry.h"
    #include <windows.h>
#endif

#define MA_UT_DB_SCHEDULER_FILENAME_STR         "ma.ut.db.scheduler"
#define MA_UT_DB_POLICY_FILENAME_STR            "ma.ut.db.policy"
#define MA_UT_DB_TASK_FILENAME_STR              "ma.ut.db.task"
#define MA_UT_DB_COMMON_SVC_FILENAME_STR        "ma.ut.db.cmnsvc"
#define MA_UT_DB_DEFAULT_FILENAME_STR           "ma.ut.db.default"

#ifdef MA_WINDOWS
    #define TEST_AGENT_ROOT    ".\\AGENT\\"
    #define TEST_AGENT_DATA_PATH ".\\AGENT\\"
    #define TEST_AGENT_SETUP_DATA_PATH ".\\AGENT\\db"
    #define TEST_AGENT_INSTALL_PATH ".\\AGENT\\"
#else
    #define TEST_AGENT_ROOT    "./AGENT/"
    #define TEST_AGENT_DATA_PATH "./AGENT/"
    #define TEST_AGENT_SETUP_DATA_PATH "./AGENT/db"
    #define TEST_AGENT_INSTALL_PATH "./AGENT/"
#endif

#ifdef _WIN64
#define MA_AGENT_BIT_AFFINITY 2
#elif _WIN32
#define MA_AGENT_BIT_AFFINITY 1
#else
#define MA_AGENT_BIT_AFFINITY 0
#endif

struct ma_ut_setup_s
{
    ma_client_t                                     *client;
    ma_logger_t                                     *logger;
    char                                            log_file_name[MA_MAX_LEN];
    ma_configurator_t                               *configurator;
    ma_policy_settings_bag_t                        *policy_settings_bag;
    ma_crypto_t										*crypto;
    ma_net_client_service_t							*net_service;
    ma_context_t                                    *context;
	ma_msgbus_t										*msgbus;
    ma_ut_policy_table_provider_callback_t          table_provider_cb;
    void                                            *table_provider_cb_data;
    ma_ut_policy_variant_provider_callback_t        variant_provider_cb;
    void                                            *variant_provider_cb_data;	
	ma_ut_policy_buffer_provider_callback_t         buffer_provider_cb;
    void                                            *buffer_provider_cb_data;
};

static ma_crypto_verify_key_type_t spipe_verify_key;

static ma_error_t ma_ut_configurator_create(ma_configurator_t **configurator);
static ma_error_t ma_ut_agent_policy_settings_bag_create(ma_ut_setup_t *ut_setup, ma_policy_settings_bag_t **policy_settings_bag);
static void do_registry_setup();
static void do_registry_cleanup();

ma_error_t ma_ut_setup_create(const char *ut_name, const char *product_id, ma_ut_setup_t **setup) {
    if(product_id && setup) {
        ma_error_t rc = MA_OK;
        ma_ut_setup_t *self = (ma_ut_setup_t *)calloc(1,sizeof(ma_ut_setup_t));
        if(!self)       return MA_ERROR_OUTOFMEMORY;

        MA_MSC_SELECT(_snprintf, snprintf)(self->log_file_name, MA_MAX_LEN,"%s.log", ut_name);
        unlink(self->log_file_name);
		do_registry_setup();
        if(MA_OK == (rc = ma_file_logger_create(NULL, ut_name, NULL, (ma_file_logger_t **)&self->logger))) {
            if(MA_OK == (rc = ma_ut_configurator_create(&self->configurator))) {
                if(MA_OK == (rc = ma_ut_agent_policy_settings_bag_create(self, &self->policy_settings_bag))) {
                    if(MA_OK == (rc = ma_client_create(product_id, &self->client))) {
						// FAIL -> Memory Leak
						if(1 == create_macrypto_object_with_keys(MA_CRYPTO_MODE_NON_FIPS, MA_CRYPTO_ROLE_OFFICER, MA_CRYPTO_AGENT_MANAGED, &self->crypto)) {
							if(MA_OK == (rc = ma_msgbus_create(product_id, &self->msgbus))) {
								ma_client_set_logger(self->client, self->logger);
								ma_client_set_thread_option(self->client, MA_MSGBUS_CALLBACK_THREAD_IO);
								ma_client_set_consumer_reachability(self->client, MSGBUS_CONSUMER_REACH_INPROC);
								// FAIL -> Memory Leak
								if(MA_OK == (rc = ma_net_client_service_create(ma_msgbus_get_event_loop(self->msgbus), &self->net_service))) {
									if(MA_OK == (rc = ma_context_create(&self->context))){
											ma_context_add_object_info(self->context, MA_OBJECT_POLICY_SETTINGS_BAG_STR, &self->policy_settings_bag);
											ma_context_add_object_info(self->context, MA_OBJECT_MA_CONFIGURATOR_NAME_STR, &self->configurator);
											ma_context_add_object_info(self->context, MA_OBJECT_LOGGER_NAME_STR, &self->logger);
											ma_context_add_object_info(self->context, MA_OBJECT_NET_CLIENT_NAME_STR, &self->net_service);
											ma_context_add_object_info(self->context, MA_OBJECT_CRYPTO_NAME_STR, &self->crypto);
											ma_context_add_object_info(self->context, MA_OBJECT_MSGBUS_NAME_STR, &self->msgbus);
											
											*setup = self;
											return MA_OK;
									}
									if(self->net_service) ma_net_client_service_release(self->net_service);
								}
								if(self->msgbus)ma_msgbus_release(self->msgbus);
							}
							release_macrypto_object(MA_CRYPTO_MODE_NON_FIPS, MA_CRYPTO_ROLE_OFFICER, MA_CRYPTO_AGENT_MANAGED, self->crypto);
					   }
                       ma_client_release(self->client);
                    }
                   ma_policy_settings_bag_release(self->policy_settings_bag);
                }
                ma_configurator_release(self->configurator);
            }
            ma_logger_release(self->logger);
            unlink(self->log_file_name);
        }
        free(self);
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}

void ma_ut_setup_release(ma_ut_setup_t *self) {
    if(self) {
        ma_client_stop(self->client);
        ma_client_release(self->client);
        ma_policy_settings_bag_release(self->policy_settings_bag);
        ma_configurator_release(self->configurator);
        ma_logger_release(self->logger);
        unlink(self->log_file_name);
		release_macrypto_object(MA_CRYPTO_MODE_NON_FIPS, MA_CRYPTO_ROLE_OFFICER, MA_CRYPTO_AGENT_MANAGED, self->crypto);
		ma_net_client_service_release(self->net_service);
		ma_msgbus_release(self->msgbus);
		ma_context_release(self->context);
        free(self);
		self = NULL;
    }
	do_registry_cleanup();
}

ma_context_t *ma_ut_setup_get_context(ma_ut_setup_t *self) {
    return self->context;
}

ma_msgbus_t *ma_ut_setup_get_msgbus(ma_ut_setup_t *self) {
	return self->msgbus;
}

ma_client_t *ma_ut_setup_get_client(ma_ut_setup_t *self) {
    return self->client;
}

ma_logger_t *ma_ut_setup_get_logger(ma_ut_setup_t *self) {
    return self->logger;
}


ma_ds_t *ma_ut_setup_get_datastore(ma_ut_setup_t *self) {
    return ma_configurator_get_datastore(self->configurator);
}

ma_db_t *ma_ut_setup_get_database(ma_ut_setup_t *self) {
    return ma_configurator_get_database(self->configurator);
}

void ma_ut_setup_set_policy_buffer_provider_callback(ma_ut_setup_t *self, ma_ut_policy_buffer_provider_callback_t cb, void *cb_data) {
    if(self) {
        self->buffer_provider_cb = cb;
        self->buffer_provider_cb_data = cb_data;
    }
}

void ma_ut_setup_set_policy_variant_provider_callback(ma_ut_setup_t *self, ma_ut_policy_variant_provider_callback_t cb, void *cb_data) {
    if(self) {
        self->variant_provider_cb = cb;
        self->variant_provider_cb_data = cb_data;
    }
}

void ma_ut_setup_set_policy_table_provider_callback(ma_ut_setup_t *self, ma_ut_policy_table_provider_callback_t cb, void *cb_data) {
    if(self) {
        self->table_provider_cb = cb;
        self->table_provider_cb_data = cb_data;
    }
}




/* configurator setup */

typedef struct ma_ut_configurator_s {
    ma_configurator_t                   configurator;
    ma_db_t                             *db_default;
    ma_db_t                             *db_policy;
    ma_db_t                             *db_task;
    ma_db_t                             *db_scheduler;
    ma_db_t                             *db_msgbus;
    ma_ds_t                             *ds_default;
    ma_ds_t                             *ds_scheduler;
}ma_ut_configurator_t;

static ma_error_t ut_configurator_get_ds_info(ma_configurator_t *configurator, int ds_type, struct ma_datastore_info_s *ds_info);
static ma_error_t ut_configurator_populate_agent_configuration(ma_configurator_t *configurator, struct ma_configuration_s *configuration);
static void ut_configurator_destroy(ma_configurator_t *configurator);

static ma_configurator_methods_t methods = {
    &ut_configurator_get_ds_info,
    &ut_configurator_populate_agent_configuration,
    &ut_configurator_destroy
};

static ma_error_t ma_ut_configurator_create(ma_configurator_t **configurator)
{
    if(configurator) {
        ma_error_t rc = MA_OK;
        ma_ut_configurator_t *self = NULL;

        self = (ma_ut_configurator_t *)calloc(1, sizeof(ma_ut_configurator_t));
        if(!self)   return MA_ERROR_OUTOFMEMORY;


        /* clean up from old one if any */
        unlink(MA_UT_DB_POLICY_FILENAME_STR);
        unlink(MA_UT_DB_TASK_FILENAME_STR);
        unlink(MA_UT_DB_COMMON_SVC_FILENAME_STR);
        unlink(MA_UT_DB_SCHEDULER_FILENAME_STR);
        unlink(MA_UT_DB_DEFAULT_FILENAME_STR);

        ma_configurator_init((ma_configurator_t*)self, &methods, self);
        *configurator = (ma_configurator_t *)self;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}


static ma_error_t ut_configurator_get_ds_info(ma_configurator_t *configurator, int ds_type, struct ma_datastore_info_s *ds_info) {
    if(configurator && ds_info) {
        ma_error_t rc = MA_OK;
        ma_ut_configurator_t *self = (ma_ut_configurator_t *)configurator;
        switch(ds_type) {
            case MA_DS_INFO_DEFAULT: {
				if(!self->ds_default)
				{
					if(self->ds_default) ma_ds_release(self->ds_default); self->ds_default = NULL;
					if(self->db_default) ma_db_close(self->db_default); self->db_default = NULL;
					if(MA_OK == (rc = ma_db_open(MA_UT_DB_DEFAULT_FILENAME_STR, MA_DB_OPEN_READWRITE, &self->db_default))) {
						if(MA_OK == (rc = ma_ds_database_create(self->db_default, MA_DS_DEFAULT_INSTANCE_NAME, (ma_ds_database_t **)&self->ds_default))){
							ma_repository_db_create_schema(self->db_default);
							ma_proxy_db_create_schema(self->db_default);
							ds_info->ds = self->ds_default;
							ds_info->db = self->db_default;
							return MA_OK;
						}
						ma_db_close(self->db_default);
					}
                }
				else
				{
					ds_info->ds = self->ds_default;
                    ds_info->db = self->db_default;
				}
            }
                break;
            case MA_DS_INFO_POLICY: {
                if(self->db_policy) ma_db_close(self->db_policy); self->db_policy = NULL;
                if(MA_OK == (rc = ma_db_open(MA_UT_DB_POLICY_FILENAME_STR, MA_DB_OPEN_READWRITE, &self->db_policy))) {
                    ds_info->ds = NULL;
                    ds_info->db = self->db_policy;
                    return MA_OK;
                }
            }
                break;
            case MA_DS_INFO_TASK: {
                if(self->db_task) ma_db_close(self->db_task); self->db_task = NULL;
                if(MA_OK == (rc = ma_db_open(MA_UT_DB_TASK_FILENAME_STR, MA_DB_OPEN_READWRITE, &self->db_task))) {
                    ds_info->ds = NULL;
                    ds_info->db = self->db_task;
                    return MA_OK;
                }
            }
                break;
            case MA_DS_INFO_SCHEDULER: {
                if(self->ds_scheduler) ma_ds_release(self->ds_scheduler); self->ds_scheduler = NULL;
                if(self->db_scheduler) ma_db_close(self->db_scheduler); self->db_scheduler = NULL;
                if(MA_OK == (rc = ma_db_open(MA_UT_DB_SCHEDULER_FILENAME_STR,MA_DB_OPEN_READWRITE, &self->db_scheduler))) {
                    if(MA_OK == (rc = ma_ds_database_create(self->db_default, MA_DS_DEFAULT_INSTANCE_NAME, (ma_ds_database_t **)&self->db_scheduler))){
                        ds_info->ds = self->ds_scheduler;
                        ds_info->db = NULL;
                        return MA_OK;
                    }
                    ma_db_close(self->db_scheduler);
                }
            }
                break;
            case MA_DS_INFO_MSGBUS: {
                if(self->db_msgbus) ma_db_close(self->db_msgbus); self->db_msgbus = NULL;
                if(MA_OK == (rc = ma_db_open(MA_UT_DB_COMMON_SVC_FILENAME_STR,MA_DB_OPEN_READWRITE, &self->db_msgbus))) {
                    ds_info->ds = NULL;
                    ds_info->db = self->db_msgbus;
                    return MA_OK;
                }
            }
            default:
                return MA_ERROR_INVALID_OPTION;
        }
        return rc;
    }
    return MA_ERROR_INVALIDARG;
}


static ma_error_t ut_configurator_populate_agent_configuration(ma_configurator_t *configurator, struct ma_configuration_s *configuration) {
    if(configurator && configuration) {
        configuration->agent_data_path = ".\\AGENT\\";
        configuration->agent_install_path = ".\\AGENT\\";
        configuration->agent_lib_path = ".\\AGENT\\";
        configuration->agent_tools_lib_path = ".\\AGENT\\";
		configuration->agent_configuration = (ma_agent_configuration_t*)calloc(1, sizeof(ma_agent_configuration_t));
        configuration->agent_configuration->agent_mode = MA_AGENT_MODE_MANAGED_INT;
		configuration->agent_configuration->agent_crypto_mode = MA_CRYPTO_MODE_NON_FIPS;
        configuration->agent_configuration->agent_crypto_role = MA_CRYPTO_ROLE_USER;
	    configuration->agent_configuration->vdi_mode = 0;
        memset(configuration->agent_configuration->agent_guid , 1, UUID_LEN_TXT - 1);
        configuration->agent_configuration->agent_guid[UUID_LEN_TXT - 1] = '\0';
        memset(configuration->agent_configuration->tenant_id , 1, UUID_LEN_TXT - 1);
        configuration->agent_configuration->tenant_id[UUID_LEN_TXT - 1] = '\0';
        configuration->agent_configuration->mode_changed = MA_FALSE;

        return MA_OK;
    }
    return MA_ERROR_INVALIDARG;
}

static void ut_configurator_destroy(ma_configurator_t *configurator) {
    if(configurator) {
        ma_ut_configurator_t *self = (ma_ut_configurator_t *)configurator;

        if(self->ds_scheduler){
            ma_ds_release(self->ds_scheduler); self->ds_scheduler = NULL;
        }

        if(self->db_scheduler) {
                ma_db_close(self->db_scheduler); self->db_scheduler = NULL;
                unlink(MA_DB_SCHEDULER_FILENAME_STR);
        }

        if(self->db_policy) {
            ma_db_close(self->db_policy); self->db_policy = NULL;
            unlink(MA_DB_POLICY_FILENAME_STR);
        }

        if(self->db_task) {
            ma_db_close(self->db_task); self->db_task = NULL;
            unlink(MA_DB_TASK_FILENAME_STR);
        }

        if(self->db_msgbus) {
            ma_db_close(self->db_msgbus);
            unlink(MA_UT_DB_COMMON_SVC_FILENAME_STR);
        }

        if(self->ds_default) {
            ma_ds_release(self->ds_default); self->ds_default = NULL;
        }
        if(self->db_default){
            ma_db_close(self->db_default); self->db_default = NULL;
            unlink(MA_DB_DEFAULT_FILENAME_STR);
        }

        unlink(MA_UT_DB_POLICY_FILENAME_STR);
        unlink(MA_UT_DB_TASK_FILENAME_STR);
        unlink(MA_UT_DB_COMMON_SVC_FILENAME_STR);
        unlink(MA_UT_DB_SCHEDULER_FILENAME_STR);
        unlink(MA_UT_DB_DEFAULT_FILENAME_STR);

        free(self);
		self = NULL;
    }
}


/*policy settings bag*/


typedef struct ma_ut_agent_policy_settings_bag_s {
    ma_policy_settings_bag_t    base;
    ma_ut_setup_t               *ut_setup;

}ma_ut_agent_policy_settings_bag_t;

static ma_error_t get_bool(ma_policy_settings_bag_t *bag, const char *section, const char *key, ma_bool_t *value);
static ma_error_t get_int(ma_policy_settings_bag_t *bag, const char *section, const char *key, ma_int32_t *value);
static ma_error_t get_str(ma_policy_settings_bag_t *bag, const char *section, const char *key, const char **value);
static ma_error_t get_buffer(ma_policy_settings_bag_t *bag, const char *section, const char *key, ma_buffer_t **value);
static ma_error_t get_variant(ma_policy_settings_bag_t *self, const char *section, const char *key, ma_variant_t **value);
static ma_error_t get_table(ma_policy_settings_bag_t *bag, const char *section, ma_table_t **value);
static void destroy(ma_policy_settings_bag_t *bag); /*< virtual destructor */


static const ma_policy_settings_bag_methods_t policy_methods = {
    &get_bool,
    &get_int,
    &get_str,
    &get_buffer,
	&get_variant,
	&destroy
};

static ma_error_t ma_ut_agent_policy_settings_bag_create(ma_ut_setup_t *ut_setup, ma_policy_settings_bag_t **bag) {
    if(bag ) {
        ma_ut_agent_policy_settings_bag_t *self = (ma_ut_agent_policy_settings_bag_t *)calloc(1, sizeof(ma_ut_agent_policy_settings_bag_t));
        if(!self) return MA_ERROR_OUTOFMEMORY;

        ma_policy_settings_bag_init((ma_policy_settings_bag_t *)self, &policy_methods, self);
        self->ut_setup = ut_setup;
        *bag = (ma_policy_settings_bag_t *)self;
        return MA_OK;
    }
    return MA_ERROR_INVALIDARG ;
}

static void destroy(ma_policy_settings_bag_t *bag) {
    if(bag) {
        ma_ut_agent_policy_settings_bag_t *self = (ma_ut_agent_policy_settings_bag_t *)bag;
        free(self);
    }
}

static ma_error_t get_table(ma_policy_settings_bag_t *bag, const char *section, ma_table_t **value) {
    if(bag) {
        ma_ut_agent_policy_settings_bag_t *self = (ma_ut_agent_policy_settings_bag_t *)bag;
        if(self->ut_setup->table_provider_cb)
            return self->ut_setup->table_provider_cb(section, value, self->ut_setup->table_provider_cb_data);
    }

    return MA_ERROR_INVALIDARG;
}

static ma_error_t get_variant(ma_policy_settings_bag_t *bag, const char *section, const char *key, ma_variant_t **value) {
    if(bag) {
        ma_ut_agent_policy_settings_bag_t *self = (ma_ut_agent_policy_settings_bag_t *)bag;
        if(self->ut_setup->variant_provider_cb)
			return self->ut_setup->variant_provider_cb(section, key, value, self->ut_setup->variant_provider_cb_data);
    }

	return MA_ERROR_INVALIDARG;
}

static ma_error_t get_buffer(ma_policy_settings_bag_t *bag, const char *section, const char *key, ma_buffer_t **value) {
    if(bag) {
        ma_ut_agent_policy_settings_bag_t *self = (ma_ut_agent_policy_settings_bag_t *)bag;
        if(self->ut_setup->buffer_provider_cb)
			return self->ut_setup->buffer_provider_cb(section, key, value, self->ut_setup->buffer_provider_cb_data);
    }

	return MA_ERROR_INVALIDARG;
}

static ma_error_t get_str(ma_policy_settings_bag_t *bag, const char *section, const char *key, const char **value ) {
    ma_variant_t *var_value = NULL;
    ma_error_t rc = MA_OK;
    ma_buffer_t *dummy_buffer = NULL;
    if((MA_OK == (rc = get_buffer(bag, section, key, &dummy_buffer)) && dummy_buffer)) {
        char *new_value = NULL;
        size_t size = 0;
        if((MA_OK == (rc = ma_buffer_get_string(dummy_buffer, &new_value, &size))) && new_value)
            *value = strdup(new_value);
        (void)ma_buffer_release(dummy_buffer);
    }
    return rc;
}

static ma_error_t get_int(ma_policy_settings_bag_t *bag, const char *section, const char *key, ma_int32_t *value) {
    ma_variant_t *var_value = NULL;
    ma_error_t rc = MA_OK;
    ma_buffer_t *buffer = NULL;
    if(MA_OK == (rc = get_buffer(bag, section, key, &buffer))) {
        char *new_value = NULL;
        size_t size = 0;
        if((MA_OK == (rc = ma_buffer_get_string(buffer, &new_value, &size))) && new_value)
            *value = atoi(new_value);
        (void) ma_buffer_release(buffer);
    }
	return rc;
}

static ma_error_t get_bool(ma_policy_settings_bag_t *bag, const char *section, const char *key, ma_bool_t *value)  {
    ma_variant_t *var_value = NULL;
    ma_error_t rc = MA_OK;
    ma_int32_t dummy_value = 0;

    if(MA_OK == (rc = get_int(bag, section, key, &dummy_value)))
        *value = dummy_value ? MA_TRUE :MA_FALSE;

    return rc;
}

static ma_crypto_verify_key_type_t MA_SPIPE_VERIFY_KEY()
{
	return spipe_verify_key;
}

ma_bool_t isSystem64Bit() {
#ifdef MA_WINDOWS

    UINT size = 0;
    HMODULE hm_k32 = (HMODULE) 0;
    FARPROC proc;
    TCHAR tmp[1];
    OSVERSIONINFOW g_osvi = {0};
    ma_bool_t ret = MA_FALSE;

    g_osvi.dwOSVersionInfoSize = sizeof( OSVERSIONINFO );
    GetVersionEx(&g_osvi);
    // GetSystemWow64Directory() requires at least Win XP or Win 2003 Server
    // and 64-bit is potentially only on at least XP.
    if (g_osvi.dwMajorVersion >= 4 || (g_osvi.dwMajorVersion >= 3 && g_osvi.dwMinorVersion >= 1))
    {
        // Since delay loading Kernel32.dll is not supported, we need to
        // check for the presence of GetSystemWow64Directory manually.
        hm_k32 = LoadLibrary(L"Kernel32.dll");
        if (hm_k32)
        {
            proc = GetProcAddress(hm_k32, "GetSystemWow64DirectoryW");
            if (proc)
            {
                // GetSystemWow64Directory always fails on 32 bit operating systems.
                // It returns 0 on failure or the length, so we must give it a chance
                // to copy at least 1 character into the buffer so that we can dif-
                // ferentiate between an error code and a size of 0.
                size = ((UINT (__stdcall *)(LPTSTR, UINT)) proc)(tmp, 1);
                if (size > 0)
                    ret = MA_TRUE;
            }
            FreeLibrary(hm_k32);
        }
    }
    return ret;
#endif
    return MA_FALSE;
}

static void add_agent_configuration(const char *scan_path1) {
    char command[1024] = {0};
    ma_xml_t *config_xml = NULL;
    ma_xml_node_t *config_root = NULL;

    ma_temp_buffer_t buffer;
    ma_temp_buffer_init(&buffer);
#ifdef MA_WINDOWS
    form_path(&buffer, TEST_AGENT_ROOT, "EPOAGENT3000", MA_FALSE);
#else
    form_path(&buffer, MA_REGISTRY_STORE_SCAN_PATH, "EPOAGENT3000", MA_FALSE);
#endif
    MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s %s", "mkdir" , (char*)ma_temp_buffer_get(&buffer));
    system(command);

    if(MA_OK == ma_xml_create(&config_xml)) {
        if(MA_OK == ma_xml_construct(config_xml)) {
            if(NULL != (config_root = ma_xml_get_node(config_xml))) {
                ma_xml_node_t *node = NULL;
                if(MA_OK == ma_xml_node_create(config_root, MA_REGISTRY_STORE_XML_PATH, &node)) {
                    ma_xml_node_t *config_node  = NULL;
                    ma_error_t error = MA_ERROR_APIFAILED;
                    if(MA_OK == (error = ma_xml_node_create(node, MA_APPLICATION_INFO_DATA_DIR, &config_node))) {
                        ma_xml_node_set_data(config_node, TEST_AGENT_DATA_PATH);
                    }
                    if(MA_OK == error  && MA_OK == (error = ma_xml_node_create(node, MA_APPLICATION_INFO_INSTALL_DIR, &config_node))) {
                        ma_xml_node_set_data(config_node, TEST_AGENT_INSTALL_PATH);
                    }
                    if(MA_OK == error  && MA_OK == (error = ma_xml_node_create(node, MA_APPLICATION_INTEGRATION_INFO_SOFTWARE_ID, &config_node))) {
                        ma_xml_node_set_data(config_node, "EPOAGENT3000");
                    }
                    if(MA_OK == error  && MA_OK == (error = ma_xml_node_create(node, MA_APPLICATION_INTEGRATION_INFO_DATACHANNEL_CONSUMER, &config_node))) {
                        ma_xml_node_set_data(config_node, "1");
                    }
                    if(MA_OK == error  && MA_OK == (error = ma_xml_node_create(node, MA_APPLICATION_INTEGRATION_INFO_EVENT_GENERATOR, &config_node))) {
                        ma_xml_node_set_data(config_node, "1");
                    }
                    if(MA_OK == error  && MA_OK == (error = ma_xml_node_create(node, MA_APPLICATION_INTEGRATION_INFO_POLICY_CONSUMER, &config_node))) {
                        ma_xml_node_set_data(config_node, "1");
                    }
                    if(MA_OK == error  && MA_OK == (error = ma_xml_node_create(node, MA_APPLICATION_INTEGRATION_INFO_PROPERTY_PROVIDER, &config_node))) {
                        ma_xml_node_set_data(config_node, "1");
                    }
                    if(MA_OK == error  && MA_OK == (error = ma_xml_node_create(node, MA_APPLICATION_INTEGRATION_INFO_SCHEDULER_CONSUMER, &config_node))) {
                        ma_xml_node_set_data(config_node, "1");
                    }
                    if(MA_OK == error  && MA_OK == (error = ma_xml_node_create(node, MA_APPLICATION_INTEGRATION_INFO_UPDATER_CONSUMER, &config_node))) {
                        ma_xml_node_set_data(config_node, "1");
                    }
                    if(MA_OK == error ){
                        form_path(&buffer, (char *)ma_temp_buffer_get(&buffer), "config.xml",MA_FALSE);
                        ma_xml_save_to_file(config_xml, (char *)ma_temp_buffer_get(&buffer));
                    }
                }
            }
        }
        ma_xml_release(config_xml);
    }

    ma_temp_buffer_uninit(&buffer);
}

static void add_software_to_configuration(const char *scan_path1, const char *software_id, ma_int32_t registered_services){
    char command[1024] = {0};
    ma_xml_t *config_xml = NULL;
    ma_xml_node_t *config_root = NULL;

    ma_temp_buffer_t buffer;
    ma_temp_buffer_init(&buffer);
    form_path(&buffer, MA_REGISTRY_STORE_SCAN_PATH, software_id, MA_FALSE);
#ifdef MA_WINDOWS
    form_path(&buffer, TEST_AGENT_ROOT, software_id, MA_FALSE);
#else
    form_path(&buffer, MA_REGISTRY_STORE_SCAN_PATH, software_id, MA_FALSE);
#endif
    MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s %s", "mkdir" , (char*)ma_temp_buffer_get(&buffer));
    system(command);

    if(MA_OK == ma_xml_create(&config_xml)) {
        if(MA_OK == ma_xml_construct(config_xml)) {
            if(NULL != (config_root = ma_xml_get_node(config_xml))) {
                ma_xml_node_t *node = NULL;
                if(MA_OK == ma_xml_node_create(config_root, MA_REGISTRY_STORE_XML_PATH, &node)) {
                    ma_xml_node_t *config_node  = NULL;
                    ma_error_t error = MA_ERROR_APIFAILED;

                    if(MA_OK == (error = ma_xml_node_create(node, MA_APPLICATION_INTEGRATION_INFO_SOFTWARE_ID, &config_node))) {
                        ma_xml_node_set_data(config_node, software_id);
                    }
                    if(MA_OK == error  && (MA_REGISTERED_DATACHANNEL_SERVICE == (registered_services & MA_REGISTERED_DATACHANNEL_SERVICE)) && MA_OK == (error = ma_xml_node_create(node, MA_APPLICATION_INTEGRATION_INFO_DATACHANNEL_CONSUMER, &config_node))) {
                        ma_xml_node_set_data(config_node, "1");
                    }
                    if(MA_OK == error  && (MA_REGISTERED_EVENT_SERVICE == (registered_services & MA_REGISTERED_EVENT_SERVICE)) && MA_OK == (error = ma_xml_node_create(node, MA_APPLICATION_INTEGRATION_INFO_EVENT_GENERATOR, &config_node))) {
                        ma_xml_node_set_data(config_node, "1");
                    }
                    if(MA_OK == error  && (MA_REGISTERED_POLICY_SERVICE == (registered_services & MA_REGISTERED_POLICY_SERVICE)) && MA_OK == (error = ma_xml_node_create(node, MA_APPLICATION_INTEGRATION_INFO_POLICY_CONSUMER, &config_node))) {
                        ma_xml_node_set_data(config_node, "1");
                    }
                    if(MA_OK == error && (MA_REGISTERED_PROPERTY_SERVICE == (registered_services & MA_REGISTERED_PROPERTY_SERVICE))   && MA_OK == (error = ma_xml_node_create(node, MA_APPLICATION_INTEGRATION_INFO_PROPERTY_PROVIDER, &config_node))) {
                        ma_xml_node_set_data(config_node, "1");
                    }
                    if(MA_OK == error && (MA_REGISTERED_SCHEDULER_SERVICE == (registered_services & MA_REGISTERED_SCHEDULER_SERVICE))  && MA_OK == (error = ma_xml_node_create(node, MA_APPLICATION_INTEGRATION_INFO_SCHEDULER_CONSUMER, &config_node))) {
                        ma_xml_node_set_data(config_node, "1");
                    }
                    if(MA_OK == error && (MA_REGISTERED_UPDATER_SERVICE == (registered_services & MA_REGISTERED_UPDATER_SERVICE))  && MA_OK == (error = ma_xml_node_create(node, MA_APPLICATION_INTEGRATION_INFO_UPDATER_CONSUMER, &config_node))) {
                        ma_xml_node_set_data(config_node, "1");
                    }
                    if(MA_OK == error ){
                        form_path(&buffer, (char *)ma_temp_buffer_get(&buffer), "config.xml",MA_FALSE);
                        ma_xml_save_to_file(config_xml, (char *)ma_temp_buffer_get(&buffer));
                    }
                }
            }
        }
        ma_xml_release(config_xml);
    }

    ma_temp_buffer_uninit(&buffer);
}


static void fill_agent_information(ma_ds_t *ds) {
    ma_ds_set_str(ds, MA_REGISTRY_STORE_AGENT_SCAN_PATH, MA_APPLICATION_INFO_DATA_DIR, TEST_AGENT_DATA_PATH, strlen(TEST_AGENT_DATA_PATH));
    ma_ds_set_str(ds, MA_REGISTRY_STORE_AGENT_SCAN_PATH, MA_APPLICATION_INFO_INSTALL_DIR, TEST_AGENT_INSTALL_PATH, strlen(TEST_AGENT_INSTALL_PATH));
}



static void create_application_list_in_x64_view(ma_ds_t *ds) {
    /*Created First APP*/
    ma_temp_buffer_t buffer;
    ma_temp_buffer_init(&buffer);

    form_path(&buffer, MA_REGISTRY_STORE_AGENT_SCAN_PATH, "Applications", MA_TRUE);
    form_path(&buffer, (char *) ma_temp_buffer_get(&buffer), "xApplication_1", MA_TRUE);
    ma_ds_set_str(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SOFTWARE_ID, "xApplication_1", strlen("xApplication_1"));
    ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_DATACHANNEL_CONSUMER, 1);
    ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_EVENT_GENERATOR, 1);

    form_path(&buffer, MA_REGISTRY_STORE_AGENT_SCAN_PATH, "Applications", MA_TRUE);
    form_path(&buffer, (char *) ma_temp_buffer_get(&buffer), "xApplication_2", MA_TRUE);
    ma_ds_set_str(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SOFTWARE_ID, "xApplication_2", strlen("xApplication_2"));
    ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_POLICY_CONSUMER, 1);
    ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_PROPERTY_PROVIDER, 1);

    form_path(&buffer, MA_REGISTRY_STORE_AGENT_SCAN_PATH, "Applications", MA_TRUE);
    form_path(&buffer, (char *) ma_temp_buffer_get(&buffer), "xApplication_3", MA_TRUE);
    ma_ds_set_str(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SOFTWARE_ID, "xApplication_3", strlen("xApplication_3"));
    ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SCHEDULER_CONSUMER, 1);
    ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_UPDATER_CONSUMER, 1);

    form_path(&buffer, MA_REGISTRY_STORE_AGENT_SCAN_PATH, "Applications", MA_TRUE);
    form_path(&buffer, (char *) ma_temp_buffer_get(&buffer), "xApplication_4", MA_TRUE);
    ma_ds_set_str(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SOFTWARE_ID, "xApplication_4", strlen("xApplication_4"));
    ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_DATACHANNEL_CONSUMER, 1);
    ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_EVENT_GENERATOR, 1);
    ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_POLICY_CONSUMER, 1);
    ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_PROPERTY_PROVIDER, 1);
    ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SCHEDULER_CONSUMER, 1);
    ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_UPDATER_CONSUMER, 1);

    ma_temp_buffer_uninit(&buffer);
}

static void create_application_list_in_x86_view(ma_ds_t *ds) {
    /*Created First APP*/
    ma_temp_buffer_t buffer;
    ma_temp_buffer_init(&buffer);

    form_path(&buffer, MA_REGISTRY_STORE_AGENT_SCAN_PATH, "Applications", MA_TRUE);
    form_path(&buffer, (char *) ma_temp_buffer_get(&buffer), "Application_1", MA_TRUE);
    ma_ds_set_str(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SOFTWARE_ID, "Application_1", strlen("Application_1"));
    ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_DATACHANNEL_CONSUMER, 1);
    ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_EVENT_GENERATOR, 1);

    form_path(&buffer, MA_REGISTRY_STORE_AGENT_SCAN_PATH, "Applications", MA_TRUE);
    form_path(&buffer, (char *) ma_temp_buffer_get(&buffer), "Application_2", MA_TRUE);
    ma_ds_set_str(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SOFTWARE_ID, "Application_2", strlen("Application_2"));
    ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_POLICY_CONSUMER, 1);
    ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_PROPERTY_PROVIDER, 1);

    form_path(&buffer, MA_REGISTRY_STORE_AGENT_SCAN_PATH, "Applications", MA_TRUE);
    form_path(&buffer, (char *) ma_temp_buffer_get(&buffer), "Application_3", MA_TRUE);
    ma_ds_set_str(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SOFTWARE_ID, "Application_3", strlen("Application_3"));
    ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SCHEDULER_CONSUMER, 1);
    ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_UPDATER_CONSUMER, 1);

    form_path(&buffer, MA_REGISTRY_STORE_AGENT_SCAN_PATH, "Applications", MA_TRUE);
    form_path(&buffer, (char *) ma_temp_buffer_get(&buffer), "Application_4", MA_TRUE);
    ma_ds_set_str(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SOFTWARE_ID, "Application_4", strlen("Application_4"));
    ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_DATACHANNEL_CONSUMER, 1);
    ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_EVENT_GENERATOR, 1);
    ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_POLICY_CONSUMER, 1);
    ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_PROPERTY_PROVIDER, 1);
    ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SCHEDULER_CONSUMER, 1);
    ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_UPDATER_CONSUMER, 1);

    ma_temp_buffer_uninit(&buffer);
}


static void populate_applications(ma_ds_t *ds, ma_bool_t is_64_bit_os, ma_bool_t default_view){
    /*
     * First create the Agent information DATAPATH and INSTALLPATH
     * In x86 Os, create directly
     * In x64 Os, create in default view if Agent Affinity is x64
     * In x64 OS, create in x86 view if Agent Affinity is x86
     */
    if(default_view) {
        fill_agent_information(ds);
    }
    switch(MA_AGENT_BIT_AFFINITY) {
        case 1: // x86
            {
                if(is_64_bit_os && !default_view) {
                    create_application_list_in_x64_view(ds);
                }
                else if(is_64_bit_os && default_view) {
                    create_application_list_in_x86_view(ds);
                }
            }
            break;
        case 2: //x64
            {
                if(is_64_bit_os && default_view) {
                    create_application_list_in_x64_view(ds);
                }
                else if(is_64_bit_os && !default_view) {
                    create_application_list_in_x86_view(ds);
                }
            }
            break;
        default:
            break;
    }
}


static void do_registry_setup() {
    char command[1024];
#ifdef MA_WINDOWS
    ma_ds_registry_t *g_registry_default = NULL;
    #ifdef MA_WINDOWS
        MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s %s", "rmdir /S /Q" , TEST_AGENT_ROOT);
    #else
        MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s %s", "rm -rf " , TEST_AGENT_ROOT);
    #endif
    system(command);
    if(MA_OK == ma_ds_registry_open(HKEY_LOCAL_MACHINE,MA_REGISTRY_STORE_SCAN_PATH, MA_TRUE, MA_TRUE, KEY_ALL_ACCESS, &g_registry_default)) {
        populate_applications((ma_ds_t *) g_registry_default, isSystem64Bit(), MA_TRUE);
        ma_ds_registry_release(g_registry_default);
    }
    if(isSystem64Bit() == MA_TRUE) {
        if(MA_OK == ma_ds_registry_open(HKEY_LOCAL_MACHINE,MA_REGISTRY_STORE_SCAN_PATH, MA_FALSE, MA_TRUE, KEY_ALL_ACCESS, &g_registry_default)) {
            populate_applications((ma_ds_t *) g_registry_default, isSystem64Bit(), MA_FALSE);
            ma_ds_registry_release(g_registry_default);
        }
    }
#endif

    MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s %s", "mkdir" , TEST_AGENT_ROOT);
    system(command);
    MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s %s", "mkdir" , TEST_AGENT_SETUP_DATA_PATH);
    system(command);
    MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s %s", "mkdir" , TEST_AGENT_INSTALL_PATH);
    system(command);

#ifdef MA_WINDOWS
    MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s", "CD");
#else
    MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s", "pwd");
#endif
    system(command);

#ifndef MA_WINDOWS
    add_agent_configuration(MA_REGISTRY_STORE_SCAN_PATH);
    add_software_to_configuration(MA_REGISTRY_STORE_SCAN_PATH, "Application_1", MA_REGISTERED_DATACHANNEL_SERVICE | MA_REGISTERED_EVENT_SERVICE);
    add_software_to_configuration(MA_REGISTRY_STORE_SCAN_PATH, "Application_2", MA_REGISTERED_POLICY_SERVICE | MA_REGISTERED_PROPERTY_SERVICE);
    add_software_to_configuration(MA_REGISTRY_STORE_SCAN_PATH, "Application_3", MA_REGISTERED_SCHEDULER_SERVICE| MA_REGISTERED_UPDATER_SERVICE);
    add_software_to_configuration(MA_REGISTRY_STORE_SCAN_PATH, "Application_4", MA_REGISTERED_ALL);
    add_software_to_configuration(MA_REGISTRY_STORE_SCAN_PATH, "xApplication_1", MA_REGISTERED_DATACHANNEL_SERVICE | MA_REGISTERED_EVENT_SERVICE);
    add_software_to_configuration(MA_REGISTRY_STORE_SCAN_PATH, "xApplication_2", MA_REGISTERED_POLICY_SERVICE | MA_REGISTERED_PROPERTY_SERVICE);
    add_software_to_configuration(MA_REGISTRY_STORE_SCAN_PATH, "xApplication_3", MA_REGISTERED_SCHEDULER_SERVICE| MA_REGISTERED_UPDATER_SERVICE);
    add_software_to_configuration(MA_REGISTRY_STORE_SCAN_PATH, "xApplication_4", MA_REGISTERED_ALL);
#endif
}

static void do_registry_cleanup() {
    char command[1024]={0};
#ifdef MA_WINDOWS
    ma_ds_registry_t *g_registry_default = NULL;
    if(MA_OK == ma_ds_registry_open(HKEY_LOCAL_MACHINE, MA_REGISTRY_STORE_SCAN_PATH, MA_TRUE, MA_TRUE, KEY_ALL_ACCESS, &g_registry_default)) {
        ma_ds_rem((ma_ds_t*)g_registry_default, "McAfee", NULL, MA_TRUE);
        ma_ds_registry_release(g_registry_default);
    }
    if(isSystem64Bit() == MA_TRUE) {
		if(MA_OK == ma_ds_registry_open(HKEY_LOCAL_MACHINE,MA_REGISTRY_STORE_SCAN_PATH, MA_FALSE, MA_TRUE, KEY_ALL_ACCESS, &g_registry_default)) {
            ma_ds_rem((ma_ds_t *)g_registry_default, "McAfee", NULL, MA_TRUE);
            ma_ds_registry_release(g_registry_default);
        }
    }
    MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s %s", "rmdir /S /Q" , TEST_AGENT_ROOT);
#else
    MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s %s", "rm -rf " , TEST_AGENT_ROOT);
#endif
    system(command);
}