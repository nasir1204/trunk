#include "unity.h"
#include "unity_fixture.h"

#include "ma_ah_client_service_test_helper.h"

#include "ma/internal/services/ahclient/ma_ah_site.h"
#include "ma/internal/services/ahclient/ma_ah_sitelist.h"
#include "ma_ah_site_url_provider.h"

#include "ma/internal/defs/ma_object_defs.h"
#include "ma/internal/utils/configurator/ma_configurator_utils.h"
#include "ma/internal/services/ma_agent_policy_settings_bag.h"
#include "ma/internal/utils/platform/ma_system_property.h"
#include "ma/internal/clients/ma/ma_client_internal.h"
#include "ma/logger/ma_file_logger.h"

#include "ma/repository/ma_repository.h"
#include "ma/internal/utils/repository/ma_repository_db_manager.h"

#include "ma_ut_setup.h"

#ifdef TEST_AGENT_GUID
#undef TEST_AGENT_GUID
#endif
#define TEST_AGENT_GUID "ekrfhn43hrlhfn34kh4ifn4krn43"

static ma_ut_setup_t *g_ut_setup = NULL;
static ma_datastore_configuration_request_t datastore_configuration_request = {{MA_TRUE,MA_DB_OPEN_READWRITE}, {MA_TRUE,MA_DB_OPEN_READWRITE}, {MA_TRUE,MA_DB_OPEN_READWRITE}, {MA_TRUE,MA_DB_OPEN_READWRITE}, {MA_TRUE,MA_DB_OPEN_READWRITE}};
static ma_agent_configuration_t agent_configuration = { 5, 10, 15, 0, 100, TEST_AGENT_GUID, "ewruhewoirlwenrwerwe", MA_FALSE };

struct repo_data
{
    const char *repo_name;
    ma_repository_type_t repo_type;
    ma_repository_url_type_t repo_url_type;
    ma_repository_namespace_t repo_namespace;
    ma_proxy_usage_type_t repo_proxy_usage_type;
    ma_repository_auth_t repo_auth;
    ma_bool_t repo_enabled;
    const char *repo_server_fqdn;
    const char *repo_server_name;
    const char *repo_server_ip;
    ma_uint32_t repo_port;
    const char *repo_server_path;
    const char *repo_domain_name;
    const char *repo_user_name;
    const char *repo_password;
    ma_uint32_t repo_pingtime;
    ma_uint32_t repo_subnetdistance;
    ma_uint32_t repo_siteorder;
    ma_repository_state_t repo_state;
};

static struct repo_data const repositories[] =
{
    // Name            Type                         URL Type                      Namespace                       Proxy Usage Type                       Authentication                    Enabled   Server FQDN        Server Name   Server IP        Port       Server Path                    Domain Name    Username              Password              Pingtime   Subnet Distance   Siteorder   State
    {  "Repository_1", MA_REPOSITORY_TYPE_MASTER,   MA_REPOSITORY_URL_TYPE_SPIPE, MA_REPOSITORY_NAMESPACE_LOCAL,  MA_PROXY_USAGE_TYPE_SERVER_CONFIGURED, MA_REPOSITORY_AUTH_CREDENTIALS,   MA_TRUE,  "Server_fqdn_1",   "Server_1",   "192.127.221.1", 32000,    "ftp:\\\\my_ftp_repository",   "My_Domain",   "Administrator",      "123j2h3,n32klh3k",    1000,      4,                3,          MA_REPOSITORY_STATE_ADDED   },
    {  "Repository_2", MA_REPOSITORY_TYPE_FALLBACK, MA_REPOSITORY_URL_TYPE_SPIPE, MA_REPOSITORY_NAMESPACE_GLOBAL, MA_PROXY_USAGE_TYPE_SYSTEM_CONFIGURED, MA_REPOSITORY_AUTH_NONE,          MA_FALSE, "Server_fqdn_1",   "Server_1",   "172.20.154.12", 80,       "http:\\\\www.repository.com", "My_Domain",   "Low Priority User",  "#$#$#%$%$FFR",        2000,      40,               30,         MA_REPOSITORY_STATE_REMOVED },
    {  "Repository_3", MA_REPOSITORY_TYPE_FALLBACK, MA_REPOSITORY_URL_TYPE_SPIPE, MA_REPOSITORY_NAMESPACE_GLOBAL, MA_PROXY_USAGE_TYPE_SYSTEM_CONFIGURED, MA_REPOSITORY_AUTH_ANONYMOUS,     MA_TRUE,  "Server_fqdn_2",   "Server_2",   "142.15.230.21", 8080,     "ftp:\\\\my_ftp_repository_1", "My_Domain",   "Administrator",      "SD#RED#RE@#@#@CCE",   3000,      100,              1,          MA_REPOSITORY_STATE_ALTERED },
    {  "Repository_4", MA_REPOSITORY_TYPE_REPO,     MA_REPOSITORY_URL_TYPE_SPIPE, MA_REPOSITORY_NAMESPACE_GLOBAL, MA_PROXY_USAGE_TYPE_SERVER_CONFIGURED, MA_REPOSITORY_AUTH_LOGGEDON_USER, MA_FALSE, "Server_fqdn_3",   "Server_3",   "178.32.220.20", 8440,     "ftp:\\\\my_ftp_repository",   "My_Domain",   "Normal User",        "@@DX#CEF$FEFEF$#R",   500,       0,                7,          MA_REPOSITORY_STATE_DEFAULT },
    {  "Repository_5", MA_REPOSITORY_TYPE_MASTER,   MA_REPOSITORY_URL_TYPE_SPIPE, MA_REPOSITORY_NAMESPACE_LOCAL,  MA_PROXY_USAGE_TYPE_SERVER_CONFIGURED, MA_REPOSITORY_AUTH_ANONYMOUS,     MA_TRUE,  "Server_fqdn_3",   "Server_3",   "192.16.200.10", 2000,     "ftp:\\\\my_ftp_repository",   "My_Domain",   "Least Access User",  "@@#@#@#@#@#@#@#@#@#", 900,       1000,             10,         MA_REPOSITORY_STATE_ALTERED },
};

ma_error_t add_repositories_to_db()
{
    ma_error_t rc = MA_ERROR_UNEXPECTED;
    ma_repository_t *repository = NULL;
    ma_uint16_t index = 0;

    ma_repository_db_remove_all_repositories(ma_configurator_get_database(MA_CONTEXT_GET_CONFIGURATOR(ma_ut_setup_get_context(g_ut_setup))));

    for(;index < sizeof(repositories)/sizeof(repositories[0]); index++)
    {
        if(MA_OK == (rc = ma_repository_create(&repository)))
        {
            if((MA_OK == (rc = ma_repository_set_name(repository, repositories[index].repo_name))) &&
                (MA_OK == (rc = ma_repository_set_type(repository, repositories[index].repo_type))) &&
                (MA_OK == (rc = ma_repository_set_url_type(repository, repositories[index].repo_url_type))) &&
                (MA_OK == (rc = ma_repository_set_namespace(repository, repositories[index].repo_namespace))) &&
                (MA_OK == (rc = ma_repository_set_proxy_usage_type(repository, repositories[index].repo_proxy_usage_type))) &&
                (MA_OK == (rc = ma_repository_set_authentication(repository, repositories[index].repo_auth))) &&
                (MA_OK == (rc = ma_repository_set_enabled(repository, repositories[index].repo_enabled))) &&
                (MA_OK == (rc = ma_repository_set_server_fqdn(repository, repositories[index].repo_server_fqdn))) &&
                (MA_OK == (rc = ma_repository_set_server_name(repository, repositories[index].repo_server_name))) &&
                (MA_OK == (rc = ma_repository_set_server_ip(repository, repositories[index].repo_server_ip))) &&
                (MA_OK == (rc = ma_repository_set_port(repository, repositories[index].repo_port))) &&
                (MA_OK == (rc = ma_repository_set_server_path(repository, repositories[index].repo_server_path))) &&
                (MA_OK == (rc = ma_repository_set_user_name(repository, repositories[index].repo_user_name))) &&
                (MA_OK == (rc = ma_repository_set_password(repository, repositories[index].repo_password))) &&
                (MA_OK == (rc = ma_repository_set_pingtime(repository, repositories[index].repo_pingtime))) &&
                (MA_OK == (rc = ma_repository_set_subnetdistance(repository, repositories[index].repo_subnetdistance))) &&
                (MA_OK == (rc = ma_repository_set_siteorder(repository, repositories[index].repo_siteorder))) &&
                (MA_OK == (rc = ma_repository_set_state(repository, repositories[index].repo_state))))
            {
                rc = ma_repository_db_add_repository(ma_configurator_get_database(MA_CONTEXT_GET_CONFIGURATOR(ma_ut_setup_get_context(g_ut_setup))), repository);
            }
        }

        if(repository)
        {
            ma_repository_release(repository);
        }
    }
    return rc;
}

extern ma_ut_setup_t *get_ut_setup();

TEST_GROUP_RUNNER(ah_client_site_url_provider_tests_group)
{
	g_ut_setup = get_ut_setup();
    
	do {RUN_TEST_CASE(ah_client_site_url_provider_tests, verify_create);} while (0);    
}

TEST_SETUP(ah_client_site_url_provider_tests)
{
    TEST_ASSERT(NULL != g_ut_setup);
    TEST_ASSERT(MA_OK == add_repositories_to_db());
}

TEST_TEAR_DOWN(ah_client_site_url_provider_tests)
{
}

extern ma_uint32_t ma_ah_site_get_order(ma_ah_site_t *self);
extern void ma_ah_site_url_provider_deinit(ma_ah_site_url_provider_t *self);

TEST(ah_client_site_url_provider_tests, verify_create)
{
    ma_uint16_t index = 0;
    ma_ah_site_url_provider_t *ah_site_url_provider;
    ma_ah_sitelist_t *site_list = NULL;
    const char *url;

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_site_url_provider_create(NULL, &ah_site_url_provider));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_site_url_provider_create(ma_ut_setup_get_context(g_ut_setup), NULL));
    TEST_ASSERT(MA_OK == ma_ah_site_url_provider_create(ma_ut_setup_get_context(g_ut_setup), &ah_site_url_provider));

    ma_ah_site_url_provider_load_sitelist(NULL);
    ma_ah_site_url_provider_load_sitelist(ah_site_url_provider);

    TEST_ASSERT(NULL == (site_list = ma_ah_site_url_provider_get_sitelist(NULL)));
    TEST_ASSERT(NULL != (site_list = ma_ah_site_url_provider_get_sitelist(ah_site_url_provider)));
    TEST_ASSERT((sizeof(repositories)/sizeof(repositories[0])) == ma_ah_sitelist_get_count(site_list));

    for(index = 0;index < ma_ah_sitelist_get_count(site_list); index++)
    {
        ma_ah_site_t *site;
        const char *name;
        ma_uint16_t temp_index = -1;

        site = ma_ah_sitelist_get_site(index, site_list);
        name = ma_ah_site_get_name(site);

        for(temp_index = 0; temp_index < (sizeof(repositories)/sizeof(repositories[0])); temp_index++)
        {
            if(0 == strcmp(repositories[temp_index].repo_name, name))
            {
                break;
            }
        }

        TEST_ASSERT(temp_index < (sizeof(repositories)/sizeof(repositories[0])));

        TEST_ASSERT(0 == strcmp(ma_ah_site_get_name(site), repositories[temp_index].repo_name));
        TEST_ASSERT(0 == strcmp(ma_ah_site_get_fqdn_name(site), repositories[temp_index].repo_server_fqdn));
        TEST_ASSERT(0 == strcmp(ma_ah_site_get_short_name(site), repositories[temp_index].repo_server_name));
        TEST_ASSERT(0 == strcmp(ma_ah_site_get_ip_address(site), repositories[temp_index].repo_server_ip));
        TEST_ASSERT(ma_ah_site_get_port(site) == repositories[temp_index].repo_port);
        // FAIL-> TEST_ASSERT(ma_ah_site_get_order(site) == repositories[temp_index].repo_siteorder);
    }

    ma_ah_site_url_provider_cache_current_site(NULL);
    ma_ah_site_url_provider_cache_current_site(ah_site_url_provider);

    TEST_ASSERT(NULL == (url = ma_ah_site_url_provider_get_next_url(NULL, MA_TRUE)));
    TEST_ASSERT(NULL == (url = ma_ah_site_url_provider_get_next_url(NULL, MA_FALSE)));
    TEST_ASSERT(NULL != (url = ma_ah_site_url_provider_get_next_url(ah_site_url_provider, MA_TRUE)));
    TEST_ASSERT(NULL != (url = ma_ah_site_url_provider_get_next_url(ah_site_url_provider, MA_FALSE)));

	ma_ah_site_url_provider_deinit(ah_site_url_provider);

    ma_ah_site_url_provider_reset_url(NULL);
    ma_ah_site_url_provider_reset_url(ah_site_url_provider);

    ma_ah_site_url_provider_release(NULL);
    ma_ah_site_url_provider_release(ah_site_url_provider);
}
