#include "unity.h"
#include "unity_fixture.h"

#include "ma_ah_client_service_test_helper.h"
#include "ma/internal/core/msgbus/ma_msgbus_private.h"
#include "ma/internal/services/ma_service.h"
#include "ma/internal/services/ahclient/ma_ah_client_service.h"
#include "ma/internal/utils/repository/ma_repository_db_manager.h"

#include "ma_ut_setup.h"

#ifdef TEST_AGENT_GUID
#undef TEST_AGENT_GUID
#endif
#define TEST_AGENT_GUID "ekrfhn43hrlhfn34kh4ifn4krn43"

#define CA_FILE_NAME        "C:\\Programdata\\cabundle.cer"
#define GLOBAL_VERSION      "VlUZlJaB6wuBd75vKrG/LUUa2dLd59wdI0BOR9lUmIs="
#define LOCAL_VERSION       "Mon, 16 Jul 2012 11:52:32 UTC"

#define STR_PKGTYPE_UNIT_TEST "UnitTest"
#define STR_PKGTYPE_UT_SITE_ORDER "UTSiteOrder"
#define STR_PKGTYPE_UT_FALL_BACK "UTFallBack"
#define STR_PKGTYPE_UT_FAILURE "UTFailure"

#define MAX_LOOP_WAIT 60

uv_timer_t wait_timer;
uv_loop_t *wait_loop;

static ma_ut_setup_t *g_ut_setup = NULL;
const char                            *service_name = "test.ah_client_service";
ma_ah_client_service_t                *ah_client_service = NULL;
ma_spipe_decorator_t                ma_ah_client_service_test_decorator;
ma_spipe_handler_t                    ma_ah_client_service_test_handler;

static ma_datastore_configuration_request_t datastore_configuration_request =
   {{MA_TRUE,MA_DB_OPEN_READWRITE},
    {MA_TRUE,MA_DB_OPEN_READWRITE},
    {MA_TRUE,MA_DB_OPEN_READWRITE},
    {MA_TRUE,MA_DB_OPEN_READWRITE},
    {MA_TRUE,MA_DB_OPEN_READWRITE}};

static ma_agent_configuration_t agent_configuration = { 5, 10, 15, 0, 100, TEST_AGENT_GUID, "ewruhewoirlwenrwerwe", MA_FALSE };

static char* package_types[] = { STR_PKGTYPE_REQUEST_PUB_KEY, STR_PKGTYPE_PROPS_RESPONSE, STR_PKGTYPE_UNIT_TEST };

typedef struct ma_callback_status_s
{
	char package_type[MA_MAX_LEN];
    ma_bool_t decorator_invoked;
    ma_bool_t handler_invoked;
    ma_error_t net_service_error_code;
    int http_response_code;
    int wait_counter;
}ma_callback_status_t;

ma_callback_status_t callback_status_send_package;

struct repository_data
{
    const char *repo_name;
    ma_repository_type_t repo_type;
    ma_repository_url_type_t repo_url_type;
    ma_repository_namespace_t repo_namespace;
    ma_proxy_usage_type_t repo_proxy_usage_type;
    ma_repository_auth_t repo_auth;
    ma_bool_t repo_enabled;
    const char *repo_server_fqdn;
    const char *repo_server_name;
    const char *repo_server_ip;
    ma_uint32_t repo_port;
    const char *repo_server_path;
    const char *repo_domain_name;
    const char *repo_user_name;
    const char *repo_password;
    ma_uint32_t repo_pingtime;
    ma_uint32_t repo_subnetdistance;
    ma_uint32_t repo_siteorder;
    ma_repository_state_t repo_state;
    ma_uint32_t ssl_port;
};

static ma_error_t ma_ah_client_service_test_decorator_decorate(ma_spipe_decorator_t *decorator, ma_spipe_priority_t priority, ma_spipe_package_t *spipe_pkg)
{
    unsigned char *value = NULL;
    size_t size = 0;
    ma_uint16_t index = 0;

    if( MA_OK == ma_spipe_package_get_info(spipe_pkg, STR_PACKAGE_TYPE, &value, &size) && NULL != value)
    {
        for(index = 0; index < (sizeof(package_types)/sizeof(package_types[0])); index++)
        {
            if(0 == strcmp((char*)value, package_types[index]))
            {
				strcpy(callback_status_send_package.package_type, (char*)value);
                callback_status_send_package.decorator_invoked = MA_TRUE;
            }
        }

        if(0 == strcmp((char*)value, STR_PKGTYPE_UT_SITE_ORDER))
        {
            strcpy(callback_status_send_package.package_type, (char*)value);
            callback_status_send_package.decorator_invoked = MA_TRUE;
        }
        if(0 == strcmp((char*)value, STR_PKGTYPE_UT_FALL_BACK))
        {
            strcpy(callback_status_send_package.package_type, (char*)value);
            callback_status_send_package.decorator_invoked = MA_TRUE;
        }
        if(0 == strcmp((char*)value, STR_PKGTYPE_UT_FAILURE))
        {
            strcpy(callback_status_send_package.package_type, (char*)value);
            callback_status_send_package.decorator_invoked = MA_TRUE;
        }

    }
    return MA_OK;
}

static ma_error_t ma_ah_client_service_test_get_priority(ma_spipe_decorator_t *decorator, ma_spipe_priority_t *priority)
{
    *priority = MA_SPIPE_PRIORITY_IMMEDIATE;
    return MA_OK;
}

static ma_error_t ma_ah_client_service_test_destroy(ma_spipe_decorator_t *decorator)
{
    return MA_OK;
}

static ma_error_t ma_ah_client_service_test_handler_handle(ma_spipe_handler_t *handler, const ma_spipe_response_t *spipe_response)
{
    unsigned char *value = NULL;
    size_t size = 0;
    ma_uint16_t index = 0;

    callback_status_send_package.http_response_code = spipe_response->http_response_code;
    callback_status_send_package.net_service_error_code = spipe_response->net_service_error_code;
    callback_status_send_package.handler_invoked = MA_TRUE;

    return MA_OK;
}

static ma_error_t ma_ah_client_service_test_handler_destroy(ma_spipe_handler_t *handler)
{
    return MA_OK;
}

static ma_spipe_decorator_methods_t ma_ah_client_service_test_decorator_methods =
{
    &ma_ah_client_service_test_decorator_decorate,
    &ma_ah_client_service_test_get_priority,
    &ma_ah_client_service_test_destroy
};

static ma_spipe_handler_methods_t ma_ah_client_service_test_handler_methods =
{
    &ma_ah_client_service_test_handler_handle,
    &ma_ah_client_service_test_handler_destroy
};



static ma_error_t create_repository_db(const struct repository_data *repo_data, ma_uint16_t count)
{
    ma_error_t rc = MA_ERROR_UNEXPECTED;
    ma_repository_t *repository = NULL;
    ma_uint16_t index = 0;

	if(MA_OK == ma_repository_db_remove_all_repositories(ma_configurator_get_database(MA_CONTEXT_GET_CONFIGURATOR(ma_ut_setup_get_context(g_ut_setup)))))
    {
        for(;index < count; index++)
        {
            if(MA_OK == (rc = ma_repository_create(&repository)))
            {
                if((MA_OK == (rc = ma_repository_set_name(repository, repo_data[index].repo_name))) &&
                    (MA_OK == (rc = ma_repository_set_type(repository, repo_data[index].repo_type))) &&
                    (MA_OK == (rc = ma_repository_set_url_type(repository, repo_data[index].repo_url_type))) &&
                    (MA_OK == (rc = ma_repository_set_namespace(repository, repo_data[index].repo_namespace))) &&
                    (MA_OK == (rc = ma_repository_set_proxy_usage_type(repository, repo_data[index].repo_proxy_usage_type))) &&
                    (MA_OK == (rc = ma_repository_set_authentication(repository, repo_data[index].repo_auth))) &&
                    (MA_OK == (rc = ma_repository_set_enabled(repository, repo_data[index].repo_enabled))) &&
                    (MA_OK == (rc = ma_repository_set_server_fqdn(repository, repo_data[index].repo_server_fqdn))) &&
                    (MA_OK == (rc = ma_repository_set_server_name(repository, repo_data[index].repo_server_name))) &&
                    (MA_OK == (rc = ma_repository_set_server_ip(repository, repo_data[index].repo_server_ip))) &&
                    (MA_OK == (rc = ma_repository_set_port(repository, repo_data[index].repo_port))) &&
                    (MA_OK == (rc = ma_repository_set_ssl_port(repository, repo_data[index].ssl_port))) &&
                    (MA_OK == (rc = ma_repository_set_server_path(repository, repo_data[index].repo_server_path))) &&
                    (MA_OK == (rc = ma_repository_set_user_name(repository, repo_data[index].repo_user_name))) &&
                    (MA_OK == (rc = ma_repository_set_password(repository, repo_data[index].repo_password))) &&
                    (MA_OK == (rc = ma_repository_set_pingtime(repository, repo_data[index].repo_pingtime))) &&
                    (MA_OK == (rc = ma_repository_set_subnetdistance(repository, repo_data[index].repo_subnetdistance))) &&
                    (MA_OK == (rc = ma_repository_set_siteorder(repository, repo_data[index].repo_siteorder))) &&
                    (MA_OK == (rc = ma_repository_set_state(repository, repo_data[index].repo_state))))
                {
                    rc = ma_repository_db_add_repository(ma_configurator_get_database(MA_CONTEXT_GET_CONFIGURATOR(ma_ut_setup_get_context(g_ut_setup))), repository);
                }
            }

            if(repository)
            {
                ma_repository_release(repository);
            }
        }
    }
    return rc;
}

void ma_callback_status_reset(ma_callback_status_t *callback_status)
{
    callback_status->decorator_invoked = MA_FALSE;
    callback_status->handler_invoked = MA_FALSE;
    callback_status->http_response_code = -1;
    callback_status->net_service_error_code = MA_ERROR_UNEXPECTED;
    callback_status->wait_counter = 0;
}

extern ma_ut_setup_t *get_ut_setup();

TEST_GROUP_RUNNER(ah_client_service_tests_group)
{
	g_ut_setup = get_ut_setup();
    do {RUN_TEST_CASE(ah_client_service_tests, send_package);} while (0);
    do {RUN_TEST_CASE(ah_client_service_tests, verify_site_order);} while (0);
    do {RUN_TEST_CASE(ah_client_service_tests, verify_fall_back);} while (0);
    do {RUN_TEST_CASE(ah_client_service_tests, verify_failure);} while (0);
}

TEST_SETUP(ah_client_service_tests)
{
	TEST_ASSERT(NULL != g_ut_setup);

	TEST_ASSERT(MA_OK == ma_event_loop_get_loop_impl(ma_msgbus_get_event_loop(ma_ut_setup_get_msgbus(g_ut_setup)), &wait_loop));
    TEST_ASSERT(0 == uv_timer_init(wait_loop, &wait_timer));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_client_service_create(NULL, (ma_service_t**)&ah_client_service));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_client_service_create(service_name, NULL));
    TEST_ASSERT(MA_OK == ma_ah_client_service_create(service_name, (ma_service_t**)&ah_client_service));

	TEST_ASSERT(MA_OK == ma_net_client_service_start(MA_CONTEXT_GET_NET_CLIENT(ma_ut_setup_get_context(g_ut_setup))));
}

TEST_TEAR_DOWN(ah_client_service_tests)
{
    TEST_ASSERT(MA_ERROR_INVALIDARG == ((ma_service_t *)ah_client_service)->methods->stop(NULL));
    TEST_ASSERT(MA_OK == ((ma_service_t *)ah_client_service)->methods->stop(((ma_service_t *)ah_client_service)));

	//FAIL-> ((ma_service_t *)ah_client_service)->methods->destroy(((ma_service_t *)ah_client_service));
	//FAIL-> ah_client_service = NULL;
    TEST_ASSERT(MA_OK == ma_net_client_service_stop(MA_CONTEXT_GET_NET_CLIENT(ma_ut_setup_get_context(g_ut_setup))));
}

void wait_timer_event(uv_timer_t *handle, int status)
{
    if(callback_status_send_package.handler_invoked == MA_FALSE)
    {
    }
    else
    {
        uv_timer_stop(handle);
    }
}

static struct repository_data const repo_data_1[] =
{
    // Name            Type                         URL Type                      Namespace                      Proxy Usage Type            Authentication          Enabled          Server FQDN             Server Name       Server IP   Port     Server Path              Domain Name Username  Password Pingtime Subnet Distance Siteorder        State                  ssl port
    {  "EPO_SER3", MA_REPOSITORY_TYPE_MASTER, MA_REPOSITORY_URL_TYPE_SPIPE, MA_REPOSITORY_NAMESPACE_LOCAL, MA_PROXY_USAGE_TYPE_NONE, MA_REPOSITORY_AUTH_NONE,    MA_TRUE,  "lt-rejeesh.corp.nai.org", "lt-rejeesh",      "127.0.0.1",    80, "ftp:\\\\my_ftp_repository", "My_Domain", "",          "",     1000,      4,               3,    MA_REPOSITORY_STATE_ADDED,    443   },
};

ma_error_t ma_event_loop_test_run(ma_event_loop_t *self) 
{
	uv_loop_t *loop = NULL;

    if (!self) {
        return MA_ERROR_INVALIDARG;
    }
	ma_event_loop_get_loop_impl(self, &loop);

#if (9 < UV_VERSION_MINOR)
    (void) uv_run(loop, UV_RUN_DEFAULT);
#else
    (void) uv_run(loop);
#endif

    /* give libuv one more shot at eliminating our async_watcher residue because we closed it after loop termination */
#if (9 < UV_VERSION_MINOR)
    (void) uv_run(loop, UV_RUN_NOWAIT);
#else
    (void) uv_run(loop);
#endif

    return MA_OK;
}

TEST(ah_client_service_tests, send_package)
{
    ma_uint16_t index = 0;
    ma_ah_sitelist_t *sitelist = NULL;

    TEST_ASSERT(MA_OK == create_repository_db( repo_data_1, (sizeof(repo_data_1)/sizeof(repo_data_1[0]))));

	TEST_ASSERT(MA_ERROR_INVALIDARG == ((ma_service_t *)ah_client_service)->methods->configure(NULL, ma_ut_setup_get_context(g_ut_setup), MA_SERVICE_CONFIG_NEW));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ((ma_service_t *)ah_client_service)->methods->configure((ma_service_t *)ah_client_service, NULL, MA_SERVICE_CONFIG_NEW));
    TEST_ASSERT(MA_OK == ((ma_service_t *)ah_client_service)->methods->configure((ma_service_t *)ah_client_service, ma_ut_setup_get_context(g_ut_setup), MA_SERVICE_CONFIG_NEW));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ((ma_service_t *)ah_client_service)->methods->start(NULL));
    TEST_ASSERT(MA_OK == ((ma_service_t *)ah_client_service)->methods->start(((ma_service_t *)ah_client_service)));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_client_service_get_sitelist(NULL, &sitelist));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_client_service_get_sitelist(ah_client_service, NULL));
    TEST_ASSERT(MA_OK == ma_ah_client_service_get_sitelist(ah_client_service, &sitelist));

    TEST_ASSERT(MA_OK == ma_ah_sitelist_set_global_version(sitelist, GLOBAL_VERSION));
    TEST_ASSERT(MA_OK == ma_ah_sitelist_set_local_version(sitelist, LOCAL_VERSION));
    TEST_ASSERT(MA_OK == ma_ah_sitelist_set_ca_file(sitelist, CA_FILE_NAME));

    //FAIL-> ma_spipe_decorator_init(NULL, &ma_ah_client_service_test_decorator_methods, NULL);
    ma_spipe_decorator_init(&ma_ah_client_service_test_decorator, NULL, NULL);
    ma_spipe_decorator_init(&ma_ah_client_service_test_decorator, &ma_ah_client_service_test_decorator_methods, NULL);

    //FAIL-> ma_spipe_handler_init(NULL, &ma_ah_client_service_test_handler_methods, NULL);
    ma_spipe_handler_init(&ma_ah_client_service_test_handler, NULL, NULL);
    ma_spipe_handler_init(&ma_ah_client_service_test_handler, &ma_ah_client_service_test_handler_methods, NULL);

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_client_service_register_spipe_decorator(NULL, &ma_ah_client_service_test_decorator));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_client_service_register_spipe_decorator(ah_client_service, NULL));
    TEST_ASSERT(MA_OK == ma_ah_client_service_register_spipe_decorator(ah_client_service, &ma_ah_client_service_test_decorator));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_client_service_register_spipe_handler(NULL, &ma_ah_client_service_test_handler));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_client_service_register_spipe_handler(ah_client_service, NULL));
    TEST_ASSERT(MA_OK == ma_ah_client_service_register_spipe_handler(ah_client_service, &ma_ah_client_service_test_handler));

    for(index = 0; index < (sizeof(package_types)/sizeof(package_types[0])); index++)
    {
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_client_service_raise_spipe_alert(NULL, package_types[index]));
        TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_client_service_raise_spipe_alert(ah_client_service, NULL));
        TEST_ASSERT(MA_OK == ma_ah_client_service_raise_spipe_alert(ah_client_service, package_types[index]));

        ma_callback_status_reset(&callback_status_send_package);
        TEST_ASSERT(0 == uv_timer_start(&wait_timer, wait_timer_event, 0, 500));
        TEST_ASSERT(MA_OK == ma_event_loop_test_run(ma_msgbus_get_event_loop(MA_CONTEXT_GET_MSGBUS(ma_ut_setup_get_context(g_ut_setup)))));

        TEST_ASSERT(MA_TRUE == callback_status_send_package.decorator_invoked);
        TEST_ASSERT(MA_TRUE == callback_status_send_package.handler_invoked);
        TEST_ASSERT(0 == strcmp(package_types[index], callback_status_send_package.package_type));
        TEST_ASSERT(200 == callback_status_send_package.http_response_code);
        TEST_ASSERT(MA_ERROR_NETWORK_REQUEST_SUCCEEDED == callback_status_send_package.net_service_error_code);
    }

    ma_callback_status_reset(&callback_status_send_package);

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_client_service_unregister_spipe_decorator(NULL, &ma_ah_client_service_test_decorator));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_client_service_unregister_spipe_decorator(ah_client_service, NULL));
    TEST_ASSERT(MA_OK == ma_ah_client_service_unregister_spipe_decorator(ah_client_service, &ma_ah_client_service_test_decorator));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_client_service_unregister_spipe_handler(NULL, &ma_ah_client_service_test_handler));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_client_service_unregister_spipe_handler(ah_client_service, NULL));
    TEST_ASSERT(MA_OK == ma_ah_client_service_unregister_spipe_handler(ah_client_service, &ma_ah_client_service_test_handler));
}

static struct repository_data const repo_data_2[] =
{
    //   Name             Type                         URL Type                      Namespace                 Proxy Usage Type          Authentication          Enabled         Server FQDN          Server Name   Server IP   Port     Server Path              Domain Name Username  Password Pingtime Subnet Distance Siteorder        State                ssl port
    {  "EPO_SER1", MA_REPOSITORY_TYPE_MASTER, MA_REPOSITORY_URL_TYPE_SPIPE, MA_REPOSITORY_NAMESPACE_LOCAL, MA_PROXY_USAGE_TYPE_NONE, MA_REPOSITORY_AUTH_NONE,    MA_TRUE, "lt-rejeesh.corp.nai.org", "lt-rejeesh", "127.0.0.1",  80, "ftp:\\\\my_ftp_repository", "My_Domain",   "",       "",     1000,      4,         18,        MA_REPOSITORY_STATE_ADDED,    443   },
    {  "EPO_SER2", MA_REPOSITORY_TYPE_MASTER, MA_REPOSITORY_URL_TYPE_SPIPE, MA_REPOSITORY_NAMESPACE_LOCAL, MA_PROXY_USAGE_TYPE_NONE, MA_REPOSITORY_AUTH_NONE,    MA_TRUE, "lt-rejeesh.corp.nai.org", "lt-rejeesh", "127.0.0.2",  80, "ftp:\\\\my_ftp_repository", "My_Domain",   "",       "",     1000,      4,          5,        MA_REPOSITORY_STATE_ADDED,    443   },
    {  "EPO_SER3", MA_REPOSITORY_TYPE_MASTER, MA_REPOSITORY_URL_TYPE_SPIPE, MA_REPOSITORY_NAMESPACE_LOCAL, MA_PROXY_USAGE_TYPE_NONE, MA_REPOSITORY_AUTH_NONE,    MA_TRUE, "lt-rejeesh.corp.nai.org", "lt-rejeesh", "127.0.0.3",  80, "ftp:\\\\my_ftp_repository", "My_Domain",   "",       "",     1000,      4,          8,        MA_REPOSITORY_STATE_ADDED,    443   },
};


TEST(ah_client_service_tests, verify_site_order)
{
    ma_ah_sitelist_t *sitelist = NULL;

    TEST_ASSERT(MA_OK == create_repository_db( repo_data_2, (sizeof(repo_data_2)/sizeof(repo_data_2[0]))));

    TEST_ASSERT(MA_OK == ((ma_service_t *)ah_client_service)->methods->configure((ma_service_t *)ah_client_service, ma_ut_setup_get_context(g_ut_setup), MA_SERVICE_CONFIG_NEW));
    TEST_ASSERT(MA_OK == ((ma_service_t *)ah_client_service)->methods->start(((ma_service_t *)ah_client_service)));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_client_service_get_sitelist(NULL, &sitelist));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_client_service_get_sitelist(ah_client_service, NULL));
    TEST_ASSERT(MA_OK == ma_ah_client_service_get_sitelist(ah_client_service, &sitelist));

    TEST_ASSERT(MA_OK == ma_ah_sitelist_set_global_version(sitelist, GLOBAL_VERSION));
    TEST_ASSERT(MA_OK == ma_ah_sitelist_set_local_version(sitelist, LOCAL_VERSION));
    TEST_ASSERT(MA_OK == ma_ah_sitelist_set_ca_file(sitelist, CA_FILE_NAME));

    ma_spipe_decorator_init(&ma_ah_client_service_test_decorator, &ma_ah_client_service_test_decorator_methods, NULL);
    ma_spipe_handler_init(&ma_ah_client_service_test_handler, &ma_ah_client_service_test_handler_methods, NULL);

    TEST_ASSERT(MA_OK == ma_ah_client_service_register_spipe_decorator(ah_client_service, &ma_ah_client_service_test_decorator));
    TEST_ASSERT(MA_OK == ma_ah_client_service_register_spipe_handler(ah_client_service, &ma_ah_client_service_test_handler));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_client_service_raise_spipe_alert(NULL, STR_PKGTYPE_UT_SITE_ORDER));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_client_service_raise_spipe_alert(ah_client_service, NULL));
    TEST_ASSERT(MA_OK == ma_ah_client_service_raise_spipe_alert(ah_client_service, STR_PKGTYPE_UT_SITE_ORDER));

    ma_callback_status_reset(&callback_status_send_package);
    TEST_ASSERT(0 == uv_timer_start(&wait_timer, wait_timer_event, 0, 500));
    TEST_ASSERT(MA_OK == ma_event_loop_test_run(ma_msgbus_get_event_loop(MA_CONTEXT_GET_MSGBUS(ma_ut_setup_get_context(g_ut_setup)))));

    TEST_ASSERT(MA_TRUE == callback_status_send_package.decorator_invoked);
    TEST_ASSERT(MA_TRUE == callback_status_send_package.handler_invoked);
    TEST_ASSERT(0 == strcmp(STR_PKGTYPE_UT_SITE_ORDER, callback_status_send_package.package_type));
    TEST_ASSERT(200 == callback_status_send_package.http_response_code);
    TEST_ASSERT(MA_ERROR_NETWORK_REQUEST_SUCCEEDED == callback_status_send_package.net_service_error_code);
    ma_callback_status_reset(&callback_status_send_package);

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_client_service_unregister_spipe_decorator(NULL, &ma_ah_client_service_test_decorator));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_client_service_unregister_spipe_decorator(ah_client_service, NULL));
    TEST_ASSERT(MA_OK == ma_ah_client_service_unregister_spipe_decorator(ah_client_service, &ma_ah_client_service_test_decorator));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_client_service_unregister_spipe_handler(NULL, &ma_ah_client_service_test_handler));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_client_service_unregister_spipe_handler(ah_client_service, NULL));
    TEST_ASSERT(MA_OK == ma_ah_client_service_unregister_spipe_handler(ah_client_service, &ma_ah_client_service_test_handler));
}

static struct repository_data const repo_data_3[] =
{
    //	  Name            Type                         URL Type                      Namespace                 Proxy Usage Type         Authentication           Enabled       Server FQDN           Server Name    Server IP   Port       Server Path              Domain Name   Username  Password Pingtime Subnet Distance Siteorder        State              ssl port
    {  "EPO_SER1", MA_REPOSITORY_TYPE_MASTER, MA_REPOSITORY_URL_TYPE_SPIPE, MA_REPOSITORY_NAMESPACE_LOCAL, MA_PROXY_USAGE_TYPE_NONE, MA_REPOSITORY_AUTH_NONE,    MA_TRUE, "lt-rejeesh.corp.nai.org", "lt-rejeesh", "127.0.0.1",  0,   "ftp:\\\\my_ftp_repository",  "My_Domain",     "",       "",     1000,        4,            1,    MA_REPOSITORY_STATE_ADDED,    0   },
    {  "EPO_SER2", MA_REPOSITORY_TYPE_MASTER, MA_REPOSITORY_URL_TYPE_SPIPE, MA_REPOSITORY_NAMESPACE_LOCAL, MA_PROXY_USAGE_TYPE_NONE, MA_REPOSITORY_AUTH_NONE,    MA_TRUE, "lt-rejeesh.corp.nai.org", "lt-rejeesh", "127.0.0.2",  0,   "ftp:\\\\my_ftp_repository",  "My_Domain",     "",       "",     1000,        4,            2,    MA_REPOSITORY_STATE_ADDED,    0   },
    {  "EPO_SER3", MA_REPOSITORY_TYPE_MASTER, MA_REPOSITORY_URL_TYPE_SPIPE, MA_REPOSITORY_NAMESPACE_LOCAL, MA_PROXY_USAGE_TYPE_NONE, MA_REPOSITORY_AUTH_NONE,    MA_TRUE, "lt-rejeesh.corp.nai.org", "lt-rejeesh", "127.0.0.3",  80,  "ftp:\\\\my_ftp_repository",  "My_Domain",     "",       "",     1000,        4,            3,    MA_REPOSITORY_STATE_ADDED,    443   },
};


TEST(ah_client_service_tests, verify_fall_back)
{
    ma_ah_sitelist_t *sitelist = NULL;

    TEST_ASSERT(MA_OK == create_repository_db( repo_data_3, (sizeof(repo_data_3)/sizeof(repo_data_3[0]))));

    TEST_ASSERT(MA_OK == ((ma_service_t *)ah_client_service)->methods->configure((ma_service_t *)ah_client_service, ma_ut_setup_get_context(g_ut_setup), MA_SERVICE_CONFIG_NEW));
    TEST_ASSERT(MA_OK == ((ma_service_t *)ah_client_service)->methods->start(((ma_service_t *)ah_client_service)));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_client_service_get_sitelist(NULL, &sitelist));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_client_service_get_sitelist(ah_client_service, NULL));
    TEST_ASSERT(MA_OK == ma_ah_client_service_get_sitelist(ah_client_service, &sitelist));

    TEST_ASSERT(MA_OK == ma_ah_sitelist_set_global_version(sitelist, GLOBAL_VERSION));
    TEST_ASSERT(MA_OK == ma_ah_sitelist_set_local_version(sitelist, LOCAL_VERSION));
    TEST_ASSERT(MA_OK == ma_ah_sitelist_set_ca_file(sitelist, CA_FILE_NAME));

    ma_spipe_decorator_init(&ma_ah_client_service_test_decorator, &ma_ah_client_service_test_decorator_methods, NULL);
    ma_spipe_handler_init(&ma_ah_client_service_test_handler, &ma_ah_client_service_test_handler_methods, NULL);

    TEST_ASSERT(MA_OK == ma_ah_client_service_register_spipe_decorator(ah_client_service, &ma_ah_client_service_test_decorator));
    TEST_ASSERT(MA_OK == ma_ah_client_service_register_spipe_handler(ah_client_service, &ma_ah_client_service_test_handler));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_client_service_raise_spipe_alert(NULL, STR_PKGTYPE_UT_FALL_BACK));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_client_service_raise_spipe_alert(ah_client_service, NULL));
    TEST_ASSERT(MA_OK == ma_ah_client_service_raise_spipe_alert(ah_client_service, STR_PKGTYPE_UT_FALL_BACK));

    ma_callback_status_reset(&callback_status_send_package);
    TEST_ASSERT(0 == uv_timer_start(&wait_timer, wait_timer_event, 0, 500));
    TEST_ASSERT(MA_OK == ma_event_loop_test_run(ma_msgbus_get_event_loop(MA_CONTEXT_GET_MSGBUS(ma_ut_setup_get_context(g_ut_setup)))));

    TEST_ASSERT(MA_TRUE == callback_status_send_package.decorator_invoked);
    TEST_ASSERT(MA_TRUE == callback_status_send_package.handler_invoked);
    TEST_ASSERT(0 == strcmp(STR_PKGTYPE_UT_FALL_BACK, callback_status_send_package.package_type));
    
    ma_callback_status_reset(&callback_status_send_package);

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_client_service_unregister_spipe_decorator(NULL, &ma_ah_client_service_test_decorator));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_client_service_unregister_spipe_decorator(ah_client_service, NULL));
    TEST_ASSERT(MA_OK == ma_ah_client_service_unregister_spipe_decorator(ah_client_service, &ma_ah_client_service_test_decorator));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_client_service_unregister_spipe_handler(NULL, &ma_ah_client_service_test_handler));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_client_service_unregister_spipe_handler(ah_client_service, NULL));
    TEST_ASSERT(MA_OK == ma_ah_client_service_unregister_spipe_handler(ah_client_service, &ma_ah_client_service_test_handler));
}

static struct repository_data const repo_data_4[] =
{
    //   Name          Type                         URL Type                      Namespace                      Proxy Usage Type            Authentication      Enabled        Server FQDN           Server Name   Server IP   Port     Server Path              Domain Name Username  Password  Pingtime	Subnet Distance Siteorder        State				ssl port
    {  "EPO_SER1", MA_REPOSITORY_TYPE_MASTER, MA_REPOSITORY_URL_TYPE_SPIPE, MA_REPOSITORY_NAMESPACE_LOCAL, MA_PROXY_USAGE_TYPE_NONE, MA_REPOSITORY_AUTH_NONE,    MA_TRUE, "lt-rejeesh.corp.nai.org", "lt-rejeesh", "127.0.0.1",  0, "ftp:\\\\my_ftp_repository", "My_Domain",	"",          "",     1000,			4,         4,    MA_REPOSITORY_STATE_ADDED,    0   },
    {  "EPO_SER2", MA_REPOSITORY_TYPE_MASTER, MA_REPOSITORY_URL_TYPE_SPIPE, MA_REPOSITORY_NAMESPACE_LOCAL, MA_PROXY_USAGE_TYPE_NONE, MA_REPOSITORY_AUTH_NONE,    MA_TRUE, "lt-rejeesh.corp.nai.org", "lt-rejeesh", "127.0.0.2",  0, "ftp:\\\\my_ftp_repository", "My_Domain",	"",          "",     1000,			4,         4,    MA_REPOSITORY_STATE_ADDED,    0   },
    {  "EPO_SER3", MA_REPOSITORY_TYPE_MASTER, MA_REPOSITORY_URL_TYPE_SPIPE, MA_REPOSITORY_NAMESPACE_LOCAL, MA_PROXY_USAGE_TYPE_NONE, MA_REPOSITORY_AUTH_NONE,    MA_TRUE, "lt-rejeesh.corp.nai.org", "lt-rejeesh", "127.0.0.3",  0, "ftp:\\\\my_ftp_repository", "My_Domain",	"",          "",     1000,			4,         4,    MA_REPOSITORY_STATE_ADDED,    0   },
};

TEST(ah_client_service_tests, verify_failure)
{
    ma_ah_sitelist_t *sitelist = NULL;

    TEST_ASSERT(MA_OK == create_repository_db( repo_data_4, (sizeof(repo_data_4)/sizeof(repo_data_4[0]))));

    TEST_ASSERT(MA_OK == ((ma_service_t *)ah_client_service)->methods->configure((ma_service_t *)ah_client_service, ma_ut_setup_get_context(g_ut_setup), MA_SERVICE_CONFIG_NEW));
    TEST_ASSERT(MA_OK == ((ma_service_t *)ah_client_service)->methods->start(((ma_service_t *)ah_client_service)));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_client_service_get_sitelist(NULL, &sitelist));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_client_service_get_sitelist(ah_client_service, NULL));
    TEST_ASSERT(MA_OK == ma_ah_client_service_get_sitelist(ah_client_service, &sitelist));

    TEST_ASSERT(MA_OK == ma_ah_sitelist_set_global_version(sitelist, GLOBAL_VERSION));
    TEST_ASSERT(MA_OK == ma_ah_sitelist_set_local_version(sitelist, LOCAL_VERSION));
    TEST_ASSERT(MA_OK == ma_ah_sitelist_set_ca_file(sitelist, CA_FILE_NAME));

    ma_spipe_decorator_init(&ma_ah_client_service_test_decorator, &ma_ah_client_service_test_decorator_methods, NULL);
    ma_spipe_handler_init(&ma_ah_client_service_test_handler, &ma_ah_client_service_test_handler_methods, NULL);

    TEST_ASSERT(MA_OK == ma_ah_client_service_register_spipe_decorator(ah_client_service, &ma_ah_client_service_test_decorator));
    TEST_ASSERT(MA_OK == ma_ah_client_service_register_spipe_handler(ah_client_service, &ma_ah_client_service_test_handler));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_client_service_raise_spipe_alert(NULL, STR_PKGTYPE_UT_FAILURE));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_client_service_raise_spipe_alert(ah_client_service, NULL));
    TEST_ASSERT(MA_OK == ma_ah_client_service_raise_spipe_alert(ah_client_service, STR_PKGTYPE_UT_FAILURE));

    ma_callback_status_reset(&callback_status_send_package);
    TEST_ASSERT(0 == uv_timer_start(&wait_timer, wait_timer_event, 0, 500));
    TEST_ASSERT(MA_OK == ma_event_loop_test_run(ma_msgbus_get_event_loop(MA_CONTEXT_GET_MSGBUS(ma_ut_setup_get_context(g_ut_setup)))));

    TEST_ASSERT(MA_TRUE == callback_status_send_package.decorator_invoked);
    TEST_ASSERT(MA_TRUE == callback_status_send_package.handler_invoked);
    TEST_ASSERT(0 == strcmp(STR_PKGTYPE_UT_FAILURE, callback_status_send_package.package_type));
    TEST_ASSERT(200 != callback_status_send_package.http_response_code);
    TEST_ASSERT(MA_ERROR_NETWORK_REQUEST_SUCCEEDED != callback_status_send_package.net_service_error_code);

    ma_callback_status_reset(&callback_status_send_package);

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_client_service_unregister_spipe_decorator(NULL, &ma_ah_client_service_test_decorator));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_client_service_unregister_spipe_decorator(ah_client_service, NULL));
    TEST_ASSERT(MA_OK == ma_ah_client_service_unregister_spipe_decorator(ah_client_service, &ma_ah_client_service_test_decorator));

    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_client_service_unregister_spipe_handler(NULL, &ma_ah_client_service_test_handler));
    TEST_ASSERT(MA_ERROR_INVALIDARG == ma_ah_client_service_unregister_spipe_handler(ah_client_service, NULL));
    TEST_ASSERT(MA_OK == ma_ah_client_service_unregister_spipe_handler(ah_client_service, &ma_ah_client_service_test_handler));
}
