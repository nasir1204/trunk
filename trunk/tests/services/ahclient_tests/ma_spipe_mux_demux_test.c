#include "unity.h"
#include "unity_fixture.h"
#include "tests/utils/macrypto_test/ma_crypto_tests_setup.h"
#include "ma_spipe_mux.h"
#include "ma_spipe_demux.h"
#include "ma_ah_client_service_test_helper.h"

#include "ma/internal/core/msgbus/ma_msgbus_private.h"

#include "ma_ut_setup.h"

#define FILE_STREAM "test_file"
#define FILE_STREAM_1 "test_file_1"
#define FILE_STREAM_2 "test_file_2"
#define FILE_STREAM_3 "test_file_3"

static ma_ut_setup_t *g_ut_setup = NULL;

extern ma_ut_setup_t *get_ut_setup();

TEST_GROUP_RUNNER(spipe_mux_demux_tests_group)
{
	do {RUN_TEST_CASE(spipe_mux_demux_tests, verify_mux);} while (0);
	do {RUN_TEST_CASE(spipe_mux_demux_tests, verify_demux);} while (0);
}

TEST_SETUP(spipe_mux_demux_tests)
{
	unlink(FILE_STREAM);
	unlink(FILE_STREAM_1);
	unlink(FILE_STREAM_2);
	unlink(FILE_STREAM_3);
	g_ut_setup = get_ut_setup();
}

TEST_TEAR_DOWN(spipe_mux_demux_tests)
{
	unlink(FILE_STREAM);
	unlink(FILE_STREAM_1);
	unlink(FILE_STREAM_2);
	unlink(FILE_STREAM_3);
}

static ma_error_t spipe_decorate_property(ma_spipe_decorator_t *decorator, ma_spipe_priority_t priority, ma_spipe_package_t *spipe_pkg) 
{
	return ma_spipe_package_add_data(spipe_pkg, "data", "test data", strlen("test data"));
}

static ma_error_t get_spipe_property_priority(ma_spipe_decorator_t *decorator, ma_spipe_priority_t *priority) 
{
    *priority =  MA_SPIPE_PRIORITY_NORMAL ;
    return MA_OK ;
}

static ma_error_t property_decorator_destroy(ma_spipe_decorator_t *decorator) 
{   
	return MA_OK ;
}

static ma_spipe_decorator_methods_t spipe_decorator_methods = 
{    
    &spipe_decorate_property,
    &get_spipe_property_priority,
    &property_decorator_destroy
};

TEST(spipe_mux_demux_tests, verify_mux)
{
	ma_spipe_mux_t spipe_mux;
	ma_spipe_decorator_t decorator;
	ma_stream_t *mem_stream = NULL, *mem_stream1 = NULL, *mem_stream2 = NULL, *mem_stream3 = NULL;

	// FAIL-> ma_spipe_mux_init(NULL);
	ma_spipe_mux_init(&spipe_mux);

	ma_spipe_decorator_init(&decorator, &spipe_decorator_methods, &decorator);
	TEST_ASSERT(MA_OK == ma_fstream_create(FILE_STREAM, MA_STREAM_ACCESS_FLAG_WRITE, NULL, &mem_stream));
	TEST_ASSERT(MA_OK == ma_fstream_create(FILE_STREAM_1, MA_STREAM_ACCESS_FLAG_WRITE, NULL, &mem_stream1));
	TEST_ASSERT(MA_OK == ma_fstream_create(FILE_STREAM_2, MA_STREAM_ACCESS_FLAG_WRITE, NULL, &mem_stream2));
	TEST_ASSERT(MA_OK == ma_fstream_create(FILE_STREAM_3, MA_STREAM_ACCESS_FLAG_WRITE, NULL, &mem_stream3));
	// FAIL-> TEST_ASSERT(MA_ERROR_INVALIDARG == ma_spipe_mux_add_spipe_decorator(NULL, &decorator));
	// FAIL-> TEST_ASSERT(MA_ERROR_INVALIDARG == ma_spipe_mux_add_spipe_decorator(&spipe_mux, NULL));
	TEST_ASSERT(MA_OK == ma_spipe_mux_add_spipe_decorator(&spipe_mux, &decorator));

	// FAIL-> TEST_ASSERT(MA_SPIPE_PRIORITY_NONE == ma_spipe_mux_get_spipe_priority(NULL));
	TEST_ASSERT(MA_SPIPE_PRIORITY_NORMAL == ma_spipe_mux_get_spipe_priority(&spipe_mux));
	
	// FAIL-> TEST_ASSERT(MA_ERROR_INVALIDARG == ma_spipe_mux_start(NULL, MA_SPIPE_PRIORITY_NONE, STR_PKGTYPE_EVENT, MA_CONTEXT_GET_CRYPTO(ma_ut_setup_get_context(g_ut_setup)), (ma_logger_t*)ma_ut_setup_get_logger(g_ut_setup), mem_stream1));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_spipe_mux_start(&spipe_mux, MA_SPIPE_PRIORITY_NONE, STR_PKGTYPE_EVENT, NULL, (ma_logger_t*)ma_ut_setup_get_logger(g_ut_setup), mem_stream1));
	TEST_ASSERT(MA_OK == ma_spipe_mux_start(&spipe_mux, MA_SPIPE_PRIORITY_NONE, STR_PKGTYPE_EVENT, MA_CONTEXT_GET_CRYPTO(ma_ut_setup_get_context(g_ut_setup)), NULL, mem_stream));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_spipe_mux_start(&spipe_mux, MA_SPIPE_PRIORITY_NONE, STR_PKGTYPE_EVENT, MA_CONTEXT_GET_CRYPTO(ma_ut_setup_get_context(g_ut_setup)), (ma_logger_t*)ma_ut_setup_get_logger(g_ut_setup), NULL));
	
	TEST_ASSERT(MA_OK == ma_spipe_mux_start(&spipe_mux, MA_SPIPE_PRIORITY_NONE, STR_PKGTYPE_EVENT, MA_CONTEXT_GET_CRYPTO(ma_ut_setup_get_context(g_ut_setup)), (ma_logger_t*)ma_ut_setup_get_logger(g_ut_setup), mem_stream1));
	TEST_ASSERT(MA_OK == ma_spipe_mux_start(&spipe_mux, MA_SPIPE_PRIORITY_NONE, STR_PKGTYPE_MSG_UPLOAD, MA_CONTEXT_GET_CRYPTO(ma_ut_setup_get_context(g_ut_setup)), (ma_logger_t*)ma_ut_setup_get_logger(g_ut_setup), mem_stream2));
	TEST_ASSERT(MA_OK == ma_spipe_mux_start(&spipe_mux, MA_SPIPE_PRIORITY_NONE, STR_PKGTYPE_PACKAGE_REQUEST, MA_CONTEXT_GET_CRYPTO(ma_ut_setup_get_context(g_ut_setup)), (ma_logger_t*)ma_ut_setup_get_logger(g_ut_setup), mem_stream3));
	
	ma_stream_release(mem_stream);
	ma_stream_release(mem_stream1);
	ma_stream_release(mem_stream2);
	ma_stream_release(mem_stream3);
	ma_spipe_mux_remove_spipe_decorator(&spipe_mux, &decorator);
	ma_spipe_mux_deinit(&spipe_mux);
}

ma_error_t spipe_key_handler_test(ma_spipe_handler_t *handler, const ma_spipe_response_t *spipe_response) 
{    
    return MA_OK;
}

static ma_error_t spipe_key_handler_destroy_test(ma_spipe_handler_t *handler) 
{
	return MA_OK;
}

static ma_spipe_handler_methods_t spipe_handler_methods = {    
    &spipe_key_handler_test,
    &spipe_key_handler_destroy_test
};

TEST(spipe_mux_demux_tests, verify_demux)
{
	ma_spipe_demux_t spipe_demux;
	ma_spipe_handler_t handler;
	ma_stream_t *mem_stream = NULL, *mem_stream1 = NULL, *mem_stream2 = NULL, *mem_stream3 = NULL;
	ma_spipe_response_t response;

	// FAIL-> ma_spipe_demux_init(NULL);
	ma_spipe_demux_init(&spipe_demux);

	ma_spipe_handler_init(&handler, &spipe_handler_methods, &handler);
	// FAIL-> TEST_ASSERT(MA_ERROR_INVALIDARG == ma_spipe_demux_add_spipe_handler(NULL, &handler));
	// FAIL-> TEST_ASSERT(MA_ERROR_INVALIDARG == ma_spipe_demux_add_spipe_handler(&spipe_demux, NULL));
	TEST_ASSERT(MA_OK == ma_spipe_demux_add_spipe_handler(&spipe_demux, &handler));

	// FAIL-> TEST_ASSERT(MA_ERROR_INVALIDARG == ma_spipe_demux_start(NULL, &response));
	// FAIL-> TEST_ASSERT(MA_ERROR_INVALIDARG == ma_spipe_demux_start(&spipe_demux, NULL));
	TEST_ASSERT(MA_OK == ma_spipe_demux_start(&spipe_demux, &response));
	
	ma_spipe_demux_remove_spipe_handler(&spipe_demux, &handler);
	ma_spipe_demux_deinit(&spipe_demux);
}