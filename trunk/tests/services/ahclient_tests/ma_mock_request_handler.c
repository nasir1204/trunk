#if defined(MA_NETWORK_USE_MOCK)

#include <stdio.h>
#include <stdlib.h>

#include "ma_mock_request.h"
#include "ma_mock_request_handler.h"
#include "ma/internal/utils/datastructures/ma_temp_buffer.h"
#include "ma_url_utils.h"
#include "ma/internal/ma_macros.h"
#include "ma_spipe_url_request.h"
#include "ma_spipe_url_request_internal.h"

#ifndef WIN32
#include <pthread.h>
#define HANDLE pthread_t
#endif

MA_CPP(extern "C" {)
	
static void wait_milliseconds( unsigned long wait_milli);

static ma_bool_t is_url_valid(char *url)
{
	ma_bool_t valid = MA_FALSE;
	ma_url_info_t *info = NULL;
	int port = 0;
	if(MA_OK == ma_url_utils_url_info_create(url, MA_FALSE, &info))
	{
		if(0 != (port = atoi(info->port)))
		{
			valid = MA_TRUE;
		}
	}
	return valid;
}

static int handle_request(ma_mock_url_request_t *req)
{
	int ret=0;
	unsigned char *data = NULL;
	int i=0;
    
	ma_error_t err = MA_OK;
	
	ma_mock_request_handler_t *handler=(ma_mock_request_handler_t*)(req->base.url_handler);
	ma_spipe_url_request_t *ma_spipe_url_request;
	if(is_url_valid(req->base.url))
	{
		MA_ATOMIC_INCREMENT(handler->running_handles) ;

		printf("Starting request handler service %d\n", req->bytes_processed);	
		req->base.request_state=MA_URL_REQUEST_STATE_REQUESTING;
		data = (unsigned char*) malloc( req->block_size+1 );
		memset(data,'a', req->block_size );
		data[ req->block_size ]='\0';
	
		for( ; req->bytes_processed < req->buffer_bytes_count ; ) {
			size_t size_to_process=req->buffer_bytes_count - req->bytes_processed ;
			if ( size_to_process > req->block_size ) {
				size_to_process = req->block_size;
			}
			wait_milliseconds(3000);
			if(req->cancel_request == MA_TRUE) { 
				req->base.request_state = MA_URL_REQUEST_STATE_CANCELLED;
				printf("request will be cancelled \n");
				free(data);
				//req->base.io_cbs->write_stream->vtable->get_size(req->base.io_cbs->write_stream, &req->range_info->start_position ) ; //still may not flushed
				req->range_info->start_position = req->bytes_processed;
				MA_ATOMIC_DECREMENT(handler->running_handles);        					
				return 0;
			}
			req->base.request_state=MA_URL_REQUEST_STATE_RECEIVING;		
			if( MA_URL_REQUEST_TYPE_POST == req->base.request_type ){ //upload			
				size_t size=0;
				req->base.io_cbs->read_stream->vtable->read(req->base.io_cbs->read_stream, data, size_to_process, &size);
				if(size != size_to_process){
					err=MA_ERROR_NETWORK_REQUEST_FAILED;
					break;
				}
				else {
					req->base.io_cbs->write_stream->vtable->write(req->base.io_cbs->write_stream, data, size_to_process, &size);
					req->bytes_processed += size_to_process ;
					printf("data uploaded ...... %d\n", req->bytes_processed) ;
				}
			}
			if( MA_URL_REQUEST_TYPE_GET == req->base.request_type ){ //download		
				size_t size=0;
				req->base.io_cbs->read_stream->vtable->read(req->base.io_cbs->read_stream, data, size_to_process, &size);
				if(size != size_to_process){
					err=MA_ERROR_NETWORK_REQUEST_FAILED;
					break;
				}
				else {
					req->base.io_cbs->write_stream->vtable->write(req->base.io_cbs->write_stream, data, size_to_process, &size);
					req->bytes_processed += size_to_process ;
					printf("data downloaded ...... %d\n", req->bytes_processed) ;
				}
			}
		}
		req->base.request_state = MA_URL_REQUEST_STATE_COMPLETED;
		free(data);
		MA_ATOMIC_DECREMENT(handler->running_handles) ;
	
		req->base.final_cb(MA_ERROR_NETWORK_REQUEST_SUCCEEDED, (MA_OK == err) ? 200 : 201, req->base.userdata, (ma_url_request_t*)req);   							
	}
	else
	{
		ma_spipe_url_request = (ma_spipe_url_request_t*)req->base.userdata;
		ma_spipe_url_request->is_cancelled = MA_TRUE;
		req->base.final_cb(MA_ERROR_NETWORK_REQUEST_FAILED, -1, req->base.userdata, (ma_url_request_t*)req);  
	}
	return 0;
}

#ifdef WIN32
static DWORD WINAPI ma_mock_request_handler_service(LPVOID pvoid)
{
	return handle_request((ma_mock_url_request_t*)pvoid);	
}

#else
static void* ma_mock_request_handler_service(void *pvoid)
{
	return handle_request((ma_mock_url_request_t*)pvoid);
	pthread_exit(NULL);
}
#endif

void wait_milliseconds( unsigned long wait_milli)
{
#ifdef WIN32
	Sleep(wait_milli);
#else
	sleep(wait_milli/1000);
#endif
}

static HANDLE ma_mock_request_handler_service_thread(ma_mock_url_request_t *request)
{	
	HANDLE  server_heandle;	
#ifdef WIN32		
	DWORD   thread_id=0;	
	server_heandle=NULL;		
	server_heandle=CreateThread(NULL,
				  0,
				  ma_mock_request_handler_service,
				  request,
				  0,
				  &thread_id);	
#else    
   pthread_attr_t attr;
   int ret;
   
   // Initialize and set thread detached attribute 
   pthread_attr_init(&attr);
   pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
   ret = pthread_create(&server_heandle, &attr, ma_mock_request_handler_service, (void *)request); 
   
   if(ret)
   {
	   //logmessage(LOG_ERROR,LOGGROUP,"Failed to start the discovery server thread");	
   }
   // Free attribute and wait for the other threads 
   pthread_attr_destroy(&attr);
#endif
	wait_milliseconds(1000);
	return server_heandle;
}

static ma_error_t ma_mock_request_handler_start(ma_mock_request_handler_t *self) ;
static ma_error_t ma_mock_request_handler_stop(ma_mock_request_handler_t *self) ;
static ma_error_t ma_mock_request_handler_is_running(ma_mock_request_handler_t *self,ma_bool_t*) ;
static ma_error_t ma_mock_request_handler_release(ma_mock_request_handler_t *self) ;	
static ma_error_t ma_mock_request_handler_create_request(ma_mock_request_handler_t *self, const char *url, ma_mock_url_request_t **url_request) ;
static ma_error_t ma_mock_request_handler_submit_request(ma_mock_request_handler_t *self, ma_mock_url_request_t *url_request, ma_url_request_finalize_callback_t completion_cb, void *userdata) ;
static ma_error_t ma_mock_request_handler_remove_request(ma_mock_request_handler_t *self , ma_mock_url_request_t *url_request) ;	
static ma_error_t ma_mock_request_handler_query_request_state(ma_mock_request_handler_t *self , ma_mock_url_request_t *url_request, ma_url_request_state_t *state) ;


static const struct ma_url_request_handler_methods_s request_handler_methods = {
    &ma_mock_request_handler_start ,
    &ma_mock_request_handler_stop ,
    &ma_mock_request_handler_is_running ,
    &ma_mock_request_handler_release ,
    &ma_mock_request_handler_create_request ,
    &ma_mock_request_handler_submit_request ,
    &ma_mock_request_handler_remove_request ,
    &ma_mock_request_handler_query_request_state
} ;

ma_error_t ma_mock_request_handler_create(ma_event_loop_t* event_loop, ma_url_request_handler_t** request_handler) {
    if(event_loop && request_handler) {
        ma_mock_request_handler_t *ma_mock_request_handler_tmp = (ma_mock_request_handler_t *)calloc(1, sizeof(ma_mock_request_handler_t)) ;
        if(!ma_mock_request_handler_tmp) 
            return MA_ERROR_OUTOFMEMORY ;

        ma_mock_request_handler_tmp->base.vtable = &request_handler_methods ;
        MA_ATOMIC_INCREMENT(ma_mock_request_handler_tmp->base.ref_count);
        ma_mock_request_handler_tmp->ma_loop = event_loop ;
        ma_mock_request_handler_tmp->running_handles = 0 ;
        ma_mock_request_handler_tmp->running_state = MA_FALSE ;
        ma_mock_request_handler_tmp->data = NULL ;
        *request_handler = (ma_url_request_handler_t *)ma_mock_request_handler_tmp ;
        return MA_OK ;
    }
    return MA_ERROR_INVALIDARG ;
}

ma_error_t ma_mock_request_handler_start(ma_mock_request_handler_t *self) {        
    if(MA_TRUE == self->running_state)
        return MA_OK ;
    self->running_state = MA_TRUE ;
    return MA_OK ;
}

ma_error_t ma_mock_request_handler_stop(ma_mock_request_handler_t *self) {        
    if(MA_FALSE == self->running_state)
        return MA_OK ;
    if(self->running_handles)
        return MA_ERROR_NETWORK_ACTIVE_HANDLES ;    
    self->running_state = MA_FALSE ;
    return MA_OK ;
}

ma_error_t ma_mock_request_handler_is_running(ma_mock_request_handler_t *self , ma_bool_t *running) {        
    *running = self->running_state ;
    return MA_OK ;
}

ma_error_t ma_mock_request_handler_release(ma_mock_request_handler_t *self) {
    if(self->running_handles || self->running_state )
        return MA_ERROR_NETWORK_ACTIVE_HANDLES ;
    MA_ATOMIC_DECREMENT(self->base.ref_count) ;
    if(0 == self->base.ref_count) {
        self->base.vtable = NULL ;
	    free(self) ;
        self = NULL ;
    }
    return MA_OK ;
}

ma_error_t ma_mock_request_handler_create_request(ma_mock_request_handler_t *self, const char *url, ma_mock_url_request_t **url_request) {
    ma_error_t rc = MA_OK ;	
    if(MA_FALSE == self->running_state)
        return MA_ERROR_NETWORK_SERVICE_NOT_STARTED ;

    if(MA_OK != (rc = ma_mock_url_request_create(url, (ma_mock_url_request_t **)url_request)) )
        return rc ;
	(*url_request)->base.url_handler = (ma_url_request_handler_t*)self ;
    return MA_OK ;
}

ma_error_t ma_mock_request_handler_submit_request(ma_mock_request_handler_t *self, ma_mock_url_request_t *url_request , ma_url_request_finalize_callback_t completion_cb, void *userdata) {
    ma_error_t ma_net_return = MA_OK ;
    ma_url_info_t *url_info = NULL ;		
    url_request->base.final_cb = completion_cb ;
    url_request->base.userdata = userdata ;
    url_request->base.request_state = MA_URL_REQUEST_STATE_INITIALIZING ;        

    if(MA_FALSE == self->running_state)
        return MA_ERROR_NETWORK_SERVICE_NOT_STARTED ;
	url_request->block_size = 10000;
	if(!( url_request->block_size ) ){
		return MA_ERROR_INVALID_OPTION;
	}
	if(! ( MA_URL_REQUEST_TYPE_POST == url_request->base.request_type || MA_URL_REQUEST_TYPE_GET == url_request->base.request_type ) ){
		return MA_ERROR_INVALID_OPTION;
	}
	if( MA_URL_REQUEST_TYPE_POST == url_request->base.request_type ) {
		size_t size = 0;
		if ( MA_OK == (ma_net_return=url_request->base.io_cbs->read_stream->vtable->get_size(url_request->base.io_cbs->read_stream, &size)) )
			url_request->buffer_bytes_count = size;
		else 
			return ma_net_return;
        url_request->bytes_processed = 0 ;
	}
	else {
		if(url_request && url_request->range_info && url_request->range_info->end_position)
				url_request->buffer_bytes_count=url_request->range_info->end_position+1;
		if(url_request && url_request->range_info )
			url_request->bytes_processed = url_request->range_info->start_position ;
	}
    if(url_request->cancel_request) url_request->cancel_request = MA_FALSE ;
    
	ma_mock_request_handler_service_thread(url_request);
    return MA_OK ;
}

ma_error_t ma_mock_request_handler_query_request_state(ma_mock_request_handler_t *self , ma_mock_url_request_t *url_request, ma_url_request_state_t *state) {
        
    if(MA_FALSE == self->running_state)
        return MA_ERROR_NETWORK_SERVICE_NOT_STARTED ;
    *state = url_request->base.request_state ;     //any logic to calculate the state ???
    return MA_OK ;
}

ma_error_t ma_mock_request_handler_remove_request(ma_mock_request_handler_t *self , ma_mock_url_request_t *url_request) {            
	 if(MA_FALSE == self->running_state)
        return MA_ERROR_NETWORK_SERVICE_NOT_STARTED ;

     url_request->cancel_request = MA_TRUE ;

     return MA_OK ;
}

MA_CPP(})

#endif  // MA_NETWORK_USE_MOCK