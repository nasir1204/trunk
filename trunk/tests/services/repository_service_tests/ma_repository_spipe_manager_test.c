/*****************************************************************************
Repository Service - Repository spipe manager UTs
******************************************************************************/

#include "unity.h"
#include "unity_fixture.h"
#include "unity_internals.h"
#include "ma_ut_setup.h"
#include "ma/internal/services/repository/ma_repository_spipe_manager.h"
#include "ma/internal/services/repository/ma_repository_service.h"
#include <string.h>

static ma_context_t *g_ma_context = NULL;
static ma_ut_setup_t *g_ut_setup = NULL;

static ma_service_t *repo_service = NULL;

TEST_GROUP_RUNNER(repository_spipe_manager_test_group)
{
    do {RUN_TEST_CASE(repository_spipe_manager_tests, repository_spipe_manager_test)} while (0);
}

TEST_SETUP(repository_spipe_manager_tests)
{
	/* Set up ut context*/
	TEST_ASSERT(MA_OK == ma_ut_setup_create("repository_service.tests", "ma.service.repository.test",&g_ut_setup));
    g_ma_context = ma_ut_setup_get_context(g_ut_setup);

	TEST_ASSERT(MA_OK == ma_repository_service_create("Repository Service Tests", &repo_service));
	//TEST_ASSERT(MA_OK == ma_service_configure(repo_service, g_ma_context,256));
	//TEST_ASSERT_EQUAL_INT(MA_OK, ma_service_start(repo_service));

}

TEST_TEAR_DOWN(repository_spipe_manager_tests)
{
	//TEST_ASSERT_EQUAL_INT(MA_OK, ma_service_stop(repo_service));
	ma_service_release(repo_service);
//	ma_ut_setup_release(g_ut_setup);
}


TEST(repository_spipe_manager_tests, repository_spipe_manager_test)
{
	ma_repository_spipe_manager_t *repo_spipe_manager = NULL;
	TEST_ASSERT(MA_OK == ma_repository_spipe_manager_create((ma_repository_service_t*)repo_service, &repo_spipe_manager) );
	
	//ma_repository_raise_spipe_alert(repo_spipe_manager) ;

	TEST_ASSERT(MA_OK == ma_repository_spipe_manager_release(repo_spipe_manager) );

	/*
	ma_error_t ma_repository_spipe_manager_create(ma_repository_service_t *service, ma_repository_spipe_manager_t **repo_spipe_manager) ;

	ma_error_t ma_repository_spipe_manager_release(ma_repository_spipe_manager_t *self) ;

	ma_spipe_decorator_t * ma_repository_spipe_manager_get_decorator(ma_repository_spipe_manager_t *self) ;

	ma_spipe_handler_t * ma_repository_spipe_manager_get_handler(ma_repository_spipe_manager_t *self) ;

	ma_error_t ma_repository_raise_spipe_alert(ma_repository_spipe_manager_t *self) ;

	*/
	
	
}



