/*****************************************************************************
Repository Service - Mirror Request UTs
******************************************************************************/

#include "unity.h"
#include "unity_fixture.h"
#include "unity_internals.h"
#include "ma_ut_setup.h"
#include "ma/internal/services/repository/ma_mirror_request_param.h"
#include "ma/internal/services/repository/ma_mirror_process.h"
#include "ma/internal/services/repository/ma_mirror_request.h"
#include <string.h>

static ma_context_t *g_ma_context = NULL;
static ma_ut_setup_t *g_ut_setup = NULL;

TEST_GROUP_RUNNER(mirror_request_test_group)
{
    do {RUN_TEST_CASE(mirror_request_tests, mirror_request_test)} while (0);
}

TEST_SETUP(mirror_request_tests)
{
	/* Set up ut context*/
	ma_ut_setup_create("repository_service.tests", "ma.service.repository.test",&g_ut_setup);
    g_ma_context = ma_ut_setup_get_context(g_ut_setup);
}

TEST_TEAR_DOWN(mirror_request_tests)
{
//	ma_ut_setup_release(g_ut_setup);
}

ma_error_t request_complete_cb(ma_mirror_request_t *request, ma_repository_mirror_state_t state, void *cb_data)
{
    return MA_OK;	
}

TEST(mirror_request_tests, mirror_request_test)
{
	ma_mirror_request_param_t my_param = { MA_MIRROR_REQUEST_TYPE_STOP, 
											 MA_REPOSITORY_MIRROR_INITIATOR_TYPE_ONDEMAND_MIRROR,
											   "Mirror Task 1_00001" };
	ma_mirror_request_t *request = NULL;

	ma_mirror_request_create(g_ma_context, &my_param, &request);

	ma_mirror_request_start(request, request_complete_cb, NULL);

	{
		ma_repository_mirror_state_t *status = NULL;
		ma_mirror_request_get_status(request, status);
		if (status)
		TEST_ASSERT(*status >= 0);
	}

	ma_mirror_request_stop(request);
	ma_mirror_request_release(request);


/*
	ma_error_t ma_mirror_request_create(ma_context_t *context, ma_mirror_request_param_t *param, ma_mirror_request_t **request);

	ma_error_t ma_mirror_request_start(ma_mirror_request_t *request, ma_mirror_request_complete_cb_t cb, void *cb_data);

	ma_error_t ma_mirror_request_stop(ma_mirror_request_t *request);

	ma_error_t ma_mirror_request_release(ma_mirror_request_t *request);

	ma_error_t ma_mirror_request_get_status(ma_mirror_request_t *request, ma_repository_mirror_state_t *status);
*/

}



