/*****************************************************************************
Repository Service - Mirror Request Parameters to/from variant conversion UTs
******************************************************************************/

#include "unity.h"
#include "unity_fixture.h"
#include "unity_internals.h"
#include "ma/internal/services/repository/ma_mirror_request_param.h"
#include <string.h>

TEST_GROUP_RUNNER(mirror_request_param_test_group)
{
    do {RUN_TEST_CASE(mirror_request_param_tests, mirror_request_param_to_from_test)} while (0);
}

TEST_SETUP(mirror_request_param_tests)
{
}

TEST_TEAR_DOWN(mirror_request_param_tests)
{
}


TEST(mirror_request_param_tests, mirror_request_param_to_from_test)
{
	ma_variant_t *variant_ptr = NULL;
	ma_mirror_request_param_t new_param;

	{
		ma_mirror_request_param_t my_param = { MA_MIRROR_REQUEST_TYPE_STOP, 
											   MA_REPOSITORY_MIRROR_INITIATOR_TYPE_ONDEMAND_MIRROR,
											   "Mirror Task 1_00001" };
	
		// Converting from Mirror Parameters to Variant
		TEST_ASSERT(MA_ERROR_INVALIDARG == ma_mirror_request_param_to_variant(my_param, NULL));
		TEST_ASSERT(MA_OK == ma_mirror_request_param_to_variant(my_param, &variant_ptr));

		// Fetching from Variant to Mirror Parameters
		TEST_ASSERT(MA_ERROR_INVALIDARG == ma_mirror_request_param_from_variant(NULL, &new_param));
		TEST_ASSERT(MA_ERROR_INVALIDARG == ma_mirror_request_param_from_variant(variant_ptr, NULL));
		TEST_ASSERT(MA_OK == ma_mirror_request_param_from_variant(variant_ptr, &new_param));
		TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

		TEST_ASSERT(MA_MIRROR_REQUEST_TYPE_STOP == new_param.request_type);
		TEST_ASSERT(MA_REPOSITORY_MIRROR_INITIATOR_TYPE_ONDEMAND_MIRROR == new_param.initiator_type);
		TEST_ASSERT(0 == strcmp("Mirror Task 1_00001", new_param.task_id));
	}

	{
		ma_mirror_request_param_t my_param = { MA_MIRROR_REQUEST_TYPE_STATUS_CHECK, 
											   MA_REPOSITORY_MIRROR_INITIATOR_TYPE_UNKNOWN,
											   "Mirror Task 2_00100" };
	
		// Converting from Mirror Parameters to Variant
		TEST_ASSERT(MA_ERROR_INVALIDARG == ma_mirror_request_param_to_variant(my_param, NULL));
		TEST_ASSERT(MA_OK == ma_mirror_request_param_to_variant(my_param, &variant_ptr));

		// Fetching from Variant to Mirror Parameters
		TEST_ASSERT(MA_ERROR_INVALIDARG == ma_mirror_request_param_from_variant(NULL, &new_param));
		TEST_ASSERT(MA_ERROR_INVALIDARG == ma_mirror_request_param_from_variant(variant_ptr, NULL));
		TEST_ASSERT(MA_OK == ma_mirror_request_param_from_variant(variant_ptr, &new_param));
		TEST_ASSERT(MA_OK == ma_variant_release(variant_ptr));

		TEST_ASSERT(MA_MIRROR_REQUEST_TYPE_STATUS_CHECK == new_param.request_type);
		TEST_ASSERT(MA_REPOSITORY_MIRROR_INITIATOR_TYPE_UNKNOWN == new_param.initiator_type);
		TEST_ASSERT(0 == strcmp("Mirror Task 2_00100", new_param.task_id));
	}

}



