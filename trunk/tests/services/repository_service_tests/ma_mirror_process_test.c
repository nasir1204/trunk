/*****************************************************************************
Repository Service - Mirror Process UTs
******************************************************************************/

#include "unity.h"
#include "unity_fixture.h"
#include "unity_internals.h"
#include "ma_ut_setup.h"
#include "ma/ma_errors.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/internal/services/repository/ma_mirror_request_param.h"
#include "ma/internal/services/repository/ma_mirror_process.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/utils/filesystem/path.h"
#include <string.h>

#include "ma/datastore/ma_ds.h"

#ifdef MA_WINDOWS
	#include "ma/datastore/ma_ds_registry.h"
    #include <windows.h>
#else
#endif

#ifdef MA_WINDOWS
#define TEST_AGENT_ROOT	".\\AGENT\\"
#define TEST_AGENT_DATA_PATH ".\\AGENT\\"
#define TEST_AGENT_SETUP_DATA_PATH ".\\AGENT\\db"
#define TEST_AGENT_INSTALL_PATH ".\\AGENT\\"
#else
#define TEST_AGENT_ROOT	"./AGENT/"
#define TEST_AGENT_DATA_PATH "./AGENT/"
#define TEST_AGENT_SETUP_DATA_PATH "./AGENT/db"
#define TEST_AGENT_INSTALL_PATH "./AGENT/"
#endif

#ifdef _WIN64
#define MA_AGENT_BIT_AFFINITY 2
#elif _WIN32
#define MA_AGENT_BIT_AFFINITY 1
#else
#endif

#ifdef MA_WINDOWS
#define MA_REGISTRY_STORE_SCAN_PATH				"Software\\"
#define MA_REGISTRY_STORE_AGENT_SCAN_PATH		"McAfee_T1\\Agent\\"
#define MA_REGISTRY_STORE_APPLICATON_LIST_PATH	"McAfee_T1\\Agent\\Applications\\"
#define MA_REGISTRY_STORE_APPLICATON_LIST_NA_PATH	"Network Associates\\ePolicy Orchestrator\\Application Plugins\\"
#define MA_REGISTRY_STORE_XML_PATH				"Configuration"
#else
#define CMA_REGISTRY_STORE_SCAN_PATH			"./AGENT/"
#define MA_REGISTRY_STORE_SCAN_PATH				"./AGENT/"
#define MA_REGISTRY_STORE_AGENT_SCAN_PATH		"EPOAGENT3000"
#define MA_REGISTRY_STORE_APPLICATON_LIST_PATH	"./AGENT/"
#define MA_REGISTRY_STORE_XML_PATH				"Configuration"
#endif



#define MA_APPLICATION_INFO_DATA_DIR							"DataPath"
#define MA_APPLICATION_INFO_INSTALL_DIR							"InstallPath"
#define MA_APPLICATION_INTEGRATION_INFO_SOFTWARE_ID				"SoftwareId"

#define MA_APPLICATION_INTEGRATION_INFO_PROPERTY_PROVIDER		"PropertyProvider"
#define MA_APPLICATION_INTEGRATION_INFO_POLICY_CONSUMER			"PolicyConsumer"
#define MA_APPLICATION_INTEGRATION_INFO_SCHEDULER_CONSUMER		"SchedulerConsumer"
#define MA_APPLICATION_INTEGRATION_INFO_EVENT_GENERATOR			"EventGenerator"
#define MA_APPLICATION_INTEGRATION_INFO_DATACHANNEL_CONSUMER	"DatachannelConsumer"
#define MA_APPLICATION_INTEGRATION_INFO_UPDATER_CONSUMER		"UpdaterConsumer"
#define MA_APPLICATION_INTEGRATION_INFO_PLUGIN_PATH				"PluginPath"

static ma_context_t *g_ma_context = NULL;
static ma_ut_setup_t *g_ut_setup = NULL;

static void exit_cb(ma_mirror_process_t *mirror_process, int exit_status, int term_signal, void *cb_data) {
}

#define MA_MIRROR_EXE_NAME_STR				"ma_mirror_task"
#define MA_MAX_MIRROR_PROCESS_ARGS			12

#ifdef MA_WINDOWS
#define MA_MIRROR_EXE_SUFFIX_STR			".exe"
#else
#define MA_MIRROR_EXE_SUFFIX_STR			""
#endif

#ifdef MA_WINDOWS
ma_bool_t isSystem64Bit() {
#ifdef MA_WINDOWS

    UINT size = 0;
    HMODULE hm_k32 = (HMODULE) 0;
    FARPROC proc;
	TCHAR tmp[1];
	OSVERSIONINFOW g_osvi = {0};
	ma_bool_t ret = MA_FALSE;

	g_osvi.dwOSVersionInfoSize = sizeof( OSVERSIONINFO );
    GetVersionEx(&g_osvi);
    // GetSystemWow64Directory() requires at least Win XP or Win 2003 Server
    // and 64-bit is potentially only on at least XP.
    if (g_osvi.dwMajorVersion >= 4 || (g_osvi.dwMajorVersion >= 3 && g_osvi.dwMinorVersion >= 1))
    {
        // Since delay loading Kernel32.dll is not supported, we need to
        // check for the presence of GetSystemWow64Directory manually.
        hm_k32 = LoadLibrary(L"Kernel32.dll");
        if (hm_k32)
        {
            proc = GetProcAddress(hm_k32, "GetSystemWow64DirectoryW");
            if (proc)
            {
                // GetSystemWow64Directory always fails on 32 bit operating systems.
                // It returns 0 on failure or the length, so we must give it a chance
                // to copy at least 1 character into the buffer so that we can dif-
                // ferentiate between an error code and a size of 0.
                size = ((UINT (__stdcall *)(LPTSTR, UINT)) proc)(tmp, 1);
                if (size > 0)
                    ret = MA_TRUE;
            }
            FreeLibrary(hm_k32);
        }
    }
    return ret;
#endif
	return MA_FALSE;
}

static void fill_agent_information(ma_ds_t *ds) {
	ma_ds_set_str(ds, MA_REGISTRY_STORE_AGENT_SCAN_PATH, MA_APPLICATION_INFO_DATA_DIR, TEST_AGENT_DATA_PATH, strlen(TEST_AGENT_DATA_PATH));
	ma_ds_set_str(ds, MA_REGISTRY_STORE_AGENT_SCAN_PATH, MA_APPLICATION_INFO_INSTALL_DIR, TEST_AGENT_INSTALL_PATH, strlen(TEST_AGENT_INSTALL_PATH));
}

static void create_application_list_in_x64_view(ma_ds_t *ds) {
	/*Created First APP*/
	ma_temp_buffer_t buffer;
	ma_temp_buffer_init(&buffer);

	form_path(&buffer, MA_REGISTRY_STORE_AGENT_SCAN_PATH, "Applications", MA_TRUE);
	form_path(&buffer, (char *) ma_temp_buffer_get(&buffer), "xApplication_1", MA_TRUE);
	ma_ds_set_str(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SOFTWARE_ID, "xApplication_1", strlen("xApplication_1"));
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_DATACHANNEL_CONSUMER, 1);
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_EVENT_GENERATOR, 1);

	form_path(&buffer, MA_REGISTRY_STORE_AGENT_SCAN_PATH, "Applications", MA_TRUE);
	form_path(&buffer, (char *) ma_temp_buffer_get(&buffer), "xApplication_2", MA_TRUE);
	ma_ds_set_str(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SOFTWARE_ID, "xApplication_2", strlen("xApplication_2"));
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_POLICY_CONSUMER, 1);
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_PROPERTY_PROVIDER, 1);

	form_path(&buffer, MA_REGISTRY_STORE_AGENT_SCAN_PATH, "Applications", MA_TRUE);
	form_path(&buffer, (char *) ma_temp_buffer_get(&buffer), "xApplication_3", MA_TRUE);
	ma_ds_set_str(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SOFTWARE_ID, "xApplication_3", strlen("xApplication_3"));
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SCHEDULER_CONSUMER, 1);
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_UPDATER_CONSUMER, 1);

	form_path(&buffer, MA_REGISTRY_STORE_AGENT_SCAN_PATH, "Applications", MA_TRUE);
	form_path(&buffer, (char *) ma_temp_buffer_get(&buffer), "xApplication_4", MA_TRUE);
	ma_ds_set_str(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SOFTWARE_ID, "xApplication_4", strlen("xApplication_4"));
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_DATACHANNEL_CONSUMER, 1);
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_EVENT_GENERATOR, 1);
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_POLICY_CONSUMER, 1);
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_PROPERTY_PROVIDER, 1);
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SCHEDULER_CONSUMER, 1);
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_UPDATER_CONSUMER, 1);

	ma_temp_buffer_uninit(&buffer);
}

static void create_application_list_in_x86_view(ma_ds_t *ds) {
	/*Created First APP*/
	ma_temp_buffer_t buffer;
	ma_temp_buffer_init(&buffer);

	form_path(&buffer, MA_REGISTRY_STORE_AGENT_SCAN_PATH, "Applications", MA_TRUE);
	form_path(&buffer, (char *) ma_temp_buffer_get(&buffer), "Application_1", MA_TRUE);
	ma_ds_set_str(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SOFTWARE_ID, "Application_1", strlen("Application_1"));
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_DATACHANNEL_CONSUMER, 1);
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_EVENT_GENERATOR, 1);

	form_path(&buffer, MA_REGISTRY_STORE_AGENT_SCAN_PATH, "Applications", MA_TRUE);
	form_path(&buffer, (char *) ma_temp_buffer_get(&buffer), "Application_2", MA_TRUE);
	ma_ds_set_str(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SOFTWARE_ID, "Application_2", strlen("Application_2"));
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_POLICY_CONSUMER, 1);
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_PROPERTY_PROVIDER, 1);

	form_path(&buffer, MA_REGISTRY_STORE_AGENT_SCAN_PATH, "Applications", MA_TRUE);
	form_path(&buffer, (char *) ma_temp_buffer_get(&buffer), "Application_3", MA_TRUE);
	ma_ds_set_str(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SOFTWARE_ID, "Application_3", strlen("Application_3"));
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SCHEDULER_CONSUMER, 1);
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_UPDATER_CONSUMER, 1);

	form_path(&buffer, MA_REGISTRY_STORE_AGENT_SCAN_PATH, "Applications", MA_TRUE);
	form_path(&buffer, (char *) ma_temp_buffer_get(&buffer), "Application_4", MA_TRUE);
	ma_ds_set_str(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SOFTWARE_ID, "Application_4", strlen("Application_4"));
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_DATACHANNEL_CONSUMER, 1);
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_EVENT_GENERATOR, 1);
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_POLICY_CONSUMER, 1);
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_PROPERTY_PROVIDER, 1);
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_SCHEDULER_CONSUMER, 1);
	ma_ds_set_int(ds, (char *) ma_temp_buffer_get(&buffer), MA_APPLICATION_INTEGRATION_INFO_UPDATER_CONSUMER, 1);

	ma_temp_buffer_uninit(&buffer);
}


static void populate_applications(ma_ds_t *ds, ma_bool_t is_64_bit_os, ma_bool_t default_view){
	/*
	 * First create the Agent information DATAPATH and INSTALLPATH
	 * In x86 Os, create directly
	 * In x64 Os, create in default view if Agent Affinity is x64
	 * In x64 OS, create in x86 view if Agent Affinity is x86
	 */
	if(default_view) {
		fill_agent_information(ds);
	}
	switch(MA_AGENT_BIT_AFFINITY) {
		case 1: // x86
			{
				if(is_64_bit_os && !default_view) {
					create_application_list_in_x64_view(ds);
				}
				else if(is_64_bit_os && default_view) {
					create_application_list_in_x86_view(ds);
				}
			}
			break;
		case 2: //x64
			{
				if(is_64_bit_os && default_view) {
					create_application_list_in_x64_view(ds);
				}
				else if(is_64_bit_os && !default_view) {
					create_application_list_in_x86_view(ds);
				}
			}
			break;
		default:
			break;
	}
}


static void do_registry_setup() {
	char command[1024];
#ifdef MA_WINDOWS
	ma_ds_registry_t *g_registry_default = NULL;
	#ifdef MA_WINDOWS
		MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s %s", "rmdir /S /Q " , TEST_AGENT_ROOT);
	#else
		MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s %s", "rm -rf " , TEST_AGENT_ROOT);
	#endif
	system(command);
	if(MA_OK == ma_ds_registry_open(HKEY_LOCAL_MACHINE,MA_REGISTRY_STORE_SCAN_PATH, MA_TRUE, MA_TRUE, KEY_ALL_ACCESS, &g_registry_default)) {
		populate_applications((ma_ds_t *) g_registry_default, isSystem64Bit(), MA_TRUE);
		ma_ds_registry_release(g_registry_default);
	}
	if(isSystem64Bit() == MA_TRUE) {
		if(MA_OK == ma_ds_registry_open(HKEY_LOCAL_MACHINE,MA_REGISTRY_STORE_SCAN_PATH, MA_FALSE, MA_TRUE, KEY_ALL_ACCESS, &g_registry_default)) {
			populate_applications((ma_ds_t *) g_registry_default, isSystem64Bit(), MA_FALSE);
			ma_ds_registry_release(g_registry_default);
		}
	}
#endif

    MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s %s", "mkdir" , TEST_AGENT_ROOT);
    system(command);
    MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s %s", "mkdir" , TEST_AGENT_SETUP_DATA_PATH);
    system(command);
    MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s %s", "mkdir" , TEST_AGENT_INSTALL_PATH);
    system(command);

#ifdef MA_WINDOWS
	MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s", "CD");
#else
	MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s", "pwd");
#endif
    system(command);

#ifndef MA_WINDOWS
	add_agent_configuration(MA_REGISTRY_STORE_SCAN_PATH);
	add_software_to_configuration(MA_REGISTRY_STORE_SCAN_PATH, "Application_1", MA_REGISTERED_DATACHANNEL_SERVICE | MA_REGISTERED_EVENT_SERVICE);
	add_software_to_configuration(MA_REGISTRY_STORE_SCAN_PATH, "Application_2", MA_REGISTERED_POLICY_SERVICE | MA_REGISTERED_PROPERTY_SERVICE);
	add_software_to_configuration(MA_REGISTRY_STORE_SCAN_PATH, "Application_3", MA_REGISTERED_SCHEDULER_SERVICE| MA_REGISTERED_UPDATER_SERVICE);
	add_software_to_configuration(MA_REGISTRY_STORE_SCAN_PATH, "Application_4", MA_REGISTERED_ALL);
	add_software_to_configuration(MA_REGISTRY_STORE_SCAN_PATH, "xApplication_1", MA_REGISTERED_DATACHANNEL_SERVICE | MA_REGISTERED_EVENT_SERVICE);
	add_software_to_configuration(MA_REGISTRY_STORE_SCAN_PATH, "xApplication_2", MA_REGISTERED_POLICY_SERVICE | MA_REGISTERED_PROPERTY_SERVICE);
	add_software_to_configuration(MA_REGISTRY_STORE_SCAN_PATH, "xApplication_3", MA_REGISTERED_SCHEDULER_SERVICE| MA_REGISTERED_UPDATER_SERVICE);
	add_software_to_configuration(MA_REGISTRY_STORE_SCAN_PATH, "xApplication_4", MA_REGISTERED_ALL);
#endif
}

static void do_registry_cleanup() {
	char command[1024]={0};
#ifdef MA_WINDOWS
	ma_ds_registry_t *g_registry_default = NULL;
	if(MA_OK == ma_ds_registry_open(HKEY_LOCAL_MACHINE,MA_REGISTRY_STORE_SCAN_PATH, MA_TRUE, MA_TRUE, KEY_ALL_ACCESS, &g_registry_default)) {
		ma_ds_rem((ma_ds_t*)g_registry_default, "McAfee_T1", NULL, MA_TRUE);
		ma_ds_registry_release(g_registry_default);
	}
	if(isSystem64Bit() == MA_TRUE) {
		if(MA_OK == ma_ds_registry_open(HKEY_LOCAL_MACHINE,MA_REGISTRY_STORE_SCAN_PATH, MA_FALSE, MA_TRUE, KEY_ALL_ACCESS, &g_registry_default)) {
			ma_ds_rem((ma_ds_t *)g_registry_default, "McAfee_T1", NULL, MA_TRUE);
			ma_ds_registry_release(g_registry_default);
		}
	}
	MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s %s", "rmdir /S /Q" , TEST_AGENT_ROOT);
#else
	MA_MSC_SELECT(_snprintf, snprintf)(command, 1024,"%s %s", "rm -rf " , TEST_AGENT_ROOT);
#endif
	system(command);
}

#endif

TEST_GROUP_RUNNER(mirror_process_test_group)
{
    do {RUN_TEST_CASE(mirror_process_tests, mirror_process_test)} while (0);
}

TEST_SETUP(mirror_process_tests)
{
	/* Set up ut context*/
	ma_ut_setup_create("repository_service.tests", "ma.service.repository.test",&g_ut_setup);
    g_ma_context = ma_ut_setup_get_context(g_ut_setup);
	//do_registry_setup();

}

TEST_TEAR_DOWN(mirror_process_tests)
{
	//do_registry_cleanup();
}


TEST(mirror_process_tests, mirror_process_test)
{
	ma_bool_t is_running;
	ma_mirror_process_t *mirror_process;
	
	//const char* args="";
	char *args[MA_MAX_MIRROR_PROCESS_ARGS] = {0};			
	int argv_index = 0;
	const char *agent_install_path = ma_configurator_get_agent_install_path(MA_CONTEXT_GET_CONFIGURATOR(g_ma_context));
	//const char *agent_install_path = "C:\\Program Files (x86)\\McAfee\\Agent";
	char mirror_exe_path[MAX_PATH_LEN] = {0};


	TEST_ASSERT(MA_OK == ma_mirror_process_create(g_ma_context, &mirror_process));
	TEST_ASSERT(MA_OK == ma_mirror_process_status(mirror_process, &is_running));

	//printf("\nAgent Install Path=%s\n",agent_install_path);
	MA_MSC_SELECT(_snprintf, snprintf)(mirror_exe_path, MAX_PATH_LEN, "%s%c%s%s", agent_install_path, MA_PATH_SEPARATOR, MA_MIRROR_EXE_NAME_STR, MA_MIRROR_EXE_SUFFIX_STR);
	//printf("\n%s\n",mirror_exe_path);
	
	args[argv_index++] = mirror_exe_path;
			args[argv_index++] = "-d";		
			args[argv_index++] = "-l";		//argv_index++] = mirror_log_path;
            args[argv_index++] = "-m";		//args[argv_index++] = (MA_AGENT_MODE_MANAGED_INT == agent_mode) ? "1" : "0";
			args[argv_index++] = "-v";		//args[argv_index++] = strlen(catalog_version) ? catalog_version : "0";
			args[argv_index++] = "-k";		//args[argv_index++] = strlen(license_key) ? license_key : "0";
			args[argv_index++] = 0;

//	TEST_ASSERT(MA_ERROR_MIRROR_PROCESS_SPAWN_FAILED == ma_mirror_process_start(mirror_process,(const char*) mirror_exe_path,args,exit_cb,NULL));
	//TEST_ASSERT(MA_OK == ma_mirror_process_terminate(mirror_process));
	TEST_ASSERT(MA_OK == ma_mirror_process_release(mirror_process));

	/*
	ma_error_t ma_mirror_process_create(ma_context_t *context, ma_mirror_process_t **mirror_process);

	ma_error_t ma_mirror_process_status(ma_mirror_process_t *mirror_process, ma_bool_t *is_running);

	ma_error_t ma_mirror_process_start(ma_mirror_process_t *mirror_process, const char *exe_path, const char **args, ma_mirror_exit_cb_t exit_cb, void *cb_data);

	ma_error_t ma_mirror_process_stop(ma_mirror_process_t *mirror_process);

	ma_error_t ma_mirror_process_terminate(ma_mirror_process_t *mirror_process);

	ma_error_t ma_mirror_process_release(ma_mirror_process_t *mirror_process);
	*/

}
