/*****************************************************************************
Repository Rank Unit Tests.
******************************************************************************/

#include "unity.h"
#include "unity_fixture.h"
#include "unity_internals.h"
#include "ma/internal/services/repository/ma_repository_rank.h"
#include <string.h>

TEST_GROUP_RUNNER(repository_rank_test_group)
{
    do {RUN_TEST_CASE(repository_rank_tests, repository_rank_create_release_test)} while (0);
}

TEST_SETUP(repository_rank_tests)
{
}

TEST_TEAR_DOWN(repository_rank_tests)
{
}

/**************************************************************************
Repository Rank Tests
**************************************************************************/
TEST(repository_rank_tests, repository_rank_create_release_test)
{
	ma_repository_service_t *repo_service = NULL;
	ma_repository_rank_t *repo_rank = NULL;

	TEST_ASSERT(MA_OK == ma_repository_service_create("repository.service.test", (ma_service_t**) &repo_service));
	
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_rank_create(NULL, &repo_rank));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_rank_create(repo_service, NULL));
	TEST_ASSERT(MA_OK == ma_repository_rank_create(repo_service, &repo_rank));

	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_rank_release(NULL));
	TEST_ASSERT(MA_OK == ma_repository_rank_release(repo_rank));
}


