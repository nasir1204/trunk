/*****************************************************************************
Repository Policy Unit Tests.
******************************************************************************/

#include "unity.h"
#include "unity_fixture.h"
#include "unity_internals.h"
#include "ma_ut_setup.h"
#include "ma/internal/services/repository/ma_repository_policy.h"
#include "ma/internal/services/repository/ma_repository_service.h"

static ma_context_t *g_ma_context = NULL;
static ma_ut_setup_t *g_ut_setup = NULL;

static ma_service_t *repo_service = NULL;


TEST_GROUP_RUNNER(repository_policy_test_group)
{
    do {RUN_TEST_CASE(repository_policy_tests, repository_policy_create_release_test)} while (0);
}

TEST_SETUP(repository_policy_tests)
{
	/* Set up ut context*/
	TEST_ASSERT(MA_OK == ma_ut_setup_create("repository_service.tests", "ma.service.repository.test",&g_ut_setup));
    g_ma_context = ma_ut_setup_get_context(g_ut_setup);

	TEST_ASSERT(MA_OK == ma_repository_service_create("Repository Service Tests", &repo_service));
	//TEST_ASSERT(MA_OK == ma_service_configure(repo_service, g_ma_context,256));
	//TEST_ASSERT_EQUAL_INT(MA_OK, ma_service_start(repo_service));

}

TEST_TEAR_DOWN(repository_policy_tests)
{
	//TEST_ASSERT_EQUAL_INT(MA_OK, ma_service_stop(repo_service));
	ma_service_release(repo_service);
}

/**************************************************************************
Repository Policy Tests
**************************************************************************/
TEST(repository_policy_tests, repository_policy_create_release_test)
{
	ma_repository_policy_t *repo_policy = NULL;

	//TEST_ASSERT(MA_OK == ma_repository_service_create("repository.service.test", &repo_service));
	//ma_repository_service_configure(repo_service,g_ma_context, 256);
	
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_policy_create(NULL, &repo_policy,256));
	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_policy_create((ma_repository_service_t *)repo_service, NULL,256));
	TEST_ASSERT(MA_OK == ma_repository_policy_create((ma_repository_service_t *)repo_service, &repo_policy,256));

	TEST_ASSERT(MA_OK == ma_repository_policy_update(repo_policy,256));

	TEST_ASSERT(MA_ERROR_INVALIDARG == ma_repository_policy_release(NULL));
	TEST_ASSERT(MA_OK == ma_repository_policy_release(repo_policy));
}


