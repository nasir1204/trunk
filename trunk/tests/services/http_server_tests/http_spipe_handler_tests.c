#include "ma/internal/services/http_server/ma_http_server.h"
#include "ma/internal/core/msgbus/ma_msgbus_private.h"
#include "ma/internal/services/http_server/ma_spipe_handler.h"
#include "ma_spipe_handler_internal.h"
#include "ma/ma_msgbus.h"
#include "ma/internal/core/msgbus/ma_msgbus_private.h"
#include "ma/internal/utils/macrypto/ma_crypto.h"

#include "unity.h"
#include "unity_fixture.h"

#include "tests/utils/macrypto_test/ma_crypto_tests_setup.h"
#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/logger/ma_outputdebugstring_logger.h"

static ma_logger_t *logger = 0;
static ma_msgbus_t *msgbus = 0;
static ma_crypto_t *crypto = 0;

TEST_GROUP_RUNNER(spipe_handler_test_group) {
    do {RUN_TEST_CASE(spipe_tests, agent_wakup_tests)} while (0);
	
}


TEST_SETUP(spipe_tests) {

	ma_msgbus_create("HTTP_SERVER_TESTS", &msgbus);

	create_macrypto_object_with_keys(MA_CRYPTO_MODE_NON_FIPS, MA_CRYPTO_ROLE_USER, MA_CRYPTO_AGENT_MANAGED, &crypto);
	

    ma_outputdebugstring_logger_create(&logger);

}

TEST_TEAR_DOWN(spipe_tests) {

	release_macrypto_object(MA_CRYPTO_MODE_NON_FIPS, MA_CRYPTO_ROLE_OFFICER, MA_CRYPTO_AGENT_MANAGED, crypto);

	ma_msgbus_release(msgbus);

    ma_logger_release(logger), logger = 0;

}

TEST(spipe_tests, agent_wakup_tests) {
    ma_http_server_t *http_server;
    ma_http_request_handler_factory_t *handler_factory; 
	ma_event_loop_t *ma_ev_loop = 0;

    ma_http_server_create(&http_server);
	ma_http_server_set_event_loop(http_server, ma_msgbus_get_event_loop(msgbus));

	ma_spipe_handler_factory_create(http_server, &handler_factory);

	//ma_spipe_handler_factory_set_msgbus(handler_factory, msgbus);
	ma_spipe_handler_factory_set_crypto(handler_factory, crypto);

    ma_http_server_add_request_handler_factory(http_server, handler_factory);
    ma_http_server_set_logger(http_server, logger);

    ma_http_server_start(http_server);	
	
	ma_msgbus_start(msgbus);

	ma_msgbus_stop(msgbus, MA_TRUE);

    ma_http_server_release(http_server);
}

