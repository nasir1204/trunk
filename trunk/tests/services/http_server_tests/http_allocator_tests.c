#include "ma_http_allocator.h"

#include "unity.h"
#include "unity_fixture.h"

#include <string.h>


#define TEST_POOL_SIZE 1024

TEST_GROUP_RUNNER(http_allocator_test_group) {
    do {RUN_TEST_CASE(allocator_tests, create_destroy)} while (0);
    do {RUN_TEST_CASE(allocator_tests, fill_single)} while (0);
    do {RUN_TEST_CASE(allocator_tests, fill_two)} while (0);
    do {RUN_TEST_CASE(allocator_tests, fill_single_reset_compare)} while (0);
    do {RUN_TEST_CASE(allocator_tests, alloc_two_reset_compare)} while (0);
    
    do {RUN_TEST_CASE(allocator_tests, fill_two_within_chunk)} while (0);
    //do {RUN_TEST_CASE(allocator_tests, fill_two)} while (0);
    //do {RUN_TEST_CASE(allocator_tests, fill_two)} while (0);
}



TEST_SETUP(allocator_tests) {


}

TEST_TEAR_DOWN(allocator_tests) {


}

TEST(allocator_tests, create_destroy) {
    ma_http_allocator_t *alloc = ma_http_allocator_create(TEST_POOL_SIZE);
    TEST_ASSERT(alloc);
    ma_http_allocator_destroy(alloc);
}

const static char fill_buffer[TEST_POOL_SIZE] = "The lazy brown fox and a lot of his friends decided to go on a different day and a different route. They shouldn't have done that because they met the man, see http://en.wikipedia.org/wiki/Dixie%27s_BBQ for the glory details";

TEST(allocator_tests, fill_single) {
    ma_http_allocator_t *alloc = ma_http_allocator_create(TEST_POOL_SIZE);
    void *p;
    TEST_ASSERT(alloc);
    p = ma_http_allocator_alloc(alloc, TEST_POOL_SIZE);
    TEST_ASSERT(p);
    memcpy(p, fill_buffer, TEST_POOL_SIZE);
    ma_http_allocator_destroy(alloc);
}


TEST(allocator_tests, fill_two) {
    ma_http_allocator_t *alloc = ma_http_allocator_create(TEST_POOL_SIZE);
    void *p;
    TEST_ASSERT(alloc);
    
    p = ma_http_allocator_alloc(alloc, TEST_POOL_SIZE);
    TEST_ASSERT(p);
    memcpy(p, fill_buffer, TEST_POOL_SIZE);

    p = ma_http_allocator_alloc(alloc, TEST_POOL_SIZE);
    TEST_ASSERT(p);
    memcpy(p, fill_buffer, TEST_POOL_SIZE);

    ma_http_allocator_destroy(alloc);
}

TEST(allocator_tests, fill_single_reset_compare) {
    ma_http_allocator_t *alloc = ma_http_allocator_create(TEST_POOL_SIZE);
    void *p, *q;
    TEST_ASSERT(alloc);
    
    p = ma_http_allocator_alloc(alloc, TEST_POOL_SIZE);
    TEST_ASSERT(p);
    memcpy(p, fill_buffer, TEST_POOL_SIZE);

    ma_http_allocator_reset(alloc);

    q = ma_http_allocator_alloc(alloc, TEST_POOL_SIZE);

    TEST_ASSERT_EQUAL_PTR(p, q);

    ma_http_allocator_destroy(alloc);
}

TEST(allocator_tests, alloc_two_reset_compare) {
    ma_http_allocator_t *alloc = ma_http_allocator_create(TEST_POOL_SIZE);
    void *p1, *p2, *q1, *q2;
    TEST_ASSERT(alloc);

    p1 = ma_http_allocator_alloc(alloc, TEST_POOL_SIZE);
    TEST_ASSERT(p1);

    p2 = ma_http_allocator_alloc(alloc, TEST_POOL_SIZE);
    TEST_ASSERT(p2);

    TEST_ASSERT_NOT_EQUAL(p1, p2);

    ma_http_allocator_reset(alloc);

    q1 = ma_http_allocator_alloc(alloc, TEST_POOL_SIZE);

    q2 = ma_http_allocator_alloc(alloc, TEST_POOL_SIZE);

    TEST_ASSERT_NOT_EQUAL(q1, q2);

    TEST_ASSERT(q1 == p1 || q1 == p2);

    TEST_ASSERT(q2 == p1 || q2 == p2);


    ma_http_allocator_destroy(alloc);
}


TEST(allocator_tests, fill_two_within_chunk) {
    ma_http_allocator_t *alloc = ma_http_allocator_create(TEST_POOL_SIZE);
    const static size_t num_elements = TEST_POOL_SIZE/2;
    void *p, *q;
    TEST_ASSERT(alloc);

    p = memcpy(ma_http_allocator_alloc(alloc, TEST_POOL_SIZE/2), fill_buffer, num_elements);

    q = memcpy(ma_http_allocator_alloc(alloc, TEST_POOL_SIZE/2), fill_buffer, num_elements);

    TEST_ASSERT_EQUAL_MEMORY_ARRAY(p, q, 1, num_elements);

    ma_http_allocator_destroy(alloc);

}
