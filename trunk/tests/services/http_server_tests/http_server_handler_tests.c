#include "ma/internal/services/http_server/ma_http_server.h"
#include "ma/internal/services/http_server/ma_ssl_proxy_handler.h"
#include "ma/internal/services/http_server/ma_http_log_file_handler.h"
#include "ma/internal/services/http_server/ma_http_proxy_handler.h"
#include "ma/internal/services/http_server/ma_http_repository_handler.h"
#include "ma/internal/services/http_server/ma_p2p_handler.h"
#include "ma/internal/services/ma_service.h"

#include "ma/internal/utils/macrypto/ma_crypto.h"
#include "ma/internal/utils/network/ma_net_client_service.h"

#include "unity.h"
#include "unity_fixture.h"

#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/logger/ma_outputdebugstring_logger.h"

#include "ma/ma_service.h"

#include "ma/internal/defs/ma_object_defs.h"
#include "ma_ut_setup.h"
#include "ma/internal/utils/configurator/ma_configurator.h"
#include "ma/internal/services/ma_policy_settings_bag.h"


static ma_event_loop_t *ma_ev_loop = 0;
static ma_logger_t *logger = 0;
static ma_http_server_t *http_server;

ma_context_t *g_ma_context = NULL;
ma_ut_setup_t *g_ut_setup = NULL;

TEST_GROUP_RUNNER(service_test_group) {
    do {RUN_TEST_CASE(all_handler_tests, verify_service_test)} while (0);
	do {RUN_TEST_CASE(all_handler_tests, verify_log_file_handler_test)} while (0);
	do {RUN_TEST_CASE(all_handler_tests, verify_proxy_handler_test)} while (0);
	do {RUN_TEST_CASE(all_handler_tests, verify_repository_handler_test)} while (0);
	do {RUN_TEST_CASE(all_handler_tests, verify_p2p_handler_test)} while (0);
}



TEST_SETUP(all_handler_tests) {

    TEST_ASSERT(MA_OK == ma_event_loop_create(&ma_ev_loop));

    ma_outputdebugstring_logger_create(&logger);

	/* Set up ut context*/
	ma_ut_setup_create("http_server.tests", "ma.service.http_server.test", &g_ut_setup);
    g_ma_context = ma_ut_setup_get_context(g_ut_setup);


}

TEST_TEAR_DOWN(all_handler_tests) {

    TEST_ASSERT(MA_OK == ma_event_loop_release(ma_ev_loop));

    ma_logger_release(logger), logger = 0;

}

static void idle_cb(uv_idle_t *h, int status) {

    ma_http_server_stop(http_server);

    uv_idle_stop(h);
}

TEST(all_handler_tests, verify_service_test) {
	ma_service_t *http_service = NULL;
	struct ma_logger_s *logger = NULL;

    //ma_http_server_service_create(char const *service_name, ma_service_t **service)
	//ma_http_server_service_create("http_service", &http_service);
	TEST_ASSERT(MA_OK == ma_http_server_create(&http_server));
	//service_configure(http_service, g_ma_context, 256)

	

	TEST_ASSERT(MA_OK == ma_http_server_set_event_loop(http_server, ma_ev_loop));

	
	TEST_ASSERT(MA_OK == ma_http_server_set_logger(http_server, MA_CONTEXT_GET_LOGGER(g_ma_context)));

	TEST_ASSERT(MA_OK == ma_http_server_set_datastore(http_server, ma_configurator_get_datastore(MA_CONTEXT_GET_CONFIGURATOR(g_ma_context))));
    TEST_ASSERT(MA_OK == ma_http_server_set_database(http_server, ma_configurator_get_database(MA_CONTEXT_GET_CONFIGURATOR(g_ma_context))));
	//TEST_ASSERT(MA_OK == ma_http_server_config(http_server, MA_CONTEXT_GET_POLICY_SETTINGS_BAG(g_ma_context), 256)); 

	//TEST_ASSERT(MA_OK == ma_http_server_start(http_server));

	//TEST_ASSERT(MA_OK == ma_http_server_restart(http_server));

//	TEST_ASSERT(MA_OK == ma_http_server_stop(http_server));

	TEST_ASSERT(MA_OK == ma_http_server_release(http_server));
}


TEST(all_handler_tests, verify_log_file_handler_test) {
	ma_http_request_handler_factory_t *log_file_handler_factory = NULL;

	TEST_ASSERT(MA_OK == ma_http_server_create(&http_server));
	
	TEST_ASSERT(MA_OK == ma_log_file_handler_factory_create(http_server, &log_file_handler_factory));
	TEST_ASSERT(MA_OK == ma_http_server_add_request_handler_factory(http_server, log_file_handler_factory));

	TEST_ASSERT(MA_OK == ma_http_server_release(http_server));
}

TEST(all_handler_tests, verify_proxy_handler_test) {
	ma_http_request_handler_factory_t *http_proxy_factory = NULL;

	TEST_ASSERT(MA_OK == ma_http_server_create(&http_server));
	
	TEST_ASSERT(MA_OK == ma_http_proxy_handler_factory_create(http_server, &http_proxy_factory));
	TEST_ASSERT(MA_OK == ma_http_proxy_handler_factory_set_event_loop(http_proxy_factory, ma_ev_loop));
	TEST_ASSERT(MA_OK == ma_http_server_add_request_handler_factory(http_server, http_proxy_factory));

	TEST_ASSERT(MA_OK == ma_http_server_release(http_server));
}

TEST(all_handler_tests, verify_repository_handler_test) {
	ma_http_request_handler_factory_t *http_repository_factory = NULL;

	TEST_ASSERT(MA_OK == ma_http_server_create(&http_server));
	
	TEST_ASSERT(MA_OK == ma_http_repository_handler_factory_create(http_server, &http_repository_factory));
	//TEST_ASSERT(MA_OK == ma_http_repository_handler_factory_set_net_client(http_repository_factory, MA_CONTEXT_GET_NET_CLIENT(g_ma_context)));
    //TEST_ASSERT(MA_OK == ma_http_repository_handler_factory_set_crypto(http_repository_factory, MA_CONTEXT_GET_CRYPTO(g_ma_context)));
    //TEST_ASSERT(MA_OK == ma_http_repository_handler_factory_set_client(http_repository_factory, MA_CONTEXT_GET_CLIENT(g_ma_context)));
    //TEST_ASSERT(MA_OK == ma_http_repository_handler_factory_set_event_loop(http_repository_factory, ma_ev_loop));
    //TEST_ASSERT(MA_OK == ma_http_repository_handler_factory_set_logger(http_repository_factory, MA_CONTEXT_GET_LOGGER(g_ma_context)));
    //TEST_ASSERT(MA_OK == ma_http_repository_handler_facotry_set_msgbus(http_repository_factory, MA_CONTEXT_GET_MSGBUS(g_ma_context)));
	TEST_ASSERT(MA_OK == ma_http_server_add_request_handler_factory(http_server, http_repository_factory));

	TEST_ASSERT(MA_OK == ma_http_server_release(http_server));
}

TEST(all_handler_tests, verify_p2p_handler_test) {
	ma_http_request_handler_factory_t *http_p2p_factory = NULL;

	TEST_ASSERT(MA_OK == ma_http_server_create(&http_server));
	
	TEST_ASSERT(MA_OK == ma_p2p_handler_factory_create(http_server, &http_p2p_factory));
	TEST_ASSERT(MA_OK == ma_p2p_handler_factory_set_context(http_p2p_factory, g_ma_context));
	TEST_ASSERT(MA_OK == ma_http_server_add_request_handler_factory(http_server, http_p2p_factory));

	TEST_ASSERT(MA_OK == ma_http_server_release(http_server));
}