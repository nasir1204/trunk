#define UNITY_FIXTURE_MALLOC_OVERRIDES_H_
#include "ma/internal/services/http_server/ma_http_server_utils.h"
#include "ma/internal/utils/filesystem/path.h"

#include "ma/internal/utils/filesystem/ma_fs_utils.h"

#include "unity.h"
#include "unity_fixture.h"

#include <string.h>
#include <stdio.h>

TEST_GROUP_RUNNER(http_server_utils_test_group) {
    do {RUN_TEST_CASE(utils_tests, url_str_tests)} while (0);   
}

#ifdef MA_WINDOWS
#define ROOT_DIR                    "C:\\SA\\DATA\\"
#else
#define ROOT_DIR                    "/opt/McAfee/data/"
#endif

#define DOWNLOAD_URL_REQUEST        "/Software/Current/VSCANDAT1000/datdet.mcs"
#define DOWNLOAD_URL_PATH           "/Software/Current/VSCANDAT1000/datdet.mcs"
#define DOWNLOAD_FILE_NAME          MA_PATH_SEPARATOR_STR"Current"MA_PATH_SEPARATOR_STR"VSCANDAT1000"MA_PATH_SEPARATOR_STR"datdet.mcs"
#define DOWNLOAD_REPLICA_LOG        MA_PATH_SEPARATOR_STR"Current"MA_PATH_SEPARATOR_STR"VSCANDAT1000"MA_PATH_SEPARATOR_STR"Replica.log"

#define LOCAL_FILE_PATH             ROOT_DIR DOWNLOAD_FILE_NAME

#define UPLOAD_URL_BASE     "/spipe/file?URL=/Software/Previous\\VSCANDAT1000\\DAT\\0000\\PkgCatalog.z"
#define UPLOAD_URL_QUERY    "URL=/Software/Previous\\VSCANDAT1000\\DAT\\0000\\PkgCatalog.z"
#define UPLOAD_URL_PATH     "/Software/Previous\\VSCANDAT1000\\DAT\\0000\\PkgCatalog.z"

TEST_SETUP(utils_tests) {
  
}

TEST_TEAR_DOWN(utils_tests) {
    
}
uv_loop_t *uv_loop;
 
TEST(utils_tests, url_str_tests) {
    uv_buf_t download_url_base, upload_url_base;
    char url_path[MA_HTTP_MAX_URL_PATH_LEN] = {0};
    char query[MA_HTTP_MAX_URL_PATH_LEN] = {0};
    char query_value[MA_HTTP_MAX_URL_PATH_LEN] = {0};
    char file[MA_HTTP_MAX_URL_PATH_LEN] = {0};
    char replica_log[MA_HTTP_MAX_URL_PATH_LEN] = {0};
    char local_file_path[MA_HTTP_MAX_URL_PATH_LEN] = {0};
    ma_bool_t b_rc;
   // char *p;

    download_url_base = uv_buf_init(DOWNLOAD_URL_REQUEST, strlen(DOWNLOAD_URL_REQUEST));
    upload_url_base = uv_buf_init(UPLOAD_URL_BASE, strlen(UPLOAD_URL_BASE));

    b_rc = get_url_path_from_url(&download_url_base, url_path, MA_HTTP_MAX_URL_PATH_LEN, 0);
    TEST_ASSERT(b_rc && !strcmp(url_path, DOWNLOAD_URL_PATH));

    b_rc = get_query_from_url(&upload_url_base, query, MA_HTTP_MAX_URL_PATH_LEN, 0);
    TEST_ASSERT(b_rc && !strcmp(query, UPLOAD_URL_QUERY));

    b_rc = get_query_value_from_url(&upload_url_base, "URL", query_value, MA_HTTP_MAX_URL_PATH_LEN, 0);
    TEST_ASSERT(b_rc && !strcmp(query_value, UPLOAD_URL_PATH));

    b_rc = get_file_from_url_path(url_path, file, MA_HTTP_MAX_URL_PATH_LEN);
    TEST_ASSERT(b_rc && !strcmp(file, DOWNLOAD_FILE_NAME));

    b_rc = get_replica_log_from_file(file, replica_log, MA_HTTP_MAX_URL_PATH_LEN);
    TEST_ASSERT(b_rc && !strcmp(replica_log, DOWNLOAD_REPLICA_LOG));
   
    b_rc = get_local_file_path_for_file(file, ROOT_DIR, local_file_path, MA_HTTP_MAX_URL_PATH_LEN);
    TEST_ASSERT(b_rc);

    uv_loop = uv_default_loop();
     
    /*memset(&file, 0, MA_HTTP_MAX_URL_PATH_LEN);
    strcpy(file, "\\Sitestate.xml");
    p = strrchr(file, MA_PATH_SEPARATOR);
    *p = 0;
    if(strlen(file) > 1)
    ma_fs_make_recursive_dir(uv_loop, ROOT_DIR, file);
    *p = MA_PATH_SEPARATOR;*/
}