#include "ma/internal/services/http_server/ma_http_server.h"
#include "ma/internal/services/http_server/ma_ssl_proxy_handler.h"

#include "unity.h"
#include "unity_fixture.h"

#include "ma/internal/utils/event_loop/ma_event_loop.h"
#include "ma/logger/ma_outputdebugstring_logger.h"


ma_event_loop_t *ma_ev_loop = 0;
static ma_logger_t *logger = 0;
static ma_http_server_t *http_server;

TEST_GROUP_RUNNER(default_test_group) {
    do {RUN_TEST_CASE(all_tests, verify_server_start_stop)} while (0);
}



TEST_SETUP(all_tests) {

    ma_event_loop_create(&ma_ev_loop);

    ma_outputdebugstring_logger_create(&logger);

}

TEST_TEAR_DOWN(all_tests) {

    ma_event_loop_release(ma_ev_loop);

    ma_logger_release(logger), logger = 0;

}

static void idle_cb(uv_idle_t *h, int status) {

    ma_http_server_stop(http_server);

    uv_idle_stop(h);
}

static uv_idle_t idle_watcher;

TEST(all_tests, verify_server_start_stop) {
    ma_http_request_handler_factory_t *ssl_proxy_factory;
    
    
    ma_http_server_create(&http_server);
	ma_http_server_set_event_loop(http_server, ma_ev_loop);

    ma_ssl_proxy_handler_factory_create(http_server, &ssl_proxy_factory);

    ma_http_server_add_request_handler_factory(http_server, ssl_proxy_factory);

    ma_http_server_set_event_loop(http_server, ma_ev_loop); 
    ma_http_server_set_logger(http_server, logger);

    ma_http_server_start(http_server);

    uv_idle_init(ma_event_loop_get_uv_loop(ma_ev_loop), &idle_watcher);
    uv_idle_start(&idle_watcher, &idle_cb);

    ma_event_loop_run(ma_ev_loop);

    ma_http_server_release(http_server);
}

