#include "ma/internal/services/http_server/ma_http_server.h"


#include "unity.h"
#include "unity_fixture.h"
#include "ma/ma_common.h"

#include <signal.h>

#ifdef MA_WINDOWS
#include <windows.h>
#endif

#ifdef _MSC_VER
# include <crtdbg.h> /* */
# define MA_TRACKLEAKS _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF)
#else
# define MA_TRACKLEAKS ((void *)0)
#endif


static void sighandler(int num) {

}

static void run_all_tests(void) {
    do { RUN_TEST_GROUP(default_test_group); } while (0);
    do { RUN_TEST_GROUP(http_allocator_test_group) ; } while (0);
    do { RUN_TEST_GROUP(http_server_utils_test_group) ; } while (0);
	//do { RUN_TEST_GROUP(spipe_handler_test_group); } while (0);
	do { RUN_TEST_GROUP(service_test_group) ; } while (0);
}

int main(int argc, char *argv[]) {
    MA_TRACKLEAKS;
    signal (SIGINT,sighandler);
    return UnityMain(argc, argv, run_all_tests);
}


